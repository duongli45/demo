package com.qupworld.service;

import com.devebot.opflow.OpflowBuilder;
import com.devebot.opflow.OpflowConfig;
import com.devebot.opflow.OpflowPubsubHandler;
import com.devebot.opflow.exception.OpflowBootstrapException;
import com.google.gson.Gson;
import com.pg.util.CommonUtils;
import com.pg.util.TimezoneUtil;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
public class QUpReportPublisher {

  private static final Logger LOG = LogManager.getLogger(
    QUpReportPublisher.class
  );
  public static QUpReportPublisher instance = null;
  private OpflowPubsubHandler handler;
  final SimpleDateFormat sdfMongo = new SimpleDateFormat(
    "EEE MMM dd HH:mm:ss Z yyyy"
  );

  public static QUpReportPublisher getInstance() {
    if (instance == null) {
      synchronized (QUpReportPublisher.class) {
        if (instance == null) {
          instance = new QUpReportPublisher();
        }
      }
    }
    return instance;
  }

  private QUpReportPublisher() {
    try {
      checkHandler();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  private void checkHandler() throws OpflowBootstrapException {
    if (handler == null) {
      Map<String, Object> properties = OpflowConfig.loadConfiguration(
        null,
        "opflow.properties",
        false
      );
      LOG.debug("QUpReportPublisher properties:   " + properties);
      handler =
        OpflowBuilder.createPubsubHandler((Map) properties.get("report"));
    }
  }

  public boolean sendListItems(String requestId, String data) {
    try {
      Gson gson = new Gson();
      JSONObject jsonObject = new JSONObject();
      jsonObject.put("from", "POST_ITEMS");
      jsonObject.put("body", gson.fromJson(data, JSONArray.class));
      jsonObject.put("traceRequestId", requestId);
      jsonObject.put(
        "receivedTime",
        TimezoneUtil.formatISODate(
          TimezoneUtil.convertServerToGMT(new Date()),
          "GMT"
        )
      );
      LOG.debug(requestId + " - sendBookingCompleted request: " + jsonObject);
      checkHandler();
      handler.publish(jsonObject.toJSONString());
      LOG.debug(requestId + " - done !");
      return true;
    } catch (Exception ex) {
      LOG.debug(requestId + " - exception: " + CommonUtils.getError(ex));
      return false;
    }
  }

  public boolean sendOneItem(
    String requestId,
    String id,
    String type,
    String data
  ) {
    try {
      Gson gson = new Gson();
      JSONObject jsonObject = new JSONObject();
      jsonObject.put("from", "PUT_ONE_ITEM");
      jsonObject.put("body", gson.fromJson(data, JSONObject.class));
      jsonObject.put("traceRequestId", requestId);
      jsonObject.put(
        "receivedTime",
        TimezoneUtil.formatISODate(
          TimezoneUtil.convertServerToGMT(new Date()),
          "GMT"
        )
      );
      JSONObject params = new JSONObject();
      params.put("id", id);
      params.put("datatype", type);
      jsonObject.put("params", params);

      LOG.debug(
        requestId + " - send " + type + "(" + id + ") -  data : " + jsonObject
      );
      checkHandler();
      handler.publish(jsonObject.toJSONString());
      LOG.debug(requestId + " - send " + type + "(" + id + ") is done !");
      return true;
    } catch (Exception ex) {
      LOG.debug(
        requestId +
        " - send " +
        type +
        "(" +
        id +
        ") exception: " +
        CommonUtils.getError(ex)
      );
      return false;
    }
  }

  public void close() {
    handler.close();
  }
}
