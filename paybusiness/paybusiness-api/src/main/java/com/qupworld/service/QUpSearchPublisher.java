package com.qupworld.service;

import com.devebot.opflow.OpflowBuilder;
import com.devebot.opflow.OpflowConfig;
import com.devebot.opflow.OpflowPubsubHandler;
import com.devebot.opflow.exception.OpflowBootstrapException;
import com.google.gson.Gson;
import com.qupworld.paymentgateway.models.mongo.collections.account.Account;
import com.qupworld.paymentgateway.models.mongo.collections.booking.Booking;
import com.pg.util.TimezoneUtil;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

/**
 * Created by hoangnguyen on 3/28/19.
 */
public class QUpSearchPublisher {

  private static final Logger LOG = LogManager.getLogger(
    QUpSearchPublisher.class
  );
  public static QUpSearchPublisher instance = null;
  private OpflowPubsubHandler handler;
  final SimpleDateFormat sdfMongo = new SimpleDateFormat(
    "EEE MMM dd HH:mm:ss Z yyyy"
  );

  public static QUpSearchPublisher getInstance() {
    if (instance == null) {
      synchronized (QUpSearchPublisher.class) {
        if (instance == null) {
          instance = new QUpSearchPublisher();
        }
      }
    }
    return instance;
  }

  private QUpSearchPublisher() {
    try {
      checkHandler();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  private void checkHandler() throws OpflowBootstrapException {
    if (handler == null) {
      Map<String, Object> properties = OpflowConfig.loadConfiguration(
        null,
        "opflow.properties",
        false
      );
      LOG.debug("QUpSearchPublisher properties:   " + properties);
      handler =
        OpflowBuilder.createPubsubHandler((Map) properties.get("search"));
    }
  }

  public void sendBookingCompleted(String requestId, Booking bookingCompleted) {
    try {
      JSONObject jsonObject = new JSONObject();
      jsonObject.put("from", "PUT_ONE_ITEM");
      jsonObject.put("traceRequestId", requestId);
      jsonObject.put(
        "receivedTime",
        TimezoneUtil.formatISODate(
          TimezoneUtil.convertServerToGMT(new Date()),
          "GMT"
        )
      );
      JSONObject params = new JSONObject();
      params.put("id", bookingCompleted._id);
      params.put("datatype", "BookingCompleted");
      jsonObject.put("params", params);
      Gson gson = new Gson();
      //re format date type
      bookingCompleted.createdDate = TimezoneUtil.convertServerToGMT(bookingCompleted.createdDate);
      bookingCompleted.latestUpdate = TimezoneUtil.convertServerToGMT(bookingCompleted.latestUpdate);
      bookingCompleted.finishedAt = TimezoneUtil.convertServerToGMT(bookingCompleted.finishedAt);

      if (bookingCompleted.time != null) {
        if (bookingCompleted.time.booked != null)
          bookingCompleted.time.booked = TimezoneUtil.convertServerToGMT(bookingCompleted.time.booked);
        if (bookingCompleted.time.created != null)
          bookingCompleted.time.created = TimezoneUtil.convertServerToGMT(bookingCompleted.time.created);
        if (bookingCompleted.time.pickUpTime != null)
          bookingCompleted.time.pickUpTime = TimezoneUtil.convertServerToGMT(bookingCompleted.time.pickUpTime);
        if (bookingCompleted.time.pending != null)
          bookingCompleted.time.pending = TimezoneUtil.convertServerToGMT(bookingCompleted.time.pending);
        if (bookingCompleted.time.queue != null)
          bookingCompleted.time.queue = TimezoneUtil.convertServerToGMT(bookingCompleted.time.queue);
        if (bookingCompleted.time.offered != null)
          bookingCompleted.time.offered = TimezoneUtil.convertServerToGMT(bookingCompleted.time.offered);
        if (bookingCompleted.time.engaged != null)
          bookingCompleted.time.engaged = TimezoneUtil.convertServerToGMT(bookingCompleted.time.engaged);
        if (bookingCompleted.time.droppedOff != null)
          bookingCompleted.time.droppedOff = TimezoneUtil.convertServerToGMT(bookingCompleted.time.droppedOff);
      }
      String json = gson.toJson(bookingCompleted);
      jsonObject.put("body", gson.fromJson(json, JSONObject.class));
      LOG.debug(requestId + " - start sendBookingCompleted");
      LOG.debug(requestId + " - request: " + jsonObject);
      checkHandler();
      handler.publish(jsonObject.toJSONString());
      LOG.debug(requestId + " - done !");
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void sendDataAccount(String requestId, Account account) {
    try {
      JSONObject jsonObject = new JSONObject();

      jsonObject.put("from", "PUT_ONE_ITEM");
      jsonObject.put("traceRequestId", requestId);
      jsonObject.put(
        "receivedTime",
        TimezoneUtil.formatISODate(
          TimezoneUtil.convertServerToGMT(new Date()),
          "GMT"
        )
      );
      JSONObject params = new JSONObject();
      params.put("id", account._id.toString());
      params.put("datatype", "Account");
      jsonObject.put("params", params);
      Gson gson = new Gson();
      //re format date type
      account.createdDate = TimezoneUtil.convertServerToGMT(account.createdDate);
      account.latestUpdate = TimezoneUtil.convertServerToGMT(account.latestUpdate);
      if (account.lastLogin != null && account.lastLogin.time != null) {
        account.lastLogin.time = TimezoneUtil.convertServerToGMT(account.lastLogin.time);
      }
      account._id = null;

      String json = gson.toJson(account);
      jsonObject.put("body", gson.fromJson(json, JSONObject.class));
      LOG.debug(requestId + " - start sendDataAccount");
      LOG.debug(requestId + " - request: " + jsonObject);
      checkHandler();
      handler.publish(jsonObject.toJSONString());
      LOG.debug(requestId + " - done !");
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void sendDataDriver(String requestId, Account account) {
    try {
      JSONObject jsonObject = new JSONObject();

      jsonObject.put("from", "PUT_ONE_ITEM");
      jsonObject.put("traceRequestId", requestId);
      jsonObject.put(
        "receivedTime",
        TimezoneUtil.formatISODate(
          TimezoneUtil.convertServerToGMT(new Date()),
          "GMT"
        )
      );
      JSONObject params = new JSONObject();
      params.put("id", account._id.toString());
      params.put("datatype", "Account");
      jsonObject.put("params", params);
      Gson gson = new Gson();
      JSONObject object = new JSONObject();
      object.put("userId", account.userId);
      object.put("fleetId", account.fleetId);
      object.put("appType", account.appType);
      object.put("phone", account.phone);
      object.put(
        "driverInfo",
        gson.fromJson(gson.toJson(account.driverInfo), JSONObject.class)
      );

      jsonObject.put("body", object);
      LOG.debug(requestId + " - start sendDataAccount");
      LOG.debug(requestId + " - request: " + jsonObject);
      checkHandler();
      handler.publish(jsonObject.toJSONString());
      LOG.debug(requestId + " - done !");
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void close() {
    handler.close();
  }
}
