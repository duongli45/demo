package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.qupworld.paymentgateway.entities.CreditEnt;
import com.qupworld.util.JetPayConnectionUtil;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.ValidCreditCard;
import com.qupworld.util.jetpay.TransactionResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class JetpayUtil {
    private DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#");
    private static final Logger logger = LogManager.getLogger(com.qupworld.paymentgateway.controllers.GatewayUtil.JetpayUtil.class);
    JetPayConnectionUtil jetPayConnectionUtil ;
    private String requestId;

    public JetpayUtil(String requestId) {
        this.requestId = requestId;
        jetPayConnectionUtil = new JetPayConnectionUtil(requestId);
    }

    public String createCreditToken(CreditEnt credit, Map<String, String> account) throws IOException {

        ObjResponse objResponse = new ObjResponse();
        String uniqueId = "";
        try {
            Boolean enableCheckZipCode = false;
            if (!isEmptyString(credit.postalCode)) {
                enableCheckZipCode = true;
            }
            String street = credit.street != null ? credit.street : "";
            String city = credit.city != null ? credit.city : "";
            String state = credit.state != null ? credit.state : "";
            String country = credit.country != null ? credit.country : "";
            if (enableCheckZipCode) {
                if (street.isEmpty() || city.isEmpty() || country.isEmpty()) {
                    objResponse.returnCode = 411;
                    return objResponse.toString();
                }
            }
            String[] date = credit.expiredDate.split("/");

            // remove UTF-8 accents
            String cardHolder = GatewayUtil.stripAccents(credit.cardHolder);
            street = GatewayUtil.stripAccents(street);
            city = GatewayUtil.stripAccents(city);
            state = GatewayUtil.stripAccents(state);
            country = GatewayUtil.stripAccents(country);
            country = GatewayUtil.getCountry3Code(country); // convert country name to code ISO-3

            Map<String,String> mapData = new HashMap<>();
            mapData.put("terminalId", account.get("terminalId"));
            mapData.put("cardHolder", cardHolder);
            mapData.put("cardNumber", credit.cardNumber);
            mapData.put("expiredMonth", date[0]);
            mapData.put("expiredYear", date[1].substring(2,4));
            mapData.put("cvv", credit.cvv);
            mapData.put("zipCode", credit.postalCode);
            mapData.put("customerEmail", account.get("customerEmail"));
            mapData.put("customerIP", account.get("customerIP"));
            mapData.put("phone", credit.phone != null ? credit.phone : "");
            mapData.put("street", street);
            mapData.put("city", city);
            mapData.put("state", state);
            mapData.put("country", country);

            TransactionResponse transactionResponse = jetPayConnectionUtil.createCreditToken(account.get("serverUrl"), mapData, enableCheckZipCode);

            // if verify CVV and AVS has failed, void this transaction
            String avsResponse = transactionResponse.getAvs() != null ? transactionResponse.getAvs() : "";
            String cvvResponse = transactionResponse.getCvv2() != null ? transactionResponse.getCvv2() : "";
            String verifyCode = getReturnCodeAVSCVV(avsResponse, cvvResponse, enableCheckZipCode, ValidCreditCard.getCardType(credit.cardNumber));
            String actionCode = transactionResponse.getActionCode();
            String transactionCode = getTransactionCode(actionCode, ValidCreditCard.getCardType(credit.cardNumber));
            uniqueId = transactionResponse.getUniqueID() != null ? transactionResponse.getUniqueID() : "";
            logger.debug(requestId + " - avsResponse: " + avsResponse + " --- cvvResponse: " + cvvResponse + " --- verifyCode: " + verifyCode);
            logger.debug(requestId + " - actionCode: " + actionCode + " --- transactionCode: " + transactionCode);
            Map<String,String> mapResult = new HashMap<>();
            if (!verifyCode.equals("200")) {
                voidJetpayTransaction(uniqueId, account.get("terminalId"), account.get("serverUrl"));
                String responseText = transactionResponse.getResponseText() != null ? transactionResponse.getResponseText() : "";
                mapResult.put("message", responseText );

                objResponse.returnCode = Integer.parseInt(verifyCode);
                objResponse.response = mapResult;
                return objResponse.toString();
            } else if (!transactionCode.equals("200")) {
                voidJetpayTransaction(uniqueId, account.get("terminalId"), account.get("serverUrl"));

                String responseText = transactionResponse.getResponseText() != null ? transactionResponse.getResponseText() : "";
                mapResult.put("message", responseText );

                objResponse.returnCode = Integer.parseInt(transactionCode);
                objResponse.response = mapResult;
                return objResponse.toString();
            }

            // void transaction to return the money
            voidJetpayTransaction(uniqueId, account.get("terminalId"), account.get("serverUrl"));

            // validate AVS and CVV2 is passed, retrieve data and return
            String cardMasked = "XXXXXXXX" + credit.cardNumber.substring(credit.cardNumber.length() - 4, credit.cardNumber.length());
            String cardType = ValidCreditCard.getCardType(credit.cardNumber);
            String token = transactionResponse.getToken() != null ? transactionResponse.getToken() : "";
            String authorizationCode = transactionResponse.getApproval() != null ? transactionResponse.getApproval() : "";
            String gatewayRejectionReason = transactionResponse.getResponseText() != null ? transactionResponse.getResponseText() : "";
            mapResult.put("message", "");
            mapResult.put("creditCard", cardMasked);
            mapResult.put("qrCode", "");
            mapResult.put("token", token);
            mapResult.put("cardType", cardType);
            mapResult.put("street", street);
            mapResult.put("city", city);
            mapResult.put("state", state);
            mapResult.put("zipCode", credit.postalCode);
            mapResult.put("country", country);

            objResponse.returnCode = 200;
            objResponse.response = mapResult;
            return objResponse.toString();
        } catch(Exception ex) {
            ex.printStackTrace();

            // get error, void completed transaction
            if (!uniqueId.equals("")) {
                voidJetpayTransaction(uniqueId, account.get("terminalId"), account.get("serverUrl"));
            }
            objResponse.returnCode = 437;
            objResponse.response = getError(ex);
            return objResponse.toString();
        }

    }
    public boolean isEmptyString(String s) {
        return (s == null || s.trim().isEmpty());
    }
    private String getReturnCodeAVSCVV(String avsResponse, String cvvResponse, boolean enableCheckZipCode, String cardType) {
        String errorCode = "437";
        // AVS code of Visa
        String validCode = "DFMPYZ";
        String inValidCode = "ABN";
        String noData = "GU";
        String systemUnable = "R";
        String notSupport = "";
        String notVerified = "CI";
        if ("MasterCard".equalsIgnoreCase(cardType)) {
            validCode = "DFMPWXYZ";
            inValidCode = "AN";
            noData = "U";
            systemUnable = "R";
            notSupport = "S";
            notVerified = "BCGI";
        } else if ("AmericanExpress".equalsIgnoreCase(cardType)) {
            validCode = "DEFKLMOYZ";
            inValidCode = "ANW";
            noData = "U";
            systemUnable = "R";
            notSupport = "S";
            notVerified = "";
        } else if ("Discover".equalsIgnoreCase(cardType)) {
            validCode = "YZ";
            inValidCode = "AN";
            noData = "";
            systemUnable = "";
            notSupport = "U";
            notVerified = "G";
        }

        avsResponse = avsResponse.trim();
        cvvResponse = cvvResponse.trim();

        // check both AVS and CVV is empty in case payment with credit token
        if (avsResponse.equals("") && cvvResponse.equals(""))
            errorCode = "200";

        /// COMMENT THIS BLOCK BECAUSE WE WILL CHECK AVS AS "N" OR NOT, AS THE CONFIRMATION FROM B2B
        /*if (!avsResponse.equals("")) {
            if (validCode.contains(avsResponse)) {
                errorCode = "200";
            } else if (inValidCode.contains(avsResponse)) {
                errorCode = "430";
            } else if (notVerified.contains(avsResponse)) {
                errorCode = "431";
            } else if (noData.contains(avsResponse)) {
                errorCode = "443";
            } else if (systemUnable.contains(avsResponse)) {
                errorCode = "445";
            } else if (notSupport.contains(avsResponse)) {
                errorCode = "446";
            } else {
                errorCode = "429";
            }
        } else {
            if (!enableCheckZipCode) {
                errorCode = "200";
            }
        }*/

        if (!avsResponse.equals("")) {
            if (avsResponse.equals("N")) {
                errorCode = "455";
            } else {
                errorCode = "200";
            }
        } else {
            if (!enableCheckZipCode) {
                errorCode = "200";
            }
        }

        if (errorCode.equals("200") && !cvvResponse.equals("")) {
            // AVS verified is pass, continue verify CVV
            if (cvvResponse.equalsIgnoreCase("M")) {
                errorCode = "200";
            } else if (cvvResponse.equalsIgnoreCase("N")) {
                errorCode = "433";
            } else if (cvvResponse.equalsIgnoreCase("P")) {
                errorCode = "439";
            } else if (cvvResponse.equalsIgnoreCase("S")) {
                errorCode = "432";
            } else if (cvvResponse.equalsIgnoreCase("U")) {
                errorCode = "441";
            } else {
                errorCode = "432";
            }
        }

        return errorCode;
    }

    private String returnPaymentFailed(String transId, String transType, String paymentType,
                                       String fleetId, String bookId, double amount,
                                       String errorCode, String errorMessage, String message, boolean isPreAuth) {
        ObjResponse response = new ObjResponse();
        Map<String, String> mapResponse = new HashMap<>();
        logger.debug(requestId + " - response: " + message);
        //mapResponse.put("response", message);
        mapResponse.put("message", errorMessage);
        response.returnCode = Integer.parseInt(errorCode);
        response.response = mapResponse;
        return response.toString();

    }
    public String getTransactionCode(String code, String cardType) {
        code = code.trim();
        String errorCode = "";
        switch (code) {
            case "000" :
                errorCode = "200"; // Approved.
                break;
            case "001" :
                errorCode = "2044"; // Declined - Call Issuer
                if ("AmericanExpress".equalsIgnoreCase(cardType))
                    errorCode = "200"; // Approve with ID.
                break;
            case "002" :
                errorCode = "2044"; // Declined - Call Issuer
                if ("AmericanExpress".equalsIgnoreCase(cardType))
                    errorCode = "200"; // Partial Approval.
                break;
            case "003" :
                errorCode = "2026"; // Invalid Merchant ID
                break;
            case "004" :
                errorCode = "2047"; // Call Issuer. Pick Up Card.
                break;
            case "005" :
                errorCode = "2000"; // Do Not Honor.
                break;
            case "006" :
                errorCode = "2043"; // Error - Do Not Retry, Call Issuer
                break;
            case "007" :
                errorCode = "2047"; // Call Issuer. Pick Up Card.
                break;
            case "011" :
                errorCode = "200"; // Approve.
                break;
            case "012" :
                errorCode = "2019"; // Invalid Transaction
                break;
            case "013" :
                errorCode = "2001"; // Insufficient Funds.
                if ("MasterCard".equalsIgnoreCase(cardType))
                    errorCode = "2048"; // Invalid Amount
                break;
            case "014" :
                errorCode = "2005"; // Invalid Credit Card Number
                break;
            case "015" :
                errorCode = "2009"; // No Such Issuer
                break;
            case "021" :
                errorCode = "2034"; // No action taken (unable to back out prior transaction).
                break;
            case "025" :
                errorCode = "2061"; // Invalid Transaction Data
                break;
            case "033" :
                errorCode = "2004"; // Expired Card
                break;
            case "035" :
                errorCode = "2014"; // Processor Declined - Fraud Suspected
                break;
            case "036" :
                errorCode = "2057"; // Issuer or Cardholder has put a restriction on the card
                break;
            case "040" :
                errorCode = "2023"; // Processor Does Not Support This Feature
                break;
            case "041" :
                errorCode = "2012"; // Processor Declined - Possible Lost Card
                break;
            case "043" :
                errorCode = "2013"; // Processor Declined - Possible Stolen Card
                break;
            case "051" :
                errorCode = "2001"; // Insufficient Funds.
                if ("Discover".equalsIgnoreCase(cardType))
                    errorCode = "2044"; // Declined - Call Issuer
                break;
            case "054" :
                errorCode = "2004"; // Expired Card
                break;
            case "057" :
                errorCode = "2015"; // Transaction Not Allowed
                break;
            case "058" :
                errorCode = "2015"; // Transaction Not Allowed
                break;
            case "059" :
                errorCode = "2014"; // Processor Declined - Fraud Suspected
                break;
            case "061" :
                errorCode = "2003"; // Cardholder's Activity Limit Exceeded
                break;
            case "062" :
                errorCode = "2057"; // Issuer or Cardholder has put a restriction on the card
                break;
            case "063" :
                errorCode = "2021"; // Security Violation
                break;
            case "065" :
                errorCode = "2003"; // Cardholder's Activity Limit Exceeded
                break;
            case "070" :
                errorCode = "2043"; // Error - Do Not Retry, Call Issuer
                break;
            case "082" :
                errorCode = "2010"; // Card Issuer Declined CVV
                break;
            case "085" :
                errorCode = "2038"; // Processor Declined.
                break;
            case "806" :
                errorCode = "2010"; // Card Issuer Declined CVV
                break;
            case "092" :
                errorCode = "200";
                break;
            case "100" :
                errorCode = "2044"; // Declined - Call Issuer
                break;
            case "101" :
                errorCode = "2004"; // Expired Card
                break;
            case "107" :
                errorCode = "2044"; // Declined - Call Issuer
                break;
            case "109" :
                errorCode = "2026"; // Invalid Merchant ID
                break;
            case "110" :
                errorCode = "2048"; // Invalid Amount
                break;
            case "115" :
                errorCode = "2015"; // Transaction Not Allowed
                break;
            case "122" :
                errorCode = "2010"; // Card Issuer Declined CVV
                break;
            case "200" :
                errorCode = "2047"; // Call Issuer. Pick Up Card.
                break;
            case "602" :
                errorCode = "2014"; // Processor Declined - Fraud Suspected
                break;
            case "901" :
                errorCode = "2026"; // Invalid Merchant ID
                break;
            default:
                errorCode = "437";
                break;
        }

        return errorCode;
    }
    public String voidJetpayTransaction(String uniqueId, String terminalId, String serverUrl) {
        logger.debug(requestId + " - voidJetpayTransaction for uniqueId " + uniqueId);
        ObjResponse response = new ObjResponse();
        try {
            Map<String,String> mapData = new HashMap<>();
            mapData.put("terminalId", terminalId);
            mapData.put("uniqueId", uniqueId);

            TransactionResponse transactionResponse = jetPayConnectionUtil.voiJetpayTransaction(serverUrl, mapData);
            logger.debug(requestId + " - actionCode: " + transactionResponse.getActionCode());
            String refundId = transactionResponse.getTransactionID() != null ? transactionResponse.getTransactionID() : "";
            response.returnCode = 200;
            logger.debug(requestId + " - uniqueId: " + uniqueId + " has been voided !!!! RefundId: " + refundId);
        } catch(Exception ex) {
            ex.printStackTrace();
            logger.debug("voidJetpayTransaction ERROR: " + getError(ex));
            response.returnCode = 304;
        }
        String result = response.toString();
        logger.debug(requestId + " - result: " + result);
        return result;
    }

    public String createACHToken(String bankNumber, String routingNumber, String firstName, String lastName, String checkNumber,
                                 String terminalId, String serverUrl) {
        ObjResponse achResponse = new ObjResponse();
        try {
            Map<String,String> mapData = new HashMap<>();
            mapData.put("terminalId", terminalId);
            mapData.put("cardName", firstName + " " + lastName);
            mapData.put("accountNumber", bankNumber);
            mapData.put("routingNumber", routingNumber);
            mapData.put("checkNumber", checkNumber);

            TransactionResponse transactionResponse = jetPayConnectionUtil.createACHToken(serverUrl, mapData);
            String actionCode = transactionResponse.getActionCode();
            String transactionCode = getTransactionCode(actionCode, "");
            logger.debug(requestId + " - actionCode: " + actionCode + " --- transactionCode: " + transactionCode);
            Map<String,String> mapResponse = new HashMap<>();
            if (transactionCode.equals("200")) {
                String token = transactionResponse.getToken();
                mapResponse.put("token", token);

                achResponse.returnCode = 200;
                achResponse.response = mapResponse;
            } else {
                String responseText = transactionResponse.getResponseText() != null ? transactionResponse.getResponseText() : "";
                mapResponse.put("message", responseText);
                achResponse.returnCode = 493;
                achResponse.response = mapResponse;
            }

            return achResponse.toString();
        } catch(Exception ex) {
            ex.printStackTrace();
            logger.debug(requestId + " - Exception Message is: " + ex.getMessage());
            achResponse.returnCode = 493;
            return achResponse.toString();

        }
    }

    public String createPaymentWithCreditToken(String fleetId, double amount, String token, String bookId, String terminalId, String serverUrl, String cardType,
                                               Map<String,String> customerData) {
        logger.debug(requestId + " - createPaymentWithCreditToken for bookId: " + bookId + " - token: " + token + " ...");
        ObjResponse response = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        String transId = "";
        String uniqueId = "";
        try {
            String street = customerData.get("street") != null ? customerData.get("street") : "";
            String city = customerData.get("city") != null ? customerData.get("city") : "";
            String state = customerData.get("state") != null ? customerData.get("state") : "";
            String zipCode = customerData.get("zipCode") != null ? customerData.get("zipCode") : "";
            String country = customerData.get("country") != null ? customerData.get("country") : "";
            if (country.length() > 3)
                country = GatewayUtil.getCountry3Code(country); // convert country name to code ISO-3

            Map<String,String> mapData = new HashMap<>();
            mapData.put("terminalId", terminalId);
            mapData.put("token", token);
            mapData.put("amount", DECIMAL_FORMAT.format(amount*100)); // amount payment in CENT
            mapData.put("customerEmail", customerData.get("customerEmail"));
            mapData.put("customerIP", customerData.get("customerIP"));
            mapData.put("phone", customerData.get("phone"));
            mapData.put("street", street);
            mapData.put("city", city);
            mapData.put("state", state);
            mapData.put("zipCode", zipCode);
            mapData.put("country", country);
            TransactionResponse transactionResponse = jetPayConnectionUtil.createPaymentWithCreditToken(serverUrl, mapData);

            // validate AVS and CVV2 is passed, retrieve data and return
            String avsResponse = transactionResponse.getAvs() != null ? transactionResponse.getAvs() : "";
            String cvvResponse = transactionResponse.getCvv2() != null ? transactionResponse.getCvv2() : "";
            String verifyCode = getReturnCodeAVSCVV(avsResponse, cvvResponse, false, cardType);
            String actionCode = transactionResponse.getActionCode();
            String transactionCode = getTransactionCode(actionCode, cardType);
            transId = transactionResponse.getTransactionID() != null ? transactionResponse.getTransactionID() : "";
            uniqueId = transactionResponse.getUniqueID() != null ? transactionResponse.getUniqueID() : "";
            logger.debug(requestId + " - avsResponse: " + avsResponse + " --- cvvResponse: " + cvvResponse + " --- verifyCode: " + verifyCode);
            logger.debug(requestId + " - actionCode: " + actionCode + " --- transactionCode: " + transactionCode);
            // if verify CVV and AVS has failed, void this transaction
            if (!verifyCode.equals("200")) {
                voidJetpayTransaction(uniqueId, terminalId, serverUrl);
                String responseText = transactionResponse.getResponseText() != null ? transactionResponse.getResponseText() : "";
                mapResponse.put("message", responseText);
                response.returnCode = Integer.parseInt(verifyCode);
                response.response = mapResponse;
                return response.toString();
            } else if (!transactionCode.equals("200")) {
                voidJetpayTransaction(uniqueId, terminalId, serverUrl);
                String responseText = transactionResponse.getResponseText() != null ? transactionResponse.getResponseText() : "";
                mapResponse.put("message", responseText);
                response.returnCode = Integer.parseInt(transactionCode);
                response.response = mapResponse;
                return response.toString();
            }

            String authorizationCode = transactionResponse.getApproval() != null ? transactionResponse.getApproval() : "";
            String gatewayRejectionReason = transactionResponse.getResponseText() != null ? transactionResponse.getResponseText() : "";

            Map<String,String> mapResult = new HashMap<>();
            mapResult.put("message", "Success!" );
            mapResult.put("transId", transId);
            mapResult.put("cardType", cardType);
            mapResult.put("authCode", authorizationCode);
            mapResult.put("cardMask", "");
            response.response = mapResult;
            response.returnCode = 200;
            return  response.toString();
        } catch(Exception ex) {
            ex.printStackTrace();

            // get error, void completed transaction
            if (!uniqueId.equals("")) {
                voidJetpayTransaction(uniqueId, terminalId, serverUrl);
            }
            return returnPaymentFailed(transId, "SALE", "token",
                    fleetId, bookId, amount,
                    "437", "", getError(ex), false);
        }
    }

    public String payByTerminal(Map<String,String> mapData) throws Exception {
        ObjResponse objResponse = new ObjResponse();

        Map<String, Object> transactionResponse = jetPayConnectionUtil.payByTerminal(mapData);
        String returnCode = transactionResponse.get("returnCode").toString();
        String message = transactionResponse.get("message").toString();
        Map<String,String> mapResponse = (Map<String, String>) transactionResponse.get("response");

        Map<String,Object> mapResult = new HashMap<>();
        mapResult.put("response", mapResponse);
        mapResult.put("message", message);
        objResponse.response = mapResult;
        objResponse.returnCode = Integer.parseInt(returnCode);
        logger.debug(requestId + " - payByTerminal: " + objResponse.toString());
        return objResponse.toString();
    }

    public String doPaymentWithCardInfo(String fleetId, String bookId, double amount,
                                        String cardNumber, String expiredDate, String cvv, String cardHolder, String postalCode, String terminalId, String serverUrl,
                                        String customerEmail, String customerIP, String phone, String street, String city, String state, String country) throws IOException {
        logger.debug(requestId + " - doPaymentWithCardInfo for card " + cardNumber.substring(cardNumber.length() - 4, cardNumber.length()) + " ...");
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        String transId = "";
        String uniqueId = "";
        try {
            Boolean enableCheckZipCode = false;
            if (!isEmptyString(postalCode)) {
                enableCheckZipCode = true;
            }
            if (enableCheckZipCode) {
                if (street.isEmpty() || city.isEmpty() || country.isEmpty()) {
                    objResponse.returnCode = 411;
                    return objResponse.toString();
                }
            }
            String[] date = expiredDate.split("/");

            // remove UTF-8 accents
            street = street != null ? GatewayUtil.stripAccents(street) : "";
            city = city != null ? GatewayUtil.stripAccents(city) : "";
            state = state != null ? GatewayUtil.stripAccents(state) : "";
            country = country != null ? GatewayUtil.stripAccents(country) : "";
            country = GatewayUtil.getCountry3Code(country); // convert country name to code ISO-3

            Map<String,String> mapData = new HashMap<>();
            mapData.put("terminalId", terminalId);
            mapData.put("cardHolder", cardHolder);
            mapData.put("cardNumber", cardNumber);
            mapData.put("expiredMonth", date[0]);
            mapData.put("expiredYear", date[1].substring(2,4));
            mapData.put("cvv", cvv);
            mapData.put("zipCode", postalCode);
            mapData.put("customerEmail", customerEmail);
            mapData.put("customerIP", customerIP);
            mapData.put("phone", phone);
            mapData.put("street", street);
            mapData.put("city", city);
            mapData.put("state", state);
            mapData.put("country", country);
            mapData.put("amount", DECIMAL_FORMAT.format(amount*100)); // amount payment in CENT

            TransactionResponse transactionResponse = jetPayConnectionUtil.createPaymentWithCardInfo(serverUrl, mapData, enableCheckZipCode);

            // if verify CVV and AVS has failed, void this transaction
            String avsResponse = transactionResponse.getAvs() != null ? transactionResponse.getAvs() : "";
            String cvvResponse = transactionResponse.getCvv2() != null ? transactionResponse.getCvv2() : "";
            String verifyCode = getReturnCodeAVSCVV(avsResponse, cvvResponse, enableCheckZipCode, ValidCreditCard.getCardType(cardNumber));
            String actionCode = transactionResponse.getActionCode();
            String transactionCode = getTransactionCode(actionCode, ValidCreditCard.getCardType(cardNumber));
            transId = transactionResponse.getTransactionID() != null ? transactionResponse.getTransactionID() : "";
            uniqueId = transactionResponse.getUniqueID() != null ? transactionResponse.getUniqueID() : "";
            logger.debug(requestId + " - avsResponse: " + avsResponse + " --- cvvResponse: " + cvvResponse + " --- verifyCode: " + verifyCode);
            logger.debug(requestId + " - actionCode: " + actionCode + " --- transactionCode: " + transactionCode);
            if (!verifyCode.equals("200")) {
                voidJetpayTransaction(uniqueId, terminalId, serverUrl);
                String responseText = transactionResponse.getResponseText() != null ? transactionResponse.getResponseText() : "";
                mapResponse.put("message", responseText);
                objResponse.returnCode = Integer.parseInt(verifyCode);
                objResponse.response = mapResponse;
                return objResponse.toString();
            } else if (!transactionCode.equals("200")) {
                voidJetpayTransaction(uniqueId, terminalId, serverUrl);
                String responseText = transactionResponse.getResponseText() != null ? transactionResponse.getResponseText() : "";
                mapResponse.put("message", responseText);
                objResponse.returnCode = Integer.parseInt(transactionCode);
                objResponse.response = mapResponse;
                return objResponse.toString();
            }

            // validate AVS and CVV2 is passed, retrieve data and return
            String cardMask = "XXXXXXXXXXXX" + cardNumber.substring(cardNumber.length() - 4, cardNumber.length());
            String cardType = ValidCreditCard.getCardType(cardNumber);
            String authorizationCode = transactionResponse.getApproval() != null ? transactionResponse.getApproval() : "";

            Map<String,String> mapResult = new HashMap<>();
            mapResult.put("message", "Success!" );
            mapResult.put("transId", transId);
            mapResult.put("cardType", cardType);
            mapResult.put("authCode", authorizationCode);
            mapResult.put("cardMask", cardMask);
            objResponse.response = mapResult;
            objResponse.returnCode = 200;
            return objResponse.toString();
        } catch(Exception ex) {
            ex.printStackTrace();

            // get error, void completed transaction
            if (!uniqueId.equals("")) {
                voidJetpayTransaction(uniqueId, terminalId, serverUrl);
            }
            return returnPaymentFailed(transId, "SALE", "token",
                    fleetId, bookId, amount,
                    "437", "", getError(ex), false);
        }

    }
    private String getError(Exception ex) {
        return GatewayUtil.getError(ex);
    }

    public String payToDriver(String fleetId, String token, String checkNumber, String name, double amount, String terminalId, String serverUrl) {
        ObjResponse objResponse = new ObjResponse();
        try {
            Map<String,String> mapData = new HashMap<>();
            mapData.put("terminalId", terminalId);
            mapData.put("cardName", name);
            mapData.put("checkNumber", checkNumber);
            mapData.put("amount", DECIMAL_FORMAT.format(amount*100)); // amount payment in CENT
            mapData.put("token", token);

            TransactionResponse transactionResponse = jetPayConnectionUtil.payToBankAccount(serverUrl, mapData);
            String actionCode = transactionResponse.getActionCode();
            String transactionCode = getTransactionCode(actionCode, "");
            logger.debug(requestId + " - actionCode: " + actionCode + " --- transactionCode: " + transactionCode);
            if (transactionCode.equals("200")) {
                String transId = transactionResponse.getTransactionID() != null ? transactionResponse.getTransactionID() : "";
                String authorizationCode = transactionResponse.getApproval() != null ? transactionResponse.getApproval() : "";
                String gatewayRejectionReason = transactionResponse.getResponseText() != null ? transactionResponse.getResponseText() : "";
                /*addGatewayTransaction(transId, "REVERSAL", "bank account",
                        fleetId, "", amount,
                        gatewayRejectionReason, "", "", actionCode,
                        "", "");*/

                Map<String,String> mapResult = new HashMap<>();
                mapResult.put("message", "Success!" );
                mapResult.put("transId", transId);
                mapResult.put("cardType", "");
                mapResult.put("authCode", authorizationCode);
                mapResult.put("cardMask", "");
                objResponse.response = mapResult;
                objResponse.returnCode = 200;
            } else {
                Map<String,String> mapResult = new HashMap<>();
                String responseText = transactionResponse.getResponseText() != null ? transactionResponse.getResponseText() : "";
                mapResult.put("message", responseText);
                objResponse.returnCode = Integer.parseInt(transactionCode);
                objResponse.response = mapResult;
            }
            return objResponse.toString();
        } catch(Exception ex) {
            ex.printStackTrace();
            objResponse.returnCode = 437;
            objResponse.response = getError(ex);
            return objResponse.toString();
        }
    }

    public String preAuthPayment(String fleetId, double amount, String token, String bookId, String terminalId, String serverUrl, Map<String,String> customerData) {
        ObjResponse preAuthResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        String transId = "";
        String uniqueId = "";
        try {
            TransactionResponse transactionResponse = jetPayConnectionUtil.preAuthPayment(terminalId, serverUrl, token, DECIMAL_FORMAT.format(amount * 100), customerData);

            // validate AVS and CVV2 is passed, retrieve data and return
            String avsResponse = transactionResponse.getAvs() != null ? transactionResponse.getAvs() : "";
            String cvvResponse = transactionResponse.getCvv2() != null ? transactionResponse.getCvv2() : "";
            String verifyCode = getReturnCodeAVSCVV(avsResponse, cvvResponse, false, customerData.get("cardType"));
            String actionCode = transactionResponse.getActionCode();
            String transactionCode = getTransactionCode(actionCode, customerData.get("cardType"));
            transId = transactionResponse.getTransactionID() != null ? transactionResponse.getTransactionID() : "";
            uniqueId = transactionResponse.getUniqueID() != null ? transactionResponse.getUniqueID() : "";
            logger.debug(requestId + " - avsResponse: " + avsResponse + " --- cvvResponse: " + cvvResponse + " --- verifyCode: " + verifyCode);
            logger.debug(requestId + " - actionCode: " + actionCode + " --- transactionCode: " + transactionCode);
            // if verify CVV and AVS has failed, void this transaction
            if (!verifyCode.equals("200")) {
                voidJetpayTransaction(uniqueId, terminalId, serverUrl);
                String responseText = transactionResponse.getResponseText() != null ? transactionResponse.getResponseText() : "";
                mapResponse.put("message", responseText);
                preAuthResponse.returnCode = Integer.parseInt(verifyCode);
                preAuthResponse.response = mapResponse;
                return preAuthResponse.toString();
            } else if (!transactionCode.equals("200")) {
                voidJetpayTransaction(uniqueId, terminalId, serverUrl);
                String responseText = transactionResponse.getResponseText() != null ? transactionResponse.getResponseText() : "";
                mapResponse.put("message", responseText);
                preAuthResponse.returnCode = Integer.parseInt(transactionCode);
                preAuthResponse.response = mapResponse;
                return preAuthResponse.toString();
            }

            String authorizationCode = transactionResponse.getApproval() != null ? transactionResponse.getApproval() : "";
            String gatewayRejectionReason = transactionResponse.getResponseText() != null ? transactionResponse.getResponseText() : "";

            // authId include 2 part:
            // - transactionId used for capture transaction
            // - uniqueId used for void transaction
            mapResponse.put("authId", transId + KeysUtil.TEMPKEY + uniqueId);
            mapResponse.put("authAmount", String.valueOf(amount));
            mapResponse.put("authCode", authorizationCode);
            mapResponse.put("authToken", token);
            mapResponse.put("allowCapture", "true");

            preAuthResponse.response = mapResponse;
            preAuthResponse.returnCode = 200;
            return preAuthResponse.toString();
        } catch(Exception ex) {
            ex.printStackTrace();

            // get error, void completed transaction
            if (!uniqueId.equals("")) {
                voidJetpayTransaction(uniqueId, terminalId, serverUrl);
            }
            return returnPaymentFailed(transId, "SALE", "token",
                    fleetId, bookId, amount,
                    "437", "", getError(ex), false);
        }
    }

    public String preAuthCapture(String fleetId, String bookId, String transactionId, double amount, String token, String terminalId, String serverUrl, String cardType) {
        ObjResponse preAuthResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        String transId = "";
        String uniqueId = "";
        try {
            TransactionResponse transactionResponse = jetPayConnectionUtil.preAuthCapture(transactionId, terminalId, serverUrl);

            // validate AVS and CVV2 is passed, retrieve data and return
            String actionCode = transactionResponse.getActionCode();
            String transactionCode = getTransactionCode(actionCode, cardType);
            transId = transactionResponse.getTransactionID() != null ? transactionResponse.getTransactionID() : "";
            uniqueId = transactionResponse.getUniqueID() != null ? transactionResponse.getUniqueID() : "";
            logger.debug(requestId + " - actionCode: " + actionCode + " --- transactionCode: " + transactionCode);
            if (transactionCode.equals("200")) {
                String authorizationCode = transactionResponse.getApproval() != null ? transactionResponse.getApproval() : "";
                String gatewayRejectionReason = transactionResponse.getResponseText() != null ? transactionResponse.getResponseText() : "";

                Map<String,String> mapResult = new HashMap<>();
                mapResult.put("message", "Success!" );
                mapResult.put("transId", transId);
                mapResult.put("cardType", "");
                mapResult.put("authCode", authorizationCode);
                mapResult.put("cardMask", "");
                preAuthResponse.response = mapResult;
                preAuthResponse.returnCode = 200;
                return preAuthResponse.toString();
            } else {
                preAuthResponse.returnCode = Integer.parseInt(transactionCode);
                preAuthResponse.response = mapResponse;
                return preAuthResponse.toString();
            }
        } catch(Exception ex) {
            ex.printStackTrace();

            // get error, void completed transaction
            if (!uniqueId.equals("")) {
                voidJetpayTransaction(uniqueId, terminalId, serverUrl);
            }
            return returnPaymentFailed(transId, "SALE", "token",
                    fleetId, bookId, amount,
                    "437", "", getError(ex), false);
        }
    }

}
