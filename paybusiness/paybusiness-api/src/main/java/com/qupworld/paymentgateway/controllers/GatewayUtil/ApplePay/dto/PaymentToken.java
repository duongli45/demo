package com.qupworld.paymentgateway.controllers.GatewayUtil.ApplePay.dto;

/**
 * Created by thuanho on 18/05/2022.
 */
public class PaymentToken extends BaseDTO {

    private PaymentData paymentData;
    private String transactionIdentifier;

    public PaymentData getPaymentData() {
        return paymentData;
    }
    public void setPaymentData(PaymentData paymentData) {
        this.paymentData = paymentData;
    }
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }
    public void setTransactionIdentifier(String transactionIdentifier) {
        this.transactionIdentifier = transactionIdentifier;
    }

}