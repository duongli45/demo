package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.alipay.ap.integration.sdk.openapi.DefaultOpenApiClient;
import com.alipay.ap.integration.sdk.openapi.OpenApiClient;
import com.alipay.ap.integration.sdk.openapi.domain.model.EnvInfo;
import com.alipay.ap.integration.sdk.openapi.domain.model.Money;
import com.alipay.ap.integration.sdk.openapi.domain.model.acquiring.InputUserInfo;
import com.alipay.ap.integration.sdk.openapi.domain.model.acquiring.NotificationUrl;
import com.alipay.ap.integration.sdk.openapi.domain.model.acquiring.Order;
import com.alipay.ap.integration.sdk.openapi.request.AlipayplusAcquiringOrderCreateRequest;
import com.alipay.ap.integration.sdk.openapi.request.AlipayplusAcquiringOrderQueryRequest;
import com.alipay.ap.integration.sdk.openapi.request.AlipayplusAcquiringOrderRefundRequest;
import com.alipay.ap.integration.sdk.openapi.response.AlipayplusAcquiringOrderCreateResponse;
import com.alipay.ap.integration.sdk.openapi.response.AlipayplusAcquiringOrderQueryResponse;
import com.alipay.ap.integration.sdk.openapi.response.AlipayplusAcquiringOrderRefundResponse;
import com.google.gson.Gson;
import com.qupworld.config.ServerConfig;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.TimezoneUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.security.*;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class TnGUtil {

    private String REQUEST_URL;
    private String CLIENT_ID;
    private String CLIENT_SECRET;
    private String MERCHANT_ID;
    private String PRODUCT_CODE;
    private String CLIENT_PRIVATE_KEY;
    private String SERVER_PUBLIC_KEY;
    private boolean IS_SANDBOX;
    private final static Logger logger = LogManager.getLogger(TnGUtil.class);

    public TnGUtil(boolean isSandbox, String requestUrl, String clientId, String clientSecret, String merchantId, String productCode, String tngPublicKey) {
        REQUEST_URL = requestUrl;
        CLIENT_ID = clientId;
        CLIENT_SECRET = clientSecret;
        MERCHANT_ID = merchantId;
        PRODUCT_CODE = productCode;
        SERVER_PUBLIC_KEY = tngPublicKey;
        IS_SANDBOX = isSandbox;
    }

    public String initiateCreditForm(String requestId, String orderTitle, String fleetId, String paxPhone, String osType, String appVersion, double amount, String currencyISO) {
        ObjResponse objResponse = new ObjResponse();
        try {
            CLIENT_PRIVATE_KEY = getClientPrivateKey();
            OpenApiClient openApiClient = new DefaultOpenApiClient(REQUEST_URL, CLIENT_ID, CLIENT_SECRET, CLIENT_PRIVATE_KEY, SERVER_PUBLIC_KEY);

            AlipayplusAcquiringOrderCreateRequest alipayplusAcquiringOrderCreateRequest = new AlipayplusAcquiringOrderCreateRequest();
            alipayplusAcquiringOrderCreateRequest.setMerchantId(MERCHANT_ID);
            alipayplusAcquiringOrderCreateRequest.setProductCode(PRODUCT_CODE);
            alipayplusAcquiringOrderCreateRequest.setSubMerchantId("");
            alipayplusAcquiringOrderCreateRequest.setSubMerchantName("MyCar");

            String merchantTransId = getOrderId();
            // use bookId as merchantTransId when process payment
            if (orderTitle.contains("Pay for booking")) {
                merchantTransId = orderTitle.replace("Pay for booking", "");
                merchantTransId = merchantTransId.trim();
            }

            // create order
            Order order = new Order();
            order.setMerchantTransId(merchantTransId);
            order.setMerchantTransType("Payment");
            order.setOrderMemo(paxPhone);
            order.setOrderTitle(orderTitle);
            InputUserInfo buyer = new InputUserInfo();
            buyer.setExternalUserType("PASSENGER");
            buyer.setExternalUserId(paxPhone);
            order.setBuyer(buyer);

            InputUserInfo seller = new InputUserInfo();
            seller.setExternalUserType("MYCAR");
            seller.setExternalUserId(fleetId);
            order.setSeller(seller);

            Date createdTime = TimezoneUtil.convertServerToGMT(new Date());
            SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ssZ" );
            String time = df.format(createdTime);
            order.setCreatedTime(df.parse(time));

            /*Calendar now = Calendar.getInstance();
            String str = String.format("%1$tY-%1$tm-%1$tdT%1$tH:%1$tM:%1$tS%1$tz", now.getTime());
            logger.debug(requestId + " - str = " + str);
            Date expiryTime = df.parse(str);
            logger.debug(requestId + " - expiryTime = " + expiryTime);
            order.setExpiryTime(expiryTime);*/

            // create order
            Money payAmount = new Money();
            payAmount.setValue((long) GatewayUtil.getIntAmount(amount, currencyISO));
            payAmount.setCurrency(currencyISO);
            order.setOrderAmount(payAmount);

            alipayplusAcquiringOrderCreateRequest.setOrder(order);

            // create URLs
            String url = "quppegasus://openapp";
            String tngNotifyUrl = ServerConfig.dispatch_server + KeysUtil.TNG_URL;
            if (ServerConfig.dispatch_server.equals("http://dispatch.local.qup.vn"))
                tngNotifyUrl = "http://113.160.232.234:1337" + KeysUtil.TNG_URL;

            List<NotificationUrl> notificationUrls = new ArrayList<>();
            NotificationUrl returnUrl = new NotificationUrl();
            returnUrl.setType("PAY_RETURN");
            returnUrl.setUrl(url);
            notificationUrls.add(returnUrl);

            NotificationUrl notifyUrl = new NotificationUrl();
            notifyUrl.setType("NOTIFICATION");
            notifyUrl.setUrl(tngNotifyUrl);
            notificationUrls.add(notifyUrl);

            alipayplusAcquiringOrderCreateRequest.setNotificationUrls(notificationUrls);

            // create EnvInfo
            EnvInfo envInfo = new EnvInfo();
            envInfo.setOrderTerminalType("APP");
            envInfo.setTerminalType("APP");
            if (osType != null && !osType.isEmpty())
                envInfo.setOsType(osType);
            if (appVersion != null && !appVersion.isEmpty())
                envInfo.setAppVersion(appVersion);
            alipayplusAcquiringOrderCreateRequest.setEnvInfo(envInfo);

            Gson gson = new Gson();
            logger.debug(requestId + " - initiateCreditForm request = " + gson.toJson(alipayplusAcquiringOrderCreateRequest));

            // submit
            AlipayplusAcquiringOrderCreateResponse alipayplusAcquiringOrderCreateOrderResponse =
                    openApiClient.execute(alipayplusAcquiringOrderCreateRequest);
            String acquirementId = alipayplusAcquiringOrderCreateOrderResponse.getAcquirementId();
            logger.debug(requestId + " - initiateCreditForm response = " + gson.toJson(alipayplusAcquiringOrderCreateOrderResponse));

            if (alipayplusAcquiringOrderCreateOrderResponse.getResultInfo() != null) {
                String status = alipayplusAcquiringOrderCreateOrderResponse.getResultInfo().getResultStatus() != null ? alipayplusAcquiringOrderCreateOrderResponse.getResultInfo().getResultStatus() : "";
                Map<String,String> mapResponse = new HashMap<>();
                if (status.equals("S")) {
                    String checkoutUrl = alipayplusAcquiringOrderCreateOrderResponse.getCheckoutUrl();
                    //logger.debug(requestId + " - checkoutUrl = " + checkoutUrl);
                    mapResponse.put("3ds_url", checkoutUrl);
                    mapResponse.put("merchantTransId", merchantTransId);
                    mapResponse.put("acquirementId", acquirementId);
                    mapResponse.put("type", "link");
                    objResponse.returnCode = 200;
                    objResponse.response = mapResponse;
                    return objResponse.toString();
                } else {
                    /*String resultCodeId = alipayplusAcquiringOrderCreateOrderResponse.getResultInfo().getResultCodeId() != null ? alipayplusAcquiringOrderCreateOrderResponse.getResultInfo().getResultCodeId() : "";*/
                    String resultCode = alipayplusAcquiringOrderCreateOrderResponse.getResultInfo().getResultCode() != null ? alipayplusAcquiringOrderCreateOrderResponse.getResultInfo().getResultCode() : "";
                    mapResponse.put("message", resultCode);
                    objResponse.returnCode = 527;
                    objResponse.response = mapResponse;
                    return objResponse.toString();
                }
            } else {
                objResponse.returnCode = 527;
                return objResponse.toString();
            }
        } catch(Exception ex) {
            logger.debug(requestId + " - initiateCreditForm exception = " + GatewayUtil.getError(ex));
            return GatewayUtil.returnError(ex, 527);
        }
    }

    public String completePayment(String requestId, String acquirementId, String merchantTransId, double amount, String currencyISO) {
        ObjResponse objResponse = new ObjResponse();
        try {
            CLIENT_PRIVATE_KEY = getClientPrivateKey();
            OpenApiClient openApiClient = new DefaultOpenApiClient(REQUEST_URL, CLIENT_ID, CLIENT_SECRET, CLIENT_PRIVATE_KEY, SERVER_PUBLIC_KEY);

            AlipayplusAcquiringOrderQueryRequest alipayplusAcquiringOrderQueryRequest = new AlipayplusAcquiringOrderQueryRequest();
            alipayplusAcquiringOrderQueryRequest.setMerchantId(MERCHANT_ID);
            alipayplusAcquiringOrderQueryRequest.setAcquirementId(acquirementId);
            alipayplusAcquiringOrderQueryRequest.setMerchantTransId(merchantTransId);

            String merchantRequestId = GatewayUtil.getOrderId();
            alipayplusAcquiringOrderQueryRequest.setMerchantTransId(merchantRequestId);
            Gson gson = new Gson();
            logger.debug(requestId + " - completePayment request = " + gson.toJson(alipayplusAcquiringOrderQueryRequest));

            AlipayplusAcquiringOrderQueryResponse response = openApiClient.execute(alipayplusAcquiringOrderQueryRequest);
            logger.debug(requestId + " - completePayment response = " + gson.toJson(response));

            int result = 527;
            if (response.getStatusDetail() != null) {
                String acquirementStatus = response.getStatusDetail().getAcquirementStatus() != null ? response.getStatusDetail().getAcquirementStatus() : "";
                if (acquirementStatus.equalsIgnoreCase("SUCCESS")) {
                    String transactionId = response.getMerchantTransId();
                    result = 200;
                    // return payment data with for refund process if necessary
                    Map<String,Object> mapResponse = new HashMap<>();
                    mapResponse.put("acquirementId", acquirementId);
                    mapResponse.put("transactionId", transactionId);
                    mapResponse.put("amount", amount);
                    mapResponse.put("currencyISO", currencyISO);
                    objResponse.response = mapResponse;
                } else {
                    result = 467; //The payment process has been cancelled. Please try again
                }
            }

            objResponse.returnCode = result;
            return objResponse.toString();
        } catch (Exception ex) {
            logger.debug(requestId + " - completePayment exception = " + GatewayUtil.getError(ex));
            return GatewayUtil.returnError(ex, 527);
        }

    }

    public String queryStatus(String requestId, String acquirementId, String merchantTransId) {
        String status = "";
        try {
            CLIENT_PRIVATE_KEY = getClientPrivateKey();
            OpenApiClient openApiClient = new DefaultOpenApiClient(REQUEST_URL, CLIENT_ID, CLIENT_SECRET, CLIENT_PRIVATE_KEY, SERVER_PUBLIC_KEY);

            AlipayplusAcquiringOrderQueryRequest alipayplusAcquiringOrderQueryRequest = new AlipayplusAcquiringOrderQueryRequest();
            alipayplusAcquiringOrderQueryRequest.setMerchantId(MERCHANT_ID);
            alipayplusAcquiringOrderQueryRequest.setAcquirementId(acquirementId);
            alipayplusAcquiringOrderQueryRequest.setMerchantTransId(merchantTransId);

            String merchantRequestId = GatewayUtil.getOrderId();
            alipayplusAcquiringOrderQueryRequest.setMerchantTransId(merchantRequestId);
            Gson gson = new Gson();
            logger.debug(requestId + " - queryStatus request = " + gson.toJson(alipayplusAcquiringOrderQueryRequest));

            AlipayplusAcquiringOrderQueryResponse response = openApiClient.execute(alipayplusAcquiringOrderQueryRequest);
            logger.debug(requestId + " - queryStatus response = " + gson.toJson(response));

            if (response.getStatusDetail() != null) {
                status = response.getStatusDetail().getAcquirementStatus() != null ? response.getStatusDetail().getAcquirementStatus() : "";
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - queryStatus exception = " + GatewayUtil.getError(ex));
            return GatewayUtil.returnError(ex, 527);
        }
        return status;
    }

    public static void main(String[] args) throws Exception
    {
        String clientId = "2019041513400300000001";
        String clientSecret = "2019041513400300000001T2hlhK";
        String merchantId = "217120000000040536944";
        String productCode = "51051000101000100001";
        String tngPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzRQmVFPS6dvMpNv2KWeeYlb8ap9po0eXmnxUItNeDCSvr";
        TnGUtil tnGUtil = new TnGUtil(true, "", clientId, clientSecret, merchantId, productCode, tngPublicKey);

        String requestId = UUID.randomUUID().toString();
        String acquirementId = "20200415211212800110171758900424784";
        double amount = 20.00;
        String currencyISO = "MYR";
        String reason = "test";
        tnGUtil.refundOrder(requestId, acquirementId, amount, currencyISO, reason);

    }
    public void refundOrder(String requestId, String acquirementId, double amount, String currencyISO, String reason) {
        try {
            CLIENT_PRIVATE_KEY = getClientPrivateKey();
            OpenApiClient openApiClient = new DefaultOpenApiClient(REQUEST_URL, CLIENT_ID, CLIENT_SECRET, CLIENT_PRIVATE_KEY, SERVER_PUBLIC_KEY);

            String merchantTransId = GatewayUtil.getOrderId();

            AlipayplusAcquiringOrderRefundRequest alipayplusAcquiringOrderRefundRequest = new AlipayplusAcquiringOrderRefundRequest();
            alipayplusAcquiringOrderRefundRequest.setMerchantId(MERCHANT_ID);
            alipayplusAcquiringOrderRefundRequest.setAcquirementId(acquirementId);
            alipayplusAcquiringOrderRefundRequest.setRequestId(merchantTransId);
            alipayplusAcquiringOrderRefundRequest.setRefundReason(reason);

            Money refundAmount = new Money();
            refundAmount.setValue((long) GatewayUtil.getIntAmount(amount, currencyISO));
            refundAmount.setCurrency(currencyISO);
            alipayplusAcquiringOrderRefundRequest.setRefundAmount(refundAmount);
            Gson gson = new Gson();
            logger.debug(requestId + " - refundOrder request = " + gson.toJson(alipayplusAcquiringOrderRefundRequest));
            AlipayplusAcquiringOrderRefundResponse response = openApiClient.execute(alipayplusAcquiringOrderRefundRequest);
            logger.debug(requestId + " - refundOrder response = " + gson.toJson(response));
        } catch (Exception ex) {
            logger.debug(requestId + " - refundOrder exception = " + GatewayUtil.getError(ex));
        }
    }

    public String getSignature(String requestId, String body) {
        logger.debug(requestId + " - getSignature body: " + body);
        String signature = "";
        try {
            KeyPair pair = getKeyPairFromKeyStore();
            //Let's sign our message
            signature = sign(body, pair.getPrivate());

        } catch (Exception ex) {
            logger.debug(requestId + " - getSignature error: " + GatewayUtil.getError(ex));
        }
        return signature;
    }

    private static String sign(String plainText, PrivateKey privateKey) throws Exception {
        Signature privateSignature = Signature.getInstance("SHA256withRSA");
        privateSignature.initSign(privateKey);
        privateSignature.update(plainText.getBytes(UTF_8));

        byte[] signature = privateSignature.sign();

        return Base64.getEncoder().encodeToString(signature);
    }

    private static KeyPair getKeyPairFromKeyStore() throws Exception {
        //Generated with:
        //  keytool -genkeypair -alias mykey -storepass s3cr3t -keypass s3cr3t -keyalg RSA -keystore keystore.jks

        InputStream ins = TnGUtil.class.getResourceAsStream("/keystore.jks");

        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        keyStore.load(ins, "QUp@1234".toCharArray());   //Keystore password
        KeyStore.PasswordProtection keyPassword =       //Key password
                new KeyStore.PasswordProtection("QUp@1234".toCharArray());

        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry("QUpKey", keyPassword);

        java.security.cert.Certificate cert = keyStore.getCertificate("QUpKey");
        PublicKey publicKey = cert.getPublicKey();
        PrivateKey privateKey = privateKeyEntry.getPrivateKey();

        return new KeyPair(publicKey, privateKey);
    }

    private String getClientPrivateKey() throws Exception {
        InputStream in;
        if (IS_SANDBOX) {
            in = TnGUtil.class.getResourceAsStream("/qup_private_key.pem");
        } else {
            in = TnGUtil.class.getResourceAsStream("/private_key_production.pem");
        }
        byte[] keyBytes = new byte[in.available()];
        in.read(keyBytes);
        in.close();

        String priKey = new String(keyBytes, "UTF-8");
        priKey = priKey.replaceAll("(-+BEGIN PRIVATE KEY-+\\r?\\n|-+END PRIVATE KEY-+\\r?\\n?)", "");
        priKey = priKey.replaceAll("\\r\\n|\\r|\\n", "");
        /*logger.debug("priKey: " + priKey);*/
        return priKey;
    }

    private static String getOrderId() {
        Calendar cal = Calendar.getInstance();
        Date currentTime = TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), "GMT");
        SimpleDateFormat formatOrder = new SimpleDateFormat("HHmmss");
        int random = (int)(Math.random()*10000);
        return formatOrder.format(currentTime) + random;
    }
}
