package com.qupworld.paymentgateway.controllers.threading;

import com.google.gson.Gson;
import com.mashape.unirest.http.Unirest;
import com.pg.util.KeysUtil;
import com.pg.util.TimezoneUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.security.MessageDigest;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by qup on 23/03/2020.
 */
@SuppressWarnings("unchecked")
public class TXGTripThread extends Thread {
    org.json.JSONObject ticket;
    Gson gson = new Gson();
    public String requestId = "";
    final static Logger logger = LogManager.getLogger(TXGTripThread.class);
    static final String txgTripURLCab = "http://192.168.254.96:1111/api/trips";
    static final String txgTripURLVNPT = "http://222.252.18.5:1112/api/trips";
    static final String txgTripURLVIETTEL = "http://117.6.130.104:1112/api/trips";
    final static String secretKey = "66c41d2ecf7c09df3b7ed54e74137920";
    @Override
    public void run() {

        logger.debug(requestId + " - run TXGTripThread ...");
        try {
            String body = getBody(ticket);
            logger.debug(requestId + " - body: " + body);
            String result = sendPost(txgTripURLCab, body, requestId);
            if (!result.equals("200")) {
                result = sendPost(txgTripURLVNPT, body, requestId);
                if (!result.equals("200")) {
                    result = sendPost(txgTripURLVIETTEL, body, requestId);
                }
            }
            logger.debug(requestId + " - TXGTripThread: " + result);
        } catch(Exception ex) {
            ex.printStackTrace();
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            logger.debug(requestId + " - report store: "+ errors.toString());
        }
    }

    private static String sendPost(String URL, String data, String requestId) {
        logger.debug(requestId + " - Sending POST request to " + URL + " ...");
        String response = "";
        try {
            Unirest.setTimeouts(30*1000, 30*1000);
            int status = Unirest.post(URL)
                    .header("Content-Type", "application/json")
                    .body(data)
                    .asBinary()
                    .getStatus();
            response = status == 200 ? "200" : String.valueOf(status);
        } catch(Exception ex) {
            logger.debug(requestId + " - exception: " + ex.getMessage());
            response = "exception";
        }
        //logger.debug(requestId + " - Response Code: " + response);
        return response;
    }

    private String getBody(org.json.JSONObject ticket) {
        String body = "";
        try {
            String bookFrom = ticket.getString("bookFrom");
            String phoneNumber = ticket.getString("customerPhone");
            if (bookFrom.equalsIgnoreCase("Car-hailing"))
                phoneNumber = "";

            Calendar cal = Calendar.getInstance();
            Date dropOffTime = TimezoneUtil.offsetTimeZone(KeysUtil.SDF_DMYHMS.parse(ticket.getString("droppedOffTime")), "GMT", "GMT+7");
            Date curTime = TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), "GMT+7");

            String strSecurity = "bookingID=" + ticket.getString("bookId") + "&phoneNumber=" + phoneNumber +
                    "&curTime=" + curTime.getTime()/1000 + "&dropOffTime=" + dropOffTime.getTime()/1000 + secretKey;
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(strSecurity.getBytes());
            byte byteData[] = md.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            String security = sb.toString();

            JSONObject objectBody = new JSONObject();
            objectBody.put("bookingID", ticket.get("bookId"));
            objectBody.put("phoneNumber", phoneNumber);
            objectBody.put("bookingType", bookFrom);
            objectBody.put("dropOffTime", dropOffTime.getTime()/1000);
            objectBody.put("promoCode", ticket.get("promoCode"));
            objectBody.put("promoValue", ticket.get("promoAmount"));
            objectBody.put("totalAmt", ticket.get("total"));
            objectBody.put("licensePlate", ticket.get("plateNumber"));
            objectBody.put("paidBy", ticket.get("paidBy"));
            objectBody.put("curTime", curTime.getTime()/1000);
            objectBody.put("security", security);

            return objectBody.toJSONString();
        } catch(Exception ex) {
            ex.printStackTrace();
        }

        return body;
    }

}