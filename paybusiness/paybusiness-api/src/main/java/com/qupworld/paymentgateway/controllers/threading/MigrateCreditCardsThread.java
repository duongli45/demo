package com.qupworld.paymentgateway.controllers.threading;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.mongo.collections.Credit;
import com.qupworld.paymentgateway.models.mongo.collections.account.Account;
import com.qupworld.paymentgateway.models.mongo.collections.corporate.Corporate;
import com.qupworld.paymentgateway.models.mongo.collections.logToken.LogToken;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.SlackAPI;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.json.JSONObject;

import java.util.List;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class MigrateCreditCardsThread extends Thread {

    private final static Logger logger = LogManager.getLogger(MigrateCreditCardsThread.class);
    public String fleetId;
    public MongoDao mongoDao;
    public String requestId;

    private JSONObject objData;

    private int total = 0;
    private int succeed = 0;
    private int failed = 0;
    @Override
    public void run() {
        try {
            /*int i = 0;
            int size = 100;
            boolean valid = true;
            while (valid) {
                List<String> listPax = mongoDao.getAllPassengerRegisteredCard(requestId, fleetId, i * size, size);
                int querySize = listPax.size();
                for (String userId : listPax) {
                    doMigrate(userId);
                }

                if (querySize < size) {
                    valid = false;
                }
                i++;
            }*/
            List<String> listCorporate = mongoDao.getCorporateByFleet(requestId, fleetId);
            for (String corporateId : listCorporate) {
                doMigrateForCorporate(corporateId);
            }
            logger.debug(requestId + " - MigrateCreditCardsThread COMPLETED !!!");
            logger.debug(requestId + " - total: " + total);
            logger.debug(requestId + " - succeed: " + succeed);
            logger.debug(requestId + " - failed: " + failed);
        } catch(Exception ex) {
            logger.debug(requestId + " - MigrateCreditCardsThread exception: "+ CommonUtils.getError(ex));
        }

    }

    private void doMigrate(String userId){
        try {
            Account account = mongoDao.getAccount(userId);
            List<Credit> credits = account.credits;
            if (credits != null && !credits.isEmpty()) {
                logger.debug(requestId + " === doMigrate for userId: "+ userId);
                for (Credit credit : credits) {
                    if (credit.gateway.equals(KeysUtil.CREDITCORP)) {
                        total += 1;
                        String data = getCardFromStore(credit.localToken);
                        if (!data.isEmpty()) {
                            LogToken logToken = new LogToken();
                            logToken.logId = credit.localToken;
                            logToken.fleetId = fleetId;
                            logToken.userId = userId;
                            logToken.logData = data;
                            mongoDao.addLogToken(logToken);
                            // check added log

                            LogToken added = mongoDao.getLogToken(fleetId, credit.localToken);
                            if (added != null && added.logData.equals(data)) {
                                succeed += 1;
                            } else {
                                failed += 1;
                            }
                        } else {
                            failed += 1;
                        }
                    }
                }
                logger.debug(requestId + " === doMigrate for userId: "+ userId + " is done!");
            }

        } catch (Exception ex) {
            failed += 1;
            logger.debug(requestId + " === doMigrate for userId: "+ userId +" exception: "+ CommonUtils.getError(ex));
        }

    }

    private void doMigrateForCorporate(String corporateId){
        try {
            Corporate corporate = mongoDao.getCorporate(corporateId);
            List<Credit> credits = corporate.credits;
            if (credits != null && !credits.isEmpty()) {
                logger.debug(requestId + " === doMigrate for corporateId: "+ corporateId);
                for (Credit credit : credits) {
                    if (credit.gateway.equals(KeysUtil.CREDITCORP)) {
                        total += 1;
                        String data = getCardFromStore(credit.localToken);
                        if (!data.isEmpty()) {
                            LogToken logToken = new LogToken();
                            logToken.logId = credit.localToken;
                            logToken.fleetId = fleetId;
                            logToken.userId = corporateId;
                            logToken.logData = data;
                            mongoDao.addLogToken(logToken);
                            // check added log

                            LogToken added = mongoDao.getLogToken(fleetId, credit.localToken);
                            if (added != null && added.logData.equals(data)) {
                                succeed += 1;
                            } else {
                                failed += 1;
                            }
                        } else {
                            failed += 1;
                        }
                    }
                }
                logger.debug(requestId + " === doMigrate for corporate: "+ corporateId + " is done!");
            }

        } catch (Exception ex) {
            failed += 1;
            logger.debug(requestId + " === doMigrate for corporate: "+ corporateId +" exception: "+ CommonUtils.getError(ex));
        }

    }

    private String getCardFromStore(String token){
        String dataStr = "";
        HttpResponse<JsonNode> jsonResponse = null;
        try {
            String body = "{\"logId\":\""+token+"\"}";
            jsonResponse = Unirest.post(ServerConfig.logs_server+"/api/service/getReport")
                    .basicAuth(ServerConfig.logs_username, ServerConfig.logs_password)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .body(body)
                    .asJson();
            org.json.JSONObject objectResponse = jsonResponse.getBody().getObject();
            logger.debug(requestId + " - stored data: " + objectResponse);
            if (objectResponse != null && objectResponse.get("returnCode") != null && Integer.parseInt(objectResponse.get("returnCode").toString()) == 200) {
                dataStr = objectResponse.get("response").toString();
            } else {
                for (int i = 0; i < 3; i++) {
                    // try to query again
                    Thread.sleep(2000);
                    HttpResponse<JsonNode> jsonResponseTry =  Unirest.post(ServerConfig.logs_server+"/api/service/getReport")
                            .basicAuth(ServerConfig.logs_username, ServerConfig.logs_password)
                            .header("Content-Type", "application/json; charset=utf-8")
                            .body(body)
                            .asJson();
                    org.json.JSONObject objectResponseTry = jsonResponseTry.getBody().getObject();
                    logger.debug(requestId + " - stored data: " + objectResponseTry);
                    if (objectResponseTry != null && objectResponseTry.get("returnCode") != null && Integer.parseInt(objectResponseTry.get("returnCode").toString()) == 200) {
                        dataStr = objectResponse.get("response").toString();

                        // if success then break the loop
                        break;
                    } else {
                        SlackAPI slack = new SlackAPI();
                        slack.sendSlack("GET DATA CREDIT CARD FALSE ","URGREN",token);
                    }
                    i++;
                }


            }
        } catch (Exception e) {
            SlackAPI slack = new SlackAPI();
            slack.sendSlack("GET DATA CREDIT CARD FALSE Exception ","URGREN",token);
            e.printStackTrace();
            logger.debug(requestId + " - Exception data: " + e.toString());
        }
        return dataStr;
    }

}
