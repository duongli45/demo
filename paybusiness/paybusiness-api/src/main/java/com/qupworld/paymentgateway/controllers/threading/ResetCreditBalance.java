package com.qupworld.paymentgateway.controllers.threading;

import com.qupworld.paymentgateway.controllers.UpdateDriverCreditBalance;
import com.qupworld.paymentgateway.controllers.WalletUtil;
import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.MongoDaoImpl;
import com.qupworld.paymentgateway.models.SQLDao;
import com.qupworld.paymentgateway.models.SQLDaoImpl;
import com.pg.util.CommonArrayUtils;
import com.pg.util.CommonUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Created by qup on 23/03/2020.
 */
@SuppressWarnings("unchecked")
public class ResetCreditBalance extends Thread {

    final static Logger logger = LogManager.getLogger(ResetCreditBalance.class);
    public String requestId;
    public String fleetId;
    public double balance;
    public String currencyISO;

    @Override
    public void run() {
        try {
            MongoDao mongoDao = new MongoDaoImpl();
            SQLDao sqlDao = new SQLDaoImpl();
            int i = 0;
            int size = 100;
            boolean valid = true;
            while (valid) {
                List<String> all = mongoDao.getDriverWithCreditBalanceLessThan(fleetId, balance, 0, size);
                logger.debug(requestId + " - get driverId from " + i*size + ", limit " + size);
                logger.debug(requestId + " - total query: " + all.size());
                for (String userId : all) {
                    WalletUtil walletUtil = new WalletUtil(requestId);
                    double currentBalance = walletUtil.getCurrentBalance(userId, "credit", currencyISO);
                    double topupAmount = balance - currentBalance;

                    UpdateDriverCreditBalance updateBalance = new UpdateDriverCreditBalance();
                    updateBalance.type = CommonArrayUtils.DRIVER_WALLET_TYPE[2];
                    updateBalance.fleetId = fleetId;
                    updateBalance.driverId = userId;
                    updateBalance.amount = topupAmount;
                    updateBalance.currencyISO = currencyISO;
                    updateBalance.mongoDao = mongoDao;
                    updateBalance.sqlDao = sqlDao;
                    updateBalance.reason = "Balance Reset";
                    updateBalance.additionalData.put("operatorId", "");
                    updateBalance.additionalData.put("operatorName", "");
                    updateBalance.requestId = requestId;
                    String result = updateBalance.doUpdate();
                    logger.debug(requestId + " - updateBalance for driver "+ userId +" result = " + result);
                }

                if (all.size() < size) {
                    valid = false;
                } else {
                    i++;
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ignore){}
                }
            }
            logger.debug(requestId + " - ResetCreditWallet DONE !!!");

        } catch(Exception ex) {
            logger.debug(requestId + " - ResetCreditWallet exception: "+ CommonUtils.getError(ex));
        }

    }

}
