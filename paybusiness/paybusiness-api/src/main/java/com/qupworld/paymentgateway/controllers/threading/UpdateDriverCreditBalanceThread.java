package com.qupworld.paymentgateway.controllers.threading;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.entities.DriverCreditBalance;
import com.qupworld.service.QUpReportPublisher;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by qup on 23/03/2020.
 */
@SuppressWarnings("unchecked")
public class UpdateDriverCreditBalanceThread extends Thread {
    public DriverCreditBalance driverCreditBalance;
    public String requestId;
    final static Logger logger = LogManager.getLogger(UpdateDriverCreditBalanceThread.class);
    @Override
    public void run() {
        String createdDate =  KeysUtil.SDF_DMYHMSTZ.format(driverCreditBalance.createdDate);
        try {
            if (ServerConfig.server_report_enable) {
                Gson gson = new GsonBuilder().serializeNulls().create();
                String driverCreditBalanceStr = gson.toJson(driverCreditBalance);
                JsonElement jsonElement = gson.fromJson(driverCreditBalanceStr, JsonElement.class);
                JsonObject jo = jsonElement.getAsJsonObject();
                jo.addProperty("createdDate",createdDate);

                QUpReportPublisher reportPublisher = QUpReportPublisher.getInstance();
                reportPublisher.sendOneItem(requestId, String.valueOf(driverCreditBalance.id), "DriverCreditBalance", gson.toJson(jo));
            }
        } catch(Exception ex) {
            logger.debug(requestId + " - report DriverCreditBalance: "+ CommonUtils.getError(ex));
        }
    }
}
