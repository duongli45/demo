package com.qupworld.paymentgateway.controllers.GatewayUtil.ApplePay;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by thuanho on 20/05/2022.
 */
public class MyTrustManager  implements javax.net.ssl.TrustManager, javax.net.ssl.X509TrustManager {
    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
        return null;
    }
    public boolean isServerTrusted(java.security.cert.X509Certificate[] certs) {
        return true;
    }
    public boolean isClientTrusted(java.security.cert.X509Certificate[] certs) {
        return true;
    }
    public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) throws java.security.cert.CertificateException {
        return;
    }
    public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) throws java.security.cert.CertificateException {
        return;
    }

    public static void trustAllHttpsCertificates() {
        try {
            // Create a trust manager that does not validate certificate chains:
            javax.net.ssl.TrustManager[] trustAllCerts = new javax.net.ssl.TrustManager[1];
            javax.net.ssl.TrustManager tm = new MyTrustManager();
            trustAllCerts[0] = tm;
            javax.net.ssl.SSLContext sc;
            sc = javax.net.ssl.SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, null);
            javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            //logger.debug(e);
        } catch (KeyManagementException e) {
            e.printStackTrace();
            //logger.debug(e);
        } catch (Exception e){
            e.printStackTrace();
            //logger.debug(e);
        }
    }
}