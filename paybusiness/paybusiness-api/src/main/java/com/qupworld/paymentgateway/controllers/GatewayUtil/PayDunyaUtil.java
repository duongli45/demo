package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.google.gson.Gson;
import com.paydunya.neptune.PaydunyaCheckoutInvoice;
import com.paydunya.neptune.PaydunyaCheckoutStore;
import com.paydunya.neptune.PaydunyaOnsiteInvoice;
import com.paydunya.neptune.PaydunyaSetup;
import com.qupworld.config.ServerConfig;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by thuanho on 29/03/2022.
 */
public class PayDunyaUtil {

    private final static Logger logger = LogManager.getLogger(PayDunyaUtil.class);
    private String MASTER_KEY;
    private String PUBLIC_KEY;
    private String SECRET_KEY;
    private String TOKEN;
    private boolean isSandbox;
    private String requestId;
    private Gson gson;

    public PayDunyaUtil(String requestId, String masterKey, String publicKey, String secretKey, String token, boolean isSandbox) {
        this.requestId = requestId;
        MASTER_KEY = masterKey;
        PUBLIC_KEY = publicKey;
        SECRET_KEY = secretKey;
        TOKEN = token;
        this.isSandbox = isSandbox;
        gson = new Gson();
    }

    public String getCheckoutURL(double amount, String orderId, String fleetName, String fleetLogo){
        ObjResponse response = new ObjResponse();
        Map<String, Object> mapResponse = new HashMap<>();
        int returnCode = 525;
        try {
            PaydunyaSetup setup = new PaydunyaSetup();
            setup.setMasterKey(MASTER_KEY);
            setup.setPrivateKey(SECRET_KEY);
            setup.setPublicKey(PUBLIC_KEY);
            setup.setToken(TOKEN);
            if (isSandbox)
                setup.setMode("test"); // Optional. Use this option for test payments.

            PaydunyaCheckoutStore store = new PaydunyaCheckoutStore();
            store.setName(fleetName); // Only the name is required
            store.setLogoUrl(fleetLogo);

            PaydunyaOnsiteInvoice invoice = new PaydunyaOnsiteInvoice(setup, store);
            invoice.setCallbackUrl(ServerConfig.hook_server + "/notification/" + KeysUtil.PAYDUNYA + "?urlAction=callback");
            invoice.setReturnUrl(ServerConfig.hook_server + "/notification/" + KeysUtil.PAYDUNYA + "?urlAction=return");
            invoice.setCancelUrl(ServerConfig.hook_server + "/notification/" + KeysUtil.PAYDUNYA + "?urlAction=cancel");
            invoice.addItem(orderId, 1, amount, amount);
            invoice.setTotalAmount(amount);
            invoice.addChannels(new String[] {"card", "wari", "jonijoni-senegal", "orange-money-senegal", "paydunya" });

            // The following code describes how to create a payment invoice at our servers,
            // and then post his payment receipt if successful.
            if (invoice.create()) {
                String status = invoice.getStatus();
                logger.debug(requestId + " - status: " + status);
                if (status.equals("success")) {
                    mapResponse.put("3ds_url", invoice.getInvoiceUrl());
                    mapResponse.put("type", "link");
                    mapResponse.put("token", invoice.getToken());
                    mapResponse.put("isDeeplink", false);
                    returnCode = 200;
                }
            }
            if (returnCode != 200) {
                String message = invoice.getResponseText() != null ? invoice.getResponseText() : "";
                if (!message.isEmpty()) {
                    logger.debug(requestId + " - message: " + message);
                    mapResponse.put("message", message);
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - getCheckoutURL exception: " + CommonUtils.getError(ex));
        }
        response.returnCode = returnCode;
        response.response = mapResponse;
        return response.toString();
    }

    public String getPaymentStatus(String token, String fleetName, String fleetLogo) throws Exception {
        ObjResponse response = new ObjResponse();
        Map<String, Object> mapResponse = new HashMap<>();
        int returnCode = 525;
        try {
            PaydunyaSetup setup = new PaydunyaSetup();
            setup.setMasterKey(MASTER_KEY);
            setup.setPrivateKey(SECRET_KEY);
            setup.setPublicKey(PUBLIC_KEY);
            setup.setToken(TOKEN);
            if (isSandbox)
                setup.setMode("test"); // Optional. Use this option for test payments.

            PaydunyaCheckoutStore store = new PaydunyaCheckoutStore();
            store.setName(fleetName); // Only the name is required
            store.setLogoUrl(fleetLogo);

            PaydunyaCheckoutInvoice invoice = new PaydunyaCheckoutInvoice(setup, store);
            // The following code describes how to create a payment invoice at our servers,
            // and then post his payment receipt if successful.
            if (invoice.confirm(token)) {
                String status = invoice.getStatus();
                logger.debug(requestId + " - status: " + status);
                // The payment status can be either completed, pending, cancelled
                mapResponse.put("status", status);
                if (status.equals("completed")) {
                    String transactionId = invoice.getTransactionId();
                    logger.debug(requestId + " - transactionId: " + transactionId);
                    mapResponse.put("transactionId", transactionId);
                    returnCode = 200;
                }
            }
            if (returnCode != 200) {
                String message = invoice.getResponseText() != null ? invoice.getResponseText() : "";
                if (!message.isEmpty()) {
                    mapResponse.put("message", message);
                }
            }


        } catch (Exception ex) {
            logger.debug(requestId + " - getPaymentStatus exception: " + CommonUtils.getError(ex));

        }
        response.returnCode = returnCode;
        response.response = mapResponse;
        return response.toString();
    }

    public static void main(String[] args) throws Exception {
        init();

        /*String token = "test_CZtv37l1u1";
        check(token);*/
    }

    private static void init() throws Exception {
        /*String MASTER_KEY = "gp6I7wUs-07Wl-10M4-IqOE-k6BtPz2LibAO";
        String PUBLIC_KEY = "test_public_Zee60QsbiWe996GC7LSRDYbpqV2";
        String SECRET_KEY = "test_private_PnsrfNYYu9VfprbZaRhk1hq4rWW";
        String TOKEN = "MF61vEQbySq3lBzY0mky";
*/
        String MASTER_KEY = "vTNlSWQd-Xdtp-8lzJ-q58A-1nPggMLvfk0k";
        String PUBLIC_KEY = "test_public_nWv1b63v7wkU4JVerdZovs8K0Aw";
        String SECRET_KEY = "test_private_ogr2vVnXS8HuHP4lawQBmTN3kj4";
        String TOKEN = "tVrS35onF29QgSQF5uvG";

        PaydunyaSetup setup = new PaydunyaSetup();
        setup.setMasterKey(MASTER_KEY);
        setup.setPrivateKey(SECRET_KEY);
        setup.setPublicKey(PUBLIC_KEY);
        setup.setToken(TOKEN);
        //setup.setMode("test"); // Optional. Use this option for test payments.

        PaydunyaCheckoutStore store = new PaydunyaCheckoutStore();
        store.setName("Fleet name"); // Only the name is required
        /*store.setTagline("Elegance has no price");
        store.setPhoneNumber("336530583");
        store.setPostalAddress("Dakar Plateau - Establishment kheweul");
        store.setWebsiteUrl("http://www.chez-sandra.sn");
        store.setLogoUrl("http://www.chez-sandra.sn/logo.png");*/
        store.setLogoUrl("https://prod-gojo-global.s3.amazonaws.com/images/gojosg/setting/gojosg_1612521481820.png");

        PaydunyaOnsiteInvoice invoice = new PaydunyaOnsiteInvoice(setup, store);
        invoice.setCallbackUrl("https://eventapis.local.qup.vn/api/webhook/notification/PayDunya?urlAction=callback");
        invoice.setReturnUrl("https://eventapis.local.qup.vn/api/webhook/notification/PayDunya?urlAction=return");
        invoice.setCancelUrl("https://eventapis.local.qup.vn/api/webhook/notification/PayDunya?urlAction=cancel");
        /*Adding items to your bill is very basic.
        The expected parameters are product name, quantity, unit price,
        the total price and an optional description. */
        /*invoice.addItem("#12345", 3, 10000, 30000);
        invoice.addItem("Ice Shirt", 1, 5000, 5000);*/
        invoice.addItem("Order #"+"1234567", 1, 200, 200);

        invoice.setTotalAmount(200);

        // Addition of several payment methods at a time
        //invoice.addChannels(new String[] {"card", "wari", "jonijoni-senegal", "orange-money-senegal", "paydunya" });
        invoice.addChannel("card");
        //invoice.addChannels(new String[] {"card", "wari", "jonijoni-senegal", "orange-money-senegal", "paydunya" });

        // The following code describes how to create a payment invoice at our servers,
        // and then post his payment receipt if successful.
        if (invoice.create()) {
            /*Gson gson = new Gson();
            System.out.println("invoice = " + gson.toJson(invoice));*/
            System.out.println(invoice.getStatus());
            System.out.println(invoice.getResponseText());
            System.out.println(invoice.getInvoiceUrl());
        } else {
            System.out.println(invoice.getResponseText());
            System.out.println(invoice.getResponseCode());
        }
    }

    private static void check(String token) throws Exception {
        // PayDunya will automatically add the invoice token as a QUERYSTRING "token"
        // if you have configured a "return_url" or "cancel_url".
        String MASTER_KEY = "gp6I7wUs-07Wl-10M4-IqOE-k6BtPz2LibAO";
        String PUBLIC_KEY = "test_public_Zee60QsbiWe996GC7LSRDYbpqV2";
        String SECRET_KEY = "test_private_PnsrfNYYu9VfprbZaRhk1hq4rWW";
        String TOKEN = "MF61vEQbySq3lBzY0mky";

        PaydunyaSetup setup = new PaydunyaSetup();
        setup.setMasterKey(MASTER_KEY);
        setup.setPrivateKey(SECRET_KEY);
        setup.setPublicKey(PUBLIC_KEY);
        setup.setToken(TOKEN);
        setup.setMode("test"); // Optional. Use this option for test payments.

        PaydunyaCheckoutStore store = new PaydunyaCheckoutStore();
        store.setName("Sandra's Store"); // Only the name is required
        store.setTagline("Elegance has no price");
        store.setPhoneNumber("336530583");
        store.setPostalAddress("Dakar Plateau - Establishment kheweul");
        store.setWebsiteUrl("http://www.chez-sandra.sn");
        store.setLogoUrl("http://www.chez-sandra.sn/logo.png");

        PaydunyaCheckoutInvoice invoice = new PaydunyaCheckoutInvoice(setup, store);


        System.out.println("invoice = " + invoice.toString());
        if (invoice.confirm(token)) {

            // Retrieve payment status

            System.out.println(invoice.getStatus());
            System.out.println(invoice.getResponseText());

            // You can retrieve the name, email address and
            // customer's phone number using
            // the following methods
            System.out.println(invoice.getCustomerInfo("name"));
            System.out.println(invoice.getCustomerInfo("email"));
            System.out.println(invoice.getCustomerInfo("phone"));

            // The following methods will be available if and
            // only if the payment status is equal to "completed".

            // Retrieve the URL of the electronic PDF receipt for download
            System.out.println(invoice.getReceiptUrl());

            // Recover any of the custom data that you had to add to the bill previously.
            // Please make sure to use the same keys used during the configuration.
            // System.out.println(invoice.getCustomData("Prix"));

            // You can also recover the total amount specified previously
            System.out.println(invoice.getTotalAmount());

        }else{
            System.out.println(invoice.getStatus());
            System.out.println(invoice.getResponseText());
            System.out.println(invoice.getResponseCode());
        }
    }
}
