package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.google.gson.Gson;
import com.qupworld.paymentgateway.entities.CreditEnt;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import net.authorize.Environment;
import net.authorize.api.contract.v1.*;
import net.authorize.api.controller.CreateTransactionController;
import net.authorize.api.controller.base.ApiOperationBase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by duyphan on 8/17/16.
 */
@SuppressWarnings("unchecked")
public class AuthorizeUtil {

    private DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.##");
    final static Logger logger = LogManager.getLogger(AuthorizeUtil.class);
    private String requestId;
    private Gson gson;
    public AuthorizeUtil(String _requestId) {
        requestId = _requestId;
        gson = new Gson();
    }

    public String createCreditToken(CreditEnt credit, String customerNumber, Map<String, String> account) throws IOException {
        ObjResponse objResponse = new ObjResponse();
        String transId = "";
        boolean isVoid = false;
        String authorizePaymentType = "";
        try {
            if (KeysUtil.PRODUCTION.equals(account.get("environment"))) {
                ApiOperationBase.setEnvironment(Environment.PRODUCTION);
            } else {
                ApiOperationBase.setEnvironment(Environment.SANDBOX);
            }
            Boolean enableCheckZipCode = false;
            if (!isEmptyString(credit.postalCode)) {
                enableCheckZipCode = true;
            }
            MerchantAuthenticationType merchantAuthenticationType = new MerchantAuthenticationType();
            merchantAuthenticationType.setName(account.get("apiLoginId"));
            merchantAuthenticationType.setTransactionKey(account.get("transactionKey"));
            ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

            CreditCardType creditCard = new CreditCardType();
            creditCard.setCardNumber(credit.cardNumber);
            creditCard.setExpirationDate(credit.expiredDate);
            creditCard.setCardCode(credit.cvv);

            PaymentType paymentType = new PaymentType();
            paymentType.setCreditCard(creditCard);

            authorizePaymentType = TransactionTypeEnum.AUTH_ONLY_TRANSACTION.value();
            TransactionRequestType transactionRequestType = new TransactionRequestType();
            transactionRequestType.setTransactionType(TransactionTypeEnum.AUTH_ONLY_TRANSACTION.value());
            transactionRequestType.setPayment(paymentType);
            transactionRequestType.setAmount(new BigDecimal(1.00));
            CustomerAddressType addressType = new CustomerAddressExType();
            String[] name = credit.cardHolder.split(" ");
            if (name.length > 1) {
                String lastName = name[name.length - 1];
                if (credit.cardHolder.length() > lastName.length()) {
                    addressType.setFirstName(credit.cardHolder.substring(0, credit.cardHolder.length() - lastName.length()));
                    addressType.setLastName(lastName);
                } else {
                    addressType.setFirstName(credit.cardHolder);
                }
            } else {
                addressType.setFirstName(credit.cardHolder);
            }
            if (!isEmptyString(credit.postalCode)) {
                addressType.setZip(credit.postalCode);
            }

            transactionRequestType.setBillTo(addressType);

            CustomerDataType customerDataType = new CustomerDataType();
            customerDataType.setId(customerNumber);
            customerDataType.setType(CustomerTypeEnum.INDIVIDUAL);
            transactionRequestType.setCustomer(customerDataType);

            CustomerProfilePaymentType customerProfilePaymentType = new CustomerProfilePaymentType();
            customerProfilePaymentType.setCreateProfile(true);
            transactionRequestType.setProfile(customerProfilePaymentType);

            CreateTransactionRequest createTransactionRequest = new CreateTransactionRequest();
            createTransactionRequest.setTransactionRequest(transactionRequestType);
            String cardLog = "XXXXXXXXXXXX" + credit.cardNumber.substring(credit.cardNumber.length() - 4);
            String log = gson.toJson(createTransactionRequest).replace(credit.cardNumber, cardLog);
            logger.debug(requestId + " - createTransactionRequest: " + log);
            CreateTransactionController controller = new CreateTransactionController(createTransactionRequest);
            controller.execute();

            CreateTransactionResponse createTransactionResponse = controller.getApiResponse();
            logger.debug(requestId + " - createTransactionResponse: " + gson.toJson(createTransactionResponse));
            int returnCode = 0;
            String avsCode = "";
            String cvvCode = "";
            String profileId = "";
            String paymentId = "";
            if (createTransactionResponse != null) {
                if (createTransactionResponse.getMessages().getResultCode() == MessageTypeEnum.OK) {
                    TransactionResponse response = createTransactionResponse.getTransactionResponse();
                    if (response.getResponseCode().equals("1")) {
                        transId = response.getTransId();
                            CreateProfileResponse profileResponse = createTransactionResponse.getProfileResponse();
                            if (profileResponse != null) {
                                if (profileResponse.getMessages().getResultCode() == MessageTypeEnum.OK) {
                                    profileId = profileResponse.getCustomerProfileId();
                                    paymentId = profileResponse.getCustomerPaymentProfileIdList().getNumericString().get(0);
                                    String token = profileId + "-" + paymentId + "-" + credit.cvv;
                                    String cardType = response.getAccountType();
                                    String cardMasked = response.getAccountNumber();

                                    Map<String,String> mapResult = new HashMap<>();
                                    mapResult.put("message", "");
                                    mapResult.put("creditCard", cardMasked);
                                    mapResult.put("qrCode", "");
                                    mapResult.put("token", token);
                                    mapResult.put("cardType", cardType);

                                    objResponse.response = mapResult;
                                    objResponse.returnCode = 200;
                                    returnCode = 200;
                                }
                            }

                    }
                }
            }

            if (!transId.equals("")) {
                isVoid = true;
                voidAuthorizeTransaction(account, transId);
            }

            if (returnCode == 200) {
                // payment success
                return objResponse.toString();

            } else {
                String transactionMessage = "";
                // get error message from Authorize
                TransactionResponse.Errors errors = createTransactionResponse.getTransactionResponse().getErrors();
                if (errors != null) {
                    List<TransactionResponse.Errors.Error> listErrors = errors.getError();
                    for (TransactionResponse.Errors.Error error : listErrors) {
                        transactionMessage = error.getErrorText();
                    }
                }
                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("message", transactionMessage);
                objResponse.response = mapResponse;
                objResponse.returnCode = 437;
                return objResponse.toString();
            }
        } catch (Exception ex) {
            // get error, void completed transaction
            if (!transId.equals("") && !isVoid) {
                voidAuthorizeTransaction(account, transId);
            }
            Map<String,String> mapResponse = new HashMap<>();
            mapResponse.put("response", GatewayUtil.getError(ex));
            mapResponse.put("message", "");
            objResponse.returnCode = 437;
            objResponse.response = mapResponse;
            return objResponse.toString();
        }
    }
    public String voidAuthorizeTransaction(Map<String, String> account, String transId) {
        ObjResponse objResponse = new ObjResponse();
        try {
            if (KeysUtil.PRODUCTION.equals(account.get("environment"))) {
                ApiOperationBase.setEnvironment(Environment.PRODUCTION);
            } else {
                ApiOperationBase.setEnvironment(Environment.SANDBOX);
            }
            MerchantAuthenticationType merchantAuthenticationType  = new MerchantAuthenticationType() ;
            merchantAuthenticationType.setName(account.get("apiLoginId"));
            merchantAuthenticationType.setTransactionKey(account.get("transactionKey"));
            ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

            // create void request
            TransactionRequestType transactionRequest = new TransactionRequestType();
            transactionRequest.setTransactionType(TransactionTypeEnum.VOID_TRANSACTION.value());
            transactionRequest.setRefTransId(transId);

            CreateTransactionRequest apiRequest = new CreateTransactionRequest();
            apiRequest.setTransactionRequest(transactionRequest);
            logger.debug(requestId + " - voidAuthorizeTransaction request: " + gson.toJson(apiRequest));
            CreateTransactionController controller = new CreateTransactionController(apiRequest);
            controller.execute();

            objResponse.returnCode = 304;
            CreateTransactionResponse response = controller.getApiResponse();
            logger.debug(requestId + " - voidAuthorizeTransaction response: " + gson.toJson(response));
            String refundId = "";
            if (response != null) {
                if (response.getMessages().getResultCode() == MessageTypeEnum.OK) {
                    TransactionResponse result = response.getTransactionResponse();
                    refundId = result.getTransId();
                    if (result.getResponseCode().equals("1")) {
                        objResponse.returnCode = 200;
                    }
                }
            }
            logger.debug(requestId + " - transId: " + transId + " has been voided !!!! RefundId: " + refundId);
        } catch(Exception ex) {
            ex.printStackTrace();
            logger.debug("voidAuthorizeTransaction ERROR: " + GatewayUtil.getError(ex));
            objResponse.returnCode = 304;
        }

        return objResponse.toString();
    }
    public boolean isEmptyString(String s) {
        return (s == null || s.trim().isEmpty());
    }
    private String getReturnCodeAVSCVV(String avsResponse, String cvvResponse, boolean enableCheckZipCode) {
        String errorCode = "437";
        String validCode = "ZWXY";
        String inValidCode = "AN";
        String verified = "P";
        avsResponse = avsResponse.trim();
        cvvResponse = cvvResponse.trim();
        // check both AVS and CVV is empty in case payment failed
        if (avsResponse.equals("") && cvvResponse.equals(""))
            return errorCode;

        if (!avsResponse.equals("")) {
            if (validCode.contains(avsResponse)) {
                errorCode = "200";
            } else if (inValidCode.contains(avsResponse)) {
                errorCode = "430";
            } else if (verified.contains(avsResponse)) {
                errorCode = "431";
            } else if (avsResponse.equalsIgnoreCase("U")) {
                errorCode = "443";
            } else if (avsResponse.equalsIgnoreCase("R")) {
                errorCode = "445";
            } else if (avsResponse.equalsIgnoreCase("G") || avsResponse.equalsIgnoreCase("S")) {
                errorCode = "446";
            } else if (avsResponse.equalsIgnoreCase("E")) {
                errorCode = "447";
            } else {
                errorCode = "429";
            }
        } else {
            if (!enableCheckZipCode) {
                errorCode = "200";
            }
        }

        if (errorCode.equals("200") && !cvvResponse.equals("")) {
            // AVS verified is pass, continue verify CVV
            if (cvvResponse.equalsIgnoreCase("M")) {
                errorCode = "200";
            } else if (cvvResponse.equalsIgnoreCase("N")) {
                errorCode = "433";
            } else if (cvvResponse.equalsIgnoreCase("P")) {
                errorCode = "439";
            } else if (cvvResponse.equalsIgnoreCase("S")) {
                errorCode = "432";
            } else if (cvvResponse.equalsIgnoreCase("U")) {
                errorCode = "441";
            } else {
                errorCode = "432";
            }
        }

        // reset return code if all validate is ok
        if (errorCode.equals("200")) errorCode = "437";

        return errorCode;
    }

    public String createPaymentWithCreditToken(double amount, String token, String bookId, Map<String,String> account) {
        ObjResponse objResponse = new ObjResponse();
        String transId = "";
        try {
            if (KeysUtil.PRODUCTION.equals(account.get("environment"))) {
                ApiOperationBase.setEnvironment(Environment.PRODUCTION);
            } else {
                ApiOperationBase.setEnvironment(Environment.SANDBOX);
            }

            MerchantAuthenticationType merchantAuthenticationType  = new MerchantAuthenticationType() ;
            merchantAuthenticationType.setName(account.get("apiLoginId"));
            merchantAuthenticationType.setTransactionKey(account.get("transactionKey"));
            ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

            // set profile to charge
            String[] tokenData = token.split("-");
            CustomerProfilePaymentType profile = new CustomerProfilePaymentType();
            profile.setCustomerProfileId(tokenData[0]);
            PaymentProfile paymentProfile = new PaymentProfile();
            paymentProfile.setPaymentProfileId(tokenData[1]);
            if (tokenData.length > 2)
                paymentProfile.setCardCode(tokenData[2]);
            profile.setPaymentProfile(paymentProfile);

            // create payment request
            TransactionRequestType transactionRequest = new TransactionRequestType();
            transactionRequest.setTransactionType(TransactionTypeEnum.AUTH_CAPTURE_TRANSACTION.value());
            transactionRequest.setProfile(profile);
            transactionRequest.setAmount(new BigDecimal(DECIMAL_FORMAT.format(amount)));

            CreateTransactionRequest apiRequest = new CreateTransactionRequest();
            apiRequest.setTransactionRequest(transactionRequest);
            logger.debug(requestId + " - pay token request: " + gson.toJson(apiRequest));
            CreateTransactionController controller = new CreateTransactionController(apiRequest);
            controller.execute();

            CreateTransactionResponse response = controller.getApiResponse();
            logger.debug(requestId + " - pay token response: " + gson.toJson(response));
            int returnCode = 0;
            String avsCode = "";
            String cvvCode = "";
            if (response != null) {
                if (response.getMessages().getResultCode() == MessageTypeEnum.OK) {
                    TransactionResponse result = response.getTransactionResponse();
                    if (result.getResponseCode().equals("1")) {
                        transId = result.getTransId();
                        String cardMask = result.getAccountNumber();
                        String cardType = result.getAccountType();
                        String authCode = result.getAuthCode();
                        String status =  result.getMessages().getMessage().get(0).getDescription();

                        Map<String,String> mapResponse = new HashMap<>();
                        mapResponse.put("message", status);
                        mapResponse.put("transId", transId);
                        mapResponse.put("cardType", cardType);
                        mapResponse.put("authCode", authCode);
                        mapResponse.put("cardMask", cardMask);
                        objResponse.response = mapResponse;
                        objResponse.returnCode = 200;

                        returnCode = 200;
                    } else {
                        avsCode = result.getAvsResultCode() != null ? result.getAvsResultCode(): "";
                        cvvCode = result.getCvvResultCode() != null ? result.getCvvResultCode() : "";
                    }
                }

            }
            if (returnCode == 200) {
                // payment success
                return objResponse.toString();
            } else {
                // get error message from Authorize
                String transactionMessage = "";
                TransactionResponse.Errors errors = response.getTransactionResponse().getErrors();
                if (errors != null) {
                    List<TransactionResponse.Errors.Error> listErrors = errors.getError();
                    for (TransactionResponse.Errors.Error error : listErrors) {
                        transactionMessage = error.getErrorText();
                    }
                }
                return returnPaymentFailed(transId, TransactionTypeEnum.AUTH_CAPTURE_TRANSACTION.value(), "token",
                        account.get("fleetId"), bookId, amount,
                        "", transactionMessage, "", false);

            }
        } catch(Exception ex) {
            ex.printStackTrace();

            // get error, void completed transaction
            if (!transId.equals("")) {
                voidAuthorizeTransaction(account, transId);
            }
            return returnPaymentFailed(transId, TransactionTypeEnum.AUTH_CAPTURE_TRANSACTION.value(), "token",
                    account.get("fleetId"), bookId, amount,
                    "437", "", ex.getMessage(), false);
        }
    }

    /*private String returnCreditCardFailed(String transId, String transType, String paymentType,
                                          String fleetId, String bookId, double amount,
                                          String avsResponse, String cvvResponse, boolean isPreAuth) {
        logger.debug("avsResponse: " + avsResponse);
        logger.debug("cvvResponse: " + cvvResponse);

        String errorCode = getReturnCodeAVSCVV(avsResponse, cvvResponse, false);
        ObjResponse response = new ObjResponse();
        response.returnCode = Integer.parseInt(errorCode);
        return response.toString();
    }*/

    private String returnPaymentFailed(String transId, String transType, String paymentType,
                                       String fleetId, String bookId, double amount,
                                       String errorCode, String errorMessage, String message, boolean isPreAuth) {
        ObjResponse response = new ObjResponse();
        // errorCode = 437: An error occurred while processing your card. Try again in a little bit.
        errorCode = "437";
        Map<String, String> mapResponse = new HashMap<>();
        logger.debug("response: " + message);
        //mapResponse.put("response", message);
        mapResponse.put("message", errorMessage);
        response.returnCode = Integer.parseInt(errorCode);
        response.response = mapResponse;
        return response.toString();
    }

    public String preAuthPayment(double amount, String token, String bookId, Map<String,String> account) {
        ObjResponse objResponse = new ObjResponse();
        String transId = "";
        try {
            if (KeysUtil.PRODUCTION.equals(account.get("environment"))) {
                ApiOperationBase.setEnvironment(Environment.PRODUCTION);
            } else {
                ApiOperationBase.setEnvironment(Environment.SANDBOX);
            }

            MerchantAuthenticationType merchantAuthenticationType  = new MerchantAuthenticationType() ;
            merchantAuthenticationType.setName(account.get("apiLoginId"));
            merchantAuthenticationType.setTransactionKey(account.get("transactionKey"));
            ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

            // set profile to charge
            String[] tokenData = token.split("-");
            CustomerProfilePaymentType profile = new CustomerProfilePaymentType();
            profile.setCustomerProfileId(tokenData[0]);
            PaymentProfile paymentProfile = new PaymentProfile();
            paymentProfile.setPaymentProfileId(tokenData[1]);
            if (tokenData.length > 2)
                paymentProfile.setCardCode(tokenData[2]);
            profile.setPaymentProfile(paymentProfile);

            // create payment request
            TransactionRequestType transactionRequest = new TransactionRequestType();
            transactionRequest.setTransactionType(TransactionTypeEnum.AUTH_ONLY_TRANSACTION.value());
            transactionRequest.setProfile(profile);
            transactionRequest.setAmount(new BigDecimal(DECIMAL_FORMAT.format(amount)));

            CreateTransactionRequest apiRequest = new CreateTransactionRequest();
            apiRequest.setTransactionRequest(transactionRequest);
            logger.debug(requestId + " - preAuthPayment request: " + gson.toJson(apiRequest));
            CreateTransactionController controller = new CreateTransactionController(apiRequest);
            controller.execute();

            CreateTransactionResponse response = controller.getApiResponse();
            logger.debug(requestId + " - preAuthPayment response: " + gson.toJson(response));
            int returnCode = 0;
            String avsCode = "";
            String cvvCode = "";
            if (response != null) {
                if (response.getMessages().getResultCode() == MessageTypeEnum.OK) {
                    TransactionResponse result = response.getTransactionResponse();
                    if (result.getResponseCode().equals("1")) {
                        transId = result.getTransId();
                        String cardMask = result.getAccountNumber();
                        String cardType = result.getAccountType();
                        String authCode = result.getAuthCode();
                        String status =  result.getMessages().getMessage().get(0).getDescription();

                        Map<String,String> mapResponse = new HashMap<>();
                        mapResponse.put("authId", transId);
                        mapResponse.put("authAmount", String.valueOf(amount));
                        mapResponse.put("authCode", authCode);
                        mapResponse.put("authToken", token);
                        mapResponse.put("allowCapture", "true");
                        objResponse.response = mapResponse;
                        objResponse.returnCode = 200;

                        returnCode = 200;
                    } else {
                        avsCode = result.getAvsResultCode() != null ? result.getAvsResultCode(): "";
                        cvvCode = result.getCvvResultCode() != null ? result.getCvvResultCode() : "";
                    }
                }

            }
            if (returnCode == 200) {
                // payment success
                return objResponse.toString();
            } else {
                // get error message from Authorize
                String transactionMessage = "";
                TransactionResponse.Errors errors = response.getTransactionResponse().getErrors();
                if (errors != null) {
                    List<TransactionResponse.Errors.Error> listErrors = errors.getError();
                    for (TransactionResponse.Errors.Error error : listErrors) {
                        transactionMessage = error.getErrorText();
                    }
                }
                return returnPaymentFailed(transId, TransactionTypeEnum.AUTH_ONLY_TRANSACTION.value(), "token",
                        account.get("fleetId"), bookId, amount,
                        "", transactionMessage, "", false);

            }
        } catch(Exception ex) {
            ex.printStackTrace();

            // get error, void completed transaction
            if (!transId.equals("")) {
                voidAuthorizeTransaction(account, transId);
            }
            return returnPaymentFailed(transId, TransactionTypeEnum.AUTH_ONLY_TRANSACTION.value(), "token",
                    account.get("fleetId"), bookId, amount,
                    "437", "", ex.getMessage(), false);
        }
    }

    public String preAuthCapture(String bookId, String token, double amount, String authId, Map<String,String> account){
        ObjResponse objResponse = new ObjResponse();
        String transId = "";
        try {
            if (KeysUtil.PRODUCTION.equals(account.get("environment"))) {
                ApiOperationBase.setEnvironment(Environment.PRODUCTION);
            } else {
                ApiOperationBase.setEnvironment(Environment.SANDBOX);
            }

            MerchantAuthenticationType merchantAuthenticationType  = new MerchantAuthenticationType() ;
            merchantAuthenticationType.setName(account.get("apiLoginId"));
            merchantAuthenticationType.setTransactionKey(account.get("transactionKey"));
            ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

            // create payment request
            TransactionRequestType transactionRequest = new TransactionRequestType();
            transactionRequest.setTransactionType(TransactionTypeEnum.PRIOR_AUTH_CAPTURE_TRANSACTION.value());
            transactionRequest.setRefTransId(authId);
            transactionRequest.setAmount(new BigDecimal(DECIMAL_FORMAT.format(amount)));

            CreateTransactionRequest apiRequest = new CreateTransactionRequest();
            apiRequest.setTransactionRequest(transactionRequest);
            logger.debug(requestId + " - preAuthCapture request: " + gson.toJson(apiRequest));
            CreateTransactionController controller = new CreateTransactionController(apiRequest);
            controller.execute();

            CreateTransactionResponse response = controller.getApiResponse();
            logger.debug(requestId + " - preAuthCapture response: " + gson.toJson(response));
            int returnCode = 0;
            String avsCode = "";
            String cvvCode = "";
            if (response != null) {
                if (response.getMessages().getResultCode() == MessageTypeEnum.OK) {
                    TransactionResponse result = response.getTransactionResponse();
                    if (result.getResponseCode().equals("1")) {
                        transId = result.getTransId();
                        String cardMask = result.getAccountNumber();
                        String cardType = result.getAccountType();
                        String authCode = result.getAuthCode();
                        String status =  result.getMessages().getMessage().get(0).getDescription();

                        Map<String,String> mapResponse = new HashMap<>();
                        mapResponse.put("message", status);
                        mapResponse.put("transId", transId);
                        mapResponse.put("cardType", cardType);
                        mapResponse.put("authCode", authCode);
                        mapResponse.put("cardMask", cardMask);
                        objResponse.response = mapResponse;
                        objResponse.returnCode = 200;

                        returnCode = 200;
                    } else {
                        avsCode = result.getAvsResultCode() != null ? result.getAvsResultCode(): "";
                        cvvCode = result.getCvvResultCode() != null ? result.getCvvResultCode() : "";
                    }
                }

            }
            if (returnCode == 200) {
                return objResponse.toString();
            } else {
                // get error message from Authorize
                String transactionMessage = "";
                TransactionResponse.Errors errors = response.getTransactionResponse().getErrors();
                if (errors != null) {
                    List<TransactionResponse.Errors.Error> listErrors = errors.getError();
                    for (TransactionResponse.Errors.Error error : listErrors) {
                        transactionMessage = error.getErrorText();
                    }
                }
                return returnPaymentFailed(transId, TransactionTypeEnum.PRIOR_AUTH_CAPTURE_TRANSACTION.value(), "token",
                        account.get("fleetId"), bookId, amount,
                        "", transactionMessage, "", false);
            }
        } catch(Exception ex) {
            ex.printStackTrace();

            // get error, void completed transaction
            if (!transId.equals("")) {
                voidAuthorizeTransaction(account, transId);
            }
            return returnPaymentFailed(transId, TransactionTypeEnum.PRIOR_AUTH_CAPTURE_TRANSACTION.value(), "token",
                    account.get("fleetId"), bookId, amount,
                    "437", "", ex.getMessage(), false);
        }
    }

    public String doPaymentWithCardInfo(String bookId, double amount,
                                        String cardNumber, String expiredDate, String cvv, String cardHolder, String postalCode, Map<String,String> account) throws IOException {
        logger.debug(requestId + " - doPaymentWithCardInfo for card " + cardNumber.substring(cardNumber.length() - 4, cardNumber.length()) + " ...");
        ObjResponse objResponse = new ObjResponse();
        String transId = "";
        String authorizePaymentType = "";
        try {
            Boolean enableCheckZipCode = false;
            if (!isEmptyString(postalCode)) {
                enableCheckZipCode = true;
            }

            if (KeysUtil.PRODUCTION.equals(account.get("environment"))) {
                ApiOperationBase.setEnvironment(Environment.PRODUCTION);
            } else {
                ApiOperationBase.setEnvironment(Environment.SANDBOX);
            }

            MerchantAuthenticationType merchantAuthenticationType  = new MerchantAuthenticationType() ;
            merchantAuthenticationType.setName(account.get("apiLoginId"));
            merchantAuthenticationType.setTransactionKey(account.get("transactionKey"));
            ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

            CreditCardType creditCard = new CreditCardType();
            creditCard.setCardNumber(cardNumber);
            creditCard.setExpirationDate(expiredDate);
            creditCard.setCardCode(cvv);

            PaymentType paymentType = new PaymentType();
            paymentType.setCreditCard(creditCard);

            authorizePaymentType = TransactionTypeEnum.AUTH_CAPTURE_TRANSACTION.value();
            TransactionRequestType transactionRequestType = new TransactionRequestType();
            transactionRequestType.setTransactionType(TransactionTypeEnum.AUTH_CAPTURE_TRANSACTION.value());
            transactionRequestType.setPayment(paymentType);
            transactionRequestType.setAmount(new BigDecimal(DECIMAL_FORMAT.format(amount)));

            CustomerAddressType addressType = new CustomerAddressExType();
            String[] name = cardHolder.split(" ");
            if (name.length > 1) {
                String lastName = name[name.length - 1];
                if (cardHolder.length() > lastName.length()) {
                    addressType.setFirstName(cardHolder.substring(0, cardHolder.length() - lastName.length() ));
                    addressType.setLastName(lastName);
                } else {
                    addressType.setFirstName(cardHolder);
                }
            } else {
                addressType.setFirstName(cardHolder);
            }
            if (enableCheckZipCode) {
                addressType.setZip(postalCode);
            }

            transactionRequestType.setBillTo(addressType);

            CreateTransactionRequest createTransactionRequest = new CreateTransactionRequest();
            createTransactionRequest.setTransactionRequest(transactionRequestType);
            String cardLog = "XXXXXXXXXXXX" + cardNumber.substring(cardNumber.length() - 4);
            String log = gson.toJson(createTransactionRequest).replace(cardNumber, cardLog);
            logger.debug(requestId + " - pay card request: " + log);
            CreateTransactionController controller = new CreateTransactionController(createTransactionRequest);
            controller.execute();

            CreateTransactionResponse createTransactionResponse = controller.getApiResponse();
            logger.debug(requestId + " - pay card response: " + gson.toJson(createTransactionResponse));
            int returnCode = 0;
            String avsCode = "";
            String cvvCode = "";
            if (createTransactionResponse != null) {
                if (createTransactionResponse.getMessages().getResultCode() == MessageTypeEnum.OK) {
                    TransactionResponse response = createTransactionResponse.getTransactionResponse();
                    if (response.getResponseCode().equals("1")) {
                        transId = response.getTransId();
                        String cardMask = response.getAccountNumber();
                        String cardType = response.getAccountType();
                        String authCode = response.getAuthCode();
                        String status =  response.getMessages().getMessage().get(0).getDescription();

                        Map<String,String> mapResult = new HashMap<>();
                        mapResult.put("message", "Success!");
                        mapResult.put("transId", transId);
                        mapResult.put("cardType", cardType);
                        mapResult.put("authCode", authCode);
                        mapResult.put("cardMask", cardMask);
                        objResponse.response = mapResult;
                        objResponse.returnCode = 200;
                        returnCode = 200;
                    } else {
                        avsCode = response.getAvsResultCode() != null ? response.getAvsResultCode(): "";
                        cvvCode = response.getCvvResultCode() != null ? response.getCvvResultCode() : "";
                    }
                }
            }

            if (returnCode == 200) {
                // payment success
                return objResponse.toString();

            } else {
                // get error message from Authorize
                String transactionMessage = "";
                TransactionResponse.Errors errors = createTransactionResponse.getTransactionResponse().getErrors();
                if (errors != null) {
                    List<TransactionResponse.Errors.Error> listErrors = errors.getError();
                    for (TransactionResponse.Errors.Error error : listErrors) {
                        transactionMessage = error.getErrorText();
                    }
                }
                return returnPaymentFailed(transId, TransactionTypeEnum.AUTH_CAPTURE_TRANSACTION.value(), "token",
                        account.get("fleetId"), bookId, amount,
                        "", transactionMessage, "", false);
            }
        } catch(Exception ex) {
            ex.printStackTrace();

            // get error, void completed transaction
            if (!transId.equals("")) {
                voidAuthorizeTransaction(account, transId);
            }
            return returnPaymentFailed(transId, authorizePaymentType, "input",
                    account.get("fleetId"), bookId, amount, "437", "", ex.getMessage(), false);
        }
    }
}
