package com.qupworld.paymentgateway.controllers;

import com.pg.util.CommonArrayUtils;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.TimezoneUtil;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.controllers.GatewayUtil.GatewayUtil;
import com.qupworld.paymentgateway.controllers.threading.UpdateDriverCreditBalanceThread;
import com.qupworld.paymentgateway.entities.DriverCreditBalance;
import com.qupworld.paymentgateway.entities.WalletTransfer;
import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.RedisDao;
import com.qupworld.paymentgateway.models.RedisImpl;
import com.qupworld.paymentgateway.models.SQLDao;
import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.account.Account;
import com.qupworld.paymentgateway.models.mongo.collections.fleet.Fleet;
import com.qupworld.paymentgateway.models.mongo.collections.operations.Operations;
import com.qupworld.paymentgateway.models.mongo.collections.pricingplan.PricingPlan;
import com.qupworld.paymentgateway.models.mongo.collections.wallet.ConfigWallet;
import com.qupworld.paymentgateway.models.mongo.collections.wallet.DriverWallet;
import com.qupworld.paymentgateway.models.mongo.collections.wallet.PaymentChannel;
import com.qupworld.service.QUpSearchPublisher;
import com.qupworld.util.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import java.util.*;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
public class UpdateDriverCreditBalance {

    final static Logger logger = LogManager.getLogger(UpdateDriverCreditBalance.class);
    public String type;
    public String fleetId;
    public String driverId;
    public String senderId;
    public String receiverId;
    public double amount;
    public String currencyISO;
    public MongoDao mongoDao;
    public SQLDao sqlDao;
    public String reason = "";
    public JSONObject additionalData = new JSONObject();
    public String method = "";
    public String transactionId = "";
    public String walletName = "";
    public String from = "";
    public PaymentUtil paymentUtil;
    public String requestId;
    public RedisDao redisDao = new RedisImpl();

    private double currentBalance = 0.0;

    public String doUpdate() {
        try {
            // check if there is another update process
            boolean isCache = redisDao.addUniqueData(requestId, driverId, "cache-" + driverId);
            while (!isCache) {
                logger.debug(requestId + " - there is another update process ...");
                // if not success means this orderId has ben cached for the previous request
                // wait 3 seconds then try again
                Thread.sleep(3000);
                isCache = redisDao.addUniqueData(requestId, driverId, "cache-" + driverId);
            }

            WalletUtil walletUtil = new WalletUtil(requestId);
            currentBalance = walletUtil.getCurrentBalance(driverId, "credit", currencyISO);
            double newAmount = Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(amount + currentBalance));
            logger.debug(requestId + " - driverId: " + driverId + " - newAmount: " + newAmount);
            Account account = mongoDao.getAccount(driverId);
            String driverName = account.fullName;
            String driverPhone = account.phone;
            String driverUsername = account.username != null ? account.username : "";
            String companyId = "";
            String companyName = "";
            Operations operations = mongoDao.getOperation(account.userId);
            if (operations != null && operations.company != null) {
                companyId = operations.company.companyId;
                companyName = operations.company.companyName;
            }
            String senderName = "";
            String senderPhone = "";
            if (senderId != null && !senderId.isEmpty()) {
                Account sender = mongoDao.getAccount(senderId);
                senderName = sender.firstName != null ? sender.firstName.trim() : "";
                senderPhone = sender.phone;
            }
            String receiverName = "";
            String receiverPhone = "";
            if (receiverId != null && !receiverId.isEmpty()) {
                Account receiver = mongoDao.getAccount(receiverId);
                receiverName = receiver.firstName != null ? receiver.firstName.trim() : "";
                receiverPhone = receiver.phone;
            }
            String currencySymbol = "";
            Fleet fleet = mongoDao.getFleetInfor(fleetId);
            for (com.qupworld.paymentgateway.models.mongo.collections.fleet.Currency currency : fleet.currencies) {
                if (currencyISO.equals(currency.iso))
                    currencySymbol = currency.symbol;
            }

            DriverCreditBalance driverCreditBalance = new DriverCreditBalance();
            driverCreditBalance.fleetId = fleet.fleetId;
            driverCreditBalance.fleetName = fleet.name;
            driverCreditBalance.driverId = driverId;
            driverCreditBalance.driverName = driverName;
            driverCreditBalance.driverPhone = driverPhone;
            driverCreditBalance.driverUserName = driverUsername;
            driverCreditBalance.companyId = companyId;
            driverCreditBalance.companyName = companyName;
            driverCreditBalance.type = type;
            driverCreditBalance.amount = amount;
            driverCreditBalance.currentAmount = currentBalance;
            driverCreditBalance.newAmount = newAmount;
            driverCreditBalance.reason = reason;
            driverCreditBalance.bookId = "";
            driverCreditBalance.operatorId = "";
            driverCreditBalance.operatorName = "";
            driverCreditBalance.currencyISO = currencyISO;
            driverCreditBalance.currencySymbol = currencySymbol;
            driverCreditBalance.cardMasked = "";
            driverCreditBalance.walletName = walletName;
            driverCreditBalance.transactionId = transactionId;
            driverCreditBalance.createdDate = TimezoneUtil.getGMTTimestamp();
            driverCreditBalance.senderId = senderId;
            driverCreditBalance.senderName = senderName;
            driverCreditBalance.senderPhone= senderPhone;
            driverCreditBalance.receiverId = receiverId;
            driverCreditBalance.receiverName = receiverName;
            driverCreditBalance.receiverPhone = receiverPhone;

            if (type.equals(CommonArrayUtils.DRIVER_WALLET_TYPE[5])) {
                String channel = additionalData.get("channel").toString();
                String sortName = "";
                if (!channel.isEmpty()) {
                    sortName = getChannelLog(requestId, fleet, "Wallet", KeysUtil.MOLPAY, channel.toLowerCase());
                }
                driverCreditBalance.channelLog = sortName;

                driverCreditBalance = updateBufferSetting(fleet, driverCreditBalance, KeysUtil.MOLPAY);

            } else if (type.equals(CommonArrayUtils.DRIVER_WALLET_TYPE[7]) ||  // driverTopUpForPax
                       type.equals(CommonArrayUtils.DRIVER_WALLET_TYPE[6])) { // cashExcess
                driverCreditBalance.bookId = additionalData.get("bookId").toString();
                driverCreditBalance.bookingFare = Double.parseDouble(additionalData.get("bookingFare").toString());
                driverCreditBalance.receivedAmount = Double.parseDouble(additionalData.get("receivedAmount").toString());
            } else if (type.equals(CommonArrayUtils.DRIVER_WALLET_TYPE[9])) { // bookingPromo
                driverCreditBalance.bookId = additionalData.get("bookId").toString();
                driverCreditBalance.bookingFare = Double.parseDouble(additionalData.get("bookingFare").toString());
            } else if (type.equals(CommonArrayUtils.DRIVER_WALLET_TYPE[0])) { // topUp
                driverCreditBalance.bookId = additionalData.get("bookId").toString();
                driverCreditBalance.cardMasked = additionalData.get("cardMasked").toString();
            } else if (type.equals(CommonArrayUtils.DRIVER_WALLET_TYPE[2])) { // editBalance
                driverCreditBalance.operatorId = additionalData.get("operatorId").toString();
                driverCreditBalance.operatorName = additionalData.get("operatorName").toString();
            } else if (type.equals(CommonArrayUtils.DRIVER_WALLET_TYPE[13])) { // topUpWallet
                driverCreditBalance = updateBufferSetting(fleet, driverCreditBalance, walletName);
            }

            List<String> includeBookingTypes = Arrays.asList(
                        CommonArrayUtils.DRIVER_WALLET_TYPE[1],  // bookingDeduction
                        CommonArrayUtils.DRIVER_WALLET_TYPE[14], // insurance
                        CommonArrayUtils.DRIVER_WALLET_TYPE[10], // referralEarning
                        CommonArrayUtils.DRIVER_WALLET_TYPE[18], // cancellationPenalty
                        CommonArrayUtils.DRIVER_WALLET_TYPE[19], // customerDebt
                        CommonArrayUtils.DRIVER_WALLET_TYPE[20] // merchantCommission
                    );
            if (includeBookingTypes.contains(type)) {
                driverCreditBalance.bookId = additionalData.get("bookId").toString();
            }

            String updateResult =  doUpdateBalance(driverCreditBalance, fleet, account, newAmount, driverCreditBalance.bookId);
            // already updated - remove cache unique then return completed
            redisDao.removeUniqueData(requestId, driverId);
            return updateResult;
        } catch(Exception ex) {
            logger.debug(requestId + " - UpdateDriverCreditBalance exception: " + CommonUtils.getError(ex));
            // exception - remove cache unique before return
            redisDao.removeUniqueData(requestId, driverId);
            ObjResponse objResponse = new ObjResponse();
            objResponse.returnCode = 525;
            objResponse.message = "Something went wrong. Please try again later.";
            return objResponse.toString();
        }

    }

    private String doUpdateBalance(DriverCreditBalance driverCreditBalance, Fleet fleet, Account account, double newBalance, String orderId) {
        ObjResponse objResponse = new ObjResponse();
        try {
            List<AmountByCurrency> balances;
            WalletUtil walletUtil = new WalletUtil(requestId);
            long idAddBalance = sqlDao.addDriverBalanceTransaction(driverCreditBalance);
            if (idAddBalance > 0) {
                if (account.driverInfo.creditBalances != null && !account.driverInfo.creditBalances.isEmpty()) {
                    balances = account.driverInfo.creditBalances;
                    boolean isNewCurrency = true;
                    for (AmountByCurrency balance : balances) {
                        if (balance.currencyISO != null && balance.currencyISO.equals(currencyISO)) {
                            isNewCurrency = false;
                            balance.value = newBalance;
                            break;
                        }
                    }
                    if (isNewCurrency) {
                        AmountByCurrency balance = new AmountByCurrency();
                        balance.value = amount;
                        balance.currencyISO = currencyISO;
                        balances.add(balance);
                    }
                } else {
                    // driver does not setup balance, initial balance with zero amount for other currencies
                    balances = new ArrayList<>();
                    for (com.qupworld.paymentgateway.models.mongo.collections.fleet.Currency currency : fleet.currencies) {
                        AmountByCurrency amountByCurrency = new AmountByCurrency();
                        amountByCurrency.currencyISO = currency.iso;
                        if (currency.iso.equals(currencyISO))
                            amountByCurrency.value = amount;
                        else
                            amountByCurrency.value = 0.0;

                        balances.add(amountByCurrency);
                    }
                }

                AmountByCurrency creditWallet = null;
                DriverWallet driverWallet = mongoDao.getDriverWallet(account.userId);
                if (driverWallet != null && driverWallet.creditWallet != null && driverWallet.creditWallet.currencyISO != null) {
                    if (driverWallet.creditWallet.currencyISO.equals(currencyISO)) {
                        creditWallet = driverWallet.creditWallet;
                        creditWallet.value = newBalance;
                    }
                } else {
                    // DriverWallet did not exists - add new one
                    AmountByCurrency balance = new AmountByCurrency();
                    balance.value = 0.0;
                    balance.currencyISO = currencyISO;

                    driverWallet = new DriverWallet();
                    driverWallet.fleetId = account.fleetId;
                    driverWallet.userId = account.userId;
                    driverWallet.creditWallet = balance;
                    mongoDao.addDriverWallet(driverWallet);
                }
                if (creditWallet == null) {
                    creditWallet = new AmountByCurrency();
                    creditWallet.currencyISO = currencyISO;
                    creditWallet.value = amount;
                }
                boolean updated = mongoDao.updateCreditWallet(account.userId, balances, creditWallet);
                // update credit balance to Account and DriverWallet were succeed!!!
                // continue update to list currencies on Operations - to disable/enable warning message when receive cash booking
                if (updated) {
                    try {
                        boolean balanceUpdated = updateDriverBalance(newBalance);
                        if (balanceUpdated) {
                            // update wallet
                            if (orderId.isEmpty())
                                orderId = GatewayUtil.getOrderId();
                            WalletTransfer walletTransfer = new WalletTransfer();
                            walletTransfer.fleetId = fleet.fleetId;
                            walletTransfer.driverId = account.userId;
                            walletTransfer.currencyISO = currencyISO;
                            walletTransfer.referenceId = type + "-" + orderId;
                            walletTransfer.transferType = type;
                            walletTransfer.transactionId = orderId;

                            if (amount > 0) {
                                walletTransfer.amount = amount;
                                walletUtil.updateCreditWallet(walletTransfer, true);
                            } else {
                                walletTransfer.amount = -amount; // convert to positive value
                                walletUtil.updateCreditWallet(walletTransfer, false);
                            }

                            // call to Reporting thread
                            UpdateDriverCreditBalanceThread topupThread = new UpdateDriverCreditBalanceThread();
                            driverCreditBalance.id = idAddBalance;
                            topupThread.driverCreditBalance = driverCreditBalance;
                            topupThread.requestId = requestId;
                            topupThread.start();

                            if(!ServerConfig.mongo_oplog){
                                //push data to module search
                                QUpSearchPublisher searchPublisher = QUpSearchPublisher.getInstance();
                                Account accountUpdated = mongoDao.getAccount(driverId);
                                searchPublisher.sendDataAccount(requestId, accountUpdated);
                            }

                            // send notify to Point System
                            if (paymentUtil.methodIncreasePoint.contains(method)) {
                                JSONObject objBody = new JSONObject();
                                objBody.put("fleetId", fleetId);
                                objBody.put("userId", driverId);
                                objBody.put("appType", "driver");
                                objBody.put("amount", amount);
                                objBody.put("currencyISO", currencyISO);
                                objBody.put("method", method);
                                objBody.put("transactionId", transactionId);
                                logger.debug(requestId + " - URL: " + PaymentUtil.notify_topup);
                                logger.debug(requestId + " - data: " + objBody.toJSONString());
                                int returnCode = CommonUtils.sendPointSystem(requestId, objBody.toJSONString(),
                                        PaymentUtil.notify_topup, ServerConfig.point_system_user, ServerConfig.point_system_pass);
                                logger.debug(requestId + " - pointSystemResponse: " + returnCode);
                                if (returnCode != 200) {
                                    // something went wrong!!!
                                    try {
                                        Thread.sleep(2000);
                                    } catch (InterruptedException ignore) {}
                                    returnCode = CommonUtils.sendPointSystem(requestId, objBody.toJSONString(),
                                            PaymentUtil.notify_topup, ServerConfig.point_system_user, ServerConfig.point_system_pass);
                                    logger.debug(requestId + " - pointSystemResponse: " + returnCode);
                                }
                            }

                            // top-up success - notify to Jupiter and driver
                            if (from.equals("cc")) {
                                paymentUtil.notifyUpdateBalanceToDispatch(requestId, driverId, "credit", amount, currencyISO);
                            }

                            objResponse.returnCode = 200;
                            objResponse.message = "Success";
                            objResponse.response = balances;
                            return objResponse.toString();
                        } else {
                            logger.debug(requestId + " - cannot update to Operations !!! Revert updated data!");
                            // do void transaction on the caller method

                            // roll back updated data
                            for (AmountByCurrency balance : balances) {
                                if (balance.currencyISO != null && balance.currencyISO.equals(currencyISO)) {
                                    balance.value = currentBalance;
                                }
                            }
                            creditWallet = new AmountByCurrency();
                            creditWallet.currencyISO = currencyISO;
                            creditWallet.value = currentBalance;

                            mongoDao.updateCreditWallet(account.userId, balances, creditWallet);

                            // remove history
                            sqlDao.deleteDriverBalanceTransaction(idAddBalance);

                            // return error
                            objResponse.returnCode = 525;
                            objResponse.message = "Something went wrong. Please try again later.";
                            return objResponse.toString();
                        }
                    } catch (Exception ex) {
                        logger.debug(requestId + " - update to credit wallet error: " + CommonUtils.getError(ex));
                        objResponse.returnCode = 525;
                        objResponse.message = "Something went wrong. Please try again later.";
                        return objResponse.toString();
                    }
                }
            } else {
                // cannot update to mysql - return error with empty amount of all currencies
                objResponse.returnCode = 525;
                objResponse.message = "Something went wrong. Please try again later.";
                balances = new ArrayList<>();
                for (com.qupworld.paymentgateway.models.mongo.collections.fleet.Currency currency : fleet.currencies) {
                    AmountByCurrency amountByCurrency = new AmountByCurrency();
                    amountByCurrency.currencyISO = currency.iso;
                    amountByCurrency.value = 0.0;
                    balances.add(amountByCurrency);
                }

            }

            // get list balances and return
            objResponse.response = balances;

            return objResponse.toString();
        } catch (Exception ex) {
            logger.debug(requestId + " - doUpdateBalance exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            objResponse.message = "Something went wrong. Please try again later.";
            return objResponse.toString();
        }
    }

    private boolean updateDriverBalance(double newBalance) {
        boolean isUpdated = false;
        PricingPlan pricingPlan = mongoDao.getPricingPlan(fleetId);
        if (pricingPlan != null && pricingPlan.driverDeposit != null && pricingPlan.driverDeposit.enable) {
            List<AmountByCurrency> currencies = pricingPlan.driverDeposit.valueByCurrencies;
            Map<String, Double> map = new HashMap<>();
            if (currencies != null && !currencies.isEmpty()) {
                for (AmountByCurrency amountByCurrency : currencies) {
                    map.put(amountByCurrency.currencyISO, amountByCurrency.value);
                }
                List<String> validCurrencies = new ArrayList<>();
                for (String key : map.keySet()) {
                    double amount = map.get(key);
                    if (key.equals(currencyISO) && newBalance > amount) {
                        validCurrencies.add(currencyISO);
                    }
                }
                isUpdated = mongoDao.updateDriverBalance(driverId, validCurrencies);
                if (!isUpdated) {
                    logger.debug(requestId + " - updating Operations is error !!! Re-try");
                    try {
                        Thread.sleep(1000);
                    } catch(InterruptedException ignore) {}
                    isUpdated = mongoDao.updateDriverBalance(driverId, validCurrencies);
                }
                logger.debug(requestId + " - isUpdated = " + isUpdated);
                if (isUpdated) {
                    logger.debug(requestId + " - validCurrencies = " + validCurrencies.size());
                    // re-check update data
                    Operations operations = mongoDao.getOperation(driverId);
                    int balanceCurrencies = operations.balanceCurrencies != null ? operations.balanceCurrencies.size() : 0;
                    logger.debug(requestId + " - balanceCurrencies = " + balanceCurrencies);
                    if (validCurrencies.size() == balanceCurrencies) {
                        logger.debug(requestId + " - updating Operations is ok");
                    } else {
                        logger.debug(requestId + " - updating Operations ERROR - try to update one more time");
                        isUpdated = mongoDao.updateDriverBalance(driverId, validCurrencies);
                        logger.debug(requestId + " - isUpdated = " + isUpdated);
                        Operations operationsUpdated = mongoDao.getOperation(driverId);
                        balanceCurrencies = operationsUpdated.balanceCurrencies != null ? operationsUpdated.balanceCurrencies.size() : 0;
                        logger.debug(requestId + " - balanceCurrencies = " + balanceCurrencies);
                        if (validCurrencies.size() == balanceCurrencies) {
                            logger.debug(requestId + " - updating Operations is ok");
                        } else {
                            logger.debug(requestId + " - retry updating Operations ERROR - notify to Slack");
                            try {
                                JSONObject objData = new JSONObject();
                                objData.put("driverId", driverId);
                                objData.put("validCurrencies", validCurrencies);
                                objData.put("balanceCurrencies", operationsUpdated.balanceCurrencies);
                                NotifyToSlack.notifyUpdateOperation(requestId,objData.toJSONString());
                            } catch (Exception ex) {
                                logger.debug(requestId + " - notifyUpdateOperation exception: " + CommonUtils.getError(ex));
                            }

                        }
                    }
                }
            }
        }

        return isUpdated;
    }

    private DriverCreditBalance updateBufferSetting(Fleet fleet, DriverCreditBalance driverCreditBalance, String gateway) {
        double chargedAmount = driverCreditBalance.amount;
        ConfigWallet configWallet = mongoDao.getConfigWallet(fleet.fleetId, gateway);
        String chargedType = "";
        double differentPercent = 0.0;
        if (configWallet != null && configWallet.chargeExtra != null) {
            chargedType = configWallet.chargeExtra.type != null ? configWallet.chargeExtra.type : "";
            differentPercent = configWallet.chargeExtra.value;
            if (chargedType.equals("extra")) {
                chargedAmount = chargedAmount + (chargedAmount*differentPercent/100);
            } else if (chargedType.equals("discount")) {
                chargedAmount = chargedAmount - (chargedAmount*differentPercent/100);
            } else if (chargedType.equals("bonus")) {
                differentPercent = 0.0;
            }
        }

        driverCreditBalance.chargedType = chargedType;
        driverCreditBalance.differentPercent = differentPercent;
        driverCreditBalance.chargedAmount = chargedAmount;

        return driverCreditBalance;
    }

    String getChannelLog(String requestId, Fleet fleet, String type, String gateway, String channel) {
        String sortName = "";
        try {
            if (gateway.equals(KeysUtil.MOLPAY)) {
                if (!channel.isEmpty()) {
                    logger.debug(requestId + " - channel: " + channel);
                    Map<String,String> supportedChannels = new HashMap<>();
                    supportedChannels.put("Credit","Credit card");
                    supportedChannels.put("maybank2u","Maybank2u");
                    supportedChannels.put("cimb","CIMB Clicks");
                    supportedChannels.put("BIMB","Bank Islam");
                    supportedChannels.put("rhb","RHB Now");
                    supportedChannels.put("publicbank","Public Bank");
                    supportedChannels.put("amb","Am Online");
                    supportedChannels.put("hlb","Hong Leong Connect");
                    supportedChannels.put("bankislam","Bank Islam");
                    supportedChannels.put("amb","AmOnline");
                    supportedChannels.put("alliancebank","Alliance online");
                    supportedChannels.put("abb / affin-epg","Affin Bank");
                    supportedChannels.put("muamalat","i-Muamalat");
                    supportedChannels.put("RazerPay","Razer Pay");
                    supportedChannels.put("webcash","Webcash");
                    supportedChannels.put("TNG-EWALLET","Touch N’ Go");
                    supportedChannels.put("BOOST","BOOST");
                    supportedChannels.put("GrabPay","GrabPay");
                    supportedChannels.put("MB2U_QRPay-Push","Maybank QRPay");
                    supportedChannels.put("cash","Razer Cash via 7E");
                    supportedChannels.put("jompay","JomPay ATM 9282");
                    supportedChannels.put("CIMB-VA","CIMB Virtual Account");
                    supportedChannels.put("cash99","Razer Cash via 99");
                    supportedChannels.put("FPX_MB2U / MB2u","Maybank2u");
                    supportedChannels.put("FPX_CIMBCLICKS / CIMB-Clicks","CIMB Clicks");
                    supportedChannels.put("FPX_RHB / RHB-ONL","RHB Now");
                    supportedChannels.put("FPX_PBB / PBeBank","PBe");
                    supportedChannels.put("FPX_HLB / HLB-ONL / PEXPLUS","Hong Leong");
                    supportedChannels.put("FPX_BIMB","Bank Islam");
                    supportedChannels.put("FPX_AMB / AMB-W2W","AmOnline");
                    supportedChannels.put("FPX_ABMB / ALB-ONL","Alliance online");
                    supportedChannels.put("FPX_ABB / Affin-EPG","Affin Online");
                    supportedChannels.put("FPX_BMMB","i-Muamalat");
                    supportedChannels.put("FPX_BKRM","i-Rakyat");
                    supportedChannels.put("FPX_BSN","myBSN");
                    supportedChannels.put("FPX_OCBC","OCBC Online");
                    supportedChannels.put("FPX_UOB","UOB Online");
                    supportedChannels.put("FPX_HSBC","HSBC Online");
                    supportedChannels.put("FPX_SCB","Standard Chartered");
                    supportedChannels.put("FPX_KFH","KFH Online");
                    supportedChannels.put("FPX_B2B","FPX B2B Model");
                    supportedChannels.put("FPX_EMANDATE","FPX e-Mandate");
                    supportedChannels.put("FPX_DIRECTDEBIT","FPX DirectDebit");
                    Optional<String> optionalChannel = supportedChannels.entrySet().stream()
                            .filter(e -> e.getKey().toLowerCase().contains(channel))
                            .map(Map.Entry::getValue)
                            .findFirst();
                    sortName = optionalChannel.orElse("Razer Pay"); // if not found the channel name, return default

                    // check specific cash channels
                    if (channel.equalsIgnoreCase("Cash-99SM")) sortName = "99SpeedMart";
                    if (channel.equalsIgnoreCase("Cash-Esapay")) sortName = "Esapay Cash Retail";
                    if (channel.equalsIgnoreCase("Cash-Deposit")) sortName = "Cash-in to PG";
                    if (channel.equalsIgnoreCase("Cash-MBBATM")) sortName = "Maybank ATM";
                }

                // check payment channel if top up with Razor
                PaymentChannel paymentChannel = mongoDao.getPaymentChannel(fleet.fleetId, gateway, type, channel);
                if (paymentChannel != null) {
                    sortName = paymentChannel.log != null ? paymentChannel.log : "";
                }
            }

        } catch (Exception ex) {
            logger.debug(requestId + " - getChannelLog exception: " + CommonUtils.getError(ex));
        }
        return sortName;
    }
}
