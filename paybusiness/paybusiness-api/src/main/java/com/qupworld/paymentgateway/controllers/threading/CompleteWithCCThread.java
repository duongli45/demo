package com.qupworld.paymentgateway.controllers.threading;

import com.google.gson.Gson;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.controllers.UpdateTicket;
import com.qupworld.paymentgateway.entities.HydraIncome;
import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.MongoDaoImpl;
import com.qupworld.paymentgateway.models.RedisDao;
import com.qupworld.paymentgateway.models.SQLDao;
import com.qupworld.paymentgateway.models.mongo.collections.UsersSystem;
import com.qupworld.paymentgateway.models.mongo.collections.account.Account;
import com.qupworld.paymentgateway.models.mongo.collections.booking.Booking;
import com.qupworld.paymentgateway.models.mongo.collections.bookingStatistic.BookingStatistic;
import com.qupworld.paymentgateway.models.mongo.collections.bookingStatistic.Statistic;
import com.qupworld.service.QUpReportPublisher;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.TimezoneUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class CompleteWithCCThread extends Thread {
    public Booking booking;
    public String fleetId;
    public String ticket;
    public String requestId;
    public String transactionStatus;
    public String gateway;
    private String psgFleetId;
    private String completedTime;
    private double profit;
    private double revenue;
    private double fleetProfit;

    public MongoDao mongoDao;
    public SQLDao sqlDao;
    public RedisDao redisDao;
    public Account passenger;
    public boolean updateData = true;
    public double newBalance = 0.0;

    Gson gson = new Gson();
    private boolean updateTicketSuccess = false;
    private SimpleDateFormat sdfMongo = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");

    final static Logger logger = LogManager.getLogger(CompleteWithCCThread.class);
    @Override
    public void run() {
        sendReportData();
        sendDashboardData();
        if (booking.bookFrom.equalsIgnoreCase(KeysUtil.dash_board) || booking.bookFrom.equalsIgnoreCase(KeysUtil.corp_board)) {
            sendDMCData(booking.bookFrom.toLowerCase());
        }
        if (booking.pricingType == 1 && (transactionStatus.equals(KeysUtil.PAYMENT_CANCELED) || transactionStatus.equals(KeysUtil.PAYMENT_NOSHOW))) {
            sendHydraReportData();
        }

        try {
            // wait 30 second for app to get Ticket data after payment
            Thread.sleep(30000);
        } catch (InterruptedException ignore) {}
        // remove cache for eta
        redisDao.removeTmp(requestId, "fareType"+"-"+booking.bookId);
        redisDao.removeTmp(requestId, "distance"+"-"+booking.bookId);
        redisDao.removeTmp(requestId, "normalFare"+"-"+booking.bookId);
        if (KeysUtil.STRIPE.equals(gateway)) {
            redisDao.removeStripeCached(requestId, booking.bookId);
        }
    }

    private void sendReportData() {
        try {
            Map<String,Object> mapData;
            if (updateData) {
                UpdateTicket updateTicket = new UpdateTicket(booking, requestId, passenger);
                mapData = updateTicket.updateTicketAfterPayment();
            } else {
                // sync data only - do not update wallet
                //UpdateTicketSync updateTicket = new UpdateTicketSync(booking, requestId, passenger, newBalance);
                mapData = new HashMap<>();
            }

            logger.debug(requestId + " - updateTicketAfterPayment: " + booking.bookId + " - result: " + mapData.get("returnCode").toString());
            if (mapData.get("returnCode").toString().equals("200")) {
                updateTicketSuccess = true;
                ticket = mapData.get("ticket").toString();
                transactionStatus = mapData.get("transactionStatus").toString();
                psgFleetId = mapData.get("psgFleetId").toString();
                completedTime = mapData.get("completedTime").toString();
                profit = Double.parseDouble(mapData.get("profit").toString());
                revenue = Double.parseDouble(mapData.get("revenue").toString());
                fleetProfit = Double.parseDouble(mapData.get("fleetProfit").toString());
                String promoCodeUse = "";
                String promoCodeUseId = "";
                if (mapData.get("promoCodeUse") != null) {
                    promoCodeUse = mapData.get("promoCodeUse").toString();
                    promoCodeUseId = mapData.get("promoCodeUseId").toString();
                }
                String prepaidTransaction = "";
                String prepaidTransactionId = "";
                if (mapData.get("prepaidTransaction") != null) {
                    prepaidTransaction = mapData.get("prepaidTransaction").toString();
                    prepaidTransactionId = mapData.get("prepaidTransactionId").toString();
                }
                String referralEarning = "";
                if (mapData.get("referralEarning") != null) {
                    referralEarning = mapData.get("referralEarning").toString();
                }
                String referralHistories = "";
                if (mapData.get("referralHistories") != null) {
                    referralHistories = mapData.get("referralHistories").toString();
                }
                String jsonReferralHistoryId = "";
                if (mapData.get("jsonReferralHistoryId") != null) {
                    jsonReferralHistoryId = mapData.get("jsonReferralHistoryId").toString();
                }
                String referralHistoriesDriver = "";
                if (mapData.get("referralHistoriesDriver") != null) {
                    referralHistoriesDriver = mapData.get("referralHistoriesDriver").toString();
                }
                String jsonReferralHistoryIdDriver = "";
                if (mapData.get("jsonReferralHistoryIdDriver") != null) {
                    jsonReferralHistoryIdDriver = mapData.get("jsonReferralHistoryIdDriver").toString();
                }
                String referralTransaction = "";
                if (mapData.get("referralTransaction") != null) {
                    referralTransaction = mapData.get("referralTransaction").toString();
                }

                if (ServerConfig.server_report_enable) {
                    JSONArray arrData = new JSONArray();
                    JSONObject objTicket = new JSONObject();
                    objTicket.put("type", "Ticket");
                    objTicket.put("id", booking.bookId);
                    objTicket.put("body", "ticketBody");
                    arrData.add(objTicket);

                    if (!promoCodeUse.equals("")) {
                        JSONObject objPromo = new JSONObject();
                        objPromo.put("type", "PromoCodeUse");
                        objPromo.put("id", promoCodeUseId);
                        objPromo.put("body", "promoCodeUseBody");
                        arrData.add(objPromo);
                    }

                    if (updateData) {
                        if (!prepaidTransaction.equals("")) {
                            JSONObject objPrePaid = new JSONObject();
                            objPrePaid.put("type", "PrepaidTransaction");
                            objPrePaid.put("id", prepaidTransactionId);
                            objPrePaid.put("body", "prepaidTransactionBody");
                            arrData.add(objPrePaid);
                        }

                        if (!referralEarning.isEmpty()) {
                            JSONObject objReferral = new JSONObject();
                            objReferral.put("type", "ReferralEarning");
                            objReferral.put("id", booking.bookId);
                            objReferral.put("body", "referralEarningBody");
                            arrData.add(objReferral);
                        }

                        if (!referralTransaction.isEmpty()) {
                            JSONObject objTransaction = new JSONObject();
                            objTransaction.put("type", "ReferralTransaction");
                            objTransaction.put("id", booking.bookId);
                            objTransaction.put("body", "referralTransactionBody");
                            arrData.add(objTransaction);
                        }

                        if (!referralHistories.isEmpty()) {
                            JSONObject objReferralHistories = new JSONObject();
                            objReferralHistories.put("type", "ReferralHistories");
                            objReferralHistories.put("id", jsonReferralHistoryId);
                            objReferralHistories.put("body", "referralHistoriesBody");
                            arrData.add(objReferralHistories);
                        }

                        if (!referralHistoriesDriver.isEmpty()) {
                            JSONObject objReferralHistories = new JSONObject();
                            objReferralHistories.put("type", "ReferralHistories");
                            objReferralHistories.put("id", jsonReferralHistoryIdDriver);
                            objReferralHistories.put("body", "referralHistoriesDriverBody");
                            arrData.add(objReferralHistories);
                        }
                    }

                    String body = arrData.toJSONString();
                    body = body.replace("\"ticketBody\"", ticket);
                    body = body.replace("\"promoCodeUseBody\"", promoCodeUse);
                    if (updateData) {
                        body = body.replace("\"prepaidTransactionBody\"", prepaidTransaction);
                        body = body.replace("\"referralEarningBody\"", referralEarning);
                        body = body.replace("\"referralTransactionBody\"", referralTransaction);
                        body = body.replace("\"referralHistoriesBody\"", referralHistories);
                        body = body.replace("\"referralHistoriesDriverBody\"", referralHistoriesDriver);
                    }
                    // submit data
                    logger.debug(requestId + " - report body: " + body);

                    // push data directly to Report
                    QUpReportPublisher reportPublisher = QUpReportPublisher.getInstance();
                    reportPublisher.sendListItems(requestId, body);
                }
            }
        } catch(Exception ex) {
            logger.debug(requestId + " - sendReportData exception: "+ CommonUtils.getError(ex));
        }
    }

    private void sendDashboardData() {
        if (!updateTicketSuccess) {
            return;
        }
        boolean success = false;
        BookingStatistic bookingStatistic = new BookingStatistic();
        try {
            bookingStatistic.fleetId = fleetId;
            bookingStatistic.time = completedTime;
            bookingStatistic.currencyISO = booking.currencyISO;
            bookingStatistic.finished = 1;
            bookingStatistic.profit = profit;
            bookingStatistic.revenue = revenue;
            bookingStatistic.fleetProfit = fleetProfit;

            Statistic statistic = new Statistic();
            switch (transactionStatus) {
                case "canceled" :
                    statistic.canceled = 1;
                    break;
                case "noShow" :
                    statistic.noShow = 1;
                    break;
                case "incident" :
                    statistic.incident = 1;
                    break;
                default:
                    statistic.completed = 1;
                    break;
            }

            switch (booking.bookFrom.toLowerCase()) {
                case "cc" :
                    bookingStatistic.cc = statistic;
                    break;
                case "car-hailing" :
                    bookingStatistic.carHailing = statistic;
                    break;
                case "web booking" :
                    bookingStatistic.webBooking = statistic;
                    break;
                case "kiosk" :
                    bookingStatistic.kiosk = statistic;
                    break;
                case "api" :
                    bookingStatistic.API = statistic;
                    break;
                case KeysUtil.dash_board :
                    bookingStatistic.dmc = statistic;
                    break;
                case KeysUtil.corp_board :
                    bookingStatistic.corp = statistic;
                    break;
                case "mdispatcher" :
                    bookingStatistic.mDispatcher = statistic;
                    break;
                default:
                    bookingStatistic.paxApp = statistic;
                    break;
            }

            String body = gson.toJson(bookingStatistic);
            // push data directly to Report
            QUpReportPublisher reportPublisher = QUpReportPublisher.getInstance();
            Date date = KeysUtil.SDF_DMYHMSTZ.parse(bookingStatistic.time);
            String id = fleetId+"_"+bookingStatistic.currencyISO+"_"+date.getTime();
            success = reportPublisher.sendOneItem(requestId, id, "BookingStatistic", body);
        } catch (Exception ex) {
            logger.debug(requestId + " - sendDashboardData exception: " + CommonUtils.getError(ex));
        }

        if (!success) {
            logger.debug(requestId + " - store BookingStatistic to MongoDB: " + gson.toJson(bookingStatistic));
            MongoDao mongoDao = new MongoDaoImpl();
            mongoDao.addBookingStatistic(bookingStatistic);
        }
    }

    private void sendDMCData(String type) {
        if (!updateTicketSuccess) {
            return;
        }

        boolean success = false;

        BookingStatistic bookingStatistic = new BookingStatistic();
        try {
            bookingStatistic.fleetId = psgFleetId;
            bookingStatistic.time = completedTime;
            bookingStatistic.currencyISO = booking.currencyISO;

            Statistic statistic = new Statistic();
            statistic.finished = 1;
            switch (transactionStatus) {
                case "canceled" :
                    statistic.canceled = 1;
                    break;
                case "noShow" :
                    statistic.noShow = 1;
                    break;
                case "incident" :
                    statistic.incident = 1;
                    break;
                default:
                    statistic.completed = 1;
                    break;
            }
            if(type.equals(KeysUtil.dash_board))
                bookingStatistic.dmcBooking = statistic;
            else
                bookingStatistic.corpBooking = statistic;

            String body = gson.toJson(bookingStatistic);
            // push data directly to Report
            QUpReportPublisher reportPublisher = QUpReportPublisher.getInstance();
            Date date = KeysUtil.SDF_DMYHMSTZ.parse(bookingStatistic.time);
            String id = fleetId+"_"+bookingStatistic.currencyISO+"_"+date.getTime();
            success = reportPublisher.sendOneItem(requestId, id, type, body);
        } catch (Exception ex) {
            logger.debug(requestId + " - sendDMCData exception: " + CommonUtils.getError(ex));
        }

        if (!success) {
            logger.debug(requestId + " - store BookingStatistic to MongoDB: " + gson.toJson(bookingStatistic));
            MongoDao mongoDao = new MongoDaoImpl();
            mongoDao.addBookingStatistic(bookingStatistic);
        }
    }
    private void sendHydraReportData() {
        if (!updateTicketSuccess) {
            return;
        }
        try {
            JSONObject objTicket = (JSONObject)new JSONParser().parse(ticket);
            ThreadUtil threadUtil = new ThreadUtil(requestId);

            double grossProfit = 0.0;
            String transactionStatus = objTicket.get("transactionStatus").toString();
            String dmcFleetId = objTicket.get("psgFleetId").toString();
            String supplierFleetId = objTicket.get("fleetId") != null ? objTicket.get("fleetId").toString() : "";
            String supplierFleetName = objTicket.get("fleetName") != null ? objTicket.get("fleetName").toString() : "";
            double penaltyAmount = objTicket.get("penaltyAmount") != null ? Double.parseDouble(objTicket.get("penaltyAmount").toString()) : 0.0;
            double compensationAmount = objTicket.get("compensationAmount") != null ? Double.parseDouble(objTicket.get("compensationAmount").toString()) : 0.0;
            double penaltyPercent = objTicket.get("penaltyPolicy") != null ? Double.parseDouble(objTicket.get("penaltyPolicy").toString()) : 0.0;
            double compensationPercent = objTicket.get("compensationPolicy") != null ? Double.parseDouble(objTicket.get("compensationPolicy").toString()) : 0.0;
            double etaFare = booking.request.estimate.fare != null ? booking.request.estimate.fare.etaFare : 0.0;
            double qupSellPrice = booking.request.estimate.fare != null ? booking.request.estimate.fare.qupSellPrice : 0.0;

            String paidBy = "";
            String penaltyReason = "";
            String penaltyDuration = "";
            String pickUpTime = booking.time.pickUpTime != null ? sdfMongo.format(booking.time.pickUpTime) : "";
            String supplierOperatorName = "";
            String supplierOperatorUsername = "";
            switch (transactionStatus) {
                case "incident" :
                    penaltyReason = "incident";
                break;
                case "noShow" :
                    penaltyReason = "paxNoShow";
                break;
                case "canceled" : {
                    String canceller = objTicket.get("canceller") != null ? objTicket.get("canceller").toString() : "";
                    switch (canceller) {
                        case "timeout":
                            penaltyReason = "canceledByTimeout";
                        break;
                        case "CC": {
                            // get data then remove cache
                            String cancelFleetId = redisDao.getTmp(requestId, booking.bookId+"fleetId");
                            redisDao.removeTmp(requestId, booking.bookId+"fleetId");
                            penaltyReason = cancelFleetId.equals(booking.request.psgFleetId) ? "canceledByHomeFleet" : "canceledBySupplier";
                            if (!pickUpTime.isEmpty()) {
                                penaltyDuration = TimezoneUtil.getPeriodTime(requestId, pickUpTime);
                                logger.debug(requestId + " - penaltyDuration = " + penaltyDuration);
                            }
                            String cancellerId = booking.cancelInfo.cancellerId != null ? booking.cancelInfo.cancellerId : "";
                            if (!cancellerId.isEmpty()) {
                                UsersSystem user = mongoDao.getUsersSystem(cancellerId);
                                if (user != null) {
                                    String firstName = user.firstName != null ? user.firstName : "";
                                    String lastName = user.lastName != null ? user.lastName : "";
                                    supplierOperatorName = CommonUtils.removeSpecialCharacter(firstName + " " + lastName).trim();
                                    supplierOperatorUsername = user.userName != null ? user.userName : "";
                                }
                            }
                        }
                        break;
                        case "passenger": {
                            penaltyReason = "canceledByCustomer";
                            if (!pickUpTime.isEmpty()) {
                                penaltyDuration = TimezoneUtil.getPeriodTime(requestId, pickUpTime);
                                logger.debug(requestId + " - penaltyDuration = " + penaltyDuration);
                            }
                        }
                        break;
                    }
                }
            }

            // remove temp data if exists
            redisDao.removeTmp(requestId, "income-"+booking.bookId);

            HydraIncome hydraIncome = new HydraIncome();
            hydraIncome.bookId = booking.bookId;
            hydraIncome.dmcFleetId = dmcFleetId;
            hydraIncome.dmcFleetName = objTicket.get("psgFleetName") != null ? objTicket.get("psgFleetName").toString() : "";
            hydraIncome.supplierFleetId = supplierFleetId;
            hydraIncome.supplierFleetName = supplierFleetName;
            hydraIncome.amount = objTicket.get("total") != null ? Double.parseDouble(objTicket.get("total").toString()) : 0.0;
            hydraIncome.quoted = etaFare;
            hydraIncome.currencyISO = objTicket.get("currencyISO").toString();
            hydraIncome.currencySymbol = objTicket.get("currencySymbol").toString();
            hydraIncome.driverName = objTicket.get("driverName") != null ? objTicket.get("driverName").toString() : "";
            hydraIncome.driverPhone = objTicket.get("phone") != null ? objTicket.get("phone").toString() : "";
            hydraIncome.plateNumber = objTicket.get("plateNumber") != null ? objTicket.get("plateNumber").toString() : "";
            hydraIncome.passengerName = objTicket.get("customerName") != null ? objTicket.get("customerName").toString() : "";
            hydraIncome.passengerPhone = objTicket.get("customerPhone") != null ? objTicket.get("customerPhone").toString() : "";
            hydraIncome.transactionStatus = transactionStatus;
            hydraIncome.canceller = objTicket.get("canceller") != null ? objTicket.get("canceller").toString() : "";
            hydraIncome.supplierOperatorName = supplierOperatorName;
            hydraIncome.supplierOperatorUsername = supplierOperatorUsername;
            hydraIncome.penaltyAmount = penaltyAmount;
            hydraIncome.compensationAmount = compensationAmount;
            hydraIncome.grossProfit = grossProfit;
            hydraIncome.totalCollect = penaltyAmount;
            hydraIncome.penaltyPercent = penaltyPercent;
            hydraIncome.compensationPercent = compensationPercent;
            hydraIncome.penaltyReason = penaltyReason;
            hydraIncome.estimatedSellPrice = qupSellPrice;
            hydraIncome.estimatedBuyPrice = etaFare;
            hydraIncome.penaltyDuration = penaltyDuration;
            hydraIncome.penaltyNote = "";   //
            hydraIncome.paidBy = paidBy;
            hydraIncome.isFinal = true;
            String itineraryId = "";
            String eventId = "";
            String eventName = "";
            String companyId = "";
            String companyName = "";
            int travelerType = 0;
            if (booking.itinerary != null) {
                itineraryId = booking.itinerary._id.toString();
                if (booking.itinerary.event != null) {
                    eventId = booking.itinerary.event._id.toString();
                    eventName = booking.itinerary.event.name != null ? booking.itinerary.event.name : "";
                    travelerType = booking.itinerary.customerType;
                }
                if (booking.itinerary.organizationInfo != null) {
                    companyId = booking.itinerary.organizationInfo._id.toString();
                    companyName = booking.itinerary.organizationInfo.name != null ? booking.itinerary.organizationInfo.name : "";
                }
            }
            hydraIncome.itineraryId = itineraryId;
            hydraIncome.eventId = eventId;
            hydraIncome.eventName = eventName;
            hydraIncome.travelerType = travelerType;
            hydraIncome.companyId = companyId;
            hydraIncome.companyName = companyName;
            String pickup = "";
            String timeZonePickup = "";
            String destination = "";
            String timeZoneDestination = "";
            if (booking.request.pickup != null) {
                pickup = booking.request.pickup.address != null ? CommonUtils.removeSpecialCharacter(booking.request.pickup.address) : "";
                if (pickup.length() > 255)
                    pickup = pickup.substring(0,255);
                timeZonePickup = booking.request.pickup.timezone != null ? booking.request.pickup.timezone : "";
            }
            if (booking.request.destination != null) {
                destination = booking.request.destination.address != null ? CommonUtils.removeSpecialCharacter(booking.request.destination.address) : "";
                if (destination.length() > 255)
                    destination = destination.substring(0,255);
                timeZoneDestination = booking.request.destination.timezone != null ? booking.request.destination.timezone : "";
            }
            hydraIncome.pickup = pickup;
            hydraIncome.timeZonePickUp = timeZonePickup;
            hydraIncome.destination = destination;
            hydraIncome.timeZoneDestination = timeZoneDestination;
            Timestamp pickupTime = null;
            try {
                if (booking.time != null) {
                    if (booking.time.pickUpTime != null) {
                        Date pickupDate = TimezoneUtil.offsetTimeZone(booking.time.pickUpTime, Calendar.getInstance().getTimeZone().getID(), "GMT");
                        pickupTime = new Timestamp(pickupDate.getTime());
                    }
                }
            } catch(Exception ex) {
                ex.printStackTrace();
            }
            hydraIncome.pickupTime = pickupTime;
            hydraIncome.exchangeRate = Double.parseDouble(objTicket.get("exchangeRate") != null ? objTicket.get("exchangeRate").toString() : "1.0");
            hydraIncome.isFarmOut = Boolean.parseBoolean(objTicket.get("isFarmOut") != null ? objTicket.get("isFarmOut").toString() : "false");

            try {
                SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss.SSS" );
                String time = completedTime.replace("Z", ""); // remove "Z"
                Date date = df.parse(time);
                hydraIncome.completedTime = new Timestamp(date.getTime());
                hydraIncome.happenedTime = new Timestamp(date.getTime());
            } catch (Exception ex) {
                logger.debug(requestId + " - format time exception: " + CommonUtils.getError(ex));
            }
            long addIncome = sqlDao.addHydraIncome(hydraIncome);
            if (addIncome == 0) {
                // something went wrong, try to add again
                try {
                    Thread.sleep(5000);
                } catch(InterruptedException ignore) {}
                addIncome = sqlDao.addHydraIncome(hydraIncome);
            }

            String body = gson.toJson(hydraIncome);
            // change format time
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject)parser.parse(body);
            // remove json format
            jsonObject.remove("completedTime");
            jsonObject.remove("happenedTime");
            jsonObject.remove("pickupTime");
            // add new format
            jsonObject.put("completedTime", completedTime);
            jsonObject.put("happenedTime", completedTime);
            jsonObject.put("pickupTime", KeysUtil.SDF_DMYHMS.format(hydraIncome.pickupTime));
            String reportBody = jsonObject.toJSONString();
            QUpReportPublisher reportPublisher = QUpReportPublisher.getInstance();
            reportPublisher.sendOneItem(requestId, String.valueOf(addIncome), "HydraIncome", reportBody);
        } catch (Exception ex) {
            logger.debug(requestId + " - hydraIncome exception: " + CommonUtils.getError(ex));
        }
    }
}
