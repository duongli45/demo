package com.qupworld.paymentgateway.controllers;

import com.google.gson.Gson;
import com.qupworld.paymentgateway.controllers.GatewayUtil.GatewayUtil;
import com.qupworld.paymentgateway.entities.PaxWalletTransaction;
import com.qupworld.paymentgateway.entities.WalletTransaction;
import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.MongoDaoImpl;
import com.qupworld.paymentgateway.models.SQLDao;
import com.qupworld.paymentgateway.models.SQLDaoImpl;
import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.passengerInfo.PassengerInfo;
import com.pg.util.TimezoneUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by qup on 11/03/2019.
 */
public class PaxWalletUtil {

    Gson gson;
    public SQLDao sqlDao;
    public MongoDao mongoDao;
    String requestId;
    final Logger logger = LogManager.getLogger(PaxWalletUtil.class);

    public PaxWalletUtil(String requestId){
        gson = new Gson();
        sqlDao = new SQLDaoImpl();
        mongoDao = new MongoDaoImpl();
        this.requestId = requestId;
    }

    public double getCurrentBalance(String fleetId, String userId, String type, String currencyISO){
        // check new wallet
        PassengerInfo passengerInfo = mongoDao.getPassengerInfo(fleetId, userId);
        if (passengerInfo == null) {
            // create new object
            passengerInfo = new PassengerInfo();
            passengerInfo.userId = userId;
            passengerInfo.fleetId = fleetId;

            List<AmountByCurrency> listWallet = new ArrayList<>();
            AmountByCurrency wallet = new AmountByCurrency();
            wallet.value = 0.0;
            wallet.currencyISO = currencyISO;
            listWallet.add(wallet);
            passengerInfo.paxWallet = listWallet;

            passengerInfo.outStanding = new ArrayList<>();

            mongoDao.addPassengerInfo(passengerInfo);
        } else {
            // insert wallet for currency if there is no wallet
            if (passengerInfo.paxWallet == null || passengerInfo.paxWallet.isEmpty()) {
                List<AmountByCurrency> listWallet = new ArrayList<>();
                AmountByCurrency wallet = new AmountByCurrency();
                wallet.value = 0.0;
                wallet.currencyISO = currencyISO;
                listWallet.add(wallet);
                passengerInfo.paxWallet = listWallet;

                mongoDao.updatePaxWallet(fleetId, userId, listWallet);
            } else {
                // insert wallet for currency if not exist
                List<AmountByCurrency> listWallet = passengerInfo.paxWallet;
                boolean isExist = false;
                for (AmountByCurrency wallet : listWallet) {
                    if (wallet.currencyISO != null && wallet.currencyISO.equals(currencyISO)) {
                        isExist = true;
                    }
                }
                if (!isExist) {
                    AmountByCurrency wallet = new AmountByCurrency();
                    wallet.value = 0.0;
                    wallet.currencyISO = currencyISO;
                    listWallet.add(wallet);
                    mongoDao.updatePaxWallet(fleetId, userId, listWallet);
                }
            }
        }
        BigDecimal paxBalance = sqlDao.getWalletBalance(requestId, userId, type, currencyISO);
        logger.debug(requestId + " - paxBalance from DB: " + paxBalance.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
        return paxBalance.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public void depositPaxWallet(PaxWalletTransaction paxWalletTransaction) {

        WalletTransaction walletTransaction = new WalletTransaction();
        walletTransaction.fleetId = paxWalletTransaction.fleetId;
        walletTransaction.userId = paxWalletTransaction.userId;
        walletTransaction.type = "paxWallet";
        walletTransaction.transactionId = paxWalletTransaction.transactionId;
        walletTransaction.description = paxWalletTransaction.transactionType;
        walletTransaction.cashIn = paxWalletTransaction.amount;
        walletTransaction.cashOut = 0.0;
        walletTransaction.currencyISO = paxWalletTransaction.currencyISO;
        walletTransaction.createdDate = paxWalletTransaction.createdDate;

        doAddDB(walletTransaction, paxWalletTransaction);
    }

    public void deductPaxWallet(PaxWalletTransaction paxWalletTransaction) {
        WalletTransaction walletTransaction = new WalletTransaction();
        walletTransaction.fleetId = paxWalletTransaction.fleetId;
        walletTransaction.userId = paxWalletTransaction.userId;
        walletTransaction.type = "paxWallet";
        walletTransaction.transactionId = paxWalletTransaction.transactionId;
        walletTransaction.description = paxWalletTransaction.transactionType;
        walletTransaction.cashIn = 0.0;
        walletTransaction.cashOut = paxWalletTransaction.amount;
        walletTransaction.currencyISO = paxWalletTransaction.currencyISO;
        walletTransaction.createdDate = TimezoneUtil.getGMTTimestamp();

        doAddDB(walletTransaction, paxWalletTransaction);
    }

    public void updatePaxWallet(PaxWalletTransaction paxWalletTransaction, boolean isDeposit) {
        logger.debug(requestId + " - updatePaxWallet - deposit: " + isDeposit + " - " + gson.toJson(paxWalletTransaction));
        WalletTransaction walletTransaction = new WalletTransaction();
        walletTransaction.fleetId = paxWalletTransaction.fleetId;
        walletTransaction.userId = paxWalletTransaction.userId;
        walletTransaction.type = "paxWallet";
        walletTransaction.transactionId = paxWalletTransaction.transactionId;
        walletTransaction.description = paxWalletTransaction.transactionType;
        if (isDeposit) {
            walletTransaction.cashIn = paxWalletTransaction.amount;
            walletTransaction.cashOut = 0.0;
        } else {
            walletTransaction.cashIn = 0.0;
            walletTransaction.cashOut = paxWalletTransaction.amount;
        }
        walletTransaction.currencyISO = paxWalletTransaction.currencyISO;
        walletTransaction.createdDate = TimezoneUtil.getGMTTimestamp();

        doAddDB(walletTransaction, paxWalletTransaction);
    }


    private void doAddDB(WalletTransaction walletTransaction, PaxWalletTransaction paxWalletTransaction) {
        if (walletTransaction.cashIn > 0 || walletTransaction.cashOut > 0) {
            logger.debug(requestId + " - doAddDB: " + gson.toJson(walletTransaction));
            long addIncome = sqlDao.addWalletTransaction(walletTransaction);
            if (addIncome == 0) {
                // something went wrong, try to add again
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException ignore) {
                }
                addIncome = sqlDao.addWalletTransaction(walletTransaction);
                if (addIncome == 0) {
                    logger.debug(requestId + " - add paxWalletTransaction error!!!");
                    // send log to email for checking later
                    GatewayUtil.sendMailWalletTransaction(requestId, gson.toJson(paxWalletTransaction));
                }
            }
        }
    }

}
