package com.qupworld.paymentgateway.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.pg.util.CommonArrayUtils;
import com.pg.util.CommonUtils;
import com.pg.util.EstimateUtil;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.TimezoneUtil;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.controllers.GatewayUtil.StripeUtil;
import com.qupworld.paymentgateway.controllers.threading.DriverWalletTransactionThread;
import com.qupworld.paymentgateway.controllers.threading.UpdatePaxWalletThread;
import com.qupworld.paymentgateway.entities.*;
import com.qupworld.paymentgateway.models.*;
import com.qupworld.paymentgateway.models.mongo.collections.AffiliateSetting.AffiliateSetting;
import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.FleetService;
import com.qupworld.paymentgateway.models.mongo.collections.account.Account;
import com.qupworld.paymentgateway.models.mongo.collections.booking.Booking;
import com.qupworld.paymentgateway.models.mongo.collections.booking.DeliveryInfo;
import com.qupworld.paymentgateway.models.mongo.collections.booking.SOS;
import com.qupworld.paymentgateway.models.mongo.collections.company.Company;
import com.qupworld.paymentgateway.models.mongo.collections.corpItinerary.CorpItinerary;
import com.qupworld.paymentgateway.models.mongo.collections.corporate.Corporate;
import com.qupworld.paymentgateway.models.mongo.collections.dynamicFare.DynamicFare;
import com.qupworld.paymentgateway.models.mongo.collections.fleet.ConfigGateway;
import com.qupworld.paymentgateway.models.mongo.collections.fleet.FeeByCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.fleet.Fleet;
import com.qupworld.paymentgateway.models.mongo.collections.fleetfare.FleetFare;
import com.qupworld.paymentgateway.models.mongo.collections.gateway.GatewayStripe;
import com.qupworld.paymentgateway.models.mongo.collections.menuMerchant.MenuMerchant;
import com.qupworld.paymentgateway.models.mongo.collections.passengerInfo.OutStanding;
import com.qupworld.paymentgateway.models.mongo.collections.passengerInfo.PassengerInfo;
import com.qupworld.paymentgateway.models.mongo.collections.paxReferral.PaxReferral;
import com.qupworld.paymentgateway.models.mongo.collections.paxReferral.RefereePromo;
import com.qupworld.paymentgateway.models.mongo.collections.pricingplan.PricingPlan;
import com.qupworld.paymentgateway.models.mongo.collections.promotionCode.PromotionCode;
import com.qupworld.paymentgateway.models.mongo.collections.referral.Referral;
import com.qupworld.paymentgateway.models.mongo.collections.referral.ReferralHistories;
import com.qupworld.paymentgateway.models.mongo.collections.referral.ReferralInfo;
import com.qupworld.paymentgateway.models.mongo.collections.referral.ReferralPackage;
import com.qupworld.paymentgateway.models.mongo.collections.serverInformation.*;
import com.qupworld.paymentgateway.models.mongo.collections.serviceFee.ServiceFee;
import com.qupworld.paymentgateway.models.mongo.collections.tripNotification.TripNotification;
import com.qupworld.paymentgateway.models.mongo.collections.wallet.DriverWallet;
import com.qupworld.paymentgateway.models.mongo.collections.zone.Zone;
import com.qupworld.service.MailService;
import com.qupworld.service.QUpSearchPublisher;
import com.qupworld.util.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class UpdateTicket {

    private static final Logger logger = LogManager.getLogger(UpdateTicket.class);
    private MongoDao mongoDao;
    private SQLDao sqlDao;
    private RedisDao redisDao;
    private Booking booking;
    private PaymentUtil paymentUtil;
    private EstimateUtil estimateUtil;
    private String requestId;
    /*private String fromDate;*/
    private Account passenger;
    private Gson gson = new Gson();
    private DecimalFormat DF = new DecimalFormat("0.00");
    public UpdateTicket(Booking booking, String requestId, Account passenger) {
        mongoDao = new MongoDaoImpl();
        sqlDao = new SQLDaoImpl();
        redisDao = new RedisImpl();
        estimateUtil = new EstimateUtil();
        this.booking = booking;
        this.requestId = requestId;
        this.passenger = passenger;
        paymentUtil = new PaymentUtil();
    }

    /*public UpdateTicket(Booking booking, String requestId, Account passenger, String fromDate) {
        mongoDao = new MongoDaoImpl();
        sqlDao = new SQLDaoImpl();
        this.booking = booking;
        this.requestId = requestId;
        this.fromDate = fromDate;
        this.passenger = passenger;
        paymentUtil = new PaymentUtil();
    }*/

    public Map<String,Object> updateTicketAfterPayment() {
        logger.debug(requestId + " - start updateTicketAfterPayment for bookId " + booking.bookId + "... ");
        Map<String,Object> map = new HashMap<>();
        try {
            TicketEnt ticketEnt = new TicketEnt();
            ticketEnt.bookId = booking.bookId;
            TicketReturn ticketReturn = sqlDao.getTicket(booking.bookId);
            ticketEnt.total = ticketReturn.total;
            if (ticketReturn == null) {
                logger.debug(requestId + " - SQL connection error --- retry query ticket from mysql");
                Thread.sleep(3000);
                ticketReturn = sqlDao.getTicket(booking.bookId);
                if (ticketReturn == null) {
                    logger.debug(requestId + " - SQL connection error --- retry query ticket from mysql again");
                    Thread.sleep(3000);
                    ticketReturn = sqlDao.getTicket(booking.bookId);
                }
            }
            logger.debug(requestId + " - ticketReturn: " + ticketReturn != null ? gson.toJson(ticketReturn) : "Ticket is null");
            ticketEnt.pricingType = ticketReturn.pricingType;
            ticketEnt.fleetId = ticketReturn.fleetId;
            ticketEnt.psgFleetId = ticketReturn.psgFleetId;
            ticketEnt.customerId = ticketReturn.customerId;
            ticketEnt.completedTime = ticketReturn.completedTime;
            Fleet fleet = mongoDao.getFleetInfor(booking.fleetId);
            String reason = "";
            String promoCode = "";
            if(booking.cancelInfo != null){
                reason = booking.cancelInfo.reason != null ? booking.cancelInfo.reason : "";
                ticketEnt.reasonCode = booking.cancelInfo.reasonCode != null ? booking.cancelInfo.reasonCode : 5;
            }
            if(!reason.equals("")) {
                ticketEnt.reason = reason;
                if (ticketEnt.reason.length() > 5000)
                    ticketEnt.reason = ticketEnt.reason.substring(0,5000);
            }
            if (booking.request!= null && booking.request.promo != null)
                promoCode = booking.request.promo;

            boolean serviceActive = false;
            if(booking.request!= null) {
                serviceActive = booking.request.serviceActive;
                ticketEnt.walletName = booking.request.gateway != null ? booking.request.gateway.trim() : "";
            }

            //get pickup zone
            String zoneId = "";
            String zoneName = "";
            Zone zone = mongoDao.findByFleetIdAndGeo(fleet.fleetId, booking.request.pickup.geo);
            if(zone!= null){
                zoneId = zone._id.toString();
                zoneName = zone.zoneName;
            }
            //get data setting & save to mysql
            Map<String, Object> mapSettings = paymentUtil.getSettings(booking.fleetId, booking, promoCode, requestId, ticketReturn.payFromCC);
            JSONObject jsonSetting = new JSONObject();
            jsonSetting.put("techFeeActive",Boolean.parseBoolean(mapSettings.get("techFeeActive").toString()));
            jsonSetting.put("tipActive",Boolean.parseBoolean(mapSettings.get("tipActive").toString()));
            jsonSetting.put("taxActive",Boolean.parseBoolean(mapSettings.get("taxActive").toString()));
            jsonSetting.put("meetDriverActive",Boolean.parseBoolean(mapSettings.get("meetDriverActive").toString()));
            jsonSetting.put("heavyTrafficActive",Boolean.parseBoolean(mapSettings.get("heavyTrafficActive").toString()));
            jsonSetting.put("airportActive",Boolean.parseBoolean(mapSettings.get("airportActive").toString()));
            jsonSetting.put("otherFeeActive",Boolean.parseBoolean(mapSettings.get("otherFeeActive").toString()));
            jsonSetting.put("rushHourActive",Boolean.parseBoolean(mapSettings.get("rushHourActive").toString()));
            jsonSetting.put("tollFeeActive",Boolean.parseBoolean(mapSettings.get("tollFeeActive").toString()));
            jsonSetting.put("parkingFeeActive",Boolean.parseBoolean(mapSettings.get("parkingFeeActive").toString()));
            jsonSetting.put("gasFeeActive",Boolean.parseBoolean(mapSettings.get("gasFeeActive").toString()));
            jsonSetting.put("serviceActive", serviceActive);
            jsonSetting.put("tollFeePayTo", Integer.parseInt(mapSettings.get("tollFeePayTo").toString()));
            jsonSetting.put("parkingFeePayTo", Integer.parseInt(mapSettings.get("parkingFeePayTo").toString()));
            jsonSetting.put("gasFeePayTo", Integer.parseInt(mapSettings.get("gasFeePayTo").toString()));
            jsonSetting.put("meetDriverPayTo", Integer.parseInt(mapSettings.get("meetDriverPayTo").toString()));
            jsonSetting.put("airportPayTo", Integer.parseInt(mapSettings.get("airportPayTo").toString()));
            ticketReturn.minimum = Double.parseDouble(mapSettings.get("minimum").toString());

            if(booking.sos!= null){
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdfMongo = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
                for (SOS sos: booking.sos){
                    if (!sos.time.isEmpty()) {
                        sos.time = TimezoneUtil.formatISODate(
                                TimezoneUtil.offsetTimeZone(sdfMongo.parse(sos.time), cal.getTimeZone().getID(), "GMT"), "GMT");
                    }
                }
            }
            jsonSetting.put("sos", booking.sos != null ? gson.fromJson(gson.toJson(booking.sos), JSONArray.class) : new ArrayList<>());
            jsonSetting.put("firstRqPaymentType", (booking.request.origin != null && booking.request.origin.paymentType != null) ?
                    booking.request.origin.paymentType : booking.request.paymentType);
            jsonSetting.put("typeRate", booking.request != null ? booking.request.typeRate : 0);
            jsonSetting.put("hideFare", booking.request.hideFare);
            logger.debug(requestId + " - setting data: " + jsonSetting.toJSONString());

            //get data itinerary & save to mysql
            double totalETA= 0;
            double markupPrice = 0.0;
            double markupDifference = 0.0;
            String reasonMarkup = "";
            if(booking.request!= null && booking.request.estimate != null && booking.request.estimate.fare != null) {
                totalETA = booking.request.estimate.fare.etaFare;
                markupPrice = booking.request.estimate.fare.markupPrice;
                markupDifference = booking.request.estimate.fare.markupDifference;
                reasonMarkup = booking.request.estimate.reasonMarkup != null ? booking.request.estimate.reasonMarkup : "";
            }
            JSONObject itinerary = paymentUtil.getDataItinerary(booking.itinerary, totalETA);
            logger.debug(requestId + " - itinerary: " + itinerary.toJSONString());

            //get data sharing & save to mysql
            JSONObject sharingObject = paymentUtil.getDataSharing(booking);
            logger.debug(requestId + " - sharing data: " + sharingObject.toJSONString());

            //get data additional service & save to mysql
            ticketEnt.services = paymentUtil.getDataService(booking);
            logger.debug(requestId + " - additional service data: " + ticketEnt.services);

            //get extra destination & save to mysql
            ticketEnt.extraDestination = CommonUtils.removeSpecialCharacter(paymentUtil.getExtraDestination(booking));
            logger.debug(requestId + " - extra destination data: " + ticketEnt.extraDestination);

            JSONObject OTWDistance = new JSONObject();
            if(booking.distance != null){
                OTWDistance.put("onTheWayDistance", booking.distance.onTheWay);
                OTWDistance.put("pobDistance", booking.distance.passOnBoard);
            }
            if(booking.request!= null && booking.request.origin != null){
                OTWDistance.put("originEstDistance",booking.request.origin.etaDistance);
                if(booking.request.origin.destination != null)
                    OTWDistance.put("originDestination", booking.request.origin.destination.address != null ?
                            booking.request.origin.destination.address : "");
            }



            // check first booking of new customer
            int totalCompletedBook = 0;
            if (passenger != null && passenger.booking != null) {
                totalCompletedBook = passenger.booking.completed != null ? passenger.booking.completed : 0;
            }

            // get referralHistory to check referral program
            ReferralHistories referralHistories = mongoDao.getLinkReferralByRefereeId(ticketReturn.customerId);
            boolean isNewIncentiveReferral = false;
            boolean isNewIncentiveReferralWithPromo = false;
            boolean paidByDriver = false;
            String referrerType = (referralHistories != null && referralHistories.referralType != null) ? referralHistories.referralType : KeysUtil.APPTYPE_DRIVER; // default = driver for old ReferralHistories
            if (referrerType.equals(KeysUtil.APPTYPE_DRIVER)) {
                isNewIncentiveReferralWithPromo = paymentUtil.checkIncentiveReferralWithPromo(ticketReturn.fleetId, ticketReturn.promoCode);
                isNewIncentiveReferral = true;
            } else if (referrerType.equals(KeysUtil.APPTYPE_PASSENGER)) {
                paidByDriver = paymentUtil.paidByDriver(ticketReturn.fleetId, ticketReturn.promoCode);
            }

            String driverId = "";
            String driverName = "";
            String driverUserName = "";
            String driverPhone = "";
            String companyId = "";
            String companyName = "";
            String driverFleetId = "";
            String driverNumber = "";
            String driverType = "";
            JSONObject jsonDriver = new JSONObject();
            // get companyId of driver
            if(booking != null && booking.drvInfo != null && booking.drvInfo.userId != null && !booking.drvInfo.userId.isEmpty())
                driverId = booking.drvInfo.userId;
            Account driver = mongoDao.getAccount(driverId);
            if (driver != null && driver.driverInfo != null) {
                driverType = driver.driverInfo.driverType != null ? driver.driverInfo.driverType : "";
                String idType = driver.driverInfo.idType != null ? driver.driverInfo.idType : "";
                jsonDriver.put("idType", idType);
                companyId = driver.driverInfo.company != null && driver.driverInfo.company.companyId != null ? driver.driverInfo.company.companyId : "";
                companyName = driver.driverInfo.company != null && driver.driverInfo.company.name != null ?  driver.driverInfo.company.name : "";
            }
            double merchantCommission = 0.0;
            //get merchant commission
            if (booking.jobType != null && (booking.jobType.equals("food") || booking.jobType.equals("mart"))) {
                String merchantId = "";
                if (booking.deliveryInfo != null && booking.deliveryInfo.merchants != null && !booking.deliveryInfo.merchants.isEmpty())
                    merchantId = booking.deliveryInfo.merchants.get(0).merchantId;
                MenuMerchant merchant = mongoDao.getMerchant(merchantId);
                if (merchant != null) {
                    if (merchant.commissionType != null && merchant.commissionType.equals("percentage"))
                        merchantCommission = merchant.commission * 0.01 * ticketReturn.itemValue;
                    else if (merchant.commissions != null && !merchant.commissions.isEmpty()) {
                        for (com.qupworld.paymentgateway.models.mongo.collections.menuMerchant.Commission mc : merchant.commissions) {
                            if (booking.currencyISO.equals(mc.iso)) {
                                merchantCommission = mc.value;
                            }
                        }
                    }
                }
                logger.debug(requestId + " - merchant commission: " + merchantCommission);
            }
            String jobType = booking.jobType != null ? booking.jobType : "";
            if ((jobType.equals("food") || jobType.equals("mart")) && !KeysUtil.INCOMPLETE_PAYMENT.contains(ticketReturn.transactionStatus)) {
                logger.debug(requestId + " - jobType: " + jobType);
                logger.debug(requestId + " - transactionStatus: " + ticketReturn.transactionStatus);
                try {
                    boolean merchantApp = false;
                    String merchantId = "";
                    if(booking.deliveryInfo != null && booking.deliveryInfo.merchants != null && !booking.deliveryInfo.merchants.isEmpty()) {
                        merchantId = booking.deliveryInfo.merchants.get(0).merchantId;
                        MenuMerchant merchant = mongoDao.getMerchant(merchantId);
                        merchantApp = merchant != null && merchant.isPreferred;
                    }

                    PricingPlan pricingPlan = mongoDao.getPricingPlan(booking.fleetId);
                    if (merchantApp) {
                        if (KeysUtil.CASH_PAYMENT.contains(ticketReturn.transactionStatus)) {
                            logger.debug(requestId + " - cash payment and using merchant app");
                            // collect commission from driver's Credit-wallet
                            // already on updateDriverDeduction
                            // updateBalance.type = CommonArrayUtils.DRIVER_WALLET_TYPE[1];

                            // check if driver enable credit wallet then deduct the commission
                            if (pricingPlan.driverDeposit != null && pricingPlan.driverDeposit.enable) {
                                UpdateDriverCreditBalance updateBalance = new UpdateDriverCreditBalance();
                                updateBalance.type = CommonArrayUtils.DRIVER_WALLET_TYPE[20];
                                updateBalance.fleetId = booking.fleetId;
                                updateBalance.driverId = driverId;
                                updateBalance.amount = merchantCommission*(-1);
                                updateBalance.currencyISO = booking.currencyISO;
                                updateBalance.mongoDao = mongoDao;
                                updateBalance.sqlDao = sqlDao;
                                updateBalance.reason = "";
                                updateBalance.requestId = requestId;
                                updateBalance.additionalData.put("bookId", booking.bookId);
                                String updateResult = updateBalance.doUpdate();
                                logger.debug(requestId + " - updateResult: " + updateResult);
                            }
                        } else {
                            logger.debug(requestId + " - online payment and using merchant app");
                            // add (applicable fee - commission = netEarning) to driver cash wallet
                            // already on updateNetEarning
                            // walletTransfer.transferType = CommonArrayUtils.DRIVER_CASH_WALLET_TYPE[2];

                            // add pending payment for merchant
                            if (ticketReturn.itemValue > 0) {
                                double pendingAmount = DF.parse(KeysUtil.DECIMAL_FORMAT.format(ticketReturn.itemValue - merchantCommission)).doubleValue();
                                MerchantTransactionEnt merchantTransactionEnt = new MerchantTransactionEnt();
                                merchantTransactionEnt.fleetId = booking.fleetId;
                                merchantTransactionEnt.merchantId = merchantId;
                                merchantTransactionEnt.type = "itemValue";
                                merchantTransactionEnt.transactionId = booking.bookId;
                                merchantTransactionEnt.pendingAmount = pendingAmount;
                                merchantTransactionEnt.currencyISO = booking.currencyISO;
                                merchantTransactionEnt.createdDate = ticketReturn.completedTime;
                                long id = sqlDao.addMerchantTransaction(merchantTransactionEnt);
                                logger.debug(requestId + " - merchantTransactionId = " + id);

                                // update to cash wallet
                                boolean enableCash = pricingPlan.merchantCashWallet != null && pricingPlan.merchantCashWallet.enable;
                                if (enableCash) {
                                    MerchantWalletUtil merchantWalletUtil = new MerchantWalletUtil(requestId);
                                    double currentBalance = merchantWalletUtil.getCurrentBalance(booking.fleetId, merchantId,"cash", booking.currencyISO);
                                    logger.debug(requestId + " - currentBalance: " + currentBalance);
                                    double newBalance = Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(currentBalance + pendingAmount));
                                    logger.debug(requestId + " - newBalance: " + newBalance);
                                    UpdateMerchantWalletBalance updateMerchantWalletBalance = new UpdateMerchantWalletBalance();
                                    updateMerchantWalletBalance.walletType = "cash";
                                    updateMerchantWalletBalance.fleetId = booking.fleetId;
                                    updateMerchantWalletBalance.merchantId = merchantId;
                                    updateMerchantWalletBalance.currentBalance = currentBalance;
                                    updateMerchantWalletBalance.amount = pendingAmount;
                                    updateMerchantWalletBalance.newBalance = newBalance;
                                    updateMerchantWalletBalance.currencyISO = booking.currencyISO;
                                    updateMerchantWalletBalance.mongoDao = mongoDao;
                                    updateMerchantWalletBalance.sqlDao = sqlDao;
                                    updateMerchantWalletBalance.reason = "";
                                    updateMerchantWalletBalance.transactionType = CommonArrayUtils.MERCHANT_CASH_WALLET_TYPE[2];
                                    updateMerchantWalletBalance.transactionId = CommonUtils.getOrderId();
                                    updateMerchantWalletBalance.bookId = booking.bookId;
                                    updateMerchantWalletBalance.operatorId = "";
                                    updateMerchantWalletBalance.requestId = requestId;
                                    String updateResult = updateMerchantWalletBalance.doUpdate();
                                    logger.debug(requestId + " - updateResult: " + updateResult);
                                }
                            }
                        }
                    } else {
                        if (KeysUtil.CASH_PAYMENT.contains(ticketReturn.transactionStatus)) {
                            logger.debug(requestId + " - cash payment and no merchant app");
                            // collect commission from driver's Credit-wallet
                            // already on updateDriverDeduction
                            // updateBalance.type = CommonArrayUtils.DRIVER_WALLET_TYPE[1];

                            // check if driver enable credit wallet then deduct the commission
                            if (pricingPlan.driverDeposit != null && pricingPlan.driverDeposit.enable) {
                                UpdateDriverCreditBalance updateBalance = new UpdateDriverCreditBalance();
                                updateBalance.type = CommonArrayUtils.DRIVER_WALLET_TYPE[20];
                                updateBalance.fleetId = booking.fleetId;
                                updateBalance.driverId = driverId;
                                updateBalance.amount = merchantCommission*(-1);
                                updateBalance.currencyISO = booking.currencyISO;
                                updateBalance.mongoDao = mongoDao;
                                updateBalance.sqlDao = sqlDao;
                                updateBalance.reason = "";
                                updateBalance.requestId = requestId;
                                updateBalance.additionalData.put("bookId", booking.bookId);
                                String updateResult = updateBalance.doUpdate();
                                logger.debug(requestId + " - updateResult: " + updateResult);
                            }
                        } else {
                            logger.debug(requestId + " - online payment and no merchant app");
                            // payout driver: item value + (applicable fee - commission) to driver's Cash-wallet
                            // add (applicable fee - commission = netEarning) to driver cash wallet
                            // already on updateNetEarning
                            // walletTransfer.transferType = CommonArrayUtils.DRIVER_CASH_WALLET_TYPE[2];

                            // add itemValue to driver cash wallet
                            if (pricingPlan.driverCashWallet != null && pricingPlan.driverCashWallet.enable && ticketReturn.itemValue > 0) {
                                WalletTransfer walletTransfer = new WalletTransfer();
                                walletTransfer.fleetId = booking.fleetId;
                                walletTransfer.driverId = booking.drvInfo.userId;
                                walletTransfer.amount = ticketReturn.itemValue;
                                walletTransfer.transferType = CommonArrayUtils.DRIVER_CASH_WALLET_TYPE[4];
                                walletTransfer.transactionId = booking.bookId;
                                walletTransfer.currencyISO = booking.currencyISO;
                                UpdateDriverCashBalance updateCash = new UpdateDriverCashBalance();
                                updateCash.requestId = requestId;
                                updateCash.walletTransfer = walletTransfer;
                                updateCash.mongoDao = mongoDao;
                                updateCash.redisDao = redisDao;
                                updateCash.isDeposit = true;
                                updateCash.pendingBalance = -1;
                                double newBalance = updateCash.doUpdate();

                                JSONObject objTrans = new JSONObject();
                                objTrans.put("fleetId", booking.fleetId);
                                objTrans.put("userId", booking.drvInfo.userId);
                                objTrans.put("transactionId", "");
                                objTrans.put("bookId", booking.bookId);
                                objTrans.put("driverName", booking.drvInfo.fullName != null ? booking.drvInfo.fullName.trim() : "");
                                objTrans.put("phoneNumber", booking.drvInfo.phone);
                                objTrans.put("transactionType", "itemValue");
                                objTrans.put("description", "itemValue-" + booking.bookId);
                                objTrans.put("cardNumber", "");
                                objTrans.put("amount", ticketReturn.itemValue);
                                objTrans.put("newBalance", DF.parse(DF.format(newBalance)).doubleValue());
                                objTrans.put("currencyISO", booking.currencyISO);
                                objTrans.put("currencySymbol", booking.currencySymbol);
                                objTrans.put("companyId", companyId);
                                objTrans.put("companyName", companyName);
                                objTrans.put("createdDate", KeysUtil.SDF_DMYHMSTZ.format(TimezoneUtil.getGMTTimestamp()));
                                DriverWalletTransactionThread driverWalletTransactionThread = new DriverWalletTransactionThread();
                                driverWalletTransactionThread.id = String.valueOf(sqlDao.getIdWalletTransaction(booking.drvInfo.userId, "cash", booking.currencyISO,  CommonArrayUtils.DRIVER_CASH_WALLET_TYPE[4], booking.bookId));
                                driverWalletTransactionThread.requestId = requestId;
                                driverWalletTransactionThread.reportBody = objTrans.toJSONString();
                                driverWalletTransactionThread.start();
                            }
                        }
                    }

                    // booking request by paxWallet - pax have to pay the itemValue amount
                    if (ticketReturn.transactionStatus.equals(KeysUtil.PAYMENT_PAXWALLET)) {
                        logger.debug(requestId + " - collect itemValue amount from paxWallet");
                        collectItemValueFromPaxWallet(ticketReturn);
                    }
                } catch (Exception ex) {
                    logger.debug(requestId + " - update itemValue for food/mart exception: " + CommonUtils.getError(ex));
                }
            }
            // calculate data for reporting
            Map<String,String> mapData = updateNetEarning(ticketReturn, ticketEnt, booking, ticketEnt.qupCommission, zoneId,
                    isNewIncentiveReferralWithPromo, paidByDriver, merchantCommission);
            int booType = booking.request.type != null ? booking.request.type : 0;
            mapData.put("bookType", String.valueOf(booType)); // save type booking
            ticketReturn.totalFare = mapData.get("totalFare") != null ? Double.parseDouble(mapData.get("totalFare")):0;
            String promoCustomerType = "";
            String promoStatus = "";
            PromoCodeUse promoCodeUse = null;
            if (!KeysUtil.INCOMPLETE_PAYMENT.contains(ticketReturn.transactionStatus)
                    || (booking.intercity && ticketReturn.totalCharged > 0)){
                if (ticketReturn.promoCode != null && !ticketReturn.promoCode.isEmpty() && !promoCode.equals(ticketReturn.promoCode)) {
                    promoCode = ticketReturn.promoCode;
                    logger.debug(requestId + " - coupon is not null, set coupon code - "+ promoCode +" - has been used!");
                    // override promo code on the booking with the code which is stored on the ticket
                    mongoDao.updatePromoCode(booking.bookId, promoCode);
                }
                String promoFleet = booking.isFarmOut ? booking.request.psgFleetId : booking.fleetId;
                Fleet fleetHasPromo = mongoDao.getFleetInfor(promoFleet);
                if (!promoCode.isEmpty()) {
                    PromotionCode promotionCode = mongoDao.findByFleetIdAndPromoCode(promoFleet, ticketReturn.promoCode);
                    logger.debug(requestId + " - coupon code applied, set coupon code - " + promoCode + " - has been used!");
                    //promoCodeUse = sqlDao.findByCustomerAndAvailable(ticketReturn.promoCode, ticketReturn.fleetId, ticketReturn.customerId, 0);
                    promoCodeUse = sqlDao.getPromoCodeUseByBookId(booking.bookId);
                    if(promotionCode != null){
                        promoCustomerType = "common";
                        if(promotionCode.newCustomer)
                            promoCustomerType = "new";
                        if (promotionCode.referredCustomers)
                            promoCustomerType = "referral";
                        promoStatus = promotionCode.promoStatus;
                    }
                    double promoAmount = mapData.get("promoAmount") != null ? Double.parseDouble(mapData.get("promoAmount")):0;
                    logger.debug(requestId + " - promoAmount: "+ promoAmount);
                    if (promoCodeUse != null) {
                        promoCodeUse.customerName = ticketReturn.customerName;
                        promoCodeUse.available = 1;
                        promoCodeUse.usedValue = booking.isFarmOut ? booking.request.estimate.fare.originPromoAmount : promoAmount;
                        promoCodeUse.totalReceipt = booking.isFarmOut ? ticketReturn.sellPriceMarkup : ticketReturn.totalFare;
                        promoCodeUse.usedDate = ticketReturn.completedTime;
                        promoCodeUse.ticketId = ticketReturn.id;
                        if(promotionCode != null){
                            promoCodeUse.promoCodeId = promotionCode._id.toString();
                            promoCodeUse.campaignId = promotionCode.campaign._id.toString();
                            promoCodeUse.campaignName = promotionCode.campaign.name;
                        }
                        logger.debug(requestId + " - updatePromoCodeUse: "+ gson.toJson(promoCodeUse));
                        sqlDao.updatePromoCodeUse(promoCodeUse);

                        deletePaxPromoList(promotionCode, ticketReturn.customerId, fleetHasPromo, ticketReturn.currencyISO);
                    } else {
                        logger.debug(requestId + " - promo use is null, set promo use to database - "+ promoCode);
                        promoCodeUse = new PromoCodeUse();
                        promoCodeUse.fleetId = promoFleet;
                        promoCodeUse.customerId = ticketReturn.customerId;
                        promoCodeUse.promoCode = ticketReturn.promoCode;
                        promoCodeUse.timesUsed = 1;
                        promoCodeUse.available = 1;
                        promoCodeUse.currencyISO = ticketReturn.currencyISO;
                        promoCodeUse.customerName = ticketReturn.customerName;
                        promoCodeUse.usedValue = booking.isFarmOut ? booking.request.estimate.fare.originPromoAmount : promoAmount;
                        promoCodeUse.totalReceipt = booking.isFarmOut ? ticketReturn.sellPriceMarkup : ticketReturn.totalFare;
                        promoCodeUse.usedDate = ticketReturn.completedTime;
                        promoCodeUse.ticketId = ticketReturn.id;
                        promoCodeUse.createdDate = new Timestamp(TimezoneUtil.convertServerToGMT(new Date()).getTime());
                        if(promotionCode != null){
                            promoCodeUse.promoCodeId = promotionCode._id.toString();
                            promoCodeUse.campaignId = promotionCode.campaign._id.toString();
                            promoCodeUse.campaignName = promotionCode.campaign.name;
                        }
                        logger.debug(requestId + " - promoCodeUse: " + gson.toJson(promoCodeUse));
                        sqlDao.addPromoCodeUse(promoCodeUse);
                        deletePaxPromoList(promotionCode, ticketReturn.customerId, fleetHasPromo, ticketReturn.currencyISO);
                        promoCodeUse = sqlDao.findByCustomerAndAvailable(promoCode, promoFleet, ticketReturn.customerId, 1);
                    }
                }
            }

            double qupCommission;
            if(booking.pricingType != 0){
                if(booking.request!= null && booking.request.estimate != null && booking.request.estimate.fare != null){
                    double minimumProvider = booking.request.estimate.fare.minFare;
                    boolean normalFare = booking.request.estimate.fare.normalFare;
                    double fareProvider = booking.request.estimate.fare.basicFare;
                    double subTotalBuy = fareProvider + ticketReturn.rushHour + ticketReturn.heavyTraffic + ticketReturn.otherFees;
                    if(normalFare && minimumProvider > subTotalBuy)
                        subTotalBuy = minimumProvider;
                    double tipProvider = booking.request.estimate.fare.tip;
                    double taxValueProvider = booking.request.estimate.fare.tax;
                    double totalBuy = booking.request.estimate.fare.etaFare;
                    qupCommission = ticketReturn.total - totalBuy;
                    ticketEnt.fareProvider = Double.valueOf(DF.format(fareProvider));
                    ticketEnt.tipProvider = Double.valueOf(DF.format(tipProvider));
                    ticketEnt.taxValueProvider = Double.valueOf(DF.format(taxValueProvider));
                    ticketEnt.subTotalProvider = ticketReturn.subTotal;
                    ticketEnt.totalProvider = mapData.get("qupBuyPrice") != null ? Double.parseDouble(mapData.get("qupBuyPrice")) : 0.0;
                    ticketEnt.qupCommission = Double.valueOf(DF.format(qupCommission));
                    ticketEnt.minimumProvider = Double.valueOf(DF.format(minimumProvider));

                    // calculator ride payout
                    PricingPlan pricingPlan = mongoDao.getPricingPlan(booking.fleetId);
                    if(pricingPlan != null && pricingPlan.affiliate != null){
                        ticketEnt.ridePayout = subTotalBuy * pricingPlan.affiliate.supplierPayout * 0.01;
                    }
                }
            }

            // save corporate traveller info
            String corpId = "";
            String corpDivision = "";
            String costCentre = "";
            String corpPO = "";
            String managerName = "";
            String managerEmail = "";
            String title = "";
            String department = "";
            if (!ticketReturn.corporateId.equals("")) {
                if (fleet.generalSetting.advanceInfoCorp) {
                    Account customer = mongoDao.getAccount(ticketReturn.customerId);
                    if (booking.corporateInfo != null) {
                        corpId = booking.corporateInfo.corpId != null ? booking.corporateInfo.corpId : "";
                        costCentre = booking.corporateInfo.costCentre != null ? booking.corporateInfo.costCentre : "";
                        managerName = booking.corporateInfo.managerName != null ? booking.corporateInfo.managerName : "";
                        managerEmail = booking.corporateInfo.managerEmail != null ? booking.corporateInfo.managerEmail : "";
                        department = booking.corporateInfo.department != null ? booking.corporateInfo.department : "";
                        corpDivision = booking.corporateInfo.division != null ? booking.corporateInfo.division : "";
                    }
                    List<String> notPax = Arrays.asList("CC", "API","Car-hailing","Web booking","Partner","mDispatcher","Kiosk");
                    if (customer != null && customer.corporateInfo != null) {
                        if (corpId.equals("") && !notPax.contains(ticketReturn.bookFrom))
                            corpId = customer.corporateInfo.corpId != null ? customer.corporateInfo.corpId : "";
                        if (costCentre.equals("") && !notPax.contains(ticketReturn.bookFrom))
                            costCentre = customer.corporateInfo.costCentre != null ? customer.corporateInfo.costCentre : "";
                        if (managerName.equals("") && !notPax.contains(ticketReturn.bookFrom))
                            managerName = customer.corporateInfo.managerName != null ? customer.corporateInfo.managerName : "";
                        if (managerEmail.equals("") && !notPax.contains(ticketReturn.bookFrom))
                            managerEmail = customer.corporateInfo.managerEmail != null ? customer.corporateInfo.managerEmail : "";
                        if (department.equals("") && !notPax.contains(ticketReturn.bookFrom))
                            department = customer.corporateInfo.department != null ? customer.corporateInfo.department : "";
                        if (corpDivision.equals("") && !notPax.contains(ticketReturn.bookFrom))
                            corpDivision = customer.corporateInfo.corpDivision != null ? customer.corporateInfo.corpDivision : "";
                        title = customer.corporateInfo.title != null ? customer.corporateInfo.title : "";
                        corpPO = customer.corporateInfo.corpPO != null ? customer.corporateInfo.corpPO : "";
                    }
                }
            }

            JSONObject mapCorporate = new JSONObject();
            mapCorporate.put("corpId", corpId);
            mapCorporate.put("corpDivision", corpDivision);
            mapCorporate.put("costCentre", costCentre);
            mapCorporate.put("corpPO", corpPO);
            mapCorporate.put("managerName", managerName);
            mapCorporate.put("managerEmail", managerEmail);
            mapCorporate.put("title", title);
            mapCorporate.put("department", department);

            // check referral program
            String referralCode = "";
            double referralEarning = 0.0;
            double totalEarning = 0.0;
            ReferralPackage referralPackage = null;
            double referralMaxAmount = 0.0;
            boolean isReferralMaxAmount = false;
            String referralType = "";
            double referralPercent = 0.0;
            String passengerId = ticketReturn.customerId;
            boolean isMatchingReferral = false;

            // check referral program - driver-refer-pax or pax-refer-pax
            Referral referral = null;
            boolean isDriverRefer = false;
            String referralDriverId = "";
            String referralFrom = "";

            if (referralHistories != null) {
                if (referrerType.equals(KeysUtil.APPTYPE_DRIVER)) {
                    // driver refer pax
                    isDriverRefer = true;
                    referral = mongoDao.getReferral(ticketReturn.fleetId);
                    int limitUser = 0;
                    if (referral != null && referral.isLimitInvitation) {
                        limitUser = referral.limitedUser != null ? referral.limitedUser : 0;
                    }
                    boolean isValid = checkLimitReferee(referralHistories, limitUser);
                    if (isValid && !KeysUtil.INCOMPLETE_PAYMENT.contains(ticketReturn.transactionStatus)) {
                        referralFrom = "driver";
                        isMatchingReferral = true;
                        double netEarning = mapData.get("netEarning") != null ? Double.parseDouble(mapData.get("netEarning")) : 0.0;
                        referralDriverId = referralHistories.userId != null ? referralHistories.userId : "";
                        // get companyId of driver
                        Account account = mongoDao.getAccount(referralDriverId);
                        if (account != null && account.driverInfo != null && account.driverInfo.company != null) {
                            driverFleetId = account.fleetId;
                            driverName = account.firstName + " " + account.lastName;
                            driverName = driverName.trim(); // remove spaces
                            driverPhone = account.phone != null ? account.phone : "";
                            driverUserName = account.username != null ? account.username.trim() : "";
                            companyId = account.driverInfo.company.companyId != null ? account.driverInfo.company.companyId : "";
                            companyName = account.driverInfo.company.name != null ? account.driverInfo.company.name : "";
                            driverNumber = account.driverInfo.drvId != null ? account.driverInfo.drvId : "";
                        }
                        if (!referralDriverId.isEmpty()) {
                            Calendar pickup = Calendar.getInstance();
                            pickup.setTimeInMillis(ticketReturn.pickupTime.getTime());
                            Map<String, Object> mapReferral = getReferralInfo(fleet.fleetId, referral, referralDriverId, ticketReturn.subTotal, netEarning, ticketReturn.currencyISO, totalCompletedBook, pickup.getTime(),
                                    isNewIncentiveReferral, ticketReturn.promoAmount);
                            referralCode = mapReferral.get("referralCode").toString();
                            referralEarning = Double.parseDouble(mapReferral.get("referralEarning").toString());
                            totalEarning = Double.parseDouble(mapReferral.get("totalEarning").toString());
                            // remove referralEarning from totalEarning if fleet setting distribute date
                            if (referral.isDistributeIncentiveToDriver) {
                                totalEarning = netEarning;
                            }
                            if (mapReferral.get("referralPackage") != null)
                                referralPackage = (ReferralPackage) mapReferral.get("referralPackage");
                            if (mapReferral.get("referralMaxAmount") != null)
                                referralMaxAmount = Double.parseDouble(mapReferral.get("referralMaxAmount").toString());
                            if (mapReferral.get("isReferralMaxAmount") != null)
                                isReferralMaxAmount = Boolean.parseBoolean(mapReferral.get("isReferralMaxAmount").toString());
                        }
                    }
                } else {
                    // pax refer pax
                    PaxReferral paxReferral = mongoDao.getPaxReferral(ticketReturn.fleetId);
                    int limitUser = 0;
                    if (paxReferral != null && paxReferral.isLimitInvitation) {
                        limitUser = paxReferral.limitedUser;
                    }
                    boolean isValid = checkLimitReferee(referralHistories, limitUser);
                    if (isValid && paxReferral != null) {
                        referralFrom = "passenger";
                        isMatchingReferral = true;
                        // check refer code
                        ReferralInfo referralInfo = mongoDao.getReferralInfo(ticketReturn.fleetId, referralHistories.userId); //userId is referrerId
                        if (referralInfo != null) {
                            referralCode = referralInfo.code != null ? referralInfo.code : "";
                            referralType = "amount"; // fixed value
                            if (paxReferral.refereeGet != null && paxReferral.refereeGet.money.size() > 0) {
                                for (AmountByCurrency amountByCurrency : paxReferral.refereeGet.money) {
                                    if (amountByCurrency.currencyISO.equals(ticketReturn.currencyISO)) {
                                        referralEarning = amountByCurrency.value;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            logger.debug(requestId + " - referralFrom: " + referralFrom);
            logger.debug(requestId + " - referralCode: " + referralCode);

            String passengerAvatar = "";
            String customerIdNumber = "";
            String customerPhone = "";
            if (passenger != null) {
                passengerAvatar = passenger.avatar;
                customerPhone = passenger.phone;
                if(passenger.profile!= null && passenger.profile.idNumber != null){
                    customerIdNumber = passenger.profile.idNumber;
                }
            }
            Gson gsonUpdate = new GsonBuilder().serializeNulls().create();
            // check and return ReferralEarning data
            if (referral !=null && isDriverRefer && !referralCode.isEmpty() && referralPackage != null) {
                referralType = referralPackage.driver.type.equals("money") ? "amount" : "percent";
                if (referralType.equals("percent") && referralPackage.driver.settlement != null) {
                    referralPercent = referralPackage.driver.settlement.value;
                }
                if (referral.isDistributeIncentiveToDriver) {
                    double currentEarning = 0.0;
                    try {
                        String startDate = KeysUtil.SDF_DMYHMS.format(referral.distributeIncentiveToDriver.startDate);
                        String endDate = KeysUtil.SDF_DMYHMS.format(referral.distributeIncentiveToDriver.endDate);
                        currentEarning = sqlDao.getReferralEarning(driverFleetId, referralDriverId, startDate, endDate, ticketReturn.currencyISO);
                    } catch (Exception ex) {
                        logger.debug(requestId + " - error: " + CommonUtils.getError(ex));
                    }

                    // will transfer on distribute date - store referral transaction
                    ReferralTransaction referralTransaction = new ReferralTransaction();
                    referralTransaction.fleetId = driverFleetId;
                    referralTransaction.userId = referralDriverId;
                    referralTransaction.companyId = companyId;
                    referralTransaction.companyName = companyName;
                    referralTransaction.bookId = ticketReturn.bookId;
                    referralTransaction.phone = driverPhone;
                    referralTransaction.fullName = driverName;
                    referralTransaction.driverNumber = driverNumber;
                    referralTransaction.transactionStatus = ticketReturn.transactionStatus;
                    referralTransaction.referralCode = referralCode;
                    referralTransaction.currencyISO = ticketReturn.currencyISO;
                    referralTransaction.cashIn = referralEarning;
                    referralTransaction.cashOut = 0.0;
                    referralTransaction.balance = referralEarning + currentEarning;
                    referralTransaction.transferred = 0;
                    referralTransaction.createdTime = TimezoneUtil.getGMTTimestamp();
                    long id = sqlDao.addReferralTransaction(referralTransaction);
                    logger.debug(requestId + " - addReferralTransaction: " + gson.toJson(referralTransaction));
                    logger.debug(requestId + " - addReferralTransaction: " + id);

                    // format time
                    String referralTransactionStr = gsonUpdate.toJson(referralTransaction);
                    JSONObject jsonReferralTransaction = gsonUpdate.fromJson(referralTransactionStr, JSONObject.class);
                    jsonReferralTransaction.remove("createdTime");
                    jsonReferralTransaction.put("createdTime", TimezoneUtil.formatISODate(referralTransaction.createdTime, "GMT"));

                    map.put("referralTransaction", jsonReferralTransaction);
                } else {
                    // add fund immediately to credit wallet
                    // update reason
                    String referralReason;
                    if (totalCompletedBook == 0) {
                        referralReason = "Customer " + ticketReturn.customerName + " " + customerPhone + " completes first booking " + booking.bookId;
                    } else {
                        referralReason = "Customer " + ticketReturn.customerName + " " + customerPhone + " completes booking " + booking.bookId;
                    }
                    UpdateDriverCreditBalance depositReferral = new UpdateDriverCreditBalance();
                    depositReferral.type = CommonArrayUtils.DRIVER_WALLET_TYPE[10];
                    depositReferral.fleetId = driverFleetId;
                    depositReferral.driverId = referralDriverId;
                    depositReferral.amount = referralEarning;
                    depositReferral.currencyISO = ticketReturn.currencyISO;
                    depositReferral.mongoDao = mongoDao;
                    depositReferral.sqlDao = sqlDao;
                    depositReferral.reason = referralReason;
                    depositReferral.requestId = requestId;
                    depositReferral.transactionId = CommonUtils.getOrderId();
                    depositReferral.senderId = "";
                    depositReferral.receiverId = "";
                    depositReferral.additionalData.put("bookId", booking.bookId);
                    String depositReferralResult = depositReferral.doUpdate();
                    logger.debug(requestId + " - deposit referral earning to credit wallet: " + depositReferralResult);
                }
            }
            JSONObject jsonReferral = new JSONObject();
            jsonReferral.put("referralCode", referralCode);
            jsonReferral.put("referralEarning", referralEarning);
            jsonReferral.put("totalEarning", totalEarning);
            jsonReferral.put("referralMaxAmount", referralMaxAmount);
            jsonReferral.put("referralType", referralType);
            jsonReferral.put("isReferralMaxAmount", isReferralMaxAmount);
            jsonReferral.put("referralPercent", referralPercent);
            jsonReferral.put("referralFrom", referralFrom);

            boolean isVipPassenger = false;
            if (booking.psgInfo != null && booking.psgInfo.rank != null)
                isVipPassenger = booking.psgInfo.rank == 1;
            JSONObject jsonRequestInfo = new JSONObject();
            jsonRequestInfo.put("passengerAvatar", passengerAvatar);
            jsonRequestInfo.put("isVipPassenger", isVipPassenger);
            jsonRequestInfo.put("pickUpZoneId", zoneId);
            jsonRequestInfo.put("pickUpZoneName", zoneName);
            jsonRequestInfo.put("customerIdNumber", customerIdNumber);
            Map<String, String> cardInfo = paymentUtil.getCreditToken(booking, booking.pricingType == 0);
            jsonRequestInfo.put("cardMask", cardInfo.get("cardMask"));
            ticketEnt.cardType = cardInfo.get("cardType");
            // get corporate data
            if (booking.bookFrom.equals(KeysUtil.corp_board)) {
                String itineraryId = booking.itinerary != null ? booking.itinerary._id.toString() : "";
                if (!itineraryId.isEmpty()) {
                    CorpItinerary corpItinerary = mongoDao.getCorpItinerary(itineraryId);
                    if (corpItinerary != null) {
                        String departmentId = "";
                        String departmentName = "";
                        if (corpItinerary.departmentInfo != null) {
                            departmentId = corpItinerary.departmentInfo._id.toString();
                            departmentName = corpItinerary.departmentInfo.name != null ? corpItinerary.departmentInfo.name : "";
                        }
                        itinerary.put("departmentId", departmentId);
                        itinerary.put("departmentName", departmentName);

                        String firstName = "";
                        String lastName = "";
                        String fullName = "";
                        String phone = "";
                        String email = "";
                        if (corpItinerary.psgInfo != null) {
                            firstName = corpItinerary.psgInfo.firstName != null ? corpItinerary.psgInfo.firstName : "";
                            lastName = corpItinerary.psgInfo.lastName != null ? corpItinerary.psgInfo.lastName : "";
                            fullName = corpItinerary.psgInfo.fullName != null ? corpItinerary.psgInfo.fullName : "";
                            phone = corpItinerary.psgInfo.phone != null ? corpItinerary.psgInfo.phone : "";
                            email = corpItinerary.psgInfo.email != null ? corpItinerary.psgInfo.email : "";
                        }
                        JSONObject corpCustomerInfo = new JSONObject();
                        corpCustomerInfo.put("firstName", firstName);
                        corpCustomerInfo.put("lastName", lastName);
                        corpCustomerInfo.put("fullName", fullName);
                        corpCustomerInfo.put("phone", phone);
                        corpCustomerInfo.put("email", email);
                        itinerary.put("corpCustomerInfo", corpCustomerInfo);
                    }
                }
            }
            String result = "";
            int i = 0;
            while (result.equals("") && i < 3) {
                try {
                    // replace data from booking
                    JSONObject jsonBooking = new JSONObject();
                    if (booking.request != null) {
                        if (booking.request.pickup != null) {
                            jsonBooking.put("pickup", booking.request.pickup.address);
                            jsonBooking.put("pickupAddressDetails", booking.request.pickup.addressDetails != null ?
                                    CommonUtils.removeSpecialCharacter(gson.toJson(booking.request.pickup.addressDetails)) : "");
                        }
                        if (booking.request.destination != null) {
                            jsonBooking.put("destination", booking.request.destination.address);
                            jsonBooking.put("destinationAddressDetails", booking.request.destination.addressDetails != null ?
                                    CommonUtils.removeSpecialCharacter(gson.toJson( booking.request.destination.addressDetails)):"");
                        }
                        String receiptComment = booking.request.note != null ? booking.request.note : "";
                        String bookingNotes = booking.request.notes != null ? booking.request.notes : "";
                        String operatorNote = booking.request.operatorNote != null ? booking.request.operatorNote : "";
                        jsonBooking.put("receiptComment", receiptComment);
                        jsonBooking.put("bookingNotes", bookingNotes);
                        jsonBooking.put("operatorNote", operatorNote);
                        jsonBooking.put("vehicleImageRequest", booking.request.vehicleImageRequest != null ? booking.request.vehicleImageRequest : "");
                        jsonBooking.put("packageRateId", booking.request.packageRateId != null ? booking.request.packageRateId : "");
                        jsonBooking.put("packageRateName", booking.request.packageRateName != null ? booking.request.packageRateName : "");
                        jsonBooking.put("flatRouteId", booking.request.estimate.fare.routeId != null ? booking.request.estimate.fare.routeId : "");
                        jsonBooking.put("flatRouteName", booking.request.estimate.fare.route != null ? booking.request.estimate.fare.route : "");
                        jsonBooking.put("rateType", booking.request.estimate.fare.rateType != null ? booking.request.estimate.fare.rateType : "");
                        jsonBooking.put("rateId", booking.request.estimate.fare.rateId != null ? booking.request.estimate.fare.rateId : "");
                        if(referrerType.equals(KeysUtil.APPTYPE_DRIVER) && isNewIncentiveReferralWithPromo || referrerType.equals(KeysUtil.APPTYPE_PASSENGER) && paidByDriver)
                            jsonBooking.put("promoCustomerType", booking.request.promoCustomerType != null ? booking.request.promoCustomerType : promoCustomerType);
                        if(booking.request.moreInfo != null && booking.request.moreInfo.flightInfo != null){
                            jsonBooking.put("flightNumber", booking.request.moreInfo.flightInfo.flightNumber != null ? booking.request.moreInfo.flightInfo.flightNumber : "");
                        }
                        if(booking.request.estimate != null){
                            jsonBooking.put("isFareEdited", booking.request.estimate.isFareEdited);
                            jsonBooking.put("reasonEditFare",  booking.request.estimate.reasonEditFare != null ? booking.request.estimate.reasonEditFare: "");
                            String dynamicType = booking.request.estimate.fare.dynamicType != null ? booking.request.estimate.fare.dynamicType: "";
                            if (dynamicType.isEmpty()) {
                                DynamicFare dF = mongoDao.getDynamicFare(fleet.fleetId, zoneId, booking.request.pickup.geo);
                                if(dF != null && dF.isActive){
                                    dynamicType = dF.type != null ? dF.type :"factor";
                                }
                            }
                            jsonBooking.put("dynamicType",  dynamicType);
                        }

                        int paxNumber = booking.request.paxNumber;
                        jsonBooking.put("paxNumber", paxNumber != 0 ? paxNumber : 1);
                        jsonBooking.put("luggageNumber", booking.request.luggageNumber);

                        int typeRate = booking.request.typeRate;
                        String packageRateName = booking.request.packageRateName != null ? booking.request.packageRateName : "";

                    }
                    if (booking.psgInfo != null) {
                        String firstName = booking.psgInfo.firstName != null ? booking.psgInfo.firstName : "";
                        String lastName = booking.psgInfo.lastName != null ? booking.psgInfo.lastName : "";
                        jsonBooking.put("customerName", firstName + " " + lastName);
                        jsonBooking.put("customerFirstName", firstName);
                        jsonBooking.put("customerLastName", lastName);
                        jsonBooking.put("supportId", booking.psgInfo.supportId != null ? booking.psgInfo.supportId : "");
                        if(passenger != null && passenger.profile != null){
                            jsonBooking.put("customerGender", passenger.profile.gender);
                            jsonBooking.put("customerBirthday", passenger.profile.dob != null ? passenger.profile.dob : "");
                        }
                    }
                    if (booking.drvInfo != null) {
                        String firstName = booking.drvInfo.firstName != null ? booking.drvInfo.firstName : "---";
                        String lastName = booking.drvInfo.lastName != null ? booking.drvInfo.lastName : "---";
                        jsonBooking.put("driverName", firstName + " " + lastName);
                        jsonBooking.put("driverFirstName", firstName);
                        jsonBooking.put("driverLastName", lastName);
                        jsonBooking.put("driverType", driverType);

                        String vehicleTypeId = booking.drvInfo.vehicleId != null ? booking.drvInfo.vehicleId.toString() : "";
                        String vehicleType = booking.request.vehicleTypeRequest != null ? booking.request.vehicleTypeRequest : "";
                        String driverVehicleType = booking.drvInfo.vehicleType != null ? booking.drvInfo.vehicleType : "";
                        jsonBooking.put("vehicleTypeId", vehicleTypeId);
                        jsonBooking.put("vehicleType", vehicleType);
                        jsonBooking.put("driverVehicleType", driverVehicleType);

                        String plateNumber = booking.drvInfo.plateNumber != null ? booking.drvInfo.plateNumber : "";
                        jsonBooking.put("plateNumber", plateNumber);

                        jsonBooking.put("companyName", companyName);
                    }
                    if (booking.corporateInfo != null) {
                        jsonBooking.put("isAirline", booking.corporateInfo.isAirline);
                        String clientCaseMatter = booking.corporateInfo.clientCaseMatter != null ? booking.corporateInfo.clientCaseMatter : "";
                        String chargeCode = booking.corporateInfo.chargeCode != null ? booking.corporateInfo.chargeCode : "";
                        jsonBooking.put("clientCaseMatter", clientCaseMatter);
                        jsonBooking.put("chargeCode", chargeCode);
                    }
                    if(booking.superHelper && booking.request != null && booking.request.instructions != null){
                        jsonBooking.put("instructions", booking.request.instructions.content);
                    }
                    if (booking.request.primaryPartialMethod != -1) {
                        int pointsEarned = 0;
                        try {
                            // send notify to Point System
                            JSONObject objBody = new JSONObject();
                            objBody.put("bookId", ticketReturn.bookId);
                            JSONArray jsonArray = new JSONArray();
                            if (ticketReturn.splitPayment) {
                                JSONObject methodPrimary = new JSONObject();
                                methodPrimary.put("method", booking.request.primaryPartialMethod);
                                methodPrimary.put("amount", ticketReturn.paidByWallet);
                                jsonArray.add(methodPrimary);
                                JSONObject methodPartial = new JSONObject();
                                methodPartial.put("method", TicketUtil.getPaymentType(ticketReturn.transactionStatus));
                                methodPartial.put("amount", ticketReturn.paidByOtherMethod);
                                jsonArray.add(methodPartial);
                            } else {
                                JSONObject methodPrimary = new JSONObject();
                                methodPrimary.put("method", booking.request.primaryPartialMethod);
                                methodPrimary.put("amount", 0.0);
                                jsonArray.add(methodPrimary);
                                JSONObject methodPartial = new JSONObject();
                                methodPartial.put("method", TicketUtil.getPaymentType(ticketReturn.transactionStatus));
                                methodPartial.put("amount", ticketReturn.total);
                                jsonArray.add(methodPartial);
                            }
                            objBody.put("payment", jsonArray);

                            logger.debug(requestId + " - URL: " + PaymentUtil.split_payment_completed);
                            logger.debug(requestId + " - data: " + objBody.toJSONString());
                            String resultPS = CommonUtils.sendPostRequest(requestId, objBody.toJSONString(),
                                    PaymentUtil.split_payment_completed, ServerConfig.point_system_user, ServerConfig.point_system_pass);
                            if (resultPS.isEmpty()) {
                                // something went wrong!!!
                                Thread.sleep(2000);
                                resultPS = CommonUtils.sendPostRequest(requestId, objBody.toJSONString(),
                                        PaymentUtil.split_payment_completed, ServerConfig.point_system_user, ServerConfig.point_system_pass);
                            }
                            logger.debug(requestId + " - result: " + resultPS);
                            if (!resultPS.isEmpty()) {
                                org.json.JSONObject json = new org.json.JSONObject(resultPS);
                                if (json.has("points")) {
                                    pointsEarned = Integer.parseInt(json.get("points").toString());
                                }
                            }
                        } catch (Exception ex) {
                            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
                        }
                        logger.debug(requestId + " - points from Point system: " + pointsEarned);
                        jsonBooking.put("pointsEarned", pointsEarned);
                    } else {
                        if (booking.completedInfo != null) {
                            jsonBooking.put("pointsEarned", booking.completedInfo.pointsEarned);
                        }
                    }
                    jsonBooking.put("superHelper", booking.superHelper);
                    // update job type with data from booking
                    jobType = "rideHailing";
                    if(booking.superHelper)
                        jobType = "superHelper";
                    if(ticketEnt.typeBooking.equals("intercity"))
                        jobType = "intercity";
                    jsonBooking.put("jobType", booking.jobType != null ? booking.jobType: jobType);

                    if(booking.createdByOperator != null){
                        jsonBooking.put("operatorId", booking.createdByOperator.userId);
                        jsonBooking.put("operatorName", booking.createdByOperator.name);
                    }

                    //get recurring info
                    if(booking.recurring != null)
                        jsonBooking.put("recurring", gson.toJson(booking.recurring));

                    //get pu && do points
                    if(booking.puPoints != null && !booking.puPoints.isEmpty())
                        jsonBooking.put("puPoints", gson.toJson(booking.puPoints));
                    if(booking.doPoints != null && !booking.doPoints.isEmpty())
                        jsonBooking.put("doPoints", gson.toJson(booking.doPoints));
                    jsonBooking.put("isMultiPoints", booking.isMultiPoints);

                    //Get delivery info
                    DeliveryInfo deliveryInfo = booking.deliveryInfo;
                    boolean merchantSelfManage = booking.deliveryInfo != null && booking.deliveryInfo.merchantSelfManage;
                    jsonBooking.put("merchantSelfManage", merchantSelfManage);
                    //set fare details
                    try{
                        if(booking.jobType != null){
                            if(booking.jobType.equals("shopForYou")){
                                deliveryInfo.merchants.get(0).fareDetails = booking.request.estimate.fare.recipients.get(0);
                            } else if(booking.jobType.equals("delivery")){
                                for (int j = 0; j < deliveryInfo.recipients.size(); j++) {
                                    deliveryInfo.recipients.get(j).fareDetails = booking.request.estimate.fare.recipients.get(j);
                                }
                            }
                        }
                    } catch (Exception ex){
                        logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
                    }
                    //jsonBooking.put("deliveryInfo", deliveryInfo != null ? gson.toJson(deliveryInfo): new JSONObject());
                    jsonBooking.put("deliveryInfo", gson.toJson(deliveryInfo));
                    ticketEnt.transactionStatus = ticketReturn.transactionStatus;
                    ticketEnt.paymentType = booking.request.paymentType;

                    // add adjust data
                    String adjustPrice = "";
                    if (booking.request.estimate != null && booking.request.estimate.fare != null) {
                        double addOnPrice = booking.request.estimate.fare.addOnPrice;
                        JSONObject objAdjust = new JSONObject();
                        objAdjust.put("total", ticketReturn.total);
                        objAdjust.put("subTotal", ticketReturn.subTotal);
                        double oldTotal = Double.parseDouble(DF.format(ticketReturn.total - addOnPrice));
                        double oldSubTotal = Double.parseDouble(DF.format(ticketReturn.subTotal - addOnPrice));
                        if(KeysUtil.INCOMPLETE_PAYMENT.contains(ticketReturn.transactionStatus)){
                            oldTotal = ticketReturn.total;
                            oldSubTotal = ticketReturn.subTotal;
                            addOnPrice = 0;
                        }
                        objAdjust.put("addOnPrice", addOnPrice);
                        objAdjust.put("oldTotal", oldTotal);
                        objAdjust.put("oldSubTotal", oldSubTotal);
                        adjustPrice = objAdjust.toJSONString();
                    }
                    mapData.put("adjustPrice", adjustPrice);

                    jsonBooking.put("markupPrice", markupPrice);
                    jsonBooking.put("markupDifference", markupDifference);
                    jsonBooking.put("reasonMarkup", reasonMarkup);

                    // save setting pay to driver/fleet
                    FleetFare fleetFare = mongoDao.getFleetFare(ticketReturn.psgFleetId);
                    if (fleetFare != null) {
                        jsonSetting.put("tollFeePayTo", fleetFare.tollFeePayTo);
                        jsonSetting.put("parkingFeePayTo", fleetFare.parkingFeePayTo);
                        jsonSetting.put("gasFeePayTo", fleetFare.gasFeePayTo);
                        jsonSetting.put("airportPayTo", fleetFare.airport != null ? fleetFare.airport.payTo : "not setting");
                        jsonSetting.put("meetDriverPayTo", fleetFare.meetDriver != null ? fleetFare.meetDriver.payTo : "not setting");
                    }
                    // save wallet info
                    if (booking.request != null) {
                        String gateway = booking.request.gateway != null ? booking.request.gateway : "";
                        String walletName = booking.request.walletName != null ? booking.request.walletName.trim() : "";
                        String iconUrl = booking.request.iconUrl != null ? booking.request.iconUrl : "";
                        jsonBooking.put("gateway", gateway);
                        jsonBooking.put("walletName", walletName);
                        jsonBooking.put("iconUrl", iconUrl);
                        if (booking.request.origin != null && booking.request.origin.destination != null && booking.request.origin.destination.geo != null) {
                            Object[] arr = booking.request.origin.destination.geo.toArray();
                            if (arr.length > 1) {
                                jsonBooking.put("originDestinationGeoLon", arr[0]);
                                jsonBooking.put("originDestinationGeoLat", arr[1]);
                            }
                        }
                    }

                    // save fleet service
                    jsonBooking.put("serviceActive", booking.request.serviceActive);
                    List<FleetService> fleetServices = booking.request.fleetServices != null ? booking.request.fleetServices : new ArrayList<>();
                    ticketEnt.fleetServices = paymentUtil.getFleetServicesFromBooking(requestId, fleetServices, ticketReturn.subTotal, true);


                    // check if booking complete without service then update status
                    if (ticketReturn.completedWithoutService) {
                        ticketEnt.transactionStatus = KeysUtil.PAYMENT_COMPLETE_WITHOUT_SERVICE;
                    } else {
                        ticketEnt.transactionStatus = ticketReturn.transactionStatus;
                    }
                    logger.debug(requestId + " - transactionStatus after payment: " + ticketEnt.transactionStatus);

                    // save external information from booking
                    if (booking.externalInfo != null) {
                        mapData.put("externalInfo", gson.toJson(booking.externalInfo));
                    }
                    mapData.put("requestId", requestId);

                    logger.debug(requestId + " - updateTicketAfterPayment ...");
                    logger.debug(requestId + " - ticketEnt: " + gson.toJson(ticketEnt));
                    logger.debug(requestId + " - mapData: " + gson.toJson(mapData));
                    logger.debug(requestId + " - mapCorporate: " + gson.toJson(mapCorporate));
                    logger.debug(requestId + " - jsonSetting: " + gson.toJson(jsonSetting));
                    logger.debug(requestId + " - sharingObject: " + gson.toJson(sharingObject));
                    logger.debug(requestId + " - itinerary: " + gson.toJson(itinerary));
                    logger.debug(requestId + " - OTWDistance: " + gson.toJson(OTWDistance));
                    logger.debug(requestId + " - jsonReferral: " + gson.toJson(jsonReferral));
                    logger.debug(requestId + " - jsonRequestInfo: " + gson.toJson(jsonRequestInfo));
                    logger.debug(requestId + " - jsonDriver: " + gson.toJson(jsonDriver));
                    logger.debug(requestId + " - jsonBooking: " + gson.toJson(jsonBooking));
                    result = sqlDao.updateTicketAfterPayment(ticketEnt, mapData, mapCorporate, jsonSetting, sharingObject, itinerary, OTWDistance,
                            jsonReferral, jsonRequestInfo, jsonDriver, jsonBooking);
                    logger.debug(requestId + " - result: " + result);
                    if (!result.isEmpty())
                        logger.debug(requestId + " - update ticket data to mysql for bookId "+ booking.bookId +" completed !!!!!!!!!!");
                } catch(Exception ex) {
                    logger.debug(requestId + " - update ticket ERROR: " + CommonUtils.getError(ex));
                }
                if (result.isEmpty()) {
                    Thread.sleep(2000);
                    logger.debug(requestId + " - update ticket data to mysql for bookId "+ booking.bookId +" has failed - RETRY update ticket  !!!!!!!!!!");
                    i++;
                }
            }
            if (result == null || result.isEmpty()) {
                // still error, do not try do update ticket
                map.put("returnCode", 304);
                return map;
            }
            // save data for trip notification email
            saveCustomerData(fleet, ticketReturn, customerPhone, referralCode, referralHistories, totalCompletedBook);

            // update first booking time
            // ignore case cancel booking
            String timezoneId = booking.request.pickup.timezone != null ? booking.request.pickup.timezone.trim() : "";
            if (timezoneId.isEmpty()) {
                timezoneId = fleet.timezone;
            }
            Date convertDate = TimezoneUtil.offsetTimeZone(ticketReturn.completedTime, "GMT", timezoneId);
            String convertTime = TimezoneUtil.formatISODate(convertDate, timezoneId);
            if (convertTime.isEmpty()) {
                convertTime = TimezoneUtil.formatISODate(ticketReturn.completedTime, "GMT");
            }
            if (!convertTime.contains("Z")) {
                convertTime = convertTime.substring(0, convertTime.length() - 5) + "Z";
            }
            logger.debug(requestId + " - firstBookingDate: " + convertTime);
            logger.debug(requestId + " - firstBookingDateGMT: " + TimezoneUtil.formatISODate(ticketReturn.completedTime, "GMT"));
            logger.debug(requestId + " - isMatchingReferral: " + isMatchingReferral);
            boolean updateReferralHistories = false;
            boolean updateReferralHistoriesDriver = false;
            double firstEarning = referralEarning;

            logger.debug(requestId + " - driverId: " + ticketReturn.driverId);
            ReferralHistories referralHistoriesDriver = mongoDao.getLinkReferralByRefereeId(ticketReturn.driverId);
            logger.debug(requestId + " - referralHistoriesDriver: " + gson.toJson(referralHistoriesDriver));
            if (!KeysUtil.INCOMPLETE_PAYMENT.contains(ticketReturn.transactionStatus)){
                // check and update referralType = driver_refer_driver with refereeId = driverId
                if (referralHistoriesDriver != null) {
                    org.json.JSONObject updateObj = new org.json.JSONObject();
                    updateObj.put("firstBookingDate", convertTime);
                    updateObj.put("firstBookingDateGMT", TimezoneUtil.formatISODate(ticketReturn.completedTime, "GMT"));
                    updateObj.put("firstBooking", booking.bookId);
                    updateObj.put("firstBookingCurrency", booking.currencyISO);
                    updateObj.put("firstEarning", firstEarning);
                    updateReferralHistoriesDriver = mongoDao.updateReferralHistories(ticketReturn.driverId, updateObj);
                }

                // check and update referralType = passenger with refereeId = customerId
                if(referralHistories != null && referralHistories.referralType.equals(KeysUtil.APPTYPE_PASSENGER)){
                    firstEarning = getRefererGetAmount(ticketReturn.fleetId, KeysUtil.APPTYPE_PASSENGER, ticketReturn.currencyISO);
                }
                org.json.JSONObject updateObj = new org.json.JSONObject();
                updateObj.put("firstBookingDate", convertTime);
                updateObj.put("firstBookingDateGMT", TimezoneUtil.formatISODate(ticketReturn.completedTime, "GMT"));
                updateObj.put("firstBooking", booking.bookId);
                updateObj.put("firstBookingCurrency", booking.currencyISO);
                updateObj.put("firstEarning", firstEarning);
                updateReferralHistories = mongoDao.updateReferralHistories(passengerId, updateObj);
            }

            String jsonReferralHistoryId = "";
            String jsonReferralHistoryIdDriver = "";
            JsonObject jsonReferralHistories = null;
            JsonObject jsonReferralHistoriesDriver = null;
            if (updateReferralHistories) {
                jsonReferralHistoryId = referralHistories._id.toString();
                referralHistories.fleetId = ticketReturn.fleetId;
                //referralHistories.createdDate = TimezoneUtil.offsetTimeZone(referralHistories.createdDate, Calendar.getInstance().getTimeZone().getID(), "GMT");
                referralHistories.firstBookingDate = TimezoneUtil.offsetTimeZone(ticketReturn.completedTime, "GMT", timezoneId);
                referralHistories.firstBookingDateGMT = ticketReturn.completedTime;
                referralHistories.firstBooking = booking.bookId;
                referralHistories.firstBookingCurrency = booking.currencyISO;
                referralHistories.firstEarning = firstEarning;
                Gson gson = new GsonBuilder().serializeNulls().create();
                String referralHistoriesStr = gson.toJson(referralHistories);
                JsonElement jsonElement = gson.fromJson(referralHistoriesStr, JsonElement.class);
                jsonReferralHistories = jsonElement.getAsJsonObject();
                jsonReferralHistories.remove("createdDate");
            }
            if (updateReferralHistoriesDriver) {
                jsonReferralHistoryIdDriver = referralHistoriesDriver._id.toString();
                referralHistoriesDriver.fleetId = ticketReturn.fleetId;
                referralHistoriesDriver.firstBookingDate = TimezoneUtil.offsetTimeZone(ticketReturn.completedTime, "GMT", timezoneId);
                referralHistoriesDriver.firstBookingDateGMT = ticketReturn.completedTime;
                referralHistoriesDriver.firstBooking = booking.bookId;
                referralHistoriesDriver.firstBookingCurrency = booking.currencyISO;
                referralHistoriesDriver.firstEarning = firstEarning;
                Gson gson = new GsonBuilder().serializeNulls().create();
                String referralHistoriesStr = gson.toJson(referralHistoriesDriver);
                JsonElement jsonElement = gson.fromJson(referralHistoriesStr, JsonElement.class);
                jsonReferralHistoriesDriver = jsonElement.getAsJsonObject();
                jsonReferralHistoriesDriver.remove("createdDate");
            }

            // get complete time in ISO format
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(ticketReturn.completedTime);

            map.put("returnCode", 200);
            map.put("ticket", result);
            map.put("transactionStatus", ticketReturn.transactionStatus);
            map.put("psgFleetId", ticketReturn.psgFleetId);
            map.put("completedTime", TimezoneUtil.formatISODate(calendar.getTime(), "GMT"));
            double profit = booking.pricingType == 0 ? Double.parseDouble(mapData.get("netEarning")) : Double.parseDouble(mapData.get("netEarningProvider"));
            map.put("profit", profit);
            double revenue = booking.pricingType == 0 ? ticketReturn.totalCharged : ticketEnt.totalProvider;
            if (booking.pricingType == 1 && KeysUtil.INCOMPLETE_PAYMENT.contains(ticketReturn.transactionStatus)
                    && (booking.intercity && ticketReturn.totalCharged == 0)
                )
                revenue = 0.0;
            map.put("revenue", revenue);
            /*double comm = mapData.get("comm") != null ? Double.parseDouble(mapData.get("comm")) : 0.0;
            double techFee = ticketReturn.techFee;
            double promoAmount = mapData.get("promoAmount") != null ? Double.parseDouble(mapData.get("promoAmount")) : 0.0;
            double fleetProfit = comm + techFee  - promoAmount;
            if(isNewIncentiveReferral || paidByDriver){
                fleetProfit = comm + techFee;
            }*/
            map.put("fleetProfit", mapData.get("fleetProfit") != null ? Double.parseDouble(mapData.get("fleetProfit")) : 0.0);
            map.put("jsonReferralHistoryId", jsonReferralHistoryId);
            map.put("referralHistories", jsonReferralHistories != null ? jsonReferralHistories : "");
            map.put("jsonReferralHistoryIdDriver", jsonReferralHistoryIdDriver);
            map.put("referralHistoriesDriver", jsonReferralHistoriesDriver != null ? jsonReferralHistoriesDriver : "");

            // check setting to store billing report
            boolean enableBilling = false;
            if (fleet.demoInfo != null && fleet.demoInfo.demoFleet) {
                enableBilling = true;
            }
            map.put("enableBilling", enableBilling);
            //update total provider to booking if book affiliate
            if(booking.pricingType != 0 && !ticketReturn.transactionStatus.equals("incident")){
                logger.debug(requestId + " - update total buy for booking completed!!!");
                boolean updated = mongoDao.updateTotalBuy(booking.bookId, ticketEnt.totalProvider, ticketReturn.transactionStatus);
                if(!updated){
                    Thread.sleep(2000);
                    logger.debug(requestId + " - update total buy failed -  try again");
                    mongoDao.updateTotalBuy(booking.bookId, ticketEnt.totalProvider, ticketReturn.transactionStatus);
                }
                //if(!ServerConfig.mongo_oplog){
                    // push data booking completed to module search
                    Booking bookingCompleted = mongoDao.getBookingCompleted(booking.bookId);
                    if(bookingCompleted != null){
                        logger.debug(requestId + " - push data booking completed to module search - bookId: " + booking.bookId);
                        try{
                            QUpSearchPublisher searchPublisher = QUpSearchPublisher.getInstance();
                            searchPublisher.sendBookingCompleted(requestId, bookingCompleted);
                        }catch (Exception ex){
                            logger.debug(requestId + " - push data booking completed to module search ERROR !!! " + CommonUtils.getError(ex));
                        }
                    }
                //}
            }

            // check and return promo code data
            SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
            if (promoCodeUse != null) {
                String promoFleet = booking.isFarmOut ? booking.request.psgFleetId : booking.fleetId;
                PromoCodeUse promo = new PromoCodeUse();
                promo.id = promoCodeUse.id;
                promo.available = promoCodeUse.available;
                promo.campaignId = promoCodeUse.campaignId;
                promo.campaignName = promoCodeUse.campaignName;
                promo.createdDate = promoCodeUse.createdDate;
                promo.customerId = promoCodeUse.customerId;
                promo.customerName = promoCodeUse.customerName;
                promo.fleetId = promoFleet;
                promo.promoCode = promoCodeUse.promoCode;
                promo.promoCodeId = promoCodeUse.promoCodeId;
                promo.ticketId = promoCodeUse.ticketId;
                promo.timesUsed = promoCodeUse.timesUsed;
                promo.usedDate = promoCodeUse.usedDate;
                promo.usedValue = promoCodeUse.usedValue;
                promo.totalReceipt = promoCodeUse.totalReceipt;
                promo.currencyISO = promoCodeUse.currencyISO;
                String promoCodeUseString = gsonUpdate.toJson(promo);
                JSONObject json = (JSONObject)new JSONParser().parse(promoCodeUseString);
                json.put("createdDate", sdf.format(promoCodeUse.createdDate));
                json.put("usedDate", sdf.format(promoCodeUse.usedDate));
                json.put("promoStatus", promoStatus);
                map.put("promoCodeUse", gsonUpdate.toJson(json));
                map.put("promoCodeUseId", promoCodeUse.id);
            }

            // check and return prepaid data
            if (ticketReturn.transactionStatus.equals(KeysUtil.PAYMENT_PREPAID)) {
                PrepaidTransaction prepaidTransaction = sqlDao.getPrepaidTransactionByBookId(booking.bookId);
                if (prepaidTransaction != null) {
                    String transaction = gsonUpdate.toJson(prepaidTransaction);
                    JSONObject json = (JSONObject)new JSONParser().parse(transaction);
                    json.put("createdDate", sdf.format(prepaidTransaction.createdDate));
                    map.put("prepaidTransaction", gsonUpdate.toJson(json));
                    map.put("prepaidTransactionId", prepaidTransaction.id);
                }
            }

            //send e-receipt to pax
            try {
                //check booking status
                logger.debug(requestId + " - ticketReturn.transactionStatus: " + ticketReturn.transactionStatus);
                if(!KeysUtil.INCOMPLETE_PAYMENT.contains(ticketReturn.transactionStatus) && !booking.request.hideFare
                        && !KeysUtil.THIRD_PARTY.contains(booking.bookFrom)){
                    MailService mailService = MailService.getInstance();
                    mailService.sendMailReceiptDetail(requestId, booking.bookId, booking.psgInfo.email);
                }
            } catch(Exception e) {
                e.printStackTrace();
            }

            // deactivate all payment link if exists
            if (ticketReturn.paymentType == 5) {
                StripeUtil stripeUtil = new StripeUtil(requestId);
                String secretKey = paymentUtil.getSecretKey(booking.fleetId, true);
                List<TransactionDetails> transactionDetailsList = sqlDao.getPaymentDetailsByBookId(requestId, booking.fleetId, booking.bookId);
                for (TransactionDetails transactionDetails : transactionDetailsList) {
                    if (transactionDetails.status.equals("pending")) {
                        String paymentLink = transactionDetails.paymentLink != null ? transactionDetails.paymentLink : "";
                        String paymentLinkId = CommonUtils.getValue(paymentLink, "paymentLinkId");
                        boolean success = stripeUtil.deactivatePaymentLink(requestId, secretKey, paymentLinkId);
                        if (success) {
                            // update transaction
                            transactionDetails.status = "inactive";
                            transactionDetails.transactionId = "";
                            int updatePD = sqlDao.updatePaymentDetails(requestId, transactionDetails);
                            if (updatePD == 0) {
                                Thread.sleep(1000);
                                updatePD = sqlDao.updatePaymentDetails(requestId, transactionDetails);
                            }
                            logger.debug(requestId + " - updated transaction: " + updatePD);
                        }
                    }
                }
            }

            return map;
        } catch(Exception ex){
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            map.put("returnCode", 304);
            return map;
        }
    }

    private Map<String, String> updateNetEarning(TicketReturn ticketReturn, TicketEnt ticketEnt, Booking booking,
                                                 double qupCommission, String zoneId,
                                                 boolean isNewIncentiveReferralWithPromo, boolean paidByDriver, double merchantCommission) {
        logger.debug(requestId + " - calculate payment data");
        logger.debug(requestId + " - ticketReturn: " + gson.toJson(ticketReturn));
        logger.debug(requestId + " - ticketEnt: " + gson.toJson(ticketEnt));
        logger.debug(requestId + " - booking: " + gson.toJson(booking));
        logger.debug(requestId + " - qupCommission: " + qupCommission);
        logger.debug(requestId + " - zoneId: " + zoneId);
        logger.debug(requestId + " - isNewIncentiveReferralWithPromo: " + isNewIncentiveReferralWithPromo);
        logger.debug(requestId + " - paidByDriver: " + paidByDriver);
        logger.debug(requestId + " - merchantCommission: " + merchantCommission);
        Map<String, String> mapData = new HashMap<>();
        double totalWithoutPromo = 0.0;
       /* double total = 0.0;*/
        double comm = 0.0;
        double deductions = 0.0;
        double ridePayment = 0.0;
        double grossEarning = 0.0;
        double netEarning = 0.0;
        double promoAmount = 0.0;
        double commProvider = 0.0;
        double deductionsProvider = 0.0;
        double ridePaymentProvider = 0.0;
        double grossEarningProvider = 0.0;
        double netEarningProvider = 0.0;
        double transactionFeeProvider = 0.0;
        double commission = 15;
        double mDispatcherCommission = 0.0;
        double transactionFee = 0;
        double driverDeduction = 0;
        double fleetCommission = 0;
        double netProfit = 0;
        double currentBalance = 0.0;
        double fleetProfit = 0.0;
        boolean isMinimumTotal = false;
        double qupPreferredAmount = 0.0;
        double qupBuyPrice = 0.0;
        double qupSellPrice = 0.0;
        double supplierPayout = 0.0;
        double buyerPayout = 0.0;
        double qupEarning = 0.0;
        double supplierCommission = 0.0;
        double buyerCommission = 0.0;
        double profitFromSupplier = 0.0;
        double profitFromBuyer = 0.0;
        double penaltyPolicy = 0;
        double penaltyAmount = 0;
        double compensationPolicy = 0;
        double compensationAmount = 0;
        boolean payoutToHomeFleet = true;
        boolean payoutToProviderFleet = true;
        String supplierFailedId = "";
        String buyerFailedId = "";
        double fleetMarkup = ticketReturn.fleetMarkup;
        double sellPriceMarkup = 0.0;
        double fleetAmount = 0.0;
        double driverAmount = 0.0;
        double companyAmount = 0.0;
        double driverTax = 0.0;
        double companyTax = 0.0;
        boolean paidToDriver = false;
        boolean paidToCompany = false;
        boolean driverSettlement = false;
        double pendingSettlement = 0.0;

        String driverEarningType = "default";
        double editedDriverEarning = 0.0;
        if (booking.request.estimate != null && booking.request.estimate.fare != null) {
            driverEarningType = booking.request.estimate.fare.driverEarningType != null ? booking.request.estimate.fare.driverEarningType : "default";
            if (driverEarningType.equals("amount")) {
                editedDriverEarning = booking.request.estimate.fare.editedDriverEarning;
            } else {
                editedDriverEarning = booking.request.estimate.fare.editedDriverEarning * ticketReturn.subTotal / 100;
            }
        }
        logger.debug(requestId + " - driverEarningType: " + driverEarningType);
        logger.debug(requestId + " - editedDriverEarning: " + editedDriverEarning);
        String transactionStatus = ticketReturn.transactionStatus;
        Fleet providerFleet = mongoDao.getFleetInfor(ticketReturn.fleetId);
        double taxSetting = 0.0;
        ServiceFee sF = mongoDao.getServiceFeeByFleetIdAndZoneId(providerFleet.fleetId, zoneId);
        FleetFare fleetFare = mongoDao.getFleetFare(booking.fleetId);
        String applyType = fleetFare.applyType != null ? fleetFare.applyType : "all";
        int tollFeePayTo = 0;
        int parkingFeePayTo = 0;
        int gasFeePayTo = 0;
        if (sF != null && providerFleet.generalSetting.allowCongfigSettingForEachZone && !applyType.equals("all")) {
            taxSetting = sF.tax;
            tollFeePayTo = sF.tollFeePayTo;
            parkingFeePayTo = sF.parkingFeePayTo;
            gasFeePayTo = sF.gasFeePayTo;
        } else{
            taxSetting = fleetFare.tax;
            tollFeePayTo = fleetFare.tollFeePayTo;
            parkingFeePayTo = fleetFare.parkingFeePayTo;
            gasFeePayTo = fleetFare.gasFeePayTo;
        }
        double fleetServiceFee = 0.0;
        double fleetCommissionFromFleetServiceFee = 0.0;
        double driverCommissionFromFleetServiceFee = 0.0;
        double fleetServiceTax = 0.0;
        if (!KeysUtil.INCOMPLETE_PAYMENT.contains(transactionStatus)) {
            List<Double> fleetServiceValues = paymentUtil.getFleetServiceFees(requestId, booking, taxSetting, ticketReturn.subTotal);
            fleetServiceFee = fleetServiceValues.get(0);
            fleetCommissionFromFleetServiceFee = fleetServiceValues.get(1);
            driverCommissionFromFleetServiceFee = fleetServiceValues.get(2);
            fleetServiceTax = fleetServiceValues.get(3);
        }

        // convert amount to USD
        double exchangeRate = 1.0;
        ServerInformation si = mongoDao.getServerInformation();
        for (com.qupworld.paymentgateway.models.mongo.collections.serverInformation.Currency currency : si.currencies) {
            if (currency.iso.equals(booking.currencyISO)) {
                exchangeRate = currency.exchangeRate;
                break;
            }
        }
        if (exchangeRate == 0) exchangeRate = 1.0;
        logger.debug(requestId + " - exchangeRate: " + exchangeRate);

        String drvUsername = "";
        String ownerId = "";
        String ownerName = "";
        String commissionType = "";
        String mDispatcherCommissionType = "amount";
        String avatar = "";

        String driverId = ticketReturn.driverId != null ? ticketReturn.driverId : "";
        Account account = mongoDao.getAccount(driverId);
        if (account != null) {
            drvUsername = account.username != null ? account.username : "";
            avatar = account.avatar != null ? account.avatar : "";
        }
        String fleetName = providerFleet.name;
        String psgFleetId = ticketReturn.psgFleetId != null ? ticketReturn.psgFleetId : "";
        Fleet fleet;
        if (!psgFleetId.isEmpty()) {
            fleet = mongoDao.getFleetInfor(psgFleetId);
        } else {
            fleet = providerFleet;
        }
        String psgFleetName = fleet.name;

        String networkType = paymentUtil.getNetworkTypeFromBooking(booking, providerFleet.fleetId);
        logger.debug(requestId + " - networkType: " + networkType);
        // check short trip
        boolean shortTrip = false;

        logger.debug(requestId + " - total: " + ticketReturn.total);
        logger.debug(requestId + " - promoAmount: " + ticketReturn.promoAmount);
        double minimum = ticketReturn.minimum;
        logger.debug(requestId + " - minimum: " + ticketReturn.minimum);
        if (ticketReturn.pricingType == 0) {
            PromotionCode promotionCode = mongoDao.findByFleetIdAndPromoCode(ticketReturn.fleetId, ticketReturn.promoCode);
            if (ticketReturn.total > 0) {
                // check if total apply minimum
                //remove markup difference if exists
                double markupDifference = 0.0;
                if (booking.request != null) {
                    if (booking.request.estimate != null && booking.request.estimate.fare != null)
                        markupDifference = booking.request.estimate.fare.markupDifference;
                }
                if (ticketReturn.total - markupDifference == minimum && minimum > 0 && promotionCode != null && promotionCode.keepMinFee) {
                    // re-calculate original total
                    totalWithoutPromo = ticketReturn.subTotal + ticketReturn.techFee + ticketReturn.tax + ticketReturn.tip +
                            ticketReturn.airportSurcharge + ticketReturn.meetDriverFee + ticketReturn.partnerCommission +
                            ticketReturn.tollFee + ticketReturn.parkingFee + ticketReturn.gasFee;
                    totalWithoutPromo += fleetServiceFee;
                    isMinimumTotal = true;
                } else {
                    // normal case
                    totalWithoutPromo = ticketReturn.total + ticketReturn.promoAmount;
                }
            } else if (ticketReturn.promoAmount > 0) {
                totalWithoutPromo = ticketReturn.subTotal + ticketReturn.techFee + ticketReturn.tax + ticketReturn.tip +
                        ticketReturn.airportSurcharge + ticketReturn.meetDriverFee + ticketReturn.partnerCommission +
                        ticketReturn.tollFee + ticketReturn.parkingFee + ticketReturn.gasFee;
                totalWithoutPromo += fleetServiceFee;
            }
            logger.debug(requestId + " - totalWithoutPromo: " + totalWithoutPromo);
        } else {
            totalWithoutPromo = booking.request.estimate.fare.etaFare;
            if (ticketReturn.tollFee > 0) {
                totalWithoutPromo = booking.request.estimate.fare.etaFare + ticketReturn.tollFee;
            }
            if (ticketReturn.parkingFee > 0) totalWithoutPromo += ticketReturn.parkingFee;
            if (ticketReturn.gasFee > 0) totalWithoutPromo += ticketReturn.gasFee;
            totalWithoutPromo = CommonUtils.getRoundValue(totalWithoutPromo);
            logger.debug(requestId + " - totalWithoutPromo for Hydra: " + totalWithoutPromo);
        }
        logger.debug(requestId + " - totalWithoutPromo: " + totalWithoutPromo);
        // update actual promoAmount
        if (ticketReturn.promoAmount > totalWithoutPromo)
            promoAmount = totalWithoutPromo;
        else
            promoAmount = ticketReturn.promoAmount;
        // check if apply minimum total
        if (isMinimumTotal) {
            promoAmount = totalWithoutPromo - minimum;
        }
        logger.debug(requestId + " - actual promoAmount: " + promoAmount);
        try {
            if (!KeysUtil.INCOMPLETE_PAYMENT.contains(transactionStatus)) {
                //service type
                String jobType = booking.jobType != null ? booking.jobType : "";
                String serviceType = CommonUtils.getJobType(jobType, booking.bookFrom, booking.intercity, booking.delivery);
                if (isNewIncentiveReferralWithPromo || paidByDriver) {
                    shortTrip = estimateUtil.checkWaiveOffCommission(providerFleet, zoneId, totalWithoutPromo - promoAmount,
                            ticketReturn.rushHour, ticketReturn.currencyISO, TimezoneUtil.convertServerToGMT(booking.time.pickUpTime),
                            booking.request.pickup.timezone, serviceType, requestId);
                } else {
                    shortTrip = estimateUtil.checkWaiveOffCommission(providerFleet, zoneId, totalWithoutPromo,
                            ticketReturn.rushHour, ticketReturn.currencyISO, TimezoneUtil.convertServerToGMT(booking.time.pickUpTime),
                            booking.request.pickup.timezone, serviceType, requestId);
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - check short trip exception: " + CommonUtils.getError(ex));
        }

        // calculate Stripe fee for all captured transaction
        double stripeFee = getStripeFee(booking.bookId, booking.fleetId);
        logger.debug(requestId + " - stripeFee: " + stripeFee);
        // check if company enable Stripe connect
        boolean companyCommission = false;
        String companyId = booking.drvInfo != null && booking.drvInfo.companyId != null ? booking.drvInfo.companyId : "";
        if (!companyId.isEmpty()) {
            Company company = mongoDao.getCompanyById(booking.drvInfo.companyId);
            String stripeConnectStatus = company.connectStatus != null ? company.connectStatus : "";
            companyCommission = providerFleet.generalSetting.companyCommission && providerFleet.drvPayout.gateway.equals("StripeConnect") && stripeConnectStatus.equals("activated");
            // check if company was activated Stripe connect
            String companyCommissionType = "";
            double companyCommissionValue = 0.0;
            if (companyCommission) {
                if (driverEarningType.equals("default")) {
                    if (account != null && account.driverInfo != null && companyCommission) {
                        companyCommissionType = account.driverInfo.commissionCompanyType != null ? account.driverInfo.commissionCompanyType : "default";
                        if (!companyCommissionType.equals("default")) {
                            String jobType = booking.jobType != null ? booking.jobType : "";
                            String serviceType = CommonUtils.getJobType(jobType, booking.bookFrom, booking.intercity, booking.delivery);
                            companyCommissionValue = account.driverInfo.commissionCompanyValue.stream().filter(c -> c.serviceType.equals(serviceType)).mapToDouble(c -> c.value).sum();
                        }
                        if (companyCommissionType.equals("default"))
                            companyCommissionValue = company.commissionValue;
                    }
                    logger.debug(requestId + " - companyCommission type: " + companyCommissionType + " --- companyCommissionValue: " + companyCommissionValue);
                    if (companyCommissionValue <= 0)
                        companyCommission = false;
                }
            } else {
                // company was NOT activated Stripe connect - check if setting default commission
                companyCommissionType = "default";
                companyCommissionValue = company.commissionValue;
                companyCommission = companyCommissionValue > 0;
                logger.debug(requestId + " - companyCommission type: " + companyCommissionType + " --- companyCommissionValue: " + companyCommissionValue);
            }

        }
        logger.debug(requestId + " - enable company commission: " + companyCommission);
        // check Stripe connect then do transfer
        logger.debug(requestId + " - auto payout => calculate amount with separateTax = fleetTax");
        List<Double> transferData = getTransferAmount(providerFleet, booking, ticketReturn, shortTrip, companyCommission, "fleetTax", driverEarningType, editedDriverEarning);
        fleetAmount = transferData.get(0);
        driverAmount = transferData.get(1);
        companyAmount = transferData.get(2);
        double fleetTax = transferData.get(3);
        driverTax = transferData.get(4);
        companyTax = transferData.get(5);
        logger.debug(requestId + " - fleetAmount: " + fleetAmount);
        logger.debug(requestId + " - driverAmount: " + driverAmount);
        logger.debug(requestId + " - companyAmount: " + companyAmount);
        logger.debug(requestId + " - fleetTax: " + fleetTax);
        logger.debug(requestId + " - driverTax: " + driverTax);
        logger.debug(requestId + " - companyTax: " + companyTax);
        String separateTax = fleet.fleetDeduction.separateTax != null ? fleet.fleetDeduction.separateTax : "allTax";
        PricingPlan pricingPlanProvider = mongoDao.getPricingPlan(ticketReturn.fleetId);
        if (promoAmount > 0) {
            if (account != null && !isNewIncentiveReferralWithPromo && !paidByDriver) {
                if (pricingPlanProvider.promoPayToWallet == 1) {
                    //logger.debug(requestId + " - promo pay to credit wallet => subtract promo value from driver amount");
                    driverAmount = driverAmount - promoAmount;
                    logger.debug(requestId + " - driverAmount after subtract promo value: " + driverAmount);
                }
            }
            if (companyCommission) {
                //logger.debug(requestId + " - enable company commission => company will pay for promo amount");
                companyAmount = companyAmount - promoAmount;
                logger.debug(requestId + " - companyAmount after subtract promo value: " + companyAmount);
            }
        }

        paidToDriver = doTransferToStripeConnect(booking, driverAmount, "driver");
        logger.debug(requestId + " - paidToDriver: " + paidToDriver);
        if (paidToDriver) {
            separateTax = "fleetTax";
        } else {
            logger.debug(requestId + " - paid to driver has failed => manual payout => calculate amount with separateTax = " + separateTax);
            // paid to driver has failed => calculate amount base on tax setting
            transferData = getTransferAmount(providerFleet, booking, ticketReturn, shortTrip, companyCommission, separateTax, driverEarningType, editedDriverEarning);
            fleetAmount = transferData.get(0);
            driverAmount = transferData.get(1);
            companyAmount = transferData.get(2);
            fleetTax = transferData.get(3);
            driverTax = transferData.get(4);
            companyTax = transferData.get(5);
            logger.debug(requestId + " - fleetAmount: " + fleetAmount);
            logger.debug(requestId + " - driverAmount: " + driverAmount);
            logger.debug(requestId + " - companyAmount: " + companyAmount);
            logger.debug(requestId + " - fleetTax: " + fleetTax);
            logger.debug(requestId + " - driverTax: " + driverTax);
            logger.debug(requestId + " - companyTax: " + companyTax);
        }
        if (providerFleet.driverSettlement && !paidToDriver) {
            List<String> arrGateway = Arrays.asList(KeysUtil.STRIPE, KeysUtil.JETPAY);
            ConfigGateway configGateway = paymentUtil.getConfigGateway(providerFleet, "", booking.request.pickup.geo);
            if (configGateway != null && arrGateway.contains(configGateway.gateway))
                driverSettlement = true;
        }
        if (stripeFee > 0 && companyCommission) {
            // Stripe will get Stripe fee from fleet balance => company need to pay Stripe fee for fleet
            companyAmount = companyAmount - stripeFee;
            logger.debug(requestId + " - companyAmount after subtract Stripe fee: " + companyAmount);
        }
        if (!paidToDriver) {
            // paid to driver has failed => driver will receive their earning to their wallet
            // company will pay back the earning to fleet
            if (companyCommission) {
                logger.debug(requestId + " - totalCharged = " + ticketReturn.totalCharged);
                logger.debug(requestId + " - fleetAmount = " + fleetAmount);
                logger.debug(requestId + " - stripeFee = " + stripeFee);
                logger.debug(requestId + " - driverAmount = " + driverAmount);
                pendingSettlement = ticketReturn.totalCharged - (fleetAmount + stripeFee) - driverAmount;
                logger.debug(requestId + " - pendingSettlement = ticketReturn.totalCharged - (fleetAmount + stripeFee) - driverAmount = " + ticketReturn.totalCharged + " - (" + fleetAmount + " + " + stripeFee + ") - " + driverAmount + " = " + pendingSettlement);
            }
        } else {
            if (companyCommission && companyAmount > 0) {
                paidToCompany = doTransferToStripeConnect(booking, companyAmount, "company");
                logger.debug(requestId + " - paidToCompany: " + paidToCompany);
                if (!paidToCompany) {
                    pendingSettlement = companyAmount;
                }
            }
        }

        // check payment status before update

        boolean completedTicketWithMoney = false;
        if (!KeysUtil.INCOMPLETE_PAYMENT.contains(transactionStatus) ||
                (booking.intercity && ticketReturn.totalCharged > 0.0)
                ) {
            completedTicketWithMoney = true;
        } else if (transactionStatus.equals(KeysUtil.PAYMENT_NOSHOW)) {
            // driver took time to the pickup but did not show the pax, so he should receive the transfer
            completedTicketWithMoney = true;
        } else if (transactionStatus.equals(KeysUtil.PAYMENT_CANCELED) && ticketReturn.totalCharged > 0) {
            completedTicketWithMoney = true;
        } else if (transactionStatus.equals(KeysUtil.PAYMENT_CANCELED) && ticketReturn.totalCharged == 0 && ticketReturn.paymentType == 5) {
            completedTicketWithMoney = true;
        } else if (transactionStatus.equals(KeysUtil.PAYMENT_CANCELED) && ticketReturn.pricingType == 1 && ticketReturn.chargeCancelPolicy) {
            completedTicketWithMoney = true;
        }
        logger.debug(requestId + " - completedTicketWithMoney: " + completedTicketWithMoney);
        logger.debug(requestId + " - transactionStatus: " + transactionStatus);
        if (completedTicketWithMoney) {
            // get fare and earning from setting for cancel/ no show booking
            if ((transactionStatus.equals(KeysUtil.PAYMENT_CANCELED) || transactionStatus.equals(KeysUtil.PAYMENT_NOSHOW)) && !booking.intercity) {
                if (ticketReturn.pricingType == 0) {
                    String getCancelData = estimateUtil.getCancelAmount(booking, providerFleet, transactionStatus, "", requestId);
                    JSONObject resultObject = paymentUtil.toObjectResponse(getCancelData);
                    double cancelAmount = 0.0;
                    double cancelEarning = 0.0;
                    double taxFee = 0.0;
                    double techFee = 0.0;
                    if (resultObject != null && resultObject.get("returnCode") != null) {
                        if (Integer.parseInt(resultObject.get("returnCode").toString()) == 200) {
                            JSONObject objResponse = (JSONObject) resultObject.get("response");
                            cancelAmount = Double.parseDouble(objResponse.get("cancelAmount").toString());
                            cancelEarning = Double.parseDouble(objResponse.get("cancelEarning").toString());
                            taxFee = Double.parseDouble(objResponse.get("taxFee").toString());
                            techFee = Double.parseDouble(objResponse.get("techFee").toString());
                            fleetCommission = Double.parseDouble(objResponse.get("fleetCommission").toString());
                        }
                    }

                    // deduct driver tax if already paid to driver or setting separate tax
                    double tax = taxFee;
                    if (separateTax.equals("fleetTax")) {
                        tax = CommonUtils.getRoundValue(tax - driverTax);
                    }
                    logger.debug(requestId + " - driverTax = " + driverTax);
                    logger.debug(requestId + " - tax = " + tax);
                    netEarning = CommonUtils.getRoundValue(cancelEarning + driverTax);
                    grossEarning = CommonUtils.getRoundValue(cancelEarning + driverTax);
                    ridePayment = cancelEarning;
                    if (companyCommission) {
                        /*if (paidToDriver) {
                            netEarning = driverAmount;
                        } else {
                            netEarning = netEarning - companyAmount - stripeFee - promoAmount;
                            driverAmount = netEarning;
                            logger.debug(requestId + " - update driverAmount = netEarning = " + driverAmount);
                        }*/
                        // driver portion is from the transfer amount calculator, not related to cancel policy amount
                        netEarning = driverAmount;
                        logger.debug(requestId + " - netEarning = " + netEarning);
                        grossEarning = netEarning;

                        Map<String, Object> mapCommission = estimateUtil.getFleetCommission(requestId, providerFleet, account, booking, zoneId);
                        commissionType = mapCommission.get("commissionType").toString();
                        commission = Double.parseDouble(mapCommission.get("commission").toString());
                        comm = ticketReturn.subTotal * (commission * 0.01);
                        if (providerFleet.fleetDeduction.commissionBasedOnTotalFare) {
                            comm = ticketReturn.total * (commission * 0.01);
                        }
                        if (commissionType.equalsIgnoreCase("amount"))
                            comm = commission;
                        logger.debug(requestId + " - fleet enable company commission => fleet just receive commission from sub-total = " + comm);
                        fleetProfit = comm;

                        // fleet enable company commission - transfer all fee to company
                        deductions = comm + fleetTax;
                    } else {
                        deductions = CommonUtils.getRoundValue(fleetCommission + techFee + tax);
                        fleetProfit = Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(fleetCommission + techFee));
                        comm = fleetCommission;
                    }
                    logger.debug(requestId + " - netEarning = " + netEarning);
                    logger.debug(requestId + " - grossEarning = " + grossEarning);
                    logger.debug(requestId + " - ridePayment = " + ridePayment);
                    logger.debug(requestId + " - fleetCommission: " + fleetCommission);
                    logger.debug(requestId + " - deductions: " + deductions);
                    logger.debug(requestId + " - fleetProfit: " + fleetProfit);
                    logger.debug(requestId + " - comm: " + comm);

                    totalWithoutPromo = cancelAmount;

                    // add earning to driver cash wallet
                    if (account != null && pricingPlanProvider.driverCashWallet != null && pricingPlanProvider.driverCashWallet.enable) {
                        try {
                            WalletUtil walletUtil = new WalletUtil(requestId);
                            currentBalance = walletUtil.getCurrentBalance(account.userId, "cash", ticketReturn.currencyISO);
                            // update wallet
                            netEarning = DF.parse(DF.format(netEarning)).doubleValue();
                            logger.debug(requestId + " - paidToDriver: " + paidToDriver);
                            if (!paidToDriver) {
                                WalletTransfer walletTransfer = new WalletTransfer();
                                walletTransfer.fleetId = pricingPlanProvider.fleetId;
                                walletTransfer.driverId = account.userId;
                                walletTransfer.amount = netEarning;
                                walletTransfer.transferType = CommonArrayUtils.DRIVER_CASH_WALLET_TYPE[2];
                                walletTransfer.transactionId = ticketReturn.bookId;
                                walletTransfer.currencyISO = ticketReturn.currencyISO;
                                UpdateDriverCashBalance updateCash = new UpdateDriverCashBalance();
                                updateCash.requestId = requestId;
                                updateCash.walletTransfer = walletTransfer;
                                updateCash.mongoDao = mongoDao;
                                updateCash.redisDao = redisDao;
                                updateCash.isDeposit = true;
                                updateCash.pendingBalance = -1;
                                double newBalance = updateCash.doUpdate();
                                // update current balance if there is any update from another process
                                currentBalance = CommonUtils.getRoundValue(newBalance - walletTransfer.amount);
                            }
                        } catch (Exception ex) {
                            logger.debug(requestId + " - add earning to cash wallet error: " + CommonUtils.getError(ex));
                        }
                    }
                } else {
                    AffiliateSetting affiliateSetting = mongoDao.getAffiliateSetting();
                    com.qupworld.paymentgateway.models.mongo.collections.AffiliateSetting.CancellationPolicy cancellationPolicy = affiliateSetting != null && affiliateSetting.cancellationPolicy != null ? affiliateSetting.cancellationPolicy : null;
                    if (transactionStatus.equals(KeysUtil.PAYMENT_NOSHOW)) {
                        logger.debug(requestId + " - no show hydra booking");
                        penaltyPolicy = cancellationPolicy.noShow.penalty;
                        compensationPolicy = cancellationPolicy.noShow.compensationSupplier;
                        penaltyAmount = booking.request.estimate.fare.qupSellPrice * penaltyPolicy / 100;
                        logger.debug(requestId + " - qupSellPrice: " + booking.request.estimate.fare.qupSellPrice + " - penaltyPolicy: " + penaltyPolicy + " ==> penaltyAmount = " + penaltyAmount);
                        compensationAmount = booking.request.estimate.fare.etaFare * compensationPolicy / 100;
                        logger.debug(requestId + " - compensationPolicy: " + penaltyPolicy + " ==> compensationAmount = " + compensationAmount);

                        supplierPayout = compensationAmount;
                        logger.debug(requestId + " - supplierPayout = compensationAmount = " + supplierPayout);
                        String fleetId = booking.isFarmOut ? booking.request.psgFleetId : booking.fleetId;
                        exchangeRate = paymentUtil.getExchangeRate(requestId, fleetId, ticketReturn.gatewayId, booking.isFarmOut, booking.currencyISO);
                        logger.debug(requestId + " - gatewayId: " + ticketReturn.gatewayId + " -> exchangeRate: " + exchangeRate);
                        double convertSupplierPayout = CommonUtils.getRoundValue(supplierPayout / exchangeRate);
                        logger.debug(requestId + " - convertSupplierPayout = " + convertSupplierPayout);
                        if (convertSupplierPayout > 0.5) {
                            HydraUtil hydraUtil = new HydraUtil();
                            String paySupplier = hydraUtil.payoutToFleet(requestId, ticketReturn.fleetId, ticketReturn.gatewayId, booking, convertSupplierPayout);
                            logger.debug(requestId + " - payout to supplier : " + paySupplier);
                            if (CommonUtils.getReturnCode(paySupplier) != 200) {
                                payoutToProviderFleet = false;
                                supplierFailedId = CommonUtils.getValue(paySupplier, "requestId");
                            }
                        } else {
                            payoutToProviderFleet = false;
                        }
                        qupBuyPrice = compensationAmount;
                        logger.debug(requestId + " - qupBuyPrice = compensationPolicy = " + qupBuyPrice);
                        qupSellPrice = penaltyAmount;
                        logger.debug(requestId + " - qupSellPrice = penaltyAmount = " + qupSellPrice);
                        qupEarning = CommonUtils.getRoundValue(qupSellPrice - qupBuyPrice);
                        logger.debug(requestId + " - qupEarning = qupSellPrice - qupBuyPrice = " + qupEarning);
                    } else {
                        String canceller = booking.cancelInfo != null && booking.cancelInfo.canceller != null ? booking.cancelInfo.canceller : "";
                        logger.debug(requestId + " - cancel hydra booking by " + canceller);
                        switch (canceller) {
                            case "CC":
                            case "timeout": {
                                if (booking.reservation && ticketReturn.chargeCancelPolicy) {
                                    penaltyPolicy = cancellationPolicy.reservation.penaltySupplier;
                                    penaltyAmount = booking.request.estimate.fare.qupSellPrice * penaltyPolicy / 100;
                                    logger.debug(requestId + " - penaltyPolicy: " + penaltyPolicy + " ==> penaltyAmount = " + penaltyAmount);

                                    profitFromSupplier = penaltyAmount;
                                    logger.debug(requestId + " - profitFromSupplier = penaltyAmount = " + profitFromSupplier);

                                    qupEarning = penaltyAmount;
                                    logger.debug(requestId + " - qupEarning = penaltyAmount = " + qupEarning);
                                }
                            }
                            break;
                            case "passenger": {
                                if (ticketReturn.chargeCancelPolicy) {
                                    if (booking.reservation != null && booking.reservation) {
                                        penaltyPolicy = cancellationPolicy.reservation.penaltyCustomer;
                                        compensationPolicy = cancellationPolicy.reservation.compensationSupplier;
                                    } else {
                                        penaltyPolicy = cancellationPolicy.onDemand.penalty;
                                        compensationPolicy = cancellationPolicy.onDemand.compensationSupplier;
                                    }
                                    penaltyAmount = booking.request.estimate.fare.qupSellPrice * penaltyPolicy / 100;
                                    logger.debug(requestId + " - qupSellPrice: " + booking.request.estimate.fare.qupSellPrice + " - penaltyPolicy: " + penaltyPolicy + " ==> penaltyAmount = " + penaltyAmount);
                                    compensationAmount = booking.request.estimate.fare.etaFare * compensationPolicy / 100;
                                    logger.debug(requestId + " - compensationPolicy: " + penaltyPolicy + " ==> compensationAmount = " + compensationAmount);
                                    supplierPayout = compensationAmount;
                                    logger.debug(requestId + " - supplierPayout = compensationAmount = " + supplierPayout);
                                    String fleetId = booking.isFarmOut ? booking.request.psgFleetId : booking.fleetId;
                                    exchangeRate = paymentUtil.getExchangeRate(requestId, fleetId, ticketReturn.gatewayId, booking.isFarmOut, booking.currencyISO);
                                    logger.debug(requestId + " - gatewayId: " + ticketReturn.gatewayId + " -> exchangeRate: " + exchangeRate);
                                    double convertSupplierPayout = CommonUtils.getRoundValue(supplierPayout / exchangeRate);
                                    logger.debug(requestId + " - convertSupplierPayout = " + convertSupplierPayout);
                                    if (convertSupplierPayout > 0.5) {
                                        HydraUtil hydraUtil = new HydraUtil();
                                        String paySupplier = hydraUtil.payoutToFleet(requestId, ticketReturn.fleetId, ticketReturn.gatewayId, booking, convertSupplierPayout);
                                        logger.debug(requestId + " - payout to supplier : " + paySupplier);
                                        if (CommonUtils.getReturnCode(paySupplier) != 200) {
                                            payoutToProviderFleet = false;
                                            supplierFailedId = CommonUtils.getValue(paySupplier, "requestId");
                                        }
                                    } else {
                                        payoutToProviderFleet = false;
                                    }

                                    qupBuyPrice = compensationAmount;
                                    logger.debug(requestId + " - qupBuyPrice = compensationPolicy = " + qupBuyPrice);
                                    qupSellPrice = penaltyAmount;
                                    logger.debug(requestId + " - qupSellPrice = penaltyAmount = " + qupSellPrice);
                                    qupEarning = CommonUtils.getRoundValue(qupSellPrice - qupBuyPrice);
                                    logger.debug(requestId + " - qupEarning = qupSellPrice - qupBuyPrice = " + qupEarning);
                                }
                            }
                        }
                    }

                }
            } else {
                // calculate for normal case
                // check if booking complete without service
                logger.debug(requestId + " - completedWithoutService = " + ticketReturn.completedWithoutService);
                if (ticketReturn.completedWithoutService) {
                    // fleet receive all money, not related to driver
                    netEarning = 0.0;
                    grossEarning = 0.0;
                    ridePayment = 0.0;
                    deductions = 0.0;
                    comm = 0.0;
                    // add markup price
                    double markupDifference = (booking.request.estimate != null && booking.request.estimate.fare != null) ? booking.request.estimate.fare.markupDifference : 0.0;
                    fleetProfit += markupDifference;
                    logger.debug(requestId + " - fleetProfit after add markupDifference: " + fleetProfit);
                    // add profit from fleet service
                    fleetProfit += fleetCommissionFromFleetServiceFee;
                    logger.debug(requestId + " - fleetProfit after add fleetCommissionFromFleetServiceFee: " + fleetProfit);
                    // add merchant commission
                    fleetProfit += merchantCommission;
                    logger.debug(requestId + " - fleetProfit after add merchantCommission: " + fleetProfit);
                } else {
                    try {
                        if (!booking.dispatch3rd && ticketReturn.pricingType == 0) {
                            //add mDispatcherCommission if bookFrom mDispatcher
                            if (KeysUtil.mDispatcher.equalsIgnoreCase(ticketReturn.bookFrom)) {
                                logger.debug(requestId + " - ticketReturn.partnerCommission: " + ticketReturn.partnerCommission);
                                String mDispatcherId = ticketReturn.mDispatcherId != null ? ticketReturn.mDispatcherId : "";
                                if (!mDispatcherId.isEmpty()) {
                                    Account mDispatcher = mongoDao.getAccount(mDispatcherId);
                                    if (mDispatcher != null && mDispatcher.isActive) {
                                        // calculate commission in case incomplete payment
                                        if (KeysUtil.INCOMPLETE_PAYMENT.contains(ticketReturn.transactionStatus)) {
                                            mDispatcherCommissionType = mDispatcher.mDispatcherInfo.commissionType != null ? mDispatcher.mDispatcherInfo.commissionType.toLowerCase() : "amount";
                                            if (mDispatcher.mDispatcherInfo.commissionByCurrencies != null && !mDispatcher.mDispatcherInfo.commissionByCurrencies.isEmpty()) {
                                                // if type = percent, there is only ONE object on commissionByCurrencies
                                                if (mDispatcherCommissionType.equalsIgnoreCase("percent")) {
                                                    AmountByCurrency amountByCurrency = mDispatcher.mDispatcherInfo.commissionByCurrencies.get(0);
                                                    mDispatcherCommission = ticketReturn.subTotal * (amountByCurrency.commissionValue * 0.01);
                                                } else {
                                                    for (AmountByCurrency amountByCurrency : mDispatcher.mDispatcherInfo.commissionByCurrencies) {
                                                        if (amountByCurrency.currencyISO.equalsIgnoreCase(ticketReturn.currencyISO)) {
                                                            mDispatcherCommission = amountByCurrency.commissionValue;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            mDispatcherCommission = ticketReturn.partnerCommission;
                                        }
                                    }
                                }
                            } else {
                                String corporateId = ticketReturn.corporateId != null ? ticketReturn.corporateId : "";
                                Corporate corporate = mongoDao.getCorporate(corporateId);
                                boolean applyCorpCommission = estimateUtil.applyCorpCommission(requestId, corporate, ticketReturn.bookFrom);
                                logger.debug(requestId + " - applyCorpCommission = " + applyCorpCommission);
                                if (applyCorpCommission && !booking.request.estimate.isFareEdited) {
                                    // check ticket other noShow and canceled
                                    if (corporate != null && corporate.isActive && !KeysUtil.INCOMPLETE_PAYMENT.contains(ticketReturn.transactionStatus)) {
                                        if (corporate.commission != null && corporate.commission.active) {
                                            mDispatcherCommissionType = corporate.commission.type != null ? corporate.commission.type.toLowerCase() : "";
                                            if (corporate.commission.commissionByCurrencies != null && !corporate.commission.commissionByCurrencies.isEmpty()) {
                                                // if type = percent, there is only ONE object on commissionByCurrencies
                                                if (mDispatcherCommissionType.equalsIgnoreCase("percent")) {
                                                    AmountByCurrency amountByCurrency = corporate.commission.commissionByCurrencies.get(0);
                                                    mDispatcherCommission = ticketReturn.subTotal * (amountByCurrency.commissionValue * 0.01);
                                                } else {
                                                    for (AmountByCurrency amountByCurrency : corporate.commission.commissionByCurrencies) {
                                                        if (amountByCurrency.currencyISO.equalsIgnoreCase(ticketReturn.currencyISO)) {
                                                            mDispatcherCommission = amountByCurrency.commissionValue;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    mDispatcherCommission = ticketReturn.partnerCommission;
                                }
                            }
                        } else {
                            mDispatcherCommission = ticketReturn.partnerCommission;
                        }
                        if (account != null && account.driverInfo != null) {
                            Map<String, Object> mapCommission = estimateUtil.getFleetCommission(requestId, providerFleet, account, booking, zoneId);
                            commissionType = mapCommission.get("commissionType").toString();
                            commission = Double.parseDouble(mapCommission.get("commission").toString());
                            double insurance = Double.parseDouble(mapCommission.get("insurance").toString());
                            logger.debug(requestId + " - insurance = " + insurance);
                            logger.debug(requestId + " - generalSetting.driverInsurance = " + providerFleet.generalSetting.driverInsurance);
                            if (insurance > 0 && providerFleet.generalSetting.driverInsurance) {
                                logger.debug(requestId + " - insurance = " + insurance + " --- deduct from driver credit wallet");
                                UpdateDriverCreditBalance updateBalance = new UpdateDriverCreditBalance();
                                updateBalance.type = CommonArrayUtils.DRIVER_WALLET_TYPE[14];
                                updateBalance.fleetId = providerFleet.fleetId;
                                updateBalance.driverId = account.userId;
                                updateBalance.amount = -insurance; // deduct fund from wallet => store negative value on history
                                updateBalance.currencyISO = booking.currencyISO;
                                updateBalance.mongoDao = mongoDao;
                                updateBalance.sqlDao = sqlDao;
                                updateBalance.reason = "";
                                updateBalance.additionalData.put("bookId", booking.bookId);
                                updateBalance.requestId = requestId;
                                String result = updateBalance.doUpdate();
                                logger.debug(requestId + " - result = " + result);
                            }
                        }
                        logger.debug(requestId + " - commission type: " + commissionType + " --- value: " + commission);
                        logger.debug(requestId + " - booking fee type: " + mDispatcherCommissionType + " --- value: " + mDispatcherCommission);

                        // calculate transactionFee

                        List<String> credits = Arrays.asList(KeysUtil.PAYMENT_CARD, KeysUtil.PAYMENT_MDISPATCHER, KeysUtil.PAYMENT_CORPORATE,
                                KeysUtil.PAYMENT_PREPAID, KeysUtil.PAYMENT_APPLEPAY, KeysUtil.PAYMENT_GOOGLEPAY);
                        if (providerFleet.fleetDeduction.transactionFee.enable) {
                            List<FeeByCurrency> feeByCurrencies = providerFleet.fleetDeduction.transactionFee.feeByCurrencies;
                            for (FeeByCurrency feeByCurrency : feeByCurrencies) {
                                if (feeByCurrency.currencyISO.equalsIgnoreCase(ticketReturn.currencyISO)) {
                                    if (credits.contains(transactionStatus)
                                            || (booking.intercity && booking.request.paymentType == 1) //credit was charged for intercity booking
                                            ) {
                                        if (providerFleet.fleetDeduction != null && providerFleet.fleetDeduction.transactionFee != null) {
                                            transactionFee = feeByCurrency.creditCardAmount +
                                                    feeByCurrency.creditCardPercent * ticketReturn.total / 100;
                                        }
                                    } else if (transactionStatus.equalsIgnoreCase(KeysUtil.PAYMENT_DIRECTBILLING)) {
                                        if (providerFleet.fleetDeduction != null && providerFleet.fleetDeduction.transactionFee != null) {
                                            transactionFee = feeByCurrency.directBillingAmount +
                                                    feeByCurrency.directBillingPercent * ticketReturn.total / 100;
                                        }
                                    } else if (transactionStatus.equalsIgnoreCase(KeysUtil.PAYMENT_FLEETCARD)) {
                                        if (providerFleet.fleetDeduction != null && providerFleet.fleetDeduction.transactionFee != null) {
                                            transactionFee = feeByCurrency.externalCardAmount +
                                                    feeByCurrency.externalCardPercent * ticketReturn.total / 100;
                                        }
                                    }
                                }
                            }
                        }

                        /*// get profit from pricing plan
                        PricingPlan pricingPlan = mongoDao.getPricingPlan(fleet.fleetId);
                        if (pricingPlan.affiliate != null) {
                            fleetCommission = qupCommission * (pricingPlan.affiliate.homeFleetCommission * 0.01);
                            netProfit = qupCommission * ((100 - pricingPlan.affiliate.homeFleetCommission) * 0.01) - mDispatcherCommission;
                        }
*/

                        //Home fleet
                        if (driverEarningType.equals("default") || companyCommission) {
                            if (isNewIncentiveReferralWithPromo || paidByDriver) {
                                comm = (ticketReturn.subTotal - promoAmount) * (commission * 0.01);
                                if (commissionType.equalsIgnoreCase("amount"))
                                    comm = commission;
                            } else {
                                comm = ticketReturn.subTotal * (commission * 0.01);
                                if (providerFleet.fleetDeduction.commissionBasedOnTotalFare) {
                                    comm = ticketReturn.total * (commission * 0.01);
                                }
                                if (commissionType.equalsIgnoreCase("amount"))
                                    comm = commission;
                            }

                            DecimalFormat df = new DecimalFormat("#.##");
                            comm = Double.valueOf(df.format(comm));

                            if (shortTrip)
                                comm = 0;
                            logger.debug(requestId + " - comm = " + comm);
                        } else {
                            comm = ticketReturn.subTotal - editedDriverEarning;
                            logger.debug(requestId + " - comm = subTotal - editedDriverEarning = " + comm);
                        }

                        double partnerCommission = 0.0;
                        String corporateId = ticketReturn.corporateId != null ? ticketReturn.corporateId : "";
                        Corporate corporate = null;
                        if (!corporateId.isEmpty()) {
                            corporate = mongoDao.getCorporate(corporateId);
                        }
                        boolean applyCorpCommission = estimateUtil.applyCorpCommission(requestId, corporate, ticketReturn.bookFrom);
                        logger.debug(requestId + " - applyCorpCommission = " + applyCorpCommission);
                        if (KeysUtil.mDispatcher.equalsIgnoreCase(ticketReturn.bookFrom) || applyCorpCommission) {
                            partnerCommission = ticketReturn.mDispatcherCommission;
                        }
                        // deduct driver tax if already paid to driver or setting separate tax
                        double tax = ticketReturn.tax;
                        if (separateTax.equals("fleetTax")) {
                            tax = CommonUtils.getRoundValue(tax - driverTax);
                        }

                        logger.debug(requestId + " - driverTax = " + driverTax);
                        logger.debug(requestId + " - tax = " + tax);
                        logger.debug(requestId + " - deductions1 = " + deductions);
                        if (companyCommission) {
                            deductions = comm + fleetTax;
                        } else {
                            deductions = calculateDeduction(providerFleet, fleetFare, comm, ticketReturn.techFee, tax, transactionFee, partnerCommission,
                                    ticketReturn.airportSurcharge, ticketReturn.meetDriverFee, ticketReturn.tollFee, ticketReturn.parkingFee, ticketReturn.gasFee,
                                    ticketReturn.creditTransactionFee, fleetCommissionFromFleetServiceFee, fleetServiceTax);
                            logger.debug(requestId + " - deductions1 = " + deductions);
                            if (deductions > totalWithoutPromo) {
                                logger.debug(requestId + " - WARNING DEDUCTION INCORRECT !!!");
                                Thread.sleep(1000);
                                deductions = calculateDeduction(providerFleet, fleetFare, comm, ticketReturn.techFee, tax, transactionFee, partnerCommission,
                                        ticketReturn.airportSurcharge, ticketReturn.meetDriverFee, ticketReturn.tollFee, ticketReturn.parkingFee, ticketReturn.gasFee,
                                        ticketReturn.creditTransactionFee, fleetCommissionFromFleetServiceFee, fleetServiceTax);
                                logger.debug(requestId + " - deductions2 = " + deductions);
                            }
                        }
                        // calculator ride payment and gross earning
                        ridePayment = ticketReturn.subTotal - comm;
                        if (isNewIncentiveReferralWithPromo || paidByDriver) {
                            ridePayment = ticketReturn.subTotal - promoAmount - comm;
                        }
                        if (companyCommission) {
                            String companyCommissionType = "";
                            double companyCommissionValue = 0.0;
                            if (account != null && account.driverInfo != null) {
                                companyCommissionType = account.driverInfo.commissionCompanyType != null ? account.driverInfo.commissionCompanyType : "default";
                                if (!companyCommissionType.equals("default")) {
                                    String jobType = booking.jobType != null ? booking.jobType : "";
                                    String serviceType = CommonUtils.getJobType(jobType, booking.bookFrom, booking.intercity, booking.delivery);
                                    companyCommissionValue = account.driverInfo.commissionCompanyValue.stream().filter(c -> c.serviceType.equals(serviceType)).mapToDouble(c -> c.value).sum();
                                }
                            }
                            if (!companyId.isEmpty()) {
                                Company company = mongoDao.getCompanyById(companyId);
                                if (company != null) {
                                    if (companyCommissionType.equals("default"))
                                        companyCommissionValue = company.commissionValue;
                                }
                            }
                            logger.debug(requestId + " - companyCommission type: " + companyCommissionType + " --- companyCommissionValue: " + companyCommissionValue);
                            double companyPortion = (ticketReturn.subTotal - comm) * companyCommissionValue / 100;
                            double oldRidePayment = ridePayment;
                            ridePayment -= companyPortion;
                            logger.debug(requestId + " - ridePayment = ridePayment - companyPortion = " + oldRidePayment + " - " + companyPortion + " = " + ridePayment);
                        }

                        // ff setting airport fee, meet driver, toll fee, parking fee, gas fee are pay to driver then get these values to calculate gross earning
                        double driverTollFee = tollFeePayTo == 0 ? ticketReturn.tollFee : 0.0;
                        double driverParkingFee = parkingFeePayTo == 0 ? ticketReturn.parkingFee : 0.0;
                        double driverGasFee = gasFeePayTo == 0 ? ticketReturn.gasFee : 0.0;
                        double driverAirportFee = 0.0;
                        if (fleetFare != null && fleetFare.airport != null && fleetFare.airport.payTo == 0) {
                            driverAirportFee = ticketReturn.airportSurcharge;
                        }
                        double driverMeetGreet = 0.0;
                        if (fleetFare != null && fleetFare.meetDriver != null && fleetFare.meetDriver.payTo == 0) {
                            driverMeetGreet = ticketReturn.meetDriverFee;
                        }
                        grossEarning = ridePayment + ticketReturn.tip + driverAirportFee + driverMeetGreet + driverTollFee + driverParkingFee + driverGasFee +
                                driverCommissionFromFleetServiceFee + driverTax;
                        // update ride payment and grossEarning of booking was customized driver earning
                        if (!driverEarningType.equals("default")) {
                            logger.debug(requestId + " - driverEarningType =  " + driverEarningType + " => update ridePayment and grossEarning and deductions");
                            ridePayment = editedDriverEarning;
                            grossEarning = editedDriverEarning + ticketReturn.tip + driverAirportFee + driverMeetGreet + driverTollFee + driverParkingFee + driverGasFee +
                                    driverCommissionFromFleetServiceFee + driverTax;
                            logger.debug(requestId + " - transactionStatus: " + ticketReturn.transactionStatus);
                            if (ticketReturn.transactionStatus.equals(KeysUtil.PAYMENT_CASH)) {
                                logger.debug(requestId + " - total: " + ticketReturn.total);
                                deductions = ticketReturn.total - grossEarning;
                            } else {
                                logger.debug(requestId + " - totalWithoutPromo: " + totalWithoutPromo);
                                deductions = totalWithoutPromo - grossEarning;
                            }

                            logger.debug(requestId + " - ridePayment: " + ridePayment);
                            logger.debug(requestId + " - grossEarning: " + grossEarning);
                            logger.debug(requestId + " - deductions: " + deductions);
                        }

                        WalletUtil walletUtil = new WalletUtil(requestId);
                        // check if partial payment wallet + cash
                        if (ticketReturn.splitPayment && ticketReturn.transactionStatus.equals(KeysUtil.PAYMENT_CASH)) {
                            if (pricingPlanProvider.promoPayToWallet == 1) {
                                logger.debug(requestId + " == calculate payment data for partial payment");
                                logger.debug(requestId + " - total: " + ticketReturn.total);
                                logger.debug(requestId + " - deductions: " + deductions);
                                logger.debug(requestId + " - pay by wallet: " + ticketReturn.paidByWallet);
                                logger.debug(requestId + " - pay by cash: " + ticketReturn.paidByOtherMethod);

                                netEarning = CommonUtils.getRoundValue(ticketReturn.total - deductions);
                                logger.debug(requestId + " - netEarning: " + netEarning);
                                if (companyCommission) {
                                    netEarning = CommonUtils.getRoundValue(netEarning - companyAmount);
                                    logger.debug(requestId + " - netEarning after company commission: " + netEarning);
                                }

                                // check promo
                                double driverReceive = netEarning;
                                if (isNewIncentiveReferralWithPromo || paidByDriver) {
                                    driverReceive = netEarning - promoAmount;
                                }
                                logger.debug(requestId + " - paidByOtherMethod: " + ticketReturn.paidByOtherMethod);
                                logger.debug(requestId + " - promoAmount: " + promoAmount);
                                logger.debug(requestId + " - driverReceive: " + driverReceive);
                                if (ticketReturn.paidByOtherMethod > driverReceive) {
                                    // fleet collect deduction from credit wallet
                                    double collectDeduction = CommonUtils.getRoundValue(ticketReturn.paidByOtherMethod - driverReceive);
                                    logger.debug(requestId + " - fleet collect deduction from credit wallet: " + collectDeduction);
                                    // update driver deposit before calculate net earning
                                    if (account != null) {
                                        driverDeduction = collectDeductionFromCreditWallet(collectDeduction, ticketReturn.bookId, providerFleet, account, ticketReturn.currencyISO);
                                    }
                                    logger.debug(requestId + " - driverDeduction: " + driverDeduction);
                                } else if (ticketReturn.paidByOtherMethod < driverReceive) {
                                    // fleet pay remain earning to cash wallet
                                    double remainEarning = CommonUtils.getRoundValue(driverReceive - ticketReturn.paidByOtherMethod);
                                    logger.debug(requestId + " - fleet pay remain earning to cash wallet: " + remainEarning);
                                    if (pricingPlanProvider.driverCashWallet != null && pricingPlanProvider.driverCashWallet.enable) {
                                        if (account != null) {
                                            // update wallet
                                            WalletTransfer walletTransfer = new WalletTransfer();
                                            walletTransfer.fleetId = pricingPlanProvider.fleetId;
                                            walletTransfer.driverId = account.userId;
                                            walletTransfer.amount = remainEarning;
                                            walletTransfer.transferType = CommonArrayUtils.DRIVER_CASH_WALLET_TYPE[10];
                                            walletTransfer.transactionId = ticketReturn.bookId;
                                            walletTransfer.currencyISO = ticketReturn.currencyISO;
                                            UpdateDriverCashBalance updateCash = new UpdateDriverCashBalance();
                                            updateCash.requestId = requestId;
                                            updateCash.walletTransfer = walletTransfer;
                                            updateCash.mongoDao = mongoDao;
                                            updateCash.redisDao = redisDao;
                                            updateCash.isDeposit = true;
                                            updateCash.pendingBalance = -1;
                                            double newBalance = updateCash.doUpdate();
                                            // update current balance if there is any update from another process
                                            currentBalance = CommonUtils.getRoundValue(newBalance - walletTransfer.amount);

                                            // send transaction to Report
                                            String companyName = booking.drvInfo.companyName != null ? CommonUtils.removeSpecialCharacter(booking.drvInfo.companyName) : "";
                                            if (companyName.length() > 255)
                                                companyName = companyName.substring(0, 255);

                                            JSONObject objTrans = new JSONObject();
                                            objTrans.put("fleetId", booking.fleetId);
                                            objTrans.put("userId", booking.drvInfo.userId);
                                            objTrans.put("transactionId", "");
                                            objTrans.put("bookId", booking.bookId);
                                            objTrans.put("driverName", booking.drvInfo.fullName != null ? booking.drvInfo.fullName.trim() : "");
                                            objTrans.put("phoneNumber", booking.drvInfo.phone);
                                            objTrans.put("transactionType", CommonArrayUtils.DRIVER_CASH_WALLET_TYPE[10]);
                                            objTrans.put("description", CommonArrayUtils.DRIVER_CASH_WALLET_TYPE[10] + "-" + booking.bookId);
                                            objTrans.put("cardNumber", "");
                                            objTrans.put("amount", remainEarning);
                                            objTrans.put("newBalance", newBalance);
                                            objTrans.put("currencyISO", booking.currencyISO);
                                            objTrans.put("currencySymbol", booking.currencySymbol);
                                            objTrans.put("companyId", companyId);
                                            objTrans.put("companyName", companyName);
                                            objTrans.put("createdDate", KeysUtil.SDF_DMYHMSTZ.format(TimezoneUtil.getGMTTimestamp()));
                                            DriverWalletTransactionThread driverWalletTransactionThread = new DriverWalletTransactionThread();
                                            driverWalletTransactionThread.id = String.valueOf(sqlDao.getIdWalletTransaction(booking.drvInfo.userId, "cash", booking.currencyISO, CommonArrayUtils.DRIVER_CASH_WALLET_TYPE[10], booking.bookId));
                                            driverWalletTransactionThread.requestId = requestId;
                                            driverWalletTransactionThread.reportBody = objTrans.toJSONString();
                                            driverWalletTransactionThread.start();
                                        }
                                    }
                                }

                                // send promo to credit wallet
                                if (account != null && promoAmount > 0 && !isNewIncentiveReferralWithPromo && !paidByDriver) {
                                    if (pricingPlanProvider.driverDeposit != null && pricingPlanProvider.driverDeposit.enable) {
                                        // deposit promo value to driver CREDIT wallet
                                        UpdateDriverCreditBalance updateBalance = new UpdateDriverCreditBalance();
                                        updateBalance.type = CommonArrayUtils.DRIVER_WALLET_TYPE[9];
                                        updateBalance.fleetId = ticketReturn.fleetId;
                                        updateBalance.driverId = ticketReturn.driverId;
                                        updateBalance.amount = promoAmount;
                                        updateBalance.currencyISO = ticketReturn.currencyISO;
                                        updateBalance.mongoDao = mongoDao;
                                        updateBalance.sqlDao = sqlDao;
                                        updateBalance.reason = "";
                                        updateBalance.additionalData.put("bookId", ticketReturn.bookId);
                                        updateBalance.additionalData.put("bookingFare", ticketReturn.total);
                                        updateBalance.requestId = requestId;
                                        String updateResult = updateBalance.doUpdate();
                                        if (CommonUtils.getReturnCode(updateResult) == 200) {
                                            // top-up success - notify to Jupiter and driver
                                            paymentUtil.notifyUpdateBalanceToDispatch(requestId, ticketReturn.driverId, "credit", promoAmount, ticketReturn.currencyISO);
                                        }
                                    }
                                }
                            } else {
                                logger.debug(requestId + " == calculate payment data for partial payment");
                                logger.debug(requestId + " - totalWithoutPromo: " + totalWithoutPromo);
                                logger.debug(requestId + " - deductions: " + deductions);
                                logger.debug(requestId + " - pay by wallet: " + ticketReturn.paidByWallet);
                                logger.debug(requestId + " - pay by cash: " + ticketReturn.paidByOtherMethod);

                                netEarning = CommonUtils.getRoundValue(totalWithoutPromo - deductions);
                                logger.debug(requestId + " - netEarning: " + netEarning);

                                // check promo
                                double driverReceive = netEarning;
                                if (isNewIncentiveReferralWithPromo || paidByDriver) {
                                    driverReceive = netEarning - promoAmount;
                                }
                                logger.debug(requestId + " - paidByOtherMethod: " + ticketReturn.paidByOtherMethod);
                                logger.debug(requestId + " - promoAmount: " + promoAmount);
                                logger.debug(requestId + " - driverReceive: " + driverReceive);
                                if (ticketReturn.paidByOtherMethod > driverReceive) {
                                    // fleet collect deduction from credit wallet
                                    double collectDeduction = CommonUtils.getRoundValue(ticketReturn.paidByOtherMethod - driverReceive);
                                    logger.debug(requestId + " - fleet collect deduction from credit wallet: " + collectDeduction);
                                    // update driver deposit before calculate net earning
                                    if (account != null) {
                                        driverDeduction = collectDeductionFromCreditWallet(collectDeduction, ticketReturn.bookId, providerFleet, account, ticketReturn.currencyISO);
                                    }
                                    logger.debug(requestId + " - driverDeduction: " + driverDeduction);
                                } else if (ticketReturn.paidByOtherMethod < driverReceive) {
                                    // fleet pay remain earning to cash wallet
                                    double remainEarning = CommonUtils.getRoundValue(driverReceive - ticketReturn.paidByOtherMethod);
                                    logger.debug(requestId + " - fleet pay remain earning to cash wallet: " + remainEarning);
                                    if (pricingPlanProvider.driverCashWallet != null && pricingPlanProvider.driverCashWallet.enable) {
                                        if (account != null) {
                                            // update wallet
                                            WalletTransfer walletTransfer = new WalletTransfer();
                                            walletTransfer.fleetId = pricingPlanProvider.fleetId;
                                            walletTransfer.driverId = account.userId;
                                            walletTransfer.amount = remainEarning;
                                            walletTransfer.transferType = CommonArrayUtils.DRIVER_CASH_WALLET_TYPE[10];
                                            walletTransfer.transactionId = ticketReturn.bookId;
                                            walletTransfer.currencyISO = ticketReturn.currencyISO;
                                            UpdateDriverCashBalance updateCash = new UpdateDriverCashBalance();
                                            updateCash.requestId = requestId;
                                            updateCash.walletTransfer = walletTransfer;
                                            updateCash.mongoDao = mongoDao;
                                            updateCash.redisDao = redisDao;
                                            updateCash.isDeposit = true;
                                            updateCash.pendingBalance = -1;
                                            double newBalance = updateCash.doUpdate();
                                            // update current balance if there is any update from another process
                                            currentBalance = CommonUtils.getRoundValue(newBalance - walletTransfer.amount);
                                            // send transaction to Report
                                            String companyName = booking.drvInfo.companyName != null ? CommonUtils.removeSpecialCharacter(booking.drvInfo.companyName) : "";
                                            if (companyName.length() > 255)
                                                companyName = companyName.substring(0, 255);

                                            JSONObject objTrans = new JSONObject();
                                            objTrans.put("fleetId", booking.fleetId);
                                            objTrans.put("userId", booking.drvInfo.userId);
                                            objTrans.put("transactionId", "");
                                            objTrans.put("bookId", booking.bookId);
                                            objTrans.put("driverName", booking.drvInfo.fullName != null ? booking.drvInfo.fullName.trim() : "");
                                            objTrans.put("phoneNumber", booking.drvInfo.phone);
                                            objTrans.put("transactionType", CommonArrayUtils.DRIVER_CASH_WALLET_TYPE[10]);
                                            objTrans.put("description", CommonArrayUtils.DRIVER_CASH_WALLET_TYPE[10] + "-" + booking.bookId);
                                            objTrans.put("cardNumber", "");
                                            objTrans.put("amount", remainEarning);
                                            objTrans.put("newBalance", newBalance);
                                            objTrans.put("currencyISO", booking.currencyISO);
                                            objTrans.put("currencySymbol", booking.currencySymbol);
                                            objTrans.put("companyId", companyId);
                                            objTrans.put("companyName", companyName);
                                            objTrans.put("createdDate", KeysUtil.SDF_DMYHMSTZ.format(TimezoneUtil.getGMTTimestamp()));
                                            DriverWalletTransactionThread driverWalletTransactionThread = new DriverWalletTransactionThread();
                                            driverWalletTransactionThread.id = String.valueOf(sqlDao.getIdWalletTransaction(booking.drvInfo.userId, "cash", booking.currencyISO, CommonArrayUtils.DRIVER_CASH_WALLET_TYPE[10], booking.bookId));
                                            driverWalletTransactionThread.requestId = requestId;
                                            driverWalletTransactionThread.reportBody = objTrans.toJSONString();
                                            driverWalletTransactionThread.start();
                                        }
                                    }
                                }
                            }
                        } else {
                            logger.debug(requestId + " == calculate payment data for normal payment");
                            logger.debug(requestId + " - total: " + ticketReturn.total);
                            logger.debug(requestId + " - deductions: " + deductions);
                            logger.debug(requestId + " - totalWithoutPromo: " + totalWithoutPromo);
                            if (KeysUtil.CASH_PAYMENT.contains(ticketReturn.transactionStatus)
                                    || (booking.intercity && booking.request.paymentType == 0)
                                    ) {
                                // update deduction to collect markup difference
                                if (booking.request != null) {
                                    double markupDifference = 0.0;
                                    if (booking.request.estimate != null && booking.request.estimate.fare != null)
                                        markupDifference = booking.request.estimate.fare.markupDifference;
                                    logger.debug(requestId + " - markupDifference: " + markupDifference);
                                    deductions = deductions + markupDifference;
                                    logger.debug(requestId + " - deductions = deductions + markupDifference = " + deductions);
                                }
                                if (driverEarningType.equals("default")) {

                                    // update driver deposit before calculate net earning
                                    if (account != null) {
                                        double deductAmount = deductions;
                                        if (companyCommission) {
                                            // if fleet enable company commission, driver earning has included company amount => deduct from driver wallet
                                            deductAmount += companyAmount;
                                        }
                                        logger.debug(requestId + " - deduct amount: " + deductAmount);
                                        driverDeduction = collectDeductionFromCreditWallet(deductAmount, ticketReturn.bookId, providerFleet, account, ticketReturn.currencyISO);
                                    }

                                    /*
                                      Add DriverCreditBalance if match all following conditions:
                                      - not an intercity booking
                                      - booking requested payment by cash
                                      - booking requested from pax app
                                      - receiveAmount > totalWithoutPromo
                                     */
                                    try {
                                        if (!ticketReturn.intercity && !KeysUtil.bookNotPax.contains(ticketReturn.bookFrom) &&
                                                ticketReturn.receivedAmount > ticketReturn.total && ticketReturn.transactionStatus.equals(KeysUtil.PAYMENT_CASH)) {
                                            JSONObject objSetting = paymentUtil.enableCashExcess(ticketReturn.fleetId, ticketReturn.currencyISO);
                                            boolean enableCashExcess = Boolean.parseBoolean(objSetting.get("enableCashExcess").toString());
                                            if (enableCashExcess) {
                                                addCashExcessDriverCreditWallet(ticketReturn.bookId, providerFleet, account, ticketReturn);
                                            }
                                        }
                                    } catch (Exception ex) {
                                        logger.debug(requestId + " - update DriverCreditBalance for cashExcess-" + ticketReturn.bookId + " error: " + CommonUtils.getError(ex));
                                    }

                                    if (promoAmount > totalWithoutPromo) {
                                        netEarning = -deductions + totalWithoutPromo;
                                    } else {
                                        if (isNewIncentiveReferralWithPromo || paidByDriver) {
                                            netEarning = -deductions;
                                        } else {
                                            if (pricingPlanProvider.promoPayToWallet == 1) {
                                                netEarning = -deductions;
                                            } else {
                                                netEarning = -deductions + promoAmount;
                                            }
                                        }
                                    }
                                    logger.debug(requestId + " - netEarning: " + netEarning);
                                    logger.debug(requestId + " - driverDeduction: " + driverDeduction);
                                    if (driverDeduction > 0)
                                        netEarning = netEarning + driverDeduction;
                                    logger.debug(requestId + " - netEarning after driver deduction: " + netEarning);
                                    if (companyCommission) {
                                        // if fleet enable company commission, driver earning has included company amount
                                        netEarning -= companyAmount;
                                        logger.debug(requestId + " - netEarning after company amount: " + netEarning);
                                    }
                                } else {
                                    logger.debug(requestId + " - driverEarningType =  " + driverEarningType + " => update netEarning");
                                    // update driver deposit before calculate net earning
                                    if (account != null) {
                                        // deductions amount include fleet amount and company amount
                                        double deductAmount = deductions;
                                        logger.debug(requestId + " - deduct amount: " + deductAmount);
                                        if (deductions > 0) {
                                            driverDeduction = collectDeductionFromCreditWallet(deductAmount, ticketReturn.bookId, providerFleet, account, ticketReturn.currencyISO);
                                        } else {
                                            driverDeduction = deductions;
                                        }

                                    }
                                    logger.debug(requestId + " - driverDeduction: " + driverDeduction);

                                    if (pricingPlanProvider.driverDeposit != null && pricingPlanProvider.driverDeposit.enable) {
                                        netEarning = -deductions;
                                        if (driverDeduction > 0)
                                            netEarning = netEarning + driverDeduction;
                                    } else {
                                        netEarning = -deductions;
                                    }
                                }
                                if (account != null && promoAmount > 0 && !isNewIncentiveReferralWithPromo && !paidByDriver) {
                                    if (pricingPlanProvider.promoPayToWallet == 1) {
                                        if (pricingPlanProvider.driverDeposit != null && pricingPlanProvider.driverDeposit.enable && driverEarningType.equals("default")) {
                                            // deposit promo value to driver CREDIT wallet
                                            UpdateDriverCreditBalance updateBalance = new UpdateDriverCreditBalance();
                                            updateBalance.type = CommonArrayUtils.DRIVER_WALLET_TYPE[9];
                                            updateBalance.fleetId = ticketReturn.fleetId;
                                            updateBalance.driverId = ticketReturn.driverId;
                                            updateBalance.amount = promoAmount;
                                            updateBalance.currencyISO = ticketReturn.currencyISO;
                                            updateBalance.mongoDao = mongoDao;
                                            updateBalance.sqlDao = sqlDao;
                                            updateBalance.reason = "";
                                            updateBalance.additionalData.put("bookId", ticketReturn.bookId);
                                            updateBalance.additionalData.put("bookingFare", ticketReturn.total);
                                            updateBalance.requestId = requestId;
                                            String updateResult = updateBalance.doUpdate();
                                            if (CommonUtils.getReturnCode(updateResult) == 200) {
                                                // top-up success - notify to Jupiter and driver
                                                paymentUtil.notifyUpdateBalanceToDispatch(requestId, ticketReturn.driverId, "credit", promoAmount, ticketReturn.currencyISO);
                                            }
                                        }
                                    } else {
                                        // deposit promo value to driver CASH wallet
                                        if (pricingPlanProvider.driverCashWallet != null && pricingPlanProvider.driverCashWallet.enable) {
                                            // update wallet
                                            WalletTransfer walletTransfer = new WalletTransfer();
                                            walletTransfer.fleetId = pricingPlanProvider.fleetId;
                                            walletTransfer.driverId = account.userId;
                                            walletTransfer.amount = promoAmount;
                                            walletTransfer.transferType = CommonArrayUtils.DRIVER_CASH_WALLET_TYPE[9];
                                            walletTransfer.transactionId = ticketReturn.bookId;
                                            walletTransfer.currencyISO = ticketReturn.currencyISO;
                                            UpdateDriverCashBalance updateCash = new UpdateDriverCashBalance();
                                            updateCash.requestId = requestId;
                                            updateCash.walletTransfer = walletTransfer;
                                            updateCash.mongoDao = mongoDao;
                                            updateCash.redisDao = redisDao;
                                            updateCash.isDeposit = true;
                                            updateCash.pendingBalance = -1;
                                            double newBalance = updateCash.doUpdate();
                                            // update current balance if there is any update from another process
                                            currentBalance = CommonUtils.getRoundValue(newBalance - walletTransfer.amount);
                                        }
                                    }
                                }
                            } else {
                                // complete online payment - except CASH
                                // check markup difference then update the total
                                double markupDifference = 0.0;
                                if (booking.request != null) {
                                    if (booking.request.estimate != null && booking.request.estimate.fare != null)
                                        markupDifference = booking.request.estimate.fare.markupDifference;
                                }
                                // check if transferred money to driver Connected account
                                if (paidToDriver) {
                                    netEarning = driverAmount;
                                } else {
                                    if (driverEarningType.equals("default")) {
                                        if (ticketReturn.pricingType == 0) {
                                            if (isNewIncentiveReferralWithPromo || paidByDriver) {
                                                netEarning = ticketReturn.total - markupDifference - deductions;
                                            } else {
                                                if (pricingPlanProvider.promoPayToWallet == 1) {
                                                    netEarning = ticketReturn.total - markupDifference - deductions;
                                                } else {
                                                    netEarning = totalWithoutPromo - deductions;
                                                }
                                            }
                                        } else {
                                            netEarning = totalWithoutPromo - deductions;
                                        }
                                        if (companyCommission) {
                                            double oldEarning = netEarning;
                                            logger.debug(requestId + " - if enable company commission => subtract companyAmount from driver earning");
                                            netEarning = netEarning - (companyAmount + stripeFee);
                                            logger.debug(requestId + " - netEarning = netEarning - (companyAmount + stripeFee) = " + oldEarning + " - (" + companyAmount + " + " + stripeFee + ") = " + netEarning);

                                            logger.debug(requestId + " - if enable company commission => company will pay for Stripe fee and promo => subtract from company amount");
                                            double oldAmount = companyAmount;
                                            companyAmount = companyAmount - stripeFee - promoAmount;
                                            logger.debug(requestId + " - companyAmount = companyAmount - stripeFee - promoAmount = " + oldAmount + " - " + stripeFee + " - " + promoAmount + " = " + companyAmount);
                                        }
                                    } else {
                                        logger.debug(requestId + " - driverEarningType =  " + driverEarningType + " => update netEarning and companyAmount");
                                        netEarning = grossEarning;
                                        if (companyCommission) {
                                            logger.debug(requestId + " - if enable company commission => company will pay for Stripe fee and promo => subtract from company amount");
                                            double oldAmount = companyAmount;
                                            companyAmount = companyAmount - stripeFee - promoAmount;
                                            logger.debug(requestId + " - companyAmount = companyAmount - stripeFee - promoAmount = " + oldAmount + " - " + stripeFee + " - " + promoAmount + " = " + companyAmount);
                                        }
                                    }
                                    driverAmount = netEarning;
                                    logger.debug(requestId + " - update driverAmount = netEarning = " + driverAmount);
                                }
                                logger.debug(requestId + " - netEarning: " + netEarning);

                                if (pricingPlanProvider.driverCashWallet != null && pricingPlanProvider.driverCashWallet.enable) {
                                    if (account != null) {
                                        currentBalance = walletUtil.getCurrentBalance(account.userId, "cash", ticketReturn.currencyISO);
                                        // update wallet
                                        netEarning = DF.parse(DF.format(netEarning)).doubleValue();
                                        logger.debug(requestId + " - paidToDriver: " + paidToDriver);
                                        if (!paidToDriver) {
                                            WalletTransfer walletTransfer = new WalletTransfer();
                                            walletTransfer.fleetId = pricingPlanProvider.fleetId;
                                            walletTransfer.driverId = account.userId;
                                            walletTransfer.amount = netEarning;
                                            walletTransfer.transferType = CommonArrayUtils.DRIVER_CASH_WALLET_TYPE[2];
                                            walletTransfer.transactionId = ticketReturn.bookId;
                                            walletTransfer.currencyISO = ticketReturn.currencyISO;
                                            UpdateDriverCashBalance updateCash = new UpdateDriverCashBalance();
                                            updateCash.requestId = requestId;
                                            updateCash.walletTransfer = walletTransfer;
                                            updateCash.mongoDao = mongoDao;
                                            updateCash.redisDao = redisDao;
                                            updateCash.isDeposit = true;
                                            updateCash.pendingBalance = -1;
                                            double newBalance = updateCash.doUpdate();
                                            // update current balance if there is any update from another process
                                            currentBalance = CommonUtils.getRoundValue(newBalance - walletTransfer.amount);
                                        }
                                    }
                                }
                                if (pricingPlanProvider.driverDeposit != null && pricingPlanProvider.driverDeposit.enable) {
                                    if (account != null && promoAmount > 0 && !isNewIncentiveReferralWithPromo && !paidByDriver) {
                                        if (pricingPlanProvider.promoPayToWallet == 1) {
                                            // deposit driver credit wallet
                                            UpdateDriverCreditBalance updateBalance = new UpdateDriverCreditBalance();
                                            updateBalance.type = CommonArrayUtils.DRIVER_WALLET_TYPE[9];
                                            updateBalance.fleetId = ticketReturn.fleetId;
                                            updateBalance.driverId = ticketReturn.driverId;
                                            updateBalance.amount = promoAmount;
                                            updateBalance.currencyISO = ticketReturn.currencyISO;
                                            updateBalance.mongoDao = mongoDao;
                                            updateBalance.sqlDao = sqlDao;
                                            updateBalance.reason = "";
                                            updateBalance.additionalData.put("bookId", ticketReturn.bookId);
                                            updateBalance.additionalData.put("bookingFare", ticketReturn.total);
                                            updateBalance.requestId = requestId;
                                            String updateResult = updateBalance.doUpdate();
                                            if (CommonUtils.getReturnCode(updateResult) == 200) {
                                                // top-up success - notify to Jupiter and driver
                                                paymentUtil.notifyUpdateBalanceToDispatch(requestId, ticketReturn.driverId, "credit", promoAmount, ticketReturn.currencyISO);
                                            }
                                        }
                                    }
                                }

                            }
                        }

                        logger.debug(requestId + " - earning: " + netEarning);
                        logger.debug(requestId + " - currentBalance: " + currentBalance);

                        //provider fleet - add data for hydra
                        if (ticketReturn.pricingType != 0) {
                            if (providerFleet.fleetDeduction.transactionFee.enable) {
                                List<FeeByCurrency> feeByCurrencies = providerFleet.fleetDeduction.transactionFee.feeByCurrencies;
                                for (FeeByCurrency feeByCurrency : feeByCurrencies) {
                                    if (feeByCurrency.currencyISO.equalsIgnoreCase(ticketReturn.currencyISO)) {
                                        if (credits.contains(transactionStatus)) {
                                            if (providerFleet.fleetDeduction != null && providerFleet.fleetDeduction.transactionFee != null) {
                                                transactionFeeProvider = feeByCurrency.creditCardAmount +
                                                        feeByCurrency.creditCardPercent * ticketEnt.totalProvider / 100;
                                            }
                                        } else if (transactionStatus.equalsIgnoreCase(KeysUtil.PAYMENT_DIRECTBILLING)) {
                                            if (providerFleet.fleetDeduction != null && providerFleet.fleetDeduction.transactionFee != null) {
                                                transactionFeeProvider = feeByCurrency.directBillingAmount +
                                                        feeByCurrency.directBillingPercent * ticketEnt.totalProvider / 100;
                                            }
                                        } else if (transactionStatus.equalsIgnoreCase(KeysUtil.PAYMENT_FLEETCARD)) {
                                            if (providerFleet.fleetDeduction != null && providerFleet.fleetDeduction.transactionFee != null) {
                                                transactionFeeProvider = feeByCurrency.externalCardAmount +
                                                        feeByCurrency.externalCardPercent * ticketEnt.totalProvider / 100;
                                            }
                                        }
                                    }
                                }
                            }

                            // assign provider report data from provider fleet
                            commProvider = comm;
                            deductionsProvider = deductions;
                            ridePaymentProvider = ridePayment;
                            grossEarningProvider = grossEarning;
                            netEarningProvider = netEarning;

                            // add hydra data
                            qupBuyPrice = CommonUtils.getRoundValue(booking.request.estimate.fare.etaFare + ticketReturn.tollFee + ticketReturn.parkingFee + ticketReturn.gasFee);

                            qupPreferredAmount = estimateUtil.getQUpPreferred(requestId, qupBuyPrice, booking.request.estimate.fare.typeRate, booking.fleetId);
                            logger.debug(requestId + " - qupPreferredAmount: " + qupPreferredAmount);

                            qupSellPrice = CommonUtils.getRoundValue(qupBuyPrice + qupPreferredAmount);
                            logger.debug(requestId + " - qupSellPrice: " + qupSellPrice);

                            sellPriceMarkup = CommonUtils.getRoundValue(qupSellPrice + fleetMarkup);
                            logger.debug(requestId + " - sellPriceMarkup: " + sellPriceMarkup);

                            supplierCommission = 0.0;
                            if (pricingPlanProvider.affiliate != null) {
                                supplierCommission = pricingPlanProvider.affiliate.supplierPayout;
                            }
                            profitFromSupplier = CommonUtils.getRoundValue(qupBuyPrice * supplierCommission / 100);
                            logger.debug(requestId + " - profitFromSupplier: " + profitFromSupplier);

                            supplierPayout = CommonUtils.getRoundValue(qupBuyPrice - qupBuyPrice * supplierCommission / 100);
                            logger.debug(requestId + " - qupBuyPrice: " + qupBuyPrice);
                            logger.debug(requestId + " - supplierCommission: " + supplierCommission + " => supplierPayout: " + supplierPayout);

                            PricingPlan pricingPlanPsg = mongoDao.getPricingPlan(psgFleetId);
                            buyerCommission = 0.0;
                            if (pricingPlanPsg.affiliate != null) {
                                buyerCommission = pricingPlanPsg.affiliate.homeFleetCommission;
                            }
                            buyerPayout = qupPreferredAmount * buyerCommission / 100;
                            logger.debug(requestId + " - buyerCommission: " + buyerCommission + " => buyerPayout: " + buyerPayout);

                            profitFromBuyer = CommonUtils.getRoundValue(qupPreferredAmount - buyerPayout);
                            logger.debug(requestId + " - profitFromBuyer: " + profitFromBuyer);

                            qupEarning = CommonUtils.getRoundValue((qupPreferredAmount - buyerPayout) + (qupBuyPrice - supplierPayout));
                            logger.debug(requestId + " - qupEarning: " + qupEarning);

                            if (networkType.equals("roaming")) {
                                // get exchange rate from Stripe
                                String fleetId = booking.isFarmOut ? booking.request.psgFleetId : booking.fleetId;
                                exchangeRate = paymentUtil.getExchangeRate(requestId, fleetId, ticketReturn.gatewayId, booking.isFarmOut, booking.currencyISO);
                                logger.debug(requestId + " - gatewayId: " + ticketReturn.gatewayId + " -> exchangeRate: " + exchangeRate);

                                // do transfer payout to fleet
                                HydraUtil hydraUtil = new HydraUtil();
                                double convertBuyerPayout = CommonUtils.getRoundValue(buyerPayout / exchangeRate);
                                logger.debug(requestId + " - convertSupplierPayout = " + convertBuyerPayout);
                                if (convertBuyerPayout > 0.5) {
                                    String payBuyer = hydraUtil.payoutToFleet(requestId, psgFleetId, ticketReturn.gatewayId, booking, convertBuyerPayout);
                                    logger.debug(requestId + " - payout to buyer : " + payBuyer);
                                    if (CommonUtils.getReturnCode(payBuyer) != 200) {
                                        payoutToHomeFleet = false;
                                        buyerFailedId = CommonUtils.getValue(payBuyer, "requestId");
                                    }
                                } else {
                                    payoutToHomeFleet = false;
                                }
                                double convertSupplierPayout = CommonUtils.getRoundValue(supplierPayout / exchangeRate);
                                logger.debug(requestId + " - convertSupplierPayout = " + convertSupplierPayout);
                                if (convertSupplierPayout > 0.5) {
                                    String paySupplier = hydraUtil.payoutToFleet(requestId, ticketReturn.fleetId, ticketReturn.gatewayId, booking, convertSupplierPayout);
                                    logger.debug(requestId + " - payout to supplier : " + paySupplier);
                                    if (CommonUtils.getReturnCode(paySupplier) != 200) {
                                        payoutToProviderFleet = false;
                                        supplierFailedId = CommonUtils.getValue(paySupplier, "requestId");
                                    }
                                } else {
                                    payoutToProviderFleet = false;
                                }
                            } else {
                                // set not payout
                                payoutToHomeFleet = false;
                                payoutToProviderFleet = false;
                            }
                        }
                        logger.debug(requestId + " - transactionStatus: " + transactionStatus);

                        // save payment details for admin - cross zone report
                        fleetName = providerFleet.name;
                    } catch (Exception ex) {
                        logger.debug(requestId + " - add updateNetEarning error: " + CommonUtils.getError(ex));
                    }

                    // ticket has been paid with total amount, or canceled with charge policy amount
                    fleetProfit = comm + ticketReturn.techFee - promoAmount;
                    if (isNewIncentiveReferralWithPromo || paidByDriver) {
                        fleetProfit = comm + ticketReturn.techFee;
                    }
                    logger.debug(requestId + " - fleetProfit: " + fleetProfit);
                    if (isMinimumTotal) {
                        logger.debug(requestId + " - isMinimumTotal = true => update fleet profit = profit + (minimum - total before minimum)");
                        fleetProfit += minimum - ticketReturn.total;
                        logger.debug(requestId + " - fleetProfit: " + fleetProfit);
                    }
                    // add markup price
                    double markupDifference = (booking.request.estimate != null && booking.request.estimate.fare != null) ? booking.request.estimate.fare.markupDifference : 0.0;
                    fleetProfit += markupDifference;
                    logger.debug(requestId + " - fleetProfit after add markupDifference: " + fleetProfit);
                    // add profit from fleet service
                    fleetProfit += fleetCommissionFromFleetServiceFee;
                    logger.debug(requestId + " - fleetProfit after add fleetCommissionFromFleetServiceFee: " + fleetProfit);
                    // add merchant commission
                    fleetProfit += merchantCommission;
                    logger.debug(requestId + " - fleetProfit after add merchantCommission: " + fleetProfit);

                    if (companyCommission) {
                        logger.debug(requestId + " - fleet enable company commission => fleet just receive commission from sub-total and markupDifference");
                        fleetProfit = comm + markupDifference;
                    }
                }
            }
        }

        try {
            netEarning = DF.parse(DF.format(netEarning)).doubleValue();
            currentBalance = DF.parse(DF.format(currentBalance)).doubleValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        mapData.put("mDispatcherCommission", DF.format(mDispatcherCommission));
        mapData.put("promoAmount", DF.format(promoAmount));
        mapData.put("netEarning", DF.format(netEarning));
        mapData.put("newBalance", DF.format(netEarning + currentBalance));
        mapData.put("totalFare", DF.format(totalWithoutPromo));
        mapData.put("comm", DF.format(comm));
        mapData.put("deductions", DF.format(deductions));
        mapData.put("ridePayment", DF.format(ridePayment));
        mapData.put("grossEarning", DF.format(grossEarning));
        mapData.put("transactionFee", DF.format(transactionFee));
        mapData.put("netEarningProvider", DF.format(netEarningProvider));
        mapData.put("commProvider", DF.format(commProvider));
        mapData.put("deductionsProvider", DF.format(deductionsProvider));
        mapData.put("ridePaymentProvider", DF.format(ridePaymentProvider));
        mapData.put("grossEarningProvider", DF.format(grossEarningProvider));
        mapData.put("transactionFeeProvider", DF.format(transactionFeeProvider));
        mapData.put("fleetCommission", DF.format(fleetCommission));
        mapData.put("netProfit", DF.format(netProfit));
        mapData.put("fleetName", fleetName);
        mapData.put("drvUsername", drvUsername);
        mapData.put("psgFleetName", psgFleetName);
        mapData.put("ownerId", ownerId);
        mapData.put("ownerName", ownerName);
        mapData.put("exchangeRate", String.valueOf(exchangeRate));
        mapData.put("commissionType", commissionType);
        mapData.put("driverDeduction", DF.format(driverDeduction));
        mapData.put("mDispatcherCommissionType", mDispatcherCommissionType);
        mapData.put("merchantCommission", DF.format(merchantCommission));
        mapData.put("avatar", avatar);
        mapData.put("shortTrip", String.valueOf(shortTrip));
        mapData.put("paidByDriver", String.valueOf(paidByDriver));
        mapData.put("isNewIncentiveReferralWithPromo", String.valueOf(isNewIncentiveReferralWithPromo));
        mapData.put("fleetProfit", DF.format(fleetProfit));
        mapData.put("fleetServiceFee", DF.format(fleetServiceFee));
        mapData.put("fleetCommissionFromFleetServiceFee", DF.format(fleetCommissionFromFleetServiceFee));
        mapData.put("driverCommissionFromFleetServiceFee", DF.format(driverCommissionFromFleetServiceFee));
        mapData.put("isMinimumTotal", isMinimumTotal ? "true" : "false");
        mapData.put("qupPreferredAmount", DF.format(qupPreferredAmount));
        mapData.put("qupBuyPrice", DF.format(qupBuyPrice));
        mapData.put("qupSellPrice", DF.format(qupSellPrice));
        mapData.put("supplierPayout", DF.format(supplierPayout));
        mapData.put("convertSupplierPayout", DF.format(supplierPayout));
        mapData.put("buyerPayout", DF.format(buyerPayout));
        mapData.put("convertBuyerPayout", DF.format(buyerPayout));
        mapData.put("qupEarning", DF.format(qupEarning));
        mapData.put("supplierCommission", DF.format(supplierCommission));
        mapData.put("buyerCommission", DF.format(buyerCommission));
        mapData.put("profitFromSupplier", DF.format(profitFromSupplier));
        mapData.put("profitFromBuyer", DF.format(profitFromBuyer));
        mapData.put("penaltyPolicy", DF.format(penaltyPolicy));
        mapData.put("penaltyAmount", DF.format(penaltyAmount));
        mapData.put("compensationPolicy", DF.format(compensationPolicy));
        mapData.put("compensationAmount", DF.format(compensationAmount));
        mapData.put("payoutToHomeFleet", String.valueOf(payoutToHomeFleet));
        mapData.put("payoutToProviderFleet", String.valueOf(payoutToProviderFleet));
        mapData.put("supplierFailedId", supplierFailedId);
        mapData.put("buyerFailedId", buyerFailedId);
        mapData.put("convertSupplierPenalty", DF.format(penaltyAmount));
        mapData.put("fleetMarkup", DF.format(fleetMarkup));
        mapData.put("networkType", networkType);
        mapData.put("hydraPaymentMethod", ticketReturn.hydraPaymentMethod);
        mapData.put("isFarmOut", String.valueOf(ticketReturn.isFarmOut));
        mapData.put("sellPriceMarkup", DF.format(sellPriceMarkup));
        mapData.put("driverTax", DF.format(driverTax));
        mapData.put("paidToDriver", String.valueOf(paidToDriver));
        mapData.put("driverSettlement", String.valueOf(driverSettlement));
        mapData.put("fleetAmount", DF.format(fleetAmount));
        mapData.put("driverAmount", DF.format(driverAmount));
        mapData.put("companyAmount", DF.format(companyAmount));
        mapData.put("companyTax", DF.format(companyTax));
        mapData.put("paidToCompany", String.valueOf(paidToCompany));
        mapData.put("companyCommission", String.valueOf(companyCommission));
        mapData.put("stripeFee", DF.format(stripeFee));
        mapData.put("pendingSettlement", DF.format(pendingSettlement));
        mapData.put("driverEarningType", driverEarningType);
        mapData.put("editedDriverEarning", DF.format(editedDriverEarning));
        logger.debug(requestId + " - updateNetEarning: " + gson.toJson(mapData));
        return mapData;
    }

    double calculateDeduction(Fleet fleet, FleetFare fleetFare, double comm, double techFee, double tax,
                              double transactionFee, double partnerCommission, double airportFee, double meetDriverFee,
                              double tollFee, double parkingFee, double gasFee, double creditTransactionFee,
                              double fleetCommissionFromFleetServiceFee, double fleetServiceTax) throws ParseException {
        Zone zone = mongoDao.findByFleetIdAndGeo(fleet.fleetId, booking.request.pickup.geo);
        String zoneId = "";
        if(zone!= null)
            zoneId = zone._id.toString();
        ServiceFee sF = mongoDao.getServiceFeeByFleetIdAndZoneId(fleet.fleetId, zoneId);
        String applyType = fleetFare.applyType != null ? fleetFare.applyType : "all";
        if (sF != null && fleet.generalSetting.allowCongfigSettingForEachZone && !applyType.equals("all")) {
            if (sF.tollFeePayTo == 0) tollFee = 0;
            if (sF.parkingFeePayTo == 0) parkingFee = 0;
            if (sF.gasFeePayTo == 0) gasFee = 0;
        } else {
            if (fleetFare.tollFeePayTo == 0) tollFee = 0;
            if (fleetFare.parkingFeePayTo == 0) parkingFee = 0;
            if (fleetFare.gasFeePayTo == 0) gasFee = 0;
        }
        if (fleetFare != null && fleetFare.airport != null && fleetFare.airport.payTo == 0) {
            airportFee = 0;
        }
        if (fleetFare != null && fleetFare.meetDriver != null && fleetFare.meetDriver.payTo == 0) {
            meetDriverFee = 0;
        }
        double ded = comm + techFee + tollFee + parkingFee + gasFee + airportFee + meetDriverFee + partnerCommission + fleetCommissionFromFleetServiceFee + fleetServiceTax;
        logger.debug(requestId + " - comm: " + comm);
        logger.debug(requestId + " - techFee: " + techFee);
        logger.debug(requestId + " - tollFee: " + tollFee);
        logger.debug(requestId + " - parkingFee: " + parkingFee);
        logger.debug(requestId + " - gasFee: " + gasFee);
        logger.debug(requestId + " - airportFee: " + airportFee);
        logger.debug(requestId + " - meetDriverFee: " + meetDriverFee);
        logger.debug(requestId + " - partnerCommission: " + partnerCommission);
        logger.debug(requestId + " - fleetServiceTax: " + fleetServiceTax);
        logger.debug(requestId + " - ded = comm + techFee + tollFee + parkingFee + gasFee + airportFee + meetDriverFee + partnerCommission + fleetCommissionFromFleetServiceFee + fleetServiceTax");
        logger.debug(requestId + " - ded = " + ded);
        if (fleet.fleetDeduction != null) {
            if (fleet.fleetDeduction.enableTax) {
                ded = ded + tax;
                logger.debug(requestId + " - tax: " + tax);
                logger.debug(requestId + " - ded =  ded + tax = " + ded);
            }
            if (fleet.fleetDeduction.transactionFee != null) {
                if (fleet.fleetDeduction.transactionFee.enable) {
                    ded = ded + transactionFee;
                    logger.debug(requestId + " - transactionFee: " + transactionFee);
                    logger.debug(requestId + " - ded =  ded + transactionFee = " + ded);
                }
            }
        }
        ded = ded + creditTransactionFee;
        logger.debug(requestId + " - creditTransactionFee: " + creditTransactionFee);
        logger.debug(requestId + " - ded =  ded + creditTransactionFee = " + ded);
        double result = Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(ded));
        logger.debug(requestId + " - result = " + result);
        return result;
    }

    private void saveCustomerData(Fleet fleet, TicketReturn ticketReturn, String customerPhone, String referralCode, ReferralHistories referralHistories, int totalCompletedBook) throws Exception{
        try {
            List<String> credits = Arrays.asList(KeysUtil.PAYMENT_CARD,KeysUtil.PAYMENT_FLEETCARD);
            if (credits.contains(ticketReturn.transactionStatus)
                || (booking.intercity && booking.request.paymentType == 1)
                ) {
                // convert to server time before query to mongoDB
                Calendar now = Calendar.getInstance();
                Date completed = TimezoneUtil.offsetTimeZone(ticketReturn.completedTime, "GMT", Calendar.getInstance().getTimeZone().getID());
                now.setTime(completed);
                String dateString = now.get(Calendar.YEAR) + "-" + (now.get(Calendar.MONTH) + 1) + "-" + now.get(Calendar.DATE) + " 00:00:00";
                Date date = KeysUtil.SDF_DMYHMS.parse(dateString);
                TripNotification tripNotification = mongoDao.getTripNotificationByCustomer(ticketReturn.fleetId, "", date); // ticketReturn.getCustomerId
                if (tripNotification != null) {
                    tripNotification.totalTrip = tripNotification.totalTrip + 1;
                    tripNotification.totalFare = tripNotification.totalFare + ticketReturn.total;
                    mongoDao.updateTripNotification(tripNotification);
                } else {
                    // save completed time as GMT
                    now.setTime(ticketReturn.completedTime);
                    dateString = now.get(Calendar.YEAR) + "-" + (now.get(Calendar.MONTH) + 1) + "-" + now.get(Calendar.DATE) + " 00:00:00";
                    tripNotification = new TripNotification();
                    tripNotification.fleetId = ticketReturn.fleetId;
                    tripNotification.fleetName = ""; // ticketReturn.getFleetName
                    tripNotification.customerId = ""; // ticketReturn.getCustomerId()
                    tripNotification.customerPhone = ""; // ticketReturn.getCustomerPhone()
                    tripNotification.customerEmail = ""; // tripNotification.setCustomerEmail;
                    tripNotification.customerName = ticketReturn.customerName;
                    tripNotification.totalTrip = 1;
                    tripNotification.totalFare = ticketReturn.total;
                    tripNotification.date = KeysUtil.SDF_DMYHMS.parse(dateString);
                    mongoDao.addTripNotification(tripNotification);
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - add tripNotification error: " + CommonUtils.getError(ex));
        }

        PassengerInfo passengerInfo = mongoDao.getPassengerInfo(ticketReturn.fleetId, ticketReturn.customerId);

        double standingAmount = 0.0;
        boolean isPartial = false;
        if (!ticketReturn.transactionStatus.equals(KeysUtil.PAYMENT_DIRECTBILLING) && ticketReturn.paymentType != 5) {
            if (ticketReturn.isPending) {
                standingAmount = ticketReturn.total;
            } else if (ticketReturn.total > ticketReturn.totalCharged) {
                standingAmount = ticketReturn.total - ticketReturn.totalCharged;
                isPartial = true;
            }
        }

        // update outstanding amount
        try {
            logger.debug(requestId + "- standingAmount: " + standingAmount);
            if (standingAmount > 0) {
                if (passengerInfo == null) {
                    OutStanding outStanding = new OutStanding();
                    outStanding.amount = standingAmount;
                    outStanding.currencyISO = ticketReturn.currencyISO;
                    outStanding.currencySymbol = ticketReturn.currencySymbol;
                    List<OutStanding> outStandings = new ArrayList<>();
                    outStandings.add(outStanding);

                    // create default wallet
                    List<AmountByCurrency> listWallet = new ArrayList<>();
                    AmountByCurrency wallet = new AmountByCurrency();
                    wallet.value = 0.0;
                    wallet.currencyISO = ticketReturn.currencyISO;
                    listWallet.add(wallet);

                    passengerInfo = new PassengerInfo();
                    passengerInfo.fleetId = ticketReturn.fleetId;
                    passengerInfo.userId = ticketReturn.customerId;
                    passengerInfo.paxWallet = listWallet;
                    passengerInfo.outStanding = outStandings;
                    mongoDao.addPassengerInfo(passengerInfo);
                } else {
                    boolean isNew = true;
                    List<OutStanding> outStandings = passengerInfo.outStanding;
                    if (outStandings.size() > 0) {
                        for (OutStanding outStanding : passengerInfo.outStanding) {
                            if (outStanding.currencyISO != null && outStanding.currencyISO.equals(ticketReturn.currencyISO)) {
                                outStanding.amount += standingAmount;
                                isNew = false;
                            }
                        }
                    }
                    if (isNew) {
                        OutStanding outStanding = new OutStanding();
                        outStanding.amount = standingAmount;
                        outStanding.currencyISO = ticketReturn.currencyISO;
                        outStanding.currencySymbol = ticketReturn.currencySymbol;
                        outStandings.add(outStanding);
                    }
                    mongoDao.updateOustanding(ticketReturn.fleetId, ticketReturn.customerId, outStandings);
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - update pax Outstanding error: " + CommonUtils.getError(ex));
        }
        double customerDebt = ticketReturn.customerDebt;
        logger.debug(requestId + " - customerDebt: " + customerDebt);
        try {
            if (customerDebt > 0 && !KeysUtil.INCOMPLETE_PAYMENT.contains(ticketReturn.transactionStatus)) {
                ObjResponse queryOutstanding = sqlDao.getOutstandingTicket(ticketReturn.fleetId, ticketReturn.customerId, ticketReturn.currencyISO);
                List<Map<String, Object>> listData = new ArrayList<>();
                if (queryOutstanding.returnCode == 200)
                    listData = (List<Map<String, Object>>) queryOutstanding.response;
                logger.debug(requestId + " - queryOutstanding - listData: " + listData.size());
                String reason = "Pay from app with booking " + ticketReturn.bookId;
                if (ticketReturn.payFromCC)
                    reason = "Pay from CC with booking " + ticketReturn.bookId;
                String result = paymentUtil.doClearOutstanding(requestId, passengerInfo, listData, ticketReturn.transactionStatus, reason);
                logger.debug(requestId + " - doClearOutstanding result: " + result);
                if (CommonUtils.getReturnCode(result) == 200) {
                    if (ticketReturn.transactionStatus.equals(KeysUtil.PAYMENT_CASH)) {
                        Account account = mongoDao.getAccount(ticketReturn.driverId);
                        deductionCustomerDebtFromCreditWallet(customerDebt, ticketReturn.bookId, fleet, account, ticketReturn.currencyISO);
                    } else if (ticketReturn.transactionStatus.equals(KeysUtil.PAYMENT_PAXWALLET)) {
                        List<String> booIds = new ArrayList<>();
                        for (Map<String, Object> map : listData) {
                            booIds.add(map.get("bookId").toString());
                        }
                        String listBookId = String.join(",", booIds);
                        String payResult = paymentUtil.writeOffDebtFromPaxWallet(requestId, ticketReturn.fleetId, ticketReturn.customerId, customerDebt, ticketReturn.currencyISO, ticketReturn.currencySymbol, listBookId, ticketReturn.bookId);
                        logger.debug(requestId + " - writeOffDebtFromPaxWallet result: " + payResult);
                    }
                }

            }
        } catch (Exception ex) {
            logger.debug(requestId + " - write of customer debt error: " + CommonUtils.getError(ex));
        }

        /*
          Add PaxWalletTransaction for the following cases:
          - collect policy fee when cancel booking which requested payment by pax wallet
          - complete (from CC) and charge booking which requested payment by pax wallet
          - request payment was not by Boost wallet
         */
        try {
            String boostId = booking.psgInfo.boostId != null ? booking.psgInfo.boostId : "";
            //service type
            String jobType = booking.jobType != null ? booking.jobType : "";
            String serviceType = CommonUtils.getJobType(jobType, booking.bookFrom, booking.intercity, booking.delivery);
            List<String> cancelBooking = Arrays.asList(KeysUtil.PAYMENT_CANCELED, KeysUtil.PAYMENT_NOSHOW);
            if (!ticketReturn.intercity && boostId.isEmpty() &&
                    (
                        (!ticketReturn.isPending && ticketReturn.transactionStatus.equals(KeysUtil.PAYMENT_PAXWALLET)) ||
                        (!ticketReturn.isPending && cancelBooking.contains(ticketReturn.transactionStatus) && ticketReturn.paymentType == 13)
                    )
                ) {
                PaxWalletUtil paxWalletUtil = new PaxWalletUtil(requestId);
                double currentBalance = paxWalletUtil.getCurrentBalance(ticketReturn.fleetId, ticketReturn.customerId, "paxWallet", ticketReturn.currencyISO);
                double totalCharged = DF.parse(DF.format(ticketReturn.totalCharged)).doubleValue();
                double newBalance = DF.parse(DF.format(currentBalance - totalCharged)).doubleValue();
                double changeAmount = totalCharged;
                if (isPartial) {
                    newBalance = 0.0;
                    changeAmount = currentBalance;
                }
                PaxWalletTransaction paxWalletTransaction = new PaxWalletTransaction();
                paxWalletTransaction.fleetId = ticketReturn.fleetId;
                paxWalletTransaction.userId = ticketReturn.customerId;
                paxWalletTransaction.transactionType = CommonArrayUtils.PAX_WALLET_TYPE[1];
                paxWalletTransaction.transactionId = ticketReturn.bookId;
                paxWalletTransaction.bookId = ticketReturn.bookId;
                paxWalletTransaction.bookingFare = ticketReturn.total;
                paxWalletTransaction.givenAmount = ticketReturn.transferredAmount;
                paxWalletTransaction.amount = changeAmount;
                paxWalletTransaction.newBalance = newBalance;
                paxWalletTransaction.description = CommonArrayUtils.PAX_WALLET_TYPE[1]+"-"+ticketReturn.bookId;
                paxWalletTransaction.customerName = ticketReturn.customerName;
                paxWalletTransaction.phoneNumber = customerPhone.equals(ticketReturn.customerId) ? "" : customerPhone;
                paxWalletTransaction.currencyISO = ticketReturn.currencyISO;
                paxWalletTransaction.currencySymbol = ticketReturn.currencySymbol;
                paxWalletTransaction.createdDate = TimezoneUtil.getGMTTimestamp();
                paxWalletTransaction.serviceType = serviceType;

                long idAddBalance = sqlDao.addPaxWalletTransaction(paxWalletTransaction);
                if (idAddBalance > 0) {
                    // subtract money from wallet
                    paxWalletUtil.updatePaxWallet(paxWalletTransaction, false);

                    // update wallet balance
                    passengerInfo = mongoDao.getPassengerInfo(ticketReturn.fleetId, ticketReturn.customerId); // passengerInfo will be added at paxWalletUtil.getCurrentBalance
                    List<AmountByCurrency> listWallets = passengerInfo.paxWallet;
                    for (AmountByCurrency wallet : listWallets) {
                        if (wallet.currencyISO != null && wallet.currencyISO.equals(ticketReturn.currencyISO)) {
                            wallet.value = newBalance;
                            break;
                        }
                    }
                    mongoDao.updatePaxWallet(ticketReturn.fleetId, ticketReturn.customerId, listWallets);

                    // update to Reporting
                    UpdatePaxWalletThread updateThread = new UpdatePaxWalletThread();
                    paxWalletTransaction.id = idAddBalance;
                    updateThread.paxWalletTransaction = paxWalletTransaction;
                    updateThread.requestId = requestId;
                    updateThread.start();
                }

            }
        } catch (Exception ex) {
            logger.debug(requestId + " - update PaxWalletTransaction for ride-"+ ticketReturn.bookId +" error: " + CommonUtils.getError(ex));
        }

        /*
          Add PaxWalletTransaction if match all following conditions:
          - not an intercity booking
          - booking requested payment by cash
          - booking requested from pax app
          - receiveAmount > totalFare
          - enable cash excess for partial payment booking
         */
        try {
            if (!ticketReturn.intercity && !KeysUtil.bookNotPax.contains(ticketReturn.bookFrom) &&
                    ticketReturn.transactionStatus.equals(KeysUtil.PAYMENT_CASH)) {
                JSONObject objSetting = paymentUtil.enableCashExcess(ticketReturn.fleetId, ticketReturn.currencyISO);
                boolean enableCashExcess = Boolean.parseBoolean(objSetting.get("enableCashExcess").toString());
                String returnChange = ticketReturn.returnChange != null ? ticketReturn.returnChange : "";
                if ((enableCashExcess && ticketReturn.receivedAmount > ticketReturn.total) || (returnChange.equals("wallet") && ticketReturn.transferredChangeByWallet > 0)) {
                    PaxWalletUtil paxWalletUtil = new PaxWalletUtil(requestId);
                    double currentBalance = paxWalletUtil.getCurrentBalance(ticketReturn.fleetId, ticketReturn.customerId, "paxWallet", ticketReturn.currencyISO);
                    double newBalance = DF.parse(DF.format(currentBalance + ticketReturn.receivedAmount - ticketReturn.total - customerDebt)).doubleValue();
                    logger.debug(requestId + " - newBalance = currentBalance + receivedAmount - total - customerDebt");
                    logger.debug(requestId + " - newBalance = "+ currentBalance +" + "+ ticketReturn.receivedAmount +" - "+ ticketReturn.total +" - "+ customerDebt +" = " + newBalance);
                    double changeAmount = DF.parse(DF.format(newBalance - currentBalance)).doubleValue();
                    logger.debug(requestId + " - changeAmount: " + changeAmount);
                    if (returnChange.equals("wallet")) {
                        changeAmount = ticketReturn.transferredChangeByWallet;
                        newBalance = CommonUtils.getRoundValue(currentBalance + changeAmount);
                    }
                    logger.debug(requestId + " - changeAmount: " + changeAmount);
                    PaxWalletTransaction paxWalletTransaction = new PaxWalletTransaction();
                    paxWalletTransaction.fleetId = ticketReturn.fleetId;
                    paxWalletTransaction.userId = ticketReturn.customerId;
                    paxWalletTransaction.transactionType = CommonArrayUtils.PAX_WALLET_TYPE[6];
                    paxWalletTransaction.transactionId = "";
                    paxWalletTransaction.bookId = ticketReturn.bookId;
                    paxWalletTransaction.amount = changeAmount;
                    paxWalletTransaction.newBalance = newBalance;
                    paxWalletTransaction.description = CommonArrayUtils.PAX_WALLET_TYPE[6]+"-"+ticketReturn.bookId;
                    paxWalletTransaction.customerName = ticketReturn.customerName;
                    paxWalletTransaction.phoneNumber = customerPhone.equals(ticketReturn.customerId) ? "" : customerPhone;
                    paxWalletTransaction.currencyISO = ticketReturn.currencyISO;
                    paxWalletTransaction.currencySymbol = ticketReturn.currencySymbol;
                    double bookingFare = ticketReturn.totalFare;
                    if (returnChange.equals("wallet")) {
                        bookingFare = CommonUtils.getRoundValue(ticketReturn.receivedAmount - ticketReturn.transferredChangeByWallet);
                    }
                    paxWalletTransaction.bookingFare = bookingFare;
                    paxWalletTransaction.givenAmount = ticketReturn.receivedAmount;
                    paxWalletTransaction.createdDate = TimezoneUtil.getGMTTimestamp();

                    long idAddBalance = sqlDao.addPaxWalletTransaction(paxWalletTransaction);
                    if (idAddBalance > 0) {
                        // add money from wallet
                        paxWalletUtil.updatePaxWallet(paxWalletTransaction, true);

                        // update wallet balance
                        passengerInfo = mongoDao.getPassengerInfo(ticketReturn.fleetId, ticketReturn.customerId); // passengerInfo will be added at paxWalletUtil.getCurrentBalance
                        List<AmountByCurrency> listWallets = passengerInfo.paxWallet;
                        for (AmountByCurrency wallet : listWallets) {
                            if (wallet.currencyISO != null && wallet.currencyISO.equals(ticketReturn.currencyISO)) {
                                wallet.value = newBalance;
                                break;
                            }
                        }
                        mongoDao.updatePaxWallet(ticketReturn.fleetId, ticketReturn.customerId, listWallets);

                        // update to Reporting
                        UpdatePaxWalletThread updateThread = new UpdatePaxWalletThread();
                        paxWalletTransaction.id = idAddBalance;
                        updateThread.paxWalletTransaction = paxWalletTransaction;
                        updateThread.requestId = requestId;
                        updateThread.start();
                    }
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - update PaxWalletTransaction for cashExcess-"+ ticketReturn.bookId +" error: " + CommonUtils.getError(ex));
        }

        /*
          Add PaxWalletTransaction if match all following conditions:
          - 1st complete booking of pax
          - pax-refer-pax was set up
         */
        try {
            if (totalCompletedBook == 0 && !KeysUtil.INCOMPLETE_PAYMENT.contains(ticketReturn.transactionStatus)) {
                // check pax-refer-pax program
                String referrerId = (referralHistories != null && referralHistories.userId != null) ? referralHistories.userId : "";
                String referralType = (referralHistories != null && referralHistories.referralType != null) ? referralHistories.referralType : "";
                double amount = getRefererGetAmount(ticketReturn.fleetId, referralType, ticketReturn.currencyISO);
                /*PaxReferral paxReferral = mongoDao.getPaxReferral(ticketReturn.fleetId);
                double amount = 0.0;
                if (referralType.equals(KeysUtil.APPTYPE_PASSENGER) && paxReferral != null) {
                    // get top up amount from setting
                    if (paxReferral.refererGet != null && paxReferral.refererGet.money.size() > 0) {
                        for (AmountByCurrency amountByCurrency : paxReferral.refererGet.money) {
                            if (amountByCurrency.currencyISO.equals(ticketReturn.currencyISO)) {
                                amount = amountByCurrency.value;
                                break;
                            }
                        }
                    }
                }*/

                if (amount > 0 && !referrerId.isEmpty()) {
                    Account account = mongoDao.getAccount(referrerId);
                    Account referee = mongoDao.getAccount(ticketReturn.customerId);

                    PaxWalletUtil paxWalletUtil = new PaxWalletUtil(requestId);
                    double currentBalance = paxWalletUtil.getCurrentBalance(ticketReturn.fleetId, referrerId, "paxWallet", ticketReturn.currencyISO);
                    double newBalance = DF.parse(DF.format(currentBalance + amount)).doubleValue();
                    PaxWalletTransaction paxWalletTransaction = new PaxWalletTransaction();
                    paxWalletTransaction.fleetId = ticketReturn.fleetId;
                    paxWalletTransaction.userId = referrerId;
                    paxWalletTransaction.referralCode = referralCode;
                    paxWalletTransaction.transactionType = CommonArrayUtils.PAX_WALLET_TYPE[9];
                    paxWalletTransaction.transactionId = ticketReturn.bookId;
                    paxWalletTransaction.bookId = ticketReturn.bookId;
                    paxWalletTransaction.amount = amount;
                    paxWalletTransaction.newBalance = newBalance;
                    paxWalletTransaction.description = CommonArrayUtils.PAX_WALLET_TYPE[9]+"-"+ticketReturn.bookId;
                    paxWalletTransaction.customerName = account.fullName.trim();
                    paxWalletTransaction.phoneNumber = account.phone.equals(account.userId) ? "" : account.phone;
                    paxWalletTransaction.refereeName = referee.fullName.trim();
                    paxWalletTransaction.refereePhone = referee.phone.equals(referee.userId) ? "" : referee.phone;
                    paxWalletTransaction.currencyISO = ticketReturn.currencyISO;
                    paxWalletTransaction.currencySymbol = ticketReturn.currencySymbol;
                    paxWalletTransaction.bookingFare = ticketReturn.total;
                    paxWalletTransaction.givenAmount = ticketReturn.receivedAmount;
                    paxWalletTransaction.createdDate = TimezoneUtil.getGMTTimestamp();

                    long idAddBalance = sqlDao.addPaxWalletTransaction(paxWalletTransaction);
                    if (idAddBalance > 0) {
                        // add money from wallet
                        paxWalletUtil.updatePaxWallet(paxWalletTransaction, true);

                        // update wallet balance
                        passengerInfo = mongoDao.getPassengerInfo(ticketReturn.fleetId, referrerId);
                        List<AmountByCurrency> listWallets = passengerInfo.paxWallet;
                        for (AmountByCurrency wallet : listWallets) {
                            if (wallet.currencyISO != null && wallet.currencyISO.equals(ticketReturn.currencyISO)) {
                                wallet.value = newBalance;
                                break;
                            }
                        }
                        mongoDao.updatePaxWallet(ticketReturn.fleetId, referrerId, listWallets);

                        // update to Reporting
                        UpdatePaxWalletThread updateThread = new UpdatePaxWalletThread();
                        paxWalletTransaction.id = idAddBalance;
                        updateThread.paxWalletTransaction = paxWalletTransaction;
                        updateThread.requestId = requestId;
                        updateThread.start();
                    }
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - update PaxWalletTransaction for paxReferPax-"+ ticketReturn.bookId +" error: " + CommonUtils.getError(ex));
        }

    }

    private double collectDeductionFromCreditWallet(double deductAmount, String bookId, Fleet fleet, Account account, String currencyISO) {
        logger.debug(requestId + " - collectDeductionFromCreditWallet with amount = " + deductAmount);
        try {
            boolean matchCurrency = false;
            DriverWallet driverWallet = mongoDao.getDriverWallet(account.userId);
            if (driverWallet != null && driverWallet.creditWallet != null) {
                String currentCurrency = driverWallet.creditWallet.currencyISO != null ? driverWallet.creditWallet.currencyISO : "";
                if (currentCurrency.isEmpty()) {
                    // did not initial wallet - add new one
                    AmountByCurrency creditWallet = new AmountByCurrency();
                    creditWallet.currencyISO = currencyISO;
                    creditWallet.value = 0.0;
                    mongoDao.updateDriverWallet(account.userId, creditWallet);
                } else {
                    matchCurrency = currentCurrency.equals(currencyISO);

                    // if currency does not match - return  driverDeduction = 0
                    if (!matchCurrency) {
                        return 0.0;
                    }
                }
            }

            UpdateDriverCreditBalance updateBalance = new UpdateDriverCreditBalance();
            updateBalance.type = CommonArrayUtils.DRIVER_WALLET_TYPE[1];
            updateBalance.fleetId = fleet.fleetId;
            updateBalance.driverId = account.userId;
            updateBalance.amount = -deductAmount; // deduct fund from wallet => store negative value on history
            updateBalance.currencyISO = currencyISO;
            updateBalance.mongoDao = mongoDao;
            updateBalance.sqlDao = sqlDao;
            updateBalance.reason = "";
            updateBalance.additionalData.put("bookId", bookId);
            updateBalance.requestId = requestId;
            String result = updateBalance.doUpdate();
            logger.debug(requestId + " - result = " + result);
            if (paymentUtil.getReturnCode(result) == 200) {
                return deductAmount;
            }

        } catch (Exception ex) {
            logger.debug(requestId + " - updateDriverDeduction error: " + CommonUtils.getError(ex));
        }
        return 0;
    }

    private void addCashExcessDriverCreditWallet(String bookId, Fleet fleet, Account account, TicketReturn ticket) {
        logger.debug(requestId + " - addCashExcessDriverCreditWallet");
        try {

            WalletUtil walletUtil = new WalletUtil(requestId);
            double currentBalance = walletUtil.getCurrentBalance(account.userId, "credit", ticket.currencyISO);
            logger.debug(requestId + " - balance after deduct commission: " + currentBalance);
            double customerDebt = paymentUtil.getCustomerDebt(requestId, ticket.fleetId, ticket.customerId, booking, ticket.currencyISO, ticket.payFromCC, ticket.writeOffDebt);
            double changeAmount = Double.valueOf(KeysUtil.DECIMAL_FORMAT.format(ticket.receivedAmount - customerDebt - ticket.total));
            logger.debug(requestId + " - changeAmount = receivedAmount - customerDebt - total");
            logger.debug(requestId + " - changeAmount = "+ ticket.receivedAmount + " - " + customerDebt +" - "+ ticket.total +" = " + changeAmount);
            if (changeAmount > 0) {
                double newDriverBalance = Double.valueOf(KeysUtil.DECIMAL_FORMAT.format(currentBalance - changeAmount));
                logger.debug(requestId + " - newDriverBalance = currentBalance - changeAmount");
                logger.debug(requestId + " - newDriverBalance = " + currentBalance + " - " + changeAmount + " = " + newDriverBalance);

                UpdateDriverCreditBalance updateBalance = new UpdateDriverCreditBalance();
                updateBalance.type = CommonArrayUtils.DRIVER_WALLET_TYPE[6];
                updateBalance.fleetId = fleet.fleetId;
                updateBalance.driverId = account.userId;
                updateBalance.amount = changeAmount * (-1); // deduct fund from wallet => store negative value on history
                updateBalance.currencyISO = ticket.currencyISO;
                updateBalance.mongoDao = mongoDao;
                updateBalance.sqlDao = sqlDao;
                updateBalance.reason = "";
                updateBalance.additionalData.put("bookId", bookId);
                updateBalance.additionalData.put("bookingFare", ticket.total);
                updateBalance.additionalData.put("receivedAmount", ticket.receivedAmount);
                updateBalance.requestId = requestId;
                String result = updateBalance.doUpdate();
                logger.debug(requestId + " - result = " + result);
            } else {
                logger.debug(requestId + " - changeAmount = 0 ==> do not initial transfer transaction");
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - addCashExcessDriverCreditWallet error: " + CommonUtils.getError(ex));
        }
    }

    private void deletePaxPromoList(PromotionCode promotionCode, String userId, Fleet fleet, String currencyISO){
        if(promotionCode != null && !promotionCode.applyLimitFinished){
            try {
                List<PromoCodeUse> promoCodeUses = sqlDao.findPromoCodeUseByPromoCodeAndCustomer(promotionCode._id.toString(), userId);
                if(promotionCode.totalUsesLimit.isLimited && promoCodeUses.size()>= promotionCode.totalUsesLimit.value){
                    mongoDao.deletePaxPromoList(promotionCode._id.toString(), userId);
                }
                if(promotionCode.type.equalsIgnoreCase("Amount") && promotionCode.budgetLimit.isLimited){
                    double totalUse = 0;
                    for(PromoCodeUse promoCodeUse:promoCodeUses){
                        totalUse += promoCodeUse.usedValue;
                    }
                    double value = 0;
                    for(AmountByCurrency valueByCurrency : promotionCode.valueByCurrencies){
                        if(currencyISO.equals(valueByCurrency.currencyISO))
                            value = valueByCurrency.value;
                    }
                    totalUse = totalUse + value;
                    if(fleet.multiCurrencies){
                        for(AmountByCurrency valueByCurrency : promotionCode.budgetLimit.valueByCurrencies){
                            if(currencyISO.equals(valueByCurrency.currencyISO)){
                                if(totalUse > valueByCurrency.value){
                                    //Promo code doesn't reach budget limitation for a pax, if turned on
                                    mongoDao.deletePaxPromoList(promotionCode._id.toString(), userId);
                                }
                            }
                        }
                    }else if(totalUse > promotionCode.budgetLimit.valueByCurrencies.get(0).value){
                        //Promo code doesn't reach budget limitation for a pax, if turned on
                        mongoDao.deletePaxPromoList(promotionCode._id.toString(), userId);
                    }
                }
                //referral program
                if(promotionCode.referredCustomers){
                    if(fleet.generalSetting != null && fleet.generalSetting.founderFund != null && fleet.generalSetting.founderFund.firstBooking){
                        if(userId != null && !userId.isEmpty()){
                            //remove promotion code of referral program
                            mongoDao.deletePaxPromoList(promotionCode._id.toString(), userId);
                            ReferralHistories referralHistories = mongoDao.getLinkReferralByRefereeId(userId);
                            if(referralHistories != null ){
                                if(referralHistories.referralType != null && referralHistories.referralType.equals(KeysUtil.APPTYPE_PASSENGER)){
                                    PaxReferral referral = mongoDao.getPaxReferral(fleet.fleetId);
                                    if(referral!= null && referral.refereeGet != null && referral.refereeGet.type != null){
                                        if(referral.refereeGet.type.equals("promo") && referral.refereeGet.promos != null &&
                                                !referral.refereeGet.promos.isEmpty()){
                                            for(RefereePromo p:referral.refereeGet.promos){
                                                mongoDao.deletePaxPromoList(p.promoId, userId);
                                            }
                                        }
                                    }
                                } else {
                                    Referral referral = mongoDao.getReferral(fleet.fleetId);
                                    if(referral!= null && referral.firstPackage != null && referral.firstPackage.promos != null &&
                                            !referral.firstPackage.promos.isEmpty()){
                                        for(RefereePromo p:referral.firstPackage.promos){
                                            mongoDao.deletePaxPromoList(p.promoId, userId);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                logger.debug(requestId + " - deletePaxPromoList error: " + CommonUtils.getError(ex));
            }
        }
    }

    private Map<String, Object> getReferralInfo(String fleetId, Referral referral, String driverId, double subTotal, double netEarning, String currencyISO, int totalCompletedBook, Date pickupTime,
                                                boolean isNewIncentiveReferral, double promoAmount) {
        logger.debug(requestId + " - totalCompletedBook: " + totalCompletedBook);
        Map<String,Object> mapData = new HashMap<>();
        if (referral == null) {
            mapData.put("referralCode", "");
            mapData.put("referralEarning", "0.0");
            mapData.put("totalEarning", "0.0");
            return mapData;
        }

        String referralCode = "";
        ReferralPackage referralPackage = null;
        double referralEarning = 0.0;
        double referralMaxAmount = 0.0;
        boolean isReferralMaxAmount = false;
        // check whether referral is expired
        try {
            boolean isExpired = false;
            if (referral.isExpired) {
                if(referral.expiryDate != null && pickupTime.after(referral.expiryDate) ){
                    isExpired = true;
                }
            }
            Map<String, Object> mapReferral = null;
            if (referral.isActive && !isExpired && isNewIncentiveReferral) {
                ReferralInfo referralInfo = mongoDao.getReferralInfo(fleetId, driverId);
                if (referralInfo != null) {
                    referralCode = referralInfo.code != null ? referralInfo.code : "";
                }
                if (totalCompletedBook == 0 && referral.firstPackage != null) {
                    // first book after link referral
                    mapReferral = getDriverReward(referral.firstPackage, subTotal, currencyISO, promoAmount, referral.paidByDriver);
                    referralPackage = referral.firstPackage;
                } else {
                    ReferralPackage secondPackage = referral.secondPackage;
                    if (secondPackage != null && secondPackage.totalBook != null && totalCompletedBook < secondPackage.totalBook) {
                        mapReferral = getDriverReward(secondPackage, subTotal, currencyISO, promoAmount, referral.paidByDriver);
                        referralPackage = referral.secondPackage;
                    } else {
                        ReferralPackage thirdPackage = referral.thirdPackage;
                        if (thirdPackage != null) {
                            boolean valid = true;
                            if (thirdPackage.isLimited != null && thirdPackage.isLimited && thirdPackage.totalBook != null && totalCompletedBook >= thirdPackage.totalBook){
                                valid = false;
                            }
                            if (valid) {
                                mapReferral = getDriverReward(thirdPackage, subTotal, currencyISO, promoAmount, referral.paidByDriver);
                                referralPackage = referral.thirdPackage;
                            }
                        }
                    }
                }
            }

            if (mapReferral != null) {
                referralEarning = Double.parseDouble(mapReferral.get("reward").toString());
                referralMaxAmount = Double.parseDouble(mapReferral.get("referralMaxAmount").toString());
                isReferralMaxAmount = Boolean.parseBoolean(mapReferral.get("isReferralMaxAmount").toString());
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - getReferralInfo error: " + CommonUtils.getError(ex));
        }

        double totalEarning = referralEarning + netEarning;
        mapData.put("referralCode", referralCode);
        mapData.put("referralEarning", KeysUtil.DECIMAL_FORMAT.format(referralEarning));
        mapData.put("totalEarning", KeysUtil.DECIMAL_FORMAT.format(totalEarning));
        mapData.put("referralPackage", referralPackage);
        mapData.put("referralMaxAmount", referralMaxAmount);
        mapData.put("isReferralMaxAmount", isReferralMaxAmount);
        return mapData;
    }

    private Map<String,Object> getDriverReward(ReferralPackage pacReferralPackage, double subTotal, String currencyISO,
                                               double promoAmount, boolean paidByDriver) {
        logger.debug(requestId + " - pacReferralPackage: " + gson.toJson(pacReferralPackage));
        Map<String,Object> mapData = new HashMap<>();
        double reward = 0.0;
        double referralMaxAmount = 0.0;
        boolean isReferralMaxAmount = false;
        // always calculate earning with subTotal without promo
        logger.debug(requestId + " - subTotal: " + subTotal);
        logger.debug(requestId + " - promoAmount: " + promoAmount);
        if(paidByDriver){
            subTotal = subTotal - promoAmount;
            logger.debug(requestId + " - subTotal without promo: " + subTotal);
        }
        try {
            if (pacReferralPackage.driver != null) {
                String type = pacReferralPackage.driver.type != null ? pacReferralPackage.driver.type : "";
                if (type.equals("money")) {
                    for (AmountByCurrency amountByCurrency : pacReferralPackage.driver.money) {
                        if (amountByCurrency.currencyISO.equals(currencyISO)) {
                            reward = amountByCurrency.value;
                            break;
                        }
                    }
                } else if (type.equals("settlement")) {
                    reward = pacReferralPackage.driver.settlement.value*subTotal/100;
                    // compare with maximum
                    if (pacReferralPackage.driver.settlement.maximum != null && pacReferralPackage.driver.settlement.maximum.size() > 0) {
                        for (AmountByCurrency amountByCurrency : pacReferralPackage.driver.settlement.maximum) {
                            if (amountByCurrency.currencyISO.equals(currencyISO)) {
                                referralMaxAmount = amountByCurrency.value;
                                if (reward > referralMaxAmount && referralMaxAmount > 0) {
                                    reward = referralMaxAmount;
                                    isReferralMaxAmount = true;
                                }
                                break;
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - getDriverReward error: " + CommonUtils.getError(ex));
        }

        mapData.put("reward", reward);
        mapData.put("referralMaxAmount", referralMaxAmount);
        mapData.put("isReferralMaxAmount", isReferralMaxAmount);
        return mapData;
    }

    private boolean checkLimitReferee(ReferralHistories referralHistories, int limitUser) {
        logger.debug(requestId + " - limit: " + limitUser);
        try {
            if (limitUser == 0) {
                // do not limit invitation
                return true;
            } else {
                String referrerId = referralHistories.userId;
                List<ReferralHistories> all = mongoDao.getLinkReferralByUserId(referrerId);
                // if total referee higher than limit then this referee is not valid
                // so check whether total referee is less than or equal with limit
                if (all.size() < limitUser)
                    return true;

                // referee still valid in case it is in the group of referees who completed first booking
                if (all.size() >= limitUser) {
                    int index = 0;
                    int i = 1;
                    String refereeId = referralHistories.refereeId != null ? referralHistories.refereeId : "";
                    for (ReferralHistories histories : all) {
                        if (refereeId.equals(histories.refereeId)) {
                            index = i;
                            break;
                        }
                        i++;
                    }
                    if (index != 0 && index <= limitUser)
                        return true;
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - checkLimitReferee error: " + CommonUtils.getError(ex));
        }
        return false;
    }

    private double getRefererGetAmount(String fleetId, String referralType, String currencyISO){
        PaxReferral paxReferral = mongoDao.getPaxReferral(fleetId);
        double amount = 0.0;
        if (referralType.equals(KeysUtil.APPTYPE_PASSENGER) && paxReferral != null) {
            // get top up amount from setting
            if (paxReferral.refererGet != null && paxReferral.refererGet.money.size() > 0) {
                for (AmountByCurrency amountByCurrency : paxReferral.refererGet.money) {
                    if (amountByCurrency.currencyISO.equals(currencyISO)) {
                        amount = amountByCurrency.value;
                        break;
                    }
                }
            }
        }
        return amount;
    }

    private void collectItemValueFromPaxWallet(TicketReturn ticketReturn) {
        try {
            // deduct itemValue from pax wallet
            String customerId = booking.psgInfo.userId != null ? booking.psgInfo.userId : "";
            String customerPhone = booking.psgInfo.phone != null ? booking.psgInfo.phone : "";
            PaxWalletUtil paxWalletUtil = new PaxWalletUtil(requestId);
            double currentBalance = paxWalletUtil.getCurrentBalance(booking.fleetId, customerId, "paxWallet", booking.currencyISO);
            double newBalance = DF.parse(DF.format(currentBalance - ticketReturn.itemValue)).doubleValue();

            PaxWalletTransaction paxWalletTransaction = new PaxWalletTransaction();
            paxWalletTransaction.fleetId = ticketReturn.fleetId;
            paxWalletTransaction.userId = ticketReturn.customerId;
            paxWalletTransaction.transactionType = CommonArrayUtils.PAX_WALLET_TYPE[11];
            paxWalletTransaction.transactionId = ticketReturn.bookId;
            paxWalletTransaction.bookId = ticketReturn.bookId;
            paxWalletTransaction.bookingFare = ticketReturn.total;
            paxWalletTransaction.givenAmount = 0.0;
            paxWalletTransaction.amount = ticketReturn.itemValue;
            paxWalletTransaction.newBalance = newBalance;
            paxWalletTransaction.description = CommonArrayUtils.PAX_WALLET_TYPE[11] + "-" + ticketReturn.bookId;
            paxWalletTransaction.customerName = ticketReturn.customerName;
            paxWalletTransaction.phoneNumber = customerPhone.equals(ticketReturn.customerId) ? "" : customerPhone;
            paxWalletTransaction.currencyISO = ticketReturn.currencyISO;
            paxWalletTransaction.currencySymbol = ticketReturn.currencySymbol;
            paxWalletTransaction.createdDate = TimezoneUtil.getGMTTimestamp();

            long idAddBalance = sqlDao.addPaxWalletTransaction(paxWalletTransaction);
            if (idAddBalance > 0) {
                // deduct money from wallet
                paxWalletUtil.updatePaxWallet(paxWalletTransaction, false);

                // update wallet balance
                PassengerInfo passengerInfo = mongoDao.getPassengerInfo(ticketReturn.fleetId, ticketReturn.customerId); // passengerInfo will be added at paxWalletUtil.getCurrentBalance
                List<AmountByCurrency> listWallets = passengerInfo.paxWallet;
                for (AmountByCurrency wallet : listWallets) {
                    if (wallet.currencyISO != null && wallet.currencyISO.equals(ticketReturn.currencyISO)) {
                        wallet.value = newBalance;
                        break;
                    }
                }
                mongoDao.updatePaxWallet(ticketReturn.fleetId, ticketReturn.customerId, listWallets);

                // update to Reporting
                UpdatePaxWalletThread updateThread = new UpdatePaxWalletThread();
                paxWalletTransaction.id = idAddBalance;
                updateThread.paxWalletTransaction = paxWalletTransaction;
                updateThread.requestId = requestId;
                updateThread.start();
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - collectItemValueFromPaxWallet error: " + CommonUtils.getError(ex));
        }
    }

    private boolean doTransferToStripeConnect(Booking booking, double transferAmount, String type) {
        boolean success = false;
        try {
            if (booking.fleetId.equals(booking.request.psgFleetId)) {
                Fleet fleet = mongoDao.getFleetInfor(booking.fleetId);
                ConfigGateway configGateway = paymentUtil.getConfigGateway(fleet, "", booking.request.pickup.geo);
                String gateway = configGateway != null ? configGateway.gateway : "";
                if (gateway.equals(KeysUtil.STRIPE)) {
                    boolean instantPayout = false;
                    String achToken = "";
                    if (type.equals("driver")) {
                        String userId = booking.drvInfo != null && booking.drvInfo.userId != null ? booking.drvInfo.userId : "";
                        if (!userId.isEmpty()) {
                            Account driver = mongoDao.getAccount(userId);
                            String stripeConnectStatus = driver.driverInfo.stripeConnectStatus != null ? driver.driverInfo.stripeConnectStatus : "";
                            instantPayout = fleet.drvPayout.gateway.equals("StripeConnect") && stripeConnectStatus.equals("activated");
                            achToken = driver.driverInfo.achToken != null ? driver.driverInfo.achToken : "";
                        }
                    } else { // type = company
                        String companyId = booking.drvInfo != null && booking.drvInfo.companyId != null ? booking.drvInfo.companyId : "";
                        if (!companyId.isEmpty()) {
                            Company company = mongoDao.getCompanyById(booking.drvInfo.companyId);
                            String stripeConnectStatus = company.connectStatus != null ? company.connectStatus : "";
                            instantPayout = fleet.drvPayout.gateway.equals("StripeConnect") && stripeConnectStatus.equals("activated");
                            achToken = company.connectToken != null ? company.connectToken : "";
                        }
                    }

                    logger.debug(requestId + " - instantPayout: " + instantPayout + " with amount " + transferAmount + " for " + type + " token " + achToken);
                    if (instantPayout) {
                        List<String> captureTransactions = redisDao.getStripeCaptured(requestId, booking.bookId);
                        logger.debug(requestId + " - captureTransactions: " + captureTransactions.size());
                        logger.debug(requestId + " - captureTransactions: " + gson.toJson(captureTransactions));
                        GatewayStripe gatewayStripe = mongoDao.getGatewayStripeFleet(booking.fleetId);
                        StripeUtil stripeUtil = new StripeUtil(requestId);
                        if (captureTransactions.size() == 1) {
                            logger.debug(requestId + " - cache data: " + captureTransactions.get(0));
                            String[] data = captureTransactions.get(0).split(Pattern.quote("|"));
                            double amount = Double.parseDouble(data[1]);
                            if (amount >= transferAmount) {
                                String result = stripeUtil.doTransfer(gatewayStripe.secretKey, data[0], booking.bookId, achToken, transferAmount, booking.currencyISO);
                                if (CommonUtils.getReturnCode(result) == 200) {
                                    success = true;
                                    if (type.equals("driver")) {
                                        // update transaction with remain amount to transfer company later if enable
                                        redisDao.removeStripeCaptured(requestId, booking.bookId, captureTransactions.get(0));
                                        double remain = CommonUtils.getRoundValue(amount - transferAmount);
                                        redisDao.cacheStripeTransaction(requestId, booking.bookId, data[0] + "|" + remain);
                                    }
                                }
                            }
                        } else if (captureTransactions.size() > 1) {
                            // check if remain amount is sufficient
                            double sum = 0.0;
                            for (String data : captureTransactions) {
                                String[] arr = data.split(Pattern.quote("|"));
                                sum += Double.parseDouble(arr[1]);
                            }
                            logger.debug(requestId + " - sum: " + sum);
                            if (sum >= transferAmount) {
                                double totalTransfer = 0;
                                for (String data : captureTransactions) {
                                    logger.debug(requestId + " - cache data: " + data);
                                    String[] arr = data.split(Pattern.quote("|"));
                                    double amount = Double.parseDouble(arr[1]);
                                    double remain = transferAmount - totalTransfer;
                                    double transferValue;
                                    boolean updateCache = false;
                                    if (remain >= amount) {
                                        transferValue = amount;
                                    } else {
                                        if (remain - amount < 0) {
                                            transferValue = remain;
                                        } else {
                                            transferValue = remain - amount;
                                        }
                                        updateCache = true;
                                    }
                                    String result = stripeUtil.doTransfer(gatewayStripe.secretKey, arr[0], booking.bookId, achToken, transferValue, booking.currencyISO);
                                    if (CommonUtils.getReturnCode(result) == 200) {
                                        success = true;
                                        totalTransfer += transferValue;
                                        if (type.equals("driver")) {
                                            redisDao.removeStripeCaptured(requestId, booking.bookId, data);
                                            // update transaction with remain amount to transfer company later if enable
                                            if (updateCache) {
                                                double remainAmount = CommonUtils.getRoundValue(amount - transferValue);
                                                redisDao.cacheStripeTransaction(requestId, booking.bookId, arr[0] + "|" + remainAmount);
                                            }
                                        }
                                    }
                                    logger.debug(requestId + " - totalTransfer: " + totalTransfer);
                                    if (CommonUtils.getRoundValue(totalTransfer) == transferAmount) {
                                        success = true;
                                        // transfer enough money, break the loop
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - doTransferToDriver exception: " + CommonUtils.getError(ex));
        }

        return success;
    }

    public List<Double> getTransferAmount(Fleet fleet, Booking booking, TicketReturn ticketReturn, boolean shortTrip, boolean isCompanyConnected, String separateTax,
                                          String driverEarningType, double editedDriverEarning) {
        List<Double> listValue = new ArrayList<>();
        double fleetAmount = 0.0;
        double driverAmount = 0.0;
        double companyAmount = 0.0;
        double fleetTax = 0.0;
        double driverTax = 0.0;
        double companyTax = 0.0;
        double techFee = ticketReturn.techFee;
        try {
            // get fleet commission from driver profile
            Account account = mongoDao.getAccount(booking.drvInfo.userId);
            //get pickup zone
            String zoneId = "";
            Zone zone = mongoDao.findByFleetIdAndGeo(fleet.fleetId, booking.request.pickup.geo);
            if (zone != null) {
                zoneId = zone._id.toString();
            }
            String commissionType = "";
            double commission = 0;
            String companyCommissionType = "";
            double companyCommission = 0.0;
            if (account != null && account.driverInfo != null) {
                Map<String, Object> mapCommission = estimateUtil.getFleetCommission(requestId, fleet, account, booking, zoneId);
                commissionType = mapCommission.get("commissionType").toString();
                commission = Double.parseDouble(mapCommission.get("commission").toString());
                if (isCompanyConnected) {
                    companyCommissionType = account.driverInfo.commissionCompanyType != null ? account.driverInfo.commissionCompanyType : "default";
                    if (!companyCommissionType.equals("default")) {
                        // get company commission in case setting customize commission
                        String jobType = booking.jobType != null ? booking.jobType : "";
                        String serviceType = CommonUtils.getJobType(jobType, booking.bookFrom, booking.intercity, booking.delivery);
                        companyCommission = account.driverInfo.commissionCompanyValue.stream().filter(c -> c.serviceType.equals(serviceType)).mapToDouble(c -> c.value).sum();
                    }
                }
            }
            logger.debug(requestId + " - commission type: " + commissionType + " --- value: " + commission);
            if (isCompanyConnected) {
                String companyId = booking.drvInfo != null && booking.drvInfo.companyId != null ? booking.drvInfo.companyId : "";
                if (!companyId.isEmpty()) {
                    Company company = mongoDao.getCompanyById(companyId);
                    if (company != null) {
                        // get company commission for default setting
                        if (companyCommissionType.equals("default"))
                            companyCommission = company.commissionValue;
                    }
                }
            }
            logger.debug(requestId + " - companyCommission type: " + companyCommissionType + " --- companyCommission: " + companyCommission);

            boolean typeAmount = commissionType.equalsIgnoreCase("amount");

            double tollFeeFleet = 0.0;
            double parkingFeeFleet = 0.0;
            double gasFeeFleet = 0.0;
            double tollFee = 0.0;
            double parkingFee = 0.0;
            double gasFee = 0.0;
            double airportFeeFleet = 0.0;
            double airportFee = 0.0;
            double meetDriverFeeFleet = 0.0;
            double meetDriverFee = 0.0;
            double fleetCommissionFromFleetServiceFee = 0.0;
            double driverCommissionFromFleetServiceFee = 0.0;
            double fleetServiceTax = 0.0;
            double subTotal = ticketReturn.subTotal;
            double tax = ticketReturn.tax;
            double payToDriver = 0;
            double cancelEarning = 0.0;
            if (KeysUtil.INCOMPLETE_PAYMENT.contains(ticketReturn.transactionStatus)) {
                String getCancelData = estimateUtil.getCancelAmount(booking, fleet, booking.status, "", requestId);
                JSONObject resultObject = paymentUtil.toObjectResponse(getCancelData);
                double fleetCommission = 0.0;
                double policyAmount = 0.0;
                if (resultObject != null && resultObject.get("returnCode") != null) {
                    if (Integer.parseInt(resultObject.get("returnCode").toString()) == 200) {
                        JSONObject objResponse = (JSONObject) resultObject.get("response");
                        tax = Double.parseDouble(objResponse.get("taxFee").toString());
                        cancelEarning = Double.parseDouble(objResponse.get("cancelEarning").toString());
                        fleetCommission = Double.parseDouble(objResponse.get("fleetCommission").toString());
                        payToDriver = Double.parseDouble(objResponse.get("payToDriver").toString());
                        policyAmount = Double.parseDouble(objResponse.get("policyAmount").toString());
                    }
                }
                logger.debug(requestId + " - taxFee: " + tax);
                logger.debug(requestId + " - cancelEarning: " + cancelEarning);
                logger.debug(requestId + " - fleetCommission: " + fleetCommission);
                logger.debug(requestId + " - payToDriver: " + payToDriver);
                logger.debug(requestId + " - commissionType: " + commissionType);
                logger.debug(requestId + " - subTotal: " + ticketReturn.subTotal);
                logger.debug(requestId + " - policyAmount: " + policyAmount);
                if (separateTax.equals("fleetTax")) {
                    if (payToDriver == 1) { // pay to driver fixAmount
                        logger.debug(requestId + " - pay to driver fixAmount");
                        driverTax = paymentUtil.getDriverTax(requestId, policyAmount, tax, fleetCommission, "amount");
                    } else {
                        driverTax = paymentUtil.getDriverTax(requestId, ticketReturn.subTotal, tax, commission, commissionType);
                    }
                }
            } else {
                FleetFare fleetFare = mongoDao.getFleetFare(fleet.fleetId);
                ServiceFee sF = mongoDao.getServiceFeeByFleetIdAndZoneId(fleet.fleetId, zoneId);
                double taxSetting = 0.0;
                String applyType = fleetFare.applyType != null ? fleetFare.applyType : "all";
                if(sF != null && fleet.generalSetting.allowCongfigSettingForEachZone && !applyType.equals("all")){
                    if (sF.tollFeePayTo == 0) {
                        tollFee = ticketReturn.tollFee;
                    } else {
                        tollFeeFleet = ticketReturn.tollFee;
                    }
                    if (sF.parkingFeePayTo == 0) {
                        parkingFee = ticketReturn.parkingFee;
                    } else {
                        parkingFeeFleet = ticketReturn.parkingFee;
                    }
                    if (sF.gasFeePayTo == 0) {
                        gasFee = ticketReturn.gasFee;
                    } else {
                        gasFeeFleet = ticketReturn.gasFee;
                    }
                    if (fleet.fleetDeduction.enableTax) {
                        taxSetting = sF.tax;
                    }
                } else {
                    if (fleetFare.tollFeePayTo == 0) {
                        tollFee = ticketReturn.tollFee;
                    } else {
                        tollFeeFleet = ticketReturn.tollFee;
                    }
                    if (fleetFare.parkingFeePayTo == 0) {
                        parkingFee = ticketReturn.parkingFee;
                    } else {
                        parkingFeeFleet = ticketReturn.parkingFee;
                    }
                    if (fleetFare.gasFeePayTo == 0) {
                        gasFee = ticketReturn.gasFee;
                    } else {
                        gasFeeFleet = ticketReturn.gasFee;
                    }
                    if (fleet.fleetDeduction.enableTax) {
                        taxSetting = fleetFare.tax;
                    }
                }

                if (fleetFare != null) {
                    if (fleetFare.airport != null) {
                        if (fleetFare.airport.payTo == 0) {
                            airportFee = ticketReturn.airportSurcharge;
                        } else {
                            airportFeeFleet = ticketReturn.airportSurcharge;
                        }
                    }
                    if (fleetFare.meetDriver != null) {
                        if (fleetFare.meetDriver.payTo == 0) {
                            meetDriverFee = ticketReturn.meetDriverFee;
                        } else {
                            meetDriverFeeFleet = ticketReturn.meetDriverFee;
                        }
                    }
                }

                List<Double> fleetServiceValues = paymentUtil.getFleetServiceFees(requestId, booking, taxSetting, ticketReturn.subTotal);
                fleetCommissionFromFleetServiceFee = fleetServiceValues.get(1);
                driverCommissionFromFleetServiceFee = fleetServiceValues.get(2);
                fleetServiceTax = fleetServiceValues.get(3);
                logger.debug(requestId + " - fleetCommissionFromFleetServiceFee: " + fleetCommissionFromFleetServiceFee);
                logger.debug(requestId + " - driverCommissionFromFleetServiceFee: " + driverCommissionFromFleetServiceFee);
                if (fleetServiceTax > 0) {
                    logger.debug(requestId + " - fleetServiceTax: " + fleetServiceTax);
                    fleetCommissionFromFleetServiceFee += fleetServiceTax;
                    logger.debug(requestId + " - fleetCommissionFromFleetServiceFee after add fleetServiceTax: " + fleetCommissionFromFleetServiceFee);
                }

            }
            logger.debug(requestId + " - subTotal: " + subTotal);
            logger.debug(requestId + " - tax: " + tax);
            if (isCompanyConnected) {
                if (commission == 0.0) {
                    if (KeysUtil.INCOMPLETE_PAYMENT.contains(ticketReturn.transactionStatus) && payToDriver == 1) {
                        // pay to driver a fixed amount, company will take all amount + tax
                        driverAmount = cancelEarning;
                        driverTax = 0.0;
                        companyTax = tax;
                        companyAmount = subTotal - driverAmount + tax;
                    } else {
                        logger.debug(requestId + " - fleet did not collect commission from driver - share between company and driver");
                        double companyPortion = subTotal * companyCommission / 100;
                        companyTax = tax * companyCommission / 100;
                        driverTax = tax - companyTax;
                        if (!driverEarningType.equals("default")) {
                            driverAmount = editedDriverEarning + driverTax + tollFee + parkingFee + gasFee + airportFee + meetDriverFee + driverCommissionFromFleetServiceFee;
                            companyAmount = subTotal - editedDriverEarning + techFee + companyTax + tollFeeFleet + parkingFeeFleet + gasFeeFleet + airportFeeFleet + meetDriverFeeFleet + fleetCommissionFromFleetServiceFee + ticketReturn.creditTransactionFee;
                        } else {
                            companyAmount = companyPortion + techFee + companyTax + tollFeeFleet + parkingFeeFleet + gasFeeFleet + airportFeeFleet + meetDriverFeeFleet + fleetCommissionFromFleetServiceFee + ticketReturn.creditTransactionFee;
                            double driverPortion = subTotal - companyPortion;
                            driverAmount = driverPortion + driverTax + tollFee + parkingFee + gasFee + airportFee + meetDriverFee + driverCommissionFromFleetServiceFee;
                        }
                    }
                } else {
                    if (KeysUtil.INCOMPLETE_PAYMENT.contains(ticketReturn.transactionStatus) && payToDriver == 1) {
                        // pay to driver a fixed amount, company will take all amount + tax
                        driverAmount = cancelEarning;
                        driverTax = 0.0;
                        companyTax = tax;
                        companyAmount = subTotal - driverAmount + tax;
                    } else {
                        logger.debug(requestId + " - fleet - company - driver share portion");
                        if (separateTax.equals("allTax")) {
                            double fleetPortion = typeAmount ? commission : subTotal * commission / 100;
                            fleetTax = tax;
                            fleetAmount = fleetPortion + fleetTax;
                            if (!driverEarningType.equals("default")) {
                                driverAmount = editedDriverEarning + tollFee + parkingFee + gasFee + airportFee + meetDriverFee + driverCommissionFromFleetServiceFee;
                                companyAmount = subTotal - fleetAmount - editedDriverEarning + techFee + tollFeeFleet + parkingFeeFleet + gasFeeFleet + airportFeeFleet + meetDriverFeeFleet + fleetCommissionFromFleetServiceFee + ticketReturn.creditTransactionFee;
                            } else {
                                double companyPortion = (subTotal - fleetPortion) * companyCommission / 100;
                                companyAmount = companyPortion + techFee + tollFeeFleet + parkingFeeFleet + gasFeeFleet + airportFeeFleet + meetDriverFeeFleet + fleetCommissionFromFleetServiceFee + ticketReturn.creditTransactionFee;

                                double driverPortion = (subTotal - fleetPortion) - companyPortion;
                                driverAmount = driverPortion + tollFee + parkingFee + gasFee + airportFee + meetDriverFee + driverCommissionFromFleetServiceFee;
                            }
                        } else {
                            if (shortTrip) {
                                driverTax = tax;
                            } else {
                                if (typeAmount) {
                                    fleetTax = tax;
                                } else {
                                    fleetTax = tax * commission / 100;
                                    companyTax = (tax - fleetTax) * companyCommission / 100;
                                    driverTax = (tax - fleetTax) - companyTax;
                                }
                            }
                            double fleetPortion = typeAmount ? commission : subTotal * commission / 100;
                            fleetAmount = fleetPortion + fleetTax;
                            if (!driverEarningType.equals("default")) {
                                driverAmount = editedDriverEarning + driverTax + tollFee + parkingFee + gasFee + airportFee + meetDriverFee + driverCommissionFromFleetServiceFee;
                                companyAmount = subTotal - fleetAmount - editedDriverEarning + techFee + companyTax + tollFeeFleet + parkingFeeFleet + gasFeeFleet + airportFeeFleet + meetDriverFeeFleet + fleetCommissionFromFleetServiceFee + ticketReturn.creditTransactionFee;
                            } else {
                                double companyPortion = (subTotal - fleetPortion) * companyCommission / 100;
                                companyAmount = companyPortion + techFee + companyTax + tollFeeFleet + parkingFeeFleet + gasFeeFleet + airportFeeFleet + meetDriverFeeFleet + fleetCommissionFromFleetServiceFee + ticketReturn.creditTransactionFee;

                                double driverPortion = (subTotal - fleetPortion) - companyPortion;
                                driverAmount = driverPortion + driverTax + tollFee + parkingFee + gasFee + airportFee + meetDriverFee + driverCommissionFromFleetServiceFee;
                            }
                        }
                    }
                }
            } else {
                logger.debug(requestId + " - company did not active Stripe connect - share between fleet and driver");
                if (separateTax.equals("allTax")) {
                    if (!driverEarningType.equals("default")) {
                        driverAmount = editedDriverEarning + tollFee + parkingFee + gasFee + airportFee + meetDriverFee + driverCommissionFromFleetServiceFee;
                        fleetAmount = subTotal - editedDriverEarning;
                    } else {
                        double fleetPortion = typeAmount ? commission : subTotal * commission / 100;
                        fleetTax = tax;
                        fleetAmount = fleetPortion + techFee + fleetTax + tollFeeFleet + parkingFeeFleet + gasFeeFleet + airportFeeFleet + meetDriverFeeFleet + fleetCommissionFromFleetServiceFee + ticketReturn.creditTransactionFee;

                        double driverPortion = typeAmount ? subTotal - commission : subTotal * (100 - commission) / 100;
                        driverAmount = driverPortion + tollFee + parkingFee + gasFee + airportFee + meetDriverFee + driverCommissionFromFleetServiceFee;
                    }
                } else {
                    if (shortTrip) {
                        fleetTax = 0.0;
                        driverTax = tax;
                    } else {
                        if (typeAmount) {
                            fleetTax = tax;
                            driverTax = 0.0;
                        } else {
                            fleetTax = tax * commission / 100;
                            driverTax = tax * (100 - commission) / 100;
                        }
                    }
                    if (!driverEarningType.equals("default")) {
                        driverAmount = editedDriverEarning + driverTax + tollFee + parkingFee + gasFee + airportFee + meetDriverFee + driverCommissionFromFleetServiceFee;
                        fleetAmount = subTotal - editedDriverEarning;
                    } else {
                        double fleetPortion = typeAmount ? commission : subTotal * commission / 100;
                        fleetAmount = fleetPortion + fleetTax + tollFeeFleet + parkingFeeFleet + gasFeeFleet + airportFeeFleet + meetDriverFeeFleet + fleetCommissionFromFleetServiceFee + ticketReturn.creditTransactionFee;

                        double driverPortion = typeAmount ? subTotal - commission : subTotal * (100 - commission) / 100;
                        driverAmount = driverPortion + driverTax + tollFee + parkingFee + gasFee + airportFee + meetDriverFee + driverCommissionFromFleetServiceFee;
                    }
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - getTransferAmount exception: " + CommonUtils.getError(ex));
        }

        if (ticketReturn.tip > 0) {
            logger.debug(requestId + " - tip: " + ticketReturn.tip);
            driverAmount += ticketReturn.tip;
        }

        logger.debug(requestId + " - fleetAmount: " + fleetAmount);
        logger.debug(requestId + " - driverAmount: " + CommonUtils.getRoundDown(driverAmount));
        logger.debug(requestId + " - companyAmount: " + CommonUtils.getRoundDown(companyAmount));
        logger.debug(requestId + " - fleetTax: " + fleetTax);
        logger.debug(requestId + " - driverTax: " + driverTax);
        logger.debug(requestId + " - companyTax: " + companyTax);
        listValue.add(CommonUtils.getRoundValue(fleetAmount));
        listValue.add(CommonUtils.getRoundDown(driverAmount));
        listValue.add(CommonUtils.getRoundDown(companyAmount));
        listValue.add(CommonUtils.getRoundValue(fleetTax));
        listValue.add(CommonUtils.getRoundValue(driverTax));
        listValue.add(CommonUtils.getRoundValue(companyTax));
        return listValue;
    }

    private double getStripeFee(String bookId, String fleetId) {
        double fee = 0.0;
        try {
            logger.debug(requestId + " - bookId: " + bookId);
            GatewayStripe gatewayStripe = mongoDao.getGatewayStripeFleet(fleetId);
            StripeUtil stripeUtil = new StripeUtil(requestId);
            List<String> captureTransactions = redisDao.getStripeCaptured(requestId, bookId);
            for (String data : captureTransactions) {
                String[] arr = data.split(Pattern.quote("|"));
                fee +=  stripeUtil.getStripeFee(gatewayStripe.secretKey, arr[0]);
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - getStripeFee exception: " + CommonUtils.getError(ex));
        }
        return CommonUtils.getRoundValue(fee);
    }

    private double deductionCustomerDebtFromCreditWallet(double customerDebt, String bookId, Fleet fleet, Account account, String currencyISO) {
        logger.debug(requestId + " - deductionCustomerDebtFromCreditWallet with amount = " + customerDebt);
        try {
            boolean matchCurrency = false;
            DriverWallet driverWallet = mongoDao.getDriverWallet(account.userId);
            if (driverWallet != null && driverWallet.creditWallet != null) {
                String currentCurrency = driverWallet.creditWallet.currencyISO != null ? driverWallet.creditWallet.currencyISO : "";
                if (currentCurrency.isEmpty()) {
                    // did not initial wallet - add new one
                    AmountByCurrency creditWallet = new AmountByCurrency();
                    creditWallet.currencyISO = currencyISO;
                    creditWallet.value = 0.0;
                    mongoDao.updateDriverWallet(account.userId, creditWallet);
                } else {
                    matchCurrency = currentCurrency.equals(currencyISO);

                    // if currency does not match - return  driverDeduction = 0
                    if (!matchCurrency) {
                        return 0.0;
                    }
                }
            }

            UpdateDriverCreditBalance updateBalance = new UpdateDriverCreditBalance();
            updateBalance.type = CommonArrayUtils.DRIVER_WALLET_TYPE[19];
            updateBalance.fleetId = fleet.fleetId;
            updateBalance.driverId = account.userId;
            updateBalance.amount = -customerDebt; // deduct fund from wallet => store negative value on history
            updateBalance.currencyISO = currencyISO;
            updateBalance.mongoDao = mongoDao;
            updateBalance.sqlDao = sqlDao;
            updateBalance.reason = "";
            updateBalance.additionalData.put("bookId", bookId);
            updateBalance.requestId = requestId;
            String result = updateBalance.doUpdate();
            logger.debug(requestId + " - result = " + result);
            if (paymentUtil.getReturnCode(result) == 200) {
                return customerDebt;
            }

        } catch (Exception ex) {
            logger.debug(requestId + " - deductionCustomerDebtFromCreditWallet error: " + CommonUtils.getError(ex));
        }
        return 0;
    }
}
