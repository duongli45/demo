package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.google.gson.Gson;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.ValidCreditCard;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.Base64Encoder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.CharacterData;
import org.w3c.dom.*;
import org.xml.sax.InputSource;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class CXPayUtil {
    Gson gson = new Gson();
    private String path;
    private String apiKey;
    private String requestId;
    final static Logger logger = LogManager.getLogger(CXPayUtil.class);
    public CXPayUtil(String _requestId, String apiKey, String path) {
        this.path = path;
        this.apiKey = apiKey;
        requestId = _requestId;
    }

    public String initiateCreditForm(String orderId, String firstName, String lastName, String customerEmail, String customerPhone, String userId) {
        ObjResponse objResponse = new ObjResponse();
        try {
            String callback = ServerConfig.hook_server + "/notification/" + KeysUtil.CXPAY + "?gwOrderid="+orderId;
            String query = "<add-customer>" +
                    "  <api-key>"+ apiKey +"</api-key>" +
                    "  <redirect-url>"+ callback +"</redirect-url>" +
                    "  <billing>" +
                    "    <first-name>"+ firstName +"</first-name>" +
                    "    <last-name>"+ lastName +"</last-name>" +
                    "    <phone>"+ customerPhone +"</phone>" +
                    "    <email>"+ customerEmail +"</email>" +
                    "  </billing>" +
                    "</add-customer>";
            Document doc = doRequest(query);
            String result = "";
            String formUrl = "";
            String message = "";
            /*
            <response>
               <result>1</result>
               <result-text>Customer Added</result-text>
               <result-code>100</result-code>
               <form-url>https://cxpay.transactiongateway.com/api/v2/three-step/6b4t1b06</form-url>
            </response>
             */
            NodeList nodes = doc.getElementsByTagName("response");
            for (int i = 0; i < nodes.getLength(); i++) {
                Element element = (Element) nodes.item(i);

                NodeList resultNode = element.getElementsByTagName("result");
                Element line = (Element) resultNode.item(0);
                result = getCharacterDataFromElement(line);

                NodeList formUrlNode = element.getElementsByTagName("form-url");
                line = (Element) formUrlNode.item(0);
                formUrl = getCharacterDataFromElement(line);

                NodeList messageNode = element.getElementsByTagName("result-text");
                if (messageNode != null) {
                    line = (Element) messageNode.item(0);
                    message = getCharacterDataFromElement(line);
                }
            }
            String transactionId = "";
            if (result.equals("1")) {
                String[] urlArr = formUrl.split("/");
                if (urlArr.length > 0) {
                    transactionId = urlArr[urlArr.length-1];
                }
            }
            Map<String,String> mapResponse = new HashMap<>();
            if (!transactionId.isEmpty()) {
                objResponse.returnCode = 200;
                //creditForm = http://localhost:3000/?action_page=https://cxpay.transactiongateway.com/api/v2/three-step/3c340b2s&billingEmail=
                //String cacheAPI =
                String cacheAPI = ServerConfig.hook_server + "/notification/" + KeysUtil.CXPAY+"?urlAction=cache";
                String creditForm = ServerConfig.webform_cxpay + "?action_page=" + URLEncoder.encode(formUrl, "UTF-8")
                        + "&billingEmail=" + customerEmail
                        + "&userId=" + userId
                        + "&cacheAPI=" + URLEncoder.encode(cacheAPI, "UTF-8");
                mapResponse.put("3ds_url", creditForm);
                mapResponse.put("type", "link");
                mapResponse.put("transactionId", transactionId);
                objResponse.response = mapResponse;
                return objResponse.toString();
            } else {
                objResponse.returnCode = 525;

                mapResponse.put("message", message);
                objResponse.response = mapResponse;
            }
            return objResponse.toString();
        } catch (Exception ex) {
            logger.debug(requestId + " - initiateCreditForm exception: " + CommonUtils.getError(ex));
            return GatewayUtil.returnError(ex, 525);
        }
    }

    public String createCreditToken(String tokenId) {
        ObjResponse objResponse = new ObjResponse();
        try {
            String query = "<complete-action>" +
                    "  <api-key>"+ apiKey +"</api-key>" +
                    "  <token-id>"+ tokenId +"</token-id>" +
                    "</complete-action>";
            Document doc = doRequest(query);

            /*
            <response>
               <result>1</result>
               <result-text>OK</result-text>
               <action-type>add-customer</action-type>
               <result-code>100</result-code>
               <amount>0.00</amount>
               <customer-id>195801610</customer-id>
               <customer-vault-id>195801610</customer-vault-id>
               <billing>
                  <billing-id>200635009</billing-id>
                  <first-name>fName</first-name>
                  <last-name>lName</last-name>
                  <phone>+84905123456</phone>
                  <email>mail@mail.com</email>
                  <cc-number>411111******1111</cc-number>
                  <cc-exp>1227</cc-exp>
                  <priority>1</priority>
               </billing>
               <shipping>
                  <shipping-id>1244420559</shipping-id>
                  <priority>1</priority>
               </shipping>
            </response>
             */
            String result = "";
            String token = "";
            String creditCard = "";
            String cardType = "";
            String message = "";
            NodeList nodes = doc.getElementsByTagName("response");
            for (int i = 0; i < nodes.getLength(); i++) {
                Element element = (Element) nodes.item(i);

                NodeList resultNode = element.getElementsByTagName("result");
                Element line = (Element) resultNode.item(0);
                result = getCharacterDataFromElement(line);

                NodeList tokenNode = element.getElementsByTagName("customer-vault-id");
                line = (Element) tokenNode.item(0);
                token = getCharacterDataFromElement(line);

                NodeList messageNode = element.getElementsByTagName("result-text");
                if (messageNode != null) {
                    line = (Element) messageNode.item(0);
                    message = getCharacterDataFromElement(line);
                }

                NodeList nodeBilling = element.getElementsByTagName("billing");
                for (int j = 0; j < nodeBilling.getLength(); j++) {
                    Element elementBilling = (Element) nodeBilling.item(j);

                    NodeList ccNumberNode = elementBilling.getElementsByTagName("cc-number");
                    Element lineCC = (Element) ccNumberNode.item(0);
                    String ccNumber = getCharacterDataFromElement(lineCC);
                    creditCard = ccNumber.length() > 4 ? ccNumber.substring(ccNumber.length() - 4) : ccNumber;
                    cardType = ValidCreditCard.getCardType(ccNumber.substring(0,6)+"0000000000");
                }
            }

            Map<String,String> mapResponse = new HashMap<>();
            if (result.equals("1") && !token.isEmpty()) {
                objResponse.returnCode = 200;
                mapResponse.put("token", token);
                mapResponse.put("creditCard", "XXXXXXXXXXXX"+creditCard);
                mapResponse.put("cardType", cardType);
                objResponse.response = mapResponse;
                return objResponse.toString();
            } else {
                objResponse.returnCode = 525;

                mapResponse.put("message", message);
                objResponse.response = mapResponse;
            }
            return objResponse.toString();
        } catch (Exception ex) {
            logger.debug(requestId + " - createCreditToken exception: " + CommonUtils.getError(ex));
            return GatewayUtil.returnError(ex, 525);
        }
    }

    public String createPaymentWithCreditToken(double total, String cardToken, String cavv) {
        ObjResponse objResponse = new ObjResponse();
        try {
            String query = "<sale>" +
                    "  <api-key>"+ apiKey +"</api-key>" +
                    "  <amount>"+ total +"</amount>" +
                    "  <customer-vault-id>"+ cardToken +"</customer-vault-id>" +
                    "</sale>";
            if (cavv != null && !cavv.isEmpty()) {
                query = "<sale>" +
                        "  <api-key>"+ apiKey +"</api-key>" +
                        "  <amount>"+ total +"</amount>" +
                        "  <customer-vault-id>"+ cardToken +"</customer-vault-id>" +
                        "  <cavv>"+ Base64Encoder.encodeString(cavv) +"</cavv>" +
                        "</sale>";
            }

            Document doc = doRequest(query);

            /*
            <response>
               <result>1</result>
               <result-text>SUCCESS</result-text>
               <transaction-id>7058574913</transaction-id>
               <result-code>100</result-code>
               <authorization-code>123456</authorization-code>
               <action-type>sale</action-type>
               <amount>6.00</amount>
               <amount-authorized>6.00</amount-authorized>
               <tip-amount>0.00</tip-amount>
               <surcharge-amount>0.00</surcharge-amount>
               <industry>ecommerce</industry>
               <processor-id>ccprocessora</processor-id>
               <currency>USD</currency>
               <customer-id>195801610</customer-id>
               <customer-vault-id>195801610</customer-vault-id>
               <billing>
                  <billing-id>200635009</billing-id>
                  <first-name>fName</first-name>
                  <last-name>lName</last-name>
                  <phone>+84905123456</phone>
                  <email>mail@mail.com</email>
                  <cc-number>411111******1111</cc-number>
                  <cc-exp>1227</cc-exp>
                  <priority>1</priority>
               </billing>
               <shipping>
                  <shipping-id>1244420559</shipping-id>
                  <priority>1</priority>
               </shipping>
            </response>
             */
            String result = "";
            String chargeId = "";
            String authCode = "";
            String message = "";
            NodeList nodes = doc.getElementsByTagName("response");
            for (int i = 0; i < nodes.getLength(); i++) {
                Element element = (Element) nodes.item(i);

                NodeList resultNode = element.getElementsByTagName("result");
                Element line = (Element) resultNode.item(0);
                result = getCharacterDataFromElement(line);

                NodeList transactionIdNode = element.getElementsByTagName("transaction-id");
                line = (Element) transactionIdNode.item(0);
                chargeId = getCharacterDataFromElement(line);

                NodeList authCodeNode = element.getElementsByTagName("authorization-code");
                line = (Element) authCodeNode.item(0);
                authCode = getCharacterDataFromElement(line);

                NodeList messageNode = element.getElementsByTagName("result-text");
                if (messageNode != null) {
                    line = (Element) messageNode.item(0);
                    message = getCharacterDataFromElement(line);
                }
            }

            Map<String,String> mapResponse = new HashMap<>();
            if (result.equals("1") && !chargeId.isEmpty()) {
                mapResponse.put("message", "Success!");
                mapResponse.put("transId", chargeId);
                mapResponse.put("cardType", "");
                mapResponse.put("authCode", authCode);
                mapResponse.put("cardMask", "");

                objResponse.returnCode = 200;
                objResponse.response = mapResponse;
            } else {
                objResponse.returnCode = 525;

                mapResponse.put("message", message);
                objResponse.response = mapResponse;
            }
            return objResponse.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }
    }

    public String preAuthPayment(double amount, String cardToken) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        try {
            String query = "<auth>" +
                    "  <api-key>"+ apiKey +"</api-key>" +
                    "  <amount>"+ amount +"</amount>" +
                    "  <customer-vault-id>"+ cardToken +"</customer-vault-id>" +
                    "</auth>";
            Document doc = doRequest(query);

            /*
            <response>
               <result>1</result>
               <result-text>SUCCESS</result-text>
               <transaction-id>7058651164</transaction-id>
               <result-code>100</result-code>
               <authorization-code>123456</authorization-code>
               <action-type>auth</action-type>
               <amount>6.00</amount>
               <amount-authorized>6.00</amount-authorized>
               <tip-amount>0.00</tip-amount>
               <surcharge-amount>0.00</surcharge-amount>
               <industry>ecommerce</industry>
               <processor-id>ccprocessora</processor-id>
               <currency>USD</currency>
               <customer-id>195801610</customer-id>
               <customer-vault-id>195801610</customer-vault-id>
               <billing>
                  <billing-id>200635009</billing-id>
                  <first-name>fName</first-name>
                  <last-name>lName</last-name>
                  <phone>+84905123456</phone>
                  <email>mail@mail.com</email>
                  <cc-number>411111******1111</cc-number>
                  <cc-exp>1227</cc-exp>
                  <priority>1</priority>
               </billing>
               <shipping>
                  <shipping-id>1244420559</shipping-id>
                  <priority>1</priority>
               </shipping>
            </response>
             */
            String result = "";
            String authId = "";
            String authCode = "";
            String message = "";
            NodeList nodes = doc.getElementsByTagName("response");
            for (int i = 0; i < nodes.getLength(); i++) {
                Element element = (Element) nodes.item(i);

                NodeList resultNode = element.getElementsByTagName("result");
                Element line = (Element) resultNode.item(0);
                result = getCharacterDataFromElement(line);

                NodeList transactionIdNode = element.getElementsByTagName("transaction-id");
                line = (Element) transactionIdNode.item(0);
                authId = getCharacterDataFromElement(line);

                NodeList authCodeNode = element.getElementsByTagName("authorization-code");
                line = (Element) authCodeNode.item(0);
                authCode = getCharacterDataFromElement(line);

                NodeList messageNode = element.getElementsByTagName("result-text");
                if (messageNode != null) {
                    line = (Element) messageNode.item(0);
                    message = getCharacterDataFromElement(line);
                }
            }

            if (result.equals("1") && !authId.isEmpty()) {
                mapResponse.put("authId", authId);
                mapResponse.put("authAmount", String.valueOf(amount));
                mapResponse.put("authCode", authCode);
                mapResponse.put("authToken", cardToken);
                mapResponse.put("allowCapture", "true");

                objResponse.returnCode = 200;
                objResponse.response = mapResponse;
            } else {
                objResponse.returnCode = 525;

                mapResponse.put("message", message);
                objResponse.response = mapResponse;
            }
            return objResponse.toString();
        } catch(Exception ex) {
            logger.debug(requestId + " - preAuthPayment exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            return objResponse.toString();
        }
    }

    public String preAuthCapture(double amount, String transactionId) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        try {
            String query = "<capture>" +
                    "  <api-key>"+ apiKey +"</api-key>" +
                    "  <transaction-id>"+ transactionId +"</transaction-id>" +
                    "  <amount>"+ amount +"</amount>" +
                    "</capture>";
            Document doc = doRequest(query);

            /*
            <response>
               <result>1</result>
               <result-text>SUCCESS</result-text>
               <transaction-id>7058651164</transaction-id>
               <result-code>100</result-code>
               <tax-amount>0.00</tax-amount>
               <shipping-amount>0.00</shipping-amount>
            </response>
             */
            String result = "";
            String chargeId = "";
            String authCode = "";
            String message = "";
            NodeList nodes = doc.getElementsByTagName("response");
            for (int i = 0; i < nodes.getLength(); i++) {
                Element element = (Element) nodes.item(i);

                NodeList resultNode = element.getElementsByTagName("result");
                Element line = (Element) resultNode.item(0);
                result = getCharacterDataFromElement(line);

                NodeList transactionIdNode = element.getElementsByTagName("transaction-id");
                line = (Element) transactionIdNode.item(0);
                chargeId = getCharacterDataFromElement(line);

                NodeList authCodeNode = element.getElementsByTagName("authorization-code");
                line = (Element) authCodeNode.item(0);
                authCode = getCharacterDataFromElement(line);

                NodeList messageNode = element.getElementsByTagName("result-text");
                if (messageNode != null) {
                    line = (Element) messageNode.item(0);
                    message = getCharacterDataFromElement(line);
                }
            }

            if (result.equals("1") && !chargeId.isEmpty()) {
                mapResponse.put("message", "Success!");
                mapResponse.put("transId", chargeId);
                mapResponse.put("cardType", "");
                mapResponse.put("authCode", authCode);
                mapResponse.put("cardMask", "");

                objResponse.returnCode = 200;
                objResponse.response = mapResponse;
            } else {
                objResponse.returnCode = 525;

                mapResponse.put("message", message);
                objResponse.response = mapResponse;
            }
            return objResponse.toString();
        } catch(Exception ex) {
            logger.debug(requestId + " - preAuthCapture exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            return objResponse.toString();
        }
    }

    public void voidTransaction(String authId) {
        try {
            String query = "<void>" +
                    "  <api-key>"+ apiKey +"</api-key>" +
                    "  <transaction-id>"+ authId +"</transaction-id>" +
                    "</void>";
            Document doc = doRequest(query);

            /*
            <response>
               <result>1</result>
               <result-text>SUCCESS</result-text>
               <transaction-id>7058651164</transaction-id>
               <result-code>100</result-code>
               <tax-amount>0.00</tax-amount>
               <shipping-amount>0.00</shipping-amount>
            </response>
             */
            String message = "";
            NodeList nodes = doc.getElementsByTagName("response");
            for (int i = 0; i < nodes.getLength(); i++) {
                Element element = (Element) nodes.item(i);

                NodeList messageNode = element.getElementsByTagName("result-text");
                if (messageNode != null) {
                    Element line = (Element) messageNode.item(0);
                    message = getCharacterDataFromElement(line);
                }
            }
            logger.debug(requestId + " - voidTransaction result: " + message);
        } catch(Exception ex) {
            logger.debug(requestId + " - voidTransaction exception: " + CommonUtils.getError(ex));
        }
    }

    private Document doRequest(String query) {
        Document doc;
        try {
            URL url = new URL(path);
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-length", String.valueOf(query.length()));
            con.setRequestProperty("Content-Type","Content-type: text/xml");
            con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0;Windows98;DigExt)");
            con.setDoOutput(true);
            con.setDoInput(true);

            DataOutputStream output = new DataOutputStream(con.getOutputStream());
            output.writeBytes(query);
            output.close();

            String resultXML = getResponse(con);
            logger.debug(requestId + " - request: " + query);
            logger.debug(requestId + " - response: " + resultXML);

            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(resultXML));
            doc = db.parse(is);
        } catch (Exception ex) {
            logger.debug(requestId + " - doRequest exception: " + CommonUtils.getError(ex));
            doc = null;
        }
        return doc;
    }

    private static String getCharacterDataFromElement(Element e) {
        try {
            Node child = e.getFirstChild();
            if (child instanceof CharacterData) {
                CharacterData cd = (CharacterData) child;
                return cd.getData();
            }
        } catch(Exception ignore) {}

        return "";
    }

    private String getResponse(HttpsURLConnection con){
        try {
            if(HttpsURLConnection.HTTP_OK == con.getResponseCode()){
                BufferedReader br = null;
                StringBuilder sb = new StringBuilder();
                String line;
                try {
                    br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    if (br != null) {
                        try {
                            br.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                return sb.toString();
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }
}
