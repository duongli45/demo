package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.google.gson.Gson;
import com.qupworld.config.ServerConfig;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.TimezoneUtil;
import okhttp3.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by thuanho on 19/05/2021.
 */
@SuppressWarnings("uncchecked")
public class DNBUtil {

    final static Logger logger = LogManager.getLogger(DNBUtil.class);
    private static String SERVER_URL;
    private static String API_KEY;
    private static String APP_ID;
    private static String DNB_SSN;
    private static String DNB_BBAN;
    private static String ENV;
    private static String requestId;
    private static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
    private static Gson gson;

    public DNBUtil(String _requestId, String apiKey, String appId, String psuSSN, String psuBBAN, String url, String environment) {
        requestId = _requestId;
        API_KEY = apiKey;
        APP_ID = appId;
        DNB_SSN = psuSSN;
        DNB_BBAN = psuBBAN;
        SERVER_URL = url;
        ENV = environment;
        gson = new Gson();
    }

    public String getPaymentId(String sessionId, double amount, String ibanNumber, String nameOfAccount, String driverName) {
        String url = ServerConfig.proxy_server + "/dnb" + "/v1/payments/norwegian-domestic-credit-transfers";
        JSONObject objRequest = new JSONObject();

        JSONObject objDebtor = new JSONObject();
        objDebtor.put("iban", DNB_BBAN);
        objRequest.put("debtorAccount", objDebtor);

        JSONObject objCreditor = new JSONObject();
        objCreditor.put("iban", ibanNumber);
        objRequest.put("creditorAccount", objCreditor);

        objRequest.put("creditorName", nameOfAccount);

        DecimalFormat df = new DecimalFormat("0.00");

        JSONObject objAmount = new JSONObject();
        objAmount.put("amount", df.format(amount));
        objAmount.put("currency", "NOK");
        objRequest.put("instructedAmount", objAmount);

        if (!driverName.isEmpty()) {
            objRequest.put("remittanceInformationUnstructured", "Payout to " + driverName);
        } else {
            objRequest.put("remittanceInformationUnstructured", "Payout");
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        objRequest.put("requestedExecutionDate", sdf.format(TimezoneUtil.getGMTTimestamp()));

        String result = sendRequest(url, "POST", objRequest.toJSONString(), true, sessionId);
        /*{
            "transactionStatus": "ACCP",
            "paymentId": "SSL_DOM-762002640",
            "_links": {
            "self": {
                "href": "/v1/payments/norwegian-domestic-credit-transfers/SSL_DOM-762002640"
            }
        },
            "psuMessage": "Use the authorisation endpoint of the payment product to start the authorisation process."
        }*/
        String paymentId = "";
        if (!result.isEmpty()) {
            org.json.JSONObject jsonResponse = new org.json.JSONObject(result);
            paymentId = jsonResponse.has("paymentId") ? jsonResponse.getString("paymentId") : "";
        }

        ObjResponse response = new ObjResponse();
        if (!paymentId.isEmpty()) {
            Map<String,String> mapResponse = new HashMap<>();
            mapResponse.put("paymentId", paymentId);
            response.returnCode = 200;
            response.response = mapResponse;
            return response.toString();
        } else {
            response.returnCode = 525;
        }
        return response.toString();

    }

    public String getSigningBaskets(JSONObject objData, String sessionId) {
        // get basketId
        String url = ServerConfig.proxy_server + "/dnb" + "/v1/signing-baskets";

        String result = sendRequest(url, "POST", objData.toJSONString(), true, sessionId);
        /*{
            "transactionStatus": "RCVD",
            "basketId": "90c809dd-79ad-4ec7-9de2-1cf382207c93",
            "_links": {
                "self": {
                    "href": "/v1/signing-baskets/90c809dd-79ad-4ec7-9de2-1cf382207c93"
                },
                "status": {
                    "href": "/v1/signing-baskets/90c809dd-79ad-4ec7-9de2-1cf382207c93/status"
                },
                "startAuthorisation": {
                    "href": "/v1/signing-baskets/90c809dd-79ad-4ec7-9de2-1cf382207c93/authorisations"
                }
            }
        }*/
        String basketId = "";
        String returnUrl = "";
        if (!result.isEmpty()) {
            org.json.JSONObject jsonResponse = new org.json.JSONObject(result);
            basketId = jsonResponse.has("basketId") ? jsonResponse.getString("basketId") : "";

            // get authen URL
            String authUrl = ServerConfig.proxy_server + "/dnb" + "/v1/signing-baskets/"+basketId+"/authorisations";
            String resultAuth = sendRequest(authUrl, "POST", "{}", true, sessionId);
            /*{
                "scaStatus": "received",
                "authorisationId": "5d550457-61f5-461b-915a-f81e75784e79",
                "_links": {
                    "scaRedirect": {
                        "href": "https://8rfdrr5jza.execute-api.eu-west-1.amazonaws.com/Prod/bankid/logon?blob=eyJ1c2VySWQiOiIzMTEyNTQ1MzkxMyIsInJlZGlyZWN0VXJsIjoiaHR0cDovLzExMy4xNjAuMjMyLjIzNDoxMzM3L3BheW1lbnROb3RpZmljYXRpb25VUkwiLCJtb2RlIjoiUHJvZCIsImNvbnNlbnRJZCI6IjVkNTUwNDU3LTYxZjUtNDYxYi05MTVhLWY4MWU3NTc4NGU3OSIsImFjY2Vzc1VybCI6ImFwcHMvbmJwL2JldGFsaW5nIiwicHNkMkVuZHBvaW50IjoiaHR0cHM6Ly9hcGkuaW50ZXJuYWwwMS5zYW5kYm94LmFwcHNoYXJlZHN2Yy50ZWNoLTAzLm5ldC92MS9uZXRiYW5rL3Byb3h5L3BheW1lbnQvc2NhLzVkNTUwNDU3LTYxZjUtNDYxYi05MTVhLWY4MWU3NTc4NGU3OSIsImNvbnNlbnRFbmRwb2ludCI6Imh0dHBzOi8vYXBpLmludGVybmFsMDEuc2FuZGJveC5hcHBzaGFyZWRzdmMudGVjaC0wMy5uZXQvdjEvbmV0YmFuay9wcm94eS9hdXRob3Jpc2F0aW9uLzVkNTUwNDU3LTYxZjUtNDYxYi05MTVhLWY4MWU3NTc4NGU3OSIsInBheW1lbnRJZCI6IjkwYzgwOWRkLTc5YWQtNGVjNy05ZGUyLTFjZjM4MjIwN2M5MyIsInRwcCI6IlBTRE5PLVRTVE5DQS0yN2E3YTRlMnxRRmFtaWx5IiwidGltZXN0YW1wIjoiMTYyMDgwNjU2Nzc3NCJ9&redirectUrl=http%3A%2F%2F113.160.232.234%3A1337%2FpaymentNotificationURL&la"
                    }
                }
            }*/
            if (!resultAuth.isEmpty()) {
                org.json.JSONObject jsonResponseAuth = new org.json.JSONObject(resultAuth);
                org.json.JSONObject objLink = jsonResponseAuth.has("_links") ? jsonResponseAuth.getJSONObject("_links") : null;
                if (objLink != null) {
                    org.json.JSONObject objSCA = objLink.has("scaRedirect") ? objLink.getJSONObject("scaRedirect") : null;
                    if (objSCA != null) {
                        returnUrl = objSCA.has("href") ? objSCA.getString("href") : "";
                    }
                }
            }

        }

        ObjResponse response = new ObjResponse();
        if (basketId.isEmpty() || returnUrl.isEmpty()) {
            response.returnCode = 525;
        } else {
            Map<String,String> mapResponse = new HashMap<>();
            mapResponse.put("basketId", basketId);
            mapResponse.put("url", returnUrl);
            response.returnCode = 200;
            response.response = mapResponse;
        }
        return response.toString();
    }

    public String checkBasketStatus(String basketId) {
        String url = ServerConfig.proxy_server + "/dnb" + "/v1/signing-baskets/"+basketId+"/status";
        String result = sendRequest(url, "GET", "{}", false, "");
        /*{
            "transactionStatus": "ACTC" // Authentication, syntactical and semantical validation are successful.
        }*/
        int returnCode = 525;
        if (!result.isEmpty()) {
            org.json.JSONObject jsonResponse = new org.json.JSONObject(result);
            String transactionStatus = jsonResponse.has("transactionStatus") ? jsonResponse.getString("transactionStatus") : "";
            if (transactionStatus.equals("ACTC")) {
                returnCode = 200;
            }
        }

        ObjResponse response = new ObjResponse();
        response.returnCode = returnCode;
        return response.toString();
    }

    public String checkPaymentStatus(String paymentId) {
        String url = ServerConfig.proxy_server + "/dnb" + "/v1/payments/norwegian-domestic-credit-transfers/"+paymentId;
        String result = sendRequest(url, "GET", "{}", false, "");
        /*{
            "debtorAccount": {
                "bban": "12041281683"
            },
            "instructedAmount": {
                "currency": "NOK",
                        "amount": "30.00"
            },
            "creditorAccount": {
                "bban": "12032202452"
            },
            "creditorName": "Foo Bar",
            "remittanceInformationUnstructured": "Test of payment to another Norwegian account.",
            "transactionStatus": "ACTV" // Payment is active in the due register.
        }*/
        int returnCode = 525;
        if (!result.isEmpty()) {
            org.json.JSONObject jsonResponse = new org.json.JSONObject(result);
            String transactionStatus = jsonResponse.has("transactionStatus") ? jsonResponse.getString("transactionStatus") : "";
            if (transactionStatus.equals("ACTV")) {
                returnCode = 200;
            }
        }

        ObjResponse response = new ObjResponse();
        response.returnCode = returnCode;
        return response.toString();
    }

    private static String sendRequest(String url, String method, String data, boolean extraHeader, String sessionId) {
        try {
            logger.debug(requestId + " - url = " + url);
            logger.debug(requestId + " - method = " + method);
            logger.debug(requestId + " - data = " + data);

            String redirectURL = ServerConfig.hook_server + KeysUtil.DNB_URL + "?dnbSessionId=" + sessionId;

            RequestBody body = RequestBody.create(data, JSON);
            Request request;
            if (method.equals("POST")) {
                if (extraHeader) {

                    request = new Request.Builder()
                            .header("x-env-type", ENV)
                            .header("PSU-ID", DNB_SSN)
                            .header("TPP-Redirect-URI", redirectURL)
                            .header("TPP-Explicit-Authorisation-Preferred", "true")
                            .url(url)
                            .post(body)
                            .build();
                } else {
                    request = new Request.Builder()
                            .header("x-env-type", ENV)
                            .header("PSU-ID", DNB_SSN)
                            .url(url)
                            .post(body)
                            .build();
                }

            } else { // get
                if (extraHeader) {
                    request = new Request.Builder()
                            .header("x-env-type", ENV)
                            .header("PSU-ID", DNB_SSN)
                            .header("TPP-Redirect-URI", redirectURL)
                            .header("TPP-Explicit-Authorisation-Preferred", "true")
                            .url(url)
                            .get()
                            .build();
                } else {
                    request = new Request.Builder()
                            .header("x-env-type", ENV)
                            .header("PSU-ID", DNB_SSN)
                            .url(url)
                            .get()
                            .build();
                }
            }

            OkHttpClient client = new OkHttpClient.Builder()
                    .build();
            Response response = client.newCall(request).execute();
            String responseString = response.body().string();
            logger.debug(requestId + " - response = " + responseString);
            return responseString;
        } catch (Exception ex) {
            logger.debug(requestId + " - sendRequest exception: " + CommonUtils.getError(ex));
        }
        return "";
    }
}
