package com.qupworld.paymentgateway.controllers.threading;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.qupworld.paymentgateway.controllers.PaymentUtil;
import com.qupworld.paymentgateway.entities.HydraPayout;
import com.qupworld.paymentgateway.entities.HydraPending;
import com.qupworld.paymentgateway.models.*;
import com.qupworld.paymentgateway.models.mongo.collections.fleet.Fleet;
import com.qupworld.paymentgateway.models.mongo.collections.pricingplan.PricingPlan;
import com.qupworld.service.MailService;
import com.qupworld.service.QUpReportPublisher;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.TimezoneUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.sql.Timestamp;
import java.util.List;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
public class HydraPayoutThread extends Thread {

    final static Logger logger = LogManager.getLogger(HydraPayoutThread.class);
    public String requestId = "";
    public String payoutId = "";
    public Timestamp dateTime;
    public String payoutType = ""; // values = auto, manual
    public String accountHolderName = "";
    public String bankName = "";
    public String accountNumber = "";
    public double totalPayout = 0.0;
    public String notes = "";
    public String fleetId = "";
    public String bookId = "";
    public String transactionId = "";
    public String operatorName = "";
    public String fleetName = "";
    public String email = "";
    public String currencyISO = "";
    public List<String> listBookId;
    public List<HydraPending> listHydraPending;
    public String settlement = "";
    private SQLDao sqlDao = new SQLDaoImpl();
    private Gson gson = new GsonBuilder().serializeNulls().create();
    private PaymentUtil paymentUtil = new PaymentUtil();

    @Override
    public void run() {
        if (listBookId != null && listBookId.size() > 0) {
            payoutMultiBooking();
        } else {
            payoutBooking();
        }

    }

    private void payoutBooking() {
        try {
            HydraPayout hydraPayout = new HydraPayout();
            hydraPayout.payoutId = payoutId;
            hydraPayout.dateTime = dateTime;
            hydraPayout.payoutType = payoutType;
            hydraPayout.accountHolderName = accountHolderName;
            hydraPayout.bankName = bankName;
            hydraPayout.accountNumber = accountNumber;
            hydraPayout.totalPayout = totalPayout;
            hydraPayout.notes = "Booking#: " + bookId;
            hydraPayout.fleetId = fleetId;
            hydraPayout.bookId = bookId;
            hydraPayout.transactionId = transactionId;
            hydraPayout.operatorName = operatorName;
            hydraPayout.fleetName = fleetName;
            hydraPayout.transactionType = "";
            hydraPayout.networkType = "roaming";
            hydraPayout.settlement = "payable";
            hydraPayout.paymentMethod = "";

            // Save to MySQL
            long idHistory = sqlDao.addHydraPayout(hydraPayout);
            if (idHistory > 0) {
                String hydraPayoutStr = gson.toJson(hydraPayout);
                JSONObject payoutHistoryJSON = gson.fromJson(hydraPayoutStr, JSONObject.class);
                payoutHistoryJSON.remove("dateTime");
                payoutHistoryJSON.put("dateTime", TimezoneUtil.formatISODate(dateTime, "GMT"));
                QUpReportPublisher reportPublisher = QUpReportPublisher.getInstance();
                reportPublisher.sendOneItem(requestId, String.valueOf(idHistory), "HydraPayout", payoutHistoryJSON.toJSONString());
            }

            //send mail payout to fleet
            if (email != null && email.length() > 0) {
                MailService mailService = MailService.getInstance();
                mailService.sendMailManualPayoutHydra(requestId, fleetId, payoutId, totalPayout, currencyISO, email.split(","));
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }
    }

    private void payoutMultiBooking() {
        try {
            MongoDao mongoDao = new MongoDaoImpl();
            Fleet fleet = mongoDao.getFleetInfor(fleetId);
            String holderName = "";
            String bankName = "";
            String accountNumber = "";
            PricingPlan pricingPlan = mongoDao.getPricingPlan(fleetId);
            if (pricingPlan.affiliate != null) {
                holderName = pricingPlan.affiliate.holderName;
                bankName = pricingPlan.affiliate.bankName;
                accountNumber = pricingPlan.affiliate.accountNumber;
            }

            JSONArray arrReport = new JSONArray();
            double payoutAmount = 0.0;
            for (HydraPending hydraPending : listHydraPending) {
                double exchangeRate = hydraPending.exchangeRate;
                if (exchangeRate == 0) exchangeRate = 1.0;
                String transactionType = paymentUtil.getTransactionTypeFromHydraPending(hydraPending, fleetId);
                double amount = paymentUtil.getAmountFromHydraPending(hydraPending, fleetId);
                amount = CommonUtils.getRoundValue(amount/exchangeRate);
                logger.debug(requestId + " - " + hydraPending.bookId + " - " + transactionType + " - " + amount);
                payoutAmount += amount;
                if (payoutAmount == 0) {
                    continue;
                }
                HydraPayout hydraPayout = new HydraPayout();
                hydraPayout.payoutId = payoutId;
                hydraPayout.dateTime = dateTime;
                hydraPayout.payoutType = payoutType;
                hydraPayout.accountHolderName = holderName;
                hydraPayout.bankName = bankName;
                hydraPayout.accountNumber = accountNumber;
                hydraPayout.totalPayout = amount;
                hydraPayout.notes = notes;
                hydraPayout.fleetId = fleetId;
                hydraPayout.bookId = hydraPending.bookId;
                hydraPayout.transactionId = transactionId;
                hydraPayout.operatorName = operatorName;
                hydraPayout.fleetName = fleet.name;
                hydraPayout.transactionType = transactionType;
                hydraPayout.networkType = hydraPending.networkType;
                hydraPayout.paymentMethod = KeysUtil.PAYMENT_DIRECT_INVOICING;
                hydraPayout.settlement = settlement;

                // Save to MySQL
                long idHistory = sqlDao.addHydraPayout(hydraPayout);
                logger.debug(requestId + " - idHistory: " + idHistory);
                if (idHistory > 0) {
                    String hydraPayoutStr = gson.toJson(hydraPayout);
                    JSONObject payoutHistoryJSON = gson.fromJson(hydraPayoutStr, JSONObject.class);
                    payoutHistoryJSON.remove("dateTime");
                    payoutHistoryJSON.put("dateTime", TimezoneUtil.formatISODate(dateTime, "GMT"));

                    JSONObject objReport = new JSONObject();
                    objReport.put("type", "HydraPayout");
                    objReport.put("id", String.valueOf(idHistory));
                    objReport.put("body", payoutHistoryJSON);
                    logger.debug(requestId + " - objReport: " + objReport.toString());
                    arrReport.add(objReport);
                    /*logger.debug(requestId + " - arrReport: " + arrReport.size());
                    logger.debug(requestId + " - arrReport: " + arrReport.toJSONString());*/
                    // update Ticket
                    boolean payoutToHomeFleet = false;
                    boolean payoutToProviderFleet = false;
                    boolean chargeTotalFromHomeFleet = false;
                    switch (transactionType) {
                        case "supplierPenalty":
                        case "supplierPayout":
                        case "rejectBooking":
                            payoutToProviderFleet = true;
                            break;
                        case "buyerCommission":
                            payoutToHomeFleet = true;
                            break;
                        case "chargeBooking": {
                            chargeTotalFromHomeFleet = true;
                            if (hydraPending.networkType.equals("farmOut")) {
                                payoutToHomeFleet = true;
                            }
                            break;
                        }

                    }
                    JSONObject ticketJSON = new JSONObject();
                    if (payoutToHomeFleet)
                        ticketJSON.put("payoutToHomeFleet", payoutToHomeFleet);
                    if (payoutToProviderFleet)
                        ticketJSON.put("payoutToProviderFleet", payoutToProviderFleet);
                    if (chargeTotalFromHomeFleet)
                        ticketJSON.put("chargeTotalFromHomeFleet", chargeTotalFromHomeFleet);

                    ticketJSON.put("completedTime", KeysUtil.SDF_DMYHMS.format(hydraPending.completedTime));

                    JSONObject objTicket = new JSONObject();
                    objTicket.put("type", "Ticket");
                    objTicket.put("id", hydraPending.bookId);
                    objTicket.put("body", ticketJSON);
                    logger.debug(requestId + " - objTicket: " + objTicket);
                    arrReport.add(objTicket);
                    /*logger.debug(requestId + " - arrReport: " + arrReport.size());*/
                }
            }
            logger.debug(requestId + " - arrReport: " + arrReport.toJSONString());
            // send data to Report
            QUpReportPublisher reportPublisher = QUpReportPublisher.getInstance();
            reportPublisher.sendListItems(requestId, arrReport.toJSONString());

            //send mail payout to fleet
            if (email != null && email.length() > 0 && settlement.equals("payable")) {
                MailService mailService = MailService.getInstance();
                mailService.sendMailManualPayoutHydra(requestId, fleetId, payoutId, payoutAmount, currencyISO, email.split(","));
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }
    }
}
