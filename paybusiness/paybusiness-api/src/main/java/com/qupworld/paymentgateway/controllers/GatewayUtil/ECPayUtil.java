package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.google.gson.Gson;
import com.qupworld.config.ServerConfig;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.TimezoneUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see <a href="http://qupworld.com/terms">http://qupworld.com/terms</a>
 * @see <a href="http://qupworld.com/privacy">http://qupworld.com/privacy</a>
 * <p/>
 * Description
 */
public class ECPayUtil {

    private final static Logger logger = LogManager.getLogger(ECPayUtil.class);
    private String requestId = "";
    private String REQUEST_URL = "";
    private String MERCHANT_ID = "";
    private String HASH_KEY = "";
    private String HASH_IV = "";
    private String CHECK_CODE = "";

    private final Gson gson = new Gson();

    private final SimpleDateFormat SDF_DMYHMS = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    public ECPayUtil(String _requestId, String merchantId, String hashKey, String hashIV, String creditCheckCode, String requestURL) {
        requestId = _requestId;
        MERCHANT_ID = merchantId;
        HASH_KEY = hashKey;
        HASH_IV = hashIV;
        CHECK_CODE = creditCheckCode;
        REQUEST_URL = requestURL;
    }
    public String initiateCreditForm(String orderNumber, String firstName, String lastName, String phoneNumber){
        ObjResponse objResponse = new ObjResponse();
        Map<String,Object> mapResponse = new HashMap<>();
        String checkoutURL = "";
        String merchantMemberID = MERCHANT_ID + CommonUtils.randomString(10);
        try {
            String returnUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.ECPAY + "?urlAction=token";
            String backToStoreUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.ECPAY + "?urlAction=registerCompleted";
            String choosePayment = "Credit";
            String merchantTradeDate = SDF_DMYHMS.format(TimezoneUtil.getGMTTimestamp());
            int amount = 5;
            String itemName = "Register new card for " + firstName + lastName;
            String tradeDesc = firstName + " " + lastName + " - " + phoneNumber;
            Map<String,Object> mapData = new HashMap<>();
            mapData.put("MerchantID", MERCHANT_ID);
            mapData.put("MerchantTradeNo", orderNumber);
            mapData.put("MerchantTradeDate", merchantTradeDate);
            mapData.put("PaymentType", "aio");
            mapData.put("TotalAmount", amount);
            mapData.put("TradeDesc", tradeDesc);
            mapData.put("ItemName", itemName);
            mapData.put("ReturnURL", returnUrl);
            mapData.put("ChoosePayment", choosePayment);
            mapData.put("EncryptType", 1);
            mapData.put("NeedExtraPaidInfo", "Y");
            mapData.put("BindingCard", 1);
            mapData.put("MerchantMemberID", merchantMemberID);
            mapData.put("ClientBackURL", backToStoreUrl);
            String genCheckMacValue = genCheckMacValue(mapData);
            String actionUrl = REQUEST_URL + "/Cashier/AioCheckOut/V5";
            checkoutURL = ServerConfig.webform_powertranz + "/ecpay"
                    + "?action=" + URLEncoder.encode(actionUrl, "UTF-8")
                    + "&EncryptType=" + 1
                    + "&PaymentType=" + "aio"
                    + "&ItemName=" + URLEncoder.encode(itemName, "UTF-8")
                    + "&MerchantTradeDate=" + URLEncoder.encode(merchantTradeDate, "UTF-8")
                    + "&ReturnURL=" + URLEncoder.encode(returnUrl, "UTF-8")
                    + "&MerchantID=" + MERCHANT_ID
                    + "&TotalAmount=" + amount
                    + "&MerchantTradeNo=" + orderNumber
                    + "&TradeDesc=" + URLEncoder.encode(tradeDesc, "UTF-8")
                    + "&ChoosePayment=" + choosePayment
                    + "&NeedExtraPaidInfo=" + "Y"
                    + "&BindingCard=" + 1
                    + "&MerchantMemberID=" + merchantMemberID
                    + "&ClientBackURL=" + URLEncoder.encode(backToStoreUrl, "UTF-8")
                    + "&CheckMacValue=" + genCheckMacValue
                    ;
            logger.debug(requestId + " - checkoutURL: " + checkoutURL);
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }

        objResponse.returnCode = 200;
        mapResponse.put("3ds_url", checkoutURL);
        mapResponse.put("type", "link");
        mapResponse.put("transactionId", orderNumber);
        mapResponse.put("merchantMemberID", merchantMemberID);
        mapResponse.put("redirect", false);
        mapResponse.put("gateway", KeysUtil.ECPAY);
        objResponse.response = mapResponse;
        return objResponse.toString();
    }

    public String preAuthByToken(String orderId, String bookId, String token, String type, double amount){
        ObjResponse objResponse = new ObjResponse();
        Map<String,Object> mapResponse = new HashMap<>();
        String checkoutURL = "";
        try {
            int intAmount = (int) amount;
            String returnUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.ECPAY + "?urlAction="+type;
            String backToStoreUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.ECPAY + "?urlAction=backToStore";
            String choosePayment = "Credit";
            String merchantTradeDate = SDF_DMYHMS.format(TimezoneUtil.getGMTTimestamp());
            String itemName = "";
            String tradeDesc = "";
            switch (type) {
                case "payment" :
                case "preAuth" :
                    itemName = "Booking #" + bookId;
                    tradeDesc = "Pay for booking #" + bookId;
                break;
                case "remain" :
                    itemName = "Pay for booking #" + bookId;
                    tradeDesc = "Collect remaining amount for booking #" + bookId;
                break;
                case "extra" :
                    itemName = "Pay for booking #" + bookId;
                    tradeDesc = "Collect extra amount for booking #" + bookId;
                    break;
                case "tip" :
                    itemName = "Tip for booking #" + bookId;
                    tradeDesc = "Tip for booking #" + bookId;
                break;
                case "outstanding" :
                    itemName = "Pay outstanding";
                    tradeDesc = "Pay outstanding";
                break;
                case "paxWallet" :
                case "topupDriver" :
                    itemName = "Top-up to wallet";
                    tradeDesc = "Top-up to wallet";
                break;
                case "topupPrepaid" :
                    itemName = "Top-up corporate prepaid";
                    tradeDesc = "Top-up corporate prepaid";
                    break;
            }
            Map<String,Object> mapData = new HashMap<>();
            mapData.put("MerchantID", MERCHANT_ID);
            mapData.put("MerchantTradeNo", orderId);
            mapData.put("MerchantTradeDate", merchantTradeDate);
            mapData.put("PaymentType", "aio");
            mapData.put("TotalAmount", intAmount);
            mapData.put("TradeDesc", tradeDesc);
            mapData.put("ItemName", itemName);
            mapData.put("ReturnURL", returnUrl);
            mapData.put("ChoosePayment", choosePayment);
            mapData.put("EncryptType", 1);
            mapData.put("NeedExtraPaidInfo", "Y");
            mapData.put("BindingCard", 1);
            mapData.put("MerchantMemberID", token);
            mapData.put("ClientBackURL", backToStoreUrl);
            String genCheckMacValue = genCheckMacValue(mapData);
            String actionUrl = REQUEST_URL + "/Cashier/AioCheckOut/V5";
            checkoutURL = ServerConfig.webform_powertranz + "/ecpay"
                    + "?action=" + URLEncoder.encode(actionUrl, "UTF-8")
                    + "&EncryptType=" + 1
                    + "&PaymentType=" + "aio"
                    + "&ItemName=" + URLEncoder.encode(itemName, "UTF-8")
                    + "&MerchantTradeDate=" + URLEncoder.encode(merchantTradeDate, "UTF-8")
                    + "&ReturnURL=" + URLEncoder.encode(returnUrl, "UTF-8")
                    + "&MerchantID=" + MERCHANT_ID
                    + "&TotalAmount=" + intAmount
                    + "&MerchantTradeNo=" + orderId
                    + "&TradeDesc=" + URLEncoder.encode(tradeDesc, "UTF-8")
                    + "&ChoosePayment=" + choosePayment
                    + "&NeedExtraPaidInfo=" + "Y"
                    + "&BindingCard=" + 1
                    + "&MerchantMemberID=" + token
                    + "&ClientBackURL=" + URLEncoder.encode(backToStoreUrl, "UTF-8")
                    + "&CheckMacValue=" + genCheckMacValue
            ;
            logger.debug(requestId + " - checkoutURL: " + checkoutURL);
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }

        objResponse.returnCode = 528;
        if (type.equals("paxWallet") || type.equals("topupDriver") || type.equals("topupPrepaid")) {
            objResponse.returnCode = 200;
        }
        mapResponse.put("3ds_url", checkoutURL);
        mapResponse.put("deepLinkUrl", checkoutURL);
        mapResponse.put("type", "link");
        mapResponse.put("transactionId", orderId);
        mapResponse.put("openInApp", true);
        objResponse.response = mapResponse;
        return objResponse.toString();
    }

    public String captureTransaction(String merchantTradeNo, String tradeNo, String authCode, double amount, Map<String,String> map) {
        ObjResponse objResponse = new ObjResponse();
        try {
            int intAmount = (int) amount;
            Map<String,Object> mapData = new HashMap<>();
            mapData.put("MerchantID", MERCHANT_ID);
            mapData.put("MerchantTradeNo", merchantTradeNo);
            mapData.put("TradeNo", tradeNo);
            mapData.put("Action", "C");
            mapData.put("TotalAmount", intAmount);
            String genCheckMacValue = genCheckMacValue(mapData).toUpperCase();

            Set<String> keySet = mapData.keySet();
            TreeSet<String> treeSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
            treeSet.addAll(keySet);
            String[] name = treeSet.toArray(new String[treeSet.size()]);
            StringBuilder paramStr = new StringBuilder();
            for (String s : name) {
                paramStr.append("&").append(s).append("=").append(mapData.get(s));
            }
            paramStr.append("&CheckMacValue=").append(genCheckMacValue);
            String actionUrl = REQUEST_URL + "/CreditDetail/DoAction";
            String result = httpPost(actionUrl, paramStr.toString(), "UTF-8");
            logger.debug(requestId + " - capture result: " + result);
            Map<String, String> mapResult = Arrays.stream(result.split("&"))
                    .map(kv -> kv.split("="))
                    .collect(Collectors.toMap(kv -> kv[0], kv -> kv[1]));
            String returnMessage = mapResult.get("RtnMsg");
            logger.debug(requestId + " - capture result: " + returnMessage);
            Map<String,Object> mapResponse = new HashMap<>();
            if (returnMessage.equalsIgnoreCase("Succeeded.")) {
                mapResponse.put("message", "Success!");
                mapResponse.put("transId", tradeNo);
                mapResponse.put("cardType", map.get("cardType"));
                mapResponse.put("authCode", authCode);
                mapResponse.put("cardMask", map.get("cardMask"));
                objResponse.response = mapResponse;
                objResponse.returnCode = 200;
                return objResponse.toString();
            } else {
                mapResponse.put("message", returnMessage);
                objResponse.returnCode = 525;
                objResponse.response = mapResponse;
            }
            return objResponse.toString();
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
        }
        return objResponse.toString();
    }

    @SuppressWarnings("unchecked")
    public String checkTransactionStatus(String gwsr, double amount) {
        ObjResponse objResponse = new ObjResponse();
        try {
            int intAmount = (int) amount;
            Map<String,Object> mapData = new HashMap<>();
            mapData.put("MerchantID", MERCHANT_ID);
            mapData.put("CreditAmount", intAmount);
            mapData.put("CreditRefundId", gwsr);
            mapData.put("CreditCheckCode", CHECK_CODE);
            String genCheckMacValue = genCheckMacValue(mapData).toUpperCase();

            Set<String> keySet = mapData.keySet();
            TreeSet<String> treeSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
            treeSet.addAll(keySet);
            String[] name = treeSet.toArray(new String[treeSet.size()]);
            StringBuilder paramStr = new StringBuilder();
            for (String s : name) {
                paramStr.append("&").append(s).append("=").append(mapData.get(s));
            }
            paramStr.append("&CheckMacValue=").append(genCheckMacValue);
            String actionUrl = REQUEST_URL + "/CreditDetail/QueryTrade/V2";
            String result = httpPost(actionUrl, paramStr.toString(), "UTF-8");
            logger.debug(requestId + " - result: " + result);
            JSONObject jsonObject = gson.fromJson(result, JSONObject.class);
            Map<String,String> map = (Map<String,String>)jsonObject.get("RtnValue");
            String status = map.get("status");
            logger.debug(requestId + " - status: " + status);
            String compare = "已授權"; // Authorized
            if (status.equalsIgnoreCase(compare)) {
                objResponse.returnCode = 200;
            } else {
                objResponse.returnCode = 525;
            }
            return objResponse.toString();
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
        }
        return objResponse.toString();
    }

    public String refundTransaction(String merchantTradeNo, String tradeNo, double amount) {
        return doAction("refund", merchantTradeNo, tradeNo, amount);
    }

    public String voidTransaction(String merchantTradeNo, String tradeNo, double amount) {
        return doAction("abandoning", merchantTradeNo, tradeNo, amount);
    }
    private String doAction(String action, String merchantTradeNo, String tradeNo, double amount) {
        ObjResponse objResponse = new ObjResponse();
        try {
            String actionKey = action.equals("refund") ? "R" : "N";
            int intAmount = (int) amount;
            Map<String,Object> mapData = new HashMap<>();
            mapData.put("MerchantID", MERCHANT_ID);
            mapData.put("MerchantTradeNo", merchantTradeNo);
            mapData.put("TradeNo", tradeNo);
            mapData.put("Action", actionKey);
            mapData.put("TotalAmount", intAmount);
            String genCheckMacValue = genCheckMacValue(mapData).toUpperCase();

            Set<String> keySet = mapData.keySet();
            TreeSet<String> treeSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
            treeSet.addAll(keySet);
            String[] name = treeSet.toArray(new String[treeSet.size()]);
            StringBuilder paramStr = new StringBuilder();
            for (String s : name) {
                paramStr.append("&").append(s).append("=").append(mapData.get(s));
            }
            paramStr.append("&CheckMacValue=").append(genCheckMacValue);
            String actionUrl = REQUEST_URL + "/CreditDetail/DoAction";
            String result = httpPost(actionUrl, paramStr.toString(), "UTF-8");
            logger.debug(requestId + " - " + action + " result: " + result);
            Map<String, String> map = Arrays.stream(result.split("&"))
                    .map(kv -> kv.split("="))
                    .collect(Collectors.toMap(kv -> kv[0], kv -> kv[1]));
            String returnMessage = map.get("RtnMsg");
            logger.debug(requestId + " - " + action + " result: " + returnMessage);
            if (returnMessage.equalsIgnoreCase("Succeeded.")) {
                objResponse.returnCode = 200;
            } else {
                objResponse.returnCode = 525;
            }
            return objResponse.toString();
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
        }
        return objResponse.toString();
    }


    private String httpPost(String url, String urlParameters, String encoding) throws Exception {
        URL obj = new URL(url);
        HttpURLConnection connection = null;
        if (obj.getProtocol().equalsIgnoreCase("https")) {
            connection = (HttpsURLConnection) obj.openConnection();
            trustAllHosts((HttpsURLConnection) connection);
        }
        else {
            connection = (HttpURLConnection) obj.openConnection();
        }
        connection.setRequestMethod("POST");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.2171.71 Safari/537.36 EcPay JAVA API Version 2.0.1");
        connection.setRequestProperty("Accept-Language", encoding);
        connection.setDoOutput(true);

        //logger.debug(requestId + " - responseCode: " + connection.getResponseCode());
        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
        wr.write(urlParameters.getBytes(encoding));
        wr.flush();
        wr.close();
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), encoding));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();
    }

    private String genCheckMacValue(Map<String, Object> mapData) throws Exception{
        // sort data before process
        Map<String, Object> sortedData = mapData.entrySet().stream()
                .sorted(Map.Entry.<String, Object>comparingByKey().reversed())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        logger.debug(requestId + " - mapData: " + gson.toJson(mapData));
        Set<String> keySet = sortedData.keySet();
        TreeSet<String> treeSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
        treeSet.addAll(keySet);
        String[] name = treeSet.toArray(new String[treeSet.size()]);
        String paramStr = "";
        for (String s : name) {
            if (!s.equals("CheckMacValue")) {
                paramStr += "&" + s + "=" + sortedData.get(s);
            }
        }
        /*logger.debug(requestId + " - 1. paramStr: " + paramStr);*/
        String encodeData = "Hashkey=" + HASH_KEY + paramStr + "&HashIV=" + HASH_IV;
        logger.debug(requestId + " - 2. encodeData: " + encodeData);
        String urlEncode = URLEncoder.encode(encodeData, "UTF-8");
        /*logger.debug(requestId + " - 3. urlEncode: " + urlEncode);*/
        urlEncode = urlEncode.toLowerCase();
        /*logger.debug(requestId + " - 4. urlEncode to lowercase: " + urlEncode);*/
        /*String netEncode = netUrlEncode(urlEncode);
        logger.debug(requestId + " - 4. convert netUrlEncode: " + netEncode.equals(urlEncode));*/

        return hash(urlEncode.getBytes(), "SHA-256");
    }

    private String netUrlEncode(String url){
        String netUrlEncode = url.replaceAll("%21", "\\!").replaceAll("%28", "\\(").replaceAll("%29", "\\)");
        return netUrlEncode;
    }

    private String hash(byte[] data, String mode){
        MessageDigest md = null;
        try{
            if(mode.equals("MD5")){
                md = MessageDigest.getInstance("MD5");
            }
            else if(mode.equals("SHA-256")){
                md = MessageDigest.getInstance("SHA-256");
            }
        } catch(NoSuchAlgorithmException e){
        }
        return bytesToHex(md.digest(data));
    }

    private String bytesToHex(byte[] bytes) {
        char[] hexArray = "0123456789ABCDEF".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    private void trustAllHosts(HttpsURLConnection connection) {
        X509TrustManager easyTrustManager = new X509TrustManager() {

            public void checkClientTrusted(
                    X509Certificate[] chain,
                    String authType) throws CertificateException {
                // Oh, I am easy!
            }

            public void checkServerTrusted(
                    X509Certificate[] chain,
                    String authType) throws CertificateException {
                // Oh, I am easy!
            }

            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

        };

        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] {easyTrustManager};

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");

            sc.init(null, trustAllCerts, new java.security.SecureRandom());

            connection.setSSLSocketFactory(sc.getSocketFactory());

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getOrderId() {
        Calendar cal = Calendar.getInstance();
        Date currentTime = TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), "GMT");
        SimpleDateFormat formatOrder = new SimpleDateFormat("yyyyMMddHHmmss");
        return formatOrder.format(currentTime) + (int)(Math.random() * 1000.0);
    }
}
