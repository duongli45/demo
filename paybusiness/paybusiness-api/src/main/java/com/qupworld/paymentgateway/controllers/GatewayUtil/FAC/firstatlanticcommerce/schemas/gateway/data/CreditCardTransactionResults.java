
package com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreditCardTransactionResults complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreditCardTransactionResults">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AVSResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AuthCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CVV2Result" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OriginalResponseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaddedCardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReasonCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReasonCodeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReferenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResponseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TokenizedPAN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditCardTransactionResults", propOrder = {
    "avsResult",
    "authCode",
    "cvv2Result",
    "originalResponseCode",
    "paddedCardNumber",
    "reasonCode",
    "reasonCodeDescription",
    "referenceNumber",
    "responseCode",
    "tokenizedPAN"
})
public class CreditCardTransactionResults {

    @XmlElement(name = "AVSResult", nillable = true)
    protected String avsResult;
    @XmlElement(name = "AuthCode", nillable = true)
    protected String authCode;
    @XmlElement(name = "CVV2Result", nillable = true)
    protected String cvv2Result;
    @XmlElement(name = "OriginalResponseCode", nillable = true)
    protected String originalResponseCode;
    @XmlElement(name = "PaddedCardNumber", nillable = true)
    protected String paddedCardNumber;
    @XmlElement(name = "ReasonCode", nillable = true)
    protected String reasonCode;
    @XmlElement(name = "ReasonCodeDescription", nillable = true)
    protected String reasonCodeDescription;
    @XmlElement(name = "ReferenceNumber", nillable = true)
    protected String referenceNumber;
    @XmlElement(name = "ResponseCode", nillable = true)
    protected String responseCode;
    @XmlElement(name = "TokenizedPAN", nillable = true)
    protected String tokenizedPAN;

    /**
     * Gets the value of the avsResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAVSResult() {
        return avsResult;
    }

    /**
     * Sets the value of the avsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAVSResult(String value) {
        this.avsResult = value;
    }

    /**
     * Gets the value of the authCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthCode() {
        return authCode;
    }

    /**
     * Sets the value of the authCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthCode(String value) {
        this.authCode = value;
    }

    /**
     * Gets the value of the cvv2Result property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCVV2Result() {
        return cvv2Result;
    }

    /**
     * Sets the value of the cvv2Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCVV2Result(String value) {
        this.cvv2Result = value;
    }

    /**
     * Gets the value of the originalResponseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalResponseCode() {
        return originalResponseCode;
    }

    /**
     * Sets the value of the originalResponseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalResponseCode(String value) {
        this.originalResponseCode = value;
    }

    /**
     * Gets the value of the paddedCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaddedCardNumber() {
        return paddedCardNumber;
    }

    /**
     * Sets the value of the paddedCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaddedCardNumber(String value) {
        this.paddedCardNumber = value;
    }

    /**
     * Gets the value of the reasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonCode() {
        return reasonCode;
    }

    /**
     * Sets the value of the reasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonCode(String value) {
        this.reasonCode = value;
    }

    /**
     * Gets the value of the reasonCodeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonCodeDescription() {
        return reasonCodeDescription;
    }

    /**
     * Sets the value of the reasonCodeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonCodeDescription(String value) {
        this.reasonCodeDescription = value;
    }

    /**
     * Gets the value of the referenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceNumber() {
        return referenceNumber;
    }

    /**
     * Sets the value of the referenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceNumber(String value) {
        this.referenceNumber = value;
    }

    /**
     * Gets the value of the responseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * Sets the value of the responseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseCode(String value) {
        this.responseCode = value;
    }

    /**
     * Gets the value of the tokenizedPAN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTokenizedPAN() {
        return tokenizedPAN;
    }

    /**
     * Sets the value of the tokenizedPAN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTokenizedPAN(String value) {
        this.tokenizedPAN = value;
    }

}
