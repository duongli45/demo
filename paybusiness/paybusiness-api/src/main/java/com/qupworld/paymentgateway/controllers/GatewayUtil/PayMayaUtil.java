package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.Base64Encoder;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import okhttp3.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by thuanho on 20/10/2021.
 */
public class PayMayaUtil {

    private final static Logger logger = LogManager.getLogger(PayMayaUtil.class);
    private static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
    private String requestId;
    private String PUBLIC_KEY;
    private String SECRET_KEY;
    private String REQUEST_URL;

    public PayMayaUtil(String _requestId, String publicKey, String secretKey, String url) {
        requestId = _requestId;
        PUBLIC_KEY = publicKey;
        SECRET_KEY = secretKey;
        REQUEST_URL = url;
    }

    public String initiateCreditForm(String orderId) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,Object> mapResponse = new HashMap<>();

        /*String htmlForm = "";
        try {
            String actionURL = REQUEST_URL + "/payment-tokens";
            String authToken = "Basic " + Base64Encoder.encodeString(PUBLIC_KEY + ":");
            String returnUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.PAYMAYA+"?urlAction=redirect";
            htmlForm = GatewayUtil.getFile(requestId,  "payMaya.html");
            htmlForm = htmlForm.replace("action_page", actionURL);
            htmlForm = htmlForm.replace("header_auth_value", authToken);
            htmlForm = htmlForm.replace("gwOrderid_value", orderId);
            htmlForm = htmlForm.replace("authenUrl_value", returnUrl);

            //logger.debug(requestId + " - htmlForm = " + htmlForm);
        } catch (Exception ex) {
            ex.printStackTrace();
        }*/

        String checkoutURL = "";
        try {
            String actionURL = REQUEST_URL + "/payment-tokens";
            String authToken = "Basic " + Base64Encoder.encodeString(PUBLIC_KEY + ":");
            String returnUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.PAYMAYA+"?urlAction=redirect";

            checkoutURL = ServerConfig.webform_powertranz + "/paymaya"
                    + "?action_page=" + URLEncoder.encode(actionURL, "UTF-8")
                    + "&header_auth=" + URLEncoder.encode(authToken, "UTF-8")
                    + "&gwOrderid=" + orderId
                    + "&authenUrl=" + URLEncoder.encode(returnUrl, "UTF-8")
                    ;

        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }

        objResponse.returnCode = 200;
        mapResponse.put("3ds_url", checkoutURL);
        mapResponse.put("type", "link");
        mapResponse.put("transactionId", orderId);
        mapResponse.put("redirect", false);
        mapResponse.put("gateway", KeysUtil.PAYMAYA);
        objResponse.response = mapResponse;
        return objResponse.toString();
    }

    public String createCustomer(String paymentTokenId, String orderId, String firstName, String lastName, String phone, String email) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        try {
            JSONObject objCustomer = new JSONObject();

            objCustomer.put("firstName", firstName);
            objCustomer.put("lastName", lastName);
            JSONObject objContact = new JSONObject();
            objContact.put("phone", phone);
            objContact.put("email", email);
            objCustomer.put("contact", objContact);

            JSONObject objAddress = new JSONObject();
            objAddress.put("firstName", firstName);
            objAddress.put("lastName", lastName);
            objAddress.put("phone", phone);
            objAddress.put("email", email);
            objCustomer.put("billingAddress", objAddress);
            objAddress.put("shippingType", "ST");
            objCustomer.put("shippingAddress", objAddress);

            logger.debug(requestId + " - objCustomer = " + objCustomer.toJSONString());
            String authenToken =  Base64Encoder.encodeString(SECRET_KEY + ":");
            RequestBody bodyCustomer = RequestBody.create(objCustomer.toJSONString(), JSON);
            Request requestCustomer = new Request.Builder()
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", "Basic " + authenToken)
                    .url(REQUEST_URL + "/customers")
                    .post(bodyCustomer)
                    .build();

            OkHttpClient client = new OkHttpClient.Builder().build();
            Response responseCustomer = client.newCall(requestCustomer).execute();
            String responseCustomerString = responseCustomer.body().string();
            logger.debug(requestId + " - responseCustomerString = " + responseCustomerString);
            org.json.JSONObject customerResponse = new org.json.JSONObject(responseCustomerString);
            /*{
                "id": "993bc6ae-44e3-49ab-abbe-73f15d4f462b",
                "firstName": "Test",
                "middleName": "",
                "lastName": "Test",
                "contact": {
                    "phone": "+84905123456",
                    "email": "hnthuan.dng@gmail.com"
                },
                "shippingAddress": {
                    "firstName": "Test",
                    "lastName": "Test",
                    "phone": "+84905123456",
                    "shippingType": "ST",
                    "email": "hnthuan.dng@gmail.com"
                },
                "sex": "",
                "birthday": "",
                "customerSince": "",
                "createdAt": "2021-10-21T11:13:45.000Z",
                "updatedAt": "2021-10-21T11:13:45.000Z"
            }*/
            String customerId = customerResponse.has("id") ? customerResponse.getString("id") : "";
            if (!customerId.isEmpty()) {
                JSONObject objBody = new JSONObject();
                String returnUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.PAYMAYA;
                objBody.put("isDefault", true);

                objBody.put("paymentTokenId", paymentTokenId);
                objBody.put("requestReferenceNumber", orderId);

                JSONObject objUrl = new JSONObject();
                objUrl.put("success", returnUrl+"?urlAction=success&gwOrderid="+orderId);
                objUrl.put("failure", returnUrl+"?urlAction=failure&gwOrderid="+orderId);
                objUrl.put("cancel", returnUrl+"?urlAction=cancel&gwOrderid="+orderId);
                objBody.put("redirectUrl", objUrl);
                logger.debug(requestId + " - body = " + objBody.toJSONString());

                RequestBody body = RequestBody.create(objBody.toJSONString(), JSON);
                Request request = new Request.Builder()
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Authorization", "Basic " + authenToken)
                        .url(REQUEST_URL + "/customers/"+customerId+"/cards")
                        .post(body)
                        .build();

                Response response = client.newCall(request).execute();
                String responseString = response.body().string();
                logger.debug(requestId + " - response = " + responseString);
                /*{
                    "cardTokenId": "hwPh3L47vB7jb8Z9J81vRmYBtAUh0cGSelLLoPIYXv1fNmRGi5rdDUm0jJqusOh8Da7hgJrN8fpT7gZl1up6ZHreyptLqSFQsXFnE2v4aAd1nMzgHmrfgEYGEnsp9SbHz7yXCRK93o416vvhu01lpOBwaLajYg2KAHE3crU",
                    "cardType": "jcb",
                    "maskedPan": "1049",
                    "createdAt": "2021-10-21T07:04:14.000Z",
                    "updatedAt": "2021-10-21T07:04:14.000Z",
                    "id": "f50ee0f1-0f91-4690-a6ef-c730d940d496",
                    "state": "PREVERIFICATION",
                    "default": true,
                    "verificationUrl": "https://payments-web-sandbox.paymaya.com/authenticate?id=f50ee0f1-0f91-4690-a6ef-c730d940d496"
                }*/
                org.json.JSONObject jsonResponse = new org.json.JSONObject(responseString);
                String verificationUrl = jsonResponse.has("verificationUrl") ? jsonResponse.getString("verificationUrl") : "";
                String cardTokenId = jsonResponse.has("cardTokenId") ? jsonResponse.getString("cardTokenId") : "";
                String maskedPan = jsonResponse.has("maskedPan") ? jsonResponse.getString("maskedPan") : "";
                String cardType = jsonResponse.has("cardType") ? jsonResponse.getString("cardType") : "";
                if (!verificationUrl.isEmpty()) {
                    objResponse.returnCode = 200;

                    // bind card token with customer
                    cardTokenId = customerId + KeysUtil.TEMPKEY + cardTokenId;

                    // format card type
                    if (cardType.equalsIgnoreCase("visa")) cardType = "Visa";
                    if (cardType.equalsIgnoreCase("master-card")) cardType = "MasterCard";
                    if (cardType.equalsIgnoreCase("jcb")) cardType = "JCB";

                    mapResponse.put("verificationUrl", verificationUrl);
                    mapResponse.put("cardTokenId", cardTokenId);
                    mapResponse.put("maskedPan", maskedPan);
                    mapResponse.put("cardType", cardType);
                    objResponse.response = mapResponse;
                    return objResponse.toString();
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }
        objResponse.returnCode = 525;
        objResponse.response = mapResponse;
        return objResponse.toString();
    }

    public String createPaymentWithCreditToken(String orderId, double amount, String token, String currencyISO) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        try {
            JSONObject objBody = new JSONObject();
            objBody.put("requestReferenceNumber", orderId);

            JSONObject objAmount = new JSONObject();
            objAmount.put("amount", amount);
            objAmount.put("currency", currencyISO);
            objBody.put("totalAmount", objAmount);

            logger.debug(requestId + " - body = " + objBody.toJSONString());

            String authenToken =  Base64Encoder.encodeString(SECRET_KEY + ":");
            RequestBody body = RequestBody.create(objBody.toJSONString(), JSON);
            String[] arrToken = token.split(KeysUtil.TEMPKEY);
            Request request = new Request.Builder()
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", "Basic " + authenToken)
                    .url(REQUEST_URL + "/customers/"+arrToken[0]+"/cards"+"/"+arrToken[1]+"/payments")
                    .post(body)
                    .build();
            OkHttpClient client = new OkHttpClient.Builder().build();
            Response response = client.newCall(request).execute();
            String responseString = response.body().string();
            logger.debug(requestId + " - response = " + responseString);
            /*
            {
                "id": "5682dbb8-74cf-40ad-9bb9-2dc63d04bbe9",
                "isPaid": true,
                "status": "PAYMENT_SUCCESS",
                "amount": "100",
                "currency": "PHP",
                "canVoid": true,
                "canRefund": false,
                "canCapture": false,
                "createdAt": "2020-05-12T15:12:24.000Z",
                "updatedAt": "2020-05-12T15:12:24.000Z",
                "description": "Charge for merchant@merchantsite.com",
                "paymentTokenId": "fIOA0DxV0WIqt4ij2qHa3ZCLKJks5A7th8Y7p8h4HK0zfipKRTtEV26sovKwbDaGwYPPsFelV2lJkrsBhvFJoary56nHjGWAGKPKn7E3XfD1pBDPv6m34V8uu1cdwSVOMiEsZlVdoMd7IR8Te124jnIdOIfqTvhO1lMDdk",
                "fundSource": {
                    "type": "card",
                    "id": "fIOA0DxV0WIqt4ij2qHa3ZCLKJks5A7th8Y7p8h4HK0zfipKRTtEV26sovKwbDaGwYPPsFelV2lJkrsBhvFJoary56nHjGWAGKPKn7E3XfD1pBDPv6m34V8uu1cdwSVOMiEsZlVdoMd7IR8Te124jnIdOIfqTvhO1lMDdk",
                    "description": "**** **** **** 2346",
                    "details": {
                        "scheme": "master-card",
                        "last4": "2346",
                        "first6": "512345",
                        "masked": "512345******2346",
                        "issuer": "Others"
                    }
                },
                "receipt": {
                    "transactionId": "749f27f4-daac-45a4-bd41-798c85782111",
                    "receiptNo": "0d703b6c9d3e",
                    "approval_code": "00001234",
                    "approvalCode": "00001234"
                },
                "approvalCode": "00001234",
                "receiptNumber": "0d703b6c9d3e",
                "requestReferenceNumber": "1551191039"
            }
             */
            org.json.JSONObject jsonResponse = new org.json.JSONObject(responseString);
            String status = jsonResponse.has("status") ? jsonResponse.getString("status") : "";
            String transId = "";
            String authCode = "";
            String cardNumber = "";
            String cardType = "";
            int returnCode = 525;
            String message = "";
            if (status.equals("PAYMENT_SUCCESS")) {
                returnCode = 200;
                message = "Success!";
                transId = jsonResponse.has("id") ? jsonResponse.getString("id") : "";
                authCode = jsonResponse.has("approvalCode") ? jsonResponse.getString("approvalCode") : "";
                org.json.JSONObject fundSource = jsonResponse.has("fundSource") ? jsonResponse.getJSONObject("fundSource") : null;
                if (fundSource != null) {
                    org.json.JSONObject details = fundSource.has("details") ? fundSource.getJSONObject("details") : null;
                    if (details != null) {
                        cardType = details.has("scheme") ? details.getString("scheme") : "";
                        if (cardType.equalsIgnoreCase("visa")) cardType = "Visa";
                        if (cardType.equalsIgnoreCase("master-card")) cardType = "MasterCard";
                        if (cardType.equalsIgnoreCase("jcb")) cardType = "JCB";

                        String last4 = details.has("last4") ? details.getString("last4") : "";
                        cardNumber = "XXXXXXXXXXXX" + last4;
                    }
                }
            }

            mapResponse.put("message", message);
            mapResponse.put("transId", transId);
            mapResponse.put("cardType", cardType);
            mapResponse.put("authCode", authCode);
            mapResponse.put("cardMask", cardNumber);
            objResponse.response = mapResponse;
            objResponse.returnCode = returnCode;
            return objResponse.toString();
        } catch(Exception ex) {
            logger.debug(requestId + " - createPaymentWithCreditToken exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            return objResponse.toString();
        }
    }

    public String preAuthPayment(String orderId, double amount, String token, String currencyISO) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        try {
            JSONObject objBody = new JSONObject();
            objBody.put("authorizationType", "PREAUTHORIZATION");
            objBody.put("requestReferenceNumber", orderId);

            JSONObject objAmount = new JSONObject();
            objAmount.put("amount", amount);
            objAmount.put("currency", currencyISO);
            objBody.put("totalAmount", objAmount);
            logger.debug(requestId + " - body = " + objBody.toJSONString());
            String[] arrToken = token.split(KeysUtil.TEMPKEY);
            String url = REQUEST_URL + "/customers/"+arrToken[0]+"/cards"+"/"+arrToken[1]+"/payments";
            logger.debug(requestId + " - url = " + url);
            String authenToken =  Base64Encoder.encodeString(SECRET_KEY + ":");
            RequestBody body = RequestBody.create(objBody.toJSONString(), JSON);

            Request request = new Request.Builder()
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", "Basic " + authenToken)
                    .url(url)
                    .post(body)
                    .build();
            OkHttpClient client = new OkHttpClient.Builder().build();
            Response response = client.newCall(request).execute();
            String responseString = response.body().string();
            logger.debug(requestId + " - response = " + responseString);
            /*
            {
                "id": "e8f50268-4001-4c54-b182-282013e80140",
                "isPaid": false,
                "status": "AUTHORIZED",
                "amount": "500",
                "currency": "PHP",
                "canVoid": true,
                "canRefund": false,
                "canCapture": true,
                "createdAt": "2021-11-05T07:48:19.000Z",
                "updatedAt": "2021-11-05T07:48:20.000Z",
                "description": "Charge for merchant@merchantsite.com",
                "paymentTokenId": "hwPh3L47vB7jb8Z9J81vRmYBtAUh0cGSelLLoPIYXv1fNmRGi5rdDUm0jJqusOh8Da7hgJrN8fpT7gZl1up6ZHreyptLqSFQsXFnE2v4aAd1nMzgHmrfgEYGEnsp9SbHz7yXCRK93o416vvhu01lpOBwaLajYg2KAHE3crU",
                "authorizationType": "PREAUTHORIZATION",
                "capturedAmount": "0",
                "fundSource": {
                    "type": "card",
                    "id": "hwPh3L47vB7jb8Z9J81vRmYBtAUh0cGSelLLoPIYXv1fNmRGi5rdDUm0jJqusOh8Da7hgJrN8fpT7gZl1up6ZHreyptLqSFQsXFnE2v4aAd1nMzgHmrfgEYGEnsp9SbHz7yXCRK93o416vvhu01lpOBwaLajYg2KAHE3crU",
                    "description": "**** **** **** 1049",
                    "details": {
                        "scheme": "jcb",
                        "last4": "1049",
                        "first6": "355099",
                        "masked": "355099******1049",
                        "issuer": "Others"
                    }
                },
                "receipt": {
                    "transactionId": "b248f9e4-d65d-4467-a90c-4fd0b6bfde6b",
                    "receiptNo": "19120bf311ba",
                    "approval_code": "00001234",
                    "approvalCode": "00001234"
                },
                "approvalCode": "00001234",
                "receiptNumber": "19120bf311ba",
                "requestReferenceNumber": "20211105074947475"
            }
             */
            org.json.JSONObject jsonResponse = new org.json.JSONObject(responseString);
            String status = jsonResponse.has("status") ? jsonResponse.getString("status") : "";
            String authId = "";
            String authCode = "";
            int returnCode = 525;
            if (status.equals("AUTHORIZED")) {
                returnCode = 200;
                authId = jsonResponse.has("id") ? jsonResponse.getString("id") : "";
                authCode = jsonResponse.has("approvalCode") ? jsonResponse.getString("approvalCode") : "";
            }
            mapResponse.put("authId", authId);
            mapResponse.put("authAmount", String.valueOf(amount));
            mapResponse.put("authCode", authCode);
            mapResponse.put("authToken", token);
            mapResponse.put("allowCapture", "true");

            objResponse.response = mapResponse;
            objResponse.returnCode = returnCode;
            return objResponse.toString();
        } catch(Exception ex) {
            logger.debug(requestId + " - preAuthPayment exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            return objResponse.toString();
        }
    }

    public String preAuthCapture(String bookId, double amount, String authId, String currencyISO) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        try {
            JSONObject objBody = new JSONObject();
            objBody.put("requestReferenceNumber", bookId);
            JSONObject objAmount = new JSONObject();
            objAmount.put("amount", amount);
            objAmount.put("currency", currencyISO);
            objBody.put("captureAmount", objAmount);

            logger.debug(requestId + " - body = " + objBody.toJSONString());

            String authenToken =  Base64Encoder.encodeString(SECRET_KEY + ":");
            RequestBody body = RequestBody.create(objBody.toJSONString(), JSON);
            Request request = new Request.Builder()
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", "Basic " + authenToken)
                    .url(REQUEST_URL + "/payments/"+authId+"/capture")
                    .post(body)
                    .build();
            OkHttpClient client = new OkHttpClient.Builder().build();
            Response response = client.newCall(request).execute();
            String responseString = response.body().string();
            logger.debug(requestId + " - response = " + responseString);
            /*
            {
                "id": "e8f50268-4001-4c54-b182-282013e80140",
                "isPaid": false,
                "status": "CAPTURED",
                "amount": "500",
                "currency": "PHP",
                "canVoid": false,
                "canRefund": false,
                "canCapture": true,
                "createdAt": "2021-11-05T07:48:19.000Z",
                "updatedAt": "2021-11-05T07:57:02.000Z",
                "description": "Charge for merchant@merchantsite.com",
                "authorizationType": "PREAUTHORIZATION",
                "capturedAmount": "300",
                "fundSource": {
                    "type": "card",
                    "id": "hwPh3L47vB7jb8Z9J81vRmYBtAUh0cGSelLLoPIYXv1fNmRGi5rdDUm0jJqusOh8Da7hgJrN8fpT7gZl1up6ZHreyptLqSFQsXFnE2v4aAd1nMzgHmrfgEYGEnsp9SbHz7yXCRK93o416vvhu01lpOBwaLajYg2KAHE3crU",
                    "description": "**** **** **** 1049",
                    "details": {
                        "scheme": "jcb",
                        "last4": "1049",
                        "first6": "355099",
                        "masked": "355099******1049",
                        "issuer": "Others"
                    }
                },
                "receipt": {
                    "transactionId": "b248f9e4-d65d-4467-a90c-4fd0b6bfde6b",
                    "receiptNo": "19120bf311ba",
                    "approval_code": "00001234",
                    "approvalCode": "00001234"
                },
                "approvalCode": "00001234",
                "receiptNumber": "19120bf311ba",
                "requestReferenceNumber": "20211105074947475",
                "capturedPaymentId": "32b40b2f-64da-4b3e-be09-7ffe78f1a987"
            }
             */
            org.json.JSONObject jsonResponse = new org.json.JSONObject(responseString);
            String status = jsonResponse.has("status") ? jsonResponse.getString("status") : "";
            String transId = "";
            String authCode = "";
            String cardNumber = "";
            String cardType = "";
            int returnCode = 525;
            String message = "";
            if (status.equals("CAPTURED")) {
                returnCode = 200;
                message = "Success!";
                transId = jsonResponse.has("id") ? jsonResponse.getString("id") : "";
                authCode = jsonResponse.has("approvalCode") ? jsonResponse.getString("approvalCode") : "";
                org.json.JSONObject fundSource = jsonResponse.has("fundSource") ? jsonResponse.getJSONObject("fundSource") : null;
                if (fundSource != null) {
                    org.json.JSONObject details = fundSource.has("details") ? fundSource.getJSONObject("details") : null;
                    if (details != null) {
                        cardType = details.has("scheme") ? details.getString("scheme") : "";
                        if (cardType.equalsIgnoreCase("visa")) cardType = "Visa";
                        if (cardType.equalsIgnoreCase("master-card")) cardType = "MasterCard";
                        if (cardType.equalsIgnoreCase("jcb")) cardType = "JCB";

                        String last4 = details.has("last4") ? details.getString("last4") : "";
                        cardNumber = "XXXXXXXXXXXX" + last4;
                    }
                }
            }

            mapResponse.put("message", message);
            mapResponse.put("transId", transId);
            mapResponse.put("cardType", cardType);
            mapResponse.put("authCode", authCode);
            mapResponse.put("cardMask", cardNumber);
            objResponse.response = mapResponse;
            objResponse.returnCode = returnCode;
            return objResponse.toString();
        } catch(Exception ex) {
            logger.debug(requestId + " - preAuthCapture exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            return objResponse.toString();
        }
    }

    public void voidTransaction(String authId) {
        try {
            JSONObject objBody = new JSONObject();
            objBody.put("reason", "cancel booking");
            logger.debug(requestId + " - body = " + objBody.toJSONString());

            String authenToken =  Base64Encoder.encodeString(SECRET_KEY + ":");
            RequestBody body = RequestBody.create(objBody.toJSONString(), JSON);
            Request request = new Request.Builder()
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", "Basic " + authenToken)
                    .url(REQUEST_URL + "/payments/"+authId+"/voids")
                    .post(body)
                    .build();
            OkHttpClient client = new OkHttpClient.Builder().build();
            Response response = client.newCall(request).execute();
            String responseString = response.body().string();
            logger.debug(requestId + " - response = " + responseString);
            /*
            {
                "id": "4a5b5e86-c3f3-4a24-8e08-5604ad053d27",
                "payment": "1e4ffaf0-329d-4005-a8f2-92682120cc73",
                "status": "SUCCESS",
                "reason": "cancel booking",
                "requestReferenceNumber": null,
                "voidAt": "2021-11-05T08:16:10.000Z",
                "createdAt": "2021-11-05T08:16:10.000Z",
                "updatedAt": "2021-11-05T08:16:10.000Z"
            }
             */
        } catch(Exception ex) {
            logger.debug(requestId + " - voidTransaction exception: " + CommonUtils.getError(ex));
        }
    }
}
