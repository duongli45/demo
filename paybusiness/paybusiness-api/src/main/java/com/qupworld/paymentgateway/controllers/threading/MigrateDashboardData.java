package com.qupworld.paymentgateway.controllers.threading;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.models.SQLDao;
import com.qupworld.paymentgateway.models.SQLDaoImpl;
import com.qupworld.paymentgateway.models.mongo.collections.bookingStatistic.BookingStatistic;
import com.qupworld.paymentgateway.models.mongo.collections.bookingStatistic.Statistic;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.TimezoneUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;

/**
 * Created by qup on 23/03/2020.
 */
@SuppressWarnings("unchecked")
public class MigrateDashboardData extends Thread {
    final static Logger logger = LogManager.getLogger(MigrateDashboardData.class);
    public String fleetId = "";
    public String from = "";
    public String to = "";
    public String currencyISO = "";
    public String requestId = "";

    @Override
    public void run() {
        try {

            SQLDao sqlDao = new SQLDaoImpl();
            boolean valid = true;
            Gson gson = new Gson();
            int size = 0;
            logger.debug(requestId + " - MIGRATE DASHBOARD DATA ...");
            Date fromDate = KeysUtil.SDF_DMYHMS.parse(from);
            Date fromServer = TimezoneUtil.offsetTimeZone(fromDate, Calendar.getInstance().getTimeZone().getID(), "GMT");

            Calendar calFrom = Calendar.getInstance();
            calFrom.setTime(fromServer);
            from = KeysUtil.SDF_DMYHMS.format(calFrom.getTime());

            Calendar calTo = Calendar.getInstance();
            calTo.setTime(fromServer);
            calTo.add(Calendar.MINUTE, 30);
            to = KeysUtil.SDF_DMYHMS.format(calTo.getTime());

            while (valid) {
                try {
                    ThreadUtil threadUtil = new ThreadUtil(requestId);
                    logger.debug(requestId + " - query ticket for fleet "+ fleetId +" from "+ from +" to " + to + " with currencyISO " + currencyISO);
                    ObjResponse response = sqlDao.getTicketInTimeRange(fleetId, from, to, currencyISO);
                    List<Map<String, Object>> listData = new ArrayList<>();
                    if (response.returnCode == 200)
                        listData = (List<Map<String, Object>>) response.response;
                    size = listData.size();
                    if (size > 0) {
                        logger.debug(requestId + " - size: " + size);
                        BookingStatistic bookingStatistic = new BookingStatistic();
                        bookingStatistic.fleetId = fleetId;
                        bookingStatistic.time = TimezoneUtil.formatISODate(calFrom.getTime(), "GMT");
                        bookingStatistic.currencyISO = currencyISO;
                        //init data
                        bookingStatistic.finished = 0;
                        bookingStatistic.profit = 0.0;
                        bookingStatistic.revenue = 0.0;
                        Statistic cc = new Statistic();
                        cc.canceled = 0;
                        cc.noShow = 0;
                        cc.incident = 0;
                        cc.completed = 0;
                        Statistic carHailing = new Statistic();
                        carHailing.canceled = 0;
                        carHailing.noShow = 0;
                        carHailing.incident = 0;
                        carHailing.completed = 0;
                        Statistic webBooking = new Statistic();
                        webBooking.canceled = 0;
                        webBooking.noShow = 0;
                        webBooking.incident = 0;
                        webBooking.completed = 0;
                        Statistic kiosk = new Statistic();
                        kiosk.canceled = 0;
                        kiosk.noShow = 0;
                        kiosk.incident = 0;
                        kiosk.completed = 0;
                        Statistic API = new Statistic();
                        API.canceled = 0;
                        API.noShow = 0;
                        API.incident = 0;
                        API.completed = 0;
                        Statistic dmc = new Statistic();
                        dmc.canceled = 0;
                        dmc.noShow = 0;
                        dmc.incident = 0;
                        dmc.completed = 0;
                        Statistic corp = new Statistic();
                        corp.canceled = 0;
                        corp.noShow = 0;
                        corp.incident = 0;
                        corp.completed = 0;
                        Statistic mDispatcher = new Statistic();
                        mDispatcher.canceled = 0;
                        mDispatcher.noShow = 0;
                        mDispatcher.incident = 0;
                        mDispatcher.completed = 0;
                        Statistic paxApp = new Statistic();
                        paxApp.canceled = 0;
                        paxApp.noShow = 0;
                        paxApp.incident = 0;
                        paxApp.completed = 0;

                        for (Map<String, Object> map : listData) {
                            bookingStatistic.finished += 1;
                            int pricingType = Integer.parseInt(map.get("pricingType").toString());
                            double revenue = pricingType == 0 ? Double.parseDouble(map.get("totalCharged").toString()) : Double.parseDouble(map.get("totalProvider").toString());
                            double profit = pricingType == 0 ? Double.parseDouble(map.get("netEarning").toString()) : Double.parseDouble(map.get("netEarningProvider").toString());
                            bookingStatistic.profit += profit;
                            bookingStatistic.revenue += revenue;
                            String transactionStatus = map.get("transactionStatus").toString();
                            String bookFrom = map.get("bookFrom").toString();
                            switch (bookFrom.toLowerCase()) {
                                case "cc" : {
                                    switch (transactionStatus) {
                                        case "canceled" :
                                            cc.canceled += 1;
                                            break;
                                        case "noShow" :
                                            cc.noShow += 1;
                                            break;
                                        case "incident" :
                                            cc.incident += 1;
                                            break;
                                        default:
                                            cc.completed += 1;
                                            break;
                                    }
                                }
                                break;
                                case "car-hailing" : {
                                    switch (transactionStatus) {
                                        case "canceled" :
                                            carHailing.canceled += 1;
                                            break;
                                        case "noShow" :
                                            carHailing.noShow += 1;
                                            break;
                                        case "incident" :
                                            carHailing.incident += 1;
                                            break;
                                        default:
                                            carHailing.completed += 1;
                                            break;
                                    }
                                }
                                break;
                                case "web booking" : {
                                    switch (transactionStatus) {
                                        case "canceled" :
                                            webBooking.canceled += 1;
                                            break;
                                        case "noShow" :
                                            webBooking.noShow += 1;
                                            break;
                                        case "incident" :
                                            webBooking.incident += 1;
                                            break;
                                        default:
                                            webBooking.completed += 1;
                                            break;
                                    }
                                }
                                break;
                                case "kiosk" : {
                                    switch (transactionStatus) {
                                        case "canceled" :
                                            kiosk.canceled += 1;
                                            break;
                                        case "noShow" :
                                            kiosk.noShow += 1;
                                            break;
                                        case "incident" :
                                            kiosk.incident += 1;
                                            break;
                                        default:
                                            kiosk.completed += 1;
                                            break;
                                    }
                                }
                                break;
                                case "api" : {
                                    switch (transactionStatus) {
                                        case "canceled" :
                                            API.canceled += 1;
                                            break;
                                        case "noShow" :
                                            API.noShow += 1;
                                            break;
                                        case "incident" :
                                            API.incident += 1;
                                            break;
                                        default:
                                            API.completed += 1;
                                            break;
                                    }
                                }
                                break;
                                case KeysUtil.dash_board : {
                                    switch (transactionStatus) {
                                        case "canceled" :
                                            dmc.canceled += 1;
                                            break;
                                        case "noShow" :
                                            dmc.noShow += 1;
                                            break;
                                        case "incident" :
                                            dmc.incident += 1;
                                            break;
                                        default:
                                            dmc.completed += 1;
                                            break;
                                    }
                                }
                                break;
                                case KeysUtil.corp_board : {
                                    switch (transactionStatus) {
                                        case "canceled" :
                                            corp.canceled += 1;
                                            break;
                                        case "noShow" :
                                            corp.noShow += 1;
                                            break;
                                        case "incident" :
                                            corp.incident += 1;
                                            break;
                                        default:
                                            corp.completed += 1;
                                            break;
                                    }
                                }
                                break;
                                case "mdispatcher" : {
                                    switch (transactionStatus) {
                                        case "canceled" :
                                            mDispatcher.canceled += 1;
                                            break;
                                        case "noShow" :
                                            mDispatcher.noShow += 1;
                                            break;
                                        case "incident" :
                                            mDispatcher.incident += 1;
                                            break;
                                        default:
                                            mDispatcher.completed += 1;
                                            break;
                                    }
                                }
                                break;
                                default: {
                                    switch (transactionStatus) {
                                        case "canceled" :
                                            paxApp.canceled += 1;
                                            break;
                                        case "noShow" :
                                            paxApp.noShow += 1;
                                            break;
                                        case "incident" :
                                            paxApp.incident += 1;
                                            break;
                                        default:
                                            paxApp.completed += 1;
                                            break;
                                    }
                                }
                                break;
                            }
                            bookingStatistic.cc = cc;
                            bookingStatistic.carHailing = carHailing;
                            bookingStatistic.webBooking = webBooking;
                            bookingStatistic.kiosk = kiosk;
                            bookingStatistic.API = API;
                            bookingStatistic.dmc = dmc;
                            bookingStatistic.corp = corp;
                            bookingStatistic.mDispatcher = mDispatcher;
                            bookingStatistic.paxApp = paxApp;
                        }
                        String body = gson.toJson(bookingStatistic);
                        logger.debug(requestId + " -  - dashboard body: " + body);
                        boolean sendSuccess = threadUtil.sendToReport(body, "dashboard", ServerConfig.server_report_dashboard);
                        if (!sendSuccess) {
                            // failed on the first time, try to re-authenticate token then send again
                            String authBody = "{ \"username\" : \"" + ServerConfig.auth_username + "\" , \"password\" : \"" + ServerConfig.auth_password + "\" }";
                            HttpResponse<JsonNode> jsonAuth = Unirest.post(ServerConfig.server_auth)
                                    .header("Content-Type", "application/json; charset=utf-8")
                                    .header("X-Trace-Request-Id", requestId)
                                    .body(authBody)
                                    .asJson();
                            JSONParser parser = new JSONParser();
                            JSONObject objectData = (JSONObject) parser.parse(jsonAuth.getBody().toString());
                            KeysUtil.TOKEN = objectData.get("token").toString();
                            // submit with new token
                            sendSuccess = threadUtil.sendToReport(body, "dashboard", ServerConfig.server_report_dashboard);
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    logger.debug(requestId + " - error: " + ex.getMessage());
                } finally {
                    Date currentTime = TimezoneUtil.offsetTimeZone(new Date(), Calendar.getInstance().getTimeZone().getID(), "GMT");
                    if (calFrom.getTime().after(currentTime)) {
                        valid = false;
                    }
                    if (valid) {
                        // next time range
                        calFrom.add(Calendar.MINUTE, 30);
                        from = KeysUtil.SDF_DMYHMS.format(calFrom.getTime());

                        calTo.add(Calendar.MINUTE, 30);
                        to = KeysUtil.SDF_DMYHMS.format(calTo.getTime());
                    }
                }
            }
            logger.debug(requestId + " - --- DONE !!!");
        } catch(Exception ex) {
            ex.printStackTrace();
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            logger.debug(requestId + " - MigrateDashboardData: "+ errors.toString());
        }

    }



}