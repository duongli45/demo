package com.qupworld.paymentgateway.controllers;

import com.google.gson.Gson;
import com.qupworld.paymentgateway.controllers.GatewayUtil.GatewayUtil;
import com.qupworld.paymentgateway.entities.*;
import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.MongoDaoImpl;
import com.qupworld.paymentgateway.models.SQLDao;
import com.qupworld.paymentgateway.models.SQLDaoImpl;
import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.BalanceByCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.wallet.MerchantWallet;
import com.pg.util.KeysUtil;
import com.pg.util.TimezoneUtil;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.text.ParseException;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
public class MerchantWalletUtil {

    private Gson gson;
    public SQLDao sqlDao;
    public MongoDao mongoDao;
    private String requestId;
    private final static Logger logger = LogManager.getLogger(MerchantWalletUtil.class);

    public MerchantWalletUtil(String requestId){
        gson = new Gson();
        sqlDao = new SQLDaoImpl();
        mongoDao = new MongoDaoImpl();
        this.requestId = requestId;
    }

    public double getCurrentBalance(String fleetId, String merchantId, String walletType, String currencyISO) throws ParseException {
        // check new wallet
        boolean update = false;
        MerchantWallet merchantWallet = mongoDao.getMerchantWallet(fleetId, merchantId);
        if (merchantWallet == null) {
            update = true;
        }
        if (update && !currencyISO.isEmpty()) {
            merchantWallet = new MerchantWallet();
            merchantWallet.fleetId = fleetId;
            merchantWallet.merchantId = merchantId;

            AmountByCurrency creditWallet = new AmountByCurrency();
            creditWallet.value = 0.0;
            creditWallet.currencyISO = currencyISO;
            merchantWallet.creditWallet = creditWallet;

            BalanceByCurrency cashWallet = new BalanceByCurrency();
            cashWallet.currentBalance = 0.0;
            cashWallet.currencyISO = currencyISO;
            merchantWallet.cashWallet = cashWallet;

            mongoDao.addMerchantWallet(merchantWallet);
        }
        double driverBalance = sqlDao.getMerchantWalletBalance(requestId, fleetId, merchantId, walletType, currencyISO);
        return KeysUtil.DECIMAL_FORMAT.parse(KeysUtil.DECIMAL_FORMAT.format(driverBalance)).doubleValue();
    }

    long updateCreditWallet(MerchantWalletTransaction merchantWalletTransaction, boolean isDeposit, double newBalance) {
        MerchantWalletBalance merchantWalletBalance = new MerchantWalletBalance();
        merchantWalletBalance.fleetId = merchantWalletTransaction.fleetId;
        merchantWalletBalance.merchantId = merchantWalletTransaction.merchantId;
        merchantWalletBalance.walletType = "credit";
        merchantWalletBalance.transactionType = merchantWalletTransaction.transactionType;
        merchantWalletBalance.transactionId = merchantWalletTransaction.transactionId;
        if (isDeposit) {
            merchantWalletBalance.cashIn = merchantWalletTransaction.amount;
            merchantWalletBalance.cashOut = 0.0;
        } else {
            merchantWalletBalance.cashIn = 0.0;
            merchantWalletBalance.cashOut = merchantWalletTransaction.amount*(-1); // change to positive amount
        }
        merchantWalletBalance.newBalance = newBalance;
        merchantWalletBalance.currencyISO = merchantWalletTransaction.currencyISO;
        merchantWalletBalance.createdDate = TimezoneUtil.getGMTTimestamp();
        return doAddDB(merchantWalletBalance);
    }

    long updateCashWallet(MerchantWalletTransaction merchantWalletTransaction, boolean isDeposit, double newBalance) {
        MerchantWalletBalance merchantWalletBalance = new MerchantWalletBalance();
        merchantWalletBalance.fleetId = merchantWalletTransaction.fleetId;
        merchantWalletBalance.merchantId = merchantWalletTransaction.merchantId;
        merchantWalletBalance.walletType = "cash";
        merchantWalletBalance.transactionType = merchantWalletTransaction.transactionType;
        merchantWalletBalance.transactionId = merchantWalletTransaction.transactionId;
        if (isDeposit) {
            merchantWalletBalance.cashIn = merchantWalletTransaction.amount;
            merchantWalletBalance.cashOut = 0.0;
        } else {
            merchantWalletBalance.cashIn = 0.0;
            merchantWalletBalance.cashOut = merchantWalletTransaction.amount*(-1); // change to positive amount
        }
        merchantWalletBalance.newBalance = newBalance;
        merchantWalletBalance.currencyISO = merchantWalletTransaction.currencyISO;
        merchantWalletBalance.createdDate = TimezoneUtil.getGMTTimestamp();
        return doAddDB(merchantWalletBalance);
    }

    private long doAddDB(MerchantWalletBalance merchantWalletBalance) {
        if (merchantWalletBalance.cashIn > 0 || merchantWalletBalance.cashOut > 0) {
            logger.debug(requestId + " - update MySQL: " + gson.toJson(merchantWalletBalance));
            long id = sqlDao.addMerchantWalletBalance(merchantWalletBalance);
            if (id == 0) {
                // something went wrong, try to add again
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException ignore) {}
                id = sqlDao.addMerchantWalletBalance(merchantWalletBalance);
                if (id == 0) {
                    logger.debug(requestId + " - add MerchantWalletBalance error!!!");
                    // send log to email for checking later
                    GatewayUtil.sendMailWalletTransaction(requestId, gson.toJson(merchantWalletBalance));
                }
            }
            if (id != 0) {
                AmountByCurrency creditWallet = new AmountByCurrency();
                BalanceByCurrency cashWallet = new BalanceByCurrency();
                if (merchantWalletBalance.walletType.equals("credit")) {
                    creditWallet.value = merchantWalletBalance.newBalance;
                    creditWallet.currencyISO = merchantWalletBalance.currencyISO;
                    logger.debug(requestId + " - update MongoDB: " + gson.toJson(creditWallet));
                } else {
                    cashWallet.currentBalance = merchantWalletBalance.newBalance;
                    cashWallet.currencyISO = merchantWalletBalance.currencyISO;
                    logger.debug(requestId + " - update MongoDB: " + gson.toJson(cashWallet));
                }
                boolean updated = mongoDao.updateMerchantWallet(merchantWalletBalance.fleetId, merchantWalletBalance.merchantId,
                        merchantWalletBalance.walletType, creditWallet, cashWallet);
                if (!updated) {
                    // something went wrong, try to add again
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException ignore) {
                    }
                    updated = mongoDao.updateMerchantWallet(merchantWalletBalance.fleetId, merchantWalletBalance.merchantId,
                            merchantWalletBalance.walletType, creditWallet, cashWallet);
                    if (!updated) {
                        logger.debug(requestId + " - add MerchantWalletBalance error!!!");
                        // send log to email for checking later
                        GatewayUtil.sendMailWalletTransaction(requestId, gson.toJson(merchantWalletBalance));
                        // still error - return 0
                        return 0;
                    }
                }
            }
            return id;
        }
        return 0;
    }

}
