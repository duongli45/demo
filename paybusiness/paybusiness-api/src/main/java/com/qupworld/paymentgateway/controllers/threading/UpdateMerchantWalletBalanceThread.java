package com.qupworld.paymentgateway.controllers.threading;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.entities.MerchantWalletTransaction;
import com.qupworld.service.QUpReportPublisher;
import com.pg.util.KeysUtil;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by qup on 23/03/2020.
 */
@SuppressWarnings("unchecked")
public class UpdateMerchantWalletBalanceThread extends Thread {
    public MerchantWalletTransaction merchantWalletTransaction;
    public String requestId;
    private final static Logger logger = LogManager.getLogger(UpdateMerchantWalletBalanceThread.class);
    @Override
    public void run() {
        String createdDate =  KeysUtil.SDF_DMYHMSTZ.format(merchantWalletTransaction.createdDate);
        try {
            if (ServerConfig.server_report_enable) {
                Gson gson = new GsonBuilder().serializeNulls().create();
                String merchantWalletTransactionStr = gson.toJson(merchantWalletTransaction);
                JsonElement jsonElement = gson.fromJson(merchantWalletTransactionStr, JsonElement.class);
                JsonObject jo = jsonElement.getAsJsonObject();
                jo.addProperty("createdDate",createdDate);

                QUpReportPublisher reportPublisher = QUpReportPublisher.getInstance();
                reportPublisher.sendOneItem(requestId, String.valueOf(merchantWalletTransaction.id), "MerchantWalletTransaction", gson.toJson(jo));

            }
        } catch(Exception ex) {
            ex.printStackTrace();
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            logger.debug(requestId + " - report MerchantWalletTransaction: "+ errors.toString());
        }
    }
}
