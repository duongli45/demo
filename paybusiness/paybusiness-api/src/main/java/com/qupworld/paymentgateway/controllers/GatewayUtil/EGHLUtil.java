package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.qupworld.config.ServerConfig;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
public class EGHLUtil {

    final static Logger logger = LogManager.getLogger(EGHLUtil.class);
    private static String URL;
    private static String SERVICE_ID;
    private static String PASSWORD;
    private final static DecimalFormat DF = new DecimalFormat("0.00");
    private static String requestId;

    public EGHLUtil(String _requestId, String serviceId, String password, String url) {
        requestId = _requestId;
        URL = url;
        SERVICE_ID = serviceId;
        PASSWORD = password;
    }

    public String initiateCreditForm(String orderId, String custName, String custEmail, String custPhone, String custIP, String currencyCode) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();

        String htmlForm = "";
        String orderNumber = "RC" + GatewayUtil.getOrderId();
        try {
            String serverURL = ServerConfig.dispatch_server;
            if (serverURL.equals("http://dispatch.local.qup.vn")) {
                serverURL = "http://113.160.232.234:1337";
            }
            String returnURL = serverURL + KeysUtil.NOTIFICATION_URL + "?OrderNumber="+orderNumber;
            String callBackURL = serverURL + KeysUtil.NOTIFICATION_URL + "?OrderNumber="+orderNumber;

            int pageTimeout = 10*60; // time to disable all entry fields and buttons
            String Amount = "1.00";

            String hashString = PASSWORD + SERVICE_ID + orderId + returnURL + callBackURL + Amount + currencyCode + custIP + pageTimeout;
            String hashValue = getHash(hashString);
            logger.debug(requestId + " - hashString: " + hashString);
            logger.debug(requestId + " - hashValue: " + hashValue);

            htmlForm = GatewayUtil.getFile(requestId, "eghl_card.jsp");
            htmlForm = htmlForm.replace("action_page", URL);

            htmlForm = htmlForm.replace("ServiceID_Value", SERVICE_ID);
            htmlForm = htmlForm.replace("PaymentID_Value", orderId);
            htmlForm = htmlForm.replace("OrderNumber_Value", orderNumber);
            htmlForm = htmlForm.replace("MerchantReturnURL_Value", returnURL);
            htmlForm = htmlForm.replace("MerchantCallbackURL_Value", callBackURL);
            htmlForm = htmlForm.replace("Amount_Value", Amount);
            htmlForm = htmlForm.replace("CurrencyCode_Value", currencyCode);
            htmlForm = htmlForm.replace("CustName_Value", custName);
            htmlForm = htmlForm.replace("CustEmail_Value", custEmail);
            htmlForm = htmlForm.replace("CustPhone_Value", custPhone);
            htmlForm = htmlForm.replace("CustIP_Value", custIP);
            htmlForm = htmlForm.replace("HashValue_Value", hashValue);
            htmlForm = htmlForm.replace("PageTimeout_Value", String.valueOf(pageTimeout));

            htmlForm = htmlForm.replace("\"","'").replace("\n","").replace("\r", "");
            System.out.println(" - htmlForm = " + htmlForm);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        objResponse.returnCode = 200;
        mapResponse.put("3ds_url", htmlForm);
        mapResponse.put("type", "form");
        mapResponse.put("transactionId", orderNumber);
        objResponse.response = mapResponse;
        return objResponse.toString();
    }

    public String payByToken(String bookId, String token, double amount,
                             String custName, String custEmail, String custPhone, String custIP, String currencyCode) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,Object> mapResponse = new HashMap<>();

        String htmlForm = "";
        String orderId = GatewayUtil.getOrderId();
        String orderNumber = "PM" + bookId;
        try {
            String serverURL = ServerConfig.dispatch_server;
            if (serverURL.equals("http://dispatch.local.qup.vn")) {
                serverURL = "http://113.160.232.234:1337";
            }
            String returnURL = serverURL + KeysUtil.NOTIFICATION_URL;
            String callBackURL = serverURL + KeysUtil.NOTIFICATION_URL + "?OrderNumber="+orderNumber;

            int pageTimeout = 10*60; // time to disable all entry fields and buttons
            String Amount = DF.format(amount);

            String hashString = PASSWORD + SERVICE_ID + orderId + returnURL + callBackURL + Amount + currencyCode + custIP + pageTimeout + token;
            String hashValue = getHash(hashString);
            logger.debug(requestId + " - hashString: " + hashString);
            logger.debug(requestId + " - hashValue: " + hashValue);

            htmlForm = GatewayUtil.getFile(requestId, "eghl_pay.jsp");
            htmlForm = htmlForm.replace("action_page", URL);

            htmlForm = htmlForm.replace("TransactionType_value", "SALE");
            htmlForm = htmlForm.replace("ServiceID_Value", SERVICE_ID);
            htmlForm = htmlForm.replace("PaymentID_Value", orderId);
            htmlForm = htmlForm.replace("OrderNumber_Value", orderNumber);
            htmlForm = htmlForm.replace("PaymentDesc_Value", "Payment for booking #"+bookId);
            htmlForm = htmlForm.replace("MerchantReturnURL_Value", returnURL);
            htmlForm = htmlForm.replace("MerchantCallbackURL_Value", callBackURL);
            htmlForm = htmlForm.replace("Amount_Value", Amount);
            htmlForm = htmlForm.replace("CurrencyCode_Value", currencyCode);
            htmlForm = htmlForm.replace("CustName_Value", custName);
            htmlForm = htmlForm.replace("CustEmail_Value", custEmail);
            htmlForm = htmlForm.replace("CustPhone_Value", custPhone);
            htmlForm = htmlForm.replace("CustIP_Value", custIP);
            htmlForm = htmlForm.replace("HashValue_Value", hashValue);
            htmlForm = htmlForm.replace("Token_Value", token);
            htmlForm = htmlForm.replace("PageTimeout_Value", String.valueOf(pageTimeout));

            htmlForm = htmlForm.replace("\"","'").replace("\n","").replace("\r", "");
            System.out.println(" - htmlForm = " + htmlForm);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        objResponse.returnCode = 528;
        mapResponse.put("3ds_url", htmlForm);
        mapResponse.put("type", "form");
        mapResponse.put("cache", false);
        mapResponse.put("transactionId", orderNumber);
        objResponse.response = mapResponse;
        return objResponse.toString();
    }

    public String doReversal(String paymentId, double amount, String currencyCode) {
        ObjResponse response = new ObjResponse();
        try {
            String hashString = PASSWORD + SERVICE_ID + paymentId + amount + currencyCode;
            String hashValue = getHash(hashString);
            ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
            postParameters.add(new BasicNameValuePair("TransactionType", "RSALE"));
            postParameters.add(new BasicNameValuePair("PymtMethod", "ANY"));
            postParameters.add(new BasicNameValuePair("ServiceID", SERVICE_ID));
            postParameters.add(new BasicNameValuePair("PaymentID", paymentId));
            postParameters.add(new BasicNameValuePair("Amount", DF.format(amount)));
            postParameters.add(new BasicNameValuePair("CurrencyCode", currencyCode));
            postParameters.add(new BasicNameValuePair("HashValue", hashValue));
            for (NameValuePair param : postParameters) {
                logger.debug(requestId + " - " + param.getName() + ": " + param.getValue());
            }
            HttpClient client = HttpClientBuilder.create().build();

            HttpPost httpRequest = new HttpPost(URL);
            httpRequest.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));

            HttpResponse httpResponse = client.execute(httpRequest);
            String captureResponse = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
            logger.debug(requestId + " - preAuthCapture result: " + captureResponse);
            return captureResponse;
        } catch (Exception ex) {
            logger.debug(requestId + " - preAuthCapture exception" + GatewayUtil.getError(ex));
            response.returnCode = 437;
        }
        return response.toString();
    }

    private static String getHash(String hashString) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] bytes = md.digest(hashString.getBytes(StandardCharsets.UTF_8));
            // Convert byte array into signum representation
            BigInteger number = new BigInteger(1, bytes);

            // Convert message digest into hex value
            StringBuilder hexString = new StringBuilder(number.toString(16));

            // Pad with leading zeros
            while (hexString.length() < 32)
            {
                hexString.insert(0, '0');
            }

            String hashValue = hexString.toString();
            if (hashValue.length() < 64)
                hashValue = "0" + hashValue;
            return hashValue;
        } catch (Exception ex) {
            logger.debug(requestId + " - getHash exception: " + ex.getMessage());
        }
        return "";
    }
}
