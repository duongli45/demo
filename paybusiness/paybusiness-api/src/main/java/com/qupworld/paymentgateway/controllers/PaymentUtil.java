package com.qupworld.paymentgateway.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.pg.util.CommonArrayUtils;
import com.pg.util.CommonUtils;
import com.pg.util.ConvertUtil;
import com.pg.util.EstimateUtil;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.SecurityUtil;
import com.pg.util.SlackAPI;
import com.pg.util.TimezoneUtil;
import com.pg.util.ValidCreditCard;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.controllers.GatewayUtil.*;
import com.qupworld.paymentgateway.controllers.threading.CompleteWithCCThread;
import com.qupworld.paymentgateway.controllers.threading.UpdatePaxWalletThread;
import com.qupworld.paymentgateway.controllers.threading.UpdateToDispatchingThread;
import com.qupworld.paymentgateway.entities.*;
import com.qupworld.paymentgateway.models.*;
import com.qupworld.paymentgateway.models.mongo.collections.*;
import com.qupworld.paymentgateway.models.mongo.collections.account.*;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateAssignedRate.AffiliateAssignedRate;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareFlat.AffiliateFlat;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareGeneral.RateGeneral;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareHourly.AffiliateHourly;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareRegular.CancellationPolicy;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareRegular.NoShowPolicy;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareRegular.RateRegular;
import com.qupworld.paymentgateway.models.mongo.collections.affiliatePackages.AffiliatePackages;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateProcess.AffiliateProcess;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateRoute.AffiliateRoute;
import com.qupworld.paymentgateway.models.mongo.collections.affiliationCarType.AffiliationCarType;
import com.qupworld.paymentgateway.models.mongo.collections.airportFee.AirportFee;
import com.qupworld.paymentgateway.models.mongo.collections.airportZone.AirportZone;
import com.qupworld.paymentgateway.models.mongo.collections.booking.*;
import com.qupworld.paymentgateway.models.mongo.collections.booking.Booking;
import com.qupworld.paymentgateway.models.mongo.collections.ccPackages.CCModule;
import com.qupworld.paymentgateway.models.mongo.collections.ccPackages.CCPackages;
import com.qupworld.paymentgateway.models.mongo.collections.corporate.Corporate;
import com.qupworld.paymentgateway.models.mongo.collections.failedCard.FailedCard;
import com.qupworld.paymentgateway.models.mongo.collections.fareNormal.FareNormal;
import com.qupworld.paymentgateway.models.mongo.collections.fareNormal.FeesByCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.flatRoutes.FlatRoutes;
import com.qupworld.paymentgateway.models.mongo.collections.fleet.*;
import com.qupworld.paymentgateway.models.mongo.collections.fleet.Currency;
import com.qupworld.paymentgateway.models.mongo.collections.fleet.PreAuthorized;
import com.qupworld.paymentgateway.models.mongo.collections.fleetfare.FleetFare;
import com.qupworld.paymentgateway.models.mongo.collections.gateway.*;
import com.qupworld.paymentgateway.models.mongo.collections.logToken.LogToken;
import com.qupworld.paymentgateway.models.mongo.collections.packageRate.FeesWithCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.packageRate.PackageRate;
import com.qupworld.paymentgateway.models.mongo.collections.passengerInfo.OutStanding;
import com.qupworld.paymentgateway.models.mongo.collections.passengerInfo.PassengerInfo;
import com.qupworld.paymentgateway.models.mongo.collections.paxReferral.PaxReferral;
import com.qupworld.paymentgateway.models.mongo.collections.paxReferral.RefereePromo;
import com.qupworld.paymentgateway.models.mongo.collections.pricingplan.PricingPlan;
import com.qupworld.paymentgateway.models.mongo.collections.promotionCode.PromotionCode;
import com.qupworld.paymentgateway.models.mongo.collections.referral.Referral;
import com.qupworld.paymentgateway.models.mongo.collections.referral.ReferralHistories;
import com.qupworld.paymentgateway.models.mongo.collections.serverInformation.ServerInformation;
import com.qupworld.paymentgateway.models.mongo.collections.serviceFee.ServiceFee;
import com.qupworld.paymentgateway.models.mongo.collections.vehicleType.CorpRate;
import com.qupworld.paymentgateway.models.mongo.collections.vehicleType.Rate;
import com.qupworld.paymentgateway.models.mongo.collections.vehicleType.VehicleType;
import com.qupworld.paymentgateway.models.mongo.collections.wallet.WalletMOLPay;
import com.qupworld.paymentgateway.models.mongo.collections.zone.Zone;
import com.qupworld.service.QUpReportPublisher;
import com.qupworld.util.*;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.xml.bind.DatatypeConverter;
import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map;

/**
 * Created by qup on 7/26/16.
 */
@SuppressWarnings("unchecked")
public class PaymentUtil {

    public RedisDao redisDao;
    public MongoDao mongoDao;
    public SQLDao sqlDao;
    public EstimateUtil estimateUtil;
    private Gson gson;
    private final static Logger logger = LogManager.getLogger(PaymentUtil.class);
    private final SimpleDateFormat sdfMongo = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
    private final SimpleDateFormat sdfSaveRedis = new SimpleDateFormat("yyyy-MM-dd");
    private final SimpleDateFormat sdfSaveRedisWithHour = new SimpleDateFormat("yyyy-MM-dd-HH");
    private int LIMIT = 0;
    private boolean RETRY = false;
    private int DURATION = 0;

    private static String getReport = ServerConfig.logs_server+"/api/service/getReport";
    private static String addlogs = ServerConfig.logs_server+"/api/service/addLogs";
    private static String updateBookingToDispatch = ServerConfig.dispatch_server+"/api/v2/booking/update_buy_fare";
    private static String sendTopupResultToDispatch = ServerConfig.dispatch_server+"/api/v2/topup_successful";
    private static String notifyUpdateBalanceToDispatch = ServerConfig.dispatch_server+"/api/v2/update_wallet_balance";
    private static String sendTipResultToDispatch = ServerConfig.dispatch_server+"/api/v2/tip_result";
    private static String sendOutstandingResultToDispatch = ServerConfig.dispatch_server+"/api/v2/pay_outstanding_successful";
    private static String sendPaymentResultToDispatch = ServerConfig.dispatch_server+"/api/v2/completedByWallet";
    private static String sendPaymentFailedToDispatch = ServerConfig.dispatch_server+"/api/v2/payment/failed";
    private static String sendChargeIntercityResultToDispatch = ServerConfig.dispatch_server+"/api/v2/hooks/paid";
    public static String sendRegisterCardResultToCC = ServerConfig.dispatch_server+"/api/v2/payment/register-card";
    public static String sendRegisterCardResultToWB = ServerConfig.dispatch_server+"/api/v2/payment/wb-register-card";
    static String webhook_update_jupiter = ServerConfig.dispatch_server+"/api/v2/stripe/webhook";
    static String add_card_senangpay = ServerConfig.dispatch_server+"/api/v2/add_card_senangpay";
    public static String payout_dnb = ServerConfig.dispatch_server+"/api/v2/dnb/result";
    public static String notify_topup = ServerConfig.point_system_url+"/api/notify/topup";
    public static String split_payment_completed = ServerConfig.point_system_url+"/api/notify/split_payment_completed";
    public static String notify_connect_status = ServerConfig.dispatch_server+"/api/notify/stripe-connect-status";
    public static String invoice_update = ServerConfig.dispatch_server+"/api/v2/invoice/update";
    public static String invoice_notify = ServerConfig.invoice_server+"/api/invoice/paid";
    public static String notify_operator = ServerConfig.dispatch_server+"/api/v2/payment/notify-operator";

    public static List<String> listIBANCountries = Arrays.asList("AT", "BH", "BE", "BA", "BG", "HR", "CY",
            "CZ", "DK", "EE", "FO", "FI", "FR", "GE", "DE", "GI", "GR", "GL", "HU", "IS", "IE", "IM",
            "IT", "JE", "JO", "KZ", "KW", "LV", "LB", "LI", "LT", "LU", "MK", "MT", "MD", "MC", "ME", "NL",
            "NO", "PS", "PL", "PT", "QA", "RO", "SM", "SA", "SK", "SI", "ES", "SE", "CH", "TN", "TR", "AE");
    public static List<String> listEUCountries = Arrays.asList("AT", "BE", "BG", "HR", "CY", "CZ", "DK", "EE",
            "FI", "FR", "DE", "GR", "HU", "IE", "IT", "LV", "LT", "LU", "MT", "NL", "PL", "PT", "RO", "SK",
            "SI", "ES", "SE", "AL", "AD", "AM", "BY", "BA", "FO", "GE", "GI", "IS", "IM", "XK", "LI", "MK",
            "MD", "MC", "NO", "RU", "SM", "RS", "CH", "TR", "UA", "GB", "VA");
    public static List<String> listRequiredRounting = Arrays.asList("AR", "AU", "BO", "BR", "CL",
            "CO", "DO", "HK", "ID", "IN", "JP", "PY", "SG", "TH", "TT", "US", "UY");

    public static final List<String> methodIncreasePoint =  Arrays.asList("credit", "razerPay", "gCash", "zainCash", "kushok",
            "telebirr", "ksher", "payTM", "payDunya", "bog", "wingbank", KeysUtil.PAYMENT_APPLEPAY, KeysUtil.PAYMENT_GOOGLEPAY);

    public static Map<String,String> mapStripeMethod = new HashMap<>();

    public PaymentUtil() {
        gson = new Gson();
        redisDao = new RedisImpl();
        mongoDao = new MongoDaoImpl();
        sqlDao = new SQLDaoImpl();
        estimateUtil = new EstimateUtil();

        mapStripeMethod.put("card", "Credit Card");
        mapStripeMethod.put("affirm", "Affirm");
        mapStripeMethod.put("promptpay", "PromptPay");
        mapStripeMethod.put("bacs_debit", "BACS Debit");
        mapStripeMethod.put("bancontact", "Bancontact");
        mapStripeMethod.put("blik", "Blik");
        mapStripeMethod.put("boleto", "Boleto");
        mapStripeMethod.put("cashapp", "Cash App");
        mapStripeMethod.put("eps", "EPS");
        mapStripeMethod.put("fpx", "FPX");
        mapStripeMethod.put("giropay", "Giropay");
        mapStripeMethod.put("grabpay", "GrabPay");
        mapStripeMethod.put("ideal", "iDEAL");
        mapStripeMethod.put("klarna", "Klarna");
        mapStripeMethod.put("konbini", "Konbini");
        mapStripeMethod.put("link", "Link");
        mapStripeMethod.put("oxxo", "OXXO");
        mapStripeMethod.put("p24", "Przelewy24");
        mapStripeMethod.put("paynow", "PayNow");
        mapStripeMethod.put("paypal", "PayPal");
        mapStripeMethod.put("pix", "PIX");
        mapStripeMethod.put("afterpay_clearpay", "Afterpay Clearpay");
        mapStripeMethod.put("alipay", "Alipay");
        mapStripeMethod.put("au_becs_debit", "AU BECS Debit");
        mapStripeMethod.put("sepa_debit", "SEPA Debit");
        mapStripeMethod.put("sofort", "SOFORT");
        mapStripeMethod.put("us_bank_account", "US Bank Account");
        mapStripeMethod.put("wechat_pay", "WeChat Pay");
        mapStripeMethod.put("google_pay", "Google Pay");
        mapStripeMethod.put("apple_pay", "Apple Pay");
    }

    String doBeforePayment(PaymentEnt paymentEnt) {
        List<Double> listValues = Arrays.asList(paymentEnt.tollFee, paymentEnt.otherFees, paymentEnt.parkingFee, paymentEnt.gasFee);
        return doBeforePayment(paymentEnt.bookId, listValues, paymentEnt.requestId);
    }
    String doBeforePayment(String bookId, List<Double> listValues, String requestId) {
        ObjResponse response = new ObjResponse();
        // check payment status - return if booking has been paid
        String payResult = getPaymentStatus(bookId, requestId);
        if (getReturnCode(payResult) != 200) {
            logger.debug(requestId + " - payment status: " + payResult);
            return payResult;
        }
        // check Booking
        Booking booking = mongoDao.getBooking(bookId);
        if(booking == null){
            logger.debug(requestId + " - booking is null - return");
            response.returnCode = 406;
            return response.toString();
        }

        boolean isLocalZone = booking.pricingType == 0 || booking.isFarmOut;

        // get gateway
        Fleet fleet = mongoDao.getFleetInfor(booking.fleetId);
        if (booking.isFarmOut) {
            fleet = mongoDao.getFleetInfor(booking.request.psgFleetId);
        }
        ConfigGateway configGateway = getConfigGateway(fleet, "", booking.request.pickup.geo);
        String gateway = configGateway != null ? configGateway.gateway : "";

        //check value can input
        if (!listValues.isEmpty()) {
            int returnCode = checkLimitValueInput(fleet, booking, listValues);
            if (returnCode != 200) {
                response.returnCode = returnCode;
                return response.toString();
            }
        }

        // lock booking - return if booking has been locked
        if (!KeysUtil.GATEWAY_3DS.contains(gateway) && !gateway.equals(KeysUtil.CXPAY)) {
            if (redisDao.lockBooking(bookId, requestId)) {
                response.returnCode = 423;
                return response.toString();
            }
        } else {
            // ignore check lock booking - in 2 cases
            // - request timed out
            // - continue to run the 2nd request
            redisDao.lockBooking(bookId, requestId);
        }

        Map<String,Object> map = new HashMap<>();
        map.put("booking", booking);
        map.put("fleet", fleet);
        map.put("isLocalZone", isLocalZone);
        map.put("gateway", gateway);

        response.returnCode = 200;
        response.response = map;
        return response.toString();

    }

    String doBeforeTopup(String userId, String requestId) {
        ObjResponse response = new ObjResponse();

        if (redisDao.lockTopup(userId, requestId)) {
            response.returnCode = 423;
        } else {
            response.returnCode = 200;
        }

        return response.toString();
    }

    String getPaymentStatus(String bookId, String requestId){
        ObjResponse paymentResponse = new ObjResponse();

        // check Redis
        if (redisDao.checkIfCompleted(requestId, bookId)) {
            logger.debug(requestId + " - bookId " + bookId + " has payment completed !!!!!!!!!!");
            TicketReturn ticket = sqlDao.getTicket(bookId);
            /// SUPPORT FOR OLD APP
            Booking booking = mongoDao.getBooking(bookId);
            if (booking == null) {
                booking = mongoDao.getBookingCompleted(bookId);
            }
            Fleet fleet = mongoDao.getFleetInfor(booking.fleetId);
            JSONObject ticketData = createReturnTicket(ticket, fleet, booking, true, requestId);
            if (ticketData == null) {
                ticketData = createReturnTicket(ticket, fleet, booking, false, requestId);
            }

            Map<String, Object> mapData = new HashMap<>();

            mapData.put("ticket", ticketData);

            // create temp object for old app - WILL BE REMOVED after a few versions
            Map<String, String> mapPayment = new HashMap<>();
            mapPayment.put("type", ticket.transactionStatus);
            mapPayment.put("cardTye", ticket.cardType);
            mapPayment.put("approvalCode", ticket.approvalCode);
            mapData.put("payment", mapPayment);
            paymentResponse.response = mapData;

            // NEW APP
            // paymentResponse.response = createReturnTicket(ticket);

            paymentResponse.returnCode = 425;
            return paymentResponse.toString();
        } else {
            paymentResponse.returnCode = 200;
            return paymentResponse.toString();
        }
    }

    TicketEnt getTicketDataFromBooking(String requestId, Booking booking, double distanceTour, double subTotal) {
        TicketEnt ticketEnt = new TicketEnt();
        ticketEnt.bookId = booking.bookId;
        ticketEnt.fleetId = booking.fleetId;
        int pricingType = booking.pricingType;
        if (booking.request != null) {
            ticketEnt.psgFleetId = booking.request.psgFleetId != null ? booking.request.psgFleetId : "";
            ticketEnt.rideSharing = booking.request.rideSharing != null ? booking.request.rideSharing: false;
            if (booking.request.pickup != null) {
                ticketEnt.pickup = booking.request.pickup.address != null ? CommonUtils.removeSpecialCharacter(booking.request.pickup.address) : "";
                if (ticketEnt.pickup.length() > 255)
                    ticketEnt.pickup = ticketEnt.pickup.substring(0,255);
                ticketEnt.timeZonePickup = booking.request.pickup.timezone != null ? booking.request.pickup.timezone : "";
                ticketEnt.locationFrom = booking.request.pickup.from != null ? booking.request.pickup.from : "gg";
                if (booking.request.pickup.geo != null && booking.request.pickup.geo.toArray().length == 2) {
                    Object[] arr = booking.request.pickup.geo.toArray();
                    ticketEnt.pickupLon = (Double) arr[0];
                    ticketEnt.pickupLat = (Double) arr[1];
                }
            }
            if (booking.request.destination != null) {
                ticketEnt.destination = booking.request.destination.address != null ? CommonUtils.removeSpecialCharacter(booking.request.destination.address) : "";
                if (ticketEnt.destination.length() > 255)
                    ticketEnt.destination = ticketEnt.destination.substring(0,255);
                ticketEnt.timeZoneDestination = booking.request.destination.timezone != null ? booking.request.destination.timezone : "";
                if (booking.request.destination.geo != null && booking.request.destination.geo.toArray().length == 2) {
                    Object[] arr = booking.request.destination.geo.toArray();
                        ticketEnt.destinationLon = (Double) arr[0];
                        ticketEnt.destinationLat = (Double) arr[1];

                }
            }
            String comment = booking.request.note != null ? booking.request.note : "";
            ticketEnt.receiptComment = CommonUtils.removeSpecialCharacter(comment);
            if (ticketEnt.receiptComment.length() > 2550)
                ticketEnt.receiptComment = ticketEnt.receiptComment.substring(0,2550);
            ticketEnt.promoCode = booking.request.promo != null ? booking.request.promo : "";
            if (pricingType == 0) {
                ticketEnt.vehicleType = booking.request.vehicleTypeRequest != null ? CommonUtils.removeSpecialCharacter(booking.request.vehicleTypeRequest) : "";
                if (ticketEnt.vehicleType.length() > 255)
                    ticketEnt.vehicleType = ticketEnt.vehicleType.substring(0,255);
            }

            ticketEnt.paymentType = booking.request.paymentType != null ? booking.request.paymentType : 0;

            if (booking.request.estimate != null && booking.request.estimate.fare != null) {
                ticketEnt.remainingAmount = booking.request.estimate.fare.remainingAmount;
            }

            List<FleetService> fleetServices = booking.request.fleetServices != null ? booking.request.fleetServices : new ArrayList<>();
            ticketEnt.fleetServices = getFleetServicesFromBooking(requestId, fleetServices, subTotal, false);

        }
        String bookFrom = booking.bookFrom != null ? booking.bookFrom : "";
        ticketEnt.bookFrom = bookFrom;
        ticketEnt.platform = booking.platform;
        if (booking.operator != null) {
            ticketEnt.userName = booking.operator.name != null ? CommonUtils.removeSpecialCharacter(booking.operator.name) : "";
            String userId = booking.operator.userId != null ? booking.operator.userId : "";
            if (!userId.equals("")) {
                UsersSystem usersSystem = mongoDao.getUsersSystem(userId);
                if (usersSystem != null) {
                    ticketEnt.agentId = usersSystem.agentId;
                }
            }
        }
        if (booking.offeredQueue != null && !booking.offeredQueue.isEmpty()) {
            ticketEnt.dispatch = booking.offeredQueue.size();
        }
        String queueId = booking.queueId != null ? booking.queueId : "";
        if(!queueId.equals("")) {
            QueuingArea queuingArea = mongoDao.getQueuingArea(queueId);
            if (queuingArea != null) {
                ticketEnt.queueId = queueId;
                ticketEnt.queueName = CommonUtils.removeSpecialCharacter(queuingArea.name);
            }
        }
        ticketEnt.dispatcherId = booking.dispatcherId != null ? booking.dispatcherId : "";
        if (booking.mDispatcherInfo != null) {
            String userId = booking.mDispatcherInfo.userId != null ? booking.mDispatcherInfo.userId : "";
            ticketEnt.mDispatcherId = userId;
            if (!userId.equals("")) {
                Account account = mongoDao.getAccount(userId);
                if (account != null) {
                    ticketEnt.mDispatcherName = CommonUtils.removeSpecialCharacter(account.fullName);
                    if (ticketEnt.mDispatcherName.length() > 255)
                        ticketEnt.mDispatcherName = ticketEnt.mDispatcherName.substring(0,255);
                    if (account.mDispatcherInfo != null && account.mDispatcherInfo.partnerType != null) {
                        ticketEnt.mDispatcherTypeId = account.mDispatcherInfo.partnerType.partnerTypeId != null ? account.mDispatcherInfo.partnerType.partnerTypeId : "";
                        ticketEnt.mDispatcherTypeName = account.mDispatcherInfo.partnerType.name != null ? CommonUtils.removeSpecialCharacter(account.mDispatcherInfo.partnerType.name) : "";
                    }
                }
            }
        }
        if (booking.psgInfo != null) {
            ticketEnt.customerId = booking.psgInfo.userId != null ? booking.psgInfo.userId : "";
            String firstName = booking.psgInfo.firstName != null ? booking.psgInfo.firstName : "";
            String lastName = booking.psgInfo.lastName != null ? booking.psgInfo.lastName : "";
            ticketEnt.customerName = CommonUtils.removeSpecialCharacter(firstName + " " + lastName);
            if (ticketEnt.customerName.length() > 255)
                ticketEnt.customerName = ticketEnt.customerName.substring(0,255);
            ticketEnt.customerPhone = booking.psgInfo.phone != null ? booking.psgInfo.phone : "";
            ticketEnt.psgEmail = booking.psgInfo.email != null ? booking.psgInfo.email : "";
            ticketEnt.rank = booking.psgInfo.rank != null ? booking.psgInfo.rank : 0;
        }
        String vehicleId = "";
        String plateNumber = "";
        if (booking.drvInfo != null) {
            String userId = booking.drvInfo.userId != null ? booking.drvInfo.userId : "";
            ticketEnt.driverId = userId;
            String firstName = booking.drvInfo.firstName != null ? booking.drvInfo.firstName : "---";
            String lastName = booking.drvInfo.lastName != null ? booking.drvInfo.lastName : "---";
            ticketEnt.driverName = CommonUtils.removeSpecialCharacter(firstName + " " + lastName);
            if (ticketEnt.driverName.length() > 255)
                ticketEnt.driverName = ticketEnt.driverName.substring(0,255);
            if (!userId.equals("")) {
                Account account = mongoDao.getAccount(userId);
                if (account != null) {
                    ticketEnt.phone = account.phone;
                    if (account.driverInfo != null) {
                        ticketEnt.driverNumber = account.driverInfo.drvId != null ? account.driverInfo.drvId : "";
                        ticketEnt.driverLicenseNumber = account.driverInfo.driverLicenseNumber != null ? CommonUtils.removeSpecialCharacter(account.driverInfo.driverLicenseNumber) : "";
                        if (ticketEnt.driverLicenseNumber.length() > 255)
                            ticketEnt.driverLicenseNumber = ticketEnt.driverLicenseNumber.substring(0,255);
                        if (account.driverInfo.company != null) {
                            ticketEnt.companyId = account.driverInfo.company.companyId != null ? account.driverInfo.company.companyId : "";
                            ticketEnt.companyName = account.driverInfo.company.name != null ? CommonUtils.removeSpecialCharacter(account.driverInfo.company.name) : "";
                            if (ticketEnt.companyName.length() > 255)
                                ticketEnt.companyName = ticketEnt.companyName.substring(0,255);
                        }
                    }
                }
            }
            if(booking.dispatch3rd){
                ticketEnt.phone = booking.drvInfo.phone != null ? booking.drvInfo.phone:"";
                ticketEnt.companyId = booking.drvInfo.companyId != null ? booking.drvInfo.companyId:"";
                ticketEnt.companyName = booking.drvInfo.companyName != null ? CommonUtils.removeSpecialCharacter(booking.drvInfo.companyName):"";
                if (ticketEnt.companyName.length() > 255)
                    ticketEnt.companyName = ticketEnt.companyName.substring(0,255);
            }
            vehicleId = booking.drvInfo.vehicleId != null ? booking.drvInfo.vehicleId.toString() : "";
            ticketEnt.vehicleId = vehicleId;
            ticketEnt.vhcId = booking.drvInfo.vhcId != null ? booking.drvInfo.vhcId: "";
            plateNumber = booking.drvInfo.plateNumber != null ? booking.drvInfo.plateNumber : "";
            if (pricingType != 0) {
                ticketEnt.vehicleType = booking.drvInfo.vehicleType != null ? CommonUtils.removeSpecialCharacter(booking.drvInfo.vehicleType) : "";
                if (ticketEnt.vehicleType.length() > 255)
                    ticketEnt.vehicleType = ticketEnt.vehicleType.substring(0,255);
            }
            String driverVehicleID = booking.drvInfo.vehicleId != null ? booking.drvInfo.vehicleId.toString() : "";
            String driverVehicleType = booking.drvInfo.vehicleType != null ? CommonUtils.removeSpecialCharacter(booking.drvInfo.vehicleType) : "";
            JSONObject driverVehicleInfo = new JSONObject();
            driverVehicleInfo.put("driverVehicleID", driverVehicleID);
            driverVehicleInfo.put("driverVehicleType", driverVehicleType);
            String info = driverVehicleInfo.toJSONString();
            if (info.length() > 255) {
                // cut down driverVehicleType length
                int over = info.length() - 255;
                driverVehicleType = driverVehicleType.substring(0, driverVehicleType.length() - over);
                // re-add driverVehicleType
                driverVehicleInfo.remove("driverVehicleType");
                driverVehicleInfo.put("driverVehicleType", driverVehicleType);
                info = driverVehicleInfo.toJSONString();
            }
            ticketEnt.driverVehicleInfo = info;

        }
        ticketEnt.plateNumber = CommonUtils.removeSpecialCharacter(plateNumber);
        if (ticketEnt.plateNumber.length() > 255)
            ticketEnt.plateNumber = ticketEnt.plateNumber.substring(0,255);
        StringBuilder permission = new StringBuilder("");
        if (!vehicleId.equals("")) {
            Vehicle vehicle = mongoDao.getVehicle(vehicleId);
            if (vehicle != null) {
                List<String> listRoles = new ArrayList<>();
                if(vehicle.listRoles!= null && !vehicle.listRoles.isEmpty())
                    listRoles = vehicle.listRoles;
                if (listRoles.size() > 0) {
                    for (int i = 0; i < listRoles.size(); i++) {
                        Roles roles = mongoDao.getRoles(listRoles.get(i));
                        if (roles != null) {
                            if (i == listRoles.size() - 1) {
                                permission.append(roles.name);
                            } else {
                                permission.append(roles.name).append(", ");
                            }
                        }
                    }
                }
            }
        }
        ticketEnt.permission = CommonUtils.removeSpecialCharacter(permission.toString());
        if (ticketEnt.permission.length() > 100)
            ticketEnt.permission = ticketEnt.permission.substring(0,100);
        ticketEnt.pricingType = booking.pricingType;
        ticketEnt.currencySymbol = booking.currencySymbol;
        ticketEnt.currencyISO = booking.currencyISO;
        ticketEnt.distanceTour = distanceTour;
        Calendar cal = Calendar.getInstance();
        Date engagedTime;
        Date pickupTime;
        Date droppedOffTime;
        Date expectedPickupTime;
        try {
            if (booking.time != null) {
                String pickupStr = booking.time.pickUpTime != null ? sdfMongo.format(booking.time.pickUpTime) : "";
                if (!pickupStr.equals("")) {
                    pickupTime = TimezoneUtil.offsetTimeZone(sdfMongo.parse(pickupStr), cal.getTimeZone().getID(), "GMT");
                    ticketEnt.pickupTime = new Timestamp(pickupTime.getTime());
                }
                String createdStr = booking.time.created != null ? sdfMongo.format(booking.time.created) : "";
                if (!createdStr.equals("")) {
                    Date createdTime = TimezoneUtil.offsetTimeZone(sdfMongo.parse(createdStr), cal.getTimeZone().getID(), "GMT");
                    ticketEnt.createdTime = new Timestamp(createdTime.getTime());
                }
                String engagedStr = booking.time.engaged != null ? sdfMongo.format(booking.time.engaged) : "";
                if (!engagedStr.equals("")) {
                    engagedTime = TimezoneUtil.offsetTimeZone(sdfMongo.parse(engagedStr), cal.getTimeZone().getID(), "GMT");
                    ticketEnt.engagedTime = new Timestamp(engagedTime.getTime());
                }

                String droppedOffStr = booking.time.droppedOff != null ? sdfMongo.format(booking.time.droppedOff) : "";
                if (!droppedOffStr.isEmpty()) {
                    droppedOffTime = TimezoneUtil.offsetTimeZone(sdfMongo.parse(droppedOffStr), cal.getTimeZone().getID(), "GMT");
                } else {
                    droppedOffTime = TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), "GMT");
                }
                ticketEnt.droppedOffTime = new Timestamp(droppedOffTime.getTime());

                String expectedPickupTimeStr = booking.time.expectedPickupTime != null ? sdfMongo.format(booking.time.expectedPickupTime) : "";
                if (!expectedPickupTimeStr.isEmpty()) {
                    expectedPickupTime = TimezoneUtil.offsetTimeZone(sdfMongo.parse(expectedPickupTimeStr), cal.getTimeZone().getID(), "GMT");
                    ticketEnt.expectedPickupTime = new Timestamp(expectedPickupTime.getTime());
                }

            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }

        ticketEnt.typeBooking = booking.reservation ? "reservation" : "asap";
        if(booking.intercity)
            ticketEnt.typeBooking = "intercity";
        if(booking.delivery)
            ticketEnt.typeBooking = "delivery";
        if(booking.jobType != null && booking.jobType.equals("shuttle"))
            ticketEnt.typeBooking = "shuttle";
        if(booking.jobType != null && booking.jobType.equals("streetSharing")) {
            /*ticketEnt.typeBooking = "streetSharing";*/
            ticketEnt.distanceTour = booking.streetSharing != null ? booking.streetSharing.distance : 0.0;
        }
        if (booking.corporateInfo != null) {
            String corporateId = booking.corporateInfo.corporateId != null ? booking.corporateInfo.corporateId : "";
            ticketEnt.corporateId = corporateId;
            ticketEnt.clientCaseMatter = booking.corporateInfo.clientCaseMatter != null ? CommonUtils.removeSpecialCharacter(booking.corporateInfo.clientCaseMatter) : "";
            if (ticketEnt.clientCaseMatter.length() > 50)
                ticketEnt.clientCaseMatter = ticketEnt.clientCaseMatter.substring(0,50);
            ticketEnt.chargeCode = booking.corporateInfo.chargeCode != null ? CommonUtils.removeSpecialCharacter(booking.corporateInfo.chargeCode) : "";
            if (ticketEnt.chargeCode.length() > 50)
                ticketEnt.chargeCode = ticketEnt.chargeCode.substring(0,50);
            if (!corporateId.equals("")) {
                Corporate corporate = mongoDao.getCorporate(corporateId);
                if (corporate != null && corporate.companyInfo != null) {
                    ticketEnt.corporateName = corporate.companyInfo.name != null ? CommonUtils.removeSpecialCharacter(corporate.companyInfo.name) : "";
                    if (ticketEnt.corporateName.length() > 100)
                        ticketEnt.corporateName = ticketEnt.corporateName.substring(0,100);
                }
            }
        }

        // add data for hydra
        if (booking.request.estimate.fare != null) {
            ticketEnt.qupPreferredAmount = booking.request.estimate.fare.qupPreferredAmount;
            ticketEnt.qupBuyPrice = booking.request.estimate.fare.etaFare;
            ticketEnt.fleetMarkup = booking.request.estimate.fare.fleetMarkup;
            ticketEnt.sellPriceMarkup = booking.request.estimate.fare.sellPriceMarkup;
            ticketEnt.merchantCommission = booking.request.estimate.fare.merchantCommission;
        }
        ticketEnt.hydraPaymentMethod = booking.hydraInfo != null && booking.hydraInfo.hydraPaymentMethod != null ? booking.hydraInfo.hydraPaymentMethod : "";
        ticketEnt.isFarmOut = booking.isFarmOut;

        boolean isLocalZone = pricingType == 0;
        Map<String,String> mapToken = getCreditToken(booking, isLocalZone);
        ticketEnt.token = mapToken.get("token");
        ticketEnt.intercity = booking.intercity;
        ticketEnt.delivery = booking.delivery;
        ticketEnt.tripId = booking.tripId != null ? booking.tripId : "";
        ticketEnt.intercityInfo = booking.intercityInfo != null ? gson.toJson(booking.intercityInfo) : "";
        return ticketEnt;
    }

    public int addOrUpdateTicket(final String bookId, PaymentEnt paymentEnt){
        //round off total before update payment data
        Booking booking = mongoDao.getBooking(bookId);
        Fleet fleet = mongoDao.getFleetInfor(booking.fleetId);
        TicketReturn ticketReturn = sqlDao.getTicket(bookId);
        if(ticketReturn != null && ticketReturn.meetDriverFee != 0 && !paymentEnt.transactionStatus.equals(KeysUtil.PAYMENT_INCIDENT))
            paymentEnt.meetDriverFee = ticketReturn.meetDriverFee;
        double unroundedTotalAmt = paymentEnt.fare + paymentEnt.rushHour + paymentEnt.otherFees + paymentEnt.serviceFee +
                paymentEnt.airportSurcharge + paymentEnt.meetDriverFee + paymentEnt.tollFee + paymentEnt.parkingFee + paymentEnt.gasFee + paymentEnt.techFee +
                paymentEnt.partnerCommission + paymentEnt.tax + paymentEnt.tip - paymentEnt.promoAmount;
        if(paymentEnt.creditTransactionFee > 0)
            unroundedTotalAmt = unroundedTotalAmt + paymentEnt.creditTransactionFee;
        if(paymentEnt.transactionStatus.equals(KeysUtil.PAYMENT_INCIDENT)){
            unroundedTotalAmt = 0.0;
        }
        if(unroundedTotalAmt <0)
            unroundedTotalAmt = 0;
        logger.debug(paymentEnt.requestId + " - transactionStatus: " + paymentEnt.transactionStatus);
        logger.debug(paymentEnt.requestId + " - unroundedTotalAmt: " + unroundedTotalAmt);
        //double unroundedTotalAmt = paymentEnt.total;`
        if(!KeysUtil.INCOMPLETE_PAYMENT.contains(paymentEnt.transactionStatus)){
            // update total for hydra booking
            if (booking.pricingType == 1) {
                paymentEnt.totalCharged = paymentEnt.total;
                double exchangeRate = 1;
                ServerInformation serverInformation = mongoDao.getServerInformation();
                for (com.qupworld.paymentgateway.models.mongo.collections.serverInformation.Currency currency : serverInformation.currencies) {
                    if (currency.iso.equals(booking.currencyISO)) {
                        exchangeRate = currency.exchangeRate;
                        break;
                    }
                }
                if (exchangeRate == 0) exchangeRate = 1.0;
                paymentEnt.exchangeRate = exchangeRate;
            } else {
                paymentEnt.total = roundingValue(paymentEnt.total, fleet.rounding, booking.currencyISO);
            }
        }

        logger.debug(paymentEnt.requestId + " - addPaymentData paymentEnt: " + gson.toJson(paymentEnt));
        int addPaymentData = sqlDao.addPaymentData(bookId, paymentEnt, unroundedTotalAmt);
        if (addPaymentData == 0) {
            // update failed - add new ticket
            logger.debug(paymentEnt.requestId + " - TICKET FOR BookId " + bookId + " IS NOT EXIST!!! - ADD NEW TICKET");
            paymentEnt.payFromCC = true; // complete from CC without drop from driver app
            TicketEnt ticketEnt = getTicketDataFromBooking(paymentEnt.requestId, booking, 0, paymentEnt.subTotal);
            boolean bookingFeeActive = estimateUtil.getBookingFeeactive(booking);
            double surchargeParameter = 0;
            double dynamicFare = 0;
            //get dynamic surcharge
            if(booking.request != null && booking.request.estimate != null){
                if(booking.request.estimate.surchargeParameter != null && booking.request.estimate.surchargeParameter >= 1){
                    surchargeParameter = booking.request.estimate.surchargeParameter;
                }else if (booking.request.estimate.fare != null && booking.request.estimate.fare.surchargeParameter >= 1) {
                    surchargeParameter = booking.request.estimate.fare.surchargeParameter;
                }
                if(booking.request.estimate.dynamicFare != null){
                    dynamicFare = booking.request.estimate.dynamicFare;
                }else if (booking.request.estimate.fare != null && booking.request.estimate.fare.dynamicFare != null) {
                    dynamicFare = booking.request.estimate.fare.dynamicFare;
                }
            }
            if(paymentEnt.dynamicFare > 0){
                dynamicFare = paymentEnt.dynamicFare;
            }
            ticketEnt.bookingFeeActive = bookingFeeActive ? 1 : 0;
            ticketEnt.serviceFee = paymentEnt.serviceFee;
            ticketEnt.fare = paymentEnt.fare;
            if (paymentEnt.distanceTour > 0)
                ticketEnt.distanceTour = paymentEnt.distanceTour;
            if(paymentEnt.meetDriverFee > 0)
                ticketEnt.meetDriverFee = paymentEnt.meetDriverFee;
            if(paymentEnt.calBasicFare > 0)
                ticketEnt.calBasicFare = paymentEnt.calBasicFare;
            if(paymentEnt.additionalFare != null)
                ticketEnt.additionalFare = paymentEnt.additionalFare;
            if(paymentEnt.dynamicSurcharge > 0)
                ticketEnt.dynamicSurcharge = paymentEnt.dynamicSurcharge;
            ticketEnt.surchargeParameter = surchargeParameter;
            ticketEnt.dynamicFare = dynamicFare;

            // update actual remaining amount base on total amount and current pax wallet balance
            int paymentTpe = booking.request != null ? booking.request.paymentType : 0;
            if (paymentTpe == 13) {
                double currentPaxBalance = 0.0;
                try {
                    PaxWalletUtil paxWalletUtil = new PaxWalletUtil(paymentEnt.requestId);
                    currentPaxBalance = paxWalletUtil.getCurrentBalance(ticketEnt.fleetId, ticketEnt.customerId, "paxWallet", booking.currencyISO);
                } catch (Exception ex) {
                    logger.debug(paymentEnt.requestId + " - exception: " + CommonUtils.getError(ex));
                }
                double remainingAmount = paymentEnt.total > currentPaxBalance ? paymentEnt.total - currentPaxBalance : 0;
                ticketEnt.remainingAmount = Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(remainingAmount));
            }
            logger.debug(paymentEnt.requestId + " - paymentEnt before addTicketData: " + gson.toJson(paymentEnt));
            double customerDebt = getCustomerDebt(paymentEnt.requestId, paymentEnt.fleetId, booking.psgInfo.userId, booking, booking.currencyISO, true, paymentEnt.writeOffDebt);
            logger.debug(paymentEnt.requestId + " - customerDebt: " + customerDebt);
            ticketEnt.customerDebt = customerDebt;
            TicketReturn ticket = sqlDao.addTicketData(ticketEnt);
            if (ticket == null) {
                // add ticket has failed - try with remove note and add ticket again
                ticketEnt.receiptComment = "";
                ticket = sqlDao.addTicketData(ticketEnt);
            }
            if (ticket != null) {
                // add ticket success, add payment data
                logger.debug(paymentEnt.requestId + " - paymentEnt before addPaymentData: " + gson.toJson(paymentEnt));
                addPaymentData = sqlDao.addPaymentData(bookId, paymentEnt, unroundedTotalAmt);
                if (addPaymentData == 1) {
                    logger.debug(paymentEnt.requestId + " - UPDATE TICKET FOR BookId " + bookId + " HAS COMPLETED!!!");
                    return 200;
                } else {
                    logger.debug(paymentEnt.requestId + " - UPDATE TICKET FOR BookId " + bookId + " HAS FAILED!!!");
                    return 418;
                }
            } else {
                // cannot add new ticket - return error
                logger.debug(paymentEnt.requestId + " - ADD TICKET FOR BookId " + bookId + " HAS FAILED!!!");
                return 418;
            }
        } else {
            logger.debug(paymentEnt.requestId + " - UPDATE TICKET FOR BookId " + bookId + " HAS COMPLETED!!!");
            // update completed - return
            return 200;
        }
    }

    String completePayment(Booking booking, String requestId) {
        return doCompleted(booking, requestId);
    }

    String completePayment(Booking booking, String gateway, String requestId) {
        return doCompleted(booking, requestId);
    }

    private String doCompleted(Booking booking, String requestId) {
        DecimalFormat df = new DecimalFormat("#.##");
        logger.debug(requestId + " - PAYMENT SUCCESS FOR bookId "+ booking.bookId +" !!!!!!!!!!");
        // UPDATE TICKET DATA
        // payResult contain data return from gateway

        // get ticket and return
        ObjResponse paymentResponse = new ObjResponse();
        paymentResponse.returnCode = 200;

        /// SUPPORT FOR OLD APP
        Map<String, Object> mapData = new HashMap<>();
        TicketReturn ticket = sqlDao.getTicket(booking.bookId);
        Fleet fleet = mongoDao.getFleetInfor(ticket.fleetId);
        JSONObject objTicket = createReturnTicket(ticket, fleet, booking, true, requestId);
        if (objTicket == null) {
            objTicket = createReturnTicket(ticket, fleet, booking, false, requestId);
        }

        // replace data from booking
        if (booking.request != null) {
            if (booking.request.pickup != null) {
                objTicket.remove("pickup");
                objTicket.put("pickup", booking.request.pickup.address);
            }
            if (booking.request.destination != null) {
                objTicket.remove("destination");
                objTicket.put("destination", booking.request.destination.address);
            }
            if (booking.request.extraDestination != null) {
                objTicket.remove("extraDestination");
                objTicket.put("extraDestination", booking.request.extraDestination);
            }
            String comment = booking.request.note != null ? booking.request.note : "";
            objTicket.remove("receiptComment");
            objTicket.put("receiptComment", comment);

            String bookingNotes = booking.request.notes != null ? booking.request.notes : "";
            objTicket.remove("bookingNotes");
            objTicket.put("bookingNotes", bookingNotes);

            String operatorNote = booking.request.operatorNote != null ? booking.request.operatorNote : "";
            objTicket.remove("operatorNote");
            objTicket.put("operatorNote", operatorNote);
        }

        Account passenger = null;
        String boostId = ""; // catch Boost data support show cancel status
        if (booking.psgInfo != null) {
            String id = booking.psgInfo.userId != null ? booking.psgInfo.userId : "";
            passenger = mongoDao.getAccount(id);
            String firstName = booking.psgInfo.firstName != null ? booking.psgInfo.firstName : "";
            String lastName = booking.psgInfo.lastName != null ? booking.psgInfo.lastName : "";

            objTicket.remove("customerName");
            objTicket.put("customerName", firstName + " " + lastName);

            boostId = booking.psgInfo.boostId != null ? booking.psgInfo.boostId : "";
        }
        if (!boostId.isEmpty()) {
            String transactionStatus = ticket.transactionStatus;
            String canceller = ticket.canceller;
            if (transactionStatus.equals(KeysUtil.PAYMENT_INCIDENT) || transactionStatus.equals(KeysUtil.PAYMENT_NOSHOW) ||
                    (transactionStatus.equals(KeysUtil.PAYMENT_CANCELED) && !canceller.equals("passenger"))) {
                logger.debug(requestId + " - add cache for boostId: " + boostId + " - " + transactionStatus + " - " + canceller);
                // calculate credit back value
                double creditValue = 0.0;
                boolean fullRefund = true;
                if (booking.preAuthorized != null) {
                    creditValue = booking.preAuthorized.authAmount;
                    if (ticket.total > 0) {
                        fullRefund = false;
                        creditValue = booking.preAuthorized.authAmount - ticket.total;
                    }
                }

                JSONObject objCache = new JSONObject();
                objCache.put("bookId", booking.bookId);
                objCache.put("transactionStatus", transactionStatus);
                objCache.put("fullRefund", fullRefund);
                objCache.put("amount", creditValue);
                objCache.put("canceller", canceller);
                objCache.put("currencyISO", ticket.currencyISO);
                redisDao.cacheData(requestId, KeysUtil.BOOST+boostId+"cancelInfo", objCache.toJSONString(), 10);
            }
        }
        if (booking.drvInfo != null) {
            String firstName = booking.drvInfo.firstName != null ? booking.drvInfo.firstName : "---";
            String lastName = booking.drvInfo.lastName != null ? booking.drvInfo.lastName : "---";

            objTicket.remove("driverName");
            objTicket.put("driverName", firstName + " " + lastName);

            String vehicleType = booking.drvInfo.vehicleType != null ? booking.drvInfo.vehicleType : "";
            objTicket.remove("vehicleType");
            objTicket.remove("driverVehicleType");
            objTicket.put("vehicleType", vehicleType);
            objTicket.put("driverVehicleType", vehicleType);

            String plateNumber = booking.drvInfo.plateNumber != null ? booking.drvInfo.plateNumber : "";
            objTicket.remove("plateNumber");
            objTicket.put("plateNumber", plateNumber);
        }

        if(booking.createdByOperator != null){
            String userId = booking.createdByOperator.userId != null ? booking.createdByOperator.userId : "";
            String name = booking.createdByOperator.name != null ? booking.createdByOperator.name : "";
            objTicket.remove("operatorId");
            objTicket.remove("operatorName");
            objTicket.put("operatorId", userId);
            objTicket.put("operatorName", name);
        }

        if (booking.deliveryInfo != null) {
            objTicket.remove("deliveryInfo");
            objTicket.put("deliveryInfo", booking.deliveryInfo);
        }

        // add credit data
        Map<String,String> mapCredit = getCreditToken(booking, booking.pricingType == 0);
        String gateway = mapCredit.get("gateway");
        JSONObject objectCredit = new JSONObject();
        objectCredit.put("cardMask", mapCredit.get("cardMask"));
        objectCredit.put("gateway", gateway);
        objectCredit.put("cardHolder", mapCredit.get("cardHolder"));
        objectCredit.put("cardType", mapCredit.get("cardType"));
        objTicket.put("creditInfo", objectCredit);

        boolean shortTripChanged = false;
        //get pickup zone
        String zoneId = "";
        Zone zone = mongoDao.findByFleetIdAndGeo(fleet.fleetId, booking.request.pickup.geo);
        if(zone!= null)
            zoneId = zone._id.toString();
        boolean shortTrip = false;
        //service type
        String serviceType = "transport";
        if(booking.intercity)
            serviceType = "intercity";
        if(booking.superHelper)
            serviceType = "delivery";
        if(booking.delivery){
            if(booking.jobType.equals("delivery") || booking.jobType.equals("shopForYou")){
                serviceType = "delivery";
            } else {
                serviceType = booking.jobType;
            }
        }
        try {
            if (!KeysUtil.INCOMPLETE_PAYMENT.contains(ticket.transactionStatus)){
                shortTrip = estimateUtil.checkWaiveOffCommission(fleet, zoneId, ticket.total, ticket.rushHour,
                        ticket.currencyISO, TimezoneUtil.convertServerToGMT(booking.time.pickUpTime),
                        booking.request.pickup.timezone, serviceType, requestId);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        if(booking.request != null && booking.request.estimate != null && booking.request.estimate.fare != null){
            if(booking.request.estimate.fare.shortTrip && !shortTrip)
                shortTripChanged = true;
        }
        double serviceFee = 0;
        if(booking.request.serviceActive){
            if(booking.request.services!= null && !booking.request.services.isEmpty()){
                serviceFee = booking.request.services.stream().filter(s ->s.active).mapToDouble(s -> s.fee).sum();
            } else if(booking.request.fleetServices != null && !booking.request.fleetServices.isEmpty()){
                List<FleetService> fleetServices = getFleetServicesFromBooking(requestId, booking.request.fleetServices, ticket.subTotal, true);
                serviceFee = fleetServices.stream().filter(s ->s.active).mapToDouble(s -> s.fee).sum();
            }
        }
        if(fleet.additionalFees != 1){
            objTicket.put("serviceFee", Double.valueOf(df.format(serviceFee)));
        } else {
            objTicket.put("fleetServiceFee", Double.valueOf(df.format(serviceFee)));
        }
        objTicket.put("shortTripChanged", shortTripChanged);
        objTicket.put("shortTrip", shortTrip);
        objTicket.put("dispatchRideSharing", booking.dispatchRideSharing);
        mapData.put("ticket", objTicket);
        // create temp object for old app - WILL BE REMOVED after a few versions
        Map<String, String> mapPayment = new HashMap<>();
        mapPayment.put("type", ticket.transactionStatus);
        mapPayment.put("cardTye", ticket.cardType);
        mapPayment.put("approvalCode", ticket.approvalCode);
        mapData.put("payment", mapPayment);
        paymentResponse.response = mapData;

        // NEW APP
        // paymentResponse.response = createReturnTicket(ticket, false);

        String ticketData = paymentResponse.toString();
        logger.debug(requestId + " - ticketData: " + ticketData);

        // call to CC update booking
        CompleteWithCCThread completeWithCCThread = new CompleteWithCCThread();
        completeWithCCThread.booking = booking;
        completeWithCCThread.fleetId = ticket.fleetId;
        completeWithCCThread.requestId = requestId;
        completeWithCCThread.mongoDao = mongoDao;
        completeWithCCThread.sqlDao = sqlDao;
        completeWithCCThread.redisDao = redisDao;
        completeWithCCThread.passenger = passenger;
        completeWithCCThread.gateway = gateway;
        completeWithCCThread.start();

        // save to Redis
        /*redisDao.addCompletedBook(requestId, booking.bookId, sdfSaveRedis.format(new Date()));
        Calendar complete = Calendar.getInstance();
        complete.setTime(ticket.completedTime);
        redisDao.addAudit(requestId, sdfSaveRedis.format(complete.getTime()), booking.bookId + "-" + ticket.totalCharged);
        redisDao.addAudit(requestId, sdfSaveRedisWithHour.format(complete.getTime()), booking.bookId+"-"+ticket.totalCharged);*/

        // check if already created payment link for this booking
        String paymentLinkId = redisDao.getTmp(requestId, booking.bookId+"paymentLinkId");
        if (!paymentLinkId.isEmpty() && !ticket.transactionStatus.equals(KeysUtil.PAYMENT_PAYMENT_LINK)) {
            GatewayStripe gatewayStripe = mongoDao.getGatewayStripeFleet(booking.fleetId);
            StripeUtil stripeUtil = new StripeUtil(booking.fleetId);
            boolean deactivate = stripeUtil.deactivatePaymentLink(requestId, gatewayStripe.secretKey, paymentLinkId);
            if (deactivate) {
                // remove cache
                redisDao.removeTmp(requestId, booking.bookId);
                redisDao.removeTmp(requestId, booking.bookId+"paymentEnt");
                redisDao.removeTmp(requestId, booking.bookId+"fee");
                redisDao.removeTmp(requestId, booking.bookId+"paymentLink");
                redisDao.removeTmp(requestId, booking.bookId+"paymentLinkId");
            }
        }

        // remove cache payment
        redisDao.removeTmp(requestId, booking.bookId);
        // cache intercity data
        if (booking.intercity) {
            redisDao.cacheIntercityData(requestId, booking.tripId, booking.bookId + "-" + ticket.totalCharged);
        }

        // remove temporarily store payment data if payment is completed successfully
        if (booking.dispatch3rd) {
            redisDao.removeTmp(requestId, booking.bookId + "-first");
        }
        if (ticket.transactionStatus.equals(KeysUtil.PAYMENT_APPLEPAY)) {
            for (PaymentClearanceHouse paymentClearanceHouse : fleet.paymentClearanceHouses) {
                if (paymentClearanceHouse.gateway.equals(KeysUtil.PAYEASE)) {
                    // check stored data
                    // if data still exist, means the payment request is called from Dispatching, no need to call the update thread
                    // if data is empty, means the payment request is called from PayEase Async request, need to update Dispatching
                    String tmpData = redisDao.getTmp(requestId, booking.bookId + "-ApplePay");
                    if (tmpData != null && !tmpData.isEmpty()) {
                        redisDao.removeTmp(requestId, booking.bookId + "-ApplePay");
                    } else {
                        // push result to server Dispatching
                        UpdateToDispatchingThread updateToDispatchingThread = new UpdateToDispatchingThread();
                        updateToDispatchingThread.data = ticketData;
                        updateToDispatchingThread.requestId = requestId;
                        updateToDispatchingThread.start();
                    }
                    break;
                }
            }
        }

        endOfPayment(booking.bookId, 200, requestId);
        return ticketData;
    }

    String completedPaymentInError(String bookId, int returnCode, String message, String requestId) {
        return doCompletedInError(bookId, returnCode, message, "", "", requestId);
    }

    String completedPaymentInError(String bookId, int returnCode, String message, String gateway, String driverId, String requestId) {
        return doCompletedInError(bookId, returnCode, message, gateway, driverId, requestId);
    }

    private String doCompletedInError(String bookId, int returnCode, String message, String gateway, String driverId, String requestId){
        logger.debug(requestId + " - PAYMENT FAILED FOR bookId "+ bookId +" !!!!!!!!!!");

        // lock card to avoid re-try payment
        if (returnCode != 448) {    // 448: card block match limit, don't add more failed card
            String lockData = redisDao.removeLockCard(bookId, requestId);
            if (lockData != null && !lockData.isEmpty()) {
                logger.debug(requestId + " - pay card failed - lock card data: " + lockData);
                String[] data = lockData.split("---");
                if (data.length > 2)
                    addFailedCard(data[0], data[1], data[2], requestId);
            }
        }

        // set default error code
        if (returnCode == 0)
            returnCode = 437;

        ObjResponse paymentResponse = new ObjResponse();
        paymentResponse.returnCode = returnCode;
        Map<String,Object> mapResult = new HashMap<>();
        mapResult.put("userId", driverId);
        if (!message.equals("") && !message.contains("data") && !message.contains("Exception"))
            mapResult.put("message", message);
        Map<String,Object> mapTicket = new HashMap<>();
        mapTicket.put("bookId", bookId);
        mapResult.put("ticket", mapTicket);
        paymentResponse.response = mapResult;

        endOfPayment(bookId, returnCode, requestId);
        return paymentResponse.toString();
    }

    public String completedPaymentInError(String bookId, int returnCode, JSONObject jsonObject, String requestId) {
        return doCompletedInError(bookId, returnCode, jsonObject, "", "", requestId);
    }

    public String completedPaymentInError(String bookId, int returnCode, JSONObject jsonObject, String gateway, String driverId, String requestId) {
        return doCompletedInError(bookId, returnCode, jsonObject, gateway, driverId, requestId);
    }

    private String doCompletedInError(String bookId, int returnCode, JSONObject jsonObject, String gateway, String driverId, String requestId){
        logger.debug(requestId + " - PAYMENT FAILED FOR bookId "+ bookId +" !!!!!!!!!!");

        // lock card to avoid re-try payment
        if (returnCode != 448) {    // 448: card block match limit, don't add more failed card
            String lockData = redisDao.removeLockCard(bookId, requestId);
            if (lockData != null && !lockData.isEmpty()) {
                logger.debug(requestId + " - pay card failed - lock card data: " + lockData);
                String[] data = lockData.split("---");
                if (data.length > 2)
                    addFailedCard(data[0], data[1], data[2], requestId);
            }
        }

        // set default error code
        if (returnCode == 0)
            returnCode = 437;

        // remove key "message" if contain invalid data
        String message = jsonObject.get("message") != null ? jsonObject.get("message").toString() : "";
        if (message.isEmpty() || message.contains("data") || message.contains("Exception"))
            jsonObject.remove("message");

        jsonObject.put("userId", driverId);
        JSONObject objTicket = new JSONObject();
        objTicket.put("bookId", bookId);
        jsonObject.put("ticket", objTicket);
        ObjResponse paymentResponse = new ObjResponse();
        paymentResponse.returnCode = returnCode;
        paymentResponse.response = jsonObject;

        endOfPayment(bookId, returnCode, requestId);
        return gson.toJson(paymentResponse);
    }

    private void endOfPayment(String bookId, int returnCode, String requestId) {
        // mark a booking has completed
        if (returnCode == 200)
            redisDao.markAsCompleted(requestId, bookId);

        // release booking at the end of payment process
        redisDao.unlockBooking(bookId, requestId);

        // remove ticket after payment
        redisDao.removeTicket(bookId, requestId);

        // remove locked card
        redisDao.removeLockCard(bookId, requestId);

        /*if (KeysUtil.GATEWAY_3DS.contains(gateway)) {
            // push result to server Dispatching
            UpdateToDispatchingThread updateToDispatchingThread = new UpdateToDispatchingThread();
            updateToDispatchingThread.data = ticketData;
            updateToDispatchingThread.type = "payInput";
            updateToDispatchingThread.start();
        }*/


    }

    public Map<String,String> getPreAuthSetting(PreAuthorized preAuthorized, double total, String currencyISO, String destination, int bookType, int typeRate) {
        Map<String,String> map = new HashMap<>();
        if (preAuthorized != null && preAuthorized.isActive) {
            double preAuthAmount = 0;
            if (preAuthorized.amountByCurrencies != null && !preAuthorized.amountByCurrencies.isEmpty()) {
                for (AmountByCurrency amountByCurrency : preAuthorized.amountByCurrencies) {
                    if (amountByCurrency.currencyISO.equalsIgnoreCase(currencyISO)) {
                        preAuthAmount = amountByCurrency.preAuthAmount;
                        break;
                    }
                }
            }

            if (preAuthorized.holdPreAuth == 0) {
                // hold a fix amount
                map.put("enablePreAuth", "true");
                map.put("holdPreAuth", "0");
                map.put("authAmount", String.valueOf(preAuthAmount));

            } else if (preAuthorized.holdPreAuth == 1) {
                double buffer = preAuthorized.buffer;
                DecimalFormat df = new DecimalFormat("0.00");
                if (destination.isEmpty()) {
                    // check if book hourly or from/to airport
                    if (bookType == 3 || (bookType == 1 && typeRate == 1)) {
                        if (total == 0) total = preAuthAmount; // if estimate amount = 0, pre-auth fix amount
                        double authAmount = (total * buffer)/100;
                        map.put("enablePreAuth", "true");
                        map.put("holdPreAuth", "1");
                        map.put("authAmount", df.format(authAmount));
                        return map;
                    } else {
                        // hold a fix amount
                        map.put("enablePreAuth", "true");
                        map.put("holdPreAuth", "1");
                        map.put("authAmount", String.valueOf(preAuthAmount));
                        return map;
                    }
                }
                // hold estimate fare if available
                if (total == 0) total = preAuthAmount; // if estimate amount = 0, pre-auth fix amount
                double authAmount = (total * buffer)/100;
                map.put("enablePreAuth", "true");
                map.put("holdPreAuth", "1");
                map.put("authAmount", df.format(authAmount));
            } else {
                map.put("enablePreAuth", "false");
                map.put("holdPreAuth", "0");
                map.put("authAmount", "0.0");
            }

        } else {
            map.put("enablePreAuth", "false");
            map.put("holdPreAuth", "0");
            map.put("authAmount", "0.0");
        }
        return map;
    }

    public Object getGatewayAccount(String fleetId, String gateway, boolean affiliateEnvironment, boolean isLocalZone) {
        // get setting environment
        String environment = affiliateEnvironment ? KeysUtil.PRODUCTION : KeysUtil.SANDBOX;
        /*switch (gateway) {
            case KeysUtil.STRIPE: {
                GatewayStripe gatewayStripe = mongoDao.getGatewayStripeFleet(fleetId);
                environment = gatewayStripe.environment;
            }
            break;
            case KeysUtil.BRAINTREE: {
                GatewayBraintree gatewayBraintree = mongoDao.getGatewayBraintreeFleet(fleetId);
                environment = gatewayBraintree.environment;
            }
            break;
            case KeysUtil.AUTHORIZE: {
                GatewayAuthorize gatewayAuthorize = mongoDao.getGatewayAuthorizeFleet(fleetId);
                environment = gatewayAuthorize.environment;
            }
            break;
            case KeysUtil.PEACH: {
                GatewayPeachPayments gatewayPeachPayments = mongoDao.getGatewayPeachPaymentsFleet(fleetId);
                environment = gatewayPeachPayments.environment;
            }
            break;
            case KeysUtil.JETPAY: {
                GatewayJetpay gatewayJetpay = mongoDao.getGatewayJetpayFleet(fleetId);
                environment = gatewayJetpay.environment;
            }
            break;
        }*/

        if (environment.equals(KeysUtil.SANDBOX)) {
            return mongoDao.getGatewayStripeSystem(true);
        } else {
            return mongoDao.getGatewayStripeSystem(false);
        }
    }

    public String getTransactionStatus(String paymentType, String cardOwner, String bookFrom) {
        switch (paymentType) {
            case KeysUtil.PAYMENT_CORPORATE_3RD:
                paymentType = KeysUtil.PAYMENT_CORPORATE;
                break;
            case KeysUtil.PAYMENT_DIRECTBILLING_3RD:
                paymentType = KeysUtil.PAYMENT_DIRECTBILLING;
                break;
            case KeysUtil.PAYMENT_CASH_3RD:
                paymentType = KeysUtil.PAYMENT_CASH;
                break;
            case KeysUtil.PAYMENT_FLEETCARD_3RD:
                paymentType = KeysUtil.PAYMENT_FLEETCARD;
                break;
        }

        String transactionStatus = paymentType;
        if ("mDispatcher".equalsIgnoreCase(cardOwner) &&
                "mDispatcher".equalsIgnoreCase(bookFrom)) {
            transactionStatus = "mDispatcherCard";

        } else if ("corporate".equalsIgnoreCase(cardOwner)) {
            transactionStatus = KeysUtil.PAYMENT_CORPORATE;
        }

        return transactionStatus;
    }

    public JSONObject createReturnTicket(TicketReturn ticket, Fleet fleet, Booking booking, boolean getSetting, String requestId){

        // get setting data from mongoDB
        boolean serviceActive = false;
        List<Service> services = new ArrayList<>();
        String avatarDriver = "";
        boolean tollFeeLimitDriverInputActive = false;
        boolean parkingFeeLimitDriverInputActive = false;
        boolean gasFeeLimitDriverInputActive = false;
        boolean otherFeeLimitDriverInputActive = false;
        boolean hideFare = false;
        double tollFeeDriverCanInput = 0.0;
        double parkingFeeDriverCanInput = 0.0;
        double gasFeeDriverCanInput = 0.0;
        double otherFeeDriverCanInput = 0.0;
        boolean keepMinFee = false;
        double maximumValue = 0.0;
        String thirdParty = "";
        if (booking.externalInfo != null && booking.externalInfo.thirdParty != null) {
            thirdParty = booking.externalInfo.thirdParty != null ? booking.externalInfo.thirdParty : "booking.com";
        }
        String bookFrom = booking.bookFrom != null ? booking.bookFrom : "";
        if (getSetting) {
            // update booking data before continue

            // try to read cache and update to booking entity
            String cached = "";
            String jobType = booking.jobType != null ? booking.jobType : "";
            jobType = CommonUtils.getJobType(jobType, booking.bookFrom, booking.intercity, booking.delivery);
            logger.debug(requestId + " - jobType: " + jobType);
            if (!KeysUtil.INCOMPLETE_PAYMENT.contains(ticket.transactionStatus) && !KeysUtil.NO_DROP_PAYMENT.contains(jobType) && !ticket.payFromCC) {
                cached = redisDao.getTmp(requestId,"normalFare"+"-"+booking.bookId);
            }
            if (!cached.isEmpty()) {
                if (booking.request.estimate.fare != null) {
                    booking.request.estimate.fare.normalFare = Boolean.parseBoolean(cached);
                } else {
                    Fare fare = new Fare();
                    fare.normalFare = Boolean.parseBoolean(cached);
                    booking.request.estimate.fare = fare;
                }
            } else {
                // check if empty then set default value
                if (booking.request.estimate.fare != null) {
                    Boolean normalFareInBooking = booking.request.estimate.fare.normalFare;
                    if (normalFareInBooking == null) {
                        booking.request.estimate.fare.normalFare = true;
                    }
                } else {
                    Fare fare = new Fare();
                    fare.normalFare = false;
                    booking.request.estimate.fare = fare;
                }
            }
            Map<String, Object> mapSettings = getSettings(ticket.fleetId, booking, ticket.promoCode, requestId, ticket.payFromCC);
            ticket.editBasicFare = Boolean.parseBoolean(mapSettings.get("editBasicFare").toString());
            ticket.editOtherFees = Boolean.parseBoolean(mapSettings.get("editOtherFees").toString());
            ticket.editTax = Boolean.parseBoolean(mapSettings.get("editTax").toString());
            ticket.editTip = Boolean.parseBoolean(mapSettings.get("editTip").toString());
            ticket.techFeeActive = Boolean.parseBoolean(mapSettings.get("techFeeActive").toString());
            ticket.techFeeType = mapSettings.get("techFeeType").toString();
            ticket.techFeeValue = Double.parseDouble(mapSettings.get("techFeeValue").toString());
            ticket.tipActive = Boolean.parseBoolean(mapSettings.get("tipActive").toString());
            ticket.taxActive = Boolean.parseBoolean(mapSettings.get("taxActive").toString());
            ticket.taxValue = Double.parseDouble(mapSettings.get("taxValue").toString());
            ticket.couponType = mapSettings.get("couponType").toString();
            ticket.couponValue = Double.parseDouble(mapSettings.get("couponValue").toString());
            ticket.meetDriverActive = Boolean.parseBoolean(mapSettings.get("meetDriverActive").toString());
            ticket.heavyTrafficActive = Boolean.parseBoolean(mapSettings.get("heavyTrafficActive").toString());
            ticket.airportActive = Boolean.parseBoolean(mapSettings.get("airportActive").toString());
            ticket.commissionType = mapSettings.get("commissionType").toString();
            ticket.commissionValue = Double.parseDouble(mapSettings.get("commissionValue").toString());
            ticket.otherFeeActive = Boolean.parseBoolean(mapSettings.get("otherFeeActive").toString());
            ticket.rushHourActive = Boolean.parseBoolean(mapSettings.get("rushHourActive").toString());
            ticket.actualFare = Boolean.parseBoolean(mapSettings.get("actualFare").toString());
            boolean basicFareOnly = Boolean.parseBoolean(mapSettings.get("basicFareOnly").toString());
            keepMinFee = Boolean.parseBoolean(mapSettings.get("keepMinFee").toString());
            maximumValue = Double.parseDouble(mapSettings.get("maximumValue").toString());
            if(!ticket.actualFare && basicFareOnly){
                ticket.couponType = "amount";
                ticket.couponValue = 0.0;
                maximumValue = 0.0;
            }

            ticket.tollFeeActive = Boolean.parseBoolean(mapSettings.get("tollFeeActive").toString());
            ticket.parkingFeeActive = Boolean.parseBoolean(mapSettings.get("parkingFeeActive").toString());
            ticket.gasFeeActive = Boolean.parseBoolean(mapSettings.get("gasFeeActive").toString());
            ticket.addNote = Boolean.parseBoolean(mapSettings.get("addNote").toString());
            ticket.bookingFee = Double.parseDouble(mapSettings.get("bookingFee").toString());
            ticket.addOnPrice = Double.parseDouble(mapSettings.get("addOnPrice").toString());
            ticket.taxFee = Double.parseDouble(mapSettings.get("taxFee").toString());
            ticket.tipFee = Double.parseDouble(mapSettings.get("tipFee").toString());
            ticket.surchargeType = mapSettings.get("surchargeType")!= null ? mapSettings.get("surchargeType").toString():"amount";
            ticket.surchargeFee = mapSettings.get("surchargeFee")!= null ? Double.parseDouble(mapSettings.get("surchargeFee").toString()): 0;
            ticket.creditTransactionFeeAmount = mapSettings.get("creditTransactionFeeAmount")!= null ? Double.parseDouble(mapSettings.get("creditTransactionFeeAmount").toString()): 0;
            ticket.creditTransactionFeePercent = mapSettings.get("creditTransactionFeePercent")!= null ? Double.parseDouble(mapSettings.get("creditTransactionFeePercent").toString()): 0;
            ticket.minimum = Double.parseDouble(mapSettings.get("minimum").toString());
            /*if(ticket.dynamicFare > 0 && ticket.actualFare){
                double minimum = Double.parseDouble(mapSettings.get("minimum").toString());
                minimum += minimum * (ticket.dynamicFare - 1);
                ticket.minimum = minimum;
            }*/
            ticket.travellerSignature = Boolean.parseBoolean(mapSettings.get("travellerSignature").toString());
            ticket.trackingLog = Boolean.parseBoolean(mapSettings.get("trackingLog").toString());
            ticket.rating = Boolean.parseBoolean(mapSettings.get("rating").toString());
            ticket.vehicleType = mapSettings.get("vehicleType").toString();
            serviceActive = Boolean.parseBoolean(mapSettings.get("serviceActive").toString());
            services = (List<Service>) mapSettings.get("services");
            avatarDriver = mapSettings.get("avatarDriver").toString();
            otherFeeLimitDriverInputActive = Boolean.parseBoolean(mapSettings.get("otherFeeLimitDriverInputActive")!= null  ? mapSettings.get("otherFeeLimitDriverInputActive").toString():"false");
            hideFare = Boolean.parseBoolean(mapSettings.get("hideFare")!= null  ? mapSettings.get("hideFare").toString():"false");
            otherFeeDriverCanInput = Double.parseDouble(mapSettings.get("otherFeeDriverCanInput") != null ? mapSettings.get("otherFeeDriverCanInput").toString(): "0");
            tollFeeLimitDriverInputActive = Boolean.parseBoolean(mapSettings.get("tollFeeLimitDriverInputActive")!= null  ? mapSettings.get("tollFeeLimitDriverInputActive").toString():"false");
            parkingFeeLimitDriverInputActive = Boolean.parseBoolean(mapSettings.get("parkingFeeLimitDriverInputActive")!= null  ? mapSettings.get("parkingFeeLimitDriverInputActive").toString():"false");
            gasFeeLimitDriverInputActive = Boolean.parseBoolean(mapSettings.get("gasFeeLimitDriverInputActive")!= null  ? mapSettings.get("gasFeeLimitDriverInputActive").toString():"false");
            tollFeeDriverCanInput = Double.parseDouble(mapSettings.get("tollFeeDriverCanInput") != null ? mapSettings.get("tollFeeDriverCanInput").toString(): "0");
            parkingFeeDriverCanInput = Double.parseDouble(mapSettings.get("parkingFeeDriverCanInput") != null ? mapSettings.get("parkingFeeDriverCanInput").toString(): "0");
            gasFeeDriverCanInput = Double.parseDouble(mapSettings.get("gasFeeDriverCanInput") != null ? mapSettings.get("gasFeeDriverCanInput").toString(): "0");
            if (ticket.pricingType == 1) {
                tollFeeLimitDriverInputActive = true;
                parkingFeeLimitDriverInputActive = true;
                gasFeeLimitDriverInputActive = true;
                tollFeeDriverCanInput = CommonUtils.getRoundValue(ticket.authAmount - ticket.total);
                parkingFeeDriverCanInput = CommonUtils.getRoundValue(ticket.authAmount - ticket.total);
                gasFeeDriverCanInput = CommonUtils.getRoundValue(ticket.authAmount - ticket.total);
            }
            // default off all setting for booking from third party
            if (KeysUtil.THIRD_PARTY.contains(bookFrom) && KeysUtil.THIRD_PARTY.contains(thirdParty)) {
                ticket.editBasicFare = false;
                ticket.editOtherFees = false;
                ticket.editTax = false;
                ticket.editTip = false;
                ticket.techFeeActive = false;
                ticket.tipActive = false;
                ticket.taxActive = false;
                ticket.meetDriverActive = false;
                ticket.heavyTrafficActive = false;
                ticket.airportActive = false;
                ticket.otherFeeActive = false;
                ticket.rushHourActive = false;
                ticket.tollFeeActive = false;
                ticket.parkingFeeActive = false;
                ticket.gasFeeActive = false;
            }
        }

        if (ticket.currencyISOCharged != null && ticket.currencyISOCharged.equals(""))
            ticket.currencyISOCharged = ticket.currencyISO;

        // return passenger avatar
        String passengerAvatar = "";
        String customerId = ticket.customerId != null ? ticket.customerId : "";
        if (!customerId.isEmpty()) {
            Account passenger = mongoDao.getAccount(ticket.customerId);
            if (passenger != null) {
                passengerAvatar = passenger.avatar != null ? passenger.avatar.trim() : "";
            }
        }
        ticket.passengerAvatar = passengerAvatar;

        // convert payment type based on transaction status
        String transactionStatus = ticket.transactionStatus != null ? ticket.transactionStatus : "";
        if (!transactionStatus.isEmpty()) {
            ticket.paymentType = TicketUtil.getPaymentType(transactionStatus);
            logger.debug(requestId + " - transactionStatus:" +  transactionStatus + " - paymentType: " + ticket.paymentType);
        }
        // return latest payment type from booking
        ticket.paymentType = booking.request != null ? booking.request.paymentType : 0;

        // update payment status and paid status
        String paymentStatus = "full";
        String paidStatus = "paid";
        double paidAmount = ticket.totalCharged;
        if (ticket.isPending) {
            paymentStatus = "pending";
            ticket.totalCharged = 0.0;
            paidStatus = "pending";
            paidAmount = 0.0;
        } else if (ticket.total > ticket.totalCharged) {
            paymentStatus = "partial";
            if (!KeysUtil.THIRD_PARTY.contains(booking.bookFrom)) {
                paidStatus = "partial";
                paidAmount = ticket.totalCharged;
            }
        } else if (KeysUtil.INCOMPLETE_PAYMENT.contains(ticket.transactionStatus) && ticket.total == 0 && ticket.totalCharged > 0) {
            paidAmount = ticket.totalCharged;
        }
        ticket.paidStatus = paidStatus;
        ticket.paidAmount = paidAmount;

        // update not pending for direct invoicing payment
        if (ticket.transactionStatus.equals(KeysUtil.PAYMENT_DIRECTBILLING)) {
            ticket.isPending = false;
            paymentStatus = "full";
        }
        // update refund status for intercity booking
        if (ticket.intercity && KeysUtil.INCOMPLETE_PAYMENT.contains(transactionStatus)) {
            if (ticket.totalCharged == 0.0) {
                String paymentType = convertRequestPaymentType(booking.request.paymentType);
                paymentStatus = KeysUtil.PAYMENT_CASH.equals(paymentType) ? "full" : "fullRefund";
            } else {
                paymentStatus = "partialRefund";
            }
        }
        ticket.paymentStatus = paymentStatus;

        // update wallet info
        String gateway = "";
        String walletName = "";
        String iconUrl = "";
        if (booking.request != null) {
            gateway = booking.request.gateway != null ? booking.request.gateway : "";
            walletName = booking.request.walletName != null ? booking.request.walletName.trim() : "";
            iconUrl = booking.request.iconUrl != null ? booking.request.iconUrl : "";
        }
        ticket.gateway = gateway;
        ticket.walletName = walletName;
        ticket.iconUrl = iconUrl;

        // if paid by wallet, will charged full amount
        if (transactionStatus.equals(KeysUtil.PAYMENT_TNG_WALLET)) {
            ticket.totalCharged = ticket.total;
            ticket.paymentStatus = "full";
        }

/*        // get credit transaction fee data
        ticket = updateCreditTransactionFee(ticket, );*/

        // return fleet services
        List<FleetService> fleetServices = booking.request.fleetServices != null ? booking.request.fleetServices : new ArrayList<>();
        ticket.fleetServices = getFleetServicesFromBooking(requestId, fleetServices, ticket.subTotal, false);
        if(booking.intercity)
            ticket.fleetServices = getFleetServicesFromBooking(requestId, fleetServices, ticket.subTotal, true);

        // replace old key (for additional services) by new key (for fleet service)
        //logger.debug(requestId + " - additionalFees: " + fleet.additionalFees);
        logger.debug(requestId + " - fleetServiceFee: " + ticket.fleetServiceFee);
        logger.debug(requestId + " - serviceFee: " + ticket.serviceFee);
        /*if (fleet.additionalFees == 1 && ticket.fleetServiceFee == 0 && ticket.serviceFee != 0) {
            ticket.fleetServiceFee = ticket.serviceFee;
            ticket.serviceFee = 0.0;
        }*/

        // check and return partial payment data if not complete
        if (booking.request.primaryPartialMethod != -1 && ticket.status == 0) {
            logger.debug(requestId + " - primaryPartialMethod: " + booking.request.primaryPartialMethod);
            if (ticket.paidByWallet == 0 && ticket.paidByOtherMethod == 0) {
                PaxWalletUtil paxWalletUtil = new PaxWalletUtil(requestId);
                double currentBalance = paxWalletUtil.getCurrentBalance(booking.fleetId, booking.psgInfo.userId, "paxWallet", booking.currencyISO);
                logger.debug(requestId + " - currentBalance: " + currentBalance);
                logger.debug(requestId + " - total: " + ticket.total);
                if (currentBalance >= ticket.total) {
                    ticket.paidByWallet = estimateUtil.roundingValue(ticket.total, fleet.rounding, booking.currencyISO);
                    ticket.paidByOtherMethod = 0.0;
                } else {
                    ticket.paidByWallet = estimateUtil.roundingValue(currentBalance, fleet.rounding, booking.currencyISO);
                    ticket.paidByOtherMethod = estimateUtil.roundingValue(ticket.total - currentBalance, fleet.rounding, booking.currencyISO);
                }
            }
            logger.debug(requestId + " - paidByWallet: " + ticket.paidByWallet);
            logger.debug(requestId + " - paidByOtherMethod: " + ticket.paidByOtherMethod);
        }

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Gson gson = new Gson();
        String result = gson.toJson(ticket);
        JSONObject json = null;
        //logger.debug(requestId + " - " +  df.format(ticket.createdTime));
        try {
            json = (JSONObject)new JSONParser().parse(result);
            logger.debug(requestId + " - parse JSON data: " + json.toJSONString());
            if (ticket.pickupTime != null)
                json.put("pickupTime",df.format(ticket.pickupTime));
            if (ticket.completedTime != null)
                json.put("completedTime",df.format(ticket.completedTime));
            if (ticket.paymentTime != null) {
                json.put("paymentTime", df.format(ticket.paymentTime));
            } else if (ticket.completedTime != null) {
                json.put("paymentTime", df.format(ticket.completedTime));
            }



            json.put("serviceActive", serviceActive);
            json.put("additionalServices", services);
            json.put("avatar", avatarDriver);
            json.put("tollFeeLimitDriverInputActive", tollFeeLimitDriverInputActive);
            json.put("parkingFeeLimitDriverInputActive", parkingFeeLimitDriverInputActive);
            json.put("gasFeeLimitDriverInputActive", gasFeeLimitDriverInputActive);
            json.put("otherFeeLimitDriverInputActive", otherFeeLimitDriverInputActive);
            json.put("tollFeeDriverCanInput", tollFeeDriverCanInput);
            json.put("parkingFeeDriverCanInput", parkingFeeDriverCanInput);
            json.put("gasFeeDriverCanInput", gasFeeDriverCanInput);
            json.put("otherFeeDriverCanInput", otherFeeDriverCanInput);
            json.put("hideFare", hideFare);

            String driverFirstName = "";
            String driverLastName = "";
            double driverCreditBalance = 0.0;
            if (booking.drvInfo != null) {
                driverFirstName = booking.drvInfo.firstName != null ? booking.drvInfo.firstName : "---";
                driverLastName = booking.drvInfo.lastName != null ? booking.drvInfo.lastName : "---";
                String userId = booking.drvInfo.userId != null ? booking.drvInfo.userId : "";
                if (!userId.isEmpty()) {
                    WalletUtil walletUtil = new WalletUtil(requestId);
                    driverCreditBalance = walletUtil.getCurrentBalance(userId, "credit", booking.currencyISO);
                }
            }
            json.put("driverFirstName", driverFirstName);
            json.put("driverLastName", driverLastName);
            json.put("driverCreditBalance", driverCreditBalance);

            String firstName = "";
            String lastName = "";
            double paxWalletBalance = 0.0;
            if (booking.psgInfo != null) {
                firstName = booking.psgInfo.firstName != null ? booking.psgInfo.firstName : "";
                lastName = booking.psgInfo.lastName != null ? booking.psgInfo.lastName : "";

                PaxWalletUtil paxWalletUtil = new PaxWalletUtil(requestId);
                String userId = booking.psgInfo.userId != null ? booking.psgInfo.userId : "";
                paxWalletBalance = paxWalletUtil.getCurrentBalance(booking.fleetId, userId, "paxWallet", booking.currencyISO);
                if (paxWalletBalance < 0)
                    paxWalletBalance = 0.0;
            }
            // return remaining amount for booking paid by paxWallet
            double remainingAmount = 0.0;
            if (booking.request != null && booking.request.paymentType != null && booking.request.paymentType == 13) {
                logger.debug(requestId + " - total: " + ticket.total);
                logger.debug(requestId + " - paxWalletBalance: " + paxWalletBalance);
                remainingAmount = ticket.total > paxWalletBalance ? Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(ticket.total - paxWalletBalance)) : 0;
                logger.debug(requestId + " - remainingAmount: " + remainingAmount);
            }
            json.put("customerFirstName", firstName);
            json.put("customerLastName", lastName);
            json.put("paxWalletBalance", paxWalletBalance);
            json.put("remainingAmount", remainingAmount);

            if(booking.completedInfo != null){
                json.put("pointsEarned", booking.completedInfo.pointsEarned);
            }
            String jobType = "rideHailing";
            if(booking.superHelper)
                jobType = "superHelper";
            if(booking.intercity)
                jobType = "intercity";
            json.put("carTypeService", booking.jobType != null ? booking.jobType: jobType);
            json.put("promoCustomerType", booking.request.promoCustomerType != null ? booking.request.promoCustomerType : "");
            json.put("maximumValue", maximumValue);
            json.put("keepMinFee", keepMinFee);
            json.put("totalOrder", ticket.total + ticket.itemValue);

            if((booking.request.estimate != null && booking.request.estimate.isFareEdited) ||
                    (KeysUtil.THIRD_PARTY.contains(bookFrom)&& KeysUtil.THIRD_PARTY.contains(thirdParty))){
                Fare originFare = booking.request.estimate.originFare;
                Fare newFare = booking.request.estimate.fare;
                if(originFare.basicFare != newFare.basicFare)
                    json.put("basicFareEdit", newFare.basicFare);
                if(originFare.airportFee != newFare.airportFee)
                    json.put("airportFeeEdit", newFare.airportFee);
                json.put("meetDriverFeeEdit", newFare.meetDriverFee);
                json.put("rushHourFeeEdit", newFare.rushHourFee);
                if(originFare.techFee != newFare.techFee)
                    json.put("techFeeEdit", newFare.techFee);
                if(originFare.bookingFee != newFare.bookingFee || originFare.subTotal != newFare.subTotal)
                    json.put("bookingFeeEdit", newFare.bookingFee);
                if(originFare.tip != newFare.tip || originFare.subTotal != newFare.subTotal)
                    json.put("tipEdit", newFare.tip);
                if(originFare.otherFees != newFare.otherFees)
                    json.put("otherFeesEdit", newFare.otherFees);
                if(originFare.promoAmount != newFare.promoAmount)
                    json.put("promoAmountEdit", newFare.promoAmount);
                if(originFare.tollFee != newFare.tollFee)
                    json.put("tollFeeEdit", newFare.tollFee);
                if(originFare.parkingFee != newFare.parkingFee)
                    json.put("parkingFeeEdit", newFare.parkingFee);
                if(originFare.gasFee != newFare.gasFee)
                    json.put("gasFeeEdit", newFare.gasFee);
                if(originFare.serviceFee != newFare.serviceFee)
                    json.put("serviceFeeEdit", newFare.serviceFee);
                if(originFare.creditTransactionFee != newFare.creditTransactionFee)
                    json.put("creditTransactionFeeEdit", newFare.creditTransactionFee);
            }
            if(booking.request.estimate != null && booking.request.estimate.fare != null){
                json.put("markupDifference", booking.request.estimate.fare.markupDifference);
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
        return json;
    }
    public Map<String,Object> getSettings(String fleetId, Booking booking, String promoCode, String requestId, boolean payFromCC) {
        Fleet fleet = mongoDao.getFleetInfor(fleetId);

        Map<String,Object> map = new HashMap<>();
        if(booking != null && booking.pricingType != 0){
            //map = getSettingsDataForAffiliate(fleet, booking);
            map = getSettingsData(fleet, booking, promoCode, requestId, payFromCC);
        }else{
            map = getSettingsData(fleet, booking, promoCode, requestId, payFromCC);
        }
        return map;
    }

    public Map<String,Object> getSettingsData(Fleet fleet, Booking booking, String promoCode, String requestId, boolean payFromCC){
        //query databases
        FleetFare fleetFare = mongoDao.getFleetFare(fleet.fleetId);

        boolean editBasicFare = true;   // set default true - allow edit fare on driver app for payable
        boolean editOtherFees = false;
        boolean editTax = false;
        boolean editTip = false;
        boolean techFeeActive = false;
        String techFeeType = "amount";
        double techFeeValue = 0.0;
        boolean tipActive = false;
        boolean taxActive = false;
        double taxValue = 0.0;
        String couponType = "";
        double couponValue = 0.0;
        double maximumValue = 0.0;
        boolean keepMinFee = false;
        boolean meetDriverActive = false;
        boolean heavyTrafficActive = false;
        boolean airportActive = false;
        String commissionType = "amount";
        double commissionValue = 0.0;
        boolean otherFeeActive = false;
        boolean rushHourActive = false;
        boolean bookingFeeActive = false;
        boolean tollFeeActive = false;
        boolean parkingFeeActive = false;
        boolean gasFeeActive = false;
        boolean addNote = false;
        boolean actualFare = true;
        boolean editedFare = false;
        double bookingFee = 0.0;
        double taxFee = 0.0;
        double tipFee = 0.0;
        double addOnPrice = 0.0;
        double minimum = 0.0;
        boolean travellerSignature = false;
        boolean trackingLog = false;
        boolean rating = false;
        double surchargeFee = 0;
        String surchargeType = "amount";
        boolean basicFareOnly = false;
        int tollFeePayTo = 0;
        int parkingFeePayTo = 0;
        int gasFeePayTo = 0;
        int meetDriverPayTo = 0;
        int airportPayTo = 0;
        boolean serviceActive = false;
        List<Service> services = new ArrayList<>();
        boolean tollFeeLimitDriverInputActive = false;
        boolean parkingFeeLimitDriverInputActive = false;
        boolean gasFeeLimitDriverInputActive = false;
        boolean otherFeeLimitDriverInputActive = false;
        double tollFeeDriverCanInput = 0.0;
        double parkingFeeDriverCanInput = 0.0;
        double gasFeeDriverCanInput = 0.0;
        double otherFeeDriverCanInput = 0.0;
        double creditTransactionFeePercent = 0.0;
        double creditTransactionFeeAmount = 0.0;

        /// GET SETTINGS
        String vehicleTypeRq = "";
        String drvId = "";
        String mDispatcherId = "";
        String corporateId = "";
        String bookFrom = "";
        String zoneId = "";
        String avatarDriver = "";
        int paymentMethod = 0;
        boolean reservation = false;
        boolean hideFare = false;
        boolean superHelper = false;
        if(booking != null){
            bookFrom = booking.bookFrom;
            reservation = booking.reservation;
            superHelper = booking.superHelper;
            if (booking.request != null)
                vehicleTypeRq = booking.request.vehicleTypeRequest != null ? booking.request.vehicleTypeRequest : "";
            if(booking.drvInfo != null )
                drvId = booking.drvInfo.userId;
            if(booking.mDispatcherInfo != null && booking.mDispatcherInfo.userId != null)
                mDispatcherId = booking.mDispatcherInfo.userId;
            if(booking.corporateInfo != null && booking.corporateInfo.corporateId != null)
                corporateId = booking.corporateInfo.corporateId;
            if(booking.request != null && booking.request.pickup != null){
                Zone zone = mongoDao.findByFleetIdAndGeo(fleet.fleetId, booking.request.pickup.geo);
                if(zone != null){
                    zoneId = zone._id.toString();
                }
            }
            if(booking.request != null && booking.request.serviceActive)
                serviceActive = booking.request.serviceActive;
            if(booking.request != null && booking.request.services != null)
                services = booking.request.services;
            if(booking.request != null && booking.request.paymentType != null)
                paymentMethod = booking.request.paymentType;
            if(booking.drvInfo != null && booking.drvInfo.avatar != null)
                avatarDriver = booking.drvInfo.avatar;
            hideFare = booking.request.hideFare;
            editedFare = booking.request.estimate != null && booking.request.estimate.isFareEdited;
        }

        //check rate by corporate
        boolean rateByCorp = false;
        if(!corporateId.isEmpty()){
            Corporate corporate = mongoDao.getCorporate(corporateId);
            if(corporate!= null && corporate.pricing!= null && corporate.pricing.differentRate!= null && corporate.pricing.differentRate)
                rateByCorp = corporate.pricing.differentRate;
        }

        //check edit basic fare, edit tax by vehicle and driver
        boolean forceMeter = false;
        Account account = mongoDao.getAccount(drvId);
        if(account != null){
            if(fleet.hardwareMeter){
                if(account.driverInfo != null && account.driverInfo.forceMeter!= null && account.driverInfo.forceMeter){
                    forceMeter = true;
                }
            }
        }
        if (booking.pricingType == 1) {
            logger.debug(requestId + " - convert car type affiliate to local");

            if (!vehicleTypeRq.isEmpty()) {
                AffiliationCarType vehicleType = mongoDao.getAffiliationCarTypeByName(vehicleTypeRq);
                if(vehicleType!= null) {
                    // get list car types local has associated with affiliate car type
                    List<String> listCarTypes = mongoDao.getListCarTypeByAffiliate(booking.fleetId, vehicleType._id.toString());
                    if (listCarTypes.size() > 0) {
                        vehicleTypeRq = listCarTypes.get(0);
                    }
                }
            }
        }
        logger.debug(requestId + " - vehicleTypeRq: " + vehicleTypeRq);
        VehicleType vehicleType = mongoDao.getVehicleTypeByFleetAndName(fleet.fleetId, vehicleTypeRq);
        if(vehicleType != null && vehicleType.drvApp != null){
            editBasicFare = vehicleType.drvApp.enableEditFare;
            if(vehicleType.drvApp.enableEditFare){
                if(forceMeter){
                    editBasicFare = vehicleType.drvApp.drvForceMeter;
                }else {
                    editBasicFare = vehicleType.drvApp.drvNotForceMeter;
                }
            }
            editTax = vehicleType.editTax;

            //get minimum setting
            if (vehicleType != null && !zoneId.isEmpty()) {
                if(rateByCorp){
                    if(vehicleType.corpRates != null && ! vehicleType.corpRates.isEmpty()){
                        for(CorpRate corpRate: vehicleType.corpRates){
                            if(corpRate.corporateId.equals(corporateId)){
                                for(Rate rate: corpRate.rates){
                                    if(rate.rateType.equals(KeysUtil.REGULAR) && rate.zoneId.equals(zoneId)){
                                        FareNormal fareNormal = mongoDao.getFareNormalById(rate.rateId);
                                        if(fareNormal!= null && fareNormal.feesByCurrencies!= null){
                                            for(FeesByCurrency feesByCurrency : fareNormal.feesByCurrencies){
                                                if(feesByCurrency.currencyISO.equals(booking.currencyISO)){
                                                    if (!reservation)
                                                        minimum = feesByCurrency.minNow;
                                                    else
                                                        minimum = feesByCurrency.minReservation;
                                                }
                                            }
                                        }
                                    }
                                }
                                break;
                            }
                        }
                    }
                }else{
                    if(vehicleType.rates!= null && !vehicleType.rates.isEmpty()){
                        for(Rate rate: vehicleType.rates){
                            if(rate.rateType.equals(KeysUtil.REGULAR) && rate.zoneId.equals(zoneId)){
                                FareNormal fareNormal = mongoDao.getFareNormalById(rate.rateId);
                                if(fareNormal!= null && fareNormal.feesByCurrencies!= null){
                                    for(FeesByCurrency feesByCurrency : fareNormal.feesByCurrencies){
                                        if(feesByCurrency.currencyISO.equals(booking.currencyISO)){
                                            if (!reservation)
                                                minimum = feesByCurrency.minNow;
                                            else
                                                minimum = feesByCurrency.minReservation;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if(!vehicleType.actualFare){
                actualFare = false;
                if(booking != null && booking.request != null && booking.request.estimate != null &&
                        booking.request.estimate.fare!= null && booking.request.estimate.fare.typeRate == 0){
                    bookingFee = booking.request.estimate.fare.bookingFee;

                }
            }

            //check dynamic fare
            if(actualFare && booking != null && booking.request != null && booking.request.estimate != null &&
                    booking.request.estimate.fare!= null && booking.request.estimate.fare.dynamicFare != null &&
                    booking.request.estimate.fare.dynamicFare > 0){
                String dynamicType = booking.request.estimate.fare.dynamicType != null ? booking.request.estimate.fare.dynamicType: "factor";
                if(dynamicType.equals("factor")){
                    minimum += minimum * (booking.request.estimate.fare.dynamicFare - 1);
                } else {
                    minimum = minimum + booking.request.estimate.fare.dynamicFare;
                }
            }

            if(!actualFare || editedFare){
                taxFee = booking.request.estimate.fare.tax;
                tipFee = booking.request.estimate.fare.tip;
                addOnPrice = booking.request.estimate.fare.addOnPrice;
                if(booking.request.estimate.fare.minFare > 0)
                    minimum = booking.request.estimate.fare.minFare;
            }
        }

        Calendar cal = Calendar.getInstance();
        Date pickupTime = null;
        Date engagedTime = null;
        Date droppedOffTime = TimezoneUtil.convertServerToGMT(new Date());
        if (booking.time != null) {
            if (booking.time.pickUpTime != null) {
                try {
                    pickupTime = TimezoneUtil.offsetTimeZone(booking.time.pickUpTime, cal.getTimeZone().getID(), "GMT");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                logger.debug(requestId + " - pickupTime: " + pickupTime);
            }

            if (booking.time.engaged != null) {
                try {
                    engagedTime = TimezoneUtil.offsetTimeZone(booking.time.engaged, cal.getTimeZone().getID(), "GMT");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                logger.debug(requestId + " - engagedTime: " + engagedTime);
            }

            if (booking.time.droppedOff != null) {
                try {
                    droppedOffTime = TimezoneUtil.offsetTimeZone(booking.time.droppedOff, cal.getTimeZone().getID(), "GMT");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                logger.debug(requestId + " - droppedOffTime: " + droppedOffTime);
            }
        }

        //get booking fee
        List<Object> bookFee = estimateUtil.getBookingFee(mDispatcherId, corporateId, bookFrom, booking.currencyISO);
        commissionType = bookFee.get(0).toString();
        commissionValue = Double.parseDouble(bookFee.get(1).toString());
        bookingFeeActive = Boolean.parseBoolean(bookFee.get(2).toString());

        //get tech fee
        List<Object> techFee = estimateUtil.getTechFeeValueByZone(fleet, bookFrom, booking.currencyISO, zoneId);
        techFeeValue = Double.parseDouble(techFee.get(0).toString());
        techFeeActive = Boolean.parseBoolean(techFee.get(1).toString());

        //get setting of fleet
        editTip = fleet.editTip;
        tipActive = fleetFare.tipActive;
        tollFeePayTo = fleetFare.tollFeePayTo;
        parkingFeePayTo = fleetFare.parkingFeePayTo;
        gasFeePayTo = fleetFare.gasFeePayTo;
        if (fleetFare.meetDriver != null) meetDriverPayTo = fleetFare.meetDriver.payTo;
        if (fleetFare.airport != null) airportPayTo = fleetFare.airport.payTo;
        JSONObject promo = getPromoCode(fleet.fleetId, booking.bookId, promoCode, booking.currencyISO);
        couponType = promo.get("type").toString().toLowerCase();
        couponValue = Double.parseDouble(promo.get("value").toString());
        maximumValue = Double.parseDouble(promo.get("maximumValue").toString());
        keepMinFee = Boolean.parseBoolean(promo.get("keepMinFee").toString());
        meetDriverActive = fleet.additionalService.fromAirport.meetDriver;
        basicFareOnly = fleet.estimateFare == 0;
        if(fleetFare.airport != null){
            if(fleetFare.airport.fromAirportActive || fleetFare.airport.toAirportActive)
                airportActive = true;
        }

        //get credit transaction fee
        if (booking.pricingType == 0 && KeysUtil.transactionFeeMethod.contains(paymentMethod)) {
            JSONObject transactionFeeObj = estimateUtil.getCreditTransactionFeeSetting(fleet.creditCardTransactionFee, paymentMethod, booking.currencyISO, requestId);
            creditTransactionFeePercent = Double.valueOf(transactionFeeObj.get("creditTransactionFeePercent").toString());
            creditTransactionFeeAmount = Double.valueOf(transactionFeeObj.get("creditTransactionFeeAmount").toString());
            if (booking.request.estimate != null && booking.request.estimate.fare != null &&
                    booking.request.estimate.fare.markupDifference != 0) {
                creditTransactionFeePercent = 0;
                creditTransactionFeeAmount = 0;
            }
        }
        double distance;
        String cachedDistance = "";
        String jobType = booking.jobType != null ? booking.jobType : "";
        jobType = CommonUtils.getJobType(jobType, booking.bookFrom, booking.intercity, booking.delivery);
        logger.debug(requestId + " - jobType: " + jobType);
        if (!KeysUtil.INCOMPLETE_PAYMENT.contains(booking.status) && !KeysUtil.NO_DROP_PAYMENT.contains(jobType) && !payFromCC) {
            cachedDistance = redisDao.getTmp(requestId, "distance"+"-"+booking.bookId);
        }
        if (!cachedDistance.isEmpty()) {
            AddTicketEnt addTicketEnt = gson.fromJson(cachedDistance, AddTicketEnt.class);
            distance = estimateUtil.getDistanceBaseOnSetting(addTicketEnt.distanceTour, addTicketEnt.distanceGG, booking.drvInfo.userId, fleet.gpsLocation);
        } else {
            distance = booking.droppedOffInfo != null && booking.droppedOffInfo.distanceTour != null ?
                    booking.droppedOffInfo.distanceTour: 0;
        }
        logger.debug(requestId+ " - distance: " + distance);
        String unitDistance = fleet.unitDistance;
        if (unitDistance.equalsIgnoreCase(KeysUtil.UNIT_DISTANCE_IMPERIAL)) {
            distance = ConvertUtil.round((distance / 1609.344), 2);
        } else {
            distance = ConvertUtil.round((distance / 1000), 2);
        }
        logger.debug(requestId+ " ==> distance after convert: " + distance);
        //get general setting apply for each zone
        ServiceFee sF = mongoDao.getServiceFeeByFleetIdAndZoneId(fleet.fleetId, zoneId);
        String applyType = fleetFare.applyType != null ? fleetFare.applyType : "all";
        if(sF != null && fleet.generalSetting.allowCongfigSettingForEachZone && !applyType.equals("all")){
            taxActive = sF.taxActive;
            taxValue = sF.tax;
            otherFeeActive = sF.otherFeeActive;
            rushHourActive = sF.rushHourActive;
            tollFeeActive = sF.tollFeeActive;
            parkingFeeActive = sF.parkingFeeActive;
            gasFeeActive = sF.gasFeeActive;
            heavyTrafficActive = sF.heavyTrafficActive;
            tollFeeLimitDriverInputActive = sF.tollFeeLimitDriverInputActive != null
                    && sF.tollFeeLimitDriverInputActive;
            parkingFeeLimitDriverInputActive = sF.parkingFeeLimitDriverInputActive;
            gasFeeLimitDriverInputActive = sF.gasFeeLimitDriverInputActive;
            otherFeeLimitDriverInputActive = sF.otherFee.limitDriverInputActive != null
                    && sF.otherFee.limitDriverInputActive;

            if(tollFeeLimitDriverInputActive && sF.tollFeeDriverCanInput != null){
                for(AmountByCurrency amountByCurrency: sF.tollFeeDriverCanInput){
                    if(amountByCurrency.currencyISO.equals(booking.currencyISO))
                        tollFeeDriverCanInput = amountByCurrency.value;
                }
            }
            if(parkingFeeLimitDriverInputActive && sF.parkingFeeDriverCanInput != null){
                for(AmountByCurrency amountByCurrency: sF.parkingFeeDriverCanInput){
                    if(amountByCurrency.currencyISO.equals(booking.currencyISO))
                        parkingFeeDriverCanInput = amountByCurrency.value;
                }
            }
            if(gasFeeLimitDriverInputActive && sF.gasFeeDriverCanInput != null){
                for(AmountByCurrency amountByCurrency: sF.gasFeeDriverCanInput){
                    if(amountByCurrency.currencyISO.equals(booking.currencyISO))
                        gasFeeDriverCanInput = amountByCurrency.value;
                }
            }

            if(otherFeeLimitDriverInputActive && sF.otherFee.otherFeeDriverCanInput != null){
                for(AmountByCurrency amountByCurrency: sF.otherFee.otherFeeDriverCanInput){
                    if(amountByCurrency.currencyISO.equals(booking.currencyISO))
                        otherFeeDriverCanInput = amountByCurrency.value;
                }
            }

            //get surcharge
            if(sF.rushHourActive){
                String pickupTz = "";
                String dropOffTz = "";
                if (booking.request.pickup != null) {
                    pickupTz = booking.request.pickup.timezone != null ? booking.request.pickup.timezone : "";
                }
                if (booking.request.destination != null) {
                    dropOffTz = booking.request.destination.timezone != null ? booking.request.destination.timezone : "";
                }
                String surchargeTypeRate = estimateUtil.getSurchargeTypeRateFromBooking(booking);
                String fareType = "";
                jobType = CommonUtils.getJobType(jobType, booking.bookFrom, booking.intercity, booking.delivery);
                logger.debug(requestId + " - jobType: " + jobType);
                if (!KeysUtil.INCOMPLETE_PAYMENT.contains(booking.status) && !KeysUtil.NO_DROP_PAYMENT.contains(jobType) && !payFromCC) {
                    fareType = redisDao.getTmp(requestId, "fareType"+"-"+booking.bookId);
                }
                logger.debug(requestId + " - surchargeTypeRate: " + surchargeTypeRate);
                logger.debug(requestId + " - fareType: " + fareType);
                if (engagedTime != null) {
                    List<Object> getRushHour = null;
                    try {
                        getRushHour = estimateUtil.getRushHour(fleet, sF.rushHours, engagedTime, droppedOffTime, booking.currencyISO,
                                pickupTz, dropOffTz, distance, surchargeTypeRate, fareType, booking.reservation, requestId);
                        surchargeType = getRushHour.get(0).toString();
                        surchargeFee = Double.parseDouble(getRushHour.get(1).toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }else {
                    if(pickupTime!= null){
                        List<Object> getRushHour = null;
                        try {
                            getRushHour = estimateUtil.getRushHour(fleet, sF.rushHours, pickupTime, droppedOffTime, booking.currencyISO,
                                    pickupTz, dropOffTz, distance, surchargeTypeRate, fareType, booking.reservation, requestId);
                            surchargeType = getRushHour.get(0).toString();
                            surchargeFee = Double.parseDouble(getRushHour.get(1).toString());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else {
            taxActive = fleetFare.taxActive;
            taxValue = fleetFare.tax;
            otherFeeActive = fleetFare.otherFeeActive;
            rushHourActive = fleetFare.rushHourActive;
            tollFeeActive = fleetFare.tollFeeActive;
            parkingFeeActive = fleetFare.parkingFeeActive;
            gasFeeActive = fleetFare.gasFeeActive;
            heavyTrafficActive = fleetFare.heavyTrafficActive;
            tollFeeLimitDriverInputActive = fleetFare.tollFeeLimitDriverInputActive != null
                    && fleetFare.tollFeeLimitDriverInputActive;
            parkingFeeLimitDriverInputActive = fleetFare.parkingFeeLimitDriverInputActive;
            gasFeeLimitDriverInputActive = fleetFare.gasFeeLimitDriverInputActive;
            otherFeeLimitDriverInputActive = fleetFare.otherFee.limitDriverInputActive != null
                    && fleetFare.otherFee.limitDriverInputActive;

            if(tollFeeLimitDriverInputActive && fleetFare.tollFeeDriverCanInput != null){
                for(AmountByCurrency amountByCurrency: fleetFare.tollFeeDriverCanInput){
                    if(amountByCurrency.currencyISO.equals(booking.currencyISO))
                        tollFeeDriverCanInput = amountByCurrency.value;
                }
            }
            if(parkingFeeLimitDriverInputActive && fleetFare.parkingFeeDriverCanInput != null){
                for(AmountByCurrency amountByCurrency: fleetFare.parkingFeeDriverCanInput){
                    if(amountByCurrency.currencyISO.equals(booking.currencyISO))
                        parkingFeeDriverCanInput = amountByCurrency.value;
                }
            }
            if(gasFeeLimitDriverInputActive && fleetFare.gasFeeDriverCanInput != null){
                for(AmountByCurrency amountByCurrency: fleetFare.gasFeeDriverCanInput){
                    if(amountByCurrency.currencyISO.equals(booking.currencyISO))
                        gasFeeDriverCanInput = amountByCurrency.value;
                }
            }
            if(otherFeeLimitDriverInputActive && fleetFare.otherFee.otherFeeDriverCanInput != null){
                for(AmountByCurrency amountByCurrency: fleetFare.otherFee.otherFeeDriverCanInput){
                    if(amountByCurrency.currencyISO.equals(booking.currencyISO))
                        otherFeeDriverCanInput = amountByCurrency.value;
                }
            }

            //get surcharge
            if(fleetFare.rushHourActive){
                String pickupTz = "";
                String dropOffTz = "";
                if (booking.request.pickup != null) {
                    pickupTz = booking.request.pickup.timezone != null ? booking.request.pickup.timezone : "";
                }
                if (booking.request.destination != null) {
                    dropOffTz = booking.request.destination.timezone != null ? booking.request.destination.timezone : "";
                }
                String surchargeTypeRate = estimateUtil.getSurchargeTypeRateFromBooking(booking);
                String fareType = "";
                if (!KeysUtil.INCOMPLETE_PAYMENT.contains(booking.status)) {
                    fareType = redisDao.getTmp(requestId, "fareType"+"-"+booking.bookId);
                }
                logger.debug(requestId + " - surchargeTypeRate: " + surchargeTypeRate);
                logger.debug(requestId + " - fareType: " + fareType);
                if (engagedTime != null) {
                    List<Object> getRushHour = null;
                    try {
                        getRushHour = estimateUtil.getRushHour(fleet, fleetFare.rushHours, engagedTime, droppedOffTime, booking.currencyISO,
                                pickupTz, dropOffTz, distance, surchargeTypeRate, fareType, booking.reservation, requestId);
                        surchargeType = getRushHour.get(0).toString();
                        surchargeFee = Double.parseDouble(getRushHour.get(1).toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }else {
                    if(pickupTime!= null){
                        List<Object> getRushHour = null;
                        try {
                            getRushHour = estimateUtil.getRushHour(fleet, fleetFare.rushHours, pickupTime, droppedOffTime, booking.currencyISO,
                                    pickupTz, dropOffTz, distance, surchargeTypeRate, fareType, booking.reservation, requestId);
                            surchargeType = getRushHour.get(0).toString();
                            surchargeFee = Double.parseDouble(getRushHour.get(1).toString());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        if(otherFeeActive){
            editOtherFees = fleetFare.otherFee != null ? fleetFare.otherFee.isEdit: false;
            addNote = fleetFare.otherFee.addNote;
        }

        if (!corporateId.equals("")) {
            Corporate corporate = mongoDao.getCorporate(corporateId);
            if (corporate != null) {
                travellerSignature = corporate.travellerSignature != null ? corporate.travellerSignature: false;
                trackingLog = corporate.trackingLog != null ? corporate.trackingLog: false;
                rating = corporate.rating != null ? corporate.rating: false;
            }
        }

        // off setting for booking from HolidayTAxis
        if (KeysUtil.THIRD_PARTY_HOLIDAYTAXIS.equals(bookFrom)) {
            editBasicFare = false;
            editOtherFees = false;
            editTax = false;
            editTip = false;
            techFeeActive = false;
            techFeeType = "amount";
            techFeeValue = 0.0;
            tipActive = false;
            taxActive = false;
            taxValue = 0.0;
            couponType = "";
            couponValue = 0.0;
            maximumValue = 0.0;
            keepMinFee = false;
            meetDriverActive = false;
            heavyTrafficActive = false;
            airportActive = false;
            commissionType = "amount";
            commissionValue = 0.0;
            otherFeeActive = false;
            rushHourActive = false;
            bookingFeeActive = false;
            tollFeeActive = false;
            parkingFeeActive = false;
            gasFeeActive = false;
            addNote = false;
            bookingFee = 0.0;
            taxFee = 0.0;
            tipFee = 0.0;
            addOnPrice = 0.0;
            minimum = 0.0;
            travellerSignature = false;
            trackingLog = false;
            surchargeFee = 0;
            surchargeType = "amount";
            basicFareOnly = false;
            serviceActive = false;
            creditTransactionFeePercent = 0.0;
            creditTransactionFeeAmount = 0.0;
        }

        Map<String,Object> map = new HashMap<>();
        map.put("editBasicFare", editBasicFare);
        map.put("editOtherFees", editOtherFees);
        map.put("editTax", editTax);
        map.put("editTip", editTip);
        map.put("techFeeActive", techFeeActive);
        map.put("techFeeType", techFeeType);
        map.put("techFeeValue", techFeeValue);
        map.put("tipActive", tipActive);
        map.put("taxActive", taxActive);
        map.put("taxValue", taxValue);
        map.put("couponType", couponType);
        map.put("couponValue", couponValue);
        map.put("maximumValue", maximumValue);
        map.put("meetDriverActive", meetDriverActive);
        map.put("heavyTrafficActive", heavyTrafficActive);
        map.put("airportActive", airportActive);
        map.put("commissionType", commissionType);
        map.put("commissionValue", commissionValue);
        map.put("otherFeeActive", otherFeeActive);
        map.put("rushHourActive", rushHourActive);
        map.put("bookingFeeActive", bookingFeeActive);
        map.put("tollFeeActive", tollFeeActive);
        map.put("parkingFeeActive", parkingFeeActive);
        map.put("gasFeeActive", gasFeeActive);
        map.put("addNote", addNote);
        map.put("actualFare", actualFare);
        map.put("bookingFee", bookingFee);
        map.put("addOnPrice", addOnPrice);
        map.put("taxFee", taxFee);
        map.put("tipFee", tipFee);
        map.put("minimum", minimum);
        map.put("travellerSignature", travellerSignature);
        map.put("trackingLog", trackingLog);
        map.put("rating", rating);
        map.put("vehicleType", vehicleTypeRq);
        map.put("surchargeType", surchargeType);
        map.put("surchargeFee", surchargeFee);
        map.put("basicFareOnly", basicFareOnly);
        map.put("tollFeePayTo", tollFeePayTo);
        map.put("parkingFeePayTo", parkingFeePayTo);
        map.put("gasFeePayTo", gasFeePayTo);
        map.put("meetDriverPayTo", meetDriverPayTo);
        map.put("airportPayTo", airportPayTo);
        map.put("serviceActive", serviceActive);
        map.put("services", services);
        map.put("avatarDriver", avatarDriver);
        map.put("tollFeeLimitDriverInputActive", tollFeeLimitDriverInputActive);
        map.put("parkingFeeLimitDriverInputActive", parkingFeeLimitDriverInputActive);
        map.put("gasFeeLimitDriverInputActive", gasFeeLimitDriverInputActive);
        map.put("tollFeeDriverCanInput", tollFeeDriverCanInput);
        map.put("parkingFeeDriverCanInput", parkingFeeDriverCanInput);
        map.put("gasFeeDriverCanInput", gasFeeDriverCanInput);
        map.put("otherFeeLimitDriverInputActive", otherFeeLimitDriverInputActive);
        map.put("otherFeeDriverCanInput", otherFeeDriverCanInput);
        map.put("hideFare", hideFare);
        map.put("superHelper", superHelper);
        map.put("creditTransactionFeeAmount", creditTransactionFeeAmount);
        map.put("creditTransactionFeePercent", creditTransactionFeePercent);
        map.put("keepMinFee", keepMinFee);
        return map;
    }

    public Map<String,Object> getSettingsDataForAffiliate(Fleet fleet, Booking booking) {
        boolean editBasicFare = true;   // set default true - allow edit fare on driver app for payable
        boolean editOtherFees = false;
        boolean editTax = false;
        boolean editTip = false;
        boolean techFeeActive = false;
        String techFeeType = "amount";
        double techFeeValue = 0.0;
        boolean tipActive = false;
        boolean taxActive = false;
        double taxValue = 0.0;
        String couponType = "";
        double couponValue = 0.0;
        boolean meetDriverActive = false;
        boolean heavyTrafficActive = false;
        boolean airportActive = false;
        String commissionType = "amount";
        double commissionValue = 0.0;
        boolean otherFeeActive = false;
        boolean rushHourActive = false;
        boolean bookingFeeActive = false;
        boolean tollFeeActive = false;
        boolean parkingFeeActive = false;
        boolean gasFeeActive = false;
        boolean addNote = false;
        boolean actualFare = false;
        double bookingFee = 0.0;
        double taxFee = 0.0;
        double tipFee = 0.0;
        double minimum = 0.0;
        boolean travellerSignature = false;
        boolean trackingLog = false;
        boolean rating = false;
        boolean basicFareOnly = false;
        int tollFeePayTo = 0;
        int parkingFeePayTo = 0;
        int gasFeePayTo = 0;
        int meetDriverPayTo = 0;
        int airportPayTo = 0;
        boolean serviceActive = false;
        List<Service> services = new ArrayList<>();
        String avatarDriver = "";

        /// GET SETTINGS
        String vehicleTypeRq = "";
        String vhcType = "";
        String drvId = "";
        String bookFrom = "";
        String zoneId = "";
        RateGeneral rateGeneral = null;
        boolean normalFare = false;
        if(booking != null){
            bookFrom = booking.bookFrom;
            if (booking.request != null)
                vehicleTypeRq = booking.request.vehicleTypeRequest != null ? booking.request.vehicleTypeRequest : "";
            if (booking.drvInfo != null)
                vhcType = booking.drvInfo.vehicleType != null ? booking.drvInfo.vehicleType : "";
            if(booking.drvInfo != null )
                drvId = booking.drvInfo.userId;
            if(booking.request != null && booking.request.pickup != null){
                Zone zone = mongoDao.findByFleetIdAndGeo(fleet.fleetId, booking.request.pickup.geo);
                if(zone != null){
                    zoneId = zone._id.toString();
                }
            }
            if(booking.request != null && booking.request.serviceActive)
                serviceActive = booking.request.serviceActive;
            if(booking.request != null && booking.request.services != null)
                services = booking.request.services;
            if(booking.drvInfo != null && booking.drvInfo.avatar != null)
                avatarDriver = booking.drvInfo.avatar;
            if(booking.request != null && booking.request.estimate != null && booking.request.estimate.fare != null)
                normalFare = booking.request.estimate.fare.normalFare != null && booking.request.estimate.fare.normalFare;
        }
        //check edit basic fare, edit tax by vehicle and driver
        boolean forceMeter = false;
        Account account = mongoDao.getAccount(drvId);
        if(account != null){
            if(fleet.hardwareMeter){
                if(account.driverInfo != null && account.driverInfo.forceMeter){
                    forceMeter = true;
                }
            }
        }
        VehicleType type = mongoDao.getVehicleTypeByFleetAndName(fleet.fleetId, vhcType);
        AffiliationCarType vehicleType = mongoDao.getAffiliationCarTypeByName(vehicleTypeRq);
        if(type != null && type.drvApp != null){
            editBasicFare = type.drvApp.enableEditFare;
            if(type.drvApp.enableEditFare){
                if(forceMeter){
                    editBasicFare = type.drvApp.drvForceMeter;
                }else {
                    editBasicFare = type.drvApp.drvNotForceMeter;
                }
            }
            editTax = type.editTax;

            //get minimum setting
            if (vehicleType != null && !zoneId.isEmpty()) {


                List<AffiliateAssignedRate> listAffiliateAssignedRate = mongoDao.getListAffiliateAssignedRate(vehicleType._id.toString(), zoneId);

                AffiliateAssignedRate assignedRateGeneral = listAffiliateAssignedRate.stream()
                        .filter(assignedRate -> assignedRate.type.equalsIgnoreCase(KeysUtil.GENERAL))
                        .findFirst().orElse(null);
                if(assignedRateGeneral!= null)
                    rateGeneral = mongoDao.getAffiliateRateGeneral(assignedRateGeneral.rate.id);

                /*AffiliateAssignedRate assignedRateNormal = listAffiliateAssignedRate.stream()
                        .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase(booking.request.psgFleetId) && assignedRate.type.equalsIgnoreCase(KeysUtil.REGULAR) && assignedRate.priceType.equalsIgnoreCase(KeysUtil.SELLPRICE))
                        .findFirst().orElse(null);
                if(assignedRateNormal == null){
                    assignedRateNormal = listAffiliateAssignedRate.stream()
                            .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase("") && assignedRate.type.equalsIgnoreCase(KeysUtil.REGULAR) && assignedRate.priceType.equalsIgnoreCase(KeysUtil.SELLPRICE))
                            .findFirst().orElse(null);
                }
                if(assignedRateNormal!= null){
                    RateRegular normal = mongoDao.getAffiliateRate(assignedRateNormal.rate.id);
                    if(normal != null && normal.minimum != null){
                        if (!reservation)
                            minimum = normal.minimum.now;
                        else
                            minimum = normal.minimum.reservation;
                    }
                }*/
            }
            if(booking != null && booking.request != null && booking.request.estimate != null && booking.request.estimate.fare!= null){
                if(booking.request.estimate.fare.etaFare >0){
                    //bookingFee = booking.request.estimate.fare.bookingFee;
                    taxFee = booking.request.estimate.fare.tax;
                    tipFee = booking.request.estimate.fare.tip;
                    if(booking.request.estimate.fare.min)
                        minimum = booking.request.estimate.fare.minFare;
                    else
                        minimum = 0;
                }
            }
        }

        //get booking fee
        /*List<Object> bookFee = getBookingFee(mDispatcherId, corporateId, bookFrom, booking.currencyISO);
        commissionType = bookFee.get(0).toString();
        commissionValue = Double.parseDouble(bookFee.get(1).toString());
        bookingFeeActive = Boolean.parseBoolean(bookFee.get(2).toString());*/

        if(rateGeneral != null && normalFare){
            if(rateGeneral.techFee != null && rateGeneral.techFee.enable != null && rateGeneral.techFee.enable){
                techFeeActive = rateGeneral.techFee.enable;
                if(KeysUtil.web_booking.equals(bookFrom))
                    techFeeValue = rateGeneral.techFee.webBooking != null ? rateGeneral.techFee.webBooking: 0;
                else if(KeysUtil.dash_board.equals(booking.bookFrom) || KeysUtil.corp_board.equals(booking.bookFrom))
                    techFeeValue = rateGeneral.techFee.bookingDashboard != null ? rateGeneral.techFee.bookingDashboard: 0;
                else if(KeysUtil.command_center.equals(bookFrom))
                    techFeeValue = rateGeneral.techFee.commandCenter != null ? rateGeneral.techFee.commandCenter: 0;
                else
                    techFeeValue = rateGeneral.techFee.paxApp != null ? rateGeneral.techFee.paxApp: 0;
            }

            //get setting of fleet
            editOtherFees = rateGeneral.otherFees != null ? rateGeneral.otherFees.edit: false;
            editTip = fleet.editTip;
            tipActive = rateGeneral.tip.enable;
            taxActive = rateGeneral.tax.enable;
            taxValue = rateGeneral.tax.surcharge;
            meetDriverActive = fleet.additionalService.fromAirport.meetDriver;
            basicFareOnly = fleet.estimateFare == 0;
            heavyTrafficActive = rateGeneral.heavyTraffic.enable;
            if(rateGeneral.airport != null){
                if(rateGeneral.airport.fromAirportActive || rateGeneral.airport.toAirportActive)
                    airportActive = true;
            }
            otherFeeActive = rateGeneral.otherFees.enable;
            rushHourActive = rateGeneral.rushHour;
            tollFeeActive = rateGeneral.tollFee;
            parkingFeeActive = rateGeneral.parkingFee;
            gasFeeActive = rateGeneral.gasFee;
            addNote = rateGeneral.otherFees.addNote;
        }

        FleetFare fleetFare = mongoDao.getFleetFare(fleet.fleetId);
        if (fleetFare != null) {
            tollFeePayTo = fleetFare.tollFeePayTo;
            parkingFeePayTo = fleetFare.parkingFeePayTo;
            gasFeePayTo = fleetFare.gasFeePayTo;
            if (fleetFare.meetDriver != null) meetDriverPayTo = fleetFare.meetDriver.payTo;
            if (fleetFare.airport != null) airportPayTo = fleetFare.airport.payTo;
        }
        Map<String,Object> map = new HashMap<>();
        map.put("editBasicFare", editBasicFare);
        map.put("editOtherFees", editOtherFees);
        map.put("editTax", editTax);
        map.put("editTip", editTip);
        map.put("techFeeActive", techFeeActive);
        map.put("techFeeType", techFeeType);
        map.put("techFeeValue", techFeeValue);
        map.put("tipActive", tipActive);
        map.put("taxActive", taxActive);
        map.put("taxValue", taxValue);
        map.put("couponType", couponType);
        map.put("couponValue", couponValue);
        map.put("meetDriverActive", meetDriverActive);
        map.put("heavyTrafficActive", heavyTrafficActive);
        map.put("airportActive", airportActive);
        map.put("commissionType", commissionType);
        map.put("commissionValue", commissionValue);
        map.put("otherFeeActive", otherFeeActive);
        map.put("rushHourActive", rushHourActive);
        map.put("bookingFeeActive", bookingFeeActive);
        map.put("tollFeeActive", tollFeeActive);
        map.put("parkingFeeActive", parkingFeeActive);
        map.put("gasFeeActive", gasFeeActive);
        map.put("addNote", addNote);
        map.put("actualFare", actualFare);
        map.put("bookingFee", bookingFee);
        map.put("taxFee", taxFee);
        map.put("tipFee", tipFee);
        map.put("minimum", minimum);
        map.put("travellerSignature", travellerSignature);
        map.put("trackingLog", trackingLog);
        map.put("rating", rating);
        map.put("vehicleType", vehicleTypeRq);
        map.put("basicFareOnly", basicFareOnly);
        map.put("tollFeePayTo", tollFeePayTo);
        map.put("parkingFeePayTo", parkingFeePayTo);
        map.put("gasFeePayTo", gasFeePayTo);
        map.put("meetDriverPayTo", meetDriverPayTo);
        map.put("airportPayTo", airportPayTo);
        map.put("serviceActive", serviceActive);
        map.put("services", services);
        map.put("avatarDriver", avatarDriver);
        return map;
    }

    private double getModifyPriceOfCorp(String corporateId, double fare, String requestId) {
        double discount = fare;
        if(corporateId!= null && !corporateId.isEmpty()){
            Corporate corporate = mongoDao.getCorporate(corporateId);
            if (corporate != null && corporate.isActive && corporate.pricing != null) {
                if(corporate.pricing.discountByPercentage!= null && corporate.pricing.discountByPercentage){
                    logger.debug(requestId + " - discount:  " + corporate.pricing.value);
                    discount = fare - (fare * corporate.pricing.value)/100;
                }else if(corporate.pricing.markUpByPercentage){
                    logger.debug(requestId + " - markup:  " + corporate.pricing.markUpValue);
                    discount = fare + (fare * corporate.pricing.markUpValue)/100;
                }
            }
        }
        return discount;
    }

    public JSONObject toObjectResponse(String string) {
        try {
            JSONParser parser = new JSONParser();
            return (JSONObject) parser.parse(string);
        } catch(Exception ex) {
            ex.getMessage();
            return new JSONObject();
        }
    }

    public int getReturnCode(String string) {
        try {
            if (!string.isEmpty()) {
                JSONParser parser = new JSONParser();
                JSONObject obj = (JSONObject) parser.parse(string);
                return Integer.parseInt(obj.get("returnCode").toString());
            }
        } catch(Exception ex) {
            ex.getMessage();
        }
        return 0;
    }

    public PaymentEnt addPaymentData(Fleet fleetInfor, PaymentEnt paymentEnt, String paymentType, String cardOwner, Booking booking) {
        logger.debug(paymentEnt.requestId + " - paymentType: " + paymentType);
        String transactionStatus = getTransactionStatus(paymentType, cardOwner, booking.bookFrom);
        logger.debug(paymentEnt.requestId + " transactionStatus: " + transactionStatus);
        paymentEnt.transactionStatus = transactionStatus;
        if (paymentType.equals(KeysUtil.PAYMENT_DIRECTBILLING_3RD) || paymentType.equals(KeysUtil.PAYMENT_CORPORATE_3RD)
                || paymentType.equals(KeysUtil.PAYMENT_CASH_3RD) || paymentType.equals(KeysUtil.PAYMENT_FLEETCARD_3RD))
            paymentEnt.avis3rd = 1;
        String canceller = paymentEnt.canceller != null ? paymentEnt.canceller : "";
        String gateway = "";
        if (transactionStatus.equals(KeysUtil.PAYMENT_EXTERNAL_WALLET)) {
            gateway = booking.request.gateway != null ? booking.request.gateway : "";
        }
        paymentEnt.paidBy = TicketUtil.getPaidBy(transactionStatus, canceller, gateway, paymentEnt.splitPayment, paymentEnt.stripeMethod);
        if (paymentType.equals(KeysUtil.PAYMENT_CORPORATE) || paymentType.equals(KeysUtil.PAYMENT_PREPAID) || paymentType.equals(KeysUtil.PAYMENT_DIRECTBILLING) ||
                (KeysUtil.PAYMENT_CANCELED.equals(transactionStatus) && "CorpAD".equalsIgnoreCase(canceller))) {
            // check airline corporate
            String corporateId = (booking.corporateInfo != null && booking.corporateInfo.corporateId != null) ? booking.corporateInfo.corporateId : "";
            logger.debug(paymentEnt.requestId + " - corporateId: " + corporateId);
            if (!corporateId.isEmpty()) {
                Corporate corporate = mongoDao.getCorporate(corporateId);
                if (corporate != null) {
                    logger.debug(paymentEnt.requestId + " - isAirLine: " + corporate.isAirline);
                    if (corporate.isAirline) {
                        if (paymentType.equals(KeysUtil.PAYMENT_CORPORATE))
                            paymentEnt.paidBy = paymentEnt.splitPayment ? "Wallet, Business card" : "Business card";
                        else if (paymentType.equals(KeysUtil.PAYMENT_PREPAID))
                            paymentEnt.paidBy = paymentEnt.splitPayment ? "Wallet, Prepaid" : "Prepaid";
                        else if (paymentType.equals(KeysUtil.PAYMENT_DIRECTBILLING))
                            paymentEnt.paidBy = paymentEnt.splitPayment ? "Wallet, Direct invoicing" : "Direct invoicing";
                        else if (paymentType.equals(KeysUtil.PAYMENT_CANCELED))
                            paymentEnt.paidBy = "Canceled by Airline Admin";
                    }
                }
            }
        }
        try {
            paymentEnt.originalFare = booking.request.estimate.fare.etaFare;
        } catch (Exception ignore) {}


        // set data for driver settlement if enabled
        if (fleetInfor != null) {
            if(fleetInfor.driverSettlement){
                List<String> arrGateway = Arrays.asList(KeysUtil.STRIPE, KeysUtil.JETPAY);
                ConfigGateway configGateway = getConfigGateway(fleetInfor, "", booking.request.pickup.geo);
                if (configGateway != null && arrGateway.contains(configGateway.gateway))
                    paymentEnt.paidToDriver = -1;
            }
        }
        String customerId = booking.psgInfo != null && booking.psgInfo.userId != null ? booking.psgInfo.userId : "";
        String driverId = booking.drvInfo != null && booking.drvInfo.userId != null ? booking.drvInfo.userId : "";
        Fare fare = booking.request != null && booking.request.estimate != null ? booking.request.estimate.fare : null;
        if (fare != null && !KeysUtil.INCOMPLETE_PAYMENT.contains(transactionStatus)) {
            paymentEnt.meetDriverFee = fare.meetDriverFee;
            boolean shortTrip = fare.shortTrip;
            if (!driverId.isEmpty() && !transactionStatus.equals(KeysUtil.PAYMENT_COMPLETE_WITHOUT_SERVICE)) {
                paymentEnt.grossEarning = calGrossEarning(booking, paymentEnt.requestId, fleetInfor, customerId, driverId, paymentEnt.promoCode, shortTrip,
                        paymentEnt.subTotal, paymentEnt.promoAmount, paymentEnt.tip, paymentEnt.airportSurcharge,
                        paymentEnt.meetDriverFee, paymentEnt.tollFee, paymentEnt.parkingFee, paymentEnt.gasFee);
            }
            paymentEnt.fleetMarkup = fare.fleetMarkup;
            paymentEnt.sellPriceMarkup = fare.sellPriceMarkup;
        }

        // add default primary partial method for all booking
        paymentEnt.primaryPartialMethod = booking.request.primaryPartialMethod;
        paymentEnt.hydraPaymentMethod = booking.hydraInfo != null && booking.hydraInfo.hydraPaymentMethod != null ? booking.hydraInfo.hydraPaymentMethod : "";
        paymentEnt.isFarmOut = booking.isFarmOut;

        paymentEnt.completedTime = TimezoneUtil.getGMTTimestamp();
        SimpleDateFormat sdfZero = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000'Z'");
        logger.debug(paymentEnt.requestId + " - completedTime: " + sdfZero.format(paymentEnt.completedTime));

        // check setting force pay outstanding then remove customer debt from total
        double customerDebt = getCustomerDebt(paymentEnt.requestId, booking.fleetId, booking.psgInfo.userId, booking, booking.currencyISO, paymentEnt.payFromCC, paymentEnt.writeOffDebt);
        if (customerDebt > 0) {
            logger.debug(paymentEnt.requestId + " - customerDebt: " + customerDebt);
            paymentEnt.total = Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(paymentEnt.total - customerDebt));
            logger.debug(paymentEnt.requestId + " - total after customerDebt: " + paymentEnt.total);
        }

        return  paymentEnt;
    }

    public Map<String,String> getCreditToken(Booking booking, boolean isLocalZone) {
        String cardOwner = "passenger";
        String localToken = "";
        String crossToken = "";
        String cardMask = "";
        String id = "";
        String gateway = "";
        String cardHolder = "";
        String cardType = "";
        if (booking.psgInfo != null && booking.psgInfo.creditInfo != null) {
            localToken = booking.psgInfo.creditInfo.localToken != null ? booking.psgInfo.creditInfo.localToken : "";
            crossToken = booking.psgInfo.creditInfo.crossToken != null ? booking.psgInfo.creditInfo.crossToken : "";
            cardMask = booking.psgInfo.creditInfo.cardMask != null ? booking.psgInfo.creditInfo.cardMask : "";
            id = booking.psgInfo.userId != null ? booking.psgInfo.userId : "";
            gateway = booking.psgInfo.creditInfo.gateway != null ? booking.psgInfo.creditInfo.gateway : "";
            cardHolder = booking.psgInfo.creditInfo.cardHolder != null ? booking.psgInfo.creditInfo.cardHolder : "";
            cardType = booking.psgInfo.creditInfo.cardType != null ? booking.psgInfo.creditInfo.cardType : "";
            if ((isLocalZone && localToken.isEmpty()) || (!isLocalZone && crossToken.isEmpty())) {
                int paymentType = booking.request.paymentType != null ? booking.request.paymentType : 1;
                if (paymentType == 2 || paymentType == 13) {
                    // did not store card info on booking - try to get from profile
                    Fleet fleet = mongoDao.getFleetInfor(booking.fleetId);
                    if (fleet.creditConfig.enable) {
                        ConfigGateway configGateway = getConfigGateway(fleet, "", booking.request.pickup.geo);
                        if (configGateway != null) {
                            gateway = configGateway.gateway != null ? configGateway.gateway : "";
                        }
                        Account account = mongoDao.getAccount(id);
                        List<Credit> listCredit = account.credits;
                        if (listCredit != null && !listCredit.isEmpty()) {
                            for (Credit credit : listCredit) {
                                if (credit.gateway.equals(gateway)) {
                                    localToken = credit.localToken != null ? credit.localToken : "";
                                    crossToken = credit.crossToken != null ? credit.crossToken : "";
                                    cardMask = credit.cardMask != null ? credit.cardMask : "";
                                    cardHolder = credit.cardHolder != null ? credit.cardHolder : "";
                                    cardType = credit.cardType != null ? credit.cardType : "";
                                }
                            }
                        }
                    }
                }
            }
        }
        if (localToken.equals("") && crossToken.equals("")) {
            // booking do not using passenger card
            // try to check card of mDispatcher of corporate
            if (booking.bookFrom.equals("mDispatcher")) {
                if (booking.mDispatcherInfo != null && booking.mDispatcherInfo.creditInfo != null) {
                    localToken = booking.mDispatcherInfo.creditInfo.localToken != null ? booking.mDispatcherInfo.creditInfo.localToken : "";
                    crossToken = booking.mDispatcherInfo.creditInfo.crossToken != null ? booking.mDispatcherInfo.creditInfo.crossToken : "";
                    cardMask = booking.mDispatcherInfo.creditInfo.cardMask != null ? booking.mDispatcherInfo.creditInfo.cardMask : "";
                    cardOwner = "mDispatcher";
                    id = booking.mDispatcherInfo.userId != null ? booking.mDispatcherInfo.userId : "";
                    gateway = booking.mDispatcherInfo.creditInfo.gateway != null ? booking.mDispatcherInfo.creditInfo.gateway : "";
                    cardHolder = booking.mDispatcherInfo.creditInfo.cardHolder != null ? booking.mDispatcherInfo.creditInfo.cardHolder : "";
                    cardType = booking.mDispatcherInfo.creditInfo.cardType != null ? booking.mDispatcherInfo.creditInfo.cardType : "";
                }
            } else {
                if (booking.corporateInfo != null && booking.corporateInfo.creditInfo != null) {
                    localToken = booking.corporateInfo.creditInfo.localToken != null ? booking.corporateInfo.creditInfo.localToken : "";
                    crossToken = booking.corporateInfo.creditInfo.crossToken != null ? booking.corporateInfo.creditInfo.crossToken : "";
                    cardMask = booking.corporateInfo.creditInfo.cardMask != null ? booking.corporateInfo.creditInfo.cardMask : "";
                    cardOwner = "corporate";
                    id = booking.corporateInfo.corporateId != null ? booking.corporateInfo.corporateId : "";
                    gateway = booking.corporateInfo.creditInfo.gateway != null ? booking.corporateInfo.creditInfo.gateway : "";
                    cardHolder = booking.corporateInfo.creditInfo.cardHolder != null ? booking.corporateInfo.creditInfo.cardHolder : "";
                    cardType = booking.corporateInfo.creditInfo.cardType != null ? booking.corporateInfo.creditInfo.cardType : "";
                }
            }
        }
        String token = isLocalZone ? localToken : crossToken;
        Map<String,String> map = new HashMap<>();
        map.put("token", token);
        map.put("cardOwner", cardOwner);
        map.put("cardMask", cardMask);
        map.put("id", id);
        map.put("gateway", gateway);
        map.put("cardHolder", cardHolder);
        map.put("cardType", cardType);
        return map;
    }

    public Map<String,String> getCreditToken(Booking booking, String paymentType, boolean isLocalZone) {
        logger.debug(booking.bookId + " - booking: " + gson.toJson(booking));
        logger.debug(booking.bookId + " - paymentType: " + paymentType);
        logger.debug(booking.bookId + " - isLocalZone: " + isLocalZone);
        String localToken = "";
        String crossToken = "";
        String id = "";
        String cardOwner = "";
        String cardMask = "";
        if (KeysUtil.PAYMENT_CARD.equalsIgnoreCase(paymentType) ||
                paymentType.equals("1") ||  // payByCreditToken - paymentType = 1: personal card
                KeysUtil.PAYMENT_PAXWALLET.equals(paymentType)) { // support top-up by token before charge wallet
            if (booking.psgInfo != null && booking.psgInfo.creditInfo != null) {
                localToken = booking.psgInfo.creditInfo.localToken != null ? booking.psgInfo.creditInfo.localToken : "";
                crossToken = booking.psgInfo.creditInfo.crossToken != null ? booking.psgInfo.creditInfo.crossToken : "";
                cardMask = booking.psgInfo.creditInfo.cardMask != null ? booking.psgInfo.creditInfo.cardMask : "";
                cardOwner = "passenger";
            }
            id = booking.psgInfo != null && booking.psgInfo.userId != null ? booking.psgInfo.userId : "";
            if ((isLocalZone && localToken.isEmpty()) || (!isLocalZone && crossToken.isEmpty())) {
                // did not store card info on booking - try to get from profile
                String gateway = "";
                Fleet fleet = mongoDao.getFleetInfor(booking.fleetId);
                if (fleet.creditConfig.enable) {
                    ConfigGateway configGateway = getConfigGateway(fleet, "", booking.request.pickup.geo);
                    if (configGateway != null) {
                        gateway = configGateway.gateway != null ? configGateway.gateway : "";
                    }
                    Account account = mongoDao.getAccount(id);
                    List<Credit> listCredit = account.credits;
                    if (listCredit != null && !listCredit.isEmpty()) {
                        for (Credit credit : listCredit) {
                            if (credit.gateway.equals(gateway)) {
                                localToken = credit.localToken != null ? credit.localToken : "";
                                crossToken = credit.crossToken != null ? credit.crossToken : "";
                                cardMask = credit.cardMask != null ? credit.cardMask : "";
                                cardOwner = "passenger";
                            }
                        }
                    }
                }
            }
        } else if (KeysUtil.PAYMENT_CORPORATE.equalsIgnoreCase(paymentType) ||
                paymentType.equals("7")) { // payByCreditToken - paymentType = 7: corporate card
            if (booking.corporateInfo != null && booking.corporateInfo.creditInfo != null) {
                localToken = booking.corporateInfo.creditInfo.localToken != null ? booking.corporateInfo.creditInfo.localToken : "";
                crossToken = booking.corporateInfo.creditInfo.crossToken != null ? booking.corporateInfo.creditInfo.crossToken : "";
                cardMask = booking.corporateInfo.creditInfo.cardMask != null ? booking.corporateInfo.creditInfo.cardMask : "";
                id = booking.corporateInfo.corporateId != null ? booking.corporateInfo.corporateId : "";
                cardOwner = "corporate";
            }
        } else if (KeysUtil.PAYMENT_MDISPATCHER.equalsIgnoreCase(paymentType) ||
                paymentType.equals("5")) { // payByCreditToken - paymentType = 5: mDispatcher card
            if (booking.mDispatcherInfo != null && booking.mDispatcherInfo.creditInfo != null) {
                localToken = booking.mDispatcherInfo.creditInfo.localToken != null ? booking.mDispatcherInfo.creditInfo.localToken : "";
                crossToken = booking.mDispatcherInfo.creditInfo.crossToken != null ? booking.mDispatcherInfo.creditInfo.crossToken : "";
                cardMask = booking.mDispatcherInfo.creditInfo.cardMask != null ? booking.mDispatcherInfo.creditInfo.cardMask : "";
                id = booking.mDispatcherInfo.userId != null ? booking.mDispatcherInfo.userId : "";
                cardOwner = "mDispatcher";
            }
        }

        String token = isLocalZone ? localToken : crossToken;
        Map<String,String> map = new HashMap<>();
        map.put("token", token);
        map.put("cardOwner", cardOwner);
        map.put("cardMask", cardMask);
        map.put("id", id);
        return map;
    }

    public Map<String,String> getCustomerData(String cardOwner, String id, String token, String requestId) {
        Map<String,String> customerData = new HashMap<>();
        String cardType = "";
        String cardMask = "";
        String customerEmail = "";
        String customerIP = "";
        String phone = "";
        String street = "";
        String city = "";
        String state = "";
        String zipCode = "";
        String country = "";
        boolean valid = false;
        try {
            if (cardOwner.equals("passenger") || cardOwner.equals("mDispatcher")) {
                Account account = mongoDao.getAccount(id);
                if (account != null) {
                    valid = true;
                    customerEmail = account.email != null ? account.email : "";
                    customerIP = account.address != null ? account.address : "";
                    phone = account.phone;
                    if (account.credits != null && account.credits.size() > 0) {
                        for (Credit credit : account.credits) {
                            if (token.equals(credit.localToken) || token.equals(credit.crossToken)) {
                                cardType = credit.cardType != null ? credit.cardType : "";
                                cardMask = credit.cardMask != null ? credit.cardMask : "";
                                street = credit.street != null ? credit.street : "";
                                city = credit.city != null ? credit.city : "";
                                state = credit.state != null ? credit.state : "";
                                zipCode = credit.zipCode != null ? credit.zipCode : "";
                                country = credit.country != null ? credit.country : "";
                                break;
                            }
                        }
                    }
                }

            } else if (cardOwner.equals("corporate")) {
                Corporate corporate = mongoDao.getCorporate(id);
                if (corporate != null && corporate.credits != null && corporate.credits.size() > 0) {
                    valid = true;
                    for (Credit credit : corporate.credits) {
                        if (token.equals(credit.localToken) || token.equals(credit.crossToken)) {
                            cardType = credit.cardType != null ? credit.cardType : "";
                            cardMask = credit.cardMask != null ? credit.cardMask : "";
                            street = credit.street != null ? credit.street : "";
                            city = credit.city != null ? credit.city : "";
                            state = credit.state != null ? credit.state : "";
                            zipCode = credit.zipCode != null ? credit.zipCode : "";
                            country = credit.country != null ? credit.country : "";
                            break;
                        }
                    }
                }
            }
        } catch(Exception ex) {
            logger.debug(requestId + " - get card info ERROR: " + CommonUtils.getError(ex));
        }
        if (valid) {
            customerData.put("cardType", cardType);
            customerData.put("cardMask", cardMask);
            customerData.put("customerEmail", customerEmail);
            customerData.put("customerIP", customerIP);
            customerData.put("phone", phone);
            customerData.put("street", street);
            customerData.put("city", city);
            customerData.put("state", state);
            customerData.put("zipCode", zipCode);
            customerData.put("country", country);
        }
        return customerData;
    }

    public Map<String,String> getCustomerDataFromBooking(Booking booking, String cardOwner, String id, String requestId) {
        Map<String,String> customerData = new HashMap<>();
        String customerEmail = "";
        String customerIP = "";
        String phone = "";
        String cardType = "";
        String cardMask = "";
        String street = "";
        String city = "";
        String state = "";
        String zipCode = "";
        String country = "";
        boolean valid = false;
        try {
            if (cardOwner.equals("passenger")) {
                Account account = mongoDao.getAccount(id);
                if (account != null) {
                    customerEmail = account.email != null ? account.email : "";
                    customerIP = account.address != null ? account.address : "";
                    phone = account.phone;
                }
                CreditInfo creditInfo = booking.psgInfo != null ? booking.psgInfo.creditInfo : null;
                if (creditInfo != null) {
                    valid = true;
                    cardType = creditInfo.cardType != null ? creditInfo.cardType : "";
                    cardMask = creditInfo.cardMask != null ? creditInfo.cardMask : "";
                    street = creditInfo.street != null ? creditInfo.street : "";
                    city = creditInfo.city != null ? creditInfo.city : "";
                    state = creditInfo.state != null ? creditInfo.state : "";
                    zipCode = creditInfo.zipCode != null ? creditInfo.zipCode : "";
                    country = creditInfo.country != null ? creditInfo.country : "";
                }
            } else if (cardOwner.equals("mDispatcher")) {
                Account account = mongoDao.getAccount(id);
                if (account != null) {
                    customerEmail = account.email != null ? account.email : "";
                    customerIP = account.address != null ? account.address : "";
                    phone = account.phone;
                }
                CreditInfo creditInfo = booking.mDispatcherInfo != null ? booking.mDispatcherInfo.creditInfo : null;
                if (creditInfo != null) {
                    valid = true;
                    cardType = creditInfo.cardType != null ? creditInfo.cardType : "";
                    cardMask = creditInfo.cardMask != null ? creditInfo.cardMask : "";
                    street = creditInfo.street != null ? creditInfo.street : "";
                    city = creditInfo.city != null ? creditInfo.city : "";
                    state = creditInfo.state != null ? creditInfo.state : "";
                    zipCode = creditInfo.zipCode != null ? creditInfo.zipCode : "";
                    country = creditInfo.country != null ? creditInfo.country : "";
                }
            } else if (cardOwner.equals("corporate")) {
                Corporate corporate = mongoDao.getCorporate(id);
                if (corporate != null && corporate.adminAccount != null) {
                    customerEmail = corporate.adminAccount.email != null ? corporate.adminAccount.email : "";
                    phone = corporate.adminAccount.phone != null ? corporate.adminAccount.phone : "";
                }
                CreditInfo creditInfo = booking.corporateInfo != null ? booking.corporateInfo.creditInfo : null;
                if (creditInfo != null) {
                    valid = true;
                    cardType = creditInfo.cardType != null ? creditInfo.cardType : "";
                    cardMask = creditInfo.cardMask != null ? creditInfo.cardMask : "";
                    street = creditInfo.street != null ? creditInfo.street : "";
                    city = creditInfo.city != null ? creditInfo.city : "";
                    state = creditInfo.state != null ? creditInfo.state : "";
                    zipCode = creditInfo.zipCode != null ? creditInfo.zipCode : "";
                    country = creditInfo.country != null ? creditInfo.country : "";
                }
            }
        } catch(Exception ex) {
            logger.debug(requestId + " - get card info ERROR: " + CommonUtils.getError(ex));
        }
        if (valid) {
            customerData.put("customerEmail", customerEmail);
            customerData.put("customerIP", customerIP);
            customerData.put("phone", phone);
            customerData.put("cardType", cardType);
            customerData.put("cardMask", cardMask);
            customerData.put("street", street);
            customerData.put("city", city);
            customerData.put("state", state);
            customerData.put("zipCode", zipCode);
            customerData.put("country", country);
        }
        return customerData;
    }

    public List<Credit> getListCredit(String cardOwner, String id, String requestId) {
        try {
            if (cardOwner.equals("passenger") || cardOwner.equals("mDispatcher") || cardOwner.equals("driver")) {
                Account account = mongoDao.getAccount(id);
                if (account != null) {
                    return account.credits;
                }

            } else if (cardOwner.equals("corporate")) {
                Corporate corporate = mongoDao.getCorporate(id);
                if (corporate != null && corporate.credits != null && corporate.credits.size() > 0) {
                    return corporate.credits;
                }
            }
        } catch(Exception ex) {
            logger.debug(requestId + " - get card info ERROR: " + CommonUtils.getError(ex));
        }
        return new ArrayList<>();
    }
    public String getCardTypeFromCorporate(String corporateId, String token, String requestId) {
        String cardType = "";
        try {
            Corporate corporate = mongoDao.getCorporate(corporateId);
            if (corporate != null && corporate.credits != null && corporate.credits.size() > 0) {
                for (Credit credit : corporate.credits) {
                    if (credit.localToken.equals(token)) {
                        cardType = credit.cardType;
                        break;
                    }
                }
            }
        } catch(Exception ex) {
            logger.debug(requestId + " - get card info ERROR: " + CommonUtils.getError(ex));
        }
        return cardType;
    }

    public String getCancelAmountForAffiliate(Booking booking, String fleetId, String status, String priceType, String requestId) {
        ObjResponse objResponse = new ObjResponse();
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        Map<String, Object> mapData = new HashMap<>();

        double compensatePolicy = 0;
        double penalizePolicy = 0;
        boolean isNoShow = false;
        FleetFare fleetFare = mongoDao.getFleetFare(booking.fleetId);
        if (fleetFare != null && fleetFare.noShow != null)
            isNoShow = fleetFare.noShow.isActive;

        AffiliateProcess affiliateProcess = mongoDao.findAffiliateProcessTopByOrderByCreatedDateDesc();
        if(booking.rv != null && booking.rv.affiliateProcess != null && !booking.rv.affiliateProcess.isEmpty()){
            affiliateProcess = mongoDao.findAffiliateProcessById(booking.rv.affiliateProcess);
        }
        Calendar cal = Calendar.getInstance();
        Date pickupTime = null;
        Date currentTime = null;
        boolean chargeInAdvance = false;
        boolean fullCharge = false;
        String chargeType = "onDemand";
        String canceller = "";
        try{
            if (booking.time != null) {
                if (booking.time.pickUpTime != null) {
                    pickupTime = TimezoneUtil.offsetTimeZone(booking.time.pickUpTime, cal.getTimeZone().getID(), "GMT");
                    logger.debug(requestId + " - pickupTime: " + pickupTime);
                }
                currentTime = TimezoneUtil.convertServerToGMT(new Date());
                logger.debug(requestId + " - currentTime: " + currentTime);
                if(currentTime.before(pickupTime)){
                    double milliseconds = (double) pickupTime.getTime() - currentTime.getTime();
                    double days = milliseconds / 86400000;
                    logger.debug(requestId + " - cancel before: " + days + " days");
                    if(days < affiliateProcess.chargeInAdvance.value && days > affiliateProcess.fullCharge.value/24){
                        logger.debug(requestId + " - cancel before: " + days + " days - charge InAdvance");
                        chargeInAdvance = true;
                    } else if(days <= affiliateProcess.fullCharge.value/24){
                        logger.debug(requestId + " - cancel before: " + days + " days - full charge");
                        fullCharge = true;
                    } else {
                        logger.debug(requestId + " - cancel before: " + days + " days - free charge");
                        chargeType = "free";
                    }
                    if(booking.status.equals(KeysUtil.canceled) && status.equals(KeysUtil.INCOME_REJECTED)){
                        logger.debug(requestId + " - Supplier rejected booking - free charge");
                        chargeType = "free";
                        chargeInAdvance = false;
                        fullCharge = false;
                    }
                }
            }
        } catch (Exception ex){

        }
        if(booking.cancelInfo != null && booking.cancelInfo.canceller != null)
            canceller = booking.cancelInfo.canceller;
        if (booking.status.equals(KeysUtil.canceled) || booking.status.equals("rejected") || status.equals(KeysUtil.canceled)
                || status.equals(KeysUtil.INCOME_REJECTED)){
            if(booking.reservation && chargeInAdvance)
                chargeType = "inAdvance";
            if(booking.reservation && fullCharge)
                chargeType = "full";
            if(canceller.equals("timeout"))
                chargeType = "timeout";
        }else
            chargeType = "noShow";
        if(booking.reservation && booking.status.equals("waitingPayment") && !chargeInAdvance && !fullCharge)
            chargeType = "free";

        logger.debug(requestId + " - chargeType: " + chargeType);
        if(!chargeType.equals("free")){
            /// GET FARE
            String zoneId = "";
            String fareNormalId = "";
            String vhcTypeId = "";
            String fareHourlyId = "";
            String fareFlatId = "";
            String zipCodeFrom = "";
            String zipCodeTo = "";
            List<Double> pickup = new ArrayList<>();
            List<Double> destination = new ArrayList<>();
            RateGeneral rateGeneral = null;
            AffiliationCarType carType = null;

            if (booking.request != null && booking.request.vehicleTypeRequest != null && !booking.request.vehicleTypeRequest.isEmpty()) {
                carType = mongoDao.getAffiliationCarTypeByName(booking.request.vehicleTypeRequest);
                if(carType!= null)
                    vhcTypeId = carType._id.toString();
            }
            logger.debug(requestId + " - vhcTypeId: "+ vhcTypeId);
            if (booking.request != null && booking.request.pickup != null) {
                zipCodeFrom = booking.request.pickup.zipCode != null ? booking.request.pickup.zipCode : "";
                pickup = booking.request.pickup.geo;
                List<Zone> zones = mongoDao.findByGeoAffiliate(booking.request.pickup.geo, booking.request.psgFleetId, vhcTypeId);
                if(zones!= null && !zones.isEmpty())
                    zoneId = zones.get(0)._id.toString();
                //get zoneId by fleetId
                if(!fleetId.isEmpty()){
                    for (Zone zone: zones){
                        if(zone.fleetId.equals(fleetId))
                            zoneId = zone._id.toString();
                    }
                }
            }
            logger.debug(requestId + " - zoneId: "+ zoneId);

            if (booking.request != null && booking.request.destination != null) {
                zipCodeTo = booking.request.destination.zipCode != null ? booking.request.destination.zipCode : "";
                destination = booking.request.destination.geo;
            }

            if (!vhcTypeId.isEmpty() && !zoneId.isEmpty()) {

                List<AffiliateAssignedRate> listAffiliateAssignedRate = mongoDao.getListAffiliateAssignedRate(vhcTypeId, zoneId);

                AffiliateAssignedRate assignedRateGeneral = listAffiliateAssignedRate.stream()
                        .filter(assignedRate -> assignedRate.type.equalsIgnoreCase(KeysUtil.GENERAL))
                        .findFirst().orElse(null);
                if(assignedRateGeneral!= null)
                    rateGeneral = mongoDao.getAffiliateRateGeneral(assignedRateGeneral.rate.id);

                AffiliateAssignedRate assignedRateNormal = listAffiliateAssignedRate.stream()
                        .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase(fleetId) && assignedRate.type.equalsIgnoreCase(KeysUtil.REGULAR) && assignedRate.priceType.equalsIgnoreCase(priceType))
                        .findFirst().orElse(null);
                if(assignedRateNormal == null){
                    assignedRateNormal = listAffiliateAssignedRate.stream()
                            .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase("") && assignedRate.type.equalsIgnoreCase(KeysUtil.REGULAR) && assignedRate.priceType.equalsIgnoreCase(priceType))
                            .findFirst().orElse(null);
                }
                if(assignedRateNormal!= null){
                    fareNormalId = assignedRateNormal.rate.id;
                }

                AffiliateAssignedRate assignedRateHourly = listAffiliateAssignedRate.stream()
                        .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase(fleetId) && assignedRate.type.equalsIgnoreCase(KeysUtil.HOURLY) && assignedRate.priceType.equalsIgnoreCase(priceType))
                        .findFirst().orElse(null);
                if(assignedRateHourly == null){
                    assignedRateHourly = listAffiliateAssignedRate.stream()
                            .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase("") && assignedRate.type.equalsIgnoreCase(KeysUtil.HOURLY) && assignedRate.priceType.equalsIgnoreCase(priceType))
                            .findFirst().orElse(null);
                }
                if(assignedRateHourly!= null){
                    fareHourlyId = assignedRateHourly.rate.id;
                }
            }
            if(!vhcTypeId.isEmpty()){
                List<AffiliateAssignedRate> listAffiliateFlatRate = mongoDao.findAffiliateFlatRateByCarType(vhcTypeId);
                AffiliateAssignedRate assignedRateFlat = listAffiliateFlatRate.stream()
                        .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase(fleetId) && assignedRate.priceType.equalsIgnoreCase(priceType))
                        .findFirst().orElse(null);
                if(assignedRateFlat == null){
                    assignedRateFlat = listAffiliateFlatRate.stream()
                            .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase("") && assignedRate.priceType.equalsIgnoreCase(priceType))
                            .findFirst().orElse(null);
                }
                if(assignedRateFlat!= null){
                    fareFlatId = assignedRateFlat.rate.id;
                }
            }
            logger.debug(requestId + " - fareNormalId: "+ fareNormalId);
            logger.debug(requestId + " - fareFlatId: "+ fareFlatId);
            logger.debug(requestId + " - fareHourlyId: "+ fareHourlyId);
            double techFeeValue = 0;
            if(rateGeneral!= null){
                //get tech fee
                if(rateGeneral.techFee != null && rateGeneral.techFee.enable != null && rateGeneral.techFee.enable){
                    if(KeysUtil.web_booking.equals(booking.bookFrom))
                        techFeeValue = rateGeneral.techFee.webBooking != null ? rateGeneral.techFee.webBooking: 0;
                    else if(KeysUtil.command_center.equals(booking.bookFrom))
                        techFeeValue = rateGeneral.techFee.commandCenter != null ? rateGeneral.techFee.commandCenter: 0;
                    else if(KeysUtil.dash_board.equals(booking.bookFrom) || KeysUtil.corp_board.equals(booking.bookFrom))
                        techFeeValue = rateGeneral.techFee.bookingDashboard != null ? rateGeneral.techFee.bookingDashboard: 0;
                    else if(!KeysUtil.kiosk.equals(booking.bookFrom) || !KeysUtil.mDispatcher.equals(booking.bookFrom)
                            || !KeysUtil.car_hailing.equals(booking.bookFrom) || !KeysUtil.api.equals(booking.bookFrom))
                        techFeeValue = rateGeneral.techFee.paxApp != null ? rateGeneral.techFee.paxApp: 0;
                }
            }
            logger.debug(requestId + " - techFee: "+ Double.valueOf(decimalFormat.format(techFeeValue)));
            mapData.put("techFee", Double.valueOf(decimalFormat.format(techFeeValue)));

            CancellationPolicy cancellationPolicy = null;
            NoShowPolicy noShowPolicy = null;
            if (booking.request.type == 3 || booking.request.typeRate == 1) {
                AffiliateHourly fareHourly = mongoDao.findAffiliateHourlyById(fareHourlyId);
                if (fareHourly != null){
                    if(!priceType.equals(KeysUtil.BUYPRICE)){
                        cancellationPolicy = fareHourly.cancellationPolicy;
                        noShowPolicy = fareHourly.noShowPolicy;
                    } else {
                        cancellationPolicy = fareHourly.cancellationPolicySupplier;
                        noShowPolicy = fareHourly.noShowPolicySupplier;
                    }
                }
            } else {
                boolean checkFlatFare = false;
                AffiliateFlat fareFlat = mongoDao.findAffiliateFlatById(fareFlatId);
                if (fareFlat != null) {
                    logger.debug(requestId + " - fareFlat: " + fareFlat._id + " - pickup: " + pickup + " - destination: " + destination + " - from: " + zipCodeFrom + " - to: " + zipCodeTo);
                    AffiliateRoute flatRoutes = getFareByFlatRoutesAffiliate(fareFlatId, pickup, destination, zipCodeFrom, zipCodeTo);
                    AffiliateRoute flatRoutesReturn = getFareByFlatRoutesReturnAffiliate(fareFlatId, pickup, destination, zipCodeFrom, zipCodeTo);
                    if (flatRoutes != null) {
                        logger.debug(requestId + " - flatRoutesId: " + flatRoutes._id);
                        checkFlatFare = true;
                    } else if (flatRoutesReturn != null) {
                        logger.debug(requestId + " - flatRoutesId: " + flatRoutesReturn._id);
                        checkFlatFare = true;
                    }
                }
                logger.debug(requestId + " - checkFlatFare: " + checkFlatFare);
                if (checkFlatFare) {
                    if (fareFlat != null){
                        cancellationPolicy = fareFlat.cancellationPolicy;
                        noShowPolicy = fareFlat.noShowPolicy;
                    }
                } else {
                    RateRegular fareNormal = mongoDao.getAffiliateRate(fareNormalId);
                    if (fareNormal != null){
                        cancellationPolicy = fareNormal.cancellationPolicy;
                        noShowPolicy = fareNormal.noShowPolicy;
                    }
                }
            }
            if (booking.status.equals(KeysUtil.canceled) || booking.status.equals("rejected") || status.equals(KeysUtil.canceled)
                    || status.equals(KeysUtil.INCOME_REJECTED)) {
                if(cancellationPolicy != null) {
                    if (booking.reservation) {
                        if(cancellationPolicy.penalize != null && cancellationPolicy.penalize.inAdvance != null &&
                                cancellationPolicy.penalize.inAdvance.isActive){
                            if(chargeInAdvance)
                                penalizePolicy = cancellationPolicy.penalize.inAdvance.beforeAdvanceTime;
                            if(fullCharge)
                                penalizePolicy = cancellationPolicy.penalize.inAdvance.afterAdvanceTime;
                        }
                        if(cancellationPolicy.compensate != null && cancellationPolicy.compensate.inAdvance != null &&
                                cancellationPolicy.compensate.inAdvance.isActive){
                            if(chargeInAdvance)
                                compensatePolicy = cancellationPolicy.compensate.inAdvance.beforeAdvanceTime;
                            if(fullCharge)
                                compensatePolicy = cancellationPolicy.compensate.inAdvance.afterAdvanceTime;
                        }
                    } else {
                        if (cancellationPolicy.penalize.onDemand != null && cancellationPolicy.penalize.onDemand.isActive){
                            penalizePolicy = cancellationPolicy.penalize.onDemand.value;
                        }
                        if (cancellationPolicy.compensate.onDemand != null && cancellationPolicy.compensate.onDemand.isActive){
                            compensatePolicy = cancellationPolicy.compensate.onDemand.value;
                        }
                    }
                }
            } else {
                if(isNoShow && noShowPolicy != null){
                    penalizePolicy = noShowPolicy.penalize;
                    compensatePolicy = noShowPolicy.compensate;
                }
            }
            if(chargeType.equals("timeout")){
                penalizePolicy = 100;
                compensatePolicy = 100;
            }
            if(penalizePolicy == 0 && compensatePolicy == 0){
                if(chargeType.equals("inAdvance") || chargeType.equals("full") || chargeType.equals("noShow")){
                    logger.debug(requestId + " - penalizePolicy = 0 & compensatePolicy = 0 !!");
                    chargeType = "free";
                }
            }

            logger.debug(requestId + " - penalizePolicy: "+ Double.valueOf(decimalFormat.format(penalizePolicy)));
            logger.debug(requestId + " - compensatePolicy: "+ Double.valueOf(decimalFormat.format(compensatePolicy)));
        }
        mapData.put("penalizePolicy", Double.valueOf(decimalFormat.format(penalizePolicy)));
        mapData.put("compensatePolicy", Double.valueOf(decimalFormat.format(compensatePolicy)));
        mapData.put("chargeType", chargeType);

        objResponse.returnCode = 200;
        objResponse.response = mapData;
        return objResponse.toString();
    }


    public void addFailedCard(String fleetId, String cardType, String last4, String requestId) {
        logger.debug(requestId + " - addFailedCard - fleetId: " + fleetId + " - cardType: " + cardType + " - last4: " + last4);
        FailedCard failedCard = new FailedCard();
        failedCard.fleetId = fleetId;
        failedCard.cardId = cardType.toLowerCase() + "_" + last4;

        Calendar calendar = Calendar.getInstance();
        failedCard.createdDate = calendar.getTime();

        // set expire time to auto remove lock record
        int lockDuration = getLockDuration();
        calendar.add(Calendar.MINUTE, lockDuration);
        failedCard.expireAt = calendar.getTime();
        Gson gson = new Gson();
        logger.debug(requestId + " - failedCard: " + gson.toJson(failedCard));
        String result = mongoDao.addFailedCard(failedCard);
        logger.debug(requestId + " - result: " + result);
    }

    public boolean checkFailedCard(Booking booking, String token, String cardOwner, String requestId) {
        logger.debug(requestId + " - checkFailedCard - bookId: " + booking.bookId + " - fleetId: " + booking.fleetId + " - token: " + token + " - cardOwner: " + cardOwner);
        String cardType = "";
        String last4 = "";
        if (cardOwner.equals("passenger") || cardOwner.equals("mDispatcher")) {
            String userId = "";
            if (cardOwner.equals("passenger") && booking.psgInfo != null) {
                userId = booking.psgInfo.userId != null ? booking.psgInfo.userId : "";
            } else if (cardOwner.equals("mDispatcher") && booking.mDispatcherInfo != null) {
                userId = booking.mDispatcherInfo.userId != null ? booking.mDispatcherInfo.userId : "";
            }
            if (!userId.equals("")) {
                Account account = mongoDao.getAccount(userId);
                for (Credit credit : account.credits) {
                    if (token.equals(credit.localToken) || token.equals(credit.crossToken)) {
                        cardType = credit.cardType;
                        String cardMasked = credit.cardMask;
                        last4 = cardMasked.length() > 4 ? cardMasked.substring(cardMasked.length() - 4, cardMasked.length()) : "";
                    }
                }
            }
        } else if (cardOwner.equals("corporate") && booking.corporateInfo != null) {
            String corporateId = booking.corporateInfo.corporateId != null ? booking.corporateInfo.corporateId : "";
            if (!corporateId.equals("")) {
                Corporate corporate = mongoDao.getCorporate(corporateId);
                for (Credit credit : corporate.credits) {
                    if (token.equals(credit.localToken) || token.equals(credit.crossToken)) {
                        cardType = credit.cardType;
                        String cardMasked = credit.cardMask;
                        last4 = cardMasked.length() > 4 ? cardMasked.substring(cardMasked.length() - 4, cardMasked.length()) : "";
                    }
                }
            }
        }

        if (cardType.equalsIgnoreCase("master")) cardType = "mastercard";
        if (cardType.equalsIgnoreCase("american express") || cardType.equalsIgnoreCase("americanexpress")) cardType = "amex";

        // save cardId before payment
        redisDao.lockCard(booking.bookId, booking.fleetId + "---" + cardType.toLowerCase() + "---" + last4, requestId);
        return validCard(booking.fleetId, cardType, last4, requestId);
    }
    public boolean checkFailedCardCorporate(String corporateId, String prepaidId, String token, String requestId) {
        logger.debug(requestId + " - checkFailedCardCorporate - corporateId: " + corporateId + " - prepaidId: " + prepaidId + " - token: " + token);
        String cardType = "";
        String last4 = "";
        if (!corporateId.equals("")) {
            Corporate corporate = mongoDao.getCorporate(corporateId);
            for (Credit credit : corporate.credits) {
                if (token.equals(credit.localToken) || token.equals(credit.crossToken)) {
                    cardType = credit.cardType;
                    String cardMasked = credit.cardMask;
                    last4 = cardMasked.length() > 4 ? cardMasked.substring(cardMasked.length() - 4, cardMasked.length()) : "";
                }
            }
        }

        if (cardType.equalsIgnoreCase("master")) cardType = "mastercard";
        if (cardType.equalsIgnoreCase("american express") || cardType.equalsIgnoreCase("americanexpress")) cardType = "amex";

        // save cardId before payment
        redisDao.lockCard(prepaidId, corporateId + "---" + cardType.toLowerCase() + "---" + last4, requestId);
        return validCard(corporateId, cardType, last4, requestId);
    }
    public boolean checkFailedCard(String bookId, String fleetId, String cardType, String last4, String requestId) {
        logger.debug(requestId + " - checkFailedCard - bookId: " + bookId + " - fleetId: " + fleetId + " - cardType: " + cardType + " - last4: " + last4);
        if (cardType.equalsIgnoreCase("master")) cardType = "mastercard";
        if (cardType.equalsIgnoreCase("american express") || cardType.equalsIgnoreCase("americanexpress")) cardType = "amex";

        // save cardID before payment
        if (!bookId.equals(""))
            redisDao.lockCard(bookId, fleetId + "---" + cardType.toLowerCase() + "---" + last4, requestId);

        return validCard(fleetId, cardType, last4, requestId);
    }

    private boolean validCard(String fleetId, String cardType, String last4, String requestId) {
        long count = mongoDao.countFailedCard(fleetId, cardType.toLowerCase() + "_" + last4);
        boolean limitRetry = isLimitRetry();
        int limitNumber = getLimitNumber();
        boolean valid = !limitRetry || count < limitNumber;
        logger.debug(requestId + " - card valid: " + valid);
        return valid;
    }

    private boolean isLimitRetry() {
        boolean isLimitRetry = RETRY;
        if (!isLimitRetry) {
            List<com.qupworld.paymentgateway.models.mongo.collections.serverInformation.PaymentClearanceHouse> paymentClearanceHouses = mongoDao.getServerInformation().paymentClearanceHouses;
            for (com.qupworld.paymentgateway.models.mongo.collections.serverInformation.PaymentClearanceHouse paymentClearanceHouse : paymentClearanceHouses) {
                if (paymentClearanceHouse.key.equalsIgnoreCase("CREDIT") && paymentClearanceHouse.isActive) {
                    isLimitRetry = paymentClearanceHouse.limitRetry;
                    RETRY = isLimitRetry;
                }
            }
        }
        return isLimitRetry;
    }

    private int getLimitNumber() {
        int limitNumber = LIMIT;
        if (limitNumber == 0) {
            List<com.qupworld.paymentgateway.models.mongo.collections.serverInformation.PaymentClearanceHouse> paymentClearanceHouses = mongoDao.getServerInformation().paymentClearanceHouses;
            for (com.qupworld.paymentgateway.models.mongo.collections.serverInformation.PaymentClearanceHouse paymentClearanceHouse : paymentClearanceHouses) {
                if (paymentClearanceHouse.key.equalsIgnoreCase("CREDIT") && paymentClearanceHouse.isActive) {
                    limitNumber = paymentClearanceHouse.numberOfRetry;
                    LIMIT = limitNumber;
                }
            }
        }
        return limitNumber;
    }

    private int getLockDuration() {
        int lockDuration = DURATION;
        if (lockDuration == 0) {
            List<com.qupworld.paymentgateway.models.mongo.collections.serverInformation.PaymentClearanceHouse> paymentClearanceHouses = mongoDao.getServerInformation().paymentClearanceHouses;
            for (com.qupworld.paymentgateway.models.mongo.collections.serverInformation.PaymentClearanceHouse paymentClearanceHouse : paymentClearanceHouses) {
                if (paymentClearanceHouse.key.equalsIgnoreCase("CREDIT") && paymentClearanceHouse.isActive) {
                    lockDuration = paymentClearanceHouse.lockDuration;
                    DURATION = lockDuration;
                }
            }
        }

        return lockDuration;
    }


    public int addCardToStore(String requestId, final String token, final String userId, final CreditEnt creditEnt) {
        int returnCode = 437;
        try {
            String dataStr = token + gson.toJson(creditEnt);
            String key =  SecurityUtil.getKey();
            String data = SecurityUtil.encryptProfile(key, dataStr);
            LogToken logToken = new LogToken();
            logToken.logId = token;
            logToken.fleetId = creditEnt.fleetId;
            logToken.userId = userId;
            logToken.logData = data;
            mongoDao.addLogToken(logToken);

            // check added log
            LogToken added = mongoDao.getLogToken(creditEnt.fleetId, token);
            if (added != null && added.logData.equals(data)) {
                returnCode = 200;
            } else {
                Thread.sleep(2000);
                mongoDao.addLogToken(logToken);
                LogToken retry = mongoDao.getLogToken(creditEnt.fleetId, token);
                if (retry == null || !retry.logData.equals(data)) {
                    returnCode = 407;
                }
            }

            return returnCode;
        } catch (Exception ex) {
            logger.debug(requestId + " - addCardToStore exception: " + CommonUtils.getError(ex));
        }
        return returnCode;
    }
    public String getCardFromStore(String fleetId, String token, String requestId){
        logger.debug(requestId + " - getCardFromStore ...");
        LogToken logToken = mongoDao.getLogToken(fleetId, token);
        String logData = logToken != null && logToken.logData != null ? logToken.logData : "";
        String dataStr = "";
        if (!logData.isEmpty()) {
            try {
                dataStr = SecurityUtil.decryptProfile(SecurityUtil.getKey(), logData);
                dataStr = dataStr.replaceAll(token, "");
            } catch (Exception ex) {
                com.pg.util.SlackAPI slack = new com.pg.util.SlackAPI();
                slack.sendSlack("GET DATA CREDIT CARD FALSE Exception ", "URGREN", token);
                logger.debug(requestId + " - Exception data: " + CommonUtils.getError(ex));
            }
        } else {
            HttpResponse<JsonNode> jsonResponse = null;
            try {
                String body = "{\"logId\":\"" + token + "\"}";
                jsonResponse = Unirest.post(getReport)
                        .basicAuth(ServerConfig.logs_username, ServerConfig.logs_password)
                        .header("Content-Type", "application/json; charset=utf-8")
                        .body(body)
                        .asJson();
                org.json.JSONObject objectResponse = jsonResponse.getBody().getObject();
                logger.debug(requestId + " - stored data: " + objectResponse);
                if (objectResponse != null && objectResponse.get("returnCode") != null && Integer.parseInt(objectResponse.get("returnCode").toString()) == 200) {
                    dataStr = SecurityUtil.decryptProfile(SecurityUtil.getKey(), objectResponse.get("response").toString());
                    dataStr = dataStr.replaceAll(token, "");
                } else {
                    for (int i = 0; i < 3; i++) {
                        // try to query again
                        Thread.sleep(2000);
                        HttpResponse<JsonNode> jsonResponseTry = Unirest.post(getReport)
                                .basicAuth(ServerConfig.logs_username, ServerConfig.logs_password)
                                .header("Content-Type", "application/json; charset=utf-8")
                                .body(body)
                                .asJson();
                        org.json.JSONObject objectResponseTry = jsonResponseTry.getBody().getObject();
                        logger.debug(requestId + " - stored data: " + objectResponseTry);
                        if (objectResponseTry != null && objectResponseTry.get("returnCode") != null && Integer.parseInt(objectResponseTry.get("returnCode").toString()) == 200) {
                            dataStr = SecurityUtil.decryptProfile(SecurityUtil.getKey(), objectResponseTry.get("response").toString());
                            dataStr = dataStr.replaceAll(token, "");

                            // if success then break the loop
                            break;
                        } else {
                            com.pg.util.SlackAPI slack = new com.pg.util.SlackAPI();
                            slack.sendSlack("GET DATA CREDIT CARD FALSE ", "URGREN", token);
                        }
                        i++;
                    }


                }
            } catch (Exception e) {
                com.pg.util.SlackAPI slack = new SlackAPI();
                slack.sendSlack("GET DATA CREDIT CARD FALSE Exception ", "URGREN", token);
                e.printStackTrace();
                logger.debug(requestId + " - Exception data: " + e.toString());
            }
        }
        return dataStr;
    }

    org.json.JSONObject sendPost(String requestId, String body, String url, String userName, String pw){
        org.json.JSONObject dataStr = new org.json.JSONObject();
        HttpResponse<JsonNode> jsonResponse = null;
        try {
            HttpResponse<String> result = Unirest.post(url)
                    .basicAuth(userName, pw)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("X-Trace-Request-Id", requestId)
                    .header("x-request-id", requestId)
                    .body(body)
                    .asString();
            String responseBody = result.getBody();
            logger.debug(requestId + " - result = " + responseBody);
            dataStr.put("result", responseBody);
        } catch (Exception ex) {
            //e.printStackTrace();
            logger.debug(requestId + " - Exception: " + CommonUtils.getError(ex));
        }
        return dataStr;
    }

    boolean createBooking(String requestId, String body, String url, String userName, String pw){
        boolean success = false;
        try {
            HttpResponse<String> result = Unirest.post(url)
                    .basicAuth(userName, pw)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("X-Trace-Request-Id", requestId)
                    .body(body)
                    .asString();
            String responseBody = result.getBody();
            logger.debug(requestId + " - result = " + responseBody);
            if (!responseBody.isEmpty()) {
                JSONObject objBody = gson.fromJson(responseBody, JSONObject.class);
                success = objBody.get("success") != null && Boolean.parseBoolean(objBody.get("success").toString());
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }
        return success;
    }

    public int updateBookingForAffiliate(String bookId, double totalProvider, String transactionStatus, String requestId){
        try {
            JSONObject js = new JSONObject();
            js.put("bookId", bookId);
            js.put("value", totalProvider);
            js.put("transactionStatus", transactionStatus);
            org.json.JSONObject data = sendPost(requestId, js.toJSONString(), updateBookingToDispatch, ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
            logger.debug(requestId + " - response: " + data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 200;
    }

    public int sendTopupResultToDispatch(String userId, String transactionId, String status, int errorCode, String from, String requestId){
        try {
            JSONObject js = new JSONObject();
            js.put("userId", userId);
            js.put("transactionId", transactionId);
            js.put("status", status);
            js.put("errorCode", errorCode);
            js.put("from", from);
            logger.debug(requestId + " - url: " + sendTopupResultToDispatch);
            logger.debug(requestId + " - body: " + js.toJSONString());
            sendPost(requestId, js.toJSONString(), sendTopupResultToDispatch, ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 200;
    }

    public int notifyUpdateBalanceToDispatch(String requestId, String userId, String walletType, double amount, String currencyISO){
        try {
            JSONObject js = new JSONObject();
            js.put("userId", userId);
            js.put("walletType", walletType);
            js.put("amount", amount);
            js.put("currencyISO", currencyISO);
            logger.debug(requestId + " - url: " + notifyUpdateBalanceToDispatch);
            logger.debug(requestId + " - body: " + js.toJSONString());
            sendPost(requestId, js.toJSONString(), notifyUpdateBalanceToDispatch, ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }
        return 200;
    }

    public int sendTipResultToDispatch(String requestId, String tipResult, String bookId, String customerId){
        try {
            Map<String,Object> resultMap = gson.fromJson(tipResult, Map.class);// tao data de gui qua dispatcher
            JSONObject js = new JSONObject();
            js.put("type", "tip" );
            js.put("bookId", bookId);
            js.put("customerId", customerId);
            js.put("info", resultMap);

            org.json.JSONObject data = sendPost(requestId, js.toJSONString(), sendTipResultToDispatch, ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
            logger.debug(requestId + " - url: " + sendTipResultToDispatch);
            logger.debug(requestId + " - response: " + data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 200;
    }

    public int sendOutStandingResultToDispatch(String requestId, String outstandingResult, String customerId){
        try {
            Map<String,Object> resultMap = gson.fromJson(outstandingResult, Map.class);// tao data de gui qua dispatcher
            JSONObject js = new JSONObject();
            js.put("type", "outstanding" );
            js.put("customerId", customerId);
            js.put("info", resultMap);

            org.json.JSONObject data = sendPost(requestId, js.toJSONString(), sendOutstandingResultToDispatch, ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
            logger.debug(requestId + " - url: " + sendOutstandingResultToDispatch);
            logger.debug(requestId + " - response: " + data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 200;
    }

    int sendPaymentResultToDispatch(String paymentResult, String requestId){
        try {
            org.json.JSONObject data = sendPost(requestId, paymentResult, sendPaymentResultToDispatch, ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
            logger.debug(requestId + " - url: " + sendPaymentResultToDispatch);
            logger.debug(requestId + " - response: " + data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 200;
    }
    int sendPaymentFailedToDispatch(String paymentResult, String requestId){
        try {
            org.json.JSONObject data = sendPost(requestId, paymentResult, sendPaymentFailedToDispatch, ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
            logger.debug(requestId + " - url: " + sendPaymentFailedToDispatch);
            logger.debug(requestId + " - response: " + data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 200;
    }

    public int sendTopupPrepaidResultToDispatch(String fleetId, String client_secret, int returnCode, String requestId){
        try {
            JSONObject js = new JSONObject();
            js.put("fleetId", fleetId);
            js.put("client_secret", client_secret);
            js.put("returnCode", returnCode);

            org.json.JSONObject data = sendPost(requestId, js.toJSONString(), sendTopupResultToDispatch, ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
            logger.debug(requestId + " - url: " + sendTopupResultToDispatch);
            logger.debug(requestId + " - response: " + data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 200;
    }

    public boolean sendChargeIntercityResultToDispatch(String fleetId, String bookId, int returnCode, String requestId){
        boolean success = false;
        try {
            JSONObject info = new JSONObject();
            info.put("returnCode", returnCode);

            JSONObject js = new JSONObject();
            js.put("type", "chargeIntercityBooking");
            js.put("fleetId", fleetId);
            js.put("bookId", bookId);
            js.put("info", info);

            org.json.JSONObject data = sendPost(requestId, js.toJSONString(), sendChargeIntercityResultToDispatch, ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
            logger.debug(requestId + " - url: " + sendChargeIntercityResultToDispatch);
            logger.debug(requestId + " - response: " + data);
            String result = data.getString("result");
            ObjectMapper mapper = new ObjectMapper();
            Map<String, Object> mapData = mapper.readValue(result, Map.class);
            returnCode = mapData.get("returnCode") != null ? Integer.parseInt(mapData.get("returnCode").toString()) : 0;
            if (returnCode == 200) {
                Map<String, Object> mapResponse = (Map<String, Object>) mapData.get("response");
                success = mapResponse.get("success") != null && Boolean.parseBoolean(mapResponse.get("success").toString());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success;
    }

    public int sendRegisterCardResultToCC(JSONObject objectData, String requestId){
        try {
            org.json.JSONObject data = sendPost(requestId, objectData.toJSONString(), sendRegisterCardResultToCC, ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
            logger.debug(requestId + " - url: " + sendRegisterCardResultToCC);
            logger.debug(requestId + " - data: " + objectData);
            logger.debug(requestId + " - response: " + data);
        } catch (Exception ex) {
            logger.debug(requestId + " - response: " + CommonUtils.getError(ex));
        }
        return 200;
    }

    public int sendRegisterCardResultToWB(JSONObject objectData, String userId, String requestId){
        try {
            org.json.JSONObject data = sendPost(requestId, objectData.toJSONString(), sendRegisterCardResultToWB, ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
            logger.debug(requestId + " - url: " + sendRegisterCardResultToWB);
            logger.debug(requestId + " - data: " + objectData);
            logger.debug(requestId + " - userId: " + userId);
            logger.debug(requestId + " - response: " + data);
        } catch (Exception ex) {
            logger.debug(requestId + " - response: " + CommonUtils.getError(ex));
        }
        return 200;
    }

    public JSONObject getPromoCode(String fleetId, String bookId, String promoCode, String currencyISO) {
        JSONObject data = new JSONObject();
        String type = "amount";
        double value = 0;
        double maximumValue = 0;
        boolean keepMinFee = false;
        Fleet fleet = mongoDao.getFleetInfor(fleetId);
        if (promoCode != null && !promoCode.isEmpty() && !currencyISO.isEmpty()) {
            PromotionCode promotionCode = mongoDao.getPromoCodeByFleetAndName(fleetId, promoCode.toUpperCase());
            if(promotionCode != null){
                type = promotionCode.type;
                if(fleet.multiCurrencies && type.equalsIgnoreCase("Amount")){
                    for(AmountByCurrency amountByCurrency: promotionCode.valueByCurrencies){
                        if(amountByCurrency.currencyISO.equals(currencyISO))
                            value = amountByCurrency.value;
                    }
                }else {
                    if (type.equalsIgnoreCase("Amount")) {
                        PromoCodeUse promoCodeUse = sqlDao.getPromoCodeUseByBookId(bookId);
                        value = promoCodeUse != null && promoCodeUse.usedValue != null ? promoCodeUse.usedValue : 0.0;
                    } else {
                        value = promotionCode.valueByCurrencies.get(0).value;
                    }
                }
                //check maximum
                if(type.equals("Percent") && promotionCode.valueLimitPerUse != null && promotionCode.valueLimitPerUse.isLimited){
                    maximumValue = promotionCode.valueLimitPerUse.value;
                }
                keepMinFee = promotionCode.keepMinFee;
            }
        }
        data.put("type", type);
        data.put("value", value);
        data.put("maximumValue", maximumValue);
        data.put("keepMinFee", keepMinFee);
        return data;
    }

    public double getAirportFee (Fleet fleet, String currencyISO, List<Double> pickup, List<Double> destination,
                                 boolean isFromAirport, boolean isRoundTrip){
        // get airport fee
        double airportFee = 0;
        boolean checkDes = false;
        FleetFare fleetFare = mongoDao.getFleetFare(fleet.fleetId);
        AirportZone airportZonePU = mongoDao.checkDestinationByGeo(pickup);
        AirportZone airportZoneDE = mongoDao.checkDestinationByGeo(destination);
        if(airportZoneDE != null)
            checkDes = true;
        if(fleetFare.airport != null){
            if(fleetFare.airport.isCustomized != null && fleetFare.airport.isCustomized){
                if(airportZonePU != null){
                    AirportFee apFee = mongoDao.getAirportFee(fleet.fleetId, airportZonePU._id.toString());
                    if(apFee != null){
                        if(isFromAirport && fleetFare.airport.fromAirportActive && apFee.fromAirportByCurrencies != null) {
                            AmountByCurrency fromAirport = apFee.fromAirportByCurrencies.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                    .findFirst().orElse(null);
                            if(fromAirport != null)
                                airportFee = fromAirport.value;
                        }
                        if(apFee.toAirportActive && fleet.additionalService.toAirport
                                && apFee.toAirportByCurrencies != null){
                            AmountByCurrency toAirport = apFee.toAirportByCurrencies.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                    .findFirst().orElse(null);
                            if(toAirport != null){
                                if(isRoundTrip){
                                    airportFee += toAirport.value;
                                }
                            }
                        }
                    }
                }
                if(airportZoneDE != null){
                    AirportFee apFee = mongoDao.getAirportFee(fleet.fleetId, airportZoneDE._id.toString());
                    if(apFee != null){
                        /*if(isFromAirportAndRoundTrip && apFee.fromAirportActive && apFee.fromAirportByCurrencies != null) {
                            AmountByCurrency fromAirport = apFee.fromAirportByCurrencies.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                    .findFirst().orElse(null);
                            if(fromAirport != null)
                                airportFee = fromAirport.value;
                        }*/
                        if(fleetFare.airport.toAirportActive && fleet.additionalService.toAirport
                                && apFee.toAirportByCurrencies != null){
                            AmountByCurrency toAirport = apFee.toAirportByCurrencies.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                    .findFirst().orElse(null);
                            if(toAirport != null)
                                airportFee += toAirport.value;
                        }
                    }
                }
            }else {
                if(isFromAirport && fleetFare.airport.fromAirportActive && fleetFare.airport.fromAirportByCurrencies != null) {
                    AmountByCurrency fromAirport = fleetFare.airport.fromAirportByCurrencies.stream()
                            .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                            .findFirst().orElse(null);
                    if(fromAirport != null)
                        airportFee = fromAirport.value;
                }
                if(fleetFare.airport.toAirportActive && fleet.additionalService.toAirport
                        && fleetFare.airport.toAirportByCurrencies != null){
                    AmountByCurrency toAirport = fleetFare.airport.toAirportByCurrencies.stream()
                            .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                            .findFirst().orElse(null);
                    if(toAirport != null){
                        if(checkDes){
                            airportFee += toAirport.value;
                        }
                        if(isRoundTrip && isFromAirport){
                            airportFee += toAirport.value;
                        }
                    }
                }
            }
        }
        return airportFee;
    }

    public List<Object> getBasicFare(int bookType, int typeRate, double durationTime, double distance, int numOfDays,
                                     String bookingType, List<Double> pickup, List<Double> destination, String zipCodeFrom,
                                     String zipCodeTo, String packageRateId, String fareNormalId, String fareFlatId, String currencyISO,
                                     String requestId){
        List<Object> data = new ArrayList<Object>();
        double basicFare = 0;
        boolean normalFare = false;
        String route = "";
        String routeId = "";
        boolean reverseRoute = false;
        double minimum = 0;
        double extraDistanceFare = 0;
        double extraDurationFare = 0;
        // query fare
        FareNormal fareNormal = mongoDao.getFareNormalById(fareNormalId);
        FlatRoutes flatRoutes = getFareByFlatRoutes(fareFlatId, pickup, destination, zipCodeFrom, zipCodeTo);
        FlatRoutes flatRoutesReturn = getFareByFlatRoutesReturn(fareFlatId, pickup, destination, zipCodeFrom, zipCodeTo);
        FlatRoutes flatRoutesRoundTrip = getFareByFlatRoutesRoundTrip(fareFlatId, pickup, destination, zipCodeFrom, zipCodeTo);

        //check round trip
        if(bookType == 4 || typeRate == 2){
            if(flatRoutesRoundTrip != null){
                List<Object> dataBaseFee = getBaseFeeFlat(durationTime, distance, flatRoutesRoundTrip, false, true, currencyISO, requestId);
                basicFare = Double.valueOf(dataBaseFee.get(0).toString());
                route = dataBaseFee.get(1).toString();
                routeId = dataBaseFee.get(2).toString();
            }
            logger.debug(requestId + " - basicFare by flat fare with round trip: " + basicFare);
        }else if(bookType == 3 || typeRate == 1){
            List<Object> dataBaseFee = getBaseFeeHourly(durationTime, distance, numOfDays, packageRateId, currencyISO, requestId);
            basicFare = Double.valueOf(dataBaseFee.get(0).toString());
            extraDistanceFare = Double.valueOf(dataBaseFee.get(1).toString());
            extraDurationFare = Double.valueOf(dataBaseFee.get(2).toString());
        }else{
            //check flat fare
            if(flatRoutes != null){
                List<Object> dataBaseFee = getBaseFeeFlat(durationTime, distance, flatRoutes, false, false, currencyISO, requestId);
                basicFare = Double.valueOf(dataBaseFee.get(0).toString());
                route = dataBaseFee.get(1).toString();
                routeId = dataBaseFee.get(2).toString();
            }else if(flatRoutesReturn != null){
                List<Object> dataBaseFee = getBaseFeeFlat(durationTime, distance, flatRoutesReturn, true, false, currencyISO, requestId);
                basicFare = Double.valueOf(dataBaseFee.get(0).toString());
                route = dataBaseFee.get(1).toString();
                routeId = dataBaseFee.get(2).toString();
                reverseRoute = true;
            }
            logger.debug(requestId + " - basicFare by flat fare: " + basicFare);
        }
        if(basicFare == 0){
            if(fareNormal != null){
                normalFare = true;
                if(fareNormal.feesByCurrencies!= null){
                    for(FeesByCurrency feesByCurrency: fareNormal.feesByCurrencies){
                        if(feesByCurrency.currencyISO.equals(currencyISO)){
                            //get minimum
                            if(bookingType.equalsIgnoreCase(KeysUtil.TIME_BOOK_NOW)){
                                minimum = feesByCurrency.minNow;
                            }else{
                                minimum = feesByCurrency.minReservation;
                            }
                            //get fare normal
                            if(distance >= fareNormal.firstDistanceFrom) {
                                if(distance <=fareNormal.firstDistanceTo){
                                    /** distance between from -- to */
                                    basicFare += (distance - fareNormal.firstDistanceFrom)* feesByCurrency.feeFirstDistance;
                                }else if(distance >= fareNormal.secondDistanceFrom){
                                    if(distance <= fareNormal.secondDistanceTo){
                                        basicFare += ((fareNormal.firstDistanceTo-fareNormal.firstDistanceFrom)*feesByCurrency.feeFirstDistance)
                                                +((distance- fareNormal.secondDistanceFrom)* feesByCurrency.feeSecondDistance);
                                    }else{
                                        /** distance > to */
                                        basicFare += (((fareNormal.firstDistanceTo - fareNormal.firstDistanceFrom)* feesByCurrency.feeFirstDistance)
                                                +((fareNormal.secondDistanceTo - fareNormal.secondDistanceFrom)* feesByCurrency.feeSecondDistance)
                                                + ((distance - fareNormal.secondDistanceTo )* feesByCurrency.feeAfterSecondDistance));
                                    }
                                }
                            }
                            basicFare += (feesByCurrency.feePerMinute * durationTime)/60;
                            if (bookingType.equalsIgnoreCase(KeysUtil.TIME_BOOK_NOW) || bookingType.equalsIgnoreCase(KeysUtil.TIME_BOOK_ASAP)) {
                                basicFare += feesByCurrency.startingNow;
                            } else {
                                basicFare += feesByCurrency.startingReservation;
                            }
                        }
                    }
                }
            }
        }
        data.add(basicFare);
        data.add(normalFare);
        data.add(route);
        data.add(minimum);
        data.add(extraDistanceFare);
        data.add(extraDurationFare);
        data.add(routeId);
        data.add(reverseRoute);
        return data;
    }

    public List<Object> getBaseFeeHourly(double durationTime, double distance, int numOfDays, String packageRateId,
                                   String currencyISO, String requestId){
        List<Object> data = new ArrayList<Object>();
        double basicFare = 0;
        double extraDistanceFare = 0;
        double extraDurationFare = 0;
        durationTime = durationTime/3600;
        logger.debug(requestId + " - durationTime: " + durationTime);
        logger.debug(requestId + " - numOfDays: " + numOfDays);
        PackageRate packageRate = mongoDao.getPackageRateById(packageRateId);
        if(packageRate != null && packageRate.feesByCurrencies != null && !packageRate.feesByCurrencies.isEmpty()){
            double coveredDistance = packageRate.coveredDistance;
            for(FeesWithCurrency feesByCurrency: packageRate.feesByCurrencies){
                if(feesByCurrency.currencyISO.equals(currencyISO)){
                    basicFare = feesByCurrency.basedFee;
                    logger.debug(requestId + " - basicFare by fare hourly: " + basicFare);
                    if(distance > coveredDistance){
                        extraDistanceFare = Math.ceil(distance - coveredDistance) * feesByCurrency.extraDistance;
                        basicFare = basicFare + extraDistanceFare;
                        logger.debug(requestId + " - basicFare by fare hourly with extra distance: " + basicFare);
                    }
                    double duration= 0;
                    if(packageRate.type.equals("hour")){
                        duration = durationTime;
                    }else {
                        duration = numOfDays;
                    }
                    logger.debug(requestId + " - real duration: " + duration);
                    if(duration > packageRate.duration){
                        extraDurationFare = Math.ceil(duration - packageRate.duration) * feesByCurrency.extraDuration;
                        basicFare = basicFare + extraDurationFare;
                        logger.debug(requestId + " - basicFare by fare hourly with extra duration: " + basicFare);
                    }
                }
            }
        }
        data.add(basicFare);
        data.add(extraDistanceFare);
        data.add(extraDurationFare);
        return data;
    }

    public List<Object> getBaseFeeFlat(double durationTime, double distance, FlatRoutes flatRoutes, boolean reverseRoute,
            boolean roundTrip, String currencyISO, String requestId){
        List<Object> data = new ArrayList<Object>();
        double basicFare = 0;
        String route = "";
        String routeId = "";
        if(flatRoutes.singleTrip.departureRouteByCurrencies!= null && !reverseRoute){
            for(AmountByCurrency amountByCurrency: flatRoutes.singleTrip.departureRouteByCurrencies){
                if(amountByCurrency.currencyISO.equals(currencyISO)){
                    basicFare = amountByCurrency.value;
                    route = flatRoutes.routeName;
                    routeId = flatRoutes._id.toString();
                }
            }
        } else if(flatRoutes.singleTrip.returnRouteByCurrencies!= null){
            for(AmountByCurrency amountByCurrency: flatRoutes.singleTrip.returnRouteByCurrencies){
                if(amountByCurrency.currencyISO.equals(currencyISO)){
                    basicFare = amountByCurrency.value;
                    route = flatRoutes.routeName;
                    routeId = flatRoutes._id.toString();
                }
            }
        }
        if(flatRoutes.singleTrip.limitation.limited){
            double coveredDistance = flatRoutes.singleTrip.limitation.coveredDistance;
            double coveredTime= flatRoutes.singleTrip.limitation.coveredTime;
            if(distance > coveredDistance && flatRoutes.singleTrip.limitation.extraDistanceByCurrencies != null){
                for(AmountByCurrency amountByCurrency: flatRoutes.singleTrip.limitation.extraDistanceByCurrencies){
                    if(amountByCurrency.currencyISO.equals(currencyISO)){
                        basicFare = basicFare + Math.ceil(distance - coveredDistance) * amountByCurrency.value;
                        logger.debug(requestId + " - extra distance fee: " + basicFare);
                    }
                }
            }
            double duration = durationTime/3600;
            if(duration > coveredTime && flatRoutes.singleTrip.limitation.extraTimeByCurrencies != null){
                for(AmountByCurrency amountByCurrency: flatRoutes.singleTrip.limitation.extraTimeByCurrencies){
                    if(amountByCurrency.currencyISO.equals(currencyISO)){
                        basicFare = basicFare + Math.ceil(duration - coveredTime) * amountByCurrency.value;
                        logger.debug(requestId + " - extra time fee: " + basicFare);
                    }
                }
            }
        }
        if(roundTrip){
            if(flatRoutes.roundTrip.roundTripFeeByCurrencies!= null){
                for(AmountByCurrency amountByCurrency: flatRoutes.roundTrip.roundTripFeeByCurrencies){
                    if(amountByCurrency.currencyISO.equals(currencyISO)){
                        basicFare = amountByCurrency.value;
                        route = flatRoutes.routeName;
                        routeId = flatRoutes._id.toString();
                    }
                }
            }
            if(flatRoutes.roundTrip.limitation.limited){
                double coveredDistance = flatRoutes.roundTrip.limitation.coveredDistance;
                double coveredTime= flatRoutes.roundTrip.limitation.coveredTime;
                if(distance > coveredDistance && flatRoutes.roundTrip.limitation.extraDistanceByCurrencies != null){
                    for(AmountByCurrency amountByCurrency: flatRoutes.roundTrip.limitation.extraDistanceByCurrencies){
                        if(amountByCurrency.currencyISO.equals(currencyISO)){
                            basicFare = basicFare + Math.ceil(distance - coveredDistance) * amountByCurrency.value;
                            logger.debug(requestId + " - extra distance fee: " + basicFare);
                        }
                    }
                }
                double duration = durationTime/3600;
                if(duration > coveredTime&& flatRoutes.roundTrip.limitation.extraTimeByCurrencies != null){
                    for(AmountByCurrency amountByCurrency: flatRoutes.roundTrip.limitation.extraTimeByCurrencies){
                        if(amountByCurrency.currencyISO.equals(currencyISO)){
                            basicFare = basicFare + Math.ceil(duration - coveredTime) * amountByCurrency.value;
                            logger.debug(requestId + " - extra time fee: " + basicFare);
                        }
                    }
                }
            }
        }
        data.add(basicFare);
        data.add(route);
        data.add(routeId);
        return data;
    }

    public List<Object> getBasicFareAffiliate(int numOfDays,ETAFareEnt etaFareEnt, String fareNormalId,
                                              String fareFlatId, String bookingType, String priceType,  String requestId){
        List<Object> data = new ArrayList<Object>();
        double basicFare = 0;
        boolean normalFare = false;
        String route = "";
        String routeId = "";
        double minimum = 0;
        double distanceByMile = ConvertUtil.round((etaFareEnt.distance / 1609.344), 2);
        double distanceByKm = ConvertUtil.round((etaFareEnt.distance / 1000), 2);
        logger.debug(requestId + " - distance by mile: " + distanceByMile);
        logger.debug(requestId + " - distance by km: " + distanceByKm);

        // query fare
        RateRegular fareNormal = mongoDao.getAffiliateRate(fareNormalId);
        AffiliatePackages packageRate = mongoDao.getPackageRateByIdAffiliate(etaFareEnt.packageRateId);
        AffiliateRoute flatRoutes = getFareByFlatRoutesAffiliate(fareFlatId, etaFareEnt.pickup, etaFareEnt.destination,
                etaFareEnt.zipCodeFrom, etaFareEnt.zipCodeTo);
        AffiliateRoute flatRoutesReturn = getFareByFlatRoutesReturnAffiliate(fareFlatId, etaFareEnt.pickup, etaFareEnt.destination,
                etaFareEnt.zipCodeFrom, etaFareEnt.zipCodeTo);
        AffiliateRoute flatRoutesRoundTrip = getFareByFlatRoutesRoundTripAffiliate(fareFlatId, etaFareEnt.pickup, etaFareEnt.destination,
                etaFareEnt.zipCodeFrom, etaFareEnt.zipCodeTo);

        //check round trip
        if(etaFareEnt.bookType == 4 || etaFareEnt.typeRate == 2){
            if(flatRoutesRoundTrip != null && flatRoutesRoundTrip.roundTrip!= null){
                basicFare = flatRoutesRoundTrip.roundTrip.roundTripFee;
                route = flatRoutesRoundTrip.routeInfo.name;
                routeId = flatRoutesRoundTrip._id.toString();
                if(flatRoutesRoundTrip.roundTrip.limitation.limited){
                    double coveredDistance = flatRoutesRoundTrip.roundTrip.limitation.coveredDistance;
                    String unitDistance = flatRoutesRoundTrip.roundTrip.limitation.unitDistance;
                    double coveredTime = flatRoutesRoundTrip.roundTrip.limitation.coveredTime;
                    if(unitDistance.equals("km")){
                        if(distanceByKm > coveredDistance){
                            basicFare = basicFare + Math.ceil(distanceByKm - coveredDistance) * flatRoutesRoundTrip.roundTrip.limitation.extraDistance;
                        }
                    }else {
                        coveredDistance = coveredDistance / 1.609344;
                        if(distanceByMile > coveredDistance){
                            double extraDistance = flatRoutesRoundTrip.roundTrip.limitation.extraDistance * 1.609344;
                            logger.debug(requestId + " - covered distance: " + coveredDistance);
                            logger.debug(requestId + " - extra distance: " + extraDistance);
                            basicFare = basicFare + Math.ceil(distanceByMile - coveredDistance) * extraDistance;
                        }
                    }
                    logger.debug(requestId + " - extra distance fee: " + basicFare);
                    double duration = etaFareEnt.duration/3600;
                    if(duration > coveredTime){
                        basicFare = basicFare + Math.ceil(duration - coveredTime) * flatRoutesRoundTrip.roundTrip.limitation.extraTime;
                        logger.debug(requestId + " - extra time fee: " + basicFare);
                    }
                }
            }
            logger.debug(requestId + " - basicFare by flat fare with round trip: " + basicFare);
        }else if(etaFareEnt.bookType == 3 || etaFareEnt.typeRate == 1){
            if(packageRate != null){
                double coveredDistance = packageRate.distanceCovered;
                String unitDistance = packageRate.unitDistance;
                double extraDistance = 0;
                double extraDuration = 0;
                if(priceType.equals(KeysUtil.SELLPRICE)){
                    logger.debug(requestId + " -  get sell price");
                    basicFare = packageRate.sellPrice.basedFee;
                    extraDistance = packageRate.sellPrice.extraDistance;
                    extraDuration = packageRate.sellPrice.extraDuration;
                } else {
                    logger.debug(requestId + " -  get buy price");
                    basicFare = packageRate.buyPrice.basedFee;
                    extraDistance = packageRate.buyPrice.extraDistance;
                    extraDuration = packageRate.buyPrice.extraDuration;
                }
                logger.debug(requestId + " - basicFare by fare hourly: " + basicFare);
                if(unitDistance.equals("km")){
                    if(distanceByKm > coveredDistance){
                        basicFare = basicFare + Math.ceil(distanceByKm - coveredDistance) * extraDistance;
                    }
                }else {
                    coveredDistance = coveredDistance / 1.609344;
                    logger.debug(requestId + " - covered distance: " + coveredDistance);
                    if(distanceByMile > coveredDistance){
                        extraDistance = extraDistance * 1.609344;
                        logger.debug(requestId + " - extra distance: " + extraDistance);
                        basicFare = basicFare + Math.ceil(distanceByMile - coveredDistance) * extraDistance;
                    }
                }
                logger.debug(requestId + " - basicFare by fare hourly with extra distance: " + basicFare);
                double duration= 0;
                if(packageRate.type.equals("hour")){
                    duration = etaFareEnt.duration/3600;
                }else {
                    duration = numOfDays;
                }
                logger.debug(requestId + " - real duration: " + duration);
                if(duration > packageRate.duration){
                    basicFare = basicFare + Math.ceil(duration - packageRate.duration) * extraDuration;
                    logger.debug(requestId + " - basicFare by fare hourly with extra duration: " + basicFare);
                }
            }
        }else{
            //check flat fare
            if(flatRoutes != null && flatRoutes.singleTrip!= null){
                basicFare = flatRoutes.singleTrip.departureRoute;
                route = flatRoutes.routeInfo.name;
                routeId = flatRoutes._id.toString();
                if(flatRoutes.singleTrip.limitation.limited){
                    double coveredDistance = flatRoutes.singleTrip.limitation.coveredDistance;
                    double coveredTime = flatRoutes.singleTrip.limitation.coveredTime;
                    String unitDistance = flatRoutes.singleTrip.limitation.unitDistance;
                    if(unitDistance.equals("km")){
                        if(distanceByKm > coveredDistance){
                            basicFare = basicFare + Math.ceil(distanceByKm - coveredDistance) * flatRoutes.singleTrip.limitation.extraDistance;
                        }
                    }else {
                        if(distanceByMile > coveredDistance){
                            coveredDistance = coveredDistance / 1.609344;
                            double extraDistance = flatRoutes.singleTrip.limitation.extraDistance * 1.609344;
                            logger.debug(requestId + " - covered distance: " + coveredDistance);
                            logger.debug(requestId + " - extra distance: " + extraDistance);
                            basicFare = basicFare + Math.ceil(distanceByMile - coveredDistance) * extraDistance;
                        }
                    }
                    logger.debug(requestId + " - extra distance fee: " + basicFare);
                    double duration = etaFareEnt.duration/3600;
                    if(duration > coveredTime){
                        basicFare = basicFare + Math.ceil(duration - coveredTime) * flatRoutes.singleTrip.limitation.extraTime;
                        logger.debug(requestId + " - extra time fee: " + basicFare);
                    }
                }
            }else if(flatRoutesReturn != null && flatRoutesReturn.singleTrip != null){
                basicFare = flatRoutesReturn.singleTrip.returnRoute;
                route = flatRoutesReturn.routeInfo.name;
                routeId = flatRoutesReturn._id.toString();
                if(flatRoutesReturn.singleTrip.limitation.limited){
                    double coveredDistance = flatRoutesReturn.singleTrip.limitation.coveredDistance;
                    double coveredTime= flatRoutesReturn.singleTrip.limitation.coveredTime;
                    String unitDistance = flatRoutesReturn.singleTrip.limitation.unitDistance;
                    if(unitDistance.equals("km")){
                        if(distanceByKm > coveredDistance){
                            basicFare = basicFare + Math.ceil(distanceByKm - coveredDistance) * flatRoutesReturn.singleTrip.limitation.extraDistance;
                        }
                    }else {
                        if(distanceByMile > coveredDistance){
                            coveredDistance = coveredDistance / 1.609344;
                            double extraDistance = flatRoutesReturn.singleTrip.limitation.extraDistance * 1.609344;
                            logger.debug(requestId + " - covered distance: " + coveredDistance);
                            logger.debug(requestId + " - extra distance: " + extraDistance);
                            basicFare = basicFare + Math.ceil(distanceByMile - coveredDistance) * extraDistance;
                        }
                    }
                    logger.debug(requestId + " - extra distance fee: " + basicFare);
                    double duration = etaFareEnt.duration/3600;
                    if(duration > coveredTime){
                        basicFare = basicFare + Math.ceil(duration - coveredTime) * flatRoutesReturn.singleTrip.limitation.extraTime;
                        logger.debug(requestId + " - extra time fee: " + basicFare);
                    }
                }
            }
            logger.debug(requestId + " - basicFare by flat fare: " + basicFare);
            if(basicFare == 0){
                if(fareNormal != null){
                    normalFare = true;
                    //get minimum
                    if(fareNormal.minimum!= null){
                        if(bookingType.equalsIgnoreCase(KeysUtil.TIME_BOOK_NOW) || bookingType.equalsIgnoreCase(KeysUtil.TIME_BOOK_ASAP))
                            minimum = fareNormal.minimum.now!= null ? fareNormal.minimum.now :0;
                        else
                            minimum = fareNormal.minimum.reservation!= null ? fareNormal.minimum.reservation :0;
                    }
                    //get fare normal
                    double firstDistanceFrom = 0;
                    double firstDistanceTo = 0;
                    double feeFirstDistance = 0;
                    double secondDistanceFrom = 0;
                    double secondDistanceTo = 0;
                    double feeSecondDistance = 0;
                    double feeAfterSecondDistance = fareNormal.afterSecondDistance.feeAfterSecondDistance!= null ? fareNormal.afterSecondDistance.feeAfterSecondDistance:0;
                    if(fareNormal.firstDistance!= null){
                        firstDistanceFrom = fareNormal.firstDistance.from != null ? fareNormal.firstDistance.from: 0;
                        firstDistanceTo = fareNormal.firstDistance.to != null ? fareNormal.firstDistance.to: 0;
                        feeFirstDistance = fareNormal.firstDistance.fee != null ? fareNormal.firstDistance.fee: 0;
                    }
                    if(fareNormal.secondDistance!= null){
                        secondDistanceFrom = fareNormal.secondDistance.from != null ? fareNormal.secondDistance.from: 0;
                        secondDistanceTo = fareNormal.secondDistance.to != null ? fareNormal.secondDistance.to: 0;
                        feeSecondDistance = fareNormal.secondDistance.fee != null ? fareNormal.secondDistance.fee: 0;
                    }
                    if(distanceByKm >= firstDistanceFrom) {
                        if(distanceByKm <= firstDistanceTo){
                            /** distance between from -- to */
                            basicFare += (distanceByKm - firstDistanceFrom)* feeFirstDistance;
                        }else if(distanceByKm >= secondDistanceFrom){
                            if(distanceByKm <= secondDistanceTo){
                                basicFare += ((firstDistanceTo-firstDistanceFrom)*feeFirstDistance)
                                        +((distanceByKm- secondDistanceFrom)* feeSecondDistance);
                            }else{
                                /** distance > to */
                                basicFare += (((firstDistanceTo - firstDistanceFrom)* feeFirstDistance)
                                        +((secondDistanceTo - secondDistanceFrom)* feeSecondDistance)
                                        + ((distanceByKm - secondDistanceTo )* feeAfterSecondDistance));
                            }
                        }
                    }
                    if(fareNormal.perMinute.feePerMinute!= null){
                        basicFare += (fareNormal.perMinute.feePerMinute * etaFareEnt.duration)/60;
                    }
                    if(fareNormal.starting!= null){
                        if (bookingType.equalsIgnoreCase(KeysUtil.TIME_BOOK_NOW) || bookingType.equalsIgnoreCase(KeysUtil.TIME_BOOK_ASAP))
                            basicFare += fareNormal.starting.now != null ? fareNormal.starting.now:0;
                        else
                            basicFare += fareNormal.starting.reservation != null ? fareNormal.starting.reservation:0;
                    }
                }
            }
        }
        data.add(basicFare);
        data.add(normalFare);
        data.add(route);
        data.add(minimum);
        data.add(routeId);
        return data;
    }


    public AffiliateRoute getFareByFlatRoutesAffiliate(String fareFlatId, List<Double> pickup, List<Double> destination, String zipCodeFrom, String zipCodeTo){
        AffiliateRoute route = mongoDao.getByGeoAffiliate(fareFlatId, pickup, destination);
        if(route != null)
            return route;
        route = mongoDao.getByZipCodeAffiliate(fareFlatId, zipCodeFrom, zipCodeTo);
        if(route != null)
            return route;
        return null;
    }

    public AffiliateRoute getFareByFlatRoutesReturnAffiliate(String fareFlatId, List<Double> pickup, List<Double> destination, String zipCodeFrom, String zipCodeTo){
        AffiliateRoute route = mongoDao.getByGeoReturnAffiliate(fareFlatId, pickup, destination);
        if(route != null)
            return route;
        route = mongoDao.getByZipCodeReturnAffiliate(fareFlatId, zipCodeFrom, zipCodeTo);
        if(route != null)
            return route;
        return null;
    }

    public AffiliateRoute getFareByFlatRoutesRoundTripAffiliate(String fareFlatId, List<Double> pickup, List<Double> destination, String zipCodeFrom, String zipCodeTo){
        AffiliateRoute route = mongoDao.getByGeoRoundTripAffiliate(fareFlatId, pickup, destination);
        if(route != null)
            return route;
        route = mongoDao.getByGeoRoundTripReturnAffiliate(fareFlatId, pickup, destination);
        if(route != null)
            return route;
        route = mongoDao.getByZipCodeRoundTripAffiliate(fareFlatId, zipCodeFrom, zipCodeTo);
        if(route != null)
            return route;
        route = mongoDao.getByZipCodeRoundTripReturnAffiliate(fareFlatId, zipCodeFrom, zipCodeTo);
        if(route != null)
            return route;
        return null;
    }

    public FlatRoutes getFareByFlatRoutes(String fareFlatId, List<Double> pickup, List<Double> destination, String zipCodeFrom, String zipCodeTo){
        FlatRoutes flatRoutes = mongoDao.getByGeo(fareFlatId, pickup, destination);
        if(flatRoutes != null)
            return flatRoutes;
        flatRoutes = mongoDao.getByZipCode(fareFlatId, zipCodeFrom, zipCodeTo);
        if(flatRoutes != null)
            return flatRoutes;
        return null;
    }

    public FlatRoutes getFareByFlatRoutesReturn(String fareFlatId, List<Double> pickup, List<Double> destination, String zipCodeFrom, String zipCodeTo){
        FlatRoutes flatRoutes = mongoDao.getByGeoReturn(fareFlatId, pickup, destination);
        if(flatRoutes != null)
            return flatRoutes;
        flatRoutes = mongoDao.getByZipCodeReturn(fareFlatId, zipCodeFrom, zipCodeTo);
        if(flatRoutes != null)
            return flatRoutes;
        return null;
    }

    public FlatRoutes getFareByFlatRoutesRoundTrip(String fareFlatId, List<Double> pickup, List<Double> destination, String zipCodeFrom, String zipCodeTo){
        FlatRoutes flatRoutes = mongoDao.getByGeoRoundTrip(fareFlatId, pickup, destination);
        if(flatRoutes != null)
            return flatRoutes;
        flatRoutes = mongoDao.getByGeoRoundTripReturn(fareFlatId, pickup, destination);
        if(flatRoutes != null)
            return flatRoutes;
        flatRoutes = mongoDao.getByZipCodeRoundTrip(fareFlatId, zipCodeFrom, zipCodeTo);
        if(flatRoutes != null)
            return flatRoutes;
        flatRoutes = mongoDao.getByZipCodeRoundTripReturn(fareFlatId, zipCodeFrom, zipCodeTo);
        if(flatRoutes != null)
            return flatRoutes;
        return null;
    }

    public String get3DSData(String type, String bookId, Fleet fleet, String gateway, Map<String,String> mapData, String customerId, String customerPhone,
                             String currencyISO, String requestId, CreditEnt creditEnt) throws IOException {
        ObjResponse createTokenResponse = new ObjResponse();
        String result = "";
        switch (gateway) {
            case KeysUtil.PAYFORT: {
                //Get gateway Information
                GatewayPayfort gatewayFleet = mongoDao.getGatewayPayfortFleet(fleet.fleetId);
                GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.PAYFORT, gatewayFleet.environment);
                if (gatewayFleet.merchantIdentifier.equals("") || gatewayFleet.accessCode.equals("") || gatewayFleet.requestPhrase.equals("")) {
                    createTokenResponse.returnCode = 415;
                    return createTokenResponse.toString();
                } else {
                    PayfortUtil payfortUtil = new PayfortUtil(requestId, gatewayFleet.merchantIdentifier, gatewayFleet.accessCode, gatewayFleet.shaType, gatewayFleet.requestPhrase, gatewayFleet.responsePhrase, gatewayURL.url, gatewayURL.payURL);
                    // create token
                    String resultCreateToken = payfortUtil.createCreditToken(requestId, mapData.get("cardNumber"), mapData.get("expiredDate"), mapData.get("cvv"), customerId);

                    ObjectMapper mapperResult = new ObjectMapper();
                    Map<String, Object> mapResult = mapperResult.readValue(resultCreateToken, new TypeReference<HashMap<String, Object>>() {
                    });

                    createTokenResponse.returnCode = Integer.parseInt(mapResult.get("returnCode").toString());
                    String token = "";
                    if (createTokenResponse.returnCode == 200) {// add token for fleet successful
                        Map<String, String> mapResponse = (Map<String, String>) mapResult.get("response");
                        token = mapResponse.get("token");
                    }else {
                        String card = mapData.get("cardNumber") != null ? mapData.get("cardNumber") : "";
                        String log = gson.toJson(mapData);
                        if (card.length() > 4) {
                            log = log.replace(card, "XXXXXXXXXXXX" + (card.length() - 4));
                        }
                        logger.debug(requestId + " - get3DSUrl error: " + resultCreateToken + " - data: " + log);
                        return createTokenResponse.toString();
                    }
                    if (!token.equals("")) {
                        String method = type.equals("addToken") ? "AUTHORIZATION" : "PURCHASE";
                        logger.debug(requestId + " - create token passed, submit "+ method +" 3DS transaction !!!!!!!!!!");
                        // after create token, do not return token, submit a transaction to verify the token with 3DS
                        String customerEmail = "";
                        String customerIP = "127.0.0.1";
                        Account account = mongoDao.getAccount(mapData.get("userId"));
                        if (account != null) {
                            customerEmail = account.email != null ? account.email : "";
                            customerIP = account.address != null ? account.address : "";
                            if (customerIP.equals(""))
                                customerIP = "127.0.0.1";
                        }
                        double amount = type.equals("addToken") ? 1.00 : Double.parseDouble(mapData.get("total"));
                        String device_fingerprint = "";
                        if (method.equals("PURCHASE") && gatewayFleet.enableRED) {
                            device_fingerprint = "";
                            String browserboxURL = ServerConfig.webview_url + "/browserbox/payfort";
                            logger.debug(requestId + " - browserboxURL: " + browserboxURL);
                            String data = "{}";
                            HttpResponse<JsonNode> jsonResponse = null;
                            try {
                                jsonResponse = Unirest.put(browserboxURL)
                                        //.basicAuth(ServerConfig.logs_username, ServerConfig.logs_password)
                                        .header("Content-Type", "application/json; charset=utf-8")
                                        .body(data)
                                        .asJson();
                                org.json.JSONObject response = jsonResponse.getBody().getObject();
                                device_fingerprint = (response != null && response.has("device_fingerprint")) ? response.getString("device_fingerprint") : "'";
                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                                // try to get data again after 5 seconds
                                try {
                                    Thread.sleep(5000);
                                    jsonResponse = null;
                                    jsonResponse = Unirest.put(browserboxURL)
                                            //.basicAuth(ServerConfig.logs_username, ServerConfig.logs_password)
                                            .header("Content-Type", "application/json; charset=utf-8")
                                            .body(data)
                                            .asJson();
                                    org.json.JSONObject response = jsonResponse.getBody().getObject();
                                    device_fingerprint = (response != null && response.has("device_fingerprint")) ? response.getString("device_fingerprint") : "'";
                                } catch(Exception ex) {
                                    ex.printStackTrace();
                                }

                            }
                            logger.debug(requestId + " - device_fingerprint: " + device_fingerprint);
                        }
                        String payResult = payfortUtil.firstPayWithToken(method, token, currencyISO, customerEmail, customerIP, customerId, amount, device_fingerprint);
                        Map<String, Object> mapPayResult = mapperResult.readValue(payResult, new TypeReference<HashMap<String, Object>>() { });
                        // check pay result, if successful then save data to Redis
                        // temporary save result to Redis and retrieve when completed payment
                        if (type.equals("addToken")) {
                            redisDao.add3DSData(requestId, customerId, resultCreateToken, "3dstoken");
                        } else {
                            if (Integer.parseInt(mapPayResult.get("returnCode").toString()) == 200) {
                                redisDao.add3DSData(requestId, customerId, payResult, "3dsinput");
                            }
                        }
                        return payResult;
                    }
                }
            }
            break;
            case KeysUtil.YEEPAY: {
                GatewayYeepay gatewayFleet = mongoDao.getGatewayYeepayFleet(fleet.fleetId);
                GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.YEEPAY, gatewayFleet.environment);
                YeepayUtil yeepayUtil = new YeepayUtil();
                String inputPhone = creditEnt.creditPhone != null ? creditEnt.creditPhone : "";
                if (!inputPhone.isEmpty())
                    customerPhone = inputPhone;
                if (!customerPhone.isEmpty() && customerPhone.length() > 11) {
                    customerPhone = customerPhone.substring(customerPhone.length() - 11);
                }
                double amount = type.equals("addToken") ? 1.00 : Double.parseDouble(mapData.get("total"));
                result = yeepayUtil.submitSaleToGetVerifyCode(requestId, type, bookId, amount, gatewayFleet.merchantId, gatewayFleet.key, gatewayFleet.terminalId, gatewayURL.url, currencyISO, customerPhone,
                        mapData.get("creditType"), mapData.get("creditCode"), mapData.get("cardNumber"), mapData.get("cardHolder"), mapData.get("cvv"), mapData.get("expiredDate"));
                logger.debug(requestId + " - result: " + result);
                ObjectMapper mapperResult = new ObjectMapper();
                Map<String,Object> mapResult = mapperResult.readValue(result, new TypeReference<HashMap<String, Object>>() { });
                int payResult = Integer.parseInt(mapResult.get("returnCode").toString());
                if (payResult == 457) {
                    Map<String,String> mapPayResponse = (Map<String,String>) mapResult.get("response");
                    String orderId = mapPayResponse.get("orderId");
                    if (type.equals("addToken"))
                        redisDao.add3DSData(requestId, mapData.get("userId"), "a" + orderId, "3dstoken");
                    else
                        redisDao.add3DSData(requestId, mapData.get("bookId"), "a" + orderId, "3dsinput"); // add prefix "a" to avoid Redis do round order number
                }

                // remove lock booking before return
                if (type.equals("payInput"))
                    redisDao.unlockBooking(mapData.get("bookId"), requestId);
            }
            break;
            case KeysUtil.FLUTTERWAVE: {
                double amount = type.equals("addToken") ? 1.00 : Double.parseDouble(mapData.get("total"));
                GatewayFlutterwave gatewayFleet = mongoDao.getGatewayFlutterwaveFleet(mapData.get("fleetId"));
                if (gatewayFleet.publicKey.equals("") || gatewayFleet.secretKey.equals("")) {
                    createTokenResponse.returnCode = 415;
                    return createTokenResponse.toString();
                }
                Boolean isSandbox = gatewayFleet.environment.equalsIgnoreCase(KeysUtil.SANDBOX);
                FlutterWaveUtil flutterWaveUtil = new FlutterWaveUtil(requestId, gatewayFleet.publicKey, gatewayFleet.secretKey);
                Account account = mongoDao.getAccount(mapData.get("userId"));

                String customerEmail = "";
                String customerIP = "127.0.0.1";
                if (account != null) {
                    customerEmail = account.email != null ? account.email : "";
                    customerIP = account.address != null ? account.address : "";
                    if (customerIP.equals(""))
                        customerIP = "127.0.0.1";
                }
                if (customerEmail.trim().isEmpty()) {
                    // try to get email from the request
                    customerEmail = mapData.get("customerEmail") != null ? mapData.get("customerEmail") : "";
                    if (customerEmail.trim().isEmpty()) {
                        createTokenResponse.returnCode = 470;
                        return createTokenResponse.toString();
                    }
                }
                // get country based on the phone number
                String countryName = "";
                if (type.equals("addToken")) {
                    Map<String,String> mapInfo = GatewayUtil.getCurrency(customerPhone);
                    countryName = mapInfo.get("countryName");
                    currencyISO = mapInfo.get("currencyISO");
                } else {
                    countryName = GatewayUtil.getCountryNameFromCurrency(currencyISO);
                }
                // check FlutterWave and local supported currencies
                ObjResponse checkCurrency = GatewayUtil.checkSupportedCurrency(currencyISO, customerPhone);
                if (checkCurrency.returnCode != 200)
                    return checkCurrency.toString();
                result = flutterWaveUtil.initiateCreditForm(mapData, amount, customerEmail, customerPhone, customerIP, countryName,
                        isSandbox, currencyISO);
                int returnCode = getReturnCode(result);
                if (returnCode == 469) {
                    // remove previous transaction if exist
                    redisDao.remove3DSData(requestId, customerPhone, "flwRefFlutter");

                    // store successful initiated request to Redis
                    String flwRef = "";
                    try {
                        JSONObject json = (JSONObject)new org.json.simple.parser.JSONParser().parse(result);
                        flwRef = json.get("response").toString();
                    } catch (Exception ex) {
                        logger.debug(requestId + " - parse json error - " + result);
                    }
                    redisDao.add3DSData(requestId, customerPhone, flwRef, "flwRefFlutter");
                }
            }
            break;
            case KeysUtil.ADYEN: {
                GatewayAdyen gatewayFleet = mongoDao.getGatewayAdyenFleet(mapData.get("fleetId"));
                if (gatewayFleet.merchantAccount.equals("") || gatewayFleet.hmacKey.equals("") || gatewayFleet.skinCode.equals("")) {
                    createTokenResponse.returnCode = 415;
                    return createTokenResponse.toString();
                }
                Boolean isSandbox = gatewayFleet.environment.equalsIgnoreCase(KeysUtil.SANDBOX);
                AdyenUtil adyenUtil = new AdyenUtil(gatewayFleet.merchantAccount, gatewayFleet.username, gatewayFleet.password,
                        gatewayFleet.hmacKey, gatewayFleet.skinCode, gatewayFleet.cseKey);
                Account account = mongoDao.getAccount(mapData.get("userId"));
                String customerEmail = "";
                if (account != null) {
                    customerEmail = account.email != null ? account.email : "";
                }
                result = adyenUtil.initiateCreditForm(requestId, customerPhone, customerEmail, isSandbox, currencyISO);
            }
            break;
            case KeysUtil.CXPAY: {
                GatewayCXPay gatewayFleet = mongoDao.getGatewayCXPayFleet(fleet.fleetId);
                GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.CXPAY, "");
                CXPayUtil cxPayUtil = new CXPayUtil(requestId, gatewayFleet.apiKey, gatewayURL.url);

                Account account = mongoDao.getAccount(mapData.get("userId"));
                String customerEmail = "";
                String firstName = "";
                String lastName = "";
                if (account != null) {
                    customerEmail = account.email != null ? account.email : "";
                    firstName = account.firstName != null ? account.firstName : "";
                    lastName = account.lastName != null ? account.lastName : "";
                } else {
                    // check if register card from corporate
                    if (mapData.get("registerType").equals("corporate")) {
                        Corporate corporate = mongoDao.getCorporate(mapData.get("corporateId"));
                        if (corporate != null && corporate.adminAccount != null) {
                            customerEmail = corporate.adminAccount.email != null ? corporate.adminAccount.email : "";
                            firstName = corporate.adminAccount.firstName != null ? corporate.adminAccount.firstName : "";
                            lastName = corporate.adminAccount.lastName != null ? corporate.adminAccount.lastName : "";
                            customerPhone = corporate.adminAccount.phone != null ? corporate.adminAccount.phone : "";
                            if (customerPhone.isEmpty()) {
                                createTokenResponse.returnCode = 475;
                                Map<String,String> map = new HashMap<>();
                                map.put("message", "Phone number is required");
                                createTokenResponse.response = map;
                                return createTokenResponse.toString();
                            }
                        }
                    }
                }
                String orderId = GatewayUtil.getOrderId();
                result = cxPayUtil.initiateCreditForm(orderId, firstName, lastName, customerEmail, customerPhone, creditEnt.userId);
                ObjResponse response = gson.fromJson(result, ObjResponse.class);
                if (response.returnCode == 200) {
                    String transactionId = CommonUtils.getValue(result, "transactionId");
                    redisDao.cacheData(requestId, orderId+"credit", gson.toJson(creditEnt), 3);
                    redisDao.cacheData(requestId, orderId+"transactionId", transactionId, 3);
                }
            }
            break;
            case KeysUtil.FAC: {
                GatewayFirstAtlanticCommerce gatewayFleet = mongoDao.getGatewayFACFleet(mapData.get("fleetId"));


                creditEnt.cardNumber = mapData.get("cardNumber");
                creditEnt.expiredDate = mapData.get("expiredDate");
                creditEnt.cvv = mapData.get("cvv");
                creditEnt.postalCode = mapData.get("postalCode");
                creditEnt.phone = customerPhone.trim().replace("+", "");
                String orderNumber = "REGISTER-" + creditEnt.phone + System.currentTimeMillis();
                if (gatewayFleet.powerTranz) {
                    if (gatewayFleet.powerTranzId.equals("") || gatewayFleet.powerTranzPassword.equals("")) {
                        createTokenResponse.returnCode = 415;
                        return createTokenResponse.toString();
                    }
                    String userId = mapData.get("userId");
                    String firstName = "";
                    String lastName = "";
                    String phoneNumber = "";
                    if (!userId.isEmpty()) {
                        Account account = mongoDao.getAccount(userId);
                        firstName = account.firstName != null ? account.firstName : "";
                        lastName = account.lastName != null ? account.lastName : "";
                        phoneNumber = account.phone;
                    }
                    GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.FAC, gatewayFleet.environment);
                    PowerTranzUtil powerTranzUtil = new PowerTranzUtil(requestId, gatewayFleet.powerTranzId, gatewayFleet.powerTranzPassword, gatewayURL.url);
                    result = powerTranzUtil.initiateCreditForm(creditEnt, currencyISO, orderNumber, firstName, lastName, phoneNumber);
                    ObjResponse response = gson.fromJson(result, ObjResponse.class);
                    if (response.returnCode == 200) {
                        String transactionId = CommonUtils.getValue(result, "transactionId");
                        redisDao.cacheData(requestId, orderNumber+"credit", gson.toJson(creditEnt), 3);
                        redisDao.cacheData(requestId, "queue-"+transactionId, creditEnt.fleetId, 3);
                    }
                } else {
                    if (gatewayFleet.merchantId.equals("") || gatewayFleet.acquirerId.equals("") || gatewayFleet.password.equals("")) {
                        createTokenResponse.returnCode = 415;
                        return createTokenResponse.toString();
                    }
                    Boolean isSandbox = gatewayFleet.environment.equalsIgnoreCase(KeysUtil.SANDBOX);
                    FirstAtlanticCommerceUtil firstAtlanticCommerceUtil = new FirstAtlanticCommerceUtil(requestId);
                    result = firstAtlanticCommerceUtil.initiateCreditForm(creditEnt, gatewayFleet.merchantId, gatewayFleet.acquirerId, gatewayFleet.password, currencyISO, isSandbox, orderNumber);
                }
                redisDao.addTmpData(requestId, "queue-"+orderNumber, creditEnt.fleetId);
            }
            break;
            case KeysUtil.CONEKTA: {
                GatewayConekta gatewayFleet = mongoDao.getGatewayConektaFleet(mapData.get("fleetId"));
                if (gatewayFleet.apiKey.isEmpty() || gatewayFleet.secretKey.isEmpty()) {
                    createTokenResponse.returnCode = 415;
                    return createTokenResponse.toString();
                }

                ConektaUtil conektaUtil = new ConektaUtil(requestId);
                result = conektaUtil.initiateCreditForm(gatewayFleet.apiKey);
            }
            break;
            case KeysUtil.SENANGPAY: {
                GatewaySenangpay gatewayFleet = mongoDao.getGatewaySenangpayFleet(fleet.fleetId);
                GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.SENANGPAY, gatewayFleet.environment);
                SenangpayUtil senangpayUtil = new SenangpayUtil(requestId, gatewayFleet.merchantAccount, gatewayFleet.wsUser, gatewayFleet.wsPassword, gatewayFleet.secretKey, gatewayURL.tokenURL, gatewayURL.url);
                Account account = mongoDao.getAccount(mapData.get("userId"));
                String customerName = "";
                String customerEmail = mapData.get("customerEmail") != null ? mapData.get("customerEmail") : "";
                if (account != null) {
                    customerName = account.fullName != null ? account.fullName.trim() : "";
                    if (customerEmail.isEmpty())
                        customerEmail = account.email != null ? account.email : "";
                }

                result = senangpayUtil.initiateCreditForm(customerName, customerPhone, customerEmail);
            }
            break;
            case KeysUtil.FIRSTDATA: {
                GatewayFirstData gatewayFleet = mongoDao.getGatewayFirstData(fleet.fleetId);
                GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.FIRSTDATA, gatewayFleet.environment);
                FirstDataUtil firstDataUtil = new FirstDataUtil(gatewayFleet.apiSecret,gatewayFleet.apiKey,gatewayFleet.storeId, gatewayFleet.paymentStoreId, gatewayURL.url);
                result = firstDataUtil.initiateCreditForm(creditEnt);
                ObjResponse response = gson.fromJson(result, ObjResponse.class);
                if (response.returnCode == 200) {
                    Map<String, String> responseData = (Map<String, String>) response.response;
                    String merchantData = responseData.get("merchantData") != null ? responseData.get("merchantData") : "";
                    String ipgTransactionId = responseData.get("ipgTransactionId") != null ? responseData.get("ipgTransactionId") : "";
                    String token = responseData.get("token") != null ? responseData.get("token") : "";
                    String key = merchantData.substring(merchantData.length() - 18);
                    redisDao.addTmpData(requestId, key+"credit", gson.toJson(creditEnt));
                    redisDao.addTmpData(requestId, key+"transaction", ipgTransactionId);
                    redisDao.addTmpData(requestId, key+"token", token);
                    redisDao.addTmpData(requestId, "queue-"+key, creditEnt.fleetId);
                }
            }
            break;
            case KeysUtil.EGHL: {
                GatewayEGHL gatewayFleet = mongoDao.getGatewayPayEGHLFleet(fleet.fleetId);
                GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.EGHL, gatewayFleet.environment);
                EGHLUtil eghlUtil = new EGHLUtil(requestId, gatewayFleet.serviceId, gatewayFleet.password, gatewayURL.url);
                Account account = mongoDao.getAccount(mapData.get("userId"));
                String customerName = "";
                String customerEmail = mapData.get("customerEmail") != null ? mapData.get("customerEmail") : "";
                String customerIP = "";
                if (account != null) {
                    customerName = account.fullName != null ? account.fullName.trim() : "";
                    if (customerEmail.isEmpty())
                        customerEmail = account.email != null ? account.email : "";

                    customerIP = account.address != null ? account.address : "";
                    if (customerIP.isEmpty())
                        customerIP = "127.0.0.1";
                }
                String paymentId = GatewayUtil.getOrderId();
                result = eghlUtil.initiateCreditForm(paymentId, customerName, customerEmail, customerPhone, customerIP, fleet.currencyISOValidation);
                ObjResponse response = gson.fromJson(result, ObjResponse.class);
                if (response.returnCode == 200) {
                    Map<String, String> responseData = (Map<String, String>) response.response;
                    String transactionId = responseData.get("transactionId") != null ? responseData.get("transactionId") : "";
                    redisDao.addTmpData(requestId, transactionId+"credit", gson.toJson(creditEnt));
                    redisDao.addTmpData(requestId, transactionId+"paymentId", paymentId);
                    redisDao.addTmpData(requestId, "queue-"+transactionId, creditEnt.fleetId);
                }
            }
            break;
            case KeysUtil.TSYS: {
                GatewayTSYS gatewayFleet = mongoDao.getGatewayTSYS(fleet.fleetId);
                GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.TSYS, gatewayFleet.environment);
                TSYSUtil tsysUtil = new TSYSUtil(requestId, gatewayFleet.apiKey, gatewayFleet.companyNumber, gatewayFleet.merchantNumber, gatewayFleet.merchantTerminalNumber,
                        gatewayURL.url);
                String sessionId = GatewayUtil.getOrderId();
                String userId = creditEnt.userId != null ? creditEnt.userId : "";
                String customerNumber =  userId.length() > 8 ? userId.substring(0, 8) : "CUS12345";
                boolean showAddress = false;
                List<ConfigGateway> listConfig = fleet.creditConfig.configGateway;
                for (ConfigGateway configGateway : listConfig) {
                    showAddress = configGateway.zipCode != null && configGateway.zipCode.enable;
                }
                result = tsysUtil.initiateCreditForm(customerNumber, sessionId, showAddress);
                ObjResponse response = gson.fromJson(result, ObjResponse.class);
                if (response.returnCode == 200) {
                    redisDao.addTmpData(requestId, KeysUtil.TSYS+sessionId+"credit", gson.toJson(creditEnt));
                    redisDao.addTmpData(requestId, KeysUtil.TSYS+sessionId+"customerNumber", customerNumber);
                    redisDao.addTmpData(requestId, "queue-"+sessionId, creditEnt.fleetId);
                }
            }
            break;
            case KeysUtil.PAYMAYA: {
                GatewayPayMaya gatewayFleet = mongoDao.getGatewayPayMaya(fleet.fleetId);
                GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.PAYMAYA, gatewayFleet.environment);
                PayMayaUtil payMayaUtil = new PayMayaUtil(requestId, gatewayFleet.publicKey, gatewayFleet.secretKey, gatewayURL.url);
                String orderId = GatewayUtil.getOrderId();
                result = payMayaUtil.initiateCreditForm(orderId);
                ObjResponse response = gson.fromJson(result, ObjResponse.class);
                if (response.returnCode == 200) {
                    redisDao.addTmpData(requestId, orderId+"credit", gson.toJson(creditEnt));
                    redisDao.addTmpData(requestId, "queue-"+orderId, creditEnt.fleetId);
                }
            }
            break;
            case KeysUtil.BOG : {
                GatewayBankOfGeorgia gatewayFleet = mongoDao.getGatewayBOG(creditEnt.fleetId);
                GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.BOG, gatewayFleet.environment);
                BOGUtil bogUtil = new BOGUtil(creditEnt.requestId, gatewayFleet.clientId, gatewayFleet.clientSecret,
                        gatewayURL.serverURL, gatewayURL.payURL, gatewayURL.authURL, gatewayFleet.environment);
                String orderId = GatewayUtil.getOrderId();
                result = bogUtil.getCheckoutURL(1.0, orderId, "Register", fleet.currencyISOValidation);
                if (CommonUtils.getReturnCode(result) == 200) {
                    String transactionId = CommonUtils.getValue(result, "transactionId");
                    redisDao.addTmpData(creditEnt.requestId, KeysUtil.BOG+transactionId+"type", "Register");
                    redisDao.addTmpData(creditEnt.requestId, KeysUtil.BOG+transactionId+"credit", gson.toJson(creditEnt));
                    redisDao.addTmpData(creditEnt.requestId, "queue-"+orderId, creditEnt.fleetId);
                }
            }
            break;
            case KeysUtil.MADA: {
                GatewayMADA gatewayFleet = mongoDao.getGatewayMADA(fleet.fleetId);
                GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.MADA, gatewayFleet.environment);
                MADAUtil madaUtil = new MADAUtil(requestId, gatewayFleet.userName, gatewayFleet.terminalID, gatewayFleet.password, gatewayFleet.secretKey, gatewayURL.url);

                String orderId = GatewayUtil.getOrderId();
                Map<String,String> map = new HashMap<>();
                Account account = mongoDao.getAccount(creditEnt.userId);
                String country = fleet.country;
                String customerEmail = "";
                if (account != null) {
                    customerEmail = account.email != null ? account.email : "";
                }
                if (customerEmail.isEmpty()) {
                    createTokenResponse.returnCode = 470;
                    return createTokenResponse.toString();
                }
                map.put("country", country);
                map.put("customerEmail", customerEmail);
                InetAddress ip = InetAddress.getLocalHost();
                map.put("merchantIp", ip.getHostAddress());
                result = madaUtil.initiateCreditForm(orderId, fleet.currencyISOValidation, map);
                ObjResponse response = gson.fromJson(result, ObjResponse.class);
                if (response.returnCode == 200) {
                    redisDao.addTmpData(requestId, orderId+"credit", gson.toJson(creditEnt));
                    redisDao.addTmpData(requestId, "queue-"+orderId, creditEnt.fleetId);
                }
            }
            break;
            case KeysUtil.PAYWAY: {
                String getConfigInfo = mongoDao.getConfigInfo(fleet.fleetId, "Gateway"+KeysUtil.PAYWAY);
                GatewayPayWay gatewayFleet = gson.fromJson(getConfigInfo, GatewayPayWay.class);
                GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.PAYWAY, gatewayFleet.environment);
                PayWayUtil payWayUtil = new PayWayUtil(requestId, gatewayFleet.merchantId, gatewayFleet.apiKey, gatewayFleet.publicKey, gatewayFleet.skipAuthenAmount, gatewayURL.url);

                String orderId = GatewayUtil.getOrderId();
                String registerType = mapData.get("registerType");
                String userId = creditEnt.userId != null ? creditEnt.userId : "";
                Map<String, String> mapUser = new HashMap<>();
                if (!userId.isEmpty()) {
                    Map<String, String> extraData = GatewayUtil.getExtraData(registerType, creditEnt.userId);
                    String customerName = creditEnt.name != null ? creditEnt.name : "";
                    if (customerName.trim().isEmpty()) {
                        customerName = extraData.get("firstName") + " " + extraData.get("lastName");
                        if (customerName.trim().isEmpty()) {
                            createTokenResponse.returnCode = 474;
                            return createTokenResponse.toString();
                        }
                    }
                    mapUser.put("firstName", extraData.get("firstName") != null ? extraData.get("firstName") : "");
                    mapUser.put("lastName", extraData.get("lastName") != null ? extraData.get("lastName") : "");
                } else {
                    mapUser.put("firstName", creditEnt.firstName != null ? creditEnt.firstName : "");
                    mapUser.put("lastName", creditEnt.lastName != null ? creditEnt.lastName : "");
                }
                // check if user already register a card
                String ctid = "";
                if (registerType.equals("user") || registerType.equals("customer")) {
                    Account account = mongoDao.getAccount(creditEnt.userId);
                    if (account != null && account.credits != null && !account.credits.isEmpty()) {
                        for (Credit credit : account.credits) {
                            String[] arrToken = credit.localToken.split(KeysUtil.TEMPKEY);
                            if (arrToken.length > 0) {
                                ctid = arrToken[0];
                                break;
                            }
                        }
                    }
                } else if (registerType.equals("corporate")) {
                    Corporate corporate = mongoDao.getCorporate(creditEnt.userId);
                    if (corporate != null && corporate.credits != null && !corporate.credits.isEmpty()) {
                        for (Credit credit : corporate.credits) {
                            String[] arrToken = credit.localToken.split(KeysUtil.TEMPKEY);
                            if (arrToken.length > 0) {
                                ctid = arrToken[0];
                                break;
                            }
                        }
                    }
                }
                logger.debug(requestId + " - ctid: " + ctid);
                mapUser.put("userId", ctid);
                result = payWayUtil.initiateCreditForm(orderId, mapUser);
                ObjResponse response = gson.fromJson(result, ObjResponse.class);
                if (response.returnCode == 200) {
                    redisDao.addTmpData(requestId, orderId+"credit", gson.toJson(creditEnt));
                    redisDao.addTmpData(requestId, "queue-"+orderId, creditEnt.fleetId);
                }
            }
            break;
            case KeysUtil.ONEPAY: {
                boolean createToken = type.equals("addToken")? true : false;
                String getConfigInfo =
                        mongoDao.getConfigInfo(fleet.fleetId, "Gateway" + KeysUtil.ONEPAY);
                GatewayOnePay gatewayOnePay = gson.fromJson(getConfigInfo, GatewayOnePay.class);
                GatewayURL gatewayURL =
                        mongoDao.getGatewayURL(KeysUtil.ONEPAY, gatewayOnePay.environment);
                String orderId = GatewayUtil.getOrderId();
                OnePayUtil onePayUtil =
                        new OnePayUtil(
                                requestId,
                                gatewayOnePay.merchantId,
                                gatewayOnePay.accessCode,
                                gatewayOnePay.hashCode,
                                gatewayURL.url);
                Account account = mongoDao.getAccount(creditEnt.userId);
                String userIP = account.address != null ? account.address : "127.0.0.1";
                String amount = createToken ? "10000" : mapData.get("total");
                Map<String, String> data = new HashMap<>();
                data.put("userId", creditEnt.userId);
                data.put("ip", userIP);
                data.put("amount", amount);
                data.put("orderId", orderId);
                result = onePayUtil.requestPayment(data, createToken);
                ObjResponse response = gson.fromJson(result, ObjResponse.class);
                if (response.returnCode == 200) {
                    redisDao.cacheData(requestId, orderId+"credit", gson.toJson(creditEnt), 3);
                }
            }
            break;
            case KeysUtil.ECPAY: {
                String getConfigInfo = mongoDao.getConfigInfo(fleet.fleetId, "Gateway"+KeysUtil.ECPAY);
                GatewayECPay gatewayFleet = gson.fromJson(getConfigInfo, GatewayECPay.class);
                GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.ECPAY, gatewayFleet.environment);
                ECPayUtil ecPayUtil = new ECPayUtil(requestId, gatewayFleet.merchantId, gatewayFleet.hashKey, gatewayFleet.hashIV, gatewayFleet.creditCheckCode, gatewayURL.url);
                Account account = mongoDao.getAccount(mapData.get("userId"));
                String firstName = "";
                String lastName = "";
                if (account != null) {
                    firstName = account.firstName != null ? account.firstName : "";
                    lastName = account.lastName != null ? account.lastName : "";
                } else {
                    // check if register card from corporate
                    if (mapData.get("registerType").equals("corporate")) {
                        Corporate corporate = mongoDao.getCorporate(mapData.get("corporateId"));
                        if (corporate != null && corporate.adminAccount != null) {
                            firstName = corporate.adminAccount.firstName != null ? corporate.adminAccount.firstName : "";
                            lastName = corporate.adminAccount.lastName != null ? corporate.adminAccount.lastName : "";
                            customerPhone = corporate.adminAccount.phone != null ? corporate.adminAccount.phone : "";
                        }
                    }
                }

                String orderId = GatewayUtil.getOrderId();
                result = ecPayUtil.initiateCreditForm(orderId, firstName, lastName, customerPhone);
                ObjResponse response = gson.fromJson(result, ObjResponse.class);
                if (response.returnCode == 200) {
                    String merchantMemberID = CommonUtils.getValue(result, "merchantMemberID");
                    redisDao.cacheData(requestId, orderId+"credit", gson.toJson(creditEnt), 3);
                    redisDao.cacheData(requestId, orderId+"merchantMemberID", merchantMemberID, 3);
                }
            }
            break;
            default: {
                createTokenResponse.returnCode = 415;
                result = createTokenResponse.toString();
            }
        }

        return result;
    }

    JSONObject getDataSharing(Booking booking){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("rideSharing", booking.dispatchRideSharing != null ? booking.dispatchRideSharing: false);
        JSONArray sharedBooks = new JSONArray();
        if(booking.bookingRideSharing != null && !booking.bookingRideSharing.isEmpty()){
            for(BookingRideSharing bookingRS: booking.bookingRideSharing){
                JSONObject sharedBook = new JSONObject();
                String firstName = "";
                String lastName = "";
                String customerId = "";
                if(bookingRS.psgInfo != null && bookingRS.psgInfo.firstName!= null)
                    firstName = bookingRS.psgInfo.firstName;
                if(bookingRS.psgInfo != null && bookingRS.psgInfo.lastName!= null)
                    lastName = bookingRS.psgInfo.lastName;
                if(bookingRS.psgInfo != null && bookingRS.psgInfo.userId != null)
                    customerId = bookingRS.psgInfo.userId;
                sharedBook.put("customerName", firstName + " " + lastName);
                sharedBook.put("customerId", customerId);
                sharedBook.put("bookId", bookingRS.bookId);
                sharedBooks.add(sharedBook);
            }
        }
        jsonObject.put("sharedBooks", sharedBooks);
        return jsonObject;
    }

    JSONObject getDataItinerary(Itinerary itinerary, double totalFare){
        JSONObject jsonObject = new JSONObject();
        if(itinerary != null) {
            jsonObject.put("itineraryId", itinerary._id.toString());
            jsonObject.put("eventId", itinerary.event != null ? itinerary.event._id.toString() : "");
            jsonObject.put("eventName", itinerary.event != null && itinerary.event.name != null ? itinerary.event.name : "");
            jsonObject.put("customerType", itinerary.customerType);
            jsonObject.put("quoted", totalFare);
            double totalMarkup = 0;
            double corporateProfit = 0;
            if(itinerary.preferredEarningType != null){
                if(itinerary.preferredEarningType.equalsIgnoreCase("Amount")){
                    totalMarkup = totalFare + itinerary.preferredEarningAmount;
                }else {
                    totalMarkup = totalFare + (itinerary.preferredEarningAmount * totalFare * 0.01);
                }
                corporateProfit = totalMarkup - totalFare;
            }
            jsonObject.put("totalMarkup", totalMarkup);
            jsonObject.put("corporateProfit", corporateProfit);
            jsonObject.put("organizationId", itinerary.organizationInfo != null ? itinerary.organizationInfo._id.toString() : "");
            jsonObject.put("organizationName", itinerary.organizationInfo != null && itinerary.organizationInfo.name!= null ? itinerary.organizationInfo.name : "");
            if(itinerary.customerType == 0){
                jsonObject.put("dmcCustomerInfo", itinerary.individualInfo != null ? itinerary.individualInfo : new JSONObject());
            }else {
                jsonObject.put("dmcCustomerInfo", itinerary.organizationInfo != null && itinerary.organizationInfo.representative != null ?
                        itinerary.organizationInfo.representative : new JSONObject());
            }
        }
        return jsonObject;
    }

    String getDataService(Booking booking){
        JSONArray services = new JSONArray();
        if(booking.request != null && booking.request.services!= null && !booking.request.services.isEmpty()){
            /*if(service.serviceId != null && !service.serviceId.isEmpty()){
                AdditionalServices sv = mongoDao.findByServiceId(service.serviceId);
                JSONObject object = new JSONObject();
                if(sv != null){
                    double serviceFee = 0;
                    if(sv.serviceFeeByCurrencies != null && !sv.serviceFeeByCurrencies.isEmpty()){
                        AmountByCurrency fee = sv.serviceFeeByCurrencies.stream()
                                .filter(amount -> amount.currencyISO.equalsIgnoreCase(booking.currencyISO))
                                .findFirst().orElse(null);
                        if(fee != null){
                            serviceFee = fee.value;
                        }
                    }
                    object.put("name", sv.serviceName);
                    object.put("value", serviceFee);
                    object.put("type", sv.serviceType);
                    services.add(object);
                }
            }*/
            booking.request.services.stream().filter(service -> service.active).forEach(service -> {
                JSONObject object = new JSONObject();
                object.put("name", service.name);
                object.put("value", service.fee);
                object.put("type", service.type);
                services.add(object);
            });
        }

        return services.toJSONString();
    }

    String getExtraDestination(Booking booking){
        JSONArray jsonArray = new JSONArray();
        if(booking.request != null && booking.request.extraDestination!= null && !booking.request.extraDestination.isEmpty()){

            booking.request.extraDestination.stream().forEach(extraDes -> {
                JSONObject object = new JSONObject();
                object.put("address", extraDes.address);
                object.put("addressDetails", extraDes.addressDetails != null ? gson.fromJson(gson.toJson(extraDes.addressDetails), JSONObject.class): new JSONObject());
                object.put("geo", extraDes.geo);
                jsonArray.add(object);
            });
        }
        return jsonArray.toJSONString();
    }

    public boolean isUnionPayCard(String fleetId, String cardNumber) {
        boolean result = false;
        Fleet fleet = mongoDao.getFleetInfor(fleetId);
        if (fleet.country.equals("China")) {
            // detect Unionpay card on Bin list
            MongoDao mongoDao = new MongoDaoImpl();
            boolean isUnionpay = mongoDao.checkBinData(cardNumber);
            if (isUnionpay) {
                result = true;
            } else {
                // try to check from card number
                if (com.pg.util.ValidCreditCard.getCardType(cardNumber).equalsIgnoreCase(KeysUtil.UNIONPAY))
                    result = true;
            }
        } else {
            if (ValidCreditCard.getCardType(cardNumber).equalsIgnoreCase(KeysUtil.UNIONPAY))
                result = true;
        }
        return result;
    }

    ConfigGateway getConfigGateway(Fleet fleet, String zoneId, List<Double> geoLocation) {
        ConfigGateway configGateway = null;
        if(fleet.creditConfig != null && fleet.creditConfig.enable){
            boolean multiGateway = false;
            if (fleet.creditConfig.multiGateway) {
                multiGateway = true;
                // if zoneId is empty, query zone by geo location
                if (zoneId.trim().isEmpty() && geoLocation != null) {
                    Zone zone = mongoDao.findByFleetIdAndGeo(fleet.fleetId, geoLocation);
                    if (zone != null) {
                        zoneId = zone._id.toString();
                    }
                }
            }

            if (multiGateway) {
                for (ConfigGateway config : fleet.creditConfig.configGateway) {
                    if (config.zones.contains(zoneId)) {
                        configGateway = config;
                        break;
                    }
                }
            } else {
                configGateway = (fleet.creditConfig.configGateway != null && fleet.creditConfig.configGateway.size() > 0) ? fleet.creditConfig.configGateway.get(0) : null;
            }
        }
        return configGateway;
    }

    String updateCorporateInfo(String requestId, String fleetId, Account account) {
        ObjResponse response = new ObjResponse();
        try {
            if (account.passengerInfo != null) {
                Fleet fleet = mongoDao.getFleetInfor(fleetId);
                if (fleet.corporate != null && fleet.corporate.verifyCorporateCC) {
                    String verifyCorporateId = fleet.corporate.verifyCorporateId != null ? fleet.corporate.verifyCorporateId : "";
                    if (!verifyCorporateId.isEmpty()) {
                        Corporate corporate = mongoDao.getCorporate(verifyCorporateId);
                        if (corporate != null) {
                            Date createdDate = TimezoneUtil.offsetTimeZone(new Date(), Calendar.getInstance().getTimeZone().getID(), "GMT");
                            int status = account.isActive ? 2 : 0;
                            mongoDao.updateCorporateInfo(account.userId, corporate, TimezoneUtil.formatISODate(createdDate, "GMT"), status);
                            Map<String,String> map = new HashMap<>();
                            map.put("verifyCorporateId", verifyCorporateId);
                            String companyName = corporate.companyInfo.name != null ? corporate.companyInfo.name : "";
                            map.put("companyName", companyName);
                            response.returnCode = 200;
                            response.response = map;
                        } else {
                            response.returnCode = 406;
                        }
                    } else {
                        response.returnCode = 406;
                    }
                }

            } else {
                response.returnCode = 406;
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            response.returnCode = 437;
        }
        logger.debug(requestId + " - updateCorporateInfo: " + response.toString());
        return response.toString();

    }

    String validateTopup(String fleetId, String from, double amount, String currencyISO) {
        ObjResponse objResponse = new ObjResponse();
        // validate minimum and maximum setting
        if (!from.equals("cc")) {
            PricingPlan pricingPlan = mongoDao.getPricingPlan(fleetId);
            if (pricingPlan != null && pricingPlan.driverDeposit != null && pricingPlan.driverDeposit.enable) {
                if (pricingPlan.driverDeposit.minimumByCurrencies != null && !pricingPlan.driverDeposit.minimumByCurrencies.isEmpty()) {
                    double minimum = 0.0;
                    for (AmountByCurrency minimumByCurrencies : pricingPlan.driverDeposit.minimumByCurrencies) {
                        if (minimumByCurrencies.currencyISO.equals(currencyISO))
                            minimum = minimumByCurrencies.value;
                    }

                    if (minimum > 0 && amount < minimum) {
                        objResponse.returnCode = 476;
                        Map<String,Double> mapResponse = new HashMap<>();
                        mapResponse.put("minimum", minimum);
                        objResponse.response = mapResponse;

                        return objResponse.toString();
                    }
                }
            }

            if (pricingPlan != null && pricingPlan.driverDeposit != null && pricingPlan.driverDeposit.enable) {
                if (pricingPlan.driverDeposit.maximumByCurrencies != null && !pricingPlan.driverDeposit.maximumByCurrencies.isEmpty()) {
                    double maximum = 0.0;
                    for (AmountByCurrency maximumByCurrencies : pricingPlan.driverDeposit.maximumByCurrencies) {
                        if (maximumByCurrencies.currencyISO.equals(currencyISO))
                            maximum = maximumByCurrencies.value;
                    }

                    if (maximum > 0 && amount > maximum) {
                        objResponse.returnCode = 476;
                        Map<String,Double> mapResponse = new HashMap<>();
                        mapResponse.put("maximum", maximum);
                        objResponse.response = mapResponse;
                        return objResponse.toString();
                    }
                }
            }
        }
        objResponse.returnCode = 200;
        return objResponse.toString();
    }

    String validatePaxWalletTopup(String fleetId, String type, double amount, String currencyISO) {
        ObjResponse objResponse = new ObjResponse();

        PricingPlan pricingPlan = mongoDao.getPricingPlan(fleetId);
        boolean isSupport = false;
        if (pricingPlan.paxCreditWallet != null && pricingPlan.paxCreditWallet.enable) {
            // validate credit type
            List<String> creditTypes = Arrays.asList(KeysUtil.PAYMENT_CARD, KeysUtil.PAYMENT_APPLEPAY, KeysUtil.PAYMENT_GOOGLEPAY);
            if (pricingPlan.paxCreditWallet.viaCredit && creditTypes.contains(type)) {
                isSupport = true;
            }
            // continue validate wallet type if not valid
            if (!isSupport) {
                if (pricingPlan.paxCreditWallet.viaWallet && KeysUtil.GATEWAY_WALLET.contains(type)) {
                    isSupport = true;
                }
            }
        }

        // return failure if not valid
        if (!isSupport) {
            objResponse.returnCode = 453;
            return objResponse.toString();
        }

        // validate minimum and maximum setting
        List<AmountByCurrency> minimumDeposits = pricingPlan.paxCreditWallet.minimumByCurrencies;
        double minimum = 0.0;
        if (minimumDeposits != null && minimumDeposits.size() > 0) {
            for (AmountByCurrency deposit : minimumDeposits) {
                if (deposit.currencyISO.equals(currencyISO)) {
                    minimum = deposit.value;
                    break;
                }
            }
        }
        if (minimum > 0 && amount < minimum) {
            objResponse.returnCode = 476;
            Map<String,Double> mapResponse = new HashMap<>();
            mapResponse.put("minimum", minimum);
            objResponse.response = mapResponse;

            return objResponse.toString();
        }

        double maximum = 0.0;
        List<AmountByCurrency> maximumDeposits = pricingPlan.paxCreditWallet.maximumByCurrencies;
        if (maximumDeposits != null && maximumDeposits.size() > 0) {
            for (AmountByCurrency deposit : maximumDeposits) {
                if (deposit.currencyISO.equals(currencyISO)) {
                    maximum = deposit.value;
                    break;
                }
            }
        }
        if (maximum > 0 && amount > maximum) {
            objResponse.returnCode = 476;
            Map<String,Double> mapResponse = new HashMap<>();
            mapResponse.put("maximum", maximum);
            objResponse.response = mapResponse;
            return objResponse.toString();
        }

        // passed all - finish validation
        objResponse.returnCode = 200;
        return objResponse.toString();
    }


    private int checkLimitValueInput(Fleet fleet, Booking booking, List<Double> listValues){
        FleetFare fleetFare = mongoDao.getFleetFare(fleet.fleetId);
        boolean tollFeeLimitDriverInputActive = false;
        boolean parkingFeeLimitDriverInputActive = false;
        boolean gasFeeLimitDriverInputActive = false;
        boolean otherFeeLimitDriverInputActive = false;
        double tollFeeDriverCanInput = 0.0;
        double parkingFeeDriverCanInput = 0.0;
        double gasFeeDriverCanInput = 0.0;
        double otherFeeDriverCanInput = 0.0;
        String zoneId = "";
        if(booking.request != null && booking.request.pickup != null && booking.request.pickup.zoneId != null)
            zoneId = booking.request.pickup.zoneId;
        if(!zoneId.isEmpty()){
            ServiceFee sF = mongoDao.getServiceFeeByFleetIdAndZoneId(fleet.fleetId, zoneId);
            if(sF != null && fleet.generalSetting.allowCongfigSettingForEachZone){
                tollFeeLimitDriverInputActive = sF.tollFeeLimitDriverInputActive != null
                        && sF.tollFeeLimitDriverInputActive;
                parkingFeeLimitDriverInputActive = sF.parkingFeeLimitDriverInputActive;
                gasFeeLimitDriverInputActive = sF.gasFeeLimitDriverInputActive;
                otherFeeLimitDriverInputActive = sF.otherFee.limitDriverInputActive != null
                        && sF.otherFee.limitDriverInputActive;

                if(tollFeeLimitDriverInputActive && sF.tollFeeDriverCanInput != null){
                    for(AmountByCurrency amountByCurrency: sF.tollFeeDriverCanInput){
                        if(amountByCurrency.currencyISO.equals(booking.currencyISO))
                            tollFeeDriverCanInput = amountByCurrency.value;
                    }
                }
                if(parkingFeeLimitDriverInputActive && sF.parkingFeeDriverCanInput != null){
                    for(AmountByCurrency amountByCurrency: sF.parkingFeeDriverCanInput){
                        if(amountByCurrency.currencyISO.equals(booking.currencyISO))
                            parkingFeeDriverCanInput = amountByCurrency.value;
                    }
                }
                if(gasFeeLimitDriverInputActive && sF.gasFeeDriverCanInput != null){
                    for(AmountByCurrency amountByCurrency: sF.gasFeeDriverCanInput){
                        if(amountByCurrency.currencyISO.equals(booking.currencyISO))
                            gasFeeDriverCanInput = amountByCurrency.value;
                    }
                }

                if(otherFeeLimitDriverInputActive && sF.otherFee.otherFeeDriverCanInput != null){
                    for(AmountByCurrency amountByCurrency: sF.otherFee.otherFeeDriverCanInput){
                        if(amountByCurrency.currencyISO.equals(booking.currencyISO))
                            otherFeeDriverCanInput = amountByCurrency.value;
                    }
                }
            }
        }
        if(fleetFare.applyType == null || fleetFare.applyType.equals("all") || !fleet.generalSetting.allowCongfigSettingForEachZone){
            tollFeeLimitDriverInputActive = fleetFare.tollFeeLimitDriverInputActive != null
                    && fleetFare.tollFeeLimitDriverInputActive;
            parkingFeeLimitDriverInputActive = fleetFare.parkingFeeLimitDriverInputActive;
            gasFeeLimitDriverInputActive = fleetFare.gasFeeLimitDriverInputActive;
            otherFeeLimitDriverInputActive = fleetFare.otherFee.limitDriverInputActive != null
                    && fleetFare.otherFee.limitDriverInputActive;

            if(tollFeeLimitDriverInputActive && fleetFare.tollFeeDriverCanInput != null){
                for(AmountByCurrency amountByCurrency: fleetFare.tollFeeDriverCanInput){
                    if(amountByCurrency.currencyISO.equals(booking.currencyISO))
                        tollFeeDriverCanInput = amountByCurrency.value;
                }
            }
            if(parkingFeeLimitDriverInputActive && fleetFare.parkingFeeDriverCanInput != null){
                for(AmountByCurrency amountByCurrency: fleetFare.parkingFeeDriverCanInput){
                    if(amountByCurrency.currencyISO.equals(booking.currencyISO))
                        parkingFeeDriverCanInput = amountByCurrency.value;
                }
            }
            if(gasFeeLimitDriverInputActive && fleetFare.gasFeeDriverCanInput != null){
                for(AmountByCurrency amountByCurrency: fleetFare.gasFeeDriverCanInput){
                    if(amountByCurrency.currencyISO.equals(booking.currencyISO))
                        gasFeeDriverCanInput = amountByCurrency.value;
                }
            }

            if(otherFeeLimitDriverInputActive && fleetFare.otherFee.otherFeeDriverCanInput != null){
                for(AmountByCurrency amountByCurrency: fleetFare.otherFee.otherFeeDriverCanInput){
                    if(amountByCurrency.currencyISO.equals(booking.currencyISO))
                        otherFeeDriverCanInput = amountByCurrency.value;
                }
            }
        }

        if(tollFeeLimitDriverInputActive && listValues.get(0) > tollFeeDriverCanInput)
            return 517;
        if(otherFeeLimitDriverInputActive && listValues.get(1) > otherFeeDriverCanInput)
            return 517;
        if(parkingFeeLimitDriverInputActive && listValues.get(2) > parkingFeeDriverCanInput)
            return 517;
        if(gasFeeLimitDriverInputActive && listValues.get(3) > gasFeeDriverCanInput)
            return 517;
        return 200;
    }

    Map<String,String> getLocationFromAddress(String address, String requestId){
        Map<String,String> map = new HashMap<>();
        try {
            String url = "";
            String username = "";
            String password = "";
            if (ServerConfig.maps_provider_server != null && !ServerConfig.maps_provider_server.isEmpty()) {
                url = ServerConfig.maps_provider_server + "/api/mapProvider/geoCoding?address=" + URLEncoder.encode(address, "UTF-8") + "&channel=payment-authorize";
                username = ServerConfig.maps_provider_userName;
                password = ServerConfig.maps_provider_password;
            }
            logger.debug(requestId + " - url: " + url);
            HttpResponse<JsonNode> jsonResponse = Unirest.get(url)
                    .basicAuth(username, password)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .asJson();
            org.json.JSONObject objectResponse = jsonResponse.getBody().getObject();
            String status = "";
            if (objectResponse != null && objectResponse.has("status") && !objectResponse.isNull("status"))
                status = objectResponse.get("status").toString();

            if (status.equals("OK") && objectResponse != null && objectResponse.has("results") && !objectResponse.isNull("results")) {
                return extractLocation(requestId, objectResponse);
            }
            logger.debug(requestId + " - status: " + status);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    Map<String,String> getLocationFromGeoLocation(List<Double> geoLocation, String requestId){
        Map<String,String> map = new HashMap<>();
        try {
            String url = "";
            String username = "";
            String password = "";
            if (ServerConfig.maps_provider_server != null && !ServerConfig.maps_provider_server.isEmpty()) {
                String latlng = geoLocation.get(1) + "," + geoLocation.get(0);
                url = ServerConfig.maps_provider_server + "/api/mapProvider/geoCoding?latlng=" + latlng + "&channel=payment-authorize";
                username = ServerConfig.maps_provider_userName;
                password = ServerConfig.maps_provider_password;
            }
            logger.debug(requestId + " - url: " + url);
            HttpResponse<JsonNode> jsonResponse = Unirest.get(url)
                    .basicAuth(username, password)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .asJson();
            org.json.JSONObject objectResponse = jsonResponse.getBody().getObject();
            String status = "";
            if (objectResponse != null && objectResponse.has("status") && !objectResponse.isNull("status"))
                status = objectResponse.get("status").toString();

            if (status.equals("OK") && objectResponse != null && objectResponse.has("results") && !objectResponse.isNull("results")) {
                return extractLocation(requestId, objectResponse);
            }
            logger.debug(requestId + " - status: " + status);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    private Map<String,String> extractLocation(String requestId, org.json.JSONObject objectResponse) {
        Map<String,String> map = new HashMap<>();
        org.json.JSONArray arrResult = (org.json.JSONArray)objectResponse.get("results");
        String lat = "";
        String lng = "";
        String street = "";
        String city = "";
        String state = "";
        String postalCode = "";
        String country = "";
        for (int i = 0; i < arrResult.length(); i++) {
            org.json.JSONObject jsonObject = arrResult.getJSONObject(i);
            if (jsonObject != null && jsonObject.has("geometry") && !jsonObject.isNull("geometry")) {
                org.json.JSONObject jsonGeometry = jsonObject.getJSONObject("geometry");
                org.json.JSONObject jsonLocation = jsonGeometry.getJSONObject("location");
                /*logger.debug(requestId + " - lat: " + jsonLocation.get("lat"));
                logger.debug(requestId + " - lng: " + jsonLocation.get("lng"));*/
                lat = jsonLocation.get("lat").toString();
                lng = jsonLocation.get("lng").toString();
            }

            if (jsonObject != null && jsonObject.has("address_components") && !jsonObject.isNull("address_components")) {
                org.json.JSONArray arrAddress = jsonObject.getJSONArray("address_components");
                for (int j = 0; j < arrAddress.length(); j++) {
                    org.json.JSONObject jsonAddress = arrAddress.getJSONObject(j);
                    org.json.JSONArray arrTypes = jsonAddress.getJSONArray("types");

                    if(arrTypes.get(0).toString().equals("street_number")) {
                        //logger.debug(requestId + " - number: " + jsonAddress.get("long_name"));
                        street = jsonAddress.get("long_name").toString();
                    }
                    if(arrTypes.get(0).toString().equals("route")) {
                                /*logger.debug(requestId + " - street: " + jsonAddress.get("long_name"));*/
                        street += " " + jsonAddress.get("long_name").toString();
                    }
                    if(arrTypes.get(0).toString().equals("locality")) {
                                /*logger.debug(requestId + " - city: " + jsonAddress.get("long_name"));*/
                        city = jsonAddress.get("long_name").toString();
                    }
                    if(arrTypes.get(0).toString().equals("administrative_area_level_1")) {
                                /*logger.debug(requestId + " - state: " + jsonAddress.get("long_name"));*/
                        if (city.isEmpty()) {
                            city = jsonAddress.get("long_name").toString();
                        } else {
                            state = jsonAddress.get("long_name").toString();
                        }
                    }
                    if(arrTypes.get(0).toString().equals("postal_code")) {
                                /*logger.debug(requestId + " - postalCode: " + jsonAddress.get("long_name"));*/
                        postalCode = jsonAddress.get("long_name").toString();
                    }
                    if(arrTypes.get(0).toString().equals("country")) {
                                /*logger.debug(requestId + " - country: " + jsonAddress.get("long_name"));*/
                        country = jsonAddress.get("long_name").toString();
                    }
                }
            }
        }
        street = street.replace("Unnamed Road", "");
        if (country.equals("Mexico") && postalCode.isEmpty()) {
            postalCode = "78230";
        }
        if (state.equals(city))
            state = "";
        if (country.equals("Mexico") && state.isEmpty()) {
            state = city;
        }
        logger.debug(requestId + " - lat: " + lat);
        logger.debug(requestId + " - lng: " + lng);
        logger.debug(requestId + " - street: " + street);
        logger.debug(requestId + " - city: " + city);
        logger.debug(requestId + " - state: " + state);
        logger.debug(requestId + " - postalCode: " + postalCode);
        logger.debug(requestId + " - country: " + country);
        map.put("lat", lat);
        map.put("lng", lng);
        map.put("street", street.trim());
        map.put("city", city);
        map.put("state", state);
        map.put("postalCode", postalCode);
        map.put("country", country);
        return map;
    }

    double roundingValue(double value, Rounding rounding, String currencyISO){
        if (rounding != null && rounding.enable && rounding.roundingByCurrencies != null){
            for(RoundingByCurrency r: rounding.roundingByCurrencies){
                if(r.currencyISO.equals(currencyISO)){
                    value = ConvertUtil.extendedRound(value, r.mode, r.digits);
                    break;
                }
            }
        }
        return value;
    }

    String storeFile(String requestId, String fleetId, String fileUrl) {
        String tempFile = "";
        try {
            tempFile = "/tmp/" + fleetId + Calendar.getInstance().getTimeInMillis() + ".png";

            BufferedInputStream inputStream = new BufferedInputStream(new URL(fileUrl).openStream());
            FileOutputStream fileOS = new FileOutputStream(tempFile);
            byte data[] = new byte[1024];
            int byteContent;
            while ((byteContent = inputStream.read(data, 0, 1024)) != -1) {
                fileOS.write(data, 0, byteContent);
            }
            logger.debug(requestId + " - create file " + tempFile + " done !!!");
        } catch(Exception ex) {
            ex.printStackTrace();
            logger.debug(requestId + " - write tmp file error !!!");
        }
        return tempFile;
    }

    PaymentEnt getPaymentDataForIntercityBooking(String requestId, Booking booking, Fleet fleet) {
        PaymentEnt paymentEnt = new PaymentEnt();
        paymentEnt.requestId = requestId;
        String paymentType = convertRequestPaymentType(booking.request.paymentType);

        Fare fare = booking.request.estimate.fare;
        paymentEnt.fleetId = booking.fleetId;
        paymentEnt.bookId = booking.bookId;
        paymentEnt.paymentType = paymentType;
        paymentEnt.total = fare.etaFare;
        paymentEnt.totalCharged = fare.etaFare;
        paymentEnt.subTotal = fare.subTotal;
        paymentEnt.subTotalCharged = fare.subTotal;
        paymentEnt.techFee = fare.techFee;
        paymentEnt.fare = fare.basicFare;
        paymentEnt.calBasicFare = fare.basicFare;
        paymentEnt.tip = fare.tip;
        paymentEnt.tax = fare.tax;
        paymentEnt.promoCode = fare.promoCode;
        paymentEnt.promoAmount = fare.promoAmount;
        paymentEnt.heavyTraffic = 0.0;
        paymentEnt.airportSurcharge = fare.airportFee;
        paymentEnt.meetDriverFee = fare.meetDriverFee;
        paymentEnt.rushHour = fare.rushHourFee;
        paymentEnt.partnerCommission = fare.bookingFee;
        paymentEnt.otherFees = fare.otherFees;
        paymentEnt.tollFee = fare.tollFee;
        paymentEnt.parkingFee = fare.parkingFee;
        paymentEnt.gasFee = fare.gasFee;
        if(fleet.additionalFees != 1)
            paymentEnt.serviceFee = fare.serviceFee;
        paymentEnt.dynamicSurcharge = fare.dynamicSurcharge;
        paymentEnt.dynamicFare = fare.dynamicFare != null ? fare.dynamicFare : 0.0;
        paymentEnt.creditTransactionFee = fare.creditTransactionFee;
        paymentEnt.isMinimum = 0;
        paymentEnt.otherFeesDetails = "";
        return paymentEnt;
    }

    PaymentEnt getPaymentDataForFoodMartBooking(String requestId, Booking booking) {
        PaymentEnt paymentEnt = new PaymentEnt();
        paymentEnt.requestId = requestId;
        int requestType = booking.request != null && booking.request.paymentType != null ? booking.request.paymentType : 0;
        String paymentType = convertRequestPaymentType(requestType);

        Fare fare = booking.request.estimate.fare;
        paymentEnt.fleetId = booking.fleetId;
        paymentEnt.bookId = booking.bookId;
        paymentEnt.paymentType = paymentType;
        paymentEnt.total = fare.etaFare;
        paymentEnt.totalCharged = fare.etaFare;
        paymentEnt.subTotal = fare.subTotal;
        paymentEnt.subTotalCharged = fare.subTotal;
        paymentEnt.itemValue = fare.itemValue;
        paymentEnt.techFee = fare.techFee;
        paymentEnt.fare = fare.basicFare;
        paymentEnt.calBasicFare = fare.basicFare;
        paymentEnt.tip = fare.tip;
        paymentEnt.tax = fare.tax;
        paymentEnt.promoCode = fare.promoCode;
        paymentEnt.promoAmount = fare.promoAmount;
        paymentEnt.heavyTraffic = 0.0;
        paymentEnt.airportSurcharge = fare.airportFee;
        paymentEnt.meetDriverFee = fare.meetDriverFee;
        paymentEnt.rushHour = fare.rushHourFee;
        paymentEnt.partnerCommission = fare.bookingFee;
        paymentEnt.otherFees = fare.otherFees;
        paymentEnt.tollFee = fare.tollFee;
        paymentEnt.parkingFee = fare.parkingFee;
        paymentEnt.gasFee = fare.gasFee;
        paymentEnt.serviceFee = fare.serviceFee;
        paymentEnt.dynamicSurcharge = fare.dynamicSurcharge;
        paymentEnt.dynamicFare = fare.dynamicFare != null ? fare.dynamicFare : 0.0;
        paymentEnt.creditTransactionFee = fare.creditTransactionFee;
        paymentEnt.isMinimum = 0;
        paymentEnt.otherFeesDetails = "";
        return paymentEnt;
    }

    String convertRequestPaymentType(int paymentType) {
        String returnType = "";
        switch (paymentType) {
            case 1: {
                returnType = KeysUtil.PAYMENT_CASH;
            }
            break;
            case 2: {
                returnType = KeysUtil.PAYMENT_CARD;
            }
            break;
            case 3: {
                returnType = KeysUtil.PAYMENT_MDISPATCHER;
            }
            break;
            case 4: {
                returnType = KeysUtil.PAYMENT_CORPORATE;
            }
            break;
            case 5: {
                returnType = KeysUtil.PAYMENT_DIRECTBILLING;
            }
            break;
            case 6: {
                returnType = KeysUtil.PAYMENT_FLEETCARD;
            }
            break;
            case 7: {
                returnType = KeysUtil.PAYMENT_PREPAID;
            }
            break;
            case 12: {
                returnType = KeysUtil.PAYMENT_CARD_EXTERNAL;
            }
            break;
            case 13: {
                returnType = KeysUtil.PAYMENT_PAXWALLET;
            }
            break;
            case 14: {
                returnType = KeysUtil.PAYMENT_TNG_WALLET;
            }
            break;
            case 20: {
                returnType = KeysUtil.PAYMENT_VIPPS;
            }
            break;
        }
        return returnType;
    }

    JSONObject enableCashExcess(String fleetId, String currencyISO) {
        boolean enableCashExcess = false;
        double maxTransfer = 0.0;
        double warningAmount = 0.0;
        double allowanceAmount = 0.0;
        PricingPlan pricingPlan = mongoDao.getPricingPlan(fleetId);
        if(pricingPlan != null){
            enableCashExcess = pricingPlan.cashExcessFlow;
            if (pricingPlan.driverDeposit != null && pricingPlan.driverDeposit.enable) {
                List<AmountByCurrency> warningAmounts = pricingPlan.driverDeposit.valueByCurrencies;
                if (warningAmounts != null && !warningAmounts.isEmpty()) {
                    warningAmount = warningAmounts.stream().filter(a -> a.currencyISO.equalsIgnoreCase(currencyISO))
                            .findFirst().map(a -> a.value).orElse(0.0);
                }
                List<AmountByCurrency> maxTransfers = pricingPlan.driverDeposit.maxTransferByCurrencies;
                if (maxTransfers != null && !maxTransfers.isEmpty()) {
                    maxTransfer = maxTransfers.stream().filter(a -> a.currencyISO.equalsIgnoreCase(currencyISO))
                            .findFirst().map(amountByCurrency -> amountByCurrency.value).orElse(0.0);
                }
                List<AmountByCurrency> allowanceAmounts = pricingPlan.driverDeposit.allowanceByCurrencies;
                if (allowanceAmounts != null && !allowanceAmounts.isEmpty()) {
                    allowanceAmount = allowanceAmounts.stream().filter(a -> a.currencyISO.equalsIgnoreCase(currencyISO))
                            .findFirst().map(amountByCurrency -> amountByCurrency.value).orElse(0.0);
                }
            }
        }
        JSONObject response = new JSONObject();
        response.put("enableCashExcess", enableCashExcess);
        response.put("maxTransfer", maxTransfer);
        response.put("warningAmount", warningAmount);
        response.put("allowanceAmount", allowanceAmount);
        return response;
    }

    String getCurrencySymbol(String currencyISO, List<Currency> listCurrencies) {
        String currencySymbol = "";
        for (com.qupworld.paymentgateway.models.mongo.collections.fleet.Currency currency : listCurrencies) {
            if (currency.iso.equals(currencyISO)) {
                currencySymbol = currency.symbol;
                break;
            }
        }
        return currencySymbol;
    }

    String getPayoutGateway(Fleet fleet) {
        // check setting payout
        boolean enablePayout = false;
        CCPackages ccPackages = mongoDao.getCCPackages(fleet.fleetId);
        if(ccPackages != null){
            for(CCModule module : ccPackages.modules){
                if((module.key.equals("DriverPayout") || module.key.equals("Payout")) && module.enable){
                    enablePayout = true;
                    break;
                }
            }
        }
        String gateway = "";
        if (enablePayout) {
            // check fixed setting for some special fleets
            List<String> fixMOLPay = Arrays.asList("gojosg");
            if (fixMOLPay.contains(fleet.fleetId)) {
                return KeysUtil.MOLPAY;
            }
            List<String> fixDNB = Arrays.asList("gojonorway", "demo-norway", "vipps");
            if (fixDNB.contains(fleet.fleetId)) {
                return KeysUtil.DNB;
            }
            String payoutTarget = fleet.drvPayout != null && fleet.drvPayout.gateway != null ? fleet.drvPayout.gateway : "RazerPay"; // default = RazerPay

            switch (payoutTarget) {
                case "RazerPay": {
                    WalletMOLPay walletMOLPay = mongoDao.getWalletMOLPay(fleet.fleetId);
                    if (walletMOLPay != null) {
                        gateway = KeysUtil.MOLPAY;
                    }
                }
                break;
                case KeysUtil.DNB :
                case KeysUtil.INDIA_DEFAULT :
                case KeysUtil.ANY_BANK : {
                    gateway = payoutTarget;
                }
                break;
                case "credit": {
                    if (fleet.creditConfig != null && !fleet.creditConfig.multiGateway) {
                        ConfigGateway configGateway = fleet.creditConfig.configGateway.size() > 0 ? fleet.creditConfig.configGateway.get(0) : null;
                        if (configGateway != null) {
                            gateway = configGateway.gateway;
                        }
                    }
                }
                break;
                case KeysUtil.STRIPE_CONNECT : {
                    gateway = KeysUtil.STRIPE;
                }
            }
        } else {
            if (fleet.creditConfig != null && !fleet.creditConfig.multiGateway) {
                ConfigGateway configGateway = fleet.creditConfig.configGateway.size() > 0 ? fleet.creditConfig.configGateway.get(0) : null;
                if (configGateway != null) {
                    gateway = configGateway.gateway;
                }
            }
        }
        return gateway;
    }

    boolean checkIncentiveReferralWithPromo(String fleetId, String promoCode) {
        PromotionCode promotionCode = mongoDao.getPromoCodeByFleetAndName(fleetId, promoCode);
        if(promotionCode!= null && promotionCode.referredCustomers){
            Referral referral = mongoDao.getReferral(fleetId);
            if(referral!= null && referral.paidByDriver && referral.firstPackage != null && referral.firstPackage.promos != null &&
                    !referral.firstPackage.promos.isEmpty()){
                for(RefereePromo p:referral.firstPackage.promos){
                    if(p.promoId.equals(promotionCode._id)){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    boolean paidByDriver(String fleetId, String promoCode) {
        PaxReferral paxReferral = mongoDao.getPaxReferral(fleetId);
        if (paxReferral != null && paxReferral.refererGet != null) {
            String type = paxReferral.refereeGet.type != null ? paxReferral.refereeGet.type : "";
            if (type.equals("promo") && paxReferral.refereeGet.promos != null) {
                for(RefereePromo p:paxReferral.refereeGet.promos){
                    String code = p.promoCode != null ? p.promoCode : "";
                    if (code.equalsIgnoreCase(promoCode)) {
                        return paxReferral.refereeGet.paidByDriver;
                    }
                }
            }
        }
        return false;
    }

    public double calGrossEarning(Booking booking, String requestId, Fleet fleet, String customerId, String driverId, String promoCode, boolean shortTrip,
            double subTotal, double promoAmount, double tip, double airportFee, double meetDriverFee, double tollFee, double parkingFee, double gasFee) {
        try {
            double grossEarning = 0;
            String fleetId = fleet.fleetId;
            // if setting airport fee, meet driver, toll fee, parking fee, gas fee are pay to driver then get these values to calculate gross earning
            FleetFare fleetFare = mongoDao.getFleetFare(fleetId);

            Zone zone = mongoDao.findByFleetIdAndGeo(fleetId, booking.request.pickup.geo);
            String zoneId = "";
            if(zone!= null)
                zoneId = zone._id.toString();
            ServiceFee sF = mongoDao.getServiceFeeByFleetIdAndZoneId(fleetId, zoneId);
            int tollFeePayTo;
            int parkingFeePayTo;
            int gasFeePayTo;
            String applyType = fleetFare.applyType != null ? fleetFare.applyType : "all";
            if (sF != null && fleet.generalSetting.allowCongfigSettingForEachZone && !applyType.equals("all")) {
                tollFeePayTo = sF.tollFeePayTo;
                parkingFeePayTo = sF.parkingFeePayTo;
                gasFeePayTo = sF.gasFeePayTo;
            } else {
                tollFeePayTo = fleetFare.tollFeePayTo;
                parkingFeePayTo = fleetFare.parkingFeePayTo;
                gasFeePayTo = fleetFare.gasFeePayTo;
            }
            double driverTollFee = 0.0;
            if (tollFeePayTo == 0){
                driverTollFee = tollFee;
            }
            double driverParkingFee = 0.0;
            if (parkingFeePayTo == 0){
                driverParkingFee = parkingFee;
            }
            double driverGasFee = 0.0;
            if (gasFeePayTo == 0){
                driverGasFee = gasFee;
            }
            double driverAirportFee = 0.0;
            if(fleetFare != null && fleetFare.airport != null && fleetFare.airport.payTo == 0){
                driverAirportFee = airportFee;
            }
            double driverMeetGreet = 0.0;
            if(fleetFare != null && fleetFare.meetDriver != null && fleetFare.meetDriver.payTo == 0){
                driverMeetGreet = meetDriverFee;
            }
            List<Double> fleetServiceValues = getFleetServiceFees(requestId, booking, 0.0, subTotal);
            double driverCommissionFromFleetServiceFee = fleetServiceValues.get(2);

            // check if booking is customize driver earning
            if (booking.request.estimate != null && booking.request.estimate.fare != null) {
                Fare fare = booking.request.estimate.fare;
                String driverEarningType = fare.driverEarningType != null ? fare.driverEarningType : "default";
                if (!driverEarningType.equals("default")) {
                    logger.debug(requestId + " - editedDriverEarning: " + fare.editedDriverEarning);
                    grossEarning =  fare.editedDriverEarning + tip + driverAirportFee + driverMeetGreet +
                            driverTollFee + driverParkingFee + driverGasFee + driverCommissionFromFleetServiceFee;
                    return grossEarning;
                }
            }

            double ridePayment = 0;
            // get referralHistory to check referral program
            ReferralHistories referralHistories = mongoDao.getLinkReferralByRefereeId(customerId);
            boolean isNewIncentiveReferralWithPromo = false;
            boolean paidByDriver = false;
            String referrerType = (referralHistories != null && referralHistories.referralType != null) ? referralHistories.referralType : KeysUtil.APPTYPE_DRIVER; // default = driver for old ReferralHistories
            if (referrerType.equals(KeysUtil.APPTYPE_DRIVER)) {
                isNewIncentiveReferralWithPromo = checkIncentiveReferralWithPromo(fleetId, promoCode);
            } else if (referrerType.equals(KeysUtil.APPTYPE_PASSENGER)) {
                paidByDriver = paidByDriver(fleetId, promoCode);
            }
            logger.debug(requestId + " - isNewIncentiveReferralWithPromo: " + isNewIncentiveReferralWithPromo);
            logger.debug(requestId + " - paidByDriver: " + paidByDriver);
            //calculator commission of driver
            Account account = mongoDao.getAccount(driverId);
            Map<String, Object> mapCommission = estimateUtil.getFleetCommission(requestId, fleet, account, booking, zoneId);
            String commissionType = mapCommission.get("commissionType").toString();
            double commission = Double.parseDouble(mapCommission.get("commission").toString());
            logger.debug(requestId + " - commission type: " + commissionType + " --- value: " + commission);

            double comm = subTotal * (commission * 0.01);
            if (commissionType.equalsIgnoreCase("amount"))
                comm = commission;
            if(isNewIncentiveReferralWithPromo || paidByDriver){
                comm = (subTotal - promoAmount) * (commission * 0.01);
                if (commissionType.equalsIgnoreCase("amount"))
                    comm = commission;
            }
            logger.debug(requestId + " - comm: " + comm);

            if(shortTrip) {
                logger.debug(requestId + " - shortTrip = true => reset comm = 0.0 ");
                comm = 0;
            }
            // calculator ride payment and gross earning
            ridePayment = subTotal - comm;
            if (isNewIncentiveReferralWithPromo || paidByDriver) {
                ridePayment = subTotal - promoAmount - comm;
            }
            grossEarning = ridePayment + tip + driverAirportFee + driverMeetGreet + driverTollFee + driverParkingFee + driverGasFee + driverCommissionFromFleetServiceFee;
            return grossEarning;
        } catch (Exception ex) {
            logger.debug(requestId + " - calGrossEarning = " + CommonUtils.getError(ex));
            return 0;
        }
    }
    public String processTopupFromNotification(String requestId, String transactionId, TopupByWalletEnt topupEnt, double chargeAmount, String topupType, String method, JSONObject additionalData) {
        String topUpResult = "";
        String topupTo = topupEnt.topupTo != null ? topupEnt.topupTo : "";
        if (topupTo.equals("paxWallet")) {
            logger.debug(requestId + " - process top-up paxWallet");

            TopupPaxWalletEnt topupPaxEnt = new TopupPaxWalletEnt();
            topupPaxEnt.type = topupType;
            topupPaxEnt.requestId = requestId;
            topupPaxEnt.token = "";
            topupPaxEnt.fleetId = topupEnt.fleetId;
            topupPaxEnt.userId = topupEnt.userId;
            topupPaxEnt.currencyISO = topupEnt.currencyISO;
            topupPaxEnt.amount = chargeAmount;
            topupPaxEnt.from = "app";
            topupPaxEnt.reason = "";
            topupPaxEnt.gateway = topupEnt.gateway != null ? topupEnt.gateway : "";

            topupPaxEnt.parameter = additionalData.get("parameter") != null ? additionalData.get("parameter").toString() : "";;
            topupPaxEnt.channel = additionalData.get("channel") != null ? additionalData.get("channel").toString() : "";

            Map<String, String> mapResponse = new HashMap<>();
            mapResponse.put("transactionId", transactionId);
            ObjResponse objResponse = new ObjResponse();
            objResponse.returnCode = 200;
            objResponse.response = mapResponse;
            Account account = mongoDao.getAccount(topupEnt.userId);
            Fleet fleet = mongoDao.getFleetInfor(topupEnt.fleetId);
            topUpResult = doTopupPaxWallet(topupPaxEnt, objResponse.toString(), account, fleet);
        } else {
            logger.debug(requestId + " - process top-up driver credit wallet");
            topupTo = "wallet";
            UpdateDriverCreditBalance updateBalance = new UpdateDriverCreditBalance();
            updateBalance.type = topupType;
            updateBalance.fleetId = topupEnt.fleetId;
            updateBalance.driverId = topupEnt.userId;
            updateBalance.amount = topupEnt.amount;
            updateBalance.currencyISO = topupEnt.currencyISO;
            updateBalance.mongoDao = mongoDao;
            updateBalance.sqlDao = sqlDao;
            updateBalance.reason = transactionId;
            updateBalance.requestId = requestId;
            updateBalance.transactionId = transactionId;
            updateBalance.method = method;
            updateBalance.walletName = topupEnt.gateway != null ? topupEnt.gateway : "";

            String channel = additionalData.get("channel") != null ? additionalData.get("channel").toString() : "";
            updateBalance.additionalData.put("channel", channel);
            topUpResult = updateBalance.doUpdate();
        }
        logger.debug(requestId + " - processTopup result = " + topUpResult);
        // save top-up result for success status only
        int returnCode = CommonUtils.getReturnCode(topUpResult);
        String status = returnCode == 200 ? "success" : "failure";
        // call to Jupiter to push notification
        sendTopupResultToDispatch(topupEnt.userId, transactionId, status, returnCode, topupTo, requestId);
        redisDao.unlockTopup(topupEnt.userId, requestId);
        return topUpResult;
    }

    public String doTopupPaxWallet(TopupPaxWalletEnt topupEnt, String jsonData, Account account, Fleet fleet) {
        logger.debug(topupEnt.requestId + " - doTopupPaxWallet: " + gson.toJson(topupEnt));
        ObjResponse objResponse = new ObjResponse();
        try {
            PaxWalletUtil paxWalletUtil = new PaxWalletUtil(topupEnt.requestId);
            double currentBalance = paxWalletUtil.getCurrentBalance(topupEnt.fleetId, topupEnt.userId, "paxWallet", topupEnt.currencyISO);
            logger.debug(topupEnt.requestId + " - currentBalance: " + currentBalance);
            double newBalance = KeysUtil.DECIMAL_FORMAT.parse(KeysUtil.DECIMAL_FORMAT.format(currentBalance + topupEnt.amount)).doubleValue();
            logger.debug(topupEnt.requestId + " - newBalance: " + newBalance);

            String sortName = "";
            if (KeysUtil.MOLPAY.equals(topupEnt.gateway)) {
                String channel = topupEnt.channel != null ? topupEnt.channel : "";
                UpdateDriverCreditBalance updateBalance = new UpdateDriverCreditBalance();
                updateBalance.mongoDao = mongoDao;
                sortName = updateBalance.getChannelLog(topupEnt.requestId, fleet, "PaxWallet", KeysUtil.MOLPAY, channel.toLowerCase());
                logger.debug(topupEnt.requestId + " - sortName: " + sortName);
            }
            PaxWalletTransaction paxWalletTransaction = new PaxWalletTransaction();
            paxWalletTransaction.fleetId = topupEnt.fleetId;
            paxWalletTransaction.userId = topupEnt.userId;
            paxWalletTransaction.transactionType = topupEnt.type;
            paxWalletTransaction.amount = topupEnt.amount;
            paxWalletTransaction.newBalance = newBalance;
            String from = topupEnt.from != null ? topupEnt.from : "";
            String reason = topupEnt.reason != null ? topupEnt.reason : "";
            paxWalletTransaction.reason = reason;
            if (from.equals("cc")) {
                paxWalletTransaction.description = "editBalance-" + reason;
                paxWalletTransaction.transactionId = GatewayUtil.getOrderId();
            } else if (from.equals("driverWallet")){
                // get payment data
                JSONObject json = gson.fromJson(jsonData, JSONObject.class);
                String bookId = json.get("bookId") != null ? json.get("bookId").toString() : "";
                double givenAmount = json.get("givenAmount") != null ? Double.parseDouble(json.get("givenAmount").toString()) : 0.0;
                double bookingFare = json.get("bookingFare") != null ? Double.parseDouble(json.get("bookingFare").toString()) : 0.0;
                String transactionId = json.get("transactionId") != null ? json.get("transactionId").toString() : GatewayUtil.getOrderId();
                paxWalletTransaction.description = "driverWallet";
                paxWalletTransaction.transactionId = transactionId;
                paxWalletTransaction.bookId = bookId;
                paxWalletTransaction.givenAmount = givenAmount;
                paxWalletTransaction.bookingFare = bookingFare;
            } else if (from.equals("voucher")) {
                paxWalletTransaction.description = "voucherRedeem-" + reason;
                paxWalletTransaction.transactionId = reason;
            } else {
                // from = app
                JSONObject objectResult = toObjectResponse(jsonData);
                JSONObject response = (JSONObject) objectResult.get("response");
                if (account == null)
                    account = mongoDao.getAccount(topupEnt.userId);
                if (topupEnt.type.equals("credit")) {
                    String cardMask = "";
                    if (account.credits != null && !account.credits.isEmpty()) {
                        for (Credit credit : account.credits) {
                            if (credit.localToken.equals(topupEnt.token)) {
                                cardMask = credit.cardMask;
                                break;
                            }
                        }
                    }
                    // return last 4 of card number only
                    if (cardMask.length() > 4) {
                        cardMask = cardMask.substring(cardMask.length() - 4);
                    }
                    String transId = response.get("transId") != null ? response.get("transId").toString() : "";
                    paxWalletTransaction.description = "topup-" + transId;
                    paxWalletTransaction.transactionId = transId;
                    paxWalletTransaction.cardNumber = cardMask;
                } else if (topupEnt.type.equals(CommonArrayUtils.PAX_WALLET_TYPE[16])) {
                    String transId = CommonUtils.getValue(jsonData, "transactionId");
                    paxWalletTransaction.description = "topup-" + transId;
                    paxWalletTransaction.walletName = topupEnt.gateway;
                    paxWalletTransaction.transactionId = transId;
                } else if (topupEnt.type.equals(KeysUtil.PAYMENT_APPLEPAY) || topupEnt.type.equals(KeysUtil.PAYMENT_GOOGLEPAY)) {
                    String transId = CommonUtils.getValue(jsonData, "transactionId");
                    paxWalletTransaction.description = topupEnt.type + "-" + transId;
                    paxWalletTransaction.transactionId = transId;
                } else {
                    String transactionId = response.get("transactionId") != null ? response.get("transactionId").toString() : "";
                    paxWalletTransaction.description = "topup-" + transactionId;
                    paxWalletTransaction.transactionId = transactionId;
                }
            }
            paxWalletTransaction.channelLog = sortName;
            paxWalletTransaction.customerName = account.fullName != null ? account.fullName.trim() : "";
            paxWalletTransaction.phoneNumber = account.phone.equals(account.userId) ? "" : account.phone;
            paxWalletTransaction.currencyISO = topupEnt.currencyISO;
            paxWalletTransaction.currencySymbol = getCurrencySymbol(topupEnt.currencyISO, fleet.currencies);
            paxWalletTransaction.createdDate = TimezoneUtil.getGMTTimestamp();

            long idAddBalance = sqlDao.addPaxWalletTransaction(paxWalletTransaction);
            if (idAddBalance > 0) {
                if (topupEnt.amount > 0) {
                    paxWalletTransaction.amount = topupEnt.amount;
                    paxWalletUtil.updatePaxWallet(paxWalletTransaction, true);
                } else {
                    paxWalletTransaction.amount = -topupEnt.amount; // convert to positive number for in/out cash flow
                    paxWalletUtil.updatePaxWallet(paxWalletTransaction, false);
                }
                // update wallet balance
                PassengerInfo passengerInfo = mongoDao.getPassengerInfo(topupEnt.fleetId, topupEnt.userId);
                List<AmountByCurrency> listWallets = passengerInfo.paxWallet;
                AmountByCurrency paxWallet = null;
                boolean isExist = false;
                for (AmountByCurrency wallet : listWallets) {
                    if (wallet.currencyISO != null && wallet.currencyISO.equals(topupEnt.currencyISO)) {
                        wallet.value = newBalance;
                        paxWallet = wallet;
                        isExist = true;
                        break;
                    }
                }
                if (!isExist) {
                    AmountByCurrency wallet = new AmountByCurrency();
                    wallet.value = topupEnt.amount;
                    wallet.currencyISO = topupEnt.currencyISO;
                    listWallets.add(wallet);
                    paxWallet = wallet;
                }

                mongoDao.updatePaxWallet(topupEnt.fleetId, topupEnt.userId, listWallets);

                // update to Reporting
                paxWalletTransaction.amount = topupEnt.amount; // reset original top-up value before sending to Report
                UpdatePaxWalletThread updateThread = new UpdatePaxWalletThread();
                paxWalletTransaction.id = idAddBalance;
                updateThread.paxWalletTransaction = paxWalletTransaction;
                updateThread.requestId = topupEnt.requestId;
                updateThread.start();

                String type = topupEnt.type != null ? topupEnt.type : "";
                String method = "";
                switch (type) {
                    case "credit":
                    case "applePay":
                    case "googlePay":
                        method = "credit";
                        break;
                    case "topUpMOLPay":
                        method = "razerPay";
                        break;
                    case "topUpWallet" : {
                        String gateway = topupEnt.gateway != null ? topupEnt.gateway : "";
                        switch (gateway) {
                            case KeysUtil.ZAINCASH:
                                method = "zainCash";
                                break;
                            case KeysUtil.GCASH:
                                method = "gCash";
                                break;
                            case KeysUtil.KUSHOK:
                                method = "kushok";
                                break;
                            case KeysUtil.TELEBIRR:
                                method = "telebirr";
                                break;
                            case KeysUtil.KSHER:
                                method = "ksher";
                                break;
                            case KeysUtil.PAYTM:
                                method = "payTM";
                                break;
                            case KeysUtil.BOG:
                                method = "bog";
                                break;
                        }
                    }
                    break;
                }

                // send notify to Point System
                if (methodIncreasePoint.contains(method)) {
                    // send notify to Point System
                    JSONObject objBody = new JSONObject();
                    objBody.put("fleetId", topupEnt.fleetId);
                    objBody.put("userId", topupEnt.userId);
                    objBody.put("appType", "passenger");
                    objBody.put("amount", topupEnt.amount);
                    objBody.put("currencyISO", topupEnt.currencyISO);
                    objBody.put("method", method);
                    objBody.put("transactionId", paxWalletTransaction.transactionId);
                    logger.debug(topupEnt.requestId + " - URL: " + PaymentUtil.notify_topup);
                    logger.debug(topupEnt.requestId + " - data: " + objBody.toJSONString());
                    int returnCode = CommonUtils.sendPointSystem(topupEnt.requestId, objBody.toJSONString(),
                            PaymentUtil.notify_topup, ServerConfig.point_system_user, ServerConfig.point_system_pass);
                    logger.debug(topupEnt.requestId + " - pointSystemResponse: " + returnCode);
                    if (returnCode != 200) {
                        // something went wrong!!!
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException ignore) {}
                        returnCode = CommonUtils.sendPointSystem(topupEnt.requestId, objBody.toJSONString(),
                                PaymentUtil.notify_topup, ServerConfig.point_system_user, ServerConfig.point_system_pass);
                        logger.debug(topupEnt.requestId + " - pointSystemResponse: " + returnCode);
                    }
                }

                Map<String, Object> mapResponse = new HashMap<>();
                mapResponse.put("paxWallet", paxWallet);
                mapResponse.put("id", idAddBalance);
                objResponse.returnCode = 200;
                objResponse.message = "Success";
                objResponse.response = mapResponse;
            } else {
                objResponse.returnCode = 525;
                objResponse.message = "Something went wrong. Please try again later.";
            }

            redisDao.unlockTopup(topupEnt.userId, topupEnt.requestId);
            return objResponse.toString();
        } catch (Exception ex) {
            redisDao.unlockTopup(topupEnt.userId, topupEnt.requestId);
            logger.debug(topupEnt.requestId + " - doTopupPaxWallet exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            objResponse.message = "Something went wrong. Please try again later.";
            return objResponse.toString();
        }

    }

    public String getFleetIdByCredentials(String requestId, String auth) {
        String fleetId = "";
        try {
            String username = "";
            try {
                String SECRET_KEY = KeysUtil.TEMPKEY +"-qup-payment-token-base-authentication-with-jwt-"+ SecurityUtil.getKey();
                String token = auth.substring("Bearer".length()).trim();
                Claims claims = Jwts.parser() // Configured and then used to parse JWT strings
                        .setSigningKey(DatatypeConverter.parseBase64Binary(SECRET_KEY)) // Sets the signing key used to verify
                        // any discovered JWS digital signature
                        .parseClaimsJws(token) // Parses the specified compact serialized JWS string based
                        .getBody();

                username = claims.getSubject();
            } catch (Exception ex) {
                logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            }
            String data = redisDao.getAuthUser("", username);
            if (!data.isEmpty()) {
                try {
                    String decrypt = SecurityUtil.decryptProfile(SecurityUtil.getKey(), data);
                    String[] arrData = decrypt.split(KeysUtil.TEMPKEY);
                    fleetId = arrData[2];
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - getFleetIdByCredentials exception: " + CommonUtils.getError(ex));
        }
        return fleetId;
    }

    public List<Double> getFleetServiceFees(String requestId, Booking booking, double taxSetting, double subTotal) {
        double fleetServiceFee = 0.0;
        double fleetCommissionFromFleetServiceFee = 0.0;
        double driverCommissionFromFleetServiceFee = 0.0;
        double fleetServiceTax = 0.0;
        List<FleetService> fleetServices = booking.request.fleetServices;
        if (fleetServices != null && !fleetServices.isEmpty()) {
            List<Double> fleetServiceValues = estimateUtil.getFleetServiceFees(requestId, fleetServices, booking.currencyISO, taxSetting, subTotal);
            fleetServiceFee = fleetServiceValues.get(0);
            fleetCommissionFromFleetServiceFee = fleetServiceValues.get(1);
            driverCommissionFromFleetServiceFee = fleetServiceValues.get(2);
            fleetServiceTax = fleetServiceValues.get(3);
        }
        return Arrays.asList(fleetServiceFee, fleetCommissionFromFleetServiceFee, driverCommissionFromFleetServiceFee, fleetServiceTax);
    }

    public double getDriverTax(String requestId, double subTotal, double taxValue, double commission, String commissionType) {
        double driverTax = 0.0;
        try {
            if (subTotal > 0) {
                double fleetTax = taxValue * (commission * 0.01);
                if (commissionType.equalsIgnoreCase("amount")) {
                    fleetTax = (commission / subTotal) * taxValue;
                }
                // round down the tax amount pay to driver => fleet will received more money
                driverTax = CommonUtils.getRoundDown(taxValue - fleetTax);
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - getDriverTax exception: " + CommonUtils.getError(ex));
        }
        logger.debug(requestId + " - driverTax: " + driverTax);
        return driverTax;
    }

    public String getStripeSystemAccount(String fleetId) {
        Fleet fleet = mongoDao.getFleetInfor(fleetId);
        // get setting environment
        String environment = fleet.affiliateEnvironment ? KeysUtil.PRODUCTION : KeysUtil.SANDBOX;

        GatewayStripe gatewayStripe;
        if (environment.equals(KeysUtil.SANDBOX)) {
            gatewayStripe = mongoDao.getGatewayStripeSystem(true);
        } else {
            gatewayStripe = mongoDao.getGatewayStripeSystem(false);
        }
        return gatewayStripe.secretKey;
    }

    public double getExchangeRate(String requestId, String fleetId, String transId, boolean isFarmOut, String currencyISO) {
        double exchangeRate = 1.0;
        try {
            ServerInformation si = mongoDao.getServerInformation();
            for (com.qupworld.paymentgateway.models.mongo.collections.serverInformation.Currency currency : si.currencies) {
                if (currency.iso.equals(currencyISO)) {
                    exchangeRate = currency.exchangeRate;
                    break;
                }
            }

            StripeUtil stripeUtil = new StripeUtil(requestId);
            String secretKey;
            if (isFarmOut) {
                GatewayStripe gatewayStripe = mongoDao.getGatewayStripeFleet(fleetId);
                secretKey = gatewayStripe.secretKey;
            } else {
                secretKey = getStripeSystemAccount(fleetId);
            }
            String getExchangeRate = stripeUtil.getExchangeRate(requestId, secretKey, transId);
            if (CommonUtils.getReturnCode(getExchangeRate) == 200) {
                exchangeRate = Double.parseDouble(CommonUtils.getValue(getExchangeRate, "exchangeRate"));
                return 1/exchangeRate;
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - getExchangeRate exception: " + CommonUtils.getError(ex));
        }
        if (exchangeRate == 0) exchangeRate = 1.0;
        logger.debug(requestId + " - exchangeRate: " + exchangeRate);
        return exchangeRate;
    }

    public String getNetworkTypeFromBooking(Booking booking, String fleetId) {
        String networkType = "";
        if (booking.pricingType == 1) {
            if (booking.request.psgFleetId.equals(fleetId)) {
                networkType = "farmIn";
            } else {
                networkType = booking.isFarmOut ? "farmOut" : "roaming";
            }
        }
        return networkType;
    }

    public double getAmountFromHydraPending(HydraPending hydraPending, String fleetId) {
        double amount = 0;
        if (hydraPending.fleetId != null && hydraPending.fleetId.equals(fleetId)) {
            if (hydraPending.convertSupplierPenalty < 0) {
                amount = hydraPending.convertSupplierPenalty;
            } else {
                amount = hydraPending.convertSupplierPayout;
            }
        } else if (hydraPending.psgFleetId != null && hydraPending.psgFleetId.equals(fleetId)) {
            amount = hydraPending.convertBuyerPayout;
        }
        return amount;
    }

    public String getTransactionTypeFromHydraPending(HydraPending hydraPending, String fleetId) {
        String transactionType = "";
        if (hydraPending.fleetId != null && hydraPending.fleetId.equals(fleetId)) {
            if (hydraPending.convertSupplierPenalty < 0) {
                transactionType = "supplierPenalty";
            } else {
                if (hydraPending.convertSupplierPayout > 0) {
                    transactionType = "supplierPayout";
                } else {
                    transactionType = "rejectBooking";
                }
            }
        } else if (hydraPending.psgFleetId != null && hydraPending.psgFleetId.equals(fleetId)) {
            if (hydraPending.convertBuyerPayout > 0) {
                transactionType = "buyerCommission";
            } else {
                transactionType = "chargeBooking";
            }
        }
        return transactionType;
    }

    public String doClearOutstanding(String requestId, PassengerInfo passengerInfo, List<Map<String, Object>> listData, String requestType, String reason) {
        try {
            logger.debug(requestId + " - doClearOutstanding: " + listData.size());
            ObjResponse response = new ObjResponse();
            JSONArray arrTicket = new JSONArray();
            JSONObject jsonBooking = new JSONObject();
            JSONArray arrBooking = new JSONArray();
            String writeOffDebtMethodMoreInfo = "";
            String type = requestType != null ? requestType : "";
            if (type.equals("paymentLink")) {
                String[] arr = reason.split(" ");
                String bookId = arr[arr.length - 1];
                TicketReturn ticketReturn = sqlDao.getTicket(bookId);
                String stripeMethod = ticketReturn != null && ticketReturn.stripeMethod != null ? ticketReturn.stripeMethod : "";
                logger.debug(requestId + " - stripeMethod: " + stripeMethod);
                writeOffDebtMethodMoreInfo = mapStripeMethod.get(stripeMethod) != null ? mapStripeMethod.get(stripeMethod) : "";
                logger.debug(requestId + " - writeOffDebtMethodMoreInfo: " + writeOffDebtMethodMoreInfo);
            }
            for (Map<String, Object> map : listData) {
                String bookId = map.get("bookId").toString();
                logger.debug(requestId + " - bookId: " + bookId);
                double total = Double.parseDouble(map.get("total").toString());
                String completedTime = map.get("completedTime").toString();
                // update outstanding amount on MySQL
                int update = sqlDao.updateOutstanding(bookId, total);
                if (update > 0) {
                    JSONObject objBody = new JSONObject();
                    objBody.put("isPending", false);
                    objBody.put("totalCharged", total);
                    objBody.put("writeOffDebtMethod", type);
                    objBody.put("writeOffDebtReason", reason);
                    objBody.put("writeOffDebtMethodMoreInfo", writeOffDebtMethodMoreInfo);
                    objBody.put("paymentStatus", "full");
                    objBody.put("currentOutstandingAmount", 0.0);
                    objBody.put("completedTime", completedTime.replace(".0", ""));
                    objBody.put("rechargedTime", KeysUtil.SDF_DMYHMS.format(TimezoneUtil.getGMTTimestamp()));

                    JSONObject objTicket = new JSONObject();
                    objTicket.put("id", bookId);
                    objTicket.put("type", "Ticket");
                    objTicket.put("body", objBody);

                    arrTicket.add(objTicket);

                    // update to BookingCompleted
                    JSONObject objBooking = new JSONObject();
                    objBooking.put("bookId", bookId);
                    objBooking.put("paidAmount", total);
                    objBooking.put("paidStatus", "paid");
                    objBooking.put("totalCharged", total);
                    objBooking.put("paymentStatus", "full");
                    arrBooking.add(objBooking);

                    // check if there is pending payment link then deactivate
                    StripeUtil stripeUtil = new StripeUtil(requestId);
                    String secretKey = getSecretKey(passengerInfo.fleetId, true);
                    List<TransactionDetails> transactionDetailsList = sqlDao.getPaymentDetailsByBookId(requestId, passengerInfo.fleetId, bookId);
                    for (TransactionDetails transactionDetails : transactionDetailsList) {
                        if (transactionDetails.status.equals("pending")) {
                            String paymentLink = transactionDetails.paymentLink != null ? transactionDetails.paymentLink : "";
                            String paymentLinkId = CommonUtils.getValue(paymentLink, "paymentLinkId");
                            boolean success = stripeUtil.deactivatePaymentLink(requestId, secretKey, paymentLinkId);
                            if (success) {
                                // update transaction
                                transactionDetails.status = "inactive";
                                transactionDetails.transactionId = "";
                                int updatePD = sqlDao.updatePaymentDetails(requestId, transactionDetails);
                                if (updatePD == 0) {
                                    Thread.sleep(1000);
                                    updatePD = sqlDao.updatePaymentDetails(requestId, transactionDetails);
                                }
                                logger.debug(requestId + " - updated transaction: " + updatePD);
                            }
                        }
                    }
                }
            }
            // Update to BookingCompleted and Search module
            jsonBooking.put("bookings", arrBooking);
            logger.debug(requestId + " - URL: " + PaymentUtil.invoice_update);
            logger.debug(requestId + " - data: " + jsonBooking.toJSONString());
            String jupiterResponse = CommonUtils.sendJupiter(requestId, jsonBooking.toJSONString(), PaymentUtil.invoice_update,
                    ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
            logger.debug(requestId + " - jupiterResponse: " + jupiterResponse);
            // update outstanding amount on PassengerInfo
            List<OutStanding> outStandings = passengerInfo.outStanding;
            if (!outStandings.isEmpty()) {
                for (OutStanding outStanding : passengerInfo.outStanding) {
                    outStanding.amount = 0.0;
                }
            }
            logger.debug(requestId + " - update outstanding for user: " + passengerInfo.userId + " - values: " + gson.toJson(outStandings));
            mongoDao.updateOustanding(passengerInfo.fleetId, passengerInfo.userId, outStandings);

            QUpReportPublisher reportPublisher = QUpReportPublisher.getInstance();
            reportPublisher.sendListItems(requestId, arrTicket.toJSONString());

            response.returnCode = 200;
            return response.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 407);
        }
    }

    public String writeOffDebtFromPaxWallet(String requestId, String fleetId, String userId, double amount, String currencyISO, String currencySymbol, String listBookId, String payId) {
        ObjResponse payObj = new ObjResponse();
        try {
            PaxWalletUtil paxWalletUtil = new PaxWalletUtil(requestId);
            double currentBalance = paxWalletUtil.getCurrentBalance(fleetId, userId, "paxWallet", currencyISO);
            double newBalance = KeysUtil.DECIMAL_FORMAT.parse(KeysUtil.DECIMAL_FORMAT.format(currentBalance - amount)).doubleValue();

            Account account = mongoDao.getAccount(userId);

            PaxWalletTransaction paxWalletTransaction = new PaxWalletTransaction();
            paxWalletTransaction.fleetId = fleetId;
            paxWalletTransaction.userId = userId;
            paxWalletTransaction.transactionType = CommonArrayUtils.PAX_WALLET_TYPE[3];
            paxWalletTransaction.bookId = listBookId;
            paxWalletTransaction.transactionId = payId.length() > 50 ? payId.substring(0, 49) : payId;
            paxWalletTransaction.amount = amount;
            paxWalletTransaction.newBalance = newBalance;
            paxWalletTransaction.description = CommonArrayUtils.PAX_WALLET_TYPE[3]+"-"+payId;
            paxWalletTransaction.customerName = account.fullName != null ? account.fullName : "";
            paxWalletTransaction.phoneNumber = account.phone.equals(account.userId) ? "" : account.phone;
            paxWalletTransaction.currencyISO = currencyISO;
            paxWalletTransaction.currencySymbol = currencySymbol;
            paxWalletTransaction.createdDate = TimezoneUtil.getGMTTimestamp();

            long idAddBalance = sqlDao.addPaxWalletTransaction(paxWalletTransaction);
            if (idAddBalance > 0) {
                // subtract money from wallet
                paxWalletUtil.updatePaxWallet(paxWalletTransaction, false);

                PassengerInfo passengerInfo = mongoDao.getPassengerInfo(fleetId, userId);
                // update wallet balance
                List<AmountByCurrency> listWallets = passengerInfo.paxWallet;
                for (AmountByCurrency wallet : listWallets) {
                    if (wallet.currencyISO != null && wallet.currencyISO.equals(currencyISO)) {
                        wallet.value = newBalance;
                        break;
                    }
                }
                mongoDao.updatePaxWallet(fleetId, userId, listWallets);

                // update to Reporting
                UpdatePaxWalletThread updateThread = new UpdatePaxWalletThread();
                paxWalletTransaction.id = idAddBalance;
                updateThread.paxWalletTransaction = paxWalletTransaction;
                updateThread.requestId = requestId;
                updateThread.start();
            }
            payObj.returnCode = 200;

        } catch (Exception ex) {
            logger.debug(requestId + " - writeOffDebtFromPaxWallet exception: " + CommonUtils.getError(ex));
            payObj.returnCode = 525;
        }
        return  payObj.toString();
    }

    public double getCustomerDebt(String requestId, String fleetId, String customerId, Booking booking, String currencyISO, boolean payFromCC, boolean writeOffDebt) {
        // check if customer pay outstanding
        double customerDebt = 0.0;
        try {
            if (customerId != null && !customerId.isEmpty()) {
                PassengerInfo passengerInfo = mongoDao.getPassengerInfo(fleetId, customerId);
                Fleet fleet = mongoDao.getFleetInfor(fleetId);
                // check setting force pay outstanding then do write of debt
                boolean applyForIndividual = false;
                boolean applyForCorporate = false;
                if (fleet.outstandingPayment != null && booking.jobType.equals("rideHailing") ) {
                    applyForIndividual = booking.travelerType == 0 && fleet.outstandingPayment.enable;
                    applyForCorporate = booking.travelerType == 1 && fleet.outstandingPayment.enable && fleet.outstandingPayment.applyForBusinessProfile;
                }
                if (applyForIndividual || applyForCorporate) {
                    if (!payFromCC || writeOffDebt) {
                        if (passengerInfo != null && passengerInfo.outStanding != null && !passengerInfo.outStanding.isEmpty()) {
                            for (OutStanding outStanding : passengerInfo.outStanding) {
                                if (outStanding.currencyISO != null && outStanding.currencyISO.equals(currencyISO)) {
                                    customerDebt = outStanding.amount;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - get outstanding error: " + CommonUtils.getError(ex));
        }
        return customerDebt;
    }

    public String getReturnChange(String requestId, String fleetId) {
        String returnChange = "";
        try {
            PricingPlan pricingPlan = mongoDao.getPricingPlan(fleetId);
            if (pricingPlan.partialPayment != null && pricingPlan.partialPayment.enable
                    && pricingPlan.partialPayment.returnChangeToWallet
                    && !pricingPlan.partialPayment.returnChangeInCash) {
                returnChange = "wallet";
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }
        return returnChange;
    }

    public void updateInvoiceAfterPayment(InvoicePayEnt invoicePayEnt, String email) {
        String requestId = invoicePayEnt.requestId;
        try {
            List<String> listBooking = mongoDao.getListBookingOfInvoice(invoicePayEnt.fleetId, invoicePayEnt.invoiceId);
            logger.debug(requestId + " - total bookings: " + listBooking.size());

            JSONObject ticketBody = new JSONObject();
            ticketBody.put("paymentStatus", "full");

            JSONObject objBody = new JSONObject();
            objBody.put("invoiceId", invoicePayEnt.invoiceId);
            JSONArray arrBooking = new JSONArray();
            JSONArray arrTicket = new JSONArray();
            for (String bookId : listBooking) {
                Booking booking = mongoDao.getBookingCompleted(bookId);

                JSONObject objData = new JSONObject();
                objData.put("bookId", bookId);
                objData.put("paidAmount", booking.completedInfo.total);
                objData.put("paidStatus", "paid");

                arrBooking.add(objData);

                String finishedAt = "";
                if (booking.finishedAt != null) {
                    finishedAt = sdfMongo.format(booking.finishedAt);
                }
                String completedTime = "";
                try {
                    Date finishedAtDate = TimezoneUtil.offsetTimeZone(sdfMongo.parse(finishedAt), Calendar.getInstance().getTimeZone().getID(), "GMT");
                    completedTime = KeysUtil.SDF_DMYHMS.format(finishedAtDate);
                    logger.debug(requestId + " - finishedAt " + finishedAt + " => completedTime: " + completedTime);
                } catch (Exception ex) {
                    logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
                }
                JSONObject objTicket = new JSONObject();
                objTicket.put("type", "Ticket");
                objTicket.put("id", bookId);
                ticketBody.put("completedTime", completedTime);
                ticketBody.put("totalCharged", booking.completedInfo.total);
                ticketBody.put("isPending", false);
                objTicket.put("body", ticketBody);
                arrTicket.add(objTicket);
            }
            objBody.put("bookings", arrBooking);
            logger.debug(requestId + " - URL: " + PaymentUtil.invoice_update);
            logger.debug(requestId + " - data: " + objBody.toJSONString());
            String jupiterResponse = CommonUtils.sendJupiter(requestId, objBody.toJSONString(), PaymentUtil.invoice_update,
                    ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
            logger.debug(requestId + " - jupiterResponse: " + jupiterResponse);

            // update payment status to Report
            logger.debug(requestId + " - report body: " + arrTicket.toJSONString());
            QUpReportPublisher reportPublisher = QUpReportPublisher.getInstance();
            reportPublisher.sendListItems(requestId, arrTicket.toJSONString());

            // send notify to invoice server
            JSONObject objInvoice = new JSONObject();
            objInvoice.put("id", invoicePayEnt.invoiceId);
            objInvoice.put("fleetId", invoicePayEnt.fleetId);
            String operatorId = invoicePayEnt.operatorId != null ? invoicePayEnt.operatorId : "";
            String operatorFirstName = "";
            String operatorLastName = "";
            String operatorUsername = "";
            if (!operatorId.isEmpty()) {
                UsersSystem usersSystem = mongoDao.getUsersSystem(operatorId);
                if (usersSystem != null) {
                    operatorFirstName = usersSystem.firstName != null ? usersSystem.firstName : "";
                    operatorLastName = usersSystem.lastName != null ? usersSystem.lastName : "";
                    operatorUsername = usersSystem.userName != null ? usersSystem.userName : "";
                }
            }
            objInvoice.put("operatorId", operatorId);
            objInvoice.put("operatorFirstName", operatorFirstName);
            objInvoice.put("operatorLastName", operatorLastName);
            objInvoice.put("operatorUsername", operatorUsername);
            logger.debug(requestId + " - URL: " + PaymentUtil.invoice_notify);
            logger.debug(requestId + " - data: " + objInvoice.toJSONString());
            String invoiceResponse = CommonUtils.sendJupiter(requestId, objInvoice.toJSONString(), PaymentUtil.invoice_notify,
                    ServerConfig.invoice_userName, ServerConfig.invoice_password);
            logger.debug(requestId + " - invoiceResponse: " + invoiceResponse);

            // send mail after completed
            /*MailService mailService = MailService.getInstance();
            mailService.sendMailPaidInvoice(requestId, invoicePayEnt.fleetId, invoicePayEnt.invoiceId, email);*/

            // cancel old intent if already created
            String cacheIntentId = redisDao.getTmp(requestId, invoicePayEnt.invoiceId+"paymentIntentId");
            if (!cacheIntentId.isEmpty()) {
                logger.debug(requestId + " - cacheIntentId: " + cacheIntentId);
                StripeUtil stripeUtil = new StripeUtil(requestId);
                GatewayStripe gatewayFleet = mongoDao.getGatewayStripeFleet(invoicePayEnt.fleetId);
                String secretKey = gatewayFleet.secretKey != null ? gatewayFleet.secretKey : "";
                stripeUtil.cancelPaymentIntent(requestId, secretKey, cacheIntentId);

                redisDao.removeTmp(requestId, cacheIntentId);
                redisDao.removeTmp(requestId, "queue-" + cacheIntentId);
            }

            // remove related cache
            redisDao.removeTmp(requestId, String.valueOf(invoicePayEnt.invoiceId+invoicePayEnt.amount));
            redisDao.removeTmp(requestId, invoicePayEnt.invoiceId+"paymentIntentId");
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }
    }

    public double getCreditTransactionFee(String requestId, Fleet fleet, double amount, String currencyISO) {
        try {
            double creditTransactionFeePercent = 0.0;
            double creditTransactionFeeAmount = 0.0;
            try {
                CreditCardTransactionFee cctf = fleet.creditCardTransactionFee;
                if(cctf != null && cctf.feeByCurrencies != null && !cctf.feeByCurrencies.isEmpty()){
                    for(FeeByCurrency feeByCurrency : cctf.feeByCurrencies){
                        if(feeByCurrency.currencyISO.equals(currencyISO)){
                            creditTransactionFeePercent = feeByCurrency.creditCardPercent;
                            creditTransactionFeeAmount = feeByCurrency.creditCardAmount;
                        }
                    }
                }
            } catch (Exception ex) {
                logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            }
            double fee = (amount * creditTransactionFeePercent / 100) + creditTransactionFeeAmount;
            logger.debug(requestId + " - fee: " + fee);
            return fee;
        } catch (Exception ex) {
            return 0.0;
        }
    }

    public void updateBookingAfterPayment(String requestId, String fleetId, Booking booking, double amount, String email, String type, long transactionDetailsId) {
        try {
            if (type.equals("prepaid")) {
                double total = booking.request.estimate.fare.etaFare;
                double paidAmount = Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(amount + booking.paidAmount));
                logger.debug(requestId + " - paidAmount = amount + booking.paidAmount = " + amount + " + " + booking.paidAmount + " = " + paidAmount);
                String paymentStatus = "paid";
                if (paidAmount < total) {
                    paymentStatus = "partial";
                }
                JSONArray arrBooking = new JSONArray();
                JSONObject objData = new JSONObject();
                objData.put("bookId", booking.bookId);
                objData.put("paidAmount", Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(paidAmount)));
                objData.put("paidStatus", paymentStatus);
                arrBooking.add(objData);

                JSONObject objBody = new JSONObject();
                objBody.put("bookingStatus", "active");
                objBody.put("bookings", arrBooking);
                logger.debug(requestId + " - URL: " + PaymentUtil.invoice_update);
                logger.debug(requestId + " - data: " + objBody.toJSONString());
                String jupiterResponse = CommonUtils.sendJupiter(requestId, objBody.toJSONString(), PaymentUtil.invoice_update,
                        ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
                logger.debug(requestId + " - jupiterResponse: " + jupiterResponse);
            } else if (type.equals("postpaid")) {
                // update payment status for booking
                JSONObject objBody = new JSONObject();
                JSONArray arrBooking = new JSONArray();
                JSONObject objData = new JSONObject();
                objData.put("bookId", booking.bookId);
                objData.put("paidAmount", booking.completedInfo.total);
                objData.put("paidStatus", "paid");
                objData.put("totalCharged", booking.completedInfo.total);
                objData.put("paymentStatus", "full");
                arrBooking.add(objData);

                String finishedAt = "";
                if (booking.finishedAt != null) {
                    finishedAt = sdfMongo.format(booking.finishedAt);
                }
                String completedTime = "";
                try {
                    Date finishedAtDate = TimezoneUtil.offsetTimeZone(sdfMongo.parse(finishedAt), Calendar.getInstance().getTimeZone().getID(), "GMT");
                    completedTime = KeysUtil.SDF_DMYHMS.format(finishedAtDate);
                    logger.debug(requestId + " - finishedAt " + finishedAt + " => completedTime: " + completedTime);
                } catch (Exception ex) {
                    logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
                }

                objBody.put("bookings", arrBooking);
                logger.debug(requestId + " - URL: " + PaymentUtil.invoice_update);
                logger.debug(requestId + " - data: " + objBody.toJSONString());
                String jupiterResponse = CommonUtils.sendJupiter(requestId, objBody.toJSONString(), PaymentUtil.invoice_update,
                        ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
                logger.debug(requestId + " - jupiterResponse: " + jupiterResponse);

                if (booking.request.paymentType == 5) {
                    // check if booking not belong to any invoice then update total received amount
                    if (booking.invoiceId == 0) {
                        JSONObject objTicket = new JSONObject();
                        objTicket.put("isPending", false);
                        objTicket.put("totalCharged", booking.completedInfo.total);
                        objTicket.put("completedTime", completedTime);
                        objTicket.put("paymentStatus", "full");
                        logger.debug(requestId + " - report body: " + objTicket.toJSONString());
                        QUpReportPublisher reportPublisher = QUpReportPublisher.getInstance();
                        reportPublisher.sendOneItem(requestId, booking.bookId, "Ticket", objTicket.toJSONString());
                    }
                } else {
                    // update outstanding amount on MySQL
                    int update = sqlDao.updateOutstanding(booking.bookId, booking.completedInfo.total);
                    if (update > 0) {
                        TransactionDetails transactionDetails = sqlDao.getPaymentDetailsById(requestId, transactionDetailsId);
                        String operatorId = transactionDetails.operatorId != null ? transactionDetails.operatorId : "";
                        String operatorName = "";
                        if (!operatorId.isEmpty()) {
                            UsersSystem usersSystem = mongoDao.getUsersSystem(transactionDetails.operatorId);
                            String firstName = usersSystem.firstName != null ? usersSystem.firstName : "";
                            String lastName = usersSystem.lastName != null ? usersSystem.lastName : "";
                            operatorName = firstName + " " + lastName;
                        }
                        String reason = "Charged by " + operatorName;
                        String chargeNote = transactionDetails.chargeNote != null ? transactionDetails.chargeNote : "";
                        if (!chargeNote.isEmpty()) {
                            reason = reason + ": " + chargeNote;
                        }
                        String writeOffDebtMethodMoreInfo = "";
                        if (transactionDetails.paymentMethod.equals("credit") || transactionDetails.paymentMethod.equals("corporateCard")) {
                            String cardMasked = transactionDetails.cardMasked != null ? transactionDetails.cardMasked : "";
                            if (cardMasked.length() > 4) {
                                writeOffDebtMethodMoreInfo = "**" + cardMasked.substring(cardMasked.length() - 4);
                            }
                        } else if (transactionDetails.paymentMethod.equals("paymentLink")) {
                            writeOffDebtMethodMoreInfo = mapStripeMethod.get(transactionDetails.stripeMethod) != null ? mapStripeMethod.get(transactionDetails.stripeMethod) : "";
                        }
                        JSONObject objTicket = new JSONObject();
                        objTicket.put("isPending", false);
                        objTicket.put("totalCharged", booking.completedInfo.total);
                        objTicket.put("writeOffDebtMethod", transactionDetails.paymentMethod);
                        objTicket.put("writeOffDebtReason", reason);
                        objTicket.put("writeOffDebtMethodMoreInfo", writeOffDebtMethodMoreInfo);
                        objTicket.put("paymentStatus", "full");
                        objTicket.put("currentOutstandingAmount", 0.0);
                        objTicket.put("completedTime", completedTime);
                        objTicket.put("rechargedTime", KeysUtil.SDF_DMYHMS.format(TimezoneUtil.getGMTTimestamp()));

                        QUpReportPublisher reportPublisher = QUpReportPublisher.getInstance();
                        reportPublisher.sendOneItem(requestId, booking.bookId, "Ticket", objTicket.toJSONString());

                        PassengerInfo passengerInfo = mongoDao.getPassengerInfo(booking.fleetId, booking.psgInfo.userId);
                        // update outstanding amount on PassengerInfo
                        List<OutStanding> outStandings = passengerInfo.outStanding;
                        if (!outStandings.isEmpty()) {
                            for (OutStanding outStanding : passengerInfo.outStanding) {
                                outStanding.amount = outStanding.amount - amount;
                            }
                        }
                        logger.debug(requestId + " - update outstanding for user: " + passengerInfo.userId + " - values: " + gson.toJson(outStandings));
                        mongoDao.updateOustanding(passengerInfo.fleetId, passengerInfo.userId, outStandings);
                    }
                }

            }
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }
    }

    public String getSecretKey(String fleetId, Boolean isLocalZone) {
        String secretKey = "";
        try {
            Fleet fleet = mongoDao.getFleetInfor(fleetId);
            GatewayStripe gateway = null;
            if (!isLocalZone) {
                if (!fleet.affiliateEnvironment) {
                    gateway = mongoDao.getGatewayStripeSystem(true);//true is sandbox
                } else {
                    gateway = mongoDao.getGatewayStripeSystem(false);//false is product
                }
                if (gateway.publicKey.isEmpty() || gateway.secretKey.isEmpty()) {
                    return "";
                } else {
                    secretKey = gateway.secretKey;
                }
            } else {
                GatewayStripe gatewayFleet = mongoDao.getGatewayStripeFleet(fleet.fleetId);
                if (gatewayFleet == null ||
                        gatewayFleet.publicKey == null || gatewayFleet.publicKey.isEmpty() ||
                        gatewayFleet.secretKey == null || gatewayFleet.secretKey.isEmpty()) {
                    return "";
                } else {
                    secretKey = gatewayFleet.secretKey;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return secretKey;
    }

    public String deactivatePaymentLinkForBooking(String requestId, String fleetId, String bookId) {
        ObjResponse response = new ObjResponse();
        try {
            String secretKey = getSecretKey(fleetId, true);
            StripeUtil stripeUtil = new StripeUtil(requestId);
            List<TransactionDetails> transactionDetailsList = sqlDao.getPaymentDetailsByBookId(requestId, fleetId, bookId);
            for (TransactionDetails transactionDetails : transactionDetailsList) {
                if (transactionDetails.paymentMethod.equals("paymentLink") && transactionDetails.status.equals("pending")) {
                    String paymentLink = transactionDetails.paymentLink != null ? transactionDetails.paymentLink : "";
                    String paymentLinkId = CommonUtils.getValue(paymentLink, "paymentLinkId");
                    boolean success = stripeUtil.deactivatePaymentLink(requestId, secretKey, paymentLinkId);
                    if (success) {
                        // remove related caches
                        redisDao.removeTmp(requestId, paymentLinkId+"bookId");
                        // update transaction
                        transactionDetails.status = "inactive";
                        transactionDetails.transactionId = "";
                        int update = sqlDao.updatePaymentDetails(requestId, transactionDetails);
                        if (update == 0) {
                            Thread.sleep(1000);
                            update = sqlDao.updatePaymentDetails(requestId, transactionDetails);
                        }
                        logger.debug(requestId + " - updated transaction: " + update);
                    }
                }
            }
            response.returnCode = 200;
        } catch (Exception ex) {
            logger.debug(requestId + " - deactivatePaymentLinkForBooking exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
        }
        return response.toString();
    }

    public String doTopUpPrepaid(String requestId, Corporate corporate, TopupEnt topupEnt, PrepaidTransaction prepaidTransaction, String topupId) {
        ObjResponse objResponse = new ObjResponse();
        try {
            String prepaidId = "";
            for (JSONObject method : corporate.paymentMethods) {
                if (method.get("type").toString().equalsIgnoreCase("prepaid") && method.get("prepaidId") != null) {
                    prepaidId = method.get("prepaidId").toString();
                    break;
                }
            }
            String company = corporate.companyInfo.name;
            String operatorId = topupEnt.operatorId != null ? topupEnt.operatorId : "";
            String operatorName = "";
            String operatorUserName = "";
            if (!operatorId.isEmpty()) {
                UsersSystem usersSystem = mongoDao.getUsersSystem(operatorId);
                String firstName = usersSystem.firstName != null ? usersSystem.firstName : "";
                String lastName = usersSystem.lastName != null ? usersSystem.lastName : "";
                operatorName = firstName + " " + lastName;
                operatorUserName = usersSystem.userName;
            }
            String topUpType = topupEnt.type != null ? topupEnt.type : "";
            String paidBy = "";
            if (topUpType.equals(KeysUtil.PAYMENT_DIRECTBILLING) || topUpType.equals(KeysUtil.PAYMENT_CARD)) {
                prepaidTransaction.type = "deposit";
                paidBy = topUpType;
            } else if (topUpType.equals("editBalance")) {
                prepaidTransaction.type = "editBalance";
                paidBy = "";
            }
            String payerFirstName = corporate.adminAccount.firstName != null ? corporate.adminAccount.firstName : "";
            String payerLastName = corporate.adminAccount.lastName != null ? corporate.adminAccount.lastName : "";
            String payerName = payerFirstName + " " + payerLastName;
            payerName = payerName.trim();

            prepaidTransaction.companyId = topupEnt.userId;
            prepaidTransaction.paidBy = paidBy;
            prepaidTransaction.prepaidId = prepaidId;
            prepaidTransaction.topUpId = topupId;
            prepaidTransaction.transactionId = topupId;
            prepaidTransaction.name = payerName;
            prepaidTransaction.company = company;
            prepaidTransaction.amount = topupEnt.amount;
            prepaidTransaction.fleetId = topupEnt.fleetId;
            prepaidTransaction.createdDate = TimezoneUtil.getGMTTimestamp();
            prepaidTransaction.currencyISO = topupEnt.currencyISO;
            prepaidTransaction.reason = topupEnt.reason != null ? CommonUtils.removeSpecialCharacter(topupEnt.reason) : "";
            prepaidTransaction.operatorId = operatorId;
            prepaidTransaction.operatorName = operatorName;
            prepaidTransaction.operatorUserName = operatorUserName;
            prepaidTransaction.id = sqlDao.addPrepaidTransaction(prepaidTransaction);
            logger.debug(requestId + " - prepaidTransaction.id: " + prepaidTransaction.id);
            double balance = sqlDao.getBalancePrepaid(prepaidId);
            logger.debug(requestId + " - new balance: " + balance);
            boolean result = mongoDao.updateCorporatePrePaid(topupEnt.userId, balance);
            logger.debug(requestId + " - updateCorporatePrePaid: " + result);

            prepaidTransaction.newAmount = Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(balance));
            GsonBuilder gb = new GsonBuilder();
            gb.serializeNulls();
            Gson gson = gb.create();

            String prepaidTransactionStr = gson.toJson(prepaidTransaction).replace("\\", "");
            JsonElement jsonElement = gson.fromJson(prepaidTransactionStr, JsonElement.class);
            JsonObject jo = jsonElement.getAsJsonObject();
            jo.addProperty("createdDate", KeysUtil.SDF_DMYHMS.format(prepaidTransaction.createdDate));
            prepaidTransactionStr = gson.toJson(jo);

            QUpReportPublisher reportPublisher = QUpReportPublisher.getInstance();
            reportPublisher.sendOneItem(requestId, String.valueOf(prepaidTransaction.id), "PrepaidTransaction", prepaidTransactionStr);
            objResponse.returnCode = 200;
            DecimalFormat decimalFormat = new DecimalFormat("0.00");
            objResponse.response = decimalFormat.format(balance);
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
        }
        return objResponse.toString();
    }

    public List<FleetService> getFleetServicesFromBooking(String requestId, List<FleetService> fleetServices, double subTotal, boolean checkActive) {
        List<FleetService> fleetServicesReturn = new ArrayList<>();
        try {
            if (!fleetServices.isEmpty()) {
                for (FleetService fleetService : fleetServices) {
                    if (checkActive) {
                        if (!fleetService.active) continue;
                    }
                    String serviceFeeType = fleetService.serviceFeeType != null ? fleetService.serviceFeeType : "amount";
                    double fee = fleetService.fee;
                    if (serviceFeeType.equals("percent")) {
                        fee = fleetService.serviceFeePercent * subTotal / 100;
                    }
                    fleetService.fee = fee;
                    fleetServicesReturn.add(fleetService);
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }
        return fleetServicesReturn;
    }
}


