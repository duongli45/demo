package com.qupworld.paymentgateway.controllers;

import com.mashape.unirest.http.Unirest;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.Base64Encoder;
import com.pg.util.CommonUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import java.text.DecimalFormat;

/**
 * Created by qup on 18/04/2019.
 */
@SuppressWarnings("unchecked")
public class InboxUtil extends Thread {

    final Logger logger = LogManager.getLogger(InboxUtil.class);
    public String requestId;
    public String action;
    public String referenceId;
    public String userId;
    public String currencyISO;
    public String type;
    public String creditNumber;
    public String bankAccount;
    public String reason;
    public String bookId;
    public String code;
    public double amount;
    public String INBOX_CASH_WALLET = "/api/v2/sendInboxForCashWallet";
    public String INBOX_DRV_PAYOUT = "/api/v2/drvPayout";
    public String INBOX_REDEEM_CODE = "/api/v2/redeemCode";
    public String INBOX_PRE_AUTH = "/api/v2/sendInboxForPaxPreauth";
    public String INBOX_MERCHANT_PAYOUT = "/api/v2/merchantPayout";

    public InboxUtil(String requestId){
        this.requestId = requestId;
    }

    @Override
    public void run() {
        try {
            if(action.equals("sendInboxForCashWallet")){
                logger.debug(requestId + " - sendInboxForCashWallet");
                sendInboxCashWallet(referenceId, userId, currencyISO, amount, type, creditNumber, reason);
            } else if(action.equals("sendInboxDriverPayout")){
                logger.debug(requestId + " - sendInboxDriverPayout");
                sendInboxDriverPayout(referenceId, userId, currencyISO, amount, bankAccount, reason);
            } else if(action.equals("sendInboxMerchantPayout")){
                sendInboxMerchantPayout(referenceId, userId, currencyISO, amount, bankAccount);
            } else if(action.equals("sendInboxRedeemCode")){
                logger.debug(requestId + " - sendInboxRedeemCode");
                sendInboxRedeemCode(userId, currencyISO, amount, type, code);
            } else {
                logger.debug(requestId + " - sendInboxPreauth");
                sendInboxPreauth(userId, currencyISO, amount, bookId);
            }

        } catch(Exception ex) {
            logger.debug(requestId + " - Send Inbox Error: "+ ex.getMessage());
        }

    }

    public void sendInboxCashWallet(String referenceId, String userId, String currencyISO, double amount, String type, String creditNumber, String reason) {
        try {
            String encoding = Base64Encoder.encodeString(ServerConfig.cc_service_username + ":" + ServerConfig.cc_service_password);
            DecimalFormat df = new DecimalFormat("#.##");
            JSONObject objBody = new JSONObject();
            objBody.put("referenceId", referenceId);
            objBody.put("userId", userId);
            objBody.put("currencyISO", currencyISO);
            objBody.put("amount", df.format(amount));
            objBody.put("type", type);
            objBody.put("requestId", requestId);
            if (!creditNumber.isEmpty()) {
                objBody.put("creditNumber", creditNumber);
            }
            if (!reason.isEmpty()) {
                objBody.put("reason", reason);
            }
            logger.debug(requestId + " - sendInboxCashWallet body: " + objBody.toJSONString());
            com.mashape.unirest.http.HttpResponse<String> json = Unirest.post(ServerConfig.qup_service + INBOX_CASH_WALLET)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("Authorization", "Basic " + encoding)
                    .header("x-request-id", requestId)
                    .body(objBody.toJSONString())
                    .asString();
            logger.debug(requestId + " - sendInboxCashWallet response: " + json.getBody().toString());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void sendInboxDriverPayout(String referenceId, String userId, String currencyISO, double amount, String bankAccount, String reason) {
        try {
            String encoding = Base64Encoder.encodeString(ServerConfig.cc_service_username + ":" + ServerConfig.cc_service_password);
            DecimalFormat df = new DecimalFormat("#.##");
            JSONObject objBody = new JSONObject();
            objBody.put("referenceId", referenceId);
            objBody.put("userId", userId);
            objBody.put("currencyISO", currencyISO);
            objBody.put("amount", df.format(amount));
            objBody.put("requestId", requestId);
            if (!bankAccount.isEmpty()) {
                objBody.put("bankAccount", bankAccount);
            }
            if (!reason.isEmpty()) {
                objBody.put("reason", reason);
            }
            logger.debug(requestId + " - sendInboxDriverPayout body: " + objBody.toJSONString());
            System.out.println(requestId + " - sendInboxDriverPayout body: " + objBody.toJSONString());
            com.mashape.unirest.http.HttpResponse<String> json = Unirest.post(ServerConfig.qup_service + INBOX_DRV_PAYOUT)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("Authorization", "Basic " + encoding)
                    .header("x-request-id", requestId)
                    .body(objBody.toJSONString())
                    .asString();
            logger.debug(requestId + " - sendInboxDriverPayout response: " + json.getBody().toString());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void sendInboxMerchantPayout(String referenceId, String userId, String currencyISO, double amount, String bankAccount) {
        try {
            String encoding = Base64Encoder.encodeString(ServerConfig.cc_service_username + ":" + ServerConfig.cc_service_password);
            DecimalFormat df = new DecimalFormat("#.##");
            JSONObject objBody = new JSONObject();
            objBody.put("merchantOwner", userId);
            objBody.put("currencyISO", currencyISO);
            objBody.put("amount", df.format(amount));
            objBody.put("bankAccount", bankAccount);
            objBody.put("referenceId", referenceId);
            objBody.put("requestId", requestId);
            logger.debug(requestId + " - sendInboxMerchantPayout body: " + objBody.toJSONString());
            com.mashape.unirest.http.HttpResponse<String> json = Unirest.post(ServerConfig.qup_service + INBOX_MERCHANT_PAYOUT)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("Authorization", "Basic " + encoding)
                    .header("x-request-id", requestId)
                    .body(objBody.toJSONString())
                    .asString();
            logger.debug(requestId + " - sendInboxDriverPayout response: " + json.getBody());

        } catch (Exception ex) {
            logger.debug(requestId + " - sendInboxMerchantPayout exception: " + CommonUtils.getError(ex));
            ex.printStackTrace();
        }
    }

    public void sendInboxPreauth(String userId, String currencyISO, double amount, String bookId) {
        try {
            String encoding = Base64Encoder.encodeString(ServerConfig.cc_service_username + ":" + ServerConfig.cc_service_password);
            DecimalFormat df = new DecimalFormat("#.##");
            JSONObject objBody = new JSONObject();
            objBody.put("userId", userId);
            objBody.put("currencyISO", currencyISO);
            objBody.put("amount", df.format(amount));
            objBody.put("bookId", bookId);
            objBody.put("requestId", requestId);

            logger.debug(requestId + " - sendInboxPreauth body: " + objBody.toJSONString());
            com.mashape.unirest.http.HttpResponse<String> json = Unirest.post(ServerConfig.qup_service + INBOX_PRE_AUTH)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("Authorization", "Basic " + encoding)
                    .header("x-request-id", requestId)
                    .body(objBody.toJSONString())
                    .asString();
            logger.debug(requestId + " - sendInboxPreauth response: " + json.getBody().toString());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void sendInboxRedeemCode(String userId, String currencyISO, double amount, String type, String code) {
        try {
            String encoding = Base64Encoder.encodeString(ServerConfig.cc_service_username + ":" + ServerConfig.cc_service_password);
            DecimalFormat df = new DecimalFormat("#.##");
            JSONObject objBody = new JSONObject();
            objBody.put("userId", userId);
            objBody.put("currencyISO", currencyISO);
            objBody.put("type", type);
            objBody.put("code", code);
            objBody.put("amount", df.format(amount));
            objBody.put("requestId", requestId);
            logger.debug(requestId + " - sendInboxRedeemCode body: " + objBody.toJSONString());
            com.mashape.unirest.http.HttpResponse<String> json = Unirest.post(ServerConfig.qup_service + INBOX_REDEEM_CODE)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("Authorization", "Basic " + encoding)
                    .header("x-request-id", requestId)
                    .body(objBody.toJSONString())
                    .asString();
            logger.debug(requestId + " - sendInboxRedeemCode response: " + json.getBody().toString());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
