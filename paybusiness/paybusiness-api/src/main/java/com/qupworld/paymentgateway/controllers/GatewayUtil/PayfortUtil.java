package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.google.gson.Gson;
import com.qupworld.paymentgateway.models.RedisDao;
import com.qupworld.paymentgateway.models.RedisImpl;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.ValidCreditCard;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class PayfortUtil {

    final static Logger logger = LogManager.getLogger(PayfortUtil.class);

    private final DecimalFormat DF2decimal = new DecimalFormat("0.00");
    private final DecimalFormat DF3decimal = new DecimalFormat("0.000");
    private String merchantIdentifier;
    private String accessCode;
    private String shaType;
    private String requestPhrase;
    private String responsePhrase;
    private String tokenUrl;
    private String paymentUrl;
    private String requestId;

    public PayfortUtil(String _requestId, String _merchantIdentifier, String _accessCode, String _shaType, String _requestPhrase, String _responsePhrase,
                       String _tokenUrl, String _paymentUrl){
        requestId = _requestId;
        merchantIdentifier = _merchantIdentifier;
        accessCode = _accessCode;
        shaType = _shaType;
        requestPhrase = _requestPhrase;
        responsePhrase = _responsePhrase;
        tokenUrl = _tokenUrl;
        paymentUrl = _paymentUrl;
    }

    public String createCreditToken(String requestId, String cardNumber, String expiredDate, String cvv, String customerId) throws IOException {
        ObjResponse createTokenResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        Map<String,String> mapResult = createTokenRequest(requestId, cardNumber, expiredDate, cvv, customerId);
        String token_name = mapResult.get("token_name") != null ? mapResult.get("token_name") : "";
        String response_code = mapResult.get("response_code") != null ? mapResult.get("response_code") : "";
        String card_number = mapResult.get("card_number") != null ? mapResult.get("card_number") : "";
        if (response_code.length() > 3)
            response_code = response_code.substring(2);
        int returnCode = getReturnCodeFromTransactionCode(response_code);
        if (returnCode == 200) {
            mapResponse.put("creditCard", card_number);
            mapResponse.put("token", token_name);
            mapResponse.put("cardType", ValidCreditCard.getCardTypeInFormat(cardNumber));

            createTokenResponse.returnCode = 200;
            createTokenResponse.response = mapResponse;
        } else {
            createTokenResponse.returnCode = returnCode;
        }
        return createTokenResponse.toString();
    }

    private Map<String,String> createTokenRequest(String requestId, String cardNumber, String expiredDate, String cvv, String customerId) {
        Map<String,String> mapResult = new HashMap<>();
        try {

            /*String returnURL = ServerConfig.payment_service + "/api/paymentgateway/completeTokenizationPayfort";*/
            URL obj = new URL(tokenUrl);
            System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
            HttpsURLConnection conn = (HttpsURLConnection) obj.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            //conn.setReadTimeout(5000);
            conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
            conn.addRequestProperty("User-Agent", "Mozilla");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded"); // needed for submitting forms data
            conn.setInstanceFollowRedirects( true );

            logger.debug("Request URL ... " + tokenUrl);
            String expiredDateConvert = expiredDate.substring(5) + expiredDate.substring(0,2);
            String signature = createTokenSignature(customerId, KeysUtil.CALLBACK_URL);
            logger.debug(requestId + " - signature: " + signature);
            String query = ""
                    + "service_command=TOKENIZATION"
                    + "&merchant_identifier="+merchantIdentifier
                    + "&access_code="+accessCode
                    + "&signature=" + signature
                    + "&merchant_reference=" + customerId
                    + "&language=en"
                    + "&card_number="+cardNumber
                    + "&expiry_date="+expiredDateConvert
                    + "&card_security_code="+cvv
                    + "&return_url="+KeysUtil.CALLBACK_URL;
            String cardLog = "XXXXXXXXXXXX" + cardNumber.substring(cardNumber.length() - 4);
            String log = query.replace(cardNumber, cardLog);
            logger.debug(requestId + " - request: " + log);
            conn.connect();

            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(query);
            wr.flush();
            wr.close();

            boolean redirect = false;
            // normally, 3xx is redirect
            int status = conn.getResponseCode();
            if (status != HttpURLConnection.HTTP_OK) {
                if (status == HttpURLConnection.HTTP_MOVED_TEMP
                        || status == HttpURLConnection.HTTP_MOVED_PERM
                        || status == HttpURLConnection.HTTP_SEE_OTHER)
                    redirect = true;
            }
            logger.debug(requestId + " - redirect: " + redirect);
            if (!redirect) {
                URL redirectURL = conn.getURL();
                logger.debug(requestId + " - dataURL: " + redirectURL.getFile());
                String data = redirectURL.getFile();
                data = data.substring(data.indexOf("?") + 1); // remove "?" symbol
                String[] test1 = data.split("&");
                for (String s : test1) {
                    String[] t = s.split("=");
                    if (t.length > 1) {
                        mapResult.put(t[0], t[1]);
                    }
                }
            } else {
                String location = conn.getURL().toString();
                if (location.equals(tokenUrl))
                    location = conn.getHeaderField("Location");
                logger.debug(requestId + " - redirectURL: " + location);
                HttpClient httpClient = HttpClientBuilder.create()
                        .setRedirectStrategy(new LaxRedirectStrategy()).build();
                HttpGet request = new HttpGet(location);
                HttpResponse responseGet = httpClient.execute(request);
                logger.debug(requestId + " - responseGet: " + responseGet.getStatusLine().getStatusCode());

                /*logger.debug(requestId + " - sleep - waiting for response data");
                Thread.sleep(2000);*/

                RedisDao redisDao = new RedisImpl();
                Gson gson = new Gson();
                mapResult = gson.fromJson(redisDao.get3DSData(requestId, customerId, "queryParams"), Map.class);
            }
            logger.debug(requestId + " - response: " + mapResult);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return mapResult;
    }

    public String firstPayWithToken(String type, String token, String currencyISO, String customerEmail, String customerIP, String customerId,
                                    double amount, String device_fingerprint) {
        ObjResponse paymentResponse = new ObjResponse();
        try {
            String AMOUNT;
            if (currencyISO.equalsIgnoreCase("JOD"))
                AMOUNT = DF3decimal.format(amount).replace(".","");
            else
                AMOUNT = DF2decimal.format(amount).replace(".","");
            if (customerEmail.equals("")) {
                customerEmail = "passenger@mail.com";
            }
            String signature = createPaySignature(type, AMOUNT, token, currencyISO, KeysUtil.CALLBACK_URL, customerEmail, customerIP, customerId, true, device_fingerprint);
            JSONObject json = new JSONObject();
            json.put("command", type);
            json.put("merchant_identifier", merchantIdentifier);
            json.put("access_code", accessCode);
            json.put("signature", signature);
            json.put("merchant_reference", customerId);
            json.put("language", "en");
            json.put("amount", AMOUNT);
            json.put("currency", currencyISO);
            json.put("customer_email", customerEmail);
            json.put("customer_ip", customerIP);
            json.put("token_name", token);
            if (!device_fingerprint.isEmpty())
                json.put("device_fingerprint", device_fingerprint);
            json.put("return_url", KeysUtil.CALLBACK_URL);
            logger.debug(requestId + " - request: " + json.toJSONString());
            String result = submitRequest(requestId, type, paymentUrl, json.toJSONString());
            JSONObject objData = (JSONObject)new JSONParser().parse(result);
            String responseCode = objData.get("response_code") != null ? objData.get("response_code").toString() : "";
            if (responseCode.length() > 3)
                responseCode = responseCode.substring(2);
            int returnCode = getReturnCodeFromTransactionCode(responseCode);
            if (returnCode == 200) {
                paymentResponse.returnCode = 200;
                objData.put("type", "link");
                paymentResponse.response = objData;
            } else {
                paymentResponse.returnCode = returnCode;
                paymentResponse.response = objData;
            }
            return paymentResponse.toString();
        } catch (Exception ex) {
            paymentResponse.returnCode = 437;
            paymentResponse.response = getError(ex);
        }
        return paymentResponse.toString();
    }

    public String payByToken(String token, double total,
                             String currencyISO, boolean isFirstPay, String customerEmail, String customerIP) {
        ObjResponse paymentResponse = new ObjResponse();
        try {
            String MR = RandomStringUtils.randomAlphanumeric(6).toUpperCase();

            String AMOUNT;
            if (currencyISO.equalsIgnoreCase("JOD"))
                AMOUNT = DF3decimal.format(total).replace(".","");
            else
                AMOUNT = DF2decimal.format(total).replace(".","");
            if (customerEmail.equals("")) {
                customerEmail = "passenger@mail.com";
            }
            String signature = createPaySignature("PURCHASE", AMOUNT, token, currencyISO, KeysUtil.CALLBACK_URL, customerEmail, customerIP, MR, isFirstPay, "");
            JSONObject json = new JSONObject();
            json.put("command", "PURCHASE");
            json.put("merchant_identifier", merchantIdentifier);
            json.put("access_code", accessCode);
            json.put("signature", signature);
            json.put("merchant_reference", MR);
            json.put("language", "en");
            json.put("amount", AMOUNT);
            json.put("currency", currencyISO);
            json.put("customer_email", customerEmail);
            json.put("customer_ip", customerIP);
            json.put("token_name", token);
            //json.put("remember_me", "YES");
            json.put("eci", "MOTO");
            //json.put("card_security_code", cvv);
            //json.put("return_url", returnURL);
            logger.debug(requestId + " - request: " + json.toJSONString());
            String result = submitRequest(requestId, "PURCHASE", paymentUrl, json.toJSONString());
            JSONObject objData = (JSONObject)new JSONParser().parse(result);

            String responseCode = objData.get("response_code") != null ? objData.get("response_code").toString() : "";
            if (responseCode.length() > 3)
                responseCode = responseCode.substring(2);
            int returnCode = getReturnCodeFromTransactionCode(responseCode);
            if (returnCode == 200) {
                // validate response data
                boolean isValid = validateResponse(objData, responsePhrase);
                if (isValid) {
                    paymentResponse.returnCode = 200;
                    Map<String,String> mapResult = new HashMap<>();
                    String transId = objData.get("fort_id") != null ? objData.get("fort_id").toString() : "";
                    String authCode = objData.get("authorization_code") != null ? objData.get("authorization_code").toString() : "";
                    String cardType = objData.get("payment_option") != null ? objData.get("payment_option").toString() : "";
                    mapResult.put("transId", transId);
                    mapResult.put("authCode", authCode);
                    mapResult.put("cardType", cardType);
                    paymentResponse.response = mapResult;
                } else {
                    paymentResponse.returnCode = 2015; // Transaction Not Allowed
                    paymentResponse.response = objData;
                }
            } else {
                paymentResponse.returnCode = returnCode;
                paymentResponse.response = objData;
            }
            return paymentResponse.toString();
        } catch (Exception ex) {
            paymentResponse.returnCode = 437;
            paymentResponse.response = getError(ex);
        }
        return paymentResponse.toString();
    }

    private boolean validateResponse(JSONObject objData, String responsePhase) {
        String access_code = objData.get("access_code").toString();
        String amount = objData.get("amount").toString();
        String authorization_code = objData.get("authorization_code").toString();
        String card_number = objData.get("card_number").toString();
        String command = objData.get("command").toString();
        String currency = objData.get("currency").toString();
        String customer_email = objData.get("customer_email").toString();
        String customer_ip = objData.get("customer_ip").toString();
        String eci = objData.get("eci").toString();
        String expiry_date = objData.get("expiry_date").toString();
        String fort_id = objData.get("fort_id").toString();
        String language = objData.get("language").toString();
        String merchant_identifier = objData.get("merchant_identifier").toString();
        String merchant_reference = objData.get("merchant_reference").toString();
        String payment_option = objData.get("payment_option").toString();
        String response_code = objData.get("response_code").toString();
        String response_message = objData.get("response_message").toString();
        String signature = objData.get("signature").toString();
        String status = objData.get("status").toString();
        String token_name = objData.get("token_name").toString();

        Map<String,String> mapData = new HashMap<>();
        if (!access_code.equals("")) mapData.put("access_code", access_code);
        if (!amount.equals("")) mapData.put("amount", amount);
        if (!authorization_code.equals("")) mapData.put("authorization_code", authorization_code);
        if (!card_number.equals("")) mapData.put("card_number", card_number);
        if (!command.equals("")) mapData.put("command", command);
        if (!currency.equals("")) mapData.put("currency", currency);
        if (!customer_email.equals("")) mapData.put("customer_email", customer_email);
        if (!customer_ip.equals("")) mapData.put("customer_ip", customer_ip);
        if (!eci.equals("")) mapData.put("eci", eci);
        if (!expiry_date.equals("")) mapData.put("expiry_date", expiry_date);
        if (!fort_id.equals("")) mapData.put("fort_id", fort_id);
        if (!language.equals("")) mapData.put("language", language);
        if (!merchant_identifier.equals("")) mapData.put("merchant_identifier", merchant_identifier);
        if (!merchant_reference.equals("")) mapData.put("merchant_reference", merchant_reference);
        if (!payment_option.equals("")) mapData.put("payment_option", payment_option);
        if (!response_code.equals("")) mapData.put("response_code", response_code);
        if (!response_message.equals("")) mapData.put("response_message", response_message);
        //if (!signature.equals("")) mapData.put("signature", signature);
        if (!status.equals("")) mapData.put("status", status);
        if (!token_name.equals("")) mapData.put("token_name", token_name);

        // sort map
        Map<String, String> treeMap = new TreeMap<>(mapData);
        String responseSignature = createResponseSignature(treeMap, responsePhase);
        logger.debug(requestId + " - signature from gw: " + signature);
        logger.debug(requestId + " - responseSignature: " + responseSignature);
        return signature.equals(responseSignature);
    }

    public void voidTransaction(String type, String transId, double total, String currencyISO) {
        try {
            String AMOUNT;
            if (currencyISO.equalsIgnoreCase("JOD"))
                AMOUNT = DF3decimal.format(total).replace(".","");
            else
                AMOUNT = DF2decimal.format(total).replace(".","");
            String signature = createVoidSignature(type, AMOUNT, transId, currencyISO);
            JSONObject json = new JSONObject();
            json.put("command", type);
            json.put("merchant_identifier", merchantIdentifier);
            json.put("access_code", accessCode);
            json.put("signature", signature);
            json.put("language", "en");
            json.put("fort_id", transId);
            //json.put("merchant_reference", MR);
            if (type.equals("REFUND")) {
                json.put("amount", AMOUNT);
                json.put("currency", currencyISO);
            }
            logger.debug(requestId + " - request: " + json.toJSONString());
            String result = submitRequest(requestId, type, paymentUrl, json.toJSONString());
            JSONObject objData = (JSONObject)new JSONParser().parse(result);
            String responseCode = objData.get("response_code") != null ? objData.get("response_code").toString() : "";
            if (responseCode.length() > 3)
                responseCode = responseCode.substring(2);
            int returnCode = getReturnCodeFromTransactionCode(responseCode);
            if (returnCode == 200) {
                logger.debug(requestId + " - transactionId: " + transId + " has been voided !!!!");
            } else {
                logger.debug(requestId + " - void transaction of: " + transId + " has failed!!!!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public int getReturnCodeFromTransactionCode(String transactionCode) {
        int returnCode = 437;
        switch (transactionCode) {
            case "000":
                returnCode = 200;
                break;
            case "064": // 3-D Secure check requested.
                returnCode = 200;
                break;
            case "001":
                returnCode = 406;
                break;
            case "002":
                returnCode = 406;
                break;
            case "005":
                returnCode = 2048;
                break;
            case "012":
                returnCode = 2004;
                break;
            case "013":
                returnCode = 2064;
                break;
            case "016":
                returnCode = 421;
                break;
            case "029":
                returnCode = 2001;
                break;
            case "031":
                returnCode = 2009;
                break;
            case "050":
                returnCode = 2002;
                break;
            case "057":
                returnCode = 2056;
                break;
            case "058":
                returnCode = 2056;
                break;
            case "666":
                returnCode = 436;
                break;
        }

        return returnCode;
    }

    private String getError(Exception ex) {
        return GatewayUtil.getError(ex);
    }

    private static String submitRequest(String requestId, String type, String url, String data) throws Exception{
        HttpURLConnection urlConn;
        boolean httpsFlag = false;
        if (url.toLowerCase().startsWith("https://")) {
            httpsFlag = true;
        }
        URL destURL = new URL(url);
        if (httpsFlag) {
            GatewayUtil.trustAllHttpsCertificates();
            urlConn = (HttpsURLConnection) destURL.openConnection();
        } else {
            urlConn = (HttpURLConnection) destURL.openConnection();
        }

        urlConn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
        urlConn.setDoOutput(true);
        urlConn.setDoInput(true);
        urlConn.setAllowUserInteraction(false);
			/*logger.info(method);*/
        urlConn.setUseCaches(false);
        urlConn.setRequestMethod("POST");
        urlConn.setConnectTimeout(10*60*1000);
        urlConn.setReadTimeout(10*60*1000);

        // do POST
        OutputStream os = urlConn.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os, StandardCharsets.UTF_8);
        osw.write(data);
        osw.flush();
        osw.close();

        BufferedInputStream is = new BufferedInputStream(
                urlConn.getInputStream());
        BufferedReader br = new BufferedReader(new InputStreamReader(is,
                StandardCharsets.UTF_8));
        String temp;
        StringBuilder result = new StringBuilder();
        while ((temp = br.readLine()) != null) {
            result.append(temp);
        }
        int httpCode = urlConn.getResponseCode();
        logger.debug(requestId + " - type: " + type);
        logger.debug(requestId + " - httpCode: " + httpCode);
        logger.debug(requestId + " - result: " + result.toString());

        return result.toString();
    }

    private String createTokenSignature(String merchant_reference, String returnURL){
        String data = requestPhrase+"access_code="+accessCode+"language=enmerchant_identifier="+merchantIdentifier+"merchant_reference="+merchant_reference+"return_url="+returnURL+"service_command=TOKENIZATION"+requestPhrase;
        logger.debug(requestId + " - signature: " + data);
        return doSign(data);
    }

    private String createPaySignature(String type, String AMOUNT, String token, String currencyISO, String returnURL, String customerEmail, String customerIP,
                                      String merchant_reference, boolean isFirstPay, String device_fingerprint){
        // ascending alphabetical order based on the parameters names.
        //String data = requestPhrase+"access_code="+accessCode+"amount="+AMOUNT+"card_security_code="+cvv+"command=PURCHASEcurrency="+currencyISO+"customer_email="+customerEmail+"customer_ip="+customerIP+"eci=RECURRINGlanguage=enmerchant_identifier="+merchantIdentifier+"merchant_reference="+merchant_reference+"remember_me=YEStoken_name="+token+requestPhrase;
        String data = requestPhrase+"access_code="+accessCode+"amount="+AMOUNT+"command=PURCHASEcurrency="+currencyISO+"customer_email="+customerEmail+"customer_ip="+customerIP+"eci=MOTOlanguage=enmerchant_identifier="+merchantIdentifier+"merchant_reference="+merchant_reference+"token_name="+token+requestPhrase;
        if (isFirstPay) {
            if (!device_fingerprint.isEmpty())
                data = requestPhrase+"access_code="+accessCode+"amount="+AMOUNT+"command="+type+"currency="+currencyISO+"customer_email="+customerEmail+"customer_ip="+customerIP+"device_fingerprint="+device_fingerprint+"language=enmerchant_identifier="+merchantIdentifier+"merchant_reference="+merchant_reference+"return_url="+returnURL+"token_name="+token+requestPhrase;
            else
                data = requestPhrase+"access_code="+accessCode+"amount="+AMOUNT+"command="+type+"currency="+currencyISO+"customer_email="+customerEmail+"customer_ip="+customerIP+"language=enmerchant_identifier="+merchantIdentifier+"merchant_reference="+merchant_reference+"return_url="+returnURL+"token_name="+token+requestPhrase;
        }

        logger.debug(requestId + " - createPaySignature data: " + data);
        return doSign(data);
    }

    private String createVoidSignature(String type, String AMOUNT, String transId, String currencyISO){
        // order theo thu tu abc
        String data = requestPhrase+"access_code="+accessCode+"amount="+AMOUNT+"command="+type+"currency="+currencyISO+"fort_id="+transId+"language=enmerchant_identifier="+merchantIdentifier+requestPhrase;
        if (type.equals("VOID_AUTHORIZATION"))
            data = requestPhrase+"access_code="+accessCode+"command="+type+"fort_id="+transId+"language=enmerchant_identifier="+merchantIdentifier+requestPhrase;
        logger.debug(requestId + " - createVoidSignature data: " + data);
        return doSign(data);
    }

    private String createResponseSignature(Map<String,String> mapData, String responsePhase){
        // ascending alphabetical order based on the parameters names.
        StringBuilder data = new StringBuilder(responsePhase);
        for (String key : mapData.keySet()) {
            data.append(key).append("=").append(mapData.get(key));
        }
        data.append(responsePhase);
        logger.debug(requestId + " - createResponseSignature data: " + data);
        return doSign(data.toString());
    }

    private String doSign(String data) {
        try {
            MessageDigest digest = MessageDigest.getInstance(shaType);
            byte[] hash = digest.digest(data.getBytes());
            //convert the byte to hex format method 2
            StringBuilder hexString = new StringBuilder();

            for (byte aHash : hash) {
                String hex = Integer.toHexString(0xff & aHash);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }
}
