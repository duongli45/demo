package com.qupworld.paymentgateway.controllers.threading;

import com.qupworld.paymentgateway.controllers.UpdateDriverCreditBalance;
import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.SQLDao;
import com.pg.util.CommonArrayUtils;
import com.pg.util.CommonUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class TopUpBonusToDriverThread extends Thread {

    final static Logger logger = LogManager.getLogger(TopUpBonusToDriverThread.class);
    public String fleetId;
    public String currencyISO;
    public String driverId;
    public double amount;
    public List<String> listDriverTypes;
    public String reason;
    public MongoDao mongoDao;
    public SQLDao sqlDao;
    public String requestId;

    @Override
    public void run() {
        try {
            if (!driverId.isEmpty()) {
                updateBalance(driverId);

            } else {
                int i = 0;
                int size = 100;
                boolean valid = true;
                while (valid) {
                    List<String> listDriver = mongoDao.getActiveDriversByType(requestId, fleetId, listDriverTypes, i * size, size);
                    int querySize = listDriver.size();
                    for (String userId : listDriver) {
                        updateBalance(userId);
                    }

                    if (querySize < size) {
                        valid = false;
                    }
                    i++;
                }
            }

        } catch(Exception ex) {
            logger.debug(requestId + " - TopUpTransportDriverThread exception: "+ CommonUtils.getError(ex));
        }

    }

    private void updateBalance(String driverId){
        UpdateDriverCreditBalance updateBalance = new UpdateDriverCreditBalance();
        updateBalance.type = CommonArrayUtils.DRIVER_WALLET_TYPE[2];
        updateBalance.fleetId = fleetId;
        updateBalance.driverId = driverId;
        updateBalance.amount = amount;
        updateBalance.currencyISO = currencyISO;
        updateBalance.mongoDao = mongoDao;
        updateBalance.sqlDao = sqlDao;
        updateBalance.reason = reason;
        updateBalance.additionalData.put("operatorId", "");
        updateBalance.additionalData.put("operatorName", "");
        updateBalance.requestId = requestId;
        String result = updateBalance.doUpdate();
        logger.debug(requestId + " - updateBalance for driver "+ driverId +" result = " + result);
    }

}
