package com.qupworld.paymentgateway.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.pg.util.CommonUtils;
import com.pg.util.ConvertUtil;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.ValidCreditCard;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.controllers.GatewayUtil.*;
import com.qupworld.paymentgateway.controllers.threading.CreateTokenThread;
import com.qupworld.paymentgateway.controllers.threading.DeleteTokenThread;
import com.qupworld.paymentgateway.entities.*;
import com.qupworld.paymentgateway.entities.PayWay.PayWayCredit;
import com.qupworld.paymentgateway.entities.PayWay.PayWayReturnParams;
import com.qupworld.paymentgateway.entities.gateway.PowerTranz.PowerTranz;
import com.qupworld.paymentgateway.entities.gateway.PowerTranz.PowerTranzError;
import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.MongoDaoImpl;
import com.qupworld.paymentgateway.models.RedisDao;
import com.qupworld.paymentgateway.models.RedisImpl;
import com.qupworld.paymentgateway.models.mongo.collections.Credit;
import com.qupworld.paymentgateway.models.mongo.collections.account.Account;
import com.qupworld.paymentgateway.models.mongo.collections.corporate.Corporate;
import com.qupworld.paymentgateway.models.mongo.collections.fleet.ConfigGateway;
import com.qupworld.paymentgateway.models.mongo.collections.fleet.Fleet;
import com.qupworld.paymentgateway.models.mongo.collections.gateway.*;
import com.qupworld.paymentgateway.models.mongo.collections.phoneZip.PhoneZip;
import com.qupworld.paymentgateway.models.mongo.collections.zone.Zone;
import com.stripe.model.Event;
import com.stripe.model.EventDataObjectDeserializer;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import java.net.URI;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see <a href="http://qupworld.com/terms>http://qupworld.com/terms</a>
 * @see <a href="http://qupworld.com/privacy>http://qupworld.com/terms</a>
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class CreditCardCtr {
    private Gson gson;
    private MongoDao mongoDao;
    private RedisDao redisDao;
    private PaymentUtil paymentUtil;
    final static Logger logger = LogManager.getLogger(CreditCardCtr.class);
    private static final List<String>  internationalCardList = Arrays.asList("VC", "MC", "JC", "AE", "CUP");

    public CreditCardCtr() {
        gson = new Gson();
        mongoDao = new MongoDaoImpl();
        paymentUtil = new PaymentUtil();
        redisDao = new RedisImpl();
    }

    public String createToken(CreditEnt creditForm, String type, String customerId, boolean isAuthorized) {
        ObjResponse createTokenResponse = new ObjResponse();
        createTokenResponse.returnCode = 200;
        creditForm.cardNumber = creditForm.cardNumber != null ? creditForm.cardNumber.trim().replace(" ", "") : "";
        creditForm.cardHolder = creditForm.cardHolder != null ? creditForm.cardHolder : "";
        creditForm.expiredDate = creditForm.expiredDate != null ? creditForm.expiredDate.trim().replace(" ", "") : "";
        creditForm.cvv = creditForm.cvv != null ? creditForm.cvv : "";
        String postalCode = creditForm.postalCode != null ? creditForm.postalCode : "";
        try {
            Fleet fleetInfor = mongoDao.getFleetInfor(creditForm.fleetId);
            // check valid card
            String cardType = "";
            String last4 = "";
            String token = creditForm.token != null ? creditForm.token : "";
            if (!token.isEmpty()) {
                try {
                    JSONObject json = (JSONObject)new org.json.simple.parser.JSONParser().parse(token);
                    JSONObject objCard = (JSONObject)json.get("mCard");
                    cardType = objCard.get("brand").toString();
                    last4 = objCard.get("last4").toString();
                } catch(Exception ex) {
                    logger.debug(creditForm.requestId + " - card info is empty: " + ex.getMessage());
                }
            } else {
                if (!creditForm.cardNumber.isEmpty()) {
                    cardType = com.pg.util.ValidCreditCard.getCardType(creditForm.cardNumber);
                    last4 = creditForm.cardNumber.substring(creditForm.cardNumber.length() - 4);
                }
            }
            boolean isValidCard = paymentUtil.checkFailedCard("", creditForm.fleetId, cardType, last4, creditForm.requestId);
            if (!isValidCard) {
                createTokenResponse.returnCode = 448;
                return createTokenResponse.toString();
            }
            String gatewayName = "";
            Credit credit = new Credit();
            //create customerId
            if (customerId.equals(""))
                customerId = generateCustomerId();
            if ("fleetOwner".equals(type) || creditForm.isCrossZone) {
                gatewayName = KeysUtil.STRIPE;
                GatewayStripe gateway = null;
                if (!fleetInfor.affiliateEnvironment) {
                    gateway = mongoDao.getGatewayStripeSystem(true);//true is sandbox
                } else {
                    gateway = mongoDao.getGatewayStripeSystem(false);//false is product
                }
                if (gateway.publicKey.equals("") || gateway.secretKey.equals("")) {
                    createTokenResponse.returnCode = 415;
                    return createTokenResponse.toString();
                } else
                    createTokenResponse = this.createTokenStripe(creditForm, customerId, gateway, credit, "USD", type); // use USD as default cross zone currency

            } else {
                String zoneId = "";
                // if using multi gateway, find zone then get gateway and currency which is assigned to this zone
                // then assign currency to fleet for using later
                // otherwise get currency from setting currency validation
                boolean multiGateway = false;
                ConfigGateway configGateway = null;
                if(fleetInfor.creditConfig != null && fleetInfor.creditConfig.enable){
                    if (fleetInfor.creditConfig.multiGateway) {
                        multiGateway = true;
                        zoneId = creditForm.zoneId != null ? creditForm.zoneId : "";
                        // if zoneId is empty, query zone by geo location
                        // otherwise, find zone by zoneId
                        Zone zone = null;
                        if (zoneId.trim().isEmpty()) {
                            zone = mongoDao.findByFleetIdAndGeo(fleetInfor.fleetId, creditForm.geoLocation);
                        } else {
                            zone = mongoDao.getZoneById(zoneId);
                        }
                        if (zone != null) {
                            zoneId = zone._id.toString();
                            fleetInfor.currencyISOValidation = zone.currency.iso;
                        } else {
                            createTokenResponse.returnCode = 406;
                            return createTokenResponse.toString();
                        }
                    }

                    if (multiGateway) {
                        for (ConfigGateway config : fleetInfor.creditConfig.configGateway) {
                            if (config.zones.contains(zoneId)) {
                                configGateway = config;
                                break;
                            }
                        }
                    } else {
                        configGateway = (fleetInfor.creditConfig.configGateway != null && fleetInfor.creditConfig.configGateway.size() > 0) ? fleetInfor.creditConfig.configGateway.get(0) : null;
                    }
                }

                if (configGateway == null) {
                    createTokenResponse.returnCode = 415;
                    return createTokenResponse.toString();
                }
                gatewayName = configGateway.gateway;
                // if gateway is Avis, check card type then switch to correct gateway
                if (gatewayName.equals(KeysUtil.AVIS)) {
                    boolean isUnionpay = paymentUtil.isUnionPayCard(creditForm.fleetId, creditForm.cardNumber);
                    if (isUnionpay)
                        gatewayName = KeysUtil.YEEPAY;
                    else
                        gatewayName = KeysUtil.PINGPONG;

                    // if type is corporate, try to get phone from Admin account
                    String phone = creditForm.phone != null ? creditForm.phone.trim() : "";
                    if (type.equals("corporate") && !phone.isEmpty()) {
                        MongoDao mongoDao = new MongoDaoImpl();
                        Corporate corporate = mongoDao.getCorporate(creditForm.userId);
                        if (corporate != null && corporate.adminAccount != null) {
                            creditForm.phone = corporate.adminAccount.phone != null ? corporate.adminAccount.phone : "";
                        }
                    }

                }
                if (gatewayName.equals(KeysUtil.YEEPAY)) cardType = KeysUtil.UNIONPAY;

                // check gateway apply 3DS
                boolean enable3DS = false;

                String adyenMode = "";
                if (gatewayName.equals(KeysUtil.ADYEN)) {
                    GatewayAdyen gatewayAdyen = mongoDao.getGatewayAdyenFleet(fleetInfor.fleetId);
                    adyenMode = gatewayAdyen.mode != null ? gatewayAdyen.mode : "";
                    if (adyenMode.isEmpty()) {
                        createTokenResponse.returnCode = 415;
                        return createTokenResponse.toString();
                    }
                }
                if (gatewayName.equals(KeysUtil.ADYEN) && adyenMode.equalsIgnoreCase("HPP")) {
                    enable3DS = true;
                }

                // check 3DS data and redirect
                String data3ds = creditForm.data3ds != null ? creditForm.data3ds : "";

                if (gatewayName.equals(KeysUtil.FAC)) {
                    GatewayFirstAtlanticCommerce fac = mongoDao.getGatewayFACFleet(fleetInfor.fleetId);
                    if (fac != null && fac.powerTranz)
                        enable3DS = true;
                }

                if (gatewayName.equals(KeysUtil.SENANGPAY)) {
                    enable3DS = ConvertUtil.compareVersion(creditForm.rv, "4.6.3900");
                    logger.debug(creditForm.requestId + " - data3ds: " + data3ds);
                    // remove cache if exists
                    if (data3ds.contains(KeysUtil.CALLBACK_URL)) {
                        data3ds = URLDecoder.decode(data3ds, "UTF-8");
                        List<NameValuePair> params = URLEncodedUtils.parse(new URI(data3ds), "UTF-8");

                        for (NameValuePair param : params) {
                            if (param.getName().equals("order_id")) {
                                String order_id = param.getValue();
                                logger.debug(creditForm.requestId + " - remove cache data for order_id: " + order_id);
                                redisDao.removeTmp(creditForm.requestId, KeysUtil.SENANGPAY + ":" + order_id);
                                break;
                            }
                        }
                    }
                }

                if (!data3ds.isEmpty() && (KeysUtil.GATEWAY_3DS.contains(gatewayName) || enable3DS)) {
                    return completed3DSTransaction(creditForm, gatewayName, type, fleetInfor.currencyISOValidation);
                }

                if (configGateway.zipCode.enable && !creditForm.isCrossZone) {
                    int codeValidate = creditForm.validatePostalCode();
                    logger.debug(creditForm.requestId + " - validate postalCode: " + codeValidate);
                    /*if (codeValidate != 200) {
                        createTokenResponse.returnCode = codeValidate;
                        return createTokenResponse.toString();
                    }*/
                } else creditForm.postalCode = "";

                if (!creditForm.isCrossZone) {
                    // check match phone with card local zone
                    if (configGateway.zipCode != null && configGateway.zipCode.matchPhone) {
                        boolean matchPhoneZip = checkMatchPhoneZip(creditForm);
                        logger.debug(creditForm.requestId + " - matchPhoneZip: " + matchPhoneZip);
                        if (!matchPhoneZip) {
                            createTokenResponse.returnCode = 452;
                            return createTokenResponse.toString();
                        }
                    }
                }

                if (!isAuthorized && (KeysUtil.GATEWAY_3DS.contains(gatewayName) || enable3DS)) {
                    Map<String,String> creditData = new HashMap<>();
                    creditData.put("fleetId", creditForm.fleetId);
                    creditData.put("userId", creditForm.userId);
                    creditData.put("cardHolder", creditForm.cardHolder);
                    creditData.put("cardNumber", creditForm.cardNumber);
                    creditData.put("cvv", creditForm.cvv);
                    creditData.put("expiredDate", creditForm.expiredDate);
                    creditData.put("street", creditForm.street != null ? creditForm.street : "");
                    creditData.put("city", creditForm.city != null ? creditForm.city : "");
                    creditData.put("state", creditForm.state != null ? creditForm.state : "");
                    creditData.put("country", creditForm.country != null ? creditForm.country : "");
                    creditData.put("postalCode", creditForm.postalCode != null ? creditForm.postalCode : "");
                    creditData.put("cardType", cardType);
                    creditData.put("creditType", creditForm.creditType != null ? creditForm.creditType : ""); // used for Avis/Yeepay
                    creditData.put("creditCode", creditForm.creditCode != null ? creditForm.creditCode : ""); // used for Avis/Yeepay
                    creditData.put("customerEmail", creditForm.email != null ? creditForm.email : ""); // used for Flutterwave
                    creditData.put("street", creditForm.street != null ? creditForm.street : ""); // used for Flutterwave
                    String registerTye = creditForm.type != null ? creditForm.type : "";
                    if (registerTye.isEmpty()) {
                        registerTye = !type.isEmpty() ? type : "user";
                    }
                    creditData.put("registerType" , creditForm.type != null ? creditForm.type : registerTye);
                    creditData.put("corporateId" , creditForm.corporateId!= null ? creditForm.corporateId : "");
                    String result = paymentUtil.get3DSData("addToken", "", fleetInfor, gatewayName, creditData, customerId, creditForm.phone,
                            fleetInfor.currencyISOValidation, creditForm.requestId,creditForm);

                    // cache data of the 1st request for async notification later
                    if (gatewayName.equals(KeysUtil.SENANGPAY)) {
                        ObjResponse response = gson.fromJson(result, ObjResponse.class);
                        if (response.returnCode == 200) {
                            Map<String,String> mapResponse = (Map<String,String>) response.response;
                            String transactionId = mapResponse.get("transactionId") != null ? mapResponse.get("transactionId") : "";
                            logger.debug(creditForm.requestId + " - cache data for order_id: " + transactionId);
                            redisDao.addTmpData(creditForm.requestId, KeysUtil.SENANGPAY + ":" + transactionId, gson.toJson(creditForm));
                        }
                    }
                    return result;
                }
                createTokenResponse = this.createTokenByGateway(creditForm, gatewayName, fleetInfor, customerId, type);
            }
            if (createTokenResponse.returnCode == 200) {//
                credit = (Credit) createTokenResponse.response;
                if (!gatewayName.equals(KeysUtil.FAC)) {
                    credit.street = creditForm.street != null ? creditForm.street : "";
                    credit.city = creditForm.city != null ? creditForm.city : "";
                    credit.state = creditForm.state != null ? creditForm.state : "";
                    credit.country = creditForm.country != null ? creditForm.country : "";
                    credit.zipCode = postalCode;
                }
                if ("fleetOwner".equals(type) || creditForm.isCrossZone) {
                    credit.gateway = KeysUtil.STRIPE;
                    // set default card if this is the first one
                    if (fleetInfor.credits == null || fleetInfor.credits.isEmpty()) {
                        credit.defaultCard = true;
                    }
                }else {
                    if (gatewayName.equals(KeysUtil.YEEPAY) || gatewayName.equals(KeysUtil.PINGPONG)) {
                        gatewayName = KeysUtil.AVIS;
                        String creditPhone = creditForm.creditPhone != null ? creditForm.creditPhone : "";
                        if (!creditPhone.isEmpty())
                            credit.creditPhone = creditPhone;
                    }
                    credit.gateway = gatewayName;
                }
                if((!this.isEmptyString(creditForm.userId) && !creditForm.oneTimeUse) || "fleetOwner".equals(type)){
                    boolean isSaveDatabase = false;

                    List<String> gatewayCacheCard = Arrays.asList(KeysUtil.CREDITCORP, KeysUtil.YEEPAY);
                    if (gatewayCacheCard.contains(gatewayName)) {
                        CreateTokenThread createTokenThread = new CreateTokenThread();
                        if ("fleetOwner".equals(type) || creditForm.isCrossZone)
                            createTokenThread.token = credit.crossToken;
                        else
                            createTokenThread.token = credit.localToken;
                        createTokenThread.creditForm = creditForm;
                        createTokenThread.requestId = creditForm.requestId;
                        createTokenThread.start();
                    }
                    String jsonString = gson.toJson(createTokenResponse.response);
                    isSaveDatabase = mongoDao.addCreditCard(jsonString, creditForm, type);
                    if (!isSaveDatabase ) {
                        createTokenResponse.returnCode = 437;
                        return createTokenResponse.toString();
                    }

                }

                createTokenResponse.response = credit;

                // check and update email to passenger profile
                if (type.equals("user") || type.equals("customer")) {
                    String email = creditForm.email != null ? creditForm.email : "";
                    if (!email.isEmpty()) {
                        boolean isUpdatedEmail = mongoDao.updateAccountEmail(creditForm.userId, email);
                        logger.debug(creditForm.requestId + " - isUpdatedEmail: " + isUpdatedEmail);
                    }
                }
            } else {
                // add card failed from gateway - add limit retry

                logger.debug(creditForm.requestId + " - createTokenFailure: " + createTokenResponse.toString());
                paymentUtil.addFailedCard(creditForm.fleetId, cardType, last4, creditForm.requestId);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(creditForm.requestId + " - createTokenException: " + CommonUtils.getError(ex) + " - data: " + creditForm.toString());
            createTokenResponse.returnCode = 437;
        }

        return createTokenResponse.toString();
    }

    public String checkUnionpayCard(String fleetId, String cardNumber){
        ObjResponse paymentResponse = new ObjResponse();

        boolean result = paymentUtil.isUnionPayCard(fleetId, cardNumber);

        paymentResponse.returnCode = 200;
        paymentResponse.response = result;
        return paymentResponse.toString();
    }

    public String deleteCreditCorporate(CreditDeleteEnt deleteCreditForm){
        if (deleteCreditForm.localToken == null)
            deleteCreditForm.localToken = "";
        if (deleteCreditForm.crossToken == null)
            deleteCreditForm.crossToken = "";
        ObjResponse deleteTokenResponse = new ObjResponse();
        deleteTokenResponse.returnCode = 200;
        try {
            String gatewayName = "";
            String deleteToken = "";
            boolean localZone = deleteCreditForm.localToken.length()>0;
            String token = localZone ? deleteCreditForm.localToken : deleteCreditForm.crossToken;
            for (Credit credit : mongoDao.getCorporate(deleteCreditForm.userId).credits) {
                deleteToken = localZone ? credit.localToken : credit.crossToken;
                if (deleteToken.equals(token) || deleteToken.contains(token)) {
                    gatewayName = credit.gateway != null ? credit.gateway.trim() : "";
                    if (localZone) {
                        deleteCreditForm.localToken = deleteToken;
                    } else {
                        deleteCreditForm.crossToken = deleteToken;
                    }
                }
            }

            boolean deleteResult = mongoDao.deleteCreditCorporate(deleteCreditForm, gatewayName);

            if (!deleteResult) {
                logger.debug(deleteCreditForm.requestId + " - dataBaseResult : false");
                deleteTokenResponse.returnCode = 437;
                deleteTokenResponse.response = "dataBaseResult : false";
            }else {
                if (gatewayName.equals(KeysUtil.AVIS)) {
                    // unbind token with userID on PingPong
                    if (deleteToken.contains(KeysUtil.TEMPKEY)) {
                        GatewayPingPong gatewayFleet = mongoDao.getGatewayPingPongFleet(deleteCreditForm.fleetId);
                        GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.PINGPONG, gatewayFleet.environment);
                        Boolean isSandbox = gatewayFleet.environment.equalsIgnoreCase(KeysUtil.SANDBOX);
                        PingPongUtil pingPongUtil = new PingPongUtil(gatewayFleet.clientId, gatewayFleet.accId, gatewayFleet.salt, isSandbox, gatewayURL.url, deleteCreditForm.requestId);
                        pingPongUtil.unbind(deleteToken, deleteCreditForm.userId);
                    }
                }

                DeleteTokenThread deleteTokenThread = new DeleteTokenThread();
                if(deleteCreditForm.crossToken.length()>0)
                    deleteTokenThread.token = deleteCreditForm.crossToken;
                else
                    deleteTokenThread.token = deleteCreditForm.localToken;
                deleteTokenThread.gateway = gatewayName;
                deleteTokenThread.requestId = deleteCreditForm.requestId;
                deleteTokenThread.start();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            deleteTokenResponse.returnCode = 406;
        }
        return deleteTokenResponse.toString();
    }

    public String deleteToken(CreditDeleteEnt deleteCreditForm){
        if (deleteCreditForm.localToken == null)
            deleteCreditForm.localToken = "";
        if (deleteCreditForm.crossToken == null)
            deleteCreditForm.crossToken = "";
        ObjResponse deleteTokenResponse = new ObjResponse();
        deleteTokenResponse.returnCode = 200;
        try {
            final List<String> tokenIds = Arrays.asList(deleteCreditForm.localToken.length()>0?deleteCreditForm.localToken.split(","):deleteCreditForm.crossToken.split(","));
            // add more token without "=" symbol, in case some tokens are ending with "=", causing miss match error
            List<String> compareToken = new ArrayList<>();
            for (String token : tokenIds) {
                compareToken.add(token);
                compareToken.add(token.replace("=", ""));
            }
            List<Credit> listCredits = new ArrayList<>();
            for (Credit credit : mongoDao.getCreditByUserId(deleteCreditForm.userId)) {
                String fleetId = credit.localFleetId != null ? credit.localFleetId : "";
                String gatewayName = credit.gateway != null ? credit.gateway.trim() : "";
                boolean isDelete = false;

                String deleteToken = deleteCreditForm.localToken.length()>0 ? credit.localToken : credit.crossToken;
                // if list delete token is contain the token then delete this credit object
                // otherwise add to the list to update account later
                if (compareToken.contains(deleteToken.replace("=", ""))) {
                    isDelete = true;
                } else {
                    if (!listCredits.contains(credit))
                        listCredits.add(credit);
                }

                if (isDelete) {
                    DeleteTokenThread deleteTokenThread = new DeleteTokenThread();
                    deleteTokenThread.token = deleteToken;
                    deleteTokenThread.gateway = gatewayName;
                    deleteTokenThread.requestId = deleteCreditForm.requestId;
                    deleteTokenThread.start();
                }

                /*if (gatewayName.equals(KeysUtil.AVIS)) {
                    // unbind token with userID on PingPong
                    if (deleteToken.contains(KeysUtil.TEMPKEY)) {
                        GatewayPingPong gatewayFleet = mongoDao.getGatewayPingPongFleet(fleetId);
                        GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.PINGPONG, gatewayFleet.environment);
                        Boolean isSandbox = gatewayFleet.environment.equalsIgnoreCase(KeysUtil.SANDBOX);
                        PingPongUtil pingPongUtil = new PingPongUtil(gatewayFleet.clientId, gatewayFleet.accId, gatewayFleet.salt, isSandbox, gatewayURL.url, deleteCreditForm.requestId);
                        pingPongUtil.unbind(deleteToken, deleteCreditForm.userId);
                    }
                }*/
            }
            boolean deleteResult = mongoDao.updateCreditCards(deleteCreditForm.userId,listCredits);
            if (!deleteResult) {
                logger.debug(deleteCreditForm.requestId + " - deleteTokenFailure :" + gson.toJson(deleteCreditForm));
                deleteTokenResponse.response = "dataBaseResult : false";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(deleteCreditForm.requestId + " - deleteTokenException: " + CommonUtils.getError(ex));
            deleteTokenResponse.returnCode = 406;
        }
        return deleteTokenResponse.toString();
    }

    public String deleteCreditTokenForFleetOwner(CreditDeleteEnt deleteCreditForm){
        if (deleteCreditForm.localToken == null)
            deleteCreditForm.localToken = "";
        if (deleteCreditForm.crossToken == null)
            deleteCreditForm.crossToken = "";
        ObjResponse deleteTokenResponse = new ObjResponse();
        deleteTokenResponse.returnCode = 200;
        try {
            String gatewayName = "";
            boolean localZone = deleteCreditForm.localToken.length()>0;
            String token = localZone ? deleteCreditForm.localToken : deleteCreditForm.crossToken;
            boolean isDefault = false;
            for (Credit credit : mongoDao.getFleetInfor(deleteCreditForm.fleetId).credits) {
                String deleteToken = localZone ? credit.localToken : credit.crossToken;
                if (deleteToken.equals(token) || deleteToken.contains(token)) {
                    gatewayName = credit.gateway != null ? credit.gateway.trim() : "";
                    if (localZone) {
                        deleteCreditForm.localToken = deleteToken;
                    } else {
                        deleteCreditForm.crossToken = deleteToken;
                    }
                    if (credit.defaultCard != null && credit.defaultCard)
                        isDefault = true;
                    break;
                }
            }
            if (isDefault) {
                deleteTokenResponse.returnCode = 481;
                return deleteTokenResponse.toString();
            }


            boolean deleteResult = mongoDao.deleteCreditFleetOwner(deleteCreditForm, gatewayName);

            if (!deleteResult) {
                logger.debug(deleteCreditForm.requestId + " - dataBaseResult : false");
                deleteTokenResponse.returnCode = 437;
                deleteTokenResponse.response = "dataBaseResult : false";
            }else {
                DeleteTokenThread deleteTokenThread = new DeleteTokenThread();
                if(deleteCreditForm.crossToken.length()>0)
                    deleteTokenThread.token = deleteCreditForm.crossToken;
                else
                    deleteTokenThread.token = deleteCreditForm.localToken;
                deleteTokenThread.gateway = gatewayName;
                deleteTokenThread.requestId = deleteCreditForm.requestId;
                deleteTokenThread.start();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            deleteTokenResponse.returnCode = 406;
        }
        return deleteTokenResponse.toString();
    }

    public String getClientSecret (ClientSecretEnt clientSecretEnt) {
        ObjResponse createTokenResponse = new ObjResponse();
        String requestId = clientSecretEnt.requestId;
        try {
            Fleet fleetInfor = mongoDao.getFleetInfor(clientSecretEnt.fleetId);
            GatewayStripe gateway;
            String stripeProfile = "";
            String name = "";
            String phone = "";
            String email = "";
            if (clientSecretEnt.type.equals("affiliate")) {
                if (!fleetInfor.affiliateEnvironment) {
                    gateway = mongoDao.getGatewayStripeSystem(true);//true is sandbox
                } else {
                    gateway = mongoDao.getGatewayStripeSystem(false);//false is product
                }
                if (gateway.publicKey.equals("") || gateway.secretKey.equals("")) {
                    createTokenResponse.returnCode = 415;
                    return createTokenResponse.toString();
                } else {
                    name = fleetInfor.name;
                    phone = fleetInfor.phone != null ? fleetInfor.phone : "";
                    email = fleetInfor.email != null ? fleetInfor.email : "";
                    if (fleetInfor.credits != null && !fleetInfor.credits.isEmpty()) {
                        for (Credit credit : fleetInfor.credits) {
                            String[] arrToken = credit.localToken.split(KeysUtil.TEMPKEY);
                            String paymentMethodId = arrToken.length > 1 ? arrToken[1] : "";
                            if (paymentMethodId.length() > 4) {
                                stripeProfile = arrToken[0];
                                break;
                            }
                        }
                    }
                    StripeUtil gatewayPayment = new StripeUtil(requestId);
                    return gatewayPayment.getClientSecret(gateway.secretKey, stripeProfile, name, phone, email);
                }
            } else {
                GatewayStripe gatewayFleet = mongoDao.getGatewayStripeFleet(clientSecretEnt.fleetId);
                if (gatewayFleet.publicKey.equals("") || gatewayFleet.secretKey.equals("")) {
                    createTokenResponse.returnCode = 415;
                } else {
                    String userType = clientSecretEnt.userType != null ? clientSecretEnt.userType : "";
                    if (userType.equals("corporate")) {
                        Corporate corporate = mongoDao.getCorporate(clientSecretEnt.userId);
                        if (corporate != null) {
                            name = corporate.companyInfo.name;
                            phone = corporate.adminAccount != null && corporate.adminAccount.phone != null ? corporate.adminAccount.phone : "";
                            email = corporate.adminAccount != null && corporate.adminAccount.email != null ? corporate.adminAccount.email : "";
                            if (corporate.credits != null && !corporate.credits.isEmpty()) {
                                for (Credit credit : corporate.credits) {
                                    String[] arrToken = credit.localToken.split(KeysUtil.TEMPKEY);
                                    String paymentMethodId = arrToken.length > 1 ? arrToken[1] : "";
                                    if (paymentMethodId.length() > 4) {
                                        stripeProfile = arrToken[0];
                                        break;
                                    }
                                }
                            }
                        }
                    } else {
                        Account account = mongoDao.getAccount(clientSecretEnt.userId);
                        if (account != null) {
                            name = account.fullName != null ? account.fullName : "";
                            phone = account.phone;
                            email = account.email != null ? account.email : "";
                            if (account.credits != null && !account.credits.isEmpty()) {
                                for (Credit credit : account.credits) {
                                    String[] arrToken = credit.localToken.split(KeysUtil.TEMPKEY);
                                    String paymentMethodId = arrToken.length > 1 ? arrToken[1] : "";
                                    if (paymentMethodId.length() > 4) {
                                        stripeProfile = arrToken[0];
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    StripeUtil gatewayPayment = new StripeUtil(requestId);
                    return gatewayPayment.getClientSecret(gatewayFleet.secretKey, stripeProfile, name, phone, email);
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            createTokenResponse.returnCode = 406;
        }
        return createTokenResponse.toString();

    }

    public String completeFirstTransaction3DS(String requestId, String data3ds, String gateway) {
        ObjResponse response = new ObjResponse();
        if (gateway.equals(KeysUtil.PAYFORT)) {
            try {
                String fort_id = "";
                String merchant_reference = "";
                String responseCode = "";
                String responseMessage = "";
                String currency = "";
                if (data3ds.contains(KeysUtil.CALLBACK_URL + "?")) { // return the parameters to the call back url
                    data3ds = URLDecoder.decode(data3ds, "UTF-8").replace(" ", "_"); // replace space
                    List<NameValuePair> params = URLEncodedUtils.parse(new URI(data3ds), "UTF-8");

                    for (NameValuePair param : params) {
                        if (param.getName().equals("fort_id")) fort_id = param.getValue();
                        if (param.getName().equals("merchant_reference")) merchant_reference = param.getValue();
                        if (param.getName().equals("response_code")) responseCode = param.getValue();
                        if (param.getName().equals("response_message")) responseMessage = param.getValue().replace("_", " "); // put spaces which were replaces above
                        if (param.getName().equals("currency")) currency = param.getValue();
                    }
                } else {
                    JSONObject json = (JSONObject)new org.json.simple.parser.JSONParser().parse(data3ds);
                    fort_id = json.get("fort_id") != null ? json.get("fort_id").toString() : "";
                    merchant_reference = json.get("merchant_reference") != null ? json.get("merchant_reference").toString() : "";
                    responseCode = json.get("response_code") != null ? json.get("response_code").toString() : "";
                    responseMessage = json.get("response_message") != null ? json.get("response_message").toString() : "";
                    currency = json.get("currency") != null ? json.get("currency").toString() : "";
                }

                if (responseCode.length() > 3)
                    responseCode = responseCode.substring(2);
                CreditEnt creditEnt =  gson.fromJson(redisDao.get3DSData(requestId, merchant_reference, "3dscredit"), CreditEnt.class);
                if (creditEnt != null) {
                    GatewayPayfort gatewayFleet = mongoDao.getGatewayPayfortFleet(creditEnt.fleetId);
                    GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.PAYFORT, gatewayFleet.environment);
                    PayfortUtil payfortUtil = new PayfortUtil(creditEnt.requestId, gatewayFleet.merchantIdentifier, gatewayFleet.accessCode, gatewayFleet.shaType, gatewayFleet.requestPhrase, gatewayFleet.responsePhrase, gatewayURL.url, gatewayURL.payURL);
                    String type = gson.fromJson(redisDao.get3DSData(requestId, merchant_reference, "3dstype"), String.class);
                    int returnCode = payfortUtil.getReturnCodeFromTransactionCode(responseCode);
                    if (returnCode == 200 && type != null) {
                        // VOID auth transaction
                        payfortUtil.voidTransaction("VOID_AUTHORIZATION", fort_id, 1.0, currency);
                        return createToken(creditEnt, type, merchant_reference, true);
                    } else {
                        Map<String,String> objResponse = new HashMap<>();
                        objResponse.put("message", responseMessage);
                        response.returnCode = returnCode;
                        response.response = objResponse;
                    }
                }

            } catch(Exception ex) {
                ex.printStackTrace();
                response.returnCode = 437;
            }
        } else {
            response.returnCode = 415;
        }
        return response.toString();
    }

    public String paymentSenangpayNotificationURL(PaymentNotificationEnt paymentNotificationEnt) {
        try {
            Thread.sleep(5000);
            // check cache data then processing
            String order_id = paymentNotificationEnt.order_id != null ? paymentNotificationEnt.order_id : "";
            String cacheData = redisDao.getTmp(paymentNotificationEnt.requestId, KeysUtil.SENANGPAY + ":" + order_id);
            if (!cacheData.isEmpty()) {
                // remove cache
                logger.debug(paymentNotificationEnt.requestId + " - remove cache data for order_id: " + order_id);
                redisDao.removeTmp(paymentNotificationEnt.requestId, KeysUtil.SENANGPAY + ":" + order_id);

                // generate data3DS
                String data3DS = KeysUtil.CALLBACK_URL + KeysUtil.NOTIFICATION_URL + "?notify=true"
                        + "&status_id=" + paymentNotificationEnt.status_id
                        + "&order_id=" + paymentNotificationEnt.order_id
                        + "&token=" + paymentNotificationEnt.token
                        + "&cc_num=" + paymentNotificationEnt.cc_num
                        + "&cc_type=" + paymentNotificationEnt.cc_type
                        + "&msg=" + paymentNotificationEnt.msg
                        + "&hash=" + paymentNotificationEnt.hash;
                CreditEnt creditEnt = gson.fromJson(cacheData, CreditEnt.class);
                logger.debug(paymentNotificationEnt.requestId + " - cacheData: " + creditEnt.toString());
                creditEnt.requestId = paymentNotificationEnt.requestId;
                creditEnt.data3ds = data3DS;
                String result = createToken(creditEnt, "user", "", false);
                ObjResponse response = gson.fromJson(result, ObjResponse.class);
                org.json.JSONObject apiResponse;
                Map<String, Object> data = new HashMap<String, Object>();
                if (response.returnCode == 200) {
                    data.put("returnCode", 200);
                    data.put("requestId", paymentNotificationEnt.requestId);
                    data.put("userId", creditEnt.userId);
                    data.put("fleetId",creditEnt.fleetId);
                    data.put("credit", response.response);
                    data.put("errorMessage", "");
                } else {
                    JSONObject objCredit = new JSONObject();
                    Map<String, String> responseData = (Map<String, String>) response.response;
                    String errorMessage = responseData.get("message") != null ? responseData.get("message") : "";
                    errorMessage = errorMessage.replace("_", " "); // remove underline
                    data.put("returnCode", 437);
                    data.put("requestId", paymentNotificationEnt.requestId);
                    data.put("userId", creditEnt.userId);
                    data.put("fleetId",creditEnt.fleetId);
                    data.put("credit", objCredit);
                    data.put("errorMessage", errorMessage);
                }
                // send result to Jupiter
                apiResponse = paymentUtil.sendPost(paymentNotificationEnt.requestId, gson.toJson(data),
                        PaymentUtil.add_card_senangpay, ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
                // show log
                logger.debug(paymentNotificationEnt.requestId + " - apiURL: " + PaymentUtil.add_card_senangpay);
                logger.debug(paymentNotificationEnt.requestId + " - data: " + gson.toJson(data));
                logger.debug(paymentNotificationEnt.requestId + " - apiResponse: " + apiResponse);
            }
        } catch (InterruptedException ex) {
            logger.debug(paymentNotificationEnt.requestId + " - InterruptedException: " + CommonUtils.getError(ex));
        }
        return "OK";
    }

    public String setDefaultCardForFleetOwner(CreditDeleteEnt deleteCreditForm){
        ObjResponse deleteTokenResponse = new ObjResponse();
        deleteTokenResponse.returnCode = 200;
        try {
            List<Credit> listCredits = new ArrayList<>();
            for (Credit credit : mongoDao.getFleetInfor(deleteCreditForm.fleetId).credits) {
                if (credit.crossToken.equals(deleteCreditForm.crossToken)) {
                    credit.defaultCard = true;
                } else {
                    credit.defaultCard = false;
                }
                listCredits.add(credit);
            }
            mongoDao.updateCreditCards("fleetOwner", deleteCreditForm.fleetId, listCredits);

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(deleteCreditForm.requestId + " - setDefaultCardForFleetOwnerException: " + CommonUtils.getError(ex));
            deleteTokenResponse.returnCode = 406;
        }
        return deleteTokenResponse.toString();
    }

    /*****/

    private ObjResponse createTokenByGateway(CreditEnt creditForm, String gatewayName, Fleet fleetInfor, String customerId, String type){
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            //call gatewayServer add CreditCar
            Credit credit = new Credit();
            switch (gatewayName) {
                case KeysUtil.STRIPE: {
                    //Get gateway Information
                    GatewayStripe gatewayFleet = mongoDao.getGatewayStripeFleet(creditForm.fleetId);
                    if (gatewayFleet.publicKey.equals("") || gatewayFleet.secretKey.equals("")) {
                        createTokenResponse.returnCode = 415;
                        return createTokenResponse;
                    } else
                        createTokenResponse = this.createTokenStripe(creditForm, customerId, gatewayFleet, credit, fleetInfor.currencyISOValidation, type);
                }
                break;
                case KeysUtil.BRAINTREE: {
                    //Get gateway Information
                    GatewayBraintree gatewayFleet = mongoDao.getGatewayBraintreeFleet(creditForm.fleetId);
                    if (gatewayFleet.publicKey.equals("") || gatewayFleet.merchantID.equals("") || gatewayFleet.privateKey.equals("")) {
                        createTokenResponse.returnCode = 415;
                        return createTokenResponse;
                    } else
                        createTokenResponse = this.createTokenBraintree(creditForm, customerId, gatewayFleet, credit);

                }
                break;
                case KeysUtil.AUTHORIZE: {
                    //Get gateway Information
                    GatewayAuthorize gatewayFleet = mongoDao.getGatewayAuthorizeFleet(creditForm.fleetId);
                    if (gatewayFleet.apiLoginId.equals("") || gatewayFleet.transactionKey.equals("")) {
                        createTokenResponse.returnCode = 415;
                        return createTokenResponse;
                    } else
                        createTokenResponse = this.createTokenAuthorize(creditForm, customerId, gatewayFleet, credit);

                }
                break;
                case KeysUtil.JETPAY: {
                    GatewayJetpay gatewayFleet = mongoDao.getGatewayJetpayFleet(creditForm.fleetId);
                    GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.JETPAY, gatewayFleet.environment);
                    gatewayFleet.serverURL = gatewayURL.url;
                    gatewayFleet.terminalURL = gatewayURL.payURL;
                    String userId = creditForm.userId != null ? creditForm.userId : "";
                    Map<String,String> extraData = GatewayUtil.getExtraData(type, userId);
                    String customerEmail = extraData.get("email");
                    String customerIP = extraData.get("ip");
                    if (gatewayFleet.terminalURL.equals("") || gatewayFleet.terminalId.equals("") || gatewayFleet.serverURL.equals("")) {
                        createTokenResponse.returnCode = 415;
                        return createTokenResponse;
                    } else
                        createTokenResponse = this.createTokenJetpay(creditForm, customerId, gatewayFleet, credit, fleetInfor.currencyISOValidation,
                                customerEmail, customerIP);

                }
                break;
                case KeysUtil.PEACH: {
                    //Get gateway Information
                    GatewayPeachPayments gatewayFleet = mongoDao.getGatewayPeachPaymentsFleet(creditForm.fleetId);
                    GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.PEACH, gatewayFleet.environment);
                    gatewayFleet.serverURL = gatewayURL.url;
                    if (gatewayFleet.clientId.equals("") || gatewayFleet.userId.equals("") || gatewayFleet.password.equals("") || gatewayFleet.serverURL.equals("")) {
                        createTokenResponse.returnCode = 415;
                        return createTokenResponse;
                    } else
                        createTokenResponse = this.createTokenPeachPayments(creditForm, customerId, gatewayFleet, credit, fleetInfor.currencyISOValidation);

                }
                break;
                case KeysUtil.CREDITCORP: {
                    //Get gateway Information
                    GatewayCreditCorp gatewayFleet = mongoDao.getGatewayCreditCorpFleet(creditForm.fleetId);
                    if (gatewayFleet.userName.equals("") || gatewayFleet.password.equals("")) {
                        createTokenResponse.returnCode = 415;
                        return createTokenResponse;
                    } else
                        createTokenResponse = this.createTokenGatewayCreditCorp(creditForm, customerId, gatewayFleet, credit, fleetInfor.currencyISOValidation);
                }
                break;
                case KeysUtil.FAC: {
                    //Get gateway Information
                    GatewayFirstAtlanticCommerce gatewayFleet = mongoDao.getGatewayFACFleet(creditForm.fleetId);
                    Boolean environment;
                    if (gatewayFleet.environment.equalsIgnoreCase(KeysUtil.SANDBOX)) {
                        environment = true;
                    } else {
                        environment = false;
                    }
                    createTokenResponse = this.createTokenFAC(creditForm, customerId, gatewayFleet, credit, fleetInfor.currencyISOValidation, environment, gatewayFleet.powerTranz);
                }
                break;
                case KeysUtil.PAYFORT: {
                    //
                    createTokenResponse = this.createTokenPayfort(creditForm, customerId, credit);
                }
                break;
                case KeysUtil.PINGPONG: {
                    //Get gateway Information
                    GatewayPingPong gatewayFleet = mongoDao.getGatewayPingPongFleet(creditForm.fleetId);
                    Boolean environment;
                    if (gatewayFleet.environment.equalsIgnoreCase(KeysUtil.SANDBOX)) {
                        environment = true;
                    } else {
                        environment = false;
                    }
                    if (gatewayFleet.clientId.equals("") || gatewayFleet.accId.equals("") || gatewayFleet.salt.equals("")) {
                        createTokenResponse.returnCode = 415;
                        return createTokenResponse;
                    } else {
                        String userId = creditForm.userId != null ? creditForm.userId : "";
                        Map<String,String> extraData = GatewayUtil.getExtraData(type, userId);
                        String customerIP = extraData.get("ip");
                        String billFirstName = extraData.get("firstName");
                        String billLastName = extraData.get("lastName");
                        String billPhoneNumber = extraData.get("phone");
                        String billEmail = extraData.get("email");
                        String inputPhone = creditForm.creditPhone != null ? creditForm.creditPhone : "";
                        if (!inputPhone.isEmpty()) {
                            billPhoneNumber = inputPhone;
                        }
                        createTokenResponse = this.createTokenPingPong(creditForm, userId, gatewayFleet, credit, environment, fleetInfor.currencyISOValidation, customerIP,
                                billFirstName,billLastName,billPhoneNumber, billEmail);
                    }
                }
                break;
                case KeysUtil.YEEPAY: {
                    //Get gateway Information
                    GatewayYeepay gatewayFleet = mongoDao.getGatewayYeepayFleet(creditForm.fleetId);
                    GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.YEEPAY, gatewayFleet.environment);
                    String customerPhone = creditForm.phone != null ? creditForm.phone : "";
                    String creditPhone = creditForm.creditPhone != null ? creditForm.creditPhone : "";
                    if (!creditPhone.isEmpty())
                        customerPhone = creditPhone;
                    if (customerPhone.length() > 11) {
                        customerPhone = customerPhone.substring(customerPhone.length() - 11);
                    }
                    createTokenResponse = this.createTokenYeepayWithSMS(creditForm, customerId, customerPhone, gatewayFleet, credit, gatewayURL.url, fleetInfor.currencyISOValidation);
                }
                break;
                case KeysUtil.CONEKTA: {
                    //Get gateway Information
                    GatewayConekta gatewayFleet = mongoDao.getGatewayConektaFleet(creditForm.fleetId);
                    if (gatewayFleet.apiKey.equals("")) {
                        createTokenResponse.returnCode = 415;
                        return createTokenResponse;
                    } else {
                        // valid username and email
                        String userId = creditForm.userId != null ? creditForm.userId : "";
                        Map<String,String> extraData = GatewayUtil.getExtraData(type, userId);
                        String customerName = creditForm.name != null ? creditForm.name : "";
                        if (customerName.trim().isEmpty()) {
                            customerName = extraData.get("firstName") + " " + extraData.get("lastName");
                            if (customerName.trim().isEmpty()) {
                                createTokenResponse.returnCode = 474;
                                return createTokenResponse;
                            }
                        }
                        String customerEmail = creditForm.email != null ? creditForm.email : "";
                        if (customerEmail.trim().isEmpty()) {
                            customerEmail = extraData.get("email");
                            if (customerEmail.trim().isEmpty()) {
                                createTokenResponse.returnCode = 470;
                                return createTokenResponse;
                            }
                        }
                        createTokenResponse = this.createTokenConekta(creditForm, customerId, gatewayFleet, credit, customerName, customerEmail, fleetInfor.currencyISOValidation);
                    }
                }
                break;
                case KeysUtil.SENANGPAY: {
                    boolean enable3DS = ConvertUtil.compareVersion(creditForm.rv, "4.6.3900");
                    if (enable3DS) {
                        String tmpData = redisDao.get3DSData(creditForm.requestId, customerId, "3dstoken");
                        logger.debug(creditForm.requestId + " - tmpData: " + tmpData);
                        ObjResponse response = gson.fromJson(tmpData, ObjResponse.class);
                        createTokenResponse.returnCode = response.returnCode;
                        if (createTokenResponse.returnCode == 200) {// add token for fleet successful
                            Map<String,String> mapResponse = (Map<String,String>) response.response;
                            if (creditForm.isCrossZone) {
                                credit.localToken = "";
                                credit.crossToken = mapResponse.get("token");
                            } else {
                                credit.localToken = mapResponse.get("token");
                                credit.crossToken = "";
                            }
                            credit._id = customerId;
                            credit.cardHolder = mapResponse.get("cardHolder");
                            credit.localFleetId = creditForm.fleetId;
                            credit.cardType = mapResponse.get("cardType");
                            credit.cardMask = mapResponse.get("creditCard");
                            credit.userId = creditForm.userId;
                            createTokenResponse.response = credit;
                        } else {
                            logger.debug(creditForm.requestId + " - createTokenSenanpayFailure: " + tmpData + " - data: " + creditForm.toString());
                            try {
                                createTokenResponse = gson.fromJson(tmpData, ObjResponse.class);
                            } catch (Exception ignore){}
                        }
                    } else {
                        //Get gateway Information
                        GatewaySenangpay gatewayFleet = mongoDao.getGatewaySenangpayFleet(creditForm.fleetId);
                        if (gatewayFleet.merchantAccount.equals("") || gatewayFleet.wsUser.equals("") || gatewayFleet.secretKey.equals("")) {
                            createTokenResponse.returnCode = 415;
                            return createTokenResponse;
                        } else {
                            String userId = creditForm.userId != null ? creditForm.userId : "";
                            Map<String,String> extraData = GatewayUtil.getExtraData(type, userId);
                            String customerName = creditForm.cardHolder != null ? creditForm.cardHolder : "";
                            if (customerName.isEmpty())
                                customerName = extraData.get("firstName") + " " + extraData.get("lastName");
                            String customerEmail = creditForm.email != null ? creditForm.email : "";
                            if (customerEmail.trim().isEmpty()) {
                                customerEmail = extraData.get("email");
                                if (customerEmail.trim().isEmpty()) {
                                    createTokenResponse.returnCode = 470;
                                    return createTokenResponse;
                                }
                            }
                            String customerPhone = creditForm.phone != null ? creditForm.phone : "";
                            if (customerPhone.trim().isEmpty()) {
                                customerPhone = extraData.get("phone");
                                if (customerPhone.trim().isEmpty()) {
                                    createTokenResponse.returnCode = 475;
                                    return createTokenResponse;
                                }
                            }
                            createTokenResponse = this.createTokenGatewaySenangpay(creditForm, customerId, customerName, customerEmail, customerPhone, gatewayFleet, credit, fleetInfor.currencyISOValidation);
                        }
                    }
                }
                break;
                case KeysUtil.FIRSTDATA: {
                    String tmpData = redisDao.get3DSData(creditForm.requestId, customerId, "3dstoken");
                    logger.debug(creditForm.requestId + " - tmpData: " + tmpData);
                    createTokenResponse = this.createTokenGatewayFirstData(creditForm, customerId, credit, tmpData);
                }
                break;
                case KeysUtil.EGHL: {
                    // retrieve token data from cache
                    createTokenResponse = this.createTokenEGHL(creditForm, customerId, credit, fleetInfor.currencyISOValidation);
                }
                break;
                case KeysUtil.CXPAY: {
                    //Get gateway Information
                    GatewayCXPay gatewayFleet = mongoDao.getGatewayCXPayFleet(creditForm.fleetId);
                    if (gatewayFleet.apiKey.equals("")) {
                        createTokenResponse.returnCode = 415;
                        return createTokenResponse;
                    } else {
                        String userId = creditForm.userId != null ? creditForm.userId : "";
                        Map<String,String> extraData = GatewayUtil.getExtraData(type, userId);
                        String customerEmail = extraData.get("email");
                        String customerPhone = extraData.get("phone");
                        //CXPayUtil cxPayUtil = new CXPayUtil();
                        createTokenResponse = this.createTokenGatewayCXPay(creditForm, customerId, customerEmail, customerPhone, gatewayFleet, credit, fleetInfor.currencyISOValidation);
                    }

                }
                break;
                case KeysUtil.FLUTTERWAVE: {
                    String customerEmail = creditForm.email != null ? creditForm.email : "";
                    if (customerEmail.equals("")) {
                        String userId = creditForm.userId != null ? creditForm.userId : "";
                        Map<String,String> extraData = GatewayUtil.getExtraData(type, userId);
                        customerEmail = extraData.get("email");
                        if (customerEmail.equals("")) {
                            createTokenResponse.returnCode = 470;
                            return createTokenResponse;
                        }
                    }
                    createTokenResponse = this.createTokenFlutterwave(creditForm, customerId, customerEmail, credit);
                }
                break;
                case KeysUtil.TSYS:
                case KeysUtil.BOG:
                case KeysUtil.PAYMAYA:
                case KeysUtil.MADA:
                case KeysUtil.ECPAY:
                case KeysUtil.PAYWAY:
                case KeysUtil.ONEPAY:{
                    // retrieve token data from cache
                    createTokenResponse = this.createTokenFromCache(creditForm, customerId, credit);
                }
                break;
                default: {
                    createTokenResponse.returnCode = 525;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(creditForm.requestId + " - createTokenByGatewayException: " + CommonUtils.getError(ex) + "    data" + creditForm.toString());
            createTokenResponse.returnCode = 437;
        }

        return createTokenResponse;
    }

    private ObjResponse createTokenJetpay(CreditEnt creditForm, String customerId, GatewayJetpay gateway, Credit creditCard, String currencyISO,
                                         String customerEmail, String customerIP){
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            JetpayUtil gatewayPayment = new JetpayUtil(creditForm.requestId);
            //call Server to add Credit Card
            Map<String, String> account = new HashMap<String, String>();
            account.put("serverUrl", gateway.serverURL);
            account.put("terminalId", gateway.terminalId);
            account.put("terminalURL", gateway.terminalURL);
            account.put("currencyISO", currencyISO);
            account.put("customerEmail", customerEmail);
            account.put("customerIP", customerIP);
            String resultCreateToken = gatewayPayment.createCreditToken(creditForm, account);

            ObjectMapper mapperResult = new ObjectMapper();
            Map<String, Object> mapResult = mapperResult.readValue(resultCreateToken, new TypeReference<HashMap<String, Object>>() {
            });
            createTokenResponse.returnCode = Integer.parseInt(mapResult.get("returnCode").toString());
            if (createTokenResponse.returnCode == 200) {// add token for fleet successful
                Map<String, String> mapResponse = (Map<String, String>) mapResult.get("response");
                if (creditForm.isCrossZone) {
                    creditCard.localToken = "";
                    creditCard.crossToken = mapResponse.get("token");
                } else {
                    creditCard.localToken = mapResponse.get("token");
                    creditCard.crossToken = "";
                }
                creditCard._id = customerId;
                creditCard.cardHolder = creditForm.cardHolder;
                creditCard.localFleetId = creditForm.fleetId;
                creditCard.cardType = mapResponse.get("cardType");
                creditCard.cardMask = mapResponse.get("creditCard");
                creditCard.street = mapResponse.get("street");
                creditCard.city = mapResponse.get("city");
                creditCard.state = mapResponse.get("state");
                creditCard.zipCode = mapResponse.get("zipCode");
                creditCard.country = mapResponse.get("country");
                createTokenResponse.response = creditCard;
            }
            else {
                logger.debug(creditForm.requestId + " - createTokenJetpayFailure: " + resultCreateToken + " - data: " + creditForm.toString());
                try {
                    createTokenResponse = gson.fromJson(resultCreateToken, ObjResponse.class);
                } catch (Exception ignore){}
            }
        } catch (Exception ex) {
            logger.debug(creditForm.requestId + " - createTokenJetpayException: " + CommonUtils.getError(ex) + " - data: " + creditForm.toString());
        }
        return createTokenResponse;
    }

    private ObjResponse createTokenPeachPayments(CreditEnt creditForm, String customerId, GatewayPeachPayments gateway, Credit creditCard, String currencyISO){
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            PeachPaymentsUtil gatewayPayment = new PeachPaymentsUtil(creditForm.requestId);
            //call Server to add Credit Card
            Map<String, String> account = new HashMap<String, String>();
            account.put("serverUrl", gateway.serverURL);
            account.put("clientId", gateway.clientId);
            account.put("userId", gateway.userId);
            account.put("password", gateway.password);
            String resultCreateToken = gatewayPayment.createCreditToken(creditForm, account, currencyISO);

            ObjectMapper mapperResult = new ObjectMapper();
            Map<String, Object> mapResult = mapperResult.readValue(resultCreateToken, new TypeReference<HashMap<String, Object>>() {
            });
            createTokenResponse.returnCode = Integer.parseInt(mapResult.get("returnCode").toString());
            if (createTokenResponse.returnCode == 200) {// add token for fleet sucessful
                Map<String, String> mapResponse = (Map<String, String>) mapResult.get("response");
                if (creditForm.isCrossZone) {
                    creditCard.localToken = "";
                    creditCard.crossToken = mapResponse.get("token");
                } else {
                    creditCard.localToken = mapResponse.get("token");
                    creditCard.crossToken = "";
                }
                creditCard._id = customerId;
                creditCard.cardHolder = creditForm.cardHolder;
                creditCard.localFleetId = creditForm.fleetId;
                creditCard.cardType = mapResponse.get("cardType");
                creditCard.cardMask = mapResponse.get("creditCard");
                createTokenResponse.response = creditCard;
            }
            else {
                logger.debug(creditForm.requestId + " - createTokenPeachPaymentsFailure: " + resultCreateToken + " - data: " + creditForm.toString());
                try {
                    createTokenResponse = gson.fromJson(resultCreateToken, ObjResponse.class);
                } catch (Exception ignore){}
            }
        } catch (Exception ex) {
            logger.debug(creditForm.requestId + " - createTokenPeachPaymentsException: " + CommonUtils.getError(ex) + " - data: " + creditForm.toString());
        }

        return createTokenResponse;
    }

    private ObjResponse createTokenAuthorize(CreditEnt creditForm, String customerId, GatewayAuthorize gateway, Credit creditCard){
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            AuthorizeUtil gatewayPayment = new AuthorizeUtil(creditForm.requestId);
            //call Server to add Credit Card
            Map<String, String> account = new HashMap<String, String>();
            account.put("apiLoginId", gateway.apiLoginId);
            account.put("transactionKey", gateway.transactionKey);
            account.put("environment", gateway.environment);
            String resultCreateToken = gatewayPayment.createCreditToken(creditForm, customerId, account);

            ObjectMapper mapperResult = new ObjectMapper();
            Map<String, Object> mapResult = mapperResult.readValue(resultCreateToken, new TypeReference<HashMap<String, Object>>() {
            });
            createTokenResponse.returnCode = Integer.parseInt(mapResult.get("returnCode").toString());
            if (createTokenResponse.returnCode == 200) {// add token for fleet sucessful
                Map<String, String> mapResponse = (Map<String, String>) mapResult.get("response");
                if (creditForm.isCrossZone) {
                    creditCard.localToken = "";
                    creditCard.crossToken = mapResponse.get("token");
                } else {
                    creditCard.localToken = mapResponse.get("token");
                    creditCard.crossToken = "";
                }
                creditCard._id = customerId;
                creditCard.cardHolder = creditForm.cardHolder;
                creditCard.localFleetId = creditForm.fleetId;
                creditCard.cardType = mapResponse.get("cardType");
                creditCard.cardMask = mapResponse.get("creditCard");
                createTokenResponse.response = creditCard;
            }
            else {
                logger.debug(creditForm.requestId + " - createTokenAuthorizeFailure: " + resultCreateToken + " - data: " + creditForm.toString());
                try {
                    createTokenResponse = gson.fromJson(resultCreateToken, ObjResponse.class);
                } catch (Exception ignore){}
            }
        } catch (Exception ex) {
            logger.debug(creditForm.requestId + " - createTokenAuthorizeException: " + CommonUtils.getError(ex) + " - data: " + creditForm.toString());
        }

        return createTokenResponse;
    }

    private ObjResponse createTokenBraintree(CreditEnt creditForm, String customerId, GatewayBraintree gateway, Credit creditCard){
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            BraintreeUtil gatewayPayment = new BraintreeUtil(creditForm.requestId);
            //call Server to add Credit Card
            Map<String, String> account = new HashMap<String, String>();
            account.put("environment", gateway.environment);
            account.put("merchantID", gateway.merchantID);
            account.put("publicKey", gateway.publicKey);
            account.put("privateKey", gateway.privateKey);
            String resultCreateToken = gatewayPayment.createCreditToken(creditForm, customerId, account);

            ObjectMapper mapperResult = new ObjectMapper();
            Map<String, Object> mapResult = mapperResult.readValue(resultCreateToken, new TypeReference<HashMap<String, Object>>() {
            });
            createTokenResponse.returnCode = Integer.parseInt(mapResult.get("returnCode").toString());
            if (createTokenResponse.returnCode == 200) {// add token for fleet sucessful
                Map<String, String> mapResponse = (Map<String, String>) mapResult.get("response");
                if (creditForm.isCrossZone) {
                    creditCard.localToken = "";
                    creditCard.crossToken = mapResponse.get("token");
                } else {
                    creditCard.localToken = mapResponse.get("token");
                    creditCard.crossToken = "";
                }
                creditCard._id = customerId;
                creditCard.cardHolder = creditForm.cardHolder;
                creditCard.localFleetId = creditForm.fleetId;
                creditCard.cardType = mapResponse.get("cardType");
                creditCard.cardMask = mapResponse.get("creditCard");
                createTokenResponse.response = creditCard;
            }
            else {
                logger.debug(creditForm.requestId + " - createTokenBraintreeFailure: " + resultCreateToken + " - data: " + creditForm.toString());
                try {
                    createTokenResponse = gson.fromJson(resultCreateToken, ObjResponse.class);
                } catch (Exception ignore){}
            }
        } catch (Exception ex) {
            logger.debug(creditForm.requestId + " - createTokenBraintreeException: " + CommonUtils.getError(ex) + " - data: " + creditForm.toString());
        }

        return createTokenResponse;

    }

    private ObjResponse createTokenPingPong(CreditEnt creditForm, String customerId, GatewayPingPong gatewayFleet, Credit creditCard,boolean isSandbox,String currencyISO, String customerIP,
                                           String billFirstName,String billLastName,String billPhoneNumber, String billEmail){
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.PINGPONG, gatewayFleet.environment);
            PingPongUtil gatewayPayment = new PingPongUtil(gatewayFleet.clientId, gatewayFleet.accId, gatewayFleet.salt, isSandbox, gatewayURL.url, creditForm.requestId);
            String merchantTransactionId = GatewayUtil.getOrderId();
            redisDao.addTmpData(creditForm.requestId, merchantTransactionId, creditForm.fleetId);
            //call Server to add Credit Card
            String resultCreateToken = gatewayPayment.createCreditToken(creditForm, customerId, currencyISO,
                    billFirstName,billLastName,billPhoneNumber,billEmail, merchantTransactionId);

            ObjectMapper mapperResult = new ObjectMapper();
            Map<String, Object> mapResult = mapperResult.readValue(resultCreateToken, new TypeReference<HashMap<String, Object>>() {
            });

            createTokenResponse.returnCode = Integer.parseInt(mapResult.get("returnCode").toString());
            if (createTokenResponse.returnCode == 200) {// add token for fleet sucessful
                Map<String, String> mapResponse = (Map<String, String>) mapResult.get("response");
                if (creditForm.isCrossZone) {
                    creditCard.localToken = "";
                    creditCard.crossToken = mapResponse.get("token");
                } else {
                    creditCard.localToken = mapResponse.get("token");
                    creditCard.crossToken = "";
                }
                creditCard._id = customerId;
                creditCard.cardHolder = creditForm.cardHolder;
                creditCard.localFleetId = creditForm.fleetId;
                creditCard.cardType = mapResponse.get("cardType");
                creditCard.cardMask = mapResponse.get("creditCard");
                createTokenResponse.response = creditCard;
            } else {
                logger.debug(creditForm.requestId + " - createTokenPingPongFailure: " + resultCreateToken + " - data: " + creditForm.toString());
                try {
                    createTokenResponse = gson.fromJson(resultCreateToken, ObjResponse.class);
                } catch (Exception ignore){}
            }
        } catch (Exception ex) {
            logger.debug(creditForm.requestId + " - createTokenPingPongException: " + CommonUtils.getError(ex) + " - data: " + creditForm.toString());
        }
        return createTokenResponse;
    }

    private ObjResponse createTokenYeepayWithSMS(CreditEnt creditForm, String customerId, String customerPhone, GatewayYeepay gateway, Credit creditCard, String requestUrl,String currencyISO){
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            YeepayUtil yeepayUtil = new YeepayUtil();
            String r2_TrxId = redisDao.get3DSData(creditForm.requestId, creditForm.userId, "3dsYeepayOrderId");
            // remove prefix "a" from orderId
            // added prefix "a" to avoid Redis do round order number
            if (r2_TrxId != null && !r2_TrxId.isEmpty())
                r2_TrxId = r2_TrxId.substring(1);
            String resultCreateToken = yeepayUtil.createCreditTokenWithSMS(creditForm, gateway.merchantId, gateway.key, customerPhone, requestUrl,
                    r2_TrxId, currencyISO);

            ObjectMapper mapperResult = new ObjectMapper();
            Map<String, Object> mapResult = mapperResult.readValue(resultCreateToken, new TypeReference<HashMap<String, Object>>() {
            });

            createTokenResponse.returnCode = Integer.parseInt(mapResult.get("returnCode").toString());
            if (createTokenResponse.returnCode == 200) {// add token for fleet sucessful
                Map<String, String> mapResponse = (Map<String, String>) mapResult.get("response");
                String userId = "noId";
                if(!this.isEmptyString(creditForm.userId))
                    userId = creditForm.userId ;
                /*int returnCode = paymentUtil.addCardToStore(mapResponse.get("token"), userId, creditForm);
                if (returnCode != 200) {
                    SlackAPI slack = new SlackAPI();
                    slack.sendSlack("SAVE DATA TO GW2 FAILEd ","URGREN", mapResponse.get("token"));

                    // return 200 to continue
                    returnCode = 200;
                }
                if(returnCode != 200 && returnCode != 2){ // 200 mean add completed, 2 mean the same token is added already
                    createTokenResponse.returnCode = 437;
                    return createTokenResponse;
                }*/

                if (creditForm.isCrossZone) {
                    creditCard.localToken = "";
                    creditCard.crossToken = mapResponse.get("token");
                } else {
                    creditCard.localToken = mapResponse.get("token");
                    creditCard.crossToken = "";
                }
                creditCard._id = customerId;
                creditCard.cardHolder = creditForm.cardHolder;
                creditCard.localFleetId = creditForm.fleetId;
                creditCard.cardType = mapResponse.get("cardType");
                creditCard.cardMask = mapResponse.get("creditCard");
                String creditPhone = creditForm.creditPhone != null ? creditForm.creditPhone : "";
                if (!creditPhone.isEmpty())
                    creditCard.creditPhone = creditPhone;

                createTokenResponse.response = creditCard;
            } else {
                logger.debug(creditForm.requestId + " -  createTokenYeepayFailure: " + resultCreateToken + " - data: " + creditForm.toString());
                createTokenResponse.response = mapResult.get("response");
                return createTokenResponse;
            }
        } catch (Exception ex) {
            logger.debug(creditForm.requestId + " - createTokenYeepayException: " + CommonUtils.getError(ex) + " - data: " + creditForm.toString());
        }
        return createTokenResponse;
    }

    private ObjResponse createTokenGatewayCreditCorp(CreditEnt creditForm, String customerId, GatewayCreditCorp gateway, Credit creditCard,String currencyISO){
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.CREDITCORP, "");
            CredicorpUtil gatewayPayment = new CredicorpUtil(creditForm.requestId, gateway.userName, gateway.password, gatewayURL.url, gatewayURL.payURL);
            //call Server to add Credit Card
            String resultCreateToken = gatewayPayment.createCreditToken(creditForm,currencyISO);

            ObjectMapper mapperResult = new ObjectMapper();
            Map<String, Object> mapResult = mapperResult.readValue(resultCreateToken, new TypeReference<HashMap<String, Object>>() {
            });

            createTokenResponse.returnCode = Integer.parseInt(mapResult.get("returnCode").toString());
            if (createTokenResponse.returnCode == 200) {// add token for fleet sucessful
                //save credit to Store
                Map<String, String> mapResponse = (Map<String, String>) mapResult.get("response");
                String userId = "noId";
                if(!this.isEmptyString(creditForm.userId))
                    userId = creditForm.userId ;
                int returnCode = paymentUtil.addCardToStore(creditForm.requestId, mapResponse.get("token"), userId, creditForm);
                if(returnCode != 200){
                    createTokenResponse.returnCode = 437;
                    return createTokenResponse;
                }
                if (creditForm.isCrossZone) {
                    creditCard.localToken = "";
                    creditCard.crossToken = mapResponse.get("token");
                } else {
                    creditCard.localToken = mapResponse.get("token");
                    creditCard.crossToken = "";
                }
                creditCard._id = customerId;
                creditCard.cardHolder = creditForm.cardHolder;
                creditCard.localFleetId = creditForm.fleetId;
                creditCard.cardType = mapResponse.get("cardType");
                creditCard.cardMask = mapResponse.get("creditCard");
                createTokenResponse.response = creditCard;
            } else {
                logger.debug(creditForm.requestId + " - createTokenCreditCorpFailure: " + resultCreateToken + " - data: " + creditForm.toString());
                try {
                    createTokenResponse = gson.fromJson(resultCreateToken, ObjResponse.class);
                } catch (Exception ignore){}
            }
        } catch (Exception ex) {
            logger.debug(creditForm.requestId + " - createTokenCreditCorpException: " + CommonUtils.getError(ex) + " - data: " + creditForm.toString());
        }
        return createTokenResponse;
    }

    boolean checkLimitBBL(String cardNumber, String userId) {
        try {
            String[] VALUES = new String[]{
                    "450535",
                    "473014",
                    "454623",
                    "454626",
                    "454627",
                    "454628",
                    "454629",
                    "454631",
                    "454632",
                    "522985",
                    "544482",
                    "544485",
                    "544488",
                    "377970",
                    "377971",
                    "622354",
                    "411111"
            };
            String beginCard = cardNumber.substring(0, 6);
            Account account = mongoDao.getAccount(userId);
            boolean checkLmitNumber = Arrays.asList(VALUES).contains(beginCard);
            String appName = account.appName;
            if (appName != null && appName.equalsIgnoreCase("blacktie_c") && !checkLmitNumber) {
                return false;
            }

        }
        catch (Exception ex){
            return true;
        }

        return true;

    }

    private ObjResponse createTokenStripe(CreditEnt creditForm, String customerId, GatewayStripe gateway, Credit creditCard, String currencyISO, String type){
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            if(!this.checkLimitBBL(creditForm.cardNumber,creditForm.userId)){
                createTokenResponse.returnCode = 555;
                return createTokenResponse;
            }
            StripeUtil gatewayPayment = new StripeUtil(creditForm.requestId);
            //call Server to add Credit Card
            String resultCreateToken = "";
            String token = creditForm.token != null ? creditForm.token : "";
            if (!token.equals("")) {
                String cardToken = "";
                boolean clientSecret = false;
                try {
                    JSONObject json = (JSONObject)new org.json.simple.parser.JSONParser().parse(token);
                    cardToken = json.get("mId") != null ? json.get("mId").toString() : "";
                    String sty = json.get("clientSecret") != null ? json.get("clientSecret").toString() : "false";
                    clientSecret = Boolean.parseBoolean(sty);
                } catch(Exception ex) {
                    logger.debug(creditForm.requestId + " - card info is empty ");
                }
                if (clientSecret) {
                    StripeIntentUtil sUtil = new  StripeIntentUtil();
                    String paymentIntentId = sUtil.getAuthenRequired(creditForm.userId);
                    // remove cache
                    sUtil.removeAuthenRequired(creditForm.userId);
                    logger.debug(creditForm.requestId + " - driverId: " + creditForm.userId);
                    logger.debug(creditForm.requestId + " - paymentIntentId: " + paymentIntentId);
                    resultCreateToken = gatewayPayment.createCreditTokenFromPI(creditForm.fleetId, paymentIntentId, gateway.secretKey);
                } else {
                    String userId = creditForm.userId != null ? creditForm.userId : "";
                    String name = "";
                    String phone = "";
                    String email = "";
                    if (!userId.isEmpty()) {
                        Account account = mongoDao.getAccount(userId);
                        if (account != null) {
                            name = account.fullName != null ? account.fullName.trim() : "";
                            if ("fleetOwner".equals(type) || creditForm.isCrossZone) {
                                Fleet fleet = mongoDao.getFleetInfor(account.fleetId);
                                name = name + " - " + fleet.name;
                            }
                            phone = account.phone;
                            email = account.email != null ? account.email : "";
                        }
                    }
                    resultCreateToken = gatewayPayment.createCreditToken(creditForm.requestId, creditForm.sca, creditForm.fleetId, name, phone, email, cardToken, customerId, gateway.secretKey);
                }
            } else {
                String userId = creditForm.userId != null ? creditForm.userId : "";
                String name = "";
                String phone = "";
                String email = "";
                if (!userId.isEmpty()) {
                    Account account = mongoDao.getAccount(userId);
                    if (account != null) {
                        name = account.fullName != null ? account.fullName.trim() : "";
                        if ("fleetOwner".equals(type) || creditForm.isCrossZone) {
                            Fleet fleet = mongoDao.getFleetInfor(account.fleetId);
                            name = name + " - " + fleet.name;
                        }
                        phone = account.phone;
                        email = account.email != null ? account.email : "";
                    }
                }
                resultCreateToken = gatewayPayment.createCreditToken(creditForm, name, phone, email, customerId, gateway.secretKey, currencyISO);
            }

            ObjectMapper mapperResult = new ObjectMapper();
            Map<String, Object> mapResult = mapperResult.readValue(resultCreateToken, new TypeReference<HashMap<String, Object>>() {
            });

            createTokenResponse.returnCode = Integer.parseInt(mapResult.get("returnCode").toString());
            if (createTokenResponse.returnCode == 200) {// add token for fleet sucessful
                Map<String, String> mapResponse = (Map<String, String>) mapResult.get("response");
                if ("fleetOwner".equals(type) || creditForm.isCrossZone) {
                    creditCard.localToken = "";
                    creditCard.crossToken = mapResponse.get("token");
                } else {
                    creditCard.localToken = mapResponse.get("token");
                    creditCard.crossToken = "";
                }
                creditCard._id = customerId;
                creditCard.cardHolder = creditForm.cardHolder;
                creditCard.localFleetId = creditForm.fleetId;
                creditCard.cardType = mapResponse.get("cardType");
                creditCard.cardMask = mapResponse.get("creditCard");
                String cardBin = "";
                try {
                    creditForm.cardNumber.substring(0,6);
                }catch (Exception ex){

                }
                boolean isBBLCard = false;
                boolean isSwitchedToBBL = false;
                String corporateId = "";
                String companyName = "";
                List<String> checkCorporateProfile = Arrays.asList("user", "customer");
                if (KeysUtil.BBL_BINCHECK.contains(cardBin) && checkCorporateProfile.contains(type)) {
                    isBBLCard = true;
                    // if account was not assigned to any corporate then assign to BBL
                    Account account = mongoDao.getAccount(creditForm.userId);
                    if (account != null && account.passengerInfo != null) {
                        String currentCorporateId = account.passengerInfo.corporateId != null ? account.passengerInfo.corporateId : "";
                        // after assign to corporate, return corporateId
                        if (currentCorporateId.isEmpty()) {
                            String updateResult = paymentUtil.updateCorporateInfo(creditForm.requestId, creditForm.fleetId, account);
                            if (paymentUtil.getReturnCode(updateResult) == 200) {
                                isSwitchedToBBL = true;
                                JSONObject object = paymentUtil.toObjectResponse(updateResult);
                                JSONObject response = (JSONObject)object.get("response");
                                corporateId = response.get("verifyCorporateId") != null ? response.get("verifyCorporateId").toString() : "";
                                companyName = response.get("companyName") != null ? response.get("companyName").toString() : "";
                            }
                        } else {
                            corporateId = currentCorporateId;
                        }
                    }
                }
                creditCard.isBBLCard = isBBLCard;
                creditCard.corporateId = corporateId;
                creditCard.isSwitchedToBBL = isSwitchedToBBL;
                creditCard.companyName = companyName;
                createTokenResponse.response = creditCard;
            }else {
                logger.debug(creditForm.requestId + " - createTokenStripeFailure: " + resultCreateToken + " - data: " + creditForm.toString());
                // get error message and return
                try {
                    createTokenResponse = gson.fromJson(resultCreateToken, ObjResponse.class);
                } catch (Exception ignore){}
            }
        } catch (Exception ex) {
            logger.debug(creditForm.requestId + " - createTokenStripeException: " + CommonUtils.getError(ex) + " - data: " + creditForm.toString());
            createTokenResponse.returnCode = 437;
        }

        return createTokenResponse;
    }

    private boolean isEmptyString(String s) {
        return (s == null || s.trim().isEmpty());
    }

    private static String generateCustomerId() {
        Calendar calendar = Calendar.getInstance();
        return String.valueOf(calendar.getTimeInMillis());
    }

    private ObjResponse createTokenFAC(CreditEnt creditForm, String customerId, GatewayFirstAtlanticCommerce gateway, Credit creditCard,
                                      String currencyISO, boolean isSandbox, boolean powerTranz){
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            logger.debug(creditForm.requestId + " - powerTranz: " + powerTranz);
            if (powerTranz) {
                String tmpData = redisDao.get3DSData(creditForm.requestId, customerId, "3dstoken");
                logger.debug(creditForm.requestId + " - tmpData: " + tmpData);
                createTokenResponse.returnCode = CommonUtils.getReturnCode(tmpData);
                if (createTokenResponse.returnCode == 200) {// add token for fleet successful
                    if (creditForm.isCrossZone) {
                        creditCard.localToken = "";
                        creditCard.crossToken = CommonUtils.getValue(tmpData, "token");
                    } else {
                        creditCard.localToken = CommonUtils.getValue(tmpData, "token");
                        creditCard.crossToken = "";
                    }
                    creditCard._id = customerId;
                    creditCard.cardHolder = creditForm.cardHolder;
                    creditCard.localFleetId = creditForm.fleetId;
                    creditCard.cardType = CommonUtils.getValue(tmpData, "cardType");
                    creditCard.cardMask = CommonUtils.getValue(tmpData, "creditCard");
                    creditCard.street = CommonUtils.getValue(tmpData, "address");
                    creditCard.city = CommonUtils.getValue(tmpData, "city");
                    creditCard.state = CommonUtils.getValue(tmpData, "state");
                    creditCard.zipCode = CommonUtils.getValue(tmpData, "postal");
                    creditCard.country = CommonUtils.getValue(tmpData, "country");
                    creditCard.billingEmail = CommonUtils.getValue(tmpData, "email");
                    creditCard.userId = creditForm.userId;
                    createTokenResponse.response = creditCard;
                } else {
                    logger.debug(creditForm.requestId + " - createTokenFACFailure: " + tmpData + " - data: " + creditForm.toString());
                    try {
                        createTokenResponse = gson.fromJson(tmpData, ObjResponse.class);
                    } catch (Exception ignore){}
                }
            } else {
                FirstAtlanticCommerceUtil gatewayPayment = new FirstAtlanticCommerceUtil(creditForm.requestId);
                //call Server to add Credit Card
                String resultCreateToken = gatewayPayment.createCreditToken(creditForm, gateway.merchantId, gateway.acquirerId, gateway.password, currencyISO, isSandbox);

                ObjectMapper mapperResult = new ObjectMapper();
                Map<String, Object> mapResult = mapperResult.readValue(resultCreateToken, new TypeReference<HashMap<String, Object>>() {
                });

                createTokenResponse.returnCode = Integer.parseInt(mapResult.get("returnCode").toString());
                if (createTokenResponse.returnCode == 200) {// add token for fleet sucessful
                    Map<String, String> mapResponse = (Map<String, String>) mapResult.get("response");
                    if (creditForm.isCrossZone) {
                        creditCard.localToken = "";
                        creditCard.crossToken = mapResponse.get("token");
                    } else {
                        creditCard.localToken = mapResponse.get("token");
                        creditCard.crossToken = "";
                    }
                    creditCard._id = customerId;
                    creditCard.cardHolder = creditForm.cardHolder;
                    creditCard.localFleetId = creditForm.fleetId;
                    creditCard.cardType = mapResponse.get("cardType");
                    creditCard.cardMask = mapResponse.get("creditCard");
                    createTokenResponse.response = creditCard;
                } else {
                    logger.debug(creditForm.requestId + " - createTokenFACFailure: " + resultCreateToken + " - data: " + creditForm.toString());
                    try {
                        createTokenResponse = gson.fromJson(resultCreateToken, ObjResponse.class);
                    } catch (Exception ignore) {
                    }
                }
            }
        } catch (Exception ex) {
            logger.debug(creditForm.requestId + " - createTokenFACException: " + CommonUtils.getError(ex) + " - data: " + creditForm.toString());
        }

        return createTokenResponse;
    }

    private ObjResponse createTokenPayfort(CreditEnt creditForm, String customerId, Credit creditCard){
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            String tmpData = redisDao.get3DSData(creditForm.requestId, customerId, "3dstoken");
            JSONObject obj = gson.fromJson(tmpData, JSONObject.class);
            DecimalFormat df = new DecimalFormat("#");
            createTokenResponse.returnCode = df.parse(obj.get("returnCode").toString()).intValue();
            if (createTokenResponse.returnCode == 200) {// add token for fleet successful
                Map<String,String> mapResponse = gson.fromJson(obj.get("response").toString(), Map.class);
                if (creditForm.isCrossZone) {
                    creditCard.localToken = "";
                    creditCard.crossToken = mapResponse.get("token");
                } else {
                    creditCard.localToken = mapResponse.get("token");
                    creditCard.crossToken = "";
                }
                creditCard._id = customerId;
                creditCard.cardHolder = creditForm.cardHolder;
                creditCard.localFleetId = creditForm.fleetId;
                creditCard.cardType = mapResponse.get("cardType");
                creditCard.cardMask = mapResponse.get("creditCard");
                creditCard.userId = creditForm.userId;
                createTokenResponse.response = creditCard;
            }else {
                logger.debug(creditForm.requestId + " - createTokenPayfortFailure: " + gson.toJson(obj) + " - data: " + creditForm.toString());
                try {
                    createTokenResponse = gson.fromJson(tmpData, ObjResponse.class);
                } catch (Exception ignore){}

            }
        } catch (Exception ex) {
            logger.debug(creditForm.requestId + " - createTokenPayfortException: " + CommonUtils.getError(ex) + " - data: " + creditForm.toString());
        }

        return createTokenResponse;
    }

    private ObjResponse createTokenConekta(CreditEnt creditForm, String customerId, GatewayConekta gateway, Credit creditCard, String customerName, String customerEmail, String currencyISO){
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            ConektaUtil conektaUtil = new ConektaUtil(creditForm.requestId);

            String resultCreateToken = conektaUtil.createCreditToken(gateway.apiKey, gateway.secretKey, creditForm, customerName, customerEmail, currencyISO);

            ObjectMapper mapperResult = new ObjectMapper();
            Map<String, Object> mapResult = mapperResult.readValue(resultCreateToken, new TypeReference<HashMap<String, Object>>() {
            });
            createTokenResponse.returnCode = Integer.parseInt(mapResult.get("returnCode").toString());
            if (createTokenResponse.returnCode == 200) {// add token for fleet successful
                Map<String, String> mapResponse = (Map<String, String>) mapResult.get("response");
                if (creditForm.isCrossZone) {
                    creditCard.localToken = "";
                    creditCard.crossToken = mapResponse.get("token");
                } else {
                    creditCard.localToken = mapResponse.get("token");
                    creditCard.crossToken = "";
                }
                creditCard._id = customerId;
                creditCard.cardHolder = creditForm.cardHolder;
                creditCard.localFleetId = creditForm.fleetId;
                creditCard.cardType = mapResponse.get("cardType");
                creditCard.cardMask = mapResponse.get("creditCard");
                createTokenResponse.response = creditCard;
            }
            else {
                logger.debug(creditForm.requestId + " - createTokenConektaFailure: " + resultCreateToken + " - data: " + creditForm.toString());
                try {
                    createTokenResponse = gson.fromJson(resultCreateToken, ObjResponse.class);
                } catch (Exception ignore){}
            }
        } catch (Exception ex) {
            logger.debug(creditForm.requestId + " - createTokenConektaException: " + CommonUtils.getError(ex) + " - data: " + creditForm.toString());
        }

        return createTokenResponse;

    }

    private ObjResponse createTokenGatewayCXPay(CreditEnt creditForm, String customerId, String customerEmail, String customerPhone, GatewayCXPay gateway, Credit creditCard,String currencyISO) {
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.CXPAY, "");
            CXPayUtil gatewayPayment = new CXPayUtil(creditForm.requestId, gateway.apiKey, gatewayURL.url);
            String tokenId = redisDao.getTmp(creditForm.requestId, customerId+"transactionId");
            if (tokenId.isEmpty()) {
                tokenId = redisDao.getTmp(creditForm.requestId, customerId+"transactionId");
            } else {
                redisDao.removeTmp(creditForm.requestId, customerId+"transactionId");
            }
            String resultCreateToken = gatewayPayment.createCreditToken(tokenId);

            ObjectMapper mapperResult = new ObjectMapper();
            Map<String, Object> mapResult = mapperResult.readValue(resultCreateToken, new TypeReference<HashMap<String, Object>>() {
            });

            createTokenResponse.returnCode = Integer.parseInt(mapResult.get("returnCode").toString());
            if (createTokenResponse.returnCode == 200) {// add token for fleet successful
                //save credit to Store
                Map<String, String> mapResponse = (Map<String, String>) mapResult.get("response");
                if (creditForm.isCrossZone) {
                    creditCard.localToken = "";
                    creditCard.crossToken = mapResponse.get("token");
                } else {
                    creditCard.localToken = mapResponse.get("token");
                    creditCard.crossToken = "";
                }
                creditCard._id = customerId;
                creditCard.cardHolder = creditForm.cardHolder;
                creditCard.localFleetId = creditForm.fleetId;
                creditCard.cardType = mapResponse.get("cardType");
                creditCard.cardMask = mapResponse.get("creditCard");
                createTokenResponse.response = creditCard;

                // initial test transaction to check if card is valid
                logger.debug(creditForm.requestId + " - initial charge 1 " + currencyISO + " for token " + creditCard.localToken);
                String cvvCache = redisDao.getTmp(creditForm.requestId, creditForm.userId+"-cvv");
                logger.debug(creditForm.requestId + " - cvvCache: " + cvvCache);
                String payResult = gatewayPayment.createPaymentWithCreditToken(1, creditCard.localToken, cvvCache);
                if (CommonUtils.getReturnCode(payResult) == 200) {
                    String transId = CommonUtils.getValue(payResult, "transId");
                    gatewayPayment.voidTransaction(transId);
                } else {
                    ObjResponse objPay = gson.fromJson(payResult, ObjResponse.class);
                    createTokenResponse.returnCode = objPay.returnCode;
                    createTokenResponse.response = objPay.response;
                }
            } else {
                logger.debug(creditForm.requestId + " - createTokenCXPayFailure: " + resultCreateToken + " - data: " + creditForm.toString());
                try {
                    createTokenResponse = gson.fromJson(resultCreateToken, ObjResponse.class);
                } catch (Exception ignore){}
            }
        } catch (Exception ex) {
            logger.debug(creditForm.requestId + " - createTokenCXPayException: " + CommonUtils.getError(ex) + " - data: " + creditForm.toString());
        }
        return createTokenResponse;
    }

    private ObjResponse createTokenGatewaySenangpay(CreditEnt creditForm, String customerId, String customerName, String customerEmail, String customerPhone, GatewaySenangpay gateway, Credit creditCard,String currencyISO){
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.SENANGPAY, gateway.environment);
            SenangpayUtil senangpayUtil = new SenangpayUtil(creditForm.requestId, gateway.merchantAccount, gateway.wsUser, gateway.wsPassword, gateway.secretKey, gatewayURL.tokenURL, gatewayURL.url);
            //call Server to add Credit Card

            String resultCreateToken = senangpayUtil.createCreditToken(creditForm, customerName, customerEmail, customerPhone);

            ObjectMapper mapperResult = new ObjectMapper();
            Map<String, Object> mapResult = mapperResult.readValue(resultCreateToken, new TypeReference<HashMap<String, Object>>() {
            });

            createTokenResponse.returnCode = Integer.parseInt(mapResult.get("returnCode").toString());
            if (createTokenResponse.returnCode == 200) {// add token for fleet successful
                //save credit to Store
                Map<String, String> mapResponse = (Map<String, String>) mapResult.get("response");
                String userId = "";
                if(!this.isEmptyString(creditForm.userId))
                    userId = creditForm.userId ;
                /*int returnCode = paymentUtil.addCardToStore(mapResponse.get("token"), userId, creditForm);
                if(returnCode != 200){
                    createTokenResponse.returnCode = 437;
                    return createTokenResponse;
                }*/
                if (creditForm.isCrossZone) {
                    creditCard.localToken = "";
                    creditCard.crossToken = mapResponse.get("token");
                } else {
                    creditCard.localToken = mapResponse.get("token");
                    creditCard.crossToken = "";
                }
                creditCard._id = customerId;
                creditCard.cardHolder = creditForm.cardHolder;
                creditCard.localFleetId = creditForm.fleetId;
                creditCard.cardType = mapResponse.get("cardType");
                creditCard.cardMask = mapResponse.get("creditCard");
                createTokenResponse.response = creditCard;
            } else {
                logger.debug(creditForm.requestId + " - createTokenSenangpayFailure: " + resultCreateToken + " - data: " + creditForm.toString());
                try {
                    createTokenResponse = gson.fromJson(resultCreateToken, ObjResponse.class);
                } catch (Exception ignore){}
            }
        } catch (Exception ex) {
            logger.debug(creditForm.requestId + " - createTokenSenangpayException: " + CommonUtils.getError(ex) + " - data: " + creditForm.toString());
        }
        return createTokenResponse;
    }

    private ObjResponse createTokenGatewayFirstData(CreditEnt creditForm, String customerId, Credit creditCard, String data){
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            ObjResponse response = gson.fromJson(data, ObjResponse.class);
            createTokenResponse.returnCode = response.returnCode;
            if (createTokenResponse.returnCode == 200) {// add token for fleet successful
                //save credit to Store
                Map<String, String> mapResponse = (Map<String, String>) response.response;
                /*String userId = "";
                if(!this.isEmptyString(creditForm.userId))
                    userId = creditForm.userId ;
                int returnCode = paymentUtil.addCardToStore(mapResponse.get("token"), userId, creditForm);
                if(returnCode != 200){
                    createTokenResponse.returnCode = 437;
                    return createTokenResponse;
                }*/
                if (creditForm.isCrossZone) {
                    creditCard.localToken = "";
                    creditCard.crossToken = mapResponse.get("token");
                } else {
                    creditCard.localToken = mapResponse.get("token");
                    creditCard.crossToken = "";
                }
                creditCard._id = customerId;
                creditCard.cardHolder = creditForm.cardHolder;
                creditCard.localFleetId = creditForm.fleetId;
                creditCard.cardType = mapResponse.get("cardType");
                creditCard.cardMask = mapResponse.get("creditCard");
                createTokenResponse.response = creditCard;
            } else {
                logger.debug(creditForm.requestId + " - createTokenGatewayFirstDataFailure: " + data + " - data: " + creditForm.toString());
                try {
                    createTokenResponse = gson.fromJson(data, ObjResponse.class);
                } catch (Exception ignore){}
            }
        } catch (Exception ex) {
            logger.debug(creditForm.requestId + " - createTokenGatewayFirstDataException: " + CommonUtils.getError(ex) + " - data: " + creditForm.toString());
        }
        return createTokenResponse;
    }

    private ObjResponse createTokenEGHL(CreditEnt creditForm, String customerId, Credit creditCard, String currencyISO){
        ObjResponse createTokenResponse = new ObjResponse();
        createTokenResponse.returnCode = 437;
        try {
            String tmpData = redisDao.getTmp(creditForm.requestId, customerId+"token");
            logger.debug(creditForm.requestId + " - tmpData: " + tmpData);
            JSONObject obj = gson.fromJson(tmpData, JSONObject.class);
            DecimalFormat df = new DecimalFormat("#");
            createTokenResponse.returnCode = df.parse(obj.get("returnCode").toString()).intValue();
            if (createTokenResponse.returnCode == 200) {// add token successful
                logger.debug(creditForm.requestId + " - response:" + obj.get("response").toString());
                Map<String,String> mapResponse = (Map<String,String>)obj.get("response");
                creditCard.localToken = mapResponse.get("token");
                creditCard.crossToken = "";
                creditCard._id = customerId;
                creditCard.cardHolder = mapResponse.get("cardHolder");
                creditCard.localFleetId = creditForm.fleetId;
                creditCard.cardType = mapResponse.get("cardType");
                creditCard.cardMask = mapResponse.get("cardMask");
                creditCard.userId = creditForm.userId;
                createTokenResponse.response = creditCard;

                // do reversal to return the fund
                String paymentId = redisDao.getTmp(creditForm.requestId, customerId+"paymentId");
                GatewayEGHL gatewayFleet = mongoDao.getGatewayPayEGHLFleet(creditForm.fleetId);
                GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.EGHL, gatewayFleet.environment);
                EGHLUtil eghlUtil = new EGHLUtil(creditForm.requestId, gatewayFleet.serviceId, gatewayFleet.password, gatewayURL.url);
                eghlUtil.doReversal(paymentId, 1.00, currencyISO);
            }else {
                logger.debug(creditForm.requestId + " - createTokenEGHLFailure: " + gson.toJson(obj) + " - data: " + creditForm.toString());
                try {
                    createTokenResponse = gson.fromJson(tmpData, ObjResponse.class);
                } catch (Exception ignore){}
            }
            //clean cache
            redisDao.removeTmp(creditForm.requestId, customerId+"credit");
            redisDao.removeTmp(creditForm.requestId, customerId+"token");
        } catch (Exception ex) {
            logger.debug(creditForm.requestId + " - createTokenEGHLException: " + CommonUtils.getError(ex) + " - data: " + creditForm.toString());
        }

        return createTokenResponse;
    }

    private ObjResponse createTokenFlutterwave(CreditEnt creditForm, String customerId, String customerEmail, Credit creditCard) {
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            String tmpData = redisDao.get3DSData(creditForm.requestId, customerId, "3dstoken");
            logger.debug(creditForm.requestId + " - customerId: " + customerId);
            logger.debug(creditForm.requestId + " - tmpData: " + tmpData);
            JSONObject obj = gson.fromJson(tmpData, JSONObject.class);
            DecimalFormat df = new DecimalFormat("#");
            createTokenResponse.returnCode = df.parse(obj.get("returnCode").toString()).intValue();
            if (createTokenResponse.returnCode == 200) {// add token for fleet successful
                logger.debug(creditForm.requestId + " - response: " + obj.get("response").toString());
                Map<String,String> mapResponse = (Map<String,String>)obj.get("response");
                if (creditForm.isCrossZone) {
                    creditCard.localToken = "";
                    creditCard.crossToken = mapResponse.get("token") + KeysUtil.TEMPKEY + customerEmail;
                } else {
                    creditCard.localToken = mapResponse.get("token") + KeysUtil.TEMPKEY + customerEmail;
                    creditCard.crossToken = "";
                }
                creditCard._id = customerId;
                creditCard.cardHolder = mapResponse.get("cardHolder");
                creditCard.localFleetId = creditForm.fleetId;
                creditCard.cardType = mapResponse.get("cardType");
                creditCard.cardMask = mapResponse.get("creditCard");
                creditCard.userId = creditForm.userId;
                createTokenResponse.response = creditCard;
            } else {
                logger.debug(creditForm.requestId + " - " +  "createTokenFlutterwaveFailure : " + gson.toJson(obj) + " - data: " + creditForm.toString());
                try {
                    createTokenResponse = gson.fromJson(tmpData, ObjResponse.class);
                } catch (Exception ignore){}
            }
        } catch (Exception ex) {
            logger.debug(creditForm.requestId + " - " +  "createTokenFlutterwaveException : " + CommonUtils.getError(ex) + " - data: " + creditForm.toString());
        }

        return createTokenResponse;
    }

    private ObjResponse createTokenFromCache(CreditEnt creditForm, String customerId, Credit creditCard){
        ObjResponse createTokenResponse = new ObjResponse();
        createTokenResponse.returnCode = 437;
        try {
            String tmpData = redisDao.getTmp(creditForm.requestId, customerId+"token");
            logger.debug(creditForm.requestId + " - tmpData: " + tmpData);
            createTokenResponse.returnCode = paymentUtil.getReturnCode(tmpData);
            if (createTokenResponse.returnCode == 200) {// add token successful
                creditCard._id = generateCustomerId();
                creditCard.localToken = CommonUtils.getValue(tmpData, "token");
                creditCard.crossToken = "";
                creditCard.cardHolder = CommonUtils.getValue(tmpData, "cardHolder");
                creditCard.localFleetId = creditForm.fleetId;
                creditCard.cardType = CommonUtils.getValue(tmpData, "cardType");
                creditCard.cardMask = CommonUtils.getValue(tmpData, "cardMask");
                creditCard.userId = creditForm.userId;
                createTokenResponse.response = creditCard;
            } else {
                logger.debug(creditForm.requestId + " - createTokenFromCache: " + tmpData + " - data: " + creditForm.toString());
                try {
                    createTokenResponse = gson.fromJson(tmpData, ObjResponse.class);
                } catch (Exception ignore){}
            }
            //clean cache
            redisDao.removeTmp(creditForm.requestId, customerId+"credit");
            redisDao.removeTmp(creditForm.requestId, customerId+"token");
        } catch (Exception ex) {
            logger.debug(creditForm.requestId + " - createTokenFromCache exception: " + CommonUtils.getError(ex) + " - data: " + creditForm.toString());
        }

        return createTokenResponse;
    }

    private boolean checkMatchPhoneZip(CreditEnt creditForm) {
        boolean matchPhoneZip = false;
        String phone = creditForm.phone != null ? creditForm.phone.trim() : "";
        String postalCode = creditForm.postalCode != null ? creditForm.postalCode.trim() : "";
        String country = creditForm.country != null ? creditForm.country.trim() : "";
        if (!country.isEmpty() && !phone.isEmpty() && phone.length() > 5 && !postalCode.isEmpty() && postalCode.length() > 1) {
            switch (country.toUpperCase()) {
                case "CANADA" :
                case "CA" : {
                    List<PhoneZip> listPhoneZip = mongoDao.getPhoneZipWithoutState("CANADA");
                    for (PhoneZip phoneZip : listPhoneZip) {
                        String phoneToCheck = phone.substring(2, 2 + phoneZip.phoneLength); // 2: phone code start after country code "+1"
                        String zipToCheck = postalCode.substring(0, phoneZip.stateLength);
                        if (phoneZip.zipStart.contains(zipToCheck)                  // match zip start character
                                && phoneZip.phoneCodes.contains(phoneToCheck)) {    // match phone area code
                            matchPhoneZip = true;
                            break;
                        }
                    }
                }
                break;
                case "UNITED STATES" :
                case "US" : {
                    List<PhoneZip> listPhoneZip = mongoDao.getPhoneZipWithoutState("UNITED STATES");
                    for (PhoneZip phoneZip : listPhoneZip) {
                        String phoneToCheck = phone.substring(2, 2 + phoneZip.phoneLength); // 2: phone code start after country code "+1"
                        String zipToCheck = postalCode.substring(0, phoneZip.stateLength);
                        if (phoneZip.zipStart.contains(zipToCheck)                  // match zip start character
                                && phoneZip.phoneCodes.contains(phoneToCheck)) {    // match phone area code
                            matchPhoneZip = true;
                            break;
                        }
                    }
                }
                break;
            }

        }

        return matchPhoneZip;
    }

    private String completed3DSTransaction(CreditEnt creditEnt, String gateway, String type, String currencyISO) {
        ObjResponse response = new ObjResponse();
        String data3ds = creditEnt.data3ds;
        switch (gateway) {
            case KeysUtil.PAYFORT: {
                try {
                    String fort_id = "";
                    String merchant_reference = "";
                    String responseCode = "";
                    String responseMessage = "";
                    String currency = "";
                    if (data3ds.contains(KeysUtil.CALLBACK_URL)) {
                        data3ds = URLDecoder.decode(data3ds, "UTF-8").replace(" ", "_"); // replace space
                        List<NameValuePair> params = URLEncodedUtils.parse(new URI(data3ds), "UTF-8");

                        for (NameValuePair param : params) {
                            if (param.getName().equals("fort_id")) fort_id = param.getValue();
                            if (param.getName().equals("merchant_reference")) merchant_reference = param.getValue();
                            if (param.getName().equals("response_code")) responseCode = param.getValue();
                            if (param.getName().equals("response_message")) responseMessage = param.getValue().replace("_", " "); // put spaces which were replaces above
                            if (param.getName().equals("currency")) currency = param.getValue();
                        }
                    } else {
                        JSONObject json = (JSONObject)new org.json.simple.parser.JSONParser().parse(data3ds);
                        fort_id = json.get("fort_id") != null ? json.get("fort_id").toString() : "";
                        merchant_reference = json.get("merchant_reference") != null ? json.get("merchant_reference").toString() : "";
                        responseCode = json.get("response_code") != null ? json.get("response_code").toString() : "";
                        responseMessage = json.get("response_message") != null ? json.get("response_message").toString() : "";
                        currency = json.get("currency") != null ? json.get("currency").toString() : "";
                    }

                    if (responseCode.length() > 3)
                        responseCode = responseCode.substring(2);
                    GatewayPayfort gatewayFleet = mongoDao.getGatewayPayfortFleet(creditEnt.fleetId);
                    GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.PAYFORT, gatewayFleet.environment);
                    PayfortUtil payfortUtil = new PayfortUtil(creditEnt.requestId, gatewayFleet.merchantIdentifier, gatewayFleet.accessCode, gatewayFleet.shaType, gatewayFleet.requestPhrase, gatewayFleet.responsePhrase, gatewayURL.url, gatewayURL.payURL);
                    int returnCode = payfortUtil.getReturnCodeFromTransactionCode(responseCode);
                    if (returnCode == 200 && type != null) {
                        // VOID auth transaction


                        payfortUtil.voidTransaction("VOID_AUTHORIZATION", fort_id, 1.0, currency);

                        // remove data3ds before continue
                        creditEnt.data3ds = "";

                        return createToken(creditEnt, type, merchant_reference, true);
                    } else {
                        Map<String,String> objResponse = new HashMap<>();
                        objResponse.put("message", responseMessage);
                        response.returnCode = returnCode;
                        response.response = objResponse;
                    }
                } catch(Exception ex) {
                    ex.printStackTrace();
                    response.returnCode = 437;
                }
            }
            break;
            case KeysUtil.YEEPAY: {
                try {
                    String reqid = creditEnt.userId;
                    GatewayYeepay gatewayFleet = mongoDao.getGatewayYeepayFleet(creditEnt.fleetId);
                    GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.YEEPAY, gatewayFleet.environment);
                    YeepayUtil yeepayUtil = new YeepayUtil();
                    String orderId = redisDao.get3DSData(creditEnt.requestId, reqid, "3dstoken");
                    // remove prefix "a" from orderId
                    // added prefix "a" to avoid Redis do round order number
                    if (orderId != null && !orderId.isEmpty())
                        orderId = orderId.substring(1);
                    String customerPhone = creditEnt.phone != null ? creditEnt.phone : "";
                    String inputPhone = creditEnt.creditPhone != null ? creditEnt.creditPhone : "";
                    if (!inputPhone.isEmpty())
                        customerPhone = inputPhone;
                    ObjResponse verifyResult = yeepayUtil.verifyTransaction("phone " + customerPhone, creditEnt.requestId, gatewayFleet.merchantId, gatewayFleet.key, gatewayURL.url, creditEnt.data3ds, orderId);
                    response.returnCode = verifyResult.returnCode;
                    if (response.returnCode == 200) {// add token for fleet successful
                        // remove data3ds before continue
                        creditEnt.data3ds = "";
                        try {
                            // save r2_TrxId to use on the refund
                            Map<String,String> mapResponse = (Map<String,String>) verifyResult.response;
                            String r2_TrxId = mapResponse.get("r2_TrxId") != null ? mapResponse.get("r2_TrxId") : "";
                            logger.debug(creditEnt.requestId + " - r2_TrxId: " + r2_TrxId);
                            redisDao.add3DSData(creditEnt.requestId, creditEnt.userId, "a" + r2_TrxId, "3dsYeepayOrderId");
                        } catch(Exception ex) {
                            logger.debug(creditEnt.requestId + " - get r2_TrxId from response is error :" + CommonUtils.getError(ex));
                        }
                        return createToken(creditEnt, type, "", true);
                    } else if (response.returnCode == 463) {
                        // Verification code is invalid
                        // re-stored orderId to retry verify with SMS again
                        redisDao.add3DSData(creditEnt.requestId, creditEnt.userId, "a" + orderId, "3dstoken");
                    }else {
                        logger.debug(creditEnt.requestId + " - createTokenYeepayFailure: " + verifyResult + " - data: " + creditEnt.toString());
                        response = verifyResult;
                    }
                } catch(Exception ex) {
                    ex.printStackTrace();
                }
            }
            break;
            case KeysUtil.FLUTTERWAVE: {
                try {
                    String chargeId = "";
                    String resultCreateToken = "{}";
                    GatewayFlutterwave gatewayFleet = mongoDao.getGatewayFlutterwaveFleet(creditEnt.fleetId);
                    Boolean isSandbox = gatewayFleet.environment.equalsIgnoreCase(KeysUtil.SANDBOX);
                    FlutterWaveUtil flutterWaveUtil = new FlutterWaveUtil(creditEnt.requestId, gatewayFleet.publicKey, gatewayFleet.secretKey);
                    if (data3ds.contains(ServerConfig.dispatch_server)) {  // 3DS mode
                        try {
                            data3ds = URLDecoder.decode(data3ds, "UTF-8");
                            int index = data3ds.indexOf("=");
                            data3ds = data3ds.substring(index + 1); // remove symbol "="
                            JSONObject json = (JSONObject)new org.json.simple.parser.JSONParser().parse(data3ds);
                            chargeId = json.get("txRef").toString();
                            String cardHolder = creditEnt.cardHolder != null ? creditEnt.cardHolder : "";
                            resultCreateToken = flutterWaveUtil.createCreditToken(chargeId, isSandbox, cardHolder, creditEnt.phone);
                            if (paymentUtil.getReturnCode(resultCreateToken) == 469) {
                                redisDao.add3DSData(creditEnt.requestId, creditEnt.phone, chargeId, "flwRefFlutter");
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    } else {    // PIN mode
                        String inputValue = data3ds.length() > 3 ? data3ds.substring(3) : "";
                        // check Redis to process with PIN or OTP
                        String flwRef = redisDao.get3DSData(creditEnt.requestId, creditEnt.phone, "flwRefFlutter");
                        if (flwRef.isEmpty()) {
                            // process auth payment with PIN then return correct code
                            resultCreateToken = flutterWaveUtil.initiateCreditFormWithPIN(inputValue, creditEnt.phone, isSandbox);
                            int returnCode = paymentUtil.getReturnCode(resultCreateToken);
                            if (returnCode == 469) {
                                // remove previous transaction if exist
                                redisDao.remove3DSData(creditEnt.requestId, creditEnt.phone, "flwRefFlutter");

                                // store successful initiated request to Redis
                                JSONObject json = (JSONObject)new org.json.simple.parser.JSONParser().parse(resultCreateToken);
                                flwRef = json.get("response").toString();
                                redisDao.add3DSData(creditEnt.requestId, creditEnt.phone, flwRef, "flwRefFlutter");

                                // return code for app to show view enter OTP
                                response.returnCode = 469;
                                return response.toString();
                            }
                        } else {
                            chargeId = flwRef;
                            // check verification by PIN or OTP
                            if (data3ds.contains("PIN")) {
                                // do PIN verification again for new transaction
                                resultCreateToken = flutterWaveUtil.initiateCreditFormWithPIN(inputValue, creditEnt.phone, isSandbox);
                                int returnCode = paymentUtil.getReturnCode(resultCreateToken);
                                if (returnCode == 469) {
                                    // remove previous transaction if exist
                                    redisDao.remove3DSData(creditEnt.requestId, creditEnt.phone, "flwRefFlutter");

                                    // store successful initiated request to Redis
                                    JSONObject json = (JSONObject)new org.json.simple.parser.JSONParser().parse(resultCreateToken);
                                    flwRef = json.get("response").toString();
                                    redisDao.add3DSData(creditEnt.requestId, creditEnt.phone, flwRef, "flwRefFlutter");

                                    // return code for app to show view enter OTP
                                    response.returnCode = 469;
                                    return response.toString();
                                }
                            } else if (data3ds.contains("OTP")) {
                                String otpSubmit = flutterWaveUtil.validateTransactionWithOTP(flwRef, inputValue, isSandbox);
                                JSONObject json = (JSONObject)new org.json.simple.parser.JSONParser().parse(otpSubmit);
                                if (json.get("returnCode") != null && Integer.parseInt(json.get("returnCode").toString()) == 200) {
                                    chargeId = json.get("response") != null ? json.get("response").toString() : "";
                                    String cardHolder = creditEnt.cardHolder != null ? creditEnt.cardHolder : "";
                                    resultCreateToken = flutterWaveUtil.createCreditToken(chargeId, isSandbox, cardHolder, creditEnt.phone);
                                }
                            }

                        }
                    }

                    logger.debug("resultCreateToken: " + resultCreateToken);
                    response.returnCode = paymentUtil.getReturnCode(resultCreateToken);
                    if (response.returnCode == 200) {// add token for fleet successful
                        // temporary save result to Redis and retrieve when completed create token
                        if (chargeId.isEmpty())
                            chargeId = generateCustomerId();
                        redisDao.add3DSData(creditEnt.requestId, chargeId, resultCreateToken, "3dstoken");

                        // remove data3ds before continue
                        creditEnt.data3ds = "";

                        return createToken(creditEnt, type, chargeId, true);
                    }else {
                        logger.debug(creditEnt.requestId + " - createTokenFlutterwaveFailure: " + resultCreateToken + " - data: " + creditEnt.toString());
                        try {
                            response = gson.fromJson(resultCreateToken, ObjResponse.class);
                        } catch (Exception ignore) {}
                    }
                } catch(Exception ex) {
                    ex.printStackTrace();
                    response.returnCode = 437;
                }
            }
            break;
            case KeysUtil.ADYEN: {
                try {
                    String authResult = "";
                    String merchantReference = "";
                    String paymentMethod = "";
                    String pspReference = "";
                    if (data3ds.contains(KeysUtil.CALLBACK_URL)) {
                        data3ds = URLDecoder.decode(data3ds, "UTF-8");
                        List<NameValuePair> params = URLEncodedUtils.parse(new URI(data3ds), "UTF-8");

                        for (NameValuePair param : params) {
                            if (param.getName().equals("authResult")) authResult = param.getValue();
                            if (param.getName().equals("merchantReference")) merchantReference = param.getValue();
                            if (param.getName().equals("paymentMethod")) paymentMethod = param.getValue();
                            if (param.getName().equals("pspReference")) pspReference = param.getValue();
                        }
                    }
                    if (authResult.equalsIgnoreCase("CANCELLED")) {
                        response.returnCode = 466;
                        return response.toString();
                    }
                    if (authResult.equalsIgnoreCase("REFUSED")) {
                        response.returnCode = 436;
                        return response.toString();
                    }

                    GatewayAdyen gatewayFleet = mongoDao.getGatewayAdyenFleet(creditEnt.fleetId);
                    if (gatewayFleet.merchantAccount.equals("") || gatewayFleet.hmacKey.equals("") || gatewayFleet.skinCode.equals("")) {
                        response.returnCode = 415;
                        return response.toString();
                    }
                    Boolean isSandbox = gatewayFleet.environment.equalsIgnoreCase(KeysUtil.SANDBOX);
                    AdyenUtil adyenUtil = new AdyenUtil(gatewayFleet.merchantAccount, gatewayFleet.username, gatewayFleet.password,
                            gatewayFleet.hmacKey, gatewayFleet.skinCode, gatewayFleet.cseKey);

                    String resultCreateToken = adyenUtil.createCreditTokenHPP(creditEnt.requestId, isSandbox, currencyISO, pspReference, merchantReference, creditEnt.phone);

                    ObjectMapper mapperResult = new ObjectMapper();
                    Map<String, Object> mapResult = mapperResult.readValue(resultCreateToken, new TypeReference<HashMap<String, Object>>() {
                    });

                    response.returnCode = Integer.parseInt(mapResult.get("returnCode").toString());
                    if (response.returnCode == 200) {// add token for fleet successful
                        // temporary save result to Redis and retrieve when completed create token
                        redisDao.add3DSData(creditEnt.requestId, merchantReference, resultCreateToken, "3dstoken");

                        // remove data3ds before continue
                        creditEnt.data3ds = "";

                        return createToken(creditEnt, type, merchantReference, true);
                    }else {
                        logger.debug(creditEnt.requestId + " - createTokenExpresspayFailure: " + resultCreateToken + " - data: " + creditEnt.toString());
                        try {
                            response = gson.fromJson(resultCreateToken, ObjResponse.class);
                        } catch (Exception ignore) {}
                    }
                } catch(Exception ex) {
                    ex.printStackTrace();
                    response.returnCode = 437;
                }
            }
            break;
            case KeysUtil.FAC : {
                try {
                    String orderId = "";
                    if (data3ds.contains(ServerConfig.dispatch_server)) {
                        data3ds = URLDecoder.decode(data3ds, "UTF-8");
                        List<NameValuePair> params = URLEncodedUtils.parse(new URI(data3ds), "UTF-8");

                        for (NameValuePair param : params) {
                            if (param.getName().equals("OrderID")) orderId = param.getValue();
                        }
                    }

                    String resultCreateToken = redisDao.getTmp(creditEnt.requestId, orderId);
                    if (resultCreateToken == null || resultCreateToken.isEmpty()) {
                        response.returnCode = 437;
                        return response.toString();
                    }
                    ObjectMapper mapperResult = new ObjectMapper();
                    Map<String, Object> mapResult = mapperResult.readValue(resultCreateToken, new TypeReference<HashMap<String, Object>>() {
                    });

                    response.returnCode = Integer.parseInt(mapResult.get("returnCode").toString());
                    if (response.returnCode == 200) {// add token for fleet successful
                        // temporary save result to Redis and retrieve when completed create token
                        redisDao.add3DSData(creditEnt.requestId, orderId, resultCreateToken, "3dstoken");

                        // void auth request
                        FirstAtlanticCommerceUtil facUtil = new FirstAtlanticCommerceUtil(creditEnt.requestId);
                        GatewayFirstAtlanticCommerce gatewayFleet = mongoDao.getGatewayFACFleet(creditEnt.fleetId);
                        Boolean isSandbox = gatewayFleet.environment.equalsIgnoreCase(KeysUtil.SANDBOX);
                        facUtil.voidTransaction(orderId, gatewayFleet.merchantId, gatewayFleet.acquirerId, gatewayFleet.password, 1.0, isSandbox);

                        // remove data3ds before continue
                        creditEnt.data3ds = "";

                        return createToken(creditEnt, type, orderId, true);
                    }else {
                        logger.debug(creditEnt.requestId + " - createTokenFACFailure: " + resultCreateToken + " - data: " + creditEnt.toString());
                        try {
                            response = gson.fromJson(resultCreateToken, ObjResponse.class);
                        } catch (Exception ignore) {}
                    }
                } catch(Exception ex) {
                    ex.printStackTrace();
                    response.returnCode = 437;
                }
            }
            break;
            case KeysUtil.CONEKTA: {
                try {
                    String token = "";
                    if (data3ds.contains(KeysUtil.CALLBACK_URL)) {
                        try {
                            List<NameValuePair> params = URLEncodedUtils.parse(new URI(data3ds), "UTF-8");

                            for (NameValuePair param : params) {
                                if (param.getName().equals("conektaTokenId")) token = param.getValue();
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                    if (token.isEmpty()) {
                        response.returnCode = 437;
                        return response.toString();
                    }
                    // valid username and email
                    String userId = creditEnt.userId != null ? creditEnt.userId : "";
                    Map<String,String> extraData = GatewayUtil.getExtraData(type, userId);
                    String customerName = creditEnt.name != null ? creditEnt.name : "";
                    if (customerName.trim().isEmpty()) {
                        customerName = extraData.get("firstName") + " " + extraData.get("lastName");
                        if (customerName.trim().isEmpty()) {
                            response.returnCode = 474;
                            return response.toString();
                        }
                    }
                    String customerEmail = creditEnt.email != null ? creditEnt.email : "";
                    if (customerEmail.trim().isEmpty()) {
                        customerEmail = extraData.get("email");
                        if (customerEmail.trim().isEmpty()) {
                            response.returnCode = 470;
                            return response.toString();
                        }
                    }
                    GatewayConekta gatewayFleet = mongoDao.getGatewayConektaFleet(creditEnt.fleetId);
                    ConektaUtil conektaUtil = new ConektaUtil(creditEnt.requestId);
                    String resultCreateToken = conektaUtil.createCreditToken(gatewayFleet.secretKey, token, customerName, customerEmail);

                    response.returnCode = paymentUtil.getReturnCode(resultCreateToken);
                    if (response.returnCode == 200) {// add token for fleet successful
                        // temporary save result to Redis and retrieve when completed create token
                        redisDao.add3DSData(creditEnt.requestId, token, resultCreateToken, "3dstoken");

                        // remove data3ds before continue
                        creditEnt.data3ds = "";

                        return createToken(creditEnt, type, token, true);
                    }else {
                        logger.debug(creditEnt.requestId + " - createTokenConektaFailure: " + resultCreateToken + " - data: " + creditEnt.toString());
                        try {
                            response = gson.fromJson(resultCreateToken, ObjResponse.class);
                        } catch (Exception ignore) {}
                    }
                } catch(Exception ex) {
                    ex.printStackTrace();
                    response.returnCode = 437;
                }
            }
            break;
            case KeysUtil.SENANGPAY : {
                try {
                    String status_id = "";
                    String order_id = "";
                    String token = "";
                    String cc_num = "";
                    String cc_type = "";
                    String msg = "";
                    String hash = "";
                    if (data3ds.contains(KeysUtil.CALLBACK_URL)) {
                        data3ds = URLDecoder.decode(data3ds, "UTF-8");
                        List<NameValuePair> params = URLEncodedUtils.parse(new URI(data3ds), "UTF-8");

                        for (NameValuePair param : params) {
                            if (param.getName().equals("status_id")) status_id = param.getValue();
                            if (param.getName().equals("order_id")) order_id = param.getValue();
                            if (param.getName().equals("token")) token = param.getValue();
                            if (param.getName().equals("cc_num")) cc_num = param.getValue();
                            if (param.getName().equals("cc_type")) cc_type = param.getValue();
                            if (param.getName().equals("msg")) msg = param.getValue();
                            if (param.getName().equals("hash")) hash = param.getValue();
                        }
                    }

                    if (!status_id.equals("1")) { // failed
                        Map<String,String> mapResponse = new HashMap<>();
                        mapResponse.put("status_id", status_id);
                        mapResponse.put("order_id", order_id);
                        mapResponse.put("token", token);
                        mapResponse.put("cc_num", cc_num);
                        mapResponse.put("cc_type", cc_type);
                        mapResponse.put("message", msg);
                        mapResponse.put("hash", hash);
                        response.response = mapResponse;
                        response.returnCode = 437;
                    } else {
                        String cardType = "Visa";
                        if (cc_type.equals("mc")) {
                            cardType = "MasterCard";
                        }
                        Map<String,String> mapResponse = new HashMap<>();
                        mapResponse.put("message", "success");
                        mapResponse.put("creditCard", "XXXXXXXXXXXX" + cc_num);
                        mapResponse.put("qrCode", "");
                        mapResponse.put("token", token);
                        mapResponse.put("cardType", cardType);

                        response.response = mapResponse;
                        response.returnCode = 200;
                    }

                    String resultCreateToken = response.toString();
                    if (response.returnCode == 200) {
                        // temporary save result to Redis and retrieve when completed create token
                        redisDao.add3DSData(creditEnt.requestId, order_id, resultCreateToken, "3dstoken");

                        // remove data3ds before continue
                        creditEnt.data3ds = "";

                        return createToken(creditEnt, type, order_id, true);
                    } else {
                        logger.debug(creditEnt.requestId + " - createTokenSenangpayFailure: " + resultCreateToken + " - data: " + creditEnt.toString());
                        try {
                            response = gson.fromJson(resultCreateToken, ObjResponse.class);
                        } catch (Exception ignore) {}
                    }
                } catch(Exception ex) {
                    ex.printStackTrace();
                    response.returnCode = 437;
                }
            }
            break;
            case KeysUtil.FIRSTDATA : {
                try {
                    Thread.sleep(3000);
                } catch (Exception ex) {

                }
                try {
                    String token = "";
                    if (data3ds.contains(ServerConfig.dispatch_server)) {
                        data3ds = URLDecoder.decode(data3ds, "UTF-8");
                        List<NameValuePair> params = URLEncodedUtils.parse(new URI(data3ds), "UTF-8");

                        for (NameValuePair param : params) {
                            if (param.getName().equals("token")) token = param.getValue();
                        }
                    }

                    String status = redisDao.getTmp(creditEnt.requestId, token);
                    if (status.equals("true")) {
                        // generate response object
                        ObjResponse createTokenResponse = new ObjResponse();
                        Map<String,String> mapResponse = new HashMap<>();
                        mapResponse.put("message", "");
                        mapResponse.put("creditCard", "XXXXXXXXXXXX" + creditEnt.cardNumber.substring(creditEnt.cardNumber.length() - 4));
                        mapResponse.put("qrCode", "");
                        mapResponse.put("token", token);
                        mapResponse.put("cardType", com.pg.util.ValidCreditCard.getCardType(creditEnt.cardNumber));
                        createTokenResponse.returnCode = 200;
                        createTokenResponse.response = mapResponse;
                        // temporary save result to Redis and retrieve when completed create token
                        redisDao.add3DSData(creditEnt.requestId, token, createTokenResponse.toString(), "3dstoken");

                        // do void later

                        // remove data3ds before continue
                        creditEnt.data3ds = "";

                        return createToken(creditEnt, type, token, true);
                    } else {
                        response.returnCode = 437;
                        Map<String,String> mapResult = new HashMap<>();
                        mapResult.put("message",status );
                        response.response = mapResult;
                        return response.toString();
                    }

                } catch(Exception ex) {
                    ex.printStackTrace();
                    response.returnCode = 437;
                }
            }
            break;
            case KeysUtil.EGHL : {
                try {
                    Thread.sleep(3000);
                } catch (Exception ex) {

                }
                try {
                    String storeId = "";
                    if (data3ds.contains(ServerConfig.dispatch_server) || data3ds.contains("http://113.160.232.234:1337")) {
                        data3ds = URLDecoder.decode(data3ds, "UTF-8");
                        List<NameValuePair> params = URLEncodedUtils.parse(new URI(data3ds), "UTF-8");

                        for (NameValuePair param : params) {
                            if (param.getName().equals("OrderNumber")) storeId = param.getValue();
                        }
                    }
                    // remove data3ds before continue
                    creditEnt.data3ds = "";
                    return createToken(creditEnt, type, storeId, true);

                } catch(Exception ex) {
                    ex.printStackTrace();
                    response.returnCode = 437;
                }
            }
            break;
            default: {
                response.returnCode = 415;
            }
        }
        return response.toString();
    }

    public String paymentTSYSNotificationURL(PaymentNotificationEnt paymentNotificationEnt) {
        ObjResponse response = new ObjResponse();
        try {
            String sessionId = paymentNotificationEnt.sessionId != null ? paymentNotificationEnt.sessionId : "";
            String cacheData = redisDao.getTmp(paymentNotificationEnt.requestId, KeysUtil.TSYS+sessionId+"credit");
            boolean success = false;
            String errorMessage = "";
            String userId = "";
            String fleetId = "";
            if (!cacheData.isEmpty()) {
                CreditEnt creditEnt = gson.fromJson(cacheData, CreditEnt.class);
                userId = creditEnt.userId != null ? creditEnt.userId : "";
                // check Account or Corporate
                String type = "user";
                Account account = mongoDao.getAccount(userId);
                if (account == null) {
                    Corporate corporate = mongoDao.getCorporate(userId);
                    if (corporate != null) {
                        type = "corporate";
                    }
                }
                fleetId = creditEnt.fleetId != null ? creditEnt.fleetId : "";
                String customerNumber = redisDao.getTmp(paymentNotificationEnt.requestId, KeysUtil.TSYS+sessionId+"customerNumber");
                // remove cache
                logger.debug(paymentNotificationEnt.requestId + " - remove cache data for sessionId: " + sessionId);
                redisDao.removeTmp(paymentNotificationEnt.requestId, KeysUtil.TSYS+sessionId+"credit");
                redisDao.removeTmp(paymentNotificationEnt.requestId, KeysUtil.TSYS+sessionId+"customerNumber");

                String token = paymentNotificationEnt.token != null ? paymentNotificationEnt.token : "";
                if (!token.isEmpty()) {
                    // submit to TSYS to get card info
                    GatewayTSYS gatewayTSYS = mongoDao.getGatewayTSYS(fleetId);
                    GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.TSYS, gatewayTSYS.environment);
                    TSYSUtil tsysUtil = new TSYSUtil(paymentNotificationEnt.requestId, gatewayTSYS.apiKey, gatewayTSYS.companyNumber, gatewayTSYS.merchantNumber, gatewayTSYS.merchantTerminalNumber,
                            gatewayURL.url);
                    String result = tsysUtil.createCreditToken(customerNumber, token);
                    if (paymentUtil.getReturnCode(result) == 200) {
                        String last4 = CommonUtils.getValue(result, "last4");
                        String cardType = CommonUtils.getValue(result, "cardType");
                        String cardHolder = "";
                        Map<String, String> mapResponse = new HashMap<>();
                        mapResponse.put("message", "success");
                        mapResponse.put("cardMask", "XXXXXXXXXXXX" + last4);
                        mapResponse.put("qrCode", "");
                        mapResponse.put("token", token + KeysUtil.TEMPKEY + customerNumber);
                        mapResponse.put("cardType", cardType);
                        mapResponse.put("cardHolder", cardHolder);
                        ObjResponse objResponse = new ObjResponse();
                        objResponse.response = mapResponse;
                        objResponse.returnCode = 200;

                        // temporary save result to Redis and retrieve when completed create token
                        redisDao.addTmpData(paymentNotificationEnt.requestId, sessionId+"token", objResponse.toString());

                        // continue add card progress
                        String resultCreateToken = createToken(creditEnt, type, sessionId, true);
                        ObjResponse resultCreateTokenObject = gson.fromJson(resultCreateToken, ObjResponse.class);
                        if (resultCreateTokenObject.returnCode == 200) {
                            success = true;
                            Map<String, Object> data = new HashMap<String, Object>();
                            data.put("returnCode", 200);
                            data.put("requestId", paymentNotificationEnt.requestId);
                            data.put("userId", creditEnt.userId);
                            data.put("fleetId",creditEnt.fleetId);
                            data.put("credit", resultCreateTokenObject.response);
                            data.put("errorMessage", "");

                            String from = creditEnt.from != null ? creditEnt.from : "";
                            // send result to Jupiter
                            if (type.equals("user") && from.isEmpty()) {
                                org.json.JSONObject apiResponse = paymentUtil.sendPost(paymentNotificationEnt.requestId, gson.toJson(data),
                                        PaymentUtil.add_card_senangpay, ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
                                // show log
                                logger.debug(paymentNotificationEnt.requestId + " - apiURL: " + PaymentUtil.add_card_senangpay);
                                logger.debug(paymentNotificationEnt.requestId + " - data: " + gson.toJson(data));
                                logger.debug(paymentNotificationEnt.requestId + " - apiResponse: " + apiResponse);
                            }

                            // check if request from WB then send notification to WB
                            if (from.equals("wb")) {
                                JSONObject objData = new JSONObject();
                                objData.put("returnCode", 200);
                                objData.put("fleetId", fleetId);
                                objData.put("userId", userId);
                                objData.put("credit", resultCreateTokenObject.response);
                                paymentUtil.sendRegisterCardResultToWB(objData, userId, paymentNotificationEnt.requestId);
                            }
                        } else {
                            errorMessage = CommonUtils.getValue(resultCreateToken, "message");
                        }
                    } else {
                        errorMessage = CommonUtils.getValue(result, "message");
                    }
                }
            } else {
                logger.debug(paymentNotificationEnt.requestId + " - cache data for sessionId " + sessionId + " has been removed!!!");
            }

            if (!success) {
                return notifyRegistrationFailed(paymentNotificationEnt.requestId, 525, userId, fleetId, errorMessage);
            } else {
                // return to request
                response.returnCode = 200;
                return response.toString();
            }
        } catch (Exception ex) {
            logger.debug(paymentNotificationEnt.requestId + " - exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response.toString();
        }
    }

    public String paymentPayMayaNotificationURL(PaymentNotificationEnt paymentNotificationEnt) {
        try {
            String gwOrderid = paymentNotificationEnt.gwOrderid != null ? paymentNotificationEnt.gwOrderid : "";
            String urlAction = paymentNotificationEnt.urlAction != null ? paymentNotificationEnt.urlAction : "";
            String cachedData = redisDao.getTmp(paymentNotificationEnt.requestId, gwOrderid + "credit");
            if (cachedData.isEmpty()) {
                logger.debug(paymentNotificationEnt.requestId + " - received payment notification");
                ObjResponse response = new ObjResponse();
                response.returnCode = 200;
                return response.toString();
            }
            CreditEnt creditEnt = gson.fromJson(cachedData, CreditEnt.class);
            String errorMessage = "";
            String userId = creditEnt.userId != null ? creditEnt.userId : "";
            String corporateId = creditEnt.corporateId != null ? creditEnt.corporateId : "";
            String fleetId = creditEnt.fleetId != null ? creditEnt.fleetId : "";
            if (urlAction.equals("redirect")) {
                String result;
                if (!userId.isEmpty()) {
                    Account account = mongoDao.getAccount(userId);
                    String phone = account.phone;
                    String firstName = account.firstName;
                    String lastName = account.lastName;
                    String email = account.email;
                    GatewayPayMaya gatewayFleet = mongoDao.getGatewayPayMaya(fleetId);
                    GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.PAYMAYA, gatewayFleet.environment);
                    PayMayaUtil payMayaUtil = new PayMayaUtil(paymentNotificationEnt.requestId, gatewayFleet.publicKey, gatewayFleet.secretKey, gatewayURL.url);
                    String paymentTokenId = paymentNotificationEnt.token;
                    result = payMayaUtil.createCustomer(paymentTokenId, gwOrderid, firstName, lastName, phone, email);
                } else if (!corporateId.isEmpty()) {
                    Corporate corporate = mongoDao.getCorporate(corporateId);
                    String phone = "";
                    String firstName = "";
                    String lastName = "";
                    String email = "";
                    if (corporate.adminAccount != null) {
                        phone = corporate.adminAccount.phone != null ? corporate.adminAccount.phone : "";
                        firstName = corporate.adminAccount.firstName != null ? corporate.adminAccount.firstName : "";
                        lastName = corporate.adminAccount.lastName != null ? corporate.adminAccount.lastName : "";
                        email = corporate.adminAccount.email != null ? corporate.adminAccount.email : "";
                    }
                    GatewayPayMaya gatewayFleet = mongoDao.getGatewayPayMaya(fleetId);
                    GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.PAYMAYA, gatewayFleet.environment);
                    PayMayaUtil payMayaUtil = new PayMayaUtil(paymentNotificationEnt.requestId, gatewayFleet.publicKey, gatewayFleet.secretKey, gatewayURL.url);
                    String paymentTokenId = paymentNotificationEnt.token;
                    result = payMayaUtil.createCustomer(paymentTokenId, gwOrderid, firstName, lastName, phone, email);
                } else {
                    ObjResponse response = new ObjResponse();
                    response.returnCode = 525;
                    result = response.toString();
                }
                if (CommonUtils.getReturnCode(result) == 200) {
                    String cardTokenId = CommonUtils.getValue(result, "cardTokenId");
                    String maskedPan = CommonUtils.getValue(result, "maskedPan");
                    String cardType = CommonUtils.getValue(result, "cardType");
                    redisDao.addTmpData(paymentNotificationEnt.requestId, gwOrderid + "cardTokenId", cardTokenId);
                    redisDao.addTmpData(paymentNotificationEnt.requestId, gwOrderid + "maskedPan", maskedPan);
                    redisDao.addTmpData(paymentNotificationEnt.requestId, gwOrderid + "cardType", cardType);
                }
                return result;
            } else if (urlAction.equals("success")) {
                String cardTokenId = redisDao.getTmp(paymentNotificationEnt.requestId, gwOrderid + "cardTokenId");
                String maskedPan = redisDao.getTmp(paymentNotificationEnt.requestId, gwOrderid + "maskedPan");
                String cardType = redisDao.getTmp(paymentNotificationEnt.requestId, gwOrderid + "cardType");
                String cardHolder = "";
                Map<String, String> mapResponse = new HashMap<>();
                mapResponse.put("message", "success");
                mapResponse.put("cardMask", "XXXXXXXXXXXX" + maskedPan);
                mapResponse.put("qrCode", "");
                mapResponse.put("token", cardTokenId);
                mapResponse.put("cardType", cardType);
                mapResponse.put("cardHolder", cardHolder);
                ObjResponse objResponse = new ObjResponse();
                objResponse.response = mapResponse;
                objResponse.returnCode = 200;

                // temporary save result to Redis and retrieve when completed create token
                redisDao.addTmpData(paymentNotificationEnt.requestId, gwOrderid+"token", objResponse.toString());

                // check Account or Corporate
                String type = creditEnt.type != null ? creditEnt.type : "user";
                Account account = mongoDao.getAccount(userId);
                if (account == null) {
                    if (type.equals("corporate")) {
                        Corporate corporate = mongoDao.getCorporate(corporateId);
                        if (corporate != null) {
                            creditEnt.userId = corporateId;
                        }
                    }
                }
                // continue add card progress
                String resultCreateToken = createToken(creditEnt, type, gwOrderid, true);
                String requestType  = creditEnt.type != null ? creditEnt.type : "";
                String from = creditEnt.from != null ? creditEnt.from : "";
                return completeRegisterProcess(paymentNotificationEnt.requestId, resultCreateToken, userId, fleetId, type, requestType, from);
            } else if (urlAction.equals("cancel")) {
                notifyRegistrationFailed(paymentNotificationEnt.requestId, 466, userId, fleetId, errorMessage);
                return "Transaction has been canceled. Please try again!";
            } else {
                notifyRegistrationFailed(paymentNotificationEnt.requestId, 437, userId, fleetId, errorMessage);
                return "Something went wrong. Please try again!";
            }
        } catch (Exception ex) {
            logger.debug(paymentNotificationEnt.requestId + " - exception: " + CommonUtils.getError(ex));
        }
        return "Something went wrong. Please try again!";
    }

    public String paymentMADANotificationURL(PaymentNotificationEnt paymentNotificationEnt) {
        ObjResponse response = new ObjResponse();
        try {
            String orderId = paymentNotificationEnt.TrackId != null ? paymentNotificationEnt.TrackId : "";
            String cacheData = redisDao.getTmp(paymentNotificationEnt.requestId, orderId+"credit");
            boolean success = false;
            String errorMessage = "";
            String userId = "";
            String fleetId = "";
            if (!cacheData.isEmpty()) {
                CreditEnt creditEnt = gson.fromJson(cacheData, CreditEnt.class);
                userId = creditEnt.userId != null ? creditEnt.userId : "";
                // check Account or Corporate
                String type = "user";
                Account account = mongoDao.getAccount(userId);
                if (account == null) {
                    Corporate corporate = mongoDao.getCorporate(userId);
                    if (corporate != null) {
                        type = "corporate";
                    }
                }
                fleetId = creditEnt.fleetId != null ? creditEnt.fleetId : "";
                logger.debug(paymentNotificationEnt.requestId + " - remove cache data for orderId: " + orderId);
                redisDao.removeTmp(paymentNotificationEnt.requestId, orderId+"credit");

                String responseCode = paymentNotificationEnt.ResponseCode != null ? paymentNotificationEnt.ResponseCode : "";
                String maskedPAN = paymentNotificationEnt.maskedPAN != null ? paymentNotificationEnt.maskedPAN : "";
                String cardToken = paymentNotificationEnt.cardToken != null ? paymentNotificationEnt.cardToken : "";
                String cardBrand = paymentNotificationEnt.cardBrand != null ? paymentNotificationEnt.cardBrand : "";
                if (responseCode.equals("000")) {
                    String last4 = maskedPAN.length() > 4 ? maskedPAN.substring(maskedPAN.length() - 4) : "";
                    Map<String, String> mapResponse = new HashMap<>();
                    mapResponse.put("message", "success");
                    mapResponse.put("cardMask", "XXXXXXXXXXXX" + last4);
                    mapResponse.put("qrCode", "");
                    mapResponse.put("token", cardToken);
                    mapResponse.put("cardType", cardBrand);
                    mapResponse.put("cardHolder", "");
                    ObjResponse objResponse = new ObjResponse();
                    objResponse.response = mapResponse;
                    objResponse.returnCode = 200;

                    // temporary save result to Redis and retrieve when completed create token
                    redisDao.addTmpData(paymentNotificationEnt.requestId, orderId+"token", objResponse.toString());

                    // continue add card progress
                    String resultCreateToken = createToken(creditEnt, type, orderId, true);
                    ObjResponse resultCreateTokenObject = gson.fromJson(resultCreateToken, ObjResponse.class);
                    if (resultCreateTokenObject.returnCode == 200) {
                        success = true;
                        Map<String, Object> data = new HashMap<String, Object>();
                        data.put("returnCode", 200);
                        data.put("requestId", paymentNotificationEnt.requestId);
                        data.put("userId", creditEnt.userId);
                        data.put("fleetId",creditEnt.fleetId);
                        data.put("credit", resultCreateTokenObject.response);
                        data.put("errorMessage", "");

                        String from = creditEnt.from != null ? creditEnt.from : "";
                        // send result to Jupiter
                        if (type.equals("user") && from.isEmpty()) {
                            org.json.JSONObject apiResponse = paymentUtil.sendPost(paymentNotificationEnt.requestId, gson.toJson(data),
                                    PaymentUtil.add_card_senangpay, ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
                            // show log
                            logger.debug(paymentNotificationEnt.requestId + " - apiURL: " + PaymentUtil.add_card_senangpay);
                            logger.debug(paymentNotificationEnt.requestId + " - data: " + gson.toJson(data));
                            logger.debug(paymentNotificationEnt.requestId + " - apiResponse: " + apiResponse);
                        }

                        // check if request from WB then send notification to WB
                        if (from.equals("wb")) {
                            JSONObject objData = new JSONObject();
                            objData.put("returnCode", 200);
                            objData.put("fleetId", fleetId);
                            objData.put("userId", userId);
                            objData.put("credit", resultCreateTokenObject.response);
                            paymentUtil.sendRegisterCardResultToWB(objData, userId, paymentNotificationEnt.requestId);
                        }
                    } else {
                        errorMessage = CommonUtils.getValue(resultCreateToken, "message");
                    }
                }
            } else {
                logger.debug(paymentNotificationEnt.requestId + " - cache data for orderId " + orderId + " has been removed!!!");
            }

            if (!success) {
                return notifyRegistrationFailed(paymentNotificationEnt.requestId, 525, userId, fleetId, errorMessage);
            } else {
                // return to request
                response.returnCode = 200;
                return response.toString();
            }
        } catch (Exception ex) {
            logger.debug(paymentNotificationEnt.requestId + " - exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response.toString();
        }
    }

    public String paymentCXPayNotificationURL(PaymentNotificationEnt paymentNotificationEnt) {
        try {
            String gwOrderid = paymentNotificationEnt.gwOrderid != null ? paymentNotificationEnt.gwOrderid : "";
            String cachedData = redisDao.getTmp(paymentNotificationEnt.requestId, gwOrderid + "credit");
            if (cachedData.isEmpty()) {
                cachedData = "";
                if (cachedData.isEmpty()) {
                    logger.debug(paymentNotificationEnt.requestId + " - received payment notification");
                    return "The request was processed successfully.";
                }
            } else {
                redisDao.removeTmp(paymentNotificationEnt.requestId, gwOrderid + "credit");
            }
            CreditEnt creditEnt = gson.fromJson(cachedData, CreditEnt.class);
            String userId = creditEnt.userId != null ? creditEnt.userId : "";
            String corporateId = creditEnt.corporateId != null ? creditEnt.corporateId : "";
            String fleetId = creditEnt.fleetId != null ? creditEnt.fleetId : "";

            // check Account or Corporate
            String type = creditEnt.type != null ? creditEnt.type : "user";
            Account account = mongoDao.getAccount(userId);
            if (account == null) {
                if (type.equals("corporate")) {
                    Corporate corporate = mongoDao.getCorporate(corporateId);
                    if (corporate != null) {
                        creditEnt.userId = corporateId;
                    }
                }
            }
            // continue add card progress
            String resultCreateToken = createToken(creditEnt, type, gwOrderid, true);
            String requestType  = creditEnt.type != null ? creditEnt.type : "";
            String from = creditEnt.from != null ? creditEnt.from : "";
            return completeRegisterProcess(paymentNotificationEnt.requestId, resultCreateToken, userId, fleetId, type, requestType, from);
        } catch (Exception ex) {
            logger.debug(paymentNotificationEnt.requestId + " - exception: " + CommonUtils.getError(ex));
        }
        return "Something went wrong. Please try again!";
    }

    public String paymentFACNotificationURL(PaymentNotificationEnt paymentNotificationEnt) {
        String requestId = paymentNotificationEnt.requestId;
        try {
            String gwOrderid = paymentNotificationEnt.gwOrderid != null ? paymentNotificationEnt.gwOrderid : "";
            String cachedData = redisDao.getTmp(paymentNotificationEnt.requestId, gwOrderid + "credit");
            if (cachedData.isEmpty()) {
                cachedData = "";
                if (cachedData.isEmpty()) {
                    logger.debug(requestId + " - received payment notification");
                    return "The request was processed successfully.";
                }
            } else {
                redisDao.removeTmp(paymentNotificationEnt.requestId, gwOrderid + "credit");
            }
            CreditEnt creditEnt = gson.fromJson(cachedData, CreditEnt.class);
            String userId = creditEnt.userId != null ? creditEnt.userId : "";
            String corporateId = creditEnt.corporateId != null ? creditEnt.corporateId : "";
            String fleetId = creditEnt.fleetId != null ? creditEnt.fleetId : "";

            // check Account or Corporate
            String type = creditEnt.type != null ? creditEnt.type : "user";
            Account account = mongoDao.getAccount(userId);
            if (account == null) {
                if (type.equals("corporate")) {
                    Corporate corporate = mongoDao.getCorporate(corporateId);
                    if (corporate != null) {
                        creditEnt.userId = corporateId;
                    }
                }
            }
            PowerTranz powerTranzData = gson.fromJson(paymentNotificationEnt.Response, PowerTranz.class);
            logger.debug(requestId + " - powerTranzData: " + gson.toJson(powerTranzData));
            String token = powerTranzData.PanToken != null ? powerTranzData.PanToken : "";
            logger.debug(requestId + " - token: " + token);
            ObjResponse createTokenResponse = new ObjResponse();
            if (powerTranzData.Errors != null && powerTranzData.Errors.size() > 0) {
                PowerTranzError error = powerTranzData.Errors.get(0);
                Map<String,String> response = new HashMap<>();
                response.put("message", "Invalid input: Please check the card info and try again");
                createTokenResponse.response = response;
                createTokenResponse.returnCode = 437;
            } else {
                if (!token.isEmpty()) {// add token for fleet successful
                    String cacheDataAPI = redisDao.getTmp(requestId, gwOrderid+"-cache");
                    logger.debug(requestId + " - cacheDataAPI: " + cacheDataAPI);
                    redisDao.removeTmp(requestId, gwOrderid+"-cache");

                    Map<String,String> mapResponse = new HashMap<>();
                    mapResponse.put("message", "");
                    mapResponse.put("creditCard", "XXXXXXXXXXXX" + powerTranzData.CardSuffix);
                    mapResponse.put("qrCode", "");
                    mapResponse.put("token", token);
                    mapResponse.put("cardType", powerTranzData.CardBrand);
                    if (!cacheDataAPI.isEmpty()) {
                        org.json.JSONObject jsonCache = new org.json.JSONObject(cacheDataAPI);
                        mapResponse.put("address", jsonCache.has("address") ? jsonCache.getString("address") : "");
                        mapResponse.put("city", jsonCache.has("city") ? jsonCache.getString("city") : "");
                        mapResponse.put("state", jsonCache.has("state") ? jsonCache.getString("state") : "");
                        mapResponse.put("postal", jsonCache.has("postal") ? jsonCache.getString("postal") : "");
                        mapResponse.put("country", jsonCache.has("country") ? jsonCache.getString("country") : "");
                        mapResponse.put("email", jsonCache.has("email") ? jsonCache.getString("email") : "");
                    }

                    createTokenResponse.returnCode = 200;
                    createTokenResponse.response = mapResponse;
                } else {
                    Map<String,String> response = new HashMap<>();
                    response.put("message", "Invalid input: Please check the card info and try again");
                    createTokenResponse.response = response;
                    createTokenResponse.returnCode = 437;
                }
            }

            // temporary save result to Redis and retrieve when completed create token
            redisDao.add3DSData(creditEnt.requestId, gwOrderid, createTokenResponse.toString(), "3dstoken");
            logger.debug(paymentNotificationEnt.requestId + " - creditEnt: " + gson.toJson(creditEnt));
            // continue add card progress
            String resultCreateToken = createToken(creditEnt, type, gwOrderid, true);
            String requestType  = creditEnt.type != null ? creditEnt.type : "";
            String from = creditEnt.from != null ? creditEnt.from : "";
            return completeRegisterProcess(paymentNotificationEnt.requestId, resultCreateToken, userId, fleetId, type, requestType, from);
        } catch (Exception ex) {
            logger.debug(paymentNotificationEnt.requestId + " - exception: " + CommonUtils.getError(ex));
        }
        return "Something went wrong. Please try again!";
    }

    public String paymentPayWayNotificationURL(PaymentNotificationEnt paymentNotificationEnt) {
        ObjResponse response = new ObjResponse();
        try {
            String orderId = paymentNotificationEnt.tran_id != null ? paymentNotificationEnt.tran_id : "";
            if (orderId.isEmpty()) {
                Map<String, String> mapResponse = new HashMap<>();
                mapResponse.put("message", "empty tran_id");
                response.response = mapResponse;
                response.returnCode = 525;
                return response.toString();
            }
            String cacheData = redisDao.getTmp(paymentNotificationEnt.requestId, orderId+"credit");
            boolean success = false;
            String errorMessage = "";
            String userId = "";
            String fleetId = "";
            if (!cacheData.isEmpty()) {
                CreditEnt creditEnt = gson.fromJson(cacheData, CreditEnt.class);
                userId = creditEnt.userId != null ? creditEnt.userId : "";
                // check Account or Corporate
                String type = "user";
                Account account = mongoDao.getAccount(userId);
                if (account == null) {
                    Corporate corporate = mongoDao.getCorporate(userId);
                    if (corporate != null) {
                        type = "corporate";
                    }
                }
                fleetId = creditEnt.fleetId != null ? creditEnt.fleetId : "";
                logger.debug(paymentNotificationEnt.requestId + " - remove cache data for orderId: " + orderId);
                redisDao.removeTmp(paymentNotificationEnt.requestId, orderId+"credit");

                String status = paymentNotificationEnt.status != null ? paymentNotificationEnt.status.toString() : "";
                PayWayReturnParams returnParams = paymentNotificationEnt.return_params;
                String paymentStatus = returnParams.payment_status != null ? returnParams.payment_status : "";
                String ctid = returnParams.ctid != null ? returnParams.ctid : "";
                PayWayCredit payWayCredit = returnParams != null && returnParams.card_status != null ? returnParams.card_status : null;
                String cardToken = "";
                String cardBrand = "";
                if (status.equals("0") && paymentStatus.equals("APPROVED") && payWayCredit != null) {
                    String maskedPAN = payWayCredit.mask_pan != null ? payWayCredit.mask_pan : "";
                    //String last4 = maskedPAN.length() > 4 ? maskedPAN.substring(maskedPAN.length() - 4) : "";
                    String pwt = payWayCredit.pwt != null ? payWayCredit.pwt : "";
                    // check if same token
                    boolean isSame = false;
                    if (type.equals("user")) {
                        if (account != null && account.credits != null && !account.credits.isEmpty()) {
                            for (Credit credit : account.credits) {
                                String[] arrToken = credit.localToken.split(KeysUtil.TEMPKEY);
                                if (arrToken.length > 0 && arrToken[1].equals(pwt)) {
                                    isSame = true;
                                    break;
                                }
                            }
                        }
                    } else if (type.equals("corporate")) {
                        Corporate corporate = mongoDao.getCorporate(creditEnt.userId);
                        if (corporate != null && corporate.credits != null && !corporate.credits.isEmpty()) {
                            for (Credit credit : corporate.credits) {
                                String[] arrToken = credit.localToken.split(KeysUtil.TEMPKEY);
                                if (arrToken.length > 0 && arrToken[1].equals(pwt)) {
                                    isSame = true;
                                    break;
                                }
                            }
                        }
                    }
                    logger.debug(paymentNotificationEnt.requestId + " - same token ? " + isSame);
                    if (!isSame) {
                        cardToken = ctid + KeysUtil.TEMPKEY + pwt;
                        cardBrand = payWayCredit.card_type != null ? payWayCredit.card_type : "";
                        if (cardBrand.equals("MC")) {
                            cardBrand = "MASTERCARD";
                        }
                        if (cardBrand.equals("CUP") || cardBrand.equals("UPI")) {
                            cardBrand = KeysUtil.UNIONPAY;
                        }
                        Map<String, String> mapResponse = new HashMap<>();
                        mapResponse.put("message", "success");
                        mapResponse.put("cardMask", maskedPAN);
                        mapResponse.put("qrCode", "");
                        mapResponse.put("token", cardToken);
                        mapResponse.put("cardType", cardBrand);
                        mapResponse.put("cardHolder", "");
                        ObjResponse objResponse = new ObjResponse();
                        objResponse.response = mapResponse;
                        objResponse.returnCode = 200;

                        // temporary save result to Redis and retrieve when completed create token
                        redisDao.addTmpData(paymentNotificationEnt.requestId, orderId + "token", objResponse.toString());

                        // continue add card progress
                        logger.debug(paymentNotificationEnt.requestId + " - type: " + type);
                        String resultCreateToken = createToken(creditEnt, type, orderId, true);
                        logger.debug(paymentNotificationEnt.requestId + " - resultCreateToken: " + resultCreateToken);
                        ObjResponse resultCreateTokenObject = gson.fromJson(resultCreateToken, ObjResponse.class);
                        if (resultCreateTokenObject.returnCode == 200) {
                            success = true;
                            Map<String, Object> data = new HashMap<String, Object>();
                            String sessionId = creditEnt.sessionId != null ? creditEnt.sessionId : "";
                            if (!sessionId.isEmpty()) {
                                data.put("sessionId", sessionId);
                            }

                            data.put("returnCode", 200);
                            data.put("requestId", paymentNotificationEnt.requestId);
                            data.put("userId", creditEnt.userId);
                            data.put("fleetId", creditEnt.fleetId);
                            data.put("credit", resultCreateTokenObject.response);
                            data.put("errorMessage", "");

                            String from = creditEnt.from != null ? creditEnt.from : "";
                            // send result to Jupiter
                            if ((type.equals("user") || type.equals("corporate")) && from.isEmpty()) {
                                org.json.JSONObject apiResponse = paymentUtil.sendPost(paymentNotificationEnt.requestId, gson.toJson(data),
                                        PaymentUtil.add_card_senangpay, ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
                                // show log
                                logger.debug(paymentNotificationEnt.requestId + " - apiURL: " + PaymentUtil.add_card_senangpay);
                                logger.debug(paymentNotificationEnt.requestId + " - data: " + gson.toJson(data));
                                logger.debug(paymentNotificationEnt.requestId + " - apiResponse: " + apiResponse);
                            }

                            // check if request from WB then send notification to WB
                            if (from.equals("wb")) {
                                JSONObject objData = new JSONObject();
                                objData.put("returnCode", 200);
                                objData.put("fleetId", fleetId);
                                objData.put("userId", userId);
                                objData.put("credit", resultCreateTokenObject.response);
                                paymentUtil.sendRegisterCardResultToWB(objData, userId, paymentNotificationEnt.requestId);
                            }
                        } else {
                            errorMessage = CommonUtils.getValue(resultCreateToken, "message");
                        }
                    }
                }
            } else {
                logger.debug(paymentNotificationEnt.requestId + " - cache data for orderId " + orderId + " has been removed!!!");
            }

            if (!success) {
                return notifyRegistrationFailed(paymentNotificationEnt.requestId, 525, userId, fleetId, errorMessage);
            } else {
                // return to request
                response.returnCode = 200;
                return response.toString();
            }
        } catch (Exception ex) {
            logger.debug(paymentNotificationEnt.requestId + " - exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response.toString();
        }
    }

    public String paymentECPayNotificationURL(PaymentNotificationEnt paymentNotificationEnt) {
        ObjResponse response = new ObjResponse();
        try {
            String orderId = paymentNotificationEnt.MerchantTradeNo != null ? paymentNotificationEnt.MerchantTradeNo : "";
            String returnCode = paymentNotificationEnt.RtnCode != null ? paymentNotificationEnt.RtnCode : "";
            String cacheData = redisDao.getTmp(paymentNotificationEnt.requestId, orderId+"credit");
            String merchantMemberID = redisDao.getTmp(paymentNotificationEnt.requestId, orderId+"merchantMemberID");
            boolean success = false;
            String errorMessage = "";
            String userId = "";
            String fleetId = "";
            if (!cacheData.isEmpty()) {
                CreditEnt creditEnt = gson.fromJson(cacheData, CreditEnt.class);
                userId = creditEnt.userId != null ? creditEnt.userId : "";
                // check Account or Corporate
                String type = "user";
                Account account = mongoDao.getAccount(userId);
                if (account == null) {
                    Corporate corporate = mongoDao.getCorporate(userId);
                    if (corporate != null) {
                        type = "corporate";
                    }
                }
                fleetId = creditEnt.fleetId != null ? creditEnt.fleetId : "";
                logger.debug(paymentNotificationEnt.requestId + " - remove cache data for orderId: " + orderId);
                redisDao.removeTmp(paymentNotificationEnt.requestId, orderId+"credit");
                redisDao.removeTmp(paymentNotificationEnt.requestId, orderId+"merchantMemberID");

                if (returnCode.equals("1")) {
                    String maskedPAN = paymentNotificationEnt.card4no != null ? paymentNotificationEnt.card4no : "";
                    String card6no = paymentNotificationEnt.card6no != null ? paymentNotificationEnt.card6no : "";
                    String cardToken = merchantMemberID;
                    String cardBrand = ValidCreditCard.getCardType(card6no+"0000000000");
                    Map<String, String> mapResponse = new HashMap<>();
                    mapResponse.put("message", "success");
                    mapResponse.put("cardMask", "XXXXXXXXXXXX"+maskedPAN);
                    mapResponse.put("qrCode", "");
                    mapResponse.put("token", cardToken);
                    mapResponse.put("cardType", cardBrand);
                    mapResponse.put("cardHolder", "");
                    ObjResponse objResponse = new ObjResponse();
                    objResponse.response = mapResponse;
                    objResponse.returnCode = 200;

                    // temporary save result to Redis and retrieve when completed create token
                    redisDao.addTmpData(paymentNotificationEnt.requestId, orderId + "token", objResponse.toString());

                    // continue to add card progress
                    logger.debug(paymentNotificationEnt.requestId + " - type: " + type);
                    String resultCreateToken = createToken(creditEnt, type, orderId, true);
                    logger.debug(paymentNotificationEnt.requestId + " - resultCreateToken: " + resultCreateToken);
                    ObjResponse resultCreateTokenObject = gson.fromJson(resultCreateToken, ObjResponse.class);
                    if (resultCreateTokenObject.returnCode == 200) {
                        success = true;
                        Map<String, Object> data = new HashMap<String, Object>();
                        String sessionId = creditEnt.sessionId != null ? creditEnt.sessionId : "";
                        if (!sessionId.isEmpty()) {
                            data.put("sessionId", sessionId);
                        }

                        data.put("returnCode", 200);
                        data.put("requestId", paymentNotificationEnt.requestId);
                        data.put("userId", creditEnt.userId);
                        data.put("fleetId", creditEnt.fleetId);
                        data.put("credit", resultCreateTokenObject.response);
                        data.put("errorMessage", "");

                        String from = creditEnt.from != null ? creditEnt.from : "";
                        // send result to Jupiter
                        if ((type.equals("user") || type.equals("corporate")) && from.isEmpty()) {
                            org.json.JSONObject apiResponse = paymentUtil.sendPost(paymentNotificationEnt.requestId, gson.toJson(data),
                                    PaymentUtil.add_card_senangpay, ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
                            // show log
                            logger.debug(paymentNotificationEnt.requestId + " - apiURL: " + PaymentUtil.add_card_senangpay);
                            logger.debug(paymentNotificationEnt.requestId + " - data: " + gson.toJson(data));
                            logger.debug(paymentNotificationEnt.requestId + " - apiResponse: " + apiResponse);
                        }

                        // check if request from WB then send notification to WB
                        if (from.equals("wb")) {
                            JSONObject objData = new JSONObject();
                            objData.put("returnCode", 200);
                            objData.put("fleetId", fleetId);
                            objData.put("userId", userId);
                            objData.put("credit", resultCreateTokenObject.response);
                            paymentUtil.sendRegisterCardResultToWB(objData, userId, paymentNotificationEnt.requestId);
                        }

                        // do void authorized amount for registration
                        String getConfigInfo = mongoDao.getConfigInfo(creditEnt.fleetId, "Gateway"+KeysUtil.ECPAY);
                        GatewayECPay gatewayFleet = gson.fromJson(getConfigInfo, GatewayECPay.class);
                        GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.ECPAY, gatewayFleet.environment);
                        ECPayUtil ecPayUtil = new ECPayUtil(paymentNotificationEnt.requestId, gatewayFleet.merchantId, gatewayFleet.hashKey, gatewayFleet.hashIV, gatewayFleet.creditCheckCode, gatewayURL.url);
                        String tradeNo = paymentNotificationEnt.TradeNo != null ? paymentNotificationEnt.TradeNo : "";
                        ecPayUtil.voidTransaction(orderId, tradeNo, 5); // default register amount is 5

                    } else {
                        errorMessage = CommonUtils.getValue(resultCreateToken, "message");
                    }
                } else {
                    errorMessage = paymentNotificationEnt.RtnMsg != null ? paymentNotificationEnt.RtnMsg : "";
                }
            } else {
                logger.debug(paymentNotificationEnt.requestId + " - cache data for orderId " + orderId + " has been removed!!!");
            }

            if (!success) {
                return notifyRegistrationFailed(paymentNotificationEnt.requestId, 525, userId, fleetId, errorMessage);
            } else {
                // Step3. Merchant: If the checksum is correct, respond 1|OK on the webpage end.
                return "OK";
            }
        } catch (Exception ex) {
            logger.debug(paymentNotificationEnt.requestId + " - exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response.toString();
        }
    }

    public String paymentOnePayNotificationURL(PaymentNotificationEnt paymentNotificationEnt, String action) {
        ObjResponse response = new ObjResponse();
        String errorMessage = "";
        boolean success = false;
        try {
            String orderId = paymentNotificationEnt.vpc_MerchTxnRef != null ? paymentNotificationEnt.vpc_MerchTxnRef : "";
            String creditCache = redisDao.getTmp(paymentNotificationEnt.requestId, orderId+"credit");

            CreditEnt creditEnt = gson.fromJson(creditCache, CreditEnt.class);
            String type = "user";
            Account account = mongoDao.getAccount(creditEnt.userId);
            if (account == null) {
                Corporate corporate = mongoDao.getCorporate(creditEnt.userId);
                if (corporate != null) {
                    type = "corporate";
                }
            }
            String fleetId = creditEnt.fleetId != null ? creditEnt.fleetId : "";
            logger.debug(paymentNotificationEnt.requestId + " - remove cache data for orderId: " + orderId);
            redisDao.removeTmp(paymentNotificationEnt.requestId, orderId+"credit");

            String responseCode = paymentNotificationEnt.vpc_TxnResponseCode;
            if (!"0".equalsIgnoreCase(responseCode)) {
                errorMessage = paymentNotificationEnt.vpc_Message;
            } else {
                String getConfigInfo =
                        mongoDao.getConfigInfo(creditEnt.fleetId, "Gateway" + KeysUtil.ONEPAY);
                GatewayOnePay gatewayOnePay = gson.fromJson(getConfigInfo, GatewayOnePay.class);
                GatewayURL gatewayURL =
                        mongoDao.getGatewayURL(KeysUtil.ONEPAY, gatewayOnePay.environment);
                OnePayUtil onePayUtil =
                        new OnePayUtil(
                                paymentNotificationEnt.requestId,
                                gatewayOnePay.merchantId,
                                gatewayOnePay.accessCode,
                                gatewayOnePay.hashCode,
                                gatewayURL.url);
                Map<String, Object> params = new HashMap<>();
                if (!StringUtils.isEmpty(paymentNotificationEnt.vpc_TokenNum)) {
                    params.put("vpc_TokenNum", paymentNotificationEnt.vpc_TokenNum);
                }
                if (!StringUtils.isEmpty(paymentNotificationEnt.vpc_TokenExp)) {
                    params.put("vpc_TokenExp", paymentNotificationEnt.vpc_TokenExp);
                }
                params.put("vpc_CardUid", paymentNotificationEnt.vpc_CardUid);
                params.put("vpc_PayChannel", paymentNotificationEnt.vpc_PayChannel);
                params.put("vpc_Card", paymentNotificationEnt.vpc_Card);
                if (paymentNotificationEnt.vpc_CardExp != null) {
                    params.put("vpc_CardExp", paymentNotificationEnt.vpc_CardExp);
                }
                params.put("vpc_Message", paymentNotificationEnt.vpc_Message);
                params.put("vpc_TransactionNo", paymentNotificationEnt.vpc_TransactionNo);
                params.put("vpc_TxnResponseCode", paymentNotificationEnt.vpc_TxnResponseCode);
                params.put("vpc_Amount", paymentNotificationEnt.vpc_Amount);
                if (paymentNotificationEnt.vpc_AuthorizeId != null) {
                    params.put("vpc_AuthorizeId", paymentNotificationEnt.vpc_AuthorizeId);
                }
                params.put("vpc_OrderInfo", paymentNotificationEnt.vpc_OrderInfo);
                params.put("vpc_Merchant", paymentNotificationEnt.vpc_Merchant);
                params.put("vpc_MerchTxnRef", paymentNotificationEnt.vpc_MerchTxnRef);
                params.put("vpc_Version", paymentNotificationEnt.vpc_Version);
                params.put("vpc_Command", paymentNotificationEnt.vpc_Command);
                params.put("vpc_CardNum", paymentNotificationEnt.vpc_CardNum);
                boolean isValidHash = onePayUtil.validateSecureHash(params,
                        paymentNotificationEnt.vpc_SecureHash);
                if (!isValidHash) {
                    logger.debug(paymentNotificationEnt.requestId + " - invalid hash");
                    errorMessage = "Something went wrong. Please try again!";
                } else {
                    saveTmpDataToRedis(paymentNotificationEnt, orderId);
                    // continue to add card progress
                    logger.debug(paymentNotificationEnt.requestId + " - type: " + type);
                    String resultCreateToken = createToken(creditEnt, type, orderId, true);
                    logger.debug(paymentNotificationEnt.requestId + " - resultCreateToken: " + resultCreateToken);
                    ObjResponse resultCreateTokenObject = gson.fromJson(resultCreateToken, ObjResponse.class);
                    if (resultCreateTokenObject.returnCode == 200) {
                        success = true;
                        Map<String, Object> data = new HashMap<String, Object>();
                        String sessionId = creditEnt.sessionId != null ? creditEnt.sessionId : "";
                        if (!sessionId.isEmpty()) {
                            data.put("sessionId", sessionId);
                        }

                        data.put("returnCode", 200);
                        data.put("requestId", paymentNotificationEnt.requestId);
                        data.put("userId", creditEnt.userId);
                        data.put("fleetId", creditEnt.fleetId);
                        data.put("credit", resultCreateTokenObject.response);
                        data.put("errorMessage", "");

                        String from = creditEnt.from != null ? creditEnt.from : "";
                        // send result to Jupiter
                        if ((type.equals("user") || type.equals("corporate")) && from.isEmpty()) {
                            org.json.JSONObject apiResponse = paymentUtil.sendPost(paymentNotificationEnt.requestId, gson.toJson(data),
                                PaymentUtil.add_card_senangpay, ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
                            // show log
                            logger.debug(paymentNotificationEnt.requestId + " - apiURL: " + PaymentUtil.add_card_senangpay);
                            logger.debug(paymentNotificationEnt.requestId + " - data: " + gson.toJson(data));
                            logger.debug(paymentNotificationEnt.requestId + " - apiResponse: " + apiResponse);
                        }

                        // check if request from WB then send notification to WB
                        if (from.equals("wb")) {
                            JSONObject objData = new JSONObject();
                            objData.put("returnCode", 200);
                            objData.put("fleetId", fleetId);
                            objData.put("userId", creditEnt.userId);
                            objData.put("credit", resultCreateTokenObject.response);
                            paymentUtil.sendRegisterCardResultToWB(objData, creditEnt.userId, paymentNotificationEnt.requestId);
                        }

                        if ("token".equalsIgnoreCase(action)) {

                            String orgMerchTxnRef = paymentNotificationEnt.vpc_MerchTxnRef;
                            String amount = paymentNotificationEnt.vpc_Amount;
                            if (internationalCardList.contains(paymentNotificationEnt.vpc_Card)) {
                                Timer timer = new Timer();
                                timer.schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        try {
                                            logger.debug(paymentNotificationEnt.requestId + " - run void transaction after 3 mins");
                                            String result = onePayUtil.voidTransaction(orgMerchTxnRef, amount, creditEnt.userId);
                                            if (CommonUtils.getReturnCode(result) != 200) {
                                                onePayUtil.refundTransaction(orgMerchTxnRef, amount, creditEnt.userId);
                                            }
                                        } catch (Exception e) {
                                            logger.debug(paymentNotificationEnt.requestId + " - exception when run void transaction: " + e.getMessage());
                                        }
                                    }}, 3 * 60 * 1000); // Charged 10.000 VND to create token, refund it, but in delay of 3 mins
                            } else {
                                onePayUtil.refundTransaction(orgMerchTxnRef, amount, creditEnt.userId);
                            }
                        }
                    }
                }
            }
            if (!success) {
                return notifyRegistrationFailed(paymentNotificationEnt.requestId, 525, creditEnt.userId, creditEnt.fleetId, errorMessage);
            } else {
                return "OK";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(paymentNotificationEnt.requestId + " - exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response.toString();
        }
    }

    private void saveTmpDataToRedis(PaymentNotificationEnt paymentNotificationEnt, String orderId) {
        Map<String, String> mapResponse = new HashMap<>();
        String lastFour = paymentNotificationEnt.vpc_CardNum.substring(paymentNotificationEnt.vpc_CardNum.length() - 4);
        mapResponse.put("message", "success");
        mapResponse.put("cardMask", "XXXXXXXXXXXX"+lastFour);
        mapResponse.put("qrCode", "");
        mapResponse.put("token", paymentNotificationEnt.vpc_TokenNum + KeysUtil.TEMPKEY + paymentNotificationEnt.vpc_TokenExp);
        mapResponse.put("cardType", getCardType(paymentNotificationEnt.vpc_Card));
        mapResponse.put("cardHolder", "");
        ObjResponse objResponse = new ObjResponse();
        objResponse.response = mapResponse;
        objResponse.returnCode = 200;
        redisDao.cacheData(paymentNotificationEnt.requestId, orderId + "token", objResponse.toString(), 3);
    }

    private String getCardType(String type) {
        switch (type) {
            case "VC":
                return "VISA";
            case "MC":
                return "MASTERCARD";
            case "JC":
                return "JCB";
            case "AE":
                return "AMEX";
            case "CUP":
                return "UNIONPAY";
            default:
                return "";
        }
    }

    private String completeRegisterProcess(String requestId, String resultCreateToken, String userId, String fleetId, String type, String requestType, String from) {
        ObjResponse resultCreateTokenObject = gson.fromJson(resultCreateToken, ObjResponse.class);
        boolean success = false;
        String errorMessage = "";
        if (resultCreateTokenObject.returnCode == 200) {
            success = true;
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("returnCode", 200);
            data.put("requestId", requestId);
            data.put("userId", userId);
            data.put("fleetId",fleetId);
            data.put("credit", resultCreateTokenObject.response);
            data.put("errorMessage", "");

            // send result to Jupiter
            if (type.equals("user")) {
                org.json.JSONObject apiResponse = paymentUtil.sendPost(requestId, gson.toJson(data),
                        PaymentUtil.add_card_senangpay, ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
                // show log
                logger.debug(requestId + " - apiURL: " + PaymentUtil.add_card_senangpay);
                logger.debug(requestId + " - data: " + gson.toJson(data));
                logger.debug(requestId + " - apiResponse: " + apiResponse);
            }

            // check if request from CC then send notification to CC
            if (!requestType.isEmpty() && from.isEmpty()) {
                JSONObject objData = new JSONObject();
                objData.put("returnCode", 200);
                objData.put("fleetId", fleetId);
                objData.put("type", requestType);
                objData.put("credit", resultCreateTokenObject.response);
                paymentUtil.sendRegisterCardResultToCC(objData, requestId);
            }

            // check if request from WB then send notification to WB
            if (from.equals("wb")) {
                JSONObject objData = new JSONObject();
                objData.put("returnCode", 200);
                objData.put("fleetId", fleetId);
                objData.put("userId", userId);
                objData.put("credit", resultCreateTokenObject.response);
                paymentUtil.sendRegisterCardResultToWB(objData, userId, requestId);
            }
        } else {
            errorMessage = CommonUtils.getValue(resultCreateToken, "message");
        }

        if (!success) {
            notifyRegistrationFailed(requestId, resultCreateTokenObject.returnCode, userId, fleetId, errorMessage);
            return "Something went wrong. Please try again!";
        } else {
            return "The request was processed successfully.";
        }
    }

    private String notifyRegistrationFailed(String requestId, int errorCode, String userId, String fleetId, String errorMessage) {
        ObjResponse response = new ObjResponse();
        JSONObject objCredit = new JSONObject();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("returnCode", errorCode);
        data.put("requestId", requestId);
        data.put("userId", userId);
        data.put("fleetId", fleetId);
        data.put("credit", objCredit);
        data.put("errorMessage", errorMessage);

        // send result to Jupiter
        org.json.JSONObject apiResponse = paymentUtil.sendPost(requestId, gson.toJson(data),
                PaymentUtil.add_card_senangpay, ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
        // show log
        logger.debug(requestId + " - apiURL: " + PaymentUtil.add_card_senangpay);
        logger.debug(requestId + " - data: " + gson.toJson(data));
        logger.debug(requestId + " - apiResponse: " + apiResponse);

        // return to request
        response.returnCode = errorCode;
        return response.toString();
    }

    public String getCheckoutPage(CreditEnt creditEnt) {
        ObjResponse response = new ObjResponse();
        try {
            String zoneId = "";
            // if using multi gateway, find zone then get gateway and currency which is assigned to this zone
            // then assign currency to fleet for using later
            // otherwise get currency from setting currency validation
            boolean multiGateway = false;
            ConfigGateway configGateway = null;
            Fleet fleet = mongoDao.getFleetInfor(creditEnt.fleetId);
            if(fleet.creditConfig != null && fleet.creditConfig.enable){
                if (fleet.creditConfig.multiGateway) {
                    // temporary not support
                    response.returnCode = 453;
                    return response.toString();
                }

                if (multiGateway) {
                    for (ConfigGateway config : fleet.creditConfig.configGateway) {
                        if (config.zones.contains(zoneId)) {
                            configGateway = config;
                            break;
                        }
                    }
                } else {
                    configGateway = (fleet.creditConfig.configGateway != null && fleet.creditConfig.configGateway.size() > 0) ? fleet.creditConfig.configGateway.get(0) : null;
                }
            }

            if (configGateway == null) {
                response.returnCode = 415;
                return response.toString();
            }
            String gateway = configGateway.gateway;
            String result;
            switch (gateway) {
                case KeysUtil.PAYMAYA : {
                    GatewayPayMaya gatewayFleet = mongoDao.getGatewayPayMaya(creditEnt.fleetId);
                    GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.PAYMAYA, gatewayFleet.environment);
                    PayMayaUtil payMayaUtil = new PayMayaUtil(creditEnt.requestId, gatewayFleet.publicKey, gatewayFleet.secretKey, gatewayURL.url);
                    String orderId = GatewayUtil.getOrderId();
                    result = payMayaUtil.initiateCreditForm(orderId);
                    if (CommonUtils.getReturnCode(result) == 200) {
                        redisDao.addTmpData(creditEnt.requestId, orderId+"credit", gson.toJson(creditEnt));
                        redisDao.addTmpData(creditEnt.requestId, "queue-"+orderId, creditEnt.fleetId);
                    }
                }
                break;
                case KeysUtil.BOG : {
                    GatewayBankOfGeorgia gatewayFleet = mongoDao.getGatewayBOG(creditEnt.fleetId);
                    GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.BOG, gatewayFleet.environment);
                    BOGUtil bogUtil = new BOGUtil(creditEnt.requestId, gatewayFleet.clientId, gatewayFleet.clientSecret, gatewayURL.serverURL, gatewayURL.payURL, gatewayURL.authURL, configGateway.environment);
                    String orderId = GatewayUtil.getOrderId();
                    result = bogUtil.getCheckoutURL(1.0, orderId, "Register", fleet.currencyISOValidation);
                    if (CommonUtils.getReturnCode(result) == 200) {
                        String transactionId = CommonUtils.getValue(result, "transactionId");
                        redisDao.addTmpData(creditEnt.requestId, KeysUtil.BOG+transactionId+"type", "Register");
                        redisDao.addTmpData(creditEnt.requestId, KeysUtil.BOG+transactionId+"credit", gson.toJson(creditEnt));
                        redisDao.addTmpData(creditEnt.requestId, "queue-"+orderId, creditEnt.fleetId);
                    }
                }
                break;
                default: {
                    response.returnCode = 453;
                    result = response.toString();
                }
            }
            return result;
        } catch (Exception ex) {
            logger.debug(creditEnt.requestId + " - exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response.toString();
        }
    }

    public String paymentBOGNotificationURL(PaymentNotificationEnt paymentNotificationEnt) {
        ObjResponse response = new ObjResponse();
        try {
            String orderId = paymentNotificationEnt.order_id != null ? paymentNotificationEnt.order_id : "";
            String cacheData = redisDao.getTmp(paymentNotificationEnt.requestId, KeysUtil.BOG+orderId+"credit");
            boolean success = false;
            String errorMessage = "";
            String userId = "";
            String fleetId = "";
            if (!cacheData.isEmpty()) {
                CreditEnt creditEnt = gson.fromJson(cacheData, CreditEnt.class);
                creditEnt.requestId = paymentNotificationEnt.requestId;
                userId = creditEnt.userId != null ? creditEnt.userId : "";
                // check Account or Corporate
                String type = "user";
                Account account = mongoDao.getAccount(userId);
                if (account == null) {
                    Corporate corporate = mongoDao.getCorporate(userId);
                    if (corporate != null) {
                        type = "corporate";
                    }
                }
                fleetId = creditEnt.fleetId != null ? creditEnt.fleetId : "";
                // remove cache
                logger.debug(paymentNotificationEnt.requestId + " - remove cache data for orderId: " + orderId);
                redisDao.removeTmp(paymentNotificationEnt.requestId, KeysUtil.BOG+orderId+"credit");
                redisDao.removeTmp(paymentNotificationEnt.requestId, KeysUtil.BOG+orderId+"type");

                // get payment data from BOG to get card info
                GatewayBankOfGeorgia gatewayBankOfGeorgia = mongoDao.getGatewayBOG(fleetId);
                GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.BOG, gatewayBankOfGeorgia.environment);
                BOGUtil bogUtil = new BOGUtil(creditEnt.requestId, gatewayBankOfGeorgia.clientId, gatewayBankOfGeorgia.clientSecret,
                        gatewayURL.serverURL, gatewayURL.payURL, gatewayURL.authURL, gatewayBankOfGeorgia.environment);
                String paymentData = bogUtil.getPaymentData(orderId, true);
                if (paymentUtil.getReturnCode(paymentData) == 200) {
                    String last4 = CommonUtils.getValue(paymentData, "last4");
                    String cardType = CommonUtils.getValue(paymentData, "cardType");
                    String token = CommonUtils.getValue(paymentData, "token");
                    String cardHolder = "";
                    Map<String, String> mapResponse = new HashMap<>();
                    mapResponse.put("message", "success");
                    mapResponse.put("cardMask", "XXXXXXXXXXXX" + last4);
                    mapResponse.put("qrCode", "");
                    mapResponse.put("token", token);
                    mapResponse.put("cardType", cardType);
                    mapResponse.put("cardHolder", cardHolder);
                    ObjResponse objResponse = new ObjResponse();
                    objResponse.response = mapResponse;
                    objResponse.returnCode = 200;

                    // trigger refund to return the money
                    bogUtil.refund(orderId);

                    // temporary save result to Redis and retrieve when completed create token
                    redisDao.addTmpData(paymentNotificationEnt.requestId, orderId+"token", objResponse.toString());

                    // continue add card progress
                    String resultCreateToken = createToken(creditEnt, type, orderId, true);
                    ObjResponse resultCreateTokenObject = gson.fromJson(resultCreateToken, ObjResponse.class);
                    if (resultCreateTokenObject.returnCode == 200) {
                        success = true;
                        Map<String, Object> data = new HashMap<String, Object>();
                        data.put("returnCode", 200);
                        data.put("requestId", paymentNotificationEnt.requestId);
                        data.put("userId", creditEnt.userId);
                        data.put("fleetId",creditEnt.fleetId);
                        data.put("credit", resultCreateTokenObject.response);
                        data.put("errorMessage", "");

                        String from = creditEnt.from != null ? creditEnt.from : "";
                        // send result to Jupiter
                        if (type.equals("user") && from.isEmpty()) {
                            org.json.JSONObject apiResponse = paymentUtil.sendPost(paymentNotificationEnt.requestId, gson.toJson(data),
                                    PaymentUtil.add_card_senangpay, ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
                            // show log
                            logger.debug(paymentNotificationEnt.requestId + " - apiURL: " + PaymentUtil.add_card_senangpay);
                            logger.debug(paymentNotificationEnt.requestId + " - data: " + gson.toJson(data));
                            logger.debug(paymentNotificationEnt.requestId + " - apiResponse: " + apiResponse);
                        }

                        // check if request from WB then send notification to WB
                        if (from.equals("wb")) {
                            JSONObject objData = new JSONObject();
                            objData.put("returnCode", 200);
                            objData.put("fleetId", fleetId);
                            objData.put("userId", userId);
                            objData.put("credit", resultCreateTokenObject.response);
                            paymentUtil.sendRegisterCardResultToWB(objData, userId, paymentNotificationEnt.requestId);
                        }
                    } else {
                        errorMessage = CommonUtils.getValue(resultCreateToken, "message");
                    }
                } else {
                    errorMessage = CommonUtils.getValue(paymentData, "message");
                }
            } else {
                logger.debug(paymentNotificationEnt.requestId + " - cache data for orderId " + orderId + " has been removed!!!");
            }

            if (!success) {
                return notifyRegistrationFailed(paymentNotificationEnt.requestId, 525, userId, fleetId, errorMessage);
            } else {
                // return to request
                response.returnCode = 200;
                return response.toString();
            }
        } catch (Exception ex) {
            logger.debug(paymentNotificationEnt.requestId + " - exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response.toString();
        }
    }

    public String cacheCXPayData(PaymentNotificationEnt paymentNotificationEnt) {
        ObjResponse objResponse = new ObjResponse();
        String userId = paymentNotificationEnt.sessionId != null ? paymentNotificationEnt.sessionId : "";
        String cvv = paymentNotificationEnt.secureID != null ? paymentNotificationEnt.secureID : "";
        redisDao.overrideTmpData(paymentNotificationEnt.requestId, userId+"-cvv", cvv);
        objResponse.returnCode = 200;
        return objResponse.toString();
    }

    public String cacheBillingData(PaymentNotificationEnt paymentNotificationEnt) {
        ObjResponse objResponse = new ObjResponse();
        String gwOrderid = paymentNotificationEnt.gwOrderid != null ? paymentNotificationEnt.gwOrderid : "";
        PaymentNotificationData data = paymentNotificationEnt.data;
        if (data != null) {
            logger.debug(paymentNotificationEnt.requestId + " - data: " + gson.toJson(data));
            redisDao.cacheData(paymentNotificationEnt.requestId, gwOrderid + "-cache", gson.toJson(data), 1);
        }
        objResponse.returnCode = 200;
        return objResponse.toString();
    }

    public String webhookStripe(Event event, String requestId) {
        ObjResponse objResponse = new ObjResponse();
        try {
            String eventId = event.getId() != null ? event.getId() : "";
            logger.debug(requestId + " - eventId = " + eventId);

            // catch to redis to avoid duplicate top-up
            boolean isCache = redisDao.addUniqueData(requestId,eventId,"cache-" + eventId);
            // if not success means this orderId has ben cached for the previous request
            if (!isCache ){
                // return for this request
                return  "DUPLICATE ID: " + event.getId();
            }
            // check if webhook from capability.updated
            String eventType = event.getType() != null ? event.getType() : "";
            logger.debug(requestId + " - eventType: " + eventType);
            EventDataObjectDeserializer dataObjectDeserializer = event.getDataObjectDeserializer();
            org.json.JSONObject jsonObject = new org.json.JSONObject(dataObjectDeserializer.getRawJson());

            String id = jsonObject.has("id") ? jsonObject.getString("id") : "";
            logger.debug(requestId + " - id: " + id);
            objResponse.returnCode = 200;
            return objResponse.toString();
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            return objResponse.toString();
        }
    }
}
