package com.qupworld.paymentgateway.controllers.threading;

import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.MongoDaoImpl;
import com.qupworld.paymentgateway.models.SQLDao;
import com.qupworld.paymentgateway.models.SQLDaoImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by qup on 23/03/2020.
 */
@SuppressWarnings("unchecked")
public class MigrateTicketAuditThread extends Thread {
    final static Logger logger = LogManager.getLogger(MigrateTicketAuditThread.class);
    public List<String> BOOKIDS;
    public String requestId = "";

    @Override
    public void run() {
        try {
            SQLDao sqlDao = new SQLDaoImpl();
            MongoDao mongoDao = new MongoDaoImpl();
            List<String> listKeys = new ArrayList<>();
            logger.debug(requestId + " - listBookId: " + BOOKIDS);
            String allTickets = sqlDao.getAllTicket(BOOKIDS);
            if (allTickets == null || allTickets.isEmpty()) {
                allTickets = sqlDao.getAllTicket(BOOKIDS);
            }
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser.parse(allTickets);

            JSONArray fields = (JSONArray) jsonObject.get("fields");
            if (listKeys.size() == 0) {
                for (Object field : fields) {
                    JSONObject object = (JSONObject) field;
                    listKeys.add(object.get("name").toString());
                }
            }
            JSONArray records = (JSONArray) jsonObject.get("records");
            MigrateTicketClone clone = new MigrateTicketClone(records, listKeys, sqlDao, mongoDao, requestId);
            clone.run();

            logger.debug(requestId + " - migrate " + records.size() + " ticket has completed!!!!");

        } catch(Exception ex) {
            ex.printStackTrace();
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            logger.debug(requestId + " - MigrateTicketAuditThread exception: "+ errors.toString());
        }

    }

}
