package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.SecurityUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class BoostUtil {

    private final static Logger logger = LogManager.getLogger(BoostUtil.class);
    /*private final static String AUTHORIZE_SANDBOX = "https://stage-wallet.boostorium.com/online/authentication/";
    private final static String AUTHORIZE_PRODUCTION = "https://wallet.boost-my.com/online/authentication/";
    private final static String BASE_SANDBOX = "https://stage-wallet.boostorium.com/api/v1.0";
    private final static String BASE_PRODUCTION = "https://wallet.boost-my.com/api/v1.0";
    private final static String SERVER_SANDBOX = "https://wblite.beta.qup.vn/payment_successful";
    private final static String SERVER_PRODUCTION = "https://weblite.gojo.asia/payment_successful";*/

    private static String merchantId;
    private static String apiKey;
    private static String apiSecret;
    private static String requestId;
    private static String authenURL;
    private static String baseURL;
    private static String serverURL;
    private static String boostPayment;
    private static String boostAck;
    private static String boostRef;
    private static String boostVoid;
    private static String boostRefund;
    private static Gson gson;

    public BoostUtil(String _merchantId, String _apiKey, String _apiSecret,String _requestId,
                     String _baseURL, String _authenURL, String _serverURL,
                     String _boostPayment, String _boostAck, String _boostRef, String _boostVoid, String _boostRefund) {
        merchantId = _merchantId;
        apiKey = _apiKey;
        apiSecret = _apiSecret;
        requestId = _requestId;
        authenURL = _authenURL;
        baseURL = _baseURL;
        serverURL = _serverURL;
        boostPayment = _boostPayment;
        boostAck = _boostAck;
        boostRef = _boostRef;
        boostVoid = _boostVoid;
        boostRefund = _boostRefund;
        gson = new Gson();
    }

    public String getCheckoutURL(String orderId, String phoneNumber, double amount, String fleetName) {
        ObjResponse response = doGetCheckoutURL(orderId, phoneNumber, amount, fleetName);
        if (response.returnCode == 401) {
            // try to generate new token then submit payment again
            KeysUtil.BOOST_API_TOKEN = getApiToken();
            response = doGetCheckoutURL(orderId, phoneNumber, amount, fleetName);
            return response.toString();
        } else { // return code = 200/525
            return response.toString();
        }
    }

    public String voidOrRefund(String onlineRefNum, String boostPaymentRefNum, boolean partial, double amount) {
        ObjResponse response = doVoidOrRefund("void", onlineRefNum, boostPaymentRefNum, partial, amount);
        if (response.returnCode == 401) {
            // try to generate new token then submit payment again
            KeysUtil.BOOST_API_TOKEN = getApiToken();
            response = doVoidOrRefund("void", onlineRefNum, boostPaymentRefNum, partial, amount);
            return response.toString();
        } else if (response.returnCode == 403) { // {"errorCode":"E0016","message":"Error occurred,  Transaction cannot be voided"}
            response = doVoidOrRefund("refund", onlineRefNum, boostPaymentRefNum, partial, amount);
            return response.toString();
        } else { // return code = 200/525
            return response.toString();
        }
    }

    public String completePayment(String onlineRefNum, String boostRefNum, double amount) {
        ObjResponse response = doCompletePayment(onlineRefNum, boostRefNum, amount);
        if (response.returnCode == 401) {
            // try to generate new token then submit payment again
            KeysUtil.BOOST_API_TOKEN = getApiToken();
            response = doCompletePayment(onlineRefNum, boostRefNum, amount);
            return response.toString();
        } else { // return code = 200/525
            return response.toString();
        }
    }

    public String checkOrderStatus(String gatewayId) {
        ObjResponse response = doCheckOrderStatus(gatewayId);
        if (response.returnCode == 401) {
            // try to generate new token then submit payment again
            KeysUtil.BOOST_API_TOKEN = getApiToken();
            response = doCheckOrderStatus(gatewayId);
            return response.toString();
        } else { // return code = 200/525
            return response.toString();
        }
    }

    private ObjResponse doGetCheckoutURL(String orderId, String phoneNumber, double amount, String fleetName) {
        ObjResponse response = new ObjResponse();
        try {
            String URL = baseURL + boostPayment;
            logger.debug(requestId + " - doGetCheckoutURL = " + URL);
            String remark = "booking";
            JSONObject merchantInfo = new JSONObject();
            merchantInfo.put("id", "Merchant");
            merchantInfo.put("merchantName", fleetName);

            //redirectURL = "https://dispatcher.lab.qup.vn/paymentNotificationURL";
            String csString = merchantId + orderId + amount + remark + serverURL;
            //logger.debug(requestId + " - csString = " + csString);
            String checksum = SecurityUtil.encryptThreeDESECB(csString, apiSecret);

            JSONObject body = new JSONObject();
            body.put("merchantId", merchantId);
            body.put("onlineRefNum", orderId);
            body.put("initiatorMobileNo", phoneNumber);
            body.put("amount", amount);
            body.put("remark", remark);
            body.put("redirectURL", serverURL);
            body.put("redirectDeepLink", serverURL);
            body.put("merchantInfo", merchantInfo);
            body.put("checksum", checksum);
            logger.debug(requestId + " - body = " + body.toJSONString());
            HttpResponse<JsonNode> jsonResponse = Unirest.post(URL)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("Authorization", "Bearer " + KeysUtil.BOOST_API_TOKEN)
                    .body(body.toJSONString())
                    .asJson();
            String responseStr = jsonResponse.getBody().toString();
            logger.debug(requestId + " - response = " + responseStr);
            JSONObject objectData = gson.fromJson(responseStr, JSONObject.class);
            String errorCode = objectData.get("errorCode") != null ? objectData.get("errorCode").toString() : "";
            if (errorCode.equals("expire.token") || errorCode.equals("900902")) {
                response.returnCode = 401;
                return response;
            } else {
                String checkoutURI = objectData.get("checkoutURI") != null ? objectData.get("checkoutURI").toString() : "";
                String deepLinkUrl = objectData.get("deepLinkUrl") != null ? objectData.get("deepLinkUrl").toString() : "";
                if (!checkoutURI.isEmpty() && !deepLinkUrl.isEmpty()) {
                    Map<String,Object> mapResponse = new HashMap<>();
                    mapResponse.put("3ds_url", checkoutURI);
                    mapResponse.put("deepLinkUrl", deepLinkUrl);
                    mapResponse.put("type", "link");
                    mapResponse.put("bookId", orderId);
                    mapResponse.put("waitingNotify", true);

                    response.returnCode = 200;
                    response.response = mapResponse;
                    return response;
                } else {
                    response.returnCode = 525;
                    return response;
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - getCheckoutURL exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response;
        }
    }

    private ObjResponse doCompletePayment(String onlineRefNum, String boostRefNum, double amount) {
        ObjResponse response = new ObjResponse();
        try {
            String URL = baseURL + boostAck;
            logger.debug(requestId + " - doCompletePayment = " + URL);
            String csString = merchantId + onlineRefNum + boostRefNum + amount;
            String checksum = SecurityUtil.encryptThreeDESECB(csString, apiSecret);

            JSONObject body = new JSONObject();
            body.put("merchantId", merchantId);
            body.put("onlineRefNum", onlineRefNum);
            body.put("boostRefNumber", boostRefNum);
            body.put("amount", amount);
            body.put("checksum", checksum);
            logger.debug(requestId + " - body = " + body.toJSONString());
            HttpResponse<JsonNode> jsonResponse = Unirest.post(URL)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("Authorization", "Bearer " + KeysUtil.BOOST_API_TOKEN)
                    .body(body.toJSONString())
                    .asJson();
            String responseStr = jsonResponse.getBody().toString();
            logger.debug(requestId + " - response = " + responseStr);
            JSONObject objectData = gson.fromJson(responseStr, JSONObject.class);
            String errorCode = objectData.get("errorCode") != null ? objectData.get("errorCode").toString() : "";
            String transactionStatus = objectData.get("transactionStatus") != null ? objectData.get("transactionStatus").toString() : "";
            if (errorCode.equals("expire.token") || errorCode.equals("900902")) {
                response.returnCode = 401;
                return response;
            } else if (transactionStatus.equals("completed-ack")) {
                String boostRefNumber = objectData.get("boostRefNumber") != null ? objectData.get("boostRefNumber").toString() : "";
                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("boostRefNumber", boostRefNumber);

                response.returnCode = 200;
                response.response = mapResponse;
                return response;
            } else {
                response.returnCode = 525;
                return response;
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - completePayment exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response;
        }
    }

    private ObjResponse doCheckOrderStatus(String orderId) {
        ObjResponse response = new ObjResponse();
        try {
            String URL = baseURL + boostRef + merchantId + "/" + orderId;
            logger.debug(requestId + " - checkOrderStatus = " + URL);
            HttpResponse<JsonNode> jsonResponse = Unirest.get(URL)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("Authorization", "Bearer " + KeysUtil.BOOST_API_TOKEN)
                    .asJson();
            String responseStr = jsonResponse.getBody().toString();
            logger.debug(requestId + " - response = " + responseStr);
            JSONObject objectData = gson.fromJson(responseStr, JSONObject.class);
            String errorCode = objectData.get("errorCode") != null ? objectData.get("errorCode").toString() : "";
            String transactionStatus = objectData.get("transactionStatus") != null ? objectData.get("transactionStatus").toString() : "";
            logger.debug(requestId + " - transactionStatus = " + transactionStatus);
            if (errorCode.equals("expire.token") || errorCode.equals("900902")) {
                response.returnCode = 401;
                return response;
            } else if (transactionStatus.equals("completed")) {
                String boostRefNum = objectData.get("boostRefNum") != null ? objectData.get("boostRefNum").toString() : "";
                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("boostRefNum", boostRefNum);

                response.returnCode = 200;
                response.response = mapResponse;
                return response;
            } else {
                response.returnCode = 525;
                return response;
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - checkOrderStatus exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response;
        }
    }

    private ObjResponse doVoidOrRefund(String type, String onlineRefNum, String boostPaymentRefNum, boolean partial, double amount) {
        ObjResponse response = new ObjResponse();
        try {
            String URL = type.equals("void") ? baseURL + boostVoid : baseURL + boostRefund;
            logger.debug(requestId + " - type = " + type);
            logger.debug(requestId + " - URL = " + URL);
            String remark = "cancel booking #"+onlineRefNum;
            String csString = merchantId + onlineRefNum + boostPaymentRefNum + remark;
            String checksum = SecurityUtil.encryptThreeDESECB(csString, apiSecret);

            JSONObject body = new JSONObject();
            body.put("merchantId", merchantId);
            body.put("onlineRefNum", onlineRefNum);
            body.put("boostPaymentRefNum", boostPaymentRefNum);
            body.put("remark", remark);
            body.put("partial", partial);
            if (partial) {
                body.put("amount", amount);
            }
            body.put("checksum", checksum);
            logger.debug(requestId + " - body = " + body.toJSONString());
            HttpResponse<JsonNode> jsonResponse = Unirest.post(URL)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("Authorization", "Bearer " + KeysUtil.BOOST_API_TOKEN)
                    .body(body.toJSONString())
                    .asJson();
            String responseStr = jsonResponse.getBody().toString();
            logger.debug(requestId + " - response = " + responseStr);
            JSONObject objectData = gson.fromJson(responseStr, JSONObject.class);
            String errorCode = objectData.get("errorCode") != null ? objectData.get("errorCode").toString() : "";
            String transactionStatus = objectData.get("transactionStatus") != null ? objectData.get("transactionStatus").toString() : "";
            if (errorCode.equals("expire.token") || errorCode.equals("900902")) {
                response.returnCode = 401;
                return response;
            } else if (errorCode.equals("E0016")) {
                response.returnCode = 403;
                return response;
            } else if (transactionStatus.equals("completed")) {
                String boostRefNumber = objectData.get("boostRefNum") != null ? objectData.get("boostRefNum").toString() : "";
                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("boostRefNumber", boostRefNumber);

                response.returnCode = 200;
                response.response = mapResponse;
                return response;
            } else {
                response.returnCode = 525;
                return response;
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - doVoidOrRefund exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response;
        }
    }

    /*private boolean checkSumComplete() throws Exception{
        String amount = "3.00";
        String merchantId = "MCM0057116";
        String transactionTime = "2020-09-15T17:12:27";
        String onlineRefNum = "20200915090902682";
        String transactionType = "payment";
        String customerLast4DigitMSISDN = "3846";
        String transactionStatus = "completed";
        String boostRefNum = "2009156194733724";
        String checksum = "Z8V6nrWEoqYpCeIOeKSi9TLXlq3LmIDJGxOJbeqhgi5b8sk91sO2igd1idMd6jpmrTGE9dtOixYl/yyewAYZgMbLeNAsrTe/35GqwBQD4VPeaVX1hPhvrQ==";

        String csString = merchantId + onlineRefNum + transactionType + amount
                + transactionTime + customerLast4DigitMSISDN + transactionStatus +
                boostRefNum;
        String encrypt = SecurityUtil.encryptThreeDESECB(csString, apiSecret);
        System.out.println("match = " + encrypt.equals(checksum));
        return encrypt.equals(checksum);
    }*/

    private String getApiToken() {
        String apiToken = "";
        try {
            JSONObject body = new JSONObject();
            body.put("apiKey", apiKey);
            body.put("apiSecret", apiSecret);
            logger.debug(requestId + " - authenURL: " + authenURL);
            logger.debug(requestId + " - body: " + body.toJSONString());
            HttpResponse<JsonNode> jsonResponse = Unirest.post(authenURL)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .body(body.toJSONString())
                    .asJson();
            String result = jsonResponse.getBody().getObject().toString();
            logger.debug(requestId + " - result: " + result);
            /*
            result = {"apiToken":"d3c45c01-02f1-3f27-95cb-a47e86aaa483","scope":"am_application_scope default","token_type":"Bearer","expires_in":3600}
             */
            JSONObject objResult = gson.fromJson(result, JSONObject.class);
            apiToken = objResult.get("apiToken") != null ? objResult.get("apiToken").toString() : "";
        } catch (Exception ex) {
            logger.debug(requestId + " - getApiToken exception: " + CommonUtils.getError(ex));
        }
        return apiToken;
    }


}
