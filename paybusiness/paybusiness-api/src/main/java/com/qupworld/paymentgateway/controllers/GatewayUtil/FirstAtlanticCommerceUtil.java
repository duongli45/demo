package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.google.gson.Gson;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.Signature;
import com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.IServices;
import com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.Services;
import com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.*;
import com.qupworld.paymentgateway.entities.CreditEnt;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.ValidCreditCard;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class FirstAtlanticCommerceUtil {

    private final static Logger logger = LogManager.getLogger(FirstAtlanticCommerceUtil.class);
    private String requestId;
    private Gson gson;

    public FirstAtlanticCommerceUtil(String _requestId) {
        requestId = _requestId;
        gson = new Gson();
    }

    public String createCreditToken(CreditEnt creditEnt, String merchantId, String acquirerId, String password, String currencyISO, boolean isSandbox) throws IOException {
        ObjResponse createTokenResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        double amount = 1.0; // default authorize value
        try {
            String orderNumber = "QUP" + System.currentTimeMillis();
            String expiredDate = creditEnt.expiredDate.substring(0,2) + creditEnt.expiredDate.substring(5);
            AuthorizeRequest request = new AuthorizeRequest();
            Services service = new Services(isSandbox);
            IServices servicePort = service.getBasicHttpBindingIServices();
            CardDetails cardDetails = new CardDetails();
            cardDetails.setCardCVV2(creditEnt.cvv);
            cardDetails.setCardExpiryDate(expiredDate);
            cardDetails.setCardNumber(creditEnt.cardNumber);
            cardDetails.setIssueNumber("");
            cardDetails.setStartDate("");
            request.setCardDetails(cardDetails);
            TransactionDetails details = new TransactionDetails();
            details.setAcquirerId(acquirerId);
            details.setAmount(getAmount(amount));
            details.setCurrency(getCurrencyNumber(currencyISO));
            details.setCurrencyExponent(2);
            details.setIPAddress("");
            details.setMerchantId(merchantId);
            details.setOrderNumber(orderNumber);
            details.setSignatureMethod("SHA1");
            details.setTransactionCode(128);
            String txSignature = Signature.SignAuthorize(password,
                    details.getMerchantId(),
                    details.getAcquirerId(),
                    details.getOrderNumber(),
                    details.getAmount(),
                    details.getCurrency());
            details.setSignature(txSignature);
            request.setTransactionDetails(details);
            String postalCode = creditEnt.postalCode != null ? creditEnt.postalCode : "";
            if (!postalCode.equals("")) {
                BillingDetails billingDetails = new BillingDetails();
                billingDetails.setBillToZipPostCode(postalCode);
                request.setBillingDetails(billingDetails);
            }
            String cardLog = "XXXXXXXXXXXX" + creditEnt.cardNumber.substring(creditEnt.cardNumber.length() - 4);
            String log = gson.toJson(request).replace(creditEnt.cardNumber, cardLog);
            logger.debug(requestId + " - request: " + log);
            AuthorizeResponse response = servicePort.authorize(request);
            logger.debug(requestId + " - response: " + gson.toJson(response));
            CreditCardTransactionResults results = response.getCreditCardTransactionResults();
            logger.debug(requestId + " - ISO Response Code:" + results.getOriginalResponseCode());
            logger.debug(requestId + " - Response Code:" + results.getResponseCode());
            logger.debug(requestId + " - Reason Code:" + results.getReasonCode());
            if (results.getOriginalResponseCode().equals("00")) {
                // create void transaction to return auth amount
                voidTransaction(orderNumber, merchantId, acquirerId, password, amount, isSandbox);

                String cvvResult = results.getCVV2Result();
                String avsResult = results.getAVSResult();
                logger.debug(requestId + " - cvvResult: " + cvvResult + " - avsResult: " + avsResult);
                int cardResult = getCardResult(cvvResult, avsResult);
                if (cardResult == 200) {
                    mapResponse.put("creditCard", results.getPaddedCardNumber());
                    mapResponse.put("token", results.getTokenizedPAN() + KeysUtil.TEMPKEY + expiredDate);
                    mapResponse.put("cardType", ValidCreditCard.getCardTypeInFormat(creditEnt.cardNumber));

                    createTokenResponse.returnCode = 200;
                    createTokenResponse.response = mapResponse;
                } else {
                    createTokenResponse.returnCode = cardResult;
                }
                return createTokenResponse.toString();
            } else {
                return returnPaymentFailed(merchantId, acquirerId, password, amount, isSandbox,
                        orderNumber, results.getOriginalResponseCode(), results.getReasonCodeDescription());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }
    }

    public String initiateCreditForm(CreditEnt creditEnt, String merchantId, String acquirerId, String password, String currencyISO, boolean isSandbox,
                                     String orderNumber){
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        double amount = 1.0; // default authorize value
        String htmlForm = "";
        try {
            String expiredDate = creditEnt.expiredDate.substring(0,2) + creditEnt.expiredDate.substring(5);
            Authorize3DSRequest request = new Authorize3DSRequest();
            String serverUrl = ServerConfig.dispatch_server;
            if (serverUrl.equals("http://dispatch.local.qup.vn"))
                serverUrl = "http://113.160.232.234:1337";
            String URL = serverUrl + KeysUtil.NOTIFICATION_URL;
            // add orderId to detect gateway
            URL = URL + "?OrderID=" + orderNumber;

            request.setMerchantResponseURL(URL);

            Services service = new Services(isSandbox);
            IServices servicePort = service.getBasicHttpBindingIServices();
            CardDetails cardDetails = new CardDetails();
            cardDetails.setCardCVV2(creditEnt.cvv);
            cardDetails.setCardExpiryDate(expiredDate);
            cardDetails.setCardNumber(creditEnt.cardNumber);
            cardDetails.setIssueNumber("");
            cardDetails.setStartDate("");
            request.setCardDetails(cardDetails);

            TransactionDetails details = new TransactionDetails();
            details.setAcquirerId(acquirerId);
            details.setAmount(getAmount(amount));
            details.setCurrency(getCurrencyNumber(currencyISO));
            details.setCurrencyExponent(2);
            details.setIPAddress("");
            details.setMerchantId(merchantId);
            details.setOrderNumber(orderNumber);
            details.setSignatureMethod("SHA1");
            details.setTransactionCode(128);
            String txSignature = Signature.SignAuthorize(password,
                    details.getMerchantId(),
                    details.getAcquirerId(),
                    details.getOrderNumber(),
                    details.getAmount(),
                    details.getCurrency());
            details.setSignature(txSignature);
            request.setTransactionDetails(details);

            String postalCode = creditEnt.postalCode != null ? creditEnt.postalCode : "";
            if (!postalCode.equals("")) {
                BillingDetails billingDetails = new BillingDetails();
                billingDetails.setBillToZipPostCode(postalCode);
                request.setBillingDetails(billingDetails);
            }
            String cardLog = "XXXXXXXXXXXX" + creditEnt.cardNumber.substring(creditEnt.cardNumber.length() - 4);
            String log = gson.toJson(request).replace(creditEnt.cardNumber, cardLog);
            logger.debug(requestId + " - request: " + log);
            Authorize3DSResponse response = servicePort.authorize3DS(request);
            logger.debug(requestId + " - response: " + gson.toJson(response));
            htmlForm = response.getHTMLFormData();

            objResponse.returnCode = 200;
            mapResponse.put("3ds_url", "html");
            mapResponse.put("type", "form");
            objResponse.response = mapResponse;
        } catch (Exception ex) {
            ex.printStackTrace();

            objResponse.returnCode = 437;
        }

        if (objResponse.returnCode == 200) {
            return objResponse.toString().replace("html", htmlForm.replace("\"", "'").replace("\n","").replace("\r",""));
        } else {
            return objResponse.toString();
        }

    }

    public String returnPaymentFailed(String merchantId, String acquirerId, String password, double amount, boolean isSandbox,
                                      String transactionId, String gwCode, String gwMessage) {
        ObjResponse payTokenResponse = new ObjResponse();
        // payment error, void completed transaction
        if (!transactionId.equals("")) {
            voidTransaction(transactionId, merchantId, acquirerId, password, amount, isSandbox);
        }

        int code = getErrorCode(gwCode);

        // add transaction pay with Stripe
        Map<String, String> mapResponse = new HashMap<>();
        logger.debug("response: " + gwMessage);
        //mapResponse.put("response", message);
        mapResponse.put("message", gwMessage);
        payTokenResponse.returnCode = code;
        payTokenResponse.response = mapResponse;
        return payTokenResponse.toString();
    }

    public String payByToken(String token, double amount, String merchantId, String acquirerId, String password, String currencyISO, boolean isSandbox, String cardType) {
        ObjResponse paymentResponse = new ObjResponse();
        Map<String, String> mapResponse = new HashMap<String, String>();
        try {
            String[] combine = token.split(KeysUtil.TEMPKEY);
            if (combine.length == 1)
                combine = token.split("tempkey");
            String cardToken = token;
            String expiredDate = "1020"; // default
            if (combine.length > 1) {
                cardToken = combine[0];
                expiredDate = combine[1];
            }
            String orderNumber = "QUP" + System.currentTimeMillis();
            AuthorizeRequest request = new AuthorizeRequest();

            Services service = new Services(isSandbox);
            IServices servicePort = service.getBasicHttpBindingIServices();
            CardDetails cardDetails = new CardDetails();
            cardDetails.setCardCVV2("");
            cardDetails.setCardExpiryDate(expiredDate);
            cardDetails.setCardNumber(cardToken);
            cardDetails.setIssueNumber("");
            cardDetails.setStartDate("");
            request.setCardDetails(cardDetails);
            TransactionDetails details = new TransactionDetails();
            details.setAcquirerId(acquirerId);
            details.setAmount(getAmount(amount));
            details.setCurrency(getCurrencyNumber(currencyISO));
            details.setCurrencyExponent(2);
            details.setIPAddress("");
            details.setMerchantId(merchantId);
            details.setOrderNumber(orderNumber);
            details.setSignatureMethod("SHA1");
            details.setTransactionCode(8);
            String txSignature = Signature.SignAuthorize(password,
                    details.getMerchantId(),
                    details.getAcquirerId(),
                    details.getOrderNumber(),
                    details.getAmount(),
                    details.getCurrency());
            details.setSignature(txSignature);
            request.setTransactionDetails(details);
            logger.debug(requestId + " - request: " + gson.toJson(request));
            AuthorizeResponse response = servicePort.authorize(request);
            logger.debug(requestId + " - response: " + gson.toJson(response));
            CreditCardTransactionResults results = response.getCreditCardTransactionResults();
            logger.debug(requestId + " - ISO Response Code:" + results.getOriginalResponseCode());
            logger.debug(requestId + " - Response Code:" + results.getResponseCode());
            logger.debug(requestId + " - Reason Code:" + results.getReasonCode());
            if (results.getOriginalResponseCode().equals("00")) {
                mapResponse.put("message", "Success!");
                mapResponse.put("transId", orderNumber);
                mapResponse.put("cardType", cardType);
                mapResponse.put("authCode", results.getAuthCode());
                mapResponse.put("cardMask", results.getPaddedCardNumber());
                paymentResponse.response = mapResponse;
                paymentResponse.returnCode = 200;
                return paymentResponse.toString();
            } else {
                return returnPaymentFailed(merchantId, acquirerId, password, amount, isSandbox,
                        orderNumber, results.getOriginalResponseCode(), results.getReasonCodeDescription());
            }
        } catch (Exception ex) {
            paymentResponse.returnCode = 437;
            paymentResponse.response = getError(ex);
            return paymentResponse.toString();
        }
    }

    public String payByToken3DS(String token, double amount, String merchantId, String acquirerId, String password, String currencyISO, boolean isSandbox, String cardType) {
        ObjResponse paymentResponse = new ObjResponse();
        Map<String, Object> mapResponse = new HashMap<String, Object>();
        String htmlForm = "";
        try {
            String[] combine = token.split(KeysUtil.TEMPKEY);
            if (combine.length == 1)
                combine = token.split("tempkey");
            String cardToken = token;
            String expiredDate = "1020"; // default
            if (combine.length > 1) {
                cardToken = combine[0];
                expiredDate = combine[1];
            }
            String orderNumber = "QUP" + System.currentTimeMillis();
            Authorize3DSRequest request = new Authorize3DSRequest();
            String serverUrl = ServerConfig.dispatch_server;
            if (serverUrl.equals("http://dispatch.local.qup.vn"))
                serverUrl = "http://113.160.232.234:1337";
            String URL = serverUrl + KeysUtil.NOTIFICATION_URL;
            request.setMerchantResponseURL(URL);

            Services service = new Services(isSandbox);
            IServices servicePort = service.getBasicHttpBindingIServices();
            CardDetails cardDetails = new CardDetails();
            cardDetails.setCardCVV2("");
            cardDetails.setCardExpiryDate(expiredDate);
            cardDetails.setCardNumber(cardToken);
            cardDetails.setIssueNumber("");
            cardDetails.setStartDate("");
            request.setCardDetails(cardDetails);
            TransactionDetails details = new TransactionDetails();
            details.setAcquirerId(acquirerId);
            details.setAmount(getAmount(amount));
            details.setCurrency(getCurrencyNumber(currencyISO));
            details.setCurrencyExponent(2);
            details.setIPAddress("");
            details.setMerchantId(merchantId);
            details.setOrderNumber(orderNumber);
            details.setSignatureMethod("SHA1");
            details.setTransactionCode(8);
            String txSignature = Signature.SignAuthorize(password,
                    details.getMerchantId(),
                    details.getAcquirerId(),
                    details.getOrderNumber(),
                    details.getAmount(),
                    details.getCurrency());
            details.setSignature(txSignature);
            request.setTransactionDetails(details);
            logger.debug(requestId + " - request: " + gson.toJson(request));
            Authorize3DSResponse response = servicePort.authorize3DS(request);
            logger.debug(requestId + " - response: " + gson.toJson(response));
            htmlForm = response.getHTMLFormData();
            htmlForm = htmlForm.replace("\"","'").replace("\n","").replace("\r", "");
            System.out.println(requestId + " - htmlForm = " + htmlForm);

            paymentResponse.returnCode = 528;
            mapResponse.put("3ds_url", htmlForm);
            mapResponse.put("type", "form");
            mapResponse.put("cache", false);
            mapResponse.put("transactionId", orderNumber);
            paymentResponse.response = mapResponse;
        } catch (Exception ex) {
            paymentResponse.returnCode = 437;
            paymentResponse.response = getError(ex);
        }
        return paymentResponse.toString();

    }
    public String paymentWithInputCard(double amount, String currencyISO, String cardNumber, String cvv, String expiredDate,
                                       String merchantId, String acquirerId, String password, boolean isSandbox) {
        ObjResponse paymentResponse = new ObjResponse();
        Map<String, String> mapResponse = new HashMap<String, String>();
        try {
            String orderNumber = "QUP" + System.currentTimeMillis();
            String currencyNumber = getCurrencyNumber(currencyISO);
            String expiredDateStr = expiredDate.substring(0,2) + expiredDate.substring(5);
            AuthorizeRequest request = new AuthorizeRequest();
            Services service = new Services(isSandbox);
            IServices servicePort = service.getBasicHttpBindingIServices();
            CardDetails cardDetails = new CardDetails();
            cardDetails.setCardCVV2(cvv);
            cardDetails.setCardExpiryDate(expiredDateStr);
            cardDetails.setCardNumber(cardNumber);
            cardDetails.setIssueNumber("");
            cardDetails.setStartDate("");
            request.setCardDetails(cardDetails);
            TransactionDetails details = new TransactionDetails();
            details.setAcquirerId(acquirerId);
            details.setAmount(getAmount(amount));
            details.setCurrency(currencyNumber);
            details.setCurrencyExponent(2);
            details.setIPAddress("");
            details.setMerchantId(merchantId);
            details.setOrderNumber(orderNumber);
            details.setSignatureMethod("SHA1");
            details.setTransactionCode(8);
            String txSignature = Signature.SignAuthorize(password,
                    details.getMerchantId(),
                    details.getAcquirerId(),
                    details.getOrderNumber(),
                    details.getAmount(),
                    details.getCurrency());
            details.setSignature(txSignature);
            request.setTransactionDetails(details);
            String cardLog = "XXXXXXXXXXXX" + cardNumber.substring(cardNumber.length() - 4);
            String log = gson.toJson(request).replace(cardNumber, cardLog);
            logger.debug(requestId + " - request: " + log);
            AuthorizeResponse response = servicePort.authorize(request);
            logger.debug(requestId + " - response: " + gson.toJson(response));
            CreditCardTransactionResults results = response.getCreditCardTransactionResults();
            logger.debug(requestId + " - ISO Response Code:" + results.getOriginalResponseCode());
            logger.debug(requestId + " - Response Code:" + results.getResponseCode());
            logger.debug(requestId + " - Reason Code:" + results.getReasonCode());
            if (results.getOriginalResponseCode().equals("00")) {
                String cvvResult = results.getCVV2Result();
                String avsResult = results.getAVSResult();
                logger.debug(requestId + " - cvvResult: " + cvvResult + " - avsResult: " + avsResult);
                int cardResult = getCardResult(cvvResult, avsResult);
                if (cardResult == 200) {
                    mapResponse.put("message", "Success!");
                    mapResponse.put("transId", orderNumber);
                    mapResponse.put("cardType", ValidCreditCard.getCardTypeInFormat(cardNumber));
                    mapResponse.put("authCode", results.getAuthCode());
                    mapResponse.put("cardMask", results.getPaddedCardNumber());
                    paymentResponse.response = mapResponse;
                    paymentResponse.returnCode = 200;
                } else {
                    paymentResponse.returnCode = cardResult;
                }

            } else {
                returnPaymentFailed(merchantId, acquirerId, password, amount, isSandbox,
                        orderNumber, results.getOriginalResponseCode(), results.getReasonCodeDescription());
            }
        } catch (Exception ex) {
            paymentResponse.returnCode = 437;
            paymentResponse.response = getError(ex);
        }
        return paymentResponse.toString();

    }

    public String preAuth(String token, double amount, String merchantId, String acquirerId, String password, String currencyISO, boolean isSandbox) {
        ObjResponse paymentResponse = new ObjResponse();
        Map<String, String> mapResponse = new HashMap<String, String>();
        try {
            String[] combine = token.split(KeysUtil.TEMPKEY);
            if (combine.length == 1)
                combine = token.split("tempkey");
            String expiredDate = "1020"; // default
            String cardToken = token;
            if (combine.length > 1) {
                cardToken = combine[0];
                expiredDate = combine[1];
            }
            String orderNumber = "QUP" + System.currentTimeMillis();
            AuthorizeRequest request = new AuthorizeRequest();
            Services service = new Services(isSandbox);
            IServices servicePort = service.getBasicHttpBindingIServices();
            CardDetails cardDetails = new CardDetails();
            cardDetails.setCardCVV2("");
            cardDetails.setCardExpiryDate(expiredDate);
            cardDetails.setCardNumber(cardToken);
            cardDetails.setIssueNumber("");
            cardDetails.setStartDate("");
            request.setCardDetails(cardDetails);
            TransactionDetails details = new TransactionDetails();
            details.setAcquirerId(acquirerId);
            details.setAmount(getAmount(amount));
            details.setCurrency(getCurrencyNumber(currencyISO));
            details.setCurrencyExponent(2);
            details.setIPAddress("");
            details.setMerchantId(merchantId);
            details.setOrderNumber(orderNumber);
            details.setSignatureMethod("SHA1");
            details.setTransactionCode(0);
            String txSignature = Signature.SignAuthorize(password,
                    details.getMerchantId(),
                    details.getAcquirerId(),
                    details.getOrderNumber(),
                    details.getAmount(),
                    details.getCurrency());
            details.setSignature(txSignature);
            request.setTransactionDetails(details);
            logger.debug(requestId + " - request: " + gson.toJson(request));
            AuthorizeResponse response = servicePort.authorize(request);
            logger.debug(requestId + " - response: " + gson.toJson(response));
            CreditCardTransactionResults results = response.getCreditCardTransactionResults();
            logger.debug(requestId + " - ISO Response Code:" + results.getOriginalResponseCode());
            logger.debug(requestId + " - Response Code:" + results.getResponseCode());
            logger.debug(requestId + " - Reason Code:" + results.getReasonCode());
            if (results.getOriginalResponseCode().equals("00")) {
                mapResponse.put("authId", orderNumber);
                mapResponse.put("authAmount", String.valueOf(amount));
                mapResponse.put("authCode", results.getAuthCode());
                mapResponse.put("authToken", token);
                mapResponse.put("allowCapture", "true");
                paymentResponse.response = mapResponse;
                paymentResponse.returnCode = 200;
            } else {
                returnPaymentFailed(merchantId, acquirerId, password, amount, isSandbox,
                        orderNumber, results.getOriginalResponseCode(), results.getReasonCodeDescription());
            }
        } catch (Exception ex) {
            paymentResponse.returnCode = 437;
            paymentResponse.response = getError(ex);
        }
        return paymentResponse.toString();
    }

    public String capture(String orderNumber, String authCode, double amount, String merchantId, String acquirerId, String password, boolean isSandbox, String cardType) {
        ObjResponse paymentResponse = new ObjResponse();
        Map<String, String> mapResponse = new HashMap<String, String>();
        try {
            TransactionModificationRequest request = new TransactionModificationRequest();
            Services service = new Services(isSandbox);
            IServices servicePort = service.getBasicHttpBindingIServices();
            request.setAcquirerId(acquirerId);
            request.setAmount(getAmount(amount));
            request.setCurrencyExponent(2);
            request.setMerchantId(merchantId);
            request.setModificationType(3);
            request.setOrderNumber(orderNumber);
            request.setPassword(password);
            logger.debug(requestId + " - request: " + gson.toJson(request));
            TransactionModificationResponse response = servicePort.transactionModification(request);
            logger.debug(requestId + " - response: " + gson.toJson(response));
            logger.debug(requestId + " - ISO Response Code:" + response.getOriginalResponseCode());
            logger.debug(requestId + " - Response Code:" + response.getResponseCode());
            logger.debug(requestId + " - Reason Code:" + response.getReasonCode());
            if (response.getResponseCode().equals("1")) {
                mapResponse.put("message", "Success!");
                mapResponse.put("transId", orderNumber);
                mapResponse.put("cardType", cardType);
                mapResponse.put("authCode", authCode);
                mapResponse.put("cardMask", "");
                paymentResponse.response = mapResponse;
                paymentResponse.returnCode = 200;
            } else {
                returnPaymentFailed(merchantId, acquirerId, password, amount, isSandbox,
                        orderNumber, response.getOriginalResponseCode(), response.getReasonCodeDescription());
            }
        } catch (Exception ex) {
            paymentResponse.returnCode = 437;
            paymentResponse.response = getError(ex);
        }
        return paymentResponse.toString();
    }

    public int getCardResult(String cvvCode, String avsCode) {
        int returnCode = 200;
        switch (cvvCode) {
            case "N":
                returnCode = 433;
                break;
            case "P":
                returnCode = 434;
                break;
            case "U":
                returnCode = 435;
                break;
            case "S":
                returnCode = 432;
                break;
        }
        if (returnCode == 200) {
            String validCode = "DMWXYZ";
            String invalidCode = "FG";
            String notMatch = "ANP";
            String notVerified = "BCEIRSU590";
            if (validCode.contains(avsCode))
                returnCode = 200;
            else if (invalidCode.contains(avsCode))
                returnCode = 429;
            else if (notMatch.contains(avsCode))
                returnCode = 430;
            else if (notVerified.contains(avsCode))
                returnCode = 431;
        }

        return returnCode;
    }
    public int getErrorCode(String errorCode){
        int code = 437;
        switch (errorCode) {
            case "01" :
                code = 2043;//Error - Do Not Retry, Call Issuer
                break;
            case "02" :
                code = 2043;//Error - Do Not Retry, Call Issuer
                break;
            case "04" :
                code = 2047;//Call Issuer. Pick Up Card. This error indicates that the card has been reported as lost or stolen by the cardholder.
                break;
            case "05" :
                code = 2000;//Do Not Honor. The customer's bank is unwilling to accept the transaction, need to contact their bank for more details.
                break;
            case "06" :
                code = 437;//An error occurred while processing your card. Try again in a little bit.
                break;
            case "07" :
                code = 2047;//Call Issuer. Pick Up Card. This error indicates that the card has been reported as lost or stolen by the cardholder.
                break;
            case "08" :
                code = 2000;//Do Not Honor. The customer's bank is unwilling to accept the transaction, need to contact their bank for more details.
                break;
            case "33" :
                code = 2004;//Expired Card
                break;
            case "34" :
                code = 2014;//Processor Declined - Fraud Suspected
                break;
            case "36" :
                code = 2057;//Issuer or Cardholder has put a restriction on the card
                break;
            case "40" :
                code = 2023;//Processor Does Not Support This Feature
                break;
            case "41" :
                code = 2012;//Processor Declined - Possible Lost Card
                break;
            case "43" :
                code = 2013;//Processor Declined - Possible Stolen Card
                break;
            case "51" :
                code = 2001;//Insufficient Funds. The account did not have sufficient funds to cover the transaction amount.
                break;
            case "54" :
                code = 2004;//Expired Card
                break;
            case "57" :
                code = 2046;//Declined. The customer's bank is unwilling to accept the transaction, need to contact their bank for more details.
                break;
            case "58" :
                code = 2046;//Declined. The customer's bank is unwilling to accept the transaction, need to contact their bank for more details.
                break;
            case "59" :
                code = 2014;//Processor Declined - Fraud Suspected
                break;
            case "61" :
                code = 4006;//Capture amount exceeded allowable limit
                break;
            case "62" :
                code = 2057;//Issuer or Cardholder has put a restriction on the card
                break;
            case "63" :
                code = 2021;//Security Violation
                break;
            case "65" :
                code = 2003;//Cardholder's Activity Limit Exceeded
                break;
            case "66" :
                code = 2047;//Call Issuer. Pick Up Card. This error indicates that the card has been reported as lost or stolen by the cardholder.
                break;
            case "80" :
                code = 2006;//Invalid Expiration Date
                break;
            case "82" :
                code = 432;//CVV is invalid
                break;
            case "100" :
                code = 436;//This card has been declined by payment provider. Please contact the provider for more information
                break;
            case "101" :
                code = 2004;//Expired Card
                break;
            case "107" :
                code = 2043;//Error - Do Not Retry, Call Issuer
                break;
            case "115" :
                code = 2023;//Processor Does Not Support This Feature
                break;
            case "121" :
                code = 2003;//Cardholder's Activity Limit Exceeded
                break;
            case "200" :
                code = 2047;//Call Issuer. Pick Up Card. This error indicates that the card has been reported as lost or stolen by the cardholder.
                break;
            case "300" :
                code = 200;//Successful
                break;
            case "800" :
                code = 200;//Accepted
                break;
        }

        return  code;
    }
    public void voidTransaction(String transactionId, String merchantId, String acquirerId, String password, double amount, boolean isSandbox) {
        try {
            TransactionModificationRequest request = new TransactionModificationRequest();
            Services service = new Services(isSandbox);
            IServices servicePort = service.getBasicHttpBindingIServices();
            request.setAcquirerId(acquirerId);
            request.setAmount(getAmount(amount));
            request.setCurrencyExponent(2);
            request.setMerchantId(merchantId);
            request.setModificationType(3);
            request.setOrderNumber(transactionId);
            request.setPassword(password);
            logger.debug(requestId + " - request: " + gson.toJson(request));
            TransactionModificationResponse response = servicePort.transactionModification(request);
            logger.debug(requestId + " - response: " + gson.toJson(response));
            logger.debug(requestId + " - ISO Response Code:" + response.getOriginalResponseCode());
            logger.debug(requestId + " - Response Code:" + response.getResponseCode());
            logger.debug(requestId + " - Reason Code:" + response.getReasonCode());
            if (response.getResponseCode().equals("1"))
                logger.debug(requestId + " - transactionId" + transactionId + " has been voided !!!");
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }
    private String getError(Exception ex) {
        return GatewayUtil.getError(ex);
    }

    private String getCurrencyNumber(String currencyISO) {
        String number = "780"; // TTD
        if (currencyISO.equals("USD"))
            number = "840";
        else if (currencyISO.equals("JMD"))
            number = "388";
        return number;
    }

    private String getAmount(double amount) {
        DecimalFormat df = new DecimalFormat("0.00");
        String number = df.format(amount).replace(".", "");
        char[] chars = new char[12 - number.length()];
        Arrays.fill(chars, '0');
        number = new String(chars) + number;
        return number;
    }
}
