package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.google.gson.Gson;
import com.qupworld.paymentgateway.entities.CreditEnt;
import com.pg.util.ObjResponse;
import com.pg.util.ValidCreditCard;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class CredicorpUtil {
    Gson gson = new Gson();
    private String server;
    private String port;
    private String path;
    private String username;
    private String password;
    private String requestId;
    final static Logger logger = LogManager.getLogger(CredicorpUtil.class);

    public CredicorpUtil(String _requestId, String user, String pass, String url, String payUrl) {
        String[] gwURL = url.split(":");
        server = gwURL[0];
        port = gwURL[1];
        path = payUrl;
        username = user;
        password = pass;
        requestId = _requestId;
    }

    public String createCreditToken(CreditEnt creditEnt, String currency) throws IOException {
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            Map<String,Object> result = new HashMap<>();
            Map<String,Object> request = new HashMap<>();
            DecimalFormat form = new DecimalFormat("#.00");
            request.put("amount", form.format(1));
            request.put("currency", currency);
            request.put("type", "auth");
            request.put("ccnumber", creditEnt.cardNumber);
            String[] arrExp = creditEnt.expiredDate.split("/");
            String formattedExp = arrExp[0] + "" + arrExp[1];
            request.put("ccexp", formattedExp);
            request.put("cvv", creditEnt.cvv);
            String data_out = prepareRequest(request);
            String cardLog = "XXXXXXXXXXXX" + creditEnt.cardNumber.substring(creditEnt.cardNumber.length() - 4);
            String log = data_out.replace(creditEnt.cardNumber, cardLog);
            logger.debug(requestId + " - request: " + log);
            Map<String,Object> objectResponse;
            objectResponse = postForm(data_out);
            logger.debug(requestId + " - response: " + gson.toJson(objectResponse));
            if (objectResponse != null && objectResponse.get("response_code") != null) {
                int code = Integer.parseInt(objectResponse.get("response_code").toString());
                if (code == 100) {
                    Map<String, String> mapResponse = new HashMap<>();
                    mapResponse.put("creditCard", "XXXXXXXXXXXX" + creditEnt.cardNumber.substring(creditEnt.cardNumber.length() - 4));
                    mapResponse.put("token", "qup_" + RandomStringUtils.random(4, true, true) + String.valueOf(Calendar.getInstance().getTimeInMillis()));
                    mapResponse.put("cardType", ValidCreditCard.getCardType(creditEnt.cardNumber));
                    createTokenResponse.returnCode = 200;
                    createTokenResponse.response = mapResponse;
                    String transactionid = (String) objectResponse.get("transactionid");
                    this.voidTransaction(transactionid);
                } else {
                    createTokenResponse.returnCode = this.getErrorCode(code);
                    Map<String,String> mapResponse = new HashMap<>();
                    mapResponse.put("message", objectResponse.get("responsetext").toString());
                    mapResponse.put("response", gson.toJson(objectResponse));
                    createTokenResponse.response = mapResponse;

                }
            } else {
                createTokenResponse.returnCode = 437;
                createTokenResponse.response = gson.toJson(objectResponse);
            }

            logger.debug(result);
        } catch (Exception ex) {
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }
        return createTokenResponse.toString();
    }

    public String preAuthPayment(CreditEnt creditEnt, double amount, String currency, String token) {
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            Map<String,Object> result = new HashMap<>();
            Map<String,Object> request = new HashMap<>();
            DecimalFormat form = new DecimalFormat("#.00");
            request.put("amount", form.format(amount));
            request.put("currency", currency);
            request.put("type", "auth");
            request.put("ccnumber", creditEnt.cardNumber);
            String[] arrExp = creditEnt.expiredDate.split("/");
            String formattedExp = arrExp[0] + "" + arrExp[1];
            request.put("ccexp", formattedExp);
            request.put("cvv", creditEnt.cvv);
            String data_out = prepareRequest(request);
            String cardLog = "XXXXXXXXXXXX" + creditEnt.cardNumber.substring(creditEnt.cardNumber.length() - 4);
            String log = data_out.replace(creditEnt.cardNumber, cardLog);
            logger.debug(requestId + " - request: " + log);
            Map<String,Object> objectResponse;
            objectResponse = postForm(data_out);
            logger.debug(requestId + " - response: " + gson.toJson(objectResponse));
            if (objectResponse != null && objectResponse.get("response_code") != null) {
                int code = Integer.parseInt(objectResponse.get("response_code").toString());
                if (code == 100) {
                    Map<String, String> mapResponse = new HashMap<>();
                    String transactionid = (String) objectResponse.get("transactionid");
                    mapResponse.put("authId", transactionid);
                    mapResponse.put("authAmount", String.valueOf(amount));
                    mapResponse.put("authCode", (String) objectResponse.get("authcode"));
                    mapResponse.put("authToken", token);
                    mapResponse.put("allowCapture", "true");
                    createTokenResponse.returnCode = 200;
                    createTokenResponse.response = mapResponse;

                } else {
                    createTokenResponse.returnCode = this.getErrorCode(code);
                    Map<String,String> mapResponse = new HashMap<>();
                    mapResponse.put("message", objectResponse.get("responsetext").toString());
                    mapResponse.put("response", gson.toJson(objectResponse));
                    createTokenResponse.response = mapResponse;

                }
            } else {
                createTokenResponse.returnCode = 437;
                createTokenResponse.response = gson.toJson(objectResponse);
            }
            logger.debug(result);
        } catch (Exception ex) {
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }
        return createTokenResponse.toString();
    }

    public String paymentWithInputCard(double amount, String currency, String number, String securityCode, String expirationDate) {
        ObjResponse paymentResponse = new ObjResponse();
        try {
            Map<String,Object> result = new HashMap<>();
            Map<String,Object> request = new HashMap<>();
            DecimalFormat form = new DecimalFormat("#.00");
            request.put("amount", form.format(amount));
            request.put("currency", currency);
            request.put("type", "sale");
            request.put("ccnumber", number);
            String[] arrExp = expirationDate.split("/");
            String formattedExp = arrExp[0] + "" + arrExp[1];
            request.put("ccexp", formattedExp);
            request.put("cvv", securityCode);
            String data_out = prepareRequest(request);
            String cardLog = "XXXXXXXXXXXX" + number.substring(number.length() - 4);
            String log = data_out.replace(number, cardLog);
            logger.debug(requestId + " - request: " + log);
            Map<String,Object> objectResponse;
            objectResponse = postForm(data_out);
            logger.debug(requestId + " - response: " + gson.toJson(objectResponse));
            if (objectResponse != null && objectResponse.get("response_code") != null) {
                int code = Integer.parseInt(objectResponse.get("response_code").toString());
                if (code == 100) {
                    Map<String, String> mapResponse = new HashMap<String, String>();
                    mapResponse.put("message", "Success!");
                    mapResponse.put("transId", (String) objectResponse.get("transactionid"));
                    mapResponse.put("cardType", ValidCreditCard.getCardType(number));
                    mapResponse.put("authCode", (String) objectResponse.get("authcode"));
                    mapResponse.put("cardMask", "XXXXXXXXXXXX" + number.substring(number.length() - 4));
                    paymentResponse.response = mapResponse;
                    paymentResponse.returnCode = 200;

                } else {
                    paymentResponse.returnCode = this.getErrorCode(code);
                    Map<String,String> mapResponse = new HashMap<>();
                    mapResponse.put("message", objectResponse.get("responsetext").toString());
                    mapResponse.put("response", gson.toJson(objectResponse));
                    paymentResponse.response = mapResponse;

                }
            } else {
                paymentResponse.returnCode = 437;
                paymentResponse.response = gson.toJson(objectResponse);
            }
            logger.debug(result);
        } catch (Exception ex) {
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }
        return paymentResponse.toString();
    }

    public String captureTransaction(double amount, String transactionid) {
        ObjResponse captureTransactionResponse = new ObjResponse();
        try {
            Map<String,Object> result = new HashMap<>();
            Map<String,Object> request = new HashMap<>();
            DecimalFormat form = new DecimalFormat("#.00");
            request.put("amount", form.format(amount));
            request.put("transactionid", transactionid);
            request.put("type", "capture");
            String data_out = prepareRequest(request);
            logger.debug(requestId + " - request: " + data_out);
            Map<String,Object> objectResponse;
            objectResponse = postForm(data_out);
            logger.debug(requestId + " - response: " + gson.toJson(objectResponse));
            if (objectResponse != null && objectResponse.get("response_code") != null) {
                int code = Integer.parseInt(objectResponse.get("response_code").toString());
                if (code == 100) {
                    captureTransactionResponse.returnCode = 200;
                    String authCode = objectResponse.get("authcode") != null ? objectResponse.get("authcode").toString() : "";
                    transactionid = objectResponse.get("transactionid") != null ? objectResponse.get("transactionid").toString() : transactionid;
                    objectResponse.put("message", "Success!");
                    objectResponse.put("transId", transactionid);
                    objectResponse.put("authCode", authCode);
                    captureTransactionResponse.response = objectResponse;
                } else {
                    captureTransactionResponse.returnCode = this.getErrorCode(code);
                    captureTransactionResponse.response = objectResponse.get("error");

                }
            } else {
                captureTransactionResponse.returnCode = 437;
                captureTransactionResponse.response = gson.toJson(objectResponse);
            }
            logger.debug(result);
        } catch (Exception ex) {
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }
        return captureTransactionResponse.toString();

    }

    public String voidTransaction(String transactionid) {
        ObjResponse voidTransactionResponse = new ObjResponse();
        try {
            Map<String,Object> result = new HashMap<>();
            Map<String,Object> request = new HashMap<>();
            DecimalFormat form = new DecimalFormat("#.00");
            request.put("transactionid", transactionid);
            request.put("type", "void");
            String data_out = prepareRequest(request);
            logger.debug(requestId + " - request: " + data_out);
            String error = "";
            String data_in = "";
            boolean success = true;
            Map<String,Object> objectResponse = postForm(data_out);
            logger.debug(requestId + " - response: " + gson.toJson(objectResponse));
            String refundId = "";
            if (objectResponse != null && objectResponse.get("response_code") != null) {
                int code = Integer.parseInt(objectResponse.get("response_code").toString());
                if (code == 100) {
                    refundId = (String) objectResponse.get("transactionid");
                    voidTransactionResponse.returnCode = 200;
                    voidTransactionResponse.response = gson.toJson(objectResponse);
                } else {
                    voidTransactionResponse.returnCode = this.getErrorCode(code);
                    voidTransactionResponse.response = objectResponse.get("error");

                }
            } else {
                voidTransactionResponse.returnCode = 437;
                voidTransactionResponse.response = gson.toJson(objectResponse);
            }

            logger.debug(result);
            logger.debug(requestId + " - transactionId: " + transactionid + " has been voided !!!! RefundId: " + refundId);
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(requestId + " - voidTransaction ERROR: " + getError(ex));
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }
        return voidTransactionResponse.toString();
    }

    public String prepareRequest(Map<String,Object> request) {

        if (request.size() == 0) {
            return "";
        }

        request.put("username", username);
        request.put("password", password);

        Set<String> s = request.keySet();
        Iterator<String> i = s.iterator();
        Object key = i.next();
        StringBuffer buffer = new StringBuffer();
        try {
            buffer.append(key).append("=")
                    .append(URLEncoder.encode((String) request.get(key), StandardCharsets.UTF_8.toString()));


            while (i.hasNext()) {
                key = i.next();
                buffer.append("&").append(key).append("=")
                        .append(URLEncoder.encode((String) request.get(key), StandardCharsets.UTF_8.toString()));
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return buffer.toString();

    }

    private Map<String,Object> postForm(String data) throws Exception {

        Map<String,Object> result = new HashMap<>();

        HttpURLConnection postConn;

        HostnameVerifier hv = new HostnameVerifier() {
            public boolean verify(String urlHostName, SSLSession session) {
                return true;
            }
        };

        HttpsURLConnection.setDefaultHostnameVerifier(hv);

        URL post = new URL("https", server, Integer.parseInt(port), path);
        postConn = (HttpURLConnection) post.openConnection();

        postConn.setRequestMethod("POST");
        postConn.setDoOutput(true);

        PrintWriter out = new PrintWriter(postConn.getOutputStream());
        out.print(data);
        out.close();

        BufferedReader in =
                new BufferedReader(new InputStreamReader(postConn.getInputStream()));

        String inputLine;
        StringBuffer buffer = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            buffer.append(inputLine);
        }
        in.close();


        String response = buffer.toString();

        result.put("response", response);

        // Parse Result
        StringTokenizer st = new StringTokenizer(response, "&");
        while (st.hasMoreTokens()) {
            String varString = st.nextToken();
            StringTokenizer varSt = new StringTokenizer(varString, "=");
            if (varSt.countTokens() > 2 || varSt.countTokens() < 1) {
                throw new Exception("Bad variable from processor center: " + varString);
            }
            if (varSt.countTokens() == 1) {
                result.put(varSt.nextToken(), "");
            } else {
                result.put(varSt.nextToken(), varSt.nextToken());
            }
        }

        if (result.get("response") == "") {
            throw new Exception("Bad response from processor center" + response);
        }

//        if (!result.get("response").toString().equals("1")) {
//            throw new Exception(result.get("responsetext").toString());
//        }

        return result;
    }

    private String getError(Exception ex) {
        return GatewayUtil.getError(ex);
    }

    public int getErrorCode(int code) {
        int returnCode;
        switch (code) {
            case 200://Declined
                returnCode = 436;
                break;
            case 223:
                returnCode = 422;//	Expired card.
                break;
            case 224://Invalid expiration date.
                returnCode = 422;
                break;
            case 225://Invalid card security code.
                returnCode = 432;
                break;
            case 260://Declined
                returnCode = 436;
                break;
            case 261://Declined
                returnCode = 436;
                break;
            case 262://Declined
                returnCode = 436;
                break;
            case 263://Declined
                returnCode = 436;
                break;
            case 264://Declined
                returnCode = 436;
                break;
            case 300://Declined
                returnCode = 436;
                break;
            default:
                returnCode = 437;
                break;

        }
        return returnCode;
    }
}
