package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.paytm.pg.merchant.PaytmChecksum;
import com.qupworld.config.ServerConfig;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by thuanho on 12/05/2022.
 */
public class PayTMUtil {

    private final static Logger logger = LogManager.getLogger(PayTMUtil.class);
    private String requestId;
    private String MERCHANT_ID;
    private String MERCHANT_KEY;
    private String REQUEST_URL;

    public PayTMUtil(String _requestId, String merchantId, String merchantKey, String requestURL) {
        requestId = _requestId;
        MERCHANT_ID = merchantId;
        MERCHANT_KEY = merchantKey;
        REQUEST_URL = requestURL;
    }

    public String getCheckoutURL(double amount, String orderId, String customerId, String environment, boolean walletAppInstalled) throws Exception{
        ObjResponse response = new ObjResponse();
        response.returnCode = 525;
        try {
            logger.debug(requestId + " - orderId: " + orderId);

            JSONObject paytmParams = new JSONObject();
            String callbackUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.PAYTM;
            if (walletAppInstalled) {
                callbackUrl = REQUEST_URL + "/theia/paytmCallback?ORDER_ID="+orderId;
            }
            String websiteName = environment.equals(KeysUtil.SANDBOX) ? "WEBSTAGING" : "DEFAULT";
            JSONObject body = new JSONObject();
            body.put("requestType", "Payment");
            body.put("mid", MERCHANT_ID);
            body.put("websiteName", websiteName);
            body.put("orderId", orderId);
            body.put("callbackUrl", callbackUrl);

            JSONObject txnAmount = new JSONObject();
            txnAmount.put("value", amount);
            txnAmount.put("currency", "INR");

            JSONObject userInfo = new JSONObject();
            userInfo.put("custId", customerId);

            body.put("txnAmount", txnAmount);
            body.put("userInfo", userInfo);

            String checksum = PaytmChecksum.generateSignature(body.toString(), MERCHANT_KEY);
            logger.debug(requestId + " - checksum: " + checksum);
            JSONObject head = new JSONObject();
            head.put("signature", checksum);

            paytmParams.put("body", body);
            paytmParams.put("head", head);

            String post_data = paytmParams.toString();
            logger.debug(requestId + " - post_data: " + post_data);

            String apiURL = REQUEST_URL + "/theia/api/v1/initiateTransaction?mid="+MERCHANT_ID+"&orderId="+orderId;
            logger.debug(requestId + " - url: " + apiURL);
            URL url = new URL(apiURL);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);

            DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
            requestWriter.writeBytes(post_data);
            requestWriter.close();
            String responseData = "";
            InputStream is = connection.getInputStream();
            BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
            if ((responseData = responseReader.readLine()) != null) {
                logger.debug(requestId + " - response: " + responseData);
            }
            responseReader.close();
            JSONObject jsondata = new JSONObject(responseData);
            JSONObject jsonBody = jsondata.getJSONObject("body");
            JSONObject jsonResult = jsonBody.getJSONObject("resultInfo");
            String resultCode = jsonResult.getString("resultCode") != null ? jsonResult.getString("resultCode") : "";
            logger.debug(requestId + " - resultCode: " + resultCode);
            String txnToken = jsonBody.getString("txnToken") != null ? jsonBody.getString("txnToken") : "";
            logger.debug(requestId + " - txnToken: " + txnToken);
            if (resultCode.equals("0000") && !txnToken.isEmpty()) {
                /*String checkoutURL = apiURL + "&txnToken="+txnToken;
                checkoutURL = checkoutURL.replace("initiateTransaction", "showPaymentPage");*/
                String checkoutPage = createHTMLForm(orderId, txnToken);
                response.returnCode = 200;
                Map<String, String> mapResponse = new HashMap<>();
                mapResponse.put("3ds_url", checkoutPage);
                mapResponse.put("type", "form");
                mapResponse.put("transactionId", orderId);
                mapResponse.put("amount", KeysUtil.DECIMAL_FORMAT.format(amount));
                mapResponse.put("orderID", orderId);
                mapResponse.put("txnToken", txnToken);
                mapResponse.put("mid", MERCHANT_ID);
                response.response = mapResponse;
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            if (ex.getMessage().contains("Network is unreachable")) {
                response.returnCode = 3000;
            }
        }

        return response.toString();
    }

    public String getPaymentStatus(String orderId) throws Exception {
        ObjResponse response = new ObjResponse();
        response.returnCode = 525;
        try {
            JSONObject paytmParams = new JSONObject();

            JSONObject body = new JSONObject();
            body.put("mid", MERCHANT_ID);
            body.put("orderId", orderId);

            String checksum = PaytmChecksum.generateSignature(body.toString(), MERCHANT_KEY);
            logger.debug(requestId + " - checksum: " + checksum);
            JSONObject head = new JSONObject();
            head.put("signature", checksum);

            paytmParams.put("body", body);
            paytmParams.put("head", head);

            String post_data = paytmParams.toString();
            logger.debug(requestId + " - post_data: " + post_data);

            String apiURL = REQUEST_URL + "/v3/order/status";
            logger.debug(requestId + " - url: " + apiURL);
            URL url = new URL(apiURL);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);

            DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
            requestWriter.writeBytes(post_data);
            requestWriter.close();
            String responseData = "";
            InputStream is = connection.getInputStream();
            BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
            if ((responseData = responseReader.readLine()) != null) {
                logger.debug(requestId + " - response: " + responseData);
            }
            responseReader.close();
            JSONObject jsonObject = new JSONObject(responseData);
            JSONObject jsonBody = jsonObject.getJSONObject("body");
            JSONObject jsonResult = jsonBody.getJSONObject("resultInfo");
            String resultStatus = jsonResult.getString("resultStatus"); //TXN_SUCCESS, TXN_FAILURE, PENDING, NO_RECORD_FOUND
            logger.debug(requestId + " - resultStatus: " + resultStatus);
            String txnId = jsonBody.get("txnId") != null ? jsonBody.getString("txnId") : "";
            Map<String,String> mapResponse = new HashMap<>();
            mapResponse.put("status", resultStatus.toUpperCase());
            mapResponse.put("transactionId", txnId);
            response.response = mapResponse;
            response.returnCode = 200;
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            if (ex.getMessage().contains("Network is unreachable")) {
                response.returnCode = 3000;
            }
        }
        return response.toString();
    }

    private String createHTMLForm(String orderId, String token){
        String url = "<html>\n" +
                "   <head>\n" +
                "      <title>Show Payment Page</title>\n" +
                "   </head>\n" +
                "   <body>\n" +
                "      <center>\n" +
                "         <h1>Please do not refresh this page...</h1>\n" +
                "      </center>\n" +
                "      <form method=\"post\" action=\"%SERVER/theia/api/v1/showPaymentPage?mid=%MID&orderId=%ORDER_ID\" name=\"paytm\">\n" +
                "         <table border=\"1\">\n" +
                "            <tbody>\n" +
                "               <input type=\"hidden\" name=\"mid\" value=\"%MID\">\n" +
                "               <input type=\"hidden\" name=\"orderId\" value=\"%ORDER_ID\">\n" +
                "               <input type=\"hidden\" name=\"txnToken\" value=\"%TOKEN\">\n" +
                "            </tbody>\n" +
                "         </table>\n" +
                "         <script type=\"text/javascript\"> document.paytm.submit(); </script>\n" +
                "      </form>\n" +
                "   </body>\n" +
                "</html>";
        url = url.replaceAll("%SERVER", REQUEST_URL);
        url = url.replaceAll("%ORDER_ID", orderId);
        url = url.replaceAll("%TOKEN", token);
        url = url.replaceAll("%MID", MERCHANT_ID);
        return url;
    }

    public static void main(String[] args) {
        String requestId = UUID.randomUUID().toString();



        PayTMUtil payTMUtil = new PayTMUtil(requestId, "eFJJXQ98807949134643", "&%g4e#ahUHiP5dat", "https://securegw-stage.paytm.in");
        /*String result = payTMUtil.init();
        logger.debug(requestId + " - result: " + result);*/

        String orderId = "20220524111849775";
        String status = payTMUtil.status(orderId);
        logger.debug(requestId + " - status: " + status);
    }

    public String init() {
        ObjResponse response = new ObjResponse();
        response.returnCode = 525;
        try {
            String orderId = GatewayUtil.getOrderId();
            logger.debug(requestId + " - orderId: " + orderId);

            JSONObject paytmParams = new JSONObject();

            //String returnUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.PAYMAYA+"?urlAction=redirect";
            String callbackUrl = "http://eventapis.local.qup.vn/api/webhook/notification/PayTM?urlAction=callback";
            JSONObject body = new JSONObject();
            body.put("requestType", "Payment");
            body.put("mid", MERCHANT_ID);
            body.put("websiteName", "WEBSTAGING");
            body.put("orderId", orderId);
            body.put("callbackUrl", callbackUrl);

            JSONObject txnAmount = new JSONObject();
            txnAmount.put("value", "1.00");
            txnAmount.put("currency", "INR");

            JSONObject userInfo = new JSONObject();
            userInfo.put("custId", "CUST_001");

            body.put("txnAmount", txnAmount);
            body.put("userInfo", userInfo);

            String checksum = PaytmChecksum.generateSignature(body.toString(), MERCHANT_KEY);
            logger.debug(requestId + " - checksum: " + checksum);
            JSONObject head = new JSONObject();
            head.put("signature", checksum);

            paytmParams.put("body", body);
            paytmParams.put("head", head);

            String post_data = paytmParams.toString();
            logger.debug(requestId + " - post_data: " + post_data);

            String apiURL = REQUEST_URL + "/theia/api/v1/initiateTransaction?mid="+MERCHANT_ID+"&orderId="+orderId;
            logger.debug(requestId + " - url: " + apiURL);
            URL url = new URL(apiURL);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);

            DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
            requestWriter.writeBytes(post_data);
            requestWriter.close();
            String responseData = "";
            InputStream is = connection.getInputStream();
            BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
            if ((responseData = responseReader.readLine()) != null) {
                logger.debug(requestId + " - response: " + responseData);
            }
            responseReader.close();
            JSONObject jsondata = new JSONObject(responseData);
            JSONObject jsonBody = jsondata.getJSONObject("body");
            JSONObject jsonResult = jsonBody.getJSONObject("resultInfo");
            String resultCode = jsonResult.getString("resultCode") != null ? jsonResult.getString("resultCode") : "";
            logger.debug(requestId + " - resultCode: " + resultCode);
            String txnToken = jsonBody.getString("txnToken") != null ? jsonBody.getString("txnToken") : "";
            logger.debug(requestId + " - txnToken: " + txnToken);
            if (resultCode.equals("0000") && !txnToken.isEmpty()) {
                String checkoutURL = apiURL + "&txnToken="+txnToken;
                checkoutURL = checkoutURL.replace("initiateTransaction", "showPaymentPage");
                response.returnCode = 200;
                Map<String, String> mapResponse = new HashMap<>();
                mapResponse.put("3ds_url", checkoutURL);
                mapResponse.put("type", "link");
                mapResponse.put("transactionId", orderId);
                response.response = mapResponse;
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }

        return response.toString();
    }

    public String status(String orderId) {
        ObjResponse response = new ObjResponse();
        response.returnCode = 525;
        try {
            JSONObject paytmParams = new JSONObject();

            JSONObject body = new JSONObject();
            body.put("mid", MERCHANT_ID);
            body.put("orderId", orderId);

            String checksum = PaytmChecksum.generateSignature(body.toString(), MERCHANT_KEY);
            logger.debug(requestId + " - checksum: " + checksum);
            JSONObject head = new JSONObject();
            head.put("signature", checksum);

            paytmParams.put("body", body);
            paytmParams.put("head", head);

            String post_data = paytmParams.toString();
            logger.debug(requestId + " - post_data: " + post_data);

            String apiURL = REQUEST_URL + "/v3/order/status";
            logger.debug(requestId + " - url: " + apiURL);
            URL url = new URL(apiURL);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);

            DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
            requestWriter.writeBytes(post_data);
            requestWriter.close();
            String responseData = "";
            InputStream is = connection.getInputStream();
            BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
            if ((responseData = responseReader.readLine()) != null) {
                logger.debug(requestId + " - response: " + responseData);
            }
            responseReader.close();

            response.returnCode = 200;
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }
        return response.toString();
    }
}
