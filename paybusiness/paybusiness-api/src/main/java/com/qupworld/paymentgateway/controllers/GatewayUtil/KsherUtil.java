package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.qupworld.config.ServerConfig;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
public class KsherUtil {

    final static Logger logger = LogManager.getLogger(KsherUtil.class);

    private String requestId;
    private String TOKEN;
    private String REQUEST_URL;
    private final String API_ENDPOINT = "/api/v1/redirect/orders";

    public KsherUtil(String _requestId, String token, String requestUrl) {
        requestId = _requestId;
        TOKEN = token;
        REQUEST_URL = requestUrl;
    }

    public String getCheckoutURL(double amount, String orderId, String reason) throws Exception{
        ObjResponse response = new ObjResponse();
        try {
            String timestamp = String.valueOf(Calendar.getInstance().getTimeInMillis());
            String redirect_url_fail = ServerConfig.hook_server + "/notification/" + KeysUtil.KSHER + "?urlAction=fail";
            String redirect_url = ServerConfig.hook_server + "/notification/" + KeysUtil.KSHER + "?urlAction=redirect";

            DecimalFormat df = new DecimalFormat("#");
            int intAmount = Integer.parseInt(df.format(amount *100));

            HashMap<String, String> data = new HashMap<>();
            data.put("redirect_url_fail", redirect_url_fail);
            data.put("redirect_url", redirect_url);
            data.put("amount", String.valueOf(intAmount));
            data.put("merchant_order_id", orderId);
            //data.put("mid", mid);
            data.put("timestamp", timestamp);
            data.put("note", reason);

            String signature = _makeSignature(API_ENDPOINT, data);

            JSONObject objBody = new JSONObject();
            objBody.put("signature", signature);
            data.forEach((k,v) -> objBody.put(k,v));
            logger.debug(requestId + " - objBody = " + objBody.toJSONString());

            RequestBody body = RequestBody.create(objBody.toJSONString(), KeysUtil.JSON);
            Request request = new Request.Builder()
                    .addHeader("Content-Type", "application/json")
                    .url(REQUEST_URL + API_ENDPOINT)
                    .post(body)
                    .build();

            OkHttpClient client = new OkHttpClient.Builder().build();
            Response gwResponse = client.newCall(request).execute();
            String responseString = gwResponse.body() != null ? gwResponse.body().string() : "";
            logger.debug(requestId + " - response = " + responseString);
            org.json.JSONObject objResponse = new org.json.JSONObject(responseString);
            String error_code = objResponse.has("error_code") ? objResponse.getString("error_code") : "";
            logger.debug(requestId + " - error_code = " + error_code);
            String error_message = "";
            if (error_code.equals("SUCCESS")) {
                String reference = objResponse.has("reference") ? objResponse.getString("reference") : "";
                String gateway_order_id = objResponse.has("gateway_order_id") ? objResponse.getString("gateway_order_id") : "";
                if (!reference.isEmpty()) {
                    Map<String, Object> mapResponse = new HashMap<>();
                    mapResponse.put("3ds_url", reference);
                    mapResponse.put("type", "link");
                    mapResponse.put("transactionId", gateway_order_id);
                    mapResponse.put("isDeeplink", true);
                    response.response = mapResponse;
                    response.returnCode = 200;
                    return response.toString();
                }
            } else {
                error_message = objResponse.has("error_message") ? objResponse.getString("error_message") : "";
            }
            if (!error_message.isEmpty()) {
                Map<String, Object> mapResponse = new HashMap<>();
                mapResponse.put("message", error_message);
                response.response = mapResponse;
            }
            response.returnCode = 525;
            return response.toString();
        } catch (Exception ex) {
            logger.debug(requestId + " - getCheckoutURL exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response.toString();
        }
    }

    public String getPaymentStatus(String orderId) throws Exception {
        ObjResponse response = new ObjResponse();
        try {
            java.text.SimpleDateFormat timeStampFormat = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
            String timestamp = timeStampFormat.format(new java.util.Date());
            HashMap<String, String> data = new HashMap<>();
            data.put("timestamp", timestamp);
            String sign = _makeSignature(API_ENDPOINT + "/" + orderId, data);

            String url = REQUEST_URL + API_ENDPOINT + "/" + orderId + "?signature="+sign+"&timestamp="+timestamp;
            logger.debug(requestId + " - url = " + url);

            Request request = new Request.Builder()
                    .url(url)
                    .build();

            OkHttpClient client = new OkHttpClient.Builder().build();
            Response gwResponse = client.newCall(request).execute();
            String responseString = gwResponse.body() != null ? gwResponse.body().string() : "";
            logger.debug(requestId + " - response = " + responseString);
            org.json.JSONObject objResponse = new org.json.JSONObject(responseString);
            String status = objResponse.has("status") ? objResponse.getString("status") : "";
            String transactionId = objResponse.has("acquirer_order_id") ? objResponse.getString("acquirer_order_id") : "";
            String error_message = objResponse.has("error_message") ? objResponse.getString("error_message") : "";
            logger.debug(requestId + " - status = " + status);
            Map<String, String> mapResponse = new HashMap<>();
            mapResponse.put("status", status);
            mapResponse.put("transactionId", transactionId);
            mapResponse.put("errorMessage", error_message);
            response.response = mapResponse;
            response.returnCode = 200;
            return response.toString();
        } catch (Exception ex) {
            logger.debug(requestId + " - getPaymentStatus exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response.toString();
        }
    }

    /**
     * making signature from concat url and all pairs of data
     * @param  url api endpoint for example /api/v1/app/orders
     * @param  params a combination of parameters and body payload sorted alphabetically
     * @return a string of signature
     */
    private String _makeSignature(String url, Map<String, String> params) throws Exception {

        // first: sort all text parameters
        String[] keys = params.keySet().toArray(new String[0]);
        Arrays.sort(keys);

        // second: connect all text parameters with key and value
        StringBuilder dataStrB = new StringBuilder();
        dataStrB.append(url);
        for (String key : keys) {
            String value = params.get(key);
            if(value.equals("false"))
            {
                value = "False";
            }

            if(value.equals("true")){
                value = "True";
            }
            dataStrB.append(key).append(value);
        }
        // next : sign the whole request
        byte[] bytes;

        logger.debug(requestId + " - raw data : " + dataStrB);
        bytes = encryptHMACSHA256(dataStrB.toString(), TOKEN);

        // finally : transfer sign result from binary to upper hex string
        return byte2hex(bytes);
    }

    private byte[] encryptHMACSHA256(String data, String secret) throws IOException {
        byte[] bytes;
        try {
            SecretKey secretKey = new SecretKeySpec(secret.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
            Mac mac = Mac.getInstance(secretKey.getAlgorithm());
            mac.init(secretKey);
            bytes = mac.doFinal(data.getBytes(StandardCharsets.UTF_8));
        } catch (GeneralSecurityException gse) {
            throw new IOException(gse.toString());
        }
        return bytes;
    }

    /**
     * Transfer binary array to HEX string.
     */
    private String byte2hex(byte[] bytes) {
        StringBuilder sign = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(bytes[i] & 0xFF);
            if (hex.length() == 1) {
                sign.append("0");
            }
            sign.append(hex.toUpperCase());
        }
        return sign.toString();
    }
}


