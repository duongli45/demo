package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.google.gson.Gson;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.controllers.PaymentUtil;
import com.qupworld.paymentgateway.entities.CreditEnt;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.stripe.Stripe;
import com.stripe.exception.*;
import com.stripe.model.*;
import com.stripe.model.checkout.Session;
import com.stripe.model.checkout.SessionCollection;
import com.stripe.net.RequestOptions;
import com.stripe.param.*;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.json.JSONObject;
import org.mockito.AdditionalMatchers;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

import static org.mockito.Mockito.*;
/**
 * Created by qup on 7/14/16.
 */
@SuppressWarnings("unchecked")
public class StripeUtil {

    private final static Logger logger = LogManager.getLogger(StripeUtil.class);
    private String requestId;
    private Gson gson;
    final static List<String> listAccountTokenCountries = Arrays.asList("FR");

    public StripeUtil(String srequestId) {
        requestId = srequestId;
         gson = new Gson();
    }
    public StripeUtil getInstance(String requestId) {

        if (ServerConfig.enableMock) {
            logger.debug(requestId + " - calling mock....");
            StripeUtil mocked =  mock(StripeUtil.class);

            // pay token
            ObjResponse payTokenResponse = new ObjResponse();
            Map<String, String> mapPayToken = new HashMap<String, String>();
            mapPayToken.put("message", "Success!");
            mapPayToken.put("transId", "mock-Stripe-id");
            mapPayToken.put("cardType", "cardType");
            mapPayToken.put("authCode", "authCode");
            mapPayToken.put("cardMask", "cardMask");
            payTokenResponse.response = mapPayToken;
            payTokenResponse.returnCode = 200;

            /*when(mocked.createPaymentWithCreditToken(any(String.class), any(String.class),
                    any(String.class), eq(18.26), any(String.class), any(String.class)))
                    .thenReturn(payTokenResponse.toString());

            // used for pay token with pre-auth
            ObjResponse payPreAuthResponse = new ObjResponse();
            payPreAuthResponse.returnCode = 437;
            when(mocked.createPaymentWithCreditToken(any(String.class), any(String.class),
                    any(String.class), AdditionalMatchers.leq(10.0), any(String.class), any(String.class)))
                    .thenReturn(payPreAuthResponse.toString());

            when(mocked.createPaymentWithCreditToken(any(String.class), any(String.class),
                    any(String.class), AdditionalMatchers.gt(10.0), any(String.class), any(String.class)))
                    .thenCallRealMethod();*/

            // pre-auth payment
            ObjResponse preAuthResponse = new ObjResponse();
            Map<String, String> mapPreauth = new HashMap<String, String>();
            mapPreauth.put("authId", "mock-Stripe-id");
            mapPreauth.put("authAmount", String.valueOf(0.0));
            mapPreauth.put("authCode", "");
            mapPreauth.put("allowCapture", "true");

            preAuthResponse.response = mapPreauth;
            preAuthResponse.returnCode = 200;
            when(mocked.preAuthPayment(any(String.class), any(String.class), any(String.class), any(String.class), AdditionalMatchers.not(eq(10.00)), any(String.class)))
                    .thenReturn(preAuthResponse.toString());

            when(mocked.preAuthPayment(any(String.class), any(String.class), any(String.class), any(String.class), eq(10.00), any(String.class)))
                    .thenCallRealMethod();

            /*when(mocked.preAuthCapture(any(String.class), any(String.class), any(String.class),
                    any(String.class), any(Double.class), any(String.class), any(String.class)))
                    .thenReturn(payTokenResponse.toString());

            when(mocked.preAuthCapture(any(String.class), any(String.class), any(String.class),
                    any(String.class), any(Double.class), any(String.class), any(String.class)))
                    .thenCallRealMethod();*/
            return mocked;
        }

        return new StripeUtil(requestId);
    }

    public String createCreditToken(CreditEnt credit, String name, String phone, String email, String stripeId, String secretKey, String currencyISO) throws IOException {
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            Stripe.apiKey = secretKey;
            //Stripe.apiVersion = apiVersion;

            Map<String, Object> tokenParams = new HashMap<>();
            Map<String, Object> cardParams = new HashMap<String, Object>();
            cardParams.put("object", "card");
            cardParams.put("number", credit.cardNumber);
            String[] arr = credit.expiredDate.split("/");
            cardParams.put("exp_month", Integer.parseInt(arr[0]));
            cardParams.put("exp_year", Integer.parseInt(arr[1]));
            cardParams.put("cvc", credit.cvv);
            cardParams.put("name", credit.cardHolder);
            cardParams.put("currency", currencyISO);
            if (!isEmptyString(credit.postalCode))
                cardParams.put("address_zip", credit.postalCode);
            tokenParams.put("card", cardParams);
            Stripe.apiKey = secretKey;
            Token token = Token.create(tokenParams);

            Map<String, Object> customerParams = new HashMap<String, Object>();
            customerParams.put("source", token.getId());
            if (!name.isEmpty())
                customerParams.put("name", name);
            if (!phone.isEmpty())
                customerParams.put("phone", phone);
            if (!email.isEmpty())
                customerParams.put("email", email);

            Stripe.apiKey = secretKey;
            Customer customer = Customer.create(customerParams);
            logger.debug(requestId + " - Create new customer with payment method");
            String tokenId = customer.getId();
            tokenId = tokenId + KeysUtil.TEMPKEY + token.getId();
            String cardType = token.getCard().getBrand();
            String cardMasked = "XXXXXXXXXXXX" + token.getCard().getLast4();

            Map<String,String> mapResponse = new HashMap<>();
            mapResponse.put("message", "");
            mapResponse.put("creditCard", cardMasked);
            mapResponse.put("qrCode", "");
            mapResponse.put("token", tokenId);
            mapResponse.put("cardType", cardType);

            createTokenResponse.returnCode = 200;
            createTokenResponse.response = mapResponse;
            return createTokenResponse.toString();
        } catch(CardException ex) {
            ex.printStackTrace();
            return returnCreditCardFailed(ex.getCode(), getError(ex), ex.getMessage());
        } catch(InvalidRequestException ex) {
            ex.printStackTrace();
            return returnPaymentFailed(credit.fleetId, "", secretKey, "", 0, "", 437, getError(ex), ex.getMessage());
        } catch(AuthenticationException ex) {
            ex.printStackTrace();
            return returnPaymentFailed(credit.fleetId, "", secretKey, "", 0, "", 415, getError(ex), ex.getMessage());
        } catch(Exception ex) {
            ex.printStackTrace();
            return returnPaymentFailed(credit.fleetId, "", secretKey, "", 0, "", 437, getError(ex), ex.getMessage());
        }
    }
    public ObjResponse getWebhook (String secretKey){
        ObjResponse synWebhookResponse = new ObjResponse();
        synWebhookResponse.returnCode = 201;
        Map<String, Object> params =
                new HashMap<String, Object>();
        params.put("limit", "30");
        try {
            Stripe.apiKey = secretKey;
            WebhookEndpointCollection wh = WebhookEndpoint.list(params);
            List<WebhookEndpoint> list = wh.getData();
            List<Map<String, Object>> results = new ArrayList<>();
            for (WebhookEndpoint endpoint : list)
            {
                Map<String, Object> result = new HashMap<>();
                result.put("url", endpoint.getUrl());
                result.put("events",endpoint.getEnabledEvents());

                results.add(result);


            }
            synWebhookResponse.response = results;
            synWebhookResponse.returnCode = 200;
            return  synWebhookResponse;

        } catch (StripeException e) {
            e.printStackTrace();
            synWebhookResponse.returnCode = 201;
        }
        return  synWebhookResponse;

    }
    public ObjResponse createWebhookAcount (String secretKey,String url) {
        ObjResponse synWebhookResponse = new ObjResponse();
        try {
            Map<String, Object> paramsUpdate = new HashMap<>();
            paramsUpdate.put("enabled_events",
                    Arrays.asList(
                            "account.updated"));
            paramsUpdate.put("connect", true);
            paramsUpdate.put("url",url);
            paramsUpdate.put("api_version",Stripe.API_VERSION);
            WebhookEndpoint webhookEndpoint = WebhookEndpoint.create(paramsUpdate);
            Map<String, Object> result =
                    new HashMap<String, Object>();

            result.put("url", webhookEndpoint.getUrl());
            result.put("events",webhookEndpoint.getEnabledEvents());
            synWebhookResponse.response = result;
            synWebhookResponse.returnCode = 200;
        }catch (Exception e) {
            e.printStackTrace();
            synWebhookResponse.returnCode = 201;
        }
        return synWebhookResponse;
    }
    public ObjResponse createWebhookPayment (String secretKey,String url) {
        ObjResponse synWebhookResponse = new ObjResponse();
        try {
            Map<String, Object> webhookendpointParams = new HashMap<String, Object>();
            webhookendpointParams.put("url",url);
            webhookendpointParams.put("enabled_events", Arrays.asList("payment_intent.succeeded", "payment_intent.payment_failed","payment_intent.amount_capturable_updated"));
            webhookendpointParams.put("api_version",Stripe.API_VERSION);

            WebhookEndpoint whReturn = WebhookEndpoint.create(webhookendpointParams);
            Map<String, Object> result =
                    new HashMap<String, Object>();
            result.put("url", whReturn.getUrl());
            result.put("events",whReturn.getEnabledEvents());

            synWebhookResponse.returnCode = 200;
            synWebhookResponse.response = result;
        } catch (StripeException e) {
            e.printStackTrace();
            //
            synWebhookResponse.returnCode = 201;
        }
        return  synWebhookResponse;
    }
    public ObjResponse createWebhookConnect(String secretKey,String url) {
        ObjResponse synWebhookResponse = new ObjResponse();
        try {
            Map<String, Object> paramsUpdate = new HashMap<>();
            paramsUpdate.put("enabled_events",
                    Arrays.asList(
                            "capability.updated"));
            paramsUpdate.put("connect", true);
            paramsUpdate.put("url",url);
            paramsUpdate.put("api_version",Stripe.API_VERSION);
            WebhookEndpoint webhookEndpoint = WebhookEndpoint.create(paramsUpdate);
            Map<String, Object> result =
                    new HashMap<String, Object>();

            result.put("url", webhookEndpoint.getUrl());
            result.put("events",webhookEndpoint.getEnabledEvents());
            synWebhookResponse.response = result;
            synWebhookResponse.returnCode = 200;
        } catch (StripeException e) {
            e.printStackTrace();
            //
            synWebhookResponse.returnCode = 201;
        }
        return  synWebhookResponse;
    }
    public ObjResponse updateWebhook (String secretKey){
        ObjResponse synWebhookResponse = new ObjResponse();
        synWebhookResponse.returnCode = 201;
        Map<String, Object> params =
                new HashMap<String, Object>();
        params.put("limit", "30");
        try {
            Stripe.apiKey = secretKey;
            WebhookEndpointCollection wh = WebhookEndpoint.list(params);
            List<WebhookEndpoint> list = wh.getData();
            if(list.size() == 1){
                try {
                    WebhookEndpoint endpoint = list.get(0);

                    Map<String, Object> paramsUpdate = new HashMap<>();
                    paramsUpdate.put("enabled_events",
                            Arrays.asList(
                                    "account.updated"));
                    paramsUpdate.put("connect", true);
                    paramsUpdate.put("url",endpoint.getUrl());
                    paramsUpdate.put("api_version",Stripe.API_VERSION);

                    Stripe.apiKey = secretKey;
                    WebhookEndpoint webhookEndpoint = WebhookEndpoint.create(paramsUpdate);
                    Map<String, Object> result =
                            new HashMap<String, Object>();

                    result.put("url", webhookEndpoint.getUrl());
                    result.put("events",webhookEndpoint.getEnabledEvents());
                    synWebhookResponse.response = result;
                    synWebhookResponse.returnCode = 200;
                    return  synWebhookResponse;

                } catch (StripeException e) {
                    e.printStackTrace();
                    synWebhookResponse.returnCode = 201;
                }

            }


        } catch (StripeException e) {
            e.printStackTrace();
            synWebhookResponse.returnCode = 201;
        }
        return  synWebhookResponse;

    }
    public ObjResponse synWebhook (String secretKey,String url){
        ObjResponse synWebhookResponse = new ObjResponse();
        synWebhookResponse.returnCode = 201;
        Map<String, Object> params =
                new HashMap<String, Object>();
        params.put("limit", "30");
        try {
            Stripe.apiKey = secretKey;
            WebhookEndpointCollection wh = WebhookEndpoint.list(params);
            List<WebhookEndpoint> list = wh.getData();
            for (WebhookEndpoint endpoint : list) {
                endpoint.delete();
            }
            /*if(list.size() == 2){
                return getWebhook(secretKey);
            }
            if(list.size()!=0 && list.size()!=2){
                for (WebhookEndpoint endpoint : list)
                {
                    endpoint.delete();

                }
                list.clear();
            }*/
            ObjResponse createWhAccount = createWebhookAcount(secretKey,url);
            ObjResponse createWhPayment = createWebhookPayment(secretKey,url);
            ObjResponse createWhConnect = createWebhookConnect(secretKey,url);
            if(createWhAccount.returnCode == 200 && createWhPayment.returnCode == 200 && createWhConnect.returnCode == 200){
                return  getWebhook(secretKey);
            }

        }catch (Exception e) {
            e.printStackTrace();
        }

        return  synWebhookResponse;

    }
    public ObjResponse createWebhook (String secretKey,String url){

        return updateWebhook(secretKey);
//        return synWebhook(secretKey,url);

    }
//returnCode
    //=1 : pay succeeded
    //=4 : preauth succeeded

    public ObjResponse webhookStripe (String requestId, Event event){
        ObjResponse webhookStripeResponse = new ObjResponse();
        try {
            // Deserialize the nested object inside the event
            EventDataObjectDeserializer dataObjectDeserializer = event.getDataObjectDeserializer();
            JSONObject jsonObject = new JSONObject(dataObjectDeserializer.getRawJson());

            // Handle the event
            logger.info(requestId + " - eventType : " + event.getType());
            List<String> setupIntentHooks = Arrays.asList("setup_intent.succeeded", "setup_intent.setup_failed", "setup_intent.requires_action", "setup_intent.created", "setup_intent.canceled");
            if(setupIntentHooks.contains(event.getType())) {
                webhookStripeResponse.returnCode = 200;
                return webhookStripeResponse;
            }
            // PaymentIntent paymentIntent = (PaymentIntent) stripeObject; // hay bi loi thieu data
            if(event.getType().equals("account.updated") || event.getType().equals("capability.updated")){
                webhookStripeResponse.returnCode = 5;
                return webhookStripeResponse;
            }
            String paymentIntentId = jsonObject.has("id") ? jsonObject.getString("id") : "";
            logger.info(requestId + " - paymentIntentId " + paymentIntentId);
            switch (event.getType()) {
                case "payment_intent.succeeded":
                    try {
                        webhookStripeResponse.returnCode = 1;
                        webhookStripeResponse.response = paymentIntentId;
                    }catch ( Exception ex){
                        logger.info(ex);
                    }

                    //handlePaymentIntentSucceeded(paymentIntent);
                    break;
                case "payment_intent.payment_failed":
                    logger.info(requestId + " - jsonObject: " + jsonObject);
                    String errCode = "";
                    String errorMessage = "";
                    JSONObject objError = jsonObject.has("last_payment_error") ? jsonObject.getJSONObject("last_payment_error") : null;
                    if (objError != null) {
                        errCode = objError.has("code") ? objError.getString("code") : "";
                        errorMessage = "";
                        if (errCode.equals("card_declined")) {
                            errCode = objError.has("decline_code") ? objError.getString("decline_code") : "";
                            errorMessage = objError.has("message") ? objError.getString("message") : "";
                        }
                    }
                    webhookStripeResponse.returnCode = 2;
                    webhookStripeResponse.response = paymentIntentId;
                    webhookStripeResponse.message = returnCreditCardFailed(errCode,  "",  errorMessage);
                    break;
                case "payment_intent.amount_capturable_updated":
                    logger.info(requestId + " - jsonObject: " + jsonObject);
                    String status = jsonObject.has("status") ? jsonObject.getString("status") : "";
                    if (status.equals("requires_capture")){
                        webhookStripeResponse.returnCode = 4;
                        webhookStripeResponse.response = paymentIntentId;
                    }
                    else {
                        webhookStripeResponse.returnCode = 0;
                    }

                    break;
                default:
                    webhookStripeResponse.returnCode = 0;
                    // Unexpected event type


            }
            return webhookStripeResponse;

        } catch (Exception e) {
            e.printStackTrace();
            webhookStripeResponse.returnCode = 437;

        }
        return webhookStripeResponse;
    }
    public String checkStatusPaymentInter (String secretKey,String paymentInterId){
        Stripe.apiKey = secretKey;
        String status = "";
        try {
            PaymentIntent paymentIntent = PaymentIntent.retrieve(paymentInterId);
            if (paymentIntent.getStatus().equals("succeeded")){
                status = "succeeded";
            }else if (paymentIntent.getStatus().equals("requires_payment_method")){
                if(paymentIntent.getLastPaymentError() != null){
                    status = "failed";
                }

            }


        } catch (StripeException e) {
            e.printStackTrace();
            return "";

        }
        return status;
    }
    public String getClientSecret(String secretKey, String stripeProfile, String name, String phone, String email){
        ObjResponse getClientSecretResponse = new ObjResponse();
        try {
            if (stripeProfile == null || stripeProfile.isEmpty()) {
                Map<String, Object> params = new HashMap<>();
                if (!name.isEmpty())
                    params.put( "name", name);
                if (!phone.isEmpty())
                    params.put( "phone", phone);
                if (!email.isEmpty())
                    params.put( "email", email);

                Stripe.apiKey = secretKey;
                Customer customer = Customer.create(params);
                stripeProfile = customer.getId();
            }
            Map<String, Object> params = new HashMap<>();
            params.put("customer", stripeProfile);
            Stripe.apiKey = secretKey;
            SetupIntent setupIntent = SetupIntent.create(params);
            String clientSecret = setupIntent.getClientSecret();

            Map<String, Object> response = new HashMap<String, Object>();
            response.put("clientSecret", clientSecret);
            getClientSecretResponse.response = response;
            getClientSecretResponse.returnCode = 200;
            return getClientSecretResponse.toString();

        } catch (StripeException e) {
            e.printStackTrace();
            getClientSecretResponse.returnCode = 437;

        }
        return getClientSecretResponse.toString();
    }

    public String getPaymentSheetData(String publicKey, String secretKey, String name, String email){
        ObjResponse getClientSecretResponse = new ObjResponse();
        try {
            CustomerCreateParams paramsCustomer =
                    CustomerCreateParams.builder()
                            .setEmail(email)
                            .setName(name)
                            .build();
            Stripe.apiKey = secretKey;
            Customer customer = Customer.create(paramsCustomer);

            EphemeralKeyCreateParams ephemeralKeyParams =
                    EphemeralKeyCreateParams.builder()
                            .setStripeVersion("2022-11-15")
                            .setCustomer(customer.getId())
                            .build();

            Stripe.apiKey = secretKey;
            EphemeralKey ephemeralKey = EphemeralKey.create(ephemeralKeyParams);


            SetupIntentCreateParams setupIntentParams =
                    SetupIntentCreateParams.builder()
                            .setCustomer(customer.getId())
                            .build();
            Stripe.apiKey = secretKey;
            SetupIntent setupIntent = SetupIntent.create(setupIntentParams);

            Map<String, String> responseData = new HashMap();
            responseData.put("setupIntent", setupIntent.getClientSecret());
            responseData.put("ephemeralKey", ephemeralKey.getSecret());
            responseData.put("customer", customer.getId());
            responseData.put("publishableKey", publicKey);

            getClientSecretResponse.response = responseData;
            getClientSecretResponse.returnCode = 200;
            return getClientSecretResponse.toString();

        } catch (StripeException e) {
            e.printStackTrace();
            getClientSecretResponse.returnCode = 437;

        }
        return getClientSecretResponse.toString();
    }

    public String getPaymentIntent(String bookId, String secretKey, double amount, String currencyISO, boolean manualCapture, boolean returnAmount){
        ObjResponse getClientSecretResponse = new ObjResponse();
        try {
            int intAmount = 0;
            if (isZeroDecimal(currencyISO)) {
                intAmount = (int) amount;
            } else {
                DecimalFormat df = new DecimalFormat("#");
                intAmount = Integer.parseInt(df.format(amount*100));
            }

            logger.debug(requestId + " - intAmount: " + intAmount);
            PaymentIntentCreateParams params;
            if (manualCapture) {
                params = PaymentIntentCreateParams.builder()
                        .setCaptureMethod(PaymentIntentCreateParams.CaptureMethod.MANUAL)
                        .setCurrency(currencyISO)
                        .setAmount((long) intAmount)
                        .setTransferGroup(bookId)
                        .setDescription(bookId)
                        .build();
            } else {
                params = PaymentIntentCreateParams.builder()
                        .setCurrency(currencyISO)
                        .setAmount((long) intAmount)
                        .setTransferGroup(bookId)
                        .setDescription(bookId)
                        .build();
            }
            Stripe.apiKey = secretKey;
            PaymentIntent intent = PaymentIntent.create(params);
            String clientSecret = intent.getClientSecret();

            Map<String, Object> response = new HashMap<String, Object>();
            response.put("clientSecret", clientSecret);
            response.put("paymentIntentId", intent.getId());
            response.put("smallestValue", intAmount);
            if (returnAmount) {
                if (isZeroDecimal(currencyISO)) {
                    response.put("amount", intAmount);
                } else {
                    response.put("amount", Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(amount)));
                }
            }
            getClientSecretResponse.response = response;
            getClientSecretResponse.returnCode = 116;
            return getClientSecretResponse.toString();
        } catch (StripeException e) {
            e.printStackTrace();
            Map<String, Object> response = new HashMap<String, Object>();
            response.put("message", e.getMessage());
            getClientSecretResponse.response = response;
            getClientSecretResponse.returnCode = 525;

        }
        return getClientSecretResponse.toString();
    }
    public ObjResponse cancelPaymentInter (String secretKey,String paymentInterId, String bookId ){
        logger.debug(requestId + " - SCALOG: PAYBYTOKEN 1 CANCEL PAYMENTINTER " + bookId);
        Stripe.apiKey = secretKey;
        ObjResponse getClientSecretResponse = new ObjResponse();
        try {
            Stripe.apiKey = secretKey;
            PaymentIntent intent = PaymentIntent.retrieve(paymentInterId);
            Stripe.apiKey = secretKey;
            intent.cancel();
            getClientSecretResponse.returnCode = 200;
            logger.debug(requestId + " - SCALOG: PAYBYTOKEN 2 CANCEL PAYMENTINTER " + bookId);
            try {
                StripeIntentUtil sUtil = new  StripeIntentUtil();
                sUtil.removeAuthenRequired(bookId);
                sUtil.removeAuthenRequired(paymentInterId);
                logger.debug(requestId + " - SCALOG: PAYBYTOKEN 3 CANCEL PAYMENTINTER " + bookId);
            }catch (Exception ex){

                logger.error(ex);
            }
            return getClientSecretResponse;

        } catch (StripeException e) {
            e.printStackTrace();
            try {
                Stripe.apiKey = secretKey;
                PaymentIntent intent = PaymentIntent.retrieve(paymentInterId);
                getClientSecretResponse.response = intent.getStatus();
                getClientSecretResponse.returnCode = 201;
                logger.debug(requestId + " - SCALOG: PAYBYTOKEN 4 CANCEL PAYMENTINTER " + bookId);
            }catch (StripeException ex){
                e.printStackTrace();
                getClientSecretResponse.returnCode = 203;
                logger.debug(requestId + " - SCALOG: PAYBYTOKEN 5 CANCEL PAYMENTINTER " + bookId);
            }

        }
        return getClientSecretResponse;
    }
    public void refundWithPaymentInterId (String secretKey,String paymentInterId ){
        logger.debug(requestId + " - SCALOG: REFUND 1  PAYMENTINTER ");
        try {
            Stripe.apiKey = secretKey;
            PaymentIntent paymentInter = PaymentIntent.retrieve(paymentInterId);
            String chargeId = paymentInter.getLatestCharge();

            Map<String, Object> params = new HashMap<String,Object>();
            params.put("charge", chargeId);
            Stripe.apiKey = secretKey;
            Refund refund = Refund.create(params);
            refund.getStatus();
            logger.debug(requestId + " - SCALOG: REFUND 2  PAYMENTINTER RESULT "+refund.getId() + ": " + refund.getStatus());
        } catch (StripeException e) {
            e.printStackTrace();
            logger.debug(requestId + " - SCALOG: REFUND 2  PAYMENTINTER RESULT "+e);

        }

    }

    // response is: succeeded or other

    public String refundWithWithAmount (String secretKey,String paymentInterId, double amount, String currencyISO ){
        logger.debug(requestId + " - SCALOG: refundWithWithAmount - paymentInterId: " + paymentInterId);
        try {
            int intAmount = 0;
            if (isZeroDecimal(currencyISO)) {
                intAmount = (int) amount;
            } else {
                DecimalFormat df = new DecimalFormat("#");
                intAmount = Integer.parseInt(df.format(amount *100));
            }

            String chargeId = paymentInterId;

            if(paymentInterId.length() > 3){
                String tmp = paymentInterId.substring(0,3);
                if(tmp.contains("pi_")){
                    Stripe.apiKey = secretKey;
                    PaymentIntent paymentInter = PaymentIntent.retrieve(paymentInterId);
                    chargeId = paymentInter.getLatestCharge();
                }else {
                    chargeId = paymentInterId;
                }
            }

            Map<String, Object> params = new HashMap<String,Object>();
            params.put("charge", chargeId);
            params.put("amount", intAmount);
            Stripe.apiKey = secretKey;
            Refund refund = Refund.create(params);

            logger.debug(requestId + " - SCALOG: refundWithWithAmount  PAYMENTINTER RESULT "+refund.getId() + ": " + refund.getStatus());
            return refund.getStatus();
        } catch (StripeException e) {
            e.printStackTrace();
            logger.debug(requestId + " - SCALOG:refundWithWithAmount  PAYMENTINTER RESULT "+e.toString());

        }
        return "failed";

    }
//    Stripe.apiKey = "sk_test_AoVxG6M8KChp7rO8Jjr4N2uB";
//    // return authorize(fleetId, "1212121", secretKey, "cus_FblatKqx59ViXASCATOKENpm_1F6XSKLtOFpVXgs4WosbBUMW", 40.0, "usd");
//    return capture(fleetId,"123",secretKey,"123",30.9,"pi_1F7x1OKskVqIgacPNFFhLzrH","usd");

    public String createCreditToken(String requestId, boolean sca,String fleetId, String name, String phone, String email, String cardToken, String stripeId, String secretKey) throws IOException {
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            Map<String, Object> customerParams = new HashMap<String, Object>();
            Customer customer;
            String cardType;
            String cardMasked;
            String token;
            PaymentMethod pay ;
            logger.debug(requestId + " - cardToken: " + cardToken);
            logger.debug(requestId + " - sca: " + sca);
            if (sca){
                if (cardToken.contains("seti_")){// for ios err
                    Stripe.apiKey = secretKey;
                    SetupIntent setupIntent =
                            SetupIntent.retrieve(
                                    cardToken
                            );
                    cardToken = setupIntent.getPaymentMethod();
                    logger.debug(requestId + " - cardToken from SetupIntent: " + cardToken);
                }
                Stripe.apiKey = secretKey;
                pay = PaymentMethod.retrieve(cardToken);
                token = pay.getCustomer();
                token = token + KeysUtil.TEMPKEY + cardToken;
                cardType = pay.getCard().getBrand();
                cardMasked = "XXXXXXXXXXXX" + pay.getCard().getLast4();

                try {
                    Stripe.apiKey = secretKey;
                    Customer customerUpdate =
                            Customer.retrieve(pay.getCustomer());
                    if (!name.isEmpty())
                        customerParams.put("name", name);
                    if (!phone.isEmpty())
                        customerParams.put("phone", phone);
                    if (!email.isEmpty())
                        customerParams.put("email", email);

                    Map<String, Object> invoice_settings = new HashMap<>();
                    invoice_settings.put("default_payment_method", cardToken);
                    customerParams.put("invoice_settings", invoice_settings);
                    Stripe.apiKey = secretKey;
                    customerUpdate.update(customerParams);
                }catch (Exception exception){
                    logger.debug(requestId + " - exception: " + CommonUtils.getError(exception));
                }
            } else {
                logger.debug(requestId + " - Not SCA - create new customer");
                customerParams.put("source", cardToken);
                if (!name.isEmpty())
                    customerParams.put("name", name);
                if (!phone.isEmpty())
                    customerParams.put("phone", phone);
                if (!email.isEmpty())
                    customerParams.put("email", email);
                Stripe.apiKey = secretKey;
                customer = Customer.create(customerParams);
                token = customer.getId();
                token = token + KeysUtil.TEMPKEY + cardToken;
                Card card = (Card) customer.getSources().getData().get(0);
                cardType = card.getBrand();
                cardMasked = "XXXXXXXXXXXX" + card.getLast4();
            }
            Map<String,String> mapResponse = new HashMap<>();
            mapResponse.put("message", "");
            mapResponse.put("creditCard", cardMasked);
            mapResponse.put("qrCode", "");
            mapResponse.put("token", token);
            mapResponse.put("cardType", cardType);
            createTokenResponse.returnCode = 200;
            createTokenResponse.response = mapResponse;
            return createTokenResponse.toString();
        } catch(CardException ex) {
            ex.printStackTrace();
            return returnCreditCardFailed(ex.getCode(), getError(ex), ex.getMessage());
        } catch (RateLimitException ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, "", secretKey, "", 0, "", 437, getError(ex), ex.getMessage());
        } catch(InvalidRequestException ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, "", secretKey, "", 0, "", 437, getError(ex), ex.getMessage());
        } catch(AuthenticationException ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, "", secretKey, "", 0, "", 415, getError(ex), ex.getMessage());
        }  catch (StripeException ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, "", secretKey, "", 0, "", 437, getError(ex), ex.getMessage());
        } catch(Exception ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, "", secretKey, "", 0, "", 437, getError(ex), ex.getMessage());
        }
    }

    public String createCreditTokenFromPI(String fleetId, String paymentIntentId, String secretKey) throws IOException {
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            if (!paymentIntentId.isEmpty()) {
                Stripe.apiKey = secretKey;
                PaymentIntent pi = PaymentIntent.retrieve(paymentIntentId);
                String method = pi.getPaymentMethod();
                Stripe.apiKey = secretKey;
                PaymentMethod pay = PaymentMethod.retrieve(method);
                String token = pay.getCustomer();
                token = token + KeysUtil.TEMPKEY + method;
                String cardType = pay.getCard().getBrand();
                String cardMasked = "XXXXXXXXXXXX" + pay.getCard().getLast4();

                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("message", "success");
                mapResponse.put("creditCard", cardMasked);
                mapResponse.put("qrCode", "");
                mapResponse.put("token", token);
                mapResponse.put("cardType", cardType);
                createTokenResponse.returnCode = 200;
                createTokenResponse.response = mapResponse;
                return createTokenResponse.toString();
            } else {
                return returnPaymentFailed(fleetId, "", secretKey, "", 0, "", 437, "paymentIntentId is null", "");
            }
        } catch(CardException ex) {
            ex.printStackTrace();
            return returnCreditCardFailed(ex.getCode(), getError(ex), ex.getMessage());
        } catch (RateLimitException ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, "", secretKey, "", 0, "", 437, getError(ex), ex.getMessage());
        } catch(InvalidRequestException ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, "", secretKey, "", 0, "", 437, getError(ex), ex.getMessage());
        } catch(AuthenticationException ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, "", secretKey, "", 0, "", 415, getError(ex), ex.getMessage());
        }  catch (StripeException ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, "", secretKey, "", 0, "", 437, getError(ex), ex.getMessage());
        } catch(Exception ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, "", secretKey, "", 0, "", 437, getError(ex), ex.getMessage());
        }
    }


    public ObjResponse deleteToken(String secretKey, String token) {
        ObjResponse deleteTokenResponse = new ObjResponse();
        Stripe.apiKey = secretKey;
        // Stripe.apiVersion = "2017-02-14";

        try {
            Customer customer = Customer.retrieve(token);
            customer.delete();
            deleteTokenResponse.returnCode = 200;
        } catch(Exception ex) {
            ex.printStackTrace();
            deleteTokenResponse.returnCode = 406;
            deleteTokenResponse.response = ex;
        }
        return deleteTokenResponse;
    }
    public ObjResponse confirmPreAuth(String bookId, String secretKey){
        ObjResponse confirmPaymentData = new ObjResponse();
        confirmPaymentData.returnCode = 3;
        try {
            StripeIntentUtil sUtil = new  StripeIntentUtil();
            String cacheAuth = sUtil.getAuthenRequired(bookId);
            if (cacheAuth.length()>0){
                Gson gson = new Gson();
                Map<String,Object> map = gson.fromJson(cacheAuth, Map.class);
                if(map.containsKey("paymentInterId")){
                    String payInId = map.get("paymentInterId").toString();
                    try {
                        if (!secretKey.isEmpty())
                            Stripe.apiKey = secretKey;
                        PaymentIntent pay = PaymentIntent.retrieve(payInId);// confirm with stripe
                        String status =  pay.getStatus();
                        if (status.equals("requires_capture")){
                            confirmPaymentData.returnCode = 1;
                            confirmPaymentData.response = pay;

                        }
                        else {
                            confirmPaymentData.returnCode = 0;
                            confirmPaymentData.response = pay;
                        }

                    } catch (StripeException e) {
                        e.printStackTrace();
                        confirmPaymentData.returnCode = 2;

                    }
                }else {
                    confirmPaymentData.returnCode = 3;
                }
            }

        }catch (Exception ex){
            confirmPaymentData.returnCode = 3;
        }
        return confirmPaymentData;
    }
    //  Confirm with Stripe
//    result:
//         returnCode = 0: payment chua succeeded
//         returnCode = 1:payment succeeded
//         returnCode = 2: loi 0 xac dinh, can xem log Stripe
//         returnCode = 3: chua tao  paymentInter
    public ObjResponse confirmPayment(String bookId, String secretKey){
        ObjResponse confirmPaymentData = new ObjResponse();
        confirmPaymentData.returnCode = 3;
        try {
            StripeIntentUtil sUtil = new  StripeIntentUtil();
            String cacheAuth = sUtil.getAuthenRequired(bookId);
            logger.debug(requestId + " - getAuthenRequired - key: " + bookId + " == value: "+ cacheAuth);
            if (cacheAuth.length()>0){
                Gson gson = new Gson();
                Map<String,Object> map = gson.fromJson(cacheAuth, Map.class);
                if(map.containsKey("paymentInterId")){
                    String payInId = map.get("paymentInterId").toString();
                    try {
                        Stripe.apiKey = secretKey;
                        PaymentIntent pay = PaymentIntent.retrieve(payInId);// confirm with stripe
                        pay.getCustomer();
                        String status =  pay.getStatus();
                        if (status.equals("succeeded")){
                            confirmPaymentData.returnCode = 1;
                            confirmPaymentData.response = pay;

                        }
                        else {
                            confirmPaymentData.returnCode = 3;
                            confirmPaymentData.response = pay;
                        }

                    } catch (StripeException e) {
                        e.printStackTrace();
                        confirmPaymentData.returnCode = 2;

                    }
                }else {
                    confirmPaymentData.returnCode = 3;
                }
            }else  confirmPaymentData.returnCode = 3;

        }catch (Exception ex){
            confirmPaymentData.returnCode = 2;
        }
        return confirmPaymentData;
    }
    public String payOneTime (String fleetId, String bookId,
                              String currencyISO, double amount, String token, String secretKey, String payerEmail, String ACHToken){
        ObjResponse payOneTimeResponse = new ObjResponse();
        Stripe.apiKey = secretKey;

        int intAmount = 0;
        if (isZeroDecimal(currencyISO)) {
            intAmount = (int) amount;
        } else {
            DecimalFormat df = new DecimalFormat("#");
            intAmount = Integer.parseInt(df.format(amount *100));
        }
        logger.debug(requestId + " - intAmount: " + intAmount);
        try {
            PaymentIntent paymentIntentOnSec = null;
            // kiem tra trang thai cua PaymentInter
            ObjResponse  confirm = confirmPayment(bookId, secretKey);
            if(confirm.returnCode == 3) {// chua tao paymentInter, khoi tao va pay
                Map<String, Object> paymentintentParams = new HashMap<String, Object>();
                paymentintentParams.put("amount", intAmount);
                paymentintentParams.put("currency", currencyISO);
                if (bookId.contains("TOPUPDRIVER")) {
                    paymentintentParams.put("setup_future_usage", "off_session");
                }
                Stripe.apiKey = secretKey;
                paymentIntentOnSec = PaymentIntent.create(paymentintentParams);
                Map<String, Object> response = new HashMap<String, Object>();
                response.put("clientSecret", paymentIntentOnSec.getClientSecret());
                response.put("bookId", bookId);
                if (bookId.contains("TOPUPDRIVER")) {
                    response.put("type", "topup_one_time");
                } else if (bookId.contains("PRE")){
                    response.put("type", "topup_prepaid_one_time");
                } else {
                    response.put("type", "pay_one_time");
                }
                StripeIntentUtil sUtil = new StripeIntentUtil();
                String dataString = sUtil.getAuthenRequired(bookId);
                if (dataString.length() > 0) {
                    Gson gson = new Gson();
                    Map<String,Object> map = gson.fromJson(dataString, Map.class);
                    map.put("clientSecret", paymentIntentOnSec.getClientSecret());
                    map.put("paymentInterId", paymentIntentOnSec.getId());

                    String json = gson.toJson(response);
                    if (sUtil.addAuthenRequired(paymentIntentOnSec.getId(), json)) {
                        sUtil.addAuthenRequired("queue-"+paymentIntentOnSec.getId(), fleetId);
                        sUtil.updateAuthData(bookId, gson.toJson(map));
                        payOneTimeResponse.returnCode = 113;
                        payOneTimeResponse.response = response;
                        return payOneTimeResponse.toString();
                    }

                }
            }
            if(confirm.returnCode == 1){// da pay thanh cong
                paymentIntentOnSec = (PaymentIntent) confirm.response;
            }
            if(confirm.returnCode == 0){// da pay that bai or loi stripe
                Gson gson = new Gson();
                PaymentIntent  paymentIntentCache = (PaymentIntent) confirm.response;
                Map<String, Object> response = new HashMap<String, Object>();
                response.put("clientSecret", paymentIntentCache.getClientSecret());
                response.put("bookId",bookId);
                payOneTimeResponse.returnCode = 113;
                payOneTimeResponse.response = response;
                return payOneTimeResponse.toString();
            }
            String chargeId = "";
            if( confirm.returnCode == 2){// da pay that bai or loi stripe

                return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 115,"", "Try Again");

            }
            String status = paymentIntentOnSec.getStatus();
            if (!status.equals("succeeded")){
                return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 115, "", "STATUS NOT succeeded ");

            }
            chargeId = paymentIntentOnSec.getLatestCharge();
            Stripe.apiKey = secretKey;
            Charge charge = Charge.retrieve(chargeId);
            String cardMask ;
            String cardType;
            cardMask = "XXXXXXXXXXXX" + charge.getPaymentMethodDetails().getCard().getLast4();
            cardType = charge.getPaymentMethodDetails().getCard().getBrand();

            Charge.Outcome outcome = charge.getOutcome();
            if (outcome != null) {
                String riskLevel = outcome.getRiskLevel() != null ? outcome.getRiskLevel() : "";
                if (riskLevel.equals("elevated")) {
                    voidStripeTransaction(requestId, fleetId, bookId, secretKey, chargeId);
                    return returnCreditCardFailed(riskLevel, "elevated risk level", outcome.getSellerMessage());
                }
            }
            String authorizationCode = "";
            Map<String, String> mapResponse = new HashMap<String, String>();
            mapResponse.put("message", "Success!");
            mapResponse.put("transId", chargeId);
            mapResponse.put("cardType", cardType);
            mapResponse.put("authCode", authorizationCode);
            mapResponse.put("cardMask", cardMask);
            payOneTimeResponse.response = mapResponse;
            payOneTimeResponse.returnCode = 200;
            String result = payOneTimeResponse.toString();
            logger.debug(requestId + " - result: " + result);
            try {
                StripeIntentUtil sUtil = new  StripeIntentUtil();
                sUtil.removeAuthenRequired(bookId);
                sUtil.removeAuthenRequired(paymentIntentOnSec.getId());

            }catch (Exception ex){
                logger.error(ex);
            }
            return result;

        } catch (StripeException e) {
            e.printStackTrace();
        }
        return  payOneTimeResponse.toString();
    }


    public String createPaymentWithCreditToken(String fleetId, String bookId,
                                               String currencyISO, double amount, String token, String secretKey) {
        logger.debug(requestId + " - SCALOG: PAYBYTOKEN 1 " + bookId);
        logger.debug(requestId + " - secretKey: " + secretKey);
        ObjResponse payTokenResponse = new ObjResponse();
        Stripe.apiKey = secretKey;

        String chargeId = "";
        String paymentMethodId = "";
        String stripeProfile = "";
        int intAmount = 0;
        try {
            PaymentIntent paymentIntent = null;
            try {
                if (isZeroDecimal(currencyISO)) {
                    intAmount = (int) amount;
                } else {
                    DecimalFormat df = new DecimalFormat("#");
                    intAmount = Integer.parseInt(df.format(amount*100));
                }
            } catch (Exception ex) {
                logger.debug(requestId + " - parse integer error for amount = " + amount);
                payTokenResponse.returnCode = 438;
                return payTokenResponse.toString();
            }

            logger.debug(requestId + " - intAmount: " + intAmount);
            String cardMask = "";
            String cardType = "";
            Map<String, Object> params = new HashMap<String, Object>();
            if (token != null && token.contains("tok")){
                Map<String, Object> customerParams = new HashMap<String, Object>();
                customerParams.put("description", "paybyToken");
                customerParams.put("source", token);
                Stripe.apiKey = secretKey;
                Customer customer = Customer.create(customerParams);
                logger.debug("4. Customer.create");
                token = customer.getId();
            }
            logger.debug(requestId + " - token: "+ token);
            // get origin token from Stripe
            String[] arrToken = token.split(KeysUtil.TEMPKEY);
            stripeProfile = arrToken[0];
            paymentMethodId = arrToken.length > 1 ? arrToken[1] : "";
            params.put("customer", stripeProfile);
            params.put("type", "card");
            if (!paymentMethodId.isEmpty()) {
                if (paymentMethodId.length() == 4) {
                    Stripe.apiKey = secretKey;
                    PaymentMethodCollection paymentMethodCollection = PaymentMethod.list(params);
                    for (PaymentMethod paymentMethod : paymentMethodCollection.getData()) {
                        if (paymentMethod.getCard().getLast4().equals(paymentMethodId)) {
                            paymentMethodId = paymentMethod.getId();
                            break;
                        }
                    }
                }
            } else {
                Stripe.apiKey = secretKey;
                PaymentMethodCollection paymentMethodCollection = PaymentMethod.list(params);
                PaymentMethod paymentMethod = paymentMethodCollection.getData().get(0);
                paymentMethodId = paymentMethod.getId();
            }
            ObjResponse  confirm = confirmPayment(bookId, secretKey); // confirm with Stripe
            // kiem tra trang thai cua PaymentInter
            if(confirm.returnCode == 3){// chua tao paymentInter, khoi tao va pay
                Stripe.apiKey = secretKey;
                PaymentIntentCreateParams paramsPI =
                        PaymentIntentCreateParams.builder()
                                .setCurrency(currencyISO)
                                .setAmount((long) intAmount)
                                .setPaymentMethod(paymentMethodId)
                                .setCustomer(stripeProfile)
                                .setConfirm(true)
                                .setOffSession(true)
                                .setTransferGroup(bookId)
                                .setDescription(bookId)
                                .build();
                Stripe.apiKey = secretKey;
                paymentIntent = PaymentIntent.create(paramsPI);

            }
            if(confirm.returnCode == 1){// da pay thanh cong
                logger.debug(requestId + " - SCALOG: PAYBYTOKEN 3 COMPLETE " + bookId);
                paymentIntent = (PaymentIntent) confirm.response;

            }
            if(confirm.returnCode == 0){// chua authen thanh cong
                logger.debug(requestId + " - SCALOG: PAYBYTOKEN 4 PAYMENT AGAIN " + bookId);
                Gson gson = new Gson();
                PaymentIntent  paymentIntentCache = (PaymentIntent) confirm.response;
                Map<String, Object> response = new HashMap<String, Object>();
                response.put("clientSecret", paymentIntentCache.getClientSecret());
                response.put("bookId",bookId);
                payTokenResponse.returnCode = 113;
                payTokenResponse.response = response;
                return payTokenResponse.toString();
            }
            if( confirm.returnCode == 2){// da pay that bai or loi stripe

                return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 115,"", "Try Again");

            }
            String status = paymentIntent != null ? paymentIntent.getStatus() : "";
            if (!status.equals("succeeded")){
                return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 115, "", "STATUS NOT succeeded ");

            }
            Stripe.apiKey = secretKey;
            Charge charge = Charge.retrieve(paymentIntent.getLatestCharge());
            if (charge != null) {
                cardMask = "XXXXXXXXXXXX" + charge.getPaymentMethodDetails().getCard().getLast4();
                cardType = charge.getPaymentMethodDetails().getCard().getBrand();
                chargeId = charge.getId();
                Charge.Outcome outcome = charge.getOutcome();
                if (outcome != null) {
                    String riskLevel = outcome.getRiskLevel() != null ? outcome.getRiskLevel() : "";
                    if (riskLevel.equals("elevated")) {
                        voidStripeTransaction(requestId, fleetId, bookId, secretKey, chargeId);
                        return returnCreditCardFailed(riskLevel, "elevated risk level", outcome.getSellerMessage());
                    }
                }
            }

            /*String transferred = "false";
            try {
                // charge success - do transfer
                if (instantPayout && !achToken.isEmpty()) {
                    TransferCreateParams transferParams =
                            TransferCreateParams.builder()
                                    .setAmount((long) (intAmount - intApplicationFee))
                                    .setCurrency(currencyISO)
                                    .setDestination(achToken)
                                    .setTransferGroup(bookId)
                                    .setSourceTransaction(chargeId)
                                    .setDescription("Transfer for booking #"+bookId)
                                    .build();
                    Stripe.apiKey = secretKey;
                    Transfer transfer  = Transfer.create(transferParams);
                    logger.debug(requestId + " - transfer = " + gson.toJson(transfer));
                    transferred = "true";
                }
            } catch (Exception ex) {
                logger.debug(requestId + " - transfer exception: " + gson.toJson(ex));
                transferred = ex.getMessage();
            }*/
            String authorizationCode = "";
            Map<String, String> mapResponse = new HashMap<String, String>();
            mapResponse.put("message", "Success!");
            mapResponse.put("transId", chargeId);
            mapResponse.put("cardType", cardType);
            mapResponse.put("authCode", authorizationCode);
            mapResponse.put("cardMask", cardMask);
            mapResponse.put("instantPayout", "false");
            mapResponse.put("transferred", "");
            payTokenResponse.response = mapResponse;
            payTokenResponse.returnCode = 200;
            String result = payTokenResponse.toString();
            logger.debug(requestId + " - result: " + result);
            try {
                StripeIntentUtil sUtil = new  StripeIntentUtil();
                logger.debug(requestId + " - removeAuthenRequired - key: " + bookId);
                sUtil.removeAuthenRequired(bookId);
                logger.debug(requestId + " - removeAuthenRequired - key: " + paymentIntent.getId());
                sUtil.removeAuthenRequired(paymentIntent.getId());
            }catch (Exception ex){
                logger.error(ex);
            }
            return result;
        } catch(CardException ex) {
            //ex.printStackTrace();
            logger.debug(requestId + " - CardException: " + ex.getCode());
            logger.debug(requestId + " - DeclineCode: " + ex.getDeclineCode());
            // payment error, void completed transaction
            String errorCode = ex.getCode();
            if (errorCode.equals("authentication_required") && paymentMethodId.length() > 0){// Ye cau xac thuc
                try {
                    logger.debug(requestId + " - SCALOG: PAYBYTOKEN 5 AUTHEN " + bookId);
                    Map<String, Object> paymentintentParamsOnSec = new HashMap<String, Object>();
                    paymentintentParamsOnSec.put("amount", intAmount);
                    paymentintentParamsOnSec.put("currency", currencyISO);
                    paymentintentParamsOnSec.put("customer", stripeProfile);
                    paymentintentParamsOnSec.put("payment_method",paymentMethodId);
                    Stripe.apiKey = secretKey;
                    PaymentIntent paymentIntentOnSec = PaymentIntent.create(paymentintentParamsOnSec);
                    Map<String, Object> response = new HashMap<String, Object>();
                    response.put("requestId", requestId);
                    response.put("clientSecret", paymentIntentOnSec.getClientSecret());
                    response.put("bookId",bookId);
                    if (bookId.contains("TOPUPANDPAY")) {
                        response.put("type", "topup_pax_then_pay");
                    } else if (bookId.contains("TOPUPPAX")) {
                        response.put("type", "topup_pax_by_token");
                    } else if (bookId.contains("TOPUPDRIVER")) {
                        response.put("type", "topup_by_token");
                    } else if (bookId.contains("PRE")){
                        response.put("type", "topup_prepaid_by_token");
                    } else {
                        response.put("type", "pay_by_token");
                    }

                    StripeIntentUtil sUtil = new  StripeIntentUtil();
                    if (!bookId.contains("HO")) {
                        String dataString = sUtil.getAuthenRequired(bookId);
                        logger.debug(requestId + " - getAuthenRequired - key: " + bookId + " == value: "+ dataString);
                        if (dataString.length() > 0){
                            Gson gson = new Gson();
                            Map<String, Object> map = gson.fromJson(dataString, Map.class);
                            map.put("clientSecret",paymentIntentOnSec.getClientSecret());
                            map.put("paymentInterId",paymentIntentOnSec.getId());

                            String json = gson.toJson(response);
                            logger.debug(requestId + " - addAuthenRequired - key: " + paymentIntentOnSec.getId() + " == value: "+ json);
                            if (sUtil.addAuthenRequired(paymentIntentOnSec.getId(), json)) {
                                logger.debug(requestId + " - addAuthenRequired - key: " + "queue-"+paymentIntentOnSec.getId() + " == value: "+ fleetId);
                                sUtil.addAuthenRequired("queue-"+paymentIntentOnSec.getId(), fleetId);
                                sUtil.addPaySCAData(requestId,bookId,paymentIntentOnSec.getId(),fleetId);
                                sUtil.updateAuthData(bookId, gson.toJson(map));
                                payTokenResponse.returnCode = 113;
                                payTokenResponse.response = response;
                            }
                        }
                    } else {
                        // collect hydra pending from customer
                        logger.debug(requestId + " -  collect hydra pending from customer");
                        Gson gson = new Gson();
                        Map<String, Object> map = new HashMap<>();
                        map.put("clientSecret",paymentIntentOnSec.getClientSecret());
                        map.put("paymentInterId",paymentIntentOnSec.getId());

                        String json = gson.toJson(response);
                        logger.debug(requestId + " - addAuthenRequired - key: " + paymentIntentOnSec.getId() + " == value: "+ json);
                        if (sUtil.addAuthenRequired(paymentIntentOnSec.getId(), json)) {
                            logger.debug(requestId + " - addAuthenRequired - key: " + "queue-"+paymentIntentOnSec.getId() + " == value: "+ fleetId);
                            sUtil.addAuthenRequired("queue-"+paymentIntentOnSec.getId(), fleetId);
                            sUtil.addPaySCAData(requestId,bookId,paymentIntentOnSec.getId(),fleetId);
                            sUtil.updateAuthData(bookId, gson.toJson(map));
                            payTokenResponse.returnCode = 113;
                            payTokenResponse.response = response;
                        }
                    }
                } catch (StripeException e) {
                    ex.printStackTrace();
                    return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 437, getError(ex), ex.getMessage());

                }
                return payTokenResponse.toString();
            }
            if (!chargeId.equals("")) {
                voidStripeTransaction(requestId, fleetId, bookId, secretKey, chargeId);
            }
            // add transaction pay with Stripe

            return returnCreditCardFailed(ex.getCode(), getError(ex), ex.getMessage());
        } catch(RateLimitException ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 437, getError(ex), ex.getMessage());
        } catch(InvalidRequestException ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 437, getError(ex), ex.getMessage());
        } catch(AuthenticationException ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 415, getError(ex), ex.getMessage());
        }  catch(Exception ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 437, getError(ex), ex.getMessage());
        }

    }

    public String doPaymentWithCardInfo(String fleetId, String bookId, double amount, String currencyISO,
                                        String cardNumber, String expiredDate, String cvv, String cardHolder, String postalCode,
                                        String secretKey, String customerEmail, String ACHToken) throws IOException {
        ObjResponse payTokenResponse = new ObjResponse();
        Stripe.apiKey = secretKey;
        //Stripe.apiVersion = apiVersion;

        String chargeId = "";
        try {
            int intAmount = 0;
            if (isZeroDecimal(currencyISO)) {
                intAmount = (int) amount;
            } else {
                DecimalFormat df = new DecimalFormat("#");
                intAmount = Integer.parseInt(df.format(amount *100));
            }
            logger.debug(requestId + " - intAmount: " + intAmount);

            /// NO NEED TO VERIFY ZIP CODE WHILE MAKING PAYMENT
            Map<String, Object> tokenParams = new HashMap<>();
            Map<String, Object> cardParams = new HashMap<String, Object>();
            cardParams.put("object", "card");
            cardParams.put("number", cardNumber);
            String[] arr = expiredDate.split("/");
            cardParams.put("exp_month", Integer.parseInt(arr[0]));
            cardParams.put("exp_year", Integer.parseInt(arr[1]));
            cardParams.put("cvc", cvv);
            cardParams.put("name", cardHolder);
            cardParams.put("currency", currencyISO);
            tokenParams.put("card", cardParams);
            Token token = Token.create(tokenParams);

            Map<String, Object> chargeParams = new HashMap<String, Object>();
            chargeParams.put("amount", intAmount);
            chargeParams.put("currency", currencyISO);
            chargeParams.put("transfer_group", fleetId);
            if (ACHToken != null && !ACHToken.isEmpty())
                chargeParams.put("on_behalf_of", ACHToken);
            if (!customerEmail.equals(""))
                chargeParams.put("description", "Charge for " + customerEmail);
            chargeParams.put("source", token.getId());

            Charge charge = Charge.create(chargeParams);
            chargeId = charge.getId();
            Card card = (Card) charge.getSource();
            String cardMask = "XXXXXXXXXXXX" + card.getLast4();
            String cardType = card.getBrand();
            String authorizationCode = "";
            String gatewayRejectionReason = "";
            Charge.Outcome outcome = charge.getOutcome();
            if (outcome != null) {
                String riskLevel = outcome.getRiskLevel() != null ? outcome.getRiskLevel() : "";
                if (riskLevel.equals("elevated")) {
                    voidStripeTransaction(requestId, fleetId, bookId, secretKey, chargeId);
                    return returnCreditCardFailed(riskLevel, "elevated risk level", outcome.getSellerMessage());
                }
            }

            Map<String, String> mapResponse = new HashMap<String, String>();
            mapResponse.put("message", "Success!");
            mapResponse.put("transId", chargeId);
            mapResponse.put("cardType", cardType);
            mapResponse.put("authCode", authorizationCode);
            mapResponse.put("cardMask", cardMask);
            payTokenResponse.response = mapResponse;
            payTokenResponse.returnCode = 200;

            String result = payTokenResponse.toString();
            logger.debug(requestId + " - result: " + result);
            return result;
        } catch(CardException ex) {
            //ex.printStackTrace();
            logger.debug(requestId + " - CardException: " + ex.getCode());
            logger.debug(requestId + " - DeclineCode: " + ex.getDeclineCode());
            // payment error, void completed transaction
            if (!chargeId.equals("")) {
                voidStripeTransaction(requestId, fleetId, bookId, secretKey, chargeId);
            }
            // add transaction pay with Stripe

            return returnCreditCardFailed(ex.getCode(), getError(ex), ex.getMessage());
        } catch(RateLimitException ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, "input card", 437, getError(ex), ex.getMessage());
        } catch(InvalidRequestException ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, "input card", 437, getError(ex), ex.getMessage());
        } catch(AuthenticationException ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, "input card", 415, getError(ex), ex.getMessage());
        }  catch(Exception ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, "input card", 437, getError(ex), ex.getMessage());
        }
    }

    public String returnPaymentFailed(String fleetId, String bookId, String secretKey
            , String chargeId, double amount, String token, int code, String message, String gwMessage) {
        ObjResponse payTokenResponse = new ObjResponse();
        // payment error, void completed transaction
        if (!chargeId.equals("")) {
            voidStripeTransaction(requestId, fleetId, bookId, secretKey, chargeId);
        }

        // add transaction pay with Stripe
        Map<String, String> mapResponse = new HashMap<>();
        logger.debug(requestId + " - message: " + message);
        logger.debug(requestId + " - response: " + gwMessage);
        //mapResponse.put("response", message);
        mapResponse.put("message", gwMessage);
        payTokenResponse.returnCode = code;
        payTokenResponse.response = mapResponse;
        return payTokenResponse.toString();
    }

    public String voidStripeTransaction(String requestId, String fleetId, String bookId, String secretKey, String chargeId) {
        ObjResponse voidResponse = new ObjResponse();
        logger.debug(requestId + " - voidStripeTransaction for bookId " + bookId + " ...");
        logger.debug(requestId + " - chargeId: " + chargeId);
        logger.debug(requestId + " - secretKey: " + secretKey);
        try {
            String refundId = "";
            // Stripe.apiVersion = apiVersion;
            PaymentIntent intent = null;
            if (!chargeId.equals("")) {
                try {
                    Stripe.apiKey = secretKey;
                    Charge charge = Charge.retrieve(chargeId);
                    Stripe.apiKey = secretKey;
                    intent = PaymentIntent.retrieve(charge.getPaymentIntent());
                    Stripe.apiKey = secretKey;
                    intent.cancel();
                }catch (Exception ex){
                    logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
                    Map<String, Object> params = new HashMap<String,Object>();
                    params.put("charge", chargeId);
                    Stripe.apiKey = secretKey;
                    Refund refund = Refund.create(params);
                    refundId = refund.getId();
                }
                if (intent == null){
                    Map<String, Object> params = new HashMap<String,Object>();
                    params.put("charge", chargeId);
                    Stripe.apiKey = secretKey;
                    Refund refund = Refund.create(params);
                    refundId = refund.getId();
                }
                logger.debug(requestId + " - chargeId: " + chargeId + " has been voided !!!! RefundId: " + refundId);
                Map<String, String> mapResponse = new HashMap<String, String>();
                mapResponse.put("refundId", refundId);
                voidResponse.response = mapResponse;
                voidResponse.returnCode = 200;
            } else {
                voidResponse.returnCode = 406;
            }
            return voidResponse.toString();
        } catch(Exception ex) {
            logger.debug(requestId + " - voidStripeTransaction ERROR: " + CommonUtils.getError(ex));
            chargeId = ""; // refund error - do not try to refund again
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, 0.0, "input card", 437, getError(ex), ex.getMessage());
        }
    }

    public String voidStripeTransaction(String requestId, String fleetId, String bookId, String currencyISO, double amount, String secretKey, String chargeId) {
        logger.debug(requestId + " - voidStripeTransaction for bookId " + bookId + " ...");
        logger.debug(requestId + " - chargeId : " + chargeId);
        logger.debug(requestId + " - secretKey: " + secretKey);
        ObjResponse voidResponse = new ObjResponse();
        try {
            String refundId = "";
            //Stripe.apiVersion = apiVersion;
            int intAmount = 0;
            if (isZeroDecimal(currencyISO)) {
                intAmount = (int) amount;
            } else {
                DecimalFormat df = new DecimalFormat("#");
                intAmount = Integer.parseInt(df.format(amount *100));
            }
            logger.debug(requestId + " - intAmount: " + intAmount);
            if (!chargeId.equals("")) {
                Map<String, Object> params = new HashMap<String,Object>();
                params.put("charge", chargeId);
                params.put("amount", intAmount);
                Stripe.apiKey = secretKey;
                Refund refund = Refund.create(params);
                refundId = refund.getId();

                logger.debug(requestId + " - chargeId: " + chargeId + " has been voided !!!! RefundId: " + refundId);

                Map<String, String> mapResponse = new HashMap<String, String>();
                mapResponse.put("refundId", refundId);
                voidResponse.response = mapResponse;
                voidResponse.returnCode = 200;
            } else {
                voidResponse.returnCode = 406;
            }
            return voidResponse.toString();
        } catch(Exception ex) {
            logger.debug(requestId + " - voidStripeTransaction ERROR: " + CommonUtils.getError(ex));
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, "input card", 437, getError(ex), ex.getMessage());
        }
    }
    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }

    private String returnCreditCardFailed(String code, String message, String gwMessage) {
        code = code != null ? code.trim(): "";
        logger.debug(requestId + " - error code: " + code);
        logger.debug(requestId + " - error message: " + message);
        logger.debug(requestId + " - gateway message: " + gwMessage);
        ObjResponse payTokenResponse = new ObjResponse();

        if (message.contains("Your card does not support this type of purchase") || gwMessage.contains("Your card does not support this type of purchase.")) {
            payTokenResponse.returnCode = 551;
            return  payTokenResponse.toString();
        }

        int errorCode = 0;
        switch (code) {
            case "invalid_number" :
                errorCode = 421;
                break;
            case "invalid_expiry_month" :
                errorCode = 422;
                break;
            case "invalid_expiry_year" :
                errorCode = 422;
                break;
            case "invalid_cvc" :
                errorCode = 432;
                break;
            case "incorrect_number" :
                errorCode = 421;
                break;
            case "expired_card" :
                errorCode = 2004;
                break;
            case "incorrect_cvc" :
                errorCode = 433;
                break;
            case "incorrect_zip" :
                errorCode = 430;
                break;
            case "card_declined" :
                errorCode = 436;
                break;
            case "missing" :
                errorCode = 437;
                break;
            case "processing_error" :
                errorCode = 437;
                break;
            case "rate_limit" :
                errorCode = 432;
                break;
            case "elevated" :
                errorCode = 436;
                break;
            case "payment_intent_authentication_failure" :
                errorCode = 911;
                break;
            case "insufficient_funds":
                errorCode = 437;
                break;
            default:
                errorCode = 437;
                break;
        }
        Map<String, String> mapResponse = new HashMap<>();
        /*logger.debug(requestId + " - response: " + message);*/
        //mapResponse.put("response", message);
        if (errorCode == 436) gwMessage = "Your card was declined.";
        mapResponse.put("message", gwMessage);
        payTokenResponse.returnCode = errorCode;
        payTokenResponse.response = mapResponse;
        return  payTokenResponse.toString();
    }

    public String preAuthPayment (String fleetId, String bookId, String secretKey,
                                  String token, double amount, String currencyISO) {
        logger.debug(requestId + " - SCALOG: PREAUTH 1 " + bookId);
        String key = token+"_"+amount;
        Stripe.apiKey = secretKey;
        ObjResponse preAuthResponse = new ObjResponse();
        String chargeId = "";
        try {
            int intAmount;
            if (isZeroDecimal(currencyISO)) {
                intAmount = (int) amount;
            } else {
                DecimalFormat df = new DecimalFormat("#");
                intAmount = Integer.parseInt(df.format(amount*100));
            }
            // get origin token from Stripe
            String[] arrToken = token.split(KeysUtil.TEMPKEY);
            String stripeProfile = arrToken[0];
            String paymentMethodId = arrToken.length > 1 ? arrToken[1] : "";
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("customer", stripeProfile);
            params.put("type", "card");
            if (!paymentMethodId.isEmpty()) {
                if (paymentMethodId.length() == 4) {
                    Stripe.apiKey = secretKey;
                    PaymentMethodCollection paymentMethodCollection = PaymentMethod.list(params);
                    for (PaymentMethod paymentMethod : paymentMethodCollection.getData()) {
                        if (paymentMethod.getCard().getLast4().equals(paymentMethodId)) {
                            paymentMethodId = paymentMethod.getId();
                            break;
                        }
                    }
                }
            } else {
                Stripe.apiKey = secretKey;
                PaymentMethodCollection paymentMethodCollection = PaymentMethod.list(params);
                PaymentMethod paymentMethod = paymentMethodCollection.getData().get(0);
                paymentMethodId = paymentMethod.getId();
            }
            ObjResponse  confirm = confirmPreAuth(bookId, secretKey); // Confirm with Stripe
            PaymentIntent paymentIntent = null;
            if(confirm.returnCode == 3){
                logger.debug(requestId + " - SCALOG: PREAUTH 2 NEW PAYMENTINTER " + bookId);
                Map<String, Object> paymentintentParams = new HashMap<String, Object>();
                paymentintentParams.put("amount", intAmount);
                paymentintentParams.put("currency", currencyISO);
                List<String> payment_method_types = new ArrayList<>();
                payment_method_types.add("card");
                paymentintentParams.put("payment_method_types", payment_method_types);
                paymentintentParams.put("customer",stripeProfile);
                paymentintentParams.put("payment_method",paymentMethodId);
                paymentintentParams.put("capture_method", "manual");
                paymentintentParams.put("confirm", true);
                paymentintentParams.put("transfer_group", bookId);
                paymentintentParams.put("description", bookId);
                Stripe.apiKey = secretKey;
                paymentIntent =  PaymentIntent.create(paymentintentParams);
                if (paymentIntent.getStatus().equals("requires_action")){
                    logger.debug(requestId + " - SCALOG: PREAUTH 3 NEW REQUIRE AUTHEN "+ bookId);
                    Map<String, Object> mapAuth = new HashMap<String, Object>();
                    mapAuth.put("clientSecret",paymentIntent.getClientSecret());
                    mapAuth.put("bookId",bookId);
                    StripeIntentUtil sUtil = new  StripeIntentUtil();
                    String dataString = sUtil.getAuthenRequired(bookId);
                    if (dataString.length() > 0){
                        Gson gson = new Gson();
                        Map<String, Object> map = gson.fromJson(dataString, Map.class);
                        map.put("paymentInterId",paymentIntent.getId());
                        map.put("clientSecret",paymentIntent.getClientSecret());

                        if (sUtil.updateAuthData(bookId, gson.toJson(map))) {
                            mapAuth.put("type","pre_auth");
                            sUtil.addAuthenRequired(paymentIntent.getId(),gson.toJson(mapAuth));
                            sUtil.addAuthenRequired("queue-"+paymentIntent.getId(), fleetId);
                            ObjResponse authResponse = new ObjResponse();
                            authResponse.returnCode = 113;
                            authResponse.response = mapAuth;
                            sUtil.removeAuthenRequired(key);
                            return authResponse.toString();
                        }
                    } else {
                        Map<String, Object> map = new HashMap<>();
                        map.put("paymentInterId",paymentIntent.getId());
                        map.put("clientSecret",paymentIntent.getClientSecret());
                        Gson gson = new Gson();
                        if (sUtil.updateAuthData(bookId, gson.toJson(map))) {
                            mapAuth.put("type","extra_auth");
                            sUtil.addAuthenRequired(paymentIntent.getId(),gson.toJson(mapAuth));
                            sUtil.addAuthenRequired("queue-"+paymentIntent.getId(), fleetId);
                            ObjResponse authResponse = new ObjResponse();
                            authResponse.returnCode = 113;
                            authResponse.response = mapAuth;
                            sUtil.removeAuthenRequired(key);
                            return authResponse.toString();
                        }
                    }

                }
                chargeId = paymentIntent.getLatestCharge();
                Stripe.apiKey = secretKey;
                Charge charge = Charge.retrieve(chargeId);
                Charge.Outcome outcome = charge.getOutcome();
                if (outcome != null) {
                    String riskLevel = outcome.getRiskLevel() != null ? outcome.getRiskLevel() : "";
                    if (riskLevel.equals("elevated")) {
                        voidStripeTransaction(requestId, fleetId, bookId, secretKey, chargeId);
                        return returnCreditCardFailed(riskLevel, "elevated risk level", outcome.getSellerMessage());
                    }
                }
            }
            if(confirm.returnCode == 1){
                logger.debug(requestId + " - SCALOG: PREAUTH 4 PAYMENT COMPLETE " +bookId);
                paymentIntent = (PaymentIntent) confirm.response;
                //Charge charge = paymentIntent.getLatestChargeObject();
                chargeId = paymentIntent.getLatestCharge();
                Stripe.apiKey = secretKey;
                Charge charge = Charge.retrieve(chargeId);
                Charge.Outcome outcome = charge.getOutcome();
                if (outcome != null) {
                    String riskLevel = outcome.getRiskLevel() != null ? outcome.getRiskLevel() : "";
                    if (riskLevel.equals("elevated")) {
                        voidStripeTransaction(requestId, fleetId, bookId, secretKey, chargeId);
                        return returnCreditCardFailed(riskLevel, "elevated risk level", outcome.getSellerMessage());
                    }
                }
                String status = paymentIntent.getStatus();
                if (!status.equals("requires_capture")){
                    return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 437, "", "requires_capture");

                }
            }
            if(confirm.returnCode == 0){// chua Pay
                logger.debug(requestId + " - SCALOG: PREAUTH 5 PAYMENT AGAIN " + bookId);
                Gson gson = new Gson();
                PaymentIntent  paymentIntentCache = (PaymentIntent) confirm.response;
                Map<String, Object> response = new HashMap<String, Object>();
                response.put("clientSecret", paymentIntentCache.getClientSecret());
                response.put("bookId",bookId);
                preAuthResponse.returnCode = 113;
                preAuthResponse.response = response;
                return preAuthResponse.toString();
            }
            if( confirm.returnCode == 2){// da pay that bai or loi stripe

                return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 115,"", "Try Again");

            }

            Map<String, Object> mapResponse = new HashMap<>();
            mapResponse.put("authId", chargeId);
            mapResponse.put("authAmount", String.valueOf(amount));
            mapResponse.put("authCode", "");
            mapResponse.put("authToken", token);
            mapResponse.put("allowCapture", "true");
            mapResponse.put("smallestValue", intAmount);

            preAuthResponse.response = mapResponse;
            preAuthResponse.returnCode = 200;

            String result = preAuthResponse.toString();
            logger.debug(requestId + " - result: " + result);
            try {
                StripeIntentUtil sUtil = new  StripeIntentUtil();
                sUtil.removeAuthenRequired(bookId);
                sUtil.removeAuthenRequired(paymentIntent.getId());
            }catch (Exception ex){
                logger.error(ex);
            }
            return result;
        } catch(CardException ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            // payment error, void completed transaction
            if (!chargeId.equals("")) {
                voidStripeTransaction(requestId, fleetId, bookId, secretKey, chargeId);
            }
            // add transaction pay with Stripe

            return returnCreditCardFailed(ex.getCode(), getError(ex), ex.getMessage());
        } catch(RateLimitException ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 437, getError(ex), ex.getMessage());
        } catch(InvalidRequestException ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 437, getError(ex), ex.getMessage());
        } catch(AuthenticationException ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 415, getError(ex), ex.getMessage());
        } catch(Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 437, getError(ex), ex.getMessage());
        }

    }

    public String preAuthCapture(String fleetId, String bookId, String secretKey,
                                 String token, double amount, String authId, String currencyISO){
        logger.debug(requestId + " - SCALOG: CAPTURE 1 " + bookId);
        logger.debug(requestId + " - secretKey: " + secretKey);
        Stripe.apiKey = secretKey;
        ObjResponse preAuthResponse = new ObjResponse();
        String chargeId = "";
        try {
            int intAmount;
            if (isZeroDecimal(currencyISO)) {
                intAmount = (int) amount;
            } else {
                DecimalFormat df = new DecimalFormat("#");
                intAmount = Integer.parseInt(df.format(amount*100));
            }
            Stripe.apiKey = secretKey;
            Charge charge1 = Charge.retrieve(authId);
            String cardMask = "";
            String cardType = "";
            PaymentIntent intent = null;
            try {
                logger.debug(requestId + " - SCALOG: CAPTURE 2 CREATE PAYMENTINTER " + bookId);
                Stripe.apiKey = secretKey;
                intent = PaymentIntent.retrieve(charge1.getPaymentIntent());
            }catch (Exception ex){
                logger.debug(requestId + " - SCALOG: CAPTURE 3 CREATE PAYMENTINTER FAILED " + bookId);
                logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            }
            if (intent != null){
                logger.debug(requestId + " - SCALOG: CAPTURE 4 PAY WITH PAYMENTINTER " + bookId);
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("amount_to_capture", intAmount);

                Stripe.apiKey = secretKey;
                intent = intent.capture(params);
                Stripe.apiKey = secretKey;
                chargeId = intent.getLatestCharge();
                Charge charge2 = Charge.retrieve(chargeId);
                cardMask = "XXXXXXXXXXXX" + charge2.getPaymentMethodDetails().getCard().getLast4();
                cardType = charge2.getPaymentMethodDetails().getCard().getBrand();
            } else {
                logger.debug(requestId + " - SCALOG: CAPTURE 5 PAY WITH CHARGE " + bookId);
                logger.debug(requestId + " - intAmount: " + intAmount);
                Map<String, Object> chargeParams = new HashMap<String, Object>();
                chargeParams.put("amount", intAmount);

                Stripe.apiKey = secretKey;
                Charge charge = Charge.retrieve(authId);
                Stripe.apiKey = secretKey;
                Charge captured = charge.capture(chargeParams);
                chargeId = captured.getId();
                Card card = (Card) captured.getSource();
                cardMask = "XXXXXXXXXXXX" + card.getLast4();
                cardType = card.getBrand();
            }

            /*String transferred = "false";
            try {
                // charge success - do transfer
                if (instantPayout && !achToken.isEmpty()) {
                    logger.debug(requestId + " - transferFee = " + transferFee);
                    TransferCreateParams transferParams =
                            TransferCreateParams.builder()
                                    .setAmount((long) intTransferFee)
                                    .setCurrency(currencyISO)
                                    .setDestination(achToken)
                                    .setTransferGroup(bookId)
                                    .setSourceTransaction(chargeId)
                                    .setDescription("Transfer for booking #"+bookId)
                                    .build();
                    Stripe.apiKey = secretKey;
                    Transfer transfer  = Transfer.create(transferParams);
                    logger.debug(requestId + " - transfer = " + gson.toJson(transfer));
                    transferred = "true";
                }
            } catch (Exception ex) {
                logger.debug(requestId + " - transfer exception: " + gson.toJson(ex));
                transferred = ex.getMessage();
            }*/


            String authorizationCode = "";
            String gatewayRejectionReason = "";
            Map<String, String> mapResponse = new HashMap<String, String>();
            mapResponse.put("message", "Success!");
            mapResponse.put("transId", chargeId);
            mapResponse.put("cardType", cardType);
            mapResponse.put("authCode", authorizationCode);
            mapResponse.put("cardMask", cardMask);
            mapResponse.put("transferred", "");
            preAuthResponse.response = mapResponse;
            preAuthResponse.returnCode = 200;
            String result = preAuthResponse.toString();
            logger.debug(requestId + " - result: " + result);
            return result;
        } catch(CardException ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            // payment error, void completed transaction
            if (!chargeId.equals("")) {
                voidStripeTransaction(requestId, fleetId, bookId, secretKey, chargeId);
            }
            // add transaction pay with Stripe

            return returnCreditCardFailed(ex.getCode(), getError(ex), ex.getMessage());
        } catch(RateLimitException ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 437, getError(ex), ex.getMessage());
        } catch(InvalidRequestException ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 437, getError(ex), ex.getMessage());
        } catch(AuthenticationException ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 415, getError(ex), ex.getMessage());
        } catch(Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 437, getError(ex), ex.getMessage());
        }

    }

    public String capturePaymentIntent(String fleetId, String bookId, String secretKey,
                                       String token, double amount, String paymentIntentId, String currencyISO){
        logger.debug(requestId + " - SCALOG: CAPTURE paymentIntentId " + paymentIntentId);
        logger.debug(requestId + " - secretKey: " + secretKey);
        Stripe.apiKey = secretKey;
        ObjResponse preAuthResponse = new ObjResponse();
        String chargeId = "";
        try {
            int intAmount;
            if (isZeroDecimal(currencyISO)) {
                intAmount = (int) amount;
            } else {
                DecimalFormat df = new DecimalFormat("#");
                intAmount = Integer.parseInt(df.format(amount*100));
            }
            Stripe.apiKey = secretKey;
            PaymentIntent intent = null;
            try {
                logger.debug(requestId + " - SCALOG: CAPTURE 2 CREATE PAYMENTINTER " + bookId);
                Stripe.apiKey = secretKey;
                intent = PaymentIntent.retrieve(paymentIntentId);
            }catch (Exception ex){
                logger.debug(requestId + " - SCALOG: CAPTURE 3 CREATE PAYMENTINTER FAILED " + bookId);
                logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            }
            String cardType = "";
            String cardMask = "";
            if (intent != null){
                logger.debug(requestId + " - SCALOG: CAPTURE 4 PAY WITH PAYMENTINTER " + bookId);
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("amount_to_capture", intAmount);

                Stripe.apiKey = secretKey;
                intent = intent.capture(params);
                Stripe.apiKey = secretKey;
                chargeId = intent.getLatestCharge();
                Charge charge2 = Charge.retrieve(chargeId);
                cardMask = "XXXXXXXXXXXX" + charge2.getPaymentMethodDetails().getCard().getLast4();
                cardType = charge2.getPaymentMethodDetails().getCard().getBrand();
            }

            String authorizationCode = "";
            String gatewayRejectionReason = "";
            Map<String, String> mapResponse = new HashMap<String, String>();
            mapResponse.put("message", "Success!");
            mapResponse.put("transId", chargeId);
            mapResponse.put("cardType", cardType);
            mapResponse.put("authCode", authorizationCode);
            mapResponse.put("cardMask", cardMask);
            preAuthResponse.response = mapResponse;
            preAuthResponse.returnCode = 200;
            String result = preAuthResponse.toString();
            logger.debug(requestId + " - result: " + result);
            return result;
        } catch(CardException ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            // payment error, void completed transaction
            if (!chargeId.equals("")) {
                voidStripeTransaction(requestId, fleetId, bookId, secretKey, chargeId);
            }
            // add transaction pay with Stripe

            return returnCreditCardFailed(ex.getCode(), getError(ex), ex.getMessage());
        } catch(RateLimitException ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 437, getError(ex), ex.getMessage());
        } catch(InvalidRequestException ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 437, getError(ex), ex.getMessage());
        } catch(AuthenticationException ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 415, getError(ex), ex.getMessage());
        } catch(Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 437, getError(ex), ex.getMessage());
        }

    }

    public List<String> getListSupportedCurrencies(String secretKey) {
        Stripe.apiKey = secretKey;
        //Stripe.apiVersion = apiVersion;

        try {
            CountrySpec countrySpec = CountrySpec.retrieve("US");

            return countrySpec.getSupportedPaymentCurrencies();

        } catch(Exception ex) {
            ex.printStackTrace();
            logger.debug(requestId + " - Message is: " + getError(ex));

            return new ArrayList<>();

        }

    }

    private String getError(Exception ex) {
        return GatewayUtil.getError(ex);
    }

    private boolean isZeroDecimal(String currencyISO) {
        List<String> fullList = Arrays.asList("BIF","CLP","DJF","GNF","JPY","KMF","KRW","MGA","PYG","RWF","UGX","VND","VUV","XAF","XOF","XPF");
        return fullList.contains(currencyISO);
    }

    public String payToDriver(String fleetId, String token, double amount, String name, String secretKey, String currencyISO){
        ObjResponse objResponse = new ObjResponse();
        String bookId = "ACH transfer";
        String chargeId = "";
        Stripe.apiKey = secretKey;
        // Stripe.apiVersion = apiVersion;

        try {
            DecimalFormat df = new DecimalFormat("#");
            int intAmount = Integer.parseInt(df.format(amount*100));
            logger.debug(requestId + " - intAmount: " + intAmount);

            Map<String, Object> transferParams = new HashMap<String, Object>();
            transferParams.put("amount", intAmount);
            transferParams.put("currency", currencyISO);
            transferParams.put("destination", token);
            transferParams.put("transfer_group", fleetId);
            /*transferParams.put("statement_descriptor", "Pay to driver " + name);*/
            Stripe.apiKey = secretKey;
            Transfer transfer = Transfer.create(transferParams);
            chargeId = transfer.getId();

            String cardMask = "";
            String cardType = "bank account";
            String authorizationCode = "";
            String gatewayRejectionReason = "";
                /*addStripeTransaction(fleetId, currencyISO, bookId,
                        transferId, amount, transfer.getStatus(),
                        cardMask, cardType, token,
                        authorizationCode, gatewayRejectionReason);*/

            Map<String,String> mapResponse = new HashMap<>();
            mapResponse.put("message", "Success!");
            mapResponse.put("transId", chargeId);
            mapResponse.put("cardType", cardType);
            mapResponse.put("authCode", authorizationCode);
            mapResponse.put("cardMask", cardMask);
            objResponse.response = mapResponse;
            objResponse.returnCode = 200;
            return objResponse.toString();
        } catch(CardException ex) {
            ex.printStackTrace();
            // payment error, void completed transaction
            if (!chargeId.equals("")) {
                voidStripeTransaction(requestId, fleetId, bookId, secretKey, chargeId);
            }
            // add transaction pay with Stripe

            return returnCreditCardFailed(ex.getCode(), getError(ex), ex.getMessage());
        } catch(RateLimitException ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 437, getError(ex), ex.getMessage());
        } catch(InvalidRequestException invalid) {
            logger.debug(requestId + " - Invalid Request Exception - Check Insufficient funds and try again without transfer_group");
            String error = invalid.getMessage();
            logger.debug(requestId + " - error message: " + error);
            if (error.toLowerCase().contains("insufficient funds")) {
                try {
                    DecimalFormat df = new DecimalFormat("#");
                    int intAmount = Integer.parseInt(df.format(amount*100));
                    logger.debug(requestId + " - intAmount: " + intAmount);

                    Map<String, Object> transferParams = new HashMap<String, Object>();
                    transferParams.put("amount", intAmount);
                    transferParams.put("currency", currencyISO);
                    transferParams.put("destination", token);
                    /*transferParams.put("statement_descriptor", "Pay to driver " + name);*/
                    Stripe.apiKey = secretKey;
                    Transfer transfer = Transfer.create(transferParams);
                    chargeId = transfer.getId();

                    String cardMask = "";
                    String cardType = "bank account";
                    String authorizationCode = "";
                    String gatewayRejectionReason = "";

                    Map<String,String> mapResponse = new HashMap<>();
                    mapResponse.put("message", "Success!");
                    mapResponse.put("transId", chargeId);
                    mapResponse.put("cardType", cardType);
                    mapResponse.put("authCode", authorizationCode);
                    mapResponse.put("cardMask", cardMask);
                    objResponse.response = mapResponse;
                    objResponse.returnCode = 200;
                    return objResponse.toString();
                } catch(InvalidRequestException invalidTry) {
                    error = invalidTry.getMessage();
                    logger.debug(requestId + " - error message: " + error);
                    int returnCode = 437;
                    if (error.toLowerCase().contains("insufficient funds")) {
                        returnCode = 524;
                    }
                    return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, returnCode, getError(invalidTry), invalidTry.getMessage());
                } catch(Exception ex) {
                    logger.debug(requestId + " - error message: " + ex.getMessage());
                }
                // if retry still not success - return error Insufficient funds
                return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 524, getError(invalid), invalid.getMessage());
            }
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 437, getError(invalid), invalid.getMessage());
        } catch(AuthenticationException ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 415, getError(ex), ex.getMessage());
        }   catch(Exception ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 437, getError(ex), ex.getMessage());
        }
    }

    public String chargeFleetOwner(String fleetId, String token, double amount, String secretKey, String currencyISO) {
        ObjResponse objResponse = new ObjResponse();
        String bookId = "Billing of " + fleetId; // charge fleet subscription
        Stripe.apiKey = secretKey;
        //Stripe.apiVersion = apiVersion;
        String chargeId = "";
        try {
            DecimalFormat df = new DecimalFormat("#");
            int intAmount = Integer.parseInt(df.format(amount*100));
            logger.debug(requestId + " - intAmount: " + intAmount);
            Map<String, Object> chargeParams = new HashMap<String, Object>();
            chargeParams.put("amount", intAmount);
            chargeParams.put("currency", currencyISO);
            chargeParams.put("customer", token);
            Stripe.apiKey = secretKey;
            Charge charge = Charge.create(chargeParams);
            chargeId = charge.getId();
            Card card = (Card) charge.getSource();
            String cardMask = "XXXXXXXXXXXX" + card.getLast4();
            String cardType = card.getBrand();
            String authorizationCode = "";
            String gatewayRejectionReason = "";
            /*addStripeTransaction(fleetId, currencyISO, bookId,
                    chargeId, amount, "success",
                    cardMask, cardType, token,
                    authorizationCode, gatewayRejectionReason);*/

            Map<String, String> mapResponse = new HashMap<String, String>();
            mapResponse.put("message", "Success!");
            mapResponse.put("transId", chargeId);
            mapResponse.put("cardType", cardType);
            mapResponse.put("authCode", authorizationCode);
            mapResponse.put("cardMask", cardMask);
            objResponse.response = mapResponse;
            objResponse.returnCode = 200;
            return objResponse.toString();
        } catch(CardException ex) {
            ex.printStackTrace();
            // payment error, void completed transaction
            if (!chargeId.equals("")) {
                voidStripeTransaction(requestId, fleetId, bookId, secretKey, chargeId);
            }
            // add transaction pay with Stripe

            return returnCreditCardFailed(ex.getCode(), getError(ex), ex.getMessage());
        } catch(RateLimitException ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 437, getError(ex), ex.getMessage());
        } catch(InvalidRequestException ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 437, getError(ex), ex.getMessage());
        } catch(AuthenticationException ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 415, getError(ex), ex.getMessage());
        } catch(Exception ex) {
            ex.printStackTrace();
            return returnPaymentFailed(fleetId, bookId, secretKey, chargeId, amount, token, 437, getError(ex), ex.getMessage());
        }
    }

    /*
        Add ACH TOKEN with VERIFICATION DOCUMENT !!!
     */
    public String createACHToken(String token, String bankNumber, String ibanNumber, String routingNumber, String institutionNumber, String sortCode,
                                 String firstName, String lastName, int day, int month, int year,
                                 String address, String city, String state, String zipCode, String ssn,
                                 String frontFileId, String backFileId, String frontAddFileId, String backAddFileId,
                                 String secretKey, String currencyISO, String countryCode, String legalToken,
                                 String email, String phone, boolean enableCardPayment){
        logger.debug(requestId + " - createACHToken for driver: " + firstName + " " + lastName);
        logger.debug(requestId + " - createACHToken secretKey: " + secretKey);
        ObjResponse achResponse = new ObjResponse();
        Stripe.apiKey = secretKey;
        // Stripe.apiVersion = apiVersion;

        try {
            Map<String, Object> accountParams = new HashMap<String, Object>();
            if (token.equals("")) {
                accountParams.put("type", "custom");
                accountParams.put("country", countryCode);
            }
            accountParams.put("default_currency", currencyISO);

            Map<String, Object> externalAccount = new HashMap<String, Object>();
            externalAccount.put("object", "bank_account");
            externalAccount.put("country", countryCode);
            externalAccount.put("currency", currencyISO);
            externalAccount.put("account_holder_name", firstName + " " + lastName);
            externalAccount.put("account_holder_type", "individual");

            // The routing number, sort code, or other country-appropriate institution number for the bank account.
            // For US bank accounts, this is required and should be the ACH routing number, not the wire routing number.
            // If you are providing an IBAN for account_number, this field is not required.

            if (PaymentUtil.listRequiredRounting.contains(countryCode.toUpperCase())) {
                externalAccount.put("routing_number", routingNumber);
                externalAccount.put("account_number", bankNumber);
            } else if ("CA".equalsIgnoreCase(countryCode)) {
                externalAccount.put("account_number", bankNumber);
                externalAccount.put("routing_number", institutionNumber.isEmpty() ? routingNumber : institutionNumber);
            } else if ("GB".equalsIgnoreCase(countryCode)) {
                externalAccount.put("account_number", bankNumber);
                externalAccount.put("routing_number", sortCode.isEmpty() ? routingNumber : sortCode);
            } else {
                // no need to include routing_number if the country belongs to Europe
                externalAccount.put("account_number", ibanNumber.isEmpty() ? bankNumber : ibanNumber);
            }
            accountParams.put("external_account", externalAccount);

            // add requested capabilities
            if (enableCardPayment)
                accountParams.put("requested_capabilities", Arrays.asList("transfers", "card_payments"));
            else
                accountParams.put("requested_capabilities", Arrays.asList("transfers"));

            Map<String, Object> individual = new HashMap<String, Object>();
            individual.put("first_name", firstName);
            individual.put("last_name", lastName);
            if (!email.isEmpty())
                individual.put("email", email);
            if (!phone.isEmpty())
                individual.put("phone", phone);

            Map<String, Object> addressMap = new HashMap<String, Object>();
            addressMap.put("city", city);
            addressMap.put("country", countryCode);
            addressMap.put("line1", address);
            if (!state.isEmpty())
                addressMap.put("state", state);
            if (!zipCode.isEmpty())
                addressMap.put("postal_code", zipCode);
            individual.put("address", addressMap);

            Map<String, Object> dobMap = new HashMap<String, Object>();
            dobMap.put("day", day);
            dobMap.put("month", month);
            dobMap.put("year", year);
            individual.put("dob", dobMap);

            if ("US".equalsIgnoreCase(countryCode)) {
                if (!ssn.isEmpty())
                    individual.put("id_number", ssn);
                String last4 = ssn.length() > 4 ? ssn.substring(ssn.length() - 4) : ssn;
                // The last 4 digits of the social security number of the representative of the legal entity. Only requested in the US.
                individual.put("ssn_last_4", last4);
            }

            Map<String, Object> verificationParams = new HashMap<>();
            Map<String, Object> document = new HashMap<>();
            // update verification for added account
            if (!frontFileId.isEmpty()) {
                document.put("front", frontFileId);
            }
            if (!backFileId.isEmpty()) {
                document.put("back", backFileId);
            }
            verificationParams.put("document", document);

            Map<String, Object> addDocument = new HashMap<>();
            if (!frontAddFileId.isEmpty()) {
                addDocument.put("front", frontAddFileId);
            }
            if (!backAddFileId.isEmpty()) {
                addDocument.put("back", backAddFileId);
            }
            verificationParams.put("additional_document", addDocument);

            if (verificationParams.size() > 0) {
                individual.put("verification", verificationParams);
            }
            if (legalToken.isEmpty()) {
                // Message is: com.stripe.exception.InvalidRequestException: Parameter 'business_type' cannot be used in conjunction with an account token. To set this field create a token with the desired changes. Pass the token with 'account_token' in a request without setting 'business_type'.
                // See https://stripe.com/docs/connect/account-tokens#updating for more information.; request-id: req_lxE1tCoj8l3pkL
                accountParams.put("business_type", "individual");

                // Message is: com.stripe.exception.InvalidRequestException: Parameter 'individual' cannot be used in conjunction with an account token. To set this field create a token with the desired changes. Pass the token with 'account_token' in a request without setting 'individual'.
                // See https://stripe.com/docs/connect/account-tokens#updating for more information.; request-id: req_zCHkksMEFc8sxq
                accountParams.put("individual", individual);
            }
            Map<String, Object> tosParams = new HashMap<String, Object>();
            tosParams.put("date", System.currentTimeMillis() / 1000L);
            tosParams.put("ip", "192.0.0.1");
            if (listAccountTokenCountries.contains(countryCode)) {
                // https://stripe.com/docs/connect/account-tokens
                if (!legalToken.isEmpty())
                    accountParams.put("account_token", legalToken);
            } else {
                accountParams.put("tos_acceptance", tosParams);
            }

            Map<String, Object> businessProfile = new HashMap<String, Object>();
            businessProfile.put("mcc", "4789");
            businessProfile.put("product_description", "Our driver application is for drivers to receive the dispatched booking and take care it. For credit card booking, we will pay the money back to drivers as the payment amount goes to our Stripe merchant account.");
            accountParams.put("business_profile", businessProfile);

            Gson gson = new Gson();
            logger.debug(requestId + " - account: " + gson.toJson(accountParams));
            Map<String, String> mapResponse = new HashMap<>();

            // create or update account
            if (token.equals("")) {
                logger.debug(requestId + " - creating new account ...");
                Stripe.apiKey = secretKey;
                Account stripeAccount = Account.create(accountParams);
                token = stripeAccount.getId();
                mapResponse.put("token", stripeAccount.getId());
            } else {
                logger.debug(requestId + " - updating " + token);
                Stripe.apiKey = secretKey;
                Account stripeAccount = Account.retrieve(token);

                String status = (stripeAccount.getIndividual() != null && stripeAccount.getIndividual().getVerification() != null && stripeAccount.getIndividual().getVerification().getStatus() != null) ? stripeAccount.getIndividual().getVerification().getStatus() : "";
                logger.debug(requestId + " - status: " + status);
                if (status.equals("verified")) {
                    achResponse.returnCode = 200;
                    mapResponse.put("token", token);
                    achResponse.response = mapResponse;
                    return achResponse.toString();
                }
                Stripe.apiKey = secretKey;
                stripeAccount.update(accountParams);
                //logger.debug(requestId + " - verification field needed: " + stripeAccount.getVerification().getFieldsNeeded().size());
                mapResponse.put("token", stripeAccount.getId());
            }

            achResponse.returnCode = 200;
            achResponse.response = mapResponse;
            return achResponse.toString();
        } catch (InvalidRequestException invalid) {
            String message = invalid.getMessage();
            logger.debug(requestId + " - Message is: " + message);
            if (message.contains("card_payments")) {
                return createACHToken(token, bankNumber, ibanNumber, routingNumber, institutionNumber, sortCode,
                        firstName, lastName, day, month, year, address, city, state, zipCode, ssn,
                        frontFileId, backFileId, frontAddFileId, backAddFileId,
                        secretKey, currencyISO, countryCode, legalToken,
                        email, phone, true);
            } else {
                int returnCode = 493;
                Map<String, String> mapResponse = new HashMap<>();
                mapResponse.put("response", message);
                mapResponse.put("message", invalid.getMessage());

                achResponse.returnCode = returnCode;
                achResponse.response = mapResponse;
                return achResponse.toString();
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            logger.debug(requestId + " - Message is: " + getError(ex));

            int returnCode = 493;
            String message = getError(ex);
            if (message.contains("birth year") || message.contains("13 years of age")) returnCode = 495;
            if (message.contains("postal code")) returnCode = 496;
            if (message.contains("province") || message.contains("state")) returnCode = 497;
            if (message.contains("Invalid transit number") || message.contains("Routing number")) returnCode = 499;
            if (message.contains("US ID numbers") || message.contains("CA ID numbers")) returnCode = 513;

            Map<String, String> mapResponse = new HashMap<>();
            mapResponse.put("response", message);
            mapResponse.put("message", ex.getMessage());

            achResponse.returnCode = returnCode;
            achResponse.response = mapResponse;
            return achResponse.toString();

        }
    }

    public boolean checkStripeACHToken(String token, String secretKey){
        logger.debug(requestId + " - checkStripeACHToken for token: " + token);
        logger.debug(requestId + " - current key: " + Stripe.apiKey);
        Stripe.apiKey = secretKey;
        logger.debug(requestId + " - new key: " + Stripe.apiKey);
        //Stripe.apiVersion = apiVersion;
        try {
            Account stripeAccount = Account.retrieve(token);
            String status = (stripeAccount.getIndividual() != null && stripeAccount.getIndividual().getVerification() != null && stripeAccount.getIndividual().getVerification().getStatus() != null) ? stripeAccount.getIndividual().getVerification().getStatus() : "";
            return status.equals("verified");
        } catch(Exception ex) {
            ex.printStackTrace();
            logger.debug(requestId + " - Message is: " + getError(ex));
        }
        return false;
    }

    /*public static void main(String[] args) throws Exception{
        testACHToken();
    }

    public static void testACHToken ()  throws Exception{
        Stripe.apiKey = "sk_live_vpl0zh4BcIqpY6eWfM58Rqyc";

        Map<String, Object> accountParams = new HashMap<String, Object>();
        accountParams.put("requested_capabilities", Arrays.asList("transfers"));

        String token = "acct_1Bv43cBnyWSgepDt";
        Account stripeAccount = Account.retrieve(token);
        String transfer = stripeAccount.getCapabilities().getTransfers();
        String legacy = stripeAccount.getCapabilities().getLegacyPayments();
        logger.debug(requestId + " - transfer: " + transfer);
        logger.debug(requestId + " - legacy: " + legacy);
        Gson gson = new Gson();
        logger.debug(requestId + " - account: " + gson.toJson(stripeAccount));

        if (transfer == null && legacy.equals("active")) {
            stripeAccount.update(accountParams);
            logger.debug(requestId + " - updated");
        }
    }*/

    public void updateOldStripeBank(String secretKey) {
        logger.debug(requestId + " - updateOldStripeBank for " + secretKey);
        Stripe.apiKey = secretKey;
        Map<String, Object> accountParams = new HashMap<String, Object>();
        accountParams.put("requested_capabilities", Arrays.asList("transfers"));
        Map<String, Object> businessProfile = new HashMap<String, Object>();
        businessProfile.put("product_description", "Our driver application is for drivers to receive the dispatched booking and take care it. For credit card booking, we will pay the money back to drivers as the payment amount goes to our Stripe merchant account.");
        accountParams.put("business_profile", businessProfile);

        try {
            String lastId = "lastId";
            boolean firstTime = true;
            List<String> warnings = new ArrayList<>();
            while (!lastId.isEmpty()) {
                Map<String, Object> params = new HashMap<>();
                params.put("limit", 20);
                if (!firstTime)
                    params.put("starting_after", lastId);

                AccountCollection accountCollection = Account.list(params);
                List<Account> accounts = accountCollection.getData();

                // reset
                lastId = "";

                logger.debug(requestId + " - accounts: " + accounts.size());
                for (Account account : accounts) {
                    //logger.debug(requestId + " ---- " + "account: " + account);
                    String transfer = account.getCapabilities().getTransfers() != null ? account.getCapabilities().getTransfers() : "";
                    //String legacy = account.getCapabilities().getLegacyPayments();
                    logger.debug(requestId + " ---- " + "account: " + account.getId());
                    logger.debug(requestId + " - transfer: " + transfer);
                    //logger.debug(requestId + " - legacy: " + legacy);
                    Account.Requirements requirements = account.getRequirements();
                    String details = account.getId();
                    if (account.getIndividual() != null) {
                        details +=  "," + account.getIndividual().getFirstName() + " " + account.getIndividual().getLastName();
                    } else {
                        details += ",";
                    }

                    if (!transfer.equals("active")) {
                        try {
                            account.update(accountParams);

                            Thread.sleep(1000);

                            Account check = Account.retrieve(account.getId());
                            transfer = check.getCapabilities().getTransfers() != null ? check.getCapabilities().getTransfers() : "";
                            requirements = check.getRequirements();
                            logger.debug(requestId + " - transfer: " + transfer);
                            if (transfer.equals("active")) {
                                logger.debug(requestId + " - is updated");
                            } else {
                                String status = (check.getIndividual() != null && check.getIndividual().getVerification() != null && check.getIndividual().getVerification().getStatus() != null) ? check.getIndividual().getVerification().getStatus() : "";
                                logger.debug(requestId + " - update failed - account status: " + status);
                            }
                        } catch (Exception ex) {
                            try {
                                accountParams.remove("requested_capabilities");
                                accountParams.put("requested_capabilities", Arrays.asList("card_payments", "transfers"));
                                account.update(accountParams);
                                Thread.sleep(1000);

                                Account check = Account.retrieve(account.getId());
                                transfer = check.getCapabilities().getTransfers() != null ? check.getCapabilities().getTransfers() : "";
                                requirements = check.getRequirements();
                                logger.debug(requestId + " - transfer: " + transfer);
                                if (transfer.equals("active")) {
                                    logger.debug(requestId + " - is updated");
                                } else {
                                    logger.debug(requestId + " - account: " + check);
                                    String status = (check.getIndividual() != null && check.getIndividual().getVerification() != null && check.getIndividual().getVerification().getStatus() != null) ? check.getIndividual().getVerification().getStatus() : "";
                                    logger.debug(requestId + " - update failed - account status: " + status);
                                }
                            } catch (Exception ex2) {
                                logger.debug(requestId + " - Message is: " + getError(ex2));
                            }
                        }
                    }
                    // check requirements
                    if (requirements != null) {
                        List<String> dues = requirements.getEventuallyDue();
                        if (dues.size() > 0) {
                            String listDue = "";
                            for (String due : dues) {
                                if (listDue.isEmpty())
                                    listDue = due;
                                else
                                    listDue += "\\n" + due;
                            }
                            details = details + "," + listDue;
                            /*for (String due : dues) {
                                details += "," + due;
                            }*/
                            warnings.add(details);
                        }
                    } else {
                        details += ",please check account details";
                        warnings.add(details);
                    }

                    lastId = account.getId();
                }
                logger.debug(requestId + " - lastId: " + lastId);
                firstTime = false;
            }

            for (String detail : warnings) {
                logger.debug(requestId + " - need update: " + detail);
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - Message is: " + getError(ex));
        }

    }

    public String saveDocument(String verificationDocument, String secretKey){
        ObjResponse achResponse = new ObjResponse();
        logger.debug(requestId + " - saveDocument: " + verificationDocument);

        logger.debug(requestId + " - current key: " + Stripe.apiKey);
        Stripe.apiKey = secretKey;
        logger.debug(requestId + " - new key: " + Stripe.apiKey);
        // Stripe.apiVersion = apiVersion;

        try {
            Map<String, Object> fileUploadParams = new HashMap<String, Object>();
            //RequestOptions requestOptions = RequestOptions.builder().setStripeAccount(stripeAccount.getId()).build();
            fileUploadParams.put("purpose", "identity_document");
            fileUploadParams.put("file", new File(verificationDocument));
            Stripe.apiKey = secretKey;
            com.stripe.model.File fileObj = com.stripe.model.File.create(fileUploadParams, (RequestOptions) null);
            logger.debug(requestId + " - file = " + fileObj.getId());

            Map<String, String> mapResponse = new HashMap<>();
            mapResponse.put("fileId", fileObj.getId());

            achResponse.returnCode = 200;
            achResponse.response = mapResponse;
            return achResponse.toString();
        } catch(Exception ex) {
            logger.debug(requestId + " - Message is: " + getError(ex));
            String message = getError(ex);
            Map<String, String> mapResponse = new HashMap<>();
            mapResponse.put("response", message);

            achResponse.returnCode = 411;
            achResponse.response = mapResponse;
            return achResponse.toString();
        }

    }

    public String stripeConnectOnboarding(String secretKey, String returnUrl) {
        ObjResponse response = new ObjResponse();
        try {
            Map<String, Object> accountParams = new HashMap<String, Object>();
            accountParams.put("type", AccountCreateParams.Type.EXPRESS);
            accountParams.put("requested_capabilities", Collections.singletonList("transfers"));
            accountParams.put("business_type", "individual");
            Stripe.apiKey = secretKey;
            Account account = Account.create(accountParams);
            logger.debug(requestId + " - account = " + gson.toJson(account));
            logger.debug(requestId + " - account = " + account.getId());

            if (returnUrl.isEmpty()) {
                returnUrl = ServerConfig.hook_server + "/content?gateway=Stripe&action=register";
            }

            Map<String, Object> linkParams = new HashMap<>();
            linkParams.put("account", account.getId());
            linkParams.put(
                    "refresh_url",
                    ServerConfig.hook_server + "/notification/" + KeysUtil.STRIPE + "?urlAction=refresh"
            );
            linkParams.put(
                    "return_url",
                    returnUrl
            );
            linkParams.put("type", "account_onboarding");
            Stripe.apiKey = secretKey;
            AccountLink accountLink = AccountLink.create(linkParams);

            logger.debug(requestId + " - accountLink: " + gson.toJson(accountLink));
            //accountLink = {"created":1663577557,"expires_at":1663577857,"object":"account_link","url":"https://connect.stripe.com/setup/e/acct_1LjfiRPCul8bj24l/cUkyEMxcnhrm"}
            String url = accountLink.getUrl() != null ? accountLink.getUrl() : "";
            logger.debug(requestId + " - url: " + url);
            if (!url.isEmpty()) {
                Map<String, String> mapResponse = new HashMap<>();
                mapResponse.put("url", url);
                mapResponse.put("type", "onboarding");
                mapResponse.put("accountId", account.getId());
                response.returnCode = 200;
                response.response = mapResponse;
                return response.toString();
            } else {
                response.returnCode = 525;
                return response.toString();
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + getError(ex));
            response.returnCode = 525;
            return response.toString();
        }
    }

    public String stripeConnectLogin(String token, String secretKey, String status, String returnUrl) {
        ObjResponse response = new ObjResponse();
        try {
            String url;
            String type;
            if (status.equals("activated")) {
                LoginLinkCreateOnAccountParams params = LoginLinkCreateOnAccountParams.builder().build();
                RequestOptions options = RequestOptions.builder().build();
                Stripe.apiKey = secretKey;
                LoginLink loginLink = LoginLink.createOnAccount(token, params, options);

                logger.debug(requestId + " - loginLink: " + gson.toJson(loginLink));
                //loginLink = {"created":1661963361,"object":"login_link","url":"https://connect.stripe.com/express/XiNdsl4KaAf6"}
                url = loginLink.getUrl() != null ? loginLink.getUrl() : "";
                type = "login";
            } else {
                if (returnUrl.isEmpty()) {
                    returnUrl = ServerConfig.hook_server + "/content?gateway=Stripe&action=register";
                }

                Map<String, Object> linkParams = new HashMap<>();
                linkParams.put("account", token);
                linkParams.put(
                        "refresh_url",
                        ServerConfig.hook_server + "/notification/" + KeysUtil.STRIPE + "?urlAction=refresh"
                );
                linkParams.put(
                        "return_url",
                        returnUrl
                );
                linkParams.put("type", "account_onboarding");
                Stripe.apiKey = secretKey;
                AccountLink accountLink = AccountLink.create(linkParams);
                logger.debug(requestId + " - accountLink = " + gson.toJson(accountLink));
                //accountLink = {"created":1663577557,"expires_at":1663577857,"object":"account_link","url":"https://connect.stripe.com/setup/e/acct_1LjfiRPCul8bj24l/cUkyEMxcnhrm"}
                url = accountLink.getUrl() != null ? accountLink.getUrl() : "";
                type = "onboarding";
            }

            logger.debug(requestId + " - url: " + gson.toJson(url));
            if (!url.isEmpty()) {
                Map<String, String> mapResponse = new HashMap<>();
                mapResponse.put("url", url);
                mapResponse.put("type", type);
                response.returnCode = 200;
                response.response = mapResponse;
                return response.toString();
            } else {
                response.returnCode = 525;
                return response.toString();
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + getError(ex));
            response.returnCode = 525;
            return response.toString();
        }
    }

    public boolean getConnectStatus(String token, String secretKey) {
        try {
            Stripe.apiKey = secretKey;
            Account account = Account.retrieve(token);
            logger.debug(requestId + " - account = " + gson.toJson(account));
            logger.debug(requestId + " - charges_enabled ? " + account.getChargesEnabled());
            return account.getChargesEnabled();
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + getError(ex));
            return false;
        }
    }

    public String getExchangeRate(String requestId, String secretKey, String chargeId) {
        ObjResponse response = new ObjResponse();
        logger.debug(requestId + " - chargeId: " + chargeId);
        logger.debug(requestId + " - secretKey: " + secretKey);
        try {
            if (!chargeId.equals("")) {
                Stripe.apiKey = secretKey;
                Charge charge = Charge.retrieve(chargeId);
                Stripe.apiKey = secretKey;
                String balanceTransactionId = charge.getBalanceTransaction();
                String currency = charge.getCurrency();
                logger.debug(requestId + " - balanceTransaction: " + balanceTransactionId);

                Stripe.apiKey = secretKey;
                BalanceTransaction balanceTransaction = BalanceTransaction.retrieve(balanceTransactionId);
                BigDecimal exchangeRate = balanceTransaction.getExchangeRate();
                if (isZeroDecimal(currency.toUpperCase())) {
                    exchangeRate = exchangeRate.divide(new BigDecimal(100));
                }
                logger.debug(requestId + " - exchangeRate: " + exchangeRate);
                Map<String, String> mapResponse = new HashMap<String, String>();
                mapResponse.put("exchangeRate", String.valueOf(exchangeRate));
                response.response = mapResponse;
                response.returnCode = 200;
            } else {
                response.returnCode = 406;
            }
        } catch(Exception ex) {
            logger.debug(requestId + " - getExchangeRate ERROR: " + CommonUtils.getError(ex));
            response.returnCode = 406;
        }
        return response.toString();
    }

    public String doTransfer(String secretKey, String chargeId, String bookId, String token, double amount, String currencyISO) {
        ObjResponse response = new ObjResponse();
        logger.debug(requestId + " - doTransfer " + amount + " to " + token + " for " + bookId + " from chargeId " + chargeId);
        try {
            DecimalFormat df = new DecimalFormat("#");
            int intAmount = Integer.parseInt(df.format(amount*100));

            TransferCreateParams transferParams =
                    TransferCreateParams.builder()
                            .setAmount((long)intAmount)
                            .setCurrency(currencyISO)
                            .setDestination(token)
                            //.setTransferGroup(bookId)
                            .setSourceTransaction(chargeId)
                            .setDescription("Transfer for booking #"+bookId)
                            .build();
            Stripe.apiKey = secretKey;
            Transfer transfer  = Transfer.create(transferParams);
            logger.debug(requestId + " - transfer = " + gson.toJson(transfer));
            Map<String, String> mapResponse = new HashMap<String, String>();
            mapResponse.put("transferId", transfer.getId());
            response.response = mapResponse;
            response.returnCode = 200;
        } catch (StripeException ex) {
            logger.debug(" - doTransfer ERROR: " + CommonUtils.getError(ex));
            Map<String, String> mapResponse = new HashMap<String, String>();
            mapResponse.put("requestId", ex.getRequestId());
            response.response = mapResponse;
            response.returnCode = 525;
        } catch (Exception ex) {
            logger.debug(requestId + " - doTransfer ERROR: " + CommonUtils.getError(ex));
            Map<String, String> mapResponse = new HashMap<String, String>();
            mapResponse.put("requestId", "");
            response.response = mapResponse;
            response.returnCode = 525;
        }
        return response.toString();
    }

    public double getStripeFee(String secretKey, String chargeId) {
        logger.debug(requestId + " - chargeId: " + chargeId);
        logger.debug(requestId + " - secretKey: " + secretKey);
        double processFee = 0.0;
        double refundFee = 0.0;
        try {
            if (!chargeId.equals("")) {
                Stripe.apiKey = secretKey;
                Charge charge = Charge.retrieve(chargeId);
                Stripe.apiKey = secretKey;
                String balanceTransactionId = charge.getBalanceTransaction();
                logger.debug(requestId + " - balanceTransaction: " + balanceTransactionId);
                Stripe.apiKey = secretKey;

                BalanceTransaction balanceTransaction = BalanceTransaction.retrieve(balanceTransactionId);
                for (BalanceTransaction.FeeDetail fee : balanceTransaction.getFeeDetails()) {
                    if (fee.getType().equals("stripe_fee")) {
                        processFee = fee.getAmount();
                        break;
                    }
                }
                logger.debug(requestId + " - processing fee: " + processFee);

                // check if transaction is partial refund

                RefundCollection refundCollection = charge.getRefunds();
                if (refundCollection != null && refundCollection.getData() != null && !refundCollection.getData().isEmpty()) {
                    String balanceRefundTransactionId = refundCollection.getData().get(0).getBalanceTransaction();
                    logger.debug(requestId + " - balanceRefundTransactionId: " + balanceRefundTransactionId);
                    Stripe.apiKey = secretKey;
                    BalanceTransaction balanceRefundTransaction = BalanceTransaction.retrieve(balanceRefundTransactionId);
                    for (BalanceTransaction.FeeDetail fee : balanceRefundTransaction.getFeeDetails()) {
                        if (fee.getType().equals("stripe_fee")) {
                            refundFee = fee.getAmount();
                            break;
                        }
                    }
                    logger.debug(requestId + " - refund fee: " + refundFee);
                }

            }
        } catch(Exception ex) {
            logger.debug(requestId + " - getStripeFee ERROR: " + CommonUtils.getError(ex));
        }
        return (processFee + refundFee)/100;
    }

    public String getChargeTransaction(String requestId, String secretKey, String paymentIntentId) {
        logger.debug(requestId + " - paymentIntentId: " + paymentIntentId);
        try {
            if (!paymentIntentId.equals("")) {
                Stripe.apiKey = secretKey;
                PaymentIntent paymentIntent = PaymentIntent.retrieve(paymentIntentId);
                String chargeId = paymentIntent.getLatestCharge() != null ? paymentIntent.getLatestCharge() : "";
                logger.debug(requestId + " - chargeId: " + chargeId);
                return chargeId;
            }
        } catch(Exception ex) {
            logger.debug(requestId + " - getChargeTransaction ERROR: " + CommonUtils.getError(ex));
        }
        return "";
    }

    public String getTransactionStatus(String requestId, String secretKey, String chargeId) {
        logger.debug(requestId + " - chargeId: " + chargeId);
        try {
            if (!chargeId.equals("")) {
                Stripe.apiKey = secretKey;
                Charge charge = Charge.retrieve(chargeId);
                String paymentIntentId = charge.getPaymentIntent();
                Stripe.apiKey = secretKey;
                PaymentIntent paymentIntent = PaymentIntent.retrieve(paymentIntentId);
                String status = paymentIntent.getStatus();
                logger.debug(requestId + " - status: " + status);
                return status;
            }
        } catch(Exception ex) {
            logger.debug(requestId + " - getTransactionStatus ERROR: " + CommonUtils.getError(ex));
        }
        return "";
    }

    public String getPaymentLink(String requestId, String secretKey, String bookId, double amount, String currencyISO, double fee, String type, String transactionId) {
        ObjResponse response = new ObjResponse();
        try {
            int intAmount;
            if (isZeroDecimal(currencyISO)) {
                intAmount = (int) amount;
            } else {
                DecimalFormat df = new DecimalFormat("#");
                intAmount = Integer.parseInt(df.format(amount * 100));
            }
            Map<String, Object> paramProduct = new HashMap<>();
            paramProduct.put("name", "Booking #" + bookId);
            Stripe.apiKey = secretKey;
            Product product = Product.create(paramProduct);

            Map<String, Object> paramPrice = new HashMap<>();
            paramPrice.put("unit_amount", intAmount);
            paramPrice.put("currency", currencyISO);
            paramPrice.put("product", product.getId());
            Stripe.apiKey = secretKey;
            Price price = Price.create(paramPrice);

            List<Object> lineItems = new ArrayList<>();
            Map<String, Object> lineItem1 = new HashMap<>();
            lineItem1.put("price", price.getId());
            lineItem1.put("quantity", 1);
            lineItems.add(lineItem1);

            Map<String, String> metadata = new HashMap<>();
            if (type.equals("prepaid") || type.equals("postpaid")) {
                metadata.put("transactionId", transactionId);
            } else {
                metadata.put("bookId", bookId);
            }
            metadata.put("type", type);

            Map<String, Object> paramPaymentIntent = new HashMap<>();
            paramPaymentIntent.put("metadata", metadata);

            Map<String, Object> completed_sessions = new HashMap<>();
            completed_sessions.put("limit", 1);

            Map<String, Object> paramRestrictions = new HashMap<>();
            paramRestrictions.put("completed_sessions", completed_sessions);

            Map<String, Object> params = new HashMap<>();
            params.put("line_items", lineItems);
            params.put("payment_intent_data", paramPaymentIntent);
            params.put("restrictions", paramRestrictions);

            Stripe.apiKey = secretKey;
            PaymentLink paymentLink =
                    PaymentLink.create(params);
            Map<String, Object> mapResponse = new HashMap<>();
            mapResponse.put("paymentLinkId", paymentLink.getId());
            mapResponse.put("paymentLink", paymentLink.getUrl());
            mapResponse.put("amount", Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(amount)));
            mapResponse.put("transactionFee", Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(fee)));

            response.returnCode = 200;
            response.response = mapResponse;
            return response.toString();
        } catch (StripeException ex) {
            logger.debug(requestId + " - getPaymentLink ERROR: " + CommonUtils.getError(ex));
            Map<String,String> mapResponse = new HashMap<>();
            mapResponse.put("message", ex.getMessage());
            response.returnCode = 525;
            response.response = mapResponse;
            return response.toString();
        } catch(Exception ex) {
            logger.debug(requestId + " - getPaymentLink ERROR: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response.toString();
        }
    }

    public String getWalletType(String requestId, String secretKey, String paymentIntentId) {
        logger.debug(requestId + " - paymentIntentId: " + paymentIntentId);
        String type = "";
        try {
            if (!paymentIntentId.equals("")) {
                Stripe.apiKey = secretKey;
                PaymentIntent paymentIntent = PaymentIntent.retrieve(paymentIntentId);
                String chargeId = paymentIntent.getLatestCharge() != null ? paymentIntent.getLatestCharge() : "";
                logger.debug(requestId + " - chargeId: " + chargeId);
                Stripe.apiKey = secretKey;
                Charge charge = Charge.retrieve(chargeId);
                logger.debug(requestId + " - charge: " + gson.toJson(charge));
                Charge.PaymentMethodDetails.Card.Wallet wallet = charge.getPaymentMethodDetails().getCard().getWallet();
                type = wallet != null && wallet.getType() != null ? wallet.getType() : ""; // apple_pay, google_pay
            }
        } catch(Exception ex) {
            logger.debug(requestId + " - getChargeTransaction ERROR: " + CommonUtils.getError(ex));
        }
        return type;
    }

    /*public boolean cancelSessionCheckoutFromPaymentIntent(String requestId, String secretKey, String paymentIntentId) {
        logger.debug(requestId + " - paymentIntentId: " + paymentIntentId);
        try {
            if (!paymentIntentId.equals("")) {
                Map<String, Object> params = new HashMap<>();
                params.put("limit", 1);
                params.put("payment_intent", paymentIntentId);
                Stripe.apiKey = secretKey;
                SessionCollection sessions = Session.list(params);
                String sessionId = sessions.getData().get(0).getId();
                logger.debug(requestId + " - sessionId: " + sessionId);
                Stripe.apiKey = secretKey;
                Session session = Session.retrieve(sessionId);
                Stripe.apiKey = secretKey;
                Session updatedSession = session.expire();                ;
                logger.debug(requestId + " - status: " + updatedSession.getStatus());
                if (updatedSession.getStatus().equals("expired"))
                    return true;
            }
        } catch(Exception ex) {
            logger.debug(requestId + " - getChargeTransaction ERROR: " + CommonUtils.getError(ex));
        }
        return false;
    }*/

    public boolean deactivatePaymentLink(String requestId, String secretKey, String paymentLinkId) {
        logger.debug(requestId + " - paymentLinkId: " + paymentLinkId);
        try {
            if (!paymentLinkId.isEmpty()) {
                Stripe.apiKey = secretKey;
                PaymentLink paymentLink = PaymentLink.retrieve(paymentLinkId);

                Map<String, Object> params = new HashMap<>();
                params.put("active", false);

                Stripe.apiKey = secretKey;
                PaymentLink updatedPaymentLink = paymentLink.update(params);
                logger.debug(requestId + " - updatedPaymentLink: " + updatedPaymentLink.getActive());

                // continue to expire all generated SessionCheckouts
                Map<String, Object> paramCheckout = new HashMap<>();
                paramCheckout.put("limit", 1);
                paramCheckout.put("payment_link", paymentLinkId);
                Stripe.apiKey = secretKey;
                SessionCollection sessions = Session.list(paramCheckout);
                for (Session session : sessions.getData()) {
                    logger.debug(requestId + " - sessionId: " + session.getId() + " - status: " + session.getStatus());
                    if (session.getStatus().equals("open")) {
                        Stripe.apiKey = secretKey;
                        Session updatedSession = session.expire();
                        logger.debug(requestId + " - updated status: " + updatedSession.getStatus());
                    }
                }
                return true;
            }
        } catch(Exception ex) {
            logger.debug(requestId + " - deactivatePaymentLink ERROR: " + CommonUtils.getError(ex));
        }
        return false;
    }

    public String getCheckoutSession(String requestId, String secretKey, String itemId, double amount, String currencyISO, double fee) {
        ObjResponse response = new ObjResponse();
        try {
            int intAmount;
            if (isZeroDecimal(currencyISO)) {
                intAmount = (int) amount;
            } else {
                DecimalFormat df = new DecimalFormat("#");
                intAmount = Integer.parseInt(df.format(amount*100));
            }
            Map<String, Object> paramProduct = new HashMap<>();
            paramProduct.put("name", "#" + itemId);
            Stripe.apiKey = secretKey;
            Product product = Product.create(paramProduct);

            Map<String, Object> paramPrice = new HashMap<>();
            paramPrice.put("unit_amount", intAmount);
            paramPrice.put("currency", currencyISO);
            paramPrice.put("product", product.getId());
            Stripe.apiKey = secretKey;
            Price price = Price.create(paramPrice);

            List<Object> lineItems = new ArrayList<>();
            Map<String, Object> lineItem1 = new HashMap<>();
            lineItem1.put("price", price.getId());
            lineItem1.put("quantity", 1);
            lineItems.add(lineItem1);

            Map<String,String> metadata = new HashMap<>();
            metadata.put("bookId", itemId);
            metadata.put("type", "checkoutSession");

            Map<String, Object> paramPaymentIntent = new HashMap<>();
            paramPaymentIntent.put("metadata", metadata);

            Map<String, Object> params = new HashMap<>();
            params.put("line_items", lineItems);
            params.put("payment_intent_data", paramPaymentIntent);

            Stripe.apiKey = secretKey;
            Session session = Session.create(params);
            Map<String,Object> mapResponse = new HashMap<>();
            mapResponse.put("checkoutSessionId", session.getId());
            mapResponse.put("amount", Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(amount)));
            mapResponse.put("transactionFee", Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(fee)));

            response.returnCode = 200;
            response.response = mapResponse;
            return response.toString();
        } catch(Exception ex) {
            logger.debug(requestId + " - getCheckoutSession ERROR: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response.toString();
        }
    }

    public boolean expireCheckoutSession(String requestId, String secretKey, String checkoutSessionId) {
        logger.debug(requestId + " - checkoutSessionId: " + checkoutSessionId);
        try {
            if (!checkoutSessionId.isEmpty()) {
                Stripe.apiKey = secretKey;
                Session session = Session.retrieve(checkoutSessionId);

                Stripe.apiKey = secretKey;
                Session updatedSession = session.expire();
                logger.debug(requestId + " - updated status: " + updatedSession.getStatus());
                return true;
            }
        } catch(Exception ex) {
            logger.debug(requestId + " - expireCheckoutSession ERROR: " + CommonUtils.getError(ex));
        }
        return false;
    }

    public String getPaymentIntentForInvoice(String bookId, String secretKey, double amount, String currencyISO){
        ObjResponse getClientSecretResponse = new ObjResponse();
        try {
            int intAmount = 0;
            if (isZeroDecimal(currencyISO)) {
                intAmount = (int) amount;
            } else {
                DecimalFormat df = new DecimalFormat("#");
                intAmount = Integer.parseInt(df.format(amount*100));
            }

            logger.debug(requestId + " - intAmount: " + intAmount);
            Map<String, Object> automaticPaymentMethods = new HashMap<>();
            automaticPaymentMethods.put("enabled", true);

            Map<String, Object> metadata = new HashMap<>();
            metadata.put("type", "invoice");
            metadata.put("invoiceId", bookId);

            Map<String, Object> params = new HashMap<>();
            params.put("amount", (long) intAmount);
            params.put("currency", currencyISO);
            params.put("description", bookId);
            params.put("automatic_payment_methods", automaticPaymentMethods);
            params.put("metadata", metadata);

            Stripe.apiKey = secretKey;
            PaymentIntent intent = PaymentIntent.create(params);
            String clientSecret = intent.getClientSecret();

            Map<String, Object> response = new HashMap<String, Object>();
            response.put("clientSecret", clientSecret);
            response.put("paymentIntentId", intent.getId());
            response.put("smallestValue", intAmount);
            /*if (returnAmount) {
                if (isZeroDecimal(currencyISO)) {
                    response.put("amount", intAmount);
                } else {
                    response.put("amount", Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(amount)));
                }
            }*/
            getClientSecretResponse.response = response;
            getClientSecretResponse.returnCode = 116;
            return getClientSecretResponse.toString();
        } catch (StripeException e) {
            e.printStackTrace();
            Map<String, Object> response = new HashMap<String, Object>();
            response.put("message", e.getMessage());
            getClientSecretResponse.response = response;
            getClientSecretResponse.returnCode = 525;

        }
        return getClientSecretResponse.toString();
    }
    public String getChargeDataFromPaymentIntent(String requestId, String secretKey, String paymentIntentId) {
        ObjResponse objResponse = new ObjResponse();
        logger.debug(requestId + " - paymentIntentId: " + paymentIntentId);
        try {
            if (!paymentIntentId.isEmpty()) {
                Stripe.apiKey = secretKey;
                PaymentIntent paymentIntent = PaymentIntent.retrieve(paymentIntentId);
                String chargeId = paymentIntent.getLatestCharge() != null ? paymentIntent.getLatestCharge() : "";
                logger.debug(requestId + " - chargeId: " + chargeId);
                Stripe.apiKey = secretKey;
                Charge charge = Charge.retrieve(chargeId);
                String cardMask = "XXXXXXXXXXXX" + charge.getPaymentMethodDetails().getCard().getLast4();
                String cardType = charge.getPaymentMethodDetails().getCard().getBrand();
                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("chargeId", chargeId);
                mapResponse.put("cardMask", cardMask);
                mapResponse.put("cardType", cardType);
                objResponse.returnCode = 200;
                objResponse.response = mapResponse;
            } else {
                objResponse.returnCode = 406;
            }
        } catch(Exception ex) {
            logger.debug(requestId + " - getChargeTransaction ERROR: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
        }
        return objResponse.toString();
    }

    public void cancelPaymentIntent(String requestId, String secretKey, String paymentIntentId) {
        try {
            if (!paymentIntentId.isEmpty()) {
                Stripe.apiKey = secretKey;
                PaymentIntent paymentIntent = PaymentIntent.retrieve(paymentIntentId);
                if (!paymentIntent.getStatus().equals("succeeded") && !paymentIntent.getStatus().equals("canceled")) {
                    Stripe.apiKey = secretKey;
                    PaymentIntent updatedPaymentIntent = paymentIntent.cancel();
                    logger.debug(requestId + " - updated status: " + updatedPaymentIntent.getStatus());
                }
            }
        } catch(Exception ex) {
            logger.debug(requestId + " - cancelPaymentIntent ERROR: " + CommonUtils.getError(ex));
        }
    }

    public String getPaymentTypeFromPaymentIntent(String requestId, String secretKey, String paymentIntentId) {
        ObjResponse objResponse = new ObjResponse();
        logger.debug(requestId + " - paymentIntentId: " + paymentIntentId);
        try {
            if (!paymentIntentId.isEmpty()) {
                Stripe.apiKey = secretKey;
                PaymentIntent paymentIntent = PaymentIntent.retrieve(paymentIntentId);
                String chargeId = paymentIntent.getLatestCharge() != null ? paymentIntent.getLatestCharge() : "";
                logger.debug(requestId + " - chargeId: " + chargeId);
                Stripe.apiKey = secretKey;
                Charge charge = Charge.retrieve(chargeId);
                return charge.getPaymentMethodDetails().getType();
            }
        } catch(Exception ex) {
            logger.debug(requestId + " - getChargeTransaction ERROR: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
        }
        return "";
    }
}
