
package com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AuthorizeResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AuthorizeResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AcquirerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillingDetails" type="{http://schemas.firstatlanticcommerce.com/gateway/data}BillingDetails" minOccurs="0"/>
 *         &lt;element name="CreditCardTransactionResults" type="{http://schemas.firstatlanticcommerce.com/gateway/data}CreditCardTransactionResults" minOccurs="0"/>
 *         &lt;element name="CustomData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IPGeoLocationResults" type="{http://schemas.firstatlanticcommerce.com/gateway/data}IPGeoLocationResults" minOccurs="0"/>
 *         &lt;element name="MerchantId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrderNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Signature" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SignatureMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BinCheckResults" type="{http://schemas.firstatlanticcommerce.com/gateway/data}BinCheckResults" minOccurs="0"/>
 *         &lt;element name="FraudControlResults" type="{http://schemas.firstatlanticcommerce.com/gateway/data}FraudControlResults" minOccurs="0"/>
 *         &lt;element name="ShippingDetails" type="{http://schemas.firstatlanticcommerce.com/gateway/data}ShippingDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AuthorizeResponse", propOrder = {
    "acquirerId",
    "billingDetails",
    "creditCardTransactionResults",
    "customData",
    "ipGeoLocationResults",
    "merchantId",
    "orderNumber",
    "signature",
    "signatureMethod",
    "binCheckResults",
    "fraudControlResults",
    "shippingDetails"
})
public class AuthorizeResponse {

    @XmlElement(name = "AcquirerId", nillable = true)
    protected String acquirerId;
    @XmlElement(name = "BillingDetails", nillable = true)
    protected BillingDetails billingDetails;
    @XmlElement(name = "CreditCardTransactionResults", nillable = true)
    protected CreditCardTransactionResults creditCardTransactionResults;
    @XmlElement(name = "CustomData", nillable = true)
    protected String customData;
    @XmlElement(name = "IPGeoLocationResults", nillable = true)
    protected IPGeoLocationResults ipGeoLocationResults;
    @XmlElement(name = "MerchantId", nillable = true)
    protected String merchantId;
    @XmlElement(name = "OrderNumber", nillable = true)
    protected String orderNumber;
    @XmlElement(name = "Signature", nillable = true)
    protected String signature;
    @XmlElement(name = "SignatureMethod", nillable = true)
    protected String signatureMethod;
    @XmlElement(name = "BinCheckResults", nillable = true)
    protected BinCheckResults binCheckResults;
    @XmlElement(name = "FraudControlResults", nillable = true)
    protected FraudControlResults fraudControlResults;
    @XmlElement(name = "ShippingDetails", nillable = true)
    protected ShippingDetails shippingDetails;

    /**
     * Gets the value of the acquirerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcquirerId() {
        return acquirerId;
    }

    /**
     * Sets the value of the acquirerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcquirerId(String value) {
        this.acquirerId = value;
    }

    /**
     * Gets the value of the billingDetails property.
     * 
     * @return
     *     possible object is
     *     {@link BillingDetails }
     *     
     */
    public BillingDetails getBillingDetails() {
        return billingDetails;
    }

    /**
     * Sets the value of the billingDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingDetails }
     *     
     */
    public void setBillingDetails(BillingDetails value) {
        this.billingDetails = value;
    }

    /**
     * Gets the value of the creditCardTransactionResults property.
     * 
     * @return
     *     possible object is
     *     {@link CreditCardTransactionResults }
     *     
     */
    public CreditCardTransactionResults getCreditCardTransactionResults() {
        return creditCardTransactionResults;
    }

    /**
     * Sets the value of the creditCardTransactionResults property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditCardTransactionResults }
     *     
     */
    public void setCreditCardTransactionResults(CreditCardTransactionResults value) {
        this.creditCardTransactionResults = value;
    }

    /**
     * Gets the value of the customData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomData() {
        return customData;
    }

    /**
     * Sets the value of the customData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomData(String value) {
        this.customData = value;
    }

    /**
     * Gets the value of the ipGeoLocationResults property.
     * 
     * @return
     *     possible object is
     *     {@link IPGeoLocationResults }
     *     
     */
    public IPGeoLocationResults getIPGeoLocationResults() {
        return ipGeoLocationResults;
    }

    /**
     * Sets the value of the ipGeoLocationResults property.
     * 
     * @param value
     *     allowed object is
     *     {@link IPGeoLocationResults }
     *     
     */
    public void setIPGeoLocationResults(IPGeoLocationResults value) {
        this.ipGeoLocationResults = value;
    }

    /**
     * Gets the value of the merchantId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * Sets the value of the merchantId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMerchantId(String value) {
        this.merchantId = value;
    }

    /**
     * Gets the value of the orderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * Sets the value of the orderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderNumber(String value) {
        this.orderNumber = value;
    }

    /**
     * Gets the value of the signature property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSignature() {
        return signature;
    }

    /**
     * Sets the value of the signature property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSignature(String value) {
        this.signature = value;
    }

    /**
     * Gets the value of the signatureMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSignatureMethod() {
        return signatureMethod;
    }

    /**
     * Sets the value of the signatureMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSignatureMethod(String value) {
        this.signatureMethod = value;
    }

    /**
     * Gets the value of the binCheckResults property.
     * 
     * @return
     *     possible object is
     *     {@link BinCheckResults }
     *     
     */
    public BinCheckResults getBinCheckResults() {
        return binCheckResults;
    }

    /**
     * Sets the value of the binCheckResults property.
     * 
     * @param value
     *     allowed object is
     *     {@link BinCheckResults }
     *     
     */
    public void setBinCheckResults(BinCheckResults value) {
        this.binCheckResults = value;
    }

    /**
     * Gets the value of the fraudControlResults property.
     * 
     * @return
     *     possible object is
     *     {@link FraudControlResults }
     *     
     */
    public FraudControlResults getFraudControlResults() {
        return fraudControlResults;
    }

    /**
     * Sets the value of the fraudControlResults property.
     * 
     * @param value
     *     allowed object is
     *     {@link FraudControlResults }
     *     
     */
    public void setFraudControlResults(FraudControlResults value) {
        this.fraudControlResults = value;
    }

    /**
     * Gets the value of the shippingDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ShippingDetails }
     *     
     */
    public ShippingDetails getShippingDetails() {
        return shippingDetails;
    }

    /**
     * Sets the value of the shippingDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShippingDetails }
     *     
     */
    public void setShippingDetails(ShippingDetails value) {
        this.shippingDetails = value;
    }

}
