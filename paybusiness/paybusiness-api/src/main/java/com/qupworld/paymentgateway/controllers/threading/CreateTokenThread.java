package com.qupworld.paymentgateway.controllers.threading;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.entities.CreditEnt;
import com.pg.util.SecurityUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by qup on 23/03/2020.
 */
@SuppressWarnings("unchecked")
public class CreateTokenThread extends Thread {
    private static String addlogs = ServerConfig.logs_server+"/api/service/addLogs";
    Logger logger = LogManager.getLogger(CreateTokenThread.class);
    public String token;
    public CreditEnt creditForm;
    public String requestId;
    public int addMaskToken(final String token, final String userId, final CreditEnt creditEnt) {
        int returnCode = 437;
        try {
            String key = "";
            String data = "";
            Gson gson = new Gson();
            String dataStr = token + gson.toJson(creditEnt);
            key =  SecurityUtil.getKey();
            data = SecurityUtil.encryptProfile(key, dataStr);
            HttpResponse<JsonNode> jsonResponse = null;
            String body = "{\"logData\":\""+data+"\",\"logId\":\""+token+"\",\"userId\":\""+userId+"\",\"type\":\"\"}";
            jsonResponse = Unirest.post(addlogs)
                    .basicAuth(ServerConfig.logs_username, ServerConfig.logs_password)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .body(body)
                    .asJson();
            returnCode =  (int)jsonResponse.getBody().getObject().get("returnCode");
            return returnCode;
        } catch (Exception e) {
            e.printStackTrace();
        }finally {

        }
        return returnCode;

    }
    @Override
    public void run() {
        // logger.debug("save to mySQL");
        try {
            int result =  this.addMaskToken(token, creditForm.userId, creditForm);
            logger.debug(requestId + " - add token: " + token + " - result = " +result);

        } catch (Exception ex) {
            ex.printStackTrace();
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            logger.debug(requestId + " - save to mySQL: " + errors.toString());
        }

    }
}
