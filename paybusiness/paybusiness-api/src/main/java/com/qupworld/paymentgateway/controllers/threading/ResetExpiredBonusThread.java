package com.qupworld.paymentgateway.controllers.threading;

import com.qupworld.paymentgateway.controllers.UpdateDriverCreditBalance;
import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.SQLDao;
import com.pg.util.CommonArrayUtils;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Created by qup on 23/03/2020.
 */
@SuppressWarnings("unchecked")
public class ResetExpiredBonusThread extends Thread {

    final static Logger logger = LogManager.getLogger(ResetExpiredBonusThread.class);
    public String fleetId;
    public String currencyISO;
    public String driverId;
    public MongoDao mongoDao;
    public SQLDao sqlDao;
    public boolean delivery;
    public String requestId;

    @Override
    public void run() {
        try {
            if (!driverId.isEmpty()) {
                doUpdate(driverId);

            } else {
                int i = 0;
                int size = 100;
                boolean valid = true;
                while (valid) {
                    List<String> listDriver = mongoDao.getActiveDrivers(fleetId, delivery, i * size, size);
                    int querySize = listDriver.size();
                    for (String userId : listDriver) {
                        doUpdate(userId);
                    }

                    if (querySize < size) {
                        valid = false;
                    }
                    i++;
                }
            }

        } catch(Exception ex) {
            logger.debug(requestId + " - ResetExpiredBonusThread exception: "+ CommonUtils.getError(ex));
        }

    }

    private void doUpdate(String userId) {
        try {
            List<Double> usage = sqlDao.getUsageWalletBalance(userId, currencyISO);
            double cashIn = Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(usage.get(0)));
            double cashOut = Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(usage.get(1)));
            double currentBalance = Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(usage.get(2)));
            logger.debug(requestId + " - cashIn (topUpMOLPay, cashWallet): " + cashIn);
            logger.debug(requestId + " - cashOut (bookingEarning): " + cashOut);
            logger.debug(requestId + " - currentBalance: " + currentBalance);
            if (currentBalance != 0) {
                if (delivery) {
                    // ignore all drivers have booking deduction >= 50
                    if (cashOut < 50) {
                        if (cashIn > 0) {
                            // if there is top-up transaction then reset balance and add top-up value
                            double amount = Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(currentBalance - cashIn));
                            updateBalance(userId, -amount);
                        } else {
                            updateBalance(userId, -currentBalance);
                        }
                    }
                } else {
                    // ignore all drivers have booking deduction >= 250
                    if (cashOut < 250) {
                        if (cashIn > 0) {
                            // driver topped-up some money
                            // keep the topped-up amount, reset the rest (currentBalance - cashIn)
                            double amount = Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(currentBalance - cashIn));
                            updateBalance(userId, -amount);
                        } else {
                            // driver did not top-up any money
                            // reset current balance
                            updateBalance(userId, -currentBalance);
                        }
                    }
                }
            }
        } catch (Exception ex) {

        }
    }

    private void updateBalance(String driverId, double amount){
        UpdateDriverCreditBalance updateBalance = new UpdateDriverCreditBalance();
        updateBalance.type = CommonArrayUtils.DRIVER_WALLET_TYPE[2];
        updateBalance.fleetId = fleetId;
        updateBalance.driverId = driverId;
        updateBalance.amount = amount;
        updateBalance.currencyISO = currencyISO;
        updateBalance.mongoDao = mongoDao;
        updateBalance.sqlDao = sqlDao;
        updateBalance.reason = "Sign-up bonus expires";
        updateBalance.additionalData.put("operatorId", "");
        updateBalance.additionalData.put("operatorName", "");
        updateBalance.requestId = requestId;
        String result = updateBalance.doUpdate();
        logger.debug(requestId + " - updateBalance for driver "+ driverId +" result = " + result);
    }

}
