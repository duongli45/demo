
package com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IPGeoLocationResult" type="{http://schemas.firstatlanticcommerce.com/gateway/data}IPGeoLocationResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ipGeoLocationResult"
})
@XmlRootElement(name = "IPGeoLocationResponse")
public class IPGeoLocationResponse {

    @XmlElement(name = "IPGeoLocationResult", nillable = true)
    protected com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.IPGeoLocationResponse ipGeoLocationResult;

    /**
     * Gets the value of the ipGeoLocationResult property.
     * 
     * @return
     *     possible object is
     *     {@link com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.IPGeoLocationResponse }
     *     
     */
    public com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.IPGeoLocationResponse getIPGeoLocationResult() {
        return ipGeoLocationResult;
    }

    /**
     * Sets the value of the ipGeoLocationResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.IPGeoLocationResponse }
     *     
     */
    public void setIPGeoLocationResult(com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.IPGeoLocationResponse value) {
        this.ipGeoLocationResult = value;
    }

}
