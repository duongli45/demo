package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.google.gson.Gson;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.Base64Encoder;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import okhttp3.*;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
public class PayWayUtil {

    private static final Logger logger = LogManager.getLogger(PayWayUtil.class);
    private String requestId;
    private String MERCHANT_ID;
    private String API_KEY;
    private String PUBLIC_KEY;
    private String REQUEST_URL;
    private double SKIP_AMOUNT;
    private SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMddHHmmss");
    private final Gson gson = new Gson();

    public PayWayUtil(String _requestId, String merchantId, String apiKey, String publicKey, double skipAuthenAmount, String requestUrl) {
        requestId = _requestId;
        MERCHANT_ID = merchantId;
        API_KEY = apiKey;
        PUBLIC_KEY = publicKey;
        SKIP_AMOUNT = skipAuthenAmount;
        REQUEST_URL = requestUrl;
    }

    public String initiateCreditForm(String orderId, Map<String,String> mapData) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,Object> mapResponse = new HashMap<>();
        try {
            String returnUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.PAYWAY+"?urlAction=token";
            returnUrl = Base64Encoder.encodeString(returnUrl);
            String successUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.PAYWAY+"?urlAction=success";
            String ctid = mapData.get("userId");
            String firstName = mapData.get("firstName");
            String lastName = mapData.get("lastName");

            String rawString = MERCHANT_ID + ctid + orderId;
            logger.debug(requestId + " - rawString: " +  rawString);
            String hash = sha512(rawString, API_KEY);
            logger.debug(requestId + " - hash: " +  hash);

            String urlAction = REQUEST_URL + "/payment-gateway/v1/cof/initial";

            HttpClient client = HttpClientBuilder.create().build();

            ArrayList<NameValuePair> postParameters = new ArrayList<>();
            postParameters.add(new BasicNameValuePair("merchant_id", MERCHANT_ID));
            postParameters.add(new BasicNameValuePair("return_param", orderId));
            postParameters.add(new BasicNameValuePair("firstname", firstName));
            postParameters.add(new BasicNameValuePair("lastname", lastName));
            postParameters.add(new BasicNameValuePair("hash", hash));
            postParameters.add(new BasicNameValuePair("return_url", returnUrl));
            postParameters.add(new BasicNameValuePair("ctid", ctid));
            postParameters.add(new BasicNameValuePair("continue_add_card_success_url", successUrl));

            HttpPost httpRequest = new HttpPost(urlAction);
            httpRequest.addHeader("Content-Type", "application/x-www-form-urlencoded");
            httpRequest.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));

            HttpResponse httpResponse = client.execute(httpRequest);
            int statusCode = httpResponse.getStatusLine().getStatusCode();
            logger.debug(requestId + " - statusCode: " + statusCode);
            if (statusCode == 302) {
                Header[] headers = httpResponse.getAllHeaders();
                String result = "";
                for (Header header : headers) {
                    if (header.getName().equalsIgnoreCase("location")) {
                        result = header.getValue();
                        break;
                    }
                }
                logger.debug(requestId + " - location: " + result);
                if (result == null || result.isEmpty()) {
                    objResponse.returnCode = 525;
                } else {
                    Map<String, Object> mapPayWay = new HashMap<>();
                    mapPayWay.put("action", urlAction);
                    mapPayWay.put("merchant_id", MERCHANT_ID);
                    mapPayWay.put("return_param", orderId);
                    mapPayWay.put("firstname", firstName);
                    mapPayWay.put("lastname", lastName);
                    mapPayWay.put("hash", hash);
                    mapPayWay.put("return_url", returnUrl);
                    mapPayWay.put("ctid", ctid);
                    mapPayWay.put("continue_add_card_success_url", successUrl);

                    objResponse.returnCode = 200;
                    mapResponse.put("3ds_url", result);
                    mapResponse.put("type", "link");
                    mapResponse.put("transactionId", orderId);
                    mapResponse.put("jsonPayWay", mapPayWay);
                    mapResponse.put("redirect", true);
                    mapResponse.put("gateway", KeysUtil.PAYWAY);
                    objResponse.response = mapResponse;
                }
            } else {
                objResponse.returnCode = 525;
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
        }
        return objResponse.toString();
    }

    public String payByToken(String orderId, String bookId, String type, String paywayToken, double amount, String currencyISO, Map<String,String> mapData) {
        ObjResponse objResponse = new ObjResponse();
        try {
            amount = Double.parseDouble(KeysUtil.DECIMAL_FORMAT0.format(amount));
            String[] arrToken = paywayToken.split(KeysUtil.TEMPKEY);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date time = calendar.getTime();
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            String req_time = sdf.format(time);
            String returnUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.PAYWAY+"?urlAction="+type;
            returnUrl = Base64Encoder.encodeString(returnUrl);
            String successUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.PAYWAY+"?urlAction=success";
            String ctid = arrToken[0];
            String token = arrToken[1];
            String firstName = mapData.get("firstName") != null ? mapData.get("firstName") : "";
            String lastName = mapData.get("lastName") != null ? mapData.get("lastName") : "";
            String payment_option = "cards";

            String items = "";
            bookId = bookId != null && !bookId.isEmpty() ? bookId : "";
            if (!bookId.isEmpty()) {
                JSONObject item = new JSONObject();
                item.put("name", "Booking " + bookId);
                item.put("quantity", 1);
                item.put("price", amount);
                JSONArray arrItems = new JSONArray();
                arrItems.put(item);
                items = Base64Encoder.encodeString(arrItems.toString());
            }

            String requestType = type.equals("preAuth") ? "pre-auth" : "purchase";
            String requestString = req_time + MERCHANT_ID + orderId + amount + items + ctid + token + firstName + lastName +
                    requestType + payment_option + returnUrl + successUrl + currencyISO;
            logger.debug(requestId + " - requestString: " +  requestString);
            String hash = sha512(requestString, API_KEY);
            logger.debug(requestId + " - hash: " +  hash);
            String urlAction = REQUEST_URL + "/payment-gateway/v1/payments/purchase";
            HttpClient client = HttpClientBuilder.create().build();

            ArrayList<NameValuePair> postParameters = new ArrayList<>();
            postParameters.add(new BasicNameValuePair("req_time", req_time));
            postParameters.add(new BasicNameValuePair("merchant_id", MERCHANT_ID));
            postParameters.add(new BasicNameValuePair("tran_id", orderId));
            postParameters.add(new BasicNameValuePair("firstname", firstName));
            postParameters.add(new BasicNameValuePair("lastname", lastName));
            postParameters.add(new BasicNameValuePair("amount", String.valueOf(amount)));
            postParameters.add(new BasicNameValuePair("items", items));
            postParameters.add(new BasicNameValuePair("hash", hash));
            postParameters.add(new BasicNameValuePair("type", requestType));
            postParameters.add(new BasicNameValuePair("payment_option", payment_option));
            postParameters.add(new BasicNameValuePair("return_url", returnUrl));
            postParameters.add(new BasicNameValuePair("ctid", ctid));
            postParameters.add(new BasicNameValuePair("pwt", token));
            postParameters.add(new BasicNameValuePair("continue_success_url", successUrl));
            postParameters.add(new BasicNameValuePair("currency", currencyISO));
            if (amount < SKIP_AMOUNT) {
                postParameters.add(new BasicNameValuePair("skip_cof3ds", String.valueOf(1)));
            }
            HttpPost httpRequest = new HttpPost(urlAction);
            httpRequest.addHeader("Content-Type", "application/x-www-form-urlencoded");
            httpRequest.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));

            HttpResponse httpResponse = client.execute(httpRequest);
            int statusCode = httpResponse.getStatusLine().getStatusCode();
            logger.debug(requestId + " - statusCode: " + statusCode);
            Map<String,Object> mapResponse = new HashMap<>();
            if (statusCode == 302) {
                Header[] headers = httpResponse.getAllHeaders();
                String result = "";
                for (Header header : headers) {
                    if (header.getName().equalsIgnoreCase("location")) {
                        result = header.getValue();
                        break;
                    }
                }
                logger.debug(requestId + " - location: " + result);
                if (result == null || result.isEmpty()) {
                    objResponse.returnCode = 525;
                } else {
                    Map<String,Object> mapPayWay = new HashMap<>();
                    mapPayWay.put("action", urlAction);
                    mapPayWay.put("req_time", req_time);
                    mapPayWay.put("merchant_id", MERCHANT_ID);
                    mapPayWay.put("tran_id", orderId);
                    mapPayWay.put("firstname", firstName);
                    mapPayWay.put("lastname", lastName);
                    mapPayWay.put("amount", String.valueOf(amount));
                    mapPayWay.put("hash", hash);
                    mapPayWay.put("type", requestType);
                    mapPayWay.put("payment_option", payment_option);
                    mapPayWay.put("return_url", returnUrl);
                    mapPayWay.put("ctid", ctid);
                    mapPayWay.put("pwt", token);
                    mapPayWay.put("continue_success_url", successUrl);
                    mapPayWay.put("currency", currencyISO);

                    mapResponse.put("3ds_url", result);
                    mapResponse.put("deepLinkUrl", result);
                    mapResponse.put("type", "link");
                    mapResponse.put("transactionId", orderId);
                    mapResponse.put("openInApp", true);
                    mapResponse.put("jsonPayWay", mapPayWay);
                    objResponse.response = mapResponse;
                    objResponse.returnCode = 528;
                }
                return objResponse.toString();
            } else {
                String result = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
                logger.debug(requestId + " - response: " + result);
                /*{
                    "tran_id": "2023100214393535620",
                    "payment_status": {
                        "status": 0,
                        "description": "pre-auth"
                    }
                }*/
                /*{
                    "tran_id": "2023100214393535620",
                    "payment_status": {
                        "status": 0,
                        "description": "approved"
                    }
                }*/

                if (!result.isEmpty()) {
                    JSONObject jsonResult = new JSONObject(result);
                    JSONObject objStatus = jsonResult.has("payment_status") ? jsonResult.getJSONObject("payment_status") : null;
                    String description = objStatus != null && objStatus.has("description") ? objStatus.getString("description") : "";
                    int status = objStatus != null && objStatus.has("status") ? objStatus.getInt("status") : -1;

                    if (status == 0 && (description.equals("approved") || description.equals("pre-auth"))) {
                        String cardType = mapData.get("cardType") != null ? mapData.get("cardType") : "";
                        String cardMask = mapData.get("cardMask") != null ? mapData.get("cardMask") : "";
                        if (type.equals("preAuth")) {
                            mapResponse.put("authId", orderId);
                            mapResponse.put("authAmount", String.valueOf(amount));
                            mapResponse.put("authCode", "");
                            mapResponse.put("authToken", paywayToken);
                            mapResponse.put("allowCapture", "true");
                        } else {
                            mapResponse.put("message", "Success!");
                            mapResponse.put("transId", orderId);
                            mapResponse.put("cardType", cardType);
                            mapResponse.put("authCode", "");
                            mapResponse.put("cardMask", cardMask);
                        }
                        objResponse.response = mapResponse;
                        objResponse.returnCode = 200;
                        return objResponse.toString();
                    }
                }
            }

            // if not return mean something went wrong - return error to retry
            objResponse.returnCode = 525;
            return objResponse.toString();
        } catch(Exception ex) {
            logger.debug(requestId + " - payToken exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            return objResponse.toString();
        }
    }

    public String checkTransactionStatus(String authId) {
        ObjResponse objResponse = new ObjResponse();
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date time = calendar.getTime();
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            String req_time = sdf.format(time);
            logger.debug(requestId + " - req_time: " +  req_time);

            JSONObject objBody = new JSONObject();
            objBody.put("req_time", req_time);
            objBody.put("merchant_id", MERCHANT_ID);
            objBody.put("tran_id", authId);

            String requestString = req_time + MERCHANT_ID + authId;
            logger.debug(requestId + " - requestString: " +  requestString);

            String hash = sha512(requestString, API_KEY);
            logger.debug(requestId + " - hash: " +  hash);
            objBody.put("hash", hash);

            String urlAction = REQUEST_URL + "/payment-gateway/v1/payments/check-transaction";
            RequestBody body = RequestBody.create(objBody.toString().getBytes(), KeysUtil.JSON);
            Request request = new Request.Builder()
                    .url(urlAction)
                    .addHeader("language", "en")
                    .post(body)
                    .build();

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            Response response = client.newCall(request).execute();
            String responseString = response.body().string();
            logger.debug(requestId + " - response = " + responseString);
            /*{
                "status": 0,
                "description": "pre-auth",
                "amount": 15.5,
                "totalAmount": 15.5,
                "apv": "",
                "payment_status": "PRE-AUTH",
                "datetime": "2023-09-30 14:07:00"
            }*/
            /*{
                "status": 0,
                "description": "approved",
                "amount": 15.5,
                "totalAmount": 15.5,
                "apv": "",
                "payment_status": "APPROVED",
                "datetime": "2023-09-30 14:17:12"
            }*/
            JSONObject jsonResponse = new JSONObject(responseString);

            String status = jsonResponse.has("payment_status") ? jsonResponse.getString("payment_status") : "";
            if (status.equalsIgnoreCase("APPROVED") || status.equalsIgnoreCase("PRE-AUTH")) {
                objResponse.returnCode = 200;
            } else {
                objResponse.returnCode = 525;
            }
            return objResponse.toString();
        } catch (Exception ex) {
            logger.debug(requestId + " - checkTransactionStatus exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            return objResponse.toString();
        }
    }

    public String preAuthCapture(String authId, double amount, Map<String,String> mapData) {
        ObjResponse objResponse = new ObjResponse();
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date time = calendar.getTime();
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            String req_time = sdf.format(time);
            logger.debug(requestId + " - req_time: " +  req_time);

            JSONObject objMerchant = new JSONObject();
            objMerchant.put("mc_id", MERCHANT_ID);
            objMerchant.put("tran_id", authId);
            objMerchant.put("complete_amount", KeysUtil.DECIMAL_FORMAT.format(amount));
            logger.debug(requestId + " - objMerchant: " + objMerchant.toString());
            String merchantAuth = java.util.Base64.getEncoder().encodeToString(encrypt(objMerchant.toString(), PUBLIC_KEY));
            JSONObject objBody = new JSONObject();

            objBody.put("request_time", req_time);
            objBody.put("merchant_id", MERCHANT_ID);
            objBody.put("merchant_auth", merchantAuth);
            String requestString = merchantAuth + req_time + MERCHANT_ID;
            logger.debug(requestId + " - requestString: " +  requestString);

            String hash = sha512(requestString, API_KEY);
            logger.debug(requestId + " - hash: " +  hash);
            objBody.put("hash", hash);

            String urlAction = REQUEST_URL + "/merchant-portal/merchant-access/online-transaction/pre-auth-completion";
            RequestBody body = RequestBody.create(objBody.toString().getBytes(), KeysUtil.JSON);
            Request request = new Request.Builder()
                    .url(urlAction)
                    .addHeader("language", "en")
                    .post(body)
                    .build();

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            Response response = client.newCall(request).execute();
            String responseString = response.body().string();
            logger.debug(requestId + " - response = " + responseString);
            /*{
                "grand_total": "10.5",
                "currency": "USD",
                "transaction_status": "COMPLETED",
                "status": {
                    "code": "00",
                    "message": "Success!",
                    "tran_id": "20230930070531113601"
                }
            }*/
            JSONObject jsonResponse = new JSONObject(responseString);

            String status = jsonResponse.has("transaction_status") ? jsonResponse.getString("transaction_status") : "";
            if (status.equalsIgnoreCase("COMPLETED")) {
                String cardType = mapData.get("cardType");
                String cardMask = mapData.get("cardMask");

                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("message", "Success!" );
                mapResponse.put("transId", authId);
                mapResponse.put("cardType", cardType);
                mapResponse.put("authCode", "");
                mapResponse.put("cardMask", cardMask);
                objResponse.response = mapResponse;
                objResponse.returnCode = 200;
                return objResponse.toString();
            } else {
                JSONObject objStatus = jsonResponse.has("status") ? jsonResponse.getJSONObject("status") : null;
                String message = "";
                if (objStatus != null && objStatus.has("message")) {
                    message = objStatus.getString("message");

                    Map<String,String> mapResponse = new HashMap<>();
                    mapResponse.put("message", message);
                    objResponse.response = mapResponse;
                }
                objResponse.returnCode = 525;
                return objResponse.toString();
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - preAuthCapture exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            return objResponse.toString();
        }
    }

    public String voidTransaction(String authId) {
        ObjResponse objResponse = new ObjResponse();
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date time = calendar.getTime();
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            String req_time = sdf.format(time);
            logger.debug(requestId + " - req_time: " +  req_time);

            JSONObject objMerchant = new JSONObject();
            objMerchant.put("mc_id", MERCHANT_ID);
            objMerchant.put("tran_id", authId);
            logger.debug(requestId + " - objMerchant: " + objMerchant.toString());
            String merchantAuth = java.util.Base64.getEncoder().encodeToString(encrypt(objMerchant.toString(), PUBLIC_KEY));
            JSONObject objBody = new JSONObject();

            objBody.put("request_time", req_time);
            objBody.put("merchant_id", MERCHANT_ID);
            objBody.put("merchant_auth", merchantAuth);

            String requestString = MERCHANT_ID + merchantAuth + req_time;
            logger.debug(requestId + " - requestString: " +  requestString);
            String hash = sha512(requestString, API_KEY);
            logger.debug(requestId + " - hash: " +  hash);
            objBody.put("hash", hash);
            String urlAction = REQUEST_URL + "/merchant-portal/merchant-access/online-transaction/pre-auth-cancellation";
            RequestBody body = RequestBody.create(objBody.toString().getBytes(), KeysUtil.JSON);
            Request request = new Request.Builder()
                    .url(urlAction)
                    .addHeader("language", "en")
                    .post(body)
                    .build();

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            Response response = client.newCall(request).execute();
            String responseString = response.body().string();
            logger.debug(requestId + " - response = " + responseString);
            /*{
                "grand_total": 1.5,
                "currency": "USD",
                "transaction_status": "CANCELLED",
                "status": {
                    "code": "00",
                    "message": "Success!"
                }
            }*/
            JSONObject jsonResponse = new JSONObject(responseString);

            String status = jsonResponse.has("transaction_status") ? jsonResponse.getString("transaction_status") : "";
            if (status.equalsIgnoreCase("CANCELLED")) {
                objResponse.returnCode = 200;
            } else {
                objResponse.returnCode = 525;
            }
            return objResponse.toString();
        } catch (Exception ex) {
            logger.debug(requestId + " - voidTransaction exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            return objResponse.toString();
        }
    }

    public static void main(String[] args) throws Exception {
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        String checkoutURL = "";
        String returnParam = GatewayUtil.getOrderId();
        try {
            String returnUrl = "https://eventapis-local.qup.vn/api/webhook/notification/PayWay?urlAction=success";
            String callbackUrl = "https://eventapis-local.qup.vn/api/webhook/notification/PayWay?urlAction=callback";
            String ctid = "5b63d475f29e0130b03880d2";
            String firstName = "David";
            String lastName = "Alaba";

            String rawString = "cambodiaride" + ctid + returnParam;
            logger.debug(" - rawString: " +  rawString);
            String hash = "";//sha512(rawString, "");
            logger.debug(" - hash: " +  hash);

            String urlAction = "https://checkout-sandbox.payway.com.kh/api/payment-gateway/v1/cof/initial";
            checkoutURL = ServerConfig.webform_powertranz
                    + "&action=" + URLEncoder.encode(urlAction, "UTF-8")
                    + "&firstname=" + URLEncoder.encode(firstName, "UTF-8")
                    + "&lastname=" + URLEncoder.encode(lastName, "UTF-8")
                    + "&ctid=" + ctid
                    + "&merchant_id=" + "cambodiaride"
                    + "&return_param=" + returnParam
                    + "&continue_add_card_success_url=" + URLEncoder.encode(returnUrl, "UTF-8")
                    + "&return_url=" + URLEncoder.encode(callbackUrl, "UTF-8")
                    + "&hash=" + URLEncoder.encode(hash, "UTF-8")
            ;

            logger.debug(" - checkoutURL: " +  checkoutURL);
        } catch (Exception ex) {
            logger.debug(" - exception: " + CommonUtils.getError(ex));
        }
    }

    private String sha512(String rawString, String apiKey) throws Exception {
        final byte[] byteKey = apiKey.getBytes(StandardCharsets.UTF_8);
        Mac sha512Hmac = Mac.getInstance("HmacSHA512");
        SecretKeySpec keySpec = new SecretKeySpec(byteKey, "HmacSHA512");
        sha512Hmac.init(keySpec);
        byte[] macData = sha512Hmac.doFinal(rawString.getBytes(StandardCharsets.UTF_8));

        // Can either base64 encode or put it right into hex
        return java.util.Base64.getEncoder().encodeToString(macData);
    }

    private byte[] encrypt(String data, String publicKey) throws BadPaddingException, IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, getPublicKey(publicKey));
        return cipher.doFinal(data.getBytes());
    }

    private PublicKey getPublicKey(String base64PublicKey){
        PublicKey publicKey = null;
        try{
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(java.util.Base64.getDecoder().decode(base64PublicKey.getBytes()));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            publicKey = keyFactory.generatePublic(keySpec);
            return publicKey;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return publicKey;
    }
}
