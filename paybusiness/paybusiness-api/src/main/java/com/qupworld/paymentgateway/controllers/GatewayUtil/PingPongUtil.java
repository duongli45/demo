package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.google.gson.Gson;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.entities.CreditEnt;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.SecurityUtil;
import com.pg.util.ValidCreditCard;
import okhttp3.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class PingPongUtil {

    final static Logger logger = LogManager.getLogger(PingPongUtil.class);
    private String clientId;
    private String accId;
    private String salt;
    private String requestUrl;
    private boolean isSandbox;
    private String requestId;
    private Gson gson;
    private DecimalFormat DF = new DecimalFormat("0.00");

    public PingPongUtil(String _clientId, String _accId, String _salt, boolean _isSandbox, String _requestUrl, String _requestId) {
        clientId = _clientId; // 2018092714313010016
        accId = _accId; // 2018092714313010016122
        salt = _salt; // F1BCE1DEB87F212625B690AD
        isSandbox =_isSandbox;
        requestId = _requestId;
        requestUrl = _requestUrl;
        gson = new Gson();
    }

    public String createCreditToken(CreditEnt creditEnt, String customerId, String currencyISO,
                                    String billFirstName, String billLastName, String billPhoneNumber, String billEmail,
                                    String merchantTransactionId) throws Exception{
        ObjResponse createTokenResponse = new ObjResponse();
        createTokenResponse.returnCode = 437;
        String description = "registration-" + billPhoneNumber;
        try {
            Map<String, String> mapResponse = new HashMap<>();
            String URL = requestUrl + "/v2/payment";
            String amount = "1.00";
            String signType = "MD5";

            String serverUrl = ServerConfig.dispatch_server;
            if (ServerConfig.dispatch_server.equals("http://dispatch.local.qup.vn")) {
                serverUrl = "http://113.160.232.234:1337";
            }
            String shopperResultUrl = serverUrl + KeysUtil.PINGPONG_URL + "?action=PingPongReturn";
            String notificationUrl = serverUrl + KeysUtil.PINGPONG_URL + "?action=PingPongNotify";

            String signString = salt+
                    "accId="+accId+
                    "&amount="+amount+
                    "&currency="+currencyISO+
                    "&merchantTransactionId="+merchantTransactionId+
                    "&notificationUrl="+notificationUrl+
                    "&shopperResultUrl="+shopperResultUrl+
                    "&signType="+signType
                    ;
            logger.debug(requestId + " - signString = " + signString);
            String sign = SecurityUtil.md5(signString);

            JSONObject objBody = new JSONObject();
            objBody.put("accId", accId);
            objBody.put("signType", signType);
            objBody.put("sign", sign);
            objBody.put("amount", amount);
            objBody.put("currency", currencyISO);
            objBody.put("merchantTransactionId", merchantTransactionId);
            objBody.put("paymentType", "AUTH");
            objBody.put("shopperResultUrl", shopperResultUrl);

            String[] expiry = creditEnt.expiredDate.split("/");
            String expiryMonth = expiry[0].length() == 1 ? "0" + expiry[0] : expiry[0];
            String[] cardHolder = creditEnt.cardHolder.split(" ");
            String firstName = cardHolder[0];
            String lastName = creditEnt.cardHolder.substring(firstName.length()).trim();
            JSONObject objCard = new JSONObject();
            objCard.put("number", creditEnt.cardNumber);
            objCard.put("expireMonth", expiryMonth);
            objCard.put("expireYear", expiry[1]);
            objCard.put("cvv", creditEnt.cvv);
            objCard.put("firstName", firstName);
            objCard.put("lastName", lastName);
            JSONObject objPayment = new JSONObject();
            objPayment.put("card", objCard);
            objBody.put("payMethodInfo", objPayment);
            objBody.put("paymentBrand", ValidCreditCard.getCardType(creditEnt.cardNumber).toUpperCase());

            objBody.put("threeDSecure", "N");

            // risk info
            JSONArray arrGoods = new JSONArray();
            JSONObject objGood = new JSONObject();
            objGood.put("name","registration");
            objGood.put("description",description);
            objGood.put("averageUnitPrice",amount);
            objGood.put("number","1");
            arrGoods.add(objGood);

            String billAddress = creditEnt.street != null ? creditEnt.street : "";
            if (billAddress.isEmpty()) billAddress = "pingpong No.88";

            String billCity = creditEnt.city != null ? creditEnt.city : "";
            if (billCity.isEmpty()) billCity = "pingpong";

            String country = creditEnt.country != null ? creditEnt.country : "";
            String billCountry = country.length() == 2 ? country.toUpperCase() : GatewayUtil.getCountry2Code(country).toUpperCase();
            if (billCountry.isEmpty()) billCountry = "CN";

            String billState = creditEnt.state != null ? creditEnt.state : "";
            if (billState.isEmpty() && (country.equals("US") || country.equals("CA"))) billState = "pingpong";

            String billPostalCode = creditEnt.postalCode != null ? creditEnt.postalCode : "";
            if (billPostalCode.isEmpty() && (country.equals("US") || country.equals("CA"))) billPostalCode = "PP88";

            if (billEmail.isEmpty()) billEmail = "null@pingpong.com";

            JSONObject objBill = new JSONObject();
            objBill.put("city", billCity);
            objBill.put("country", billCountry);
            objBill.put("email", billEmail);
            objBill.put("firstName", billFirstName);
            objBill.put("lastName", billLastName);
            objBill.put("phone", billPhoneNumber);

            if (!billPostalCode.isEmpty())
                objBill.put("postcode", billPostalCode);
            if (!billState.isEmpty())
                objBill.put("state", billState);
            objBill.put("street", billAddress);

            JSONObject objShipping = new JSONObject();
            objShipping.put("city", billCity);
            objShipping.put("country", billCountry);
            objShipping.put("email", billEmail);
            objShipping.put("firstName", billFirstName);
            objShipping.put("lastName", billLastName);
            objShipping.put("phone", billPhoneNumber);
            if (!billPostalCode.isEmpty())
                objShipping.put("postcode", billPostalCode);
            if (!billState.isEmpty())
                objShipping.put("state", billState);
            objShipping.put("street", billAddress);

            String fingerprint = getFingerprint();
            JSONObject objDevice = new JSONObject();
            objDevice.put("fingerprintId", fingerprint);
            objDevice.put("orderTerminal", "01");

            JSONObject riskInfo = new JSONObject();
            riskInfo.put("goods", arrGoods);
            riskInfo.put("billing", objBill);
            riskInfo.put("shipping", objShipping);
            riskInfo.put("device", objDevice);

            objBody.put("riskInfo", riskInfo);
            // end risk

            objBody.put("notificationUrl", notificationUrl);
            objBody.put("merchantUserId", customerId);
            objBody.put("createToken", "Y");
            objBody.put("default", "Y");

            String log = objBody.toString().replace(creditEnt.cardNumber, "XXXXXXXXXXXX" + creditEnt.cardNumber.substring(creditEnt.cardNumber.length() - 4));
            logger.debug(requestId + " - createCreditToken objBody = " + log);

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            RequestBody body = RequestBody.create(JSON, objBody.toString());
            Request request = new Request.Builder()
                    .url(URL)
                    .post(body)
                    .build();

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            Response response = client.newCall(request).execute();
            String result = response.body().string();
            logger.debug(requestId + " - createCreditToken result = " + result);
            /**
             * Result sample
             * {
                 "accId":"2018092714313010016122",
                 "amount":"1.00",
                 "clientId":"2018092714313010016",
                 "code":"000000",
                 "currency":"CNY",
                 "description":"Transaction succeeded",
                 "language":"en",
                 "merchantTransactionId":"20200528085853165",
                 "paymentType":"AUTH",
                 "relateTransactionId":"2020052809025923441",
                 "shopperResultUrl":"http://113.160.232.234:1337/paymentNotificationURL?action=PingPongReturn",
                 "sign":"39F83611E65FDF8D935D364B1FB7B387",
                 "signType":"MD5",
                 "status":"SUCCESS",
                 "token":"2pdRwvJTVIJfoAQyRZh",
                 "transactionId":"2020052809025923441",
                 "transactionTime":"1590656581000"
                 }
             */
            JSONObject objResponse = gson.fromJson(result, JSONObject.class);
            String code = objResponse.get("code") != null ? objResponse.get("code").toString() : "";
            if (!code.isEmpty() && code.equals("000000")) {
                //successful
                mapResponse.put("creditCard", "XXXXXXXXXXXX" + creditEnt.cardNumber.substring(creditEnt.cardNumber.length() - 4));
                // append token with encrypted CVV
                String token = objResponse.get("token") != null ? objResponse.get("token").toString() : "";
                if (!token.isEmpty()) {
                    String encryptCVV = SecurityUtil.encryptProfile(SecurityUtil.getKey(), creditEnt.cvv);
                    String cardToken = token + KeysUtil.TEMPKEY + encryptCVV;
                    mapResponse.put("token", cardToken);
                    mapResponse.put("cardType", ValidCreditCard.getCardType(creditEnt.cardNumber));
                    createTokenResponse.returnCode = 200;
                    createTokenResponse.response = mapResponse;
                } else {
                    createTokenResponse.returnCode = 413;
                    createTokenResponse.response = mapResponse;
                }
            } else {
                GatewayUtil.sendMail(requestId, KeysUtil.PINGPONG, log, objResponse.toString(), description);

                // error - get response from gateway
                String gwMessage = objResponse.get("description") != null ? objResponse.get("description").toString() : "";
                if(!description.isEmpty()) {
                    mapResponse.put("message", gwMessage);
                    createTokenResponse.response = mapResponse;
                }
            }
            return createTokenResponse.toString();
        } catch (Exception ex) {
            GatewayUtil.sendMail(requestId, KeysUtil.PINGPONG, ex, description);

            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }
    }

    public String payByToken(String type, String bookId, double amount, String currencyISO, String token, String cardMask, String cardType,
                             String customerId, String billFirstName, String billLastName, String billPhoneNumber, String billEmail,
                             String billAddress, String billCity, String billState, String billPostalCode, String billCountry, Map<String,String> industryData) {
        ObjResponse paymentResponse = new ObjResponse();
        paymentResponse.returnCode = 437;
        String description = "payment-" + billPhoneNumber + "-booking-" + bookId;
        try {
            Map<String, String> mapResponse = new HashMap<>();
            String URL = requestUrl + "/v2/payment";

            String signType = "MD5";

            String serverUrl = ServerConfig.dispatch_server;
            if (ServerConfig.dispatch_server.equals("http://dispatch.local.qup.vn")) {
                serverUrl = "http://113.160.232.234:1337";
            }
            String shopperResultUrl = serverUrl + KeysUtil.PINGPONG_URL + "?action=PingPongReturn";
            String notificationUrl = serverUrl + KeysUtil.PINGPONG_URL + "?action=PingPongNotify";

            String signString = salt+
                    "accId="+accId+
                    "&amount="+DF.format(amount)+
                    "&currency="+currencyISO+
                    "&merchantTransactionId="+bookId+
                    "&notificationUrl="+notificationUrl+
                    "&shopperResultUrl="+shopperResultUrl+
                    "&signType="+signType
                    ;
            logger.debug(requestId + " - signString = " + signString);
            String sign = SecurityUtil.md5(signString);

            JSONObject objBody = new JSONObject();
            objBody.put("accId", accId);
            objBody.put("signType", signType);
            objBody.put("sign", sign);
            objBody.put("amount", DF.format(amount));
            objBody.put("currency", currencyISO);
            objBody.put("merchantTransactionId", bookId);
            objBody.put("paymentType", type);
            objBody.put("shopperResultUrl", shopperResultUrl);

            String[] arrToken = token.split(KeysUtil.TEMPKEY);
            JSONObject objCard = new JSONObject();
            objCard.put("cvv", SecurityUtil.decryptProfile(SecurityUtil.getKey(), arrToken[1]));
            JSONObject objPayment = new JSONObject();
            objPayment.put("card", objCard);
            objBody.put("payMethodInfo", objPayment);
            objBody.put("token", arrToken[0]);

            objBody.put("threeDSecure", "N");

            // risk info
            JSONArray arrGoods = new JSONArray();
            JSONObject objGoodIndustry = new JSONObject();
            objGoodIndustry.put("mileage", industryData.get("mileage"));
            objGoodIndustry.put("carModel", industryData.get("carModel"));
            objGoodIndustry.put("rentalWebsite", "avischina.com");
            objGoodIndustry.put("carRentalUnitPrice", DF.format(amount));
            objGoodIndustry.put("destinationAddress", industryData.get("destinationAddress"));

            JSONObject objGood = new JSONObject();
            objGood.put("name", "booking");
            objGood.put("description", "Chauffeur Drive");
            objGood.put("averageUnitPrice", DF.format(amount));
            objGood.put("number", "1");
            objGood.put("industryProperties", objGoodIndustry);
            arrGoods.add(objGood);

            // get country code
            billCountry = billCountry.length() == 2 ? billCountry.toUpperCase() : GatewayUtil.getCountry2Code(billCountry).toUpperCase();

            if (billAddress.isEmpty()) billAddress = "pingpong No.88";
            if (billCity.isEmpty()) billCity = "pingpong";
            if (billCountry.isEmpty()) billCountry = "CN";
            if (billState.isEmpty() && (billCountry.equals("US") || billCountry.equals("CA"))) billState = "pingpong";
            if (billPostalCode.isEmpty() && (billCountry.equals("US") || billCountry.equals("CA"))) billPostalCode = "PP88";

            if (billEmail.isEmpty()) billEmail = "null@pingpong.com";

            JSONObject objBill = new JSONObject();
            objBill.put("city", billCity);
            objBill.put("country", billCountry);
            objBill.put("email", billEmail);
            objBill.put("firstName", billFirstName);
            objBill.put("lastName", billLastName);
            objBill.put("phone", billPhoneNumber);

            if (!billPostalCode.isEmpty())
                objBill.put("postcode", billPostalCode);
            if (!billState.isEmpty())
                objBill.put("state", billState);
            objBill.put("street", billAddress);

            JSONObject objShipping = new JSONObject();
            objShipping.put("city", billCity);
            objShipping.put("country", billCountry);
            objShipping.put("email", billEmail);
            objShipping.put("firstName", billFirstName);
            objShipping.put("lastName", billLastName);
            objShipping.put("phone", billPhoneNumber);
            if (!billPostalCode.isEmpty())
                objShipping.put("postcode", billPostalCode);
            if (!billState.isEmpty())
                objShipping.put("state", billState);
            objShipping.put("street", billAddress);

            String fingerprint = getFingerprint();
            JSONObject objDevice = new JSONObject();
            objDevice.put("fingerprintId", fingerprint);
            objDevice.put("orderTerminal", "01");

            JSONObject riskInfo = new JSONObject();
            riskInfo.put("goods", arrGoods);
            riskInfo.put("billing", objBill);
            riskInfo.put("shipping", objShipping);
            riskInfo.put("device", objDevice);

            objBody.put("riskInfo", riskInfo);
            // end risk

            objBody.put("notificationUrl", notificationUrl);
            objBody.put("merchantUserId", customerId);

            logger.debug(requestId + " - "+ type +" objBody = " + objBody.toString());

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            RequestBody body = RequestBody.create(JSON, objBody.toString());
            Request request = new Request.Builder()
                    .url(URL)
                    .post(body)
                    .build();

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            Response response = client.newCall(request).execute();
            String result = response.body().string();
            logger.debug(requestId + " - "+ type +" result = " + result);
            /**
             * Result sample
             * {
                 "notificationUrl": "http://113.160.232.234:1337/paymentNotificationURL?action=notify",
                 "amount": "10.000000",
                 "clientId": "2018092714313010016",
                 "code": "000000",
                 "threeDSecure": "N",
                 "sign": "686660ECD691A9032541F3920A4FC89E",
                 "description": " ",
                 "transactionTime": "1590485470000",
                 "transactionId": "2020052609310823353",
                 "paymentType": "DEBIT",
                 "outFlowId": "2020052609310823353",
                 "merchantTransactionId": "20200526003",
                 "accId": "2018092714313010016122",
                 "signType": "MD5",
                 "currency": "CNY",
                 "status": "SUCCESS"
                 }
             */
            JSONObject objResponse = gson.fromJson(result, JSONObject.class);
            String code = objResponse.get("code") != null ? objResponse.get("code").toString() : "";
            if (!code.isEmpty() && code.equals("000000")) {
                String transactionId = objResponse.get("transactionId") != null ? objResponse.get("transactionId").toString() : "";
                if (type.equals("SALE")) {
                    //successful
                    mapResponse.put("message", "Success!");
                    mapResponse.put("transId", transactionId);
                    mapResponse.put("cardType", cardType);
                    mapResponse.put("authCode", "");
                    mapResponse.put("cardMask", cardMask);
                    paymentResponse.response = mapResponse;
                    paymentResponse.returnCode = 200;
                } else if (type.equals("AUTH")) {
                    mapResponse.put("authId", transactionId);
                    mapResponse.put("authAmount", String.valueOf(amount));
                    mapResponse.put("authCode", "");
                    mapResponse.put("authToken", token);
                    mapResponse.put("allowCapture", "true");
                    paymentResponse.response = mapResponse;
                    paymentResponse.returnCode = 200;
                }
            } else {
                GatewayUtil.sendMail(requestId, KeysUtil.PINGPONG, objBody.toString(), objResponse.toString(), description);

                // error - get response from gateway
                String gwMessage = objResponse.get("description") != null ? objResponse.get("description").toString() : "";
                if(!description.isEmpty()) {
                    mapResponse.put("message", gwMessage);
                    paymentResponse.response = mapResponse;
                }
            }
            return paymentResponse.toString();
        } catch (Exception ex) {
            GatewayUtil.sendMail(requestId, KeysUtil.PINGPONG, ex, description);

            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }
    }

    public String processTransaction(String type, String bookId, String transactionId, double amount,
                                     String cardType, String cardMask, String currencyISO, String billPhoneNumber){
        ObjResponse paymentResponse = new ObjResponse();
        paymentResponse.returnCode = 437;
        Map<String, String> mapResponse = new HashMap<String, String>();
        String description = type +"-" + billPhoneNumber + "-booking-" + bookId;
        try {
            String URL = requestUrl + "/v2/payment/" + transactionId;

            String serverUrl = ServerConfig.dispatch_server;
            if (ServerConfig.dispatch_server.equals("http://dispatch.local.qup.vn")) {
                serverUrl = "http://113.160.232.234:1337";
            }
            String notificationUrl = serverUrl + KeysUtil.PINGPONG_URL + "?action=PingPongNotify";

            String signType = "MD5";
            String merchantTransactionId = GatewayUtil.getOrderId();

            String signString = salt+
                    "accId="+accId+
                    "&amount="+DF.format(amount)+
                    "&currency="+currencyISO+
                    "&merchantTransactionId="+merchantTransactionId+
                    "&notificationUrl="+notificationUrl+
                    "&signType="+signType+
                    "&transactionId="+transactionId
                    ;
            logger.debug(requestId + " - signString = " + signString);
            String sign = SecurityUtil.md5(signString);

            JSONObject objBody = new JSONObject();
            objBody.put("accId", accId);
            objBody.put("merchantTransactionId", merchantTransactionId);
            objBody.put("paymentType", type);
            objBody.put("amount", DF.format(amount));
            objBody.put("currency", currencyISO);
            objBody.put("signType", signType);
            objBody.put("sign", sign);
            objBody.put("notificationUrl", notificationUrl);
            logger.debug(requestId + " - "+ type +" objBody = " + objBody.toString());

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            RequestBody body = RequestBody.create(JSON, objBody.toString());
            Request request = new Request.Builder()
                    .url(URL)
                    .post(body)
                    .build();

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            Response response = client.newCall(request).execute();
            String result = response.body().string();
            logger.debug(requestId + " - "+ type +" result = " + result);

            /**
             * Sample response
             * {
                 "notificationUrl": "http://113.160.232.234:1337/paymentNotificationURL?action=notify",
                 "amount": "10.000000",
                 "clientId": "2018092714313010016",
                 "code": "000000",
                 "threeDSecure": "N",
                 "sign": "085C48F17BB7397B75A5D5BEBA0D9A62",
                 "description": " ",
                 "transactionTime": "1590486051000",
                 "transactionId": "2020052609405123357",
                 "paymentType": "CAPTURE",
                 "outFlowId": "2020052609405123357",
                 "merchantTransactionId": "20200526004",
                 "accId": "2018092714313010016122",
                 "signType": "MD5",
                 "currency": "CNY",
                 "status": "SUCCESS"
             }
             */
            JSONObject objResponse = gson.fromJson(result, JSONObject.class);
            String code = objResponse.get("code") != null ? objResponse.get("code").toString() : "";
            if (!code.isEmpty() && code.equals("000000")) {
                String transId = objResponse.get("transactionId") != null ? objResponse.get("transactionId").toString() : "";
                if (type.equals("CAPTURE")) {
                    //successful
                    mapResponse.put("message", "Success!");
                    mapResponse.put("transId", transId);
                    mapResponse.put("cardType", cardType);
                    mapResponse.put("authCode", "");
                    mapResponse.put("cardMask", cardMask);
                    paymentResponse.response = mapResponse;
                    paymentResponse.returnCode = 200;
                }
            } else {
                GatewayUtil.sendMail(requestId, KeysUtil.PINGPONG, objBody.toString(), objResponse.toString(), description);

                // error - get response from gateway
                String gwMessage = objResponse.get("description") != null ? objResponse.get("description").toString() : "";
                if(!description.isEmpty()) {
                    mapResponse.put("message", gwMessage);
                    paymentResponse.response = mapResponse;
                }
            }
        } catch (Exception ex) {
            GatewayUtil.sendMail(requestId, KeysUtil.PINGPONG, ex, description);

            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }
        return paymentResponse.toString();
    }

    public void unbind(String token, String userId) {
        try {
            OkHttpClient client = new OkHttpClient();

            String URL = requestUrl + "/v2/registrations/" + token;
            String signType = "MD5";

            String signString = salt+
                    "accId="+accId+
                    "&signType="+signType
                    ;
            logger.debug(requestId + " - signString = " + signString);
            String sign = SecurityUtil.md5(signString);


            JSONObject objBody = new JSONObject();
            objBody.put("accId", accId);
            objBody.put("signType", signType);
            objBody.put("sign", sign);
            objBody.put("merchantUserId", userId);

            logger.debug(requestId + " - unbind objBody = " + objBody.toString());

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            RequestBody body = RequestBody.create(JSON, objBody.toString());
            Request request = new Request.Builder()
                    .url(URL)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            String result = response.body().string();
            logger.debug(requestId + " - unbind result = " + result);
        } catch (Exception ex) {
            logger.debug(requestId + " - unbind exception = " + GatewayUtil.getError(ex));
        }

    }

    private String getFingerprint() throws Exception {
        String session_token = accId + UUID.randomUUID().toString();
        String deviceFingerprintID = "https://h.online-metrix.net/fp/tags.js?org_id=k8vif92e&session_id=avis" + session_token;
        if (isSandbox) {
            deviceFingerprintID = "https://h.online-metrix.net/fp/tags.js?org_id=1snn5n9w&session_id=avis" + session_token;
        }
        return deviceFingerprintID;
    }
}
