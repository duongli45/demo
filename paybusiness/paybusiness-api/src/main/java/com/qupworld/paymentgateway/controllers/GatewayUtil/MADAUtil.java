package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.google.gson.Gson;
import com.qupworld.config.ServerConfig;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import okhttp3.*;
import org.apache.commons.codec.binary.Hex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
public class MADAUtil {

    private static final Logger logger = LogManager.getLogger(MADAUtil.class);
    private String requestId;
    private String USERNAME;
    private String TERMINAL_ID;
    private String PASSWORD;
    private String SECRET_KEY;
    private String REQUEST_URL;
    private final Gson gson = new Gson();
    private final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    public MADAUtil(String _requestId, String userName, String terminalId, String password, String secretKey, String requestURL) {
        requestId = _requestId;
        USERNAME = userName;
        TERMINAL_ID = terminalId;
        PASSWORD = password;
        SECRET_KEY = secretKey;
        REQUEST_URL = requestURL;
    }

    public String initiateCreditForm(String orderId, String currencyISO, Map<String,String> mapData) {
        ObjResponse objResponse = new ObjResponse();
        try {
            double amount = 1.0;
            String callbackUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.MADA + "?urlAction=token";
            String requestHash = generateHashKey(orderId, KeysUtil.DECIMAL_FORMAT0.format(amount), currencyISO);
            org.json.simple.JSONObject objBody = new org.json.simple.JSONObject();
            objBody.put("terminalId", TERMINAL_ID);
            objBody.put("password", PASSWORD);
            objBody.put("action", "12");
            objBody.put("amount", KeysUtil.DECIMAL_FORMAT0.format(amount));
            objBody.put("currency", currencyISO);
            objBody.put("country", mapData.get("country"));
            objBody.put("customerEmail", mapData.get("customerEmail"));
            objBody.put("trackid", orderId);
            objBody.put("transid", "");
            objBody.put("tokenOperation", "A");
            objBody.put("tokenizationType", "0");
            objBody.put("merchantIp", mapData.get("merchantIp"));
            objBody.put("udf1", "");
            objBody.put("udf3", "");
            objBody.put("udf4", "");
            objBody.put("udf5", "");
            objBody.put("udf2", callbackUrl);
            objBody.put("requestHash", requestHash);
            objBody.put("instrumentType", "DEFAULT");
            objBody.put("cardToken", "");
            logger.debug(requestId + " - request = " + objBody.toJSONString());

            RequestBody body = RequestBody.create(objBody.toJSONString(), JSON);
            Request request = new Request.Builder()
                    .addHeader("Content-Type", "application/json")
                    .url(REQUEST_URL)
                    .post(body)
                    .build();

            OkHttpClient client = new OkHttpClient.Builder().build();
            Response response = client.newCall(request).execute();
            String responseString = response.body().string();
            logger.debug(requestId + " - response = " + responseString);
            /*{
              "result": null,
              "responseCode": null,
              "authcode": null,
              "tranid": null,
              "trackid": null,
              "terminalid": null,
              "udf1": null,
              "udf2": null,
              "udf3": null,
              "udf4": null,
              "udf5": null,
              "rrn": null,
              "eci": null,
              "subscriptionId": null,
              "trandate": null,
              "tranType": null,
              "integrationModule": null,
              "integrationData": null,
              "payid": "2223315123281225627",
              "targetUrl": "https://payments-dev.urway-tech.com/URWAYPGService/direct.jsp",
              "postData": null,
              "intUrl": null,
              "responseHash": null,
              "amount": null,
              "cardBrand": "",
              "maskedPAN": null,
              "linkBasedUrl": null,
              "sadadNumber": null,
              "billNumber": null,
              "cardToken": null
            }*/
            JSONObject jsonResponse = new JSONObject(responseString);
            String targetUrl = jsonResponse.has("targetUrl") ? jsonResponse.getString("targetUrl") : "";
            String payid = jsonResponse.has("payid") ? jsonResponse.getString("payid") : "";
            String url = targetUrl + "?paymentid=" + payid;
            logger.debug(requestId + " - url = " + url);
            Map<String,String> mapResponse = new HashMap<>();
            mapResponse.put("3ds_url", url);
            mapResponse.put("type", "link");
            mapResponse.put("transactionId", orderId);
            objResponse.response = mapResponse;
            objResponse.returnCode = 200;
            return objResponse.toString();
        } catch(Exception ex) {
            logger.debug(requestId + " - initiateCreditForm exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            return objResponse.toString();
        }
    }

    public String payToken(String orderId, String type, String token, double amount, String currencyISO, Map<String,String> mapData) {
        ObjResponse objResponse = new ObjResponse();
        try {
            String action = type.equals("preAuth") ? "4" : "1"; // 1 is charge
            String callbackUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.MADA + "?urlAction="+type;
            String requestHash = generateHashKey(orderId, KeysUtil.DECIMAL_FORMAT0.format(amount), currencyISO);
            org.json.simple.JSONObject objBody = new org.json.simple.JSONObject();
            objBody.put("terminalId", TERMINAL_ID);
            objBody.put("password", PASSWORD);
            objBody.put("action", action);
            objBody.put("tokenizationType", "0");
            objBody.put("amount", KeysUtil.DECIMAL_FORMAT0.format(amount));
            objBody.put("currency", currencyISO);
            objBody.put("country", mapData.get("country"));
            objBody.put("customerEmail", mapData.get("customerEmail"));
            objBody.put("trackid", orderId);
            objBody.put("transid", "");
            objBody.put("merchantIp", mapData.get("merchantIp"));
            objBody.put("udf1", "");
            objBody.put("udf2", callbackUrl);
            objBody.put("udf3", "");
            objBody.put("udf4", "");
            objBody.put("udf5", "");
            objBody.put("requestHash", requestHash);
            objBody.put("instrumentType", "DEFAULT");
            objBody.put("cardToken", token);
            logger.debug(requestId + " - request = " + objBody.toJSONString());

            RequestBody body = RequestBody.create(objBody.toJSONString(), JSON);
            Request request = new Request.Builder()
                    .addHeader("Content-Type", "application/json")
                    .url(REQUEST_URL)
                    .post(body)
                    .build();

            OkHttpClient client = new OkHttpClient.Builder().build();
            Response response = client.newCall(request).execute();
            String responseString = response.body().string();
            logger.debug(requestId + " - response = " + responseString);
            /*{
              "result": null,
              "responseCode": null,
              "authcode": null,
              "tranid": null,
              "trackid": null,
              "terminalid": null,
              "udf1": null,
              "udf2": null,
              "udf3": null,
              "udf4": null,
              "udf5": null,
              "rrn": null,
              "eci": null,
              "subscriptionId": null,
              "trandate": null,
              "tranType": null,
              "integrationModule": null,
              "integrationData": null,
              "payid": "2223315123281225627",
              "targetUrl": "https://payments-dev.urway-tech.com/URWAYPGService/direct.jsp",
              "postData": null,
              "intUrl": null,
              "responseHash": null,
              "amount": null,
              "cardBrand": "",
              "maskedPAN": null,
              "linkBasedUrl": null,
              "sadadNumber": null,
              "billNumber": null,
              "cardToken": null
            }*/
            JSONObject jsonResponse = new JSONObject(responseString);
            String targetUrl = jsonResponse.has("targetUrl") ? jsonResponse.getString("targetUrl") : "";
            String payid = jsonResponse.has("payid") ? jsonResponse.getString("payid") : "";
            String url = targetUrl + "?paymentid=" + payid;
            logger.debug(requestId + " - url = " + url);
            List<String> requireAuthen = Arrays.asList("pay", "remain", "preAuth");
            int returnCode = requireAuthen.contains(type) ? 528 : 200;
            Map<String,Object> mapResponse = new HashMap<>();
            mapResponse.put("3ds_url", url);
            mapResponse.put("deepLinkUrl", url);
            mapResponse.put("type", "link");
            mapResponse.put("transactionId", orderId);
            mapResponse.put("waitingNotify", true);
            objResponse.response = mapResponse;
            objResponse.returnCode = returnCode;
            return objResponse.toString();
        } catch(Exception ex) {
            logger.debug(requestId + " - payToken exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            return objResponse.toString();
        }
    }

    public String preAuthCapture(String orderId, double amount, String authId, String currencyISO, Map<String,String> mapData) {
        ObjResponse objResponse = new ObjResponse();
        try {
            String requestHash = generateHashKey(orderId, KeysUtil.DECIMAL_FORMAT0.format(amount), currencyISO);
            org.json.simple.JSONObject objBody = new org.json.simple.JSONObject();
            objBody.put("terminalId", TERMINAL_ID);
            objBody.put("password", PASSWORD);
            objBody.put("action", "5");
            objBody.put("amount", KeysUtil.DECIMAL_FORMAT0.format(amount));
            objBody.put("currency", currencyISO);
            objBody.put("country", mapData.get("country"));
            objBody.put("customerEmail", mapData.get("customerEmail"));
            objBody.put("trackid", orderId);
            objBody.put("transid", authId);
            objBody.put("merchantIp", mapData.get("merchantIp"));
            objBody.put("udf1", "");
            objBody.put("udf2", "");
            objBody.put("udf3", "");
            objBody.put("udf4", "");
            objBody.put("udf5", "");
            objBody.put("requestHash", requestHash);
            logger.debug(requestId + " - request = " + objBody.toJSONString());

            RequestBody body = RequestBody.create(objBody.toJSONString(), JSON);
            Request request = new Request.Builder()
                    .addHeader("Content-Type", "application/json")
                    .url(REQUEST_URL)
                    .post(body)
                    .build();

            OkHttpClient client = new OkHttpClient.Builder().build();
            Response response = client.newCall(request).execute();
            String responseString = response.body().string();
            logger.debug(requestId + " - response = " + responseString);
            /*{
              {
                  "result": "Successful",
                  "responseCode": "000",
                  "authcode": "",
                  "tranid": "2223405123774253417",
                  "trackid": "2202214195530",
                  "terminalid": "gojosaudi",
                  "udf1": "",
                  "udf2": null,
                  "udf3": "",
                  "udf4": "",
                  "udf5": "",
                  "rrn": "",
                  "eci": "",
                  "subscriptionId": null,
                  "trandate": null,
                  "tranType": null,
                  "integrationModule": null,
                  "integrationData": null,
                  "payid": null,
                  "targetUrl": null,
                  "postData": null,
                  "intUrl": null,
                  "responseHash": "0475e64d8883649f31f7e7bd15a5bd91345016b77988f402e1191bc81ce07a4f",
                  "amount": "5.00",
                  "cardBrand": "DEFAULT",
                  "maskedPAN": null,
                  "linkBasedUrl": null,
                  "sadadNumber": null,
                  "billNumber": null,
                  "cardToken": null
                }
            }*/

            JSONObject jsonResponse = new JSONObject(responseString);
            String result = jsonResponse.has("result") ? jsonResponse.getString("result") : "";
            if (result.equalsIgnoreCase("Successful")) {
                String tranid = jsonResponse.has("tranid") ? jsonResponse.getString("tranid") : "";
                String cardType = mapData.get("cardType");
                String cardMask = mapData.get("cardMask");

                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("message", "Success!" );
                mapResponse.put("transId", tranid);
                mapResponse.put("cardType", cardType);
                mapResponse.put("authCode", "");
                mapResponse.put("cardMask", cardMask);
                objResponse.response = mapResponse;
                objResponse.returnCode = 200;
                return objResponse.toString();
            } else {
                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("message", result);
                objResponse.response = mapResponse;
                objResponse.returnCode = 525;
                return objResponse.toString();
            }
        } catch(Exception ex) {
            logger.debug(requestId + " - preAuthCapture exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            return objResponse.toString();
        }
    }

    private String generateHashKey(String orderId, String amount, String currencyISO) {
        String hashKey = null;
        try {
            String pipeSeperatedString = orderId + "|" + TERMINAL_ID + "|" +
                    PASSWORD + "|" + SECRET_KEY + "|" + amount + "|" +
                    currencyISO;
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(pipeSeperatedString.getBytes("utf-8"));
            byte[] sha384 = md.digest();
            hashKey = Hex.encodeHexString(sha384);
        } catch (NoSuchAlgorithmException |UnsupportedEncodingException e) {
            logger.debug(requestId + " - generateHashKey exception:" + CommonUtils.getError(e));
        }
        return hashKey;
    }
}
