
package com.qupworld.paymentgateway.controllers.GatewayUtil.PayUSDK;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Order {

    public String id;
    public String accountId;
    public String referenceCode;
    public String description;
    public String language;
    public String signature;
    public String notifyUrl;
    public AdditionalValues additionalValues;
    public Buyer buyer;
    public ShippingAddress_ shippingAddress;

}
