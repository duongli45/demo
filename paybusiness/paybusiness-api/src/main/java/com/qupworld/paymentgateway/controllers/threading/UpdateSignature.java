package com.qupworld.paymentgateway.controllers.threading;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.qupworld.config.ServerConfig;
import com.pg.util.KeysUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by qup on 23/03/2020.
 */
@SuppressWarnings("unchecked")
public class UpdateSignature extends Thread {
    public String bookId;
    public String url;
    public String requestId;
    final static Logger logger = LogManager.getLogger(UpdateSignature.class);
    @Override
    public void run() {
        try {
            String body = "[{ \"id\": \"" + bookId + "\", \"type\": \"Ticket\", \"body\": { \"signatureUrl\": \"" + url + "\"} }]";
            // submit data
            logger.debug(requestId + " - report body: " + body);
            HttpResponse<JsonNode> json = Unirest.post(ServerConfig.server_report)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("x-access-token", KeysUtil.TOKEN)
                    .header("x-trace-request-id", requestId)
                    .body(body)
                    .asJson();
            logger.debug(requestId + " - report update: " + bookId + " - " + json.getBody().toString());
            org.json.JSONArray storeObj = json.getBody().getArray();
            org.json.JSONObject object = (org.json.JSONObject) storeObj.get(0);
            boolean success = object.getBoolean("success");
            if (!success) {
                // re-authenticate
                String authBody = "{ \"username\" : \"" + ServerConfig.auth_username + "\" , \"password\" : \"" + ServerConfig.auth_password + "\" }";
                HttpResponse<JsonNode> jsonAuth = Unirest.post(ServerConfig.server_auth)
                        .header("Content-Type", "application/json; charset=utf-8")
                        .body(authBody)
                        .asJson();
                JSONParser parser = new JSONParser();
                JSONObject objectData = (JSONObject) parser.parse(jsonAuth.getBody().toString());
                KeysUtil.TOKEN = objectData.get("token").toString();
                // submit with new token
                json = Unirest.post(ServerConfig.server_report)
                        .header("Content-Type", "application/json; charset=utf-8")
                        .header("x-access-token", KeysUtil.TOKEN)
                        .header("x-trace-request-id", requestId)
                        .body(body)
                        .asJson();
                logger.debug(requestId + " - report update: " + bookId + " - " + json.getBody().toString());
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            logger.debug(requestId + " - report update: "+ errors.toString());
        }

    }


}
