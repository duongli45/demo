package com.qupworld.paymentgateway.controllers.threading;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.qupworld.config.ServerConfig;
import com.pg.util.KeysUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;

/**
 * Created by qup on 04/09/2018.
 */
public class ThreadUtil {
    final static Logger logger = LogManager.getLogger(ThreadUtil.class);
    private Gson gson;
    private String requestId;
    public ThreadUtil(String requestId) {
        this.requestId = requestId;
        gson = new Gson();
    }

    public boolean submit(String body, String URL, String method) {
        boolean success = false;
        try {
            HttpResponse<String> responseString;
            if (method.equals("put")) {
                responseString = Unirest.put(URL)
                        .header("Content-Type", "application/json; charset=utf-8")
                        .header("x-access-token", KeysUtil.TOKEN)
                        .header("x-trace-request-id", requestId)
                        .body(body)
                        .asString();
            } else {
                responseString = Unirest.post(URL)
                        .header("Content-Type", "application/json; charset=utf-8")
                        .header("x-access-token", KeysUtil.TOKEN)
                        .header("x-trace-request-id", requestId)
                        .body(body)
                        .asString();
            }

            success = getResult(responseString);

            if (!success) {
                // re-authenticate
                String authBody = "{ \"username\" : \"" + ServerConfig.auth_username + "\", \"password\" : \"" + ServerConfig.auth_password + "\" }";
                HttpResponse<JsonNode> jsonAuth = Unirest.post(ServerConfig.server_auth)
                        .header("Content-Type", "application/json; charset=utf-8")
                        .body(authBody)
                        .asJson();
                JSONObject objectData = gson.fromJson(jsonAuth.getBody().toString(), JSONObject.class);
                KeysUtil.TOKEN = objectData.get("token").toString();

                // submit with new token
                if (method.equals("put")) {
                    responseString = Unirest.put(URL)
                            .header("Content-Type", "application/json; charset=utf-8")
                            .header("x-access-token", KeysUtil.TOKEN)
                            .header("x-trace-request-id", requestId)
                            .body(body)
                            .asString();
                } else {
                    responseString = Unirest.post(URL)
                            .header("Content-Type", "application/json; charset=utf-8")
                            .header("x-access-token", KeysUtil.TOKEN)
                            .header("x-trace-request-id", requestId)
                            .body(body)
                            .asString();
                }

                success = getResult(responseString);
            }
        } catch (Exception ex) {
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            logger.debug(requestId + " - exception: " + errors.toString());

        }
        return success;
    }

    private boolean getResult(HttpResponse<String> responseString) throws Exception {
        String result = responseString.getBody();
        logger.debug(requestId + " - resultStr: " + result);
        //{"success":false,"authenticators":["httpauth","jwt","token"],"message":"All of authenticators were failed"}
        //[{"success":true,...}]
        try {
            JSONArray storeObj = gson.fromJson(result, JSONArray.class);
            JSONObject object = gson.fromJson(gson.toJson(storeObj.get(0)), JSONObject.class);
            return object.get("success") != null && Boolean.valueOf(object.get("success").toString());
        } catch (Exception ex) {
            JSONObject object = gson.fromJson(result, JSONObject.class);
            return object.get("success") != null && Boolean.valueOf(object.get("success").toString());
        }
    }

    public boolean sendToReport(String body, String target, String URL) {
        boolean success = false;
        try {
            HttpResponse<String> responseString = null;
            List<String> post = Arrays.asList("report", "hydraIncome", "DriverWalletTransaction", "SendBilling", "SendHydraIncome", "SendTipReport",
                    "SendWalletTransfer", "UpdateOutStanding", "Trip");
            if (post.contains(target)) {
                responseString = Unirest.post(URL)
                        .header("Content-Type", "application/json; charset=utf-8; charset=utf-8")
                        .header("x-access-token", KeysUtil.TOKEN)
                        .header("X-Trace-Request-Id", requestId)
                        .body(body)
                        .asString();
            } else if (target.equals("dashboard") || target.equals("dmc") || target.equals("corp")) {
                responseString = Unirest.put(URL)
                        .header("Content-Type", "application/json; charset=utf-8; charset=utf-8")
                        .header("x-access-token", KeysUtil.TOKEN)
                        .header("X-Trace-Request-Id", requestId)
                        .body(body)
                        .asString();
            }
            if (responseString != null) {
                logger.debug(requestId + " - " + target + " response: " + responseString.getBody());
                success = getResult(responseString);
            }

            if (!success) {
                // re-authenticate
                String authBody = "{ \"username\" : \"" + ServerConfig.auth_username + "\", \"password\" : \"" + ServerConfig.auth_password + "\" }";
                HttpResponse<JsonNode> jsonAuth = Unirest.post(ServerConfig.server_auth)
                        .header("Content-Type", "application/json; charset=utf-8")
                        .body(authBody)
                        .asJson();
                JSONObject objectData = gson.fromJson(jsonAuth.getBody().toString(), JSONObject.class);
                KeysUtil.TOKEN = objectData.get("token").toString();

                // submit with new token
                if (post.contains(target)) {
                    responseString = Unirest.post(URL)
                            .header("Content-Type", "application/json; charset=utf-8; charset=utf-8")
                            .header("x-access-token", KeysUtil.TOKEN)
                            .header("X-Trace-Request-Id", requestId)
                            .body(body)
                            .asString();
                } else if (target.equals("dashboard") || target.equals("dmc") || target.equals("corp")) {
                    responseString = Unirest.put(URL)
                            .header("Content-Type", "application/json; charset=utf-8; charset=utf-8")
                            .header("x-access-token", KeysUtil.TOKEN)
                            .header("X-Trace-Request-Id", requestId)
                            .body(body)
                            .asString();
                }
                if (responseString != null) {
                    logger.debug(requestId + " - " + target + " response: " + responseString.getBody());
                    success = getResult(responseString);
                }
            }
        } catch (Exception ex) {
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            logger.debug(requestId + " - " + target + " response: " + errors.toString());
            // try to submit again to get error message
            try {
                HttpResponse<String> json = null;
                // submit data
                if (target.equals("report")) {
                    json = Unirest.post(URL)
                            .header("Content-Type", "application/json; charset=utf-8; charset=utf-8")
                            .header("x-access-token", KeysUtil.TOKEN)
                            .header("X-Trace-Request-Id", requestId)
                            .body(body)
                            .asString();
                } else if (target.equals("dashboard") || target.equals("dmc")) {
                    json = Unirest.put(URL)
                            .header("Content-Type", "application/json; charset=utf-8; charset=utf-8")
                            .header("x-access-token", KeysUtil.TOKEN)
                            .header("X-Trace-Request-Id", requestId)
                            .body(body)
                            .asString();
                }
                logger.debug(requestId + " - " + target + " response: " + json);
            } catch (Exception ex2) {
                errors = new StringWriter();
                ex2.printStackTrace(new PrintWriter(errors));
                logger.debug(requestId + " - " + target + " response: " + errors.toString());
            }

        }
        return success;
    }

}