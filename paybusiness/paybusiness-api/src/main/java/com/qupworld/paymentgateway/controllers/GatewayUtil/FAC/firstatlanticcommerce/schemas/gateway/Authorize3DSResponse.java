
package com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Authorize3DSResult" type="{http://schemas.firstatlanticcommerce.com/gateway/data}Authorize3DSResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "authorize3DSResult"
})
@XmlRootElement(name = "Authorize3DSResponse")
public class Authorize3DSResponse {

    @XmlElement(name = "Authorize3DSResult", nillable = true)
    protected com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.Authorize3DSResponse authorize3DSResult;

    /**
     * Gets the value of the authorize3DSResult property.
     * 
     * @return
     *     possible object is
     *     {@link com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.Authorize3DSResponse }
     *     
     */
    public com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.Authorize3DSResponse getAuthorize3DSResult() {
        return authorize3DSResult;
    }

    /**
     * Sets the value of the authorize3DSResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.Authorize3DSResponse }
     *     
     */
    public void setAuthorize3DSResult(com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.Authorize3DSResponse value) {
        this.authorize3DSResult = value;
    }

}
