package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.google.gson.Gson;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.entities.SettlementNotificationEnt;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class MolPayUtil {
    final static Logger logger = LogManager.getLogger(MolPayUtil.class);

    private static String VERIFY_KEY = "f7a213d69e4b6326f7e7836d2b030452";
    private static String MERCHANT_ID = "SB_mycar";
    private static String SECRET_KEY = "7cade1fef3fdad52c3bd90cf8361e259";
    private static String url;
    private static String tokenUrl;
    private static String payUrl;
    private static String requestId;

    /*final String fullName = "Receiver Name";
    final String NIRC = "1122545521";
    final String companyName = "1122545521";
    final String bank_name = "CIMB Bank Berhad";
    final String bank_code = "CIBBMYKL";
    final String bank_accName = "CIMB Bank Berhad";
    final String bank_accNumber = "8003453572";
    final String email = "receiver@molpay.com";
    final String mobile = "0143342991";*/

    public MolPayUtil(String _requestId, String merchantId, String secretKey, String verifyKey,
                      String _url, String _tokenUrl, String _payUrl) {
        MERCHANT_ID = merchantId;
        SECRET_KEY = secretKey;
        VERIFY_KEY = verifyKey;
        url = _url;
        tokenUrl = _tokenUrl;
        payUrl = _payUrl;
        requestId = _requestId;
    }

    public String createACHToken(String fullName, String NIRC, String idType, String bankNumber, String bankCode, String nameOfAccount, String phone, String email){
        ObjResponse achResponse = new ObjResponse();
        try {
            JSONObject objBank = new JSONObject();
            objBank.put("Type", "Individual");
            objBank.put("Full_Name", fullName);
            objBank.put("NRIC_Passport", NIRC);
            objBank.put("ID_Type", idType);
            objBank.put("Country", "MY");
            objBank.put("Bank_Code", bankCode);
            objBank.put("Bank_AccName", nameOfAccount);
            objBank.put("Bank_AccNumber", bankNumber);
            objBank.put("Email", email);
            objBank.put("Mobile", phone);

            String profile_hash = md5(objBank.toJSONString());
            String skey = md5("new" + MERCHANT_ID + objBank.toJSONString() + profile_hash + sha1(SECRET_KEY));

            ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
            postParameters.add(new BasicNameValuePair("operator", MERCHANT_ID));
            postParameters.add(new BasicNameValuePair("func", "new"));
            postParameters.add(new BasicNameValuePair("profile", objBank.toString()));
            postParameters.add(new BasicNameValuePair("profile_hash", profile_hash));
            postParameters.add(new BasicNameValuePair("skey", skey));
            for (NameValuePair param : postParameters) {
                logger.debug(requestId + " - " + param.getName() + ": " + param.getValue());
            }
            HttpClient client = HttpClientBuilder.create().build();

            HttpPost httpRequest = new HttpPost(tokenUrl);
            httpRequest.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));

            HttpResponse httpResponse = client.execute(httpRequest);
            String paymentResponse = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
            logger.debug(requestId + " - Payee Result: " + paymentResponse);

            /*String sample = "{\"StatCode\":\"00\",\"payeeID\":18,\"skey\":\"5de5b84a388648313f9cb3a47a9417b4\"," +
                    "\"profile_hash\":\"78749c71a7eb144e608d2ce5ad32de17\",\"operator\":\"SB_mycar\",\"VrfKey\":\"982820056375620758939f5120390345\"}";*/

            Gson gson = new Gson();
            JSONObject objResponse = gson.fromJson(paymentResponse, JSONObject.class);
            String StatCode = objResponse.get("StatCode") != null ? String.valueOf(objResponse.get("StatCode")) : "";

            Map<String,Object> mapResponse = new HashMap<>();
            if (StatCode.equals("00")) {
                int payeeID = Integer.valueOf(objResponse.get("payeeID") != null ? objResponse.get("payeeID").toString().replace(".0", "") : "0");
                /*String token = objResponse.get("payeeID") != null ? String.valueOf(objResponse.get("payeeID")) : "";*/
                mapResponse.put("token", payeeID);

                achResponse.returnCode = 200;
                achResponse.response = mapResponse;
            } else {
                String responseText = objResponse.get("error_desc") != null ? String.valueOf(objResponse.get("error_desc")) : "";
                mapResponse.put("message", responseText);
                achResponse.returnCode = 493;
                achResponse.response = mapResponse;
            }

            return achResponse.toString();
        } catch(Exception ex) {
            ex.printStackTrace();
            logger.debug(requestId + " - Message is: " + ex.getMessage());
            achResponse.returnCode = 493;
            return achResponse.toString();

        }
    }

    public String payToDriver(String driverId, String referenceId, String token, double amount, String currencyISO) {
        logger.debug(requestId + " - payToDriver: " + driverId);
        ObjResponse objResponse = new ObjResponse();
        try {
            String notifyURL = ServerConfig.dispatch_server + KeysUtil.SETTLEMENT_MOLPAY_URL;
            if (ServerConfig.dispatch_server.equals("http://dispatch.local.qup.vn")) {
                notifyURL = "http://113.160.232.234:1337" + KeysUtil.SETTLEMENT_MOLPAY_URL;
            }
            String skey = md5(MERCHANT_ID + token + KeysUtil.DECIMAL_FORMAT.format(amount) + currencyISO + sha1(SECRET_KEY));

            ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
            postParameters.add(new BasicNameValuePair("operator", MERCHANT_ID));
            postParameters.add(new BasicNameValuePair("payeeID", token));
            postParameters.add(new BasicNameValuePair("amount", KeysUtil.DECIMAL_FORMAT.format(amount)));
            postParameters.add(new BasicNameValuePair("currency", currencyISO));
            postParameters.add(new BasicNameValuePair("skey", skey));
            postParameters.add(new BasicNameValuePair("reference_id", referenceId));
            postParameters.add(new BasicNameValuePair("notify_url", notifyURL));
            for (NameValuePair param : postParameters) {
                logger.debug(requestId + " - " + param.getName() + ": " + param.getValue());
            }
            HttpClient client = HttpClientBuilder.create().build();

            HttpPost httpRequest = new HttpPost(payUrl);
            httpRequest.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));

            HttpResponse httpResponse = client.execute(httpRequest);
            String paymentResponse = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
            logger.debug(requestId + " - Pay Result: " + paymentResponse);

            /*String pay = "Credit Result: {\"StatCode\":\"00\",\"skey\":\"4be16021ad913b00d018b2f28c39083a\",\"operator\":\"SB_mycar\"," +
                    "\"VrfKey\":\"d305e0636d41a1da3106c871d3bdb4b8\",\"mass_id\":45,\"reference_id\":\"20190306082111181\"}";*/

            Gson gson = new Gson();
            JSONObject response = gson.fromJson(paymentResponse, JSONObject.class);
            String statCode = response.get("StatCode") != null ? String.valueOf(response.get("StatCode")) : "";
            String mass_id = response.get("mass_id") != null ? String.valueOf(response.get("mass_id")) : "";

            if (statCode.equals("00")) {
                Map<String,String> mapResult = new HashMap<>();
                mapResult.put("transactionId", referenceId);
                mapResult.put("mass_id", mass_id);
                objResponse.response = mapResult;
                objResponse.returnCode = 518;
            } else {
                Map<String,String> mapResult = new HashMap<>();
                String responseText = response.get("error_desc") != null ? String.valueOf(response.get("error_desc")) : "";
                mapResult.put("message", responseText);
                objResponse.returnCode = getReturnCode(statCode);
                objResponse.response = mapResult;
            }
            return objResponse.toString();
        } catch(Exception ex) {
            ex.printStackTrace();
            objResponse.returnCode = 437;
            objResponse.response = GatewayUtil.getError(ex);
            return objResponse.toString();
        }
    }

    public boolean isValidRequest(SettlementNotificationEnt settlementEnt, String payeeID, double amount, String currencyISO) {
        try {
            String skeyString = settlementEnt.operator + payeeID + KeysUtil.DECIMAL_FORMAT.format(amount) + currencyISO + sha1(SECRET_KEY);
            logger.debug(requestId + " - skeyString: " + skeyString);
            String skey = md5(skeyString);
            logger.debug(requestId + " - skey: " + skey);
            String VrfKeyString = sha1(SECRET_KEY) + skey + settlementEnt.operator + settlementEnt.StatCode + settlementEnt.mass_id + settlementEnt.reference_id;
            logger.debug(requestId + " - VrfKeyString: " + VrfKeyString);
            String VrfKey = md5(VrfKeyString);
            logger.debug(requestId + " - VrfKey: " + VrfKey);
            return VrfKey.equals(settlementEnt.VrfKey);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    private int getReturnCode(String statCode) {
        return 525;
    }


    public String initiateCreditForm(String orderId, String fleetName, String name, String phone, String email, double amount, String plateNumber,
                                     String osType, String channel) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();

        String checkoutUrl = "";
        try {
            String baseUrl = url + MERCHANT_ID;
            if (!channel.isEmpty()) {
                baseUrl = baseUrl + "/" + channel.trim();
            }
            String callbackURL = ServerConfig.hook_server + "/notification/" + KeysUtil.MOLPAY + "?urlAction=callback";

            String vcode = getMD5Hex(String.valueOf(amount) + MERCHANT_ID + orderId + VERIFY_KEY);

            String desc = fleetName + " Credit Wallet Top Up for "+ phone;
            if (!plateNumber.isEmpty())
                desc = desc + " and " + plateNumber;

            phone = phone.replace(" ", "");

            String returnURL;
            if (osType.toLowerCase().contains("android")) {
                    /*String returnURL = ServerConfig.hook_server + "/notification/" + KeysUtil.MOLPAY + "?urlAction=return";*/

                if (orderId.contains("PW")) {
                    returnURL = "https://link.gojo.global/deeplink?schema=gojosgp://";
                } else {
                    returnURL = "https://link.gojo.global/deeplink?schema=gojosgd://";
                }
            } else {
                // add orderId to detect gateway
                returnURL = ServerConfig.dispatch_server + KeysUtil.NOTIFICATION_URL;
                if (ServerConfig.dispatch_server.equals("http://dispatch.local.qup.vn")) {
                    returnURL = "http://113.160.232.234:1337" + KeysUtil.NOTIFICATION_URL;
                }

                if (ServerConfig.dispatch_server.equals("https://dispatch-sea.qupworld.com")) {
                    returnURL = "https://dispatch.gojo.asia" + KeysUtil.NOTIFICATION_URL;
                }
                returnURL = returnURL + "?MOLPayOrderid=" + orderId;
            }

            checkoutUrl = baseUrl + "?orderid=" + orderId
                    + "&vcode=" + vcode
                    + "&merchant_id=" + MERCHANT_ID
                    + "&bill_desc=" + URLEncoder.encode(desc, StandardCharsets.UTF_8.toString())
                    + "&amount=" + String.valueOf(amount)
                    + "&bill_name=" + URLEncoder.encode(name, StandardCharsets.UTF_8.toString())
                    + "&bill_email=" + email
                    + "&bill_mobile=" + URLEncoder.encode(phone, StandardCharsets.UTF_8.toString())
                    + "&country=MY"
                    + "&cur=MYR"
                    + "&returnurl=" + returnURL
                    + "&callbackurl=" + callbackURL
            ;

        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }

        objResponse.returnCode = 200;
        mapResponse.put("3ds_url", checkoutUrl);
        mapResponse.put("type", "link");
        mapResponse.put("transactionId", orderId);
        objResponse.response = mapResponse;
        return objResponse.toString();
    }

    public String completePayment(String requestId, String molPayOrderId, double amount, String currencyISO) {
        logger.debug(requestId + " - molPayOrderId: " + molPayOrderId);
        ObjResponse objResponse = new ObjResponse();
        Map<String,Object> mapResponse = new HashMap<>();
        Gson gson = new Gson();
        JSONObject obj = GatewayUtil.getDataFromRedis(requestId, molPayOrderId + "-topupresult", 60);
        logger.debug(requestId + " - getDataFromRedis :" + gson.toJson(obj));
        if (obj != null) {
            if (obj.get("status").toString().equals("00")) {
                String cardType = "";
                String cardMask = "";
                String extraP = obj.get("extraP") != null? obj.get("extraP").toString() : "";
                if (!extraP.isEmpty()) {
                    extraP = extraP.replace("\\","");
                    JSONObject objExtra = gson.fromJson(extraP, JSONObject.class);
                    cardType = objExtra.get("ccbrand") != null ? objExtra.get("ccbrand").toString() : "";
                    cardMask = objExtra.get("cclast4") != null ? "XXXXXXXXXXXX" + objExtra.get("cclast4").toString() : "";
                }
                mapResponse.put("message", "Success!");
                mapResponse.put("transactionId", obj.get("tranID") != null? obj.get("tranID").toString() : "");
                mapResponse.put("amount", amount);
                mapResponse.put("currencyISO", currencyISO);
                objResponse.response = mapResponse;
                objResponse.returnCode = 200;
            } else {
                mapResponse.put("message", obj.get("error_desc") != null? obj.get("error_desc").toString() : "");
                objResponse.response = mapResponse;
                objResponse.returnCode = 437;
            }
        } else {
            objResponse.returnCode = 437;
        }

        return objResponse.toString();
    }


    /***********************************************************
     * Either two of method could be used for verification
     * @param input String text
     * @return MD5 Hashing
     * @throws java.security.NoSuchAlgorithmException
     ************************************************************/
    public static String md5(String input) throws NoSuchAlgorithmException {
        String result = input;
        if(input != null) {
            MessageDigest md = MessageDigest.getInstance("MD5"); //MD5 or SHA-1
            md.update(input.getBytes());
            BigInteger hash = new BigInteger(1, md.digest());
            result = hash.toString(16);
            while(result.length() < 32) { //32 for MD5, 40 for SHA-1
                result = "0" + result;
            }
        }
        return result;
    }

    public static String sha1(String input) throws NoSuchAlgorithmException {
        String result = input;
        if(input != null) {
            MessageDigest md = MessageDigest.getInstance("SHA-1"); //MD5 or SHA-1
            md.update(input.getBytes());
            BigInteger hash = new BigInteger(1, md.digest());
            result = hash.toString(16);
            while(result.length() < 40) { //32 for MD5, 40 for SHA-1
                result = "0" + result;
            }
        }
        return result;
    }

    /**
     * @param inputString
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static String getMD5Hex(final String inputString) throws NoSuchAlgorithmException {

        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(inputString.getBytes());

        byte[] digest = md.digest();

        return convertByteToHex(digest);
    }

    public static String getSHA1Hex(final String inputString) throws NoSuchAlgorithmException {

        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(inputString.getBytes());

        byte[] digest = md.digest();

        return convertByteToHex(digest);
    }

    private static String convertByteToHex(byte[] byteData) {

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }

    /***********************************************************
     * Generate random order ID
     * @return Get random between 0-100000
     ************************************************************/

    public static String getRandomOrderId(){
        SimpleDateFormat formatOrder = new SimpleDateFormat("SSS");
        return formatOrder.format(new Date()) + (int)(Math.random()*10000000);
    }

    /***********************************************************
     * Get the current redirect URL based on demo type
     * e.g. usage for form action=""
     * @param input
     * @param merchantid
     * @return
     ************************************************************/

    /*public static String url(String input, String merchantid, String channel) {
        String url = "";
        if(input.equals("production")) {
            url = "https://www.onlinepayment.com.my/MOLPay/pay/" + merchantid;
        } else if(input.equals("sandbox")){
            url = "https://sandbox.merchant.razer.com/MOLPay/pay/" + merchantid;
        }
        if (!channel.isEmpty()) {
            url = url + "/" + channel;
        }
        logger.debug(requestId + " - url: " + url);
        return url;
    }*/
    /***********************************************************
     * Get the BaseURL. The payment page could be obtain using method
     * e.g return values: https://localhost/root/folder/payment/
     * @param requestURL http://localhost:8080/test/uriTest.jsp
     * @param requestURI /test/uriTest.jsp
     * @param contextPath Request Context Path: /test
     * @return URL without filename
     ************************************************************/
    public static String getBaseURL(String requestURL, String requestURI, String contextPath){
        String url = requestURL.toString();
        String baseURL = url.substring(0, url.length() - requestURI.length()) + contextPath + "/";
        return baseURL;
    }

    /***********************************************************
     * Key1 : Hashstring generated on Merchant system
     * either $merchant or $domain could be one from POST
     * and one that predefined internally
     * by right both values should be identical
     * @return Hash String
     * @throws java.security.NoSuchAlgorithmException
     ************************************************************/

    public static String generateKeys(String tranID, String orderid, String status, String domain, String amount, String currency, String paydate, String appcode, String vkey ) throws NoSuchAlgorithmException {
        // on response payment page
        String key0 = getMD5Hex(tranID + orderid + status + domain + amount + currency);
        String key1 = getMD5Hex(paydate + domain + key0 + appcode + vkey);
        return key1;
    }

    /**
     *
     */
    private void TrustCert() {
        TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {}

                    public void checkServerTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {}
                }
        };

        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {}
    }

    /**
     * Request parameters:
     * @param type Demo type
     * @param skey This is the data integrity protection hash string
     * @param tranID Generated by MOLPay System. Integer, 10 digits. Unique transaction ID for tracking purpose
     * @param domain Merchant ID in MOLPay system.
     * @param status 2-digit numeric value, 00 for Successful payment; 11 for failure; 22 for pending.
     * @param amount 2 decimal points numeric value. The total amount paid or to be paid for CASH payment request.
     * @param currency 2 or 3 chars (ISO-4217) currency code. Default currency is RM (indicating MYR) for Malaysia channels
     * @param paydate Date/Time( YYYY-MM-DD HH:mm:ss). Date/Time of the transaction
     * @param orderid Alphanumeric, 32 characters. Invoice or order number from merchant system.
     * @param appcode Alphanumeric, 16 chars. Bank approval code. Mandatory for Credit Card. Certain channel returns empty value
     * @param error_code Error Codes section
     * @param error_desc Error message or description
     * @param channel Predefined string in MOLPay system. Channel references for merchant system
     * @param extraP JSON encoded string or array
     * @return Sent Parameters
     * @throws Exception
     */

    public String IPN_Return(String type, String skey, String tranID, String domain, String status, String amount, String currency, String paydate, String orderid, String appcode, String error_code, String error_desc, String channel, String extraP) throws Exception {
        TrustCert();
        // Check either demo type is production or sandbox
        String url = CheckDemo(type);

        // each received values from MOLPay
        String blocks[] = {
                skey, tranID, domain, status, amount, currency, paydate, orderid, appcode, error_code, error_desc, channel, extraP
        };

        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        ArrayList<String> parameter = new ArrayList<String>();
        parameter.add("skey");
        parameter.add("tranID");
        parameter.add("domain");
        parameter.add("status");
        parameter.add("amount");
        parameter.add("currency");
        parameter.add("paydate");
        parameter.add("orderid");
        parameter.add("appcode");
        parameter.add("error_code");
        parameter.add("error_desc");
        parameter.add("channel");
        parameter.add("extraP");

        // Add request header
        String USER_AGENT = "Mozilla/5.0";
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        // Dont change default values
        String urlParameters = "treq=1"; // Additional parameter for IPN. Value always set to 1.
        // Generate urlString for each received values from MOLPay
        for (int i = 0; i < parameter.size() ; i++) {
            urlParameters = urlParameters.concat("&" + parameter.get(i) + "=" + blocks[i]);
        }

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        logger.debug(requestId + " - urlParameters: " + urlParameters);
        logger.debug(requestId + " - responseCode: " + responseCode);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in .readLine()) != null) {
            response.append(inputLine);
        } in .close();
        return response.toString();
    }

    /**
     *
     * @param type get demo type to either sandbox or production demo
     * @return demo type
     */
    private String CheckDemo(String type) {
        // Check either demo type is production or sandbox
        String url = "";
        if (type.equals("production")) {
            url = "https://onlinepayment.com.my/MOLPay/API/chkstat/returnipn.php";
        } else if (type.equals("sandbox")) {
            url = "https://sandbox.merchant.razer.com/MOLPay/API/chkstat/returnipn.php";
        }
        return url;
    }

    /**
     * Request parameters:
     * @param type Demo type
     * @param nbcb Always equal to 2​, which indicates this is a notification from MOLPay
     * @param skey This is the data integrity protection hash string
     * @param tranID Generated by MOLPay System. Integer, 10 digits. Unique transaction ID for tracking purpose
     * @param domain Merchant ID in MOLPay system.
     * @param status 2-digit numeric value, 00 for Successful payment; 11 for failure; 22 for pending.
     * @param amount 2 decimal points numeric value. The total amount paid or to be paid for CASH payment request.
     * @param currency 2 or 3 chars (ISO-4217) currency code. Default currency is RM (indicating MYR) for Malaysia channels
     * @param paydate Date/Time( YYYY-MM-DD HH:mm:ss). Date/Time of the transaction
     * @param orderid Alphanumeric, 32 characters. Invoice or order number from merchant system.
     * @param appcode Alphanumeric, 16 chars. Bank approval code. Mandatory for Credit Card. Certain channel returns empty value
     * @param error_code Error Codes section
     * @param error_desc Error message or description
     * @param channel Predefined string in MOLPay system. Channel references for merchant system
     * @param extraP JSON encoded string or array
     * @return Sent Parameters
     * @throws Exception
     */
    public String IPN_Notification(String type, String nbcb, String skey, String tranID, String domain, String status, String amount, String currency, String paydate, String orderid, String appcode, String error_code, String error_desc, String channel, String extraP) throws Exception {
        TrustCert();
        // Check either demo type is production or sandbox
        String url = CheckDemo(type);

        // each received values from MOLPay
        String blocks[] = {
                nbcb, skey, tranID, domain, status, amount, currency, paydate, orderid, appcode, error_code, error_desc, channel, extraP
        };

        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        ArrayList<String> parameter = new ArrayList<String>();
        parameter.add("nbcb");
        parameter.add("skey");
        parameter.add("tranID");
        parameter.add("domain");
        parameter.add("status");
        parameter.add("amount");
        parameter.add("currency");
        parameter.add("paydate");
        parameter.add("orderid");
        parameter.add("appcode");
        parameter.add("error_code");
        parameter.add("error_desc");
        parameter.add("channel");
        parameter.add("extraP");

        // Add request header
        String USER_AGENT = "Mozilla/5.0";
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        // Dont change default values
        String urlParameters = "treq=1"; // Additional parameter for IPN. Value always set to 1.
        // Generate urlString for each received values from MOLPay
        for (int i = 0; i < parameter.size() ; i++) {
            urlParameters = urlParameters.concat("&" + parameter.get(i) + "=" + blocks[i]);
        }

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        logger.debug(requestId + " - urlParameters: " + urlParameters);
        logger.debug(requestId + " - responseCode: " + responseCode);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in .readLine()) != null) {
            response.append(inputLine);
        } in .close();
        return response.toString();
    }
}
