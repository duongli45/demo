package com.qupworld.paymentgateway.controllers.scheduling;

import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.MongoDaoImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.UUID;

/**
 * Created by myasus on 1/13/21.
 */
public class RedeemExpiredJob implements Job {
    final static Logger logger = LogManager.getLogger(RedeemExpiredJob.class);

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        String requestId = UUID.randomUUID().toString();
        logger.debug(requestId + "- check expired voucher....");
        MongoDao mongoDao = new MongoDaoImpl();
        try {
            boolean updated = mongoDao.updateVoucherCodeExpired();
            if(updated)
                logger.debug(requestId + "- updates expired voucher done ....");
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }
}
