package com.qupworld.paymentgateway.controllers.threading;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.entities.ReferralTransaction;
import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.MongoDaoImpl;
import com.qupworld.paymentgateway.models.SQLDao;
import com.qupworld.paymentgateway.models.SQLDaoImpl;
import com.qupworld.paymentgateway.models.mongo.collections.account.Account;
import com.qupworld.paymentgateway.models.mongo.collections.referral.ReferralInfo;
import com.pg.util.KeysUtil;
import com.pg.util.TimezoneUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

/**
 * Created by qup on 23/03/2020.
 */
@SuppressWarnings("unchecked")
public class SyncReferralTransaction extends Thread {

    final static Logger logger = LogManager.getLogger(SyncReferralTransaction.class);
    public String requestId;

    @Override
    public void run() {
        try {
            Gson gsonUpdate = new GsonBuilder().serializeNulls().create();
            MongoDao mongoDao = new MongoDaoImpl();
            SQLDao sqlDao = new SQLDaoImpl();
            int i = 0;
            int size = 10;
            boolean valid = true;
            while (valid) {

                List<ReferralTransaction> all = sqlDao.getReferralTransactions(i, size);
                JSONArray arrData = new JSONArray();
                logger.debug(requestId + " - get transaction from " + i*size + ", limit " + size);
                for (ReferralTransaction transaction : all) {
                    logger.debug(requestId + " - userId:" + transaction.userId+ " - bookId: " + transaction.bookId);
                    Account account = mongoDao.getAccount(transaction.userId);
                    String companyId = "";
                    String companyName = "";
                    String driverPhone = "";
                    String driverName = "";
                    String driverNumber = "";
                    if (account != null && account.driverInfo != null && account.driverInfo.company != null) {
                        driverName = account.firstName + " " + account.lastName;
                        driverName = driverName.trim(); // remove spaces
                        driverPhone = account.phone != null ? account.phone : "";
                        companyId = account.driverInfo.company.companyId != null ? account.driverInfo.company.companyId : "";
                        companyName = account.driverInfo.company.name != null ? account.driverInfo.company.name : "";
                        driverNumber = account.driverInfo.drvId != null ? account.driverInfo.drvId : "";
                    }
                    String referralCode = "";
                    ReferralInfo referralInfo = mongoDao.getReferralInfo(transaction.fleetId, transaction.userId);
                    if (referralInfo != null) {
                        referralCode = referralInfo.code != null ? referralInfo.code : "";
                    }

                    transaction.companyId = companyId;
                    transaction.companyName = companyName;
                    transaction.phone = driverPhone;
                    transaction.fullName = driverName;
                    transaction.driverNumber = driverNumber;
                    transaction.referralCode = referralCode;
                    transaction.createdTime = TimezoneUtil.getGMTTimestamp();

                    String referralTransactionStr = gsonUpdate.toJson(transaction);
                    JSONObject jsonReferralTransaction = gsonUpdate.fromJson(referralTransactionStr, JSONObject.class);
                    jsonReferralTransaction.remove("createdTime");
                    jsonReferralTransaction.put("createdTime", TimezoneUtil.formatISODate(transaction.createdTime, "GMT"));

                    JSONObject objTicket = new JSONObject();
                    objTicket.put("type", "ReferralTransaction");
                    objTicket.put("id", transaction.bookId);
                    objTicket.put("body", jsonReferralTransaction);
                    arrData.add(objTicket);
                }
                logger.debug(requestId + " - data: " + arrData.toJSONString());
                ThreadUtil threadUtil = new ThreadUtil(requestId);
                boolean sendSuccess = threadUtil.sendToReport(arrData.toJSONString(), "report", ServerConfig.server_report);
                if (!sendSuccess) {
                    // failed on the first time, try to re-authenticate token then send again
                    String authBody = "{ \"username\" : \"" + ServerConfig.auth_username + "\" , \"password\" : \"" + ServerConfig.auth_password + "\" }";
                    HttpResponse<JsonNode> jsonAuth = Unirest.post(ServerConfig.server_auth)
                            .header("Content-Type", "application/json; charset=utf-8")
                            .header("X-Trace-Request-Id", requestId)
                            .body(authBody)
                            .asJson();
                    JSONParser parser = new JSONParser();
                    JSONObject objectData = (JSONObject) parser.parse(jsonAuth.getBody().toString());
                    KeysUtil.TOKEN = objectData.get("token").toString();
                    // submit with new token
                    threadUtil.sendToReport(arrData.toJSONString(), "report", ServerConfig.server_report);
                }

                if (all.size() < size) {
                    valid = false;
                } else {
                    i++;
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ignore){}
                }
            }


        } catch(Exception ex) {
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            logger.debug(requestId + " - ResetCashWallet: "+ errors.toString());
        }

    }

}
