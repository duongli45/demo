package com.qupworld.paymentgateway.controllers;

import com.pg.util.CommonUtils;
import com.pg.util.ObjResponse;
import com.pg.util.TimezoneUtil;
import com.qupworld.paymentgateway.controllers.threading.UpdateMerchantWalletBalanceThread;
import com.qupworld.paymentgateway.entities.MerchantWalletTransaction;
import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.SQLDao;
import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.BalanceByCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.UsersSystem;
import com.qupworld.paymentgateway.models.mongo.collections.fleet.Fleet;
import com.qupworld.paymentgateway.models.mongo.collections.menuMerchant.MenuMerchant;
import com.qupworld.paymentgateway.models.mongo.collections.menuMerchantUser.MenuMerchantUser;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
public class UpdateMerchantWalletBalance {

    private final static Logger logger = LogManager.getLogger(UpdateMerchantWalletBalance.class);
    public String walletType;
    public String fleetId;
    public String merchantId;
    public double currentBalance;
    public double amount;
    public double newBalance;
    public String currencyISO;
    public MongoDao mongoDao;
    public SQLDao sqlDao;
    public String reason = "";
    public String transactionType = "";
    public String transactionId = "";
    public String bookId = "";
    public String operatorId = "";
    public String requestId;

    public String doUpdate() {
        try {
            String currencySymbol = "";
            Fleet fleet = mongoDao.getFleetInfor(fleetId);
            for (com.qupworld.paymentgateway.models.mongo.collections.fleet.Currency currency : fleet.currencies) {
                if (currencyISO.equals(currency.iso))
                    currencySymbol = currency.symbol;
            }
            String operatorName = "";
            String operatorUserName = "";
            if (!operatorId.isEmpty()) {
                UsersSystem usersSystem = mongoDao.getUsersSystem(operatorId);
                if (usersSystem != null) {
                    String firstName = usersSystem.firstName != null ? usersSystem.firstName.trim() : "";
                    String lastName = usersSystem.lastName != null ? usersSystem.lastName.trim() : "";
                    operatorName = firstName + " " + lastName;
                    operatorUserName = usersSystem.userName;
                }
            }
            MenuMerchant merchant = mongoDao.getMerchant(merchantId);
            String merchantName = "";
            String merchantUserName = "";
            String merchantUserPhone = "";
            String merchantUserEmail = "";
            if (merchant != null) {
                merchantName = merchant.name;
                String adminId = merchant.admin != null ? merchant.admin.toString() : "";
                if (!adminId.isEmpty()) {
                    MenuMerchantUser menuMerchantUser = mongoDao.getMenuMerchantUser(adminId);
                    if (menuMerchantUser != null) {
                        merchantUserPhone = menuMerchantUser.phone != null && menuMerchantUser.phone.full != null ? menuMerchantUser.phone.full : "";
                        merchantUserEmail = menuMerchantUser.email != null ? menuMerchantUser.email : "";
                        String firstName = menuMerchantUser.firstName != null ? menuMerchantUser.firstName.trim() : "";
                        String lastName = menuMerchantUser.lastName != null ? menuMerchantUser.lastName.trim() : "";
                        merchantUserName = firstName + " " + lastName;
                    }
                }
            }

            MerchantWalletTransaction merchantWalletTransaction = new MerchantWalletTransaction();
            merchantWalletTransaction.fleetId = fleetId;
            merchantWalletTransaction.merchantId = merchantId;
            merchantWalletTransaction.walletType = walletType;
            merchantWalletTransaction.transactionType = transactionType;
            merchantWalletTransaction.transactionId = transactionId;
            merchantWalletTransaction.bookId = bookId;
            merchantWalletTransaction.reason = reason;
            merchantWalletTransaction.amount = amount;
            merchantWalletTransaction.newBalance = newBalance;
            merchantWalletTransaction.currencyISO = currencyISO;
            merchantWalletTransaction.currencySymbol = currencySymbol;
            merchantWalletTransaction.operatorId = operatorId;
            merchantWalletTransaction.operatorName = operatorName;
            merchantWalletTransaction.operatorUserName = operatorUserName;
            merchantWalletTransaction.merchantName = merchantName;
            merchantWalletTransaction.merchantUserName = merchantUserName.trim();
            merchantWalletTransaction.merchantUserPhone = merchantUserPhone;
            merchantWalletTransaction.merchantUserEmail = merchantUserEmail;
            merchantWalletTransaction.createdDate = TimezoneUtil.getGMTTimestamp();
            String result = doUpdateBalance(merchantWalletTransaction);
            if (CommonUtils.getReturnCode(result) == 200) {
                // save transaction to sync later
                long id = sqlDao.addMerchantWalletTransaction(merchantWalletTransaction);
                if (id == 0) {
                    // something went wrong, try to add again
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ignore) {}
                    id = sqlDao.addMerchantWalletTransaction(merchantWalletTransaction);
                }
                logger.debug(requestId + " - id merchantWalletTransaction: " + id);
            }
            return result;
        } catch(Exception ex) {
            logger.debug(requestId + " - UpdateDriverCreditBalance exception: " + CommonUtils.getError(ex));
            ObjResponse objResponse = new ObjResponse();
            objResponse.returnCode = 525;
            objResponse.message = "Something went wrong. Please try again later.";
            return objResponse.toString();
        }

    }

    private String doUpdateBalance(MerchantWalletTransaction merchantWalletTransaction) {
        ObjResponse objResponse = new ObjResponse();
        try {
            MerchantWalletUtil merchantWalletUtil = new MerchantWalletUtil(requestId);
            boolean isDeposit = merchantWalletTransaction.amount > 0;
            long id = 0;
            if (merchantWalletTransaction.walletType.equals("credit")) {
                id = merchantWalletUtil.updateCreditWallet(merchantWalletTransaction, isDeposit, merchantWalletTransaction.newBalance);
            } else if (merchantWalletTransaction.walletType.equals("cash")) {
                id = merchantWalletUtil.updateCashWallet(merchantWalletTransaction, isDeposit, merchantWalletTransaction.newBalance);
            }

            if (id != 0) {
                // call to Reporting thread
                UpdateMerchantWalletBalanceThread updateMerchantWalletBalanceThread = new UpdateMerchantWalletBalanceThread();
                merchantWalletTransaction.id = id;
                updateMerchantWalletBalanceThread.merchantWalletTransaction = merchantWalletTransaction;
                updateMerchantWalletBalanceThread.requestId = requestId;
                updateMerchantWalletBalanceThread.start();

                // everything is ok - return completed
                if (merchantWalletTransaction.walletType.equals("credit")) {
                    AmountByCurrency creditWallet = new AmountByCurrency();
                    creditWallet.value = merchantWalletTransaction.newBalance;
                    creditWallet.currencyISO = merchantWalletTransaction.currencyISO;
                    objResponse.response = creditWallet;
                } else if (merchantWalletTransaction.walletType.equals("cash")) {
                    BalanceByCurrency cashWallet = new BalanceByCurrency();
                    cashWallet.currentBalance = merchantWalletTransaction.newBalance;
                    cashWallet.currencyISO = merchantWalletTransaction.currencyISO;
                    objResponse.response = cashWallet;
                }
                objResponse.returnCode = 200;
                return objResponse.toString();
            } else {
                logger.debug(requestId + " - cannot update balance to database!!!");
                objResponse.returnCode = 525;
                return objResponse.toString();
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - doUpdateBalance exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            return objResponse.toString();
        }
    }
}
