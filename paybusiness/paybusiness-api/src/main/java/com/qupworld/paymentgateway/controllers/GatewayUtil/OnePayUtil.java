package com.qupworld.paymentgateway.controllers.GatewayUtil;

import static java.nio.charset.StandardCharsets.UTF_8;

import com.qupworld.config.ServerConfig;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class OnePayUtil {
    private static final Logger logger = LogManager.getLogger(OnePayUtil.class);
    private static final char[] hexCode = "0123456789ABCDEF".toCharArray();
    private static final String ALGORITHM_HMAC_SHA256 = "HMACSHA256";
    private static final String ENDPOINT_PAYMENT = "paygate/vpcpay.op";
    private static final String ENDPOINT_REFUND = "msp/api/v1/vpc/refunds";
    private static final String ENDPOINT_VOID =
            "paygate/api/v1/vpc/merchants/%s/purchases/%s/voids/%s?%s&vpc_SecureHash=%s";
    private static final String COMMAND_PAY = "pay";
    private static final String COMMAND_REFUND = "refund";
    private static final String COMMAND_VOID = "voidPurchase";
    private static final String VERSION = "2";
    private static final String CURRENCY = "VND";

    private String requestId;
    private String merchantId;
    private String accessCode;
    private String hashCode;
    private String serverUrl;

    public OnePayUtil() {}

    public OnePayUtil(
            String requestId,
            String merchantId,
            String accessCode,
            String hashCode,
            String serverUrl) {
        this.requestId = requestId;
        this.merchantId = merchantId;
        this.accessCode = accessCode;
        this.hashCode = hashCode;
        this.serverUrl = serverUrl;
    }

    public String requestPayment(Map<String, String> data, boolean createToken) {
        ObjResponse objResponse = new ObjResponse();
        Map<String, Object> mapResponse = new HashMap<>();
        String urlAction = createToken ? "token" : "payment";
        String paymentChannel =
                createToken ? "INTERNATIONAL,DOMESTIC" : "INTERNATIONAL,DOMESTIC,QR";
        try {
            String returnUrl =
                    ServerConfig.hook_server
                            + "/notification/"
                            + KeysUtil.ONEPAY
                            + "?urlAction="
                            + urlAction;
            Map<String, Object> param = new HashMap<>();
            if (!createToken) {
                param.put("vpc_TokenNum", data.get("token"));
                param.put("vpc_TokenExp", data.get("tokenExp"));
            }
            param.put("vpc_CreateToken", createToken);
            param.put("vpc_Customer_Id", data.get("userId"));
            param.put("vpc_Version", VERSION);
            param.put("vpc_Currency", CURRENCY);
            param.put("vpc_Command", COMMAND_PAY);
            param.put("vpc_AccessCode", accessCode);
            param.put("vpc_Merchant", merchantId);
            param.put("vpc_Locale", "vn");
            param.put("vpc_ReturnURL", returnUrl);
            param.put("vpc_MerchTxnRef", data.get("orderId"));
            param.put("vpc_OrderInfo", requestId.substring(0, 33));
            param.put("vpc_Amount", data.get("amount") + "00"); // requirement from OnePay
            param.put("vpc_TicketNo", data.get("ip"));
            param.put("vpc_CardList", paymentChannel);
            param.put("AgainLink", "https://www.goodjourney.io/");
            param.put("Title", "Pay with OnePay");
            String requestParams = buildSendRequest(param);
            String redirectUrl = execute(serverUrl + ENDPOINT_PAYMENT, requestParams);
            logger.debug(requestId + " - payment url: " + redirectUrl);
            objResponse.returnCode = 200;
            mapResponse.put("3ds_url", redirectUrl);
            mapResponse.put("deepLinkUrl", redirectUrl);
            mapResponse.put("type", "link");
            mapResponse.put("transactionId", data.get("orderId"));
            mapResponse.put("openInApp", true);
            objResponse.response = mapResponse;
            return objResponse.toString();
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
        }
        return objResponse.toString();
    }

    public String voidTransaction(String orgMerchTxnRef, String amount, String userId)
            throws Exception {
        ObjResponse objResponse = new ObjResponse();
        Map<String, Object> param = new HashMap<>();
        String merchTxnRef = GatewayUtil.getOrderId();
        param.put("vpc_Command", COMMAND_VOID);
        param.put("vpc_Amount", amount);
        param.put("vpc_AccessCode", accessCode);
        param.put("vpc_Merchant", merchantId);
        param.put("vpc_MerchTxnRef", merchTxnRef);
        param.put("vpc_OrgMerchTxnRef", orgMerchTxnRef);
        param.put("vpc_Operator", userId);
        param.put("vpc_Version", VERSION);
        TreeMap<String, Object> queryParamSorted = sortMap(param);
        String stringToHash = generateStringToHash(queryParamSorted);
        String merchantSecureHash = generateSecureHash(stringToHash, hashCode);
        String urlString =
                serverUrl
                        + String.format(
                                ENDPOINT_VOID,
                                merchantId,
                                orgMerchTxnRef,
                                merchTxnRef,
                                stringToHash,
                                merchantSecureHash);
        logger.debug(requestId + " - void url string: " + urlString);
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPut httpPut = new HttpPut(urlString);
        HttpResponse response = httpClient.execute(httpPut);
        String paymentResponse = EntityUtils.toString(response.getEntity(), "UTF-8");
        logger.debug(
                requestId
                        + " - voidTransaction statusCode: "
                        + response.getStatusLine().getStatusCode()
                        + " - voidTransaction response: "
                        + paymentResponse);
        Map<String, String> mapResult =
                Arrays.stream(paymentResponse.split("&"))
                        .map(kv -> kv.split("="))
                        .collect(Collectors.toMap(kv -> kv[0], kv -> kv[1]));
        if ("0".equalsIgnoreCase(mapResult.get("vpc_TxnResponseCode"))) {
            objResponse.returnCode = 200;
        } else {
            objResponse.returnCode = 525;
        }
        return objResponse.toString();
    }

    public void refundTransaction(String orgMerchTxnRef, String amount, String userId)
            throws Exception {
        Map<String, Object> param = new HashMap<>();
        String merchTxnRef = GatewayUtil.getOrderId();
        param.put("vpc_Command", COMMAND_REFUND);
        param.put("vpc_Amount", amount);
        param.put("vpc_AccessCode", accessCode);
        param.put("vpc_Merchant", merchantId);
        param.put("vpc_MerchTxnRef", merchTxnRef);
        param.put("vpc_OrgMerchTxnRef", orgMerchTxnRef);
        param.put("vpc_Operator", userId);
        param.put("vpc_Version", VERSION);
        TreeMap<String, Object> queryParamSorted = sortMap(param);
        String stringToHash = generateStringToHash(queryParamSorted);
        String merchantSecureHash = generateSecureHash(stringToHash, hashCode);
        param.put("vpc_SecureHash", merchantSecureHash);
        ArrayList<NameValuePair> postParameters = new ArrayList<>();
        for (Map.Entry<String, Object> entry : param.entrySet()) {
            postParameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue().toString()));
        }
        String urlString = serverUrl + ENDPOINT_REFUND;
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(urlString);
        httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
        httpPost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
        HttpResponse response = httpClient.execute(httpPost);
        String paymentResponse = EntityUtils.toString(response.getEntity(), "UTF-8");
        logger.debug(
                requestId
                        + " - refundTransaction statusCode: "
                        + response.getStatusLine().getStatusCode()
                        + " - refundTransaction response: "
                        + paymentResponse);
    }

    private String buildSendRequest(Map<String, Object> param) throws Exception {
        TreeMap<String, Object> queryParamSorted = sortMap(param);
        String stringToHash = generateStringToHash(queryParamSorted);
        String merchantSecureHash = generateSecureHash(stringToHash, hashCode);
        param.put("vpc_SecureHash", merchantSecureHash);
        String requestParam = "";
        for (Map.Entry<String, Object> items : param.entrySet()) {
            String key = items.getKey();
            String value = items.getValue().toString();
            requestParam += key + "=" + URLEncoder.encode(value, "UTF-8") + "&";
        }
        return requestParam;
    }

    public boolean validateSecureHash(Map<String, Object> param, String secureHash)
            throws Exception {
        TreeMap<String, Object> queryParamSorted = sortMap(param);
        String stringToHash = generateStringToHash(queryParamSorted);
        return secureHash.equalsIgnoreCase(generateSecureHash(stringToHash, hashCode));
    }

    public String execute(String targetURL, String urlParameters)
            throws IOException, InterruptedException {
        String urlString = targetURL + "?" + urlParameters;
        HttpClient httpClient = HttpClientBuilder.create().disableRedirectHandling().build();
        HttpGet httpGet = new HttpGet(urlString);
        HttpResponse response = httpClient.execute(httpGet);
        int statusLine = response.getStatusLine().getStatusCode();
        if (statusLine == 302) {
            Header[] headers = response.getAllHeaders();
            for (Header header : headers) {
                if (header.getName().equalsIgnoreCase("Location")) {
                    return header.getValue();
                }
            }
        }
        return null;
    }

    private TreeMap<String, Object> sortMap(Map<String, Object> queryParamMap) {
        return new TreeMap<>(queryParamMap);
    }

    private String generateStringToHash(Map<String, Object> queryParamSorted) {
        String stringToHash = "";
        for (Map.Entry<String, Object> items : queryParamSorted.entrySet()) {
            String key = items.getKey();
            String value = items.getValue().toString();
            String pref4 = key.substring(0, 4);
            String pref5 = key.substring(0, 5);
            if ("vpc_".equals(pref4) || "user_".equals(pref5)) {
                if (!"vpc_SecureHash".equals(key) && !"vpc_SecureHashType".equals(key)) {
                    if (!value.isEmpty()) {
                        if (!stringToHash.isEmpty()) {
                            stringToHash += "&";
                        }
                        stringToHash += key + "=" + value;
                    }
                }
            }
        }
        logger.debug(requestId + " - stringToHash: " + stringToHash);
        return stringToHash;
    }

    private String generateSecureHash(String stringToHash, String merchantHashCode)
            throws Exception {
        byte[] merchantHashHex = parseHexBinary(merchantHashCode);
        SecretKeySpec signingKey = new SecretKeySpec(merchantHashHex, ALGORITHM_HMAC_SHA256);
        Mac mac = Mac.getInstance(ALGORITHM_HMAC_SHA256);
        mac.init(signingKey);
        return printHexBinary(mac.doFinal(stringToHash.getBytes(UTF_8)));
    }

    public byte[] parseHexBinary(String s) {
        final int len = s.length();
        if (len % 2 != 0)
            throw new IllegalArgumentException("hexBinary needs to be even-length: " + s);
        byte[] out = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            int h = hexToBin(s.charAt(i));
            int l = hexToBin(s.charAt(i + 1));
            if (h == -1 || l == -1)
                throw new IllegalArgumentException(
                        "contains illegal character for hexBinary: " + s);
            out[i / 2] = (byte) (h * 16 + l);
        }
        return out;
    }

    public String printHexBinary(byte[] data) {
        StringBuilder r = new StringBuilder(data.length * 2);
        for (byte b : data) {
            r.append(hexCode[(b >> 4) & 0xF]);
            r.append(hexCode[(b & 0xF)]);
        }
        return r.toString();
    }

    private JSONObject convertToJson(String input) {
        String[] pairs = input.split("&");
        JSONObject jsonObject = new JSONObject();
        for (String pair : pairs) {
            String[] keyValue = pair.split("=");
            if (keyValue.length == 2) {
                String key = keyValue[0];
                String value = keyValue[1];
                jsonObject.put(key, value);
            }
        }
        return jsonObject;
    }

    private int hexToBin(char ch) {
        if ('0' <= ch && ch <= '9') return ch - '0';
        if ('A' <= ch && ch <= 'F') return ch - 'A' + 10;
        if ('a' <= ch && ch <= 'f') return ch - 'a' + 10;
        return -1;
    }
}
