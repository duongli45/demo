package com.qupworld.paymentgateway.controllers.threading;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.qupworld.config.ServerConfig;
import com.pg.util.KeysUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by qup on 23/03/2020.
 */
@SuppressWarnings("unchecked")
public class SendBillingReportThread extends Thread {
    public String id;
    public String reportBody;
    public String requestId;
    int max = 3; // retry
    Gson gson = new Gson();
    final static Logger logger = LogManager.getLogger(SendBillingReportThread.class);
    @Override
    public void run() {
        try {
            JSONArray arrData = new JSONArray();
            JSONObject objTicket = new JSONObject();
            objTicket.put("type", "FleetSubscription");
            objTicket.put("id", id);
            objTicket.put("body", "data");
            arrData.add(objTicket);

            String body = arrData.toJSONString();
            body = body.replace("\"data\"", reportBody);

            logger.debug(requestId + " - SendBilling body: " + body);
            for (int i = 0; i < max; i++) {
                try {
                    ThreadUtil threadUtil = new ThreadUtil(requestId);
                    boolean sendSuccess = threadUtil.sendToReport(body, "SendBilling", ServerConfig.server_report);
                    if (sendSuccess) {
                        // sent success !!! break the loop
                        break;
                    } else {
                        if (i == 0) {
                            // failed on the first time, try to re-authenticate token then send again
                            String authBody = "{ \"username\" : \"" + ServerConfig.auth_username + "\" , \"password\" : \"" + ServerConfig.auth_password + "\" }";
                            HttpResponse<JsonNode> jsonAuth = Unirest.post(ServerConfig.server_auth)
                                    .header("Content-Type", "application/json; charset=utf-8")
                                    .header("X-Trace-Request-Id", requestId)
                                    .body(authBody)
                                    .asJson();
                            JSONParser parser = new JSONParser();
                            JSONObject objectData = (JSONObject) parser.parse(jsonAuth.getBody().toString());
                            KeysUtil.TOKEN = objectData.get("token").toString();
                            // submit with new token
                            sendSuccess = threadUtil.sendToReport(body, "SendBilling", ServerConfig.server_report);
                            if (sendSuccess) {
                                // sent success with new token !!! break the loop
                                break;
                            } else {
                                // something went wrong, sleep 30s before continue
                                Thread.sleep(30000);
                            }
                        } else {
                            // something went wrong, sleep 30s before continue
                            Thread.sleep(30000);
                        }
                    }
                } catch (Exception ex) {
                    StringWriter errors = new StringWriter();
                    ex.printStackTrace(new PrintWriter(errors));
                    logger.debug(requestId + " - SendBilling response: " + errors.toString());
                    try {
                        // sent error !!! sleep 30s before continue
                        Thread.sleep(30000);
                    } catch (InterruptedException ie) {
                        ie.printStackTrace(new PrintWriter(errors));
                        logger.debug(requestId + " - SendBilling response: " + errors.toString());
                    }
                }
            }
        } catch(Exception ex) {
            //ex.printStackTrace();
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            logger.debug(requestId + " - SendBilling store: "+ errors.toString());
        }
    }

}
