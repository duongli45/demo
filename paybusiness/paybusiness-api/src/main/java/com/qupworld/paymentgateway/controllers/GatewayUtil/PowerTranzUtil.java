package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.entities.CreditEnt;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import okhttp3.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
public class PowerTranzUtil {

    private final static Logger logger = LogManager.getLogger(PowerTranzUtil.class);
    private String requestId;
    private String powerTranzId = "";
    private String powerTranzPassword = "";
    private String domain = "";

    public PowerTranzUtil(String _requestId, String _powerTranzId, String _powerTranzPassword, String _domain) {
        this.requestId = _requestId;
        this.powerTranzId = _powerTranzId;
        this.powerTranzPassword = _powerTranzPassword;
        this.domain = _domain;
    }

    public String initiateCreditForm(CreditEnt creditEnt, String currencyISO, String orderNumber, String firstName, String lastName, String phoneNumber){
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        String checkoutURL = "";
        try {
            String returnUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.FAC+"?action=register&gwOrderid="+orderNumber+"&fleetId="+creditEnt.fleetId;
            String cacheAPI = ServerConfig.hook_server + "/notification/CACHE?urlAction=cache&gwOrderid="+orderNumber;

            checkoutURL = ServerConfig.webform_powertranz + "?action=register"
                    + "&fleetId=" + creditEnt.fleetId
                    + "&orderIdentifier=" + orderNumber
                    + "&firstName=" + URLEncoder.encode(firstName, "UTF-8")
                    + "&lastName=" + URLEncoder.encode(lastName, "UTF-8")
                    + "&phoneNumber=" + phoneNumber
                    + "&currencyISO=" + getCurrencyNumber(currencyISO)
                    + "&merchantResponseUrl=" + URLEncoder.encode(returnUrl, "UTF-8")
                    + "&cacheAPI=" + URLEncoder.encode(cacheAPI, "UTF-8")
                    + "&apiServer=" + URLEncoder.encode(ServerConfig.hook_server, "UTF-8")
            ;
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }

        objResponse.returnCode = 200;
        mapResponse.put("3ds_url", checkoutURL);
        mapResponse.put("type", "link");
        mapResponse.put("transactionId", orderNumber);
        objResponse.response = mapResponse;
        return objResponse.toString();
    }

    public String payByToken(String fleetId, String bookId, String token, double amount, String currencyISO, Map<String,String> customerData) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        String checkoutURL = "";
        String orderId = "SALE-" + bookId;
        try {
            String returnUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.FAC+"?action=sale&gwOrderid="+orderId+"&fleetId="+fleetId;

            String firstName = customerData.get("firstName").isEmpty() ? "" : URLEncoder.encode(customerData.get("firstName"), "UTF-8");
            String lastName = customerData.get("lastName").isEmpty() ? "" : URLEncoder.encode(customerData.get("lastName"), "UTF-8");
            String line1 = customerData.get("line1").isEmpty() ? "" : URLEncoder.encode(customerData.get("line1"), "UTF-8");
            String city = customerData.get("city").isEmpty() ? "" : URLEncoder.encode(customerData.get("city"), "UTF-8");
            String state = customerData.get("state").isEmpty() ? "" : URLEncoder.encode(customerData.get("state"), "UTF-8");
            String postalCode = customerData.get("postalCode").isEmpty() ? "" : URLEncoder.encode(customerData.get("postalCode"), "UTF-8");
            String countryCode = customerData.get("countryCode");
            String emailAddress = customerData.get("emailAddress");
            String phoneNumber = customerData.get("phoneNumber");

            checkoutURL = ServerConfig.webform_powertranz + "?action=sale"
                    + "&fleetId=" + fleetId
                    + "&token=" + token
                    + "&amount=" + amount
                    + "&orderIdentifier=" + orderId
                    + "&orderIdentifier=" + orderId
                    + "&firstName=" + firstName
                    + "&lastName=" + lastName
                    + "&line1=" + line1
                    + "&city=" + city
                    + "&state=" + state
                    + "&postalCode=" + postalCode
                    + "&countryCode=" + countryCode
                    + "&emailAddress=" + emailAddress
                    + "&phoneNumber=" + phoneNumber
                    + "&currencyISO=" + getCurrencyNumber(currencyISO)
                    + "&merchantResponseUrl=" + URLEncoder.encode(returnUrl, "UTF-8")
                    + "&apiServer=" + URLEncoder.encode(ServerConfig.hook_server, "UTF-8")
            ;
            objResponse.returnCode = 200;
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
        }

        mapResponse.put("3ds_url", checkoutURL);
        mapResponse.put("type", "link");
        mapResponse.put("transactionId", orderId);
        objResponse.response = mapResponse;
        return objResponse.toString();
    }

    public String preAuthPayment(String fleetId, String bookId, String token, double amount, String currencyISO, Map<String,String> customerData) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,Object> mapResponse = new HashMap<>();
        String checkoutURL = "";
        String orderId = "AUTH-" + bookId;
        try {
            String returnUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.FAC+"?action=auth&gwOrderid="+orderId+"&fleetId="+fleetId;

            String firstName = customerData.get("firstName").isEmpty() ? "" : URLEncoder.encode(customerData.get("firstName"), "UTF-8");
            String lastName = customerData.get("lastName").isEmpty() ? "" : URLEncoder.encode(customerData.get("lastName"), "UTF-8");
            String line1 = customerData.get("line1").isEmpty() ? "" : URLEncoder.encode(customerData.get("line1"), "UTF-8");
            String city = customerData.get("city").isEmpty() ? "" : URLEncoder.encode(customerData.get("city"), "UTF-8");
            String state = customerData.get("state").isEmpty() ? "" : URLEncoder.encode(customerData.get("state"), "UTF-8");
            String postalCode = customerData.get("postalCode").isEmpty() ? "" : URLEncoder.encode(customerData.get("postalCode"), "UTF-8");
            String countryCode = customerData.get("countryCode");
            String emailAddress = customerData.get("emailAddress");
            String phoneNumber = customerData.get("phoneNumber");

            checkoutURL = ServerConfig.webform_powertranz + "?action=auth"
                    + "&fleetId=" + fleetId
                    + "&token=" + token
                    + "&amount=" + amount
                    + "&orderIdentifier=" + orderId
                    + "&firstName=" + firstName
                    + "&lastName=" + lastName
                    + "&line1=" + line1
                    + "&city=" + city
                    + "&state=" + state
                    + "&postalCode=" + postalCode
                    + "&countryCode=" + countryCode
                    + "&emailAddress=" + emailAddress
                    + "&phoneNumber=" + phoneNumber
                    + "&currencyISO=" + getCurrencyNumber(currencyISO)
                    + "&merchantResponseUrl=" + URLEncoder.encode(returnUrl, "UTF-8")
                    + "&apiServer=" + URLEncoder.encode(ServerConfig.hook_server, "UTF-8")
            ;
            objResponse.returnCode = 528;
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
        }

        mapResponse.put("3ds_url", checkoutURL);
        mapResponse.put("deepLinkUrl", checkoutURL);
        mapResponse.put("type", "link");
        mapResponse.put("transactionId", orderId);
        mapResponse.put("openInApp", true);
        objResponse.response = mapResponse;
        return objResponse.toString();
    }

    public String preAuthCapture(String bookId, String authId, double amount, Map<String,String> mapData) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        try {
            JSONObject objData = new JSONObject();
            objData.put("TransactionIdentifier", authId);
            objData.put("TotalAmount", amount);
            objData.put("ExternalIdentifier", "CAPTURE-"+bookId);

            String URL = domain + "/api/capture";
            logger.debug(requestId + " - URL = " + URL);
            logger.debug(requestId + " - data = " + objData.toString());
            MediaType JSON = MediaType.get("application/json; charset=utf-8");
            RequestBody body = RequestBody.create(objData.toString(), JSON);
            Request request = new Request.Builder()
                    .addHeader("Content-Type", "application/json; charset=utf-8")
                    .addHeader("PowerTranz-PowerTranzId", powerTranzId)
                    .addHeader("PowerTranz-PowerTranzPassword", powerTranzPassword)
                    .url(URL)
                    .post(body)
                    .build();

            OkHttpClient client = new OkHttpClient.Builder().build();
            Response response = client.newCall(request).execute();
            String responseString = response.body().string();
            logger.debug(requestId + " - response = " + responseString);
            /**
             * {
                 "OriginalTrxnIdentifier": "8f03184a-e5dd-4d3f-9561-f1c55cfd6283",
                 "TransactionType": 3,
                 "Approved": true,
                 "TransactionIdentifier": "ee3ac5cf-7300-4fbd-b8c2-f53591b72d83",
                 "TotalAmount": 80,
                 "CurrencyCode": "840",
                 "RRN": "313110526989",
                 "IsoResponseCode": "00",
                 "ResponseMessage": "Transaction is approved",
                 "ExternalIdentifier": "CAPTURE-632541",
                 "OrderIdentifier": "AUTH-632541"
             }
             */
            JSONObject jsonResponse = new JSONObject(responseString);
            String responseCode = jsonResponse.has("IsoResponseCode") ? jsonResponse.getString("IsoResponseCode") : "";
            if (responseCode.equalsIgnoreCase("00")) {
                String transId = jsonResponse.has("TransactionIdentifier") ? jsonResponse.getString("TransactionIdentifier") : "";
                String cardType = mapData.get("cardType");
                String cardMask = mapData.get("cardMask");

                mapResponse.put("message", "Success!" );
                mapResponse.put("transId", transId);
                mapResponse.put("cardType", cardType);
                mapResponse.put("authCode", "");
                mapResponse.put("cardMask", cardMask);
                objResponse.response = mapResponse;
                objResponse.returnCode = 200;
                return objResponse.toString();
            } else {
                String message = jsonResponse.has("ResponseMessage") ? jsonResponse.getString("ResponseMessage") : "";
                mapResponse.put("message", message);
                objResponse.response = mapResponse;
                objResponse.returnCode = 525;
                return objResponse.toString();
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - preAuthCapture exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            return objResponse.toString();
        }
    }

    public String voidStripeTransaction(String authId) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        try {
            JSONObject objData = new JSONObject();
            objData.put("TransactionIdentifier", authId);
            objData.put("TerminalCode", "");
            objData.put("TerminalSerialNumber", "");
            objData.put("AutoReversal", false);

            String URL = domain + "/api/void";
            logger.debug(requestId + " - URL = " + URL);
            logger.debug(requestId + " - data = " + objData.toString());
            MediaType JSON = MediaType.get("application/json; charset=utf-8");
            RequestBody body = RequestBody.create(objData.toString(), JSON);
            Request request = new Request.Builder()
                    .addHeader("Content-Type", "application/json; charset=utf-8")
                    .addHeader("PowerTranz-PowerTranzId", powerTranzId)
                    .addHeader("PowerTranz-PowerTranzPassword", powerTranzPassword)
                    .url(URL)
                    .post(body)
                    .build();

            OkHttpClient client = new OkHttpClient.Builder().build();
            Response response = client.newCall(request).execute();
            String responseString = response.body().string();
            logger.debug(requestId + " - response = " + responseString);
            /**
             * {
                 "OriginalTrxnIdentifier": "8f03184a-e5dd-4d3f-9561-f1c55cfd6283",
                 "TransactionType": 4,
                 "Approved": true,
                 "TransactionIdentifier": "5b770d2c-9a76-4054-8604-519bacf02859",
                 "TotalAmount": 50,
                 "CurrencyCode": "840",
                 "RRN": "313404558142",
                 "IsoResponseCode": "00",
                 "ResponseMessage": "Transaction is approved",
                 "OrderIdentifier": "AUTH-632541"
             }
             */
            JSONObject jsonResponse = new JSONObject(responseString);
            String responseCode = jsonResponse.has("IsoResponseCode") ? jsonResponse.getString("IsoResponseCode") : "";
            if (responseCode.equalsIgnoreCase("00")) {
                String transId = jsonResponse.has("TransactionIdentifier") ? jsonResponse.getString("TransactionIdentifier") : "";
                mapResponse.put("transId", transId);
                objResponse.returnCode = 200;
            } else {
                String message = jsonResponse.has("ResponseMessage") ? jsonResponse.getString("ResponseMessage") : "";
                mapResponse.put("message", message);
                objResponse.response = mapResponse;
                objResponse.returnCode = 525;
            }
            return objResponse.toString();
        } catch (Exception ex) {
            logger.debug(requestId + " - voidStripeTransaction exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            return objResponse.toString();
        }
    }

    public static void main(String[] args) throws Exception {
        String requestId = UUID.randomUUID().toString();
        String powerTranzId = "88805186";
        String powerTranzPassword = "rxJirLoNUSNS8cHxDaSi1k2CU26sGcpm5QAAWztsuJmizPDnNfNE67";
        String domain = "https://staging.ptranz.com";

        PowerTranzUtil powerTranzUtil = new PowerTranzUtil(requestId, powerTranzId, powerTranzPassword, domain);

        String id = CommonUtils.getOrderId();
        CreditEnt creditEnt = new CreditEnt();
        creditEnt.phone = "+84905111111";
        creditEnt.email = "hnthuan01@gmail.com";
        creditEnt.cardNumber = "4012000000020071";
        creditEnt.cvv = "123";
        creditEnt.expiredDate = "12/2027";
        creditEnt.cardHolder = "Thuan Ho";
        creditEnt.street = "271 Nguyen Van Linh";
        creditEnt.city = "Da Nang";
        creditEnt.postalCode = "600000";
        creditEnt.country = "Curaçao";

        String result = powerTranzUtil.initiateCreditForm(creditEnt, "USD", id, "", "", "");
        logger.debug(requestId + " - result: " + result);
    }

    private String getCurrencyNumber(String currencyISO) {
        String number = "780"; // TTD
        if (currencyISO.equals("USD"))
            number = "840";
        else if (currencyISO.equals("JMD"))
            number = "388";
        return number;
    }

}
