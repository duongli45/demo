
package com.qupworld.paymentgateway.controllers.GatewayUtil.PayUSDK;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Transaction {

    public Order order;
    public Payer payer;
    public String creditCardTokenId;
    public CreditCard creditCard;
    public ExtraParameters extraParameters;
    public String type;
    public String paymentMethod;
    public String paymentCountry;
    public String deviceSessionId;
    public String ipAddress;
    public String cookie;
    public String userAgent;
    public String signature;
    public String parentTransactionId;
    public String reason;

}
