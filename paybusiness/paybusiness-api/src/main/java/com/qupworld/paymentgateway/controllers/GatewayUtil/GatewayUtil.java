package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.google.gson.Gson;
import com.neovisionaries.i18n.CountryCode;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.MongoDaoImpl;
import com.qupworld.paymentgateway.models.RedisDao;
import com.qupworld.paymentgateway.models.RedisImpl;
import com.qupworld.paymentgateway.models.mongo.collections.account.Account;
import com.qupworld.paymentgateway.models.mongo.collections.corporate.Corporate;
import com.qupworld.service.MailService;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.TimezoneUtil;
import com.pg.util.VersionUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by qup on 8/31/17.
 */
@SuppressWarnings("unchecked")
public class GatewayUtil {

    private final static Logger logger = LogManager.getLogger(GatewayUtil.class);
    private static VersionUtil versionUtil = new VersionUtil();
    private static Gson gson = new Gson();

    public static List<String> flutterWaveLocalCurrencies = Arrays.asList("NGN", "GHS", "KES", "ZAR");
    public static boolean isEmptyString(String s) {
        return (s == null || s.trim().isEmpty());
    }

    public static String stripAccents(String s) {
        if (s != null && !s.isEmpty()) {
            s = Normalizer.normalize(s, Normalizer.Form.NFD);
            s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
            s = s.replace("Đ", "D").replace("đ", "d");
        }
        return s;
    }

    public static String getError(Exception ex) {
        StringWriter errors = new StringWriter();
        ex.printStackTrace(new PrintWriter(errors));
        return gson.toJson(errors.toString());
    }

    public static String getCountry3Code(String countryName){
        Map<String, String> countries = new HashMap<>();
        for (String iso : Locale.getISOCountries()) {
            Locale l = new Locale("", iso);
            String iso3 = l.getISO3Country();
            countries.put(l.getDisplayCountry(), iso3.toUpperCase());
        }

        if( countryName != null && !countryName.isEmpty() )
            return countries.get(countryName) != null ? countries.get(countryName) : "";
        else
            return "";
    }

    public static String getCountry2Code(String countryName){
        Map<String, String> countries = new HashMap<>();
        for (String iso : Locale.getISOCountries()) {
            Locale l = new Locale("", iso);
            countries.put(l.getDisplayCountry(), iso);
        }

        if( countryName != null && !countryName.isEmpty() )
            return countries.get(countryName) != null ? countries.get(countryName) : "";
        else
            return "";
    }

    public static int getCountryNumber(String countryName){
        Map<String, Integer> countries = new HashMap<>();

        for (String iso : Locale.getISOCountries()) {
            Locale l = new Locale("", iso);
            CountryCode cc = CountryCode.getByCode(iso);
            countries.put(l.getCountry(), cc.getNumeric());
        }

        if( countryName != null && !countryName.isEmpty() )
            return countries.get(countryName) != null ? countries.get(countryName) : 0;
        else
            return 0;
    }

    public static String getOrderId() {
        Calendar cal = Calendar.getInstance();
        Date currentTime = TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), "GMT");
        SimpleDateFormat formatOrder = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        return formatOrder.format(currentTime);
    }

    public static Map<String,String> getCurrency(String phone) {
        // determine country name and currency based on the phone code
        String countryName = "";
        String currencyISO = "";
        if (!phone.isEmpty()) {
            phone = phone.trim().replace("+", "");
            String phoneCode = phone.substring(0,3);
            switch (phoneCode) {
                case "234":
                    countryName = "Nigeria";
                    currencyISO = "NGN";
                    break;
                case "233":
                    countryName = "Ghana";
                    currencyISO = "GHS";
                    break;
                case "254":
                    countryName = "Kenya";
                    currencyISO = "KES";
                    break;
                case "221":
                    countryName = "Senegal";
                    currencyISO = "XOF";
                    break;
            }
        }
        if (countryName.isEmpty()) {
            String phoneCode = phone.substring(0,2);
            if (phoneCode.equals("27")) {
                countryName = "South Africa";
                currencyISO = "ZAR";
            }
        }
        Map<String,String> mapData = new HashMap<>();
        mapData.put("countryName", countryName);
        mapData.put("currencyISO", currencyISO);
        return mapData;
    }

    public static String getCountryNameFromCurrency(String currencyISO) {
        String countryName = "";
        switch (currencyISO) {
            case "NGN" :
                countryName = "Nigeria";
                break;
            case "GHS" :
                countryName = "Ghana";
                break;
            case "KES" :
                countryName = "Kenya";
                break;
            case "ZAR" :
                countryName = "South Africa";
                break;
        }
        return countryName;
    }

    public static ObjResponse checkSupportedCurrency(String currencyISO, String phoneNumber) {
        ObjResponse objResponse = new ObjResponse();
        objResponse.returnCode = 200;
        if (flutterWaveLocalCurrencies.contains(currencyISO)) {
            Map<String,String> currencyFromPhone = getCurrency(phoneNumber);
            String currencyByPhone = currencyFromPhone.get("currencyISO");
            if (!currencyByPhone.equals(currencyISO)) {
                objResponse.returnCode = 453;
                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("message", "Do not support");
                objResponse.response = mapResponse;
                objResponse.message = "Do not support";

            }
        }
        return objResponse;
    }

    public static Map<String,String> getExtraData(String type, String id) {
        String email = "";
        String firstName = "";
        String lastName = "";
        String ip = "";
        String phone = "";
        if (!id.isEmpty()) {
            if (type.equals("user") || type.equals("customer") || type.equals("driver")) {
                MongoDao mongoDao = new MongoDaoImpl();
                Account account = mongoDao.getAccount(id);
                if (account != null) {
                    email = account.email != null ? account.email.trim() : "";
                    firstName = account.firstName != null ? account.firstName.trim() : "";
                    lastName = account.lastName != null ? account.lastName.trim() : "";
                    ip = account.address != null ? account.address : "127.0.0.1";
                    phone = account.phone != null ? account.phone.trim() : "";
                }
            }
            if (type.equals("corporate")) {
                MongoDao mongoDao = new MongoDaoImpl();
                Corporate corporate = mongoDao.getCorporate(id);
                if (corporate != null && corporate.adminAccount != null) {
                    email = corporate.adminAccount.email != null ? corporate.adminAccount.email.trim() : "";
                    firstName = corporate.adminAccount.firstName != null ? corporate.adminAccount.firstName.trim() : "";
                    lastName = corporate.adminAccount.lastName != null ? corporate.adminAccount.lastName.trim() : "";
                    ip = "127.0.0.1";
                    phone = corporate.adminAccount.phone != null ? corporate.adminAccount.phone.trim() : "";
                }
            }
        }
        Map<String,String> mapResponse = new HashMap<>();
        mapResponse.put("email", email.trim());
        mapResponse.put("firstName", firstName.trim());
        mapResponse.put("lastName", lastName.trim());
        mapResponse.put("ip", ip);
        mapResponse.put("phone", phone);
        return mapResponse;
    }

    public static void sendMail(String requestId, String gateway, String request, String response, String key) {
        try {
            InetAddress ip = InetAddress.getLocalHost();
            MailService mailService = MailService.getInstance();
            String subject = "[QUp_Mail_Report] [" + ServerConfig.mail_server.toUpperCase() + "] [Payment] ["+ gateway +"] " + " payment log for " + key;
           /* String body = "- RequestId: " + requestId + "<br>&nbsp;<br>";
            body = body + "- Request: <br>" + request + "<br>&nbsp;<br>";
            body = body + "- Response: <br>" + response + "<br>&nbsp;<br>";*/
            String body = "Server name: "+ ServerConfig.mail_server.toUpperCase() + "<br>";
            body += "Host name: " + ip.getHostName() + "<br>";
            body += "Version: " + versionUtil.getVersion() + "<br>";
            body += "Time: " + KeysUtil.SDF_DMYHMS.format(new Date()) + "<br>";
            body += "Trace ID: " + requestId + "<br>";
            body += "Message: ["+ gateway +"] " + " payment log for " + key + "<br>";
            body += "Data: <br>";
            body += "\t- Request: <br>" + request + "<br>&nbsp;<br>";
            body += "\t- Response: <br>" + response + "<br>&nbsp;<br>";
            body += "Error:";
            mailService.sendMailReport(requestId, subject, body);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void sendMail(String requestId, String gateway, Exception ex, String key) {
        try {
            InetAddress ip = InetAddress.getLocalHost();
            MailService mailService = MailService.getInstance();
            String subject = "[QUp_Mail_Report] [" + ServerConfig.mail_server.toUpperCase() + "] [Payment] ["+ gateway +"] " + " payment log for " + key;
          /*  String bodyEmail = "- RequestId: " + requestId + "<br>&nbsp;<br>";
            bodyEmail = bodyEmail + "- Exception: <br>" + ex.getMessage();
            String bodyDetails = "- RequestId: " + requestId + "<br>&nbsp;<br>";
            bodyDetails = bodyDetails + "- Exception: <br>" + getError(ex);*/
            String body = "Server name: "+ ServerConfig.mail_server.toUpperCase() + "<br>";
            body += "Host name: " + ip.getHostName() + "<br>";
            body += "Version: " + versionUtil.getVersion() + "<br>";
            body += "Time: " + KeysUtil.SDF_DMYHMS.format(new Date()) + "<br>";
            body += "Trace ID: " + requestId + "<br>";
            body += "Message: ["+ gateway +"] " + " payment log for " + key + "<br>";
            body += "Data: <br>";
            body += "Error: "  + ex.getMessage();

            String bodyDetails = "Server name: "+ ServerConfig.mail_server.toUpperCase() + "<br>";
            bodyDetails += "Host name: " + ip.getHostName() + "<br>";
            bodyDetails += "Version: " + versionUtil.getVersion() + "<br>";
            bodyDetails += "Time: " + KeysUtil.SDF_DMYHMS.format(new Date()) + "<br>";
            bodyDetails += "Trace ID: " + requestId + "<br>";
            bodyDetails += "Message: ["+ gateway +"] " + " payment log for " + key + "<br>";
            bodyDetails += "Data: <br>";
            bodyDetails += "Error: "  + getError(ex);
            mailService.sendMailReport(requestId, subject, body);
            mailService.sendMailReportToSpecificEmail(requestId, subject, bodyDetails);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendMailHydraBooking(String requestId, String type, String bookId, String log) {
        try {
            InetAddress ip = InetAddress.getLocalHost();
            MailService mailService = MailService.getInstance();
            String subject = "[QUp_Mail_Report] [" + ServerConfig.mail_server.toUpperCase() + "] [Payment] [HydraBooking] " + type + " log for bookId " + bookId;
            /*String body = "- RequestId: " + requestId + "<br>&nbsp;<br>";
            body = body + "- Log: <br>" + log;*/
            String body = "Server name: "+ ServerConfig.mail_server.toUpperCase() + "<br>";
            body += "Host name: " + ip.getHostName() + "<br>";
            body += "Version: " + versionUtil.getVersion() + "<br>";
            body += "Time: " + KeysUtil.SDF_DMYHMS.format(new Date()) + "<br>";
            body += "Trace ID: " + requestId + "<br>";
            body += "Message: [HydraBooking] " + type + " log for bookId " + bookId + "<br>";
            body += "Data: " + log +"<br>";
            body += "Error: ";
            mailService.sendMailReport(requestId, subject, body);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendMailWalletTransfer(String requestId, String type, String destination, String log) {
        try {
            InetAddress ip = InetAddress.getLocalHost();
            MailService mailService = MailService.getInstance();
            String subject = "[QUp_Mail_Report] [" + ServerConfig.mail_server.toUpperCase() + "] [Payment] [WalletTransfer] "  + type + " to " + destination;
           /* String body = "- RequestId: " + requestId + "<br>&nbsp;<br>";
            body = body + "- Log: <br>" + log;*/
            String body = "Server name: "+ ServerConfig.mail_server.toUpperCase() + "<br>";
            body += "Host name: " + ip.getHostName() + "<br>";
            body += "Version: " + versionUtil.getVersion() + "<br>";
            body += "Time: " + KeysUtil.SDF_DMYHMS.format(new Date()) + "<br>";
            body += "Trace ID: " + requestId + "<br>";
            body += "Message: [WalletTransfer] "  + type + " to " + destination + "<br>";
            body += "Data: " + log +"<br>";
            body += "Error: ";
            mailService.sendMailReport(requestId, subject, body);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendMailWalletTransaction(String requestId, String log) {
        try {
            InetAddress ip = InetAddress.getLocalHost();
            MailService mailService = MailService.getInstance();
            String subject = "[QUp_Mail_Report] [" + ServerConfig.mail_server.toUpperCase() + "] [Payment] [WalletTransaction] insert MySQL error";
           /* String body = "- RequestId: " + requestId + "<br>&nbsp;<br>";
            body = body + "- Log: <br>" + log;*/
            String body = "Server name: "+ ServerConfig.mail_server.toUpperCase() + "<br>";
            body += "Host name: " + ip.getHostName() + "<br>";
            body += "Version: " + versionUtil.getVersion() + "<br>";
            body += "Time: " + KeysUtil.SDF_DMYHMS.format(new Date()) + "<br>";
            body += "Trace ID: " + requestId + "<br>";
            body += "Message: [WalletTransaction] insert MySQL error <br>";
            body += "Data: " + log +"<br>";
            body += "Error: ";
            mailService.sendMailReport(requestId, subject, body);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static int getIntAmount(double amount, String currencyISO) {
        int intAmount;
        List<String> fullList = Arrays.asList("BIF", "DJF", "JPY", "KRW", "PYG", "VND", "XAF", "XPF", "CLP", "GNF", "KMF", "MGA", "RWF", "VUV", "XOF");
        if (fullList.contains(currencyISO)) {
            intAmount = (int) amount;
        } else {
            DecimalFormat df = new DecimalFormat("#");
            intAmount = Integer.parseInt(df.format(amount*100));
        }
        return intAmount;
    }

    public static String returnError(Exception ex, int errorCode) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        //mapResponse.put("message", ex.getMessage());
        mapResponse.put("details", GatewayUtil.getError(ex));
        objResponse.returnCode = errorCode;
        objResponse.response = mapResponse;
        return objResponse.toString();
    }

    public static JSONObject getDataFromRedis(String requestId, String id, int numberTryAgain) {
        logger.debug(requestId + " - numberTryAgain:" + numberTryAgain);
        Gson gson = new Gson();
        RedisDao redisDao = new RedisImpl();
        JSONObject obj = gson.fromJson(redisDao.getTmp(requestId, id), JSONObject.class);
        //logger.debug(requestId + " - getDataFromRedis = " + gson.toJson(obj));
        if (obj == null && numberTryAgain > 0) {
            try {
                Thread.sleep(10*1000); // wait 10s before continue
            } catch (Exception ignore) {

            }
            return getDataFromRedis(requestId, id, numberTryAgain - 1);
        }
        return obj;
    }

    public static String getCurrencyCode(String currencyISO) {
        String code = "";
        switch (currencyISO) {
            case "HKD":
                code = "344";
                break;
            case "USD":
                code = "840";
                break;
            case "SGD":
                code = "702";
                break;
            case "CNY":
                code = "156";
                break;
            case "JPY":
                code = "392";
                break;
            case "TWD":
                code = "901";
                break;
            case "AUD":
                code = "036";
                break;
            case "EUR":
                code = "978";
                break;
            case "GBP":
                code = "826";
                break;
            case "CAD":
                code = "124";
                break;
            case "MOP":
                code = "446";
                break;
            case "PHP":
                code = "608";
                break;
            case "THB":
                code = "764";
                break;
            case "MYR":
                code = "458";
                break;
            case "IDR":
                code = "360";
                break;
            case "KRW":
                code = "410";
                break;
            case "SAR":
                code = "682";
                break;
            case "NZD":
                code = "554";
                break;
            case "AED":
                code = "784";
                break;
            case "BND":
                code = "096";
                break;
            case "VND":
                code = "704";
                break;
            case "INR":
                code = "356";
                break;

        }
        return code;
    }

    public static String getFile(String requestId, String fileName) {
        StringBuilder result = new StringBuilder("");
        //Get file from resources folder
        ClassLoader classLoader = GatewayUtil.class.getClassLoader();
        try {
            File file = new File(classLoader.getResource(fileName).getFile());
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                result.append(line).append("\n");
            }
        } catch (IOException ex) {
            logger.debug(requestId + " - getFile exception: " + getError(ex));
        }

        return result.toString();

    }

    public static void trustAllHttpsCertificates() {
        try {
            // Create a trust manager that does not validate certificate chains:
            javax.net.ssl.TrustManager[] trustAllCerts = new javax.net.ssl.TrustManager[1];
            javax.net.ssl.TrustManager tm = new com.qupworld.util.MyTrustManager();
            trustAllCerts[0] = tm;
            javax.net.ssl.SSLContext sc;
            sc = javax.net.ssl.SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, null);
            javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            logger.debug(e);
        } catch (KeyManagementException e) {
            e.printStackTrace();
            logger.debug(e);
        } catch (Exception e){
            e.printStackTrace();
            logger.debug(e);
        }
    }
}
