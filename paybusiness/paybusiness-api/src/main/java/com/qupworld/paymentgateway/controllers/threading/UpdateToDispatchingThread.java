package com.qupworld.paymentgateway.controllers.threading;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.qupworld.config.ServerConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by qup on 23/03/2020.
 */
@SuppressWarnings("unchecked")
public class UpdateToDispatchingThread extends Thread {
    private static String dispatchingApplePayCompleted = ServerConfig.dispatch_server+"/api/v2/booking/applePayCompleted";
    Logger logger = LogManager.getLogger(UpdateToDispatchingThread.class);
    public String requestId;
    public String data;
    @Override
    public void run() {
        logger.debug(requestId + " - push data result to Dispatching: " + data);
        int returnCode = 437;
        try {
            HttpResponse<JsonNode> jsonResponse = null;
            jsonResponse = Unirest.post(dispatchingApplePayCompleted)
                    //.basicAuth(ServerConfig.logs_username, ServerConfig.logs_password)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("x-trace-request-id", requestId)
                    .body(data)
                    .asJson();
            logger.debug(requestId + " - " + jsonResponse.getBody().getObject().toString());
            returnCode =  (int)jsonResponse.getBody().getObject().get("returnCode");
            logger.debug(requestId + " - push result to Dispatching result: " + returnCode);
        } catch (Exception ex) {
            //ex.printStackTrace();
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            logger.debug(requestId + " - push result to Dispatching error: " + errors.toString());
        } finally {
            if (returnCode != 200) {
                try {
                    Thread.sleep(2000);
                    logger.debug(requestId + " - push result to Dispatching 1st try ... ");
                    HttpResponse<JsonNode> jsonResponse = null;
                    jsonResponse = Unirest.post(dispatchingApplePayCompleted)
                            //.basicAuth(ServerConfig.logs_username, ServerConfig.logs_password)
                            .header("Content-Type", "application/json; charset=utf-8")
                            .header("x-trace-request-id", requestId)
                            .body(data)
                            .asJson();
                    returnCode =  (int)jsonResponse.getBody().getObject().get("returnCode");
                    logger.debug(requestId + " - push result to Dispatching 1st try result: " + returnCode);
                } catch (Exception ex) {

                } finally {
                    if (returnCode != 200) {
                        try {
                            Thread.sleep(2000);
                            logger.debug(requestId + " - push result to Dispatching 2nd try ... ");
                            HttpResponse<JsonNode> jsonResponse = null;
                            jsonResponse = Unirest.post(dispatchingApplePayCompleted)
                                    //.basicAuth(ServerConfig.logs_username, ServerConfig.logs_password)
                                    .header("Content-Type", "application/json; charset=utf-8")
                                    .header("x-trace-request-id", requestId)
                                    .body(data)
                                    .asJson();
                            returnCode =  (int)jsonResponse.getBody().getObject().get("returnCode");
                            logger.debug(requestId + " - push result to Dispatching 2nd try result: " + returnCode);
                        } catch (Exception ex) {

                        } finally {
                            if (returnCode != 200) {
                                logger.debug(requestId + " - push result to Dispatching failed - give up !!!");
                            }
                        }
                    }
                }
            }
        }

    }
}
