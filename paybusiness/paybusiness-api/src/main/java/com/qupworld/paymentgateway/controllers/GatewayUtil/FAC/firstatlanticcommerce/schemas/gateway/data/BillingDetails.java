
package com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BillingDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BillingDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BillToAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillToAddress2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillToCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillToCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillToEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillToFirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillToLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillToState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillToTelephone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillToZipPostCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillToCounty" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillToMobile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BillingDetails", propOrder = {
    "billToAddress",
    "billToAddress2",
    "billToCity",
    "billToCountry",
    "billToEmail",
    "billToFirstName",
    "billToLastName",
    "billToState",
    "billToTelephone",
    "billToZipPostCode",
    "billToCounty",
    "billToMobile"
})
public class BillingDetails {

    @XmlElement(name = "BillToAddress", nillable = true)
    protected String billToAddress;
    @XmlElement(name = "BillToAddress2", nillable = true)
    protected String billToAddress2;
    @XmlElement(name = "BillToCity", nillable = true)
    protected String billToCity;
    @XmlElement(name = "BillToCountry", nillable = true)
    protected String billToCountry;
    @XmlElement(name = "BillToEmail", nillable = true)
    protected String billToEmail;
    @XmlElement(name = "BillToFirstName", nillable = true)
    protected String billToFirstName;
    @XmlElement(name = "BillToLastName", nillable = true)
    protected String billToLastName;
    @XmlElement(name = "BillToState", nillable = true)
    protected String billToState;
    @XmlElement(name = "BillToTelephone", nillable = true)
    protected String billToTelephone;
    @XmlElement(name = "BillToZipPostCode", nillable = true)
    protected String billToZipPostCode;
    @XmlElement(name = "BillToCounty", nillable = true)
    protected String billToCounty;
    @XmlElement(name = "BillToMobile", nillable = true)
    protected String billToMobile;

    /**
     * Gets the value of the billToAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillToAddress() {
        return billToAddress;
    }

    /**
     * Sets the value of the billToAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillToAddress(String value) {
        this.billToAddress = value;
    }

    /**
     * Gets the value of the billToAddress2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillToAddress2() {
        return billToAddress2;
    }

    /**
     * Sets the value of the billToAddress2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillToAddress2(String value) {
        this.billToAddress2 = value;
    }

    /**
     * Gets the value of the billToCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillToCity() {
        return billToCity;
    }

    /**
     * Sets the value of the billToCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillToCity(String value) {
        this.billToCity = value;
    }

    /**
     * Gets the value of the billToCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillToCountry() {
        return billToCountry;
    }

    /**
     * Sets the value of the billToCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillToCountry(String value) {
        this.billToCountry = value;
    }

    /**
     * Gets the value of the billToEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillToEmail() {
        return billToEmail;
    }

    /**
     * Sets the value of the billToEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillToEmail(String value) {
        this.billToEmail = value;
    }

    /**
     * Gets the value of the billToFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillToFirstName() {
        return billToFirstName;
    }

    /**
     * Sets the value of the billToFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillToFirstName(String value) {
        this.billToFirstName = value;
    }

    /**
     * Gets the value of the billToLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillToLastName() {
        return billToLastName;
    }

    /**
     * Sets the value of the billToLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillToLastName(String value) {
        this.billToLastName = value;
    }

    /**
     * Gets the value of the billToState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillToState() {
        return billToState;
    }

    /**
     * Sets the value of the billToState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillToState(String value) {
        this.billToState = value;
    }

    /**
     * Gets the value of the billToTelephone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillToTelephone() {
        return billToTelephone;
    }

    /**
     * Sets the value of the billToTelephone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillToTelephone(String value) {
        this.billToTelephone = value;
    }

    /**
     * Gets the value of the billToZipPostCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillToZipPostCode() {
        return billToZipPostCode;
    }

    /**
     * Sets the value of the billToZipPostCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillToZipPostCode(String value) {
        this.billToZipPostCode = value;
    }

    /**
     * Gets the value of the billToCounty property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillToCounty() {
        return billToCounty;
    }

    /**
     * Sets the value of the billToCounty property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillToCounty(String value) {
        this.billToCounty = value;
    }

    /**
     * Gets the value of the billToMobile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillToMobile() {
        return billToMobile;
    }

    /**
     * Sets the value of the billToMobile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillToMobile(String value) {
        this.billToMobile = value;
    }

}
