
package com.qupworld.paymentgateway.controllers.GatewayUtil.PayUSDK;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class CreditCard {

    public String number;
    public String securityCode;
    public String expirationDate;
    public String name;

}
