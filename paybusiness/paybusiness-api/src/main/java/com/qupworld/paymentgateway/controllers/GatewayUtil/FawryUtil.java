
package com.qupworld.paymentgateway.controllers.GatewayUtil;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.Base64Encoder;
import com.qupworld.paymentgateway.controllers.PaymentUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.entities.FawryFormEnt.*;
import com.qupworld.paymentgateway.entities.FawryResponseEnt.BalanceAmt;
import com.qupworld.paymentgateway.entities.FawryResponseEnt.FawryResponseEnt;
import com.qupworld.paymentgateway.entities.FawryResponseEnt.PmtStatusRec;
import com.qupworld.paymentgateway.entities.FawryResponseEnt.Status;


public class FawryUtil {
    private final static Logger logger = LogManager.getLogger(PaymentUtil.class);
    public String hashPmtNotifyRq(FawryEnt pmtNotifyRq) {
        try {
            String msgToHash = "";
            msgToHash += pmtNotifyRq.getMsgCode().trim();
            msgToHash += pmtNotifyRq.getAsyncRqUID().trim();
            PmtRec pmtRec = pmtNotifyRq.getPmtRec().get(0);
            logger.debug(pmtRec);
            msgToHash += pmtRec.getPmtInfo().getBillingAcct().trim();
            msgToHash += pmtRec.getPmtInfo().getBillTypeCode();
            if (pmtRec.getPmtInfo().getExtraBillingAccts() != null &&!pmtRec.getPmtInfo().getExtraBillingAccts().isEmpty()){
                for (ExtraBillingAcct extraBillingAcct : pmtRec.getPmtInfo().getExtraBillingAccts()) {
                    msgToHash += extraBillingAcct.getKey().trim();
                    msgToHash += extraBillingAcct.getValue().trim();
                }
            }

            for (PmtId pmtId : pmtRec.getPmtInfo().getPmtIds()) {
                msgToHash += pmtId.getPmtId().trim();
                msgToHash += pmtId.getPmtIdType().trim();
                msgToHash += pmtId.getCreationDt();
            }
            msgToHash += pmtRec.getPmtInfo().getPmtAmt().getAmt();
            return digestMessage(msgToHash);
        } catch (Exception ex) {
        }
        return "";
    }

    public FawryResponseEnt pmtNotifyFawry(FawryEnt pmtNotifyRq,String duplicatetransactionId) {
        FawryResponseEnt fawryResponseEnt = new FawryResponseEnt();
        Status status = new Status();
        try {
        String f1 = pmtNotifyRq.getSignature();
        String f2 = hashPmtNotifyRq(pmtNotifyRq);
        if (f1.equals(f2) ) {
            if (duplicatetransactionId.length()>0){
                status.setStatusCode("21021");
                status.setDescription("Duplicate payment transaction.");
            }else {
                status.setStatusCode("200");
                status.setDescription("Success");
            }


        } else {
            status.setStatusCode("31");
            status.setDescription("Message Authentication Error");
        }
        PmtStatusRec pmtStatusRec = new PmtStatusRec();
        pmtStatusRec.setStatus(status);
        PmtRec pmtRec = pmtNotifyRq.getPmtRec().get(0);
        pmtStatusRec.setPmtIds(pmtRec.getPmtInfo().getPmtIds());

        BalanceAmt balanceAmt = new BalanceAmt();
        balanceAmt.setAmt(pmtRec.getPmtInfo().getPmtAmt().getAmt());
        balanceAmt.setCurCode(pmtRec.getPmtInfo().getPmtAmt().getCurCode());
        ArrayList<PmtStatusRec> list = new ArrayList<PmtStatusRec>();
        list.add(pmtStatusRec);
        fawryResponseEnt.setPmtStatusRec(list);
        fawryResponseEnt.setStatus(status);
        fawryResponseEnt.setMsgCode("PmtNotifyRs");


        fawryResponseEnt.setIsRetry(pmtNotifyRq.getIsRetry());


        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S");
        String strDate = dateFormat.format(date);
        fawryResponseEnt.setServerDt(strDate);
        fawryResponseEnt.setRqUID(pmtNotifyRq.getRqUID());
        fawryResponseEnt.setAsyncRqUID(pmtNotifyRq.getAsyncRqUID());
        fawryResponseEnt.setTerminalId(pmtNotifyRq.getTerminalId());
        fawryResponseEnt.setClientTerminalSeqId(pmtNotifyRq.getClientTerminalSeqId());
        //fawryResponseEnt.setCustomProperties((com.qupworld.paymentgateway.entities.FawryResponseEnt.CustomProperty)*pmtNotifyRq.getCustomProperties());
        fawryResponseEnt.setSignature(hashPmtNotifyRs(fawryResponseEnt));

        } catch (Exception ex) {
            status.setStatusCode("211");
            status.setDescription("Exception Error");
            fawryResponseEnt.setStatus(status);
        }
        return fawryResponseEnt;
    }

    public String hashPmtNotifyRs(FawryResponseEnt pmtNotifyRs) {
        String msgToHash = "";
        msgToHash += pmtNotifyRs.getMsgCode().trim();
        msgToHash += pmtNotifyRs.getAsyncRqUID().trim();
        for (PmtId pmtId : pmtNotifyRs.getPmtStatusRec().get(0).getPmtIds()) {
            msgToHash += pmtId.getPmtId().trim();
            msgToHash += pmtId.getPmtIdType().trim();
            msgToHash += pmtId.getCreationDt();
        }
        if (pmtNotifyRs.getPmtStatusRec().get(0).getStatus() != null) {
            msgToHash += pmtNotifyRs.getPmtStatusRec().get(0).getStatus().getStatusCode();
        }

        try {
            return digestMessage(msgToHash);
        } catch (Exception ex) {
        }
        return "";
    }

    private String digestMessage(String text) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String secureKey = "QUPMEN!@#125743";
        text = text + secureKey;
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update(text.getBytes("utf-8"));
        String result = Base64Encoder.encode(messageDigest.digest());
        logger.debug("digestMessage: " +  result);
        return result;
    }

}