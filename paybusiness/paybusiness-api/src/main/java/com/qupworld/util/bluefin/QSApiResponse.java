package com.qupworld.util.bluefin;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */

public class QSApiResponse {

    private String transaction_id;
    private String original_transaction_id;
    private String tender_type;
    private String transaction_timestamp;
    private String card_brand;
    private String transaction_type;
    private String last4;
    private String card_expiration;
    private String authorization_code;
    private String authorization_message;
    private double transaction_amount;
    private String avs_response;
    private String cvv_response;
    private String cvv2_response;
    private String custom_id;
    private boolean error;
    private String error_code;
    private String error_message;
    private boolean keyed;
    private boolean swiped;
    private boolean transaction_approved;
    private String custom_data;
    private String transaction_description;
    private String ip_address;
    private String first_name;
    private String last_name;
    private double request_amount;


    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getOriginal_transaction_id() {
        return original_transaction_id;
    }

    public void setOriginal_transaction_id(String original_transaction_id) {
        this.original_transaction_id = original_transaction_id;
    }

    public String getTender_type() {
        return tender_type;
    }

    public void setTender_type(String tender_type) {
        this.tender_type = tender_type;
    }

    public String getTransaction_timestamp() {
        return transaction_timestamp;
    }

    public void setTransaction_timestamp(String transaction_timestamp) {
        this.transaction_timestamp = transaction_timestamp;
    }

    public String getCard_brand() {
        return card_brand;
    }

    public void setCard_brand(String card_brand) {
        this.card_brand = card_brand;
    }

    public String getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(String transaction_type) {
        this.transaction_type = transaction_type;
    }

    public String getLast4() {
        return last4;
    }

    public void setLast4(String last4) {
        this.last4 = last4;
    }

    public String getCard_expiration() {
        return card_expiration;
    }

    public void setCard_expiration(String card_expiration) {
        this.card_expiration = card_expiration;
    }

    public String getAuthorization_code() {
        return authorization_code;
    }

    public void setAuthorization_code(String authorization_code) {
        this.authorization_code = authorization_code;
    }

    public String getAuthorization_message() {
        return authorization_message;
    }

    public void setAuthorization_message(String authorization_message) {
        this.authorization_message = authorization_message;
    }

    public double getTransaction_amount() {
        return transaction_amount;
    }

    public void setTransaction_amount(double transaction_amount) {
        this.transaction_amount = transaction_amount;
    }

    public String getAvs_response() {
        return avs_response;
    }

    public void setAvs_response(String avs_response) {
        this.avs_response = avs_response;
    }

    public String getCvv_response() {
        return cvv_response;
    }

    public void setCvv_response(String cvv_response) {
        this.cvv_response = cvv_response;
    }

    public String getCustom_id() {
        return custom_id;
    }

    public void setCustom_id(String custom_id) {
        this.custom_id = custom_id;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_message() {
        return error_message;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }

    public boolean isKeyed() {
        return keyed;
    }

    public void setKeyed(boolean keyed) {
        this.keyed = keyed;
    }

    public boolean isSwiped() {
        return swiped;
    }

    public void setSwiped(boolean swiped) {
        this.swiped = swiped;
    }

    public boolean isTransaction_approved() {
        return transaction_approved;
    }

    public void setTransaction_approved(boolean transaction_approved) {
        this.transaction_approved = transaction_approved;
    }

    public String getCustom_data() {
        return custom_data;
    }

    public void setCustom_data(String custom_data) {
        this.custom_data = custom_data;
    }

    public String getTransaction_description() {
        return transaction_description;
    }

    public void setTransaction_description(String transaction_description) {
        this.transaction_description = transaction_description;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public double getRequest_amount() {
        return request_amount;
    }

    public void setRequest_amount(double request_amount) {
        this.request_amount = request_amount;
    }

    public String getCvv2_response() {
        return cvv2_response;
    }

    public void setCvv2_response(String cvv2_response) {
        this.cvv2_response = cvv2_response;
    }
}
