package com.qupworld.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qupworld.util.bluefin.QSApiRequest;
import com.qupworld.util.bluefin.QSApiResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class BluefinConnectionUtil {

    private static final Logger _log = LogManager.getLogger(BluefinConnectionUtil.class);

    public QSApiResponse createCreditToken(String serverUrl, QSApiRequest qsApiRequest, boolean enableCheckZipCode) throws Exception {
        // implement TSL 1.2
        SSLContext sslctx = SSLContext.getInstance("TLSv1.2");
        sslctx.init(null, null, null);
        HttpsURLConnection.setDefaultSSLSocketFactory(sslctx.getSocketFactory());

        URL url = new URL(serverUrl);
        String query = "account_id=" + qsApiRequest.getAccount_id() + "&" +
                "api_accesskey=" + qsApiRequest.getApi_accesskey() + "&" +
                "tender_type=" + qsApiRequest.getTender_type() + "&" +
                "transaction_type=" + qsApiRequest.getTransaction_type() + "&" +
                "custom_id=" + qsApiRequest.getCustom_id() + "&" +
                "card_number=" + qsApiRequest.getCard_number() + "&" +
                "card_expiration=" + qsApiRequest.getCard_expiration() + "&" +
                "card_verification=" + qsApiRequest.getCard_verification() + "&" +
                "first_name=" + qsApiRequest.getFirst_name() + "&" +
                "last_name=" + qsApiRequest.getLast_name() + "&" +
                "disable_cvv=" + qsApiRequest.isDisable_cvv() + "&" +
                "response_format=" + qsApiRequest.getResponse_format();
        if (enableCheckZipCode) {
            query = query + "&zip=" + qsApiRequest.getZip() +
                    "&disable_avs=" + 0;
        } else {
            query = query + "&disable_avs=" + 1;
        }
        String cardNumber = qsApiRequest.getCard_number();
        if (cardNumber != null && cardNumber.length() > 4) {
            String queryLog = query.replace(cardNumber, "XXXXXXXX" + cardNumber.substring(cardNumber.length() - 4));
            _log.debug("query: " + queryLog);
        }
        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-length", String.valueOf(query.length()));
        con.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
        con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0;Windows98;DigExt)");
        con.setDoOutput(true);
        con.setDoInput(true);

        //InputStream in = con.getInputStream();

        DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.close();

        return getResponse(con);
    }

    public QSApiResponse doPaymentByCardInfo(String serverUrl, QSApiRequest qsApiRequest, boolean isPayToDriver, boolean enableCheckZipCode) throws Exception {
        // implement TSL 1.2
        SSLContext sslctx = SSLContext.getInstance("TLSv1.2");
        sslctx.init(null, null, null);
        HttpsURLConnection.setDefaultSSLSocketFactory(sslctx.getSocketFactory());

        URL url = new URL(serverUrl);
        String query = "account_id=" + qsApiRequest.getAccount_id() + "&" +
                "api_accesskey=" + qsApiRequest.getApi_accesskey() + "&" +
                "tender_type=" + qsApiRequest.getTender_type() + "&" +
                "transaction_type=" + qsApiRequest.getTransaction_type() + "&" +
                "transaction_amount=" + qsApiRequest.getTransaction_amount() + "&" +
                "card_number=" + qsApiRequest.getCard_number() + "&" +
                "card_expiration=" + qsApiRequest.getCard_expiration() + "&" +
                "card_verification=" + qsApiRequest.getCard_verification() + "&" +
                "first_name=" + qsApiRequest.getFirst_name() + "&" +
                "last_name=" + qsApiRequest.getLast_name() + "&" +
                "custom_id=" + qsApiRequest.getCustom_id() + "&" +
                "response_format=" + qsApiRequest.getResponse_format();
        if (isPayToDriver)
            query += "&group=" + qsApiRequest.getGroup();
        if (enableCheckZipCode) {
            query = query + "&zip=" + qsApiRequest.getZip() +
                    "&disable_avs=" + 0;
        } else {
            query = query + "&disable_avs=" + 1;
        }
        String cardNumber = qsApiRequest.getCard_number();
        if (cardNumber != null && cardNumber.length() > 4) {
            String queryLog = query.replace(cardNumber, "XXXXXXXX" + cardNumber.substring(cardNumber.length() - 4));
            _log.debug("query: " + queryLog);
        }

        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-length", String.valueOf(query.length()));
        con.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
        con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0;Windows98;DigExt)");
        con.setDoOutput(true);
        con.setDoInput(true);

        //InputStream in = con.getInputStream();

        DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.close();

        return getResponse(con);
    }

    public QSApiResponse doPaymentByToken(String serverUrl, QSApiRequest qsApiRequest, boolean isPayToDriver) throws Exception {
        // implement TSL 1.2
        SSLContext sslctx = SSLContext.getInstance("TLSv1.2");
        sslctx.init(null, null, null);
        HttpsURLConnection.setDefaultSSLSocketFactory(sslctx.getSocketFactory());

        URL url = new URL(serverUrl);
        String query = "account_id=" + qsApiRequest.getAccount_id() + "&" +
                "api_accesskey=" + qsApiRequest.getApi_accesskey() + "&" +
                "tender_type=" + qsApiRequest.getTender_type() + "&" +
                "transaction_type=" + qsApiRequest.getTransaction_type() + "&" +
                "token_id=" + qsApiRequest.getToken_id() + "&" +
                "reissue=" + qsApiRequest.isReissue() + "&" +
                "transaction_amount=" + qsApiRequest.getTransaction_amount() + "&" +
                "custom_id=" + qsApiRequest.getCustom_id() + "&" +
                "response_format=" + qsApiRequest.getResponse_format();
        if (isPayToDriver )
            query += "&group=" + qsApiRequest.getGroup();
        _log.debug("query: " + query);

        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-length", String.valueOf(query.length()));
        con.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
        con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0;Windows98;DigExt)");
        con.setDoOutput(true);
        con.setDoInput(true);

        //InputStream in = con.getInputStream();

        DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.close();

        return getResponse(con);
    }

    public QSApiResponse doCreditByACH(String serverUrl, QSApiRequest qsApiRequest, boolean isPayToDriver) throws Exception {
        // implement TSL 1.2
        SSLContext sslctx = SSLContext.getInstance("TLSv1.2");
        sslctx.init(null, null, null);
        HttpsURLConnection.setDefaultSSLSocketFactory(sslctx.getSocketFactory());

        URL url = new URL(serverUrl);
        String query = "account_id=" + qsApiRequest.getAccount_id() + "&" +
                "api_accesskey=" + qsApiRequest.getApi_accesskey() + "&" +
                "tender_type=" + qsApiRequest.getTender_type() + "&" +
                "transaction_type=" + qsApiRequest.getTransaction_type() + "&" +
                "bank_account_number=" + qsApiRequest.getBank_account_number() + "&" +
                "bank_routing_number=" + qsApiRequest.getBank_routing_number() + "&" +
                "transaction_amount=" + qsApiRequest.getTransaction_amount() + "&" +
                "custom_id=" + qsApiRequest.getCustom_id() + "&" +
                "response_format=" + qsApiRequest.getResponse_format();
        if (isPayToDriver )
            query += "&group=" + qsApiRequest.getGroup();
        //_log.debug("query: " + query);

        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-length", String.valueOf(query.length()));
        con.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
        con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0;Windows98;DigExt)");
        con.setDoOutput(true);
        con.setDoInput(true);

        //InputStream in = con.getInputStream();

        DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.close();

        return getResponse(con);
    }

    public QSApiResponse createACHToken(String serverUrl, QSApiRequest qsApiRequest) throws Exception {
        // implement TSL 1.2
        SSLContext sslctx = SSLContext.getInstance("TLSv1.2");
        sslctx.init(null, null, null);
        HttpsURLConnection.setDefaultSSLSocketFactory(sslctx.getSocketFactory());

        URL url = new URL(serverUrl);
        String query = "account_id=" + qsApiRequest.getAccount_id() + "&" +
                "api_accesskey=" + qsApiRequest.getApi_accesskey() + "&" +
                "tender_type=" + qsApiRequest.getTender_type() + "&" +
                "transaction_type=" + qsApiRequest.getTransaction_type() + "&" +
                "bank_account_number=" + qsApiRequest.getBank_account_number() + "&" +
                "bank_routing_number=" + qsApiRequest.getBank_routing_number() + "&" +
                "first_name=" + qsApiRequest.getFirst_name() + "&" +
                "last_name=" + qsApiRequest.getLast_name() + "&" +
                "custom_id=" + qsApiRequest.getCustom_id() + "&" +
                "response_format=" + qsApiRequest.getResponse_format();
        _log.debug("query: " + query);

        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-length", String.valueOf(query.length()));
        con.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
        con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0;Windows98;DigExt)");
        con.setDoOutput(true);
        con.setDoInput(true);

        //InputStream in = con.getInputStream();

        DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.close();

        return getResponse(con);
    }

    public QSApiResponse doVoidTransaction(String serverUrl, QSApiRequest qsApiRequest) throws Exception {
        // implement TSL 1.2
        SSLContext sslctx = SSLContext.getInstance("TLSv1.2");
        sslctx.init(null, null, null);
        HttpsURLConnection.setDefaultSSLSocketFactory(sslctx.getSocketFactory());

        URL url = new URL(serverUrl);
        String query = "account_id=" + qsApiRequest.getAccount_id() + "&" +
                "api_accesskey=" + qsApiRequest.getApi_accesskey() + "&" +
                "transaction_type=" + qsApiRequest.getTransaction_type() + "&" +
                "token_id=" + qsApiRequest.getToken_id() + "&" +
                "response_format=" + qsApiRequest.getResponse_format();
        _log.debug("query: " + query);

        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-length", String.valueOf(query.length()));
        con.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
        con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0;Windows98;DigExt)");
        con.setDoOutput(true);
        con.setDoInput(true);

        //InputStream in = con.getInputStream();

        DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.close();

        return getResponse(con);
    }

    private QSApiResponse getResponse(HttpsURLConnection con){
        QSApiResponse apiResponse= new QSApiResponse();
        apiResponse.setError(true);// set default result is error in case something expected error
        try {
            if(HttpsURLConnection.HTTP_OK == con.getResponseCode()){
                BufferedReader br = null;
                StringBuilder sb = new StringBuilder();
                String line;
                try {
                    br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    if (br != null) {
                        try {
                            br.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                _log.debug("response: " + sb.toString());
                ObjectMapper mapper = new ObjectMapper();
                Map<String, String> data = mapper.readValue(sb.toString(), new TypeReference<HashMap<String,String>>(){});
                if (data.get("transaction_id") != null)
                    apiResponse.setTransaction_id(data.get("transaction_id"));
                if (data.get("tender_type") != null)
                    apiResponse.setTender_type(data.get("tender_type"));
                if (data.get("transaction_timestamp") != null)
                    apiResponse.setTransaction_timestamp(data.get("transaction_timestamp"));
                if (data.get("card_brand") != null) {
                    String cardBrand = data.get("card_brand");
                    apiResponse.setCard_brand(cardBrand);
                }
                if (data.get("transaction_type") != null)
                    apiResponse.setTransaction_type(data.get("transaction_type"));
                if (data.get("last4") != null)
                    apiResponse.setLast4(data.get("last4"));
                if (data.get("card_expiration") != null)
                    apiResponse.setCard_expiration(data.get("card_expiration"));
                if (data.get("authorization_code") != null)
                    apiResponse.setAuthorization_code(data.get("authorization_code"));
                if (data.get("authorization_message") != null) {
                    _log.debug("authorization message: " + data.get("authorization_message"));
                    apiResponse.setAuthorization_message(data.get("authorization_message"));
                }
                if (data.get("transaction_amount") != null)
                    apiResponse.setTransaction_amount(Double.parseDouble(data.get("transaction_amount")));
                if (data.get("avs_response") != null) {
                    _log.debug("avs response: " + data.get("avs_response"));
                    apiResponse.setAvs_response(data.get("avs_response"));
                }
                if (data.get("cvv_response") != null) {
                    _log.debug("cvv response: " + data.get("cvv_response"));
                    apiResponse.setCvv_response(data.get("cvv_response"));
                }
                if (data.get("cvv2_response") != null) {
                    _log.debug("cvv2 response: " + data.get("cvv2_response"));
                    apiResponse.setCvv2_response(data.get("cvv2_response"));
                }
                if (data.get("custom_id") != null)
                    apiResponse.setCustom_id(data.get("custom_id"));
                if (data.get("error") != null) {
                    _log.debug("error: " + data.get("error"));
                    apiResponse.setError(Boolean.parseBoolean(data.get("error")));
                }
                if (data.get("error_code") != null) {
                    _log.debug("error code: " + data.get("error_code"));
                    apiResponse.setError_code(data.get("error_code"));
                }
                if (data.get("error_message") != null) {
                    _log.debug("error message: " + data.get("error_message"));
                    apiResponse.setError_message(data.get("error_message"));
                }
                if (data.get("transaction_approved") != null)
                    apiResponse.setTransaction_approved(Boolean.parseBoolean(data.get("transaction_approved")));
                if (data.get("first_name") != null)
                    apiResponse.setFirst_name(data.get("first_name"));
                if (data.get("last_name") != null)
                    apiResponse.setLast_name(data.get("last_name"));
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return apiResponse;
    }

    public QSApiResponse doPaymentBySwiped(String serverUrl, QSApiRequest qsApiRequest, boolean isPayToDriver) throws Exception {
        // implement TSL 1.2
        SSLContext sslctx = SSLContext.getInstance("TLSv1.2");
        sslctx.init(null, null, null);
        HttpsURLConnection.setDefaultSSLSocketFactory(sslctx.getSocketFactory());

        URL url = new URL(serverUrl);
        String query = "account_id=" + qsApiRequest.getAccount_id() + "&" +
                "api_accesskey=" + qsApiRequest.getApi_accesskey() + "&" +
                "tender_type=" + qsApiRequest.getTender_type() + "&" +
                "transaction_type=" + qsApiRequest.getTransaction_type() + "&" +
                "transaction_amount=" + qsApiRequest.getTransaction_amount() + "&" +
                "card_tracks=" + qsApiRequest.getCard_tracks() + "&" +
                "custom_id=" + qsApiRequest.getCustom_id() + "&" +
                "response_format=" + qsApiRequest.getResponse_format();
        if (isPayToDriver)
            query += "&group=" + qsApiRequest.getGroup();
        _log.debug("query: " + query);

        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-length", String.valueOf(query.length()));
        con.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
        con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0;Windows98;DigExt)");
        con.setDoOutput(true);
        con.setDoInput(true);

        //InputStream in = con.getInputStream();

        DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.close();

        return getResponse(con);
    }
}
