package com.qupworld.util;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.controllers.UpdateDriverCreditBalance;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

public class NotifyToSlack {

    final static Logger logger = LogManager.getLogger(NotifyToSlack.class);
    public static void notifyUpdateOperation(String requestId, String data) throws Exception {
        StringBuffer buffer = new StringBuffer();
        buffer.append("```");
        buffer.append("requestId: " + requestId + "\n");
        buffer.append("error: " + data + "\n");
        buffer.append("<@UGSEF7KN3> ");
        buffer.append("<@UH4NY3MB7> ");
        buffer.append("<@UH3ACCSSH>");
        buffer.append("```");
        JSONObject objBody = new JSONObject();
        objBody.put("channel", ServerConfig.notify_channel);
        objBody.put("text", buffer.toString());
        HttpResponse<String> httpResponse = Unirest.post(ServerConfig.notify_url)
                .header("Authorization", "Bearer " + ServerConfig.notify_token)
                .header("Content-Type", "application/json; charset=utf-8")
                .body(objBody.toJSONString())
                .asString();
        String result = httpResponse.getBody();
        logger.debug(requestId + " - Notify to slack: " + result);
    }
}
