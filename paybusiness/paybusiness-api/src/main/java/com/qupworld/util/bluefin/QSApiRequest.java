package com.qupworld.util.bluefin;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */

public class QSApiRequest {

    private String account_id;
    private String api_accesskey;
    private String tender_type;
    private String transaction_type;
    private double transaction_amount;
    private String group;
    private String token_id;
    private String transaction_description;
    private String custom_id;
    private String custom_data;
    private String card_number;
    private String card_expiration;
    private String card_verification;
    private String card_tracks;
    private String card_track1;
    private String card_track2;
    private String cashier;
    private String bank_account_number;
    private String bank_routing_number;
    private String check_number;
    private String first_name;
    private String last_name;
    private String street_address1;
    private String street_address2;
    private String city;
    private String state;
    private String zip;
    private String country;
    private String ach_account_type;
    private String authorization_code;
    private String pin;
    private String ksn;
    private String ebt_type;
    private String ebt_voucher;
    private int disable_avs;
    private int disable_cvv;
    private int disable_fraudfirewall;
    private String ach_sec_code;
    private String ach_opcode;
    private String phone;
    private String email;
    private boolean send_customer_receipt;
    private boolean send_merchant_receipt;
    private String ip_address;
    private long transaction_id;
    private String response_format;
    private String jsonp;
    private int allow_partial;
    private double level2_tax;
    private String level2_zip;
    private String level2_order_id;
    private String level2_merchant_reference;
    private int restaurant_server_id;
    private double restaurant_gratuity;
    private String payment_type;
    private long installment_number;
    private long installment_count;
    private int reissue;


    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getApi_accesskey() {
        return api_accesskey;
    }

    public void setApi_accesskey(String api_accesskey) {
        this.api_accesskey = api_accesskey;
    }

    public String getTender_type() {
        return tender_type;
    }

    public void setTender_type(String tender_type) {
        this.tender_type = tender_type;
    }

    public String getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(String transaction_type) {
        this.transaction_type = transaction_type;
    }

    public double getTransaction_amount() {
        return transaction_amount;
    }

    public void setTransaction_amount(double transaction_amount) {
        this.transaction_amount = transaction_amount;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getToken_id() {
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }

    public String getTransaction_description() {
        return transaction_description;
    }

    public void setTransaction_description(String transaction_description) {
        this.transaction_description = transaction_description;
    }

    public String getCustom_id() {
        return custom_id;
    }

    public void setCustom_id(String custom_id) {
        this.custom_id = custom_id;
    }

    public String getCustom_data() {
        return custom_data;
    }

    public void setCustom_data(String custom_data) {
        this.custom_data = custom_data;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getCard_expiration() {
        return card_expiration;
    }

    public void setCard_expiration(String card_expiration) {
        this.card_expiration = card_expiration;
    }

    public String getCard_verification() {
        return card_verification;
    }

    public void setCard_verification(String card_verification) {
        this.card_verification = card_verification;
    }

    public String getCard_tracks() {
        return card_tracks;
    }

    public void setCard_tracks(String card_tracks) {
        this.card_tracks = card_tracks;
    }

    public String getCard_track1() {
        return card_track1;
    }

    public void setCard_track1(String card_track1) {
        this.card_track1 = card_track1;
    }

    public String getCard_track2() {
        return card_track2;
    }

    public void setCard_track2(String card_track2) {
        this.card_track2 = card_track2;
    }

    public String getCashier() {
        return cashier;
    }

    public void setCashier(String cashier) {
        this.cashier = cashier;
    }

    public String getBank_account_number() {
        return bank_account_number;
    }

    public void setBank_account_number(String bank_account_number) {
        this.bank_account_number = bank_account_number;
    }

    public String getBank_routing_number() {
        return bank_routing_number;
    }

    public void setBank_routing_number(String bank_routing_number) {
        this.bank_routing_number = bank_routing_number;
    }

    public String getCheck_number() {
        return check_number;
    }

    public void setCheck_number(String check_number) {
        this.check_number = check_number;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getStreet_address1() {
        return street_address1;
    }

    public void setStreet_address1(String street_address1) {
        this.street_address1 = street_address1;
    }

    public String getStreet_address2() {
        return street_address2;
    }

    public void setStreet_address2(String street_address2) {
        this.street_address2 = street_address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAch_account_type() {
        return ach_account_type;
    }

    public void setAch_account_type(String ach_account_type) {
        this.ach_account_type = ach_account_type;
    }

    public String getAuthorization_code() {
        return authorization_code;
    }

    public void setAuthorization_code(String authorization_code) {
        this.authorization_code = authorization_code;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getKsn() {
        return ksn;
    }

    public void setKsn(String ksn) {
        this.ksn = ksn;
    }

    public String getEbt_type() {
        return ebt_type;
    }

    public void setEbt_type(String ebt_type) {
        this.ebt_type = ebt_type;
    }

    public String getEbt_voucher() {
        return ebt_voucher;
    }

    public void setEbt_voucher(String ebt_voucher) {
        this.ebt_voucher = ebt_voucher;
    }

    public int isDisable_avs() {
        return disable_avs;
    }

    public void setDisable_avs(int disable_avs) {
        this.disable_avs = disable_avs;
    }

    public int isDisable_cvv() {
        return disable_cvv;
    }

    public void setDisable_cvv(int disable_cvv) {
        this.disable_cvv = disable_cvv;
    }

    public int isDisable_fraudfirewall() {
        return disable_fraudfirewall;
    }

    public void setDisable_fraudfirewall(int disable_fraudfirewall) {
        this.disable_fraudfirewall = disable_fraudfirewall;
    }

    public String getAch_sec_code() {
        return ach_sec_code;
    }

    public void setAch_sec_code(String ach_sec_code) {
        this.ach_sec_code = ach_sec_code;
    }

    public String getAch_opcode() {
        return ach_opcode;
    }

    public void setAch_opcode(String ach_opcode) {
        this.ach_opcode = ach_opcode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isSend_customer_receipt() {
        return send_customer_receipt;
    }

    public void setSend_customer_receipt(boolean send_customer_receipt) {
        this.send_customer_receipt = send_customer_receipt;
    }

    public boolean isSend_merchant_receipt() {
        return send_merchant_receipt;
    }

    public void setSend_merchant_receipt(boolean send_merchant_receipt) {
        this.send_merchant_receipt = send_merchant_receipt;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public long getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(long transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getResponse_format() {
        return response_format;
    }

    public void setResponse_format(String response_format) {
        this.response_format = response_format;
    }

    public String getJsonp() {
        return jsonp;
    }

    public void setJsonp(String jsonp) {
        this.jsonp = jsonp;
    }

    public int getAllow_partial() {
        return allow_partial;
    }

    public void setAllow_partial(int allow_partial) {
        this.allow_partial = allow_partial;
    }

    public double getLevel2_tax() {
        return level2_tax;
    }

    public void setLevel2_tax(double level2_tax) {
        this.level2_tax = level2_tax;
    }

    public String getLevel2_zip() {
        return level2_zip;
    }

    public void setLevel2_zip(String level2_zip) {
        this.level2_zip = level2_zip;
    }

    public String getLevel2_order_id() {
        return level2_order_id;
    }

    public void setLevel2_order_id(String level2_order_id) {
        this.level2_order_id = level2_order_id;
    }

    public String getLevel2_merchant_reference() {
        return level2_merchant_reference;
    }

    public void setLevel2_merchant_reference(String level2_merchant_reference) {
        this.level2_merchant_reference = level2_merchant_reference;
    }

    public int getRestaurant_server_id() {
        return restaurant_server_id;
    }

    public void setRestaurant_server_id(int restaurant_server_id) {
        this.restaurant_server_id = restaurant_server_id;
    }

    public double getRestaurant_gratuity() {
        return restaurant_gratuity;
    }

    public void setRestaurant_gratuity(double restaurant_gratuity) {
        this.restaurant_gratuity = restaurant_gratuity;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public long getInstallment_number() {
        return installment_number;
    }

    public void setInstallment_number(long installment_number) {
        this.installment_number = installment_number;
    }

    public long getInstallment_count() {
        return installment_count;
    }

    public void setInstallment_count(long installment_count) {
        this.installment_count = installment_count;
    }

    public int isReissue() {
        return reissue;
    }

    public void setReissue(int reissue) {
        this.reissue = reissue;
    }
}
