package com.qupworld;

import com.devebot.opflow.OpflowBuilder;
import com.devebot.opflow.OpflowServerlet;
import com.devebot.opflow.exception.OpflowBootstrapException;
import com.qupworld.config.ServerConfig;
import com.qupworld.service.PayBusiness;
import com.pg.util.KeysUtil;
import com.pg.util.SlackAPI;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.web.EmbeddedServletContainerAutoConfiguration;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * Created by myasus on 10/31/19.
 */
@SpringBootApplication
@EnableAutoConfiguration(
  exclude = {
    MongoAutoConfiguration.class,
    MongoDataAutoConfiguration.class,
    DataSourceAutoConfiguration.class,
    DataSourceTransactionManagerAutoConfiguration.class,
    HibernateJpaAutoConfiguration.class,
    EmbeddedServletContainerAutoConfiguration.class,
    WebMvcAutoConfiguration.class,
  }
)
public class Main {

  public static void main(String[] args) throws OpflowBootstrapException {
    SlackAPI slack = new SlackAPI();
    Logger logger = LogManager.getLogger(Main.class);
    String pathconfig = System.getProperty("pathconfig");
    logger.debug("pathconfig : " + pathconfig);
    if (pathconfig == null) {
      logger.debug("No PathConfig");
      slack.sendSlack("GET CONFIG ERROR", KeysUtil.URGREN, "");
    }
    ServerConfig.getInstance("/" + pathconfig);
    logger.debug("PayBusiness start: ");

    OpflowServerlet server = null;
    if (pathconfig == null) {
      server =
        OpflowBuilder.createServerlet(
          "privateConfig/qup-paybusiness-worker.properties"
        );
    } else {
      String opflowPath =
        "file:///" + pathconfig + "/qup-paybusiness-worker.properties";
      server = OpflowBuilder.createServerlet(opflowPath);
      logger.debug("Using Opflow From Config Path");
      slack.sendSlack("Using Opflow From Config Path", KeysUtil.INFO, "");
    }

    SpringApplicationBuilder app = new SpringApplicationBuilder(Main.class);
    ConfigurableApplicationContext context = app.web(false).run(args);
    server.instantiateType(
      PayBusiness.class,
      context.getBean("payBusinessImpl")
    );
    server.serve();

    logger.debug("[*] Waiting for message. To exit press CTRL+C");
  }
}
