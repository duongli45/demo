#!/bin/sh

java -Dlog4j.configuration=file:/app/config/log4j.properties \
     -Dpathconfig=/app/config \
     -Dopflow.configuration=file:/app/config/qup-payservice-worker.properties \
     -jar /app/payservice.jar $@