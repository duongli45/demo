package com.pg.util;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.qupworld.config.ServerConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.net.InetAddress;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

class CallSlackThread extends Thread {
    Logger logger = LogManager.getLogger(SlackAPI.class);
    public String message;
    public String type;
    String requestId;
    public void callSlack(String message,String type,String requestId) {
        VersionUtil versionUtil = new VersionUtil();
        logger.debug("Version: " + versionUtil.getVersion());
        try{
            InetAddress inetAddress = InetAddress.getLocalHost();
            if (ServerConfig.slack_url == null || ServerConfig.slack_url == ""){
                return;
            }
            String statusColor = "#29a365";
            switch (type) {
                case KeysUtil.URGREN:
                    statusColor = "#fb4473";
                    break;
                case KeysUtil.INFO:
                    statusColor = "#29a365";
                    break;
                case KeysUtil.ERROR:
                    statusColor = "#db5048";
                    break;
                case KeysUtil.WARNING:
                    statusColor = "#fecd52";
                    break;
            }
            Date now = new Date();
            long ut3 = now.getTime()/1000;
            String infor = "            \"pretext\": \""+type+"\",\n";
            if(requestId.length() > 0){
                infor =  "            \"pretext\": \""+type+" RequestId: "+requestId+"\",\n";
            }
            String body = "{\n" +
                    "    \"attachments\": [\n" +
                    "        {\n" +
                    infor +
                    "            \"color\": \""+statusColor+"\",\n" +
                    "            \"title\": \"PayService (Host: "+ inetAddress +" - Vesion: " +versionUtil.getVersion()+")\",\n" +
                    "            \"text\": \""+message+"\",\n" +
                    "            \"ts\": "+ut3+"\n" +
                    "        }\n" +
                    "    ]\n" +
                    "}";
            HttpResponse<String> jsonAuth = Unirest.post(ServerConfig.slack_url)
                    .header("Content-Type", "application/json")
                    .body(body)
                    .asString();


        }catch (Exception e){
            e.printStackTrace();

        }
    }

    @Override
    public void run() {
        logger.debug(requestId + " - " + "SendSlackThread");
        try {
            this.callSlack(message,type,requestId);

        } catch(Exception ex) {
            ex.printStackTrace();
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            logger.debug(requestId + " - " + "SendSlackThread Error: "+ errors.toString());
        }

    }

}
public class SlackAPI {

    public void sendSlack(String message,String type, String requestId){
        try {
            CallSlackThread callSlackThread = new CallSlackThread();
            callSlackThread.message = message;
            callSlackThread.type = type;
            callSlackThread.requestId = requestId;
            callSlackThread.start();
        }catch(Exception ex) {
            ex.printStackTrace();
        }


    }
}
