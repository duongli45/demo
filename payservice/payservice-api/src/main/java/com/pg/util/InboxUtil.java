package com.pg.util;

import com.mashape.unirest.http.Unirest;
import com.qupworld.config.ServerConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import java.text.DecimalFormat;

/**
 * Created by qup on 18/04/2019.
 */
public class InboxUtil {

    final Logger logger = LogManager.getLogger(InboxUtil.class);
    String requestId;
    public String INBOX_CASH_WALLET = "/api/v2/sendInboxForCashWallet";
    public String INBOX_PRE_AUTH = "/api/v2/sendInboxForPaxPreauth";

    public InboxUtil(String requestId){
        this.requestId = requestId;
    }

    public void sendInboxCashWallet(String referenceId, String userId, String currencyISO, double amount, String type, String creditNumber, String reason) {
        try {
            String encoding = Base64Encoder.encodeString(ServerConfig.cc_service_username + ":" + ServerConfig.cc_service_password);
            DecimalFormat df = new DecimalFormat("#.##");
            JSONObject objBody = new JSONObject();
            objBody.put("referenceId", referenceId);
            objBody.put("userId", userId);
            objBody.put("currencyISO", currencyISO);
            objBody.put("amount", df.format(amount));
            objBody.put("type", type);
            objBody.put("requestId", requestId);
            if (!creditNumber.isEmpty()) {
                objBody.put("creditNumber", creditNumber);
            }
            if (!reason.isEmpty()) {
                objBody.put("reason", reason);
            }
            logger.debug(requestId + " - sendInboxCashWallet body: " + objBody.toJSONString());
            com.mashape.unirest.http.HttpResponse<String> json = Unirest.post(ServerConfig.qup_service + INBOX_CASH_WALLET)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("Authorization", "Basic " + encoding)
                    .body(objBody.toJSONString())
                    .asString();
            logger.debug(requestId + " - sendInboxCashWallet response: " + json.getBody().toString());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void sendInboxPreauth(String userId, String currencyISO, double amount, String bookId) {
        try {
            String encoding = Base64Encoder.encodeString(ServerConfig.cc_service_username + ":" + ServerConfig.cc_service_password);
            DecimalFormat df = new DecimalFormat("#.##");
            JSONObject objBody = new JSONObject();
            objBody.put("userId", userId);
            objBody.put("currencyISO", currencyISO);
            objBody.put("amount", df.format(amount));
            objBody.put("bookId", bookId);
            objBody.put("requestId", requestId);

            logger.debug(requestId + " - sendInboxPreauth body: " + objBody.toJSONString());
            com.mashape.unirest.http.HttpResponse<String> json = Unirest.post(ServerConfig.qup_service + INBOX_PRE_AUTH)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("Authorization", "Basic " + encoding)
                    .body(objBody.toJSONString())
                    .asString();
            logger.debug(requestId + " - sendInboxPreauth response: " + json.getBody().toString());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
