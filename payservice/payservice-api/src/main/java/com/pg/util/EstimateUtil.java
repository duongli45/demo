package com.pg.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.entities.*;
import com.qupworld.paymentgateway.entities.Passenger;
import com.qupworld.paymentgateway.entities.Waypoint;
import com.qupworld.paymentgateway.models.*;
import com.qupworld.paymentgateway.models.mongo.collections.*;
import com.qupworld.paymentgateway.models.mongo.collections.AffiliateSetting.AffiliateSetting;
import com.qupworld.paymentgateway.models.mongo.collections.account.Account;
import com.qupworld.paymentgateway.models.mongo.collections.additionalServices.AdditionalServices;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateAssignedRate.AffiliateAssignedRate;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareFlat.AffiliateFlat;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareGeneral.RateGeneral;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareHourly.AffiliateHourly;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareRegular.CancellationPolicy;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareRegular.NoShowPolicy;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareRegular.RateRegular;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateMarkupPrice.AffiliateMarkupPrice;
import com.qupworld.paymentgateway.models.mongo.collections.affiliatePackages.AffiliatePackages;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateProcess.AffiliateProcess;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateRoute.AffiliateRoute;
import com.qupworld.paymentgateway.models.mongo.collections.affiliationCarType.AffiliationCarType;
import com.qupworld.paymentgateway.models.mongo.collections.airportFee.AirportFee;
import com.qupworld.paymentgateway.models.mongo.collections.airportZone.AirportZone;
import com.qupworld.paymentgateway.models.mongo.collections.booking.*;
import com.qupworld.paymentgateway.models.mongo.collections.company.Company;
import com.qupworld.paymentgateway.models.mongo.collections.corporate.Corporate;
import com.qupworld.paymentgateway.models.mongo.collections.dynamicFare.DynamicFare;
import com.qupworld.paymentgateway.models.mongo.collections.dynamicSurcharge.DynamicSurcharge;
import com.qupworld.paymentgateway.models.mongo.collections.fareDelivery.FareDelivery;
import com.qupworld.paymentgateway.models.mongo.collections.fareFlat.FareFlat;
import com.qupworld.paymentgateway.models.mongo.collections.fareHourly.FareHourly;
import com.qupworld.paymentgateway.models.mongo.collections.fareNormal.FareNormal;
import com.qupworld.paymentgateway.models.mongo.collections.fareNormal.FeesByCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.fareSharing.FareSharing;
import com.qupworld.paymentgateway.models.mongo.collections.fareSharing.FeePerPax;
import com.qupworld.paymentgateway.models.mongo.collections.flatRoutes.FlatRoutes;
import com.qupworld.paymentgateway.models.mongo.collections.fleet.*;
import com.qupworld.paymentgateway.models.mongo.collections.fleetfare.*;
import com.qupworld.paymentgateway.models.mongo.collections.intercityRoute.CancelPolicy;
import com.qupworld.paymentgateway.models.mongo.collections.intercityRoute.IntercityRoute;
import com.qupworld.paymentgateway.models.mongo.collections.menuMerchant.MenuMerchant;
import com.qupworld.paymentgateway.models.mongo.collections.packageRate.FeesWithCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.packageRate.PackageRate;
import com.qupworld.paymentgateway.models.mongo.collections.pricingplan.PricingPlan;
import com.qupworld.paymentgateway.models.mongo.collections.promotionCode.PromotionCode;
import com.qupworld.paymentgateway.models.mongo.collections.serviceFee.ServiceFee;
import com.qupworld.paymentgateway.models.mongo.collections.vehicleType.CorpRate;
import com.qupworld.paymentgateway.models.mongo.collections.vehicleType.Rate;
import com.qupworld.paymentgateway.models.mongo.collections.vehicleType.VehicleType;
import com.qupworld.paymentgateway.models.mongo.collections.zone.Zone;
import org.apache.commons.lang.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by qup on 7/26/16.
 */
public class EstimateUtil {

    public MongoDao mongoDao;
    public SQLDao sqlDao;
    public RedisDao redisDao;
    public static String getTzFromDispatch = "/api/v2/timezone/cache";
    public static String getTzFromMapsProvider = "/api/timezone";
    private static String get_receipts_output_data = "/api/item/get_items_output_data";
    private final static Logger logger = LogManager.getLogger(EstimateUtil.class);
    private final SimpleDateFormat sdfMongo = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
    private final SimpleDateFormat sdfSaveRedis = new SimpleDateFormat("yyyy-MM-dd");
    private final List<String> bookPax = Arrays.asList("CC", "API", "Web booking", "Partner", "mDispatcher", "Kiosk",
            "webBooking", "partner", "PWA", "");
    private Gson gson = new Gson();

    public EstimateUtil() {
        mongoDao = new MongoDaoImpl();
        sqlDao = new SQLDaoImpl();
        redisDao = new RedisImpl();
    }

    private double getModifyPriceOfCorp(String corporateId, double fare, String requestId) {
        double discount = fare;
        if (corporateId != null && !corporateId.isEmpty()) {
            Corporate corporate = mongoDao.getCorporate(corporateId);
            if (corporate != null && corporate.isActive && corporate.pricing != null) {
                if (corporate.pricing.discountByPercentage != null && corporate.pricing.discountByPercentage) {
                    logger.debug(requestId + " - " + "discount:  " + corporate.pricing.value);
                    discount = fare - (fare * corporate.pricing.value) / 100;
                } else if (corporate.pricing.markUpByPercentage) {
                    logger.debug(requestId + " - " + "markup:  " + corporate.pricing.markUpValue);
                    discount = fare + (fare * corporate.pricing.markUpValue) / 100;
                }
            }
        }
        return discount;
    }

    public Map<String, String> getCreditToken(Booking booking, boolean isLocalZone) {
        String cardOwner = "passenger";
        String localToken = "";
        String crossToken = "";
        String cardMask = "";
        String id = "";
        String gateway = "";
        String cardHolder = "";
        String cardType = "";
        if (booking.psgInfo != null && booking.psgInfo.creditInfo != null) {
            localToken = booking.psgInfo.creditInfo.localToken != null ? booking.psgInfo.creditInfo.localToken : "";
            crossToken = booking.psgInfo.creditInfo.crossToken != null ? booking.psgInfo.creditInfo.crossToken : "";
            cardMask = booking.psgInfo.creditInfo.cardMask != null ? booking.psgInfo.creditInfo.cardMask : "";
            id = booking.psgInfo.userId != null ? booking.psgInfo.userId : "";
            gateway = booking.psgInfo.creditInfo.gateway != null ? booking.psgInfo.creditInfo.gateway : "";
            cardHolder = booking.psgInfo.creditInfo.cardHolder != null ? booking.psgInfo.creditInfo.cardHolder : "";
            cardType = booking.psgInfo.creditInfo.cardType != null ? booking.psgInfo.creditInfo.cardType : "";
        }
        if (localToken.equals("") && crossToken.equals("")) {
            // booking do not using passenger card
            // try to check card of mDispatcher of corporate
            if (booking.bookFrom.equals("mDispatcher")) {
                if (booking.mDispatcherInfo != null && booking.mDispatcherInfo.creditInfo != null) {
                    localToken = booking.mDispatcherInfo.creditInfo.localToken != null
                            ? booking.mDispatcherInfo.creditInfo.localToken
                            : "";
                    crossToken = booking.mDispatcherInfo.creditInfo.crossToken != null
                            ? booking.mDispatcherInfo.creditInfo.crossToken
                            : "";
                    cardMask = booking.mDispatcherInfo.creditInfo.cardMask != null
                            ? booking.mDispatcherInfo.creditInfo.cardMask
                            : "";
                    cardOwner = "mDispatcher";
                    id = booking.mDispatcherInfo.userId != null ? booking.mDispatcherInfo.userId : "";
                    gateway = booking.mDispatcherInfo.creditInfo.gateway != null
                            ? booking.mDispatcherInfo.creditInfo.gateway
                            : "";
                    cardHolder = booking.mDispatcherInfo.creditInfo.cardHolder != null
                            ? booking.mDispatcherInfo.creditInfo.cardHolder
                            : "";
                    cardType = booking.mDispatcherInfo.creditInfo.cardType != null
                            ? booking.mDispatcherInfo.creditInfo.cardType
                            : "";
                }
            } else {
                if (booking.corporateInfo != null && booking.corporateInfo.creditInfo != null) {
                    localToken = booking.corporateInfo.creditInfo.localToken != null
                            ? booking.corporateInfo.creditInfo.localToken
                            : "";
                    crossToken = booking.corporateInfo.creditInfo.crossToken != null
                            ? booking.corporateInfo.creditInfo.crossToken
                            : "";
                    cardMask = booking.corporateInfo.creditInfo.cardMask != null
                            ? booking.corporateInfo.creditInfo.cardMask
                            : "";
                    cardOwner = "corporate";
                    id = booking.corporateInfo.corporateId != null ? booking.corporateInfo.corporateId : "";
                    gateway = booking.corporateInfo.creditInfo.gateway != null
                            ? booking.corporateInfo.creditInfo.gateway
                            : "";
                    cardHolder = booking.corporateInfo.creditInfo.cardHolder != null
                            ? booking.corporateInfo.creditInfo.cardHolder
                            : "";
                    cardType = booking.corporateInfo.creditInfo.cardType != null
                            ? booking.corporateInfo.creditInfo.cardType
                            : "";
                }
            }
        }
        String token = isLocalZone ? localToken : crossToken;
        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        map.put("cardOwner", cardOwner);
        map.put("cardMask", cardMask);
        map.put("id", id);
        map.put("gateway", gateway);
        map.put("cardHolder", cardHolder);
        map.put("cardType", cardType);
        return map;
    }

    public String getCancelAmount(Booking booking, Fleet fleet, String status, String cancelFrom, String requestId) {
        List<Double> listPolicy;
        if (booking.intercity) {
            listPolicy = getCancelValueForIntercity(booking, fleet, status, requestId);
        } else {
            listPolicy = getCancelValue(booking, fleet, status, cancelFrom, requestId);
        }
        logger.debug(requestId + " - listPolicy: " + gson.toJson(listPolicy));
        ObjResponse objResponse = new ObjResponse();
        Map<String, Double> mapData = new HashMap<>();

        double cancelAmount = listPolicy.get(0);
        if (booking.delivery || (booking.intercity && booking.request != null && booking.request.paymentType == 1)) {
            cancelAmount = 0;
        }
        double driverGetAmount = listPolicy.get(1);
        double tax = listPolicy.get(2);
        double techFee = listPolicy.get(3);
        double fleetCommission = listPolicy.get(4);
        double payToDriver = listPolicy.size() > 5 ? listPolicy.get(5) : 0;
        double policyAmount = listPolicy.size() > 6 ? listPolicy.get(6) : 0;
        mapData.put("cancelAmount", CommonUtils.getRoundFormat(cancelAmount));
        mapData.put("cancelEarning", CommonUtils.getRoundFormat(driverGetAmount));
        mapData.put("taxFee", CommonUtils.getRoundFormat(tax));
        mapData.put("techFee", CommonUtils.getRoundFormat(techFee));
        mapData.put("fleetCommission", CommonUtils.getRoundFormat(fleetCommission));
        mapData.put("payToDriver", CommonUtils.getRoundFormat(payToDriver));
        mapData.put("policyAmount", CommonUtils.getRoundFormat(policyAmount));
        objResponse.returnCode = 200;
        objResponse.response = mapData;
        return objResponse.toString();

    }

    public String getCancelAmountForAffiliate(Booking booking, String fleetId, String status, String priceType,
            String requestId) {
        ObjResponse objResponse = new ObjResponse();
        Map<String, Object> mapData = new HashMap<>();

        double compensatePolicy = 0;
        double penalizePolicy = 0;
        boolean isNoShow = false;
        FleetFare fleetFare = mongoDao.getFleetFare(booking.fleetId);
        if (fleetFare != null && fleetFare.noShow != null)
            isNoShow = fleetFare.noShow.isActive;

        AffiliateProcess affiliateProcess = mongoDao.findAffiliateProcessTopByOrderByCreatedDateDesc();
        if (booking.rv != null && booking.rv.affiliateProcess != null && !booking.rv.affiliateProcess.isEmpty()) {
            affiliateProcess = mongoDao.findAffiliateProcessById(booking.rv.affiliateProcess);
        }
        Calendar cal = Calendar.getInstance();
        Date pickupTime = null;
        Date currentTime = null;
        boolean chargeInAdvance = false;
        boolean fullCharge = false;
        String chargeType = "onDemand";
        String canceller = "";
        try {
            if (booking.time != null) {
                if (booking.time.pickUpTime != null) {
                    pickupTime = TimezoneUtil.offsetTimeZone(booking.time.pickUpTime,
                            cal.getTimeZone().getID(), "GMT");
                    logger.debug(requestId + " - " + "pickupTime: " + pickupTime);
                }
                currentTime = TimezoneUtil.convertServerToGMT(new Date());
                logger.debug(requestId + " - " + "currentTime: " + currentTime);
                if (currentTime.before(pickupTime)) {
                    double milliseconds = (double) pickupTime.getTime() - currentTime.getTime();
                    double days = milliseconds / 86400000;
                    logger.debug(requestId + " - " + "cancel before : " + days + " days");
                    if (days < affiliateProcess.chargeInAdvance.value
                            && days > affiliateProcess.fullCharge.value / 24) {
                        logger.debug(requestId + " - " + "cancel before : " + days + " days - charge InAdvance");
                        chargeInAdvance = true;
                    } else if (days <= affiliateProcess.fullCharge.value / 24) {
                        logger.debug(requestId + " - " + "cancel before : " + days + " days - full charge");
                        fullCharge = true;
                    } else {
                        logger.debug(requestId + " - " + "cancel before : " + days + " days - free charge");
                        chargeType = "free";
                    }
                    if (booking.status.equals(KeysUtil.canceled) && status.equals(KeysUtil.INCOME_REJECTED)) {
                        logger.debug(requestId + " - " + "Supplier rejected booking - free charge");
                        chargeType = "free";
                        chargeInAdvance = false;
                        fullCharge = false;
                    }
                }
            }
        } catch (Exception ex) {

        }
        if (booking.cancelInfo != null && booking.cancelInfo.canceller != null)
            canceller = booking.cancelInfo.canceller;
        if (booking.status.equals(KeysUtil.canceled) || booking.status.equals("rejected")
                || status.equals(KeysUtil.canceled)
                || status.equals(KeysUtil.INCOME_REJECTED)) {
            if (booking.reservation && chargeInAdvance)
                chargeType = "inAdvance";
            if (booking.reservation && fullCharge)
                chargeType = "full";
            if (canceller.equals("timeout"))
                chargeType = "timeout";
        } else
            chargeType = "noShow";
        if (booking.reservation && booking.status.equals("waitingPayment") && !chargeInAdvance && !fullCharge)
            chargeType = "free";

        logger.debug(requestId + " - " + "chargeType: " + chargeType);
        if (!chargeType.equals("free")) {
            /// GET FARE
            String zoneId = "";
            String fareNormalId = "";
            String vhcTypeId = "";
            String fareHourlyId = "";
            String fareFlatId = "";
            String zipCodeFrom = "";
            String zipCodeTo = "";
            List<Double> pickup = new ArrayList<>();
            List<Double> destination = new ArrayList<>();
            RateGeneral rateGeneral = null;
            AffiliationCarType carType = null;

            if (booking.request != null && booking.request.vehicleTypeRequest != null
                    && !booking.request.vehicleTypeRequest.isEmpty()) {
                carType = mongoDao.getAffiliationCarTypeByName(booking.request.vehicleTypeRequest);
                if (carType != null)
                    vhcTypeId = carType._id.toString();
            }
            logger.debug(requestId + " - " + "vhcTypeId: " + vhcTypeId);
            if (booking.request != null && booking.request.pickup != null) {
                zipCodeFrom = booking.request.pickup.zipCode != null ? booking.request.pickup.zipCode : "";
                pickup = booking.request.pickup.geo;
                List<Zone> zones = mongoDao.findByGeoAffiliate(booking.request.pickup.geo, booking.request.psgFleetId,
                        vhcTypeId);
                if (zones != null && !zones.isEmpty())
                    zoneId = zones.get(0)._id.toString();
                // get zoneId by fleetId
                if (!fleetId.isEmpty()) {
                    for (Zone zone : zones) {
                        if (zone.fleetId.equals(fleetId))
                            zoneId = zone._id.toString();
                    }
                }
            }
            logger.debug(requestId + " - " + "zoneId: " + zoneId);

            if (booking.request != null && booking.request.destination != null) {
                zipCodeTo = booking.request.destination.zipCode != null ? booking.request.destination.zipCode : "";
                destination = booking.request.destination.geo;
            }

            if (!vhcTypeId.isEmpty() && !zoneId.isEmpty()) {

                List<AffiliateAssignedRate> listAffiliateAssignedRate = mongoDao.getListAffiliateAssignedRate(vhcTypeId,
                        zoneId);

                AffiliateAssignedRate assignedRateGeneral = listAffiliateAssignedRate.stream()
                        .filter(assignedRate -> assignedRate.type.equalsIgnoreCase(KeysUtil.GENERAL))
                        .findFirst().orElse(null);
                if (assignedRateGeneral != null)
                    rateGeneral = mongoDao.getAffiliateRateGeneral(assignedRateGeneral.rate.id);

                AffiliateAssignedRate assignedRateNormal = listAffiliateAssignedRate.stream()
                        .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase(fleetId)
                                && assignedRate.type.equalsIgnoreCase(KeysUtil.REGULAR)
                                && assignedRate.priceType.equalsIgnoreCase(priceType))
                        .findFirst().orElse(null);
                if (assignedRateNormal == null) {
                    assignedRateNormal = listAffiliateAssignedRate.stream()
                            .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase("")
                                    && assignedRate.type.equalsIgnoreCase(KeysUtil.REGULAR)
                                    && assignedRate.priceType.equalsIgnoreCase(priceType))
                            .findFirst().orElse(null);
                }
                if (assignedRateNormal != null) {
                    fareNormalId = assignedRateNormal.rate.id;
                }

                AffiliateAssignedRate assignedRateHourly = listAffiliateAssignedRate.stream()
                        .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase(fleetId)
                                && assignedRate.type.equalsIgnoreCase(KeysUtil.HOURLY)
                                && assignedRate.priceType.equalsIgnoreCase(priceType))
                        .findFirst().orElse(null);
                if (assignedRateHourly == null) {
                    assignedRateHourly = listAffiliateAssignedRate.stream()
                            .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase("")
                                    && assignedRate.type.equalsIgnoreCase(KeysUtil.HOURLY)
                                    && assignedRate.priceType.equalsIgnoreCase(priceType))
                            .findFirst().orElse(null);
                }
                if (assignedRateHourly != null) {
                    fareHourlyId = assignedRateHourly.rate.id;
                }
            }
            if (!vhcTypeId.isEmpty()) {
                List<AffiliateAssignedRate> listAffiliateFlatRate = mongoDao.findAffiliateFlatRateByCarType(vhcTypeId);
                AffiliateAssignedRate assignedRateFlat = listAffiliateFlatRate.stream()
                        .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase(fleetId)
                                && assignedRate.priceType.equalsIgnoreCase(priceType))
                        .findFirst().orElse(null);
                if (assignedRateFlat == null) {
                    assignedRateFlat = listAffiliateFlatRate.stream()
                            .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase("")
                                    && assignedRate.priceType.equalsIgnoreCase(priceType))
                            .findFirst().orElse(null);
                }
                if (assignedRateFlat != null) {
                    fareFlatId = assignedRateFlat.rate.id;
                }
            }
            logger.debug(requestId + " - " + "fareNormalId: " + fareNormalId);
            logger.debug(requestId + " - " + "fareFlatId: " + fareFlatId);
            logger.debug(requestId + " - " + "fareHourlyId: " + fareHourlyId);
            double techFeeValue = 0;
            if (rateGeneral != null) {
                // get tech fee
                if (rateGeneral.techFee != null && rateGeneral.techFee.enable != null && rateGeneral.techFee.enable) {
                    if (KeysUtil.web_booking.equals(booking.bookFrom))
                        techFeeValue = rateGeneral.techFee.webBooking != null ? rateGeneral.techFee.webBooking : 0;
                    else if (KeysUtil.command_center.equals(booking.bookFrom))
                        techFeeValue = rateGeneral.techFee.commandCenter != null ? rateGeneral.techFee.commandCenter
                                : 0;
                    else if (KeysUtil.dash_board.equals(booking.bookFrom))
                        techFeeValue = rateGeneral.techFee.bookingDashboard != null
                                ? rateGeneral.techFee.bookingDashboard
                                : 0;
                    else if (!KeysUtil.kiosk.equals(booking.bookFrom) || !KeysUtil.mDispatcher.equals(booking.bookFrom)
                            || !KeysUtil.car_hailing.equals(booking.bookFrom) || !KeysUtil.api.equals(booking.bookFrom))
                        techFeeValue = rateGeneral.techFee.paxApp != null ? rateGeneral.techFee.paxApp : 0;
                }
            }
            logger.debug(requestId + " - " + "techFee: " + CommonUtils.getRoundValue(techFeeValue));
            mapData.put("techFee", CommonUtils.getRoundValue(techFeeValue));

            CancellationPolicy cancellationPolicy = null;
            NoShowPolicy noShowPolicy = null;
            if (booking.request.type == 3 || booking.request.typeRate == 1) {
                AffiliateHourly fareHourly = mongoDao.findAffiliateHourlyById(fareHourlyId);
                if (fareHourly != null) {
                    if (!priceType.equals(KeysUtil.BUYPRICE)) {
                        cancellationPolicy = fareHourly.cancellationPolicy;
                        noShowPolicy = fareHourly.noShowPolicy;
                    } else {
                        cancellationPolicy = fareHourly.cancellationPolicySupplier;
                        noShowPolicy = fareHourly.noShowPolicySupplier;
                    }
                }
            } else {
                boolean checkFlatFare = false;
                AffiliateFlat fareFlat = mongoDao.findAffiliateFlatById(fareFlatId);
                if (fareFlat != null) {
                    logger.debug(requestId + " - " + "fareFlat: " + fareFlat._id + " - pickup: " + pickup
                            + " - destination: " + destination + " - from: " + zipCodeFrom + " - to: " + zipCodeTo);
                    AffiliateRoute flatRoutes = getFareByFlatRoutesAffiliate(fareFlatId, pickup, destination,
                            zipCodeFrom, zipCodeTo);
                    AffiliateRoute flatRoutesReturn = getFareByFlatRoutesReturnAffiliate(fareFlatId, pickup,
                            destination, zipCodeFrom, zipCodeTo);
                    if (flatRoutes != null) {
                        logger.debug(requestId + " - " + "flatRoutesId: " + flatRoutes._id);
                        checkFlatFare = true;
                    } else if (flatRoutesReturn != null) {
                        logger.debug(requestId + " - " + "flatRoutesId: " + flatRoutesReturn._id);
                        checkFlatFare = true;
                    }
                }
                logger.debug(requestId + " - " + "checkFlatFare: " + checkFlatFare);
                if (checkFlatFare) {
                    if (fareFlat != null) {
                        cancellationPolicy = fareFlat.cancellationPolicy;
                        noShowPolicy = fareFlat.noShowPolicy;
                    }
                } else {
                    RateRegular fareNormal = mongoDao.getAffiliateRate(fareNormalId);
                    if (fareNormal != null) {
                        cancellationPolicy = fareNormal.cancellationPolicy;
                        noShowPolicy = fareNormal.noShowPolicy;
                    }
                }
            }
            if (booking.status.equals(KeysUtil.canceled) || booking.status.equals("rejected")
                    || status.equals(KeysUtil.canceled)
                    || status.equals(KeysUtil.INCOME_REJECTED)) {
                if (cancellationPolicy != null) {
                    if (booking.reservation) {
                        if (cancellationPolicy.penalize != null && cancellationPolicy.penalize.inAdvance != null &&
                                cancellationPolicy.penalize.inAdvance.isActive) {
                            if (chargeInAdvance)
                                penalizePolicy = cancellationPolicy.penalize.inAdvance.beforeAdvanceTime;
                            if (fullCharge)
                                penalizePolicy = cancellationPolicy.penalize.inAdvance.afterAdvanceTime;
                        }
                        if (cancellationPolicy.compensate != null && cancellationPolicy.compensate.inAdvance != null &&
                                cancellationPolicy.compensate.inAdvance.isActive) {
                            if (chargeInAdvance)
                                compensatePolicy = cancellationPolicy.compensate.inAdvance.beforeAdvanceTime;
                            if (fullCharge)
                                compensatePolicy = cancellationPolicy.compensate.inAdvance.afterAdvanceTime;
                        }
                    } else {
                        if (cancellationPolicy.penalize.onDemand != null
                                && cancellationPolicy.penalize.onDemand.isActive) {
                            penalizePolicy = cancellationPolicy.penalize.onDemand.value;
                        }
                        if (cancellationPolicy.compensate.onDemand != null
                                && cancellationPolicy.compensate.onDemand.isActive) {
                            compensatePolicy = cancellationPolicy.compensate.onDemand.value;
                        }
                    }
                }
            } else {
                if (isNoShow && noShowPolicy != null) {
                    penalizePolicy = noShowPolicy.penalize;
                    compensatePolicy = noShowPolicy.compensate;
                }
            }
            if (chargeType.equals("timeout")) {
                penalizePolicy = 100;
                compensatePolicy = 100;
            }
            if (penalizePolicy == 0 && compensatePolicy == 0) {
                if (chargeType.equals("inAdvance") || chargeType.equals("full") || chargeType.equals("noShow")) {
                    logger.debug(requestId + " - " + "penalizePolicy = 0 & compensatePolicy = 0 !!");
                    chargeType = "free";
                }
            }

            logger.debug(requestId + " - " + "penalizePolicy: " + CommonUtils.getRoundValue(penalizePolicy));
            logger.debug(requestId + " - " + "compensatePolicy: " + CommonUtils.getRoundValue(compensatePolicy));
        }
        mapData.put("penalizePolicy", CommonUtils.getRoundValue(penalizePolicy));
        mapData.put("compensatePolicy", CommonUtils.getRoundValue(compensatePolicy));
        mapData.put("chargeType", chargeType);

        objResponse.returnCode = 200;
        objResponse.response = mapData;
        return objResponse.toString();
    }

    public List<Object> getTechFeeValueByZone(Fleet fleet, String bookFrom, String currencyISO, String zoneId) {
        List<Object> data = new ArrayList<Object>();
        double techFee = 0;
        boolean techFeeActive = false;
        if (fleet.fleetType.equals("Limo") && !KeysUtil.THIRD_PARTY_HOLIDAYTAXIS.equals(bookFrom)) {
            FleetFare fleetFare = mongoDao.getFleetFare(fleet.fleetId);
            TechFeeByCurrency techFeeByCurrency = null;
            if (fleetFare.applyType == null || fleetFare.applyType.equals("all")
                    || !fleet.generalSetting.allowCongfigSettingForEachZone) {
                PricingPlan pricingPlan = mongoDao.getPricingPlan(fleet.fleetId);
                if (pricingPlan != null) {
                    techFeeActive = pricingPlan.withinZone.techFeeActive;
                    if (pricingPlan.withinZone.techFeeActive && pricingPlan.withinZone.techFeeByCurrencies != null) {
                        for (TechFeeByCurrency feeByCurrency : pricingPlan.withinZone.techFeeByCurrencies) {
                            if (feeByCurrency.currencyISO.equals(currencyISO)) {
                                techFeeByCurrency = feeByCurrency;
                                break;
                            }
                        }
                    }
                }
            } else {
                ServiceFee serviceFee = mongoDao.getServiceFeeByFleetIdAndZoneId(fleet.fleetId, zoneId);
                if (serviceFee != null) {
                    techFeeActive = serviceFee.techFeeActive;
                    if (serviceFee.techFeeActive && serviceFee.techFeeByCurrencies != null) {
                        for (TechFeeByCurrency feeByCurrency : serviceFee.techFeeByCurrencies) {
                            if (feeByCurrency.currencyISO.equals(currencyISO)) {
                                techFeeByCurrency = feeByCurrency;
                                break;
                            }
                        }
                    }
                }
            }
            if (techFeeByCurrency != null) {
                if (KeysUtil.web_booking.equals(bookFrom) || KeysUtil.PWA.equals(bookFrom)) {
                    techFee = techFeeByCurrency.webBooking;
                } else if (KeysUtil.command_center.equals(bookFrom)) {
                    techFee = techFeeByCurrency.commandCenter;
                } else if (KeysUtil.partner.equals(bookFrom)) {
                    techFee = techFeeByCurrency.partner;
                } else if (KeysUtil.mDispatcher.equals(bookFrom)) {
                    techFee = techFeeByCurrency.partner;
                } else if (KeysUtil.car_hailing.equals(bookFrom)) {
                    techFee = techFeeByCurrency.carHailing;
                } else if (KeysUtil.kiosk.equals(bookFrom)) {
                    techFee = techFeeByCurrency.kiosk;
                } else if (KeysUtil.api.equals(bookFrom)) {
                    techFee = techFeeByCurrency.api;
                } else if (KeysUtil.street_sharing.equals(bookFrom)) {
                    techFee = techFeeByCurrency.streetSharing;
                } else {
                    techFee = techFeeByCurrency.paxApp;
                }
            }
        }
        data.add(techFee);
        data.add(techFeeActive);
        return data;
    }

    private List<Double> getCancelValueForIntercity(Booking booking, Fleet fleet, String status, String requestId) {
        double policyAmount = 0.0;
        String payToDrv = "commission"; // default type
        double driverGetPolicy = 0.0;
        boolean enableTax = false;
        boolean enableTechFee = false;
        String zoneId = "";
        if (booking.request != null && booking.request.pickup != null) {
            if (booking.request.pickup.zoneId != null && booking.request.pickup.zoneId.isEmpty()) {
                zoneId = booking.request.pickup.zoneId;
            } else if (booking.request.pickup.geo != null) {
                List<Double> pickup = booking.request.pickup.geo;
                Zone zone = mongoDao.findByFleetIdAndGeo(booking.fleetId, pickup);
                if (zone != null) {
                    zoneId = zone._id.toString();
                }
            }
        }
        // check book intercity, get cancel policy from intercity route
        boolean isNoShow = true;
        if (booking.intercityInfo != null && booking.intercityInfo.routeId != null
                && !booking.intercityInfo.routeId.isEmpty()) {
            IntercityRoute ir = mongoDao.getIntercityRouteRouteById(booking.intercityInfo.routeId);
            if (ir != null) {
                CancelPolicy cancelPolicy = null;
                com.qupworld.paymentgateway.models.mongo.collections.intercityRoute.NoShowPolicy noShowPolicy = null;
                if (booking.intercityInfo.routeNumber == 1 && ir.routeOneSetting != null) {
                    cancelPolicy = ir.routeOneSetting.cancellationPolicy;
                    noShowPolicy = ir.routeOneSetting.noShowPolicy;
                } else if (ir.routeTwoEnable && booking.intercityInfo.routeNumber == 2 && ir.routeTwoSetting != null) {
                    cancelPolicy = ir.routeTwoSetting.cancellationPolicy;
                    noShowPolicy = ir.routeTwoSetting.noShowPolicy;
                }

                if (booking.status.equals(KeysUtil.canceled) || status.equals(KeysUtil.canceled)) {
                    isNoShow = false;
                    if (cancelPolicy != null) {
                        for (AmountByCurrency amountByCurrency : cancelPolicy.valueByCurrencies) {
                            if (amountByCurrency.currencyISO.equals(booking.currencyISO)) {
                                policyAmount = amountByCurrency.value;
                            }
                        }
                        payToDrv = cancelPolicy.payToDrv != null ? cancelPolicy.payToDrv : "";
                        for (AmountByCurrency amountByCurrency : cancelPolicy.drvGetAmtByCurrencies) {
                            if (amountByCurrency.currencyISO.equals(booking.currencyISO)) {
                                driverGetPolicy = amountByCurrency.value;
                            }
                        }
                        enableTax = cancelPolicy.enableTax;
                        enableTechFee = cancelPolicy.enableTechFee;
                    }

                }
                if (isNoShow) {
                    boolean isNoShowActive = false;
                    FleetFare fleetFare = mongoDao.getFleetFare(booking.fleetId);
                    if (fleetFare != null && fleetFare.noShow != null)
                        isNoShowActive = fleetFare.noShow.isActive;

                    if (isNoShowActive && noShowPolicy != null && (booking.status.equals(KeysUtil.PAYMENT_NOSHOW)
                            || status.equals(KeysUtil.PAYMENT_NOSHOW))) {
                        for (AmountByCurrency amountByCurrency : noShowPolicy.valueByCurrencies) {
                            if (amountByCurrency.currencyISO.equals(booking.currencyISO)) {
                                policyAmount = amountByCurrency.value;
                            }
                        }
                        payToDrv = noShowPolicy.payToDrv != null ? noShowPolicy.payToDrv : "";
                        for (AmountByCurrency amountByCurrency : noShowPolicy.drvGetAmtByCurrencies) {
                            if (amountByCurrency.currencyISO.equals(booking.currencyISO)) {
                                driverGetPolicy = amountByCurrency.value;
                            }
                        }
                        enableTax = noShowPolicy.enableTax;
                        enableTechFee = noShowPolicy.enableTechFee;
                    }
                }
            }
        }
        double taxSetting = 0.0;
        if (enableTax) {
            ServiceFee sF = mongoDao.getServiceFeeByFleetIdAndZoneId(fleet.fleetId, zoneId);
            FleetFare fleetFare = mongoDao.getFleetFare(booking.fleetId);
            String applyType = fleetFare.applyType != null ? fleetFare.applyType : "all";
            if (sF != null && fleet.generalSetting.allowCongfigSettingForEachZone && !applyType.equals("all")) {
                taxSetting = sF.tax;
            } else if (fleetFare.applyType == null || fleetFare.applyType.equals("all")
                    || !fleet.generalSetting.allowCongfigSettingForEachZone) {
                taxSetting = fleetFare.tax;
            }
        }
        double techFee = 0.0;
        if (enableTechFee) {
            List<Object> getTechFee = getTechFeeValueByZone(fleet, booking.bookFrom, booking.currencyISO, zoneId);
            techFee = Double.parseDouble(getTechFee.get(0).toString());
        }
        int seat = booking.request.paxNumber;
        if (seat == 0)
            seat = 1;
        policyAmount = policyAmount * seat;
        logger.debug(requestId + " - status: " + status);
        logger.debug(requestId + " - booking.status: " + booking.status);
        logger.debug(requestId + " - payToDrv: " + payToDrv);
        logger.debug(requestId + " - policyAmount: " + policyAmount);
        logger.debug(requestId + " - driverGetPolicy: " + driverGetPolicy);
        logger.debug(requestId + " - enableTax: " + enableTax);
        logger.debug(requestId + " - taxSetting: " + taxSetting);
        logger.debug(requestId + " - enableTechFee: " + enableTechFee);
        logger.debug(requestId + " - techFee: " + techFee);
        logger.debug(requestId + " - seat: " + seat);
        List<Double> listPolicy = new ArrayList<>();
        double tax = policyAmount * taxSetting * 0.01 * seat;
        double cancelPolicyAmount = policyAmount + tax + techFee;
        // if noShow intercity, charge policy amount only
        if (isNoShow)
            cancelPolicyAmount = policyAmount;
        double driverGetAmount;
        double fleetCommission = 0.0;
        if (payToDrv.equals("fixAmount")) {
            driverGetAmount = driverGetPolicy;
            fleetCommission = policyAmount - driverGetAmount;
        } else {
            // payToDrv = commission
            double commission = 0.0;
            String userId = booking.drvInfo != null && booking.drvInfo.userId != null ? booking.drvInfo.userId : "";
            Account driver = mongoDao.getAccount(userId);
            if (driver != null && driver.driverInfo != null) {
                String commissionType = "";
                if (driver.driverInfo.commissionType != null && !driver.driverInfo.commissionType.equals(""))
                    commissionType = driver.driverInfo.commissionType;
                if (driver.driverInfo.commissionByCurrencies != null
                        && !driver.driverInfo.commissionByCurrencies.isEmpty()) {
                    // if type = percent, there is only ONE object on commissionByCurrencies
                    if (commissionType.equalsIgnoreCase("percent")) {
                        AmountByCurrency amountByCurrency = driver.driverInfo.commissionByCurrencies.get(0);
                        commission = amountByCurrency.commissionValue;
                    } else {
                        for (AmountByCurrency amountByCurrency : driver.driverInfo.commissionByCurrencies) {
                            if (amountByCurrency.currencyISO.equalsIgnoreCase(booking.currencyISO)) {
                                commission = amountByCurrency.commissionValue;
                                break;
                            }
                        }
                    }
                }
            }
            fleetCommission = policyAmount * commission * 0.01;
            driverGetAmount = policyAmount - fleetCommission;
        }

        logger.debug(requestId + " - cancelPolicyAmount: " + cancelPolicyAmount);
        logger.debug(requestId + " - driverGetAmount: " + driverGetAmount);
        logger.debug(requestId + " - tax: " + tax);
        logger.debug(requestId + " - techFee: " + techFee);
        logger.debug(requestId + " - fleetCommission: " + fleetCommission);
        listPolicy.add(cancelPolicyAmount);
        listPolicy.add(driverGetAmount);
        listPolicy.add(tax);
        listPolicy.add(techFee);
        listPolicy.add(fleetCommission);
        return listPolicy;
    }

    private List<Double> getCancelValue(Booking booking, Fleet fleet, String status, String cancelFrom,
            String requestId) {
        String fareHourlyId = "";
        String fareFlatId = "";
        String fareNormalId = "";
        String vehicleTypeRequest = "";
        String zipCodeFrom = "";
        String zipCodeTo = "";
        String zoneId = "";
        String corporateId = "";
        double estimateFare = 0;
        int requestMethod = booking.request.paymentType;
        List<Double> pickup = new ArrayList<>();
        List<Double> destination = new ArrayList<>();
        if (booking.request != null && booking.request.pickup != null) {
            zipCodeFrom = booking.request.pickup.zipCode != null ? booking.request.pickup.zipCode : "";
            if (booking.request.pickup.zoneId != null && booking.request.pickup.zoneId.isEmpty()) {
                zoneId = booking.request.pickup.zoneId;
            } else if (booking.request.pickup.geo != null) {
                pickup = booking.request.pickup.geo;
                Zone zone = mongoDao.findByFleetIdAndGeo(booking.fleetId, pickup);
                if (zone != null) {
                    zoneId = zone._id.toString();
                }
            }
        }
        if (booking.request != null && booking.request.destination != null) {
            zipCodeTo = booking.request.destination.zipCode != null ? booking.request.destination.zipCode : "";
            if (booking.request.destination.geo != null)
                destination = booking.request.destination.geo;
        }
        if (booking.request != null && booking.request.estimate != null && booking.request.estimate.fare != null) {
            estimateFare = booking.request.estimate.fare.etaFare;
        }

        if (booking.request != null && booking.request.extraDestination != null
                && !booking.request.extraDestination.isEmpty()) {
            zipCodeTo = booking.request.extraDestination.get(0).zipCode != null
                    ? booking.request.extraDestination.get(0).zipCode
                    : "";
            if (booking.request.extraDestination.get(0).geo != null)
                destination = booking.request.extraDestination.get(0).geo;
        }
        if (booking.pricingType != 0) {
            if (booking.drvInfo != null)
                vehicleTypeRequest = booking.drvInfo.vehicleType != null ? booking.drvInfo.vehicleType : "";
        } else {
            if (booking.request != null)
                vehicleTypeRequest = booking.request.vehicleTypeRequest != null ? booking.request.vehicleTypeRequest
                        : "";
        }
        Account account = mongoDao.getAccount(booking.psgInfo.userId);
        if (booking.corporateInfo != null && booking.corporateInfo.corporateId != null && account != null &&
                account.corporateInfo != null && account.corporateInfo.status == 2)
            corporateId = booking.corporateInfo.corporateId;

        boolean isNoShowActive = false;
        FleetFare fleetFare = mongoDao.getFleetFare(booking.fleetId);
        if (fleetFare != null && fleetFare.noShow != null)
            isNoShowActive = fleetFare.noShow.isActive;

        String policyType = "amount";
        double policyAmount = 0.0;
        double cancelAmount = 0.0;
        double driverGetPolicy = 0.0;
        boolean enableTax = false;
        boolean enableTechFee = false;
        String payToDrv = "fixAmount"; // default type
        String payToDrvCash = "fixAmount"; // default type
        double payToDrvCashPolicy = 0.0;
        boolean isNoShow = false;
        // check rate by corporate
        boolean rateByCorp = false;
        if (!corporateId.isEmpty()) {
            Corporate corporate = mongoDao.getCorporate(corporateId);
            if (corporate != null && corporate.pricing != null && corporate.pricing.differentRate != null
                    && corporate.pricing.differentRate)
                rateByCorp = corporate.pricing.differentRate;
        }

        VehicleType vehicleType = mongoDao.getVehicleTypeByFleetAndName(booking.fleetId, vehicleTypeRequest);
        if (vehicleType != null && !zoneId.isEmpty()) {
            if (rateByCorp) {
                if (vehicleType.corpRates != null && !vehicleType.corpRates.isEmpty()) {
                    for (CorpRate corpRate : vehicleType.corpRates) {
                        if (corpRate.corporateId.equals(corporateId) && corpRate.rates != null && !corpRate.rates.isEmpty()) {
                            for (Rate rate : corpRate.rates) {
                                if (rate.rateType.equals(KeysUtil.REGULAR) && rate.zoneId.equals(zoneId))
                                    fareNormalId = rate.rateId;
                                if (rate.rateType.equals(KeysUtil.FLAT))
                                    fareFlatId = rate.rateId;
                                if (rate.rateType.equals(KeysUtil.HOURLY) && rate.zoneId.equals(zoneId))
                                    fareHourlyId = rate.rateId;
                            }
                            break;
                        }
                    }
                }
            } else {
                if (vehicleType.rates != null && !vehicleType.rates.isEmpty()) {
                    for (Rate rate : vehicleType.rates) {
                        if (rate.rateType.equals(KeysUtil.REGULAR) && rate.zoneId.equals(zoneId))
                            fareNormalId = rate.rateId;
                        if (rate.rateType.equals(KeysUtil.FLAT))
                            fareFlatId = rate.rateId;
                        if (rate.rateType.equals(KeysUtil.HOURLY) && rate.zoneId.equals(zoneId))
                            fareHourlyId = rate.rateId;
                    }
                }
            }
        }
        logger.debug(requestId + " - fareNormalId: " + fareNormalId);
        logger.debug(requestId + " - fareFlatId: " + fareFlatId);
        logger.debug(requestId + " - fareHourlyId: " + fareHourlyId);
        String canceller = booking.cancelInfo != null && booking.cancelInfo.canceller != null
                ? booking.cancelInfo.canceller
                : "";
        logger.debug(requestId + " - booking was canceled by " + canceller);
        logger.debug(requestId + " - booking was canceled from " + cancelFrom);
        if (booking.request != null && (booking.request.typeRate == 1 || booking.request.type == 3)) {
            FareHourly fareHourly = mongoDao.getFareHourlyById(fareHourlyId);
            if (booking.status.equals(KeysUtil.canceled) || status.equals(KeysUtil.canceled)) {
                if (canceller.equals("driver") || cancelFrom.equals("driver")) {
                    if (booking.reservation) {
                        logger.debug(requestId + " - cancel booking hourly rate reservation");
                        if (fareHourly != null && fareHourly.driverCancelPolicy != null &&
                                fareHourly.driverCancelPolicy.inAdvance != null &&
                                fareHourly.driverCancelPolicy.inAdvance.isActive) {
                            policyType = fareHourly.driverCancelPolicy.inAdvance.type;
                            policyAmount = fareHourly.driverCancelPolicy.inAdvance.value;
                        }
                    } else {
                        logger.debug(requestId + " - cancel booking hourly rate onDemand");
                        if (fareHourly != null && fareHourly.driverCancelPolicy != null &&
                                fareHourly.driverCancelPolicy.onDemand != null &&
                                fareHourly.driverCancelPolicy.onDemand.isActive) {
                            policyType = fareHourly.driverCancelPolicy.onDemand.type;
                            policyAmount = fareHourly.driverCancelPolicy.onDemand.value;
                        }
                    }
                } else {
                    if (booking.reservation) {
                        logger.debug(requestId + " - cancel booking hourly rate reservation");
                        if (fareHourly != null && fareHourly.cancellationPolicy != null &&
                                fareHourly.cancellationPolicy.inAdvance != null &&
                                fareHourly.cancellationPolicy.inAdvance.isActive) {
                            payToDrv = fareHourly.cancellationPolicy.inAdvance.payToDrv != null
                                    ? fareHourly.cancellationPolicy.inAdvance.payToDrv
                                    : "commission";
                            payToDrvCash = fareHourly.cancellationPolicy.inAdvance.payDrvCash != null
                                    && fareHourly.cancellationPolicy.inAdvance.payDrvCash.type != null
                                            ? fareHourly.cancellationPolicy.inAdvance.payDrvCash.type
                                            : "commission";
                            payToDrvCashPolicy = fareHourly.cancellationPolicy.inAdvance.payDrvCash != null
                                    ? fareHourly.cancellationPolicy.inAdvance.payDrvCash.value
                                    : 0.0;
                            enableTax = fareHourly.cancellationPolicy.inAdvance.enableTax;
                            enableTechFee = fareHourly.cancellationPolicy.inAdvance.enableTechFee;
                            policyType = fareHourly.cancellationPolicy.inAdvance.type;
                            if (fareHourly.cancellationPolicy.inAdvance.valueByCurrencies != null) {
                                for (AmountByCurrency amountByCurrency : fareHourly.cancellationPolicy.inAdvance.valueByCurrencies) {
                                    if (amountByCurrency.currencyISO.equals(booking.currencyISO)) {
                                        policyAmount = amountByCurrency.value;
                                    }
                                }
                            }
                            if (fareHourly.cancellationPolicy.inAdvance.drvGetAmtByCurrencies != null) {
                                for (AmountByCurrency amountByCurrency : fareHourly.cancellationPolicy.inAdvance.drvGetAmtByCurrencies) {
                                    if (amountByCurrency.currencyISO.equals(booking.currencyISO)) {
                                        driverGetPolicy = amountByCurrency.value;
                                    }
                                }
                            }
                        }
                    } else {
                        logger.debug(requestId + " - cancel booking hourly rate onDemand");
                        if (fareHourly != null && fareHourly.cancellationPolicy != null &&
                                fareHourly.cancellationPolicy.onDemand != null &&
                                fareHourly.cancellationPolicy.onDemand.isActive) {
                            payToDrv = fareHourly.cancellationPolicy.onDemand.payToDrv != null
                                    ? fareHourly.cancellationPolicy.onDemand.payToDrv
                                    : "commission";
                            payToDrvCash = fareHourly.cancellationPolicy.onDemand.payDrvCash != null
                                    && fareHourly.cancellationPolicy.onDemand.payDrvCash.type != null
                                            ? fareHourly.cancellationPolicy.onDemand.payDrvCash.type
                                            : "commission";
                            payToDrvCashPolicy = fareHourly.cancellationPolicy.onDemand.payDrvCash != null
                                    ? fareHourly.cancellationPolicy.onDemand.payDrvCash.value
                                    : 0.0;
                            enableTax = fareHourly.cancellationPolicy.onDemand.enableTax;
                            enableTechFee = fareHourly.cancellationPolicy.onDemand.enableTechFee;
                            policyType = fareHourly.cancellationPolicy.onDemand.type;
                            if (fareHourly.cancellationPolicy.onDemand.valueByCurrencies != null) {
                                for (AmountByCurrency amountByCurrency : fareHourly.cancellationPolicy.onDemand.valueByCurrencies) {
                                    if (amountByCurrency.currencyISO.equals(booking.currencyISO)) {
                                        policyAmount = amountByCurrency.value;
                                    }
                                }
                            }
                            if (fareHourly.cancellationPolicy.onDemand.drvGetAmtByCurrencies != null) {
                                for (AmountByCurrency amountByCurrency : fareHourly.cancellationPolicy.onDemand.drvGetAmtByCurrencies) {
                                    if (amountByCurrency.currencyISO.equals(booking.currencyISO)) {
                                        driverGetPolicy = amountByCurrency.value;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                logger.debug(requestId + " - noShow booking hourly rate");
                if (isNoShowActive && fareHourly != null && fareHourly.noShow != null) {
                    payToDrv = fareHourly.noShow.payToDrv != null ? fareHourly.noShow.payToDrv : "commission";
                    payToDrvCash = fareHourly.noShow.payDrvCash != null && fareHourly.noShow.payDrvCash.type != null
                            ? fareHourly.noShow.payDrvCash.type
                            : "commission";
                    payToDrvCashPolicy = fareHourly.noShow.payDrvCash != null ? fareHourly.noShow.payDrvCash.value
                            : 0.0;
                    enableTax = fareHourly.noShow.enableTax;
                    enableTechFee = fareHourly.noShow.enableTechFee;
                    policyType = fareHourly.noShow.type;
                    if (fareHourly.noShow.valueByCurrencies != null) {
                        for (AmountByCurrency amountByCurrency : fareHourly.noShow.valueByCurrencies) {
                            if (amountByCurrency.currencyISO.equals(booking.currencyISO)) {
                                policyAmount = amountByCurrency.value;
                            }
                        }
                    }
                    if (fareHourly.noShow.drvGetAmtByCurrencies != null) {
                        for (AmountByCurrency amountByCurrency : fareHourly.noShow.drvGetAmtByCurrencies) {
                            if (amountByCurrency.currencyISO.equals(booking.currencyISO)) {
                                driverGetPolicy = amountByCurrency.value;
                            }
                        }
                    }
                }
            }
        } else {
            boolean checkFlatFare = false;
            FareFlat fareFlat = mongoDao.getFareFlatById(fareFlatId);
            if (fareFlat != null) {
                logger.debug(requestId + " - " + "fareFlat: " + fareFlat._id + " - pickup: " + pickup
                        + " - destination: " + destination + " - from: " + zipCodeFrom + " - to: " + zipCodeTo);
                FlatRoutes flatRoutes = getFareByFlatRoutes(fareFlatId, pickup, destination, zipCodeFrom, zipCodeTo);
                FlatRoutes flatRoutesReturn = getFareByFlatRoutesReturn(fareFlatId, pickup, destination, zipCodeFrom,
                        zipCodeTo);
                if (flatRoutes != null) {
                    logger.debug(requestId + " - " + "flatRoutesId: " + flatRoutes._id);
                    checkFlatFare = true;
                } else if (flatRoutesReturn != null) {
                    logger.debug(requestId + " - " + "flatRoutesId: " + flatRoutesReturn._id);
                    checkFlatFare = true;
                }
            }
            logger.debug(requestId + " - " + "checkFlatFare: " + checkFlatFare);
            if (checkFlatFare) {
                if (booking.status.equals(KeysUtil.canceled) || status.equals(KeysUtil.canceled)) {
                    if (canceller.equals("driver") || cancelFrom.equals("driver")) {
                        if (booking.reservation) {
                            logger.debug(requestId + " - cancel booking flat rate reservation");
                            if (fareFlat.driverCancelPolicy != null &&
                                    fareFlat.driverCancelPolicy.inAdvance != null &&
                                    fareFlat.driverCancelPolicy.inAdvance.isActive) {
                                policyType = fareFlat.driverCancelPolicy.inAdvance.type;
                                policyAmount = fareFlat.driverCancelPolicy.inAdvance.value;
                            }
                        } else {
                            logger.debug(requestId + " - cancel booking flat rate onDemand");
                            if (fareFlat.driverCancelPolicy != null &&
                                    fareFlat.driverCancelPolicy.onDemand != null &&
                                    fareFlat.driverCancelPolicy.onDemand.isActive) {
                                policyType = fareFlat.driverCancelPolicy.onDemand.type;
                                policyAmount = fareFlat.driverCancelPolicy.onDemand.value;
                            }
                        }
                    } else {
                        if (booking.reservation) {
                            logger.debug(requestId + " - cancel booking flat rate reservation");
                            if (fareFlat.cancellationPolicy != null &&
                                    fareFlat.cancellationPolicy.inAdvance != null &&
                                    fareFlat.cancellationPolicy.inAdvance.isActive) {
                                payToDrv = fareFlat.cancellationPolicy.inAdvance.payToDrv != null
                                        ? fareFlat.cancellationPolicy.inAdvance.payToDrv
                                        : "commission";
                                payToDrvCash = fareFlat.cancellationPolicy.inAdvance.payDrvCash != null
                                        && fareFlat.cancellationPolicy.inAdvance.payDrvCash.type != null
                                                ? fareFlat.cancellationPolicy.inAdvance.payDrvCash.type
                                                : "commission";
                                payToDrvCashPolicy = fareFlat.cancellationPolicy.inAdvance.payDrvCash != null
                                        ? fareFlat.cancellationPolicy.inAdvance.payDrvCash.value
                                        : 0.0;
                                enableTax = fareFlat.cancellationPolicy.inAdvance.enableTax;
                                enableTechFee = fareFlat.cancellationPolicy.inAdvance.enableTechFee;
                                policyType = fareFlat.cancellationPolicy.inAdvance.type;
                                if (fareFlat.cancellationPolicy.inAdvance.valueByCurrencies != null) {
                                    for (AmountByCurrency amountByCurrency : fareFlat.cancellationPolicy.inAdvance.valueByCurrencies) {
                                        if (amountByCurrency.currencyISO.equals(booking.currencyISO)) {
                                            policyAmount = amountByCurrency.value;
                                        }
                                    }
                                }
                                if (fareFlat.cancellationPolicy.inAdvance.drvGetAmtByCurrencies != null) {
                                    for (AmountByCurrency amountByCurrency : fareFlat.cancellationPolicy.inAdvance.drvGetAmtByCurrencies) {
                                        if (amountByCurrency.currencyISO.equals(booking.currencyISO)) {
                                            driverGetPolicy = amountByCurrency.value;
                                        }
                                    }
                                }
                            }
                        } else {
                            logger.debug(requestId + " - cancel booking flat rate onDemand");
                            if (fareFlat.cancellationPolicy != null &&
                                    fareFlat.cancellationPolicy.onDemand != null &&
                                    fareFlat.cancellationPolicy.onDemand.isActive) {
                                payToDrv = fareFlat.cancellationPolicy.onDemand.payToDrv != null
                                        ? fareFlat.cancellationPolicy.onDemand.payToDrv
                                        : "commission";
                                payToDrvCash = fareFlat.cancellationPolicy.onDemand.payDrvCash != null
                                        && fareFlat.cancellationPolicy.onDemand.payDrvCash.type != null
                                                ? fareFlat.cancellationPolicy.onDemand.payDrvCash.type
                                                : "commission";
                                payToDrvCashPolicy = fareFlat.cancellationPolicy.onDemand.payDrvCash != null
                                        ? fareFlat.cancellationPolicy.onDemand.payDrvCash.value
                                        : 0.0;
                                enableTax = fareFlat.cancellationPolicy.onDemand.enableTax;
                                enableTechFee = fareFlat.cancellationPolicy.onDemand.enableTechFee;
                                policyType = fareFlat.cancellationPolicy.onDemand.type;
                                if (fareFlat.cancellationPolicy.onDemand.valueByCurrencies != null) {
                                    for (AmountByCurrency amountByCurrency : fareFlat.cancellationPolicy.onDemand.valueByCurrencies) {
                                        if (amountByCurrency.currencyISO.equals(booking.currencyISO)) {
                                            policyAmount = amountByCurrency.value;
                                        }
                                    }
                                }
                                if (fareFlat.cancellationPolicy.onDemand.drvGetAmtByCurrencies != null) {
                                    for (AmountByCurrency amountByCurrency : fareFlat.cancellationPolicy.onDemand.drvGetAmtByCurrencies) {
                                        if (amountByCurrency.currencyISO.equals(booking.currencyISO)) {
                                            driverGetPolicy = amountByCurrency.value;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    logger.debug(requestId + " - noShow booking flat rate");
                    if (isNoShowActive && fareFlat != null && fareFlat.noShow != null) {
                        payToDrv = fareFlat.noShow.payToDrv != null ? fareFlat.noShow.payToDrv : "commission";
                        payToDrvCash = fareFlat.noShow.payDrvCash != null && fareFlat.noShow.payDrvCash.type != null
                                ? fareFlat.noShow.payDrvCash.type
                                : "commission";
                        payToDrvCashPolicy = fareFlat.noShow.payDrvCash != null ? fareFlat.noShow.payDrvCash.value
                                : 0.0;
                        enableTax = fareFlat.noShow.enableTax;
                        enableTechFee = fareFlat.noShow.enableTechFee;
                        policyType = fareFlat.noShow.type;
                        if (fareFlat.noShow.valueByCurrencies != null) {
                            for (AmountByCurrency amountByCurrency : fareFlat.noShow.valueByCurrencies) {
                                if (amountByCurrency.currencyISO.equals(booking.currencyISO)) {
                                    policyAmount = amountByCurrency.value;
                                }
                            }
                        }
                        if (fareFlat.noShow.drvGetAmtByCurrencies != null) {
                            for (AmountByCurrency amountByCurrency : fareFlat.noShow.drvGetAmtByCurrencies) {
                                if (amountByCurrency.currencyISO.equals(booking.currencyISO)) {
                                    driverGetPolicy = amountByCurrency.value;
                                }
                            }
                        }
                    }
                }
            } else {
                FareNormal fareNormal = mongoDao.getFareNormalById(fareNormalId);
                if (fareNormal != null) {
                    if (booking.status.equals(KeysUtil.canceled) || status.equals(KeysUtil.canceled)) {
                        if (canceller.equals("driver") || cancelFrom.equals("driver")) {
                            if (booking.reservation) {
                                logger.debug(requestId + " - cancel booking normal rate reservation");
                                if (fareNormal.driverCancelPolicy != null &&
                                        fareNormal.driverCancelPolicy.inAdvance != null &&
                                        fareNormal.driverCancelPolicy.inAdvance.isActive) {
                                    policyType = fareNormal.driverCancelPolicy.inAdvance.type;
                                    policyAmount = fareNormal.driverCancelPolicy.inAdvance.value;
                                }
                            } else {
                                logger.debug(requestId + " - cancel booking normal rate onDemand");
                                if (fareNormal.driverCancelPolicy != null &&
                                        fareNormal.driverCancelPolicy.onDemand != null &&
                                        fareNormal.driverCancelPolicy.onDemand.isActive) {
                                    policyType = fareNormal.driverCancelPolicy.onDemand.type;
                                    policyAmount = fareNormal.driverCancelPolicy.onDemand.value;
                                }
                            }
                        } else {
                            if (booking.reservation) {
                                logger.debug(requestId + " - cancel booking normal rate reservation");
                                if (fareNormal.cancellationPolicy != null &&
                                        fareNormal.cancellationPolicy.inAdvance != null &&
                                        fareNormal.cancellationPolicy.inAdvance.isActive) {
                                    payToDrv = fareNormal.cancellationPolicy.inAdvance.payToDrv != null
                                            ? fareNormal.cancellationPolicy.inAdvance.payToDrv
                                            : "commission";
                                    payToDrvCash = fareNormal.cancellationPolicy.inAdvance.payDrvCash != null
                                            && fareNormal.cancellationPolicy.inAdvance.payDrvCash.type != null
                                                    ? fareNormal.cancellationPolicy.inAdvance.payDrvCash.type
                                                    : "commission";
                                    payToDrvCashPolicy = fareNormal.cancellationPolicy.inAdvance.payDrvCash != null
                                            ? fareNormal.cancellationPolicy.inAdvance.payDrvCash.value
                                            : 0.0;
                                    enableTax = fareNormal.cancellationPolicy.inAdvance.enableTax;
                                    enableTechFee = fareNormal.cancellationPolicy.inAdvance.enableTechFee;
                                    policyType = fareNormal.cancellationPolicy.inAdvance.type;
                                    if (fareNormal.cancellationPolicy.inAdvance.valueByCurrencies != null) {
                                        for (AmountByCurrency amountByCurrency : fareNormal.cancellationPolicy.inAdvance.valueByCurrencies) {
                                            if (amountByCurrency.currencyISO.equals(booking.currencyISO)) {
                                                policyAmount = amountByCurrency.value;
                                            }
                                        }
                                    }
                                    if (fareNormal.cancellationPolicy.inAdvance.drvGetAmtByCurrencies != null) {
                                        for (AmountByCurrency amountByCurrency : fareNormal.cancellationPolicy.inAdvance.drvGetAmtByCurrencies) {
                                            if (amountByCurrency.currencyISO.equals(booking.currencyISO)) {
                                                driverGetPolicy = amountByCurrency.value;
                                            }
                                        }
                                    }
                                }
                            } else {
                                logger.debug(requestId + " - cancel booking normal rate onDemand");
                                if (fareNormal.cancellationPolicy != null &&
                                        fareNormal.cancellationPolicy.onDemand != null &&
                                        fareNormal.cancellationPolicy.onDemand.isActive) {
                                    payToDrv = fareNormal.cancellationPolicy.onDemand.payToDrv != null
                                            ? fareNormal.cancellationPolicy.onDemand.payToDrv
                                            : "commission";
                                    payToDrvCash = fareNormal.cancellationPolicy.onDemand.payDrvCash != null
                                            && fareNormal.cancellationPolicy.onDemand.payDrvCash.type != null
                                                    ? fareNormal.cancellationPolicy.onDemand.payDrvCash.type
                                                    : "commission";
                                    payToDrvCashPolicy = fareNormal.cancellationPolicy.onDemand.payDrvCash != null
                                            ? fareNormal.cancellationPolicy.onDemand.payDrvCash.value
                                            : 0.0;
                                    enableTax = fareNormal.cancellationPolicy.onDemand.enableTax;
                                    enableTechFee = fareNormal.cancellationPolicy.onDemand.enableTechFee;
                                    policyType = fareNormal.cancellationPolicy.onDemand.type;
                                    if (fareNormal.cancellationPolicy.onDemand.valueByCurrencies != null) {
                                        for (AmountByCurrency amountByCurrency : fareNormal.cancellationPolicy.onDemand.valueByCurrencies) {
                                            if (amountByCurrency.currencyISO.equals(booking.currencyISO)) {
                                                policyAmount = amountByCurrency.value;
                                            }
                                        }
                                    }
                                    if (fareNormal.cancellationPolicy.onDemand.drvGetAmtByCurrencies != null) {
                                        for (AmountByCurrency amountByCurrency : fareNormal.cancellationPolicy.onDemand.drvGetAmtByCurrencies) {
                                            if (amountByCurrency.currencyISO.equals(booking.currencyISO)) {
                                                driverGetPolicy = amountByCurrency.value;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        logger.debug(requestId + " - noShow booking normal rate");
                        if (isNoShowActive && fareNormal != null && fareNormal.noShow != null) {
                            payToDrv = fareNormal.noShow.payToDrv != null ? fareNormal.noShow.payToDrv : "commission";
                            payToDrvCash = fareNormal.noShow.payDrvCash != null
                                    && fareNormal.noShow.payDrvCash.type != null ? fareNormal.noShow.payDrvCash.type
                                            : "commission";
                            payToDrvCashPolicy = fareNormal.noShow.payDrvCash != null
                                    ? fareNormal.noShow.payDrvCash.value
                                    : 0.0;
                            enableTax = fareNormal.noShow.enableTax;
                            enableTechFee = fareNormal.noShow.enableTechFee;
                            policyType = fareNormal.noShow.type;
                            if (fareNormal.noShow.valueByCurrencies != null) {
                                for (AmountByCurrency amountByCurrency : fareNormal.noShow.valueByCurrencies) {
                                    if (amountByCurrency.currencyISO.equals(booking.currencyISO)) {
                                        policyAmount = amountByCurrency.value;
                                    }
                                }
                            }
                            if (fareNormal.noShow.drvGetAmtByCurrencies != null) {
                                for (AmountByCurrency amountByCurrency : fareNormal.noShow.drvGetAmtByCurrencies) {
                                    if (amountByCurrency.currencyISO.equals(booking.currencyISO)) {
                                        driverGetPolicy = amountByCurrency.value;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        double taxSetting = 0.0;
        if (enableTax) {
            ServiceFee sF = mongoDao.getServiceFeeByFleetIdAndZoneId(fleet.fleetId, zoneId);
            String applyType = fleetFare.applyType != null ? fleetFare.applyType : "all";
            if (sF != null && fleet.generalSetting.allowCongfigSettingForEachZone && !applyType.equals("all")) {
                taxSetting = sF.tax;
            }
            if (fleetFare.applyType == null || fleetFare.applyType.equals("all")
                    || !fleet.generalSetting.allowCongfigSettingForEachZone) {
                taxSetting = fleetFare.tax;
            }
        }
        double techFee = 0.0;
        if (enableTechFee) {
            List<Object> getTechFee = getTechFeeValueByZone(fleet, booking.bookFrom, booking.currencyISO, zoneId);
            techFee = Double.parseDouble(getTechFee.get(0).toString());
        }
        if (policyType != null && !policyType.equals("amount")) {
            cancelAmount = policyAmount * estimateFare * 0.01;
            policyAmount = policyAmount * estimateFare * 0.01;
        } else {
            cancelAmount = policyAmount;
        }
        logger.debug(requestId + " - status: " + status);
        logger.debug(requestId + " - booking.status: " + booking.status);
        logger.debug(requestId + " - payToDrv: " + payToDrv);
        logger.debug(requestId + " - payToDrvCash: " + payToDrvCash);
        logger.debug(requestId + " - payToDrvCashPolicy: " + payToDrvCashPolicy);
        logger.debug(requestId + " - policyAmount: " + policyAmount);
        logger.debug(requestId + " - cancelAmount: " + cancelAmount);
        logger.debug(requestId + " - driverGetPolicy: " + driverGetPolicy);
        logger.debug(requestId + " - enableTax: " + enableTax);
        logger.debug(requestId + " - taxSetting: " + taxSetting);
        logger.debug(requestId + " - enableTechFee: " + enableTechFee);
        logger.debug(requestId + " - techFee: " + techFee);
        List<Double> listPolicy = new ArrayList<>();
        double tax = taxSetting * cancelAmount * 0.01;
        double cancelPolicyAmount = cancelAmount + tax + techFee;
        if (booking.intercity) {
            // if noShow intercity, charge policy amount only
            if (isNoShow)
                cancelPolicyAmount = cancelAmount;
        }
        double driverGetAmount;
        double fleetCommission;
        if (requestMethod == 1) {
            driverGetAmount = payToDrvCashPolicy;
            if (payToDrvCash.equals("fixAmount")) {
                fleetCommission = cancelAmount - driverGetAmount;
            } else {
                // payToDrvCash = commission
                double commission = 0.0;
                String commissionType = "";
                String userId = booking.drvInfo != null && booking.drvInfo.userId != null ? booking.drvInfo.userId : "";
                Account driver = mongoDao.getAccount(userId);
                if (driver != null && driver.driverInfo != null) {
                    Map<String, Object> commissionData = getFleetCommission(requestId, fleet, driver, booking, zoneId);
                    commission = Double.parseDouble(commissionData.get("commission").toString());
                    commissionType = commissionData.get("commissionType").toString();
                }
                fleetCommission = cancelAmount * (commission * 0.01);
                if (commissionType.equalsIgnoreCase("amount"))
                    fleetCommission = commission;
                logger.debug(requestId + " - commission type: " + commissionType + " - commission: " + commission);
                driverGetAmount = cancelAmount - fleetCommission;
            }
        } else {
            driverGetAmount = driverGetPolicy;
            if (payToDrv.equals("fixAmount")) {
                fleetCommission = cancelAmount - driverGetAmount;
            } else {
                // payToDrv = commission
                double commission = 0.0;
                String commissionType = "";
                String userId = booking.drvInfo != null && booking.drvInfo.userId != null ? booking.drvInfo.userId : "";
                Account driver = mongoDao.getAccount(userId);
                if (driver != null && driver.driverInfo != null) {
                    Map<String, Object> commissionData = getFleetCommission(requestId, fleet, driver, booking, zoneId);
                    commission = Double.parseDouble(commissionData.get("commission").toString());
                    commissionType = commissionData.get("commissionType").toString();
                }
                fleetCommission = cancelAmount * (commission * 0.01);
                if (commissionType.equalsIgnoreCase("amount"))
                    fleetCommission = commission;
                logger.debug(requestId + " - commission type: " + commissionType + " - commission: " + commission);
                driverGetAmount = cancelAmount - fleetCommission;
            }
        }

        logger.debug(requestId + " - cancelPolicyAmount: " + cancelPolicyAmount);
        logger.debug(requestId + " - driverGetAmount: " + driverGetAmount);
        logger.debug(requestId + " - tax: " + tax);
        logger.debug(requestId + " - techFee: " + techFee);
        logger.debug(requestId + " - fleetCommission: " + fleetCommission);
        double payToDriver;
        if (requestMethod == 1) {
            payToDriver = payToDrvCash.equals("fixAmount") ? 1 : 0;
        } else {
            payToDriver = payToDrv.equals("fixAmount") ? 1 : 0;
        }
        listPolicy.add(cancelPolicyAmount);
        listPolicy.add(driverGetAmount);
        listPolicy.add(tax);
        listPolicy.add(techFee);
        listPolicy.add(fleetCommission);
        listPolicy.add(payToDriver);
        listPolicy.add(policyAmount);
        return listPolicy;
    }

    public org.json.JSONObject sendPost(String requestId, String body, String url, String userName, String pw) {
        org.json.JSONObject dataStr = new org.json.JSONObject();
        HttpResponse<JsonNode> jsonResponse = null;
        try {
            jsonResponse = Unirest.post(url)
                    .basicAuth(userName, pw)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .body(body)
                    .asJson();
            org.json.JSONObject objectResponse = jsonResponse.getBody().getObject();
            if (objectResponse != null && objectResponse.get("returnCode") != null
                    && Integer.parseInt(objectResponse.get("returnCode").toString()) == 200) {
                if (objectResponse.has("response") && !objectResponse.isNull("response"))
                    dataStr = objectResponse.getJSONObject("response");
            }
        } catch (UnirestException e) {
            // e.printStackTrace();
            logger.debug(requestId + " - UnirestException: " + e.getMessage());
        } catch (Exception e) {
            // e.printStackTrace();
            logger.debug(requestId + " - Exception: " + e.getMessage());
        }
        return dataStr;
    }

    public String getTimeZoneId(List<Double> location, String city, String requestId){
        String timeZone = "";
        try {
            if (location != null && !location.isEmpty()) {
                JSONObject js = new JSONObject();
                js.put("language", "en");
                String loc = location.get(1) + "," + location.get(0);
                js.put("location", loc);
                if (city != null && !city.isEmpty())
                    js.put("city", city);
                String url = "";
                String username = "";
                String password = "";
                if (ServerConfig.default_timezone != null && !ServerConfig.default_timezone.isEmpty()) {
                    return ServerConfig.default_timezone;
                }
                if (ServerConfig.maps_provider_server != null && !ServerConfig.maps_provider_server.isEmpty()) {
                    url = ServerConfig.maps_provider_server + getTzFromMapsProvider;
                    username = ServerConfig.maps_provider_userName;
                    password = ServerConfig.maps_provider_password;
                } else {
                    // String getTzFromDispatch = "/api/v2/timezone/cache";
                    url = ServerConfig.dispatch_server + getTzFromDispatch;
                    username = ServerConfig.dispatch_userName;
                    password = ServerConfig.dispatch_password;
                }
                logger.debug(requestId + " - body: " + js.toJSONString());
                org.json.JSONObject data = sendPost(requestId, js.toJSONString(), url, username, password);
                if (data != null && data.has("tz") && !data.isNull("tz"))
                    timeZone = data.get("tz").toString();
                logger.debug(requestId + " - " + "timeZone : " + timeZone);
            } else {
                logger.debug(requestId + " - " + "getTimeZone ERROR !!! ");
            }
        } catch (Exception e) {
            logger.debug(requestId + " - " + "getTimeZoneException : " + e.getMessage());
        }
        return timeZone;
    }

    public FareReturn calculatorFare(Fleet fleet, FleetFare fleetFare, Account account, String userId,
            String corporateId,
            String packageRateId, String code, int bookType, int typeRate, double durationTime,
            double distance, int numOfDays, String bookingType, String bookFrom, int meetDriver,
            double tip, List<Double> pickup, List<Double> destination, String zipCodeFrom,
            String zipCodeTo, Date pickupDate, Date desDate, int actualFare, String fareNormalId,
            String fareFlatId, String currencyISO, String zoneId, String requestId,
            List<String> services, String timeZoneId, boolean compareVersion, boolean pegasus,
            boolean recurring, int paymentMethod, String jobType, String vehicleTypeId, String vehicleTypeObjectId, int pricingType, String bookId)
            throws Exception {
        FareReturn fareReturn = new FareReturn();
        // Calculator Basic fare
        double basicFare = 0;
        boolean normalFare = false;
        String route = "";
        String routeId = "";
        boolean reverseRoute = false;
        double minimum = 0;
        double extraDistanceFare = 0;
        double extraDurationFare = 0;
        List<Object> dataFare = getBasicFare(bookType, typeRate, durationTime, distance, numOfDays, bookingType, pickup,
                destination, zipCodeFrom, zipCodeTo, packageRateId, fareNormalId, fareFlatId, currencyISO, requestId);
        logger.debug(requestId + " - dataFare: " + gson.toJson(dataFare));
        if (dataFare == null)
            return null;
        basicFare = Double.parseDouble(dataFare.get(0).toString());
        normalFare = Boolean.parseBoolean(dataFare.get(1).toString());
        route = dataFare.get(2).toString();
        minimum = Double.parseDouble(dataFare.get(3).toString());
        extraDistanceFare = Double.parseDouble(dataFare.get(4).toString());
        extraDurationFare = Double.parseDouble(dataFare.get(5).toString());
        routeId = dataFare.get(6).toString();
        reverseRoute = Boolean.parseBoolean(dataFare.get(7).toString());
        basicFare = CommonUtils.getRoundValue(basicFare);
        String fareType = dataFare.get(8).toString(); // value = regular / flat / hourly
        String rateType = fareType;
        // check round down setting
        double airportFee = 0;
        double meetDriverFee = 0;
        double rushHourFee = 0;
        double techFee = 0;
        double bookingFee = 0;
        double taxFee = 0;
        double tax = 0;
        double tipFee = 0;
        double otherFees = 0;
        double promoValue = 0;
        double serviceFee = 0;
        double serviceTax = 0;
        double fleetCommissionFromFleetServiceFee = 0;
        double driverCommissionFromFleetServiceFee = 0;
        double subTotal = 0;
        boolean min = false;
        boolean bookingFeeActive = false;
        double etaFare = 0;
        double totalWithoutPromo = 0;
        double dynamicSurcharge = 0;
        double surchargeParameter = 0;
        double dynamicFare = 0;
        String dynamicType = "factor";
        boolean shortTrip = false;
        boolean keepMinFee = false;
        if (basicFare >= 0) {
            // get dynamic fare
            if (fleet.generalSetting.dynamicFare && normalFare) {
                DynamicFare dF = mongoDao.getDynamicFare(fleet.fleetId, zoneId, pickup);
                if (dF != null && dF.isActive) {
                    dynamicFare = dF.parameter;
                    dynamicType = dF.type != null ? dF.type : "factor";
                    if (dF.type == null || dF.type.equals("factor")) {
                        basicFare += basicFare * (dF.parameter - 1);
                        minimum += minimum * (dF.parameter - 1);
                    } else {
                        basicFare = basicFare + dF.parameter;
                        minimum = minimum + dF.parameter;
                    }
                    if (basicFare < 0)
                        basicFare = 0;
                    if (minimum < 0)
                        minimum = 0;
                }
            }

            // check discount if book corporate
            basicFare = getModifyPriceOfCorp(corporateId, basicFare, requestId);

            // get basic fare completed, get fare setting
            subTotal = basicFare;

            // get dynamic surcharge
            if (fleet.generalSetting.dynamicSurcharge && bookingType.equals("Now") && !recurring) {
                surchargeParameter = getDynamicSurcharge(fleet.fleetId, zoneId, pickup, destination);
                dynamicSurcharge = basicFare * (surchargeParameter - 1);
                if (dynamicSurcharge < 0)
                    dynamicSurcharge = 0;
            }

            if (fleet.estimateFare != 0 || actualFare == 1) {
                // get tech fee
                List<Object> getTechFee = getTechFeeValueByZone(fleet, bookFrom, currencyISO, zoneId);
                techFee = Double.parseDouble(getTechFee.get(0).toString());

                // get airport fee
                airportFee = getAirportFee(fleet, currencyISO, pickup, destination, bookType == 1,
                        typeRate == 2, distance > 1);

                // get meet driver fee
                if (pricingType == 1 && bookType == 1 && meetDriver == -1) {
                    meetDriver = 0;
                }
                if (fleet.additionalService.fromAirport.meetDriver) {
                    if (meetDriver == 0) {
                        if (fleetFare.meetDriver.onCurbByCurrencies != null) {
                            AmountByCurrency onCurb = fleetFare.meetDriver.onCurbByCurrencies.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                    .findFirst().orElse(null);
                            if (onCurb != null)
                                meetDriverFee = onCurb.value;
                        }
                    }
                    if (meetDriver == 1) {
                        if (fleetFare.meetDriver.meetDrvByCurrencies != null) {
                            AmountByCurrency meetDrv = fleetFare.meetDriver.meetDrvByCurrencies.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                    .findFirst().orElse(null);
                            if (meetDrv != null)
                                meetDriverFee = meetDrv.value;
                        }
                    }
                }

                // check apply zone
                // get general setting apply for each zone
                ServiceFee sF = mongoDao.getServiceFeeByFleetIdAndZoneId(fleet.fleetId, zoneId);
                // check if no need to update rushHourFee: book now, bookId is exist, same
                boolean ignoreuUpdateRushHour = false;
                if (bookingType.equals("Now")) {
                    if (!bookId.isEmpty()) {
                        Booking booking = mongoDao.getBooking(bookId);
                        String vehicleTypeRequest = booking != null && booking.request != null
                                && booking.request.vehicleTypeRequest != null ? booking.request.vehicleTypeRequest : "";
                        logger.debug(requestId + " vehicleTypeRequest: " + vehicleTypeRequest);
                        logger.debug(requestId + " vehicleTypeId: " + vehicleTypeId);
                        if (vehicleTypeRequest.equals(vehicleTypeId)) {
                            Fare fare = booking.request.estimate != null && booking.request.estimate.fare != null
                                    ? booking.request.estimate.fare
                                    : null;
                            if (fare != null) {
                                rushHourFee = fare.rushHourFee;
                                ignoreuUpdateRushHour = true;
                            }
                        }
                    }
                }
                logger.debug(requestId + " - ignore update rushHourFee: " + ignoreuUpdateRushHour);
                logger.debug(requestId + " - rushHourFee from booking: " + rushHourFee);
                String applyType = fleetFare.applyType != null ? fleetFare.applyType : "all";
                if (sF != null && fleet.generalSetting.allowCongfigSettingForEachZone && !applyType.equals("all")) {

                    // get rush hour fee
                    String surchargeTypeRate = getSurchargeTypeRate(jobType, typeRate, normalFare);
                    logger.debug(requestId + " - surchargeTypeRate: " + surchargeTypeRate);
                    if (surchargeTypeRate.equals("shuttle")) {
                        fareType = "shuttle";
                    }
                    logger.debug(requestId + " - fareType: " + fareType);
                    boolean isReservation = bookingType.equalsIgnoreCase("Reservation");

                    if (sF.rushHourActive && pickupDate != null && !recurring && !ignoreuUpdateRushHour) {
                        String rushHourFeeType = redisDao.getTmp(requestId, requestId + "rushHourFeeType");
                        String rushHourFeeValue = redisDao.getTmp(requestId, requestId + "rushHourFeeValue");
                        if (rushHourFeeType == null || rushHourFeeType.isEmpty() || rushHourFeeValue == null || rushHourFeeValue.isEmpty()) {
                            List<Object> getRushHour = getRushHour(fleet, sF.rushHours, pickupDate, desDate, currencyISO,
                                    timeZoneId, timeZoneId, distance, surchargeTypeRate, fareType, isReservation,
                                requestId);
                            rushHourFeeType = getRushHour.get(0).toString();
                            rushHourFeeValue = getRushHour.get(1).toString();
                            redisDao.cacheData(requestId, requestId + "rushHourFeeType", rushHourFeeType, 1);
                            redisDao.cacheData(requestId, requestId + "rushHourFeeValue", rushHourFeeValue, 1);
                        }
                        if (rushHourFeeType.equalsIgnoreCase("amount")) {
                            rushHourFee = Double.parseDouble(rushHourFeeValue);
                        } else {
                            rushHourFee = (Double.parseDouble(rushHourFeeValue) * basicFare) / 100;
                        }
                    }

                    // get other fees
                    if (sF.otherFeeActive && sF.otherFee.valueByCurrencies != null) {
                        AmountByCurrency otherFee = sF.otherFee.valueByCurrencies.stream()
                                .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                .findFirst().orElse(null);
                        if (otherFee != null)
                            otherFees = otherFee.value;
                    }

                    // get tax
                    if (sF.taxActive) {
                        tax = sF.tax;
                    }
                }
                // get default general setting
                if (fleetFare.applyType == null || fleetFare.applyType.equals("all")
                        || !fleet.generalSetting.allowCongfigSettingForEachZone) {
                    // reset all fee
                    otherFees = 0;
                    tax = 0;
                    if (bookId.isEmpty())
                        rushHourFee = 0;

                    // get rush hour fee
                    if (fleetFare.rushHourActive && pickupDate != null && !recurring && !ignoreuUpdateRushHour) {
                        String surchargeTypeRate = getSurchargeTypeRate(jobType, typeRate, normalFare);
                        logger.debug(requestId + " - surchargeTypeRate: " + surchargeTypeRate);
                        if (surchargeTypeRate.equals("shuttle")) {
                            fareType = "shuttle";
                        }
                        logger.debug(requestId + " - fareType: " + fareType);
                        boolean isReservation = bookingType.equalsIgnoreCase("Reservation");
                        String rushHourFeeType = redisDao.getTmp(requestId, requestId + "rushHourFeeType");
                        String rushHourFeeValue = redisDao.getTmp(requestId, requestId + "rushHourFeeValue");
                        if (rushHourFeeType == null || rushHourFeeType.isEmpty() || rushHourFeeValue == null || rushHourFeeValue.isEmpty()) {
                        List<Object> getRushHour = getRushHour(fleet, fleetFare.rushHours, pickupDate, desDate,
                                currencyISO,
                                timeZoneId, timeZoneId, distance, surchargeTypeRate, fareType, isReservation,
                                requestId);
                        rushHourFeeType = getRushHour.get(0).toString();
                            rushHourFeeValue = getRushHour.get(1).toString();
                            redisDao.cacheData(requestId, requestId + "rushHourFeeType", rushHourFeeType, 1);
                            redisDao.cacheData(requestId, requestId + "rushHourFeeValue", rushHourFeeValue, 1);
                        }
                        if (rushHourFeeType.equalsIgnoreCase("amount")) {
                            rushHourFee = Double.parseDouble(rushHourFeeValue);
                        } else {
                            rushHourFee = (Double.parseDouble(rushHourFeeValue) * basicFare) / 100;
                        }
                    }

                    // get other fees
                    if (fleetFare.otherFeeActive && fleetFare.otherFee.valueByCurrencies != null) {
                        AmountByCurrency otherFee = fleetFare.otherFee.valueByCurrencies.stream()
                                .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                .findFirst().orElse(null);
                        if (otherFee != null)
                            otherFees = otherFee.value;
                    }

                    // get tax
                    if (fleetFare.taxActive) {
                        tax = fleetFare.tax;
                    }
                }

                //calculator sub total
                subTotal = CommonUtils.getRoundValue(basicFare) + CommonUtils.getRoundValue(rushHourFee) + otherFees;

                //update dynamicSurcharge
                if (!ignoreuUpdateRushHour) {
                    rushHourFee = CommonUtils.getRoundValue(rushHourFee) + CommonUtils.getRoundValue(dynamicSurcharge);
                    subTotal = subTotal + CommonUtils.getRoundValue(dynamicSurcharge);
                }
                // check if subTotal less than minimum before calculate fleet services
                if(minimum > 0 && subTotal < minimum){
                    subTotal = minimum;
                    min = true;
                }
                // do not get service fee for hydra booking
                if (pricingType == 0) {
                    List<Double> listServiceFees = getServiceFee(services, fleet, fleetFare, zoneId, currencyISO, tax, vehicleTypeObjectId, subTotal);
                    logger.debug(requestId + " - listServiceFees: " + gson.toJson(listServiceFees));
                    serviceFee = listServiceFees.get(0);
                    serviceTax = listServiceFees.get(1);

                    // get fleet commission for fleet services only
                    if(fleet.additionalFees == 1) {
                        // get fleet service
                        List<FleetService> fleetServices = new ArrayList<>();
                        if(fleetFare.applyType == null || fleetFare.applyType.equals("all") || !fleet.generalSetting.allowCongfigSettingForEachZone){
                            if(fleetFare.fleetServiceActive && fleetFare.fleetServices != null){
                                fleetServices = fleetFare.fleetServices;
                            }
                        } else {
                            if(sF != null && fleet.generalSetting.allowCongfigSettingForEachZone && !applyType.equals("all")){
                                if(sF.fleetServiceActive && sF.fleetServices != null){
                                    fleetServices = sF.fleetServices;
                                }
                            }
                        }

                        List<FleetService> selectedServices = new ArrayList<>();
                        for (FleetService fs : fleetServices) {
                            logger.debug(requestId + " - service name: " + fs.serviceName + " - type:" + fs.serviceType);
                            if ((services != null && services.contains(fs.serviceId)) || (fs.serviceType.equalsIgnoreCase("Compulsory") && fs.vehicleType.contains(vehicleTypeObjectId)))
                                fs.active = fs.isActive;
                            selectedServices.add(fs);
                            logger.debug(requestId + " - selectedServices: " + selectedServices.size());
                        }
                        if (!selectedServices.isEmpty()) {
                            List<Double> selectedFee = getFleetServiceFees(requestId, selectedServices, currencyISO, subTotal);
                            fleetCommissionFromFleetServiceFee = selectedFee.get(1);
                            driverCommissionFromFleetServiceFee = selectedFee.get(2);
                        }
                    }
                }
                // include additional services to subTotal
                if (fleet.additionalFees != 1)
                    subTotal += CommonUtils.getRoundValue(serviceFee);
                if (minimum > 0 && subTotal < minimum) {
                    subTotal = minimum;
                    min = true;
                }

                // get booking fee
                logger.debug(requestId + " - userId: " + userId);
                logger.debug(requestId + " - corporateId: " + corporateId);
                logger.debug(requestId + " - bookFrom: " + bookFrom);
                logger.debug(requestId + " - currencyISO: " + currencyISO);
                List<Object> data = getBookingFee(userId, corporateId, bookFrom, currencyISO);
                logger.debug(requestId + " - getBookingFee: " + gson.toJson(data));
                if (data.get(0).toString().equalsIgnoreCase("amount")) {
                    bookingFee = Double.parseDouble(data.get(1).toString());
                } else {
                    bookingFee = (Double.parseDouble(data.get(1).toString()) * subTotal) / 100;
                }
                bookingFeeActive = Boolean.parseBoolean(data.get(2).toString());

                // get tax fee
                taxFee = (tax * subTotal) / 100 + serviceTax;

                // get tip fee
                if (fleetFare.tipActive && pricingType == 0) {
                    tipFee = (fleetFare.tips * subTotal) / 100;
                    if (account != null && account.isActive && (tip != -1 || !pegasus && tip == -1)) {
                        tipFee = (account.tips * subTotal) / 100;
                    }
                    if (KeysUtil.command_center.equals(bookFrom) || KeysUtil.api.equals(bookFrom) ||
                            KeysUtil.web_booking.equals(bookFrom) || pegasus
                            || !pegasus && tip != -1) {
                        tipFee = (tip * subTotal) / 100;
                        if (tip == -1 || tipFee < 0) {
                            tipFee = 0;
                        }
                    }
                    if (tip == -1 && KeysUtil.PWA.equals(bookFrom) && account == null) {
                        tipFee = (fleetFare.tips * subTotal) / 100;
                    }
                }

                // get Promo value for local booking only
                if (pricingType == 0) {
                    JSONObject promoCode = getPromoCode(requestId, fleet.fleetId, userId, code, currencyISO, subTotal,
                            bookId);
                    // JSONObject promoCode = getPromoCode(requestId, fleet.fleetId, code,
                    // currencyISO);
                    logger.debug(requestId + " - promoCode: " + promoCode);
                    promoValue = Double.parseDouble(promoCode.get("value").toString());
                    keepMinFee = Boolean.parseBoolean(promoCode.get("keepMinFee").toString());
                    if (promoValue > Double.parseDouble(promoCode.get("maximumValue").toString()) &&
                            Double.parseDouble(promoCode.get("maximumValue").toString()) > 0) {
                        promoValue = Double.parseDouble(promoCode.get("maximumValue").toString());
                    }
                }
            } else {
                rushHourFee = CommonUtils.getRoundValue(dynamicSurcharge);
                subTotal = subTotal + CommonUtils.getRoundValue(rushHourFee);
                if (fleet.additionalFees != 1)
                    subTotal += CommonUtils.getRoundValue(serviceFee);
                if (minimum > 0 && subTotal < minimum) {
                    subTotal = minimum;
                    min = true;
                }
            }
        }
        if (KeysUtil.THIRD_PARTY_HOLIDAYTAXIS.equals(bookFrom)) {
            airportFee = 0;
            meetDriverFee = 0;
            rushHourFee = 0;
            techFee = 0;
            bookingFee = 0;
            taxFee = 0;
            tax = 0;
            tipFee = 0;
            otherFees = 0;
            promoValue = 0;
            serviceFee = 0;
            serviceTax = 0;
            fleetCommissionFromFleetServiceFee = 0;
            driverCommissionFromFleetServiceFee = 0;
            min = false;
            bookingFeeActive = false;
            totalWithoutPromo = 0;
            dynamicSurcharge = 0;
            surchargeParameter = 0;
            dynamicFare = 0;
            dynamicType = "factor";
            shortTrip = false;
            keepMinFee = false;

            // update fix subTotal value
            subTotal = basicFare;
        }
        fareReturn.basicFare = CommonUtils.getRoundValue(basicFare);
        fareReturn.normalFare = normalFare;
        fareReturn.currencyISO = currencyISO;
        fareReturn.route = route;
        fareReturn.routeId = routeId;
        fareReturn.reverseRoute = reverseRoute;
        fareReturn.airportFee = airportFee;
        fareReturn.rushHourFee = CommonUtils.getRoundValue(rushHourFee);
        fareReturn.dynamicSurcharge = CommonUtils.getRoundValue(dynamicSurcharge);
        fareReturn.surchargeParameter = surchargeParameter;
        fareReturn.meetDriverFee = meetDriverFee;
        fareReturn.techFee = techFee;
        fareReturn.otherFees = otherFees;
        fareReturn.minFare = minimum;
        fareReturn.min = min;
        fareReturn.extraDistanceFee = extraDistanceFare;
        fareReturn.extraDurationFee = extraDurationFare;
        fareReturn.bookingFee = CommonUtils.getRoundValue(bookingFee);
        fareReturn.bookingFeeActive = bookingFeeActive;
        fareReturn.tax = CommonUtils.getRoundValue(taxFee);
        fareReturn.tip = CommonUtils.getRoundValue(tipFee);
        fareReturn.serviceFee = CommonUtils.getRoundValue(serviceFee);
        fareReturn.serviceTax = CommonUtils.getRoundValue(serviceTax);
        fareReturn.fleetCommissionFromFleetServiceFee = CommonUtils.getRoundValue(fleetCommissionFromFleetServiceFee);
        fareReturn.driverCommissionFromFleetServiceFee = CommonUtils.getRoundValue(driverCommissionFromFleetServiceFee);
        fareReturn.dynamicFare = CommonUtils.getRoundValue(dynamicFare);
        fareReturn.dynamicType = dynamicType;
        fareReturn.promoCode = code;
        fareReturn.taxSetting = tax;
        totalWithoutPromo = subTotal + techFee + CommonUtils.getRoundValue(bookingFee) +
                CommonUtils.getRoundValue(taxFee) + CommonUtils.getRoundValue(tipFee);
        totalWithoutPromo = totalWithoutPromo + airportFee + meetDriverFee;
        if (fleet.additionalFees == 1)
            totalWithoutPromo += CommonUtils.getRoundValue(serviceFee);
        etaFare = totalWithoutPromo - CommonUtils.getRoundValue(promoValue);
        double originPromo = 0;
        if (keepMinFee && minimum > 0 && etaFare <= minimum) {
            fareReturn.isMinimumTotal = true;
            originPromo = totalWithoutPromo - minimum;
            logger.debug(requestId + " - " + "originPromo: " + originPromo);
            etaFare = totalWithoutPromo - CommonUtils.getRoundValue(originPromo);
        }
        fareReturn.promoAmount = CommonUtils.getRoundValue(promoValue);
        fareReturn.subTotal = CommonUtils.getRoundValue(subTotal);
        // do not apply transaction fee for Hydra

        if (pricingType == 0 && KeysUtil.transactionFeeMethod.contains(paymentMethod)) {
            fareReturn.creditTransactionFee = getTransactionFee(fleet.creditCardTransactionFee, etaFare, paymentMethod,
                    currencyISO, requestId);
            etaFare = etaFare + fareReturn.creditTransactionFee;
        }
        if (etaFare < 0)
            etaFare = 0;
        fareReturn.unroundedTotalAmt = CommonUtils.getRoundValue(etaFare);
        fareReturn.etaFare = roundingValue(fareReturn.unroundedTotalAmt, fleet.rounding, currencyISO);
        // check and return markup estimate
        double markupEstimate = 0.0;
        double markupEstimateAmount = fareReturn.unroundedTotalAmt;
        if (!vehicleTypeId.isEmpty()) {
            VehicleType vehicleType = mongoDao.getVehicleTypeByFleetAndName(fleet.fleetId, vehicleTypeId);
            if (vehicleType != null && vehicleType.actualFare && vehicleType.markupEstimate > 0) {
                markupEstimate = etaFare * vehicleType.markupEstimate / 100;
                markupEstimateAmount = etaFare + markupEstimate;
            }
        }
        fareReturn.markupEstimate = CommonUtils.getRoundValue(markupEstimate);
        fareReturn.markupEstimateAmount = roundingValue(markupEstimateAmount, fleet.rounding, currencyISO);
        if (fareReturn.etaFare > 0 && originPromo == 0) {
            fareReturn.totalWithoutPromo = CommonUtils
                    .getRoundValue(fareReturn.etaFare + CommonUtils.getRoundValue(promoValue));
        } else if (promoValue > 0) {
            fareReturn.totalWithoutPromo = roundingValue(totalWithoutPromo + fareReturn.creditTransactionFee,
                    fleet.rounding, currencyISO);
        }
        // check short trip without dynamicSurcharge
        shortTrip = checkWaiveOffCommission(fleet, zoneId, totalWithoutPromo + fareReturn.creditTransactionFee,
                CommonUtils.getRoundValue(rushHourFee), currencyISO, pickupDate, timeZoneId, "transport", requestId);
        fareReturn.shortTrip = shortTrip;
        if (pricingType == 0) {
            double creditTransactionFeePercent = 0.0;
            double creditTransactionFeeAmount = 0.0;
            if (KeysUtil.transactionFeeMethod.contains(paymentMethod)) {
                JSONObject object = getCreditTransactionFeeSetting(fleet.creditCardTransactionFee, paymentMethod,
                        currencyISO, requestId);
                creditTransactionFeePercent = Double.valueOf(object.get("creditTransactionFeePercent").toString());
                creditTransactionFeeAmount = Double.valueOf(object.get("creditTransactionFeeAmount").toString());
            }
            fareReturn.creditTransactionFeePercent = creditTransactionFeePercent;
            fareReturn.creditTransactionFeeAmount = creditTransactionFeeAmount;
        }
        fareReturn.rateType = rateType; // value = regular / flat / hourly
        if (rateType.equals("flat"))
            fareReturn.rateId = routeId;
        else if (rateType.equals("hourly")) {
            fareReturn.rateId = packageRateId;
        } else {
            fareReturn.rateId = fareNormalId;
        }
        return fareReturn;
    }

    public FareReturn calFareByMeter(Booking booking, Fleet fleet, FleetFare fleetFare, Account account,
            String mDispatcherId, String corporateId,
            String packageRateId, String code, int bookType, int typeRate, double durationTime,
            double distance, int numOfDays, String bookingType, String bookFrom, int meetDriver,
            double tip, List<Double> pickup, List<Double> destination, Date pickupDate, Date desDate,
            String fareNormalId, String flatRouteId, boolean isReverseRoute, String currencyISO,
            String zoneId, String requestId, List<String> services, String timeZoneId, boolean recurring, String vehicleTypeId)
            throws Exception {
        FareReturn fareReturn = new FareReturn();
        // Calculator Basic fare
        double basicFare = 0;
        boolean normalFare = false;
        String route = "";
        String routeId = "";
        boolean reverseRoute = false;
        double minimum = 0;
        double extraDistanceFare = 0;
        double extraDurationFare = 0;
        List<Object> dataFare = getBasicFareByMeter(bookType, typeRate, durationTime, distance, numOfDays, bookingType,
                packageRateId, fareNormalId, flatRouteId, isReverseRoute, currencyISO, requestId);
        basicFare = Double.parseDouble(dataFare.get(0).toString());
        normalFare = Boolean.parseBoolean(dataFare.get(1).toString());
        route = dataFare.get(2).toString();
        minimum = Double.parseDouble(dataFare.get(3).toString());
        extraDistanceFare = Double.parseDouble(dataFare.get(4).toString());
        extraDurationFare = Double.parseDouble(dataFare.get(5).toString());
        routeId = dataFare.get(6).toString();
        reverseRoute = Boolean.parseBoolean(dataFare.get(7).toString());
        basicFare = CommonUtils.getRoundValue(basicFare);
        String fareType = dataFare.get(8).toString();

        // check round down setting
        double airportFee = 0;
        double meetDriverFee = 0;
        double rushHourFee = 0;
        double techFee = 0;
        double bookingFee = 0;
        double taxFee = 0;
        double tax = 0;
        double tipFee = 0;
        double otherFees = 0;
        double promoValue = 0;
        double serviceFee = 0;
        double serviceTax = 0;
        double subTotal = 0;
        boolean min = false;
        boolean bookingFeeActive = false;
        double etaFare = 0;
        double totalWithoutPromo = 0;
        double dynamicSurcharge = 0;
        double surchargeParameter = 0;
        double dynamicFare = 0;
        String dynamicType = "factor";
        boolean shortTrip = false;
        boolean keepMinFee = false;
        if (basicFare >= 0) {
            // get dynamic fare
            if (fleet.generalSetting.dynamicFare && normalFare) {
                DynamicFare dF = mongoDao.getDynamicFare(fleet.fleetId, zoneId, pickup);
                if (dF != null && dF.isActive) {
                    dynamicFare = dF.parameter;
                    dynamicType = dF.type != null ? dF.type : "factor";
                    if (dF.type == null || dF.type.equals("factor")) {
                        basicFare += basicFare * (dF.parameter - 1);
                        minimum += minimum * (dF.parameter - 1);
                    } else {
                        basicFare = basicFare + dF.parameter;
                        minimum = minimum + dF.parameter;
                    }
                    if (basicFare < 0)
                        basicFare = 0;
                    if (minimum < 0)
                        minimum = 0;
                }
            }

            // check discount if book corporate
            basicFare = getModifyPriceOfCorp(corporateId, basicFare, requestId);

            // get basic fare completed, get fare setting
            subTotal = basicFare;

            // get dynamic surcharge
            if (fleet.generalSetting.dynamicSurcharge && bookingType.equals("Now") && !recurring) {
                surchargeParameter = getDynamicSurcharge(fleet.fleetId, zoneId, pickup, destination);
                dynamicSurcharge = basicFare * (surchargeParameter - 1);
                if (dynamicSurcharge < 0)
                    dynamicSurcharge = 0;
            }

            if (fleet.estimateFare != 0) {
                // get tech fee
                List<Object> getTechFee = getTechFeeValueByZone(fleet, bookFrom, currencyISO, zoneId);
                techFee = Double.parseDouble(getTechFee.get(0).toString());

                // get airport fee
                airportFee = getAirportFee(fleet, currencyISO, pickup, destination, bookType == 1,
                        typeRate == 2, distance > 1);

                // get meet driver fee
                if (fleet.additionalService.fromAirport.meetDriver) {
                    if (meetDriver == 0) {
                        if (fleetFare.meetDriver.onCurbByCurrencies != null) {
                            AmountByCurrency onCurb = fleetFare.meetDriver.onCurbByCurrencies.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                    .findFirst().orElse(null);
                            if (onCurb != null)
                                meetDriverFee = onCurb.value;
                        }
                    }
                    if (meetDriver == 1) {
                        if (fleetFare.meetDriver.meetDrvByCurrencies != null) {
                            AmountByCurrency meetDrv = fleetFare.meetDriver.meetDrvByCurrencies.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                    .findFirst().orElse(null);
                            if (meetDrv != null)
                                meetDriverFee = meetDrv.value;
                        }
                    }
                }

                // check apply zone
                // get general setting apply for each zone
                ServiceFee sF = mongoDao.getServiceFeeByFleetIdAndZoneId(fleet.fleetId, zoneId);
                String applyType = fleetFare.applyType != null ? fleetFare.applyType : "all";
                if (sF != null && fleet.generalSetting.allowCongfigSettingForEachZone && !applyType.equals("all")) {
                    // get rush hour fee
                    String surchargeTypeRate = getSurchargeTypeRateFromBooking(booking);
                    logger.debug(requestId + " - surchargeTypeRate: " + surchargeTypeRate);
                    if (surchargeTypeRate.equals("shuttle")) {
                        fareType = "shuttle";
                    }
                    logger.debug(requestId + " - fareType: " + fareType);
                    boolean isReservation = bookingType.equalsIgnoreCase("Reservation");
                    if (sF.rushHourActive && pickupDate != null && !recurring) {
                        List<Object> getRushHour = getRushHour(fleet, sF.rushHours, pickupDate, desDate, currencyISO,
                                timeZoneId, timeZoneId, distance, surchargeTypeRate, fareType, isReservation,
                                requestId);
                        if (getRushHour.get(0).toString().equalsIgnoreCase("amount")) {
                            rushHourFee = Double.parseDouble(getRushHour.get(1).toString());
                        } else {
                            rushHourFee = (Double.parseDouble(getRushHour.get(1).toString()) * basicFare) / 100;
                        }
                    }

                    // get other fees
                    if (sF.otherFeeActive && sF.otherFee.valueByCurrencies != null) {
                        AmountByCurrency otherFee = sF.otherFee.valueByCurrencies.stream()
                                .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                .findFirst().orElse(null);
                        if (otherFee != null)
                            otherFees = otherFee.value;
                    }

                    // get tax
                    if (sF.taxActive) {
                        tax = sF.tax;
                    }
                }
                // get default general setting
                if (fleetFare.applyType == null || fleetFare.applyType.equals("all")
                        || !fleet.generalSetting.allowCongfigSettingForEachZone) {
                    // reset all fee
                    rushHourFee = 0;
                    otherFees = 0;
                    tax = 0;

                    // get rush hour fee
                    if (fleetFare.rushHourActive && pickupDate != null && !recurring) {
                        String surchargeTypeRate = getSurchargeTypeRateFromBooking(booking);
                        logger.debug(requestId + " - surchargeTypeRate: " + surchargeTypeRate);
                        if (surchargeTypeRate.equals("shuttle")) {
                            fareType = "shuttle";
                        }
                        logger.debug(requestId + " - fareType: " + fareType);
                        boolean isReservation = bookingType.equalsIgnoreCase("Reservation");
                        List<Object> getRushHour = getRushHour(fleet, fleetFare.rushHours, pickupDate, desDate,
                                currencyISO,
                                timeZoneId, timeZoneId, distance, surchargeTypeRate, fareType, isReservation,
                                requestId);
                        if (getRushHour.get(0).toString().equalsIgnoreCase("amount")) {
                            rushHourFee = Double.parseDouble(getRushHour.get(1).toString());
                        } else {
                            rushHourFee = (Double.parseDouble(getRushHour.get(1).toString()) * basicFare) / 100;
                        }
                    }

                    // get other fees
                    if (fleetFare.otherFeeActive && fleetFare.otherFee.valueByCurrencies != null) {
                        AmountByCurrency otherFee = fleetFare.otherFee.valueByCurrencies.stream()
                                .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                .findFirst().orElse(null);
                        if (otherFee != null)
                            otherFees = otherFee.value;
                    }

                    // get tax
                    if (fleetFare.taxActive) {
                        tax = fleetFare.tax;
                    }
                }
                //calculator sub total
                subTotal = basicFare + CommonUtils.getRoundValue(rushHourFee) + otherFees;

                // update dynamicSurcharge
                rushHourFee = CommonUtils.getRoundValue(rushHourFee) + CommonUtils.getRoundValue(dynamicSurcharge);
                subTotal = subTotal + CommonUtils.getRoundValue(dynamicSurcharge);
                // check if subTotal less than minimum before calculate fleet services
                if(minimum > 0 && subTotal < minimum){
                    subTotal = minimum;
                    min = true;
                }
                // get fleet service fee
                List<Double> listServiceFees = getServiceFee(services, fleet, fleetFare, zoneId, currencyISO, tax, vehicleTypeId, subTotal);
                logger.debug(requestId + " - listServiceFees: " + gson.toJson(listServiceFees));
                serviceFee = listServiceFees.get(0);
                serviceTax = listServiceFees.get(1);

                // include additional services to subTotal
                if (fleet.additionalFees != 1)
                    subTotal += CommonUtils.getRoundValue(serviceFee);
                if (minimum > 0 && subTotal < minimum) {
                    subTotal = minimum;
                    min = true;
                }

                // get booking fee
                List<Object> data = getBookingFee(mDispatcherId, corporateId, bookFrom, currencyISO);
                if (data.get(0).toString().equalsIgnoreCase("amount")) {
                    bookingFee = Double.parseDouble(data.get(1).toString());
                } else {
                    bookingFee = (Double.parseDouble(data.get(1).toString()) * subTotal) / 100;
                }
                bookingFeeActive = Boolean.parseBoolean(data.get(2).toString());

                // get tax fee
                taxFee = (tax * subTotal) / 100 + serviceTax;

                // get tip fee
                if (fleetFare.tipActive) {
                    tipFee = (fleetFare.tips * subTotal) / 100;
                    if (account != null && account.isActive) {
                        tipFee = (account.tips * subTotal) / 100;
                    }
                    if (KeysUtil.command_center.equals(bookFrom) || KeysUtil.api.equals(bookFrom) ||
                            KeysUtil.web_booking.equals(bookFrom)) {
                        tipFee = (tip * subTotal) / 100;
                        if (tipFee < 0) {
                            tipFee = 0;
                        }
                    }
                }

                // get Promo value
                // JSONObject promoCode = getPromoCode(requestId, fleet.fleetId, account.userId,
                // code, currencyISO, subTotal);
                JSONObject promoCode = getPromoCode(requestId, fleet.fleetId, code, currencyISO);
                logger.debug(requestId + " - promoCode: " + promoCode.toJSONString());
                keepMinFee = Boolean.parseBoolean(promoCode.get("keepMinFee").toString());
                if (promoCode.get("type").toString().equalsIgnoreCase(KeysUtil.COMMISION_AMOUNT)) {
                    promoValue = Double.parseDouble(promoCode.get("value").toString());
                } else {
                    promoValue = (Double.parseDouble(promoCode.get("value").toString()) * subTotal) / 100;
                    if (promoValue > Double.parseDouble(promoCode.get("maximumValue").toString()) &&
                            Double.parseDouble(promoCode.get("maximumValue").toString()) > 0)
                        promoValue = Double.parseDouble(promoCode.get("maximumValue").toString());
                }
            } else {
                rushHourFee = CommonUtils.getRoundValue(dynamicSurcharge);
                subTotal = subTotal + CommonUtils.getRoundValue(rushHourFee);
                if (fleet.additionalFees != 1)
                    subTotal += CommonUtils.getRoundValue(serviceFee);
                if (minimum > 0 && subTotal < minimum) {
                    subTotal = minimum;
                    min = true;
                }
            }
        }
        fareReturn.basicFare = CommonUtils.getRoundValue(basicFare);
        fareReturn.normalFare = normalFare;
        fareReturn.currencyISO = currencyISO;
        fareReturn.route = route;
        fareReturn.routeId = routeId;
        fareReturn.reverseRoute = reverseRoute;
        fareReturn.airportFee = airportFee;
        fareReturn.rushHourFee = CommonUtils.getRoundValue(rushHourFee);
        fareReturn.dynamicSurcharge = CommonUtils.getRoundValue(dynamicSurcharge);
        fareReturn.surchargeParameter = surchargeParameter;
        fareReturn.meetDriverFee = meetDriverFee;
        fareReturn.techFee = techFee;
        fareReturn.otherFees = otherFees;
        fareReturn.minFare = minimum;
        fareReturn.min = min;
        fareReturn.extraDistanceFee = extraDistanceFare;
        fareReturn.extraDurationFee = extraDurationFare;
        fareReturn.bookingFee = CommonUtils.getRoundValue(bookingFee);
        fareReturn.bookingFeeActive = bookingFeeActive;
        fareReturn.tax = CommonUtils.getRoundValue(taxFee);
        fareReturn.tip = CommonUtils.getRoundValue(tipFee);
        fareReturn.serviceFee = CommonUtils.getRoundValue(serviceFee);
        fareReturn.serviceTax = CommonUtils.getRoundValue(serviceTax);
        fareReturn.dynamicFare = CommonUtils.getRoundValue(dynamicFare);
        fareReturn.dynamicType = dynamicType;
        fareReturn.promoCode = code;
        fareReturn.taxSetting = tax;
        totalWithoutPromo = subTotal + airportFee + meetDriverFee + techFee
                + CommonUtils.getRoundValue(bookingFee) +
                CommonUtils.getRoundValue(taxFee) + CommonUtils.getRoundValue(tipFee);
        if (fleet.additionalFees == 1)
            totalWithoutPromo += CommonUtils.getRoundValue(serviceFee);
        etaFare = totalWithoutPromo - CommonUtils.getRoundValue(promoValue);
        if (etaFare < 0)
            etaFare = 0;
        double originPromo = 0;
        if (keepMinFee && minimum > 0 && etaFare <= minimum) {
            fareReturn.isMinimumTotal = true;
            originPromo = totalWithoutPromo - minimum;
            logger.debug(requestId + " - " + "originPromo: " + originPromo);
            etaFare = totalWithoutPromo - CommonUtils.getRoundValue(originPromo);
        }
        fareReturn.promoAmount = CommonUtils.getRoundValue(promoValue);
        fareReturn.subTotal = CommonUtils.getRoundValue(subTotal);
        fareReturn.unroundedTotalAmt = CommonUtils.getRoundValue(etaFare);
        fareReturn.etaFare = roundingValue(fareReturn.unroundedTotalAmt, fleet.rounding, currencyISO);
        if (fareReturn.etaFare > 0 && originPromo == 0) {
            fareReturn.totalWithoutPromo = CommonUtils
                    .getRoundValue(fareReturn.etaFare + CommonUtils.getRoundValue(promoValue));
        } else if (promoValue > 0) {
            fareReturn.totalWithoutPromo = roundingValue(totalWithoutPromo, fleet.rounding, currencyISO);
        }
        // check short trip without dynamicSurcharge
        shortTrip = checkWaiveOffCommission(fleet, zoneId, totalWithoutPromo, CommonUtils.getRoundValue(rushHourFee),
                currencyISO, pickupDate,
                timeZoneId, "transport", requestId);
        fareReturn.shortTrip = shortTrip;
        return fareReturn;
    }

    public FareReturn calFareCarHailing(Fleet fleet, FleetFare fleetFare, Account account, int bookType,
            double durationTime,
            double distance, List<Double> pickup, List<Double> destination, String zipCodeFrom,
            String zipCodeTo, Date pickupDate, Date desDate, String fareNormalId, String fareFlatId,
            String currencyISO, String zoneId, String requestId, List<String> services, String timeZoneId,
            String jobType, int typeRate, String vehicleTypeId) throws Exception {
        FareReturn fareReturn = new FareReturn();
        // Calculator Basic fare
        double basicFare = 0;
        boolean normalFare = false;
        String route = "";
        String routeId = "";
        boolean reverseRoute = false;
        double minimum = 0;
        List<Object> dataFare = getBaseFeeOneWay(durationTime, distance, KeysUtil.TIME_BOOK_NOW, pickup, destination,
                zipCodeFrom, zipCodeTo, fareNormalId, fareFlatId, currencyISO, requestId);
        basicFare = Double.parseDouble(dataFare.get(0).toString());
        normalFare = Boolean.parseBoolean(dataFare.get(1).toString());
        minimum = Double.parseDouble(dataFare.get(2).toString());
        route = dataFare.get(3).toString();
        routeId = dataFare.get(4).toString();
        reverseRoute = Boolean.parseBoolean(dataFare.get(5).toString());
        basicFare = CommonUtils.getRoundValue(basicFare);
        String fareType = normalFare ? "regular" : "flat";

        // check round down setting
        double airportFee = 0;
        double meetDriverFee = 0;
        double rushHourFee = 0;
        double techFee = 0;
        double bookingFee = 0;
        double taxFee = 0;
        double tax = 0;
        double tipFee = 0;
        double otherFees = 0;
        double promoValue = 0;
        double serviceFee = 0;
        double serviceTax = 0;
        double subTotal = 0;
        boolean min = false;
        double etaFare = 0;
        double totalWithoutPromo = 0;
        double dynamicSurcharge = 0;
        double surchargeParameter = 0;
        double dynamicFare = 0;
        String dynamicType = "factor";
        if (basicFare >= 0) {
            // get dynamic fare
            if (fleet.generalSetting.dynamicFare && normalFare) {
                DynamicFare dF = mongoDao.getDynamicFare(fleet.fleetId, zoneId, pickup);
                if (dF != null && dF.isActive) {
                    dynamicFare = dF.parameter;
                    dynamicType = dF.type != null ? dF.type : "factor";
                    if (dF.type == null || dF.type.equals("factor")) {
                        basicFare += basicFare * (dF.parameter - 1);
                        minimum += minimum * (dF.parameter - 1);
                    } else {
                        basicFare = basicFare + dF.parameter;
                        minimum = minimum + dF.parameter;
                    }
                    if (basicFare < 0)
                        basicFare = 0;
                    if (minimum < 0)
                        minimum = 0;
                }
            }

            // get basic fare completed, get fare setting
            subTotal = basicFare;

            // get dynamic surcharge
            if (fleet.generalSetting.dynamicSurcharge) {
                surchargeParameter = getDynamicSurcharge(fleet.fleetId, zoneId, pickup, destination);
                dynamicSurcharge = basicFare * (surchargeParameter - 1);
                if (dynamicSurcharge < 0)
                    dynamicSurcharge = 0;
            }

            if (fleet.estimateFare != 0) {
                // get tech fee
                List<Object> getTechFee = getTechFeeValueByZone(fleet, KeysUtil.car_hailing, currencyISO, zoneId);
                techFee = Double.parseDouble(getTechFee.get(0).toString());

                // get airport fee
                airportFee = getAirportFee(fleet, currencyISO, pickup, destination, bookType == 1, false, distance > 1);

                // check apply zone
                // get general setting apply for each zone
                ServiceFee sF = mongoDao.getServiceFeeByFleetIdAndZoneId(fleet.fleetId, zoneId);
                String surchargeTypeRate = getSurchargeTypeRate(jobType, typeRate, normalFare);
                logger.debug(requestId + " - surchargeTypeRate: " + surchargeTypeRate);
                if (surchargeTypeRate.equals("shuttle")) {
                    fareType = "shuttle";
                }
                logger.debug(requestId + " - fareType: " + fareType);
                boolean isReservation = false; // default
                String applyType = fleetFare.applyType != null ? fleetFare.applyType : "all";
                if (sF != null && fleet.generalSetting.allowCongfigSettingForEachZone && !applyType.equals("all")) {
                    // get rush hour fee
                    if (sF.rushHourActive && pickupDate != null) {
                        List<Object> getRushHour = getRushHour(fleet, sF.rushHours, pickupDate, desDate, currencyISO,
                                timeZoneId, timeZoneId, distance, surchargeTypeRate, fareType, isReservation,
                                requestId);
                        if (getRushHour.get(0).toString().equalsIgnoreCase("amount")) {
                            rushHourFee = Double.parseDouble(getRushHour.get(1).toString());
                        } else {
                            rushHourFee = (Double.parseDouble(getRushHour.get(1).toString()) * basicFare) / 100;
                        }
                    }

                    // get other fees
                    if (sF.otherFeeActive && sF.otherFee.valueByCurrencies != null) {
                        AmountByCurrency otherFee = sF.otherFee.valueByCurrencies.stream()
                                .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                .findFirst().orElse(null);
                        if (otherFee != null)
                            otherFees = otherFee.value;
                    }

                    // get tax
                    if (sF.taxActive) {
                        tax = sF.tax;
                    }
                }
                // get default general setting
                if (fleetFare.applyType == null || fleetFare.applyType.equals("all")
                        || !fleet.generalSetting.allowCongfigSettingForEachZone) {
                    // reset all fee
                    rushHourFee = 0;
                    otherFees = 0;
                    tax = 0;

                    // get rush hour fee
                    if (fleetFare.rushHourActive && pickupDate != null) {
                        List<Object> getRushHour = getRushHour(fleet, fleetFare.rushHours, pickupDate, desDate,
                                currencyISO,
                                timeZoneId, timeZoneId, distance, surchargeTypeRate, fareType, isReservation,
                                requestId);
                        if (getRushHour.get(0).toString().equalsIgnoreCase("amount")) {
                            rushHourFee = Double.parseDouble(getRushHour.get(1).toString());
                        } else {
                            rushHourFee = (Double.parseDouble(getRushHour.get(1).toString()) * basicFare) / 100;
                        }
                    }

                    // get other fees
                    if (fleetFare.otherFeeActive && fleetFare.otherFee.valueByCurrencies != null) {
                        AmountByCurrency otherFee = fleetFare.otherFee.valueByCurrencies.stream()
                                .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                .findFirst().orElse(null);
                        if (otherFee != null)
                            otherFees = otherFee.value;
                    }

                    // get tax
                    if (fleetFare.taxActive) {
                        tax = fleetFare.tax;
                    }
                }

                //calculator sub total
                subTotal = basicFare + CommonUtils.getRoundValue(rushHourFee) + otherFees;

                // update dynamicSurcharge
                rushHourFee = CommonUtils.getRoundValue(rushHourFee) + CommonUtils.getRoundValue(dynamicSurcharge);
                subTotal = subTotal + CommonUtils.getRoundValue(dynamicSurcharge);
                // check if subTotal less than minimum before calculate fleet services
                if(minimum > 0 && subTotal < minimum){
                    subTotal = minimum;
                    min = true;
                }
                // get fleet service fee
                List<Double> listServiceFees = getServiceFee(services, fleet, fleetFare, zoneId, currencyISO, tax, vehicleTypeId, subTotal);
                logger.debug(requestId + " - listServiceFees: " + gson.toJson(listServiceFees));
                serviceFee = listServiceFees.get(0);
                serviceTax = listServiceFees.get(1);

                // include additional services to subTotal
                if (fleet.additionalFees != 1)
                    subTotal += CommonUtils.getRoundValue(serviceFee);
                if (minimum > 0 && subTotal < minimum) {
                    subTotal = minimum;
                    min = true;
                }

                // get tax fee
                taxFee = (tax * subTotal) / 100 + serviceTax;

                // get tip fee
                if (fleetFare.tipActive) {
                    tipFee = (fleetFare.tips * subTotal) / 100;
                    if (account != null && account.isActive) {
                        tipFee = (account.tips * subTotal) / 100;
                    }
                }

            } else {
                rushHourFee = CommonUtils.getRoundValue(dynamicSurcharge);
                subTotal = subTotal + CommonUtils.getRoundValue(rushHourFee);
                if (fleet.additionalFees != 1)
                    subTotal += CommonUtils.getRoundValue(serviceFee);
                if (minimum > 0 && subTotal < minimum) {
                    subTotal = minimum;
                    min = true;
                }
            }
        }
        fareReturn.basicFare = CommonUtils.getRoundValue(basicFare);
        fareReturn.normalFare = normalFare;
        fareReturn.currencyISO = currencyISO;
        fareReturn.route = route;
        fareReturn.routeId = routeId;
        fareReturn.reverseRoute = reverseRoute;
        fareReturn.airportFee = airportFee;
        fareReturn.rushHourFee = CommonUtils.getRoundValue(rushHourFee);
        fareReturn.dynamicSurcharge = CommonUtils.getRoundValue(dynamicSurcharge);
        fareReturn.surchargeParameter = surchargeParameter;
        fareReturn.meetDriverFee = meetDriverFee;
        fareReturn.techFee = techFee;
        fareReturn.otherFees = otherFees;
        fareReturn.minFare = minimum;
        fareReturn.min = min;
        fareReturn.bookingFeeActive = false;
        fareReturn.tax = CommonUtils.getRoundValue(taxFee);
        fareReturn.tip = CommonUtils.getRoundValue(tipFee);
        fareReturn.serviceFee = CommonUtils.getRoundValue(serviceFee);
        fareReturn.serviceTax = CommonUtils.getRoundValue(serviceTax);
        fareReturn.dynamicFare = CommonUtils.getRoundValue(dynamicFare);
        fareReturn.dynamicType = dynamicType;
        fareReturn.taxSetting = tax;
        totalWithoutPromo = subTotal + techFee + CommonUtils.getRoundValue(bookingFee) +
                CommonUtils.getRoundValue(taxFee) + CommonUtils.getRoundValue(tipFee) + airportFee + meetDriverFee
                ;
        if (fleet.additionalFees == 1)
            totalWithoutPromo += CommonUtils.getRoundValue(serviceFee);
        etaFare = totalWithoutPromo - CommonUtils.getRoundValue(promoValue);
        if (etaFare < 0)
            etaFare = 0;
        fareReturn.subTotal = CommonUtils.getRoundValue(subTotal);
        fareReturn.unroundedTotalAmt = CommonUtils.getRoundValue(etaFare);
        fareReturn.etaFare = roundingValue(fareReturn.unroundedTotalAmt, fleet.rounding, currencyISO);
        if (fareReturn.etaFare > 0) {
            fareReturn.totalWithoutPromo = CommonUtils
                    .getRoundValue(fareReturn.etaFare + CommonUtils.getRoundValue(promoValue));
        }
        return fareReturn;
    }

    public ETAFareSharingResponse calFareSharing(Fleet fleet, StreetSharing streetSharing, Rounding rounding,
            List<Integer> dropOffOrder,
            int order, double duration, double distance, String fareSharingId,
            String currencyISO, String zoneId, String requestId) throws Exception {
        ETAFareSharingResponse response = new ETAFareSharingResponse();

        // Calculator Basic fare
        double feePerPax = 0;
        double minimum = 0;
        int numOfPax = (int) streetSharing.passengers.stream().filter(pax -> pax.status.equals("engaged")).count();
        List<Object> dataFare = getBaseFeeSharing(order == 1, numOfPax,
                duration, distance, fareSharingId, currencyISO, requestId);
        feePerPax = Double.parseDouble(dataFare.get(0).toString());
        minimum = Double.parseDouble(dataFare.get(1).toString());
        feePerPax = CommonUtils.getRoundValue(feePerPax);
        /*
         * if(feePerPax < minimum)
         * feePerPax = minimum;
         */

        // get tech fee
        List<Object> getTechFee = getTechFeeValueByZone(fleet, KeysUtil.street_sharing, currencyISO, zoneId);
        double techFee = Double.parseDouble(getTechFee.get(0).toString());
        logger.debug(requestId + " - " + "techFee: " + techFee);

        Waypoint waypoint = new Waypoint();
        waypoint.order = order;
        waypoint.fare = feePerPax;
        if (order == 1) {
            waypoint.fare = feePerPax + techFee;
        }

        double totalFareWaypoint = 0;
        List<Passenger> passengers = new ArrayList<>();
        for (SharedPassenger pax : streetSharing.passengers) {
            if (pax.status.equals("engaged")) {
                Passenger p = new Passenger();
                p.order = pax.order;
                if (order == 1) {
                    p.baseFare = feePerPax;
                } else {
                    p.baseFare = pax.baseFare + feePerPax;
                }
                p.techFee = techFee;
                if (p.baseFare < minimum)
                    p.baseFare = minimum;
                p.totalFare = p.baseFare + techFee;
                if (dropOffOrder != null && dropOffOrder.contains(p.order))
                    p.totalFare = roundingValue(p.totalFare, rounding, currencyISO);
                totalFareWaypoint += p.totalFare;
                passengers.add(p);
            } else {
                totalFareWaypoint += pax.totalFare;
            }
        }

        response.waypoint = waypoint;
        response.passengers = passengers;
        response.currencyISO = currencyISO;
        response.minimumFare = minimum;
        response.techFee = techFee;
        response.totalFare = totalFareWaypoint;
        return response;
    }

    public FareReturn calFareWithExtraLocation(Fleet fleet, FleetFare fleetFare, Account account, ETAFareEnt etaFareEnt,
            String userId, String corporateId, String bookingType, Date pickupDate,
            Date desDate, String fareNormalId, String fareFlatId, String currencyISO,
            String zoneId, String timeZoneId, boolean pegasus, String vehicleTypeId) throws Exception {
        FareReturn fareReturn = new FareReturn();
        // Calculator Basic fare
        double baseFee = 0;
        double airportFee = 0;
        double meetDriverFee = 0;
        double dynamicFare = 0;
        String dynamicType = "factor";
        double minimum = 0;
        double totalDistance = 0;
        double totalDuration = 0;
        List<Double> pickup = etaFareEnt.pickup;
        String zipCodeFrom = etaFareEnt.zipCodeFrom;
        int index = 0;
        String unitDistance = fleet.unitDistance;
        double tip = etaFareEnt.tip != null ? etaFareEnt.tip : -1;

        if (fleet.additionalService.fromAirport.meetDriver) {
            if (etaFareEnt.meetDriver == 0) {
                if (fleetFare.meetDriver.onCurbByCurrencies != null) {
                    AmountByCurrency onCurb = fleetFare.meetDriver.onCurbByCurrencies.stream()
                            .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                            .findFirst().orElse(null);
                    if (onCurb != null)
                        meetDriverFee = onCurb.value;
                }
            }
            if (etaFareEnt.meetDriver == 1) {
                if (fleetFare.meetDriver.meetDrvByCurrencies != null) {
                    AmountByCurrency meetDrv = fleetFare.meetDriver.meetDrvByCurrencies.stream()
                            .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                            .findFirst().orElse(null);
                    if (meetDrv != null)
                        meetDriverFee = meetDrv.value;
                }
            }
        }

        boolean normalFare = false;
        for (index = 0; index <= etaFareEnt.extraDestination.size(); index++) {
            double baseFeePerPoint = 0;
            double minimumPerPoint = 0;
            boolean regularFarePerPoint = false;
            double distancePerPoint = 0;
            double durationPerPoint = 0;
            double airportFeePerPoint = 0;
            List<Double> destination = new ArrayList<>();
            String zipCodeTo = "";
            if (index != etaFareEnt.extraDestination.size()) {
                ExtraLocation exLoc = etaFareEnt.extraDestination.get(index);
                distancePerPoint = exLoc.distance;
                durationPerPoint = exLoc.duration;
                destination = exLoc.geo;
                zipCodeTo = exLoc.zipCode;
                totalDistance += distancePerPoint;
                totalDuration += durationPerPoint;
            } else {
                distancePerPoint = etaFareEnt.distance - totalDistance;
                durationPerPoint = etaFareEnt.duration - totalDuration;
                destination = etaFareEnt.destination;
                zipCodeTo = etaFareEnt.zipCodeTo;
            }
            if (unitDistance.equalsIgnoreCase(KeysUtil.UNIT_DISTANCE_IMPERIAL)) {
                distancePerPoint = ConvertUtil.round((distancePerPoint / 1609.344), 2);
            } else {
                distancePerPoint = ConvertUtil.round((distancePerPoint / 1000), 2);
            }
            logger.debug(etaFareEnt.requestId + " - route: " + index + " - duration: " + durationPerPoint +
                    " - distance: " + distancePerPoint + " - pickup: " + pickup + " - destination: " + destination);
            List<Object> dataFare1 = getBaseFeeOneWay(durationPerPoint, distancePerPoint, bookingType, pickup,
                    destination, zipCodeFrom, zipCodeTo, fareNormalId, fareFlatId, currencyISO, etaFareEnt.requestId);
            baseFeePerPoint = Double.parseDouble(dataFare1.get(0).toString());
            regularFarePerPoint = Boolean.parseBoolean(dataFare1.get(1).toString());
            // get setting surcharge for fisrt range
            if (index == 0 && regularFarePerPoint)
                normalFare = true;
            minimumPerPoint = Double.parseDouble(dataFare1.get(2).toString());
            // get dynamic fare
            if (fleet.generalSetting.dynamicFare && regularFarePerPoint) {
                DynamicFare dF = mongoDao.getDynamicFare(fleet.fleetId, zoneId, pickup);
                if (dF != null && dF.isActive) {
                    dynamicFare = dF.parameter;
                    dynamicType = dF.type != null ? dF.type : "factor";
                    if (dF.type == null || dF.type.equals("factor")) {
                        baseFeePerPoint += baseFeePerPoint * (dF.parameter - 1);
                        minimumPerPoint += minimumPerPoint * (dF.parameter - 1);
                    } else {
                        baseFeePerPoint = baseFeePerPoint + dF.parameter;
                        minimumPerPoint = minimumPerPoint + dF.parameter;
                    }
                    if (baseFeePerPoint < 0)
                        baseFeePerPoint = 0;
                    if (minimumPerPoint < 0)
                        minimumPerPoint = 0;
                }
            }
            // check rate type support
            if (baseFeePerPoint == 0) {
                fareReturn.supportExtraLocation = false;
                return fareReturn;
            }
            baseFee += baseFeePerPoint;
            if (minimumPerPoint > minimum)
                minimum = minimumPerPoint;
            logger.debug(etaFareEnt.requestId + " - route: " + index + " - baseFee: " + baseFeePerPoint +
                    " - regular rate: " + regularFarePerPoint + " - minimum: " + minimumPerPoint);

            boolean fromAirport = false;
            AirportZone airportZone = mongoDao.checkDestinationByGeo(pickup);
            if (airportZone != null) {
                fromAirport = true;
                logger.debug(etaFareEnt.requestId + " - route: " + index + " - fromAirport: " + fromAirport);
            }
            airportFeePerPoint = getAirportFee(fleet, currencyISO, pickup, destination, fromAirport, false,
                    distancePerPoint > 1);
            logger.debug(etaFareEnt.requestId + " - route: " + index + " - airportFee: " + airportFeePerPoint);
            airportFee += airportFeePerPoint;
            // set pickup for next route
            pickup = destination;
        }
        String fareType = normalFare ? "regular" : "flat";
        logger.debug(etaFareEnt.requestId + " - minimum: " + minimum);
        baseFee = CommonUtils.getRoundValue(baseFee);
        logger.debug(etaFareEnt.requestId + " - basic fare: " + baseFee);

        // check round down setting
        double rushHourFee = 0;
        double techFee = 0;
        double bookingFee = 0;
        double taxFee = 0;
        double tax = 0;
        double tipFee = 0;
        double otherFees = 0;
        double promoValue = 0;
        double serviceFee = 0;
        double serviceTax = 0;
        double subTotal = 0;
        boolean min = false;
        boolean bookingFeeActive = false;
        double etaFare = 0;
        double totalWithoutPromo = 0;
        double dynamicSurcharge = 0;
        double surchargeParameter = 0;
        boolean shortTrip = false;
        boolean keepMinFee = false;
        if (baseFee >= 0) {
            // check discount if book corporate
            baseFee = getModifyPriceOfCorp(corporateId, baseFee, etaFareEnt.requestId);

            // get basic fare completed, get fare setting
            subTotal = baseFee;

            // get dynamic surcharge
            if (fleet.generalSetting.dynamicSurcharge && bookingType.equals("Now")) {
                surchargeParameter = getDynamicSurcharge(fleet.fleetId, zoneId, etaFareEnt.pickup,
                        etaFareEnt.destination);
                dynamicSurcharge = baseFee * (surchargeParameter - 1);
                if (dynamicSurcharge < 0)
                    dynamicSurcharge = 0;
            }

            if (fleet.estimateFare != 0 || etaFareEnt.actualFare == 1) {
                // get tech fee
                List<Object> getTechFee = getTechFeeValueByZone(fleet, etaFareEnt.bookFrom, currencyISO, zoneId);
                techFee = Double.parseDouble(getTechFee.get(0).toString());

                // check apply zone
                // get general setting apply for each zone
                ServiceFee sF = mongoDao.getServiceFeeByFleetIdAndZoneId(fleet.fleetId, zoneId);
                String applyType = fleetFare.applyType != null ? fleetFare.applyType : "all";
                if (sF != null && fleet.generalSetting.allowCongfigSettingForEachZone && !applyType.equals("all")) {
                    // get  rush hour fee
                    if (sF.rushHourActive && pickupDate != null) {
                        String surchargeTypeRate = getSurchargeTypeRate(etaFareEnt.jobType, etaFareEnt.typeRate,
                                normalFare);
                        logger.debug(etaFareEnt.requestId + " - surchargeTypeRate: " + surchargeTypeRate);
                        logger.debug(etaFareEnt.requestId + " - fareType: " + fareType);
                        boolean isReservation = !etaFareEnt.pickupTime.equals("Now");
                        List<Object> getRushHour = getRushHour(fleet, sF.rushHours, pickupDate, desDate, currencyISO,
                                timeZoneId, timeZoneId, etaFareEnt.distance, surchargeTypeRate, fareType, isReservation,
                                etaFareEnt.requestId);
                        if (getRushHour.get(0).toString().equalsIgnoreCase("amount")) {
                            rushHourFee = Double.parseDouble(getRushHour.get(1).toString());
                        } else {
                            rushHourFee = (Double.parseDouble(getRushHour.get(1).toString()) * baseFee) / 100;
                        }
                    }

                    // get other fees
                    if (sF.otherFeeActive && sF.otherFee.valueByCurrencies != null) {
                        AmountByCurrency otherFee = sF.otherFee.valueByCurrencies.stream()
                                .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                .findFirst().orElse(null);
                        if (otherFee != null)
                            otherFees = otherFee.value;
                    }

                    // get tax
                    if (sF.taxActive) {
                        tax = sF.tax;
                    }
                }
                // get default general setting
                if (fleetFare.applyType == null || fleetFare.applyType.equals("all")
                        || !fleet.generalSetting.allowCongfigSettingForEachZone) {
                    // reset all fee
                    rushHourFee = 0;
                    otherFees = 0;
                    tax = 0;

                    // get rush hour fee
                    if (fleetFare.rushHourActive && pickupDate != null) {
                        String surchargeTypeRate = getSurchargeTypeRate(etaFareEnt.jobType, etaFareEnt.typeRate,
                                normalFare);
                        logger.debug(etaFareEnt.requestId + " - surchargeTypeRate: " + surchargeTypeRate);
                        logger.debug(etaFareEnt.requestId + " - fareType: " + fareType);
                        boolean isReservation = !etaFareEnt.pickupTime.equals("Now");
                        List<Object> getRushHour = getRushHour(fleet, fleetFare.rushHours, pickupDate, desDate,
                                currencyISO,
                                timeZoneId, timeZoneId, etaFareEnt.distance, surchargeTypeRate, fareType, isReservation,
                                etaFareEnt.requestId);
                        if (getRushHour.get(0).toString().equalsIgnoreCase("amount")) {
                            rushHourFee = Double.parseDouble(getRushHour.get(1).toString());
                        } else {
                            rushHourFee = (Double.parseDouble(getRushHour.get(1).toString()) * baseFee) / 100;
                        }
                    }

                    // get other fees
                    if (fleetFare.otherFeeActive && fleetFare.otherFee.valueByCurrencies != null) {
                        AmountByCurrency otherFee = fleetFare.otherFee.valueByCurrencies.stream()
                                .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                .findFirst().orElse(null);
                        if (otherFee != null)
                            otherFees = otherFee.value;
                    }

                    // get tax
                    if (fleetFare.taxActive) {
                        tax = fleetFare.tax;
                    }
                }
                //calculator sub total
                subTotal = baseFee + CommonUtils.getRoundValue(rushHourFee) + otherFees;

                // update dynamicSurcharge
                rushHourFee = CommonUtils.getRoundValue(rushHourFee) + CommonUtils.getRoundValue(dynamicSurcharge);
                subTotal = subTotal + CommonUtils.getRoundValue(dynamicSurcharge);
                // check if subTotal less than minimum before calculate fleet services
                if(minimum > 0 && subTotal < minimum){
                    subTotal = minimum;
                    min = true;
                }
                // get fleet service fee
                List<Double> listServiceFees = getServiceFee(etaFareEnt.services, fleet, fleetFare, zoneId, currencyISO, tax, vehicleTypeId, subTotal);
                logger.debug(etaFareEnt.requestId + " - listServiceFees: " + gson.toJson(listServiceFees));
                serviceFee = listServiceFees.get(0);
                serviceTax = listServiceFees.get(1);

                // include additional services to subTotal
                if (fleet.additionalFees != 1)
                    subTotal += CommonUtils.getRoundValue(serviceFee);
                if (minimum > 0 && subTotal < minimum) {
                    subTotal = minimum;
                    min = true;
                }

                // get booking fee
                List<Object> data = getBookingFee(userId, corporateId, etaFareEnt.bookFrom, currencyISO);
                if (data.get(0).toString().equalsIgnoreCase("amount")) {
                    bookingFee = Double.parseDouble(data.get(1).toString());
                } else {
                    bookingFee = (Double.parseDouble(data.get(1).toString()) * subTotal) / 100;
                }
                bookingFeeActive = Boolean.parseBoolean(data.get(2).toString());

                // get tax fee
                taxFee = (tax * subTotal) / 100 + serviceTax;

                // get tip fee
                if (fleetFare.tipActive) {
                    tipFee = (fleetFare.tips * subTotal) / 100;
                    if (account != null && account.isActive && (tip != -1 || !pegasus && tip == -1)) {
                        tipFee = (account.tips * subTotal) / 100;
                    }
                    if (KeysUtil.command_center.equals(etaFareEnt.bookFrom) || KeysUtil.api.equals(etaFareEnt.bookFrom)
                            ||
                            KeysUtil.web_booking.equals(etaFareEnt.bookFrom) || pegasus || !pegasus && tip != -1) {
                        tipFee = (tip * subTotal) / 100;
                        if (tip == -1 || tipFee < 0) {
                            tipFee = 0;
                        }
                    }
                }

                // get Promo value
                // JSONObject promoCode = getPromoCode(etaFareEnt.requestId, fleet.fleetId,
                // userId, etaFareEnt.promoCode, currencyISO, subTotal);
                JSONObject promoCode = getPromoCode(etaFareEnt.requestId, fleet.fleetId, etaFareEnt.promoCode,
                        currencyISO);
                logger.debug(etaFareEnt.requestId + " - promoCode: " + promoCode.toJSONString());
                keepMinFee = Boolean.parseBoolean(promoCode.get("keepMinFee").toString());
                if (promoCode.get("type").toString().equalsIgnoreCase(KeysUtil.COMMISION_AMOUNT)) {
                    promoValue = Double.parseDouble(promoCode.get("value").toString());
                } else {
                    promoValue = (Double.parseDouble(promoCode.get("value").toString()) * subTotal) / 100;
                    if (promoValue > Double.parseDouble(promoCode.get("maximumValue").toString()) &&
                            Double.parseDouble(promoCode.get("maximumValue").toString()) > 0)
                        promoValue = Double.parseDouble(promoCode.get("maximumValue").toString());
                }
            } else {
                rushHourFee = CommonUtils.getRoundValue(dynamicSurcharge);
                subTotal = subTotal + rushHourFee;
                if (fleet.additionalFees != 1)
                    subTotal += CommonUtils.getRoundValue(serviceFee);
                if (minimum > 0 && subTotal < minimum) {
                    subTotal = minimum;
                    min = true;
                }
            }
        }
        fareReturn.basicFare = CommonUtils.getRoundValue(baseFee);
        fareReturn.currencyISO = currencyISO;
        fareReturn.airportFee = airportFee;
        fareReturn.rushHourFee = rushHourFee;
        fareReturn.dynamicSurcharge = CommonUtils.getRoundValue(dynamicSurcharge);
        fareReturn.surchargeParameter = surchargeParameter;
        fareReturn.meetDriverFee = meetDriverFee;
        fareReturn.techFee = techFee;
        fareReturn.otherFees = otherFees;
        fareReturn.minFare = minimum;
        fareReturn.min = min;
        fareReturn.bookingFee = CommonUtils.getRoundValue(bookingFee);
        fareReturn.bookingFeeActive = bookingFeeActive;
        fareReturn.tax = CommonUtils.getRoundValue(taxFee);
        fareReturn.tip = CommonUtils.getRoundValue(tipFee);
        fareReturn.serviceFee = CommonUtils.getRoundValue(serviceFee);
        fareReturn.serviceTax = CommonUtils.getRoundValue(serviceTax);
        fareReturn.dynamicFare = dynamicFare;
        fareReturn.dynamicType = dynamicType;
        fareReturn.promoCode = etaFareEnt.promoCode;
        fareReturn.taxSetting = tax;
        logger.debug(etaFareEnt.requestId + " - subTotal: " + subTotal);
        totalWithoutPromo = subTotal + airportFee + meetDriverFee + techFee
                + CommonUtils.getRoundValue(bookingFee) +
                CommonUtils.getRoundValue(taxFee) + CommonUtils.getRoundValue(tipFee);
        if (fleet.additionalFees == 1)
            totalWithoutPromo += CommonUtils.getRoundValue(serviceFee);
        etaFare = totalWithoutPromo - CommonUtils.getRoundValue(promoValue);
        double originPromo = 0;
        if (keepMinFee && minimum > 0 && etaFare <= minimum) {
            fareReturn.isMinimumTotal = true;
            originPromo = totalWithoutPromo - minimum;
            logger.debug(etaFareEnt.requestId + " - " + "originPromo: " + originPromo);
            etaFare = totalWithoutPromo - CommonUtils.getRoundValue(originPromo);
        }
        fareReturn.promoAmount = CommonUtils.getRoundValue(promoValue);
        fareReturn.subTotal = subTotal;
        if (KeysUtil.transactionFeeMethod.contains(etaFareEnt.paymentMethod)) {
            fareReturn.creditTransactionFee = getTransactionFee(fleet.creditCardTransactionFee, etaFare,
                    etaFareEnt.paymentMethod,
                    currencyISO, etaFareEnt.requestId);
            etaFare = etaFare + fareReturn.creditTransactionFee;
        }
        if (etaFare < 0)
            etaFare = 0;
        fareReturn.unroundedTotalAmt = CommonUtils.getRoundValue(etaFare);
        fareReturn.etaFare = roundingValue(fareReturn.unroundedTotalAmt, fleet.rounding, currencyISO);
        if (fareReturn.etaFare > 0 && originPromo == 0) {
            fareReturn.totalWithoutPromo = CommonUtils
                    .getRoundValue(fareReturn.etaFare + CommonUtils.getRoundValue(promoValue));
        } else if (promoValue > 0) {
            fareReturn.totalWithoutPromo = roundingValue(totalWithoutPromo + fareReturn.creditTransactionFee,
                    fleet.rounding, currencyISO);
        }
        // check short trip without dynamicSurcharge
        shortTrip = checkWaiveOffCommission(fleet, zoneId, totalWithoutPromo + fareReturn.creditTransactionFee,
                rushHourFee, currencyISO, pickupDate, timeZoneId, "transport", etaFareEnt.requestId);
        fareReturn.shortTrip = shortTrip;
        double creditTransactionFeePercent = 0.0;
        double creditTransactionFeeAmount = 0.0;
        if (KeysUtil.transactionFeeMethod.contains(etaFareEnt.paymentMethod)) {
            JSONObject object = getCreditTransactionFeeSetting(fleet.creditCardTransactionFee, etaFareEnt.paymentMethod,
                    currencyISO, etaFareEnt.requestId);
            creditTransactionFeePercent = Double.valueOf(object.get("creditTransactionFeePercent").toString());
            creditTransactionFeeAmount = Double.valueOf(object.get("creditTransactionFeeAmount").toString());
        }
        fareReturn.creditTransactionFeePercent = creditTransactionFeePercent;
        fareReturn.creditTransactionFeeAmount = creditTransactionFeeAmount;
        fareReturn.rateType = "";
        fareReturn.rateId = "";
        return fareReturn;
    }

    public FareReturn calFareIntercity(Fleet fleet, FleetFare fleetFare, ETAFareEnt etaFareEnt, String zoneId, String vehicleTypeId)
            throws Exception {
        FareReturn fareReturn = new FareReturn();
        // Calculator Basic fare
        double basicFare = 0;
        String routeName = "";
        String currencyISO = fleet.currencies.get(0).iso;
        List<Object> dataFare = getBaseFeeIntercity(etaFareEnt.routeNumber, etaFareEnt.intercityRouteId,
                etaFareEnt.seat, etaFareEnt.luggage, etaFareEnt.tripType, etaFareEnt.requestId);
        basicFare = Double.parseDouble(dataFare.get(0).toString());
        routeName = dataFare.get(1).toString();
        if (!dataFare.get(2).toString().isEmpty())
            currencyISO = dataFare.get(2).toString();
        basicFare = CommonUtils.getRoundValue(basicFare);
        logger.debug(etaFareEnt.requestId + " - basic fare: " + basicFare);

        // check round down setting
        double bookingFee = 0;
        double taxFee = 0;
        double tax = 0;
        double promoValue = 0;
        double serviceFee = 0;
        double serviceTax = 0;
        double subTotal = 0;
        double etaFare = 0;
        double totalWithoutPromo = 0;
        boolean bookingFeeActive = false;
        if (basicFare >= 0) {
            // get basic fare completed, get fare setting
            subTotal = basicFare;

            if (fleet.estimateFare != 0 || etaFareEnt.actualFare == 1) {
                // check apply zone
                // get general setting apply for each zone
                ServiceFee sF = mongoDao.getServiceFeeByFleetIdAndZoneId(fleet.fleetId, zoneId);
                String applyType = fleetFare.applyType != null ? fleetFare.applyType : "all";
                if (sF != null && fleet.generalSetting.allowCongfigSettingForEachZone && !applyType.equals("all")) {
                    // get tax
                    if (sF.taxActive) {
                        tax = sF.tax;
                    }
                }
                // get default general setting
                if (fleetFare.applyType == null || fleetFare.applyType.equals("all")
                        || !fleet.generalSetting.allowCongfigSettingForEachZone) {
                    // get tax
                    if (fleetFare.taxActive) {
                        tax = fleetFare.tax;
                    }
                }

                //calculator sub total
                subTotal = basicFare;

                // get fleet service fee
                List<Double> listServiceFees = getServiceFee(etaFareEnt.services, fleet, fleetFare, zoneId, currencyISO, tax, vehicleTypeId, subTotal);
                logger.debug(etaFareEnt.requestId + " - listServiceFees: " + gson.toJson(listServiceFees));
                serviceFee = listServiceFees.get(0);
                serviceTax = listServiceFees.get(1);

                if (fleet.additionalFees != 1)
                    subTotal += CommonUtils.getRoundValue(serviceFee);

                // get booking fee
                List<Object> data = getBookingFee(etaFareEnt.userId, etaFareEnt.corporateId, etaFareEnt.bookFrom,
                        currencyISO);
                if (data.get(0).toString().equalsIgnoreCase("amount")) {
                    bookingFee = Double.parseDouble(data.get(1).toString());
                } else {
                    bookingFee = (Double.parseDouble(data.get(1).toString()) * subTotal) / 100;
                }
                bookingFeeActive = Boolean.parseBoolean(data.get(2).toString());
                // get tax fee
                taxFee = (tax * subTotal) / 100 + serviceTax;

                // get Promo value
                // JSONObject promoCode = getPromoCode(etaFareEnt.requestId, fleet.fleetId,
                // etaFareEnt.userId, etaFareEnt.promoCode, currencyISO, subTotal);
                JSONObject promoCode = getPromoCode(etaFareEnt.requestId, fleet.fleetId, etaFareEnt.promoCode,
                        currencyISO);
                logger.debug(etaFareEnt.requestId + " - promoCode: " + promoCode.toJSONString());
                if (promoCode.get("type").toString().equalsIgnoreCase(KeysUtil.COMMISION_AMOUNT)) {
                    promoValue = Double.parseDouble(promoCode.get("value").toString());
                } else {
                    promoValue = (Double.parseDouble(promoCode.get("value").toString()) * subTotal) / 100;
                    if (promoValue > Double.parseDouble(promoCode.get("maximumValue").toString()) &&
                            Double.parseDouble(promoCode.get("maximumValue").toString()) > 0)
                        promoValue = Double.parseDouble(promoCode.get("maximumValue").toString());
                }
            }
        }
        fareReturn.basicFare = CommonUtils.getRoundValue(basicFare);
        fareReturn.currencyISO = currencyISO;
        fareReturn.airportFee = 0;
        fareReturn.rushHourFee = 0;
        fareReturn.dynamicSurcharge = 0;
        fareReturn.surchargeParameter = 0;
        fareReturn.meetDriverFee = 0;
        fareReturn.techFee = 0;
        fareReturn.otherFees = 0;
        fareReturn.minFare = 0;
        fareReturn.min = false;
        fareReturn.route = routeName;
        fareReturn.bookingFee = CommonUtils.getRoundValue(bookingFee);
        fareReturn.tollFee = 0;
        fareReturn.parkingFee = 0;
        fareReturn.gasFee = 0;
        fareReturn.bookingFeeActive = bookingFeeActive;
        fareReturn.tax = CommonUtils.getRoundValue(taxFee);
        fareReturn.tip = 0;
        fareReturn.promoAmount = CommonUtils.getRoundValue(promoValue);
        fareReturn.serviceFee = CommonUtils.getRoundValue(serviceFee);
        fareReturn.serviceTax = CommonUtils.getRoundValue(serviceTax);
        fareReturn.dynamicFare = 0;
        fareReturn.promoCode = etaFareEnt.promoCode;
        fareReturn.taxSetting = tax;
        logger.debug(etaFareEnt.requestId + " - subTotal: " + subTotal);
        totalWithoutPromo = subTotal + CommonUtils.getRoundValue(taxFee);
        if (fleet.additionalFees == 1)
            totalWithoutPromo += CommonUtils.getRoundValue(serviceFee);
        etaFare = totalWithoutPromo - CommonUtils.getRoundValue(promoValue);
        fareReturn.subTotal = subTotal;
        if (KeysUtil.transactionFeeMethod.contains(etaFareEnt.paymentMethod)) {
            fareReturn.creditTransactionFee = getTransactionFee(fleet.creditCardTransactionFee, etaFare,
                    etaFareEnt.paymentMethod,
                    currencyISO, etaFareEnt.requestId);
            etaFare = etaFare + fareReturn.creditTransactionFee;
        }
        if (etaFare < 0)
            etaFare = 0;
        fareReturn.unroundedTotalAmt = CommonUtils.getRoundValue(etaFare);
        fareReturn.etaFare = roundingValue(fareReturn.unroundedTotalAmt, fleet.rounding, currencyISO);
        if (fleet.fleetId.equalsIgnoreCase("mycar") || fleet.fleetId.equalsIgnoreCase("myqup")) {
            fareReturn.totalWithoutPromo = CommonUtils.getRoundValue(totalWithoutPromo);
        } else {
            if (fareReturn.etaFare > 0) {
                fareReturn.totalWithoutPromo = CommonUtils
                        .getRoundValue(fareReturn.etaFare + CommonUtils.getRoundValue(promoValue));
            } else if (promoValue > 0) {
                fareReturn.totalWithoutPromo = roundingValue(totalWithoutPromo, fleet.rounding, currencyISO);
            }
        }
        fareReturn.shortTrip = false;
        double creditTransactionFeePercent = 0.0;
        double creditTransactionFeeAmount = 0.0;
        if (KeysUtil.transactionFeeMethod.contains(etaFareEnt.paymentMethod)) {
            JSONObject object = getCreditTransactionFeeSetting(fleet.creditCardTransactionFee, etaFareEnt.paymentMethod,
                    currencyISO, etaFareEnt.requestId);
            creditTransactionFeePercent = Double.valueOf(object.get("creditTransactionFeePercent").toString());
            creditTransactionFeeAmount = Double.valueOf(object.get("creditTransactionFeeAmount").toString());
        }
        fareReturn.creditTransactionFeePercent = creditTransactionFeePercent;
        fareReturn.creditTransactionFeeAmount = creditTransactionFeeAmount;
        fareReturn.rateType = "intercity";
        fareReturn.rateId = etaFareEnt.intercityRouteId;
        return fareReturn;
    }

    public DeliveryResponse calFareDelivery(Fleet fleet, FleetFare fleetFare, ETADeliveryEnt etaFareEnt,
            String deliveryFlatRateId, String fareDeliveryId, String zoneId, Date pickupDate, String timeZoneId)
            throws Exception {
        DeliveryResponse response = new DeliveryResponse();
        double etaFare = 0;
        double promoValue = 0;
        double unroundedTotal = 0;
        double subTotal = 0;
        double total = 0;
        double deliveryFee = 0;
        double sumItemsFee = 0;
        double taxFee = 0;
        double techFee = 0;
        double tax = 0;

        // get tech fee
        List<Object> getTechFee = getTechFeeValueByZone(fleet, etaFareEnt.bookFrom, etaFareEnt.currencyISO, zoneId);
        techFee = Double.parseDouble(getTechFee.get(0).toString());

        // check apply zone
        // get general setting apply for each zone
        ServiceFee sF = mongoDao.getServiceFeeByFleetIdAndZoneId(fleet.fleetId, zoneId);
        String applyType = fleetFare.applyType != null ? fleetFare.applyType : "all";
        if (sF != null && fleet.generalSetting.allowCongfigSettingForEachZone && !applyType.equals("all")) {
            // get tax
            if (sF.taxActive) {
                tax = sF.tax;
            }
        }
        // get default general setting
        if (fleetFare.applyType == null || fleetFare.applyType.equals("all")
                || !fleet.generalSetting.allowCongfigSettingForEachZone) {
            // get tax
            if (fleetFare.taxActive) {
                tax = fleetFare.tax;
            }
        }

        List<RecipientFare> recipientFares = new ArrayList<>();
        // Calculator fare for each recipient
        List<LocationDetails> recipients = etaFareEnt.recipients;
        // only accept promo when have one recipient
        /*
         * if(recipients.size() > 1)
         * etaFareEnt.promoCode = "";
         */

        double totalDistance = 0;
        double totalDuration = 0;
        int i = 0;
        List<Double> pickup = new ArrayList<>();
        List<Double> destination = new ArrayList<>();
        for (LocationDetails recipient : recipients) {
            double itemsFee = 0;
            totalDuration += recipient.duration;
            totalDistance += recipient.distance;
            JSONArray arrayData = new JSONArray();
            if (recipient.menuData != null && !recipient.menuData.isEmpty()) {
                JSONObject obj = getItemsFee(gson.fromJson(recipient.menuData, JSONArray.class), etaFareEnt.currencyISO,
                        etaFareEnt.requestId, etaFareEnt.language);
                if (obj != null && obj.get("itemFees") != null) {
                    itemsFee = Double.valueOf(obj.get("itemFees").toString());
                }
                if (obj != null && obj.get("data") != null) {
                    arrayData = gson.fromJson(obj.get("data").toString(), JSONArray.class);
                }
            }
            if (etaFareEnt.merchants != null && !etaFareEnt.merchants.isEmpty()) {
                if (etaFareEnt.cashOnPickUp) {
                    totalDistance += etaFareEnt.merchants.get(0).distance;
                    totalDuration += etaFareEnt.merchants.get(0).duration;
                }
                if (etaFareEnt.merchants.get(0).menuData != null && !etaFareEnt.merchants.get(0).menuData.isEmpty()) {
                    JSONObject obj = getItemsFee(gson.fromJson(etaFareEnt.merchants.get(0).menuData, JSONArray.class),
                            etaFareEnt.currencyISO, etaFareEnt.requestId, etaFareEnt.language);
                    if (obj != null && obj.get("itemFees") != null) {
                        itemsFee = Double.valueOf(obj.get("itemFees").toString());
                    }
                    if (obj != null && obj.get("data") != null) {
                        arrayData = gson.fromJson(obj.get("data").toString(), JSONArray.class);
                    }
                }
            }
            sumItemsFee += CommonUtils.getRoundValue(itemsFee);

            RecipientFare recipientFare = new RecipientFare();
            if (etaFareEnt.deliveryType == 0 || etaFareEnt.deliveryType == 1) {
                recipientFare.itemsFee = CommonUtils.getRoundValue(itemsFee);
                recipientFare.totalOrder = CommonUtils.getRoundValue(itemsFee);
            } else {
                recipientFare.itemValue = CommonUtils.getRoundValue(itemsFee);
                recipientFare.totalOrder = CommonUtils.getRoundValue(itemsFee);
                recipientFare.distance = recipient.distance;
                recipientFare.duration = recipient.duration;
            }
            recipientFare.menuData = arrayData;
            recipientFare.order = recipient.order;

            // calculate fare for each recipient
            if (!deliveryFlatRateId.isEmpty()) {
                // check if food/mart then initial key
                String vehicleTypeId = etaFareEnt.vehicleTypeId != null ? etaFareEnt.vehicleTypeId : "";
                if (vehicleTypeId.equalsIgnoreCase("food") || vehicleTypeId.equalsIgnoreCase("mart")) {
                    etaFareEnt.pickup = etaFareEnt.merchants.get(0);
                }

                if (fleet.multiPointCalFareMode != null) {
                    // ignore zipCode for delivery
                    String zipCodeFrom = "";
                    String zipCodeTo = "";
                    if (fleet.multiPointCalFareMode.equals("perPoint")) {
                        logger.debug(etaFareEnt.requestId + " - multiPointCalFareMode perPoint");
                        logger.debug(etaFareEnt.requestId + " ========= get fare for user number " + i);
                        if (i == 0) {
                            pickup = etaFareEnt.pickup.geo;
                        } else {
                            pickup = destination;
                        }
                        destination = recipient.geo;
                        double durationTimeRange = recipient.duration;
                        // duration time in minutes -> convert to second
                        durationTimeRange *= 60;
                        double distanceRange = recipient.distance;
                        logger.debug(etaFareEnt.requestId + " --- get fare for user number " + i
                                + " durationTimeRange: " + durationTimeRange);
                        distanceRange = distanceRange >= 0 ? distanceRange : 0;
                        // convert distance
                        if (fleet.unitDistance.equalsIgnoreCase(KeysUtil.UNIT_DISTANCE_IMPERIAL)) {
                            distanceRange = ConvertUtil.round((distanceRange / 1609.344), 2);
                        } else {
                            distanceRange = ConvertUtil.round((distanceRange / 1000), 2);
                        }
                        logger.debug(etaFareEnt.requestId + " --- get fare for user number " + i + " distanceRange: "
                                + distanceRange);
                        logger.debug(
                                etaFareEnt.requestId + " --- get fare for user number " + i + " pickup: " + pickup);
                        logger.debug(etaFareEnt.requestId + " --- get fare for user number " + i + " destination: "
                                + destination);
                        List<Object> dataFare = getBasicFareForFlat(durationTimeRange, distanceRange, pickup,
                                destination, zipCodeFrom, zipCodeTo, deliveryFlatRateId, etaFareEnt.currencyISO,
                                etaFareEnt.requestId);
                        double basicFareRange = Double.parseDouble(dataFare.get(0).toString());
                        logger.debug(etaFareEnt.requestId + " --- get fare for user number " + i + " basicFareRange: "
                                + basicFareRange);
                        recipientFare.deliveryFee = basicFareRange;
                    }
                }
            }
            recipientFares.add(recipientFare);
            i++;
        }
        deliveryFee = getDeliveryFee(etaFareEnt, totalDuration, totalDistance, fleet.unitDistance,
                deliveryFlatRateId, fareDeliveryId, etaFareEnt.currencyISO, etaFareEnt.recipients.size(),
                etaFareEnt.requestId);
        logger.debug(etaFareEnt.requestId + " - deliveryFee = " + deliveryFee);
        // return null if deliveryFee == 0
        if (deliveryFee == 0)
            return null;
        // check discount if book corporate
        deliveryFee = getModifyPriceOfCorp(etaFareEnt.corporateId, deliveryFee, etaFareEnt.requestId);

        // calculator sub total
        subTotal = deliveryFee;
        if (etaFareEnt.deliveryType == 0 || etaFareEnt.deliveryType == 1)
            subTotal = subTotal + sumItemsFee;
        // get tax fee
        taxFee = (tax * subTotal) / 100;
        taxFee = CommonUtils.getRoundValue(taxFee);
        total = subTotal + techFee + taxFee;

        // get Promo value
        // JSONObject promoCode = getPromoCode(etaFareEnt.requestId, fleet.fleetId,
        // etaFareEnt.userId, etaFareEnt.promoCode, etaFareEnt.currencyISO, subTotal);
        JSONObject promoCode = getPromoCode(etaFareEnt.requestId, fleet.fleetId, etaFareEnt.promoCode,
                etaFareEnt.currencyISO);
        logger.debug(etaFareEnt.requestId + " - promoCode: " + promoCode.toJSONString());
        if (promoCode.get("type").toString().equalsIgnoreCase(KeysUtil.COMMISION_AMOUNT)) {
            promoValue = Double.parseDouble(promoCode.get("value").toString());
        } else {
            promoValue = (Double.parseDouble(promoCode.get("value").toString()) * subTotal) / 100;
            if (promoValue > Double.parseDouble(promoCode.get("maximumValue").toString()) &&
                    Double.parseDouble(promoCode.get("maximumValue").toString()) > 0)
                promoValue = Double.parseDouble(promoCode.get("maximumValue").toString());
        }
        unroundedTotal = total - CommonUtils.getRoundValue(promoValue);
        if (KeysUtil.transactionFeeMethod.contains(etaFareEnt.paymentMethod)) {
            response.creditTransactionFee = getTransactionFee(fleet.creditCardTransactionFee, unroundedTotal,
                    etaFareEnt.paymentMethod,
                    etaFareEnt.currencyISO, etaFareEnt.requestId);
            unroundedTotal = unroundedTotal + response.creditTransactionFee;
        }
        if (unroundedTotal < 0)
            unroundedTotal = 0;
        etaFare = roundingValue(unroundedTotal, fleet.rounding, etaFareEnt.currencyISO);
        response.currencyISO = etaFareEnt.currencyISO;
        response.promoAmount = CommonUtils.getRoundValue(promoValue);
        response.promoCode = etaFareEnt.promoCode;
        response.subTotal = CommonUtils.getRoundValue(subTotal);
        response.techFee = CommonUtils.getRoundValue(techFee);
        response.tax = CommonUtils.getRoundValue(taxFee);
        response.taxSetting = tax;
        response.deliveryFee = CommonUtils.getRoundValue(deliveryFee);
        if (etaFareEnt.deliveryType == 0 || etaFareEnt.deliveryType == 1) {
            response.itemsFee = CommonUtils.getRoundValue(sumItemsFee);
            response.totalOrder = CommonUtils.getRoundValue(etaFare);
        } else {
            response.itemValue = CommonUtils.getRoundValue(sumItemsFee);
            response.totalOrder = CommonUtils.getRoundValue(sumItemsFee + etaFare);
        }
        if (recipientFares.size() == 1) {
            recipientFares.get(0).deliveryFee = CommonUtils.getRoundValue(deliveryFee);
            recipientFares.get(0).techFee = CommonUtils.getRoundValue(techFee);
            recipientFares.get(0).tax = CommonUtils.getRoundValue(taxFee);
            recipientFares.get(0).subTotal = CommonUtils.getRoundValue(subTotal);
            recipientFares.get(0).promoAmount = CommonUtils.getRoundValue(promoValue);
            recipientFares.get(0).total = roundingValue(unroundedTotal, fleet.rounding, etaFareEnt.currencyISO);
        }
        response.recipients = recipientFares;
        response.unroundedTotalAmt = CommonUtils.getRoundValue(unroundedTotal);
        response.etaFare = CommonUtils.getRoundValue(etaFare);
        response.totalWithoutPromo = roundingValue(total, fleet.rounding, etaFareEnt.currencyISO);

        // check short trip without dynamicSurcharge
        response.shortTrip = checkWaiveOffCommission(fleet, zoneId, total, 0,
                etaFareEnt.currencyISO, pickupDate, timeZoneId, "delivery", etaFareEnt.requestId);

        // re-calculator if have edit fare
        if (etaFareEnt.editFare != null) {
            response = checkEditFareDelivery(etaFareEnt.requestId, fleet, response, etaFareEnt.editFare,
                    etaFareEnt.userId,
                    etaFareEnt.deliveryType == 0 || etaFareEnt.deliveryType == 1, tax);
            // recheck short trip without dynamicSurcharge
            response.shortTrip = checkWaiveOffCommission(fleet, zoneId, total, 0,
                    etaFareEnt.currencyISO, pickupDate, timeZoneId, "delivery", etaFareEnt.requestId);
        }
        double creditTransactionFeePercent = 0.0;
        double creditTransactionFeeAmount = 0.0;
        if (KeysUtil.transactionFeeMethod.contains(etaFareEnt.paymentMethod)) {
            JSONObject object = getCreditTransactionFeeSetting(fleet.creditCardTransactionFee, etaFareEnt.paymentMethod,
                    etaFareEnt.currencyISO, etaFareEnt.requestId);
            creditTransactionFeePercent = Double.valueOf(object.get("creditTransactionFeePercent").toString());
            creditTransactionFeeAmount = Double.valueOf(object.get("creditTransactionFeeAmount").toString());
        }
        response.creditTransactionFeePercent = creditTransactionFeePercent;
        response.creditTransactionFeeAmount = creditTransactionFeeAmount;

        // check if booking is customized driver earning
        double editedDriverEarning = 0.0;
        double originalDriverEarning = 0.0;
        String driverEarningType = "default";
        double merchantCommission = 0.0;
        String bookId = etaFareEnt.bookId != null ? etaFareEnt.bookId : "";
        if (!bookId.isEmpty()) {
            Booking booking = mongoDao.getBooking(bookId);
            if (booking == null) {
                booking = mongoDao.getBookingCompleted(bookId);
            }
            if (booking.request.estimate != null && booking.request.estimate.fare != null) {
                Fare fare = booking.request.estimate.fare;
                driverEarningType = fare.driverEarningType != null ? fare.driverEarningType : "default";
                if (!driverEarningType.equals("default")) {
                    editedDriverEarning = fare.editedDriverEarning;
                    originalDriverEarning = fare.originalDriverEarning;
                }
            }

        }
        String merchantId = "";
        if (etaFareEnt.merchants != null && !etaFareEnt.merchants.isEmpty()) {
            merchantId = etaFareEnt.merchants.get(0).merchantId != null ? etaFareEnt.merchants.get(0).merchantId : "";
            logger.debug(etaFareEnt.requestId + " - merchantId: " + merchantId);
        }
        if (!merchantId.isEmpty()) {
            MenuMerchant merchant = mongoDao.getMerchant(merchantId);
            if (merchant != null) {
                if (merchant.commissionType != null && merchant.commissionType.equals("percentage"))
                    merchantCommission = merchant.commission * 0.01 * sumItemsFee;
                else if (merchant.commissions != null && !merchant.commissions.isEmpty()) {
                    for (com.qupworld.paymentgateway.models.mongo.collections.menuMerchant.Commission mc : merchant.commissions) {
                        if (etaFareEnt.currencyISO.equals(mc.iso)) {
                            merchantCommission = mc.value;
                        }
                    }
                }
            }
            logger.debug(etaFareEnt.requestId + " - merchant commission: " + merchantCommission);
        }
        response.merchantCommission = CommonUtils.getRoundValue(merchantCommission);

        if (!driverEarningType.equals("default")) {
            response.driverEarningType = driverEarningType;
            response.editedDriverEarning = editedDriverEarning;
            response.originalDriverEarning = originalDriverEarning;
        } else if (etaFareEnt.deliveryType == 0) {
            /**
             * 0: Parcel
             * 1: Shop For You
             * 2: Food
             * 3: Mart
             */
            String jobType = "parcel"; // just update for parcel booking only
            String commissionType = "";
            double commission = 0.0;
            // check fleet commission and company commission
            String defaultFleetCommissionType = fleetFare.defaultFleetCommissionType != null
                    ? fleetFare.defaultFleetCommissionType
                    : "";
            if (defaultFleetCommissionType.equals("sameZones")) {
                List<CommissionServices> services = fleetFare.defaultFleetCommissionValue.sameZones;
                if (services != null && !services.isEmpty()) {
                    for (CommissionServices commissionServices : services) {
                        if (jobType.equals(commissionServices.serviceType)) {
                            commissionType = commissionServices.type;
                            commission = commissionServices.value;
                            break;
                        }
                    }
                }
            } else if (defaultFleetCommissionType.equals("differentZones")) {
                List<DifferentZone> differentZones = fleetFare.defaultFleetCommissionValue.differentZones;
                if (differentZones != null && !differentZones.isEmpty()) {
                    for (DifferentZone differentZone : differentZones) {
                        if (zoneId.equals(differentZone.zoneId)) {
                            List<CommissionServices> services = differentZone.value;
                            if (services != null && !services.isEmpty()) {
                                for (CommissionServices commissionServices : services) {
                                    if (jobType.equals(commissionServices.serviceType)) {
                                        commissionType = commissionServices.type;
                                        commission = commissionServices.value;
                                        break;
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
            }
            logger.debug(
                    etaFareEnt.requestId + " - commission type: " + commissionType + " - commission: " + commission);
            double fleetCommission = commissionType.equalsIgnoreCase("amount") ? commission
                    : commission * response.subTotal / 100;

            // check company commission
            double companyCommission = 0.0;
            String companyId = etaFareEnt.companyId != null ? etaFareEnt.companyId : "all";
            if (!companyId.equals("all")) {
                Company company = mongoDao.getCompanyById(companyId);
                if (company != null && company.commissionValue > 0) {
                    companyCommission = company.commissionValue * (response.subTotal - fleetCommission) / 100;
                }
                logger.debug(etaFareEnt.requestId + " - companyCommission value: " + companyCommission);
            }
            response.driverEarningType = driverEarningType;
            response.editedDriverEarning = editedDriverEarning;
            response.originalDriverEarning = CommonUtils
                    .getRoundValue(response.subTotal - fleetCommission - companyCommission);
        }
        return response;
    }

    public FareReturn calFareAffiliate(Fleet fleet, RateGeneral rate, ETAFareEnt etaFareEnt,
            String bookType, Date pickupDate, Date desDate, String fareNormalId,
            String fareFlatId, int numOfDays, String currencyISO, String timeZoneId,
            String requestId, boolean compareVersion, String priceType) throws ParseException {
        FareReturn fareReturn = new FareReturn();

        // Calculator Basic fare
        List<Object> dataFare = getBasicFareAffiliate(numOfDays, etaFareEnt, fareNormalId, fareFlatId, bookType,
                priceType, requestId);
        double basicFare = Double.parseDouble(dataFare.get(0).toString());
        boolean normalFare = Boolean.parseBoolean(dataFare.get(1).toString());
        String route = dataFare.get(2).toString();
        String routeId = dataFare.get(4).toString();
        double minimum = Double.parseDouble(dataFare.get(3).toString());
        // check round down setting
        if (fleet.roundDown)
            basicFare = (int) basicFare;
        double airportFee = 0;
        double meetDriverFee = 0;
        double rushHourFee = 0;
        double techFee = 0;
        double bookingFee = 0;
        double taxFee = 0;
        double tipFee = 0;
        double otherFees = 0;
        double subTotal = 0;
        boolean min = false;
        boolean bookingFeeActive = false;
        double etaFare = 0;
        double taxValue = 0;
        double tipValue = 0;
        AffiliateRoute affiliateRoute = null;
        AffiliatePackages affiliatePackages = null;
        if (!routeId.isEmpty())
            affiliateRoute = mongoDao.getAffiliateRouteById(routeId);
        if (etaFareEnt.packageRateId != null && !etaFareEnt.packageRateId.isEmpty()) {
            affiliatePackages = mongoDao.getPackageRateByIdAffiliate(etaFareEnt.packageRateId);
        }
        if (basicFare > 0) {
            // get basic fare completed, get fare setting
            subTotal = basicFare;
            if (rate != null) {
                // get airport fee
                boolean checkDes = false;
                AirportZone airportZone = mongoDao.checkDestinationByGeo(etaFareEnt.destination);
                if (airportZone != null)
                    checkDes = true;
                if (etaFareEnt.bookType == 1 && rate.airport != null && rate.airport.fromAirportActive) {
                    airportFee = rate.airport.fromAirport != null ? rate.airport.fromAirport : 0;
                }
                if (rate.airport != null && rate.airport.toAirportActive && fleet.additionalService.toAirport) {
                    if (checkDes) {
                        airportFee += rate.airport.toAirport != null ? rate.airport.toAirport : 0;
                    } else if (etaFareEnt.bookType == 1 && etaFareEnt.typeRate == 2) {
                        airportFee += rate.airport.toAirport != null ? rate.airport.toAirport : 0;
                    }
                }
                if (!normalFare && affiliateRoute != null && affiliateRoute.otherFees != null &&
                        affiliateRoute.otherFees.isAirportFee) {
                    airportFee = 0;
                }
                if ((etaFareEnt.bookType == 3 || etaFareEnt.typeRate == 1) && affiliatePackages != null
                        && affiliatePackages.otherFees != null &&
                        affiliatePackages.otherFees.isAirportFee)
                    airportFee = 0;
                // get rush hour fee
                if (normalFare && rate.rushHour != null && rate.rushHour && rate.rushHours != null
                        && !rate.rushHours.isEmpty() && pickupDate != null) {
                    List<Object> getRushHour = getRushHourAffiliate(fleet, rate.rushHours, pickupDate, desDate,
                            timeZoneId, timeZoneId, requestId);
                    if (getRushHour.get(0).toString().equalsIgnoreCase("amount")) {
                        rushHourFee = Double.parseDouble(getRushHour.get(1).toString());
                    } else {
                        rushHourFee = (Double.parseDouble(getRushHour.get(1).toString()) * basicFare) / 100;
                    }
                }

                // get meet driver fee
                if (fleet.additionalService.fromAirport.meetDriver && rate.meetDriver != null) {
                    if (etaFareEnt.meetDriver == 0)
                        meetDriverFee = rate.meetDriver.onCurb != null ? rate.meetDriver.onCurb : 0;
                    else if (etaFareEnt.meetDriver == 1)
                        meetDriverFee = rate.meetDriver.meetDrv != null ? rate.meetDriver.meetDrv : 0;
                }
                if (!normalFare && affiliateRoute != null && affiliateRoute.otherFees != null &&
                        affiliateRoute.otherFees.isMeetDriverFee) {
                    meetDriverFee = 0;
                }
                if ((etaFareEnt.bookType == 3 || etaFareEnt.typeRate == 1) && affiliatePackages != null
                        && affiliatePackages.otherFees != null &&
                        affiliatePackages.otherFees.isMeetDriverFee)
                    meetDriverFee = 0;
                // get tech fee
                if (normalFare && rate.techFee != null && rate.techFee.enable != null && rate.techFee.enable) {
                    if (KeysUtil.web_booking.equals(etaFareEnt.bookFrom))
                        techFee = rate.techFee.webBooking != null ? rate.techFee.webBooking : 0;
                    else if (KeysUtil.command_center.equals(etaFareEnt.bookFrom))
                        techFee = rate.techFee.commandCenter != null ? rate.techFee.commandCenter : 0;
                    else if (KeysUtil.dash_board.equals(etaFareEnt.bookFrom)
                            || KeysUtil.corp_board.equalsIgnoreCase(etaFareEnt.bookFrom))
                        techFee = rate.techFee.bookingDashboard != null ? rate.techFee.bookingDashboard : 0;
                    else if (!bookPax.contains(etaFareEnt.bookFrom))
                        techFee = rate.techFee.paxApp != null ? rate.techFee.paxApp : 0;
                }

                //  calculator sub total
                subTotal = basicFare + CommonUtils.getRoundValue(rushHourFee) + otherFees;

                if (minimum > 0 && subTotal < minimum) {
                    subTotal = minimum;
                    min = true;
                }

                // get tax fee
                if (rate.tax != null && rate.tax.enable != null && rate.tax.enable && rate.tax.surcharge != null) {
                    taxValue = rate.tax.surcharge;
                    taxFee = (rate.tax.surcharge * subTotal) / 100;
                }
                if (!normalFare && affiliateRoute != null && affiliateRoute.otherFees != null &&
                        affiliateRoute.otherFees.isTax) {
                    taxValue = 0;
                    taxFee = 0;
                }
                if ((etaFareEnt.bookType == 3 || etaFareEnt.typeRate == 1) && affiliatePackages != null
                        && affiliatePackages.otherFees != null &&
                        affiliatePackages.otherFees.isTax) {
                    taxValue = 0;
                    taxFee = 0;
                }

                // get tip fee
                if (normalFare && rate.tip != null && rate.tip.enable != null && rate.tip.enable
                        && rate.tip.value != null) {
                    tipValue = rate.tip.value;
                    tipFee = (rate.tip.value * subTotal) / 100;
                    if (etaFareEnt.tip >= 0) {
                        tipValue = etaFareEnt.tip;
                        tipFee = (etaFareEnt.tip * subTotal) / 100;
                    }
                }
            }
            if (minimum > 0 && subTotal < minimum) {
                subTotal = minimum;
                min = true;
            }
        }
        fareReturn.basicFare = CommonUtils.getRoundValue(basicFare);
        fareReturn.normalFare = normalFare;
        fareReturn.route = route;
        fareReturn.airportFee = airportFee;
        fareReturn.rushHourFee = CommonUtils.getRoundValue(rushHourFee);
        fareReturn.meetDriverFee = meetDriverFee;
        fareReturn.techFee = techFee;
        fareReturn.otherFees = otherFees;
        fareReturn.minFare = minimum;
        fareReturn.min = min;
        fareReturn.bookingFee = CommonUtils.getRoundValue(bookingFee);
        fareReturn.bookingFeeActive = bookingFeeActive;
        fareReturn.tax = CommonUtils.getRoundValue(taxFee);
        fareReturn.tip = CommonUtils.getRoundValue(tipFee);
        fareReturn.taxValue = taxValue;
        fareReturn.tipValue = tipValue;
        etaFare = subTotal + techFee + CommonUtils.getRoundValue(bookingFee) + CommonUtils.getRoundValue(taxFee)
                + CommonUtils.getRoundValue(tipFee);
        etaFare = etaFare + airportFee + meetDriverFee;
        if (etaFare < 0)
            etaFare = 0;
        fareReturn.etaFare = CommonUtils.getRoundValue(etaFare);
        fareReturn.totalWithoutPromo = CommonUtils.getRoundValue(etaFare);

        return fareReturn;
    }

    public List<Object> getRushHour(Fleet fleet, List<RushHour> rushHours, Date startTime, Date endTime,
            String currencyISO, String pickupTz, String dropOffTz, double distance, String surchargeTypeRate,
            String fareType, boolean isReservation, String requestId) throws ParseException {
        List<Object> data = new ArrayList<Object>();
        double rushHourFee = 0;
        String surchargeType = "amount";
        if (pickupTz.isEmpty())
            pickupTz = fleet.timezone;
        if (dropOffTz.isEmpty())
            dropOffTz = fleet.timezone;
        List<RushHour> applyRushHours = new ArrayList<>();
        List<RushHour> applyRushHoursSingle = new ArrayList<>();
        List<RushHour> applyRushHoursYearly = new ArrayList<>();
        try {
            Date pickupTime = TimezoneUtil.offsetTimeZone(startTime, "GMT", pickupTz);
            Date dropOffTime = TimezoneUtil.offsetTimeZone(endTime, "GMT", dropOffTz);
            String startDay = ConvertUtil.getDay(pickupTime);
            String endDay = ConvertUtil.getDay(dropOffTime);
            int minutesPickup = ConvertUtil.getMinutesTime(pickupTime);
            int minutesDropOff = ConvertUtil.getMinutesTime(dropOffTime);
            logger.debug(requestId + " - " + "minutesPickup:    " + minutesPickup);
            logger.debug(requestId + " - " + "minutesDropOff:    " + minutesDropOff);
            if (rushHours == null) {
                rushHours = new ArrayList<>();
            }
            for (RushHour r : rushHours) {
                int timeHourStart = 0;
                int timeMinuteStart = 0;
                int timeHourEnd = 0;
                int timeMinuteEnd = 0;
                String[] timeHFrom = r.start.split(":");
                if (timeHFrom.length > 1) {
                    timeHourStart = Integer.valueOf(timeHFrom[0]);
                    timeMinuteStart = Integer.valueOf(timeHFrom[1]);
                }
                String[] timeHTo = r.end.split(":");
                if (timeHTo.length > 1) {
                    timeHourEnd = Integer.valueOf(timeHTo[0]);
                    timeMinuteEnd = Integer.valueOf(timeHTo[1]);
                }
                int minutesStart = (timeHourStart * 60) + timeMinuteStart;
                int minutesEnd = (timeHourEnd * 60) + timeMinuteEnd;
                if (timeHourEnd < timeHourStart)
                    minutesEnd = 1440 + minutesEnd;
                logger.debug(requestId + " - " + "minutesStart:    " + minutesStart);
                logger.debug(requestId + " - " + "minutesEnd:    " + minutesEnd);
                if (r.repeat.equals("single") || r.repeat.equals("yearly")) {
                    Date startDate = null;
                    if (r.startDate != null && !r.startDate.isEmpty()) {
                        startDate = sdfSaveRedis.parse(r.startDate);
                        logger.debug(requestId + " - " + "startDate:    " + startDate);
                    }
                    if (r.repeat.equals("yearly")) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(pickupTime);
                        startDate = ConvertUtil.customizeDateByCurrentYear(startDate, cal.get(Calendar.YEAR));
                        logger.debug(requestId + " - " + "startDate with pickup year:    " + startDate);
                    }
                    if (DateUtils.isSameDay(startDate, pickupTime)) {
                        if (ConvertUtil.getDay(startDate).equals(startDay)
                                || ConvertUtil.getDay(startDate).equals(endDay)) {
                            if (minutesStart == 0 && minutesEnd == 0) {

                            } else {
                                double min = r.timeCal;
                                if (min >= 0) {
                                    if (!startDay.equals(endDay)) {
                                        if (1440 - minutesPickup >= min) {
                                            if (1440 - minutesStart >= min && minutesEnd - minutesPickup >= min) {
                                                if (r.repeat.equals("single"))
                                                    applyRushHoursSingle.add(r);
                                                else
                                                    applyRushHoursYearly.add(r);
                                            } else if (minutesPickup >= minutesStart && 1440 <= minutesEnd) {
                                                if (r.repeat.equals("single"))
                                                    applyRushHoursSingle.add(r);
                                                else
                                                    applyRushHoursYearly.add(r);
                                            } else if (minutesPickup <= minutesStart && 1440 >= minutesEnd) {
                                                if (r.repeat.equals("single"))
                                                    applyRushHoursSingle.add(r);
                                                else
                                                    applyRushHoursYearly.add(r);
                                            }
                                        }
                                        if (minutesDropOff >= min) {
                                            if (minutesDropOff - minutesStart >= min && minutesEnd >= min) {
                                                if (r.repeat.equals("single"))
                                                    applyRushHoursSingle.add(r);
                                                else
                                                    applyRushHoursYearly.add(r);
                                            } else if (0 >= minutesStart && minutesDropOff <= minutesEnd) {
                                                if (r.repeat.equals("single"))
                                                    applyRushHoursSingle.add(r);
                                                else
                                                    applyRushHoursYearly.add(r);
                                            } else if (0 <= minutesStart && minutesDropOff >= minutesEnd) {
                                                if (r.repeat.equals("single"))
                                                    applyRushHoursSingle.add(r);
                                                else
                                                    applyRushHoursYearly.add(r);
                                            }
                                        }
                                    } else {
                                        if (minutesDropOff - minutesPickup >= min) {
                                            if (timeHourEnd < timeHourStart) {
                                                minutesEnd = minutesEnd + 1440;
                                            }
                                            if (timeHourEnd == timeHourStart && timeMinuteStart > timeMinuteEnd) {
                                                minutesEnd = minutesEnd + 1440;
                                            }
                                            if (minutesDropOff - minutesStart >= min
                                                    && minutesEnd - minutesPickup >= min) {
                                                if (r.repeat.equals("single"))
                                                    applyRushHoursSingle.add(r);
                                                else
                                                    applyRushHoursYearly.add(r);
                                            } else if (minutesPickup >= minutesStart && minutesDropOff <= minutesEnd) {
                                                if (r.repeat.equals("single"))
                                                    applyRushHoursSingle.add(r);
                                                else
                                                    applyRushHoursYearly.add(r);
                                            } else if (minutesPickup <= minutesStart && minutesDropOff >= minutesEnd) {
                                                if (r.repeat.equals("single"))
                                                    applyRushHoursSingle.add(r);
                                                else
                                                    applyRushHoursYearly.add(r);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (r.repeat.equals("weekly")) {
                    int start = 0;
                    if (r.date.contains(startDay))
                        start = 1;
                    int end = 0;
                    if (r.date.contains(endDay))
                        end = 1;
                    if (timeHourEnd < timeHourStart) {
                        for (String date : r.date) {
                            if (ConvertUtil.getNextDay(date).equals(endDay)) {
                                end = 1;
                                break;
                            }
                        }
                    }
                    if (start == 1 || end == 1) {
                        if (minutesStart == 0 && minutesEnd == 0) {

                        } else {
                            double min = r.timeCal;
                            if (min >= 0) {
                                if (!startDay.equals(endDay)) {
                                    if (1440 - minutesPickup >= min) {
                                        if (1440 - minutesStart >= min && minutesEnd - minutesPickup >= min) {
                                            applyRushHours.add(r);
                                        } else if (minutesPickup >= minutesStart && 1440 <= minutesEnd) {
                                            applyRushHours.add(r);
                                        } else if (minutesPickup <= minutesStart && 1440 >= minutesEnd) {
                                            applyRushHours.add(r);
                                        }
                                    }
                                    if (minutesDropOff >= min && end == 1) {
                                        if (minutesDropOff - minutesStart >= min && minutesEnd >= min) {
                                            applyRushHours.add(r);
                                        } else if (0 >= minutesStart && minutesDropOff <= minutesEnd) {
                                            applyRushHours.add(r);
                                        } else if (0 <= minutesStart && minutesDropOff >= minutesEnd) {
                                            applyRushHours.add(r);
                                        }
                                    }
                                } else {
                                    if (minutesDropOff - minutesPickup >= min) {
                                        double minPickup = minutesPickup;
                                        double minDropoff = minutesDropOff;
                                        if (timeHourEnd < timeHourStart && start != 1 && end == 1) {
                                            minPickup = minutesPickup + 1440;
                                            minDropoff = minutesDropOff + 1440;
                                        }
                                        if (minutesStart < 1440 && 1440 < minutesEnd && minutesDropOff < minutesStart) {
                                            minPickup = minutesPickup + 1440;
                                            minDropoff = minutesDropOff + 1440;
                                        }
                                        if (minDropoff - minutesStart >= min && minutesEnd - minPickup >= min) {
                                            applyRushHours.add(r);
                                        } else if (minPickup >= minutesStart && minDropoff <= minutesEnd) {
                                            applyRushHours.add(r);
                                        } else if (minPickup <= minutesStart && minDropoff >= minutesEnd) {
                                            applyRushHours.add(r);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            logger.debug(requestId + " - applyRushHoursSingle: " + applyRushHoursSingle.size());
            logger.debug(requestId + " - applyRushHoursYearly: " + applyRushHoursYearly.size());
            logger.debug(requestId + " - applyRushHours: " + applyRushHours.size());
            if (!applyRushHoursSingle.isEmpty()) {
                applyRushHours = applyRushHoursSingle;
            } else if (!applyRushHoursYearly.isEmpty()) {
                applyRushHours = applyRushHoursYearly;
            }
            logger.debug(requestId + " - applyRushHours: " + applyRushHours.size());
            if (!applyRushHours.isEmpty()) {
                List<RushHour> rushHoursByPercent = new ArrayList<>();
                for (RushHour r : applyRushHours) {
                    logger.debug(requestId + " - rushHours: " + gson.toJson(r));
                    double surcharge = 0;
                    boolean isOldSetting = false;
                    List<SurchargeByCurrency> listSurcharges;
                    boolean differentSurchargeForDifferentService = fleet.generalSetting.differentSurchargeForDifferentService;
                    if (!surchargeTypeRate.isEmpty() && differentSurchargeForDifferentService) {
                        if (isReservation || surchargeTypeRate.equals("shuttle")) {
                            listSurcharges = r.surchargeReservation;
                        } else {
                            listSurcharges = r.surchargeNow;
                        }
                        if (listSurcharges == null || listSurcharges.isEmpty()) {
                            listSurcharges = r.surchargeByCurrencies;
                            isOldSetting = true;
                        }
                    } else {
                        // can not get surchargeTypeRate - use old setting
                        listSurcharges = r.surchargeByCurrencies;
                        isOldSetting = true;
                    }
                    logger.debug(requestId + " - isOldSetting: " + isOldSetting);
                    logger.debug(requestId + " - listSurcharges: " + listSurcharges.size());
                    if (listSurcharges != null && !listSurcharges.isEmpty()) {
                        if (isOldSetting) {
                            SurchargeByCurrency rushHourSurcharge = listSurcharges.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                    .findFirst().orElse(null);
                            logger.debug(requestId + " - rushHourSurcharge: " + gson.toJson(rushHourSurcharge));
                            if (rushHourSurcharge != null) {
                                if (r.distanceDiversity) {
                                    logger.debug(requestId + " - " + "get surcharge by distance: " + distance);
                                    surcharge = getSurchargeByDistance(distance, rushHourSurcharge);
                                } else {
                                    surcharge = rushHourSurcharge.value;
                                }
                            }
                        } else {
                            logger.debug(requestId + " - fareType: " + fareType);
                            logger.debug(requestId + " - isReservation: " + isReservation);
                            SurchargeByCurrency rushHourSurcharge = listSurcharges.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                    .findFirst().orElse(null);
                            if (rushHourSurcharge != null) {
                                if (r.distanceDiversity) {
                                    logger.debug(requestId + " - " + "get surcharge by distance: " + distance);
                                    surcharge = getSurchargeByDistance(distance, rushHourSurcharge, fareType);
                                } else {
                                    SurchargeByService surchargeByService = rushHourSurcharge.surchargeByServices
                                            .stream()
                                            .filter(surchargeService -> surchargeService.typeRate.equals(fareType))
                                            .findFirst().orElse(null);
                                    surcharge = surchargeByService != null ? surchargeByService.value : 0.0;
                                }
                            }
                        }
                        logger.debug(requestId + " - surcharge: " + surcharge);
                    }
                    if (r.surchargeType == null || r.surchargeType.equalsIgnoreCase("amount")) {
                        rushHourFee += surcharge;
                    } else {
                        rushHoursByPercent.add(r);
                    }
                }
                logger.debug(requestId + " - surchargeType: " + surchargeType);
                logger.debug(requestId + " - rushHourFee: " + rushHourFee);
                logger.debug(requestId + " - rushHoursByPercent.size(): " + rushHoursByPercent.size());
                if (rushHoursByPercent.size() > 0) {
                    // get maximum percentage
                    rushHourFee = 0;
                    surchargeType = "percent";
                    for (RushHour r : rushHoursByPercent) {
                        logger.debug(requestId + " - rushHoursByPercent: " + gson.toJson(r));
                        double surcharge = 0;
                        boolean isOldSetting = false;
                        List<SurchargeByCurrency> listSurcharges;

                        if (isReservation || surchargeTypeRate.equals("shuttle")) {
                            listSurcharges = r.surchargeReservation;
                        } else {
                            listSurcharges = r.surchargeNow;
                        }
                        if (listSurcharges == null || listSurcharges.isEmpty()) {
                            listSurcharges = r.surchargeByCurrencies;
                            isOldSetting = true;
                        }
                        if (isOldSetting) {
                            if (listSurcharges != null && !listSurcharges.isEmpty()) {
                                if (r.distanceDiversity) {
                                    logger.debug(requestId + " - get surcharge by distance: " + distance);
                                    surcharge = getSurchargeByDistance(distance, listSurcharges.get(0));
                                } else {
                                    surcharge = listSurcharges.get(0).value;
                                }
                            }
                            if (surcharge > rushHourFee) {
                                rushHourFee = surcharge;
                            }
                        } else {
                            if (listSurcharges != null && !listSurcharges.isEmpty()) {
                                logger.debug(requestId + " - fareType: " + fareType);
                                if (r.distanceDiversity) {
                                    logger.debug(requestId + " - get surcharge by distance: " + distance);
                                    surcharge = getSurchargeByDistance(distance, listSurcharges.get(0), fareType);
                                } else {
                                    SurchargeByService surchargeByService = listSurcharges.get(0).surchargeByServices
                                            .stream()
                                            .filter(surchargeService -> surchargeService.typeRate.equals(fareType))
                                            .findFirst().orElse(null);
                                    surcharge = surchargeByService != null ? surchargeByService.value : 0.0;
                                }
                            }
                            if (surcharge > rushHourFee) {
                                rushHourFee = surcharge;
                            }
                        }
                    }
                }
                logger.debug(requestId + " - surchargeType: " + surchargeType);
                logger.debug(requestId + " - rushHourFee: " + rushHourFee);
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.debug(requestId + " - getRushHour ERROR:  " + e.getMessage());
        }
        logger.debug(requestId + " - surchargeType:  " + surchargeType + " -  surchargeFee:  " + rushHourFee);
        data.add(surchargeType);
        data.add(rushHourFee);
        return data;
    }

    public List<Object> getRushHourAffiliate(Fleet fleet, List<RushHour> rushHours, Date startTime, Date endTime,
            String pickupTz, String dropOffTz, String requestId) throws ParseException {
        List<Object> data = new ArrayList<Object>();
        double rushHourFee = 0;
        String surchargeType = "amount";
        if (pickupTz.isEmpty())
            pickupTz = fleet.timezone;
        if (dropOffTz.isEmpty())
            dropOffTz = fleet.timezone;
        Date pickupTime = TimezoneUtil.offsetTimeZone(startTime, "GMT", pickupTz);
        Date dropOffTime = TimezoneUtil.offsetTimeZone(endTime, "GMT", dropOffTz);
        String startDay = ConvertUtil.getDay(pickupTime);
        String endDay = ConvertUtil.getDay(dropOffTime);
        int minutesPickup = ConvertUtil.getMinutesTime(pickupTime);
        int minutesDropOff = ConvertUtil.getMinutesTime(dropOffTime);
        logger.debug(requestId + " - " + "minutesPickup:    " + minutesPickup);
        logger.debug(requestId + " - " + "minutesDropOff:    " + minutesDropOff);
        List<RushHour> applyRushHours = new ArrayList<>();
        List<RushHour> applyRushHoursSingle = new ArrayList<>();
        List<RushHour> applyRushHoursYearly = new ArrayList<>();
        try {
            for (RushHour r : rushHours) {
                int timeHourStart = 0;
                int timeMinuteStart = 0;
                int timeHourEnd = 0;
                int timeMinuteEnd = 0;
                String[] timeHFrom = r.start.split(":");
                if (timeHFrom.length > 1) {
                    timeHourStart = Integer.valueOf(timeHFrom[0]);
                    timeMinuteStart = Integer.valueOf(timeHFrom[1]);
                }
                String[] timeHTo = r.end.split(":");
                if (timeHTo.length > 1) {
                    timeHourEnd = Integer.valueOf(timeHTo[0]);
                    timeMinuteEnd = Integer.valueOf(timeHTo[1]);
                }
                int minutesStart = (timeHourStart * 60) + timeMinuteStart;
                int minutesEnd = (timeHourEnd * 60) + timeMinuteEnd;
                if (timeHourEnd < timeHourStart)
                    minutesEnd = 1440 + minutesEnd;
                logger.debug(requestId + " - " + "minutesStart:    " + minutesStart);
                logger.debug(requestId + " - " + "minutesEnd:    " + minutesEnd);
                if ((r.repeat != null && r.repeat.equals("single"))
                        || (r.repeat != null && r.repeat.equals("yearly"))) {
                    Date startDate = null;
                    if (r.startDate != null && !r.startDate.isEmpty()) {
                        startDate = sdfSaveRedis.parse(r.startDate);
                    }
                    if (r.repeat.equals("yearly")) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(pickupTime);
                        startDate = ConvertUtil.customizeDateByCurrentYear(startDate, cal.get(Calendar.YEAR));
                    }
                    if (DateUtils.isSameDay(startDate, pickupTime)) {
                        if (ConvertUtil.getDay(startDate).equals(startDay)
                                || ConvertUtil.getDay(startDate).equals(endDay)) {
                            if (minutesStart == 0 && minutesEnd == 0) {

                            } else {
                                double min = r.timeCal;
                                if (min >= 0) {
                                    if (minutesDropOff - minutesPickup >= min) {
                                        if (minutesDropOff - minutesStart >= min && minutesEnd - minutesPickup >= min) {
                                            if (r.repeat.equals("single"))
                                                applyRushHoursSingle.add(r);
                                            else
                                                applyRushHoursYearly.add(r);
                                        } else if (minutesPickup >= minutesStart && minutesDropOff <= minutesEnd) {
                                            if (r.repeat.equals("single"))
                                                applyRushHoursSingle.add(r);
                                            else
                                                applyRushHoursYearly.add(r);
                                        } else if (minutesPickup <= minutesStart && minutesDropOff >= minutesEnd) {
                                            if (r.repeat.equals("single"))
                                                applyRushHoursSingle.add(r);
                                            else
                                                applyRushHoursYearly.add(r);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (r.repeat == null || r.repeat.equals("weekly")) {
                    int start = 0;
                    if (r.date.contains(startDay))
                        start = 1;
                    int end = 0;
                    if (r.date.contains(endDay))
                        end = 1;
                    if (timeHourEnd < timeHourStart) {
                        for (String date : r.date) {
                            if (ConvertUtil.getNextDay(date).equals(endDay)) {
                                end = 1;
                                break;
                            }
                        }
                    }
                    if (start == 1 || end == 1) {
                        if (minutesStart == 0 && minutesEnd == 0) {

                        } else {
                            double min = r.timeCal;
                            if (min >= 0) {
                                if (!startDay.equals(endDay)) {
                                    if (1440 - minutesPickup >= min) {
                                        if (1440 - minutesStart >= min && minutesEnd - minutesPickup >= min) {
                                            applyRushHours.add(r);
                                        } else if (minutesPickup >= minutesStart && 1440 <= minutesEnd) {
                                            applyRushHours.add(r);
                                        } else if (minutesPickup <= minutesStart && 1440 >= minutesEnd) {
                                            applyRushHours.add(r);
                                        }
                                    }
                                    if (minutesDropOff >= min && end == 1) {
                                        if (minutesDropOff - minutesStart >= min && minutesEnd >= min) {
                                            applyRushHours.add(r);
                                        } else if (0 >= minutesStart && minutesDropOff <= minutesEnd) {
                                            applyRushHours.add(r);
                                        } else if (0 <= minutesStart && minutesDropOff >= minutesEnd) {
                                            applyRushHours.add(r);
                                        }
                                    }
                                } else {
                                    if (minutesDropOff - minutesPickup >= min) {
                                        if (timeHourEnd < timeHourStart && start != 1 && end == 1) {
                                            minutesPickup = minutesPickup + 1440;
                                            minutesDropOff = minutesDropOff + 1440;
                                        }
                                        if (minutesStart < 1440 && 1440 < minutesEnd && minutesDropOff < minutesStart) {
                                            minutesPickup = minutesPickup + 1440;
                                            minutesDropOff = minutesDropOff + 1440;
                                        }
                                        if (minutesDropOff - minutesStart >= min && minutesEnd - minutesPickup >= min) {
                                            applyRushHours.add(r);
                                        } else if (minutesPickup >= minutesStart && minutesDropOff <= minutesEnd) {
                                            applyRushHours.add(r);
                                        } else if (minutesPickup <= minutesStart && minutesDropOff >= minutesEnd) {
                                            applyRushHours.add(r);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (!applyRushHoursSingle.isEmpty()) {
                applyRushHours = applyRushHoursSingle;
            } else if (!applyRushHoursYearly.isEmpty()) {
                applyRushHours = applyRushHoursYearly;
            }
            if (!applyRushHours.isEmpty()) {
                List<RushHour> rushHoursByPercent = new ArrayList<>();
                for (RushHour r : applyRushHours) {
                    logger.debug(requestId + " - rushHours: " + gson.toJson(r));
                    if (r.surchargeType == null || r.surchargeType.equalsIgnoreCase("amount")) {
                        rushHourFee += r.surcharge;
                    } else {
                        rushHoursByPercent.add(r);
                    }
                }
                logger.debug(requestId + " - surchargeType: " + surchargeType);
                logger.debug(requestId + " - rushHourFee: " + rushHourFee);
                logger.debug(requestId + " - rushHoursByPercent.size(): " + rushHoursByPercent.size());
                if (rushHoursByPercent.size() > 0) {
                    // get maximum percentage
                    rushHourFee = 0;
                    surchargeType = "percent";
                    for (RushHour r : applyRushHours) {
                        logger.debug(requestId + " - rushHoursByPercent: " + gson.toJson(r));
                        if (r.surchargeType.equalsIgnoreCase("percent") && r.surcharge > rushHourFee) {
                            rushHourFee = r.surcharge;
                        }
                    }
                }
                logger.debug(requestId + " - surchargeType: " + surchargeType);
                logger.debug(requestId + " - rushHourFee: " + rushHourFee);
            }

        } catch (Exception e) {
            logger.debug(requestId + " - " + "getRushHour ERROR:  " + e.getMessage());
        }
        logger.debug(requestId + " - " + "surchargeType:  " + surchargeType + " -  surchargeFee:  " + rushHourFee);
        data.add(surchargeType);
        data.add(rushHourFee);
        return data;
    }

    private double getSurchargeByDistance(double distance, SurchargeByCurrency surchargeByCurrency) {
        double surcharge = 0;
        if (surchargeByCurrency.distanceRange != null && !surchargeByCurrency.distanceRange.isEmpty()) {
            for (DistanceRange dr : surchargeByCurrency.distanceRange) {
                if (dr.from <= distance && distance <= dr.to) {
                    surcharge = dr.surchargeValue;
                }
            }
        }
        return surcharge;
    }

    private double getSurchargeByDistance(double distance, SurchargeByCurrency surchargeByCurrency,
            String surchargeTypeRate) {
        double surcharge = 0;
        if (surchargeByCurrency.distanceRange != null && !surchargeByCurrency.distanceRange.isEmpty()) {
            for (DistanceRange dr : surchargeByCurrency.distanceRange) {
                if (dr.from <= distance && distance <= dr.to) {
                    SurchargeByService surchargeByService = dr.surchargeByServices.stream()
                            .filter(surchargeService -> surchargeService.typeRate.equals(surchargeTypeRate))
                            .findFirst().orElse(null);
                    surcharge = surchargeByService != null ? surchargeByService.value : 0.0;
                }
            }
        }
        return surcharge;
    }

    public double getTransactionFee(CreditCardTransactionFee cctf, double total, int paymentMethod, String currencyISO,
            String requestId) {
        double transactionFee = 0;
        double creditTransactionFeePercent = 0.0;
        double creditTransactionFeeAmount = 0.0;
        try {
            JSONObject object = getCreditTransactionFeeSetting(cctf, paymentMethod, currencyISO, requestId);
            creditTransactionFeePercent = Double.valueOf(object.get("creditTransactionFeePercent").toString());
            creditTransactionFeeAmount = Double.valueOf(object.get("creditTransactionFeeAmount").toString());
            transactionFee = (creditTransactionFeePercent * total * 0.01) + creditTransactionFeeAmount;
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }
        logger.debug(requestId + " - " + "transactionFee by paymentMethod: " + paymentMethod + " is: "
                + CommonUtils.getRoundValue(transactionFee));
        return CommonUtils.getRoundValue(transactionFee);
    }

    public JSONObject getCreditTransactionFeeSetting(CreditCardTransactionFee cctf, int paymentMethod,
            String currencyISO, String requestId) {
        JSONObject data = new JSONObject();
        double creditTransactionFeePercent = 0.0;
        double creditTransactionFeeAmount = 0.0;
        try {
            if (cctf != null && cctf.feeByCurrencies != null && !cctf.feeByCurrencies.isEmpty()) {
                for (FeeByCurrency fee : cctf.feeByCurrencies) {
                    if (fee.currencyISO.equals(currencyISO)) {
                        if (paymentMethod == 2 || paymentMethod == 3 || paymentMethod == 4 || paymentMethod == 7
                                || paymentMethod == 9 || paymentMethod == 23) {
                            creditTransactionFeePercent = fee.creditCardPercent;
                            creditTransactionFeeAmount = fee.creditCardAmount;
                        } else if (paymentMethod == 5) {
                            creditTransactionFeePercent = fee.directBillingPercent;
                            creditTransactionFeeAmount = fee.directBillingAmount;
                        } else if (paymentMethod == 6 || paymentMethod == 12) {
                            creditTransactionFeePercent = fee.externalCardPercent;
                            creditTransactionFeeAmount = fee.externalCardAmount;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }

        data.put("creditTransactionFeePercent", creditTransactionFeePercent);
        data.put("creditTransactionFeeAmount", creditTransactionFeeAmount);
        return data;
    }

    public List<Object> getBookingFee(String userId, String corporateId, String bookFrom, String currencyISO) {
        List<Object> data = new ArrayList<Object>();
        // query databases
        Account account = null;
        if (userId != null && !userId.isEmpty())
            account = mongoDao.getAccount(userId);
        Corporate corporate = null;
        if (corporateId != null && !corporateId.isEmpty())
            corporate = mongoDao.getCorporate(corporateId);
        String type = "amount";
        double value = 0;
        boolean active = false;
        boolean applyCorpCommission = applyCorpCommission("", corporate, bookFrom);
        logger.debug(corporateId + " - applyCorpCommission: " + applyCorpCommission);
        if (KeysUtil.mDispatcher.equalsIgnoreCase(bookFrom) || KeysUtil.PWA.equalsIgnoreCase(bookFrom)) {
            if (account != null) {
                if (account.isActive && account.mDispatcherInfo != null) {
                    type = account.mDispatcherInfo.commissionType != null ? account.mDispatcherInfo.commissionType
                            : "amount";
                    if (account.mDispatcherInfo.commissionByCurrencies != null
                            && !account.mDispatcherInfo.commissionByCurrencies.isEmpty()) {
                        if (type.equalsIgnoreCase("amount")) {
                            AmountByCurrency commission = account.mDispatcherInfo.commissionByCurrencies.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                    .findFirst().orElse(null);
                            if (commission != null)
                                value = commission.commissionValue;
                        } else {
                            value = account.mDispatcherInfo.commissionByCurrencies.get(0).commissionValue;
                        }
                    }
                    active = true;
                }
            }
        } else if (applyCorpCommission && corporate != null) {
            logger.debug(corporateId + " - applyCorpCommission && corporate => get commission ");
            if (corporate.commission != null) {
                if (corporate.commission.active) {
                    type = corporate.commission.type != null ? corporate.commission.type : "amount";
                    if (corporate.commission.commissionByCurrencies != null
                            && !corporate.commission.commissionByCurrencies.isEmpty()) {
                        if (type.equalsIgnoreCase("amount")) {
                            AmountByCurrency commission = corporate.commission.commissionByCurrencies.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                    .findFirst().orElse(null);
                            if (commission != null)
                                value = commission.commissionValue;
                        } else {
                            value = corporate.commission.commissionByCurrencies.get(0).commissionValue;
                        }
                    }
                }
                active = corporate.commission.active;
            }
        }
        data.add(type);
        data.add(value);
        data.add(active);
        return data;
    }

    public JSONObject getPromoCode(String requestId, String fleetId, String promoCode, String currencyISO) {
        JSONObject data = new JSONObject();
        String type = "amount";
        double value = 0;
        double maximumValue = 0;
        boolean keepMinFee = false;
        Fleet fleet = mongoDao.getFleetInfor(fleetId);
        if (promoCode != null && !promoCode.isEmpty() && !currencyISO.isEmpty()) {
            PromotionCode promotionCode = mongoDao.getPromoCodeByFleetAndName(fleetId, promoCode.toUpperCase());
            logger.debug(requestId + " - promotionCode: " + gson.toJson(promotionCode));
            if (promotionCode != null) {
                type = promotionCode.type;
                if (fleet.multiCurrencies && type.equalsIgnoreCase("Amount")) {
                    for (AmountByCurrency amountByCurrency : promotionCode.valueByCurrencies) {
                        if (amountByCurrency.currencyISO.equals(currencyISO))
                            value = amountByCurrency.value;
                    }
                } else {
                    value = promotionCode.valueByCurrencies.get(0).value;
                }
                // check maximum
                if (type.equals("Percent") && promotionCode.valueLimitPerUse != null
                        && promotionCode.valueLimitPerUse.isLimited) {
                    maximumValue = promotionCode.valueLimitPerUse.value;
                }
                keepMinFee = promotionCode.keepMinFee;
            }
            logger.debug(requestId + " - type: " + type);
            logger.debug(requestId + " - value: " + value);
        }
        data.put("type", type);
        data.put("value", value);
        data.put("maximumValue", maximumValue);
        data.put("keepMinFee", keepMinFee);
        return data;
    }

    public JSONObject getPromoCode(String requestId, String fleetId, String userId, String promoCode,
            String currencyISO, double fare, String bookId) {
        JSONObject data = new JSONObject();
        String type = "amount";
        double value = 0;
        double maximumValue = 0;
        boolean keepMinFee = false;
        Fleet fleet = mongoDao.getFleetInfor(fleetId);
        if (currencyISO.isEmpty()) {
            currencyISO = fleet.currencies.get(0).iso;
        }
        if (promoCode != null && !promoCode.isEmpty() && !currencyISO.isEmpty()) {
            PromotionCode promotionCode = mongoDao.getPromoCodeByFleetAndName(fleetId, promoCode.toUpperCase());
            if (promotionCode != null) {
                type = promotionCode.type;
                value = getPromoValue(requestId, promotionCode, userId, currencyISO, fare, bookId);

                // check maximum
                if (promotionCode.budgetLimit.isLimited) {
                    maximumValue = value;
                }
                logger.debug(requestId + " - maximumValue: " + maximumValue);
                if (type.equals("Percent") && promotionCode.valueLimitPerUse != null
                        && promotionCode.valueLimitPerUse.isLimited) {
                    maximumValue = promotionCode.valueLimitPerUse.value;
                }
                keepMinFee = promotionCode.keepMinFee;
            }
            logger.debug(requestId + " - type: " + type);
            logger.debug(requestId + " - value: " + value);
        }
        data.put("type", type);
        data.put("value", value);
        data.put("maximumValue", maximumValue);
        data.put("keepMinFee", keepMinFee);
        return data;
    }

    public double getAirportFee(Fleet fleet, String currencyISO, List<Double> pickup, List<Double> destination,
            boolean isFromAirport, boolean isRoundTrip, boolean checkDistance) {
        // get airport fee
        double airportFee = 0;
        boolean checkDes = false;
        FleetFare fleetFare = mongoDao.getFleetFare(fleet.fleetId);
        AirportZone airportZonePU = mongoDao.checkDestinationByGeo(pickup);
        AirportZone airportZoneDE = mongoDao.checkDestinationByGeo(destination);
        if (airportZoneDE != null)
            checkDes = true;
        if (fleetFare.airport != null) {
            if (fleetFare.airport.isCustomized != null && fleetFare.airport.isCustomized) {
                if (airportZonePU != null) {
                    AirportFee apFee = mongoDao.getAirportFee(fleet.fleetId, airportZonePU._id.toString());
                    if (apFee != null) {
                        if (isFromAirport && fleetFare.airport.fromAirportActive
                                && apFee.fromAirportByCurrencies != null) {
                            AmountByCurrency fromAirport = apFee.fromAirportByCurrencies.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                    .findFirst().orElse(null);
                            if (fromAirport != null)
                                airportFee = fromAirport.value;
                        }
                        if (apFee.toAirportActive && fleet.additionalService.toAirport
                                && apFee.toAirportByCurrencies != null && checkDistance) {
                            AmountByCurrency toAirport = apFee.toAirportByCurrencies.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                    .findFirst().orElse(null);
                            if (toAirport != null) {
                                if (isRoundTrip) {
                                    airportFee += toAirport.value;
                                }
                            }
                        }
                    }
                }
                if (airportZoneDE != null) {
                    AirportFee apFee = mongoDao.getAirportFee(fleet.fleetId, airportZoneDE._id.toString());
                    if (apFee != null) {
                        if (fleetFare.airport.toAirportActive && fleet.additionalService.toAirport
                                && apFee.toAirportByCurrencies != null && checkDistance) {
                            AmountByCurrency toAirport = apFee.toAirportByCurrencies.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                    .findFirst().orElse(null);
                            if (toAirport != null)
                                airportFee += toAirport.value;
                        }
                    }
                }
            } else {
                if (isFromAirport && fleetFare.airport.fromAirportActive
                        && fleetFare.airport.fromAirportByCurrencies != null) {
                    AmountByCurrency fromAirport = fleetFare.airport.fromAirportByCurrencies.stream()
                            .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                            .findFirst().orElse(null);
                    if (fromAirport != null)
                        airportFee = fromAirport.value;
                }
                if (fleetFare.airport.toAirportActive && fleet.additionalService.toAirport
                        && fleetFare.airport.toAirportByCurrencies != null && checkDistance) {
                    AmountByCurrency toAirport = fleetFare.airport.toAirportByCurrencies.stream()
                            .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                            .findFirst().orElse(null);
                    if (toAirport != null) {
                        if (checkDes) {
                            airportFee += toAirport.value;
                        }
                        if (isRoundTrip && isFromAirport) {
                            airportFee += toAirport.value;
                        }
                    }
                }
            }
        }
        return airportFee;
    }

    public double getAirportFeeWithExtraLocation(Fleet fleet, String currencyISO, List<Double> pickup,
            List<Double> destination,
            List<Double> extraLocation, boolean isFromAirport) {
        // get airport fee
        double airportFee = 0;
        boolean checkDes = false;
        FleetFare fleetFare = mongoDao.getFleetFare(fleet.fleetId);
        AirportZone airportZonePU = mongoDao.checkDestinationByGeo(pickup);
        AirportZone airportZoneDE = mongoDao.checkDestinationByGeo(destination);
        AirportZone airportZoneEX = mongoDao.checkDestinationByGeo(extraLocation);
        if (airportZoneDE != null)
            checkDes = true;
        if (fleetFare.airport != null) {
            if (fleetFare.airport.isCustomized != null && fleetFare.airport.isCustomized) {
                if (airportZonePU != null) {
                    AirportFee apFee = mongoDao.getAirportFee(fleet.fleetId, airportZonePU._id.toString());
                    if (apFee != null) {
                        if (isFromAirport && fleetFare.airport.fromAirportActive
                                && apFee.fromAirportByCurrencies != null) {
                            AmountByCurrency fromAirport = apFee.fromAirportByCurrencies.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                    .findFirst().orElse(null);
                            if (fromAirport != null)
                                airportFee = fromAirport.value;
                        }
                    }
                }
                if (airportZoneDE != null) {
                    AirportFee apFee = mongoDao.getAirportFee(fleet.fleetId, airportZoneDE._id.toString());
                    if (apFee != null) {
                        if (fleetFare.airport.toAirportActive && fleet.additionalService.toAirport
                                && apFee.toAirportByCurrencies != null) {
                            AmountByCurrency toAirport = apFee.toAirportByCurrencies.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                    .findFirst().orElse(null);
                            if (toAirport != null)
                                airportFee += toAirport.value;
                        }
                    }
                }

                if (airportZoneEX != null) {
                    AirportFee apFee = mongoDao.getAirportFee(fleet.fleetId, airportZoneEX._id.toString());
                    if (apFee != null) {
                        if (fleetFare.airport.fromAirportActive && apFee.fromAirportByCurrencies != null) {
                            AmountByCurrency fromAirport = apFee.fromAirportByCurrencies.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                    .findFirst().orElse(null);
                            if (fromAirport != null)
                                airportFee += fromAirport.value;
                        }
                        if (fleetFare.airport.toAirportActive && fleet.additionalService.toAirport
                                && apFee.toAirportByCurrencies != null) {
                            AmountByCurrency toAirport = apFee.toAirportByCurrencies.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                    .findFirst().orElse(null);
                            if (toAirport != null)
                                airportFee += toAirport.value;
                        }
                    }
                }
            } else {
                if (isFromAirport && fleetFare.airport.fromAirportActive
                        && fleetFare.airport.fromAirportByCurrencies != null) {
                    AmountByCurrency fromAirport = fleetFare.airport.fromAirportByCurrencies.stream()
                            .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                            .findFirst().orElse(null);
                    if (fromAirport != null)
                        airportFee = fromAirport.value;
                }
                if (fleetFare.airport.toAirportActive && fleet.additionalService.toAirport
                        && fleetFare.airport.toAirportByCurrencies != null) {
                    AmountByCurrency toAirport = fleetFare.airport.toAirportByCurrencies.stream()
                            .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                            .findFirst().orElse(null);
                    if (toAirport != null) {
                        if (checkDes) {
                            airportFee += toAirport.value;
                        }
                    }
                }
                if (airportZoneEX != null) {
                    if (fleetFare.airport.fromAirportActive && fleetFare.airport.fromAirportByCurrencies != null) {
                        AmountByCurrency fromAirport = fleetFare.airport.fromAirportByCurrencies.stream()
                                .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                .findFirst().orElse(null);
                        if (fromAirport != null)
                            airportFee += fromAirport.value;
                    }
                    if (fleetFare.airport.toAirportActive && fleet.additionalService.toAirport
                            && fleetFare.airport.toAirportByCurrencies != null) {
                        AmountByCurrency toAirport = fleetFare.airport.toAirportByCurrencies.stream()
                                .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                .findFirst().orElse(null);
                        if (toAirport != null) {
                            airportFee += toAirport.value;
                        }
                    }
                }
            }
        }
        return airportFee;
    }

    public List<Object> getBasicFare(int bookType, int typeRate, double durationTime, double distance, int numOfDays,
            String bookingType, List<Double> pickup, List<Double> destination, String zipCodeFrom,
            String zipCodeTo, String packageRateId, String fareNormalId, String fareFlatId,
            String currencyISO, String requestId) {
        List<Object> data = new ArrayList<Object>();
        double basicFare = 0;
        boolean normalFare = false;
        String route = "";
        String routeId = "";
        boolean reverseRoute = false;
        double minimum = 0;
        double extraDistanceFare = 0;
        double extraDurationFare = 0;
        String fareType = "";
        // query fare
        FareNormal fareNormal = null;
        if (!fareNormalId.isEmpty()) {
            fareNormal = mongoDao.getFareNormalById(fareNormalId);
        }
        FlatRoutes flatRoutes = null;
        FlatRoutes flatRoutesReturn = null;
        FlatRoutes flatRoutesRoundTrip = null;
        if (!fareFlatId.isEmpty()) {
            flatRoutes = getFareByFlatRoutes(fareFlatId, pickup, destination, zipCodeFrom, zipCodeTo);
            flatRoutesReturn = getFareByFlatRoutesReturn(fareFlatId, pickup, destination, zipCodeFrom,
                zipCodeTo);
            flatRoutesRoundTrip = getFareByFlatRoutesRoundTrip(fareFlatId, pickup, destination, zipCodeFrom,
                zipCodeTo);
        }
        // check round trip
        if (bookType == 4 || typeRate == 2) {
            if (flatRoutesRoundTrip != null) {
                List<Object> dataBaseFee = getBaseFeeFlat(durationTime, distance, flatRoutesRoundTrip, false, true,
                        currencyISO, requestId);
                basicFare = Double.valueOf(dataBaseFee.get(0).toString());
                route = dataBaseFee.get(1).toString();
                routeId = dataBaseFee.get(2).toString();
            }
            logger.debug(requestId + " - " + "basicFare by flat fare with round trip: " + basicFare);
            fareType = "flat";
        } else if (bookType == 3 || typeRate == 1) {
            List<Object> dataBaseFee = getBaseFeeHourly(durationTime, distance, numOfDays, packageRateId, currencyISO,
                    requestId);
            basicFare = Double.valueOf(dataBaseFee.get(0).toString());
            extraDistanceFare = Double.valueOf(dataBaseFee.get(1).toString());
            extraDurationFare = Double.valueOf(dataBaseFee.get(2).toString());
            fareType = "hourly";
        } else {
            // return null if not match with any flat rate and not assign regular rate
            if (flatRoutes == null && flatRoutesReturn == null && fareNormal == null)
                return null;
            // check flat fare
            if (flatRoutes != null) {
                List<Object> dataBaseFee = getBaseFeeFlat(durationTime, distance, flatRoutes, false, false, currencyISO,
                        requestId);
                basicFare = Double.valueOf(dataBaseFee.get(0).toString());
                route = dataBaseFee.get(1).toString();
                routeId = dataBaseFee.get(2).toString();
            } else if (flatRoutesReturn != null) {
                List<Object> dataBaseFee = getBaseFeeFlat(durationTime, distance, flatRoutesReturn, true, false,
                        currencyISO, requestId);
                basicFare = Double.valueOf(dataBaseFee.get(0).toString());
                route = dataBaseFee.get(1).toString();
                routeId = dataBaseFee.get(2).toString();
                reverseRoute = true;
            }
            fareType = "flat";
            logger.debug(requestId + " - " + "basicFare by flat fare: " + basicFare);
            if (fareNormal != null && basicFare == 0) {
                normalFare = true;
                List<Object> dataBaseFee = getBaseFeeRegular(durationTime, distance, fareNormal, bookingType,
                        currencyISO);
                basicFare = Double.valueOf(dataBaseFee.get(0).toString());
                minimum = Double.valueOf(dataBaseFee.get(1).toString());
                logger.debug(requestId + " - " + "basicFare by regular fare: " + basicFare);
                fareType = "regular";
            }
        }
        data.add(basicFare);
        data.add(normalFare);
        data.add(route);
        data.add(minimum);
        data.add(extraDistanceFare);
        data.add(extraDurationFare);
        data.add(routeId);
        data.add(reverseRoute);
        data.add(fareType);
        return data;
    }

    public List<Object> getBasicFareForFlat(double durationTime, double distance,
            List<Double> pickup, List<Double> destination, String zipCodeFrom,
            String zipCodeTo, String fareFlatId, String currencyISO, String requestId) {
        List<Object> data = new ArrayList<Object>();
        double basicFare = 0;
        String route = "";
        String routeId = "";
        // query fare
        FlatRoutes flatRoutes = getFareByFlatRoutes(fareFlatId, pickup, destination, zipCodeFrom, zipCodeTo);
        logger.debug(requestId + " - flatRoutes: " + gson.toJson(flatRoutes));
        FlatRoutes flatRoutesReturn = getFareByFlatRoutesReturn(fareFlatId, pickup, destination, zipCodeFrom,
                zipCodeTo);
        logger.debug(requestId + " - flatRoutesReturn: " + gson.toJson(flatRoutesReturn));
        // check flat fare
        if (flatRoutes != null) {
            List<Object> dataBaseFee = getBaseFeeFlat(durationTime, distance, flatRoutes, false, false, currencyISO,
                    requestId);
            basicFare = Double.valueOf(dataBaseFee.get(0).toString());
            route = dataBaseFee.get(1).toString();
            routeId = dataBaseFee.get(2).toString();
        } else if (flatRoutesReturn != null) {
            List<Object> dataBaseFee = getBaseFeeFlat(durationTime, distance, flatRoutesReturn, true, false,
                    currencyISO, requestId);
            basicFare = Double.valueOf(dataBaseFee.get(0).toString());
            route = dataBaseFee.get(1).toString();
            routeId = dataBaseFee.get(2).toString();
        }
        logger.debug(requestId + " - basicFare by flat fare: " + basicFare);
        data.add(basicFare);
        data.add(route);
        data.add(routeId);
        return data;
    }

    public List<Object> getBasicFareByMeter(int bookType, int typeRate, double durationTime, double distance,
            int numOfDays, String bookingType, String packageRateId, String fareNormalId,
            String flatRouteId, boolean isReverseRoute, String currencyISO, String requestId) {
        List<Object> data = new ArrayList<Object>();
        double basicFare = 0;
        boolean normalFare = false;
        String route = "";
        String routeId = "";
        boolean reverseRoute = false;
        double minimum = 0;
        double extraDistanceFare = 0;
        double extraDurationFare = 0;
        String fareType = "";
        // query fare
        FareNormal fareNormal = mongoDao.getFareNormalById(fareNormalId);
        FlatRoutes flatRoutes = null;
        if (flatRouteId != null && !flatRouteId.isEmpty()) {
            flatRoutes = mongoDao.getRouteById(flatRouteId);
            if (flatRoutes != null) {
                // check round trip
                boolean roundTrip = false;
                if (bookType == 4 || typeRate == 2) {
                    roundTrip = true;
                }
                List<Object> dataBaseFee = getBaseFeeFlat(durationTime, distance, flatRoutes, isReverseRoute, roundTrip,
                        currencyISO, requestId);
                basicFare = Double.valueOf(dataBaseFee.get(0).toString());
                route = dataBaseFee.get(1).toString();
                routeId = dataBaseFee.get(2).toString();
                reverseRoute = isReverseRoute;
                logger.debug(
                        requestId + " - " + "basicFare by flat fare: " + basicFare + " - round trip: " + roundTrip);
                fareType = "flat";
            }
        } else if (bookType == 3 || typeRate == 1) {
            List<Object> dataBaseFee = getBaseFeeHourly(durationTime, distance, numOfDays, packageRateId, currencyISO,
                    requestId);
            basicFare = Double.valueOf(dataBaseFee.get(0).toString());
            extraDistanceFare = Double.valueOf(dataBaseFee.get(1).toString());
            extraDurationFare = Double.valueOf(dataBaseFee.get(2).toString());
            fareType = "hourly";
        } else if (fareNormal != null) {
            normalFare = true;
            List<Object> dataBaseFee = getBaseFeeRegular(durationTime, distance, fareNormal, bookingType, currencyISO);
            basicFare = Double.valueOf(dataBaseFee.get(0).toString());
            minimum = Double.valueOf(dataBaseFee.get(1).toString());
            fareType = "regular";
            logger.debug(requestId + " - " + "basicFare by regular fare: " + basicFare);
        }
        data.add(basicFare);
        data.add(normalFare);
        data.add(route);
        data.add(minimum);
        data.add(extraDistanceFare);
        data.add(extraDurationFare);
        data.add(routeId);
        data.add(reverseRoute);
        data.add(fareType);
        return data;
    }

    public List<Object> getBaseFeeSharing(boolean isStarted, int numOfPax, double duration, double distance,
            String fareSharingId, String currencyISO, String requestId) {
        List<Object> data = new ArrayList<Object>();
        double baseFee = 0;
        double minimum = 0;
        // query fare
        FareSharing fareSharing = mongoDao.getFareSharingById(fareSharingId);
        if (fareSharing != null && fareSharing.feesByCurrencies != null && !fareSharing.feesByCurrencies.isEmpty()) {
            for (com.qupworld.paymentgateway.models.mongo.collections.fareSharing.FeesByCurrency feesByCurrency : fareSharing.feesByCurrencies) {
                if (feesByCurrency.currencyISO.equals(currencyISO)) {
                    minimum = feesByCurrency.minimumFee;
                    logger.debug(requestId + " - " + "minimum: " + minimum);
                    // check starting fee
                    if (isStarted)
                        baseFee += feesByCurrency.startingFee;
                    // get rate per km
                    if (feesByCurrency.feePerPax != null && !feesByCurrency.feePerPax.isEmpty()) {
                        FeePerPax feePerPax = feesByCurrency.feePerPax.stream()
                                .filter(f -> f.numOfPax == numOfPax).findAny().orElse(null);
                        if (feePerPax == null) {
                            // get biggest num of pax
                            feePerPax = feesByCurrency.feePerPax.stream().max(Comparator.comparing(f -> f.numOfPax))
                                    .get();
                        }
                        if (feePerPax != null) {
                            logger.debug(requestId + " - " + "numOfPax: " + feePerPax.numOfPax);
                            baseFee += distance * feePerPax.value;
                        }
                    }
                    // get fee per minute
                    baseFee += (duration * feesByCurrency.feePerMinute) / 60;
                }
            }
            // base fee per pax
            baseFee = baseFee / numOfPax;
            logger.debug(requestId + " - " + "baseFee by fare sharing: " + baseFee);
        }
        data.add(baseFee);
        data.add(minimum);
        return data;
    }

    public List<Object> getBaseFeeRegular(double durationTime, double distance, FareNormal fareNormal,
            String bookingType,
            String currencyISO) {
        List<Object> data = new ArrayList<Object>();
        double basicFare = 0;
        double minimum = 0;
        if (fareNormal.feesByCurrencies != null) {
            for (FeesByCurrency feesByCurrency : fareNormal.feesByCurrencies) {
                if (feesByCurrency.currencyISO.equals(currencyISO)) {
                    // get minimum
                    if (bookingType.equalsIgnoreCase(KeysUtil.TIME_BOOK_NOW)) {
                        minimum = feesByCurrency.minNow;
                    } else {
                        minimum = feesByCurrency.minReservation;
                    }
                    // get fare normal
                    if (distance >= fareNormal.firstDistanceFrom) {
                        if (distance <= fareNormal.firstDistanceTo) {
                            /** distance between from -- to */
                            basicFare += (distance - fareNormal.firstDistanceFrom) * feesByCurrency.feeFirstDistance;
                        } else if (distance >= fareNormal.secondDistanceFrom) {
                            if (distance <= fareNormal.secondDistanceTo) {
                                basicFare += ((fareNormal.firstDistanceTo - fareNormal.firstDistanceFrom)
                                        * feesByCurrency.feeFirstDistance)
                                        + ((distance - fareNormal.secondDistanceFrom)
                                                * feesByCurrency.feeSecondDistance);
                            } else {
                                /** distance > to */
                                basicFare += (((fareNormal.firstDistanceTo - fareNormal.firstDistanceFrom)
                                        * feesByCurrency.feeFirstDistance)
                                        + ((fareNormal.secondDistanceTo - fareNormal.secondDistanceFrom)
                                                * feesByCurrency.feeSecondDistance)
                                        + ((distance - fareNormal.secondDistanceTo)
                                                * feesByCurrency.feeAfterSecondDistance));
                            }
                        }
                    }
                    basicFare += (feesByCurrency.feePerMinute * durationTime) / 60;
                    if (bookingType.equalsIgnoreCase(KeysUtil.TIME_BOOK_NOW)
                            || bookingType.equalsIgnoreCase(KeysUtil.TIME_BOOK_ASAP)) {
                        basicFare += feesByCurrency.startingNow;
                    } else {
                        basicFare += feesByCurrency.startingReservation;
                    }
                }
            }
        }
        data.add(basicFare);
        data.add(minimum);
        return data;
    }

    public List<Object> getBaseFeeHourly(double durationTime, double distance, int numOfDays, String packageRateId,
            String currencyISO, String requestId) {
        List<Object> data = new ArrayList<Object>();
        double basicFare = 0;
        double extraDistanceFare = 0;
        double extraDurationFare = 0;
        PackageRate packageRate = mongoDao.getPackageRateById(packageRateId);
        if (packageRate != null && packageRate.feesByCurrencies != null && !packageRate.feesByCurrencies.isEmpty()) {
            double coveredDistance = packageRate.coveredDistance;
            for (FeesWithCurrency feesByCurrency : packageRate.feesByCurrencies) {
                if (feesByCurrency.currencyISO.equals(currencyISO)) {
                    basicFare = feesByCurrency.basedFee;
                    logger.debug(requestId + " - basicFare by fare hourly: " + basicFare);
                    if (distance > coveredDistance) {
                        extraDistanceFare = Math.ceil(distance - coveredDistance) * feesByCurrency.extraDistance;
                        basicFare = basicFare + extraDistanceFare;
                        logger.debug(requestId + " - basicFare by fare hourly with extra distance: " + basicFare);
                    }
                    if (packageRate.type.equals("hour")) {
                        // duration = durationTime/3600;
                        double durationInHour = durationTime / 3600;
                        logger.debug(requestId + " - durationInHour: " + durationInHour);
                        if (durationInHour > packageRate.duration) {
                            String extraDurationType = packageRate.extraDurationType != null
                                    ? packageRate.extraDurationType
                                    : "hour";
                            double extraDuration;
                            if (extraDurationType.equals("hour")) {
                                extraDuration = Math.ceil(durationInHour - packageRate.duration);
                            } else { // extraDurationType = minute
                                double durationInMinute = durationTime / 60;
                                double coverDurationInMinute = packageRate.duration * 60;
                                extraDuration = Math.ceil(durationInMinute - coverDurationInMinute);
                            }
                            logger.debug(requestId + " - extraDuration: " + extraDuration);
                            extraDurationFare = extraDuration * feesByCurrency.extraDuration;
                            basicFare = basicFare + extraDurationFare;
                            logger.debug(requestId + " - basicFare by fare hourly with extra duration: " + basicFare);
                        }
                    } else {
                        // duration = numOfDays
                        logger.debug(requestId + " - numOfDays: " + numOfDays);
                        if (numOfDays > packageRate.duration) {
                            extraDurationFare = Math.ceil(numOfDays - packageRate.duration)
                                    * feesByCurrency.extraDuration;
                            basicFare = basicFare + extraDurationFare;
                            logger.debug(requestId + " - basicFare by fare hourly with extra duration: " + basicFare);
                        }
                    }
                }
            }
        }
        data.add(basicFare);
        data.add(extraDistanceFare);
        data.add(extraDurationFare);
        return data;
    }

    public List<Object> getBaseFeeFlat(double durationTime, double distance, FlatRoutes flatRoutes,
            boolean reverseRoute,
            boolean roundTrip, String currencyISO, String requestId) {
        List<Object> data = new ArrayList<Object>();
        double basicFare = 0;
        String route = "";
        String routeId = "";
        if (flatRoutes.singleTrip.departureRouteByCurrencies != null && !reverseRoute) {
            for (AmountByCurrency amountByCurrency : flatRoutes.singleTrip.departureRouteByCurrencies) {
                if (amountByCurrency.currencyISO.equals(currencyISO)) {
                    basicFare = amountByCurrency.value;
                    route = flatRoutes.routeName;
                    routeId = flatRoutes._id.toString();
                }
            }
        } else if (flatRoutes.singleTrip.returnRouteByCurrencies != null) {
            for (AmountByCurrency amountByCurrency : flatRoutes.singleTrip.returnRouteByCurrencies) {
                if (amountByCurrency.currencyISO.equals(currencyISO)) {
                    basicFare = amountByCurrency.value;
                    route = flatRoutes.routeName;
                    routeId = flatRoutes._id.toString();
                }
            }
        }
        if (flatRoutes.singleTrip.limitation.limited) {
            double coveredDistance = flatRoutes.singleTrip.limitation.coveredDistance;
            double coveredTime = flatRoutes.singleTrip.limitation.coveredTime;
            if (distance > coveredDistance && flatRoutes.singleTrip.limitation.extraDistanceByCurrencies != null) {
                for (AmountByCurrency amountByCurrency : flatRoutes.singleTrip.limitation.extraDistanceByCurrencies) {
                    if (amountByCurrency.currencyISO.equals(currencyISO)) {
                        basicFare = basicFare + Math.ceil(distance - coveredDistance) * amountByCurrency.value;
                        logger.debug(requestId + " - " + "extra distance fee: " + basicFare);
                    }
                }
            }
            double duration = durationTime / 3600;
            if (duration > coveredTime && flatRoutes.singleTrip.limitation.extraTimeByCurrencies != null) {
                for (AmountByCurrency amountByCurrency : flatRoutes.singleTrip.limitation.extraTimeByCurrencies) {
                    if (amountByCurrency.currencyISO.equals(currencyISO)) {
                        basicFare = basicFare + Math.ceil(duration - coveredTime) * amountByCurrency.value;
                        logger.debug(requestId + " - " + "extra time fee: " + basicFare);
                    }
                }
            }
        }
        if (roundTrip) {
            if (flatRoutes.roundTrip.roundTripFeeByCurrencies != null) {
                for (AmountByCurrency amountByCurrency : flatRoutes.roundTrip.roundTripFeeByCurrencies) {
                    if (amountByCurrency.currencyISO.equals(currencyISO)) {
                        basicFare = amountByCurrency.value;
                        route = flatRoutes.routeName;
                        routeId = flatRoutes._id.toString();
                    }
                }
            }
            if (flatRoutes.roundTrip.limitation.limited) {
                double coveredDistance = flatRoutes.roundTrip.limitation.coveredDistance;
                double coveredTime = flatRoutes.roundTrip.limitation.coveredTime;
                if (distance > coveredDistance && flatRoutes.roundTrip.limitation.extraDistanceByCurrencies != null) {
                    for (AmountByCurrency amountByCurrency : flatRoutes.roundTrip.limitation.extraDistanceByCurrencies) {
                        if (amountByCurrency.currencyISO.equals(currencyISO)) {
                            basicFare = basicFare + Math.ceil(distance - coveredDistance) * amountByCurrency.value;
                            logger.debug(requestId + " - " + "extra distance fee: " + basicFare);
                        }
                    }
                }
                double duration = durationTime / 3600;
                if (duration > coveredTime && flatRoutes.roundTrip.limitation.extraTimeByCurrencies != null) {
                    for (AmountByCurrency amountByCurrency : flatRoutes.roundTrip.limitation.extraTimeByCurrencies) {
                        if (amountByCurrency.currencyISO.equals(currencyISO)) {
                            basicFare = basicFare + Math.ceil(duration - coveredTime) * amountByCurrency.value;
                            logger.debug(requestId + " - " + "extra time fee: " + basicFare);
                        }
                    }
                }
            }
        }
        data.add(basicFare);
        data.add(route);
        data.add(routeId);
        return data;
    }

    public List<Object> getBaseFeeOneWay(double durationTime, double distance, String bookingType, List<Double> pickup,
            List<Double> destination, String zipCodeFrom, String zipCodeTo, String fareNormalId,
            String fareFlatId, String currencyISO, String requestId) {
        List<Object> data = new ArrayList<Object>();
        double basicFare = 0;
        boolean normalFare = false;
        double minimum = 0;
        String route = "";
        String routeId = "";
        boolean reverseRoute = false;
        durationTime = durationTime >= 0 ? durationTime : 0;
        distance = distance >= 0 ? distance : 0;
        logger.debug(requestId + " - " + "distance: " + distance);
        logger.debug(requestId + " - " + "duration: " + durationTime);
        // query fare
        FareNormal fareNormal = mongoDao.getFareNormalById(fareNormalId);
        FlatRoutes flatRoutes = getFareByFlatRoutes(fareFlatId, pickup, destination, zipCodeFrom, zipCodeTo);
        FlatRoutes flatRoutesReturn = getFareByFlatRoutesReturn(fareFlatId, pickup, destination, zipCodeFrom,
                zipCodeTo);

        // check flat fare
        if (flatRoutes != null) {
            List<Object> dataBaseFee = getBaseFeeFlat(durationTime, distance, flatRoutes, false, false, currencyISO,
                    requestId);
            basicFare = Double.valueOf(dataBaseFee.get(0).toString());
        } else if (flatRoutesReturn != null) {
            List<Object> dataBaseFee = getBaseFeeFlat(durationTime, distance, flatRoutesReturn, true, false,
                    currencyISO, requestId);
            basicFare = Double.valueOf(dataBaseFee.get(0).toString());
        }
        if (flatRoutes != null) {
            List<Object> dataBaseFee = getBaseFeeFlat(durationTime, distance, flatRoutes, false, false, currencyISO,
                    requestId);
            basicFare = Double.valueOf(dataBaseFee.get(0).toString());
            route = dataBaseFee.get(1).toString();
            routeId = dataBaseFee.get(2).toString();
        } else if (flatRoutesReturn != null) {
            List<Object> dataBaseFee = getBaseFeeFlat(durationTime, distance, flatRoutesReturn, true, false,
                    currencyISO, requestId);
            basicFare = Double.valueOf(dataBaseFee.get(0).toString());
            route = dataBaseFee.get(1).toString();
            routeId = dataBaseFee.get(2).toString();
            reverseRoute = true;
        }
        logger.debug(requestId + " - " + "basicFare by flat fare: " + basicFare);
        if (basicFare == 0) {
            if (fareNormal != null) {
                normalFare = true;
                List<Object> dataBaseFee = getBaseFeeRegular(durationTime, distance, fareNormal, bookingType,
                        currencyISO);
                basicFare = Double.valueOf(dataBaseFee.get(0).toString());
                minimum = Double.valueOf(dataBaseFee.get(1).toString());
            }
        }
        data.add(basicFare);
        data.add(normalFare);
        data.add(minimum);
        data.add(route);
        data.add(routeId);
        data.add(reverseRoute);
        return data;
    }

    public List<Object> getBaseFeeIntercity(int rn, String routeId, int seat, int luggage, String tripType,
            String requestId) {
        List<Object> data = new ArrayList<Object>();
        double basicFare = 0;
        String routeName = "";
        String currencyISO = "";
        IntercityRoute ir = mongoDao.getIntercityRouteRouteById(routeId);
        if (ir != null) {
            routeName = ir.routeName;
            currencyISO = ir.currencyISO;
            if (rn == 1 && ir.routeOneSetting != null) {
                if (ir.routeOneSetting.pricePerSeat != null) {
                    for (AmountByCurrency amountByCurrency : ir.routeOneSetting.pricePerSeat) {
                        if (amountByCurrency.currencyISO.equals(currencyISO)) {
                            basicFare = amountByCurrency.value;
                        }
                    }
                }
                if (("requested").equals(tripType) && ir.routeOneSetting.requestTimeEnable
                        && ir.routeOneSetting.extraFee != null) {
                    for (AmountByCurrency amountByCurrency : ir.routeOneSetting.extraFee) {
                        if (amountByCurrency.currencyISO.equals(currencyISO)) {
                            basicFare += amountByCurrency.value;
                        }
                    }
                }
            } else if (ir.routeTwoEnable && rn == 2 && ir.routeTwoSetting != null) {
                if (ir.routeTwoSetting.pricePerSeat != null) {
                    for (AmountByCurrency amountByCurrency : ir.routeTwoSetting.pricePerSeat) {
                        if (amountByCurrency.currencyISO.equals(currencyISO)) {
                            basicFare = amountByCurrency.value;
                        }
                    }
                }
                if (("requested").equals(tripType) && ir.routeTwoSetting.requestTimeEnable
                        && ir.routeTwoSetting.extraFee != null) {
                    for (AmountByCurrency amountByCurrency : ir.routeOneSetting.extraFee) {
                        if (amountByCurrency.currencyISO.equals(currencyISO)) {
                            basicFare += amountByCurrency.value;
                        }
                    }
                }
            }
            logger.debug(requestId + " - " + "pricePerSeat: " + basicFare);
            basicFare = basicFare * seat;

            // check fee for luggage
            if (ir.limitSeatLuggage != null && ir.limitSeatLuggage.allowToAddExtraLuggage &&
                    luggage > ir.limitSeatLuggage.freeLuggagePerSeat * seat) {
                double extraLuggage = luggage - ir.limitSeatLuggage.freeLuggagePerSeat * seat;
                double feePerExtraLuggage = 0;
                if (ir.limitSeatLuggage.feePerExtraLuggage != null) {
                    for (AmountByCurrency amountByCurrency : ir.limitSeatLuggage.feePerExtraLuggage) {
                        if (amountByCurrency.currencyISO.equals(currencyISO)) {
                            feePerExtraLuggage = amountByCurrency.value;
                        }
                    }
                }
                double extraFee = feePerExtraLuggage * extraLuggage;
                logger.debug(requestId + " - " + "fee extra luggage " + extraLuggage + " : " + extraFee);
                basicFare = basicFare + extraFee;
            }
        }

        data.add(basicFare);
        data.add(routeName);
        data.add(currencyISO);
        return data;
    }

    public double getDeliveryFee(ETADeliveryEnt etaDeliveryEnt, double durationTime, double distance,
            String unitDistance, String deliveryFlatRateId, String fareDeliveryId, String currencyISO,
            int numOfRecipients, String requestId) {
        double basicFare = 0;
        double minimum = 0;
        durationTime = durationTime >= 0 ? durationTime : 0;
        distance = distance >= 0 ? distance : 0;
        // convert distance
        if (unitDistance.equalsIgnoreCase(KeysUtil.UNIT_DISTANCE_IMPERIAL)) {
            distance = ConvertUtil.round((distance / 1609.344), 2);
        } else {
            distance = ConvertUtil.round((distance / 1000), 2);
        }

        if (!deliveryFlatRateId.isEmpty()) {
            // check if food/mart then initial key
            String vehicleTypeId = etaDeliveryEnt.vehicleTypeId != null ? etaDeliveryEnt.vehicleTypeId : "";
            if (vehicleTypeId.equalsIgnoreCase("food") || vehicleTypeId.equalsIgnoreCase("mart")) {
                etaDeliveryEnt.pickup = etaDeliveryEnt.merchants.get(0);
            }
            // ignore zipCode for delivery
            String zipCodeFrom = "";
            String zipCodeTo = "";
            Fleet fleet = mongoDao.getFleetInfor(etaDeliveryEnt.fleetId);
            if (fleet.multiPointCalFareMode != null) {
                if (fleet.multiPointCalFareMode.equals("perPoint")) {
                    logger.debug(requestId + " - multiPointCalFareMode perPoint");
                    int i = 0;
                    List<Double> pickup = new ArrayList<>();
                    List<Double> destination = new ArrayList<>();
                    for (LocationDetails recipient : etaDeliveryEnt.recipients) {
                        logger.debug(requestId + " ========= IN TOTAL get fare for user number " + i);
                        if (i == 0) {
                            pickup = etaDeliveryEnt.pickup.geo;
                        } else {
                            pickup = destination;
                        }
                        destination = recipient.geo;
                        double durationTimeRange = recipient.duration;
                        // duration time in minutes -> convert to second
                        durationTimeRange *= 60;
                        double distanceRange = recipient.distance;
                        logger.debug(requestId + " --- IN TOTAL get fare for user number " + i + " durationTimeRange: "
                                + durationTimeRange);
                        distanceRange = distanceRange >= 0 ? distanceRange : 0;
                        // convert distance
                        if (unitDistance.equalsIgnoreCase(KeysUtil.UNIT_DISTANCE_IMPERIAL)) {
                            distanceRange = ConvertUtil.round((distanceRange / 1609.344), 2);
                        } else {
                            distanceRange = ConvertUtil.round((distanceRange / 1000), 2);
                        }
                        logger.debug(requestId + " --- IN TOTAL get fare for user number " + i + " distanceRange: "
                                + distanceRange);
                        logger.debug(requestId + " --- IN TOTAL get fare for user number " + i + " pickup: " + pickup);
                        logger.debug(requestId + " --- IN TOTAL get fare for user number " + i + " destination: "
                                + destination);
                        List<Object> dataFare = getBasicFareForFlat(durationTimeRange, distanceRange, pickup,
                                destination, zipCodeFrom, zipCodeTo, deliveryFlatRateId, currencyISO, requestId);
                        double basicFareRange = Double.parseDouble(dataFare.get(0).toString());
                        logger.debug(requestId + " --- IN TOTAL get fare for user number " + i + " basicFareRange: "
                                + basicFareRange);
                        basicFare += basicFareRange;
                        i++;
                    }
                } else {
                    logger.debug(requestId + " - multiPointCalFareMode start to end");
                    List<Double> pickup = etaDeliveryEnt.pickup.geo;
                    List<Double> destination = new ArrayList<>();
                    for (LocationDetails recipient : etaDeliveryEnt.recipients) {
                        destination = recipient.geo;
                    }
                    // duration time in minutes -> convert to second
                    double convertDurationTime = durationTime * 60;
                    logger.debug(requestId + " - distance: " + distance);
                    logger.debug(requestId + " - convertDurationTime: " + convertDurationTime);
                    List<Object> dataFare = getBasicFareForFlat(convertDurationTime, distance, pickup, destination,
                            zipCodeFrom, zipCodeTo, deliveryFlatRateId, currencyISO, requestId);
                    basicFare = Double.parseDouble(dataFare.get(0).toString());
                    logger.debug(requestId + " - basicFare: " + basicFare);
                }

            } else {
                List<Double> pickup = etaDeliveryEnt.pickup.geo;
                List<Double> destination = etaDeliveryEnt.recipients.get(0).geo;
                // duration time in minutes -> convert to second
                double convertDurationTime = durationTime * 60;
                logger.debug(requestId + " - distance: " + distance);
                logger.debug(requestId + " - convertDurationTime: " + convertDurationTime);
                List<Object> dataFare = getBasicFareForFlat(convertDurationTime, distance, pickup, destination,
                        zipCodeFrom, zipCodeTo, deliveryFlatRateId, currencyISO, requestId);
                basicFare = Double.parseDouble(dataFare.get(0).toString());
                logger.debug(requestId + " - basicFare: " + basicFare);
            }
        }
        logger.debug(requestId + " - basicFare by deliveryFlatRateId == " + basicFare);
        if (deliveryFlatRateId.isEmpty() || basicFare == 0) {
            logger.debug(requestId + " - " + "distance: " + distance);
            logger.debug(requestId + " - " + "duration: " + durationTime);
            // query fare
            FareDelivery fareDelivery = mongoDao.getFareDeliveryById(fareDeliveryId);
            if (fareDelivery != null && fareDelivery.feesByCurrencies != null) {
                for (com.qupworld.paymentgateway.models.mongo.collections.fareDelivery.FeesByCurrency feesByCurrency : fareDelivery.feesByCurrencies) {
                    if (feesByCurrency.currencyISO.equals(currencyISO)) {
                        // get minimum
                        minimum = feesByCurrency.feeMinimum;
                        logger.debug(requestId + " - minimum: " + minimum);
                        // first distance
                        if (distance >= fareDelivery.firstDistanceFrom) {
                            double firstDistance = distance - fareDelivery.firstDistanceFrom;
                            if (distance >= fareDelivery.secondDistanceFrom)
                                firstDistance = fareDelivery.secondDistanceFrom - fareDelivery.firstDistanceFrom;
                            if (fareDelivery.feeFirstDistanceType.equals("distance"))
                                basicFare += firstDistance * feesByCurrency.feeFirstDistance;
                            else if (fareDelivery.feeFirstDistanceType.equals("flat"))
                                basicFare += feesByCurrency.feeFirstDistance;
                        }
                        // secondDistance
                        if (distance > fareDelivery.secondDistanceFrom) {
                            double secondDistance = distance - fareDelivery.secondDistanceFrom;
                            if (distance >= fareDelivery.thirdDistanceFrom)
                                secondDistance = fareDelivery.thirdDistanceFrom - fareDelivery.secondDistanceFrom;
                            if (fareDelivery.feeSecondDistanceType.equals("distance"))
                                basicFare += secondDistance * feesByCurrency.feeSecondDistance;
                            else if (fareDelivery.feeSecondDistanceType.equals("flat"))
                                basicFare += feesByCurrency.feeSecondDistance;
                        }
                        // thirdDistance
                        if (distance > fareDelivery.thirdDistanceFrom) {
                            double thirdDistance = distance - fareDelivery.thirdDistanceFrom;
                            if (distance >= fareDelivery.thirdDistanceTo)
                                thirdDistance = fareDelivery.thirdDistanceTo - fareDelivery.thirdDistanceFrom;
                            if (fareDelivery.feeThirdDistanceType.equals("distance"))
                                basicFare += thirdDistance * feesByCurrency.feeThirdDistance;
                            else if (fareDelivery.feeThirdDistanceType.equals("flat"))
                                basicFare += feesByCurrency.feeThirdDistance;
                        }
                        // after third distance
                        if (distance > fareDelivery.thirdDistanceTo) {
                            double thirdDistance = distance - fareDelivery.thirdDistanceTo;
                            if (fareDelivery.feeAfterThirdDistanceType.equals("distance"))
                                basicFare += thirdDistance * feesByCurrency.feeAfterThirdDistance;
                            else if (fareDelivery.feeAfterThirdDistanceType.equals("flat"))
                                basicFare += feesByCurrency.feeAfterThirdDistance;
                        }
                        basicFare += (feesByCurrency.feePerMinute * durationTime);
                        basicFare += feesByCurrency.startingFee;
                        if (feesByCurrency.feeFirstStop != null) {
                            logger.debug(requestId + " - feeFirstStop: " + feesByCurrency.feeFirstStop);
                            basicFare += feesByCurrency.feeFirstStop;
                        }
                        if (numOfRecipients > 1 && feesByCurrency.feeExtraStop != null) {
                            logger.debug(requestId + " - feeExtraStop: " + feesByCurrency.feeExtraStop);
                            logger.debug(requestId + " - numOfRecipients: " + numOfRecipients);
                            basicFare += feesByCurrency.feeExtraStop * (numOfRecipients - 1);
                        }
                        if (minimum > basicFare)
                            basicFare = minimum;
                        logger.debug(requestId + " - delivery fee: " + basicFare);
                    }
                }
            }
        }
        return basicFare;
    }

    public List<Object> getBasicFareAffiliate(int numOfDays, ETAFareEnt etaFareEnt, String fareNormalId,
            String fareFlatId, String bookingType, String priceType, String requestId) {
        List<Object> data = new ArrayList<Object>();
        double basicFare = 0;
        boolean normalFare = false;
        String route = "";
        String routeId = "";
        double minimum = 0;
        double distanceByMile = ConvertUtil.round((etaFareEnt.distance / 1609.344), 2);
        double distanceByKm = ConvertUtil.round((etaFareEnt.distance / 1000), 2);
        logger.debug(requestId + " - " + "distance by mile: " + distanceByMile);
        logger.debug(requestId + " - " + "distance by km: " + distanceByKm);

        // query fare
        RateRegular fareNormal = mongoDao.getAffiliateRate(fareNormalId);
        AffiliatePackages packageRate = mongoDao.getPackageRateByIdAffiliate(etaFareEnt.packageRateId);
        AffiliateRoute flatRoutes = getFareByFlatRoutesAffiliate(fareFlatId, etaFareEnt.pickup, etaFareEnt.destination,
                etaFareEnt.zipCodeFrom, etaFareEnt.zipCodeTo);
        AffiliateRoute flatRoutesReturn = getFareByFlatRoutesReturnAffiliate(fareFlatId, etaFareEnt.pickup,
                etaFareEnt.destination,
                etaFareEnt.zipCodeFrom, etaFareEnt.zipCodeTo);
        AffiliateRoute flatRoutesRoundTrip = getFareByFlatRoutesRoundTripAffiliate(fareFlatId, etaFareEnt.pickup,
                etaFareEnt.destination,
                etaFareEnt.zipCodeFrom, etaFareEnt.zipCodeTo);

        // check round trip
        if (etaFareEnt.bookType == 4 || etaFareEnt.typeRate == 2) {
            if (flatRoutesRoundTrip != null && flatRoutesRoundTrip.roundTrip != null) {
                basicFare = flatRoutesRoundTrip.roundTrip.roundTripFee;
                route = flatRoutesRoundTrip.routeInfo.name;
                routeId = flatRoutesRoundTrip._id.toString();
                if (flatRoutesRoundTrip.roundTrip.limitation.limited) {
                    double coveredDistance = flatRoutesRoundTrip.roundTrip.limitation.coveredDistance;
                    String unitDistance = flatRoutesRoundTrip.roundTrip.limitation.unitDistance;
                    double coveredTime = flatRoutesRoundTrip.roundTrip.limitation.coveredTime;
                    if (unitDistance.equals("km")) {
                        if (distanceByKm > coveredDistance) {
                            basicFare = basicFare + Math.ceil(distanceByKm - coveredDistance)
                                    * flatRoutesRoundTrip.roundTrip.limitation.extraDistance;
                        }
                    } else {
                        coveredDistance = coveredDistance / 1.609344;
                        if (distanceByMile > coveredDistance) {
                            double extraDistance = flatRoutesRoundTrip.roundTrip.limitation.extraDistance * 1.609344;
                            logger.debug(requestId + " - " + "covered distance: " + coveredDistance);
                            logger.debug(requestId + " - " + "extra distance: " + extraDistance);
                            basicFare = basicFare + Math.ceil(distanceByMile - coveredDistance) * extraDistance;
                        }
                    }
                    logger.debug(requestId + " - " + "extra distance fee: " + basicFare);
                    double duration = etaFareEnt.duration / 3600;
                    if (duration > coveredTime) {
                        basicFare = basicFare + Math.ceil(duration - coveredTime)
                                * flatRoutesRoundTrip.roundTrip.limitation.extraTime;
                        logger.debug(requestId + " - " + "extra time fee: " + basicFare);
                    }
                }
            }
            logger.debug(requestId + " - " + "basicFare by flat fare with round trip: " + basicFare);
        } else if (etaFareEnt.bookType == 3 || etaFareEnt.typeRate == 1) {
            if (packageRate != null) {
                double coveredDistance = packageRate.distanceCovered;
                String unitDistance = packageRate.unitDistance;
                double extraDistance = 0;
                double extraDuration = 0;
                if (priceType.equals(KeysUtil.SELLPRICE)) {
                    logger.debug(requestId + " - " + " get sell price");
                    basicFare = packageRate.sellPrice.basedFee;
                    extraDistance = packageRate.sellPrice.extraDistance;
                    extraDuration = packageRate.sellPrice.extraDuration;
                } else {
                    logger.debug(requestId + " - " + " get buy price");
                    basicFare = packageRate.buyPrice.basedFee;
                    extraDistance = packageRate.buyPrice.extraDistance;
                    extraDuration = packageRate.buyPrice.extraDuration;
                }
                logger.debug(requestId + " - " + "basicFare by fare hourly: " + basicFare);
                if (unitDistance.equals("km")) {
                    if (distanceByKm > coveredDistance) {
                        basicFare = basicFare + Math.ceil(distanceByKm - coveredDistance) * extraDistance;
                    }
                } else {
                    coveredDistance = coveredDistance / 1.609344;
                    logger.debug(requestId + " - " + "covered distance: " + coveredDistance);
                    if (distanceByMile > coveredDistance) {
                        extraDistance = extraDistance * 1.609344;
                        logger.debug(requestId + " - " + "extra distance: " + extraDistance);
                        basicFare = basicFare + Math.ceil(distanceByMile - coveredDistance) * extraDistance;
                    }
                }
                logger.debug(requestId + " - " + "basicFare by fare hourly with extra distance: " + basicFare);
                double duration = 0;
                if (packageRate.type.equals("hour")) {
                    duration = etaFareEnt.duration / 3600;
                } else {
                    duration = numOfDays;
                }
                logger.debug(requestId + " - " + "real duration: " + duration);
                if (duration > packageRate.duration) {
                    basicFare = basicFare + Math.ceil(duration - packageRate.duration) * extraDuration;
                    logger.debug(requestId + " - " + "basicFare by fare hourly with extra duration: " + basicFare);
                }
            }
        } else {
            // check flat fare
            if (flatRoutes != null && flatRoutes.singleTrip != null) {
                basicFare = flatRoutes.singleTrip.departureRoute;
                route = flatRoutes.routeInfo.name;
                routeId = flatRoutes._id.toString();
                if (flatRoutes.singleTrip.limitation.limited) {
                    double coveredDistance = flatRoutes.singleTrip.limitation.coveredDistance;
                    double coveredTime = flatRoutes.singleTrip.limitation.coveredTime;
                    String unitDistance = flatRoutes.singleTrip.limitation.unitDistance;
                    if (unitDistance.equals("km")) {
                        if (distanceByKm > coveredDistance) {
                            basicFare = basicFare + Math.ceil(distanceByKm - coveredDistance)
                                    * flatRoutes.singleTrip.limitation.extraDistance;
                        }
                    } else {
                        if (distanceByMile > coveredDistance) {
                            coveredDistance = coveredDistance / 1.609344;
                            double extraDistance = flatRoutes.singleTrip.limitation.extraDistance * 1.609344;
                            logger.debug(requestId + " - " + "covered distance: " + coveredDistance);
                            logger.debug(requestId + " - " + "extra distance: " + extraDistance);
                            basicFare = basicFare + Math.ceil(distanceByMile - coveredDistance) * extraDistance;
                        }
                    }
                    logger.debug(requestId + " - " + "extra distance fee: " + basicFare);
                    double duration = etaFareEnt.duration / 3600;
                    if (duration > coveredTime) {
                        basicFare = basicFare
                                + Math.ceil(duration - coveredTime) * flatRoutes.singleTrip.limitation.extraTime;
                        logger.debug(requestId + " - " + "extra time fee: " + basicFare);
                    }
                }
            } else if (flatRoutesReturn != null && flatRoutesReturn.singleTrip != null) {
                basicFare = flatRoutesReturn.singleTrip.returnRoute;
                route = flatRoutesReturn.routeInfo.name;
                routeId = flatRoutesReturn._id.toString();
                if (flatRoutesReturn.singleTrip.limitation.limited) {
                    double coveredDistance = flatRoutesReturn.singleTrip.limitation.coveredDistance;
                    double coveredTime = flatRoutesReturn.singleTrip.limitation.coveredTime;
                    String unitDistance = flatRoutesReturn.singleTrip.limitation.unitDistance;
                    if (unitDistance.equals("km")) {
                        if (distanceByKm > coveredDistance) {
                            basicFare = basicFare + Math.ceil(distanceByKm - coveredDistance)
                                    * flatRoutesReturn.singleTrip.limitation.extraDistance;
                        }
                    } else {
                        if (distanceByMile > coveredDistance) {
                            coveredDistance = coveredDistance / 1.609344;
                            double extraDistance = flatRoutesReturn.singleTrip.limitation.extraDistance * 1.609344;
                            logger.debug(requestId + " - " + "covered distance: " + coveredDistance);
                            logger.debug(requestId + " - " + "extra distance: " + extraDistance);
                            basicFare = basicFare + Math.ceil(distanceByMile - coveredDistance) * extraDistance;
                        }
                    }
                    double duration = etaFareEnt.duration / 3600;
                    if (duration > coveredTime) {
                        basicFare = basicFare
                                + Math.ceil(duration - coveredTime) * flatRoutesReturn.singleTrip.limitation.extraTime;
                        logger.debug(requestId + " - " + "extra time fee: " + basicFare);
                    }
                }
            }
            logger.debug(requestId + " - " + "basicFare by flat fare: " + basicFare);
            if (basicFare == 0) {
                if (fareNormal != null) {
                    normalFare = true;
                    // get minimum
                    if (fareNormal.minimum != null) {
                        if (bookingType.equalsIgnoreCase(KeysUtil.TIME_BOOK_NOW)
                                || bookingType.equalsIgnoreCase(KeysUtil.TIME_BOOK_ASAP))
                            minimum = fareNormal.minimum.now != null ? fareNormal.minimum.now : 0;
                        else
                            minimum = fareNormal.minimum.reservation != null ? fareNormal.minimum.reservation : 0;
                    }
                    // get fare normal
                    double firstDistanceFrom = 0;
                    double firstDistanceTo = 0;
                    double feeFirstDistance = 0;
                    double secondDistanceFrom = 0;
                    double secondDistanceTo = 0;
                    double feeSecondDistance = 0;
                    double feeAfterSecondDistance = fareNormal.afterSecondDistance.feeAfterSecondDistance != null
                            ? fareNormal.afterSecondDistance.feeAfterSecondDistance
                            : 0;
                    if (fareNormal.firstDistance != null) {
                        firstDistanceFrom = fareNormal.firstDistance.from != null ? fareNormal.firstDistance.from : 0;
                        firstDistanceTo = fareNormal.firstDistance.to != null ? fareNormal.firstDistance.to : 0;
                        feeFirstDistance = fareNormal.firstDistance.fee != null ? fareNormal.firstDistance.fee : 0;
                    }
                    if (fareNormal.secondDistance != null) {
                        secondDistanceFrom = fareNormal.secondDistance.from != null ? fareNormal.secondDistance.from
                                : 0;
                        secondDistanceTo = fareNormal.secondDistance.to != null ? fareNormal.secondDistance.to : 0;
                        feeSecondDistance = fareNormal.secondDistance.fee != null ? fareNormal.secondDistance.fee : 0;
                    }
                    if (distanceByKm >= firstDistanceFrom) {
                        if (distanceByKm <= firstDistanceTo) {
                            /** distance between from -- to */
                            basicFare += (distanceByKm - firstDistanceFrom) * feeFirstDistance;
                        } else if (distanceByKm >= secondDistanceFrom) {
                            if (distanceByKm <= secondDistanceTo) {
                                basicFare += ((firstDistanceTo - firstDistanceFrom) * feeFirstDistance)
                                        + ((distanceByKm - secondDistanceFrom) * feeSecondDistance);
                            } else {
                                /** distance > to */
                                basicFare += (((firstDistanceTo - firstDistanceFrom) * feeFirstDistance)
                                        + ((secondDistanceTo - secondDistanceFrom) * feeSecondDistance)
                                        + ((distanceByKm - secondDistanceTo) * feeAfterSecondDistance));
                            }
                        }
                    }
                    if (fareNormal.perMinute.feePerMinute != null) {
                        basicFare += (fareNormal.perMinute.feePerMinute * etaFareEnt.duration) / 60;
                    }
                    if (fareNormal.starting != null) {
                        if (bookingType.equalsIgnoreCase(KeysUtil.TIME_BOOK_NOW)
                                || bookingType.equalsIgnoreCase(KeysUtil.TIME_BOOK_ASAP))
                            basicFare += fareNormal.starting.now != null ? fareNormal.starting.now : 0;
                        else
                            basicFare += fareNormal.starting.reservation != null ? fareNormal.starting.reservation : 0;
                    }
                }
            }
        }
        data.add(basicFare);
        data.add(normalFare);
        data.add(route);
        data.add(minimum);
        data.add(routeId);
        return data;
    }

    public AffiliateRoute getFareByFlatRoutesAffiliate(String fareFlatId, List<Double> pickup, List<Double> destination,
            String zipCodeFrom, String zipCodeTo) {
        AffiliateRoute route = mongoDao.getByGeoAffiliate(fareFlatId, pickup, destination);
        if (route != null)
            return route;
        route = mongoDao.getByZipCodeAffiliate(fareFlatId, zipCodeFrom, zipCodeTo);
        if (route != null)
            return route;
        return null;
    }

    public AffiliateRoute getFareByFlatRoutesReturnAffiliate(String fareFlatId, List<Double> pickup,
            List<Double> destination, String zipCodeFrom, String zipCodeTo) {
        AffiliateRoute route = mongoDao.getByGeoReturnAffiliate(fareFlatId, pickup, destination);
        if (route != null)
            return route;
        route = mongoDao.getByZipCodeReturnAffiliate(fareFlatId, zipCodeFrom, zipCodeTo);
        if (route != null)
            return route;
        return null;
    }

    public AffiliateRoute getFareByFlatRoutesRoundTripAffiliate(String fareFlatId, List<Double> pickup,
            List<Double> destination, String zipCodeFrom, String zipCodeTo) {
        AffiliateRoute route = mongoDao.getByGeoRoundTripAffiliate(fareFlatId, pickup, destination);
        if (route != null)
            return route;
        route = mongoDao.getByGeoRoundTripReturnAffiliate(fareFlatId, pickup, destination);
        if (route != null)
            return route;
        route = mongoDao.getByZipCodeRoundTripAffiliate(fareFlatId, zipCodeFrom, zipCodeTo);
        if (route != null)
            return route;
        route = mongoDao.getByZipCodeRoundTripReturnAffiliate(fareFlatId, zipCodeFrom, zipCodeTo);
        if (route != null)
            return route;
        return null;
    }

    public FlatRoutes getFareByFlatRoutes(String fareFlatId, List<Double> pickup, List<Double> destination,
            String zipCodeFrom, String zipCodeTo) {
        FlatRoutes flatRoutes = mongoDao.getByGeo(fareFlatId, pickup, destination);
        if (flatRoutes != null)
            return flatRoutes;
        flatRoutes = mongoDao.getByZipCode(fareFlatId, zipCodeFrom, zipCodeTo);
        if (flatRoutes != null)
            return flatRoutes;
        return null;
    }

    public FlatRoutes getFareByFlatRoutesReturn(String fareFlatId, List<Double> pickup, List<Double> destination,
            String zipCodeFrom, String zipCodeTo) {
        FlatRoutes flatRoutes = mongoDao.getByGeoReturn(fareFlatId, pickup, destination);
        if (flatRoutes != null)
            return flatRoutes;
        flatRoutes = mongoDao.getByZipCodeReturn(fareFlatId, zipCodeFrom, zipCodeTo);
        if (flatRoutes != null)
            return flatRoutes;
        return null;
    }

    public FlatRoutes getFareByFlatRoutesRoundTrip(String fareFlatId, List<Double> pickup, List<Double> destination,
            String zipCodeFrom, String zipCodeTo) {
        FlatRoutes flatRoutes = mongoDao.getByGeoRoundTrip(fareFlatId, pickup, destination);
        if (flatRoutes != null)
            return flatRoutes;
        flatRoutes = mongoDao.getByGeoRoundTripReturn(fareFlatId, pickup, destination);
        if (flatRoutes != null)
            return flatRoutes;
        flatRoutes = mongoDao.getByZipCodeRoundTrip(fareFlatId, zipCodeFrom, zipCodeTo);
        if (flatRoutes != null)
            return flatRoutes;
        flatRoutes = mongoDao.getByZipCodeRoundTripReturn(fareFlatId, zipCodeFrom, zipCodeTo);
        if (flatRoutes != null)
            return flatRoutes;
        return null;
    }

    public boolean checkWaiveOffCommission(Fleet fleet, String zoneId, double total, double surcharge,
            String currencyISO, Date startTime, String pickupTz, String bookType, String requestId) {
        double waiveOffValue = 0;
        boolean shortTrip = false;
        boolean appliedSurcharge = false;
        boolean appliedTimeRange = false;
        if (fleet.waiveOffCommission != null && fleet.waiveOffCommission.enable) {
            if (fleet.waiveOffCommission.applyTimeRange) {
                appliedTimeRange = true;
                if (pickupTz.isEmpty())
                    pickupTz = fleet.timezone;
                try {
                    List<TimeRange> timeRanges = fleet.waiveOffCommission.timeRanges != null
                            ? fleet.waiveOffCommission.timeRanges
                            : new ArrayList<>();
                    for (TimeRange r : timeRanges) {
                        Calendar cal = Calendar.getInstance();
                        Date pickupTime = TimezoneUtil.offsetTimeZone(startTime, "GMT", pickupTz);
                        Date startDate = ConvertUtil.customizeDateByHour(pickupTime, r.from);
                        Date endDate = ConvertUtil.customizeDateByHour(pickupTime, r.to);
                        logger.debug(requestId + " - " + "pickupTime:    " + pickupTime);
                        logger.debug(requestId + " - " + "startDate:    " + startDate);
                        logger.debug(requestId + " - " + "endDate:    " + endDate);
                        if (pickupTime.after(startDate) && pickupTime.before(endDate)
                                || pickupTime.compareTo(startDate) == 0
                                || pickupTime.compareTo(endDate) == 0) {
                            // set false when applyTimeRange & pickup time in time range
                            appliedTimeRange = false;
                            break;
                        }
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (fleet.waiveOffCommission.applyZone == null || fleet.waiveOffCommission.applyZone.equals("All")) {
                appliedSurcharge = fleet.waiveOffCommission.applySurcharge;
                AmountByCurrency waiveOffCommission = fleet.waiveOffCommission.amountByCurrencies.stream()
                        .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                        .findFirst().orElse(null);
                if (waiveOffCommission != null)
                    waiveOffValue = waiveOffCommission.value;
            } else {
                List<WaiveOffByZone> waiveOffByZones = fleet.waiveOffCommission.waiveOffByZones;
                if (waiveOffByZones != null) {
                    for (WaiveOffByZone w : waiveOffByZones) {
                        if (w.zoneId.equals(zoneId) && w.currencyISO.equals(currencyISO)) {
                            appliedSurcharge = w.applySurcharge;
                            waiveOffValue = w.amount;
                        }
                    }
                }
            }
            if (!appliedTimeRange) {
                if (!appliedSurcharge || bookType.equals("delivery") || bookType.equals("food")
                        || bookType.equals("mart")) {
                    if (waiveOffValue > 0 && total < waiveOffValue) {
                        shortTrip = true;
                    }
                } else {
                    if (waiveOffValue > 0 && total < waiveOffValue && surcharge > 0) {
                        shortTrip = true;
                    }
                }
            }
        }
        return shortTrip;
    }

    public String updateAffiliateRate(int value, String priceType) {

        // update regular rate
        List<RateRegular> rateRegulars = mongoDao.getAllAffiliateRegularByPriceType(priceType);
        logger.debug("rateRegulars :" + rateRegulars.size());
        for (RateRegular regular : rateRegulars) {
            regular.starting.now = regular.starting.now + regular.starting.now * value * 0.01;
            regular.starting.reservation = regular.starting.reservation + regular.starting.reservation * value * 0.01;
            regular.firstDistance.fee = regular.firstDistance.fee + regular.firstDistance.fee * value * 0.01;
            regular.secondDistance.fee = regular.secondDistance.fee + regular.secondDistance.fee * value * 0.01;
            regular.afterSecondDistance.feeAfterSecondDistance = regular.afterSecondDistance.feeAfterSecondDistance
                    + regular.afterSecondDistance.feeAfterSecondDistance * value * 0.01;
            regular.perMinute.feePerMinute = regular.perMinute.feePerMinute
                    + regular.perMinute.feePerMinute * value * 0.01;
            mongoDao.updateAffiliateRegular(regular);
        }

        // update flat route
        List<AffiliateFlat> affiliateFlats = mongoDao.getAllAffiliateFlatByPriceType(priceType);
        for (AffiliateFlat flat : affiliateFlats) {
            List<AffiliateRoute> affiliateRoutes = mongoDao.getAllAffiliateRouteByFlatId(flat._id.toString());
            logger.debug("affiliateRoutes :" + affiliateRoutes.size());
            for (AffiliateRoute route : affiliateRoutes) {
                route.singleTrip.departureRoute = route.singleTrip.departureRoute
                        + route.singleTrip.departureRoute * value * 0.01;
                route.singleTrip.returnRoute = route.singleTrip.returnRoute
                        + route.singleTrip.returnRoute * value * 0.01;
                route.roundTrip.roundTripFee = route.roundTrip.roundTripFee
                        + route.roundTrip.roundTripFee * value * 0.01;
                mongoDao.updateAffiliateRoute(route);
            }
        }
        return "Completed";
    }

    // NEW
    public Map<String, Object> getPaymentData(String bookId, String fleetId, double distanceTour, double distanceGG,
            String requestId) {
        try {
            logger.debug(requestId + " - " + "fleetId: " + fleetId);
            logger.debug(requestId + " - " + "bookId: " + bookId);
            logger.debug(requestId + " - " + "distanceTour: " + distanceTour);
            logger.debug(requestId + " - " + "distanceGG: " + distanceGG);
            Fleet fleet = mongoDao.getFleetInfor(fleetId);
            Booking booking = mongoDao.getBooking(bookId);
            Map<String, Object> map = new HashMap<>();
            if (booking.pricingType != 0) {
                /*
                 * map = calFareAffiliatePayment(booking, fleet, distanceTour,
                 * distanceGG,KeysUtil.SELLPRICE, requestId);
                 */
                map = calFarePayment(booking, fleet, distanceTour, distanceGG, requestId);
            } else {
                // check if booking still not save droppedOffInfo
                if (booking.droppedOffInfo == null) {
                    DroppedOffInfo droppedOffInfo = new DroppedOffInfo();
                    droppedOffInfo.distanceTour = distanceTour;
                    booking.droppedOffInfo = droppedOffInfo;
                }
                map = calFarePayment(booking, fleet, distanceTour, distanceGG, requestId);
            }
            return map;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public Map<String, Object> calFareAffiliatePayment(Booking booking, Fleet providerFleet, double distance,
            double distanceGG,
            String priceType, String requestId) {
        try {
            double fare = 0.0;
            double tip = 0.0;
            double heavyTraffic = 0.0;
            double airportSurcharge = 0.0;
            double rushHour = 0.0;
            double taxValue = 0.0;
            double meetDriverFee = 0.0;
            double otherFees = 0.0;
            double techFee = 0.0;
            double promoAmount = 0.0;
            double distanceTour = 0.0;
            double tollFee = 0.0;
            double parkingFee = 0.0;
            double gasFee = 0.0;
            boolean normalFare = false;
            double minimumProvider = 0;
            String currencyISO = booking.currencyISO;
            String currencySymbol = booking.currencySymbol;

            /// GET FARE

            String corporateId = "";
            String zoneId = "";
            String fareNormalId = "";
            String fareFlatId = "";
            String vhcTypeId = "";
            String routeId = "";
            RateGeneral rateGeneral = null;
            AffiliationCarType carType = null;
            /*
             * if(providerFleet.etaDistanceByGPS){
             * if(distance < distanceGG*0.85){
             * distance = distanceGG;
             * }
             * }
             */
            distance = getDistanceBaseOnSetting(distance, distanceGG, booking.drvInfo.userId,
                    providerFleet.gpsLocation);
            logger.debug(requestId + " - Distance: " + distance);
            distanceTour = distance;
            // distance = ConvertUtil.round((distance / 1000), 2);
            String bookingType = "Now";
            if (booking.reservation) {
                bookingType = "Reservation";
            }
            int bookType = 0;
            List<Double> destination = new ArrayList<>();
            String packageRateId = "";
            if (booking.request.packageRateId != null)
                packageRateId = booking.request.packageRateId;
            if (booking.request.type == 3)
                bookType = 3;
            if (booking.corporateInfo != null && booking.corporateInfo.corporateId != null)
                corporateId = booking.corporateInfo.corporateId;
            String zipCodeFrom = "";
            if (booking.request.pickup != null) {
                zipCodeFrom = booking.request.pickup.zipCode != null ? booking.request.pickup.zipCode : "";
            }
            String zipCodeTo = "";
            if (booking.request.destination != null) {
                destination = booking.request.destination.geo;
                zipCodeTo = booking.request.destination.zipCode != null ? booking.request.destination.zipCode : "";
            }
            if (booking.request.type == 4) {
                bookType = 4;
                if (booking.request.origin != null) {
                    destination = booking.request.origin.destination.geo;
                    zipCodeTo = booking.request.origin.destination.zipCode != null
                            ? booking.request.origin.destination.zipCode
                            : "";
                }
            }
            if (booking.request != null && booking.request.vehicleTypeRequest != null
                    && !booking.request.vehicleTypeRequest.isEmpty()) {
                carType = mongoDao.getAffiliationCarTypeByName(booking.request.vehicleTypeRequest);
                if (carType != null)
                    vhcTypeId = carType._id.toString();
            }
            /*
             * if(booking.drvInfo!= null && booking.drvInfo.vehicleType!= null &&
             * !booking.drvInfo.vehicleType.isEmpty())
             * vehicleType = mongoDao.getVehicleTypeByFleetAndName(providerFleet.fleetId,
             * booking.drvInfo.vehicleType);
             */

            if (booking.request.pickup != null) {
                List<Zone> zones = mongoDao.findByGeoAffiliate(booking.request.pickup.geo, booking.request.psgFleetId,
                        vhcTypeId);
                if (zones != null && !zones.isEmpty()) {
                    zoneId = zones.get(0)._id.toString();
                    currencyISO = zones.get(0).currency.iso;
                    currencySymbol = zones.get(0).currency.symbol;
                    logger.debug(requestId + " - " + "currency :   " + currencyISO);
                }
            }

            if (!vhcTypeId.isEmpty() && !zoneId.isEmpty()) {

                List<AffiliateAssignedRate> listAffiliateAssignedRate = mongoDao.getListAffiliateAssignedRate(vhcTypeId,
                        zoneId);

                AffiliateAssignedRate assignedRateGeneral = listAffiliateAssignedRate.stream()
                        .filter(assignedRate -> assignedRate.type.equalsIgnoreCase(KeysUtil.GENERAL))
                        .findFirst().orElse(null);
                if (assignedRateGeneral != null) {
                    rateGeneral = mongoDao.getAffiliateRateGeneral(assignedRateGeneral.rate.id);
                    logger.debug(requestId + " - " + "assignedRateGeneral :   " + assignedRateGeneral._id);
                }

                AffiliateAssignedRate assignedRateNormal = null;
                if (priceType.equals(KeysUtil.SELLPRICE)) {
                    assignedRateNormal = listAffiliateAssignedRate.stream()
                            .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase(booking.request.psgFleetId)
                                    && assignedRate.type.equalsIgnoreCase(KeysUtil.REGULAR)
                                    && assignedRate.priceType.equalsIgnoreCase(priceType))
                            .findFirst().orElse(null);
                } else {
                    assignedRateNormal = listAffiliateAssignedRate.stream()
                            .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase(booking.fleetId)
                                    && assignedRate.type.equalsIgnoreCase(KeysUtil.REGULAR)
                                    && assignedRate.priceType.equalsIgnoreCase(priceType))
                            .findFirst().orElse(null);
                }
                if (assignedRateNormal == null) {
                    assignedRateNormal = listAffiliateAssignedRate.stream()
                            .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase("")
                                    && assignedRate.type.equalsIgnoreCase(KeysUtil.REGULAR)
                                    && assignedRate.priceType.equalsIgnoreCase(priceType))
                            .findFirst().orElse(null);
                }
                if (assignedRateNormal != null) {
                    fareNormalId = assignedRateNormal.rate.id;
                }

                AffiliateAssignedRate assignedRateFlat = null;
                if (priceType.equals(KeysUtil.SELLPRICE)) {
                    assignedRateFlat = listAffiliateAssignedRate.stream()
                            .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase(booking.request.psgFleetId) &&
                                    assignedRate.type.equalsIgnoreCase(KeysUtil.FLAT)
                                    && assignedRate.priceType.equalsIgnoreCase(priceType))
                            .findFirst().orElse(null);
                } else {
                    assignedRateFlat = listAffiliateAssignedRate.stream()
                            .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase(booking.fleetId) &&
                                    assignedRate.type.equalsIgnoreCase(KeysUtil.FLAT)
                                    && assignedRate.priceType.equalsIgnoreCase(priceType))
                            .findFirst().orElse(null);
                }
                if (assignedRateFlat == null) {
                    assignedRateFlat = listAffiliateAssignedRate.stream()
                            .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase("") &&
                                    assignedRate.priceType.equalsIgnoreCase(KeysUtil.BUYPRICE)
                                    && assignedRate.type.equalsIgnoreCase(KeysUtil.FLAT))
                            .findFirst().orElse(null);
                }
                if (assignedRateFlat != null) {
                    fareFlatId = assignedRateFlat.rate.id;
                }
            }

            logger.debug(requestId + " - " + "fareNormalId: " + fareNormalId);
            logger.debug(requestId + " - " + "fareFlatId: " + fareFlatId);
            Calendar cal = Calendar.getInstance();
            Date pickupTime = null;
            Date engagedTime = null;
            String engagedStr = "";
            String pickupStr = "";
            String droppedStr = sdfMongo.format(new Date());
            Date droppedOffTime = TimezoneUtil.convertServerToGMT(new Date());
            logger.debug(requestId + " - " + "droppedOffTime: " + droppedOffTime);
            if (booking.time != null) {
                if (booking.time.pickUpTime != null) {
                    pickupStr = sdfMongo.format(booking.time.pickUpTime);
                    pickupTime = TimezoneUtil.offsetTimeZone(sdfMongo.parse(pickupStr),
                            cal.getTimeZone().getID(), "GMT");
                    logger.debug(requestId + " - " + "pickupTime: " + pickupTime);
                }

                if (booking.time.engaged != null) {
                    engagedStr = sdfMongo.format(booking.time.engaged);
                    engagedTime = TimezoneUtil.offsetTimeZone(sdfMongo.parse(engagedStr),
                            cal.getTimeZone().getID(), "GMT");
                    logger.debug(requestId + " - " + "engagedTime: " + engagedTime);
                }

                if (booking.time.droppedOff != null) {
                    droppedStr = sdfMongo.format(booking.time.droppedOff);
                    droppedOffTime = TimezoneUtil.offsetTimeZone(sdfMongo.parse(droppedStr),
                            cal.getTimeZone().getID(), "GMT");
                    logger.debug(requestId + " - " + "droppedOffTime: " + droppedOffTime);
                }
            }
            String pickupTz = "";
            String dropOffTz = "";
            if (booking.request.pickup != null)
                pickupTz = booking.request.pickup.timezone != null ? booking.request.pickup.timezone : "";
            if (booking.request.destination != null)
                dropOffTz = booking.request.destination.timezone != null ? booking.request.destination.timezone : "";

            int durationTime = 0;
            int numOfDays = 0;
            String timeZone = "";
            if (booking.request.pickup != null)
                timeZone = booking.request.pickup.timezone != null ? booking.request.pickup.timezone : "";
            logger.debug(requestId + " - " + "timeZone: " + timeZone);
            if (timeZone.isEmpty())
                timeZone = providerFleet.timezone;
            if (engagedTime != null) {
                durationTime = ConvertUtil.getDurationTime(engagedStr, droppedStr);
                numOfDays = ConvertUtil.getDaysBetween(engagedTime, droppedOffTime, timeZone);
            } else {
                if (pickupTime != null) {
                    durationTime = ConvertUtil.getDurationTime(engagedStr, droppedStr);
                    numOfDays = ConvertUtil.getDaysBetween(pickupTime, droppedOffTime, timeZone);
                }
            }
            numOfDays = numOfDays + 1;
            ETAFareEnt etaFareEnt = new ETAFareEnt();
            etaFareEnt.fleetId = providerFleet.fleetId;
            etaFareEnt.bookType = bookType;
            etaFareEnt.bookFrom = booking.bookFrom;
            etaFareEnt.distance = distance;
            etaFareEnt.duration = durationTime;
            etaFareEnt.corporateId = corporateId;
            etaFareEnt.packageRateId = packageRateId;
            etaFareEnt.pickup = booking.request.pickup.geo;
            etaFareEnt.destination = destination;
            etaFareEnt.zipCodeFrom = zipCodeFrom;
            etaFareEnt.zipCodeTo = zipCodeTo;

            List<Object> dataFare = getBasicFareAffiliate(numOfDays, etaFareEnt, fareNormalId, fareFlatId, bookingType,
                    priceType, requestId);

            fare = Double.parseDouble(dataFare.get(0).toString());
            normalFare = Boolean.parseBoolean(dataFare.get(1).toString());
            minimumProvider = Double.parseDouble(dataFare.get(3).toString());
            routeId = dataFare.get(4).toString();

            // check round down setting
            if (providerFleet.roundDown) {
                fare = (int) fare;
            }
            AffiliateRoute affiliateRoute = null;
            AffiliatePackages affiliatePackages = null;
            if (!routeId.isEmpty())
                affiliateRoute = mongoDao.getAffiliateRouteById(routeId);
            if (!packageRateId.isEmpty()) {
                affiliatePackages = mongoDao.getPackageRateByIdAffiliate(packageRateId);
            }
            if (rateGeneral != null) {
                logger.debug(requestId + " - " + "general " + rateGeneral.rateInfo.name);
                // get heavy traffic
                if (rateGeneral.heavyTraffic != null && rateGeneral.heavyTraffic.enable != null
                        && rateGeneral.heavyTraffic.enable)
                    heavyTraffic = rateGeneral.heavyTraffic.surcharge != null ? rateGeneral.heavyTraffic.surcharge : 0;

                // get airport surcharge
                boolean checkDes = false;
                AirportZone airportZone = mongoDao.checkDestinationByGeo(booking.request.destination.geo);
                if (airportZone != null) {
                    checkDes = true;
                }
                if (booking.request.type == 1 && rateGeneral.airport != null
                        && rateGeneral.airport.fromAirportActive != null &&
                        rateGeneral.airport.fromAirportActive)
                    airportSurcharge = rateGeneral.airport.fromAirport != null ? rateGeneral.airport.fromAirport : 0;
                else if (rateGeneral.airport != null && rateGeneral.airport.toAirportActive != null &&
                        rateGeneral.airport.toAirportActive && providerFleet.additionalService.toAirport && checkDes)
                    airportSurcharge = rateGeneral.airport.toAirport != null ? rateGeneral.airport.toAirport : 0;

                if (!normalFare && affiliateRoute != null && affiliateRoute.otherFees != null &&
                        affiliateRoute.otherFees.isAirportFee) {
                    airportSurcharge = 0;
                }
                if ((etaFareEnt.bookType == 3 || etaFareEnt.typeRate == 1) && affiliatePackages != null
                        && affiliatePackages.otherFees != null &&
                        affiliatePackages.otherFees.isAirportFee)
                    airportSurcharge = 0;
                // get rush hour
                if (normalFare && rateGeneral.rushHour != null && rateGeneral.rushHour && rateGeneral.rushHours != null
                        && !rateGeneral.rushHours.isEmpty()) {
                    if (engagedTime != null) {
                        List<Object> getRushHour = getRushHourAffiliate(providerFleet, rateGeneral.rushHours,
                                engagedTime, droppedOffTime, pickupTz, dropOffTz, requestId);
                        if (getRushHour.get(0).toString().equalsIgnoreCase("amount")) {
                            rushHour = Double.parseDouble(getRushHour.get(1).toString());
                        } else {
                            rushHour = (Double.parseDouble(getRushHour.get(1).toString()) * fare) / 100;
                        }
                    } else {
                        if (pickupTime != null) {
                            List<Object> getRushHour = getRushHourAffiliate(providerFleet, rateGeneral.rushHours,
                                    pickupTime, droppedOffTime, pickupTz, dropOffTz, requestId);
                            if (getRushHour.get(0).toString().equalsIgnoreCase("amount")) {
                                rushHour = Double.parseDouble(getRushHour.get(1).toString());
                            } else {
                                rushHour = (Double.parseDouble(getRushHour.get(1).toString()) * fare) / 100;
                            }
                        }
                    }
                }

                // get tax fee
                if (rateGeneral.tax != null && rateGeneral.tax.enable != null && rateGeneral.tax.enable
                        && rateGeneral.tax.surcharge != null) {
                    taxValue = rateGeneral.tax.surcharge;
                }
                if (!normalFare && affiliateRoute != null && affiliateRoute.otherFees != null &&
                        affiliateRoute.otherFees.isTax) {
                    taxValue = 0;
                }
                if ((etaFareEnt.bookType == 3 || etaFareEnt.typeRate == 1) && affiliatePackages != null
                        && affiliatePackages.otherFees != null &&
                        affiliatePackages.otherFees.isTax)
                    taxValue = 0;

                if (rateGeneral.tip != null && rateGeneral.tip.enable)
                    tip = booking.request.tip != null ? booking.request.tip : 0;

                // get meet driver
                if (booking.request.moreInfo != null) {
                    if (booking.request.moreInfo.flightInfo != null && rateGeneral.meetDriver != null) {
                        if (booking.request.moreInfo.flightInfo.type == 0) {
                            meetDriverFee = rateGeneral.meetDriver.onCurb != null ? rateGeneral.meetDriver.onCurb : 0;
                        }
                        if (booking.request.moreInfo.flightInfo.type == 1) {
                            meetDriverFee = rateGeneral.meetDriver.meetDrv != null ? rateGeneral.meetDriver.meetDrv : 0;
                        }
                    }
                }
                if (!normalFare && affiliateRoute != null && affiliateRoute.otherFees != null &&
                        affiliateRoute.otherFees.isMeetDriverFee) {
                    meetDriverFee = 0;
                }
                if ((etaFareEnt.bookType == 3 || etaFareEnt.typeRate == 1) && affiliatePackages != null
                        && affiliatePackages.otherFees != null &&
                        affiliatePackages.otherFees.isMeetDriverFee)
                    meetDriverFee = 0;
            }

            if (booking.request.estimate != null && booking.request.estimate.fare != null) {
                if (!priceType.equalsIgnoreCase(KeysUtil.BUYPRICE)) {
                    fare = booking.request.estimate.fare.basicFare;
                    airportSurcharge = booking.request.estimate.fare.airportFee;
                    rushHour = booking.request.estimate.fare.rushHourFee;
                    techFee = booking.request.estimate.fare.techFee;
                    meetDriverFee = booking.request.estimate.fare.meetDriverFee;
                    otherFees = booking.request.estimate.fare.otherFees;
                    promoAmount = booking.request.estimate.fare.promoAmount;
                    tollFee = booking.request.estimate.fare.tollFee;
                    parkingFee = booking.request.estimate.fare.parkingFee;
                    gasFee = booking.request.estimate.fare.gasFee;
                    normalFare = booking.request.estimate.fare.min;
                } else {
                    if (booking.request.estimate.fare.estimateFareBuy != null) {
                        fare = booking.request.estimate.fare.estimateFareBuy.basicFare;
                        airportSurcharge = booking.request.estimate.fare.estimateFareBuy.airportFee;
                        rushHour = booking.request.estimate.fare.estimateFareBuy.rushHourFee;
                        techFee = booking.request.estimate.fare.estimateFareBuy.techFee;
                        meetDriverFee = booking.request.estimate.fare.estimateFareBuy.meetDriverFee;
                        otherFees = booking.request.estimate.fare.estimateFareBuy.otherFees;
                        promoAmount = booking.request.estimate.fare.estimateFareBuy.promoAmount;
                        tollFee = booking.request.estimate.fare.estimateFareBuy.tollFee;
                        parkingFee = booking.request.estimate.fare.estimateFareBuy.parkingFee;
                        gasFee = booking.request.estimate.fare.estimateFareBuy.gasFee;
                        normalFare = booking.request.estimate.fare.estimateFareBuy.min;
                    }
                }
            }

            Map<String, Object> map = new HashMap<>();
            map.put("fare", CommonUtils.getRoundValue(fare));
            map.put("normalFare", normalFare);
            map.put("tip", CommonUtils.getRoundValue(tip));
            map.put("heavyTraffic", CommonUtils.getRoundValue(heavyTraffic));
            map.put("airportSurcharge", CommonUtils.getRoundValue(airportSurcharge));
            map.put("rushHour", CommonUtils.getRoundValue(rushHour));
            map.put("taxValue", CommonUtils.getRoundValue(taxValue));
            map.put("meetDriverFee", CommonUtils.getRoundValue(meetDriverFee));
            map.put("otherFees", CommonUtils.getRoundValue(otherFees));
            map.put("techFee", CommonUtils.getRoundValue(techFee));
            map.put("tollFee", CommonUtils.getRoundValue(tollFee));
            map.put("parkingFee", CommonUtils.getRoundValue(parkingFee));
            map.put("gasFee", CommonUtils.getRoundValue(gasFee));
            map.put("promoAmount", CommonUtils.getRoundValue(promoAmount));
            map.put("bookingFeeActive", false);
            map.put("distanceTour", distanceTour);
            map.put("currencyISO", currencyISO);
            map.put("currencySymbol", currencySymbol);
            map.put("minimumProvider", minimumProvider);
            logger.debug(requestId + " - " + "map return:  " + map.toString());
            return map;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public Map<String, Object> calFarePayment(Booking booking, Fleet fleet, double distanceTour, double distanceGG,
            String requestId) {
        try {
            double fare = 0.0;
            double tip = 0.0;
            double heavyTraffic = 0.0;
            double airportSurcharge = 0.0;
            double rushHour = 0.0;
            double dynamicSurcharge = 0.0;
            double surchargeParameter = 0.0;
            double dynamicFare = 0.0;
            String dynamicType = "factor";
            double taxValue = 0.0;
            double meetDriverFee = 0.0;
            double otherFees = 0.0;
            double techFee = 0.0;
            double promoAmount = 0.0;
            double maximumValue = 0.0;
            double tollFee = 0.0;
            double parkingFee = 0.0;
            double gasFee = 0.0;
            double serviceFee = 0.0;
            double serviceTax = 0.0;
            double subTotal = 0.0;
            double total = 0.0;
            double taxFee = 0.0;
            double tipFee = 0.0;
            double creditTransactionFee = 0.0;
            double minimum = 0.0;
            double bookingFee = 0.0;
            double addOnPrice = 0.0;
            double markupDifference = 0.0;
            int extraWaitTime = 0;
            double extraWaitFee = 0.0;
            boolean normalFare = false;
            boolean isMinimum = false;
            boolean isMinimumTotal = false;
            String fareType = "";
            String surchargeTypeRate = "";
            /// GET FARE
            FleetFare fleetFare = mongoDao.getFleetFare(fleet.fleetId);
            VehicleType vehicleType = null;
            String vehicleTypeId = "";
            String corporateId = "";
            String zoneId = "";
            String fareHourlyId = "";
            String fareFlatId = "";
            String fareNormalId = "";

            if (booking.request != null) {
                vehicleTypeId = booking.request.vehicleTypeRequest != null ? booking.request.vehicleTypeRequest : "";
                if (booking.pricingType == 1) {
                    logger.debug(requestId + " - convert car type affiliate to local");
                    AffiliationCarType affiliationCarType = mongoDao.getAffiliationCarTypeByName(vehicleTypeId);
                    if (affiliationCarType != null) {
                        // get list car types local has associated with affiliate car type
                        List<String> listCarTypes = mongoDao.getListCarTypeByAffiliate(booking.fleetId,
                                affiliationCarType._id.toString());
                        if (listCarTypes.size() > 0) {
                            vehicleTypeId = listCarTypes.get(0);
                        }
                    }
                }
            }
            logger.debug(requestId + " - vehicleTypeId: " + vehicleTypeId);
            if (!vehicleTypeId.isEmpty()) {
                vehicleType = mongoDao.getVehicleTypeByFleetAndName(fleet.fleetId, vehicleTypeId);
            }
            double distance;
            if (vehicleType.dispatchRideSharing) {
                if (vehicleType.actualFare) {
                    // mode actual - use GG distance from app
                    distance = distanceGG;
                } else {
                    // mode estimate - retrieve GG distance from booking
                    distance = booking.request.origin != null ? booking.request.origin.etaDistance
                            : booking.request.estimate.distanceValue;
                }
            } else {
                distance = getDistanceBaseOnSetting(distanceTour, distanceGG, booking.drvInfo.userId,
                        fleet.gpsLocation);
            }

            logger.debug(requestId + " - distance: " + distance);
            // reset distanceTour value
            distanceTour = distance;
            String unitDistance = fleet.unitDistance;
            if (unitDistance.equalsIgnoreCase(KeysUtil.UNIT_DISTANCE_IMPERIAL)) {
                distance = ConvertUtil.round((distance / 1609.344), 2);
            } else {
                distance = ConvertUtil.round((distance / 1000), 2);
            }
            logger.debug(requestId + " ==> distance after convert: " + distance);
            String bookingType = "Now";
            if (booking.reservation) {
                bookingType = "Reservation";
            }
            List<Double> destination = new ArrayList<>();
            String packageRateId = "";
            if (booking.request.packageRateId != null)
                packageRateId = booking.request.packageRateId;

            if (booking.corporateInfo != null && booking.corporateInfo.corporateId != null)
                corporateId = booking.corporateInfo.corporateId;

            String zipCodeFrom = "";
            if (booking.request.pickup != null) {
                zipCodeFrom = booking.request.pickup.zipCode != null ? booking.request.pickup.zipCode : "";
                if (booking.request.pickup.zoneId != null && booking.request.pickup.zoneId.isEmpty()) {
                    zoneId = booking.request.pickup.zoneId;
                } else {
                    Zone zone = mongoDao.findByFleetIdAndGeo(fleet.fleetId, booking.request.pickup.geo);
                    if (zone != null) {
                        zoneId = zone._id.toString();
                    }
                }
            }
            String zipCodeTo = "";
            if (booking.request.destination != null) {
                destination = booking.request.destination.geo;
                zipCodeTo = booking.request.destination.zipCode != null ? booking.request.destination.zipCode : "";
            }
            if (booking.request.type == 4 || booking.request.typeRate == 2) {
                if (booking.request.origin != null) {
                    destination = booking.request.origin.destination.geo;
                    zipCodeTo = booking.request.origin.destination.zipCode != null
                            ? booking.request.origin.destination.zipCode
                            : "";
                }
            }
            // check rate by corporate
            boolean rateByCorp = false;
            logger.debug(requestId + " - corporateId: " + corporateId);
            if (!corporateId.isEmpty()) {
                Corporate corporate = mongoDao.getCorporate(corporateId);
                if (corporate != null && corporate.pricing != null && corporate.pricing.differentRate != null
                        && corporate.pricing.differentRate)
                    rateByCorp = corporate.pricing.differentRate;
            }
            logger.debug(requestId + " - differentRate: " + rateByCorp);
            logger.debug(requestId + " - vehicleType: " + gson.toJson(vehicleType));
            logger.debug(requestId + " - zoneId: " + zoneId);
            if (vehicleType != null && !zoneId.isEmpty()) {
                if (rateByCorp) {
                    if (vehicleType.corpRates != null && !vehicleType.corpRates.isEmpty()) {
                        for (CorpRate corpRate : vehicleType.corpRates) {
                            logger.debug(requestId + " - corpRate: " + gson.toJson(corpRate));
                            if (corpRate.corporateId.equals(corporateId) && corpRate.rates != null && !corpRate.rates.isEmpty()) {
                                for (Rate rate : corpRate.rates) {
                                    if (rate.rateType.equals(KeysUtil.REGULAR) && rate.zoneId.equals(zoneId))
                                        fareNormalId = rate.rateId;
                                    if (rate.rateType.equals(KeysUtil.FLAT))
                                        fareFlatId = rate.rateId;
                                    if (rate.rateType.equals(KeysUtil.HOURLY) && rate.zoneId.equals(zoneId))
                                        fareHourlyId = rate.rateId;
                                }
                                break;
                            }
                        }
                    }
                } else {
                    if (vehicleType.rates != null && !vehicleType.rates.isEmpty()) {
                        for (Rate rate : vehicleType.rates) {
                            if (rate.rateType.equals(KeysUtil.REGULAR) && rate.zoneId.equals(zoneId))
                                fareNormalId = rate.rateId;
                            if (rate.rateType.equals(KeysUtil.FLAT))
                                fareFlatId = rate.rateId;
                            if (rate.rateType.equals(KeysUtil.HOURLY) && rate.zoneId.equals(zoneId))
                                fareHourlyId = rate.rateId;
                        }
                    }
                }
            }
            logger.debug(requestId + " - " + "fareNormalId: " + fareNormalId + "---fareFlatId: " + fareFlatId
                    + "---fareHourlyId: " + fareHourlyId);
            Calendar cal = Calendar.getInstance();
            Date pickupTime = null;
            Date engagedTime = null;
            String engagedStr = "";
            String pickupStr = "";
            String droppedStr = sdfMongo.format(new Date());
            Date droppedOffTime = TimezoneUtil.convertServerToGMT(new Date());
            logger.debug(requestId + " - " + "droppedOffTime: " + droppedOffTime);
            if (booking.time != null) {
                if (booking.time.pickUpTime != null) {
                    pickupStr = sdfMongo.format(booking.time.pickUpTime);
                    pickupTime = TimezoneUtil.offsetTimeZone(sdfMongo.parse(pickupStr), cal.getTimeZone().getID(),
                            "GMT");
                    logger.debug(requestId + " - " + "pickupTime: " + pickupTime);
                }
                if (booking.time.engaged != null) {
                    engagedStr = sdfMongo.format(booking.time.engaged);
                    engagedTime = TimezoneUtil.offsetTimeZone(sdfMongo.parse(engagedStr), cal.getTimeZone().getID(),
                            "GMT");
                    logger.debug(requestId + " - " + "engagedTime: " + engagedTime);
                }

                if (booking.time.droppedOff != null) {
                    droppedStr = sdfMongo.format(booking.time.droppedOff);
                    droppedOffTime = TimezoneUtil.offsetTimeZone(sdfMongo.parse(droppedStr),
                            cal.getTimeZone().getID(), "GMT");
                    logger.debug(requestId + " - " + "droppedOffTime: " + droppedOffTime);
                }
            }

            int durationTime = 0;
            int numOfDays = 0;
            String timeZone = "";
            if (booking.request.pickup != null)
                timeZone = booking.request.pickup.timezone != null ? booking.request.pickup.timezone : "";
            logger.debug(requestId + " - " + "timeZone: " + timeZone);
            if (timeZone.isEmpty())
                timeZone = fleet.timezone;
            if (engagedTime != null) {
                durationTime = ConvertUtil.getDurationTime(engagedStr, droppedStr);
                numOfDays = ConvertUtil.getDaysBetween(engagedTime, droppedOffTime, timeZone);
            } else {
                if (pickupTime != null) {
                    durationTime = ConvertUtil.getDurationTime(pickupStr, droppedStr);
                    numOfDays = ConvertUtil.getDaysBetween(pickupTime, droppedOffTime, timeZone);
                }
            }
            logger.debug(requestId + " - " + "durationTime: " + durationTime);
            numOfDays = numOfDays + 1;

            // check estimate data
            boolean actualFare = false;
            if (vehicleType.actualFare) {
                actualFare = true;
            }

            List<Object> dataFare = getBasicFare(booking.request.type, booking.request.typeRate, durationTime, distance,
                    numOfDays, bookingType, booking.request.pickup.geo, destination, zipCodeFrom, zipCodeTo,
                    packageRateId,
                    fareNormalId, fareFlatId, booking.currencyISO, requestId);
            logger.debug(requestId + " dataFare: " + gson.toJson(dataFare));
            String thirdParty = "";
            if (booking.externalInfo != null && booking.externalInfo.thirdParty != null) {
                thirdParty = booking.externalInfo.thirdParty != null ? booking.externalInfo.thirdParty : "booking.com";
            }
            String bookFrom = booking.bookFrom != null ? booking.bookFrom : "";
            if (dataFare == null && !KeysUtil.THIRD_PARTY.contains(bookFrom))
                return null;
            double extraWaitSetting = 0.0;
            if (dataFare != null) {
                fare = Double.parseDouble(dataFare.get(0).toString());
                normalFare = Boolean.parseBoolean(dataFare.get(1).toString());
                minimum = Double.parseDouble(dataFare.get(3).toString());
                fareType = dataFare.get(8).toString();
                if (fareType.equals("regular")) {
                    FareNormal fareNormal = mongoDao.getFareNormalById(fareNormalId);
                    if (fareNormal != null && bookingType.equals("Now")) {
                        extraWaitSetting = fareNormal.fareWaitTimeNow != null ? fareNormal.fareWaitTimeNow : 0.0;
                    } else {
                        extraWaitSetting = fareNormal.fareWaitTimeLater != null ? fareNormal.fareWaitTimeLater : 0.0;
                    }
                } else if (fareType.equals("flat")) {
                    FareFlat fareFlat = mongoDao.getFareFlatById(fareFlatId);
                    if (fareFlat != null && bookingType.equals("Now")) {
                        extraWaitSetting = fareFlat.fareWaitTimeNow != null ? fareFlat.fareWaitTimeNow : 0.0;
                    } else {
                        extraWaitSetting = fareFlat.fareWaitTimeLater != null ? fareFlat.fareWaitTimeLater : 0.0;
                    }
                } else if (fareType.equals("hourly")) {
                    FareHourly fareHourly = mongoDao.getFareHourlyById(fareHourlyId);
                    if (fareHourly != null && bookingType.equals("Now")) {
                        extraWaitSetting = fareHourly.fareWaitTimeNow != null ? fareHourly.fareWaitTimeNow : 0.0;
                    } else {
                        extraWaitSetting = fareHourly.fareWaitTimeLater != null ? fareHourly.fareWaitTimeLater : 0.0;
                    }
                }
            }
            if (extraWaitSetting > 0) {
                logger.debug(requestId + " - bookingType: " + bookingType);
                logger.debug(requestId + " - extraWaitSetting: " + extraWaitSetting);
                boolean active = fleetFare.noShow != null && fleetFare.isActive != null && fleetFare.isActive;
                if (active) {
                    int freeTime = fleetFare.noShow.countdownDuration != null ? fleetFare.noShow.countdownDuration : 0;
                    int maxTime = fleetFare.noShow.maxWaitTime != null ? fleetFare.noShow.maxWaitTime : 0;
                    logger.debug(requestId + " - freeTime: " + freeTime);
                    logger.debug(requestId + " - maxTime: " + maxTime);
                    Date engaged = booking.time != null && booking.time.engaged != null ? booking.time.engaged : null;
                    Date arrival = booking.time != null && booking.time.arrived != null ? booking.time.arrived : null;
                    Date expectedPickupTime = booking.time != null && booking.time.expectedPickupTime != null ? booking.time.expectedPickupTime : null;
                    long duration = 0;
                    if (bookingType.equalsIgnoreCase("Now") && arrival != null && engaged != null) {
                        logger.debug(requestId + " - arrival: " + arrival);
                        logger.debug(requestId + " - engaged: " + engaged);
                        duration = engaged.getTime() - arrival.getTime();
                    } else if (bookingType.equalsIgnoreCase("Reservation") && expectedPickupTime != null && engaged != null) {
                        logger.debug(requestId + " - expectedPickupTime: " + expectedPickupTime);
                        logger.debug(requestId + " - engaged: " + engaged);
                        duration = engaged.getTime() - expectedPickupTime.getTime();

                    }
                    int diffInMinutes = (int)TimeUnit.MILLISECONDS.toMinutes(duration);
                    int diffInSeconds  = (int) TimeUnit.MILLISECONDS.toSeconds(duration);
                    if (diffInSeconds - diffInMinutes*60 > 0) {
                        diffInMinutes +=1;
                    }
                    logger.debug(requestId + " - diffInMinutes: " + diffInMinutes);
                    if (diffInMinutes > freeTime) {
                        if (diffInMinutes <= maxTime) {
                            extraWaitTime = diffInMinutes - freeTime;
                        } else {
                            if (diffInMinutes - freeTime > maxTime) {
                                extraWaitTime = maxTime;
                            } else {
                                extraWaitTime = diffInMinutes - freeTime;
                            }
                        }
                    }
                    extraWaitFee = extraWaitTime*extraWaitSetting;
                    logger.debug(requestId + " - extraWaitTime: " + extraWaitTime + " => extraWaitFee: " + extraWaitFee);
                }
            }
            surchargeTypeRate = getSurchargeTypeRateFromBooking(booking);
            if (!actualFare && booking.request.typeRate != 2 && booking.request.estimate.fare != null
                    && booking.request.estimate.fare.routeId != null &&
                    !booking.request.estimate.fare.routeId.isEmpty()) {
                // cal again with flat rate
                logger.debug(
                        requestId + " - " + "re calculator with routeId: " + booking.request.estimate.fare.routeId);
                normalFare = false;
                FlatRoutes flatRoutes = mongoDao.getRouteById(booking.request.estimate.fare.routeId);
                if (flatRoutes != null) {
                    if (booking.request.typeRate != 2) {
                        List<Object> dataFlatRate = getBaseFeeFlat(durationTime, distance, flatRoutes,
                                booking.request.estimate.fare.reverseRoute, false, booking.currencyISO, requestId);
                        fare = Double.parseDouble(dataFlatRate.get(0).toString());
                    } else {
                        List<Object> dataFlatRate = getBaseFeeFlat(durationTime, distance, flatRoutes,
                                booking.request.estimate.fare.reverseRoute, true, booking.currencyISO, requestId);
                        fare = Double.parseDouble(dataFlatRate.get(0).toString());
                    }
                }
                if (booking.request.estimate.fare.basicFare > fare)
                    fare = booking.request.estimate.fare.basicFare;
            }

            if (fleet.generalSetting.dynamicFare && booking.request != null && booking.request.estimate != null
                    && normalFare) {
                if (booking.request.estimate.dynamicFare != null) {
                    dynamicFare = booking.request.estimate.dynamicFare;
                } else if (booking.request.estimate.fare != null && booking.request.estimate.fare.dynamicFare != null) {
                    dynamicFare = booking.request.estimate.fare.dynamicFare;
                    dynamicType = booking.request.estimate.fare.dynamicType != null
                            ? booking.request.estimate.fare.dynamicType
                            : "factor";
                }
                logger.debug(requestId + " - Basic fare: " + fare);
                logger.debug(requestId + " - Dynamic Fare: " + dynamicFare);

                if (dynamicFare == 0 && booking.request.pickup.geo != null) {
                    // get dynamic fare
                    DynamicFare dF = mongoDao.getDynamicFare(fleet.fleetId, zoneId, booking.request.pickup.geo);
                    if (dF != null && dF.isActive) {
                        dynamicFare = dF.parameter;
                        dynamicType = dF.type != null ? dF.type : "factor";
                    }
                    logger.debug(requestId + " - get dynamic fare from setting: " + dynamicFare);
                }

                // get dynamic fare
                if (dynamicFare > 0) {
                    if (dynamicType.equals("factor")) {
                        fare += fare * (dynamicFare - 1);
                        minimum += minimum * (dynamicFare - 1);
                    } else {
                        fare = fare + dynamicFare;
                        minimum = minimum + dynamicFare;
                    }
                    if (fare < 0)
                        fare = 0;
                    if (minimum < 0)
                        minimum = 0;
                }
            }

            // check discount if book corporate
            fare = getModifyPriceOfCorp(corporateId, fare, requestId);

            tip = booking.request.tip != null ? booking.request.tip : 0;

            // get tech fee
            List<Object> getTechFee = getTechFeeValueByZone(fleet, booking.bookFrom, booking.currencyISO, zoneId);
            techFee = Double.parseDouble(getTechFee.get(0).toString());

            // get airport fee
            airportSurcharge = getAirportFee(fleet, booking.currencyISO, booking.request.pickup.geo,
                    booking.request.destination.geo, booking.request.type == 1,
                    booking.request.typeRate == 2 && !actualFare, distance > 1);

            if (booking.request.moreInfo != null) {
                if (booking.request.moreInfo.flightInfo != null) {
                    if (booking.request.moreInfo.flightInfo.type == 0) {
                        if (fleetFare.meetDriver.onCurbByCurrencies != null) {
                            AmountByCurrency onCurb = fleetFare.meetDriver.onCurbByCurrencies.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(booking.currencyISO))
                                    .findFirst().orElse(null);
                            if (onCurb != null)
                                meetDriverFee = onCurb.value;
                        }
                    }
                    if (booking.request.moreInfo.flightInfo.type == 1) {
                        if (fleetFare.meetDriver.meetDrvByCurrencies != null) {
                            AmountByCurrency meetDrv = fleetFare.meetDriver.meetDrvByCurrencies.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(booking.currencyISO))
                                    .findFirst().orElse(null);
                            if (meetDrv != null)
                                meetDriverFee = meetDrv.value;
                        }
                    }
                }
            }

            // check apply zone
            // get general setting apply for each zone
            ServiceFee sF = mongoDao.getServiceFeeByFleetIdAndZoneId(fleet.fleetId, zoneId);
            String applyType = fleetFare.applyType != null ? fleetFare.applyType : "all";
            if (sF != null && fleet.generalSetting.allowCongfigSettingForEachZone && !applyType.equals("all")) {
                // get heavy traffic
                if (sF.heavyTrafficActive && sF.heavyTrafficByCurrencies != null) {
                    AmountByCurrency heavy = sF.heavyTrafficByCurrencies.stream()
                            .filter(amount -> amount.currencyISO.equalsIgnoreCase(booking.currencyISO))
                            .findFirst().orElse(null);
                    if (heavy != null)
                        heavyTraffic = heavy.value;
                }

                // get rush hour fee
                if (sF.rushHourActive) {
                    String dropOffTz = "";
                    if (booking.request.destination != null) {
                        dropOffTz = booking.request.destination.timezone != null ? booking.request.destination.timezone
                                : "";
                    }
                    logger.debug(requestId + " - surchargeTypeRate: " + surchargeTypeRate);
                    if (surchargeTypeRate.equals("shuttle")) {
                        fareType = "shuttle";
                    }
                    logger.debug(requestId + " - fareType: " + fareType);
                    if (engagedTime != null) {
                        List<Object> getRushHour = getRushHour(fleet, sF.rushHours, engagedTime, droppedOffTime,
                                booking.currencyISO, timeZone, dropOffTz, distance, surchargeTypeRate, fareType,
                                booking.reservation, requestId);
                        if (getRushHour.get(0).toString().equalsIgnoreCase("amount")) {
                            rushHour = Double.parseDouble(getRushHour.get(1).toString());
                        } else {
                            rushHour = (Double.parseDouble(getRushHour.get(1).toString()) * fare) / 100;
                        }
                    } else {
                        if (pickupTime != null) {
                            List<Object> getRushHour = getRushHour(fleet, sF.rushHours, pickupTime, droppedOffTime,
                                    booking.currencyISO, timeZone, dropOffTz, distance, surchargeTypeRate, fareType,
                                    booking.reservation, requestId);
                            if (getRushHour.get(0).toString().equalsIgnoreCase("amount")) {
                                rushHour = Double.parseDouble(getRushHour.get(1).toString());
                            } else {
                                rushHour = (Double.parseDouble(getRushHour.get(1).toString()) * fare) / 100;
                            }
                        }
                    }
                }

                // get other fees
                if (sF.otherFeeActive && sF.otherFee.valueByCurrencies != null) {
                    AmountByCurrency otherFee = sF.otherFee.valueByCurrencies.stream()
                            .filter(amount -> amount.currencyISO.equalsIgnoreCase(booking.currencyISO))
                            .findFirst().orElse(null);
                    if (otherFee != null)
                        otherFees = otherFee.value;
                }

                // get tax
                if (sF.taxActive) {
                    taxValue = sF.tax;
                }
            }
            // get default general setting
            if (fleetFare.applyType == null || fleetFare.applyType.equals("all")
                    || !fleet.generalSetting.allowCongfigSettingForEachZone) {
                // reset all fee
                heavyTraffic = 0;
                rushHour = 0;
                otherFees = 0;
                taxValue = 0;
                // get heavy traffic
                if (fleetFare.heavyTrafficActive && fleetFare.heavyTrafficByCurrencies != null) {
                    AmountByCurrency heavy = fleetFare.heavyTrafficByCurrencies.stream()
                            .filter(amount -> amount.currencyISO.equalsIgnoreCase(booking.currencyISO))
                            .findFirst().orElse(null);
                    if (heavy != null)
                        heavyTraffic = heavy.value;
                }

                // get rush hour fee
                if (fleetFare.rushHourActive) {
                    String dropOffTz = "";
                    if (booking.request.destination != null) {
                        dropOffTz = booking.request.destination.timezone != null ? booking.request.destination.timezone
                                : "";
                    }
                    logger.debug(requestId + " - surchargeTypeRate: " + surchargeTypeRate);
                    if (surchargeTypeRate.equals("shuttle")) {
                        fareType = "shuttle";
                    }
                    logger.debug(requestId + " - fareType: " + fareType);
                    if (engagedTime != null) {
                        List<Object> getRushHour = getRushHour(fleet, fleetFare.rushHours, engagedTime, droppedOffTime,
                                booking.currencyISO, timeZone, dropOffTz, distance, surchargeTypeRate, fareType,
                                booking.reservation, requestId);
                        if (getRushHour.get(0).toString().equalsIgnoreCase("amount")) {
                            rushHour = Double.parseDouble(getRushHour.get(1).toString());
                        } else {
                            rushHour = (Double.parseDouble(getRushHour.get(1).toString()) * fare) / 100;
                        }
                    } else {
                        if (pickupTime != null) {
                            List<Object> getRushHour = getRushHour(fleet, fleetFare.rushHours, pickupTime,
                                    droppedOffTime,
                                    booking.currencyISO, timeZone, dropOffTz, distance, surchargeTypeRate, fareType,
                                    booking.reservation, requestId);
                            if (getRushHour.get(0).toString().equalsIgnoreCase("amount")) {
                                rushHour = Double.parseDouble(getRushHour.get(1).toString());
                            } else {
                                rushHour = (Double.parseDouble(getRushHour.get(1).toString()) * fare) / 100;
                            }
                        }
                    }
                }

                // get other fees
                if (fleetFare.otherFeeActive && fleetFare.otherFee.valueByCurrencies != null) {
                    AmountByCurrency otherFee = fleetFare.otherFee.valueByCurrencies.stream()
                            .filter(amount -> amount.currencyISO.equalsIgnoreCase(booking.currencyISO))
                            .findFirst().orElse(null);
                    if (otherFee != null)
                        otherFees = otherFee.value;
                }

                // get tax
                if (fleetFare.taxActive) {
                    taxValue = fleetFare.tax;
                }
            }

            // get dynamic surcharge
            if (fleet.generalSetting.dynamicSurcharge && bookingType.equals("Now")) {
                if (booking.request != null && booking.request.estimate != null) {
                    if (booking.request.estimate.surchargeParameter != null
                            && booking.request.estimate.surchargeParameter >= 1) {
                        surchargeParameter = booking.request.estimate.surchargeParameter;
                    } else if (booking.request.estimate.fare != null
                            && booking.request.estimate.fare.surchargeParameter >= 1) {
                        surchargeParameter = booking.request.estimate.fare.surchargeParameter;
                    }
                }
                if (actualFare) {
                    // re-check dynamic surcharge
                    surchargeParameter = getDynamicSurcharge(fleet.fleetId, zoneId, booking.request.pickup.geo,
                            booking.request.destination.geo);
                }
                dynamicSurcharge = CommonUtils.getRoundValue(fare) * (surchargeParameter - 1);
                if (dynamicSurcharge < 0)
                    dynamicSurcharge = 0;
            }
            logger.debug(requestId + " - dynamicSurcharge: " + CommonUtils.getRoundValue(dynamicSurcharge));
            rushHour = rushHour + CommonUtils.getRoundValue(dynamicSurcharge);

            // check estimate data and edited fare by Operator
            boolean editedFare = booking.request.estimate != null && booking.request.estimate.isFareEdited;
            boolean markupPrice = booking.request.estimate != null && booking.request.estimate.fare != null
                    && booking.request.estimate.fare.markupDifference != 0;
            boolean estimateFare = false;
            boolean keepMinFee = false;
            if (!actualFare && booking.request.estimate != null && booking.request.estimate.fare != null) {
                if (booking.puPoints != null && booking.puPoints.size() > 1
                        || booking.doPoints != null && booking.doPoints.size() > 1 ||
                        booking.request.extraDestination != null && !booking.request.extraDestination.isEmpty() ||
                        (booking.request.estimate.fare.normalFare != null && booking.request.estimate.fare.normalFare)
                                && booking.request.estimate.fare.route.isEmpty()
                        ||
                        booking.request.estimate.fare.routeId != null
                                && !booking.request.estimate.fare.routeId.isEmpty()
                                && booking.request.estimate.fare.basicFare == fare) {
                    estimateFare = true;
                }
            }

            if (booking.request.estimate.fare != null) {
                if (estimateFare || editedFare || markupPrice
                        || (KeysUtil.THIRD_PARTY.contains(thirdParty) && KeysUtil.THIRD_PARTY.contains(bookFrom))) {
                    fare = booking.request.estimate.fare.basicFare;
                    airportSurcharge = booking.request.estimate.fare.airportFee;
                    dynamicSurcharge = booking.request.estimate.fare.dynamicSurcharge;
                    surchargeParameter = booking.request.estimate.fare.surchargeParameter;
                    techFee = booking.request.estimate.fare.techFee;
                    meetDriverFee = booking.request.estimate.fare.meetDriverFee;
                    otherFees = booking.request.estimate.fare.otherFees;
                    promoAmount = booking.request.estimate.fare.promoAmount;
                    maximumValue = booking.request.estimate.fare.promoAmount;
                    tollFee = booking.request.estimate.fare.tollFee;
                    parkingFee = booking.request.estimate.fare.parkingFee;
                    gasFee = booking.request.estimate.fare.gasFee;
                    normalFare = booking.request.estimate.fare.normalFare != null
                            && booking.request.estimate.fare.normalFare;

                    addOnPrice = booking.request.estimate.fare.addOnPrice;
                    markupDifference = booking.request.estimate.fare.markupDifference;
                    bookingFee = booking.request.estimate.fare.bookingFee;
                    taxFee = booking.request.estimate.fare.tax;
                    tipFee = booking.request.estimate.fare.tip;
                    creditTransactionFee = booking.request.estimate.fare.creditTransactionFee;
                    isMinimum = booking.request.estimate.fare.min;
                    minimum = booking.request.estimate.fare.minFare;
                }
                if (!actualFare || editedFare || markupPrice) {
                    rushHour = booking.request.estimate.fare.rushHourFee;
                }
            }
            // calculator sub total, total fare
            subTotal = CommonUtils.getRoundValue(fare) + CommonUtils.getRoundValue(extraWaitFee) +
                    CommonUtils.getRoundValue(rushHour) + CommonUtils.getRoundValue(otherFees);
            logger.debug(requestId + " =====>>> subTotal = " + CommonUtils.getRoundValue(fare) +
                    " + " + CommonUtils.getRoundValue(extraWaitTime) +
                    " + "
                    + CommonUtils.getRoundValue(rushHour) +
                    " + " + CommonUtils.getRoundValue(otherFees) +
                    " = " + subTotal);
            logger.debug(requestId + " - " + "subTotal: " + subTotal);

            // check if subTotal less than minimum before calculate fleet services
            if(minimum > 0 && subTotal < minimum){
                subTotal = minimum;
                isMinimum = true;
            }
            if(booking.request.serviceActive && !KeysUtil.THIRD_PARTY_HOLIDAYTAXIS.equals(bookFrom)){
                if(booking.request.services!= null && !booking.request.services.isEmpty()){
                    serviceFee = booking.request.services.stream().filter(s ->{
                                logger.debug(requestId + " - name: " + s.name + " - active: " + s.active + " - fee: " + s.fee);
                                return s.active;
                            }
                    ).mapToDouble(s -> s.fee).sum();
                } else if(booking.request.fleetServices != null && !booking.request.fleetServices.isEmpty()){
                    for (FleetService fs : booking.request.fleetServices) {
                        if (fs.active) {
                            String serviceFeeType = fs.serviceFeeType != null ? fs.serviceFeeType : "amount";
                            if (serviceFeeType.equals("percent")) {
                                double fee = fs.serviceFeePercent * subTotal / 100;
                                serviceFee += fee;
                                if (fs.applyTax) {
                                    serviceTax += taxValue * fee / 100;
                                }
                            } else if (fs.serviceFeeByCurrencies != null && !fs.serviceFeeByCurrencies.isEmpty()) {
                                AmountByCurrency fee = fs.serviceFeeByCurrencies.stream()
                                        .filter(amount -> amount.currencyISO.equalsIgnoreCase(booking.currencyISO))
                                        .findFirst().orElse(null);
                                if (fee != null) {
                                    serviceFee += fee.value;
                                    if (fs.applyTax) {
                                        serviceTax += taxValue * fee.value / 100;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            logger.debug(requestId + " - serviceFee: " + CommonUtils.getRoundValue(serviceFee));
            // include additional services to subTotal
            logger.debug(requestId + " - " + "fleet.additionalFees: " + fleet.additionalFees);
            if (fleet.additionalFees != 1) {
                subTotal += CommonUtils.getRoundValue(serviceFee);
            }
            logger.debug(requestId + " - " + "subTotal: " + subTotal);
            if (minimum > 0 && subTotal < minimum) {
                subTotal = minimum;
                isMinimum = true;
                logger.debug(requestId + " - " + "subTotal: " + subTotal);
            }
            logger.debug(requestId + " - " + "subTotal: " + subTotal);

            // get Promo value
            if (!editedFare) {
                taxFee = (taxValue * subTotal) / 100 + serviceTax;
                if (fleetFare.tipActive)
                    tipFee = (tip * subTotal) / 100;
                String mDispatcherId = booking.mDispatcherInfo != null && booking.mDispatcherInfo.userId != null
                        ? booking.mDispatcherInfo.userId
                        : "";
                List<Object> data = getBookingFee(mDispatcherId, corporateId, booking.bookFrom, booking.currencyISO);
                if (data.get(0).toString().equalsIgnoreCase("amount")) {
                    bookingFee = Double.parseDouble(data.get(1).toString());
                } else {
                    bookingFee = (Double.parseDouble(data.get(1).toString()) * subTotal) / 100;
                }
                String code = "";
                if (booking.request != null && booking.request.promo != null)
                    code = booking.request.promo;
                if (!code.isEmpty()) {
                    if (booking.isFarmOut) {
                        promoAmount = booking.request.estimate.fare.originPromoAmount;
                    } else {
                        JSONObject promoCode = getPromoCode(requestId, fleet.fleetId, booking.psgInfo.userId, code,
                                booking.currencyISO, subTotal, booking.bookId);
                        // JSONObject promoCode = getPromoCode(requestId, fleet.fleetId, code,
                        // booking.currencyISO);
                        logger.debug(requestId + " - promoCode: " + promoCode.toJSONString());
                        promoAmount = Double.parseDouble(promoCode.get("value").toString());
                        maximumValue = Double.parseDouble(promoCode.get("maximumValue").toString());
                        keepMinFee = Boolean.parseBoolean(promoCode.get("keepMinFee").toString());
                        if (promoAmount > maximumValue && maximumValue > 0) {
                            promoAmount = maximumValue;
                        }
                    }
                }
            }
            logger.debug(requestId + " - subTotal: " + subTotal);
            logger.debug(requestId + " - techFee: " + techFee);
            logger.debug(requestId + " - bookingFee: " + CommonUtils.getRoundValue(bookingFee));
            logger.debug(requestId + " - taxFee: " + CommonUtils.getRoundValue(taxFee));
            logger.debug(requestId + " - tipFee: " + CommonUtils.getRoundValue(tipFee));
            logger.debug(requestId + " - promoAmount: " + CommonUtils.getRoundValue(promoAmount));
            logger.debug(requestId + " - maximumValue: " + CommonUtils.getRoundValue(maximumValue));

            total = subTotal + techFee + CommonUtils.getRoundValue(bookingFee) +
                    CommonUtils.getRoundValue(taxFee) + CommonUtils.getRoundValue(tipFee)
                    - CommonUtils.getRoundValue(promoAmount);
            total = total + CommonUtils.getRoundValue(airportSurcharge) +
                    CommonUtils.getRoundValue(meetDriverFee) + CommonUtils.getRoundValue(tollFee) + CommonUtils.getRoundValue(parkingFee)
                    + CommonUtils.getRoundValue(gasFee);
            if (fleet.additionalFees == 1)
                total += CommonUtils.getRoundValue(serviceFee);

            if (keepMinFee && minimum > 0 && total <= minimum) {
                isMinimumTotal = true;
                double originPromo = (total + CommonUtils.getRoundValue(promoAmount)) - minimum;
                logger.debug(requestId + " - " + "originPromo: " + originPromo);
                total = minimum;
            }

            // add data for Hydra base on total without credit transaction fee
            double qupPreferredAmount = 0.0;
            double qupSellPrice = 0.0;
            double fleetMarkup = 0.0;
            if (booking.request != null && booking.request.estimate != null && booking.request.estimate.fare != null) {
                qupPreferredAmount = booking.request.estimate.fare.qupPreferredAmount;
                qupSellPrice = CommonUtils.getRoundValue(total + qupPreferredAmount);
                fleetMarkup = booking.request.estimate.fare.fleetMarkup;
            }

            if (actualFare && booking.pricingType == 0
                    && KeysUtil.transactionFeeMethod.contains(booking.request.paymentType)) {
                creditTransactionFee = getTransactionFee(fleet.creditCardTransactionFee, total,
                        booking.request.paymentType,
                        booking.currencyISO, requestId);
            }
            total = total + creditTransactionFee;
            logger.debug(requestId + " - total after creditTransactionFee: " + fare);
            if (total < 0)
                total = 0;

            Map<String, Object> map = new HashMap<>();
            logger.debug(requestId + " - fare: " + fare);
            map.put("fare", CommonUtils.getRoundValue(fare));
            double unroundTotal = total;
            if (addOnPrice != 0) {
                unroundTotal = unroundTotal + addOnPrice;
                total = roundingValue(total, fleet.rounding, booking.currencyISO) + addOnPrice;
                subTotal = subTotal + addOnPrice;
            }
            if (markupDifference != 0) {
                unroundTotal = unroundTotal + markupDifference;
                total = roundingValue(total, fleet.rounding, booking.currencyISO) + markupDifference;
            }
            if (fleetMarkup != 0) {
                qupSellPrice = CommonUtils.getRoundValue(qupSellPrice + fleetMarkup);
            }

            map.put("total", roundingValue(total, fleet.rounding, booking.currencyISO));
            map.put("unroundedTotalAmt", CommonUtils.getRoundValue(unroundTotal));
            map.put("subTotal", CommonUtils.getRoundValue(subTotal));
            if (KeysUtil.THIRD_PARTY.contains(thirdParty) && KeysUtil.THIRD_PARTY.contains(bookFrom)) {
                map.put("total", roundingValue(fare, fleet.rounding, booking.currencyISO));
                map.put("unroundedTotalAmt", CommonUtils.getRoundValue(fare));
                map.put("subTotal", CommonUtils.getRoundValue(fare));
            }
            map.put("isMinimum", isMinimum);
            map.put("isMinimumTotal", isMinimumTotal);
            map.put("normalFare", normalFare);
            map.put("tip", CommonUtils.getRoundValue(tip));
            map.put("heavyTraffic", CommonUtils.getRoundValue(heavyTraffic));
            map.put("airportSurcharge", CommonUtils.getRoundValue(airportSurcharge));
            map.put("rushHour", CommonUtils.getRoundValue(rushHour));
            map.put("dynamicSurcharge", CommonUtils.getRoundValue(dynamicSurcharge));
            map.put("dynamicFare", CommonUtils.getRoundValue(dynamicFare));
            map.put("dynamicType", dynamicType);
            map.put("surchargeParameter", CommonUtils.getRoundValue(surchargeParameter));
            map.put("taxValue", CommonUtils.getRoundValue(taxValue));
            map.put("meetDriverFee", CommonUtils.getRoundValue(meetDriverFee));
            map.put("otherFees", CommonUtils.getRoundValue(otherFees));
            map.put("techFee", CommonUtils.getRoundValue(techFee));
            map.put("tollFee", CommonUtils.getRoundValue(tollFee));
            map.put("parkingFee", CommonUtils.getRoundValue(parkingFee));
            map.put("gasFee", CommonUtils.getRoundValue(gasFee));
            map.put("serviceFee", CommonUtils.getRoundValue(serviceFee));
            map.put("promoAmount", CommonUtils.getRoundValue(promoAmount));
            map.put("maximumValue", CommonUtils.getRoundValue(maximumValue));
            map.put("creditTransactionFee", CommonUtils.getRoundValue(creditTransactionFee));
            map.put("bookingFeeActive", getBookingFeeactive(booking));
            map.put("distanceTour", distanceTour);
            map.put("keepMinFee", keepMinFee);
            map.put("fareType", fareType);
            map.put("surchargeTypeRate", surchargeTypeRate);
            map.put("qupPreferredAmount", qupPreferredAmount);
            map.put("qupSellPrice", qupSellPrice);
            map.put("extraWaitTime", extraWaitTime);
            map.put("extraWaitFee", CommonUtils.getRoundValue(extraWaitFee));
            return map;

        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            return null;
        }
    }

    public boolean getBookingFeeactive(Booking booking) {
        boolean bookingFeeActive = false;
        String bookFrom = booking.bookFrom;
        String corporateId = "";
        String mDispatcherId = booking.mDispatcherInfo != null && booking.mDispatcherInfo.userId != null
                ? booking.mDispatcherInfo.userId
                : "";
        if (KeysUtil.partner.equals(bookFrom) || KeysUtil.mDispatcher.equals(bookFrom) ||
                (KeysUtil.PWA.equalsIgnoreCase(bookFrom) && !mDispatcherId.isEmpty())) {
            bookingFeeActive = true;
        } else if (KeysUtil.command_center.equals(bookFrom) && booking.corporateInfo != null) {
            corporateId = booking.corporateInfo.corporateId != null ? booking.corporateInfo.corporateId : "";
            if (!corporateId.equals("")) {
                Corporate corporate = mongoDao.getCorporate(corporateId);
                if (corporate != null && corporate.isActive && corporate.commission != null
                        && corporate.commission.active != null) {
                    bookingFeeActive = corporate.commission.active;
                }
            }
        } else if (!KeysUtil.bookNotPax.contains(bookFrom) && booking.corporateInfo != null) {
            corporateId = booking.corporateInfo.corporateId != null ? booking.corporateInfo.corporateId : "";
            if (!corporateId.equals("")) {
                Corporate corporate = mongoDao.getCorporate(corporateId);
                // check if enable both commission and setting apply commission for booking from
                // app
                if (corporate != null && corporate.isActive && corporate.commission != null
                        && corporate.commission.active != null) {
                    return corporate.commission.active && corporate.isCommissionBookingApp;
                }
            }
        }
        return bookingFeeActive;
    }

    public double roundingValue(double value, Rounding rounding, String currencyISO) {
        if (rounding != null && rounding.enable && rounding.roundingByCurrencies != null) {
            for (RoundingByCurrency r : rounding.roundingByCurrencies) {
                if (r.currencyISO.equals(currencyISO)) {
                    value = ConvertUtil.extendedRound(value, r.mode, r.digits);
                    break;
                }
            }
        }
        return value;
    }

    public double getDynamicSurcharge(String fleetId, String zoneId, List<Double> pickup, List<Double> destination) {
        double surchargeParameter = 1;
        // check dynamic surcharge by pickup point
        DynamicSurcharge pds = mongoDao.getDynamicSurcharge(fleetId, zoneId, pickup);
        if (pds != null && pds.isActive && pds.pickupPoint) {
            surchargeParameter = pds.parameter;
        }
        // check dynamic surcharge by DO point
        DynamicSurcharge dds = mongoDao.getDynamicSurcharge(fleetId, zoneId, destination);
        if (dds != null && dds.isActive && dds.dropOffPoint) {
            if (dds.parameter > surchargeParameter)
                surchargeParameter = dds.parameter;
        }
        return surchargeParameter;
    }

    public List<Double> getServiceFee(List<String> services, Fleet fleet, FleetFare fleetFare, String zoneId,
            String currencyISO, double tax, String vehicleTypeId, double subTotal) {
        double serviceFee = 0;
        double serviceTax = 0;
        if (fleet.additionalFees != 1) {
            // get additional service
            if (services != null && !services.isEmpty()) {
                for (String serviceId : services) {
                    AdditionalServices service = mongoDao.findByServiceId(serviceId);
                    if (service != null) {
                        if (service.serviceFeeByCurrencies != null && !service.serviceFeeByCurrencies.isEmpty()) {
                            AmountByCurrency fee = service.serviceFeeByCurrencies.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                                    .findFirst().orElse(null);
                            if (fee != null) {
                                serviceFee += fee.value;
                            }
                        }
                    }
                }
            }
        } else {
            // get fleet service
            List<FleetService> fleetServices = new ArrayList<>();
            if (fleetFare.applyType == null || fleetFare.applyType.equals("all")
                    || !fleet.generalSetting.allowCongfigSettingForEachZone) {
                if (fleetFare.fleetServiceActive && fleetFare.fleetServices != null) {
                    fleetServices = fleetFare.fleetServices;
                }
            } else {
                ServiceFee sF = mongoDao.getServiceFeeByFleetIdAndZoneId(fleet.fleetId, zoneId);
                String applyType = fleetFare.applyType != null ? fleetFare.applyType : "all";
                if (sF != null && fleet.generalSetting.allowCongfigSettingForEachZone && !applyType.equals("all")) {
                    if (sF.fleetServiceActive && sF.fleetServices != null) {
                        fleetServices = sF.fleetServices;
                    }
                }
            }
            for (FleetService fs : fleetServices) {
                if ((fs.serviceType.equalsIgnoreCase("Compulsory")
                       && fs.vehicleType.contains(vehicleTypeId)) || (services != null && services.contains(fs.serviceId))) {
                        String serviceFeeType = fs.serviceFeeType != null ? fs.serviceFeeType : "amount";
                    if (serviceFeeType.equals("percent")) {
                        double fee = fs.serviceFeePercent * subTotal / 100;
                        serviceFee += fee;
                        if (fs.applyTax) {
                            serviceTax += tax * fee / 100;
                        }
                    } else if (fs.serviceFeeByCurrencies != null && !fs.serviceFeeByCurrencies.isEmpty()) {
                    AmountByCurrency fee = fs.serviceFeeByCurrencies.stream()
                            .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                            .findFirst().orElse(null);
                    if (fee != null) {
                        serviceFee += fee.value;
                            if (fs.applyTax) {
                                serviceTax += tax * fee.value / 100;
                            }
                        }
                    }

                }
            }
        }
        List<Double> listReturn = new ArrayList<>();
        listReturn.add(serviceFee);
        listReturn.add(serviceTax);
        return listReturn;
    }

    public List<Double> getFleetServiceFees(String requestId, List<FleetService> fleetServices, String currencyISO, double subTotal) {
        return getFleetServiceFees(requestId, fleetServices, currencyISO, 0.0, subTotal);
    }
    public List<Double> getFleetServiceFees(String requestId, List<FleetService> fleetServices, String currencyISO, double taxSetting, double subTotal) {
        double fleetServiceFee = 0.0;
        double fleetCommissionFromFleetServiceFee = 0.0;
        double driverCommissionFromFleetServiceFee = 0.0;
        double fleetServiceTax = 0.0;
        if (fleetServices != null && !fleetServices.isEmpty()) {
            logger.debug(requestId + " - getFleetServiceFees: " + gson.toJson(fleetServices));
            logger.debug(requestId + " - taxSetting : " + taxSetting);
            for (FleetService fleetService : fleetServices) {
                try {
                    logger.debug(
                            requestId + " - service: " + fleetService.serviceName + " active? " + fleetService.active);
                    // String jobType = getJobTypeFromBooking(booking);
                    if (fleetService.active) {
                        // get data for each service
                        double serviceFee = 0.0;
                        String serviceFeeType = fleetService.serviceFeeType != null ? fleetService.serviceFeeType : "amount";
                        if (serviceFeeType.equals("percent")) {
                            serviceFee = fleetService.serviceFeePercent * subTotal / 100;
                        } else if (fleetService.serviceFeeByCurrencies != null && !fleetService.serviceFeeByCurrencies.isEmpty()) {
                            serviceFee = fleetService.serviceFeeByCurrencies.stream().filter(a -> a.currencyISO.equals(currencyISO))
                                    .findFirst().map(a -> a.value).orElse(0.0);
                        }
                        logger.debug(requestId + " - serviceFee: " + serviceFee);
                        double serviceTax = 0.0;
                        double fleetCommission = 0.0;
                        if (fleetService.fleetCommission != null &&
                                fleetService.fleetCommission.commissionByCurrencies != null
                                && !fleetService.fleetCommission.commissionByCurrencies.isEmpty()) {
                            String commissionType = fleetService.fleetCommission.commissionType != null
                                    ? fleetService.fleetCommission.commissionType
                                    : "";
                            double commissionValue = fleetService.fleetCommission.commissionByCurrencies.stream()
                                    .filter(a -> a.currencyISO.equals(currencyISO))
                                    .findFirst().map(a -> a.value).orElse(0.0);
                            logger.debug(requestId + " - commissionType: " + commissionType + " - commissionValue: " + commissionValue);
                            fleetCommission = commissionType.equals("amount") ? commissionValue
                                    : commissionValue * serviceFee / 100;
                            if (taxSetting > 0 && commissionType.equals("percent") && fleetService.applyTax) {
                                double tax = taxSetting * serviceFee / 100;
                                serviceTax = commissionValue * tax / 100;
                                logger.debug(requestId + " - tax: " + tax + " => serviceTax: " + serviceTax);
                                fleetServiceTax += serviceTax;
                            }
                        }
                        logger.debug(requestId + " - fleetCommission: " + fleetCommission);
                        // validate in-case setting fleet commission is too high
                        if (fleetCommission > serviceFee) {
                            fleetCommission = serviceFee;
                            logger.debug(
                                    requestId + " - setting fleet commission is too high ==> updated fleetCommission: "
                                            + fleetCommission);
                        }
                        double driverCommission = serviceFee - fleetCommission;
                        logger.debug(requestId + " - driverCommission: " + driverCommission);

                        // sum data for all services
                        fleetServiceFee += serviceFee;
                        fleetCommissionFromFleetServiceFee += fleetCommission;
                        driverCommissionFromFleetServiceFee += driverCommission;
                    }
                } catch (Exception ex) {
                    logger.debug(requestId + " - get fleet commission error: " + CommonUtils.getError(ex));
                }
            }
        }
        fleetServiceFee = Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(fleetServiceFee));
        fleetCommissionFromFleetServiceFee = Double
                .parseDouble(KeysUtil.DECIMAL_FORMAT.format(fleetCommissionFromFleetServiceFee));
        driverCommissionFromFleetServiceFee = Double
                .parseDouble(KeysUtil.DECIMAL_FORMAT.format(driverCommissionFromFleetServiceFee));
        fleetServiceTax = Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(fleetServiceTax));
        logger.debug(requestId + " - fleetServiceFee: " + fleetServiceFee);
        logger.debug(requestId + " - fleetCommissionFromFleetServiceFee: " + fleetCommissionFromFleetServiceFee);
        logger.debug(requestId + " - driverCommissionFromFleetServiceFee: " + driverCommissionFromFleetServiceFee);
        logger.debug(requestId + " - fleetServiceTax: " + fleetServiceTax);
        return Arrays.asList(fleetServiceFee, fleetCommissionFromFleetServiceFee, driverCommissionFromFleetServiceFee, fleetServiceTax);
    }

    public List<String> getCompulsoryServices(String vehicleTypeId, Fleet fleet, FleetFare fleetFare, String zoneId,
            String bookFrom, String requestId) {
        List<String> services = new ArrayList<>();
        if (fleet.additionalFees != 1) {
            // get additional service
            if (fleetFare.additionalServicesActive) {
                List<AdditionalServices> additionalServices = mongoDao.findByVehicleIdAndServiceType(vehicleTypeId,
                        "Compulsory");
                if (!additionalServices.isEmpty()) {
                    services.addAll(
                            additionalServices.stream().map(s -> s._id.toString()).collect(Collectors.toList()));
                }
            }
        } else {
            // get fleet service
            List<FleetService> fleetServices = new ArrayList<>();
            if (fleetFare.applyType == null || fleetFare.applyType.equals("all")
                    || !fleet.generalSetting.allowCongfigSettingForEachZone) {
                if (fleetFare.fleetServiceActive && fleetFare.fleetServices != null
                        && !fleetFare.fleetServices.isEmpty()) {
                    if (bookFrom.equals(KeysUtil.PWA)) {
                        fleetServices.addAll(fleetFare.fleetServices.stream().filter(
                                s -> s.serviceType.equals("Compulsory") && s.vehicleType.contains(vehicleTypeId))
                                .collect(Collectors.toList()));
                    } else {
                        fleetServices.addAll(fleetFare.fleetServices.stream()
                                .filter(s -> s.serviceType.equals("Compulsory")).collect(Collectors.toList()));
                    }
                }
            } else {
                ServiceFee sF = mongoDao.getServiceFeeByFleetIdAndZoneId(fleet.fleetId, zoneId);
                String applyType = fleetFare.applyType != null ? fleetFare.applyType : "all";
                if (sF != null && fleet.generalSetting.allowCongfigSettingForEachZone && !applyType.equals("all")) {
                    if (sF.fleetServiceActive && sF.fleetServices != null && !sF.fleetServices.isEmpty()) {
                        if (bookFrom.equals(KeysUtil.PWA)) {
                            fleetServices.addAll(sF.fleetServices.stream().filter(
                                    s -> s.serviceType.equals("Compulsory") && s.vehicleType.contains(vehicleTypeId))
                                    .collect(Collectors.toList()));
                        } else {
                            fleetServices.addAll(sF.fleetServices.stream()
                                    .filter(s -> s.serviceType.equals("Compulsory")).collect(Collectors.toList()));
                        }
                    }
                }
            }
            if (bookFrom.equals(KeysUtil.PWA)) {
                // only applied transport booking
                services = fleetServices.stream()
                        .filter(s -> s.bookingType.equals("all") || s.bookingType.equals("transport"))
                        .map(s -> s.serviceId).collect(Collectors.toList());
            }
        }
        logger.debug(requestId + " - services: " + gson.toJson(services));
        return services;
    }

    public JSONObject getItemsFee(JSONArray menuData, String currencyISO, String requestId, String language) {
        JSONObject objRespone = new JSONObject();

        JSONObject js = new JSONObject();
        js.put("currency", currencyISO);
        js.put("items", menuData);
        String url = ServerConfig.menu_api + get_receipts_output_data;
        ;
        String username = ServerConfig.menu_username;
        String password = ServerConfig.menu_password;
        logger.debug(requestId + " - url: " + url);
        logger.debug(requestId + " - body: " + js.toJSONString());
        HttpResponse<String> jsonResponse = null;
        double itemFees = 0;
        try {
            jsonResponse = Unirest.post(url)
                    .basicAuth(username, password)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("Accept-Language", language)
                    .body(js.toJSONString()).asString();
            String objectResponse = jsonResponse.getBody();
            logger.debug(requestId + " - objectResponse: " + objectResponse);
            JSONObject object = gson.fromJson(objectResponse, JSONObject.class);
            if (!object.isEmpty() && object.get("data") != null) {
                objRespone.put("data", gson.toJson(object.get("data")));
                Type type = new TypeToken<List<ItemEnt>>() {
                }.getType();
                // JSONArray jsonArray = (JSONArray) object.get("data");
                List<ItemEnt> items = gson.fromJson(gson.toJson(object.get("data")), type);
                // List<ItemEnt> items = mapper.readValue(gson.toJson(object.get("data")), new
                // TypeReference<List<ItemEnt>>() {});
                itemFees = items.stream().filter(item -> item.price != null).mapToDouble(item -> item.price.value)
                        .sum();
                logger.debug(requestId + " - itemFees: " + itemFees);
            }
        } catch (Exception e) {
            logger.debug(requestId + " - Exception: " + CommonUtils.getError(e));
        }
        objRespone.put("itemFees", itemFees);
        return objRespone;
    }

    public boolean applyCorpCommission(String requestId, Corporate corporate, String bookFrom) {
        boolean apply = false;
        try {
            // book from CC: enable commission
            // book from app: enable commission and setting apply commission for booking
            // from app
            if (corporate != null && corporate.isActive && corporate.commission != null) {
                if (KeysUtil.command_center.equals(bookFrom))
                    return corporate.commission.active != null && corporate.commission.active;
                else if (!KeysUtil.bookNotPax.contains(bookFrom))
                    return corporate.commission.active != null && corporate.commission.active
                            && corporate.isCommissionBookingApp;
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - applyCorpCommission exception: " + CommonUtils.getError(ex));
        }
        return apply;
    }

    public FareReturn checkEditFare(String requestId, Fleet fleet, FareReturn fareReturn, EditFare editFare,
            String userId, String corporateId,
            String bookFrom, double tip) {
        boolean keepMinFee = false;
        if (editFare.basicFare > 0)
            fareReturn.basicFare = editFare.basicFare;
        if (editFare.airportFee > 0)
            fareReturn.airportFee = editFare.airportFee;
        if (editFare.meetDriverFee > 0)
            fareReturn.meetDriverFee = editFare.meetDriverFee;
        if (editFare.rushHourFee > 0)
            fareReturn.rushHourFee = editFare.rushHourFee;
        if (editFare.techFee > 0)
            fareReturn.techFee = editFare.techFee;
        if (editFare.creditTransactionFee > 0)
            fareReturn.creditTransactionFee = editFare.creditTransactionFee;
        if (editFare.otherFees > 0)
            fareReturn.otherFees = editFare.otherFees;

        if (editFare.tollFee > 0)
            fareReturn.tollFee = editFare.tollFee;
        if (editFare.parkingFee > 0)
            fareReturn.parkingFee = editFare.parkingFee;
        if (editFare.gasFee > 0)
            fareReturn.gasFee = editFare.gasFee;
        if (editFare.serviceFee > 0)
            fareReturn.serviceFee = editFare.serviceFee;
        // re calculator sub-total, total
        fareReturn.subTotal = fareReturn.basicFare + fareReturn.rushHourFee + fareReturn.otherFees;
        if (fleet.additionalFees != 1)
            fareReturn.subTotal += fareReturn.serviceFee;
        if (editFare.bookingFee > 0) {
            fareReturn.bookingFee = editFare.bookingFee;
        } else {
            List<Object> data = getBookingFee(userId, corporateId, bookFrom, fareReturn.currencyISO);
            if (data.get(0).toString().equalsIgnoreCase("amount")) {
                fareReturn.bookingFee = Double.parseDouble(data.get(1).toString());
            } else {
                fareReturn.bookingFee = (Double.parseDouble(data.get(1).toString()) * fareReturn.subTotal) / 100;
            }
        }
        fareReturn.tax = fareReturn.taxSetting * fareReturn.subTotal * 0.01;
        if (editFare.tip > 0) {
            fareReturn.tip = editFare.tip;
        } else {
            if (KeysUtil.command_center.equals(bookFrom) || KeysUtil.api.equals(bookFrom) ||
                    KeysUtil.web_booking.equals(bookFrom)) {
                fareReturn.tip = tip * fareReturn.subTotal * 0.01;
                if (tip == -1 || fareReturn.tip < 0) {
                    fareReturn.tip = 0;
                }
            }
        }
        if (editFare.promoAmount > 0) {
            fareReturn.promoAmount = editFare.promoAmount;
        } else {
            // JSONObject promoCode = getPromoCode(requestId, fleet.fleetId, userId,
            // fareReturn.promoCode, fareReturn.currencyISO, fareReturn.subTotal);
            JSONObject promoCode = getPromoCode(requestId, fleet.fleetId, fareReturn.promoCode, fareReturn.currencyISO);
            logger.debug(requestId + " - promoCode: " + promoCode.toJSONString());
            keepMinFee = Boolean.parseBoolean(promoCode.get("keepMinFee").toString());
            if (promoCode.get("type").toString().equalsIgnoreCase(KeysUtil.COMMISION_AMOUNT)) {
                fareReturn.promoAmount = Double.parseDouble(promoCode.get("value").toString());
            } else {
                fareReturn.promoAmount = (Double.parseDouble(promoCode.get("value").toString()) * fareReturn.subTotal)
                        / 100;
                if (fareReturn.promoAmount > Double.parseDouble(promoCode.get("maximumValue").toString()) &&
                        Double.parseDouble(promoCode.get("maximumValue").toString()) > 0)
                    fareReturn.promoAmount = Double.parseDouble(promoCode.get("maximumValue").toString());
            }
        }
        fareReturn.totalWithoutPromo = fareReturn.subTotal + fareReturn.techFee + fareReturn.bookingFee +
                fareReturn.tax + fareReturn.tip + fareReturn.airportFee + fareReturn.meetDriverFee +
                fareReturn.tollFee + fareReturn.parkingFee + fareReturn.gasFee;
        if (fleet.additionalFees == 1)
            fareReturn.totalWithoutPromo += fareReturn.serviceFee;
        fareReturn.etaFare = fareReturn.totalWithoutPromo - fareReturn.promoAmount;
        double originPromo = 0;
        if (keepMinFee && fareReturn.minFare > 0 && fareReturn.etaFare <= fareReturn.minFare) {
            fareReturn.isMinimumTotal = true;
            fareReturn.promoAmount = fareReturn.totalWithoutPromo - fareReturn.minFare;
            originPromo = fareReturn.totalWithoutPromo - fareReturn.minFare;
            fareReturn.etaFare = fareReturn.totalWithoutPromo - originPromo;
        }
        fareReturn.etaFare = fareReturn.etaFare + fareReturn.creditTransactionFee;
        if (fareReturn.etaFare < 0)
            fareReturn.etaFare = 0;
        fareReturn.unroundedTotalAmt = fareReturn.etaFare;
        fareReturn.etaFare = roundingValue(fareReturn.unroundedTotalAmt, fleet.rounding, fareReturn.currencyISO);
        if (fareReturn.etaFare > 0 && originPromo == 0) {
            fareReturn.totalWithoutPromo = fareReturn.etaFare + fareReturn.promoAmount;
        } else if (fareReturn.promoAmount > 0) {
            fareReturn.totalWithoutPromo = roundingValue(fareReturn.totalWithoutPromo + fareReturn.creditTransactionFee,
                    fleet.rounding, fareReturn.currencyISO);
        }
        return fareReturn;
    };

    public DeliveryResponse checkEditFareDelivery(String requestId, Fleet fleet, DeliveryResponse response,
            EditFare editFare, String userId,
            boolean isParcel, double taxSetting) {
        if (editFare.deliveryFee > 0)
            response.deliveryFee = editFare.deliveryFee;
        if (editFare.techFee > 0)
            response.techFee = editFare.techFee;
        if (editFare.creditTransactionFee > 0)
            response.creditTransactionFee = editFare.creditTransactionFee;

        // re calculator sub-total, total
        response.subTotal = response.deliveryFee;
        if (isParcel)
            response.subTotal = response.deliveryFee + response.itemsFee;

        if (editFare.promoAmount > 0) {
            response.promoAmount = editFare.promoAmount;
        } else {
            JSONObject promoCode = getPromoCode(requestId, fleet.fleetId, userId, response.promoCode,
                    response.currencyISO, response.subTotal, "eta");
            if (promoCode.get("type").toString().equalsIgnoreCase(KeysUtil.COMMISION_AMOUNT)) {
                response.promoAmount = Double.parseDouble(promoCode.get("value").toString());
            } else {
                response.promoAmount = (Double.parseDouble(promoCode.get("value").toString()) * response.subTotal)
                        / 100;
                if (response.promoAmount > Double.parseDouble(promoCode.get("maximumValue").toString()) &&
                        Double.parseDouble(promoCode.get("maximumValue").toString()) > 0)
                    response.promoAmount = Double.parseDouble(promoCode.get("maximumValue").toString());
            }
        }

        response.tax = taxSetting * response.subTotal * 0.01;

        response.totalWithoutPromo = response.subTotal + response.techFee + response.tax;

        response.etaFare = response.totalWithoutPromo - response.promoAmount;
        response.etaFare = response.etaFare + response.creditTransactionFee;
        if (response.etaFare < 0)
            response.etaFare = 0;
        response.unroundedTotalAmt = response.etaFare;
        response.etaFare = roundingValue(response.unroundedTotalAmt, fleet.rounding, response.currencyISO);
        if (response.etaFare > 0) {
            response.totalWithoutPromo = response.etaFare + response.promoAmount;
        } else if (response.promoAmount > 0) {
            response.totalWithoutPromo = roundingValue(response.totalWithoutPromo + response.creditTransactionFee,
                    fleet.rounding, response.currencyISO);
        }
        return response;
    };

    public double getDistanceBaseOnSetting(double distanceTour, double distanceGG, String driverId,
            GpsLocation gpsLocation) {
        double distance = distanceTour;
        String platform = "";
        Account driver = mongoDao.getAccount(driverId);
        if (driver != null) {
            platform = driver.platform;
        }
        if (gpsLocation != null) {
            if (gpsLocation.android != null && gpsLocation.android.enable && platform.equals("android")) {
                if (distanceTour < distanceGG * gpsLocation.android.value * 0.01) {
                    distance = distanceGG;
                }
            }
            if (gpsLocation.ios != null && gpsLocation.ios.enable && platform.equals("iOS")) {
                if (distanceTour < distanceGG * gpsLocation.ios.value * 0.01) {
                    distance = distanceGG;
                }
            }
            if (gpsLocation.androidGreater != null && gpsLocation.androidGreater.enable && platform.equals("android")) {
                if (distanceTour > distanceGG * gpsLocation.androidGreater.value * 0.01) {
                    distance = distanceGG;
                }
            }
            if (gpsLocation.iosGreater != null && gpsLocation.iosGreater.enable && platform.equals("iOS")) {
                if (distanceTour > distanceGG * gpsLocation.iosGreater.value * 0.01) {
                    distance = distanceGG;
                }
            }
        }
        return distance;
    }

    public Map<String, Object> getFleetCommission(String requestId, Fleet fleet, Account account, Booking booking,
            String zoneId) {
        String commissionType = "";
        double commission = 0.0;
        double insurance = 0.0;
        try {
            String jobType = booking.jobType != null ? booking.jobType : "";
            jobType = CommonUtils.getJobType(jobType, booking.bookFrom, booking.intercity, booking.delivery);
            logger.debug(requestId + " - jobType: " + jobType);
            String commissionDriverType = account.driverInfo.commissionDriverType != null
                    ? account.driverInfo.commissionDriverType
                    : "";
            logger.debug(requestId + " - commissionDriverType: " + commissionDriverType);
            // check if fleet enable new setting
            boolean differentFleetCommission = fleet.generalSetting != null
                    && fleet.generalSetting.differentFleetCommission;
            logger.debug(requestId + " - differentFleetCommission: " + differentFleetCommission);
            boolean userDefaultCommission = false;
            // new key is not exists => need to check if fleet enable different commission
            if (commissionDriverType.isEmpty()) {
                if (differentFleetCommission) {
                    userDefaultCommission = true;
                } else {
                    // use old setting
                    if (account.driverInfo.commissionType != null && !account.driverInfo.commissionType.equals(""))
                        commissionType = account.driverInfo.commissionType;
                    if (account.driverInfo.commissionByCurrencies != null
                            && !account.driverInfo.commissionByCurrencies.isEmpty()) {
                        // if type = percent, there is only ONE object on commissionByCurrencies
                        if (commissionType.equalsIgnoreCase("percent")) {
                            AmountByCurrency amountByCurrency = account.driverInfo.commissionByCurrencies.get(0);
                            commission = amountByCurrency.commissionValue;
                        } else {
                            for (AmountByCurrency amountByCurrency : account.driverInfo.commissionByCurrencies) {
                                if (amountByCurrency.currencyISO.equalsIgnoreCase(booking.currencyISO)) {
                                    commission = amountByCurrency.commissionValue;
                                    break;
                                }
                            }
                        }
                    }
                }
            } else {
                userDefaultCommission = false;
                if (commissionDriverType.equals("customize")) {
                    List<CommissionServices> services = account.driverInfo.commissionDriverValue;
                    if (services == null || services.isEmpty()) {
                        // use old setting
                        if (account.driverInfo.commissionType != null && !account.driverInfo.commissionType.equals(""))
                            commissionType = account.driverInfo.commissionType;
                        if (account.driverInfo.commissionByCurrencies != null
                                && !account.driverInfo.commissionByCurrencies.isEmpty()) {
                            // if type = percent, there is only ONE object on commissionByCurrencies
                            if (commissionType.equalsIgnoreCase("percent")) {
                                AmountByCurrency amountByCurrency = account.driverInfo.commissionByCurrencies.get(0);
                                commission = amountByCurrency.commissionValue;
                            } else {
                                for (AmountByCurrency amountByCurrency : account.driverInfo.commissionByCurrencies) {
                                    if (amountByCurrency.currencyISO.equalsIgnoreCase(booking.currencyISO)) {
                                        commission = amountByCurrency.commissionValue;
                                        break;
                                    }
                                }
                            }
                        }
                    } else {
                        // read new setting
                        for (CommissionServices commissionServices : services) {
                            if (jobType.equals(commissionServices.serviceType)) {
                                commissionType = commissionServices.type;
                                commission = commissionServices.value;
                                insurance = commissionServices.insurance;
                                break;
                            }
                        }
                        // check case fleet enable new service => no new serviceType
                        if (commissionType.isEmpty()) {
                            logger.debug(requestId + " - no setting for customize - use default commission");
                            userDefaultCommission = true;
                        }
                    }
                } else {
                    // check if fleet enable new setting
                    if (differentFleetCommission) {
                        userDefaultCommission = true;
                    } else {
                        // use old setting
                        if (account.driverInfo.commissionType != null && !account.driverInfo.commissionType.equals(""))
                            commissionType = account.driverInfo.commissionType;
                        if (account.driverInfo.commissionByCurrencies != null
                                && !account.driverInfo.commissionByCurrencies.isEmpty()) {
                            // if type = percent, there is only ONE object on commissionByCurrencies
                            if (commissionType.equalsIgnoreCase("percent")) {
                                AmountByCurrency amountByCurrency = account.driverInfo.commissionByCurrencies.get(0);
                                commission = amountByCurrency.commissionValue;
                            } else {
                                for (AmountByCurrency amountByCurrency : account.driverInfo.commissionByCurrencies) {
                                    if (amountByCurrency.currencyISO.equalsIgnoreCase(booking.currencyISO)) {
                                        commission = amountByCurrency.commissionValue;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            logger.debug(requestId + " - userDefaultCommission: " + userDefaultCommission);
            if (userDefaultCommission) {
                FleetFare fleetFare = mongoDao.getFleetFare(fleet.fleetId);
                String defaultFleetCommissionType = fleetFare.defaultFleetCommissionType != null
                        ? fleetFare.defaultFleetCommissionType
                        : "";
                if (defaultFleetCommissionType.equals("sameZones")) {
                    List<CommissionServices> services = fleetFare.defaultFleetCommissionValue.sameZones;
                    if (services != null && !services.isEmpty()) {
                        for (CommissionServices commissionServices : services) {
                            if (jobType.equals(commissionServices.serviceType)) {
                                commissionType = commissionServices.type;
                                commission = commissionServices.value;
                                insurance = commissionServices.insurance;
                                break;
                            }
                        }
                    }
                } else if (defaultFleetCommissionType.equals("differentZones")) {
                    List<DifferentZone> differentZones = fleetFare.defaultFleetCommissionValue.differentZones;
                    if (differentZones != null && !differentZones.isEmpty()) {
                        for (DifferentZone differentZone : differentZones) {
                            if (zoneId.equals(differentZone.zoneId)) {
                                List<CommissionServices> services = differentZone.value;
                                if (services != null && !services.isEmpty()) {
                                    for (CommissionServices commissionServices : services) {
                                        if (jobType.equals(commissionServices.serviceType)) {
                                            commissionType = commissionServices.type;
                                            commission = commissionServices.value;
                                            insurance = commissionServices.insurance;
                                            break;
                                        }
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - getFleetCommission exception: " + CommonUtils.getError(ex));
        }
        Map<String, Object> mapReturn = new HashMap<>();
        mapReturn.put("commissionType", commissionType);
        mapReturn.put("commission", commission);
        mapReturn.put("insurance", insurance);
        return mapReturn;
    }

    public String getSurchargeTypeRateFromBooking(Booking booking) {
        String surchargeTypeRate = "";
        String jobType = booking.jobType;
        if (booking.intercity) {
            jobType = "intercity";
        }
        if (booking.superHelper || booking.delivery) {
            jobType = "delivery";
        }
        if (booking.bookFrom.equals("Car-hailing")) {
            jobType = "carHailing";
        }
        int type = booking.request.type != null ? booking.request.type : 0;
        int typeRate = booking.request.typeRate;
        boolean normalFare = false;
        if (booking.request.estimate.fare != null) {
            normalFare = booking.request.estimate.fare.normalFare != null && booking.request.estimate.fare.normalFare;
        }
        switch (jobType) {
            case "rideHailing": {
                surchargeTypeRate = "regular";
                if (type == 0 || typeRate == 0) {
                    surchargeTypeRate = normalFare ? "regular" : "flat";
                }
                if (type == 3 || typeRate == 1) {
                    surchargeTypeRate = "hourly";
                }
                if (type == 4 || typeRate == 2) {
                    // book round trip
                    surchargeTypeRate = "flat";
                }
            }
                break;
            case "carHailing": {
                surchargeTypeRate = "carHailing";
            }
                break;
            case "shuttle": {
                surchargeTypeRate = "shuttle";
            }
        }
        return surchargeTypeRate;
    }

    public String getSurchargeTypeRate(String jobType, int typeRate, boolean normalFare) {
        String surchargeTypeRate = "";
        if (jobType != null && !jobType.isEmpty()) {
            switch (jobType) {
                case "transport":
                case "rideHailing": {
                    surchargeTypeRate = "regular";
                    if (typeRate == 0) {
                        surchargeTypeRate = normalFare ? "regular" : "flat";
                    }
                    if (typeRate == 1) {
                        surchargeTypeRate = "hourly";
                    }
                    if (typeRate == 2) {
                        // book round trip
                        surchargeTypeRate = "flat";
                    }
                }
                    break;
                case "carHailing": {
                    surchargeTypeRate = "carHailing";
                }
                    break;
                case "shuttle": {
                    surchargeTypeRate = "shuttle";
                }
            }
        }
        return surchargeTypeRate;
    }

    public String getFareType(Booking booking) {
        int typeRate = booking.request.typeRate;
        String fareType = "regular";
        switch (typeRate) {
            case 1:
            case 3:
                fareType = "flat";
                break;
            case 2:
                fareType = "hourly";
                break;
        }
        return fareType;
    }

    private double getPromoValue(String requestId, PromotionCode promotionCode, String userId, String currencyISO,
            double etaFare, String bookId) {
        double value = 0;
        try {
            if (promotionCode.valueByCurrencies != null && !promotionCode.valueByCurrencies.isEmpty()) {
                if (currencyISO.equals(promotionCode.valueByCurrencies.get(0).currencyISO)) {
                    value = promotionCode.valueByCurrencies.get(0).value;
                }
            }

            // check type percentage
            if (!promotionCode.type.equalsIgnoreCase("amount") && etaFare > 0) {
                value = CommonUtils.getRoundValue(etaFare * value / 100);
            }
            logger.debug(requestId + " - original value: " + value);
            // start check valid promo code
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Calendar cal = Calendar.getInstance();
            Fleet fleet = mongoDao.getFleetInfor(promotionCode.fleetId);
            String fromDate = sdf1.format(TimezoneUtil.offsetTimeZone(
                    TimezoneUtil.atStartOfDay(
                            TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), fleet.timezone)),
                    fleet.timezone, "GMT"));
            String toDate = sdf1.format(TimezoneUtil.offsetTimeZone(
                    TimezoneUtil.atEndOfDay(
                            TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), fleet.timezone)),
                    fleet.timezone, "GMT"));
            logger.debug(requestId + " - promotionCode: " + gson.toJson(promotionCode));
            if (promotionCode.budgetLimit.isLimited && userId != null && !userId.isEmpty()) {
                List<PromoCodeUse> promoCodeUses = sqlDao.findPromoCodeUseByPromoCodeAndCustomer(promotionCode._id.toString(),
                        userId);
                double totalUse = 0;
                double totalApplied = 0;
                logger.debug(requestId + " - bookId: " + bookId);
                for (PromoCodeUse promoCodeUse : promoCodeUses) {
                    logger.debug(requestId + " - promoCodeUse: " + gson.toJson(promoCodeUse));
                    logger.debug(requestId + " - usedValue: " + promoCodeUse.usedValue);
                    // don't count used amount if booking still not completed
                    if (promoCodeUse.ticketId != null && String.valueOf(promoCodeUse.ticketId).equals(bookId))
                        continue;

                    totalUse += promoCodeUse.usedValue;
                    totalApplied += promoCodeUse.usedValue;
                }
                logger.debug(requestId + " - totalUse: " + totalUse);
                totalUse = totalUse + value;
                logger.debug(requestId + " - totalUse = totalUse + value = " + totalUse);
                if (totalUse > promotionCode.budgetLimit.valueByCurrencies.get(0).value) {
                    if (promotionCode.applyLimitFinished) {
                        double budgetLimit = promotionCode.budgetLimit.valueByCurrencies.get(0).value;
                        double remainValue = CommonUtils.getRoundValue(budgetLimit - totalApplied);
                        logger.debug(requestId + " - budgetLimit: " + budgetLimit);
                        logger.debug(requestId + " - totalApplied: " + totalApplied);
                        logger.debug(requestId + " -> remainValue: " + remainValue);
                        if (remainValue < value)
                            value = remainValue;
                    }
                }
            }
            logger.debug(requestId + " -> value: " + value);
            if (promotionCode.budgetLimitDay != null && promotionCode.budgetLimitDay.isLimited && userId != null
                    && !userId.isEmpty()) {
                double totalUse = sqlDao.findPromoCodeUseByPromoCodeAndCustomerOnRange(promotionCode._id.toString(), userId,
                        fromDate, toDate);
                double totalApplied = totalUse;
                // add value of this time
                double useValue = value;
                // check type percentage
                if (!promotionCode.type.equalsIgnoreCase("amount") && etaFare > 0) {
                    useValue = CommonUtils.getRoundValue(etaFare * value / 100);
                }
                totalUse = totalUse + useValue;
                if (totalUse > promotionCode.budgetLimitDay.value) {
                    if (promotionCode.applyLimitFinished) {
                        double budgetLimit = promotionCode.budgetLimitDay.value;
                        double remainValue = CommonUtils.getRoundValue(budgetLimit - totalApplied);
                        if (remainValue < value)
                            value = remainValue;
                    }
                }
            }
            logger.debug(requestId + " -> value: " + value);
            if (promotionCode.budgetLimitMonth != null && promotionCode.budgetLimitMonth.isLimited && userId != null
                    && !userId.isEmpty()) {
                String startMonth = sdf1.format(TimezoneUtil.offsetTimeZone(
                        TimezoneUtil.atStartOfMonth(
                                TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), fleet.timezone)),
                        fleet.timezone, "GMT"));
                String endMonth = sdf1.format(TimezoneUtil.offsetTimeZone(
                        TimezoneUtil.atEndOfMonth(
                                TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), fleet.timezone)),
                        fleet.timezone, "GMT"));
                double totalUse = sqlDao.findPromoCodeUseByPromoCodeAndCustomerOnRange(promotionCode._id.toString(), userId,
                        startMonth, endMonth);
                double totalApplied = totalUse;
                // add value of this time
                double useValue = value;
                // check type percentage
                if (!promotionCode.type.equalsIgnoreCase("amount") && etaFare > 0) {
                    useValue = CommonUtils.getRoundValue(etaFare * value / 100);
                }
                totalUse = totalUse + useValue;
                if (totalUse > promotionCode.budgetLimitMonth.value) {
                    if (promotionCode.applyLimitFinished) {
                        double budgetLimit = promotionCode.budgetLimitMonth.value;
                        double remainValue = CommonUtils.getRoundValue(budgetLimit - totalApplied);
                        if (remainValue < value)
                            value = remainValue;
                    }
                }
            }
            logger.debug(requestId + " -> value: " + value);
            if (promotionCode.budgetLimitYear != null && promotionCode.budgetLimitYear.isLimited && userId != null
                    && !userId.isEmpty()) {
                String startYear = sdf1.format(TimezoneUtil.offsetTimeZone(
                        TimezoneUtil.atStartOfYear(
                                TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), fleet.timezone)),
                        fleet.timezone, "GMT"));
                String endYear = sdf1.format(TimezoneUtil.offsetTimeZone(
                        TimezoneUtil.atEndOfYear(
                                TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), fleet.timezone)),
                        fleet.timezone, "GMT"));
                double totalUse = sqlDao.findPromoCodeUseByPromoCodeAndCustomerOnRange(promotionCode._id.toString(), userId,
                        startYear, endYear);
                double totalApplied = totalUse;
                // add value of this time
                double useValue = value;
                // check type percentage
                if (!promotionCode.type.equalsIgnoreCase("amount") && etaFare > 0) {
                    useValue = CommonUtils.getRoundValue(etaFare * value / 100);
                }
                totalUse = totalUse + useValue;
                if (totalUse > promotionCode.budgetLimitYear.value) {
                    if (promotionCode.applyLimitFinished) {
                        double budgetLimit = promotionCode.budgetLimitYear.value;
                        double remainValue = CommonUtils.getRoundValue(budgetLimit - totalApplied);
                        if (remainValue < value)
                            value = remainValue;
                    }
                }
            }
            logger.debug(requestId + " -> value: " + value);
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }

        return value;
    }

    public double getQUpPreferred(String requestId, double buyPrice, int typeRate, String fleetId) {
        try {
            AffiliateMarkupPrice affiliateMarkupPrice = mongoDao.getAffiliateMarkupPrice(fleetId);
            String markupType = "";
            double markupValue = 0.0;
            if (affiliateMarkupPrice != null) {
                switch (typeRate) {
                    case 1: { // flat
                        if (affiliateMarkupPrice.flat != null) {
                            markupType = affiliateMarkupPrice.flat.type != null ? affiliateMarkupPrice.flat.type : "";
                            markupValue = affiliateMarkupPrice.flat.value;
                        }
                    }
                        break;
                    case 2: { // hourly
                        if (affiliateMarkupPrice.hourly != null) {
                            markupType = affiliateMarkupPrice.hourly.type != null ? affiliateMarkupPrice.hourly.type
                                    : "";
                            markupValue = affiliateMarkupPrice.hourly.value;
                        }
                    }
                        break;
                    default: { // regular
                        if (affiliateMarkupPrice.regular != null) {
                            markupType = affiliateMarkupPrice.regular.type != null ? affiliateMarkupPrice.regular.type
                                    : "";
                            markupValue = affiliateMarkupPrice.regular.value;
                        }
                    }
                }
            }
            logger.debug(requestId + " - markupType: " + markupType);
            logger.debug(requestId + " - markupValue: " + markupValue);
            if (markupType.equals("percent")) {
                markupValue = CommonUtils.getRoundValue(buyPrice * markupValue / 100);
            }
            logger.debug(requestId + " - markup amount: " + markupValue);
            return markupValue;
        } catch (Exception ex) {
            logger.debug(requestId + " - getQUpPreferred exception: " + CommonUtils.getError(ex));
            return 0;
        }
    }

    public String getCancelAmountForHydra(Booking booking, String requestId) {
        ObjResponse objResponse = new ObjResponse();
        Map<String, Object> mapData = new HashMap<>();

        double compensatePolicy = 0;
        double penalizePolicy = 0;
        String chargeType = "";
        try {
            AffiliateSetting affiliateSetting = mongoDao.getAffiliateSetting();
            com.qupworld.paymentgateway.models.mongo.collections.AffiliateSetting.CancellationPolicy cancellationPolicy = affiliateSetting != null
                    && affiliateSetting.cancellationPolicy != null ? affiliateSetting.cancellationPolicy : null;
            if (cancellationPolicy != null) {
                if (booking.status.equals(KeysUtil.PAYMENT_NOSHOW)) {
                    chargeType = "";
                    if (booking.reservation != null && booking.reservation) {
                        penalizePolicy = cancellationPolicy.reservation.penaltyCustomer;
                        compensatePolicy = cancellationPolicy.reservation.compensationSupplier;
                    } else {
                        penalizePolicy = cancellationPolicy.onDemand.penalty;
                        compensatePolicy = cancellationPolicy.onDemand.compensationSupplier;
                    }
                } else {
                    String canceller = booking.cancelInfo != null && booking.cancelInfo.canceller != null
                            ? booking.cancelInfo.canceller
                            : "";
                    switch (canceller) {
                        case "CC": {
                            if (booking.reservation != null && booking.reservation) {
                                penalizePolicy = cancellationPolicy.reservation.penaltySupplier;
                            }
                        }
                            break;
                        case "timeout": {
                            if (booking.reservation != null && booking.reservation) {
                                penalizePolicy = cancellationPolicy.reservation.penaltySupplier;
                            }
                        }
                            break;
                        case "passenger": {
                            if (booking.reservation != null && booking.reservation) {
                                penalizePolicy = cancellationPolicy.reservation.penaltyCustomer;
                                compensatePolicy = cancellationPolicy.reservation.compensationSupplier;
                            } else {
                                penalizePolicy = cancellationPolicy.onDemand.penalty;
                                compensatePolicy = cancellationPolicy.onDemand.compensationSupplier;
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - getCancelAmountForHydra exception: " + CommonUtils.getError(ex));
        }
        mapData.put("penalizePolicy", CommonUtils.getRoundValue(penalizePolicy));
        mapData.put("compensatePolicy", CommonUtils.getRoundValue(compensatePolicy));
        mapData.put("chargeType", chargeType);

        objResponse.returnCode = 200;
        objResponse.response = mapData;
        return objResponse.toString();
    }

    public org.json.JSONObject getGeneralServiceSetting(String requestId, Fleet fleet, String zoneId, String currencyISO) {
        String cache = redisDao.getTmp(requestId, "generalSetting"+requestId);
        if (!cache.isEmpty()) {
            return gson.fromJson(cache, org.json.JSONObject.class);
        }

        FleetFare fleetFare = mongoDao.getFleetFare(fleet.fleetId);
        ServiceFee sF = mongoDao.getServiceFeeByFleetIdAndZoneId(fleet.fleetId, zoneId);
        boolean rushHourActive;
        List<RushHour> rushHours = new ArrayList<>();
        boolean heavyTrafficActive;
        double heavyTraffic = 0.0;
        boolean tollFeeActive;
        int tollFeePayTo = 0;
        double tollFeeDriverCanInput = 0.0;
        boolean parkingFeeActive;
        int parkingFeePayTo = 0;
        double parkingFeeDriverCanInput = 0.0;
        boolean gasFeeActive;
        int gasFeePayTo = 0;
        double gasFeeDriverCanInput = 0.0;
        boolean otherFeesActive;
        double otherFees = 0.0;
        int otherFeesPayTo = 0;
        double otherFeesDriverCanInput = 0.0;
        boolean taxActive;
        double tax = 0.0;

        String applyType = fleetFare.applyType != null ? fleetFare.applyType : "all";
        if (sF != null && fleet.generalSetting.allowCongfigSettingForEachZone && !applyType.equals("all")) {
            // get custom setting apply for each zone
            // get heavy traffic
            heavyTrafficActive = sF.heavyTrafficActive != null && sF.heavyTrafficActive;
            if(heavyTrafficActive && sF.heavyTrafficByCurrencies!= null){
                AmountByCurrency heavy = sF.heavyTrafficByCurrencies.stream()
                        .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                        .findFirst().orElse(null);
                if(heavy != null)
                    heavyTraffic = heavy.value;
            }

            // get rush hour
            rushHourActive = sF.rushHourActive != null &&  sF.rushHourActive;
            if(rushHourActive){
                rushHours = sF.rushHours;
            }

            // get toll fee
            tollFeeActive = sF.tollFeeActive != null && sF.tollFeeActive;
            if (tollFeeActive) {
                tollFeePayTo = sF.tollFeePayTo;
                for(AmountByCurrency amountByCurrency: sF.tollFeeDriverCanInput){
                    if(amountByCurrency.currencyISO.equals(currencyISO))
                        tollFeeDriverCanInput = amountByCurrency.value;
                }
            }

            // get parking fee
            parkingFeeActive = sF.parkingFeeActive;
            if (parkingFeeActive) {
                parkingFeePayTo = sF.parkingFeePayTo;
                for(AmountByCurrency amountByCurrency: sF.parkingFeeDriverCanInput){
                    if(amountByCurrency.currencyISO.equals(currencyISO))
                        parkingFeeDriverCanInput = amountByCurrency.value;
                }
            }

            // get gas fee
            gasFeeActive = sF.gasFeeActive;
            if (gasFeeActive) {
                gasFeePayTo = sF.gasFeePayTo;
                for(AmountByCurrency amountByCurrency: sF.gasFeeDriverCanInput){
                    if(amountByCurrency.currencyISO.equals(currencyISO))
                        gasFeeDriverCanInput = amountByCurrency.value;
                }
            }

            // get other fees
            otherFeesActive = sF.otherFeeActive != null && sF.fleetServiceActive;
            if(otherFeesActive && sF.otherFee.valueByCurrencies != null) {
                AmountByCurrency otherFee = sF.otherFee.valueByCurrencies.stream()
                        .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                        .findFirst().orElse(null);
                if(otherFee != null)
                    otherFees = otherFee.value;
                for(AmountByCurrency amountByCurrency: sF.otherFee.otherFeeDriverCanInput){
                    if(amountByCurrency.currencyISO.equals(currencyISO))
                        otherFeesDriverCanInput = amountByCurrency.value;
                }
            }

            // get tax
            taxActive = sF.taxActive != null && sF.taxActive;
            if(taxActive) {
                tax = sF.tax;
            }
        } else {
            // get default general setting
            //get heavy traffic
            heavyTrafficActive = fleetFare.heavyTrafficActive != null && fleetFare.heavyTrafficActive;
            if(heavyTrafficActive && fleetFare.heavyTrafficByCurrencies!= null){
                AmountByCurrency heavy = fleetFare.heavyTrafficByCurrencies.stream()
                        .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                        .findFirst().orElse(null);
                if(heavy != null)
                    heavyTraffic = heavy.value;
            }

            // get rush hour
            rushHourActive = fleetFare.rushHourActive != null &&  fleetFare.rushHourActive;
            if(rushHourActive){
                rushHours = fleetFare.rushHours;
            }

            // get toll fee
            tollFeeActive = fleetFare.tollFeeActive != null && fleetFare.tollFeeActive;
            if (tollFeeActive) {
                tollFeePayTo = fleetFare.tollFeePayTo;
                for(AmountByCurrency amountByCurrency: fleetFare.tollFeeDriverCanInput){
                    if(amountByCurrency.currencyISO.equals(currencyISO))
                        tollFeeDriverCanInput = amountByCurrency.value;
                }
            }

            // get parking fee
            parkingFeeActive = fleetFare.parkingFeeActive;
            if (parkingFeeActive) {
                parkingFeePayTo = fleetFare.parkingFeePayTo;
                for(AmountByCurrency amountByCurrency: fleetFare.parkingFeeDriverCanInput){
                    if(amountByCurrency.currencyISO.equals(currencyISO))
                        parkingFeeDriverCanInput = amountByCurrency.value;
                }
            }

            // get gas fee
            gasFeeActive = fleetFare.gasFeeActive;
            if (gasFeeActive) {
                gasFeePayTo = fleetFare.gasFeePayTo;
                for(AmountByCurrency amountByCurrency: fleetFare.gasFeeDriverCanInput){
                    if(amountByCurrency.currencyISO.equals(currencyISO))
                        gasFeeDriverCanInput = amountByCurrency.value;
                }
            }

            // get other fees
            otherFeesActive = fleetFare.otherFeeActive != null && fleetFare.fleetServiceActive;
            if(otherFeesActive && fleetFare.otherFee.valueByCurrencies != null) {
                AmountByCurrency otherFee = fleetFare.otherFee.valueByCurrencies.stream()
                        .filter(amount -> amount.currencyISO.equalsIgnoreCase(currencyISO))
                        .findFirst().orElse(null);
                if(otherFee != null)
                    otherFees = otherFee.value;
                for(AmountByCurrency amountByCurrency: fleetFare.otherFee.otherFeeDriverCanInput){
                    if(amountByCurrency.currencyISO.equals(currencyISO))
                        otherFeesDriverCanInput = amountByCurrency.value;
                }
            }

            // get tax
            taxActive = fleetFare.taxActive != null && fleetFare.taxActive;
            if(taxActive) {
                tax = fleetFare.tax;
            }
        }
        org.json.JSONObject objData = new org.json.JSONObject();
        objData.put("rushHourActive", rushHourActive);
        objData.put("rushHours", rushHours);
        objData.put("heavyTrafficActive", heavyTrafficActive);
        objData.put("heavyTraffic", heavyTraffic);
        objData.put("tollFeeActive", tollFeeActive);
        objData.put("tollFeePayTo", tollFeePayTo);
        objData.put("tollFeeDriverCanInput", tollFeeDriverCanInput);
        objData.put("parkingFeeActive", parkingFeeActive);
        objData.put("parkingFeePayTo", parkingFeePayTo);
        objData.put("parkingFeeDriverCanInput", parkingFeeDriverCanInput);
        objData.put("gasFeeActive", gasFeeActive);
        objData.put("gasFeePayTo", gasFeePayTo);
        objData.put("gasFeeDriverCanInput", gasFeeDriverCanInput);
        objData.put("otherFeesActive", otherFeesActive);
        objData.put("otherFees", otherFees);
        objData.put("otherFeesPayTo", otherFeesPayTo);
        objData.put("otherFeesDriverCanInput", otherFeesDriverCanInput);
        objData.put("taxActive", taxActive);
        objData.put("tax", tax);
        redisDao.cacheData(requestId, "generalSetting"+requestId, objData.toString(), 1);
        return objData;
    }
}
