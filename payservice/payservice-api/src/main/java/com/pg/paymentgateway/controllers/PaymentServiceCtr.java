package com.pg.paymentgateway.controllers;

import com.google.gson.Gson;
import com.pg.util.CommonUtils;
import com.pg.util.ConvertUtil;
import com.pg.util.EstimateUtil;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.TimezoneUtil;
import com.qupworld.paymentgateway.entities.*;
import com.qupworld.paymentgateway.models.*;
import com.qupworld.paymentgateway.models.mongo.collections.*;
import com.qupworld.paymentgateway.models.mongo.collections.account.Account;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateAssignedRate.AffiliateAssignedRate;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareGeneral.RateGeneral;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateRoute.AffiliateRoute;
import com.qupworld.paymentgateway.models.mongo.collections.affiliationCarType.AffiliationCarType;
import com.qupworld.paymentgateway.models.mongo.collections.airportZone.AirportZone;
import com.qupworld.paymentgateway.models.mongo.collections.booking.Booking;
import com.qupworld.paymentgateway.models.mongo.collections.corporate.Corporate;
import com.qupworld.paymentgateway.models.mongo.collections.fareNormal.FareNormal;
import com.qupworld.paymentgateway.models.mongo.collections.fareNormal.FeesByCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.flatRoutes.FlatRoutes;
import com.qupworld.paymentgateway.models.mongo.collections.fleet.Fleet;
import com.qupworld.paymentgateway.models.mongo.collections.fleetfare.FleetFare;
import com.qupworld.paymentgateway.models.mongo.collections.fleetfare.RushHour;
import com.qupworld.paymentgateway.models.mongo.collections.serviceFee.ServiceFee;
import com.qupworld.paymentgateway.models.mongo.collections.vehicleType.CorpRate;
import com.qupworld.paymentgateway.models.mongo.collections.vehicleType.Rate;
import com.qupworld.paymentgateway.models.mongo.collections.vehicleType.VehicleType;
import com.qupworld.paymentgateway.models.mongo.collections.zone.Zone;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by qup on 7/13/16.
 * Business Logic Layer
 */
public class PaymentServiceCtr {
    private Gson gson;
    private MongoDao mongoDao;
    private SQLDao sqlDao;
    private EstimateUtil estimateUtil;
    private final SimpleDateFormat sdfMongo = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
    private final static Logger logger = LogManager.getLogger(PaymentServiceCtr.class);

    public PaymentServiceCtr() {
        gson = new Gson();
        sqlDao = new SQLDaoImpl();
        mongoDao = new MongoDaoImpl();
        estimateUtil = new EstimateUtil();
    }

    public String getGeneralSettingAffiliate(ETAFareEnt etaFareEnt) throws IOException {
        ObjResponse etaFareResponse = new ObjResponse();
        etaFareResponse.returnCode = 200;
        try {
            // get data request
            String fleetId = etaFareEnt.fleetId != null ? etaFareEnt.fleetId : "";
            String vehicleTypeId = etaFareEnt.vehicleTypeId != null ? etaFareEnt.vehicleTypeId : "";
            List<Double> pickup = etaFareEnt.pickup;

            String zoneId = "";
            RateGeneral rateGeneral = null;
            List<Zone> zone = mongoDao.findByGeoAffiliate(pickup, fleetId, vehicleTypeId);
            if (zone != null && !zone.isEmpty())
                zoneId = zone.get(0)._id.toString();
            if (!vehicleTypeId.isEmpty() && !zoneId.isEmpty()) {
                List<AffiliateAssignedRate> listAffiliateAssignedRate = mongoDao
                        .getListAffiliateAssignedRate(vehicleTypeId, zoneId);

                AffiliateAssignedRate assignedRateGeneral = listAffiliateAssignedRate.stream()
                        .filter(assignedRate -> assignedRate.type.equalsIgnoreCase(KeysUtil.GENERAL))
                        .findFirst().orElse(null);
                if (assignedRateGeneral != null) {
                    rateGeneral = mongoDao.getAffiliateRateGeneral(assignedRateGeneral.rate.id);
                }
            }
            if (rateGeneral != null) {
                // return to client
                etaFareResponse.response = rateGeneral;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(etaFareEnt.requestId + " - " + "getGeneralSettingAffiliateException : "
                    + CommonUtils.getError(ex) + "    data : " + etaFareEnt.toString());
            etaFareResponse.returnCode = 437;
        }
        return etaFareResponse.toString();
    }

    public String checkFlatRoutes(List<CheckFlatRoutesEnt> checkFlatRoutesEnts, String requestId) throws IOException {
        ObjResponse objResponse = new ObjResponse();
        objResponse.returnCode = 200;
        JSONArray jsonArray = new JSONArray();
        try {
            for (CheckFlatRoutesEnt checkFlatRoutesEnt : checkFlatRoutesEnts) {
                if (checkFlatRoutesEnt.validateData() == 200) {
                    JSONObject object = new JSONObject();
                    List<Boolean> checkRoutes = new ArrayList<>();
                    for (String flatRateId : checkFlatRoutesEnt.flatRateId) {
                        // check available flat route
                        AffiliateRoute flatRoutes = estimateUtil.getFareByFlatRoutesAffiliate(flatRateId,
                                checkFlatRoutesEnt.pickup, checkFlatRoutesEnt.destination,
                                checkFlatRoutesEnt.zipCodeFrom, checkFlatRoutesEnt.zipCodeTo);
                        AffiliateRoute flatRoutesReturn = estimateUtil.getFareByFlatRoutesReturnAffiliate(flatRateId,
                                checkFlatRoutesEnt.pickup, checkFlatRoutesEnt.destination,
                                checkFlatRoutesEnt.zipCodeFrom, checkFlatRoutesEnt.zipCodeTo);
                        if (flatRoutes == null && flatRoutesReturn == null) {
                            checkRoutes.add(false);
                        } else {
                            checkRoutes.add(true);
                        }
                    }
                    object.put("availble", checkRoutes);
                    jsonArray.add(object);
                }
            }
            objResponse.response = jsonArray;
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(requestId + " - " + "checkFlatRoutesException : " + CommonUtils.getError(ex) + "    data : "
                    + gson.toJson(checkFlatRoutesEnts));
            objResponse.returnCode = 437;
        }
        return objResponse.toString();
    }

    public String getCancelAmount(CancelBookingEnt cancelEnt) {
        ObjResponse objResponse = new ObjResponse();
        try {
            Booking booking = mongoDao.getBooking(cancelEnt.bookId);
            Fleet fleet = mongoDao.getFleetInfor(booking.fleetId);
            if (booking.pricingType == 0) {
                String cancelFrom = cancelEnt.from != null ? cancelEnt.from : "";
                return estimateUtil.getCancelAmount(booking, fleet, KeysUtil.canceled, cancelFrom, cancelEnt.requestId);
            } else {
                // return estimateUtil.getCancelAmountForAffiliate(booking,
                // booking.request.psgFleetId, KeysUtil.canceled, KeysUtil.SELLPRICE,
                // cancelEnt.requestId);
                return estimateUtil.getCancelAmountForHydra(booking, cancelEnt.requestId);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(cancelEnt.requestId + " - " + "getCancelAmount : " + CommonUtils.getError(ex) + "    data : "
                    + cancelEnt.toString());
            objResponse.returnCode = 304;
            return objResponse.toString();
        }

    }

    public String getPaymentData(String bookId, String requestId) {
        ObjResponse objResponse = new ObjResponse();
        objResponse.returnCode = 200;
        try {
            // query with DataBase
            Calendar cal = Calendar.getInstance();

            // get data from booking
            Booking booking = mongoDao.getBooking(bookId);

            // create new ticket
            PaymentSettingEnt paymentSetting = new PaymentSettingEnt();
            paymentSetting.fleetId = booking.fleetId;
            paymentSetting.bookId = bookId;
            paymentSetting.bookFrom = booking.bookFrom;
            paymentSetting.currencySymbol = booking.currencySymbol;
            paymentSetting.currencyISO = booking.currencyISO;
            paymentSetting.paymentType = booking.request.paymentType;
            // get token from booking
            String localToken = estimateUtil.getCreditToken(booking, true).get("token");
            String crossToken = estimateUtil.getCreditToken(booking, false).get("token");
            String token = localToken.equals("") ? crossToken : localToken;
            paymentSetting.token = token;

            // set pickup and droppedOff time with timezone GMT
            Date pickupTime = null;
            Date engagedTime = null;
            if (booking.time != null) {
                if (booking.time.pickUpTime != null) {
                    pickupTime = TimezoneUtil.offsetTimeZone(booking.time.pickUpTime,
                            cal.getTimeZone().getID(), "GMT");
                    logger.debug(requestId + " - " + "pickupTime: " + pickupTime);
                }

                if (booking.time.engaged != null) {
                    engagedTime = TimezoneUtil.offsetTimeZone(booking.time.engaged,
                            cal.getTimeZone().getID(), "GMT");
                    logger.debug(requestId + " - " + "engagedTime: " + engagedTime);
                }
            }
            Date droppedOffTime = TimezoneUtil.convertServerToGMT(new Date());
            logger.debug(requestId + " - " + "convert droppedOffTime -- server: " + new Date() + " ---- convert: "
                    + droppedOffTime);

            // set data from fleet setting
            Fleet fleet = mongoDao.getFleetInfor(booking.fleetId);
            FleetFare fleetFare = mongoDao.getFleetFare(booking.fleetId);

            // heavy traffic
            double heavyTraffic = 0;
            if (fleetFare.heavyTrafficActive && fleetFare.heavyTrafficByCurrencies != null) {
                AmountByCurrency heavy = fleetFare.heavyTrafficByCurrencies.stream()
                        .filter(amount -> amount.currencyISO.equalsIgnoreCase(booking.currencyISO))
                        .findFirst().orElse(null);
                if (heavy != null)
                    heavyTraffic = heavy.value;
            }
            paymentSetting.heavyTraffic = heavyTraffic;

            // airport surcharge
            double airportSurcharge = 0;
            boolean checkDes = false;
            AirportZone airportZone = mongoDao.checkDestinationByGeo(booking.request.destination.geo);
            if (airportZone != null) {
                checkDes = true;
            }
            if (booking.request.type == 1 && fleetFare.airport != null && fleetFare.airport.fromAirportActive
                    && fleetFare.airport.fromAirportByCurrencies != null) {
                AmountByCurrency fromAirport = fleetFare.airport.fromAirportByCurrencies.stream()
                        .filter(amount -> amount.currencyISO.equalsIgnoreCase(booking.currencyISO))
                        .findFirst().orElse(null);
                if (fromAirport != null)
                    airportSurcharge = fromAirport.value;
            } else if (fleetFare.airport != null && fleetFare.airport.toAirportActive
                    && fleet.additionalService.toAirport &&
                    checkDes && fleetFare.airport.toAirportByCurrencies != null) {
                AmountByCurrency toAirport = fleetFare.airport.toAirportByCurrencies.stream()
                        .filter(amount -> amount.currencyISO.equalsIgnoreCase(booking.currencyISO))
                        .findFirst().orElse(null);
                if (toAirport != null)
                    airportSurcharge = toAirport.value;
            }
            paymentSetting.airportSurcharge = airportSurcharge;

            // rush hour
            double distance = booking.droppedOffInfo != null && booking.droppedOffInfo.distanceTour != null
                    ? booking.droppedOffInfo.distanceTour
                    : 0;
            String unitDistance = fleet.unitDistance;
            if (unitDistance.equalsIgnoreCase(KeysUtil.UNIT_DISTANCE_IMPERIAL)) {
                distance = ConvertUtil.round((distance / 1609.344), 2);
            } else {
                distance = ConvertUtil.round((distance / 1000), 2);
            }
            double surchargeFee = 0;
            String surchargeType = "amount";
            if (fleetFare.rushHourActive) {
                String pickupTz = "";
                String dropOffTz = "";
                if (booking.request.pickup != null)
                    pickupTz = booking.request.pickup.timezone != null ? booking.request.pickup.timezone : "";
                if (booking.request.destination != null)
                    dropOffTz = booking.request.destination.timezone != null ? booking.request.destination.timezone
                            : "";
                String surchargeTypeRate = estimateUtil.getSurchargeTypeRateFromBooking(booking);
                String fareType = estimateUtil.getFareType(booking);
                logger.debug(requestId + " - surchargeTypeRate: " + surchargeTypeRate);
                if (surchargeTypeRate.equals("shuttle")) {
                    fareType = "shuttle";
                }
                logger.debug(requestId + " - fareType: " + fareType);
                if (engagedTime != null) {
                    List<Object> getRushHour = estimateUtil.getRushHour(fleet, fleetFare.rushHours, engagedTime,
                            droppedOffTime,
                            booking.currencyISO, pickupTz, dropOffTz, distance, surchargeTypeRate, fareType,
                            booking.reservation, requestId);
                    surchargeType = getRushHour.get(0).toString();
                    surchargeFee = Double.parseDouble(getRushHour.get(1).toString());
                } else {
                    if (pickupTime != null) {
                        List<Object> getRushHour = estimateUtil.getRushHour(fleet, fleetFare.rushHours, pickupTime,
                                droppedOffTime,
                                booking.currencyISO, pickupTz, dropOffTz, distance, surchargeTypeRate, fareType,
                                booking.reservation, requestId);
                        surchargeType = getRushHour.get(0).toString();
                        surchargeFee = Double.parseDouble(getRushHour.get(1).toString());
                    }
                }
            }
            paymentSetting.surchargeFee = surchargeFee;
            paymentSetting.surchargeType = surchargeType;

            // add tech fee data
            double techFee = 0;
            String zoneId = "";
            if (booking.request != null && booking.request.pickup != null) {
                List<Double> pickup = booking.request.pickup.geo;
                if (booking.request.pickup.zoneId != null && booking.request.pickup.zoneId.isEmpty()) {
                    zoneId = booking.request.pickup.zoneId;
                } else if (pickup != null) {
                    Zone zone = mongoDao.findByFleetIdAndGeo(fleet.fleetId, pickup);
                    if (zone != null) {
                        zoneId = zone._id.toString();
                    }
                }
            }
            List<Object> getTechFee = estimateUtil.getTechFeeValueByZone(fleet, booking.bookFrom, booking.currencyISO,
                    zoneId);
            techFee = Double.parseDouble(getTechFee.get(0).toString());
            paymentSetting.techFeeValue = techFee;
            paymentSetting.techFeeType = "amount";

            // tax
            paymentSetting.taxValue = fleetFare.tax;

            // meet driver
            double meetDriverFee = 0;
            if (booking.request.moreInfo != null) {
                if (booking.request.moreInfo.flightInfo != null) {
                    if (booking.request.moreInfo.flightInfo.type == 0) {
                        if (fleetFare.meetDriver.onCurbByCurrencies != null) {
                            AmountByCurrency onCurb = fleetFare.meetDriver.onCurbByCurrencies.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(booking.currencyISO))
                                    .findFirst().orElse(null);
                            if (onCurb != null)
                                meetDriverFee = onCurb.value;
                        }
                    }
                    if (booking.request.moreInfo.flightInfo.type == 1) {
                        if (fleetFare.meetDriver.meetDrvByCurrencies != null) {
                            AmountByCurrency meetDrv = fleetFare.meetDriver.meetDrvByCurrencies.stream()
                                    .filter(amount -> amount.currencyISO.equalsIgnoreCase(booking.currencyISO))
                                    .findFirst().orElse(null);
                            if (meetDrv != null)
                                meetDriverFee = meetDrv.value;
                        }
                    }
                }
            }
            paymentSetting.meetDriverFee = meetDriverFee;

            // other fee
            double otherFees = 0;
            if (fleetFare.otherFeeActive && fleetFare.otherFee.valueByCurrencies != null) {
                AmountByCurrency otherFee = fleetFare.otherFee.valueByCurrencies.stream()
                        .filter(amount -> amount.currencyISO.equalsIgnoreCase(booking.currencyISO))
                        .findFirst().orElse(null);
                if (otherFee != null)
                    otherFees = otherFee.value;
            }
            paymentSetting.otherFees = otherFees;

            // add commission data
            String mDispatcherId = "";
            String corporateId = "";
            if (booking.mDispatcherInfo != null && booking.mDispatcherInfo.userId != null)
                mDispatcherId = booking.mDispatcherInfo.userId;
            if (booking.corporateInfo != null && booking.corporateInfo.corporateId != null)
                corporateId = booking.corporateInfo.corporateId;
            List<Object> data = estimateUtil.getBookingFee(mDispatcherId, corporateId, booking.bookFrom,
                    booking.currencyISO);
            paymentSetting.commissionValue = Double.parseDouble(data.get(1).toString());
            paymentSetting.commissionType = data.get(0).toString().toLowerCase();
            if (KeysUtil.mDispatcher.equalsIgnoreCase(booking.bookFrom)) {
                paymentSetting.partnerId = mDispatcherId;
            } else if (KeysUtil.command_center.equalsIgnoreCase(booking.bookFrom) && !corporateId.isEmpty()) {
                paymentSetting.partnerId = corporateId;
            }

            // add coupon data
            String couponCode = booking.request.promo != null ? booking.request.promo : "";
            paymentSetting.promoCode = couponCode;
            double subTotal = booking.request.estimate.fare != null ? booking.request.estimate.fare.subTotal : 0.0;
            JSONObject couponData = estimateUtil.getPromoCode(requestId, fleet.fleetId, booking.psgInfo.userId,
                    couponCode, booking.currencyISO, subTotal, "");
            // JSONObject couponData = estimateUtil.getPromoCode(booking.fleetId,
            // couponCode, booking.currencyISO);
            /* logger.debug(requestId + " - couponData: " + couponData); */
            paymentSetting.couponType = couponData.get("type").toString().toLowerCase();
            PromoCodeUse promoCodeUse = sqlDao.getPromoCodeUseByBookId(bookId);
            logger.debug(requestId + " - promoCodeUse: " + gson.toJson(promoCodeUse));
            paymentSetting.couponValue = promoCodeUse != null && promoCodeUse.usedValue != null ? promoCodeUse.usedValue
                    : 0;
            paymentSetting.maximumValue = Double.parseDouble(couponData.get("maximumValue").toString());

            boolean forceMeter = false;
            try {
                Account account = mongoDao.getAccount(booking.drvInfo.userId);
                if (account != null) {
                    if (fleet.hardwareMeter) {
                        if (account.driverInfo.forceMeter) {
                            forceMeter = true;
                        }
                    }
                }
            } catch (Exception e) {
            }
            try {
                VehicleType vehicleType = mongoDao.getVehicleTypeByFleetAndName(booking.fleetId,
                        booking.request.vehicleTypeRequest);
                if (vehicleType.drvApp.enableEditFare) {
                    if (forceMeter) {
                        paymentSetting.editBasicFare = vehicleType.drvApp.drvForceMeter;
                    } else {
                        paymentSetting.editBasicFare = vehicleType.drvApp.drvNotForceMeter;
                    }
                }
                boolean rateByCorp = false;
                if (!corporateId.isEmpty()) {
                    Corporate corporate = mongoDao.getCorporate(corporateId);
                    if (corporate != null && corporate.pricing != null && corporate.pricing.differentRate != null)
                        rateByCorp = corporate.pricing.differentRate;
                }

                String fareNormalId = "";
                double minimum = 0;
                if (!zoneId.isEmpty()) {
                    if (rateByCorp) {
                        if (vehicleType.corpRates != null && !vehicleType.corpRates.isEmpty()) {
                            for (CorpRate corpRate : vehicleType.corpRates) {
                                if (corpRate.corporateId.equals(corporateId) && corpRate.rates != null && !corpRate.rates.isEmpty()) {
                                    for (Rate rate : corpRate.rates) {
                                        if (rate.rateType.equals(KeysUtil.REGULAR) && rate.zoneId.equals(zoneId))
                                            fareNormalId = rate.rateId;
                                    }
                                    break;
                                }
                            }
                        }
                    } else {
                        if (vehicleType.rates != null && !vehicleType.rates.isEmpty()) {
                            for (Rate rate : vehicleType.rates) {
                                if (rate.rateType.equals(KeysUtil.REGULAR) && rate.zoneId.equals(zoneId))
                                    fareNormalId = rate.rateId;
                            }
                        }
                    }
                }

                if (!fareNormalId.isEmpty()) {
                    FareNormal fareNormal = mongoDao.getFareNormalById(fareNormalId);
                    if (fareNormal.feesByCurrencies != null) {
                        for (FeesByCurrency feesByCurrency : fareNormal.feesByCurrencies) {
                            if (feesByCurrency.currencyISO.equals(booking.currencyISO)) {
                                // get minimum
                                if (!booking.reservation)
                                    minimum = feesByCurrency.minNow;
                                else
                                    minimum = feesByCurrency.minReservation;
                            }
                        }
                    }
                }

                logger.debug(requestId + " - " + "minimum:  " + minimum);
                paymentSetting.minimum = minimum;
                paymentSetting.editTax = vehicleType.editTax;
            } catch (Exception e) {
                logger.debug(requestId + " - " + "getVehicleTypeByFleetAndName ERROR: " + e.getMessage());
                paymentSetting.editBasicFare = false;
                paymentSetting.editTax = false;
                paymentSetting.minimum = 0;
            }
            paymentSetting.pricingType = booking.pricingType;
            // add rate setting active
            paymentSetting.techFeeActive = Boolean.valueOf(getTechFee.get(1).toString());
            paymentSetting.airportActive = fleetFare.airportActive;
            paymentSetting.rushHourActive = fleetFare.rushHourActive;
            paymentSetting.heavyTrafficActive = fleetFare.heavyTrafficActive;
            paymentSetting.otherFeeActive = fleetFare.otherFeeActive;
            paymentSetting.taxActive = fleetFare.taxActive;
            paymentSetting.meetDriverActive = fleet.additionalService.fromAirport.meetDriver;
            paymentSetting.editTip = fleet.editTip;
            paymentSetting.tipActive = fleetFare.tipActive;
            paymentSetting.editOtherFees = fleetFare.otherFee.isEdit;
            paymentSetting.actualFare = true;
            paymentSetting.tollFeeActive = fleetFare.tollFeeActive;
            paymentSetting.parkingFeeActive = fleetFare.parkingFeeActive;
            paymentSetting.gasFeeActive = fleetFare.gasFeeActive;
            paymentSetting.tollFee = 0;
            paymentSetting.parkingFee = 0;
            paymentSetting.gasFee = 0;
            // return to client
            objResponse.response = paymentSetting;

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(requestId + " - " + "getPaymentData exception : " + CommonUtils.getError(ex) + "    data : "
                    + bookId);
            objResponse.returnCode = 304;
        }
        return objResponse.toString();
    }

    public String getRushHour(String requestId, String bookId) throws IOException {
        ObjResponse objResponse = new ObjResponse();
        objResponse.returnCode = 200;
        try {
            // query with DataBase
            double surchargeFee = 0;
            double surchargeParameter = 0;
            String surchargeType = "amount";
            Booking booking = mongoDao.getBooking(bookId);
            if (booking != null) {
                Fleet fleet = mongoDao.getFleetInfor(booking.fleetId);
                FleetFare fleetFare = mongoDao.getFleetFare(booking.fleetId);
                Calendar cal = Calendar.getInstance();
                double distance = booking.droppedOffInfo != null && booking.droppedOffInfo.distanceTour != null
                        ? booking.droppedOffInfo.distanceTour
                        : 0;
                String unitDistance = fleet.unitDistance;
                if (unitDistance.equalsIgnoreCase(KeysUtil.UNIT_DISTANCE_IMPERIAL)) {
                    distance = ConvertUtil.round((distance / 1609.344), 2);
                } else {
                    distance = ConvertUtil.round((distance / 1000), 2);
                }
                Date pickupTime = null;
                Date engagedTime = null;
                Date droppedOffTime = null;
                if (booking.time != null) {
                    if (booking.time.pickUpTime != null) {
                        pickupTime = TimezoneUtil.offsetTimeZone(booking.time.pickUpTime,
                                cal.getTimeZone().getID(), "GMT");
                        logger.debug(requestId + " - " + "pickupTime: " + pickupTime);
                    }
                    if (booking.time.engaged != null) {
                        engagedTime = TimezoneUtil.offsetTimeZone(booking.time.engaged,
                                cal.getTimeZone().getID(), "GMT");
                        logger.debug(requestId + " - " + "engagedTime: " + engagedTime);
                    }

                    if (booking.time.droppedOff != null) {
                        droppedOffTime = TimezoneUtil.offsetTimeZone(booking.time.droppedOff,
                                cal.getTimeZone().getID(), "GMT");
                        logger.debug(requestId + " - " + "droppedOffTime: " + droppedOffTime);
                    } else {
                        droppedOffTime = TimezoneUtil.convertServerToGMT(new Date());
                    }
                }
                String pickupTz = "";
                String dropOffTz = "";
                if (booking.request.pickup != null)
                    pickupTz = booking.request.pickup.timezone != null ? booking.request.pickup.timezone : "";
                if (booking.request.destination != null)
                    dropOffTz = booking.request.destination.timezone != null ? booking.request.destination.timezone
                            : "";
                if (booking.pricingType == 0) {
                    String zoneId = "";
                    if (booking.request.pickup.zoneId != null && booking.request.pickup.zoneId.isEmpty()) {
                        zoneId = booking.request.pickup.zoneId;
                    } else {
                        Zone zone = mongoDao.findByFleetIdAndGeo(fleet.fleetId, booking.request.pickup.geo);
                        if (zone != null) {
                            zoneId = zone._id.toString();
                        }
                    }
                    List<RushHour> rushHours = new ArrayList<>();
                    ServiceFee sF = mongoDao.getServiceFeeByFleetIdAndZoneId(fleet.fleetId, zoneId);
                    if (sF != null && sF.rushHourActive && fleet.generalSetting.allowCongfigSettingForEachZone) {
                        rushHours = sF.rushHours;
                    }
                    if (fleetFare.applyType == null || fleetFare.applyType.equals("all")
                            || !fleet.generalSetting.allowCongfigSettingForEachZone) {
                        if (fleetFare.rushHourActive)
                            rushHours = fleetFare.rushHours;
                    }
                    String surchargeTypeRate = estimateUtil.getSurchargeTypeRateFromBooking(booking);
                    String fareType = estimateUtil.getFareType(booking);
                    logger.debug(requestId + " - surchargeTypeRate: " + surchargeTypeRate);
                    if (surchargeTypeRate.equals("shuttle")) {
                        fareType = "shuttle";
                    }
                    logger.debug(requestId + " - fareType: " + fareType);
                    if (engagedTime != null) {
                        List<Object> getRushHour = estimateUtil.getRushHour(fleet, rushHours, engagedTime,
                                droppedOffTime,
                                booking.currencyISO, pickupTz, dropOffTz, distance, surchargeTypeRate, fareType,
                                booking.reservation, requestId);
                        surchargeType = getRushHour.get(0).toString();
                        surchargeFee = Double.parseDouble(getRushHour.get(1).toString());
                    } else {
                        if (pickupTime != null) {
                            List<Object> getRushHour = estimateUtil.getRushHour(fleet, fleetFare.rushHours, pickupTime,
                                    droppedOffTime,
                                    booking.currencyISO, pickupTz, dropOffTz, distance, surchargeTypeRate, fareType,
                                    booking.reservation, requestId);
                            surchargeType = getRushHour.get(0).toString();
                            surchargeFee = Double.parseDouble(getRushHour.get(1).toString());
                        }
                    }

                    // get dynamic surcharge
                    if (fleet.generalSetting.dynamicSurcharge && !booking.reservation) {
                        if (booking.request != null && booking.request.estimate != null) {
                            if (booking.request.estimate.surchargeParameter != null
                                    && booking.request.estimate.surchargeParameter >= 1) {
                                surchargeParameter = booking.request.estimate.surchargeParameter;
                            } else if (booking.request.estimate.fare != null
                                    && booking.request.estimate.fare.surchargeParameter >= 1) {
                                surchargeParameter = booking.request.estimate.fare.surchargeParameter;
                            }
                        }
                    }
                } else {
                    // get rush hour for affiliate
                    String zoneId = "";
                    RateGeneral rateGeneral = null;
                    AffiliationCarType carType = null;
                    String vhcTypeId = "";
                    if (booking.request != null && booking.request.vehicleTypeRequest != null
                            && !booking.request.vehicleTypeRequest.isEmpty()) {
                        carType = mongoDao.getAffiliationCarTypeByName(booking.request.vehicleTypeRequest);
                        if (carType != null)
                            vhcTypeId = carType._id.toString();
                    }
                    if (booking.request != null && booking.request.pickup != null) {
                        List<Zone> zones = mongoDao.findByGeoAffiliate(booking.request.pickup.geo,
                                booking.request.psgFleetId, vhcTypeId);
                        if (zones != null && !zones.isEmpty())
                            zoneId = zones.get(0)._id.toString();
                    }

                    if (!vhcTypeId.isEmpty() && !zoneId.isEmpty()) {
                        List<AffiliateAssignedRate> listAffiliateAssignedRate = mongoDao
                                .getListAffiliateAssignedRate(vhcTypeId, zoneId);
                        AffiliateAssignedRate assignedRateGeneral = listAffiliateAssignedRate.stream()
                                .filter(assignedRate -> assignedRate.type.equalsIgnoreCase(KeysUtil.GENERAL))
                                .findFirst().orElse(null);
                        if (assignedRateGeneral != null) {
                            rateGeneral = mongoDao.getAffiliateRateGeneral(assignedRateGeneral.rate.id);
                        }
                        if (rateGeneral != null) {

                        }
                        if (rateGeneral != null && rateGeneral.rushHour != null && rateGeneral.rushHour &&
                                rateGeneral.rushHours != null && !rateGeneral.rushHours.isEmpty()) {
                            if (engagedTime != null) {
                                List<Object> getRushHour = estimateUtil.getRushHourAffiliate(fleet,
                                        rateGeneral.rushHours,
                                        engagedTime, droppedOffTime, pickupTz, dropOffTz, requestId);
                                surchargeType = getRushHour.get(0).toString();
                                surchargeFee = Double.parseDouble(getRushHour.get(1).toString());
                            } else {
                                if (pickupTime != null) {
                                    List<Object> getRushHour = estimateUtil.getRushHourAffiliate(fleet,
                                            rateGeneral.rushHours,
                                            pickupTime, droppedOffTime, pickupTz, dropOffTz, requestId);
                                    surchargeType = getRushHour.get(0).toString();
                                    surchargeFee = Double.parseDouble(getRushHour.get(1).toString());
                                }
                            }
                            surchargeType = "amount";
                        }
                    }
                }
            } else {
                logger.debug(requestId + " - " + "getRushHourException : booking null");
                objResponse.returnCode = 437;
            }
            JSONObject object = new JSONObject();
            object.put("surchargeType", surchargeType);
            object.put("surchargeFee", surchargeFee);
            object.put("surchargeParameter", surchargeParameter);
            objResponse.response = object;
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(requestId + " - " + "getRushHourException : " + CommonUtils.getError(ex));
            objResponse.returnCode = 437;
        }
        return objResponse.toString();
    }

    public String checkFareAvailable(String requestId, ETAFareEnt fareEnt) throws IOException {
        ObjResponse objResponse = new ObjResponse();
        objResponse.returnCode = 200;
        try {
            // check rate by corporate
            boolean rateByCorp = false;
            if (!fareEnt.corporateId.isEmpty()) {
                Corporate corporate = mongoDao.getCorporate(fareEnt.corporateId);
                rateByCorp = corporate != null && corporate.pricing != null && corporate.pricing.differentRate != null
                        &&
                        corporate.pricing.differentRate;
            }
            String zoneId = "";
            String fareHourlyId = "";
            String fareFlatId = "";
            String fareNormalId = "";
            String currencyISO = "";
            Zone zone = mongoDao.findByFleetIdAndGeo(fareEnt.fleetId, fareEnt.pickup);
            if (zone != null) {
                zoneId = zone._id.toString();
                if (zone.currency != null) {
                    currencyISO = zone.currency.iso;
                }
            }

            VehicleType vehicleType = null;
            if (!fareEnt.vehicleTypeId.isEmpty()) {
                vehicleType = mongoDao.getVehicleTypeByFleetAndName(fareEnt.fleetId, fareEnt.vehicleTypeId);
            }
            if (vehicleType != null && !zoneId.isEmpty()) {
                if (rateByCorp) {
                    if (vehicleType.corpRates != null && !vehicleType.corpRates.isEmpty()) {
                        for (CorpRate corpRate : vehicleType.corpRates) {
                            if (corpRate.corporateId.equals(fareEnt.corporateId) && corpRate.rates != null && !corpRate.rates.isEmpty()) {
                                for (Rate rate : corpRate.rates) {
                                    if (rate.rateType.equals(KeysUtil.REGULAR) && rate.zoneId.equals(zoneId))
                                        fareNormalId = rate.rateId;
                                    if (rate.rateType.equals(KeysUtil.FLAT))
                                        fareFlatId = rate.rateId;
                                    if (rate.rateType.equals(KeysUtil.HOURLY) && rate.zoneId.equals(zoneId))
                                        fareHourlyId = rate.rateId;
                                }
                                break;
                            }
                        }
                    }
                } else {
                    if (vehicleType.rates != null && !vehicleType.rates.isEmpty()) {
                        for (Rate rate : vehicleType.rates) {
                            if (rate.rateType.equals(KeysUtil.REGULAR) && rate.zoneId.equals(zoneId))
                                fareNormalId = rate.rateId;
                            if (rate.rateType.equals(KeysUtil.FLAT))
                                fareFlatId = rate.rateId;
                            if (rate.rateType.equals(KeysUtil.HOURLY) && rate.zoneId.equals(zoneId))
                                fareHourlyId = rate.rateId;
                        }
                    }
                }
            }
            logger.debug(requestId + " - " + "fareNormalId: " + fareNormalId);
            logger.debug(requestId + " - " + "fareFlatId: " + fareFlatId);
            logger.debug(requestId + " - " + "fareHourlyId: " + fareHourlyId);
            // check flat rate and round trip
            boolean flatRate = false;
            boolean roundTrip = false;
            if (!fareFlatId.isEmpty()) {
                FlatRoutes flatRoutes = estimateUtil.getFareByFlatRoutes(fareFlatId, fareEnt.pickup,
                        fareEnt.destination,
                        fareEnt.zipCodeFrom, fareEnt.zipCodeTo);
                FlatRoutes flatRoutesReturn = estimateUtil.getFareByFlatRoutesReturn(fareFlatId, fareEnt.pickup,
                        fareEnt.destination, fareEnt.zipCodeFrom, fareEnt.zipCodeTo);
                FlatRoutes flatRoutesRoundTrip = estimateUtil.getFareByFlatRoutesRoundTrip(fareFlatId, fareEnt.pickup,
                        fareEnt.destination, fareEnt.zipCodeFrom, fareEnt.zipCodeTo);
                if (flatRoutes != null) {
                    if (flatRoutes.singleTrip.departureRouteByCurrencies != null) {
                        for (AmountByCurrency amountByCurrency : flatRoutes.singleTrip.departureRouteByCurrencies) {
                            if (amountByCurrency.currencyISO.equals(currencyISO)) {
                                flatRate = true;
                                break;
                            }
                        }
                    }
                } else if (flatRoutesReturn != null) {
                    if (flatRoutesReturn.singleTrip.returnRouteByCurrencies != null) {
                        for (AmountByCurrency amountByCurrency : flatRoutesReturn.singleTrip.returnRouteByCurrencies) {
                            if (amountByCurrency.currencyISO.equals(currencyISO)) {
                                flatRate = true;
                                break;
                            }
                        }
                    }
                }
                if (flatRoutesRoundTrip != null) {
                    if (flatRoutesRoundTrip.roundTrip.roundTripFeeByCurrencies != null) {
                        for (AmountByCurrency amountByCurrency : flatRoutesRoundTrip.roundTrip.roundTripFeeByCurrencies) {
                            if (amountByCurrency.currencyISO.equals(currencyISO)) {
                                roundTrip = true;
                                break;
                            }
                        }
                    }
                }
            }

            JSONObject object = new JSONObject();
            object.put("roundTrip", roundTrip);
            object.put("flatRate", flatRate);
            object.put("regular", !fareNormalId.isEmpty());
            object.put("hourly", !fareHourlyId.isEmpty());
            objResponse.response = object;
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(requestId + " - " + "checkFareAvailable Exception : " + CommonUtils.getError(ex));
            objResponse.returnCode = 437;
        }
        return objResponse.toString();
    }

    public String saveSettlementHistory(SettlementHistoryEnt settlementHistoryEnt) {
        ObjResponse response = new ObjResponse();
        response.returnCode = 200;
        // settlementHistoryEnt.createdDate = ConvertUtil.getUTCISOTime("GMT");
        try {
            Date from = KeysUtil.SDF_DMYHMSTZ.parse(settlementHistoryEnt.dateRangeFrom.replace("Z", ""));
            settlementHistoryEnt.dateRangeFrom = KeysUtil.SDF_DMYHMS.format(from);

            Date to = KeysUtil.SDF_DMYHMSTZ.parse(settlementHistoryEnt.dateRangeTo.replace("Z", ""));
            settlementHistoryEnt.dateRangeTo = KeysUtil.SDF_DMYHMS.format(to);

            Date create = KeysUtil.SDF_DMYHMSTZ.parse(settlementHistoryEnt.createdDate.replace("Z", ""));
            settlementHistoryEnt.createdDate = KeysUtil.SDF_DMYHMS.format(create);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        SettlementHistory history = sqlDao.saveSettlementHistory(settlementHistoryEnt);
        if (history == null) {
            // something went wrong, try to add again
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ignore) {
            }
            history = sqlDao.saveSettlementHistory(settlementHistoryEnt);
            if (history == null) {
                logger.debug(settlementHistoryEnt.requestId + " - SettlementHistoryEnt: "
                        + gson.toJson(settlementHistoryEnt));
                response.returnCode = 306;
                response.response = "";
            }
        }
        // response.response = gson.fromJson(gson.toJson(history), JSONObject.class);
        response.response = history;
        return response.toString();
    }

    public String reportSettlementHistory(ReportSettlementEnt reportSettlementEnt) throws ParseException {
        ObjResponse objResponse = new ObjResponse();
        SimpleDateFormat dateF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

        Calendar cal = Calendar.getInstance();

        reportSettlementEnt.fromDate = KeysUtil.SDF_DMYHMS.format(dateF.parse(reportSettlementEnt.fromDate));
        reportSettlementEnt.toDate = KeysUtil.SDF_DMYHMS.format(dateF.parse(reportSettlementEnt.toDate));
        try {
            List<SettlementReturn> list = new ArrayList<>();
            int size = 0;
            double from = 0.0;
            JSONObject result = sqlDao.reportSettlementEnt(reportSettlementEnt);

            JSONParser parser = new JSONParser();
            JSONObject jsonDetails = (JSONObject) parser.parse(result.get("list").toString());

            size = Integer.parseInt(result.get("size").toString());
            from = Double.parseDouble(result.get("from").toString());

            JSONArray fields = (JSONArray) jsonDetails.get("fields");
            List<String> listKeys = new ArrayList<>();
            for (Object field : fields) {
                JSONObject object = (JSONObject) field;
                listKeys.add(object.get("name").toString());
            }

            JSONArray records = (JSONArray) jsonDetails.get("records");
            for (Object record : records) {
                JSONArray object = (JSONArray) record;
                SettlementReturn settlementReturn = new SettlementReturn();
                settlementReturn.driverId = object.get(listKeys.indexOf("driverId")).toString();
                settlementReturn.driverName = object.get(listKeys.indexOf("driverName")) != null
                        ? object.get(listKeys.indexOf("driverName")).toString()
                        : "";
                settlementReturn.companyId = object.get(listKeys.indexOf("companyId")) != null
                        ? object.get(listKeys.indexOf("companyId")).toString()
                        : "";
                settlementReturn.totalUnsettledAmount = Double
                        .parseDouble(object.get(listKeys.indexOf("total")).toString());
                settlementReturn.totalUnsettledTransactions = Integer
                        .parseInt(object.get(listKeys.indexOf("size")).toString());
                settlementReturn.books = new ArrayList<String>(
                        Arrays.asList(object.get(listKeys.indexOf("books")).toString().split(",")));
                list.add(settlementReturn);
            }
            JSONObject summaryDetails = (JSONObject) parser.parse(result.get("summary").toString());
            JSONArray fieldsSummary = (JSONArray) summaryDetails.get("fields");
            List<String> listKeysSummary = new ArrayList<>();
            for (Object field : fieldsSummary) {
                JSONObject object = (JSONObject) field;
                listKeysSummary.add(object.get("name").toString());
            }
            JSONArray recordsSummary = (JSONArray) summaryDetails.get("records");
            JSONArray object = (JSONArray) recordsSummary.get(0);
            JSONObject summary = new JSONObject();
            summary.put("totalDriver",
                    object.get(listKeysSummary.indexOf("totalDriver")) != null
                            ? Integer.parseInt(object.get(listKeysSummary.indexOf("totalDriver")).toString())
                            : 0);
            summary.put("totalUnsettledAmount",
                    object.get(listKeysSummary.indexOf("totalUnsettledAmount")) != null
                            ? Double.parseDouble(object.get(listKeysSummary.indexOf("totalUnsettledAmount")).toString())
                            : 0.00);
            summary.put("totalUnsettledTransactions",
                    object.get(listKeysSummary.indexOf("totalUnsettledTransactions")) != null ? Integer.parseInt(
                            object.get(listKeysSummary.indexOf("totalUnsettledTransactions")).toString()) : 0);

            Map<String, Object> mapResponse = new HashMap<>();
            mapResponse.put("size", size);
            mapResponse.put("from", from);
            mapResponse.put("total", list.size());
            mapResponse.put("list", list);
            mapResponse.put("summary", summary);
            objResponse.response = mapResponse;
            objResponse.returnCode = 200;
            return objResponse.toString();
        } catch (Exception ex) {
            objResponse.returnCode = 437;
            Map<String, String> map = new HashMap<>();
            map.put("message", CommonUtils.getError(ex));
            objResponse.response = map;
            return objResponse.toString();
        }
    }

    public String updatePaidToDriver(ReportSettlementEnt reportSettlementEnt) throws ParseException {
        ObjResponse objResponse = new ObjResponse();
        try {
            int result = sqlDao.updatePaidToDriver(reportSettlementEnt);
            Map<String, String> map = new HashMap<>();
            if (result > 0) {
                map.put("message", "Update successfully");
                objResponse.response = map;
                objResponse.returnCode = 200;
            } else {
                objResponse.returnCode = 437;
                map.put("message", "Update failed");
                objResponse.response = map;
            }
            return objResponse.toString();
        } catch (Exception ex) {
            objResponse.returnCode = 437;
            Map<String, String> map = new HashMap<>();
            map.put("message", CommonUtils.getError(ex));
            objResponse.response = map;
            return objResponse.toString();
        }
    }

    public String updateAffiliateRate(int value, String priceType) throws ParseException {
        ObjResponse response = new ObjResponse();
        response.response = estimateUtil.updateAffiliateRate(value, priceType);
        return response.toString();
    }
}