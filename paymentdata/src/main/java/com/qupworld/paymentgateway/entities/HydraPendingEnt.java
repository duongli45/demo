package com.qupworld.paymentgateway.entities;

import java.util.UUID;

/**
 * Created by thuanho on 16/11/2022.
 */
public class HydraPendingEnt {

    public String bookId;
    public String action;
    public String operatorId;
    public String note;
    public String fleetId;
    public String userId;
    public String token;
    public String currencyISO;
    public boolean chargeSucceed;

    // for log
    public String requestId = "";

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }
}
