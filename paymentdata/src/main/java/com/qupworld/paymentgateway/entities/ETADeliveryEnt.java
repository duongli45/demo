package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.List;
import java.util.UUID;

/**
 * Created by myasus on 6/11/20.
 */
public class ETADeliveryEnt implements PaymentIdentifiable {
    public String fleetId;
    public String bookId;
    public String vehicleTypeId;
    public String bookFrom;
    public String pickupTime;
    public String userId;
    public String corporateId;
    public String city;
    public String timezone;
    public String promoCode;
    public String menuId;
    public String currencyISO;
    public String requestId;
    public String zoneId;
    public String language;
    public int deliveryType;
    public int paymentMethod;
    public boolean cashOnPickUp;
    public LocationDetails pickup;
    public List<LocationDetails> merchants;
    public List<LocationDetails> recipients;
    public List<RateDetails> rateDetails;
    public String companyId;

    //for edit fare
    public EditFare editFare;

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    public int validateData() throws JsonProcessingException {
        if (this.rateDetails == null || this.rateDetails.isEmpty()) {
            if (this.isEmptyString(this.fleetId) || this.isEmptyString(this.bookFrom) ||
                    ((this.deliveryType == 0 || this.deliveryType == 1) && this.isEmptyString(this.vehicleTypeId))) {
                return 406;
            }
        }
        return 200;
    }

    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }

    @Override
    public String getRequestId() {
        return requestId;
    }
}
