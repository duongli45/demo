
package com.qupworld.paymentgateway.entities.FawryFormEnt;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "amt",
    "curCode"
})
public class PmtAmt {

    @JsonProperty("amt")
    private Double amt;
    @JsonProperty("curCode")
    private String curCode;

    @JsonProperty("amt")
    public Double getAmt() {
        return amt;
    }

    @JsonProperty("amt")
    public void setAmt(Double amt) {
        this.amt = amt;
    }

    @JsonProperty("curCode")
    public String getCurCode() {
        return curCode;
    }

    @JsonProperty("curCode")
    public void setCurCode(String curCode) {
        this.curCode = curCode;
    }

}
