package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.UUID;

/**
 * Created by qup on 11/03/2019.
 */
public class WalletTransferEnt {

    public String fleetId;
    public String driverId;
    public double amount;
    public String currencyISO;
    public String referenceId;
    public String operatorId;
    public String reason;
    public boolean isPayout;
    public String wingBankAccount;

    // for request info
    public String destination; //bankAccount, creditWallet

    // for log
    public String requestId;


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    public int validateData() throws JsonProcessingException {
        if (this.isEmptyString(this.fleetId) || this.isEmptyString(this.driverId) || this.isEmptyString(this.currencyISO) || this.isEmptyString(this.destination)
                || amount <= 0.0) {
            return 406;
        }
        return 200;
    }

    public int validateUpdate() throws JsonProcessingException {
        if (this.isEmptyString(this.fleetId) || this.isEmptyString(this.driverId) || this.isEmptyString(this.currencyISO)) {
            return 406;
        }
        return 200;
    }
    public int validateDataHandleRequest() throws JsonProcessingException {
        if (this.isEmptyString(this.fleetId) || this.isEmptyString(this.referenceId) || this.isEmptyString(this.operatorId)) {
            return 406;
        }
        return 200;
    }
    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }
}
