
package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.UUID;

public class PrePaidByWalletEnt {

    public String fleetId;
    public String paymentType;
    public String gateway;
    public String boostId;
    public String bookId;
    public String phone;
    public double total;
    public String customerId;
    public boolean walletAppInstalled;
    public String currencyISO;

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    /**
     * Valid a Credit Card Data
     */
    public int validateData() throws JsonProcessingException {
        if (this.isEmptyString(this.fleetId) || this.isEmptyString(this.paymentType) || this.isEmptyString(this.bookId) ||
                this.isEmptyString(this.phone) || this.total <= 0.0 || this.isEmptyString(this.currencyISO)) {
            return 406;
        }
        return 200;
    }
    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }
}
