
package com.qupworld.paymentgateway.entities;


import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.UUID;

public class SignatureForm {

    public String fleetId;
    public String type;
    public String data;
    public String bookId;

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    public int validateData() throws JsonProcessingException {
        if(this.isEmptyString(this.type)||
                this.isEmptyString(this.data)||
                this.isEmptyString(this.bookId)){
            return 406;
        }
        return 200;
    }
    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }
}
