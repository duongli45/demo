package com.qupworld.paymentgateway.entities;

/**
 * Created by myasus on 12/13/21.
 */
public class Passenger {
    public int order;
    public double baseFare;
    public double techFee;
    public double totalFare;
}
