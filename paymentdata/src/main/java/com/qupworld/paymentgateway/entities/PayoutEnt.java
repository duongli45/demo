package com.qupworld.paymentgateway.entities;

import java.util.List;
import java.util.UUID;

/**
 * Created by qup on 9/22/16.
 */
public class PayoutEnt {

    public String fleetId;
    public boolean isPayAll;
    public List<String> listDriver;
    public List<String> listMerchant;
    public String operatorId;
    public String payoutDate;
    public double minPayoutAmount;
    public double holdAmount;
    public String currencyISO;
    public String email;
    public String companyId;

    // for log
    public String requestId = "";

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    /**
     * Valid a Credit Card Data
     */
    public int validateData() {
        if (this.isEmptyString(this.fleetId) || this.isEmptyString(this.payoutDate) || this.isEmptyString(this.email)) {
            return 406;
        }
        return 200;
    }
    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }
}

