package com.qupworld.paymentgateway.entities;

import java.sql.Timestamp;

/**
 * Created by qup on 26/11/2018.
 */
public class HydraIncome {

    public String bookId = "";

    // save dmc fleet info
    public String dmcFleetId = "";
    public String dmcFleetName = "";

    // save supplier
    public String supplierFleetId = "";
    public String supplierFleetName = "";

    // save pre charge info
    public Double amount = 0.0;
    public Double refundAmount = 0.0;
    public String currencyISO = "";
    public String currencySymbol = "";
    public String reason = "";
    public String status = "";
    public String chargeId = "";
    public Timestamp createdDate;
    public Timestamp paidDate;
    public Timestamp refundedDate;

    public String driverName = "";
    public String passengerName = "";
    public String transactionStatus = "";
    public String paidBy = "";
    public String canceller = "";
    public Double compensationAmount;
    public Double penaltyAmount = 0.0;
    public Double grossProfit = 0.0;
    public Double totalCollect = 0.0;
    public Double penaltyPercent = 0.0;
    public Double compensationPercent = 0.0;
    public String penaltyReason = "";
    public String penaltyType = "";
    public String penaltyDuration = "";
    public String penaltyNote = "";
    public Timestamp completedTime;
    public Timestamp happenedTime;
    public Boolean isFinal = false;
    public String itineraryId = "";
    public String eventId = "";
    public String eventName = "";
    public String companyId = "";
    public String companyName = "";
    public Integer travelerType = 0;
    public Double quoted = 0.0;
    public String pickup = "";
    public String timeZonePickUp = "";
    public String destination = "";
    public String timeZoneDestination = "";
    public Timestamp pickupTime;
    public String passengerPhone = "";
    public String driverPhone = "";
    public double exchangeRate = 1.0;
    public String plateNumber = "";
    public double estimatedSellPrice = 0.0;
    public double estimatedBuyPrice = 0.0;
    public String supplierOperatorName = "";
    public String supplierOperatorUsername = "";
    public int dispatchType = 0;
    public String bookFrom = "";
    public double totalFare = 0.0;
    public boolean isFarmOut = false;
}
