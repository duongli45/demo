
package com.qupworld.paymentgateway.entities;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pg.util.ValidCreditCard;

import java.util.List;
import java.util.UUID;

public class CreditEnt {

    public String fleetId;
    public String cardNumber;
    public String expiredDate;
    public String cvv;
    public String cardHolder;
    public String postalCode;
    public String userId;
    public String customerId;
    public String driverId;
    public String isPayment;
    public boolean isCrossZone;
    public String phone;
    public String creditPhone;
    public String data3ds;
    public String rv;
    public String type; // user or corporate
    public String corporateId;
    public String from;

    // add info for AVS verification
    public String street;
    public String city;
    public String state;
    public String country;

    // used to add credit with card info through SDK
    public String token = "";
    public boolean sca = false;

    // for Avis/Yeepay
    public String creditType = "";
    public String creditCode = "";

    // for the gateway which required customer name and email
    public String name = "";
    public String email = "";

    // for multi gateway
    public List<Double> geoLocation;
    public String zoneId;

    public String sessionId;
    public String firstName;
    public String lastName;

    public boolean oneTimeUse = false;

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    /**
     * Valid a Credit Card Data
     */
    public int validateData() throws JsonProcessingException {
        String data3ds = this.data3ds != null ? this.data3ds : "";
        if (!data3ds.equals("")) {
            return 200;
        }
        String token = this.token != null ? this.token : "";
        if (!token.equals("")) {
            return 200;
        }

        // all credit information is empty - request initial credit form from the gateway URL
        if(!this.isEmptyString(this.fleetId) &&
                this.isEmptyString(this.cardNumber) &&
                this.isEmptyString(this.expiredDate) &&
                this.isEmptyString(this.cardHolder) &&
                this.isEmptyString(this.cvv)){
            return 200;
        }
        if(this.isEmptyString(this.fleetId)||
                this.isEmptyString(this.cardNumber)||
                this.isEmptyString(this.expiredDate)||
                this.isEmptyString(this.cardHolder)||
                this.isEmptyString(this.cvv)){
            return 406;
        }
        int errorCode = 0;
        if (this.isEmptyString(this.cardHolder)) errorCode = 462;
        if (!ValidCreditCard.checkCardNumber(this.cardNumber)) errorCode = 421;
        if (!ValidCreditCard.checkExpiredDate(this.expiredDate)) errorCode = 422;
        if (!ValidCreditCard.checkCVV(this.cvv)) errorCode = 432;
        if (errorCode != 0) {
            return errorCode;
        }
        return 200;
    }

    public int validateCardnumber() throws JsonProcessingException {
        if(this.isEmptyString(this.fleetId)||
                this.isEmptyString(this.cardNumber)){
            return 406;
        }
        return 200;
    }
    /**
     * Valid a Credit Card postalCode
     */
    public int getCarType() throws JsonProcessingException {


        return 200;
    }
    /**
     * Valid a Credit Card postalCode
     */
    public int validatePostalCode() throws JsonProcessingException {
        if(this.isEmptyString(this.postalCode)){
            return 406;
        }
        if (!ValidCreditCard.checkPostalCode(this.postalCode)){
            return  429;
        }

        return 200;
    }

    /**
     *
     * @return
     * boolean
     */
    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }
    @Override
    public String toString() {
        Gson gson = new GsonBuilder().serializeNulls().create();
        String strCredit = gson.toJson(this);
        if (this.cardNumber != null && this.cardNumber.length() > 4)
            strCredit = strCredit.replace(this.cardNumber,"XXXXXX"+this.cardNumber.substring(this.cardNumber.length()-4));
        return strCredit;
    }
}