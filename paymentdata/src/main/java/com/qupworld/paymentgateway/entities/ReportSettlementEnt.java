package com.qupworld.paymentgateway.entities;


import javax.annotation.Generated;
import java.util.List;
import java.util.UUID;

/**
 * Created by hoangnguyen on 3/20/19.
 */
@Generated("org.jsonschema2pojo")
public class ReportSettlementEnt implements PaymentIdentifiable {
    public int from;
    public int size;
    public String fleetId;
    public String timezone;
    public String unitDistance;
    public String fromDate;
    public String toDate;
    public String companyId;
    public String driverId;
    public List<String> paymentMethod;
    public List<String> books;
    public String bookingService;
    public String currency;
    public String requestId;

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    public int validateData() {
        if (this.isEmptyString(this.fleetId) || this.isEmptyString(this.driverId)
                || this.isEmptyString(this.companyId)) {
            return 406;
        }
        return 200;
    }
    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }

    @Override
    public String getRequestId() {
        return requestId;
    }
}