package com.qupworld.paymentgateway.entities;


import org.json.simple.JSONObject;

import java.util.List;
import java.util.Map;

/**
 * Created by myasus on 6/19/20.
 */
public class MenuData {
    public String parentItemId;
    public String componentType;
    public List<String> children;
    public String fieldKey;
    public String fieldType;
    public JSONObject fieldValue;
}
