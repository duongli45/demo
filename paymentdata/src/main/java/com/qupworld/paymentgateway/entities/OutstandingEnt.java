package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.List;
import java.util.UUID;

/**
 * Created by qup on 13/08/2019.
 */
public class OutstandingEnt {

    public String bookId;
    public String fleetId;
    public String type;
    public String reason;
    public String userId;
    public String currencyISO;

    //for payment
    public String paymentType; // paxWallet, credit, prePaid, vippsWallet
    public String token;
    public String corporateId;
    public double amount;
    public String zoneId;
    public List<Double> geoLocation;
    public boolean isResetCache = false;
    public boolean walletAppInstalled = false;

    // used to store temp data
    public String tmpKey;

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    public int validateData() throws JsonProcessingException {
        if (this.isEmptyString(this.fleetId) || this.isEmptyString(this.type) || this.isEmptyString(this.reason) || this.isEmptyString(this.userId)) {
            return 406;
        }
        return 200;
    }

    public int validateGetAmount() throws JsonProcessingException {
        if (this.isEmptyString(this.fleetId) || this.isEmptyString(this.userId) || this.isEmptyString(this.currencyISO)) {
            return 406;
        }
        return 200;
    }

    public int validatePay() throws JsonProcessingException {
        if (this.isEmptyString(this.fleetId) || this.isEmptyString(this.userId) || this.isEmptyString(this.currencyISO)
                || this.isEmptyString(this.paymentType) || this.amount <=0
                || (this.paymentType.equals("credit") && this.isEmptyString(this.token))
                || (this.paymentType.equals("prePaid") && this.isEmptyString(this.corporateId))
                || (this.paymentType.equals("corporateCard") && this.isEmptyString(this.token) && this.isEmptyString(this.corporateId))
                ) {
            return 406;
        }
        return 200;
    }

    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }

}
