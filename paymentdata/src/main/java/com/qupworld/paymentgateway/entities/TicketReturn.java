
package com.qupworld.paymentgateway.entities;

import com.qupworld.paymentgateway.models.mongo.collections.FleetService;

import javax.annotation.Generated;
import java.sql.Timestamp;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class TicketReturn {

    //// PAYMENT DATA
    public Long id;
    public String fleetId;
    public String bookId;
    public int paymentType;
    public double total;
    public double subTotal;
    public double techFee;
    public double fare;
    public double oldBasicFare;
    public double calBasicFare;
    public double tip;
    public double tax;
    public String promoCode;
    public double promoAmount;
    public double meetDriverFee;
    public double heavyTraffic;
    public double airportSurcharge;
    public double partnerCommission;
    public double otherFees;
    public double rushHour;
    public int isMinimum;
    public String transactionStatus;
    public String paidBy;
    public String approvalCode; // returned from gateway
    public String cardType;
    public String cardMasked;
    public double tollFee;
    public double parkingFee;
    public double gasFee;
    public double serviceFee;
    public double dynamicSurcharge;
    public double surchargeParameter;
    public double dynamicFare;
    public double authAmount;
    public String otherFeesDetails;
    public double surchargeFee;
    public String surchargeType;
    public Timestamp paymentTime;
    public Timestamp expectedPickupTime;
    public boolean isPending;
    public double originalOutstandingAmount;
    public double currentOutstandingAmount;
    public String paymentStatus;
    public double unroundedTotalAmt;
    public double receivedAmount;
    public double transferredAmount;
    public double itemValue;
    public double creditTransactionFee;
    public boolean completedWithoutService;
    public double driverTax;
    public double merchantCommission;

    //// TICKET DATA
    public String bookFrom;
    public String plateNumber;
    public String customerName;
    public String psgEmail;
    public String driverName;
    public String pickup;
    public String destination;
    public int pricingType;
    public String currencyISO;
    public String currencySymbol;
    public double distanceTour;
    public Timestamp pickupTime;
    public Timestamp completedTime;
    public String timeZoneDestination;
    public String receiptComment;
    public String token;
    public String currencyISOCharged;
    public String currencySymbolCharged;
    public double totalCharged;
    public boolean avis3rd;
    public double deductions;
    public int status;
    public String tipAfterPayment;
    public boolean intercity = false;
    public boolean delivery = false;
    public String tripId;
    public String intercityInfo;
    public double originalFare;
    public double changedAmount;
    public double remainingAmount;
    public String canceller;
    public String typeBooking;
    public double grossEarning;
    public String gateway;
    public String walletName;
    public String iconUrl;
    public List<FleetService> fleetServices;
    public double fleetServiceFee;
    public boolean serviceActive;
    public double netEarning;
    public boolean payoutToProviderFleet;
    public double supplierPayout;
    public boolean payoutToHomeFleet;
    public double buyerPayout;
    public double penaltySupplier;
    public String supplierFailedId;
    public String buyerFailedId;
    public String fleetName;
    public double hydraOutstandingAmount;
    public double fleetMarkup;
    public String hydraPaymentMethod;
    public boolean isFarmOut;
    public double sellPriceMarkup;
    public boolean paidToDriver;
    public boolean paidToCompany;
    public boolean payFromCC;
    public boolean writeOffDebt;
    public double customerDebt;
    public String stripeMethod;

    //// SETTING DATA
    public boolean editBasicFare;
    public boolean editOtherFees;
    public boolean editTax;
    public boolean editTip;
    public boolean techFeeActive;
    public String techFeeType;
    public double techFeeValue;
    public boolean tipActive;
    public boolean taxActive;
    public double taxValue;
    public String couponType;
    public double couponValue;
    public boolean meetDriverActive;
    public boolean heavyTrafficActive;
    public boolean airportActive;
    public String commissionType;
    public double commissionValue;
    public boolean otherFeeActive;
    public boolean rushHourActive;
    public boolean tollFeeActive;
    public boolean parkingFeeActive;
    public boolean gasFeeActive;
    public boolean addNote;
    public int bookingFeeActive;
    public boolean travellerSignature;
    public boolean trackingLog;
    public boolean rating;
    public String vehicleType;
    public double addOnPrice;
    public double creditTransactionFeePercent;
    public double creditTransactionFeeAmount;

    //// ESTIMATE DATA
    public boolean actualFare;
    public double bookingFee = 0.0;
    public double taxFee;
    public double tipFee;
    public double minimum;

    //// UPDATE TICKET
    public String driverId;
    public String customerId;
    public String mDispatcherId;
    public String corporateId;
    public double mDispatcherCommission;
    public double totalFare;
    public double subTotalCharged;
    public String psgFleetId;
    public double exchangeRate;
    public double totalExchange;
    public double subTotalExchange;
    public boolean rideSharing;
    public String bookingFeeType;
    public double driverDeduction;
    public boolean isMinimumTotal;
    public boolean splitPayment;
    public double paidByWallet;
    public double paidByOtherMethod;
    public double cashReceived;
    public double transferredChangeByWallet;
    public double returnedChangeByCash;
    public int primaryPartialMethod;
    public String otherMethod;
    public String returnChange;
    public boolean tollFeeLimitDriverInputActive;
    public boolean parkingFeeLimitDriverInputActive;
    public boolean gasFeeLimitDriverInputActive;
    public double tollFeeDriverCanInput;
    public double parkingFeeDriverCanInput;
    public double gasFeeDriverCanInput;
    public double qupPreferredAmount;
    public double qupBuyPrice;
    public double qupSellPrice;
    public boolean chargeCancelPolicy;
    public String gatewayId;
    public boolean additionalAuth;

    //// RETURN QRCODE SETTING
    public String qrImage;
    public String qrName;

    /// RETRURN AVATAR
    public String passengerAvatar;

    public String paidStatus;
    public double paidAmount;
    public int extraWaitTime;
    public double extraWaitFee;
}