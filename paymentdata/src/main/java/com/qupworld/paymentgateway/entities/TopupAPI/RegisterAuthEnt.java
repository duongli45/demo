package com.qupworld.paymentgateway.entities.TopupAPI;

import java.util.UUID;

/**
 * Created by thuanho on 17/01/2022.
 */
public class RegisterAuthEnt {

    public String username = "";
    public String password = "";
    public String fleetId = "";

    // for log
    public String requestId = "";

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }
}
