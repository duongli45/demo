package com.qupworld.paymentgateway.entities;

import java.sql.Timestamp;

/**
 * Created by qup on 12/12/16.
 */
public class PromoCodeUse {

    public Long id;
    public Integer available;
    public String campaignId;
    public String campaignName;
    public Timestamp createdDate;
    public String customerId;
    public String customerName;
    public String fleetId;
    public String promoCode;
    public String promoCodeId;
    public Long ticketId;
    public Integer timesUsed;
    public Timestamp usedDate;
    public Double usedValue;
    public Double totalReceipt;
    public String currencyISO;
}
