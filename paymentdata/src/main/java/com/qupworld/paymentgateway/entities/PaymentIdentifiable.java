package com.qupworld.paymentgateway.entities;

/**
 * Created by myasus on 1/13/20.
 */
public interface PaymentIdentifiable {
    String getRequestId();
}
