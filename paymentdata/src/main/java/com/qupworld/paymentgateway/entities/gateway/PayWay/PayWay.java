package com.qupworld.paymentgateway.entities.gateway.PayWay;

/**
 * Created by thuanho on 14/06/2023.
 */
public class PayWay {

    public String tran_id;
    public String apv;
    public int status;
    public PayWayReturnParam return_params;
}
