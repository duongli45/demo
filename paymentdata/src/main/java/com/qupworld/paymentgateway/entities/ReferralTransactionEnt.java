package com.qupworld.paymentgateway.entities;

import java.util.UUID;

/**
 * Created by thuanho on 11/08/2020.
 */
public class ReferralTransactionEnt {

    public String fleetId;
    public String driverId;
    public String fromDate;
    public String toDate;
    public String currencyISO;

    // for log
    public String requestId = "";

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

}
