package com.qupworld.paymentgateway.entities;

import javax.annotation.Generated;
import java.sql.Timestamp;

/**
 * Created by qup on 08/03/2019.
 */
@Generated("org.jsonschema2pojo")
public class WalletTransfer {

    public Long id;
    public String fleetId;
    public String referenceId;
    public String transactionId;
    public String driverId;
    public String driverName;
    public String driverPhone;
    public String driverUserName;
    public String companyId;
    public String companyName;
    public String operatorId;
    public String operatorName;
    public String description;
    public String transferType; // withdraw
    public String receiverAccount; // last 4 number of bank account
    public String reason; // in case reject a request
    public Double amount;
    public Double currentBalance;
    public Double newBalance;
    public String currencyISO;
    public String currencySymbol;
    public String status; //pending, approved, rejected
    public Timestamp createdDate;
    public Timestamp completedDate;
    public String destination; //'creditWallet', 'bankAccount', 'WingBank'

}
