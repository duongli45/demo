package com.qupworld.paymentgateway.entities;

import java.util.List;
import java.util.UUID;

/**
 * Created by qup on 9/22/16.
 */
public class HydraPayoutEnt {

    public String fleetId;
    public String listFleetId;
    public boolean isPayAll;
    public String operatorId;
    public String payoutDate;
    public String payoutOption;
    public double minPayoutAmount;
    public String email;
    public String transactionType;
    public String settlement;
    public String networkType;
    public int from;
    public int size;

    // for log
    public String requestId = "";

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    /**
     * Valid a Credit Card Data
     */
    public int validateData() {
        if (this.isEmptyString(this.payoutDate) || this.isEmptyString(this.email)) {
            return 406;
        }
        return 200;
    }
    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }
}

