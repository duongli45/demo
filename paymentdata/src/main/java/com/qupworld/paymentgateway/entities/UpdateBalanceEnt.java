package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.UUID;

/**
 * Created by qup on 15/08/2019.
 */
public class UpdateBalanceEnt {

    public String fleetId;
    public Double amount;
    public String currencyISO;

    // for log
    public String requestId = "";

    public int validateData() throws JsonProcessingException {
        if (this.isEmptyString(this.fleetId) || this.isEmptyString(this.currencyISO) ||
                this.amount == null || this.amount <= 0) {
            return 406;
        }
        return 200;
    }

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }
    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }
}
