package com.qupworld.paymentgateway.entities;

import java.util.UUID;

/**
 * Created by qup on 9/22/16.
 */
public class PayToDriverEnt {

    public String fleetId;
    public String driverId;
    public double total;
    public String token;
    public String currencyISO;
    public String operatorId;
    public String referenceId;

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    /**
     * Valid a Credit Card Data
     */
    public int validateData() {
        if (this.isEmptyString(this.fleetId) || this.isEmptyString(this.driverId) || this.isEmptyString(this.token) || total <= 0) {
            return 406;
        }
        return 200;
    }
    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }
}
