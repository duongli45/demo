package com.qupworld.paymentgateway.entities;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */

public class PaymentNotificationData {

    // Telebirr
    public String outTradeNo;
    public String tradeNo;

    // PowerTranz
    public String address;
    public String city;
    public String state;
    public String postal;
    public String country;
    public String email;
}
