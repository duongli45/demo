
package com.qupworld.paymentgateway.entities;

import java.sql.Timestamp;

public class TransactionDetails {

    public long id;
    public String fleetId;
    public String bookId;
    public String transactionId;
    public Timestamp createdTime;
    public double amount;
    public String cardType;
    public String cardMasked;
    public String paymentMethod;
    public String currencyISO;
    public String currencySymbol;
    public String chargeNote;
    public String status;
    public String operatorId;
    public String stripeMethod;
    public String paymentLink;

}
