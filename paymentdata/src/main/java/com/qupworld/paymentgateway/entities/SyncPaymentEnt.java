package com.qupworld.paymentgateway.entities;

import java.util.List;
import java.util.UUID;

/**
 * Created by thuanho on 07/04/2022.
 */
public class SyncPaymentEnt {

    public List<String> bookIds;
    // for log
    public String requestId = "";

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }
}
