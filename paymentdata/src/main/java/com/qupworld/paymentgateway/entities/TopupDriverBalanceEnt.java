
package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;
import java.util.UUID;

public class TopupDriverBalanceEnt {

    public String userId;
    public String fleetId;
    public String from; // values: cc, app
    public String type; // values: topUp, editBalance, bookingDeduction
    public Double amount;
    public Double currentAmount;
    public String currencyISO;
    public String bookId = "";
    public String operatorId;
    public String reason;

    // used for top up with token - passed from app
    public String token = ""; // received token from Stripe if pay input with Stripe API
    public String localToken = "";
    // used for top up with new card - passed from app
    public String cardNumber = "";
    public String expiredDate = "";
    public String cvv = "";
    public String cardHolder = "";
    public String postalCode = "";

    // add info for AVS verification - passed from app
    public String street;
    public String city;
    public String state;
    public String country;

    public String data3ds = "";

    // for the gateway which required customer name and email
    public String name = "";
    public String email = "";

    // for log
    public String requestId = "";

    public List<Double> geoLocation;
    public String zoneId;
    public Boolean sca;


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    /**
     * Valid a Credit Card Data
     */
    public int validateData() throws JsonProcessingException {
        if (this.isEmptyString(this.userId) || this.isEmptyString(this.fleetId)) {
            return 406;
        }
        return 200;
    }
    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().serializeNulls().create();
        String strCredit = gson.toJson(this);
        if (this.cardNumber != null && this.cardNumber.length() > 4)
            strCredit = strCredit.replace(this.cardNumber,"XXXXXX"+this.cardNumber.substring(this.cardNumber.length()-4));
        return strCredit;
    }
}
