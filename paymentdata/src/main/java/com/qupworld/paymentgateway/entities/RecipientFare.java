package com.qupworld.paymentgateway.entities;

import org.json.simple.JSONArray;
/**
 * Created by myasus on 6/11/20.
 */
public class RecipientFare {
    public double deliveryFee;
    public double itemsFee;
    public double itemValue;
    public double techFee;
    public double tax;
    public double subTotal;
    public double promoAmount;
    public double total;
    public double totalOrder;
    public JSONArray menuData;
    public int order;
    public double distance;
    public double duration;
}
