package com.qupworld.paymentgateway.entities;

import java.util.UUID;

/**
 * Created by qup on 3/21/17.
 */
public class SettlementNotificationEnt {

    // MOLPay
    public String skey;
    public String payeeID;
    public String operator;
    public String VrfKey;
    public String StatCode;
    public String reference_id;
    public String mass_id;

    // for log
    public String requestId = "";

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }
}
