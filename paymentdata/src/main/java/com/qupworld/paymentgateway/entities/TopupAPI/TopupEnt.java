package com.qupworld.paymentgateway.entities.TopupAPI;

import java.util.UUID;

/**
 * Created by thuanho on 13/01/2022.
 */
public class TopupEnt {

    public String secureToken;
    public double amount;
    public String currencyISO;

    public String setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        return requestId;
    }
}
