package com.qupworld.paymentgateway.entities;

import java.util.List;

/**
 * Created by myasus on 2/12/20.
 */
public class ExtraLocation {
    public List<Double> geo;
    public String zipCode;
    public double distance;
    public double duration;
}
