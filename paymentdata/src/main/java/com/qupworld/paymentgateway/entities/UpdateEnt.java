package com.qupworld.paymentgateway.entities;

import java.util.UUID;

/**
 * Created by hoangnguyen on 7/28/17.
 */
public class UpdateEnt implements PaymentIdentifiable{
    public String fleetId;
    public String from;

    // used for update completed booking from Cue
    public String bookId;
    public Double oldAmount;
    public Double newAmount;
    public String userName;
    public String updateFrom;
    public String reasonUpdate;

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    @Override
    public String getRequestId() {
        return requestId;
    }
}
