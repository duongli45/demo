package com.qupworld.paymentgateway.entities;

import java.util.UUID;

public class AddCardEnt {

    public String fleetId;
    public String fleetToken;
    public String userType;
    public String userId;

    // for log
    public String requestId = "";

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }
}
