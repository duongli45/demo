package com.qupworld.paymentgateway.entities;

import java.util.UUID;

/**
 * Created by thuanho on 11/01/2022.
 */
public class TopupAPIEnt {

    public String fleetId;
    public String userType;
    public String phone;

    public String transactionId;
    public String token;
    public double amount;
    public String currencyISO;

    // for log
    public String requestId = "";

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

}
