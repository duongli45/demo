package com.qupworld.paymentgateway.entities;

import java.util.List;

/**
 * Created by hoangnguyen on 9/26/18.
 */
public class RateDetails {
    public String vehicleTypeId;
    public String packageRateId;
    public String pickupTime; // ("Now", "Reservation" or yyyy-MM-dd HH:mm)
    public Integer typeRate;
    public int bookType;
    public double distance;
    public double duration;
    public List<String> services;
    public List<ExtraLocation> extraDestination;
    public int seat;
    public int luggage;
    public String intercityRouteId;
    public int routeNumber;

}
