
package com.qupworld.paymentgateway.entities.FawryResponseEnt;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.qupworld.paymentgateway.entities.FawryFormEnt.PmtId;

public class PmtStatusRec {

    @SerializedName("status")
    @Expose
    private Status status;
    @SerializedName("voucherInfo")
    @Expose
    private VoucherInfo voucherInfo;
    @SerializedName("pmtIds")
    @Expose
    private List<PmtId> pmtIds = null;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public VoucherInfo getVoucherInfo() {
        return voucherInfo;
    }

    public void setVoucherInfo(VoucherInfo voucherInfo) {
        this.voucherInfo = voucherInfo;
    }

    public List<PmtId> getPmtIds() {
        return pmtIds;
    }

    public void setPmtIds(List<PmtId> pmtIds) {
        this.pmtIds = pmtIds;
    }


}
