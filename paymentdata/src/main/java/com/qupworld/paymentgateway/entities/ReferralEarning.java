
package com.qupworld.paymentgateway.entities;

public class ReferralEarning {

    public String fleetId;
    public String bookId;
    public String driverId;
    public String driverName;
    public String driverUserName;
    public String phone;
    public String companyId;
    public String companyName;
    public Double referralEarning = 0.0;
    public Double referralMaxAmount = 0.0;
    public String referralType;
    public Boolean isReferralMaxAmount;
    public Double referralPercent;
    public String referralCode;
    public String completedTime;
    public String currencyISO;
    public String transactionStatus;
    public Double newBalance;
    public String driverNumber;
    public String driverType;
    public String customerName;
    public String customerPhone;
    public String reason;

}
