
package com.qupworld.paymentgateway.entities;


import java.util.UUID;

public class ApplePayAsyncEnt {

    // for PayEase
    public String v_oid;
    public String v_pmode;
    public String v_pstatus;
    public String v_pstring;
    public String v_count;
    public String v_amount;
    public String v_moneytype;
    public String v_md5money;
    public String v_mac;
    public String v_sign;

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }
}
