package com.qupworld.paymentgateway.entities.TopupAPI;

import com.google.gson.Gson;

/**
 * Created by thuanho on 20/01/2022.
 */
public class AuthResponseEnt {

    public int returnCode;
    public String token;

    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
