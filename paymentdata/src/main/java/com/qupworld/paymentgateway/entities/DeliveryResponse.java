package com.qupworld.paymentgateway.entities;

import java.util.List;

/**
 * Created by myasus on 6/11/20.
 */
public class DeliveryResponse {
    public String currencyISO;
    public String vehicleType;
    public String promoCode;
    public boolean shortTrip;
    public double etaFare;
    public double subTotal;
    public double techFee;
    public double tax;
    public double promoAmount;
    public double deliveryFee;
    public double itemsFee;
    public double itemValue;
    public double totalOrder;
    public double totalWithoutPromo;
    public double unroundedTotalAmt;
    public double taxSetting;
    public double creditTransactionFee;
    public double creditTransactionFeePercent;
    public double creditTransactionFeeAmount;
    public List<RecipientFare> recipients;
    public String driverEarningType = "default";
    public double editedDriverEarning;
    public double originalDriverEarning;
    public double merchantCommission;
    public String rateType;
    public String rateId;
}