package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.UUID;

/**
 * Created by thuanho on 09/03/2019.
 */
public class UserWalletEnt {

    public String fleetId;
    public String driverId;
    public String userId;
    public String currencyISO;
    public String zoneId;

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    public int validateDriverWallet() throws JsonProcessingException {
        if (this.isEmptyString(this.fleetId) || this.isEmptyString(this.driverId)
                || ( this.isEmptyString(this.currencyISO) && this.isEmptyString(this.zoneId) )) {
            return 406;
        }
        return 200;
    }

    public int validateDPaxWallet() throws JsonProcessingException {
        if (this.isEmptyString(this.fleetId) || this.isEmptyString(this.userId) || this.isEmptyString(this.currencyISO)) {
            return 406;
        }
        return 200;
    }
    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }
}
