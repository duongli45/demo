package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.UUID;

/**
 * Created by qup on 08/04/2020.
 */
public class AirPayNotifyEnt {

    public String app_key;
    public String order_id;
    public String order_status;
    public String payment_id;
    public String requestId;

    /**
     * Valid a Input Data
     */
    public int validateData() throws JsonProcessingException {
        if (this.isEmptyString(this.order_id)) {
            return 40001;
        }
        if (this.isEmptyString(this.app_key)) {
            return 40002;
        }
        if (this.isEmptyString(this.order_status)) {
            return 40003;
        }
        return 200;
    }

    public void setRequestId(String requestId) {
        if(requestId == null || requestId.isEmpty()) {
            requestId = UUID.randomUUID().toString();
        }

        this.requestId = requestId;
    }
    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }
}
