package com.qupworld.paymentgateway.entities.PayWay;

/**
 * Created by thuanho on 29/09/2023.
 */
public class PayWayReturnParams {

    public String tran_id;
    public String ctid;
    public String payment_status;
    public String return_param;
    public PayWayCredit card_status;

}
