
package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.UUID;

public class TopupByWalletEnt {

    public String userId;
    public String fleetId;
    public double amount;
    public double currentAmount;
    public String currencyISO;
    public String reason;
    public String osType;
    public String appVersion;
    public String topupTo; // value = paxWallet, wallet (for driver credit wallet)
    public String gateway; // name of wallet
    public String parameter; // additional data added to the request to wallet
    public boolean walletAppInstalled;


    public String data3ds = "";

    // for the gateway which required customer name and email, phone
    public String name = "";
    public String email = "";
    public String phone = "";

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    /**
     * Valid a Credit Card Data
     */
    public int validateData() throws JsonProcessingException {
        if (this.isEmptyString(this.userId) || this.isEmptyString(this.fleetId) || this.amount <= 0.0) {
            return 406;
        }
        return 200;
    }
    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().serializeNulls().create();
        String strCredit = gson.toJson(this);
        return strCredit;
    }
}
