package com.qupworld.paymentgateway.entities.gateway.PowerTranz;

/**
 * Created by thuanho on 10/05/2023.
 */
public class PowerTranzThreeDSecure {

    public String Eci;
    public String Cavv;
    public String Xid;
    public String AuthenticationStatus;
    public String ProtocolVersion;
    public String FingerprintIndicator;
    public String DsTransId;
    public String ResponseCode;
    public String CardholderInfo;
}
