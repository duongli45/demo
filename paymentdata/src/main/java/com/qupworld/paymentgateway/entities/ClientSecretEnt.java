package com.qupworld.paymentgateway.entities;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;
import java.util.UUID;

/**
 * Created by qup on 8/26/19.
 */
public class ClientSecretEnt {
    public String fleetId;
    public String userId;
    public String userType;
    public String type;
    public String zoneId;
    public List<Double> geo;
    public String requestId = "";

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().serializeNulls().create();
        String strCredit = gson.toJson(this);
        return strCredit;
    }
}
