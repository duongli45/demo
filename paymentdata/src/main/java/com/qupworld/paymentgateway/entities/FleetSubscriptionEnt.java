package com.qupworld.paymentgateway.entities;

import javax.xml.crypto.Data;
import java.util.Date;
import java.util.UUID;

/**
 * Created by qup on 26/11/2018.
 */
public class FleetSubscriptionEnt {

    public Integer fleetSubscriptionId;
    public String fleetId = "";
    public Integer chargeDriver = 0;
    public Double costPerDriver = 0.0;
    public Double driverFee = 0.0;

    // save vehicle fee
    public Integer chargeVehicle = 0;
    public Double costPerVehicle = 0.0;
    public Double vehicleFee = 0.0;

    // save transaction fee amount
    public Integer chargeTransaction = 0;
    public Double costPerTransaction = 0.0;
    public Double transactionFeeAmount = 0.0;
    // save transaction fee percentage
    public Double transactionFeePercentExchange = 0.0;
    public Double totalTransactionFee = 0.0;

    // save other fee
    public Double otherFee = 0.0;

    // save adjust desc
    public String adjustDriverDesc = "";
    public String adjustVehicleDesc = "";
    public String adjustTransactionAmountDesc = "";
    public String adjustTransactionPercentDesc = "";
    public String adjustOtherDesc = "";

    // save charge info
    public Double subTotalQupFee = 0.0;
    public Double tax = 0.0;
    public Double totalQupFee = 0.0;

    // invoice status
    public Boolean invoiceSent;

    // mark as charge
    public String transactionId;

    // insert completed date for Reporting
    public Date createdDate;

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }
}
