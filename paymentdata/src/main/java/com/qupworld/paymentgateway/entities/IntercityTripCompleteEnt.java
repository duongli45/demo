package com.qupworld.paymentgateway.entities;

import java.util.UUID;

/**
 * Created by qup on 20/04/2020.
 */
public class IntercityTripCompleteEnt {

    public String tripId;
    public String fleetId;

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }
}
