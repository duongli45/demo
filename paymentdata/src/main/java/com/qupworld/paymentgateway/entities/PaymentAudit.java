
package com.qupworld.paymentgateway.entities;

import java.util.UUID;

public class PaymentAudit {

    public String from;
    public String to;

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

}
