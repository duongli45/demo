package com.qupworld.paymentgateway.entities;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.List;

/**
 * Created by hoang.nguyen on 11/28/16.
 */
public class FareReturn {
    public double basicFare;
    public double etaFare;
    public String currencyISO;
    public double airportFee;
    public double meetDriverFee;
    public double rushHourFee;
    public double techFee;
    public double bookingFee;
    public double tollFee;
    public double parkingFee;
    public double gasFee;
    public double tax;
    public double tip;
    public double minFare;
    public boolean normalFare;
    public String route;
    public String routeId;
    public boolean reverseRoute;
    public String vehicleType; // return to WB
    public String vehicleTypeLocal;
    public String timeZoneId;
    public boolean min;
    public boolean isMinimumTotal;
    public double otherFees;
    public double promoAmount;
    public boolean bookingFeeActive;
    public double serviceFee;
    public double serviceTax;
    public JSONArray packages;
    public JSONObject estimateFareBuy;
    public int typeRate;
    public double extraDistanceFee;
    public double extraDurationFee;
    public double taxValue;
    public double tipValue;
    public double totalWithoutPromo;
    public double dynamicSurcharge;
    public double surchargeParameter;
    public double dynamicFare;
    public String dynamicType;
    public boolean shortTrip;
    public double unroundedTotalAmt;
    public Boolean supportExtraLocation;
    public double subTotal;
    public double taxSetting;
    public double addOnPrice;
    public double markupDifference;
    public double creditTransactionFee;
    public double creditTransactionFeeAmount;
    public double creditTransactionFeePercent;
    public String promoCode;
    public JSONObject priceAdjustable;
    public Boolean validPriceAdjustable;
    public Boolean actualFare;
    public double markupEstimate;
    public double markupEstimateAmount;
    public double qupPreferredAmount;
    public double qupSellPrice;
    public double totalWoPromoSellPrice;
    public String localFleetId;
    public double fleetMarkup;
    public double sellPriceMarkup;
    public double originPromoAmount;
    public String driverEarningType = "default";
    public double editedDriverEarning;
    public double originalDriverEarning;
    public double fleetCommissionFromFleetServiceFee;
    public double driverCommissionFromFleetServiceFee;
    public String rateType; // values: regular, flat, hourly, intercity, streetSharing, delivery
    public String rateId; // flat => routeId, hourly => packageRateId, intercity => intercityRouteId

}
