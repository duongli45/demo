package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.qupworld.paymentgateway.models.mongo.collections.booking.Waypoint;

import java.util.List;
import java.util.UUID;

/**
 * Created by myasus on 12/13/21.
 */
public class ETASharingEnt implements PaymentIdentifiable {
    public String bookId;
    public String vehicleType;
    public String fleetId;
    public List<Double> currentLocation;
    public List<Integer> dropOffOrder;
    public Waypoint currentWaypoint;
    public int numOfPax;
    public String requestId = "";

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    public int validateData() throws JsonProcessingException {
        if((this.isEmptyString(this.bookId) && this.isEmptyString(this.vehicleType) && this.isEmptyString(this.fleetId))
                || (this.currentWaypoint == null && this.isEmptyString(this.vehicleType) && this.isEmptyString(this.fleetId))){
            return 406;
        }
        return 200;
    }

    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }

    @Override
    public String getRequestId() {
        return requestId;
    }
}
