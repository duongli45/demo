package com.qupworld.paymentgateway.entities;

import java.util.UUID;

/**
 * Created by myasus on 12/6/21.
 */
public class ETAThirdPartyEnt implements PaymentIdentifiable{

    public String fleetId;
    public double amount;
    public String currencyISO;
    public String vehicleTypeId;
    public String zoneId;
    public String requestId;

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    @Override
    public String getRequestId() {
        return requestId;
    }
}
