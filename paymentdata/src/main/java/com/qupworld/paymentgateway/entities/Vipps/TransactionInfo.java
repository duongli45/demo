package com.qupworld.paymentgateway.entities.Vipps;

/**
 * Created by thuanho on 13/04/2021.
 */
public class TransactionInfo {

    public double amount;
    public String status;
    public String timeStamp;
    public String transactionId;
}
