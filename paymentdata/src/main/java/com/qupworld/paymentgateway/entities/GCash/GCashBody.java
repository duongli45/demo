package com.qupworld.paymentgateway.entities.GCash;

/**
 * Created by qup on 06/08/2019.
 */
public class GCashBody {

    public String acquirementId = "";
    public GCashOrderAmount orderAmount;
    public String merchantId = "";
    public String merchantTransId = "";
    public String finishedTime = "";
    public String createdTime = "";
    public String acquirementStatus = "";
}
