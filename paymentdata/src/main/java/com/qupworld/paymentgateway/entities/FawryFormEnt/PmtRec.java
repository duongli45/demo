
package com.qupworld.paymentgateway.entities.FawryFormEnt;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "pmtInfo"
})
public class PmtRec {

    @JsonProperty("pmtInfo")
    private PmtInfo pmtInfo;

    @JsonProperty("pmtInfo")
    public PmtInfo getPmtInfo() {
        return pmtInfo;
    }

    @JsonProperty("pmtInfo")
    public void setPmtInfo(PmtInfo pmtInfo) {
        this.pmtInfo = pmtInfo;
    }

}
