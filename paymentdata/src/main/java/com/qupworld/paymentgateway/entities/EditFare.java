package com.qupworld.paymentgateway.entities;

/**
 * Created by myasus on 11/15/21.
 */
public class EditFare {
    public String bookId;
    public double basicFare;
    public double airportFee;
    public double meetDriverFee;
    public double rushHourFee;
    public double techFee;
    public double bookingFee;
    public double tip;
    public double otherFees;
    public double promoAmount;
    public double tollFee;
    public double parkingFee;
    public double gasFee;
    public double serviceFee;
    public double deliveryFee;
    public double creditTransactionFee;
}
