
package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.qupworld.paymentgateway.entities.ApplePay.AppleToken;
import com.pg.util.ValidCreditCard;
import org.json.simple.JSONObject;

import java.sql.Timestamp;
import java.util.UUID;

public class PaymentEnt {

    // basic info - passed from app/CC
    public String fleetId;
    public String bookId;
    public String paymentType = "";
    public double total = 0.0;
    public double subTotal = 0.0;
    public double techFee = 0.0;
    public double fare = 0.0;
    public double calBasicFare = 0.0;
    public double tip = 0.0;
    public double tax = 0.0;
    public String promoCode = "";
    public double promoAmount = 0.0;
    public double meetDriverFee = 0.0;
    public double heavyTraffic = 0.0;
    public double airportSurcharge = 0.0;
    public double partnerCommission = 0.0;
    public double otherFees = 0.0;
    public double rushHour = 0.0;
    public int isMinimum = 0;
    public double tollFee = 0.0;
    public double parkingFee = 0.0;
    public double gasFee = 0.0;
    public double serviceFee = 0.0;
    public double dynamicSurcharge = 0.0;
    public double dynamicFare = 0.0;
    public double creditTransactionFee = 0.0;
    public String otherFeesDetails = "";
    public String gateway;
    public boolean isPending = false;
    public boolean isPartialPayment = false;
    public String driverAppVersion = "";
    public String data3ds; // used to check pay input 3DS
    public String token = "";// used to check payment with card info through SDK
    public boolean isCharge = false;// support charge on CC
    public String currencyISO = "";
    public JSONObject additionalFare;
    public double receivedAmount = 0.0; // used for cash excess flow
    public double transferredAmount = 0.0; // used for transfer to pax wallet + complete by pax wallet
    public String excessReturnTarget = ""; // value = cash/wallet
    public double itemValue = 0.0;
    public boolean walletAppInstalled = false;
    public String from = "";
    public AppleToken appleToken;
    public boolean completedWithoutService = false;
    public String returnChange;
    public double totalAffiliate;
    public boolean additionalAuth;
    public boolean payFromCC;
    public boolean writeOffDebt;

    // used for pay input card - passed from app
    public String cardNumber = "";
    public String expiredDate = "";
    public String cvv = "";
    public String cardHolder = "";
    public String postalCode = "";

    // for Avis/Yeepay
    public String creditType = "";
    public String creditCode = "";

    // used for pay swipe card - passed from app
    public String cardTracks = "";

    // add info for AVS verification - passed from app
    public String street;
    public String city;
    public String state;
    public String country;

    // used to update ticket - passed from payment process
    public String transactionStatus = "";
    public String paidBy = "";
    public String approvalCode = "";
    public String gatewayId = "";
    public int paidToDriver = 0;
    public String cardType = "";
    public String canceller = "";
    public String cancellerName = "";
    public boolean isLocalZone = true;
    public Timestamp paymentTime;
    public Timestamp completedTime;
    public String currencyISOCharged = "";
    public String currencySymbolCharged = "";
    public double subTotalCharged = 0.0;
    public double totalCharged = 0.0;
    public double exchangeRate = 0.0;
    public double subTotalExchange = 0.0;
    public double totalExchange = 0.0;
    public boolean isToDriver = false;
    public double grossEarning = 0.0;
    public boolean splitPayment;
    public double paidByWallet;
    public double paidByOtherMethod;
    public double transferredChangeByWallet;
    public double returnedChangeByCash;
    public int primaryPartialMethod;
    public String otherMethod;
    public double driverTax = 0.0;
    public double qupPreferredAmount;
    public double qupBuyPrice;
    public double hydraOutstandingAmount;
    public String hydraPaymentStatus = "full";
    public double fleetMarkup;
    public String hydraPaymentMethod;
    public boolean isFarmOut;
    public double sellPriceMarkup;
    public double customerDebt;
    public String stripeMethod;

    public boolean isResetCache = false;
    public OutstandingEnt outstandingEnt;
    public String customerId = "";
    public String walletName = "";
    public String walletNumber = "";
    public String walletData = "";
    // used in case pay by Hardware - add ticket and payment
    public double distanceTour = 0.0;
    public double originalFare = 0.0;

    // used to mark booking completed from AVIS 3rd
    public int avis3rd = 0;

    public String dynamicPayType = "";

    // used to detect whether captured remaining amount for Stripe SCA
    public boolean remainingCaptured = false;
    public String remainingId = "";
    public String finishedBookingStatus = "completed";

    // for import booking
    public int min = 0;
    public int max = 0;

    // for log
    public String requestId = "";

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    /**
     * Valid a Credit Card Data
     */
    public int validateData() throws JsonProcessingException {
        if (this.isEmptyString(this.bookId) || this.isEmptyString(this.paymentType)) {
            return 406;
        }
        return 200;
    }

    public int validateCredit() throws JsonProcessingException {
        String data3ds = this.data3ds != null ? this.data3ds : "";
        if (!data3ds.equals("")) {
            return 200;
        }
        String token = this.token != null ? this.token : "";
        if (!token.equals("")) {
            return 200;
        }

        // all credit information is empty - request initial credit form from the gateway URL
        if(!this.isEmptyString(this.bookId) &&
                this.isEmptyString(this.cardNumber) &&
                this.isEmptyString(this.expiredDate) &&
                this.isEmptyString(this.cardHolder) &&
                this.isEmptyString(this.cvv)){
            return 200;
        }

        if(this.isEmptyString(this.bookId)||
                this.isEmptyString(this.cardNumber)||
                this.isEmptyString(this.expiredDate)||
                this.isEmptyString(this.cardHolder)||
                this.isEmptyString(this.cvv)){
            return 406;
        }
        int errorCode = 200;
        if (!ValidCreditCard.checkCardNumber(this.cardNumber)) errorCode = 421;
        if (!ValidCreditCard.checkExpiredDate(this.expiredDate)) errorCode = 422;
        if (!ValidCreditCard.checkCVV(this.cvv)) errorCode = 432;
        return errorCode;
    }
    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().serializeNulls().create();
        String strCredit = gson.toJson(this);
        if (this.cardNumber != null && this.cardNumber.length() > 4)
            strCredit = strCredit.replace(this.cardNumber,"XXXXXX"+this.cardNumber.substring(this.cardNumber.length()-4));
        return strCredit;
    }
}
