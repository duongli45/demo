
package com.qupworld.paymentgateway.entities.FawryResponseEnt;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.qupworld.paymentgateway.entities.FawryFormEnt.CustomProperty;

public class FawryResponseEnt {

    @JsonProperty("isRetry")
    private Boolean isRetry;
    @SerializedName("status")
    @Expose
    private Status status;
    @SerializedName("clientTerminalSeqId")
    @Expose
    private String clientTerminalSeqId;
    @SerializedName("serverDt")
    @Expose
    private String serverDt;
    @SerializedName("rqUID")
    @Expose
    private String rqUID;
    @SerializedName("asyncRqUID")
    @Expose
    private String asyncRqUID;
    @SerializedName("terminalId")
    @Expose
    private String terminalId;
    @SerializedName("signature")
    @Expose
    private String signature;

    @SerializedName("extraBillInfo")
    @Expose
    private String extraBillInfo;


    @SerializedName("msgCode")
    @Expose
    private String msgCode;
    @SerializedName("pmtStatusRec")

    @JsonProperty("isRetry")
    public Boolean getIsRetry() {
        return isRetry;
    }

    @JsonProperty("isRetry")
    public void setIsRetry(Boolean isRetry) {
        this.isRetry = isRetry;
    }

    @Expose
    private List<PmtStatusRec> pmtStatusRec = null;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getClientTerminalSeqId() {
        return clientTerminalSeqId;
    }

    public void setClientTerminalSeqId(String clientTerminalSeqId) {
        this.clientTerminalSeqId = clientTerminalSeqId;
    }

    public String getServerDt() {
        return serverDt;
    }

    public void setServerDt(String serverDt) {
        this.serverDt = serverDt;
    }


    public String getRqUID() {
        return rqUID;
    }

    public void setRqUID(String rqUID) {
        this.rqUID = rqUID;
    }

    public String getAsyncRqUID() {
        return asyncRqUID;
    }

    public void setAsyncRqUID(String asyncRqUID) {
        this.asyncRqUID = asyncRqUID;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getExtraBillInfo() {
        return extraBillInfo;
    }

    public void setExtraBillInfo(String extraBillInfo) {
        this.extraBillInfo = extraBillInfo;
    }



    public String getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(String msgCode) {
        this.msgCode = msgCode;
    }

    public List<PmtStatusRec> getPmtStatusRec() {
        return pmtStatusRec;
    }

    public void setPmtStatusRec(List<PmtStatusRec> pmtStatusRec) {
        this.pmtStatusRec = pmtStatusRec;
    }

}
