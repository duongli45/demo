package com.qupworld.paymentgateway.entities;

/**
 * Created by myasus on 6/22/20.
 */
public class Price {
    public String iso;
    public String symbol;
    public double value;
}
