
package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.UUID;

public class RedeemVoucherEnt {

    public String fleetId;
    public String userId;
    public String phone;
    public String redeemCode;

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    /**
     * Valid request data
     */
    public int validateData() throws JsonProcessingException {
        if (this.isEmptyString(this.fleetId)) {
            return 406;
        }
        if (this.isEmptyString(this.redeemCode)) {
            return 538;
        }
        if (this.isEmptyString(this.userId) && this.isEmptyString(this.phone)) {
            return 537;
        }
        return 200;
    }
    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }
}
