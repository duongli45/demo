package com.qupworld.paymentgateway.entities;

/**
 * Created by qup on 3/21/17.
 */
public class PayfortResponseEnt {

    public String amount;
    public String response_code;
    public String card_number;
    public String signature;
    public String merchant_identifier;
    public String access_code;
    public String expiry_date;
    public String payment_option;
    public String customer_ip;
    public String language;
    public String eci;
    public String fort_id;
    public String command;
    public String response_message;
    public String merchant_reference;
    public String authorization_code;
    public String customer_email;
    public String token_name;
    public String currency;
    public String remember_me;
    public String status;

    // Test FAC
    public String MerID;
    public String AcqID;
    public String OrderID;
    public String ResponseCode;
    public String ReasonCode;
    public String ReasonCodeDesc;
    public String TokenizedPAN;
    public String ReferenceNo;
    public String PaddedCardNo;
    public String AuthCode;
    public String CVV2Result;
    public String BillToPostCode;
    public String OriginalResponseCode;
    public String Signature;
    public String SignatureMethod;

}
