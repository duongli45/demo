package com.qupworld.paymentgateway.entities;


import java.util.UUID;

/**
 * Created by duyphan on 8/25/16.
 */
public class CreditDeleteEnt {
    public String fleetId;
    public String userId;
    public String localToken;
    public String crossToken;

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    public int validateData() {
        if (this.isEmptyString(this.fleetId) ||
                (this.isEmptyString(this.localToken) &&
                        this.isEmptyString(this.crossToken)))
        {
            return 406;
        }

        return 200;
    }

    /**
     * @return boolean
     */
    public boolean isEmptyString(String s) {
        return (s == null || s.trim().isEmpty());
    }
}
