package com.qupworld.paymentgateway.entities.ApplePay;

/**
 * Created by thuanho on 24/05/2022.
 */
public class TokenHeader {

    public String publicKeyHash;
    public String transactionId;
    public String wrappedKey;
}
