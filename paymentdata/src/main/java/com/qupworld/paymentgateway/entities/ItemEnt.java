package com.qupworld.paymentgateway.entities;

import java.util.List;

/**
 * Created by myasus on 6/22/20.
 */
public class ItemEnt {
    public String componentType;
    public String title;
    public List<Object> items;
    public Object value;
    public Price price;
}
