
package com.qupworld.paymentgateway.entities.FawryFormEnt;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "terminalId",
    "clientTerminalSeqId",
    "asyncRqUID",
    "receiver",
    "clientDt",
    "custLang",
    "msgCode",
    "rqUID",
    "signature",
    "customProperties",
    "sender",
    "isRetry",
    "pmtRec"
})
public class FawryEnt {
    public String fleetId;
    @JsonProperty("terminalId")
    private String terminalId;
    @JsonProperty("clientTerminalSeqId")
    private String clientTerminalSeqId;
    @JsonProperty("asyncRqUID")
    private String asyncRqUID;
    @JsonProperty("receiver")
    private String receiver;
    @JsonProperty("clientDt")
    private String clientDt;
    @JsonProperty("custLang")
    private String custLang;
    @JsonProperty("msgCode")
    private String msgCode;
    @JsonProperty("rqUID")
    private String rqUID;
    @JsonProperty("signature")
    private String signature;
    @JsonProperty("customProperties")
    private List<CustomProperty> customProperties = null;
    @JsonProperty("sender")
    private String sender;
    @JsonProperty("isRetry")
    private Boolean isRetry;
    @JsonProperty("pmtRec")
    private List<PmtRec> pmtRec = null;

    @JsonProperty("terminalId")
    public String getTerminalId() {
        return terminalId;
    }

    @JsonProperty("terminalId")
    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    @JsonProperty("clientTerminalSeqId")
    public String getClientTerminalSeqId() {
        return clientTerminalSeqId;
    }

    @JsonProperty("clientTerminalSeqId")
    public void setClientTerminalSeqId(String clientTerminalSeqId) {
        this.clientTerminalSeqId = clientTerminalSeqId;
    }

    @JsonProperty("asyncRqUID")
    public String getAsyncRqUID() {
        return asyncRqUID;
    }

    @JsonProperty("asyncRqUID")
    public void setAsyncRqUID(String asyncRqUID) {
        this.asyncRqUID = asyncRqUID;
    }

    @JsonProperty("receiver")
    public String getReceiver() {
        return receiver;
    }

    @JsonProperty("receiver")
    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    @JsonProperty("clientDt")
    public String getClientDt() {
        return clientDt;
    }

    @JsonProperty("clientDt")
    public void setClientDt(String clientDt) {
        this.clientDt = clientDt;
    }

    @JsonProperty("custLang")
    public String getCustLang() {
        return custLang;
    }

    @JsonProperty("custLang")
    public void setCustLang(String custLang) {
        this.custLang = custLang;
    }

    @JsonProperty("msgCode")
    public String getMsgCode() {
        return msgCode;
    }

    @JsonProperty("msgCode")
    public void setMsgCode(String msgCode) {
        this.msgCode = msgCode;
    }

    @JsonProperty("rqUID")
    public String getRqUID() {
        return rqUID;
    }

    @JsonProperty("rqUID")
    public void setRqUID(String rqUID) {
        this.rqUID = rqUID;
    }

    @JsonProperty("signature")
    public String getSignature() {
        return signature;
    }

    @JsonProperty("signature")
    public void setSignature(String signature) {
        this.signature = signature;
    }

    @JsonProperty("customProperties")
    public List<CustomProperty> getCustomProperties() {
        return customProperties;
    }

    @JsonProperty("customProperties")
    public void setCustomProperties(List<CustomProperty> customProperties) {
        this.customProperties = customProperties;
    }

    @JsonProperty("sender")
    public String getSender() {
        return sender;
    }

    @JsonProperty("sender")
    public void setSender(String sender) {
        this.sender = sender;
    }

    @JsonProperty("isRetry")
    public Boolean getIsRetry() {
        return isRetry;
    }

    @JsonProperty("isRetry")
    public void setIsRetry(Boolean isRetry) {
        this.isRetry = isRetry;
    }

    @JsonProperty("pmtRec")
    public List<PmtRec> getPmtRec() {
        return pmtRec;
    }

    @JsonProperty("pmtRec")
    public void setPmtRec(List<PmtRec> pmtRec) {
        this.pmtRec = pmtRec;
    }

}
