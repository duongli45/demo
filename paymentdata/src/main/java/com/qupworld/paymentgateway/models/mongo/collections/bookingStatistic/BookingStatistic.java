package com.qupworld.paymentgateway.models.mongo.collections.bookingStatistic;

/**
 * Created by qup on 06/09/2018.
 */
public class BookingStatistic {

    public String fleetId;
    public String time;
    public String currencyISO;
    public Integer finished;
    public Statistic paxApp;
    public Statistic webBooking;
    public Statistic cc;
    public Statistic carHailing;
    public Statistic kiosk;
    public Statistic API;
    public Statistic dmc;
    public Statistic corp;
    public Statistic mDispatcher;
    public Statistic dmcBooking;
    public Statistic corpBooking;
    public Double profit;
    public Double revenue;
    public Double fleetProfit;

}
