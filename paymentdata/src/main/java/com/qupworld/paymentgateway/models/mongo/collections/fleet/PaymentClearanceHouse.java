
package com.qupworld.paymentgateway.models.mongo.collections.fleet;

import javax.annotation.Generated;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class PaymentClearanceHouse {

    public String type;
    public String gateway;
    public String environment;
    public Boolean isActive;
    public String airpayEnvironment;
    public String airpayAppKey;
    public String airpayAppId;

    // QR Code
    public String qrImage;
    public String qrName;

    // Apple pay - PayEase
    public String merchantId;
    public String aliasName;
    public String aliasPassword;
    public String pfxPassword;
    public String certificateUrl;
    public String publicKeyUrl;
    public String privateKeyUrl;
    public String requestUrl;

    // Apple pay - Stripe
    public String publicKey;
    public String secretKey;

    /*// Wallet - TnGeWallet
    public String clientId;
    public String clientSecret;
    //public String merchantId; // same key with Apple pay - PayEase
    public String productCode;*/

    /*// Wallet - Boost
    public String boostMerchantId;
    public String boostApiKey;
    public String boostApiSecret;*/

    public String momoPartnerCode;
    public String momoSecretKey;
    public String momoPublicKey;
    public Boolean isMomoSanbox;

    public String nganLuongMerchantId;
    public String nganLuongMerchantAccount;
    public String nganLuongMerchantPassword;
    public Boolean isNganLuongSanbox;

}