
package com.qupworld.paymentgateway.models.mongo.collections.gateway;


import org.bson.types.ObjectId;

public class GatewayPayCorp {

    public ObjectId _id;
    public String fleetId;
    public Boolean isSystem;
    public String environment;
    public String clientId;
    public String authToken;
    public String hmacSecret;
    public Boolean isActive;

}
