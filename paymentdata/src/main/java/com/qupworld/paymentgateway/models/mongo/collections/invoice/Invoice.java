package com.qupworld.paymentgateway.models.mongo.collections.invoice;

import javax.annotation.Generated;
import java.util.Date;

@Generated("org.jsonschema2pojo")
public class Invoice {

    public String fleetId;
    public long invoiceId;
    public String invoiceToken;
    public String creatorId;
    public String updaterId;
    public Date dueTime;
    public int status;
    public int invoiceType;
    public int numOfItems;
    public String transactionId;
    public String customerId;
    public String customerName;
    public String billingName;
    public String billingPhone;
    public String billingEmail;
    public String billingAddress;
    public Date paidTime;
    public Date fromTime;
    public Date toTime;
    public double amount;
    public double paidAmount;
    public double tax;
    public double subTotal;
    public String memo;
    public String cardType;
    public String cardMasked;
    public String paymentMethod;
    public String chargeNote;
    public String currencyISO;
    public String currencySymbol;
    public String timezone;
    public String jobId;
    public Date latestUpdate;
}
