package com.qupworld.paymentgateway.models.mongo.codec;

import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

public class DoubleCodec implements Codec<Double> {

    @Override
    public Double decode(BsonReader reader, DecoderContext decoderContext) {
        // Implement your deserialization logic here
        // Read data from BSON and construct a Number object
        return reader.readDouble();
    }

    @Override
    public void encode(BsonWriter writer, Double value, EncoderContext encoderContext) {
        // Implement your serialization logic here
        // Write Number object to BSON
        writer.writeDouble(value.doubleValue()); // Example: writing a double
    }

    @Override
    public Class<Double> getEncoderClass() {
        return double.class;
    }
}
