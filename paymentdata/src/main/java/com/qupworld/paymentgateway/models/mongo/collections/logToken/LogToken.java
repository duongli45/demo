package com.qupworld.paymentgateway.models.mongo.collections.logToken;

import javax.annotation.Generated;

/**
 * Created by thuanho on 18/07/2023.
 */
@Generated("org.jsonschema2pojo")
public class LogToken {

    public String logId;
    public String fleetId;
    public String userId;
    public String logData;
}
