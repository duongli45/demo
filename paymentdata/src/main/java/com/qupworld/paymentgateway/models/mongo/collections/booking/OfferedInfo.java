
package com.qupworld.paymentgateway.models.mongo.collections.booking;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class OfferedInfo {

    public Double distanceValue;
    public String distance;
    public Double estimateValue;
    public String estimate;
    public Location location;
    public String address;

}
