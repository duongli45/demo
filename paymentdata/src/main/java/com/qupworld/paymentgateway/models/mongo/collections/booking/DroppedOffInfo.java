
package com.qupworld.paymentgateway.models.mongo.collections.booking;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class DroppedOffInfo {

    public Boolean googleDistance;
    public String incident;
    public Double distanceTour;
    public Integer receiptId;
    public Double total;
}
