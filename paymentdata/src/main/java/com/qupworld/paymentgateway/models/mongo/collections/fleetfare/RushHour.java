package com.qupworld.paymentgateway.models.mongo.collections.fleetfare;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import javax.annotation.Generated;
import java.util.List;

/**
 * Created by hoang.nguyen on 11/28/16.
 */
@Generated("org.jsonschema2pojo")
public class RushHour {
    public String rushHourId;
    public String name;
    public List<String> date;
    public String start;
    public String end;
    public int timeCal;
    public double surcharge;
    public String surchargeType; // amount | percent
    public List<SurchargeByCurrency> surchargeByCurrencies;
    public String repeat;   // single | yearly | weekly
    public String startDate;  // YYYY-MM-DD
    public boolean distanceDiversity;
    public List<SurchargeByCurrency> surchargeNow;
    public List<SurchargeByCurrency> surchargeReservation;
}
