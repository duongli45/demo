package com.qupworld.paymentgateway.models.mongo.collections.failedCard;

import javax.annotation.Generated;
import java.util.Date;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@Generated("org.jsonschema2pojo")
public class FailedCard {

    public String fleetId;
    public String cardId;
    public Date expireAt;
    public Date createdDate;
}
