
package com.qupworld.paymentgateway.models.mongo.collections.pricingplan;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class FleetPayment {

    public String currencyISO;
    public Double monthlyFee;
    public int applyMonthlyFee;// 0 - Always calculate monthly + subscription fees | 1 - Only apply monthly fee if sum of subscription fees are less than monthly fee
    public Double tax;
    public FeeDetail driver;
    public FeeDetail vehicle;
    public FeeDetail transaction;
}
