package com.qupworld.paymentgateway.models.mongo.collections.packageRate;

/**
 * Created by hoangnguyen on 5/17/17.
 */
public class FeesWithCurrency {
    public String currencyISO;
    public Double basedFee;
    public Double extraDuration;
    public Double extraDistance;
}
