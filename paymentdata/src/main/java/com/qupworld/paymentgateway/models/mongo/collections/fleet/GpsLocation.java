package com.qupworld.paymentgateway.models.mongo.collections.fleet;

/**
 * Created by myasus on 11/2/21.
 */
public class GpsLocation {
    public GpsLocationSetting android;
    public GpsLocationSetting androidGreater;
    public GpsLocationSetting ios;
    public GpsLocationSetting iosGreater;
}
