
package com.qupworld.paymentgateway.models.mongo.collections.zone;

import javax.annotation.Generated;
import java.util.Set;

@Generated("org.jsonschema2pojo")
public class Geo {

    public String type;
    public Set<double[][]> coordinates;
}
