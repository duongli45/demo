package com.qupworld.paymentgateway.models.mongo.collections.wallet;

import javax.annotation.Generated;

/**
 * Created by thuanho on 22/02/2021.
 */
@Generated("org.jsonschema2pojo")
public class ChargeExtra {

    public String type; // normal | discount | extra | bonus
    public double value;
    public double fixValue;
    public double razerPayBonusMinimum;
    public String razerPayBonusDriverChannel;
    public String razerPayBonusPassengerChannel;
    public String razerPayBonusReason;
}
