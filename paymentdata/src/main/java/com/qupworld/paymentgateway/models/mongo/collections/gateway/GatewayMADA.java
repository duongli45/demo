
package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GatewayMADA {

    public String fleetId;
    public String environment;
    public String userName;
    public String terminalID;
    public String password;
    public String secretKey;
}
