
package com.qupworld.paymentgateway.models.mongo.collections.fareHourly;

import com.qupworld.paymentgateway.models.mongo.collections.CancellationPolicy;
import com.qupworld.paymentgateway.models.mongo.collections.DriverCancelPolicy;
import com.qupworld.paymentgateway.models.mongo.collections.NoShow;
import org.bson.types.ObjectId;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class FareHourly {

    public ObjectId _id;
    public String name;
    public String fleetId;
    public Integer minimumHours;
    public Integer maximumHours;
    public Double feePerHour;
    public String type;
    public CancellationPolicy cancellationPolicy;
    public LimitDistance limitDistance;
    public NoShow noShow;
    public Boolean isActive;
    public DriverCancelPolicy driverCancelPolicy;
    public Double fareWaitTimeNow;
    public Double fareWaitTimeLater;

}
