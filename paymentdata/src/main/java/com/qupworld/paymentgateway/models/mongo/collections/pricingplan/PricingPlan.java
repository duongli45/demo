
package com.qupworld.paymentgateway.models.mongo.collections.pricingplan;

import org.bson.types.ObjectId;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class PricingPlan {

    public ObjectId _id;
    public String _class;
    public String fleetId;
    public WithinZone withinZone;
    public SameZone sameZone;
    public Domestic domestic;
    public International international;
    public Subscription subscription;
    public Double ccHandle;
    public Double hosting;
    public Integer dispatchRadius;
    public Boolean isActive;
    public Affiliate affiliate;
    public DriverDeposit driverDeposit;
    public DriverCashWallet driverCashWallet;
    public BankingInfo bankingInfo;
    public FleetPayment fleetPayment;
    public PaxCreditWallet paxCreditWallet;
    public boolean cashExcessFlow;
    public boolean contactLessChange;
    public boolean allowCreateBookingEstimateGtBalance;
    public PriceAdjustable priceAdjustable;
    public boolean verifyMerchantBank;
    public int promoPayToWallet;
    public MerchantCashWallet merchantCashWallet;
    public MerchantCreditWallet merchantCreditWallet;
    public boolean creditExcessFlow;
    public boolean allowPaxCreateBookingEstimateGtBalance;
    public boolean forceCreditCardToCreateBooking;
    public boolean allowDrvTopupDriverViaCreditWallet;
    public PartialPayment partialPayment;

}
