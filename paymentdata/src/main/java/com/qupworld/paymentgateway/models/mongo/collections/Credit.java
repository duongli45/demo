
package com.qupworld.paymentgateway.models.mongo.collections;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class Credit {

    public String _id;
    public String cardType = "";
    public String cardMask = "";
    public String cardHolder = "";
    public String localToken = "";
    public String crossToken = "";
    public String localFleetId = "";
    public String gateway = "";
    public String userId; // used for Dispatching send socket to mobile

    // save address info to use later
    public String street = "";
    public String city = "";
    public String state = "";
    public String zipCode = "";
    public String country = "";
    public String billingEmail = "";
    public String creditPhone = "";

    // support multiple gateway
    public List<String> zones = new ArrayList<String>();

    // mark as BBL card
    public Boolean isBBLCard;
    public String corporateId;
    public Boolean isSwitchedToBBL;
    public String companyName;

    // set default card for fleet owner
    public Boolean defaultCard;

}
