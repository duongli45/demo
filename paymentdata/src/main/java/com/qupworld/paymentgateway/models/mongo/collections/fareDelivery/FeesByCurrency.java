package com.qupworld.paymentgateway.models.mongo.collections.fareDelivery;

/**
 * Created by hoangnguyen on 5/17/17.
 */
public class FeesByCurrency {
    public String currencyISO;
    public Double feeFirstDistance;
    public Double feeSecondDistance;
    public Double feeThirdDistance;
    public Double feeAfterThirdDistance;
    public Double feePerMinute;
    public Double startingFee;
    public Double feeMinimum;
    public Double feeFirstStop;
    public Double feeExtraStop;
}
