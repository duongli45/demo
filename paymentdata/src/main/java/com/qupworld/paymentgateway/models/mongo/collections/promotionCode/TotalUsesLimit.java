package com.qupworld.paymentgateway.models.mongo.collections.promotionCode;

/**
 * Created by hoangnguyen on 7/19/17.
 */
public class TotalUsesLimit {
    public boolean isLimited;
    public double value;
}
