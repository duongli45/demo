
package com.qupworld.paymentgateway.models.mongo.collections.booking;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class CreditInfo_ {

    public String crossToken;
    public String gateway;
    public String localToken;
    public String cardMask;

}
