
package com.qupworld.paymentgateway.models.mongo.collections.account;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class RecentPlaces {

    public List<Pickup> pickup = new ArrayList<Pickup>();
    public List<Destination> destination = new ArrayList<Destination>();

}
