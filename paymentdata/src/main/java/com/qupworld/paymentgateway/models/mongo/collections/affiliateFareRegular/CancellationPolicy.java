
package com.qupworld.paymentgateway.models.mongo.collections.affiliateFareRegular;


public class CancellationPolicy {

    public InAdvance inAdvance;
    public OnDemand onDemand;
    public Policy penalize;
    public Policy compensate;

}
