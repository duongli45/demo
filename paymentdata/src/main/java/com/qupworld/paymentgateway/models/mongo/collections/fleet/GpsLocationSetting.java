package com.qupworld.paymentgateway.models.mongo.collections.fleet;

/**
 * Created by myasus on 11/2/21.
 */
public class GpsLocationSetting {
    public boolean enable;
    public double value;
}
