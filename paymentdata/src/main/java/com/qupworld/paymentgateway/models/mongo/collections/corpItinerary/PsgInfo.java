
package com.qupworld.paymentgateway.models.mongo.collections.corpItinerary;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class PsgInfo {

    public String firstName;
    public String lastName;
    public String fullName;
    public String userId;
    public String email;
    public String phone;
    public Integer rank;

}
