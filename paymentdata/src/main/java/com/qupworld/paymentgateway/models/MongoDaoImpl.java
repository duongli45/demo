package com.qupworld.paymentgateway.models;

import com.google.gson.Gson;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.model.Updates;
import com.mongodb.client.model.geojson.Point;
import com.mongodb.client.model.geojson.Position;
import com.mongodb.client.result.InsertOneResult;
import com.mongodb.client.result.UpdateResult;
import com.qupworld.paymentgateway.entities.ACHEnt;
import com.qupworld.paymentgateway.entities.CreditDeleteEnt;
import com.qupworld.paymentgateway.entities.CreditEnt;
import com.qupworld.paymentgateway.entities.Process;
import com.qupworld.paymentgateway.models.mongo.collections.AffiliateSetting.AffiliateSetting;
import com.qupworld.paymentgateway.models.mongo.collections.*;
import com.qupworld.paymentgateway.models.mongo.collections.Logs.Logs;
import com.qupworld.paymentgateway.models.mongo.collections.account.Account;
import com.qupworld.paymentgateway.models.mongo.collections.account.UpdateBalanceId;
import com.qupworld.paymentgateway.models.mongo.collections.additionalServices.AdditionalServices;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateAssignedRate.AffiliateAssignedRate;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareFlat.AffiliateFlat;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareGeneral.RateGeneral;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareHourly.AffiliateHourly;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareRegular.RateRegular;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateMarkupPrice.AffiliateMarkupPrice;
import com.qupworld.paymentgateway.models.mongo.collections.affiliatePackages.AffiliatePackages;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateProcess.AffiliateProcess;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateRoute.AffiliateRoute;
import com.qupworld.paymentgateway.models.mongo.collections.affiliationCarType.AffiliationCarType;
import com.qupworld.paymentgateway.models.mongo.collections.airportFee.AirportFee;
import com.qupworld.paymentgateway.models.mongo.collections.airportZone.AirportZone;
import com.qupworld.paymentgateway.models.mongo.collections.bankInfo.BankInfo;
import com.qupworld.paymentgateway.models.mongo.collections.booking.Booking;
import com.qupworld.paymentgateway.models.mongo.collections.bookingStatistic.BookingStatistic;
import com.qupworld.paymentgateway.models.mongo.collections.ccPackages.CCPackages;
import com.qupworld.paymentgateway.models.mongo.collections.company.Company;
import com.qupworld.paymentgateway.models.mongo.collections.configGateway.ConfigGateway;
import com.qupworld.paymentgateway.models.mongo.collections.corpItinerary.CorpItinerary;
import com.qupworld.paymentgateway.models.mongo.collections.corporate.Corporate;
import com.qupworld.paymentgateway.models.mongo.collections.driverFields.DriverFields;
import com.qupworld.paymentgateway.models.mongo.collections.dynamicFare.DynamicFare;
import com.qupworld.paymentgateway.models.mongo.collections.dynamicSurcharge.DynamicSurcharge;
import com.qupworld.paymentgateway.models.mongo.collections.failedCard.FailedCard;
import com.qupworld.paymentgateway.models.mongo.collections.fareDelivery.FareDelivery;
import com.qupworld.paymentgateway.models.mongo.collections.fareFlat.FareFlat;
import com.qupworld.paymentgateway.models.mongo.collections.fareHourly.FareHourly;
import com.qupworld.paymentgateway.models.mongo.collections.fareNormal.FareNormal;
import com.qupworld.paymentgateway.models.mongo.collections.fareSharing.FareSharing;
import com.qupworld.paymentgateway.models.mongo.collections.flatRoutes.FlatRoutes;
import com.qupworld.paymentgateway.models.mongo.collections.fleet.Fleet;
import com.qupworld.paymentgateway.models.mongo.collections.fleetfare.FleetFare;
import com.qupworld.paymentgateway.models.mongo.collections.gateway.*;
import com.qupworld.paymentgateway.models.mongo.collections.intercityRoute.IntercityRoute;
import com.qupworld.paymentgateway.models.mongo.collections.intercityTrip.Trip;
import com.qupworld.paymentgateway.models.mongo.collections.invoice.Invoice;
import com.qupworld.paymentgateway.models.mongo.collections.logToken.LogToken;
import com.qupworld.paymentgateway.models.mongo.collections.menuMerchant.MenuMerchant;
import com.qupworld.paymentgateway.models.mongo.collections.menuMerchantUser.MenuMerchantUser;
import com.qupworld.paymentgateway.models.mongo.collections.operations.Operations;
import com.qupworld.paymentgateway.models.mongo.collections.packageRate.PackageRate;
import com.qupworld.paymentgateway.models.mongo.collections.passengerInfo.OutStanding;
import com.qupworld.paymentgateway.models.mongo.collections.passengerInfo.PassengerInfo;
import com.qupworld.paymentgateway.models.mongo.collections.paxPromoList.PaxPromoList;
import com.qupworld.paymentgateway.models.mongo.collections.paxReferral.PaxReferral;
import com.qupworld.paymentgateway.models.mongo.collections.payoutTime.PayoutTime;
import com.qupworld.paymentgateway.models.mongo.collections.phoneZip.PhoneZip;
import com.qupworld.paymentgateway.models.mongo.collections.pricingplan.PricingPlan;
import com.qupworld.paymentgateway.models.mongo.collections.profile.Profile;
import com.qupworld.paymentgateway.models.mongo.collections.promotionCode.PromotionCode;
import com.qupworld.paymentgateway.models.mongo.collections.referral.Referral;
import com.qupworld.paymentgateway.models.mongo.collections.referral.ReferralHistories;
import com.qupworld.paymentgateway.models.mongo.collections.referral.ReferralInfo;
import com.qupworld.paymentgateway.models.mongo.collections.serverInformation.ServerInformation;
import com.qupworld.paymentgateway.models.mongo.collections.serviceFee.ServiceFee;
import com.qupworld.paymentgateway.models.mongo.collections.tripNotification.TripNotification;
import com.qupworld.paymentgateway.models.mongo.collections.updateHistory.UpdateHistory;
import com.qupworld.paymentgateway.models.mongo.collections.vehicleType.VehicleType;
import com.qupworld.paymentgateway.models.mongo.collections.voucher.VoucherCode;
import com.qupworld.paymentgateway.models.mongo.collections.wallet.*;
import com.qupworld.paymentgateway.models.mongo.collections.zone.Zone;
import com.qupworld.paymentgateway.models.mongo.collections.zonePayment.ZonePayment;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.TimezoneUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.JSONObject;
import org.json.simple.JSONArray;

import java.util.*;

/**
 * Created by qup on 7/13/16.
 * Data Access Layer
 */
public class MongoDaoImpl implements MongoDao {


    final static Logger logger = LogManager.getLogger(MongoDaoImpl.class);
    private MongoDatabase database;
    private Gson gson;
    public MongoDaoImpl() {
        try {
            database = DAOManager.getMongoDB();
//            database = TestConnect.getMongoDB();
            gson = new Gson();
        } catch (Exception e) {
            // Handle errors for Exception
            e.printStackTrace();
        }

    }

    @SuppressWarnings("unchecked")
    private <T> T processRequest(Bson filter, Class<T> clazz) {
        try {
            MongoCollection<Document> collection = database.getCollection(clazz.getSimpleName(), Document.class);
            Document document = collection.find(filter).first();
            return gson.fromJson(gson.toJson(document), clazz);
        } catch (Exception ex) {
            System.out.println(" - processRequest exception" + CommonUtils.getError(ex));
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    private <T> List<T> processListRequest(Bson filter, Bson sort, int skip, int size, Class<T> clazz) {
        try {
            MongoCollection<Document> collection = database.getCollection(clazz.getSimpleName(), Document.class);
            FindIterable findIterable = collection.find(filter);
            if (sort != null)
                findIterable.sort(sort);
            if (skip > 0)
                findIterable.skip(skip);
            if (size > 0)
                findIterable.limit(size);
            MongoCursor<Document> cursor = findIterable.iterator();
            List<T> list = new ArrayList<>();
            while(cursor.hasNext()) {
                T doc = gson.fromJson(gson.toJson(cursor.next()), clazz);
                list.add(doc);
            }
            return list;
        } catch (Exception ex) {
            System.out.println(" - processRequest exception" + CommonUtils.getError(ex));
            return null;
        }
    }

    public Fleet getFleetInfor (String fleetId){
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, Fleet.class);
    }
    public GatewayJetpay getGatewayJetpayFleet(String fleetId){
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayJetpay.class);
    }
    public GatewayJetpay getGatewayJetpaySystem(boolean isSanbox){
        Bson filter;
        if (isSanbox) {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "sandbox")
            );
        } else {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "production")
            );
        }
        return processRequest(filter, GatewayJetpay.class);
    }
    public GatewayCielo getGatewayCieloFleet(String fleetId){
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayCielo.class);
    }
    public GatewayCielo getGatewayCieloSystem(boolean isSanbox){
        Bson filter;
        if (isSanbox) {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "sandbox")
            );
        } else {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "production")
            );
        }
        return processRequest(filter, GatewayCielo.class);
    }
    public GatewayPeachPayments getGatewayPeachPaymentsFleet(String fleetId){
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayPeachPayments.class);
    }
    public GatewayPeachPayments getGatewayPeachPaymentsSystem(boolean isSanbox){
        Bson filter;
        if (isSanbox) {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "sandbox")
            );
        } else {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "production")
            );
        }
        return processRequest(filter, GatewayPeachPayments.class);
    }
    public GatewayAuthorize getGatewayAuthorizeFleet(String fleetId){
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayAuthorize.class);
    }
    public GatewayAuthorize getGatewayAuthorizeSystem(boolean isSanbox){
        Bson filter;
        if (isSanbox) {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "sandbox")
            );
        } else {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "production")
            );
        }
        return processRequest(filter, GatewayAuthorize.class);
    }
    public GatewayBluefin getGatewayBluefinFleet(String fleetId){
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayBluefin.class);
    }
    public GatewayBluefin getGatewayBluefinSystem(boolean isSanbox){
        Bson filter;
        if (isSanbox) {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "sandbox")
            );
        } else {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "production")
            );
        }
        return processRequest(filter, GatewayBluefin.class);
    }
    public GatewayCreditCorp getGatewayCreditCorpFleet(String fleetId){
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayCreditCorp.class);
    }
    public GatewayCXPay getGatewayCXPayFleet(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayCXPay.class);
    }
    public GatewayFirstAtlanticCommerce getGatewayFACFleet(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayFirstAtlanticCommerce.class);
    }
    public GatewayPayfort getGatewayPayfortFleet(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayPayfort.class);
    }
    public GatewayOmise getGatewayOmiseFleet(String fleetId){
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayOmise.class);
    }
    public GatewayPayCorp getGatewayPayCorpFleet(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayPayCorp.class);
    }
    public GatewayMasapay getGatewayMasapayFleet(String fleetId){
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayMasapay.class);
    }
    public GatewayPingPong getGatewayPingPongFleet(String fleetId){
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayPingPong.class);
    }
    public GatewayPayU getGatewayPayUFleet(String fleetId){
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayPayU.class);
    }
    public GatewayExpresspay getGatewayExpresspayFleet(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayExpresspay.class);
    }
    public GatewayFlutterwave getGatewayFlutterwaveFleet(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayFlutterwave.class);
    }
    public  GatewayStripe getGatewayStripeFleet(String fleetId){
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayStripe.class);
    }
    public  GatewayStripe getGatewayStripeSystem(boolean isSanbox){
        Bson filter;
        if (isSanbox) {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "sandbox")
            );
        } else {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "production")
            );
        }
        return processRequest(filter, GatewayStripe.class);
    }

    public  GatewayBraintree getGatewayBraintreeFleet(String fleetId){
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayBraintree.class);
    }
    public  GatewayBraintree getGatewayBraintreeSystem(boolean isSanbox){
        Bson filter;
        if (isSanbox) {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "sandbox")
            );
        } else {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "production")
            );
        }
        return processRequest(filter, GatewayBraintree.class);
    }


    public GatewayYeepay getGatewayYeepayFleet(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayYeepay.class);
    }

    public GatewayAdyen getGatewayAdyenFleet(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayAdyen.class);
    }

    public GatewayConekta getGatewayConektaFleet(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayConekta.class);
    }

    public GatewaySenangpay getGatewaySenangpayFleet(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewaySenangpay.class);
    }

    public GatewayPayDollar getGatewayPayDollarFleet(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayPayDollar.class);
    }

    public GatewayEGHL getGatewayPayEGHLFleet(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayEGHL.class);
    }

    public  boolean getPayToFleet(String fleetId){
        Bson filter = Filters.eq("fleetId", fleetId);
        PricingPlan pricingPlan = processRequest(filter, PricingPlan.class);
        return pricingPlan != null && pricingPlan.withinZone.ccPayToFleet != null && pricingPlan.withinZone.ccPayToFleet;
    }
    public  PricingPlan getPricingPlan(String fleetId){
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, PricingPlan.class);
    }
    public  boolean addCreditCard(String credit, CreditEnt cardInfor, String type){
        Bson update = Updates.push("credits", gson.fromJson(credit, org.json.simple.JSONObject.class));
        if (type.equals("user") || type.equals("customer")) {
            MongoCollection<Account> collection = database.getCollection("Account", Account.class);
            Bson filter = Filters.eq("userId", cardInfor.userId);

            // Defines options that configure the operation to return a document in its post-operation state
            /*FindOneAndUpdateOptions options = new FindOneAndUpdateOptions()
                    .returnDocument(ReturnDocument.AFTER);*/
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        }
        if (type.equals("corporate")){
            MongoCollection<Corporate> collection = database.getCollection("Corporate", Corporate.class);
            Bson filter = Filters.eq("_id", new ObjectId(cardInfor.userId));
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        }
        if (type.equals("fleetOwner")){
            MongoCollection<Fleet> collection = database.getCollection("Fleet", Fleet.class);
            Bson filter = Filters.eq("fleetId", cardInfor.fleetId);
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        }
        return false;
    }

    public Credit getCreditByToken(String userId,String localToken,String crossToken, String gateway){
        Bson filter = Filters.eq("userId", userId);
        Account account = processRequest(filter, Account.class);
        if (account != null && account.credits != null && !account.credits.isEmpty()) {
            account.credits.stream().filter(credit -> credit.localToken.equals(localToken) && credit.gateway.equals(gateway))
                    .findFirst().get();
        }
        return null;
    }
    public boolean deleteCreditByToke(CreditDeleteEnt creditDelete,String gateway){
        MongoCollection<Account> collection = database.getCollection("Account", Account.class);
        Bson filter = Filters.eq("userId", creditDelete.userId);
        org.json.simple.JSONObject objCredit = new org.json.simple.JSONObject();
        objCredit.put("localToken", creditDelete.localToken);
        objCredit.put("crossToken", creditDelete.crossToken);
        objCredit.put("gateway", gateway);
        Bson update = Updates.pull("credits", objCredit);
        // Defines options that configure the operation to return a document in its post-operation state
            /*FindOneAndUpdateOptions options = new FindOneAndUpdateOptions()
                    .returnDocument(ReturnDocument.AFTER);*/
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public Credit getCreditCorporateByToken(CreditDeleteEnt creditDelete, String gateway){
        Bson filter = Filters.eq("_id", new ObjectId(creditDelete.userId));
        Corporate account = processRequest(filter, Corporate.class);
        if (account != null && account.credits != null && !account.credits.isEmpty()) {
            return account.credits.stream().filter(credit -> credit.localToken.equals(creditDelete.localToken) && credit.crossToken.equals(creditDelete.crossToken) && credit.gateway.equals(gateway))
                    .findFirst().get();
        }
        return null;
    }
    public boolean deleteCreditCorporate(CreditDeleteEnt creditDelete,String gateway){
        MongoCollection<Corporate> collection = database.getCollection("Corporate", Corporate.class);
        Bson filter = Filters.eq("_id", new ObjectId(creditDelete.userId));
        org.json.simple.JSONObject objCredit = new org.json.simple.JSONObject();
        objCredit.put("localToken", creditDelete.localToken);
        objCredit.put("crossToken", creditDelete.crossToken);
        objCredit.put("gateway", gateway);
        Bson update = Updates.pull("credits", objCredit);
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }
    public boolean deleteCreditFleetOwner(CreditDeleteEnt creditDelete,String gateway){
        MongoCollection<Fleet> collection = database.getCollection("Fleet", Fleet.class);
        Bson filter = Filters.eq("fleetId", creditDelete.fleetId);
        org.json.simple.JSONObject objCredit = new org.json.simple.JSONObject();
        objCredit.put("localToken", creditDelete.localToken);
        objCredit.put("crossToken", creditDelete.crossToken);
        objCredit.put("gateway", gateway);
        Bson update = Updates.pull("credits", objCredit);
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }
    public List<Credit> getCreditByUserId(String userId){
        if (userId != null && !userId.isEmpty()) {
            Bson filter = Filters.eq("userId", userId);
            Account account = processRequest(filter, Account.class);
            if (account != null) {
                return account.credits;
            } else {
                return new ArrayList<>();
            }
        } else {
            return new ArrayList<>();
        }
    }

    public  boolean updateCreditCards(String userId, List<Credit> credis){
        MongoCollection<Account> collection = database.getCollection("Account", Account.class);
        Bson filter = Filters.eq("userId", userId);
        Bson update = Updates.set("credits", credis);

        // Updates all documents and prints the number of matched and modified documents
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public void updateCreditCards(String cardOwner, String id, List<Credit> credits){
        Bson update = Updates.set("credits", credits);
        if (cardOwner.equals("user") || cardOwner.equals("customer") || cardOwner.equals("passenger") || cardOwner.equals("mDispatcher")
                || cardOwner.equals("driver")) {
            MongoCollection<Account> collection = database.getCollection("Account", Account.class);
            Bson filter = Filters.eq("userId", id);
            collection.updateOne(filter, update);
        }
        if (cardOwner.equals("corporate")){
            MongoCollection<Corporate> collection = database.getCollection("Corporate", Corporate.class);
            Bson filter = Filters.eq("_id", new ObjectId(id));
            collection.updateOne(filter, update);
        }
        if (cardOwner.equals("fleetOwner")){
            MongoCollection<Fleet> collection = database.getCollection("Fleet", Fleet.class);
            Bson filter = Filters.eq("fleetId", id);
            collection.updateOne(filter, update);
        }
    }
    public  boolean addProfile(Profile profile){
        MongoCollection<Profile> collection = database.getCollection("Profile", Profile.class);
        InsertOneResult result = collection.insertOne(profile);
        return result.wasAcknowledged();
    }
    public Booking getBooking(String bookId){
        Bson filter = Filters.eq("bookId", bookId);
        return processRequest(filter, Booking.class);
    }

    public Booking getBookingCompleted(String bookId){
        MongoCollection<Document> collection = database.getCollection("BookingCompleted", Document.class);
        Bson filter = Filters.eq("bookId", bookId);
        Document document = collection.find(filter).first();
        return gson.fromJson(gson.toJson(document), Booking.class);

    }

    public FleetFare getFleetFare(String fleetId){
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, FleetFare.class);
    }
    public Account getAccount(String id){
        if (id != null && !id.isEmpty()) {
            Bson filter = Filters.eq("_id", new ObjectId(id));
            /*MongoCollection<Document> collection = database.getCollection("Account", Document.class);
            Document document = collection.find(filter).first();
            Account account = gson.fromJson(gson.toJson(document), Account.class);
            logger.debug(requestId + " - old query: " + gson.toJson(account));*/
            return processRequest(filter, Account.class);
        } else {
            return null;
        }
    }

    public Account getAccountByPhoneAndFleetAndType(String phone, String fleetId, String type){
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("phone", phone),
                Filters.eq("appType", type)
        );
        return processRequest(filter, Account.class);
    }

    public QueuingArea getQueuingArea(String id){
        try {
            Bson filter = Filters.eq("_id", new ObjectId(id));
            return processRequest(filter, QueuingArea.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public Corporate getCorporate(String id) {
        try {
            Bson filter = Filters.eq("_id", new ObjectId(id));
            return processRequest(filter, Corporate.class);
        } catch(Exception ex) {
            return null;
        }
    }
    public boolean updateCorporatePrePaid(String id,double balance){
        MongoCollection<Corporate> collection = database.getCollection("Corporate", Corporate.class);
        Bson filter = Filters.and(
                Filters.eq("_id", new ObjectId(id)),
                Filters.eq("paymentMethods.type", "prepaid")
        );
        Bson update = Updates.set("paymentMethods.$.balance", balance);
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }
    public UsersSystem getUsersSystem(String id) {
        try {
            Bson filter = Filters.eq("_id", new ObjectId(id));
            return processRequest(filter, UsersSystem.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public Vehicle getVehicle(String id) {
        try {
            Bson filter = Filters.eq("_id", new ObjectId(id));
            return processRequest(filter, Vehicle.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public Roles getRoles(String id) {
        try{
            Bson filter = Filters.eq("_id", new ObjectId(id));
            return processRequest(filter, Roles.class);
        }catch (Exception ex){
            return null;
        }
    }

    public ServerInformation getServerInformation() {
        MongoCollection<Document> collection = database.getCollection("ServerInformation", Document.class);
        Document document = collection.find().first();
        return gson.fromJson(gson.toJson(document), ServerInformation.class);
    }

    public VehicleType getVehicleTypeByFleetAndName(String fleetId, String name){
        try {
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("vehicleType", name)
            );
            return processRequest(filter, VehicleType.class);
        } catch(Exception ex) {
            return null;
        }
    }
    public List<VehicleType> getVehicleTypesByFleet(String fleetId){
        try {
            Bson filter = Filters.eq("fleetId", fleetId);
            return processListRequest(filter, null, 0, 0, VehicleType.class);
        } catch(Exception ex) {
            return null;
        }
    }
    public VehicleType getVehicleTypeByFleetAndNameAndRate(String fleetId, String name, String rateType, String zoneId){
        try {
            Bson filterRate = Filters.and(
                    Filters.eq("rateType", rateType),
                    Filters.eq("zoneId", zoneId)
            );
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("vehicleType", name),
                    Filters.elemMatch("rates", filterRate)
            );
            return processRequest(filter, VehicleType.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public FareHourly getFareHourlyById(String fareHourlyId) {
        try {
            Bson filter = Filters.eq("_id", new ObjectId(fareHourlyId));
            return processRequest(filter, FareHourly.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public FareFlat getFareFlatById(String fareFlatId) {
        try {
            Bson filter = Filters.eq("_id", new ObjectId(fareFlatId));
            return processRequest(filter, FareFlat.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public FareNormal getFareNormalById(String fareNormalId) {
        try {
            Bson filter = Filters.eq("_id", new ObjectId(fareNormalId));
            return processRequest(filter, FareNormal.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public FareDelivery getFareDeliveryById(String fareDeliveryId) {
        try {
            Bson filter = Filters.eq("_id", new ObjectId(fareDeliveryId));
            return processRequest(filter, FareDelivery.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public FareSharing getFareSharingById(String fareSharingId) {
        try {
            Bson filter = Filters.eq("_id", new ObjectId(fareSharingId));
            return processRequest(filter, FareSharing.class);
        } catch(Exception ex) {
            return null;
        }
    }


    public PackageRate getPackageRateById(String packageId) {
        try {
            Bson filter = Filters.eq("_id", new ObjectId(packageId));
            return processRequest(filter, PackageRate.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public List<PackageRate> findPackageRateByFareHourlyId(String fareHourlyId) {
        try {
            Bson filter = Filters.eq("fareHourlyId", fareHourlyId);
            Bson sort = Sorts.ascending("createdDate");
            return processListRequest(filter, sort, 0, 0, PackageRate.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public RateRegular getAffiliateRate(String rateId) {
        try {
            MongoCollection<Document> collection = database.getCollection("AffiliateRate", Document.class);
            Bson filter = Filters.eq("_id", new ObjectId(rateId));
            Document document = collection.find(filter).first();
            return gson.fromJson(gson.toJson(document), RateRegular.class);
        } catch(Exception ex) {
            return null;
        }
    }
    public RateGeneral getAffiliateRateGeneral(String rateId){
        try {
            MongoCollection<Document> collection = database.getCollection("AffiliateRate", Document.class);
            Bson filter = Filters.eq("_id", new ObjectId(rateId));
            Document document = collection.find(filter).first();
            return gson.fromJson(gson.toJson(document), RateGeneral.class);
        } catch(Exception ex) {
            return null;
        }
    }
    public AffiliationCarType getAffiliationCarTypeByName(String name) {
        try {
            Bson filter = Filters.eq("name", name);
            return processRequest(filter, AffiliationCarType.class);
        } catch(Exception ex) {
            return null;
        }
    }
    public AffiliationCarType getAffiliationCarType(String id) {
        try {
            Bson filter = Filters.eq("_id", new ObjectId(id));
            return processRequest(filter, AffiliationCarType.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public List<AffiliateAssignedRate> getListAffiliateAssignedRate(String carTypeId, String zoneId) {
        try {
            Bson filter = Filters.and(
                    Filters.eq("carTypeId", carTypeId),
                    Filters.eq("zone.id", zoneId)
            );
            return processListRequest(filter, null, 0, 0, AffiliateAssignedRate.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public List<AffiliateAssignedRate> findAffiliateFlatRateByCarType(String carTypeId) {
        try {
            Bson filter = Filters.and(
                    Filters.eq("carTypeId", carTypeId),
                    Filters.eq("type", "Flat")
            );
            return processListRequest(filter, null, 0, 0, AffiliateAssignedRate.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public List<AffiliateAssignedRate> findAffiliateHourlyRateByCarTypeAndFleetId(String carTypeId, String fleetId) {
        try {
            Bson filter = Filters.and(
                    Filters.eq("carTypeId", carTypeId),
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("type", "Hourly")
            );
            return processListRequest(filter, null, 0, 0, AffiliateAssignedRate.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateAssignedRate findRateByCarTypeIdAndZoneIdAndType(String carTypeId, String zoneId, String type, String rateType) {
        try {
            Bson filter = Filters.and(
                    Filters.eq("fleetId", ""),
                    Filters.eq("carTypeId", carTypeId),
                    Filters.eq("zone.id", zoneId),
                    Filters.eq("type", type),
                    Filters.eq("priceType", rateType)
            );
            return processRequest(filter, AffiliateAssignedRate.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateAssignedRate findRateByCarTypeIdAndZoneIdAndTypeAndFleetId(String carTypeId, String zoneId, String fleetId, String type, String rateType) {
        try {
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("carTypeId", carTypeId),
                    Filters.eq("zone.id", zoneId),
                    Filters.eq("type", type),
                    Filters.eq("priceType", rateType)
            );
            return processRequest(filter, AffiliateAssignedRate.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateHourly findAffiliateHourlyById (String fareHourlyId) {
        try {
            MongoCollection<Document> collection = database.getCollection("AffiliateRate", Document.class);
            Bson filter = Filters.eq("_id", new ObjectId(fareHourlyId));
            Document document = collection.find(filter).first();
            return gson.fromJson(gson.toJson(document), AffiliateHourly.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateFlat findAffiliateFlatById(String flatId) {
        try {
            MongoCollection<Document> collection = database.getCollection("AffiliateRate", Document.class);
            Bson filter = Filters.eq("_id", new ObjectId(flatId));
            Document document = collection.find(filter).first();
            return gson.fromJson(gson.toJson(document), AffiliateFlat.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliatePackages getPackageRateByIdAffiliate(String packageId) {
        try {
            Bson filter = Filters.eq("_id", new ObjectId(packageId));
            return processRequest(filter, AffiliatePackages.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateRoute getAffiliateRouteById(String routeId) {
        try {
            Bson filter = Filters.eq("_id", new ObjectId(routeId));
            return processRequest(filter, AffiliateRoute.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateRoute getByGeoAffiliate(String fareFlatId, List<Double> pickup, List<Double> destination) {
        try {
            Point geoPickup = new Point(new Position(pickup.get(0),pickup.get(1)));
            Point geoDestination = new Point(new Position(destination.get(0),destination.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("flatRateId", fareFlatId),
                    Filters.geoIntersects("zone.fromZone", geoPickup),
                    Filters.geoIntersects("zone.toZone", geoDestination)
            );
            return processRequest(filter, AffiliateRoute.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateRoute getByGeoReturnAffiliate(String fareFlatId, List<Double> pickup, List<Double> destination) {
        try {
            Point geoPickup = new Point(new Position(pickup.get(0),pickup.get(1)));
            Point geoDestination = new Point(new Position(destination.get(0),destination.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("flatRateId", fareFlatId),
                    Filters.eq("singleTrip.enableReturnRoute", true),
                    Filters.geoIntersects("zone.fromZone", geoDestination),
                    Filters.geoIntersects("zone.toZone", geoPickup)
            );
            return processRequest(filter, AffiliateRoute.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateRoute getByGeoRoundTripAffiliate(String fareFlatId, List<Double> pickup, List<Double> destination) {
        try {
            Point geoPickup = new Point(new Position(pickup.get(0),pickup.get(1)));
            Point geoDestination = new Point(new Position(destination.get(0),destination.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("flatRateId", fareFlatId),
                    Filters.eq("roundTrip.enableRoundTrip", true),
                    Filters.geoIntersects("zone.fromZone", geoPickup),
                    Filters.geoIntersects("zone.toZone", geoDestination)
            );
            return processRequest(filter, AffiliateRoute.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateRoute getByGeoRoundTripReturnAffiliate(String fareFlatId, List<Double> pickup, List<Double> destination) {
        try {
            Point geoPickup = new Point(new Position(pickup.get(0),pickup.get(1)));
            Point geoDestination = new Point(new Position(destination.get(0),destination.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("flatRateId", fareFlatId),
                    Filters.eq("roundTrip.enableRoundTrip", true),
                    Filters.eq("singleTrip.enableReturnRoute", true),
                    Filters.geoIntersects("zone.fromZone", geoDestination),
                    Filters.geoIntersects("zone.toZone", geoPickup)
            );
            return processRequest(filter, AffiliateRoute.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateRoute getByZipCodeAffiliate(String fareFlatId, String zipCodeFrom, String zipCodeTo) {
        try {
            List<String> zipFrom = Collections.singletonList(zipCodeFrom);
            List<String> zipTo = Collections.singletonList(zipCodeTo);
            Bson filter = Filters.and(
                    Filters.eq("flatRateId", fareFlatId),
                    Filters.in("zipCode.fromZipCode", zipFrom),
                    Filters.in("zipCode.toZipCode", zipTo)
            );
            return processRequest(filter, AffiliateRoute.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateRoute getByZipCodeReturnAffiliate(String fareFlatId, String zipCodeFrom, String zipCodeTo) {
        try {
            List<String> zipFrom = Collections.singletonList(zipCodeFrom);
            List<String> zipTo = Collections.singletonList(zipCodeTo);
            Bson filter = Filters.and(
                    Filters.eq("flatRateId", fareFlatId),
                    Filters.eq("singleTrip.enableReturnRoute", true),
                    Filters.in("zipCode.fromZipCode", zipTo),
                    Filters.in("zipCode.toZipCode", zipFrom)
            );
            return processRequest(filter, AffiliateRoute.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateRoute getByZipCodeRoundTripAffiliate(String fareFlatId, String zipCodeFrom, String zipCodeTo) {
        try {
            List<String> zipFrom = Collections.singletonList(zipCodeFrom);
            List<String> zipTo = Collections.singletonList(zipCodeTo);
            Bson filter = Filters.and(
                    Filters.eq("flatRateId", fareFlatId),
                    Filters.eq("roundTrip.enableRoundTrip", true),
                    Filters.in("zipCode.fromZipCode", zipFrom),
                    Filters.in("zipCode.toZipCode", zipTo)
            );
            return processRequest(filter, AffiliateRoute.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateRoute getByZipCodeRoundTripReturnAffiliate(String fareFlatId, String zipCodeFrom, String zipCodeTo) {
        try {
            List<String> zipFrom = Collections.singletonList(zipCodeFrom);
            List<String> zipTo = Collections.singletonList(zipCodeTo);
            Bson filter = Filters.and(
                    Filters.eq("flatRateId", fareFlatId),
                    Filters.eq("singleTrip.enableReturnRoute", true),
                    Filters.eq("roundTrip.enableRoundTrip", true),
                    Filters.in("zipCode.fromZipCode", zipTo),
                    Filters.in("zipCode.toZipCode", zipFrom)
            );
            return processRequest(filter, AffiliateRoute.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public long countFailedCard(String fleetId, String cardId) {
        MongoCollection<FailedCard> collection = database.getCollection("FailedCard", FailedCard.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("cardId", cardId)
        );
        return collection.countDocuments(filter);
    }
    public String addFailedCard(FailedCard failedCard) {
        MongoCollection<FailedCard> collection = database.getCollection("FailedCard", FailedCard.class);
        InsertOneResult result = collection.insertOne(failedCard);
        return result.getInsertedId().toString();

    }

    public boolean updateCreditCardsOfCorporate(String corporateId, List<Credit> credits){
        MongoCollection<Corporate> collection = database.getCollection("Corporate", Corporate.class);
        Bson filter = Filters.eq("_id", new ObjectId(corporateId));
        Bson update = Updates.set("credits", credits);

        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public Zone findByFleetIdAndGeo(String fleetId, List<Double> pickup) {
        try {
            Point point = new Point(new Position(pickup.get(0),pickup.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("isActive", true),
                    Filters.geoIntersects("geo", point)
            );
            return processRequest(filter, Zone.class);
        } catch(Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    public List<Zone> findByGeoAffiliate(List<Double> pickup, String fleetId, String vehicleTypeId) {
        List<Zone> zones = new ArrayList<>();
        try {
            List<String> vehicle = Collections.singletonList(vehicleTypeId);
            Point geometry = new Point(new Position(pickup.get(0),pickup.get(1)));
            Bson filter = Filters.and(
                    Filters.ne("fleetId", fleetId),
                    Filters.eq("isActive", true),
                    Filters.eq("assignAffiliate", true),
                    Filters.in("affiliateCarType", vehicle),
                    Filters.geoIntersects("geo", geometry)
            );
            return processListRequest(filter, null, 0, 0, Zone.class);
        } catch(Exception ex) {

        }
        return zones;
    }

    public Zone findByGeoSuplier(List<Double> pickup, String fleetId, String vehicleTypeId) {
        try {
            List<String> vehicle = Collections.singletonList(vehicleTypeId);
            Point geometry = new Point(new Position(pickup.get(0),pickup.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("isActive", true),
                    Filters.eq("assignAffiliate", true),
                    Filters.in("affiliateCarType", vehicle),
                    Filters.geoIntersects("geo", geometry)
            );
            return processRequest(filter, Zone.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public FlatRoutes getByGeo(String fareFlatId, List<Double> pickup, List<Double> destination) {
        try {
            Point geoPickup = new Point(new Position(pickup.get(0),pickup.get(1)));
            Point geoDestination = new Point(new Position(destination.get(0),destination.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("fareFlatId", fareFlatId),
                    Filters.geoIntersects("zone.fromZone", geoPickup),
                    Filters.geoIntersects("zone.toZone", geoDestination)
            );
            return processRequest(filter, FlatRoutes.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public FlatRoutes getRouteById(String routeId) {
        try {
            Bson filter = Filters.eq("_id", new ObjectId(routeId));
            return processRequest(filter, FlatRoutes.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public FlatRoutes getByGeoReturn(String fareFlatId, List<Double> pickup, List<Double> destination) {
        try {
            Point geoPickup = new Point(new Position(pickup.get(0),pickup.get(1)));
            Point geoDestination = new Point(new Position(destination.get(0),destination.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("fareFlatId", fareFlatId),
                    Filters.eq("singleTrip.enableReturnRoute", true),
                    Filters.geoIntersects("zone.fromZone", geoDestination),
                    Filters.geoIntersects("zone.toZone", geoPickup)
            );
            return processRequest(filter, FlatRoutes.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public FlatRoutes getByGeoRoundTrip(String fareFlatId, List<Double> pickup, List<Double> destination) {
        try {
            Point geoPickup = new Point(new Position(pickup.get(0),pickup.get(1)));
            Point geoDestination = new Point(new Position(destination.get(0),destination.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("fareFlatId", fareFlatId),
                    Filters.eq("roundTrip.enableRoundTrip", true),
                    Filters.geoIntersects("zone.fromZone", geoPickup),
                    Filters.geoIntersects("zone.toZone", geoDestination)
            );
            return processRequest(filter, FlatRoutes.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public FlatRoutes getByGeoRoundTripReturn(String fareFlatId, List<Double> pickup, List<Double> destination) {
        try {
            Point geoPickup = new Point(new Position(pickup.get(0),pickup.get(1)));
            Point geoDestination = new Point(new Position(destination.get(0),destination.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("fareFlatId", fareFlatId),
                    Filters.eq("roundTrip.enableRoundTrip", true),
                    Filters.eq("singleTrip.enableReturnRoute", true),
                    Filters.geoIntersects("zone.fromZone", geoDestination),
                    Filters.geoIntersects("zone.toZone", geoPickup)
            );
            return processRequest(filter, FlatRoutes.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public FlatRoutes getByZipCode(String fareFlatId, String zipCodeFrom, String zipCodeTo) {
        try {
            List<String> zipFrom = Collections.singletonList(zipCodeFrom);
            List<String> zipTo = Collections.singletonList(zipCodeTo);
            Bson filter = Filters.and(
                    Filters.eq("fareFlatId", fareFlatId),
                    Filters.in("zipCode.fromZipCode", zipFrom),
                    Filters.in("zipCode.toZipCode", zipTo)
            );
            return processRequest(filter, FlatRoutes.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public FlatRoutes getByZipCodeReturn(String fareFlatId, String zipCodeFrom, String zipCodeTo) {
        try {
            List<String> zipFrom = Collections.singletonList(zipCodeFrom);
            List<String> zipTo = Collections.singletonList(zipCodeTo);
            Bson filter = Filters.and(
                    Filters.eq("fareFlatId", fareFlatId),
                    Filters.eq("singleTrip.enableReturnRoute", true),
                    Filters.in("zipCode.fromZipCode", zipTo),
                    Filters.in("zipCode.toZipCode", zipFrom)
            );
            return processRequest(filter, FlatRoutes.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public FlatRoutes getByZipCodeRoundTrip(String fareFlatId, String zipCodeFrom, String zipCodeTo) {
        try {
            List<String> zipFrom = Collections.singletonList(zipCodeFrom);
            List<String> zipTo = Collections.singletonList(zipCodeTo);
            Bson filter = Filters.and(
                    Filters.eq("fareFlatId", fareFlatId),
                    Filters.eq("roundTrip.enableRoundTrip", true),
                    Filters.in("zipCode.fromZipCode", zipFrom),
                    Filters.in("zipCode.toZipCode", zipTo)
            );
            return processRequest(filter, FlatRoutes.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public FlatRoutes getByZipCodeRoundTripReturn(String fareFlatId, String zipCodeFrom, String zipCodeTo) {
        try {
            List<String> zipFrom = Collections.singletonList(zipCodeFrom);
            List<String> zipTo = Collections.singletonList(zipCodeTo);
            Bson filter = Filters.and(
                    Filters.eq("fareFlatId", fareFlatId),
                    Filters.eq("singleTrip.enableReturnRoute", true),
                    Filters.eq("roundTrip.enableRoundTrip", true),
                    Filters.in("zipCode.fromZipCode", zipTo),
                    Filters.in("zipCode.toZipCode", zipFrom)
            );
            return processRequest(filter, FlatRoutes.class);
        } catch(Exception ex) {
            return null;
        }
    }

    @Override
    public IntercityRoute getIntercityRouteByGeo(String intercityRateId, List<Double> pickup, List<Double> destination) {
        try {
            Point geoPickup = new Point(new Position(pickup.get(0),pickup.get(1)));
            Point geoDestination = new Point(new Position(destination.get(0),destination.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("intercityRateId", intercityRateId),
                    Filters.geoIntersects("firstLocation", geoDestination),
                    Filters.geoIntersects("secondLocation", geoPickup)
            );
            return processRequest(filter, IntercityRoute.class);
        } catch(Exception ex) {
            return null;
        }
    }

    @Override
    public IntercityRoute getIntercityRouteRouteById(String intercityRouteId) {
        try {
            Bson filter = Filters.eq("_id", new ObjectId(intercityRouteId));
            return processRequest(filter, IntercityRoute.class);
        } catch(Exception ex) {
            return null;
        }
    }

    @Override
    public IntercityRoute getIntercityRouteByGeoReturn(String intercityRateId, List<Double> pickup, List<Double> destination) {
        try {
            Point geoPickup = new Point(new Position(pickup.get(0),pickup.get(1)));
            Point geoDestination = new Point(new Position(destination.get(0),destination.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("intercityRateId", intercityRateId),
                    Filters.eq("roundTwoEnable", true),
                    Filters.geoIntersects("firstLocation", geoDestination),
                    Filters.geoIntersects("secondLocation", geoPickup)
            );
            return processRequest(filter, IntercityRoute.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public AirportZone checkDestinationByGeo(List<Double> destination) {
        try {
            Point geoDestination = new Point(new Position(destination.get(0),destination.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("isActive", true),
                    Filters.geoIntersects("geo", geoDestination)
            );
            return processRequest(filter, AirportZone.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public PromotionCode getPromoCodeByFleetAndName(String fleetId, String promoCode) {
        String promo = promoCode != null ? promoCode : "";
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("promotionCode", promo.toUpperCase())
        );
        return processRequest(filter, PromotionCode.class);
    }

    public PromotionCode getPromoCodeByFleetAndNameAndZone(String fleetId, String promoCode, String zoneId) {
        String promo = promoCode != null ? promoCode : "";
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("promotionCode", promo.toUpperCase()),
                Filters.in("zoneId", zoneId)
        );
        return processRequest(filter, PromotionCode.class);
    }

    public PromotionCode getPromoCodeByFleetAndNameAndCustomer(String fleetId, String promoCode, String customerId) {
        String promo = promoCode != null ? promoCode : "";
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("promotionCode", promo.toUpperCase()),
                Filters.in("customerId", customerId)
        );
        return processRequest(filter, PromotionCode.class);
    }

    public TripNotification getTripNotificationByCustomer(String fleetId, String customerId, Date date) {
        try{
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("customerId", customerId),
                    Filters.eq("date", date)
            );
            return processRequest(filter, TripNotification.class);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public void addTripNotification(TripNotification tripNotification) {
        MongoCollection<TripNotification> collection = database.getCollection("TripNotification", TripNotification.class);
        InsertOneResult result = collection.insertOne(tripNotification);
    }

    public void updateTripNotification(TripNotification tripNotification) {
        MongoCollection<TripNotification> collection = database.getCollection("TripNotification", TripNotification.class);
        Bson filter = Filters.eq("_id", tripNotification._id);
        collection.replaceOne(filter, tripNotification);
    }

    public List<PhoneZip> getByPhone(String phone) {
        Bson filter = Filters.in("phone", phone);
        return processListRequest(filter, null, 0, 0, PhoneZip.class);
    }

    public PromotionCode findByFleetIdAndPromoCode(String fleetId, String promoCode) {
        try{
            String promo = promoCode != null ? promoCode : "";
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("promotionCode", promo.toUpperCase())
            );
            return processRequest(filter, PromotionCode.class);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public PaxPromoList findByFleetIdAndPromoCodeIdAndUserId(String fleetId, String promoCodeId, String customerId) {
        try{
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("promoCodeId", promoCodeId),
                    Filters.eq("userId", customerId)
            );
            return processRequest(filter, PaxPromoList.class);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public void addPaxPromoList(PaxPromoList paxPromoList) {
        MongoCollection<PaxPromoList> collection = database.getCollection("PaxPromoList", PaxPromoList.class);
        InsertOneResult result = collection.insertOne(paxPromoList);
    }

    public void addReferralHistories(ReferralHistories histories) {
        MongoCollection<ReferralHistories> collection = database.getCollection("ReferralHistories", ReferralHistories.class);
        InsertOneResult result = collection.insertOne(histories);
    }

    public void removePaxPromoByPromoCodeId(String promoCodeId){
        MongoCollection<PaxPromoList> collection = database.getCollection("PaxPromoList", PaxPromoList.class);
        Bson filter = Filters.in("promoCodeId", promoCodeId);
        collection.deleteMany(filter);
    }

    public void deletePaxPromoList(String promoCodeId, String userId){
        MongoCollection<PaxPromoList> collection = database.getCollection("PaxPromoList", PaxPromoList.class);
        Bson filter = Filters.and(
                Filters.eq("promoCodeId", promoCodeId),
                Filters.eq("userId", userId)
        );
        collection.deleteMany(filter);
    }

    public Operations getOperation(String driverId) {
        try {
            Bson filter = Filters.in("driverId", driverId);
            return processRequest(filter, Operations.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public SolutionPartner findSolutionPartnerById (String SID) {
        try {
            Bson filter = Filters.eq("_id", new ObjectId(SID));
            return processRequest(filter, SolutionPartner.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public String addUpdateHistory(UpdateHistory updateHistory) {
        updateHistory._id = new ObjectId();
        MongoCollection<UpdateHistory> collection = database.getCollection("UpdateHistory", UpdateHistory.class);
        InsertOneResult result = collection.insertOne(updateHistory);
        return result.getInsertedId().asObjectId().getValue().toString();
    }

    public AdditionalServices findByServiceId(String serviceId) {
        try{
            Bson filter = Filters.eq("_id", new ObjectId(serviceId));
            return processRequest(filter, AdditionalServices.class);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public List<AdditionalServices> findByVehicleIdAndServiceType(String vehicleId, String serviceType) {
        List<AdditionalServices> additionalServices = new ArrayList<>();
        try{
            Bson filter = Filters.and(
                    Filters.in("vehicleType", vehicleId),
                    Filters.eq("serviceType", serviceType)
            );
            return processListRequest(filter, null, 0, 0, AdditionalServices.class);
        }catch (Exception e){
            e.printStackTrace();
            return additionalServices;
        }
    }

    public Boolean checkBinData(String cardNumber) {
        try {
            Bson filter1 = Filters.and(
                    Filters.eq("binLength", 5),
                    Filters.eq("binNumber", cardNumber.substring(0, 5))
            );
            Bson filter2 = Filters.and(
                    Filters.eq("binLength", 6),
                    Filters.eq("binNumber", cardNumber.substring(0, 6))
            );
            Bson filter3 = Filters.and(
                    Filters.eq("binLength", 7),
                    Filters.eq("binNumber", cardNumber.substring(0, 7))
            );
            Bson filterOr = Filters.or(filter1, filter2, filter3);
            Bson filterLength = Filters.eq("accountLength", cardNumber.length());
            Bson filter = Filters.and(filterLength, filterOr);
            UnionpayBin unionpayBin = processRequest(filter, UnionpayBin.class);
            return unionpayBin != null;
        } catch(Exception ex) {
            return false;
        }
    }

    public String getTokenFromAccount(String cardOwner, String id, String last4){
        if (cardOwner.equals("user") || cardOwner.equals("customer") || cardOwner.equals("passenger") || cardOwner.equals("mDispatcher")
                || cardOwner.equals("driver")) {
            Bson filter = Filters.eq("userId", id);
            Account account = processRequest(filter, Account.class);
            if (account != null && !account.credits.isEmpty()) {
                for (Credit credit : account.credits) {
                    if (credit.cardMask.substring(credit.cardMask.length() - 4).equals(last4))
                        return credit.localToken;
                }
            }
        }
        if (cardOwner.equals("corporate")){
            Bson filter = Filters.eq("_id", new ObjectId(id));
            Corporate account = processRequest(filter, Corporate.class);
            if (account != null && !account.credits.isEmpty()) {
                for (Credit credit : account.credits) {
                    if (credit.cardMask.substring(credit.cardMask.length() - 4).equals(last4))
                        return credit.localToken;
                }
            }
        }
        if (cardOwner.equals("fleetOwner")){
            Bson filter = Filters.eq("_id", new ObjectId(id));
            UsersSystem account = processRequest(filter, UsersSystem.class);
            if (account != null && !account.credits.isEmpty()) {
                for (Credit credit : account.credits) {
                    if (credit.cardMask.substring(credit.cardMask.length() - 4).equals(last4))
                        return credit.localToken;
                }
            }
        }
        return "";
    }

    public Fleet getFleetFromPayEaseMerchant(String merchantId){
        Bson filter = Filters.and(
                Filters.eq("paymentClearanceHouses.type", "ApplePay"),
                Filters.eq("paymentClearanceHouses.merchantId", merchantId)
        );
        return processRequest(filter, Fleet.class);
    }

    public void logPayEaseAsync(LogThirdParties logThirdParties) {
        MongoCollection<LogThirdParties> collection = database.getCollection("LogThirdParties", LogThirdParties.class);
        InsertOneResult result = collection.insertOne(logThirdParties);

    }

    public Zone getZoneById(String zoneId) {
        try{
            Bson filter = Filters.eq("_id", new ObjectId(zoneId));
            return processRequest(filter, Zone.class);
        }catch (Exception e){
            e.getMessage();
            return null;
        }
    }

    public ConfigGateway getSingleGateway(String fleetId) {
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("singleGateway", true),
                Filters.eq("isActive", true)
        );
        return processRequest(filter, ConfigGateway.class);
    }


    public ConfigGateway getGatewayByZone(String fleetId, String zoneId) {
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.in("zones", zoneId),
                Filters.eq("singleGateway", false),
                Filters.eq("isActive", true)
        );
        return processRequest(filter, ConfigGateway.class);
    }

    public boolean updatePaymentMethod(String bookId, int paymentType){
        MongoCollection<Booking> collection = database.getCollection("BookingCompleted", Booking.class);
        Bson filter = Filters.eq("bookId", bookId);
        Bson update = Updates.set("completedInfo.paymentType", paymentType);
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }


    public List<PhoneZip> getPhoneZipWithoutState(String country) {
        Bson filter = Filters.eq("country", country);
        return processListRequest(filter, null, 0, 0, PhoneZip.class);
    }

    public boolean updateDriverBalance(String userId, List<String> listCurrencies){
        MongoCollection<Operations> collection = database.getCollection("Operations", Operations.class);
        Bson filter = Filters.eq("driverId", userId);
        Bson update = Updates.combine(Updates.set("balanceCurrencies", listCurrencies), Updates.currentDate("latestUpdate"));
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public void addBookingStatistic(BookingStatistic bookingStatistic) {
        MongoCollection<BookingStatistic> collection = database.getCollection("BookingStatistic", BookingStatistic.class);
        InsertOneResult result = collection.insertOne(bookingStatistic);
    }

    public boolean updateCorporateInfo(String userId, Corporate corporate, String createdDate, int status){
        try {
            MongoCollection<Account> collection = database.getCollection("Account", Account.class);
            Bson filter = Filters.eq("userId", userId);
            Bson update = Updates.combine(
                    Updates.set("passengerInfo.corporateId", corporate._id.toString()),
                    Updates.set("passengerInfo.corporateName", corporate.companyInfo.name),
                    Updates.set("passengerInfo.corporateNameSort", corporate.companyInfo.name.toLowerCase()),
                    Updates.currentDate("passengerInfo.createdCorporate"),
                    Updates.set("corporateInfo.status", status));
            UpdateResult result = collection.updateMany(filter, update);
            return result.wasAcknowledged();
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public ReferralHistories getLinkReferralByRefereeId(String refereeId) {
        Bson filter = Filters.eq("refereeId", refereeId);
        return processRequest(filter, ReferralHistories.class);
    }

    public List<ReferralHistories> getLinkReferralByUserId(String userId) {
        Bson filter = Filters.and(
                Filters.eq("userId", userId),
                Filters.exists("firstBookingDate")
        );
        Bson sort = Sorts.ascending("firstBookingDate");
        return processListRequest(filter, sort, 0, 0, ReferralHistories.class);
    }

    public Referral getReferral(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, Referral.class);
    }

    public PaxReferral getPaxReferral(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, PaxReferral.class);
    }

    public ReferralInfo getReferralInfo(String fleetId, String userId) {
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("userId", userId)
        );
        return processRequest(filter, ReferralInfo.class);
    }

    public boolean updateReferralHistories(String refereeId, JSONObject updateObj){
        try {
            MongoCollection<ReferralHistories> collection = database.getCollection("ReferralHistories", ReferralHistories.class);
            Bson filter = Filters.eq("refereeId", refereeId);
            ReferralHistories referralHistories = processRequest(filter, ReferralHistories.class);
            if (referralHistories != null) {
                if (referralHistories.firstBookingDate == null) {
                    Bson update = Updates.combine(
                            Updates.set("firstBookingDate", KeysUtil.SDF_DMYHMSTZ.parse(updateObj.getString("firstBookingDate"))),
                            Updates.set("firstBookingDateGMT", KeysUtil.SDF_DMYHMSTZ.parse(updateObj.getString("firstBookingDateGMT"))),
                            Updates.set("firstBooking", updateObj.getString("firstBooking")),
                            Updates.set("firstBookingCurrency", updateObj.getString("firstBookingCurrency")),
                            Updates.set("firstEarning", updateObj.getDouble("firstEarning"))
                        );
                    UpdateResult result = collection.updateOne(filter, update);
                    return result.wasAcknowledged();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public AffiliateProcess findAffiliateProcessById(String id){
        try{
            Bson filter = Filters.eq("_id", new ObjectId(id));
            return processRequest(filter, AffiliateProcess.class);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public AffiliateProcess findAffiliateProcessTopByOrderByCreatedDateDesc(){
        try{
            MongoCollection<AffiliateProcess> collection = database.getCollection("AffiliateProcess", AffiliateProcess.class);
            Bson sorts = Sorts.ascending("createdDate");
            MongoCursor<AffiliateProcess> cursor = collection.find().sort(sorts).iterator();
            return cursor.next();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public boolean updateAffiliateStatus(String bookId, int status){
        MongoCollection<Booking> collection = database.getCollection("Booking", Booking.class);
        Bson filter = Filters.eq("bookId", bookId);
        Bson update = Updates.set("affiliateStatus", status);
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public boolean updatePromoCode(String bookId, String promoCode){
        MongoCollection<Booking> collectionBooking = database.getCollection("Booking", Booking.class);
        MongoCollection<Booking> collectionBookingCompleted = database.getCollection("BookingCompleted", Booking.class);
        Bson filter = Filters.eq("bookId", bookId);
        Bson update = Updates.set("request.promo", promoCode);
        UpdateResult result = collectionBooking.updateOne(filter, update);
        UpdateResult resultCompleted = collectionBookingCompleted.updateOne(filter, update);
        return resultCompleted.wasAcknowledged();
    }
    public boolean updateTotalBuy(String bookId, double value, String transactionStatus){
        MongoCollection<Booking> collection = database.getCollection("BookingCompleted", Booking.class);
        Bson filter = Filters.eq("bookId", bookId);
        Bson update = Updates.set("completedInfo.totalBuyFare", value);
        if(transactionStatus.equals("canceled") || transactionStatus.equals("noShow")){
            update = Updates.set("cancelInfo.policies.valueBuy", value);
        }
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public boolean updateAccountEmail(String userId, String email){
        MongoCollection<Account> collection = database.getCollection("Account", Account.class);
        Bson filter = Filters.eq("userId", userId);
        Bson update = Updates.set("email", email);
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public boolean updateBankStatus(String userId){
        MongoCollection<Account> collection = database.getCollection("Account", Account.class);
        Bson filter = Filters.eq("userId", userId);
        Bson update = Updates.set("driverInfo.isBankVerified", true);
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public boolean updateBankProfile(ACHEnt achEnt, boolean isVerified, String gateway) {
        MongoCollection<Account> collection = database.getCollection("Account", Account.class);
        Bson filter = Filters.eq("userId", achEnt.driverId);
        String token = achEnt.token != null ? achEnt.token : "";
        String bankName = achEnt.bankName != null ? achEnt.bankName : "";
        String firstName = achEnt.firstName != null ? achEnt.firstName : "";
        String lastName = achEnt.lastName != null ? achEnt.lastName : "";
        String accountName = firstName + " " + lastName;
        if (accountName.trim().isEmpty()) {
            accountName = achEnt.accountName != null ? achEnt.accountName : "";
        }
        String routingNumber = achEnt.routingNumber != null ? achEnt.routingNumber : "";
        String bankNumber = achEnt.bankNumber != null ? achEnt.bankNumber : "";
        String birthDay = achEnt.birthDay != null ? achEnt.birthDay : "";
        String address = achEnt.address != null ? achEnt.address : "";
        String city = achEnt.city != null ? achEnt.city : "";
        String state = achEnt.state != null ? achEnt.state : "";
        String zipCode = achEnt.zipCode != null ? achEnt.zipCode : "";
        String ssn = achEnt.ssn != null ? achEnt.ssn : "";
        String checkNumber = achEnt.checkNumber != null ? achEnt.checkNumber : "";
        String verificationDocument = achEnt.verificationDocument != null ? achEnt.verificationDocument : "";
        String additionalDocument = achEnt.additionalDocument != null ? achEnt.additionalDocument : "";
        String legalToken = achEnt.legalToken != null ? achEnt.legalToken : "";
        String driverIc = achEnt.driverIc != null ? achEnt.driverIc : "";
        String idType = achEnt.idType != null ? achEnt.idType : "";
        boolean isBankAccountOwner = achEnt.isBankAccountOwner;
        String beneficiaryIDIC = achEnt.beneficiaryIDIC != null ? achEnt.beneficiaryIDIC : "";
        int bankRelationship = achEnt.bankRelationship;
        String relationshipOtherName = achEnt.relationshipOtherName != null ? achEnt.relationshipOtherName : "";
        String sortCode = achEnt.sortCode != null ? achEnt.sortCode : "";
        String IFSCCode = achEnt.IFSCCode != null ? achEnt.IFSCCode : "";
        String verificationDocumentBack = achEnt.verificationDocumentBack != null ? achEnt.verificationDocumentBack : "";
        String additionalDocumentBack = achEnt.additionalDocumentBack != null ? achEnt.additionalDocumentBack : "";
        String frontFileId = achEnt.frontFileId != null ? achEnt.frontFileId : "";
        String frontAddFileId = achEnt.frontAddFileId != null ? achEnt.frontAddFileId : "";
        String backFileId = achEnt.backFileId != null ? achEnt.backFileId : "";
        String backAddFileId = achEnt.backAddFileId != null ? achEnt.backAddFileId : "";
        boolean verifyStatus = achEnt.verifyStatus;
        Bson update;
        if (gateway.equals(KeysUtil.STRIPE) && isVerified) {
            update = Updates.combine(
                    Updates.set("driverInfo.dateOfBirth", birthDay),
                    Updates.set("driverInfo.bankAddress", address),
                    Updates.set("driverInfo.bankCity", city),
                    Updates.set("driverInfo.bankState", state),
                    Updates.set("driverInfo.bankZip", zipCode)
                );
        } else {
            update = Updates.combine(
                    Updates.set("driverInfo.achToken", token),
                    Updates.set("driverInfo.nameOfBank", bankName),
                    Updates.set("driverInfo.nameOfAccount", accountName),
                    Updates.set("driverInfo.routingNumber", routingNumber),
                    Updates.set("driverInfo.accountNumber", bankNumber),
                    Updates.set("driverInfo.dateOfBirth", birthDay),
                    Updates.set("driverInfo.bankAddress", address),
                    Updates.set("driverInfo.bankCity", city),
                    Updates.set("driverInfo.bankState", state),
                    Updates.set("driverInfo.bankZip", zipCode),
                    Updates.set("driverInfo.ssn", ssn),
                    Updates.set("driverInfo.checkNumber", checkNumber),
                    Updates.set("driverInfo.legalToken", legalToken),
                    Updates.set("driverInfo.drvId", driverIc),
                    Updates.set("driverInfo.idType", idType),
                    Updates.set("driverInfo.isBankAccountOwner", isBankAccountOwner),
                    Updates.set("driverInfo.beneficiaryIDIC", beneficiaryIDIC),
                    Updates.set("driverInfo.bankRelationship", bankRelationship),
                    Updates.set("driverInfo.relationshipOtherName", relationshipOtherName),
                    Updates.set("driverInfo.sortCode", sortCode),
                    Updates.set("driverInfo.IFSCCode", IFSCCode),
                    Updates.set("driverInfo.verificationDocument", verificationDocument),
                    Updates.set("driverInfo.additionalDocument", additionalDocument),
                    Updates.set("driverInfo.verificationDocumentBack", verificationDocumentBack),
                    Updates.set("driverInfo.additionalDocumentBack", additionalDocumentBack),
                    Updates.set("driverInfo.frontFileId", frontFileId),
                    Updates.set("driverInfo.frontAddFileId", frontAddFileId),
                    Updates.set("driverInfo.backFileId", backFileId),
                    Updates.set("driverInfo.backAddFileId", backAddFileId),
                    Updates.set("driverInfo.isBankVerified", verifyStatus)
                );
        }
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public boolean removeBankProfile(String driverId) {
        MongoCollection<Account> collection = database.getCollection("Account", Account.class);
        Bson filter = Filters.eq("userId", driverId);
        Bson update = Updates.combine(
                Updates.set("driverInfo.achToken", ""),
                Updates.set("driverInfo.nameOfBank", ""),
                Updates.set("driverInfo.nameOfAccount", ""),
                Updates.set("driverInfo.routingNumber", ""),
                Updates.set("driverInfo.accountNumber", ""),
                Updates.set("driverInfo.ssn", ""),
                Updates.set("driverInfo.checkNumber", ""),
                Updates.set("driverInfo.verificationDocument", ""),
                Updates.set("driverInfo.additionalDocument", ""),
                Updates.set("driverInfo.legalToken", ""),
                Updates.set("driverInfo.isBankVerified", false)
            );
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public DriverWallet getDriverWallet(String userId) {
        Bson filter = Filters.eq("userId", userId);
        try {
            return processRequest(filter, DriverWallet.class);
        } catch(Exception ex) {
            System.out.println("==== Error mapping DriverWallet for userId " + userId);
            try {
                MongoCollection<DriverWallet> collection = database.getCollection("DriverWallet", DriverWallet.class);
                MongoCollection<DriverWalletError> collectionError = database.getCollection("DriverWallet", DriverWalletError.class);
                DriverWalletError errorObject = collectionError.find(filter).first();
                DriverWallet driverWallet = new DriverWallet();
                driverWallet.fleetId = errorObject.fleetId;
                driverWallet.userId = errorObject.userId;

                if (errorObject.cashWallet != null) {
                    AmountByCurrency creditWallet = new AmountByCurrency();
                    creditWallet.value = 0.0;
                    creditWallet.currencyISO = errorObject.cashWallet.currencyISO;
                    driverWallet.creditWallet = creditWallet;
                    driverWallet.cashWallet = errorObject.cashWallet;

                    // update to repair data model
                    Bson update = Updates.set("creditWallet", creditWallet);
                    collection.updateOne(filter, update);
                } else {
                    Fleet fleet = getFleetInfor(errorObject.fleetId);
                    com.qupworld.paymentgateway.models.mongo.collections.fleet.Currency currency = fleet.currencies.get(0);
                    AmountByCurrency creditWallet = new AmountByCurrency();
                    creditWallet.value = 0.0;
                    creditWallet.currencyISO = currency.iso;
                    driverWallet.creditWallet = creditWallet;

                    BalanceByCurrency cashWallet = new BalanceByCurrency();
                    cashWallet.currentBalance = 0.0;
                    cashWallet.pendingBalance = 0.0;
                    cashWallet.availableBalance = 0.0;
                    cashWallet.currencyISO = currency.iso;
                    driverWallet.cashWallet = cashWallet;

                    // update to repair data model
                    Bson updateCredit = Updates.set("creditWallet", creditWallet);
                    Bson updateCash = Updates.set("cashWallet", cashWallet);
                    collection.updateOne(filter, updateCredit);
                    collection.updateOne(filter, updateCash);
                }
                // return correct data
                return driverWallet;
            } catch (Exception e) {
                return null;
            }
        }
    }

    public boolean updateCashWallet(String userId, double currentBalance, double pendingBalance, String currencyISO) {
        MongoCollection<DriverWallet> collection = database.getCollection("DriverWallet", DriverWallet.class);
        Bson filter = Filters.eq("userId", userId);
        Bson update = Updates.combine(
                Updates.set("cashWallet.currentBalance", currentBalance),
                Updates.set("cashWallet.pendingBalance", pendingBalance),
                Updates.set("cashWallet.currencyISO", currencyISO),
                Updates.currentDate("latestUpdate"));
        if (pendingBalance == -1) {
            update = Updates.combine(
                    Updates.set("cashWallet.currentBalance", currentBalance),
                    Updates.set("cashWallet.currencyISO", currencyISO),
                    Updates.currentDate("latestUpdate"));
        }
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public boolean updateCreditWallet(String userId, List<AmountByCurrency> creditBalances, AmountByCurrency creditWallet) {
        MongoCollection<Account> collectionAccount = database.getCollection("Account", Account.class);
        Bson filter = Filters.eq("userId", userId);
        Bson updateAccount = Updates.combine(
                Updates.set("driverInfo.creditBalances", creditBalances),
                Updates.currentDate("latestUpdate"));
        UpdateResult result = collectionAccount.updateOne(filter, updateAccount);
        if (result.wasAcknowledged()) {
            MongoCollection<DriverWallet> collectionWallet = database.getCollection("DriverWallet", DriverWallet.class);
            Bson updateWallet = Updates.combine(
                    Updates.set("creditWallet", creditWallet),
                    Updates.currentDate("latestUpdate"));
            UpdateResult resultWallet = collectionWallet.updateOne(filter, updateWallet);
            return resultWallet.wasAcknowledged();
        } else {
            return result.wasAcknowledged();
        }
    }

    public void addDriverWallet(DriverWallet driverWallet) {
        MongoCollection<DriverWallet> collection = database.getCollection("DriverWallet", DriverWallet.class);
        try {
            collection.insertOne(driverWallet);
        } catch (Exception ignore) {}

    }

    public void updateDriverWallet(String userId, AmountByCurrency creditWallet) {
        MongoCollection<DriverWallet> collection = database.getCollection("DriverWallet", DriverWallet.class);
        Bson filter = Filters.eq("userId", userId);
        Bson update = Updates.combine(
                Updates.set("creditWallet", creditWallet),
                Updates.currentDate("latestUpdate"));
        UpdateResult result = collection.updateOne(filter, update);
    }

    public ServiceFee getServiceFeeByFleetIdAndZoneId(String fleetId, String zoneId){
        try {
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("zoneId", zoneId)
            );
            return processRequest(filter, ServiceFee.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public DynamicSurcharge getDynamicSurcharge(String fleetId, String zoneId, List<Double> pickup){
        try {
            Point geometry = new Point(new Position(pickup.get(0),pickup.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("zoneId", zoneId),
                    Filters.geoIntersects("geo", geometry)
            );
            return processRequest(filter, DynamicSurcharge.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public DynamicFare getDynamicFare(String fleetId, String zoneId, List<Double> pickup){
        try {
            Point geometry = new Point(new Position(pickup.get(0),pickup.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("zoneId", zoneId),
                    Filters.geoIntersects("geo", geometry)
            );
            return processRequest(filter, DynamicFare.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public PassengerInfo getPassengerInfo(String fleetId, String userId) {
        try {
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("userId", userId)
            );
            return processRequest(filter, PassengerInfo.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public void updatePaxWallet(String fleetId, String userId, List<AmountByCurrency> wallets) {
        try {
            MongoCollection<PassengerInfo> collection = database.getCollection("PassengerInfo", PassengerInfo.class);
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("userId", userId)
            );
            Bson update = Updates.combine(
                    Updates.set("paxWallet", wallets),
                    Updates.currentDate("latestUpdate"));

            UpdateResult result = collection.updateOne(filter, update);
        } catch(Exception ex) {
            logger.debug("updatePaxWallet error: " + CommonUtils.getError(ex));
        }
    }

    public void addPassengerInfo(PassengerInfo passengerInfo) {
        MongoCollection<PassengerInfo> collection = database.getCollection("PassengerInfo", PassengerInfo.class);
        try {
            collection.insertOne(passengerInfo);
        } catch (Exception ignore) {}

    }

    public void updateOustanding(String fleetId, String userId, List<OutStanding> outStandings) {
        try {
            MongoCollection<PassengerInfo> collection = database.getCollection("PassengerInfo", PassengerInfo.class);
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("userId", userId)
            );
            Bson update = Updates.combine(
                    Updates.set("outStanding", outStandings),
                    Updates.currentDate("latestUpdate"));

            UpdateResult result = collection.updateOne(filter, update);
        } catch(Exception ignore) {}
    }

    public AirportFee getAirportFee(String fleetId, String airportZoneId){
        try {
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("airportZoneId", airportZoneId)
            );
            return processRequest(filter, AirportFee.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public List<String> getAllFleetIds() {
        List<String> listIds = new ArrayList<>();
        try {
            Bson filter = Filters.and(
                    Filters.eq("isActive", true),
                    Filters.eq("demoInfo.demoFleet", true)
            );
            Bson projections = Projections.fields(Projections.include("fleetId"), Projections.excludeId());
            MongoCollection<Document> collection = database.getCollection("Fleet", Document.class);

            MongoCursor<Document> cursor = collection.find(filter).projection(projections).iterator();
            while(cursor.hasNext()) {
                Fleet doc = gson.fromJson(gson.toJson(cursor.next()), Fleet.class);
                listIds.add(doc.fleetId);
            }
        } catch(Exception ignore) {}
        return listIds;
    }

    public CorpItinerary getCorpItinerary(String itineraryId) {
        try {
            Bson filter = Filters.eq("itineraryId", itineraryId);
            return processRequest(filter, CorpItinerary.class);
        } catch(Exception ex) {
            return null;
        }
    }

    //for update affiliate rate
    public List<RateRegular> getAllAffiliateRegularByPriceType(String priceType){
        MongoCollection<Document> collection = database.getCollection("AffiliateRate", Document.class);
        Bson filter = Filters.and(
                Filters.eq("rateInfo.priceType", priceType),
                Filters.eq("type", "Regular")
        );
        MongoCursor<Document> cursor = collection.find(filter).iterator();
        List<RateRegular> listData = new ArrayList<>();
        while(cursor.hasNext()) {
            listData.add(gson.fromJson(gson.toJson(cursor.next()), RateRegular.class));
        }
        return listData;
    }

    public void updateAffiliateRegular(RateRegular regular){
        MongoCollection<RateRegular> collection = database.getCollection("AffiliateRate", RateRegular.class);
        Bson filter = Filters.eq("_id", regular._id);
        collection.replaceOne(filter, regular);
    }

    public List<AffiliateFlat> getAllAffiliateFlatByPriceType(String priceType){
        Bson filter = Filters.and(
                Filters.eq("rateInfo.priceType", priceType),
                Filters.eq("type", "Flat")
        );
        return processListRequest(filter, null, 0, 0, AffiliateFlat.class);
    }

    public List<AffiliateRoute> getAllAffiliateRouteByFlatId(String flatId){
        Bson filter = Filters.eq("flatRateId", flatId);
        return processListRequest(filter, null, 0, 0, AffiliateRoute.class);
    }

    public void updateAffiliateRoute(AffiliateRoute route){
        MongoCollection<AffiliateRoute> collection = database.getCollection("AffiliateRoute", AffiliateRoute.class);
        Bson filter = Filters.eq("_id", route._id);
        collection.replaceOne(filter, route);
    }

    public List<String> getListDriverId(String fleetId, List<String> listZone) {
        MongoCollection<Document> collection = database.getCollection("Account", Document.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("appType", "driver"),
                Filters.eq("isActive", true),
                Filters.in("driverInfo.zoneId", listZone)
        );
        Bson sort = Sorts.ascending("createdDate");
        Bson projections = Projections.fields(Projections.include("userId"), Projections.excludeId());
        int i = 0;
        int size = 50;
        List<String> listAccount = new ArrayList<>();
        while (i >= 0) {
            MongoCursor<Document> cursor = collection.find(filter).skip(i*size).limit(size).sort(sort).projection(projections).iterator();
            while(cursor.hasNext()) {
                Account account = gson.fromJson(gson.toJson(cursor.next()), Account.class);
                listAccount.add(account.userId);
            }
            if (listAccount.size() == size || listAccount.size() == (i+1)*size) {
                i++;
            } else {
                i = -1;
            }
        }

        return listAccount;
    }

    public List<UpdateBalanceId> getIdUpdateBalance(String key) {
        Bson filter = Filters.eq("key", key);
        return processListRequest(filter, null , 0, 0, UpdateBalanceId.class);
    }

    public void addUpdateBalanceId(UpdateBalanceId updateBalanceId) {
        MongoCollection<UpdateBalanceId> collection = database.getCollection("UpdateBalanceId", UpdateBalanceId.class);
        try {
            collection.insertOne(updateBalanceId);
        } catch (Exception ignore) {}
    }

    public void removeUpdateBalanceId(String key, String driverId){
        MongoCollection<UpdateBalanceId> collection = database.getCollection("UpdateBalanceId", UpdateBalanceId.class);
        Bson filter = Filters.and(
                Filters.eq("key", key),
                Filters.eq("driverId", driverId)
        );
        try {
            collection.deleteOne(filter);
        } catch (Exception ignore) {}
    }

    public List<String> getListZone(String fleetId, String currencyISO) {
        MongoCollection<Document> collection = database.getCollection("Zone", Document.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("currency.iso", currencyISO)
        );
        MongoCursor<Document> cursor = collection.find(filter).iterator();
        List<String> listData = new ArrayList<>();
        while(cursor.hasNext()) {
            Zone item = gson.fromJson(gson.toJson(cursor.next()), Zone.class);
            listData.add(item._id.toString());
        }
        return listData;
    }

    public List<BankInfo> getBankInfo(String gateway, String environment, String country) {
        Bson filter = Filters.and(
                Filters.eq("gateway", gateway),
                Filters.eq("environment", environment),
                Filters.eq("country", country)
        );
        Bson sorts = Sorts.ascending("bankName");
        return processListRequest(filter, sorts, 0, 0, BankInfo.class);
    }

    public List<BankInfo> getBankInfo(String gateway, String country) {
        Bson filter = Filters.and(
                Filters.eq("gateway", gateway),
                Filters.eq("country", country)
        );
        Bson sorts = Sorts.ascending("bankName");
        return processListRequest(filter, sorts, 0, 0, BankInfo.class);
    }

    public BankInfo getBankInfoByBankCode(String gateway, String bankCode, String country) {
        try {
            Bson filter = Filters.and(
                    Filters.eq("gateway", gateway),
                    Filters.eq("bankCode", bankCode),
                    Filters.eq("country", country)
            );
            return processRequest(filter, BankInfo.class);
        } catch(Exception ex) {
            return null;
        }
    }
    public Process getSettingPayment (String fleetId){
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, Process.class);
    }

    public GatewayFirstData getGatewayFirstData(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayFirstData.class);
    }

    public List<DriverWallet> getAllDriverWallets(int count, int size) {
        Bson filter = Filters.empty();
        return processListRequest(filter, null, count*size, size, DriverWallet.class);
    }

    public Trip getCorpIntercityTrip(String tripId) {
        try {
            Bson filter = Filters.eq("tripId", tripId);
            return processRequest(filter, Trip.class);
        } catch(Exception ex) {
            return null;
        }
    }

    public List<DriverWallet> getCashWalletOfDriver(String fleetId) {
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.gt("cashWallet.currentBalance", 0.0)
        );
        return processListRequest(filter, null, 0, 0, DriverWallet.class);
    }

    public GatewayURL getGatewayURL(String gateway, String environment) {
        Bson filter = Filters.and(
                Filters.eq("gateway", gateway),
                Filters.eq("environment", environment)
        );
        return processRequest(filter, GatewayURL.class);
    }

    public List<String> getDriverWithCreditBalanceLessThan(String fleetId, double balance, int from, int size){
        List<String> driverIds = new ArrayList<>();
        try {
            MongoCollection<Document> collection = database.getCollection("Account", Document.class);
            Bson filterOr = Filters.or(
                    Filters.exists("driverInfo.creditBalances", false),
                    Filters.size("driverInfo.creditBalances", 0),
                    Filters.lt("driverInfo.creditBalances.value", balance)
            );
            Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("appType", "driver"),
                Filters.eq("isActive", true),
                filterOr
            );
            Bson projections = Projections.fields(Projections.include("userId"), Projections.excludeId());
            MongoCursor<Document> cursor = collection.find(filter).skip(from).limit(size).projection(projections).iterator();
            while(cursor.hasNext()) {
                Account item = gson.fromJson(gson.toJson(cursor.next()), Account.class);
                driverIds.add(item.userId);
            }
        } catch(Exception ignore) {}
        return driverIds;
    }

    public Account getAccountFromBoostId(String fleetId, String boostId, String type) {
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("boostId", boostId),
                Filters.eq("appType", type)
                );
        return processRequest(filter, Account.class);
    }

    public Booking getActiveBookingFromBoostId(String fleetId, String boostId) {
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("psgInfo.boostId", boostId)
        );
        return processRequest(filter, Booking.class);
    }

    public String addPayoutTime(PayoutTime payoutTime) {
        MongoCollection<PayoutTime> collection = database.getCollection("PayoutTime", PayoutTime.class);
        payoutTime._id = new ObjectId();
        collection.insertOne(payoutTime);
        return payoutTime._id.toString();
    }

    public PayoutTime getPayoutTime(String fleetId) {
        try {
            Bson filter = Filters.eq("fleetId", fleetId);
            Bson sort = Sorts.descending("payoutTime");
            return processListRequest(filter, sort, 0, 0, PayoutTime.class).get(0);
        } catch (Exception ex) {
            return null;
        }
    }

    public org.json.simple.JSONObject getListDriver(String fleetId, String gateway, String companyId, String payoutOption, double minAmount, String currencyISO, int skip, int size) {
        MongoCollection<Document> collection = database.getCollection("Account", Document.class);

        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("appType", "driver"),
                Filters.eq("isActive", true)
        );

        if (!companyId.equals("all")) {
            filter = Filters.and(
                    filter,
                    Filters.eq("driverInfo.company.companyId", companyId)
            );
        }

        if (payoutOption.equals("invalidBank")) {
            filter = Filters.and(
                    filter,
                    Filters.eq("driverInfo.invalidBank", true)
            );
        } else {
            List<String> requireRouting = Arrays.asList(KeysUtil.MOLPAY);
            List<String> requireBankName = Arrays.asList(KeysUtil.ANY_BANK, KeysUtil.INDIA_DEFAULT);
            if (payoutOption.equals("hasBank")) {
                Bson invalidBank = Filters.or(
                        Filters.exists("driverInfo.invalidBank", false),
                        Filters.eq("driverInfo.invalidBank", false)
                );
                Bson nameOfAccount = Filters.and(
                        Filters.exists("driverInfo.nameOfAccount", true),
                        Filters.ne("driverInfo.nameOfAccount", "")
                );
                Bson accountNumber = Filters.and(
                        Filters.exists("driverInfo.accountNumber", true),
                        Filters.ne("driverInfo.accountNumber", "")
                );
                filter = Filters.and(filter, invalidBank, nameOfAccount, accountNumber);

                if (requireRouting.contains(gateway)) {
                    Bson routingNumber = Filters.and(
                            Filters.exists("driverInfo.routingNumber", true),
                            Filters.ne("driverInfo.routingNumber", "")
                    );
                    filter = Filters.and(filter, routingNumber);
                }

                if (requireBankName.contains(gateway)) {
                    Bson nameOfBank = Filters.and(
                            Filters.exists("driverInfo.nameOfBank", true),
                            Filters.ne("driverInfo.nameOfBank", "")
                    );
                    filter = Filters.and(filter, nameOfBank);
                }
                if (gateway.equals(KeysUtil.INDIA_DEFAULT)) {
                    Bson IFSCCode = Filters.and(
                            Filters.exists("driverInfo.IFSCCode", true),
                            Filters.ne("driverInfo.IFSCCode", "")
                    );
                    filter = Filters.and(filter, IFSCCode);
                }
            } else if (payoutOption.equals("noBank")) {
                Bson noBank = Filters.or(
                        Filters.exists("driverInfo.nameOfAccount", false),
                        Filters.eq("driverInfo.nameOfAccount", ""),
                        Filters.exists("driverInfo.accountNumber", false),
                        Filters.eq("driverInfo.accountNumber", "")
                );
                if (requireRouting.contains(gateway)) {
                    Bson routingNumber = Filters.or(
                            Filters.exists("driverInfo.routingNumber", false),
                            Filters.eq("driverInfo.routingNumber", "")
                    );
                    noBank = Filters.or(noBank, routingNumber);
                }
                if (requireBankName.contains(gateway)) {
                    Bson nameOfBank = Filters.or(
                            Filters.exists("driverInfo.nameOfBank", false),
                            Filters.eq("driverInfo.nameOfBank", "")
                    );
                    noBank = Filters.or(noBank, nameOfBank);
                }
                if (gateway.equals(KeysUtil.INDIA_DEFAULT)) {
                    Bson IFSCCode = Filters.or(
                            Filters.exists("driverInfo.IFSCCode", false),
                            Filters.eq("driverInfo.IFSCCode", "")
                    );
                    noBank = Filters.or(noBank, IFSCCode);
                }
                filter = Filters.and(filter, noBank);
            }
        }
        Bson sort = Sorts.ascending("createdDate");
        Bson projections = Projections.fields(Projections.include("userId"), Projections.excludeId());
        MongoCursor<Document> cursor = collection.find(filter).skip(skip).limit(size).projection(projections).sort(sort).iterator();
        List<String> listDriverId = new ArrayList<>();
        while(cursor.hasNext()) {
            Account item = gson.fromJson(gson.toJson(cursor.next()), Account.class);
            listDriverId.add(item.userId);
        }
        return returnListDriver(fleetId, listDriverId, currencyISO);
    }

    private org.json.simple.JSONObject returnListDriver(String fleetId, List<String> listDriverId, String currencyISO) {
        // get cash balance of all driver in the list
        // for some driver who did not initiate DriverWallet, so need to manually add to the arrDriver list
        // initiate list driver who already has credit wallet
        List<String> driverHasDriverWallet = new ArrayList<>();
        MongoCollection<Document> collection = database.getCollection("DriverWallet", Document.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.in("userId", listDriverId),
                Filters.eq("cashWallet.currencyISO", currencyISO)
        );
        Bson projections = Projections.fields(Projections.include("userId", "cashWallet.currentBalance"), Projections.excludeId());
        MongoCursor<Document> cursor = collection.find(filter).projection(projections).iterator();
        JSONArray arrDriver = new JSONArray();
        while(cursor.hasNext()) {
            DriverWallet item = gson.fromJson(gson.toJson(cursor.next()), DriverWallet.class);
            driverHasDriverWallet.add(item.userId);
            org.json.simple.JSONObject objData = new org.json.simple.JSONObject();
            objData.put("driverId", item.userId);
            objData.put("cashBalance", item.cashWallet.currentBalance);
            arrDriver.add(objData);
        }
        if (listDriverId.size() > driverHasDriverWallet.size()) {
            // there are some driver did not initiate DriverWallet
            // add a default value
            for (String driverId : listDriverId) {
                if (!driverHasDriverWallet.contains(driverId)) {
                    org.json.simple.JSONObject objData = new org.json.simple.JSONObject();
                    objData.put("driverId", driverId);
                    objData.put("cashBalance", 0.0);
                    arrDriver.add(objData);
                }
            }
        }

        org.json.simple.JSONObject objReturn = new org.json.simple.JSONObject();
        objReturn.put("size", listDriverId.size());
        objReturn.put("data", arrDriver);
        return objReturn;
    }

    public void addLogs(Logs logs) {
        MongoCollection<Logs> collection = database.getCollection("Logs", Logs.class);
        try {
            collection.insertOne(logs);
        } catch (Exception ignore) {}

    }

    public List<String> getActiveDrivers(String fleetId, boolean delivery, int skip, int size) {
        List<String> listType = Arrays.asList("Delivery by Car", "Delivery by Bike");
        MongoCollection<Document> collection = database.getCollection("Account", Document.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("appType", "driver"),
                Filters.nin("driverInfo.carType.name", listType),
                Filters.eq("isActive", true)
        );
        if (delivery) {
            filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("appType", "driver"),
                    Filters.in("driverInfo.carType.name", listType),
                    Filters.eq("isActive", true)
            );
        }
        Bson sort = Sorts.descending("createdDate");
        Bson projections = Projections.fields(Projections.include("userId"), Projections.exclude("_id"));
        MongoCursor<Document> cursor = collection.find(filter).skip(skip).limit(size).sort(sort).projection(projections).iterator();
        List<String> listData = new ArrayList<>();
        while(cursor.hasNext()) {
            Account item = gson.fromJson(gson.toJson(cursor.next()), Account.class);
            listData.add(item.userId);
        }
        return listData;
    }

    public List<String> getActiveTransportDrivers(String fleetId, int skip, int size) {
        MongoCollection<Document> collection = database.getCollection("Account", Document.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("appType", "driver"),
                Filters.eq("driverInfo.driverType", "Fleet Owner"),
                Filters.eq("isActive", true)
        );
        Bson sort = Sorts.descending("createdDate");
        Bson projections = Projections.fields(Projections.include("userId"), Projections.exclude("_id"));
        MongoCursor<Document> cursor = collection.find(filter).skip(skip).limit(size).sort(sort).projection(projections).iterator();
        List<String> listData = new ArrayList<>();
        while(cursor.hasNext()) {
            Account item = gson.fromJson(gson.toJson(cursor.next()), Account.class);
            listData.add(item.userId);
        }
        return listData;
    }

    public boolean updateBankStatus(String requestId, String fleetId, String userId, boolean status) {
        try {
            MongoCollection<Account> collection = database.getCollection("Account", Account.class);
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("userId", userId)
            );
            Bson update = Updates.combine(
                    Updates.set("driverInfo.invalidBank", status),
                    Updates.currentDate("latestUpdate")
            );
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        } catch (Exception ex) {
            logger.debug(requestId + " - updateBankStatus error: " + CommonUtils.getError(ex));
            return false;
        }
    }

    public VoucherCode getVoucherCode(String fleetId, String voucherCode) {
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("voucherCode", voucherCode)
        );
        return processRequest(filter, VoucherCode.class);
    }

    public boolean updateVoucherCodeExpired() {
        MongoCollection<VoucherCode> collection = database.getCollection("VoucherCode", VoucherCode.class);
        Bson filter = Filters.and(
                Filters.eq("isUsed", ""),
                Filters.eq("isActive", true),
                Filters.lte("validTo", TimezoneUtil.offsetTimeZone(new Date(), Calendar.getInstance().getTimeZone().getID(), "GMT"))
        );

        Bson update = Updates.combine(
                Updates.set("isUsed", "expired"),
                Updates.currentDate("latestUpdate")
        );
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public boolean updateVoucherCode(String fleetId, String voucherCode, String status) {
        MongoCollection<VoucherCode> collection = database.getCollection("VoucherCode", VoucherCode.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("voucherCode", voucherCode)
        );
        Bson update = Updates.combine(
                Updates.set("isUsed", status),
                Updates.currentDate("latestUpdate")
        );
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public List<String> getAllActiveDrivers(String fleetId, int skip, int size) {
        MongoCollection<Document> collection = database.getCollection("Account", Document.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("appType", "driver"),
                Filters.eq("isActive", true)
        );
        Bson sort = Sorts.descending("createdDate");
        Bson projections = Projections.fields(Projections.include("userId"), Projections.exclude("_id"));
        MongoCursor<Document> cursor = collection.find(filter).skip(skip).limit(size).sort(sort).projection(projections).iterator();
        List<String> listData = new ArrayList<>();
        while(cursor.hasNext()) {
            Account item = gson.fromJson(gson.toJson(cursor.next()), Account.class);
            listData.add(item.userId);
        }
        return listData;
    }

    public ConfigWallet getConfigWallet(String fleetId, String gateway) {
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("gateway", gateway)
        );
        return processRequest(filter, ConfigWallet.class);
    }

    public PaymentChannel getPaymentChannel(String fleetId, String gateway, String type, String channel) {
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("gateway", gateway),
                Filters.eq("type", type),
                Filters.eq("channel", channel)
        );
        return processRequest(filter, PaymentChannel.class);
    }

    public ConfigWallet getPaymentChannel(String fleetId, String gateway) {
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("gateway", gateway),
                Filters.eq("isActive", true)
        );
        return processRequest(filter, ConfigWallet.class);
    }

    public WalletMOLPay getWalletMOLPay(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, WalletMOLPay.class);
    }

    public WalletGCash getWalletGCash(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, WalletGCash.class);
    }

    public WalletTnG getWalletTnG(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, WalletTnG.class);
    }

    public WalletBoost getWalletBoost(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, WalletBoost.class);
    }

    public WalletMoMo getWalletMoMo(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, WalletMoMo.class);
    }

    public WalletVipps getWalletVipps(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, WalletVipps.class);
    }

    public MenuMerchant getMerchant(String merchantId){
        Bson filter = Filters.eq("_id", new ObjectId(merchantId));
        return processRequest(filter, MenuMerchant.class);
    }

    public GatewayDNB getGatewayDNB(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayDNB.class);
    }

    public CCPackages getCCPackages(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, CCPackages.class);
    }

    public List<String> getListMerchant(String fleetId, String gateway, String payoutOption, String currencyISO, int skip, int size) {
        MongoCollection<Document> collection = database.getCollection("MenuMerchant", Document.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("available", true),
                Filters.eq("isPreferred", true)
        );
        if (payoutOption.equals("invalidBank")) {
            filter = Filters.and(
                    filter,
                    Filters.eq("bankInfo.invalidBank", true)
            );
        } else {
            if (gateway.equals(KeysUtil.MOLPAY)) {
                if (payoutOption.equals("hasBank")) {
                    /*"bankInfo" : {
                        "accountHolder" : "QA Test",
                        "accountNumber" : "123456789",
                        "bankName" : "BOFAMY2X",
                        "IDnumber" : "12346548971",
                        "IDtype" : "OLDNRIC",
                        "bankCode" : "PEMBMYK1"
                    },*/
                    Bson validBank = Filters.or(
                            Filters.exists("bankInfo.invalidBank", false),
                            Filters.eq("bankInfo.invalidBank", false)
                    );
                    Bson nameOfAccount = Filters.and(
                            Filters.exists("bankInfo.accountHolder", true),
                            Filters.ne("bankInfo.accountHolder", "")
                    );
                    Bson accountNumber = Filters.and(
                            Filters.exists("bankInfo.accountNumber", true),
                            Filters.ne("bankInfo.accountNumber", "")
                    );
                    Bson IDnumber = Filters.and(
                            Filters.exists("bankInfo.IDnumber", true),
                            Filters.ne("bankInfo.IDnumber", "")
                    );
                    Bson routingNumber = Filters.and(
                            Filters.exists("bankInfo.bankCode", true),
                            Filters.ne("bankInfo.bankCode", "")
                    );
                    filter = Filters.and(filter, validBank, nameOfAccount, accountNumber, IDnumber, routingNumber);
                } else if (payoutOption.equals("noBank")) {
                    Bson noBank = Filters.or(
                            Filters.exists("bankInfo.accountHolder", false),
                            Filters.eq("bankInfo.accountHolder", ""),
                            Filters.exists("bankInfo.accountNumber", false),
                            Filters.eq("bankInfo.accountNumber", ""),
                            Filters.exists("bankInfo.IDnumber", false),
                            Filters.eq("bankInfo.IDnumber", ""),
                            Filters.exists("bankInfo.bankCode", false),
                            Filters.eq("bankInfo.accountHolder", "")
                    );
                    filter = Filters.and(filter, noBank);
                }
            }
            else if (gateway.equals(KeysUtil.STRIPE)) {
                if (payoutOption.equals("hasBank")) {
                    /*"bankInfo" : {
                        "accountHolder" : "Texas 06",
                        "accountNumber" : "000123456789",
                        "address" : "1033 Young St, Dallas, TX 75202, Hoa Kỳ",
                        "bankToken" : "acct_1JI3T04hTKRJ0leK",
                        "birthDay" : "05/02/1995",
                        "city" : "Dallas",
                        "isBankAccountOwner" : true,
                        "postalCode" : "75202",
                        "routingNumber" : "110000000",
                        "ssn" : "123456789",
                        "state" : "TX",
                        "verificationDocumentBack" : "https://lab-gojo.s3.ap-southeast-1.amazonaws.com/images/gojotexas/merchant/1627442954861_untitle_2_d_png",
                        "verificationDocumentFront" : "https://lab-gojo.s3.ap-southeast-1.amazonaws.com/images/gojotexas/merchant/1627442954890_add_error_jpg",
                        "isBankVerified" : true
                    },*/
                    Bson validBank = Filters.or(
                            Filters.exists("bankInfo.invalidBank", false),
                            Filters.eq("bankInfo.invalidBank", false)
                    );
                    Bson accountNumber = Filters.and(
                            Filters.exists("bankInfo.bankToken", true),
                            Filters.ne("bankInfo.bankToken", "")
                    );
                    filter = Filters.and(filter, validBank, accountNumber);
                } else if (payoutOption.equals("noBank")) {
                    Bson noBank = Filters.or(
                            Filters.exists("bankInfo.bankToken", false),
                            Filters.eq("bankInfo.bankToken", "")
                    );
                    filter = Filters.and(filter, noBank);
                }
            }
            else if (gateway.equals(KeysUtil.ANY_BANK)) {
                if (payoutOption.equals("hasBank")) {
                    Bson validBank = Filters.or(
                            Filters.exists("bankInfo.invalidBank", false),
                            Filters.eq("bankInfo.invalidBank", false)
                    );
                    Bson bankName = Filters.and(
                            Filters.exists("bankInfo.bankName", true),
                            Filters.ne("bankInfo.bankName", "")
                    );
                    Bson accountHolder = Filters.and(
                            Filters.exists("bankInfo.accountHolder", true),
                            Filters.ne("bankInfo.accountHolder", "")
                    );
                    Bson accountNumber = Filters.and(
                            Filters.exists("bankInfo.accountNumber", true),
                            Filters.ne("bankInfo.accountNumber", "")
                    );
                    filter = Filters.and(validBank, bankName, accountNumber, accountHolder);
                } else if (payoutOption.equals("noBank")) {
                    Bson noBank = Filters.or(
                            Filters.exists("bankInfo.bankName", false),
                            Filters.eq("bankInfo.bankName", ""),
                            Filters.exists("bankInfo.accountHolder", false),
                            Filters.eq("bankInfo.accountHolder", ""),
                            Filters.exists("bankInfo.accountNumber", false),
                            Filters.eq("bankInfo.accountNumber", "")
                    );
                    filter = Filters.and(filter, noBank);
                }
            }
            else if (gateway.equals(KeysUtil.INDIA_DEFAULT)) {
                if (payoutOption.equals("hasBank")) {
                    Bson validBank = Filters.or(
                            Filters.exists("bankInfo.invalidBank", false),
                            Filters.eq("bankInfo.invalidBank", false)
                    );
                    Bson bankName = Filters.and(
                            Filters.exists("bankInfo.bankName", true),
                            Filters.ne("bankInfo.bankName", "")
                    );
                    Bson accountHolder = Filters.and(
                            Filters.exists("bankInfo.accountHolder", true),
                            Filters.ne("bankInfo.accountHolder", "")
                    );
                    Bson accountNumber = Filters.and(
                            Filters.exists("bankInfo.accountNumber", true),
                            Filters.ne("bankInfo.accountNumber", "")
                    );
                    Bson IFSCCode = Filters.and(
                            Filters.exists("bankInfo.IFSCCode", true),
                            Filters.ne("bankInfo.IFSCCode", "")
                    );
                    filter = Filters.and(filter, validBank, bankName, accountHolder, accountNumber, IFSCCode);
                } else if (payoutOption.equals("noBank")) {
                    Bson noBank = Filters.or(
                            Filters.exists("bankInfo.bankName", false),
                            Filters.eq("bankInfo.bankName", ""),
                            Filters.exists("bankInfo.accountHolder", false),
                            Filters.eq("bankInfo.accountHolder", ""),
                            Filters.exists("bankInfo.accountNumber", false),
                            Filters.eq("bankInfo.accountNumber", ""),
                            Filters.exists("bankInfo.IFSCCode", false),
                            Filters.eq("bankInfo.IFSCCode", "")
                    );
                    filter = Filters.and(filter, noBank);
                }
            }
        }
        Bson projection = Projections.fields(Projections.include("userId"), Projections.excludeId());
        Bson sort = Sorts.ascending("createdDate");
        MongoCursor<Document> cursor  = collection.find(filter).projection(projection).sort(sort).skip(skip).limit(size).iterator();
        List<String> listMerchantId = new ArrayList<>();
        while(cursor.hasNext()) {
            MenuMerchant menuMerchant = gson.fromJson(gson.toJson(cursor.next()), MenuMerchant.class);
            listMerchantId.add(menuMerchant._id.toString());
        }
        return listMerchantId;
    }

    public boolean updateBankStatusForMerchant(String requestId, String fleetId, String merchantId, boolean status) {
        try {
            MongoCollection<MenuMerchant> collection = database.getCollection("MenuMerchant", MenuMerchant.class);
            Bson filter = Filters.eq("_id", new ObjectId(merchantId));
            Bson update = Updates.combine(
                    Updates.set("bankInfo.invalidBank", status),
                    Updates.currentDate("updated_at"));
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        } catch (Exception ex) {
            logger.debug(requestId + " - updateBankStatusForMerchant error: " + CommonUtils.getError(ex));
            return false;
        }
    }

    public GatewayTSYS getGatewayTSYS(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayTSYS.class);
    }

    public GatewayMADA getGatewayMADA(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayMADA.class);
    }


    public MenuMerchantUser getMenuMerchantUser(String adminId) {
        Bson filter = Filters.eq("_id", new ObjectId(adminId));
        return processRequest(filter, MenuMerchantUser.class);
    }

    public DriverFields getDriverFields(String fleetId, String fieldKey) {
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("fieldKey", fieldKey)
        );
        return processRequest(filter, DriverFields.class);
    }

    public boolean updateBankVerifyForMerchant(String requestId, String merchantId){
        try {
            MongoCollection<MenuMerchant> collection = database.getCollection("MenuMerchant", MenuMerchant.class);
            Bson filter = Filters.eq("_id", new ObjectId(merchantId));
            Bson update = Updates.combine(
                    Updates.set("bankInfo.isBankVerified", true),
                    Updates.currentDate("updated_at"));
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        } catch (Exception ex) {
            logger.debug(requestId + " - updateBankVerifyForMerchant error: " + CommonUtils.getError(ex));
            return false;
        }
    }

    public List<String> getActiveDriversByType(String requestId, String fleetId, List<String> driverTypes, int skip, int size) {
        MongoCollection<Document> collection = database.getCollection("Account", Document.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("appType", "driver"),
                Filters.in("driverInfo.driverType", driverTypes),
                Filters.eq("isActive", true)
        );
        Bson sort = Sorts.descending("createdDate");
        Bson projections = Projections.fields(Projections.include("userId"), Projections.exclude("_id"));
        MongoCursor<Document> cursor = collection.find(filter).skip(skip).limit(size).sort(sort).projection(projections).iterator();
        List<String> listData = new ArrayList<>();
        while(cursor.hasNext()) {
            Account item = gson.fromJson(gson.toJson(cursor.next()), Account.class);
            listData.add(item.userId);
        }
        return listData;
    }

    public WalletYenePay getWalletYenePay(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, WalletYenePay.class);
    }

    public WalletZainCash getWalletZainCash(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, WalletZainCash.class);
    }

    public GatewayPayMaya getGatewayPayMaya(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayPayMaya.class);
    }

    public String getConfigInfo(String fleetId, String tableName) {
        MongoCollection<Document> collection = database.getCollection(tableName);
        Bson filter = Filters.eq("fleetId", fleetId);
        Document doc = collection.find(filter).first();
        return gson.toJson(doc);
    }

    public List<String> getAllPassengerRegisteredCard(String requestId, String fleetId, int skip, int size) {
        MongoCollection<Document> collection = database.getCollection("Account", Document.class);
        Bson filterCredit = Filters.and(
                Filters.exists("credits"),
                Filters.not(Filters.size("credits", 0))
        );
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("appType", "passenger"),
                Filters.eq("isActive", true),
                filterCredit
        );
        Bson sort = Sorts.descending("createdDate");
        Bson projections = Projections.fields(Projections.include("userId"), Projections.exclude("_id"));
        MongoCursor<Document> cursor = collection.find(filter).skip(skip).limit(size).sort(sort).projection(projections).iterator();
        List<String> listData = new ArrayList<>();
        while(cursor.hasNext()) {
            Account item = gson.fromJson(gson.toJson(cursor.next()), Account.class);
            listData.add(item.userId);
        }
        return listData;
    }

    public MerchantWallet getMerchantWallet(String fleetId, String merchantId) {
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("merchantId", merchantId)
        );
        try {
            return processRequest(filter, MerchantWallet.class);
        } catch(Exception ex) {
            System.out.println("==== Error mapping MerchantWallet for merchantId " + merchantId);
            return null;
        }
    }

    public void addMerchantWallet(MerchantWallet merchantWallet) {
        MongoCollection<MerchantWallet> collection = database.getCollection("MerchantWallet", MerchantWallet.class);
        try {
            collection.insertOne(merchantWallet);
        } catch (Exception ignore) {}

    }

    public boolean updateMerchantWallet(String fleetId, String merchantId, String walletType, AmountByCurrency creditWallet, BalanceByCurrency cashWallet) {
        MongoCollection<MerchantWallet> collection = database.getCollection("MerchantWallet", MerchantWallet.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("merchantId", merchantId)
        );
        UpdateResult result;
        switch (walletType) {
            case "credit": {
                Bson update = Updates.combine(
                        Updates.set("creditWallet", creditWallet),
                        Updates.currentDate("latestUpdate")
                );
                result = collection.updateOne(filter, update);
                break;
            }
            case "cash": {
                Bson update = Updates.combine(
                        Updates.set("cashWallet", cashWallet),
                        Updates.currentDate("latestUpdate")
                );
                result = collection.updateOne(filter, update);
                break;
            }
            default:
                result = null;
                break;
        }
        return result != null && result.wasAcknowledged();
    }

    public GatewayBankOfGeorgia getGatewayBOG(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, GatewayBankOfGeorgia.class);
    }

    public boolean updateStripeConnectStatusOperation(String requestId, String userId, String status){
        try {
            MongoCollection<Operations> collection = database.getCollection("Operations", Operations.class);
            Bson filter = Filters.eq("driverId", userId);
            Bson update = Updates.combine(
                    Updates.set("stripeConnectStatus", status),
                    Updates.currentDate("latestUpdate")
            );
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        } catch (Exception ex) {
            logger.debug(requestId + " - updateStripeConnectStatusOperation error: " + CommonUtils.getError(ex));
            return false;
        }
    }

    public boolean updateStripeConnectStatus(String requestId, String fleetId, String userId, String token, String status) {
        try {
            MongoCollection<Account> collection = database.getCollection("Account", Account.class);
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("userId", userId)
            );
            Bson update = Updates.combine(
                    Updates.set("driverInfo.stripeConnectStatus", status),
                    Updates.set("driverInfo.achToken", token),
                    Updates.set("driverInfo.isBankVerified", true),
                    Updates.currentDate("latestUpdate")
            );
            if (status.equals("activated")) {
                update = Updates.combine(
                        Updates.set("driverInfo.signupStep", "stripeConnect"),
                        Updates.set("driverInfo.stripeConnectStatus", status),
                        Updates.set("driverInfo.achToken", token),
                        Updates.currentDate("latestUpdate")
                );
            }
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        } catch (Exception ex) {
            logger.debug(requestId + " - updateStripeConnectStatus error: " + CommonUtils.getError(ex));
            return false;
        }
    }

    public boolean disconnectStripe(String requestId, String fleetId, String userId, boolean updateStatus) {
        try {
            MongoCollection<Account> collection = database.getCollection("Account", Account.class);
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("userId", userId)
            );
            Bson update = Updates.combine(
                    Updates.set("driverInfo.stripeConnectStatus", ""),
                    Updates.set("driverInfo.achToken", ""),
                    Updates.currentDate("latestUpdate")
            );
            UpdateResult result = collection.updateOne(filter, update);
            if (result.wasAcknowledged() && updateStatus) {
                Bson updateAccountStatus = Updates.combine(
                        Updates.set("isActive", false),
                        Updates.set("driverInfo.isActivate", false),
                        Updates.set("driverInfo.statusReview", "inProgress"),
                        Updates.currentDate("latestUpdate")
                );
                UpdateResult resultUpdate = collection.updateOne(filter, updateAccountStatus);
                return resultUpdate.wasAcknowledged();
            } else {
                return result.wasAcknowledged();
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - disconnectStripe error: " + CommonUtils.getError(ex));
            return false;
        }
    }

    public boolean updateStripeConnectStatusForFleet(String requestId, String fleetId, String token, String status) {
        try {
            MongoCollection<Fleet> collection = database.getCollection("Fleet", Fleet.class);
            Bson filter = Filters.eq("fleetId", fleetId);
            Bson update = Updates.combine(
                    Updates.set("stripeConnect.connectStatus", status),
                    Updates.set("stripeConnect.connectToken", token),
                    Updates.currentDate("latestUpdate")
            );
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        } catch (Exception ex) {
            logger.debug(requestId + " - updateStripeConnectStatusForFleet error: " + CommonUtils.getError(ex));
            return false;
        }
    }

    public boolean disconnectStripeForFleet(String requestId, String fleetId) {
        try {
            MongoCollection<Fleet> collection = database.getCollection("Fleet", Fleet.class);
            Bson filter = Filters.eq("fleetId", fleetId);
            Bson update = Updates.combine(
                    Updates.set("stripeConnect.connectStatus", ""),
                    Updates.set("stripeConnect.connectToken", ""),
                    Updates.currentDate("latestUpdate")
            );
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        } catch (Exception ex) {
            logger.debug(requestId + " - disconnectStripeForFleet error: " + CommonUtils.getError(ex));
            return false;
        }
    }

    public boolean updateStripeConnectStatusForCompany(String requestId, String fleetId, String companyId, String token, String status) {
        try {
            MongoCollection<Company> collection = database.getCollection("Company", Company.class);
            Bson filter = Filters.eq("_id", new ObjectId(companyId));
            Bson update = Updates.combine(
                    Updates.set("connectStatus", status),
                    Updates.set("connectToken", token),
                    Updates.currentDate("latestUpdate")
            );
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        } catch (Exception ex) {
            logger.debug(requestId + " - updateStripeConnectStatusForCompany error: " + CommonUtils.getError(ex));
            return false;
        }
    }

    public boolean disconnectStripeForCompany(String requestId, String fleetId, String companyId) {
        try {
            MongoCollection<Company> collection = database.getCollection("Company", Company.class);
            Bson filter = Filters.eq("_id", new ObjectId(companyId));
            Bson update = Updates.combine(
                    Updates.set("connectStatus", ""),
                    Updates.set("connectToken", ""),
                    Updates.currentDate("latestUpdate")
            );
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        } catch (Exception ex) {
            logger.debug(requestId + " - disconnectStripeForCompany error: " + CommonUtils.getError(ex));
            return false;
        }
    }

    public List<String> getListCarTypeByAffiliate(String fleetId, String affiliateCarTypeId) {
        try {
            MongoCollection<Document> collection = database.getCollection("VehicleType", Document.class);
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.in("affiliateCarType", affiliateCarTypeId),
                    Filters.in("isActive", true)
            );
            Bson projections = Projections.fields(Projections.include("vehicleType"), Projections.excludeId());
            MongoCursor<Document> cursor = collection.find(filter).projection(projections).iterator();
            List<String> listCarTypeName = new ArrayList<>();
            while(cursor.hasNext()) {
                VehicleType vehicleType = gson.fromJson(gson.toJson(cursor.next()), VehicleType.class);
                listCarTypeName.add(vehicleType.vehicleType);
            }
            return listCarTypeName;
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateSetting getAffiliateSetting() {
        Bson filter = Filters.empty();
        return processRequest(filter, AffiliateSetting.class);
    }

    public AffiliateMarkupPrice getAffiliateMarkupPrice(String fleetId) {
        Bson filter = Filters.eq("fleetId", fleetId);
        return processRequest(filter, AffiliateMarkupPrice.class);
    }

    public List<String> getListFleetToPayout(String requestId, String payoutOption, int skip, int size) {
        MongoCollection<Document> collection = database.getCollection("PricingPlan", Document.class);
        Bson hydra = Filters.or(
            Filters.eq("affiliate.dispatching", true),
            Filters.eq("affiliate.receiveOndemandBooking", true),
            Filters.eq("affiliate.receiveReservationBooking", true)
        );

        Bson filter;
        if (payoutOption.equals("hasBank")) {
            //finalQuery.add(new BasicDBObject("affiliate.isActiveBankAccount", true));
            Bson bankName = Filters.and(
                    Filters.exists("affiliate.bankName", true),
                    Filters.ne("affiliate.bankName", "")
            );
            Bson holderName = Filters.and(
                    Filters.exists("affiliate.holderName", true),
                    Filters.ne("affiliate.holderName", "")
            );
            Bson accountNumber = Filters.and(
                    Filters.exists("affiliate.accountNumber", true),
                    Filters.ne("affiliate.accountNumber", "")
            );
            filter = Filters.and(hydra, bankName, holderName, accountNumber);
        } else if (payoutOption.equals("noBank")) {
            Bson noBank = Filters.or(
                Filters.exists("affiliate.bankName", false),
                Filters.eq("affiliate.bankName", ""),
                Filters.exists("affiliate.holderName", false),
                Filters.eq("affiliate.holderName", ""),
                Filters.exists("affiliate.accountNumber", false),
                Filters.eq("affiliate.accountNumber", "")
            );
            filter = Filters.and(hydra, noBank);
        } else {
            filter = Filters.empty();
        }
        Bson projection = Projections.fields(Projections.include("fleetId"), Projections.excludeId());
        Bson sort = Sorts.ascending("createdDate");
        MongoCursor<Document> cursor  = collection.find(filter).projection(projection).sort(sort).skip(skip).limit(size).iterator();
        List<String> listFleetId = new ArrayList<>();
        while(cursor.hasNext()) {
            PricingPlan pricingPlan = gson.fromJson(gson.toJson(cursor.next()), PricingPlan.class);
            listFleetId.add(pricingPlan.fleetId);
        }
        return listFleetId;
    }

    public Company getCompanyById(String companyId) {
        try{
            Bson filter = Filters.eq("_id", new ObjectId(companyId));
            return processRequest(filter, Company.class);
        }catch (Exception e){
            e.getMessage();
            return null;
        }
    }

    public GatewayPayWay getGatewayPayWay(String fleetId) {
        MongoCollection<GatewayPayWay> collection = database.getCollection("GatewayPayWay", GatewayPayWay.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public List<String> getWingBankAPIKey() {
        try {
            MongoCollection<Document> collection = database.getCollection("WalletWingBank", Document.class);
            MongoCursor<Document> cursor = collection.find().iterator();
            List<String> listKey = new ArrayList<>();
            while(cursor.hasNext()) {
                WalletWingBank walletWingBank = gson.fromJson(gson.toJson(cursor.next()), WalletWingBank.class);
                String apiKey = walletWingBank.apiKey != null ? walletWingBank.apiKey : "";
                if (!apiKey.isEmpty() && !listKey.contains(apiKey))
                    listKey.add(apiKey);
            }
            return listKey;
        } catch(Exception ex) {
            return null;
        }
    }

    public String getDroppedOffBooking(String fleetId, String userId) {
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("status", "droppedOff"),
                Filters.eq("psgInfo.userId", userId)
        );
        try {
            Booking booking = processRequest(filter, Booking.class);
            return booking != null ? booking.bookId : "";
        } catch(Exception ex) {
            return "";
        }
    }

    public void addLogToken(LogToken logToken) {
        MongoCollection<LogToken> collection = database.getCollection("LogToken", LogToken.class);
        try {
            collection.insertOne(logToken);
        } catch (Exception ignore) {}
    }

    public LogToken getLogToken(String fleetId, String logId) {
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("logId", logId)
        );
        return processRequest(filter, LogToken.class);
    }

    public void deleteLogToken(String fleetId, String logId) {
        MongoCollection<LogToken> collection = database.getCollection("LogToken", LogToken.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("logId", logId)
        );
        collection.deleteOne(filter);
    }

    public List<String> getCorporateByFleet(String requestId, String fleetId) {
        MongoCollection<Document> collection = database.getCollection("Corporate", Document.class);
        Bson filterCredit = Filters.and(
                Filters.exists("credits"),
                Filters.not(Filters.size("credits", 0))
        );
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("isActive", true),
                filterCredit
        );
        MongoCursor<Document> cursor = collection.find(filter).iterator();
        List<String> listData = new ArrayList<>();
        while(cursor.hasNext()) {
            Corporate item = gson.fromJson(gson.toJson(cursor.next()), Corporate.class);
            listData.add(item._id.toString());
        }
        return listData;
    }

    public ZonePayment getZonePayment(String fleetId, String zoneId) {
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("zoneId", zoneId)
        );
        return processRequest(filter, ZonePayment.class);
    }

    public Booking getBookingByInvoiceId(String fleetId, long invoiceId) {
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("invoiceId", invoiceId)
        );
        Booking booking = processRequest(filter, Booking.class);
        if (booking == null) {
            MongoCollection<Document> collection = database.getCollection("BookingCompleted", Document.class);
            Document document = collection.find(filter).first();
            booking = gson.fromJson(gson.toJson(document), Booking.class);
        }
        return booking;
    }

    public Invoice getInvoice(String fleetId, long invoiceId) {
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("invoiceId", invoiceId)

        );
        return processRequest(filter, Invoice.class);
    }

    public List<String> getListBookingOfInvoice(String fleetId, long invoiceId) {
        MongoCollection<Document> collection = database.getCollection("BookingCompleted", Document.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("invoiceId", invoiceId)

        );
        Bson projections = Projections.fields(Projections.include("bookId"), Projections.exclude("_id"));
        MongoCursor<Document> cursor = collection.find(filter).projection(projections).iterator();
        List<String> listBookId = new ArrayList<>();
        while(cursor.hasNext()) {
            Booking booking = gson.fromJson(gson.toJson(cursor.next()), Booking.class);
            listBookId.add(booking.bookId);
        }
        return listBookId;
    }

    public boolean updateInvoice(String requestId, long invoiceId, JSONObject updateObj){
        try {
            Bson filter = Filters.eq("invoiceId", invoiceId);
            Invoice invoice = processRequest(filter, Invoice.class);
            if (invoice != null) {
                MongoCollection<Invoice> collection = database.getCollection("Invoice", Invoice.class);
                Bson update = Updates.combine(
                        Updates.set("status", updateObj.getInt("status")),
                        Updates.set("transactionId", updateObj.getString("transactionId")),
                        Updates.set("paymentMethod", updateObj.getString("paymentMethod")),
                        Updates.set("paidAmount", updateObj.getDouble("paidAmount")),
                        Updates.set("cardType", updateObj.getString("cardType")),
                        Updates.set("cardMasked", updateObj.getString("cardMasked")),
                        Updates.set("chargeNote", updateObj.getString("chargeNote")),
                        Updates.set("paidTime", KeysUtil.SDF_DMYHMSTZ.parse(updateObj.getString("paidTime"))),
                        Updates.currentDate("latestUpdate"));
                UpdateResult result = collection.updateOne(filter, update);
                return result.wasAcknowledged();
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - updateInvoice exception: " + CommonUtils.getError(ex));
        }
        return false;
    }

    public boolean updatePaymentLinkToBooking(String bookId, boolean hasPendingPaymentLink){
        MongoCollection<Booking> collection = database.getCollection("BookingCompleted", Booking.class);
        Bson filter = Filters.eq("bookId", bookId);
        Bson update = Updates.set("hasPendingPaymentLink", hasPendingPaymentLink);
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public Fleet findByFleetToken(String fleetToken){
        Bson filter = Filters.eq("fleetToken", fleetToken);
        return processRequest(filter, Fleet.class);
    }
}
