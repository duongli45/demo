package com.qupworld.paymentgateway.models.mongo.collections.zonePayment;

import com.qupworld.paymentgateway.models.mongo.collections.fleet.PaymentClearanceHouse;

import javax.annotation.Generated;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class ZonePayment {
    public String fleetId;
    public String zoneId;
    public String customizedPaymentZone;
    public boolean forceCreditCardApp;
    public boolean forceCreditCardWeb;
    public boolean forceCreditCardMDispatcher;
    public boolean forceCreditCardPWA;
    public boolean forceCreditCardKiosk;
    public List<PaymentClearanceHouse> paymentClearanceHouses;
}
