package com.qupworld.paymentgateway.models.mongo.collections.booking;

import javax.annotation.Generated;

/**
 * Created by qup on 08/04/2020.
 */
@Generated("org.jsonschema2pojo")
public class AirpayInfo {
    public String paymentToken;
    public String userRef;
}
