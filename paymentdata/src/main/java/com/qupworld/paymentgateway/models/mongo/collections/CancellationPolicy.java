
package com.qupworld.paymentgateway.models.mongo.collections;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class CancellationPolicy {

    public Boolean isActive;
    public OnDemand onDemand;
    public InAdvance inAdvance;

}
