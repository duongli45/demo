package com.qupworld.paymentgateway.models.mongo.collections.affiliateRoute;

/**
 * Created by hoangnguyen on 7/26/17.
 */
public class SingleTrip {
    public Double departureRoute;
    public Boolean enableReturnRoute;
    public Double returnRoute;
    public Limitation limitation;
}
