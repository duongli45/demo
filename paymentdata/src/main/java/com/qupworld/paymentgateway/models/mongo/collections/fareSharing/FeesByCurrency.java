package com.qupworld.paymentgateway.models.mongo.collections.fareSharing;

import java.util.List;

/**
 * Created by hoangnguyen on 5/17/17.
 */
public class FeesByCurrency {
    public String currencyISO;
    public double startingFee;
    public double minimumFee;
    public double feePerMinute;
    public List<FeePerPax> feePerPax;
}
