
package com.qupworld.paymentgateway.models.mongo.collections.fleet;

import javax.annotation.Generated;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class TransactionFee {

    public Boolean enable;
    public List<FeeByCurrency> feeByCurrencies;

}
