
package com.qupworld.paymentgateway.models.mongo.collections.account;

import com.qupworld.paymentgateway.models.mongo.collections.Credit;
import org.bson.types.ObjectId;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class Account {

    public ObjectId _id;
    public String _class;
    public String avatar;
    public String appType;
    public String deviceToken;
    public String fleetId;
    public String language;
    public String phone;
    public String username;
    public String platform;
    public Boolean verified;
    public Boolean agreement;
    public String verifyCode;
    public String firstName;
    public String lastName;
    public Integer tips;
    public String registerFrom;
    public String firstRegisterFrom;
    public String fullName;
    public String fullNameSort;
    public String email;
    public String address;
    public List<Credit> credits = new ArrayList<Credit>();
    public PassengerInfo passengerInfo;
    public DriverInfo driverInfo;
    public MDispatcherInfo mDispatcherInfo;
    public FrequentAddress frequentAddress;
    public HomeAddress homeAddress;
    public Date firstVerified;
    public Boolean isLogin;
    public String appName;
    public String ime;
    public Agent agent;
    public LastLogin lastLogin;
    public Boolean isActive;
    public Date createdDate;
    public Date latestUpdate;
    public Integer v;
    public String userId;
    public CorporateInfo corporateInfo;
    public Booking booking;
    public List<Double> firstSignUpGeo = new ArrayList<Double>();
    public Profile profile;

}
