
package com.qupworld.paymentgateway.models.mongo.collections.booking;

import org.bson.types.ObjectId;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class Booking {

    public ObjectId _id;
    public String ip;
    public String bookId;
    public String platform;
    public String bookFrom;
    public String dispatcherId;
    public String token;
    public String dispatcherPhone;
    public String queueId;
    public String status;
    public String fleetId;
    public String fleetName;
    public Boolean reservation;
    public Date latestUpdate;
    public Boolean timeout;
    public Date createdDate;
    public Date finishedAt;
    public String currencyISO;
    public String currencySymbol;
    public CancelInfo cancelInfo;
    public List<OfferedQueue> offeredQueue = new ArrayList<OfferedQueue>();
    public Time time;
    public Request request;
    public MDispatcherInfo mDispatcherInfo;
    public PsgInfo psgInfo;
    public int dispatchType;
    public OfferedInfo offeredInfo;
    public DrvInfo drvInfo;
    public int pricingType;
//    public DrvCancel drvCancel;
    public EngagedInfo engagedInfo;
    public DroppedOffInfo droppedOffInfo;
    public Operator operator;
    public Operator createdByOperator;
    public CorporateInfo corporateInfo;
    public PreAuthorized preAuthorized;
    public String vehicleType;
    public Boolean dispatch3rd;
    public Boolean stoppedDispatch;
    public Boolean fromPaxApp;
    public Boolean dispatchRideSharing;
    public List<BookingRideSharing> bookingRideSharing;
    public CompletedInfo completedInfo;
    public List<SOS> sos;
    public Itinerary itinerary;
    public Distance distance;
    public Rv rv;
    public Integer affiliateStatus;
    public boolean updatedTotal;
    public boolean superHelper;
    public boolean intercity;
    public String tripId;
    public IntercityInfo intercityInfo;
    public List<EstimateFareBuy> estimateFareBuy;
    public boolean delivery;
    public String jobType; //['transport', 'intercity', 'delivery', 'shopForYou', 'food', 'mart', 'chauffeur', 'shuttle']
    public DeliveryInfo deliveryInfo;
    public boolean walletAppInstalled;
    public boolean isMultiPoints;
    public Recurring recurring;
    public List<Point> puPoints;
    public List<Point> doPoints;
    public StreetSharing streetSharing;
    public HydraInfo hydraInfo;
    public boolean isFarmOut;
    public ExternalInfo externalInfo;
    public int travelerType;
    public long invoiceId;
    public String paidStatus;
    public double paidAmount;
    public boolean hasPendingPaymentLink;
}
