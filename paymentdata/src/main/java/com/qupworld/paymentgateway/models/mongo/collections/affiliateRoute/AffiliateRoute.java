package com.qupworld.paymentgateway.models.mongo.collections.affiliateRoute;


import org.bson.types.ObjectId;

/**
 * Created by hoangnguyen on 7/26/17.
 */
public class AffiliateRoute {
    public ObjectId _id;
    public String flatRateId;
    public Zone zone;
    public ZipCode zipCode;
    public SingleTrip singleTrip;
    public RoundTrip roundTrip;
    public RouteInfo routeInfo;
    public OtherFees otherFees;
}
