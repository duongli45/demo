
package com.qupworld.paymentgateway.models.mongo.collections.pricingplan;


import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import javax.annotation.Generated;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class DriverDeposit {

    public Boolean enable;
    public List<AmountByCurrency> valueByCurrencies;
    public List<AmountByCurrency> minimumByCurrencies;
    public List<AmountByCurrency> maximumByCurrencies;
    public List<AmountByCurrency> maxTransferByCurrencies;
    public List<AmountByCurrency> allowanceByCurrencies;
    public List<AmountByCurrency> maxTopupDriverByCurrencies;
}
