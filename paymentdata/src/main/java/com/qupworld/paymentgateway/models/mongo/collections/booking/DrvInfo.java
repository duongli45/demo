
package com.qupworld.paymentgateway.models.mongo.collections.booking;

import org.bson.types.ObjectId;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class DrvInfo {

    public Integer disLiked;
    public Integer liked;
    public String avatar;
    public String vehicleMakeName;
    public String drvId;
    public String vhcId;
    public String vehicleModelName;
    public String vehicleAvatar;
    public String companyId;
    public String companyName;
    public String plateNumber;
    public String vehicleType;
    public String userId;
    public String fleetId;
    public String phone;
    public String fullName;
    public String lastName;
    public String firstName;
    public ObjectId vehicleId;
    public String rv;
    public String platform;
    public String vhcColor;
    public Rating rating;

}
