package com.qupworld.paymentgateway.models.mongo.collections;

/**
 * Created by hoangnguyen on 5/19/17.
 */
public class TechFeeByCurrency {
    public String currencyISO;
    public double commandCenter;
    public double paxApp;
    public double webBooking;
    public double mDispatcher;
    public double partner;
    public double carHailing;
    public double kiosk;
    public double api;
    public double streetSharing;
}
