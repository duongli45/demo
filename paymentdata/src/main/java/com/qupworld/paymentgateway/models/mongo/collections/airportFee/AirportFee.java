package com.qupworld.paymentgateway.models.mongo.collections.airportFee;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import javax.annotation.Generated;
import java.util.List;

/**
 * Created by myasus on 6/4/19.
 */
@Generated("org.jsonschema2pojo")
public class AirportFee {
    public String fleetId;
    public String airportZoneId;
    public boolean fromAirportActive;
    public boolean toAirportActive;
    public boolean isActive;
    public List<AmountByCurrency> fromAirportByCurrencies;
    public List<AmountByCurrency> toAirportByCurrencies;
}
