package com.qupworld.paymentgateway.models.mongo.collections.referral;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import java.util.List;

/**
 * Created by qup on 18/10/2018.
 */
public class Driver {

    public String type;
    public List<AmountByCurrency> money;
    public Settlement settlement;
}
