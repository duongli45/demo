
package com.qupworld.paymentgateway.models.mongo.collections.fareFlat;

import com.qupworld.paymentgateway.models.mongo.collections.CancellationPolicy;
import com.qupworld.paymentgateway.models.mongo.collections.DriverCancelPolicy;
import com.qupworld.paymentgateway.models.mongo.collections.NoShow;
import org.bson.types.ObjectId;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class FareFlat {

    public ObjectId _id;
    public String name;
    public String fleetId;
    public CancellationPolicy cancellationPolicy;
    public NoShow noShow;
    public Boolean isDefault;
    public Boolean isActive;
    public String serviceType;
    public DriverCancelPolicy driverCancelPolicy;
    public Double fareWaitTimeNow;
    public Double fareWaitTimeLater;
}
