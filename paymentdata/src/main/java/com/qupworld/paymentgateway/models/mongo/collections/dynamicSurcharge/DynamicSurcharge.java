package com.qupworld.paymentgateway.models.mongo.collections.dynamicSurcharge;

import com.qupworld.paymentgateway.models.mongo.collections.flatRoutes.ZoneData;
import org.bson.types.ObjectId;

/**
 * Created by myasus on 4/22/19.
 */
public class DynamicSurcharge {
    public ObjectId _id;
    public String fleetId;
    public String zoneId;
    public String name;
    public double parameter;
    public boolean pickupPoint;
    public boolean dropOffPoint;
    public Boolean isActive;
    public ZoneData geo;
}
