
package com.qupworld.paymentgateway.models.mongo.collections;

import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Vehicle {

    public ObjectId _id;
    public String fleetId;
    public String plateNumber;
    public String color;
    public Integer year;
    public Integer maxPassengers;
    public Integer maxLuggage;
    public String licenseExp;
    public List<String> listRoles;
    public String vhcId;
    public Integer assigned;
    public Boolean isActive;
    public Integer v;
    public VhcType type;

    public class VhcType {
        public boolean isActive;
        public ObjectId _id;
        public String typeId;
        public String name;
    }
}
