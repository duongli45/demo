package com.qupworld.paymentgateway.models.mongo.collections;

import java.util.List;

/**
 * Created by qup on 12/08/2019.
 */
public class CreditFee {

    public Boolean getHigher;
    public Double percentage;
    public List<AmountByCurrency> flatRateByCurrencies;
}
