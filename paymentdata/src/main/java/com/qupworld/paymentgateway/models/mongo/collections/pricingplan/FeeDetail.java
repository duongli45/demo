
package com.qupworld.paymentgateway.models.mongo.collections.pricingplan;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class FeeDetail {

    public Boolean enable;
    public Integer free;
    public Double cost;
    public Double percentOfBaseFee;
}
