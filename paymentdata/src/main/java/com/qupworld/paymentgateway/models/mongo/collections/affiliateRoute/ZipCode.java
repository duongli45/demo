package com.qupworld.paymentgateway.models.mongo.collections.affiliateRoute;

import java.util.List;

/**
 * Created by hoangnguyen on 7/26/17.
 */
public class ZipCode {
    public List<String> fromZipCode;
    public List<String> toZipCode;
}
