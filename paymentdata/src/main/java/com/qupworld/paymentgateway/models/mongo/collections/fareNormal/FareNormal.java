
package com.qupworld.paymentgateway.models.mongo.collections.fareNormal;

import com.qupworld.paymentgateway.models.mongo.collections.CancellationPolicy;
import com.qupworld.paymentgateway.models.mongo.collections.DriverCancelPolicy;
import com.qupworld.paymentgateway.models.mongo.collections.NoShow;
import com.qupworld.paymentgateway.models.mongo.collections.corporate.Pricing;
import com.qupworld.paymentgateway.models.mongo.collections.pricingplan.PriceAdjustable;
import org.bson.types.ObjectId;

import javax.annotation.Generated;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class FareNormal {

    public ObjectId _id;
    public String fleetId;
    public String name;
    public Double firstDistanceFrom;
    public Double firstDistanceTo;
    public Double afterFirstDistance;
    public Double secondDistanceFrom;
    public Double secondDistanceTo;
    public Double afterSecondDistance;
    public Boolean isDefault;
    public Integer timeOver;
    public CancellationPolicy cancellationPolicy;
    public NoShow noShow;
    public Boolean isActive;
    public List<FeesByCurrency> feesByCurrencies;
    public PriceAdjustment priceAdjustment;
    public DriverCancelPolicy driverCancelPolicy;
    public Double fareWaitTimeNow;
    public Double fareWaitTimeLater;

}
