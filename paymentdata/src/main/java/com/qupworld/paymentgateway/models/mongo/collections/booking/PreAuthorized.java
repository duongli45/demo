
package com.qupworld.paymentgateway.models.mongo.collections.booking;

import javax.annotation.Generated;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class PreAuthorized {

    public double authAmount;
    public String authCode;
    public String authId;
    public String authToken;
    public List<AdditionalPreauth> additionalPreauths;

}

