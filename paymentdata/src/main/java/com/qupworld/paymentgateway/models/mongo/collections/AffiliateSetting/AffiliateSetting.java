package com.qupworld.paymentgateway.models.mongo.collections.AffiliateSetting;

/**
 * Created by thuanho on 20/10/2022.
 */
public class AffiliateSetting {

    public GeneralSetting generalSetting;
    public CancellationPolicy cancellationPolicy;
    public double maxPendingAmount;
}
