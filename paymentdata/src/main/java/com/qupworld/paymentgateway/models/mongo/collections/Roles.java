
package com.qupworld.paymentgateway.models.mongo.collections;

import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Roles {

    public ObjectId _id;
    public String _class;
    public String name;
    public String fleetId;
    public Boolean systemRole;
    public List<Object> modules = new ArrayList<Object>();
    public Boolean allDriver;
    public Boolean allVehicle;
    public Integer checkDriverCar;
    public String companyId;
    public Boolean isActive;
    public Integer v;

}
