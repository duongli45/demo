
package com.qupworld.paymentgateway.models.mongo.collections;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class UnionpayBin {

    public String issuerCode;
    public String bankName;
    public String cardName;
    public Integer accountLength;
    public String binNumber;
    public Integer binLength;
}
