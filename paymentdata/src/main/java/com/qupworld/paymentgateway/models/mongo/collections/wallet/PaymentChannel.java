package com.qupworld.paymentgateway.models.mongo.collections.wallet;

import org.bson.types.ObjectId;

import javax.annotation.Generated;
import java.util.Date;

/**
 * Created by thuanho on 23/02/2021.
 */
@Generated("org.jsonschema2pojo")
public class PaymentChannel {

    public ObjectId _id;
    public String fleetId;
    public String type; //Wallet | PaxWallet
    public String name;
    public String log;
    public String parameter;
    public String fileName;
    public String logo;
    public int index;
    public boolean isActive;
    public Date createdDate;
    public Date latestUpdate;
}
