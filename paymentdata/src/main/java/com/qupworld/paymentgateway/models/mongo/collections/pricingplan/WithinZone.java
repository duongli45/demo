
package com.qupworld.paymentgateway.models.mongo.collections.pricingplan;

import com.qupworld.paymentgateway.models.mongo.collections.TechFeeByCurrency;

import javax.annotation.Generated;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class WithinZone {

    public String fleetType;
    public String techFeeType;
    public Double techFeeValue;
    public Boolean ccPayToFleet;
    public Double qupFeeAmount;
    public Double qupFeePercentage;
    public Double driverMonthlyFee;
    public Double carMonthlyFee;
    public Boolean techFeeActive;
    //public TechFee techFee;
    public List<TechFeeByCurrency> techFeeByCurrencies;

}
