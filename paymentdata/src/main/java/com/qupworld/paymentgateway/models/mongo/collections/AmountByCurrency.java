
package com.qupworld.paymentgateway.models.mongo.collections;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class AmountByCurrency {

    public String currencyISO;
    public double value;
    public double preAuthAmount; // used for pre-auth payment
    public double commissionValue; // used on driver/mDispatcher/corporate

}
