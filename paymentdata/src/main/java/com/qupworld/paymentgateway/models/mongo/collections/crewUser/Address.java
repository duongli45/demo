package com.qupworld.paymentgateway.models.mongo.collections.crewUser;

/**
 * Created by myasus on 5/5/21.
 */
public class Address {
    public String address;
    public String city;
    public String state;
    public String zipcode;
    public String country;
}