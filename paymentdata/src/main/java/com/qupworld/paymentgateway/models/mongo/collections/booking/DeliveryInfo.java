package com.qupworld.paymentgateway.models.mongo.collections.booking;

import java.util.List;

/**
 * Created by myasus on 6/25/20.
 */
public class DeliveryInfo {
    public String menuId;
    public boolean cashOnPickUp;
    public boolean cashOnDelivery;
    public DeliveryPickUp pickup;
    public List<Merchant> merchants;
    public List<Recipient> recipients;
    public boolean merchantSelfManage;
}
