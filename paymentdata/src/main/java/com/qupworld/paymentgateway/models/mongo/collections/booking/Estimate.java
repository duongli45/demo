
package com.qupworld.paymentgateway.models.mongo.collections.booking;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Estimate {

    public Double distanceValue;
    public Double estimateValue;
    public String time;
    public String distance;
    public Fare fare;
    public Fare originFare;
    public Double surchargeParameter;
    public Double dynamicFare;
    public boolean isFareEdited;
    public String reasonEditFare;
    public String reasonMarkup;
    public String markupType;
    public double markupValue;

}
