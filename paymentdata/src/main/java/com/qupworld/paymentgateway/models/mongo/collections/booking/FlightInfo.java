
package com.qupworld.paymentgateway.models.mongo.collections.booking;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class FlightInfo {

    public String flightNumber;
    public Integer type;
    public String airline;

}
