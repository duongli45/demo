
package com.qupworld.paymentgateway.models.mongo.collections.pricingplan;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class FarmOut {

    public Double homeFleetAmount;
    public Double homeFleetPercentage;
    public Double affiliateOwnerAmount;
    public Double affiliateOwnerPercentage;

}
