
package com.qupworld.paymentgateway.models.mongo.collections;

import javax.annotation.Generated;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class OnDemand {

    public boolean isActive;
    public double value;
    public List<AmountByCurrency> valueByCurrencies;
    public String payToDrv; // fixAmount | commission
    public String type; // amount | percent
    public boolean enableTax;
    public boolean enableTechFee;
    public List<AmountByCurrency> drvGetAmtByCurrencies;
    public PayDrvForCashBooking payDrvCash;
}
