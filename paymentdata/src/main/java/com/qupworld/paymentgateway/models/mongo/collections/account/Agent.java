
package com.qupworld.paymentgateway.models.mongo.collections.account;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Agent {

    public Cpu cpu;
    public Device device;
    public Os os;
    public Engine engine;
    public Browser browser;
    public String ua;

}
