package com.qupworld.paymentgateway.models.mongo.collections.intercityRoute;

import org.bson.types.ObjectId;

/**
 * Created by myasus on 4/14/20.
 */
public class IntercityRoute {
    public ObjectId _id;
    public String fleetId;
    public String intercityRateId;
    public String routeName;
    public Boolean isActive;
    public LimitSeatLuggage limitSeatLuggage;
    public RouteSetting routeOneSetting;
    public RouteSetting routeTwoSetting;
    public Zone firstLocation;
    public Zone secondLocation;
    public boolean routeTwoEnable;
    public String currencyISO;
    public String currencySymbol;
}
