package com.qupworld.paymentgateway.models.mongo.collections.intercityTrip;

import org.bson.types.ObjectId;

import javax.annotation.Generated;
import java.util.Date;

/**
 * Created by qup on 20/04/2020.
 */
@Generated("org.jsonschema2pojo")
public class Trip {
    public String tripType;
    public String status;
    public ObjectId route;
    public Date startTime;
}
