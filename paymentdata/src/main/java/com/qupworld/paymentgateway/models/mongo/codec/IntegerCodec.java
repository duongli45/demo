package com.qupworld.paymentgateway.models.mongo.codec;

import org.bson.BsonReader;
import org.bson.BsonType;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

public class IntegerCodec implements Codec<Integer> {

    private NullCodec nullCodec = new NullCodec();
    @Override
    public Integer decode(BsonReader reader, DecoderContext decoderContext) {
        // Implement your deserialization logic here
        // Read data from BSON and construct a Number object
        if (reader.getCurrentBsonType() == BsonType.NULL)
            return 0;
        else
            return reader.readInt32(); //
    }

    @Override
    public void encode(BsonWriter writer, Integer value, EncoderContext encoderContext) {
        // Write the value to BSON
        if (value == null) {
            nullCodec.encode(writer, null, encoderContext);
        } else {
            writer.writeInt32(value);
        }
    }

    @Override
    public Class<Integer> getEncoderClass() {
        //return int.class;
        return int.class;
    }
}
