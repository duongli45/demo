package com.qupworld.paymentgateway.models.mongo.collections.wallet;

import javax.annotation.Generated;

/**
 * Created by thuanho on 25/02/2021.
 */
@Generated("org.jsonschema2pojo")
public class WalletMoMo {

    public String fleetId;
    public String environment;
    public String partnerCode;
    public String accessKey;
    public String secretKey;
}
