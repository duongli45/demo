package com.qupworld.paymentgateway.models.mongo.collections.affiliateProcess;

import org.bson.types.ObjectId;

/**
 * Created by hoangnguyen on 12/17/18.
 */
public class AffiliateProcess {
    public ObjectId _id;
    public ObjectValue dispatchRadius;
    public ObjectValue timeOut;
    public ObjectValue minBookAhead;
    public ObjectValue maxBookAhead;
    public ObjectValue chargeInAdvance;
    public ObjectValue fullCharge;
}
