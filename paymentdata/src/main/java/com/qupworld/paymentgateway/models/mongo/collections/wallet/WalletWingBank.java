package com.qupworld.paymentgateway.models.mongo.collections.wallet;

import javax.annotation.Generated;

/**
 * Created by thuanho on 26/06/2023.
 */
@Generated("org.jsonschema2pojo")
public class WalletWingBank {

    public String fleetId;
    public String environment;
    public String userId;
    public String password;
    public String apiKey;
    public String referer;
}

