package com.qupworld.paymentgateway.models.mongo.collections.affiliateRoute;

/**
 * Created by myasus on 7/23/19.
 */
public class OtherFees {
    public boolean isAirportFee;
    public boolean isMeetDriverFee;
    public boolean isTax;
}
