
package com.qupworld.paymentgateway.models.mongo.collections.pricingplan;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class Affiliate {

    public Boolean dispatching;
    public String providerFleet;
    public List<Object> fleets = new ArrayList<>();
    public Boolean onlyTopDrvReceiveBook;
    public FarmOut farmOut;
    public FarmIn farmIn;
    public double homeFleetCommission;
    public double supplierPayout;
    public Boolean showPriceDO;
    public Boolean receiveBooking;
    public String notePayout;
    public boolean isActiveBankAccount;
    public String bankName;
    public String holderName;
    public String accountNumber;
}
