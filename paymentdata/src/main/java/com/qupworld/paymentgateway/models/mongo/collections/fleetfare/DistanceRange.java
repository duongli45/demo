package com.qupworld.paymentgateway.models.mongo.collections.fleetfare;

import java.util.List;

/**
 * Created by myasus on 6/2/21.
 */
public class DistanceRange {
    public double from;
    public double to;
    public double surchargeValue;
    public List<SurchargeByService> surchargeByServices;
}
