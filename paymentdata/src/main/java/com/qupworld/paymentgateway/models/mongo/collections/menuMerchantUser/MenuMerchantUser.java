package com.qupworld.paymentgateway.models.mongo.collections.menuMerchantUser;

/**
 * Created by thuanho on 22/07/2021.
 */
public class MenuMerchantUser {

    public String firstName;
    public String lastName;
    public String email;
    public Phone phone;
}
