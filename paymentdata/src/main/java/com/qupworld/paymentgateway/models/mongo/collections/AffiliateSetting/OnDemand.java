package com.qupworld.paymentgateway.models.mongo.collections.AffiliateSetting;

/**
 * Created by thuanho on 20/10/2022.
 */
public class OnDemand {

    public int freeCancellation;
    public String freeCancellationUnit;
    public double penalty;
    public double compensationSupplier;
}
