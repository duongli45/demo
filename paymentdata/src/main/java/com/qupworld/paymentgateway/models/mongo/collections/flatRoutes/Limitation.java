package com.qupworld.paymentgateway.models.mongo.collections.flatRoutes;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import javax.annotation.Generated;
import java.util.List;

/**
 * Created by hoang.nguyen on 12/14/16.
 */

@Generated("org.jsonschema2pojo")
public class Limitation {
    public boolean limited;
    public double coveredDistance;
    //public double extraDistance;
    public double coveredTime;
    //public double extraTime;
    public List<AmountByCurrency> extraDistanceByCurrencies;
    public List<AmountByCurrency> extraTimeByCurrencies;
}
