package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GatewayOnePay {
    public String fleetId;
    public String environment;
    public String merchantId;
    public String accessCode;
    public String hashCode;
}
