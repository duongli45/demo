
package com.qupworld.paymentgateway.models.mongo.collections.account;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class PassengerInfo {

    public RecentPlaces recentPlaces;
    public Integer rank;
    public String corporateId;

}
