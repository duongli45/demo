
package com.qupworld.paymentgateway.models.mongo.collections.corporate;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class CompanyInfo {

    public String name;
    public String nameSort;
    public String address;
    public String invoiceEmail;
    public String companyId;

}
