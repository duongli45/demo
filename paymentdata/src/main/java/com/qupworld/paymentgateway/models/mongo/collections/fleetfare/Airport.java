
package com.qupworld.paymentgateway.models.mongo.collections.fleetfare;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import javax.annotation.Generated;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class Airport {

    public Boolean fromAirportActive;
    public Boolean toAirportActive;
    public Boolean isCustomized;
    public int payTo; // 1 - fleet | 0 - driver
    public List<AmountByCurrency> fromAirportByCurrencies;
    public List<AmountByCurrency> toAirportByCurrencies;

}
