package com.qupworld.paymentgateway.models.mongo.collections.booking;

/**
 * Created by hoangnguyen on 9/5/18.
 */
public class Distance {
    public double onTheWay;
    public double passOnBoard;
}
