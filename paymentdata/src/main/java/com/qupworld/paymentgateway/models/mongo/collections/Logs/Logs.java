package com.qupworld.paymentgateway.models.mongo.collections.Logs;

import org.bson.types.ObjectId;

import javax.annotation.Generated;
import java.util.Date;

/**
 * Created by qup on 20/04/2020.
 */
@Generated("org.jsonschema2pojo")
public class Logs {
    public ObjectId _id;
    public String userId;
    public String fullName;
    public String userName;
    public String operator;
    public String fleetId;
    public String module;
    public String action;
    public String oldValue;
    public String newValue;
    public Date createdDate;
    public Date latestUpdate;
    public String description;
    public String companyId;
}
