package com.qupworld.paymentgateway.models.mongo.collections.company;

import org.bson.types.ObjectId;

import javax.annotation.Generated;

/**
 * Created by thuanho on 22/12/2022.
 */

@Generated("org.jsonschema2pojo")
public class Company {

    public ObjectId _id;
    public String fleetId;
    public boolean isActive;
    public String connectToken;
    public String connectStatus;
    public double commissionValue;
}
