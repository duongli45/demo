package com.qupworld.paymentgateway.models.mongo.collections.promotionCode;

import java.util.List;

/**
 * Created by hoangnguyen on 3/25/21
 */
public class Schedule {
    public int dayOfWeek;
    public List<TimeSchedule> times;
}