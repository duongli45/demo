
package com.qupworld.paymentgateway.models.mongo.collections.fleet;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class WebBooking {

    public String backgroundDesktop;
    public String backgroundMobile;
    public String mainColor;
    public String secondColor;

}
