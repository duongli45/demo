
package com.qupworld.paymentgateway.models.mongo.collections.account;

import javax.annotation.Generated;
import java.util.Date;

@Generated("org.jsonschema2pojo")
public class LastLogin {

    public String iOSVersion;
    public String androidVersion;
    public Date time;

}
