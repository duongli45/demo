package com.qupworld.paymentgateway.models.mongo.collections;

import java.util.List;

/**
 * Created by thuanho on 18/03/2022.
 */
public class FleetCommission {

    public String commissionType;
    public List<AmountByCurrency> commissionByCurrencies;
}
