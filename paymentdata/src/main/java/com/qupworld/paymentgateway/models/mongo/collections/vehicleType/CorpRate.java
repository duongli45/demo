package com.qupworld.paymentgateway.models.mongo.collections.vehicleType;

import javax.annotation.Generated;
import java.util.List;

/**
 * Created by hoangnguyen on 4/4/17.
 */
@Generated("org.jsonschema2pojo")
public class CorpRate {
    public String corporateId;
    public List<Rate> rates;
}
