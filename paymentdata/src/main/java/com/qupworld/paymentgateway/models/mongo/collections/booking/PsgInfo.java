
package com.qupworld.paymentgateway.models.mongo.collections.booking;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class PsgInfo {

    public String firstName;
    public String lastName;
    public String userId;
    public String email;
    public String phone;
    public String avatar;
    public String fullName;
    public CreditInfo creditInfo;
    public AirpayInfo airpayInfo;
    public Integer rank;
    public String rv;
    public String supportId;
    public String boostId;
    public String boostSessionId;

}
