package com.qupworld.paymentgateway.models.mongo.collections.corpItinerary;

import org.bson.types.ObjectId;

/**
 * Created by thuanho on 30/10/2019.
 */
public class DepartmentInfo {

    public ObjectId _id;
    public String name;
}
