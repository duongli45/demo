package com.qupworld.paymentgateway.models.mongo.collections.booking;

import org.bson.types.ObjectId;

/**
 * Created by hoangnguyen on 8/10/18.
 */
public class Itinerary {
    public ObjectId _id;
    public String preferredEarningType;
    public double preferredEarningAmount;
    public Event event;
    public OrganizationInfo organizationInfo;
    public IndividualInfo individualInfo;
    public int customerType;
}
