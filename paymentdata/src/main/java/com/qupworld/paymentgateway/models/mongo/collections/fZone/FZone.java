
package com.qupworld.paymentgateway.models.mongo.collections.fZone;
import org.bson.types.ObjectId;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class FZone{

    public ObjectId _id;
    public String fleetId;
    public boolean isDefault;
    public boolean display;
    public boolean isAssign;
    public boolean activate;
    public String zoneName;
    public GeoCoordinate geo;
    public Boolean isActive;

}
