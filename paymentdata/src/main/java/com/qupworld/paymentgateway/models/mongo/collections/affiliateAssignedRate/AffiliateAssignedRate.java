package com.qupworld.paymentgateway.models.mongo.collections.affiliateAssignedRate;


import org.bson.types.ObjectId;

/**
 * Created by hoangnguyen on 4/19/17.
 */
public class AffiliateAssignedRate {
    public ObjectId _id;
    public String fleetId;
    public String carTypeId;
    public String priceType; //sellPrice,buyPrice
    public String type; //General,Regular,Hourly,Flat
    public Currency currency;
    public Rate rate;
    public Zone zone;
}
