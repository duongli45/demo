package com.qupworld.paymentgateway.models.mongo.collections.booking;

import java.util.List;

/**
 * Created by myasus on 9/6/21.
 */
public class Point {
    public int order;
    public Address address;
    public int seats;
    public int luggage;
    public String status; //['arrived', 'engaged', 'booked', 'accepted']
    public String time; // 'HH:mm'
    public String notes;
    public List<Passenger> passengers;
}
