
package com.qupworld.paymentgateway.models.mongo.collections.fleetfare;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class CancellationPolicy {

    public Boolean isActive;
    public Double duration;
    public Double times;
    public Double value;
    public Boolean onDemand;
    public Boolean inAdvance;
    public Double durationIn;
    public Double penaltyIn;

}
