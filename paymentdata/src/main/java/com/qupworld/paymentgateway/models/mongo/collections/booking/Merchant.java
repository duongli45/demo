package com.qupworld.paymentgateway.models.mongo.collections.booking;

import com.qupworld.paymentgateway.entities.RecipientFare;
import org.json.simple.JSONObject;

/**
 * Created by myasus on 6/25/20.
 */
public class Merchant {
    public RecipientFare fareDetails;
    public int order;
    public String name;
    public String phone;
    public String merchantId;
    public boolean isPreferred;
    public Address address;
    public String[] collectedPhotos;
    public JSONObject menuData;
    public MerchantAdmin merchantAdmin;
}
