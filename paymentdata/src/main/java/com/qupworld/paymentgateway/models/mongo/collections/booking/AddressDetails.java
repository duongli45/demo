package com.qupworld.paymentgateway.models.mongo.collections.booking;

/**
 * Created by myasus on 3/26/20.
 */
public class AddressDetails {
    public String floor;
    public String room;
    public String pointOfInterest;
    public String streetNumber;
    public String route;
    public String postalCode;
    public String country;
    public String cityName;
    public String political;
    public String countryCode;
}