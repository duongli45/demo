package com.qupworld.paymentgateway.models.mongo.collections.wallet;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.BalanceByCurrency;

import javax.annotation.Generated;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@Generated("org.jsonschema2pojo")
public class MerchantWallet {

    public String fleetId;
    public String merchantId;
    public AmountByCurrency creditWallet;
    public BalanceByCurrency cashWallet;
}
