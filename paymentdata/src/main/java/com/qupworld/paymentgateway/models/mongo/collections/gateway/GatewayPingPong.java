
package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GatewayPingPong {

    public String fleetId;
    public Boolean isSystem;
    public String environment;
    public String clientId;
    public String accId;
    public String salt;
    public Boolean isActive;

}
