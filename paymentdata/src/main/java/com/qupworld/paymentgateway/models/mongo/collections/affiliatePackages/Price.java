package com.qupworld.paymentgateway.models.mongo.collections.affiliatePackages;

/**
 * Created by hoangnguyen on 11/28/18.
 */
public class Price {
    public double basedFee;
    public double extraDuration;
    public double extraDistance;
}
