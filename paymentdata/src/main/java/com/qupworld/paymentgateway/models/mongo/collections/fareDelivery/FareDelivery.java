
package com.qupworld.paymentgateway.models.mongo.collections.fareDelivery;

import com.qupworld.paymentgateway.models.mongo.collections.CancellationPolicy;
import com.qupworld.paymentgateway.models.mongo.collections.NoShow;
import org.bson.types.ObjectId;

import javax.annotation.Generated;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class FareDelivery {

    public ObjectId _id;
    public String fleetId;
    public String name;
    public Double firstDistanceFrom;
    public Double firstDistanceTo;
    public Double secondDistanceFrom;
    public Double secondDistanceTo;
    public Double thirdDistanceFrom;
    public Double thirdDistanceTo;
    public String feeFirstDistanceType;
    public String feeSecondDistanceType;
    public String feeThirdDistanceType;
    public String feeAfterThirdDistanceType;
    public Boolean isActive;
    public List<FeesByCurrency> feesByCurrencies;
}
