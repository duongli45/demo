
package com.qupworld.paymentgateway.models.mongo.collections.pricingplan;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class TechFee {

    public Double commandCenter;
    public Double paxApp;
    public Double webBooking;
    public Double mDispatcher;
    public Double partner;
    public Double carHailing;
    public Double kiosk;

}
