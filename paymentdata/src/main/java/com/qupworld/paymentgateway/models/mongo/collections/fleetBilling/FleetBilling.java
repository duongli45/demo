
package com.qupworld.paymentgateway.models.mongo.collections.fleetBilling;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class FleetBilling {

    public String fleetId;
    public Integer month;
    public Integer year;
    // count diver/vehicle/ticket
    public String type; //driver/vehicle/ticket
    public String typeId; //driverId/plateNumber/bookId
    public String typeName; //driverName/plateNumber/bookId
    // transaction info
    public Double fare;

}
