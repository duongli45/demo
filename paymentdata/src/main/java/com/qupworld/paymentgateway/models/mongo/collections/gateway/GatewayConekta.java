
package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import org.bson.types.ObjectId;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GatewayConekta {

    public ObjectId _id;
    public String fleetId;
    public Boolean isSystem;
    public String environment;
    public String apiKey;
    public String secretKey;
    public Boolean isActive;

}
