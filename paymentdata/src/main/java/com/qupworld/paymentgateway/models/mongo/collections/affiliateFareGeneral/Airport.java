
package com.qupworld.paymentgateway.models.mongo.collections.affiliateFareGeneral;


public class Airport {

    public Double fromAirport;
    public Double toAirport;
    public Boolean fromAirportActive;
    public Boolean toAirportActive;

}
