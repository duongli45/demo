package com.qupworld.paymentgateway.models.mongo.collections.account;

/**
 * Created by myasus on 3/6/20.
 */
public class Profile {
    public int profileStatus;
    public String idNumber;
    public String dob;
    public int gender;
    public Address address;
}
