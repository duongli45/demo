
package com.qupworld.paymentgateway.models.mongo.collections.intercityRoute;

import javax.annotation.Generated;
import java.util.Set;

@Generated("org.jsonschema2pojo")
public class Zone {
    public String zoneId;
    public String name;
    public String type;
    public Set<double[][]> coordinates;
}
