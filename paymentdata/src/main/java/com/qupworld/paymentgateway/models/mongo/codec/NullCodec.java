package com.qupworld.paymentgateway.models.mongo.codec;

import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

public class NullCodec implements Codec<Void> {

    @Override
    public Void decode(BsonReader reader, DecoderContext decoderContext) {
        // When decoding, return null directly
        return null;
    }

    @Override
    public void encode(BsonWriter writer, Void value, EncoderContext encoderContext) {
        // When encoding, write a special value or do nothing, depending on your requirements
        // For example, you could write a special field to represent null
        writer.writeNull();
    }

    @Override
    public Class<Void> getEncoderClass() {
        return Void.class;
    }
}
