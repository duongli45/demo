package com.qupworld.paymentgateway.models.mongo.collections.wallet;

import javax.annotation.Generated;

/**
 * Created by thuanho on 22/02/2021.
 */
@Generated("org.jsonschema2pojo")
public class WalletTnG {

    public String fleetId;
    public String environment;
    public String clientId;
    public String clientSecret;
    public String merchantId;
    public String productCode;
    public String publicKey;
}