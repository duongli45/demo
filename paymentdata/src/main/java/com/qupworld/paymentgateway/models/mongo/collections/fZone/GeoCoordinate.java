
package com.qupworld.paymentgateway.models.mongo.collections.fZone;
import javax.annotation.Generated;
import java.util.Set;

@Generated("org.jsonschema2pojo")
public class GeoCoordinate {

    public String type;
    public Set<double[][]> coordinates;

}
