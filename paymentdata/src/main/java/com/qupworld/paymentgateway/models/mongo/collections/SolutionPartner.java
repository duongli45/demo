
package com.qupworld.paymentgateway.models.mongo.collections;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.bson.types.ObjectId;

public class SolutionPartner {

    @SerializedName("_id")
    @Expose
    public ObjectId id;
    @SerializedName("companyName")
    @Expose
    public String companyName;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("website")
    @Expose
    public String website;
    @SerializedName("isActive")
    @Expose
    public Boolean isActive;

}
