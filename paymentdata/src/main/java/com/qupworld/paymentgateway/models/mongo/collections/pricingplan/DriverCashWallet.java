package com.qupworld.paymentgateway.models.mongo.collections.pricingplan;

import javax.annotation.Generated;

/**
 * Created by hoangnguyen on 3/14/19.
 */
@Generated("org.jsonschema2pojo")
public class DriverCashWallet {
    public boolean enable;
    public DriverDeposit cashToBank;
    public DriverDeposit cashToCredit;
}
