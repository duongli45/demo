
package com.qupworld.paymentgateway.models.mongo.collections.flatRoutes;

import javax.annotation.Generated;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class ZipCode {

    public List<String> fromZipCode;
    public List<String> toZipCode;

}
