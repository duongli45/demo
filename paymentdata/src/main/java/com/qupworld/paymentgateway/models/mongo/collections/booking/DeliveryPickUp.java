package com.qupworld.paymentgateway.models.mongo.collections.booking;

import javax.annotation.Generated;

/**
 * Created by myasus on 6/25/20.
 */
@Generated("org.jsonschema2pojo")
public class DeliveryPickUp {
    public String name;
    public String phone;
    public Address address;
    public String[] collectedPhotos;
}
