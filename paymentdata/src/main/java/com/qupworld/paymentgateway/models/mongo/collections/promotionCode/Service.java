package com.qupworld.paymentgateway.models.mongo.collections.promotionCode;

/**
 * Created by myasus on 1/4/21.
 */
public class Service {
    public Boolean transport;
    public Boolean shuttle;
    public Boolean intercity;
    public Boolean parcel;
    public Boolean food;
    public Boolean mart;
}
