package com.qupworld.paymentgateway.models.mongo.collections.paxReferral;

import javax.annotation.Generated;
import java.util.Date;

/**
 * Created by thuanho on 31/08/2020.
 */
@Generated("org.jsonschema2pojo")
public class PaxReferral {

    public String fleetId;
    public boolean isExpired;
    public Date fromDate;
    public Date expiryDate;
    public boolean isLimitInvitation;
    public int limitedUser;
    public boolean isActive;
    public Referer refererGet;
    public Referee refereeGet;
}
