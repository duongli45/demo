package com.qupworld.paymentgateway.models.mongo.collections.wallet;

/**
 * Created by thuanho on 24/08/2022.
 */
public class WalletBOG {

    public String fleetId;
    public String environment;
    public String clientId;
    public String clientSecret;
    public String clientINN;
    public String terminalId;
    public String merchantName;
}

