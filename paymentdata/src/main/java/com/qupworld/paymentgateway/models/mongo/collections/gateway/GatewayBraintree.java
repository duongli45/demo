
package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import org.bson.types.ObjectId;

import javax.annotation.Generated;
import java.util.Date;

@Generated("org.jsonschema2pojo")
public class GatewayBraintree {

    public ObjectId _id;
    public String _class;
    public Boolean isSystem;
    public String fleetId;
    public String environment;
    public String merchantID;
    public String publicKey;
    public String privateKey;
    public String merchantAccountID;
    public Boolean payLocalZone;
    public Boolean isActive;
    public Date createdDate;

}
