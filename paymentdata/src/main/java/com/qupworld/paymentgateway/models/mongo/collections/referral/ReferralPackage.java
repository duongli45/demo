package com.qupworld.paymentgateway.models.mongo.collections.referral;

import com.qupworld.paymentgateway.models.mongo.collections.paxReferral.RefereePromo;

import java.util.List;

/**
 * Created by qup on 18/10/2018.
 */
public class ReferralPackage {

    public Boolean isLimited;
    public Integer totalBook;
    public Driver driver;
    public List<RefereePromo> promos;
    //public Passenger passenger;

}
