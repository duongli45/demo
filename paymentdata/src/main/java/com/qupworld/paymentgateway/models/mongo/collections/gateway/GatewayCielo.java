
package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import org.bson.types.ObjectId;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GatewayCielo {

    public ObjectId _id;
    public String _class;
    public Boolean isSystem;
    public String fleetId;
    public String environment;
    public String merchantId;
    public String chave;
    public String merchantNumber;
    public String serverURL;
    public Boolean isActive;

}
