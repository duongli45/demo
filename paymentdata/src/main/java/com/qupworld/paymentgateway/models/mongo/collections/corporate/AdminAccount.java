
package com.qupworld.paymentgateway.models.mongo.collections.corporate;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class AdminAccount {

    public String userName;
    public String firstName;
    public String lastName;
    public String phone;
    public String email;

}
