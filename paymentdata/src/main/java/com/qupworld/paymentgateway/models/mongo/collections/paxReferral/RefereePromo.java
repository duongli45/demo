package com.qupworld.paymentgateway.models.mongo.collections.paxReferral;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import java.util.List;

/**
 * Created by thuanho on 31/08/2020.
 */
public class RefereePromo {

    public String promoCode;
    public String promoId;
    public String type; //value = percent/ amount
    public List<AmountByCurrency> valueByCurrencies;
}
