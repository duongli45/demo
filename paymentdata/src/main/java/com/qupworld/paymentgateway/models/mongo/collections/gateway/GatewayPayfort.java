
package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import org.bson.types.ObjectId;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GatewayPayfort {

    public ObjectId _id;
    public String _class;
    public String fleetId;
    public Boolean isSystem;
    public String environment;
    public String merchantIdentifier;
    public String accessCode;
    public String shaType;
    public String requestPhrase;
    public String responsePhrase;
    public boolean enableRED;
    public Boolean isActive;

}
