
package com.qupworld.paymentgateway.models.mongo.collections.booking;

import com.qupworld.paymentgateway.entities.RecipientFare;

import javax.annotation.Generated;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class Fare {

    public double basicFare;
    public double subTotal;
    public double etaFare;
    public double airportFee;
    public double meetDriverFee;
    public double rushHourFee;
    public double techFee;
    public double bookingFee;
    public double tax;
    public double taxSetting;
    public double tip;
    public Boolean normalFare;
    public String route;
    public String routeId;
    public boolean reverseRoute;
    public double otherFees;
    public String promoCode;
    public double promoAmount;
    public double minFare;
    public double tollFee;
    public double parkingFee;
    public double gasFee;
    public int typeRate;
    public double serviceFee;
    public double dynamicSurcharge;
    public double surchargeParameter;
    public Double dynamicFare;
    public String dynamicType;
    public boolean min;
    public boolean shortTrip;
    public Fare estimateFareBuy;
    public List<RecipientFare> recipients;
    public double itemValue;
    public double remainingAmount;
    public double addOnPrice;
    public double totalOrder;
    public double markupPrice;
    public double markupDifference;
    public double creditTransactionFee;
    public double qupPreferredAmount;
    public double qupSellPrice;
    public double totalWoPromoSellPrice;
    public double fleetMarkup;
    public double sellPriceMarkup;
    public double originPromoAmount;
    public String driverEarningType = "default";
    public double editedDriverEarning;
    public double originalDriverEarning;
    public double merchantCommission;
    public String rateType;
    public String rateId;
}
