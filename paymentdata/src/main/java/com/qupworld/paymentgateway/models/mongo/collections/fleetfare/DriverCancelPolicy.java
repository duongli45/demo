package com.qupworld.paymentgateway.models.mongo.collections.fleetfare;

import javax.annotation.Generated;

/**
 * Created by thuanho on 09/08/2022.
 */
@Generated("org.jsonschema2pojo")
public class DriverCancelPolicy {

    public BookType bookNow;
    public BookType bookReservation;
}
