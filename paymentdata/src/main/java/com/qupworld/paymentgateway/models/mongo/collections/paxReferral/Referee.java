package com.qupworld.paymentgateway.models.mongo.collections.paxReferral;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import java.util.List;

/**
 * Created by thuanho on 31/08/2020.
 */
public class Referee {

    //public RefereePromo promo;
    public List<RefereePromo> promos;
    public String type; // value = money/ promo
    public boolean paidByDriver; //default = false - paid by fleet as normal promo flow
    public List<AmountByCurrency> money;
}
