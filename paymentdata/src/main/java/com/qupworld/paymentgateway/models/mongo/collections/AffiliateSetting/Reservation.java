package com.qupworld.paymentgateway.models.mongo.collections.AffiliateSetting;

/**
 * Created by thuanho on 20/10/2022.
 */
public class Reservation {

    public int freeCancellation;
    public String freeCancellationUnit;
    public double penaltyCustomer;
    public double penaltySupplier;
    public double compensationSupplier;
}
