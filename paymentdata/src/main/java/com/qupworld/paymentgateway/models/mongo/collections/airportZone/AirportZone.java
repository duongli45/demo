package com.qupworld.paymentgateway.models.mongo.collections.airportZone;

import com.qupworld.paymentgateway.models.mongo.collections.zone.Geo;
import org.bson.types.ObjectId;

import javax.annotation.Generated;

/**
 * Created by hoang.nguyen on 11/25/16.
 */
@Generated("org.jsonschema2pojo")
public class AirportZone {
    public ObjectId _id;
    public String zoneName;
    public Boolean isActive;
    public Geo geo;
}
