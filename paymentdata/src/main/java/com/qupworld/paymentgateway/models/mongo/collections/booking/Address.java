package com.qupworld.paymentgateway.models.mongo.collections.booking;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myasus on 6/25/20.
 */
public class Address {
    public String cityName;
    public String locationDetails;
    public String zoneId;
    public String zoneName;
    public String from;
    public String address;
    public AddressDetails addressDetails;
    public String instructionLink;
    public List<Double> geo = new ArrayList<Double>();
    public String zipCode;
    public String timezone;
    public String offset;
    public String businessName;
    public String country;
}
