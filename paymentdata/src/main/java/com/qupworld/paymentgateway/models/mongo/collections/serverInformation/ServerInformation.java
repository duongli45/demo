
package com.qupworld.paymentgateway.models.mongo.collections.serverInformation;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class ServerInformation {

    public List<PaymentClearanceHouse> paymentClearanceHouses = new ArrayList<PaymentClearanceHouse>();
    /*public MailServer mailServer;
    public MailTemplate mailTemplate;
    public Inbox inbox;
    public Voice voice;
    public Smsurl smsurl;
    public Track track;*/
    public List<Currency> currencies = new ArrayList<Currency>();
}

