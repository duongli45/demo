
package com.qupworld.paymentgateway.models.mongo.collections;

import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class QueuingArea {

    public ObjectId _id;
    public String fleetId;
    public String name;
    public Double radius;
    public Integer minDriver;
    public List<Object> partnerList = new ArrayList<Object>();
    public List<Object> carList = new ArrayList<Object>();
    public Boolean openQueue;
    public String listPartnerName;
    public Boolean isActive;

}
