
package com.qupworld.paymentgateway.models.mongo.collections.affiliateFareRegular;


public class RateInfo {

    public String name;
    public String priceType;
    public String unitDistance;
    public Currency currency;

}
