package com.qupworld.paymentgateway.models;

import java.util.List;

/**
 * Created by qup on 7/28/16.
 */
public interface RedisDao {
    public boolean addUniqueData(String requestId, String key, String data);
    public void removeUniqueData(String requestId, String key);
    public boolean lockBookingWebhook(String bookId, String requestId);
    public boolean checkLockBookingWebhook(String requestId,String bookId);
    public void unlockBookingWebhook (String bookId, String requestId);
    public boolean addPaySCAData(String requestId,String bookId, String data);
    public String getPaySCAData(String requestId,String bookId);
    public void removePaySCAData(String requestId,  String bookId);

    public boolean addTrackResultSCA(String requestId, String bookId,String time);
    public List<String> getTrackResultSCA(String requestId);
    public void removeTrackResultSCA(String requestId,  String bookId, String time);
    public void addTicket(String requestId, String bookId, String data);
    public void removeTicket(String bookId, String requestId);
    public String getTicket(String bookId, String requestId);
    public boolean lockBooking (String bookId, String requestId);
    public boolean lockTopup (String userId, String requestId);
    public boolean checkLockPayout (String requestId, String fleetId);
    public boolean lockPayout (String requestId, String fleetId);
    public void unlockBooking (String bookId, String requestId);
    public void unlockTopup (String userId, String requestId);
    public void unlockPayout (String requestId, String fleetId);
    public void addCompletedBook(String requestId, String bookId, String data);
    public void removeCompletedBook(String requestId, String date);
    public String getCompletedBook(String bookId, String data, String requestId);
    public void lockCard(String bookId, String data, String requestId);
    public String removeLockCard(String bookId, String requestId);
    public void add3DSData(String requestId, String customerId, String data, String key);
    public String get3DSData(String requestId, String customerId, String key);
    public void remove3DSData(String requestId, String customerId, String key);
    public void addTmpData(String requestId, String key, String data) ;
    public void overrideTmpData(String requestId, String key, String data);
    public void removeTmp(String requestId, String key) ;
    public String getTmp(String requestId, String key) ;
    public boolean addAuthData(String requestId, String bookId, String data);
    public boolean updateAuthData(String requestId, String bookId, String data);
    public void removeAuthData(String requestId, String bookId);
    public String getAuthData(String requestId, String bookId);
    public void addStripeWebhook(String requestId, String pmId, String data);
    public String getStripeWebhook(String requestId, String pmId);
    public void reportMQ(String key, String field);
    public String getReportMQ();
    public void addSettlementData(String requestId, String key, String data);
    public String getSettlementData(String requestId, String key);
    public void addAudit(String requestId, String date, String value);
    public int countTicket(String requestId, String date);
    public double sumTotalTicket(String requestId, String date);
    public void removeAudit(String requestId, String date);
    public void deleteReportMQ();
    public List<String> getListBooks(String requestId, String date);
    public void cacheIntercityData(String requestId, String tripId, String value);
    public int countIntercityTicket(String requestId, String tripId);
    public double sumTotalIntercityTicket(String requestId, String tripId);
    public List<String> getListIntercityBooks(String requestId, String tripId);
    public void removeCacheIntercity(String requestId, String tripId);
    public boolean cacheData(String requestId, String key, String value, int day);
    public boolean cacheToken(String requestId, String key, String value);
    public void addInvalidPayout(String requestId, String fleetId, String value);
    public void removeInvalidPayout(String requestId, String fleetId, String value);
    public List<String> getInvalidPaypout(String requestId, String fleetId);
    public void addCacheWalletDetail(String requestId, String key, String value);
    public void addCacheWalletList(String requestId, String key, String value);
    public String getCacheWalletDetail(String requestId, String key);
    public void deleteCacheWalletDetail(String requestId, String key);
    public void deleteCacheWalletList(String requestId, String key, String orderId);
    public boolean addAuthUser(String requestId, String userId, String data);
    public String getAuthUser(String requestId, String userId);
    public boolean addCacheWallet(String requestId, String internalId, String externalId, String cacheKey, String cacheData);
    public String getCacheByKey(String requestId, String cacheKey);
    public List<String> getCacheKeyFromMapping(String requestId, String mapKey);
    public int removeCacheWallet(String requestId, String internalId, String externalId, String cacheKey);
    public int removeCacheWalletKey(String requestId, String mapKey, String cacheKey);
    public List<String> getCacheWallet(String requestId, String key);
    public int cacheStripeTransaction(String requestId, String bookId, String cacheData);
    public List<String> getStripeCaptured(String requestId, String bookId);
    public int removeStripeCaptured(String requestId, String bookId, String transactionId);
    public int removeStripeCached(String requestId, String bookId);
    public boolean addAuthData(String requestId, String key, String value, int day);
    public boolean markAsCompleted(String requestId, String bookId);
    public boolean checkIfCompleted(String requestId, String bookId);
}
