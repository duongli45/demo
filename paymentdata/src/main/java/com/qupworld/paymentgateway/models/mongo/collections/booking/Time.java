
package com.qupworld.paymentgateway.models.mongo.collections.booking;

import javax.annotation.Generated;
import java.util.Date;

@Generated("org.jsonschema2pojo")
public class Time {

    public Date created;
    public Date pickUpTime;
    public Date pending;
    public Date queue;
    public Date offered;
    public Date booked;
    public Date engaged;
    public Date droppedOff;
    public Date expectedPickupTime;
    public Date arrived;

}
