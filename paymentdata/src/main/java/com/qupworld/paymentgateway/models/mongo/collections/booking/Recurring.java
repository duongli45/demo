package com.qupworld.paymentgateway.models.mongo.collections.booking;

import java.util.List;

/**
 * Created by myasus on 9/6/21.
 */
public class Recurring {
    public String batchId;
    public String mode; //['recurring', 'single']
    public String fromDate;
    public String toDate;
    public String pickupTime;
    public WeekDays weekDays;
    public List<String> selectedDates;
}