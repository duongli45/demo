
package com.qupworld.paymentgateway.models.mongo.collections.serverInformation;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class PaymentClearanceHouse {

    public String type;
    public String key;
    public Boolean limitRetry;
    public Integer numberOfRetry;
    public Integer lockDuration;
    public Boolean isActive;
    public List<SubType> subTypes = new ArrayList<SubType>();

}
