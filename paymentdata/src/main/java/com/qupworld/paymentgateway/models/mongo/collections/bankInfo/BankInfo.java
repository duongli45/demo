package com.qupworld.paymentgateway.models.mongo.collections.bankInfo;

import javax.annotation.Generated;

/**
 * Created by qup on 21/11/2019.
 */
@Generated("org.jsonschema2pojo")
public class BankInfo {

    public String gateway;
    public String bankName;
    public String bankCode;
    public String environment;
    public String country;
}
