
package com.qupworld.paymentgateway.models.mongo.collections.account;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class CorporateInfo {
    public int status;
    public String corpId;
    public String corpDivision;
    public String costCentre;
    public String corpPO;
    public String managerName;
    public String managerEmail;
    public String title;
    public String department;

}
