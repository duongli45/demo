package com.qupworld.paymentgateway.models.mongo.collections.promotionCode;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;
import org.bson.types.ObjectId;

import javax.annotation.Generated;
import java.util.Date;
import java.util.List;

/**
 * Created by hoang.nguyen on 11/24/16.
 */
@Generated("org.jsonschema2pojo")
public class PromotionCode {

    public ObjectId _id;
    public String fleetId;
    public String promotionCode;
    public Campaign campaign;
    public String type;
    public List<AmountByCurrency> valueByCurrencies;
    public Date validFrom;
    public Date validTo;
    public String promoStatus;
    public boolean newCustomer;
    public boolean referredCustomers;
    public String notes;
    public boolean isActive;
    public BudgetLimit budgetLimit;
    public Quantity totalUsesLimit;
    public Quantity quantity;
    public Quantity totalUsesLimitPerDay;
    public Quantity quantityLimitPerDay;
    public List<String> zoneId;
    public boolean applyVerifiedCustomers;
    public Quantity quantityLimitPerMonth;
    public TotalUsesLimit valueLimitPerUse;
    public Service service;
    public boolean scheduleEnable;
    public boolean keepMinFee;
    public List<Schedule> schedules;
    public Quantity quantityLimitPerYear;
    public BudgetLimit minimumEstFareApply;
    public List<Object> paymentMethodsApply;
    public String termAndCondition;
    public BudgetLimit budgetLimitDay;
    public BudgetLimit budgetLimitMonth;
    public BudgetLimit budgetLimitYear;
    public boolean isCorporateCustomers;
    public List<String> corporateId;
    public boolean applyLimitFinished;

}
