
package com.qupworld.paymentgateway.models.mongo.collections.fleet;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class EmailServer {

    public String email;
    public String password;
    public String port;
    public String smtpServer;
    public Boolean isAuthenticated;
    public Boolean isStarttlsEnable;
    public Boolean isSSLEnable;
    public String option;
    public Boolean isDefaultServer;

}
