package com.qupworld.paymentgateway.models.mongo.collections.dynamicFare;

import com.qupworld.paymentgateway.models.mongo.collections.flatRoutes.ZoneData;
import org.bson.types.ObjectId;

/**
 * Created by myasus on 7/10/19.
 */
public class DynamicFare {
    public ObjectId _id;
    public String fleetId;
    public String zoneId;
    public String name;
    public String type;
    public double parameter;
    public boolean isActive;
    public ZoneData geo;
}
