
package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GatewayBluefin {

    public String _class;
    public String fleetId;
    public Boolean isSystem;
    public String environment;
    public String accountId;
    public String accessKey;
    public String accountId1;
    public String accessKey1;
    public String serverURL;
    public Boolean isActive;

}
