package com.qupworld.paymentgateway.models.mongo.collections.promotionCode;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import java.util.List;

/**
 * Created by hoangnguyen on 7/19/17.
 */
public class BudgetLimit {
    public boolean isLimited;
    public List<AmountByCurrency> valueByCurrencies;
    public double value;
}
