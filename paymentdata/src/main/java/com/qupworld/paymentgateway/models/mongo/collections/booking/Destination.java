
package com.qupworld.paymentgateway.models.mongo.collections.booking;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Destination {

    public String businessName;
    public String zipCode;
    public List<Double> geo = new ArrayList<Double>();
    public String offset;
    public String timezone;
    public String address;
    public AddressDetails addressDetails;
}
