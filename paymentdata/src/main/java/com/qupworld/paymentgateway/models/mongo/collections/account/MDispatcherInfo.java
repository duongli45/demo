
package com.qupworld.paymentgateway.models.mongo.collections.account;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import javax.annotation.Generated;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class MDispatcherInfo {

    public String commissionType;
    //public Double value;
    public PartnerType partnerType;
    public List<AmountByCurrency> commissionByCurrencies;

}
