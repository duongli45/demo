package com.qupworld.paymentgateway.models.mongo.collections.booking;

import com.qupworld.paymentgateway.entities.RecipientFare;
import org.json.simple.JSONObject;

/**
 * Created by myasus on 6/25/20.
 */
public class Recipient {
    public RecipientFare fareDetails;
    public int order = 1;
    public String status; //  booked, arrived, delivered, completed, failed
    public String name;
    public String phone;
    public Address address;
    public String[] deliveredPhotos;
    public JSONObject menuData;
    public String orderId;
    public FinishedInfo finishedInfo;
}
