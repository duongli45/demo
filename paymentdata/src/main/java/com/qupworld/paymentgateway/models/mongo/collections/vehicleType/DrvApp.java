package com.qupworld.paymentgateway.models.mongo.collections.vehicleType;

import javax.annotation.Generated;

/**
 * Created by hoang.nguyen on 11/29/16.
 */
@Generated("org.jsonschema2pojo")
public class DrvApp {
    public boolean enableEditFare;
    public boolean drvForceMeter;
    public boolean drvNotForceMeter;
    public boolean hidePrice;
}
