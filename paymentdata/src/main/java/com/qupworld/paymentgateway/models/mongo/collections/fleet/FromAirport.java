
package com.qupworld.paymentgateway.models.mongo.collections.fleet;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class FromAirport {

    public Boolean flightInfo;
    public Boolean meetDriver;
    public Boolean isActive;

}
