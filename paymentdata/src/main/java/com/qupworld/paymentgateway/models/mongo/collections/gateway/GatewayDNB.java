
package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import org.bson.types.ObjectId;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GatewayDNB {

    public ObjectId _id;
    public String fleetId;
    public String environment;
    public String apiKey;
    public String appId;
    public String psuSSN;
    public String psuBBAN;
    public boolean isActive;
    public boolean requireIBAN;

}
