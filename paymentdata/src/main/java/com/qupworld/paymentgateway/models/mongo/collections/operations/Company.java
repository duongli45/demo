package com.qupworld.paymentgateway.models.mongo.collections.operations;

/**
 * Created by qup on 7/25/17.
 */
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Company {

    public String companyId;
    public String companyName;
}
