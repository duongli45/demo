
package com.qupworld.paymentgateway.models.mongo.collections.vehicleType;

import org.bson.types.ObjectId;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class FareNormal {

    public ObjectId _id;
    public String nameFare;
    public Boolean isActive;

}
