
package com.qupworld.paymentgateway.models.mongo.collections.account;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Device {

    public Object type;
    public Object vendor;
    public Object model;

}
