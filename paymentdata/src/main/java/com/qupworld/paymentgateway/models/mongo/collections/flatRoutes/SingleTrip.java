package com.qupworld.paymentgateway.models.mongo.collections.flatRoutes;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import javax.annotation.Generated;
import java.util.List;

/**
 * Created by hoang.nguyen on 1/6/17.
 */
@Generated("org.jsonschema2pojo")
public class SingleTrip {
    //public double departureRoute;
    public boolean enableReturnRoute;
    //public double returnRoute;
    public Limitation limitation;
    public List<AmountByCurrency> departureRouteByCurrencies;
    public List<AmountByCurrency> returnRouteByCurrencies;
}
