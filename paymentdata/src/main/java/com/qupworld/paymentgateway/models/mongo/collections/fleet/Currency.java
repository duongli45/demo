
package com.qupworld.paymentgateway.models.mongo.collections.fleet;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Currency {

    public String iso;
    public String symbol;

}
