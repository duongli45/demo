package com.qupworld.paymentgateway.models.mongo.collections.booking;

/**
 * Created by myasus on 4/14/20.
 */
public class IntercityInfo {
    public String routeId;
    public String routeName;
    public String routeFromZone;
    public String routeToZone;
    public String routeFromZoneId;
    public String routeToZoneId;
    public String tripType;
    public int routeNumber; // enum: [1,2]
}