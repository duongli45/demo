package com.qupworld.paymentgateway.models;

import com.qupworld.config.ServerConfig;
import com.pg.util.CommonUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.exceptions.JedisException;

import java.util.*;

/**
 * Created by qup on 7/19/16.
 * Data Access Layer
 */
public class RedisImpl implements RedisDao {

    private static final String LOCK = "lock:";
    private static final String LOCK_WEBHOOK = "lockBookingWebhook:";
    private static final String TICKET = "ticket:";
    private static final String AUTHDATA = "stripeauth:";
    private static final String WEBHOOKDATA = "webhookdata:";
    private static final String TMP = "tmp:";
    private static final String SETTLEMENT = "settlement:";
    private static final String COMPLETEDBOOK = "completedBook";
    private static final String AUDIT = "audit:";
    private static final String RAZERPAY = "razerPay";
    private static final String WALLET = "wallet:";
    private static final String INTERCITY = "intercity:";
    private static final String LOCKCARD = "lockCard:";
    private static final String PAYOUT = "payout:";
    private static final String SCA_TRACK = "scaTrack:";
    private static final String SCA_TRACK_BOOKID = "scaTrackBookId:";
    private static final String AUTH_API = "authAPI:";
    private static final int EXPIRY_10D = 864000; // be expired after 10 days: 10*24*60*60 = 864000
    private static final int EXPIRY_10M = 600; // be expired after 10 mins: 10*60 = 600
    private static final int EXPIRY_30M = 1800; // be expired after 30 mins: 30*60 = 1800
    private static final int EXPIRY_3D = 259200; // be expired after 3 days: 3*24*60*60 = 259200
    private static final int EXPIRY_30D = 2592000; // be expired after 30 days: 30*24*60*60 = 2592000

    private final static Logger logger = LogManager.getLogger(RedisImpl.class);
    private static JedisPool pool = null;

    public RedisImpl() {
        this.createPool();
    }

    public void createPool() {
        try {
            if (pool == null) {
                logger.debug(" - ConnectionPool is null - create new pool!!!!");
                System.out.println(" - ConnectionPool is null - create new pool!!!!");
                synchronized (logger) {
                    if (pool == null) {
                        JedisPoolConfig config = new JedisPoolConfig();
                        config.setMaxTotal(1000);
                        config.setMaxIdle(10);
                        config.setMinIdle(1);
                        config.setMaxWaitMillis(20000);
                        pool = new JedisPool(config, ServerConfig.redis_host, ServerConfig.redis_port);
                        logger.debug("create pool");
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Jedis getConnection(String requestId) {
        /*
         * logger.debug(requestId + " - " + "Redis connection : " +
         * pool.getNumActive());
         */
        try {
            this.createPool();
            int maxRetry = 2;
            int retry = 0;
            while (retry < maxRetry) {
                Jedis jedis = pool.getResource();
                if (!jedis.isConnected()) {
                    retry++;
                    logger.debug(requestId + " - Redis getConnection ERROR: " + retry);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ignore) {
                    }
                } else {
                    logger.debug(requestId + " - Redis connected ? " + jedis.isConnected());
                    return jedis;
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - " + "Redis getConnection ERROR !!!!");
        }
        return null;
    }

    public void releaseConnection(Jedis redis, String requestId) {
        try {
            redis.close();
        } catch (Exception ex) {
            logger.debug(requestId + " - " + "Release Redis connection ERROR !!!!");
        }
        /*
         * logger.debug(requestId + " - " + "Redis release connection : " +
         * pool.getNumActive());
         */

    }

    public boolean addAuthData(String requestId, String bookId, String data) {
        Jedis redis = null;
        String result = "";
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            // save to redis
            if (!redis.exists(AUTHDATA + bookId)) {
                result = redis.setex(AUTHDATA + bookId, EXPIRY_10M, data);
            }
        } catch (JedisException e) {
            logger.debug(requestId + " - " + bookId + " - exception:" + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return result.equals("OK");
    }

    public boolean updateAuthData(String requestId, String bookId, String data) {
        Jedis redis = null;
        String result = "";
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            result = redis.setex(AUTHDATA + bookId, EXPIRY_10M, data);
        } catch (JedisException e) {
            logger.debug(requestId + " - " + bookId + " - exception:" + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return result.equals("OK");
    }

    public void removeAuthData(String requestId, String bookId) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            redis.del(AUTHDATA + bookId);
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public String getAuthData(String requestId, String bookId) {
        String data = "";
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            if (redis.exists(AUTHDATA + bookId)) {
                data = redis.get(AUTHDATA + bookId);
                logger.debug(requestId + " - " + "getAuthData BookingId:" + bookId + " data : " + data);
            }
        } catch (JedisException e) {
            logger.debug(requestId + " - " + bookId + " - exception:" + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return data;
    }

    public void addStripeWebhook(String requestId, String pmId, String data) {
        Jedis redis = null;
        String result = "";
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            if (!redis.exists(WEBHOOKDATA + pmId)) {
                result = redis.setex(WEBHOOKDATA + pmId, EXPIRY_3D, data);
            }
        } catch (JedisException e) {
            logger.debug(requestId + " - " + pmId + " - exception:" + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public String getStripeWebhook(String requestId, String pmId) {
        String data = "";
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            if (redis.exists(WEBHOOKDATA + pmId)) {
                data = redis.get(WEBHOOKDATA + pmId);
                logger.debug(requestId + " - " + "getAuthData WEBHOOK:" + pmId + " data : " + data);
            }
        } catch (JedisException e) {
            logger.debug(requestId + " - " + pmId + " - exception:" + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return data;
    }

    public void addTicket(String requestId, String bookId, String data) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            String result = redis.setex(TICKET + bookId, EXPIRY_3D, data);
            logger.debug(requestId + " - " + bookId + " - result:" + result);
        } catch (JedisException e) {
            logger.debug(requestId + " - " + bookId + " - exception:" + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public void removeTicket(String bookId, String requestId) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            redis.del(TICKET + bookId);
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public String getTicket(String bookId, String requestId) {
        String data = "";
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            if (redis.exists(TICKET + bookId)) {
                data = redis.get(TICKET + bookId);
                logger.debug(requestId + " - getTicket BookingId: " + bookId + " data : " + data);
            }
        } catch (JedisException e) {
            logger.debug(requestId + " - " + bookId + " - exception:" + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return data;
    }

    public boolean lockBooking(String bookId, String requestId) {

        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            if (!redis.exists(LOCK + bookId)) {
                logger.debug(requestId + " - " + "=== bookId " + bookId + " will be locked for payment !!!");
                redis.setex(LOCK + bookId, EXPIRY_10M, "lock");
                return false;
            } else {
                logger.debug(requestId + " - " + "=== bookId " + bookId + " was being locked - please wait !!!");
                return true;
            }

        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return true;
    }

    public boolean lockTopup(String userId, String requestId) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            if (!redis.exists(LOCK + userId)) {
                logger.debug(requestId + " - " + "=== topup for userId " + userId + " will be locked !!!");
                redis.setex(LOCK + userId, EXPIRY_10M, "lock");
                return false;
            } else {
                logger.debug(
                        requestId + " - " + "=== topup for userId " + userId + " was being locked - please wait !!!");
                return true;
            }

        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return true;
    }

    public boolean checkLockPayout(String requestId, String fleetId) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            return redis.exists(PAYOUT + fleetId);

        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return true;
    }

    public boolean lockPayout(String requestId, String fleetId) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            logger.debug(requestId + " - " + "=== payout will be locked !!!");
            redis.setex(PAYOUT + fleetId, EXPIRY_3D, "lock");

        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return true;
    }

    public void unlockBooking(String bookId, String requestId) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            redis.del(LOCK + bookId);
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        logger.debug(requestId + " - " + "=== release bookId " + bookId + " after payment !!! ");
    }

    public void unlockTopup(String userId, String requestId) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            redis.del(LOCK + userId);
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        logger.debug(requestId + " - " + "=== release topup for userId " + userId + " !!! ");
    }

    public void unlockPayout(String requestId, String fleetId) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            redis.del(PAYOUT + fleetId);
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        logger.debug(requestId + " - " + "=== release lock for driver payout !!! ");
    }

    public void addCompletedBook(String requestId, String bookId, String data) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            if (!redis.sismember(COMPLETEDBOOK + data, bookId)) {
                redis.sadd(COMPLETEDBOOK + data, bookId);
            }
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public void removeCompletedBook(String requestId, String date) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            redis.del(COMPLETEDBOOK + date);
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public String getCompletedBook(String bookId, String data, String requestId) {
        String result = "";
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            if (redis.sismember(COMPLETEDBOOK + data, bookId)) {
                result = "completed";
            }
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return result;
    }

    public void lockCard(String bookId, String data, String requestId) {

        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            if (!redis.exists(LOCKCARD + bookId)) {
                redis.set(LOCKCARD + bookId, data);
            }

        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public String removeLockCard(String bookId, String requestId) {
        String data = "";
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            if (redis.exists(LOCKCARD + bookId)) {
                data = redis.get(LOCKCARD + bookId);
                redis.del(LOCKCARD + bookId);
            }
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return data;
    }

    public void add3DSData(String requestId, String customerId, String data, String key) {
        Jedis redis = null;
        String result = "";
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            // save to redis - auto override exist data
            result = redis.setex(key + ":" + customerId, EXPIRY_3D, data);
        } catch (JedisException e) {
            logger.debug(requestId + " - " + customerId + " - exception:" + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public String get3DSData(String requestId, String customerId, String key) {
        String data = "";
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            if (redis.exists(key + ":" + customerId)) {
                data = redis.get(key + ":" + customerId);
            }
        } catch (JedisException e) {
            logger.debug(requestId + " - " + customerId + " - exception:" + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return data;
    }

    public void remove3DSData(String requestId, String customerId, String key) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            redis.del(key + ":" + customerId);
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public boolean lockBookingWebhook(String bookId, String requestId) {

        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            if (!redis.exists(LOCK_WEBHOOK + bookId)) {
                logger.debug(
                        requestId + " - " + "===LOCK_WEBHOOK  bookId  " + bookId + " will be locked for payment !!!");
                redis.setex(LOCK_WEBHOOK + bookId, EXPIRY_10M, "lock");
                return false;
            } else {
                logger.debug(
                        requestId + " - " + "===LOCK_WEBHOOK bookId " + bookId + " was being locked - please wait !!!");
                return true;
            }

        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return true;
    }

    public boolean checkLockBookingWebhook(String requestId, String bookId) {
        String data = "";
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            if (redis.exists(LOCK_WEBHOOK + bookId)) {
                return true;
            }

        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return false;
    }

    public void unlockBookingWebhook(String bookId, String requestId) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            redis.del(LOCK_WEBHOOK + bookId);
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        logger.debug(requestId + " - " + "===LOCK_WEBHOOK release bookId " + bookId + " after payment !!! ");
    }

    public boolean addPaySCAData(String requestId, String bookId, String data) {
        Jedis redis = null;
        String result = "";
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            // save to redis
            result = redis.setex(SCA_TRACK + bookId, EXPIRY_3D, data);

        } catch (JedisException e) {
            logger.debug(requestId + " - " + bookId + " - exception:" + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return result.equals("OK");
    }

    public String getPaySCAData(String requestId, String bookId) {
        String data = "";
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            if (redis.exists(SCA_TRACK + bookId)) {
                data = redis.get(SCA_TRACK + bookId);
                logger.debug(requestId + " - " + "getPaySCAData key:" + bookId + " data : " + data);
            }
        } catch (JedisException e) {
            logger.debug(requestId + " - " + bookId + " - exception:" + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return data;
    }

    public void removePaySCAData(String requestId, String bookId) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            redis.del(SCA_TRACK + bookId);
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public void addTmpData(String requestId, String key, String data) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            // save to redis
            if (!redis.exists(TMP + key)) {
                String result = redis.setex(TMP + key, EXPIRY_3D, data);
                logger.debug(requestId + " - " + key + " - result:" + result);
            }
        } catch (JedisException e) {
            logger.debug(requestId + " - " + key + " - exception:" + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public boolean addUniqueData(String requestId, String key, String data) {
        Jedis redis = null;
        boolean success = false;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            success = redis.setnx("unique:" + key, data) == 1;
            if (success) {
                redis.expire("unique:" + key, EXPIRY_30M);
                return true;
            }
        } catch (JedisException e) {
            logger.debug(requestId + " - " + key + " - exception:" + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return success;
    }

    public void removeUniqueData(String requestId, String key) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            redis.del("unique:" + key);
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public void overrideTmpData(String requestId, String key, String data) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            // save to redis
            String result = redis.setex(TMP + key, EXPIRY_3D, data);
            logger.debug(requestId + " - " + key + " - result:" + result);
        } catch (JedisException e) {
            logger.debug(requestId + " - " + key + " - exception:" + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public void removeTmp(String requestId, String key) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            redis.del(TMP + key);
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public String getTmp(String requestId, String key) {
        String data = "";
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            if (redis.exists(TMP + key)) {
                data = redis.get(TMP + key);
                logger.debug(requestId + " - getTMP key: " + key + " => data : " + data);
            }
        } catch (JedisException e) {
            logger.debug(requestId + " - " + key + " - exception:" + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return data;
    }

    public void reportMQ(String key, String field) {
        Jedis redis = null;
        try {
            redis = getConnection("");
            redis.select(ServerConfig.redis_number);
            // save to redis
            if (!redis.sismember("PAYMENT_MQ_", key)) {
                redis.sadd("PAYMENT_MQ_", key);
            }
            Long number = redis.hincrBy("PAYMENT_MQ_" + key, field, 1);
            logger.debug("number : " + number);

        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, "");
        }
    }

    public void deleteReportMQ() {
        Jedis redis = null;
        try {
            redis = getConnection("");
            redis.select(ServerConfig.redis_number);
            // remove redis
            redis.del("PAYMENT_MQ_");

        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, "");
        }
    }

    public String getReportMQ() {
        Jedis redis = null;
        try {
            redis = getConnection("");
            redis.select(ServerConfig.redis_number);
            List<Map<String, Object>> array = new ArrayList<Map<String, Object>>();
            Set<String> listAPI = redis.smembers("PAYMENT_MQ_");
            String title = "{\n" +
                    "                    \"title\": \"API\",\n" +
                    "                    \"value\": \"\",\n" +
                    "                    \"short\": true\n" +
                    "                },\n" +
                    "\t\t\t\t{\n" +
                    "                   \n" +
                    "                    \"title\": \"Total ReturnCode\",\n" +
                    "                    \"short\": true\n" +
                    "                }\n,";
            String fieldsString = "            \"fields\": [\n" + title;
            int i = 0;
            for (String apiName : listAPI) {
                Map<String, String> mss1 = redis.hgetAll("PAYMENT_MQ_" + apiName);
                String field1 =

                        "{\n" +
                                "                    \"value\": \"" + i + ". " + apiName + "\",\n" +
                                "                    \"short\": true\n" +
                                "                }\n,"

                ;
                String fileld2 =

                        "{\n" +

                                "                    \"value\": \"" + mss1.toString() + "\",\n" +
                                "                    \"short\": true\n" +
                                "                }\n"

                ;
                fieldsString = fieldsString + field1 + fileld2;
                if (i + 1 < listAPI.size()) {
                    fieldsString = fieldsString + ",";
                }
                i++;

            }
            fieldsString = fieldsString + "            ]";
            return fieldsString;

        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, "");
        }
        return null;
    }

    public void addSettlementData(String requestId, String key, String data) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            // save to redis
            redis.setex(SETTLEMENT + key, EXPIRY_10D, data);
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public String getSettlementData(String requestId, String key) {
        String data = "";
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            if (redis.exists(SETTLEMENT + key)) {
                data = redis.get(SETTLEMENT + key);
            }

        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return data;
    }

    public void addAudit(String requestId, String date, String value) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            redis.sadd(AUDIT + date, value);

        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public int countTicket(String requestId, String date) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            Set values = redis.smembers(AUDIT + date);
            return values.size();
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return 0;
    }

    public double sumTotalTicket(String requestId, String date) {
        Jedis redis = null;
        double sum = 0.0;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);

            Set values = redis.smembers(AUDIT + date);
            for (Object value : values) {
                String[] data = value.toString().split("-");
                sum += Double.parseDouble(data[1]);
            }
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return sum;
    }

    public void removeAudit(String requestId, String date) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            redis.del(AUDIT + date);
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public List<String> getListBooks(String requestId, String date) {
        Jedis redis = null;
        List<String> listBooks = new ArrayList<>();
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            Set values = redis.smembers(AUDIT + date);
            for (Object value : values) {
                String[] data = value.toString().split("-");
                listBooks.add(data[0]);
            }
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return listBooks;
    }

    public void cacheIntercityData(String requestId, String tripId, String value) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            redis.sadd(INTERCITY + tripId, value);
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public int countIntercityTicket(String requestId, String tripId) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            Set values = redis.smembers(INTERCITY + tripId);
            return values.size();
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return 0;
    }

    public double sumTotalIntercityTicket(String requestId, String tripId) {
        Jedis redis = null;
        double sum = 0.0;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);

            Set values = redis.smembers(INTERCITY + tripId);
            for (Object value : values) {
                String[] data = value.toString().split("-");
                sum += Double.parseDouble(data[1]);
            }
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return sum;
    }

    public List<String> getListIntercityBooks(String requestId, String tripId) {
        Jedis redis = null;
        List<String> listBooks = new ArrayList<>();
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            Set values = redis.smembers(INTERCITY + tripId);
            for (Object value : values) {
                String[] data = value.toString().split("-");
                listBooks.add(data[0]);
            }
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return listBooks;
    }

    public void removeCacheIntercity(String requestId, String tripId) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            redis.del(INTERCITY + tripId);
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public static RedisImpl getInstance() {
        return new RedisImpl();
    }

    public void close() {
        pool.close();
    }

    public boolean cacheData(String requestId, String key, String value, int day) {
        Jedis redis = null;
        String result = "";
        int i = 0;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            if (day == 1)
                result = redis.setex(TMP + key, EXPIRY_10M, value);
            if (day == 3)
                result = redis.setex(TMP + key, EXPIRY_3D, value);
            if (day == 10)
                result = redis.setex(TMP + key, EXPIRY_10D, value);
            if (day == 30)
                result = redis.setex(TMP + key, EXPIRY_30D, value);
            if (day > 600)
                result = redis.setex(TMP + key, day, value);
        } catch (JedisException e) {
            logger.debug(requestId + " - cacheData exception: " + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return result.equals("OK");
    }

    public boolean cacheToken(String requestId, String key, String value) {
        Jedis redis = null;
        String result = "";
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            result = redis.setex(TMP + key, EXPIRY_30M, value);
        } catch (JedisException e) {
            logger.debug(requestId + " - cacheToken exception: " + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return result.equals("OK");
    }

    public void addInvalidPayout(String requestId, String fleetId, String value) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            redis.sadd(PAYOUT + fleetId, value);
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public void removeInvalidPayout(String requestId, String fleetId, String value) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            redis.srem(PAYOUT + fleetId, value);
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public List<String> getInvalidPaypout(String requestId, String fleetId) {
        Jedis redis = null;
        List<String> list = new ArrayList<>();
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            Set values = redis.smembers(PAYOUT + fleetId);
            for (Object value : values) {
                list.add(value.toString());
            }
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return list;
    }

    public boolean addTrackResultSCA(String requestId, String bookId, String time) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            // save to redis
            redis.sadd(SCA_TRACK_BOOKID, bookId + "-SCA-" + time);

        } catch (JedisException e) {
            e.printStackTrace();
            return false;
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return true;
    }

    public List<String> getTrackResultSCA(String requestId) {
        Jedis redis = null;
        List<String> list = new ArrayList<>();
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            Set values = redis.smembers(SCA_TRACK_BOOKID);
            for (Object value : values) {
                list.add(value.toString());
            }
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return list;
    }

    public void removeTrackResultSCA(String requestId, String bookId, String time) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            redis.srem(SCA_TRACK_BOOKID, bookId + "-SCA-" + time);
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public void addCacheWalletDetail(String requestId, String key, String value) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            redis.setex(key, EXPIRY_10D, value);
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public void addCacheWalletList(String requestId, String key, String value) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            redis.sadd(key, value);
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public String getCacheWalletDetail(String requestId, String key) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(7);
            return redis.get(key);
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return "";
    }

    public void deleteCacheWalletDetail(String requestId, String key) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            redis.del(key);
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public void deleteCacheWalletList(String requestId, String key, String orderId) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            redis.srem(key, orderId);
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
    }

    public boolean addAuthUser(String requestId, String userId, String data) {
        Jedis redis = null;
        String result = "";
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            result = redis.set(AUTH_API + userId, data);
        } catch (JedisException e) {
            logger.debug(requestId + " - addAuthUser exception: " + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return result.equals("OK");
    }

    public String getAuthUser(String requestId, String userId) {
        Jedis redis = null;
        String data = "";
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            if (redis.exists(AUTH_API + userId)) {
                data = redis.get(AUTH_API + userId);
            }
        } catch (JedisException e) {
            logger.debug(requestId + " - " + userId + " - exception:" + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return data;
    }

    public boolean addCacheWallet(String requestId, String internalId, String externalId, String cacheKey,
            String cacheData) {
        Jedis redis = null;
        String result = "";
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            int add1 = redis.sadd(WALLET + "mapping:" + internalId, cacheKey).intValue();
            if (add1 == 1) {
                redis.expire(WALLET+"mapping:"+internalId, EXPIRY_30M);
                if (externalId.isEmpty()) {
                    result = redis.setex(WALLET + cacheKey, EXPIRY_30M, cacheData);
                } else {
                    int add2 = redis.sadd(WALLET + "mapping:" + externalId, cacheKey).intValue();
                    if (add2 == 1) {
                        redis.expire(WALLET+"mapping:"+externalId, EXPIRY_30M);
                        result = redis.setex(WALLET + cacheKey, EXPIRY_30M, cacheData);
                    }
                }
            }
        } catch (JedisException e) {
            logger.debug(requestId + " - addCacheWallet exception: " + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return result.equals("OK");
    }

    public String getCacheByKey(String requestId, String cacheKey) {
        Jedis redis = null;
        String cacheData = "";
        try {
            redis = getConnection(requestId);
            redis.select(7);
            cacheData = redis.get(WALLET + cacheKey);
        } catch (JedisException e) {
            logger.debug(requestId + " - getCacheByKey exception: " + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return (cacheData == null) ? "" : cacheData;
    }

    public List<String> getCacheKeyFromMapping(String requestId, String mapKey) {
        Jedis redis = null;
        List<String> listKey = new ArrayList<>();
        try {
            redis = getConnection(requestId);
            redis.select(7);
            Set values = redis.smembers(WALLET + "mapping:" + mapKey);
            for (Object value : values) {
                listKey.add(String.valueOf(value));
            }
        } catch (JedisException e) {
            logger.debug(requestId + " - getCacheByUser exception: " + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return listKey;
    }

    public int removeCacheWallet(String requestId, String internalId, String externalId, String cacheKey) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(7);
            logger.debug(requestId + " - key: " + WALLET + "mapping:" + internalId);
            int delete1 = redis.srem(WALLET + "mapping:" + internalId, cacheKey).intValue();
            logger.debug(requestId + " - delete1: " + delete1);
            if (delete1 == 1) {
                if (externalId.isEmpty()) {
                    return redis.del(WALLET + cacheKey).intValue();
                } else {
                    logger.debug(requestId + " - key: " + WALLET + "mapping:" + externalId);
                    int delete2 = redis.srem(WALLET + "mapping:" + externalId, cacheKey).intValue();
                    logger.debug(requestId + " - delete2: " + delete2);
                    if (delete2 == 1) {
                        logger.debug(requestId + " - key: " + WALLET + cacheKey);
                        return redis.del(WALLET + cacheKey).intValue();
                    }
                }
            }
        } catch (JedisException e) {
            logger.debug(requestId + " - removeCacheWallet exception: " + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return 0;
    }

    public int removeCacheWalletKey(String requestId, String mapKey, String cacheKey) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(7);
            return redis.srem(WALLET + "mapping:" + mapKey, cacheKey).intValue();

        } catch (JedisException e) {
            logger.debug(requestId + " - removeCacheWallet exception: " + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return 0;
    }

    public List<String> getCacheWallet(String requestId, String key) {
        Jedis redis = null;
        List<String> listCache = new ArrayList<>();
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            Set values = redis.smembers(key);
            for (Object value : values) {
                listCache.add(value.toString());
            }
        } catch (JedisException e) {
            e.printStackTrace();
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return listCache;
    }

    public int cacheStripeTransaction(String requestId, String bookId, String cacheData) {
        Jedis redis = null;
        int result = 0;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            result = redis.sadd("stripe:" + bookId, cacheData).intValue();
            if (result == 1) {
                redis.expire("stripe:"+bookId, EXPIRY_30M);
            }
            logger.debug(requestId + " - cacheStripeTransaction for bookId " + bookId + ": " + cacheData + " - result: "
                    + result);
        } catch (JedisException e) {
            logger.debug(requestId + " - cacheStripeTransaction exception: " + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return result;
    }

    public List<String> getStripeCaptured(String requestId, String bookId) {
        Jedis redis = null;
        List<String> listKey = new ArrayList<>();
        try {
            redis = getConnection(requestId);
            redis.select(7);
            Set values = redis.smembers("stripe:" + bookId);
            for (Object value : values) {
                listKey.add(String.valueOf(value));
            }
        } catch (JedisException e) {
            logger.debug(requestId + " - getStripeCaptured exception: " + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return listKey;
    }

    public int removeStripeCaptured(String requestId, String bookId, String transactionId) {
        Jedis redis = null;
        int result = 0;
        try {
            redis = getConnection(requestId);
            redis.select(7);
            result = redis.srem("stripe:" + bookId, transactionId).intValue();
        } catch (JedisException e) {
            logger.debug(requestId + " - removeStripeCaptured exception: " + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return result;
    }

    public int removeStripeCached(String requestId, String bookId) {
        Jedis redis = null;
        try {
            redis = getConnection(requestId);
            redis.select(7);
            return redis.del("stripe:" + bookId).intValue();

        } catch (JedisException e) {
            logger.debug(requestId + " - removeCacheWallet exception: " + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return 0;
    }

    public boolean addAuthData(String requestId, String key, String value, int minute) {
        Jedis redis = null;
        String result = "";
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            result = redis.setex(AUTHDATA + key, minute, value);
        } catch (JedisException e) {
            logger.debug(requestId + " - exception:" + CommonUtils.getError(e));
        } finally {
            /// it's important to return the Jedis instance to the pool once you've finished
            /// using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return result.equals("OK");
    }

    public boolean markAsCompleted(String requestId, String bookId) {
        Jedis redis = null;
        String result = "";
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            result = redis.setex(COMPLETEDBOOK + ":" + bookId, EXPIRY_30M, "completed");
        } catch (JedisException e) {
            logger.debug(requestId + " - exception:" + CommonUtils.getError(e));
        } finally {
            ///it's important to return the Jedis instance to the pool once you've finished using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return result.equals("OK");
    }

    public boolean checkIfCompleted(String requestId, String bookId) {
        Jedis redis = null;
        boolean result = false;
        try {
            redis = getConnection(requestId);
            redis.select(ServerConfig.redis_number);
            result = redis.exists(COMPLETEDBOOK + ":" + bookId);
            logger.debug(requestId + " - checkIfCompleted bookId "+bookId+": "+ result);
        } catch (JedisException e) {
            logger.debug(requestId + " - exception:" + CommonUtils.getError(e));
        } finally {
            ///it's important to return the Jedis instance to the pool once you've finished using it
            if (null != redis)
                releaseConnection(redis, requestId);
        }
        return result;
    }
}
