package com.qupworld.paymentgateway.models.mongo.collections.referral;

import java.util.Date;

/**
 * Created by qup on 18/10/2018.
 */
public class Referral {

    public String fleetId;
    public ReferralPackage firstPackage;
    public ReferralPackage secondPackage;
    public ReferralPackage thirdPackage;
    public boolean isExpired;
    public Date expiryDate;
    public boolean isLimitInvitation;
    public Integer limitedUser;
    public boolean isActive;
    public boolean isDistributeIncentiveToDriver; // false = Immediate , true = Periodic
    public DistributeIncentiveToDriver distributeIncentiveToDriver;
    public boolean paidByDriver;
}
