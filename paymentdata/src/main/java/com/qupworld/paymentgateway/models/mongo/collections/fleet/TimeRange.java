package com.qupworld.paymentgateway.models.mongo.collections.fleet;

/**
 * Created by myasus on 12/10/19.
 */
public class TimeRange {
    public String name;
    public String from;
    public String to;
}
