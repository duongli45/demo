package com.qupworld.paymentgateway.models.mongo.codec;

import org.bson.BsonReader;
import org.bson.BsonType;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

import java.util.ArrayList;
import java.util.List;

public class StringArrayCodec implements Codec<String[]> {

    @Override
    public void encode(BsonWriter writer, String[] value, EncoderContext encoderContext) {
        writer.writeStartArray();
        for (String str : value) {
            writer.writeString(str);
        }
        writer.writeEndArray();
    }

    @Override
    public String[] decode(BsonReader reader, DecoderContext decoderContext) {
        reader.readStartArray();
        List<String> list = new ArrayList<>();
        while (reader.readBsonType() != BsonType.END_OF_DOCUMENT) {
            list.add(reader.readString());
        }
        reader.readEndArray();

        return list.toArray(new String[0]);
    }

    @Override
    public Class<String[]> getEncoderClass() {
        return String[].class;
    }
}