
package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import org.bson.types.ObjectId;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GatewayBankOfGeorgia {

    public ObjectId _id;
    public String fleetId;
    public Boolean isSystem;
    public String environment;
    public String clientId;
    public String clientSecret;
}
