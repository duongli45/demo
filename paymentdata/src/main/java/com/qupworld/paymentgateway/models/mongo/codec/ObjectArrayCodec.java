package com.qupworld.paymentgateway.models.mongo.codec;

import org.bson.BsonReader;
import org.bson.BsonType;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

import java.util.ArrayList;
import java.util.List;

public class ObjectArrayCodec implements Codec<Object[]> {

    @Override
    public void encode(BsonWriter writer, Object[] value, EncoderContext encoderContext) {
        writer.writeStartArray();
        for (Object obj : value) {
            // Depending on the actual types in the array, you may need additional logic here
            // to handle different types appropriately.
            if (obj == null) {
                writer.writeNull();
            } else if (obj instanceof String) {
                writer.writeString((String) obj);
            } else if (obj instanceof Integer) {
                writer.writeInt32((Integer) obj);
            } else if (obj instanceof Double) {
                writer.writeDouble((Double) obj);
            } // Add more cases as needed for other types
            // Alternatively, you can use a more general approach like below
            // writer.writeName(obj.getClass().getName());
            // encodeObject(writer, obj, encoderContext);
        }
        writer.writeEndArray();
    }

    @Override
    public Object[] decode(BsonReader reader, DecoderContext decoderContext) {
        reader.readStartArray();
        List<Object> list = new ArrayList<>();
        while (reader.readBsonType() != BsonType.END_OF_DOCUMENT) {
            switch (reader.getCurrentBsonType()) {
                case NULL:
                    reader.readNull();
                    list.add(null);
                    break;
                case STRING:
                    list.add(reader.readString());
                    break;
                case INT32:
                    list.add(reader.readInt32());
                    break;
                case DOUBLE:
                    list.add(reader.readDouble());
                    break;
                // Add cases for other types as needed
                default:
                    throw new IllegalArgumentException("Unsupported BSON type: " + reader.getCurrentBsonType());
            }
        }
        reader.readEndArray();
        return list.toArray(new Object[0]);
    }

    @Override
    public Class<Object[]> getEncoderClass() {
        return Object[].class;
    }

}
