package com.qupworld.paymentgateway.models.mongo.collections.wallet;

import javax.annotation.Generated;

/**
 * Created by thuanho on 22/02/2021.
 */
@Generated("org.jsonschema2pojo")
public class WalletPayDunya {

    public String fleetId;
    public String environment;
    public String masterKey;
    public String publicKey;
    public String secretKey;
    public String token;
}
