package com.qupworld.paymentgateway.models.mongo.collections.paxPromoList;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;
import org.bson.types.ObjectId;

import javax.annotation.Generated;
import java.util.Date;
import java.util.List;

/**
 * Created by hoangnguyen on 7/21/17.
 */
@Generated("org.jsonschema2pojo")
public class PaxPromoList {
    public ObjectId _id;
    public String fleetId;
    public String promotionCode;
    public String promoCodeId;
    public String type;
    public Double value;
    public Date validFrom;
    public Date validTo;
    public String notes;
    public String userId;
    public String promoCustomerType;
    public Date createdDate;
    public List<AmountByCurrency> valueByCurrencies;
    public boolean applyVerifiedCustomers;
    public String termAndCondition;

}
