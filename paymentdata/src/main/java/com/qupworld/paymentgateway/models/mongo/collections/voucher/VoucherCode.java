package com.qupworld.paymentgateway.models.mongo.collections.voucher;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;
import org.bson.types.ObjectId;

import java.util.Date;
import java.util.List;

/**
 * Created by thuanho on 12/01/2021.
 */
public class VoucherCode {

    public ObjectId _id;
    public String fleetId;
    public String voucherCode;
    public Campaign campaign;
    public List<AmountByCurrency> valueByCurrencies;
    public String isUsed;
    public Date validFrom;
    public Date validTo;
    public boolean isActive;
}
