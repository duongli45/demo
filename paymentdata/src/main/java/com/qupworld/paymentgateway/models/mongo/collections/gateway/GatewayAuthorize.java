
package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GatewayAuthorize {

    public String _class;
    public String fleetId;
    public Boolean isSystem;
    public String environment;
    public String apiLoginId;
    public String transactionKey;
    public Boolean isActive;

}
