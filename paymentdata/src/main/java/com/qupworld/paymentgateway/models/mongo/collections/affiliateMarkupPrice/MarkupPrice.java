package com.qupworld.paymentgateway.models.mongo.collections.affiliateMarkupPrice;

/**
 * Created by thuanho on 22/10/2022.
 */
public class MarkupPrice {

    public String type;
    public double value;
}
