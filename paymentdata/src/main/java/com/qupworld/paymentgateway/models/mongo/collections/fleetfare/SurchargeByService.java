package com.qupworld.paymentgateway.models.mongo.collections.fleetfare;

import javax.annotation.Generated;
import java.util.List;

/**
 * Created by myasus on 6/2/21.
 */
@Generated("org.jsonschema2pojo")
public class SurchargeByService {
    public String typeRate;
    public double value;
}
