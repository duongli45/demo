package com.qupworld.util;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.qupworld.paymentgateway.entities.PaymentSettingMQ;

import java.util.concurrent.TimeUnit;

public class CacheUtil {
    private static Object flag = "FLAG";
    private static Cache<String, PaymentSettingMQ> cachePaymentSettingMQ = null;
    public Cache<String, PaymentSettingMQ> getCachePaymentSettingMQ() {
        try {
            synchronized(flag) {
                if (cachePaymentSettingMQ == null) {
                    cachePaymentSettingMQ = Caffeine.newBuilder()
                            .expireAfterWrite(999999, TimeUnit.MINUTES)
                            .maximumSize(10000)
                            .build();

                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
        return cachePaymentSettingMQ;

    }
}
