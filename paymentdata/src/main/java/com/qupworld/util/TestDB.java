package com.qupworld.util;

import com.google.gson.Gson;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.pg.util.CommonUtils;
import com.qupworld.paymentgateway.models.mongo.codec.*;
import com.qupworld.paymentgateway.models.mongo.collections.vehicleType.VehicleType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;

import static com.mongodb.MongoClientSettings.getDefaultCodecRegistry;
import static org.bson.codecs.configuration.CodecRegistries.fromCodecs;

/**
 * Created by thuan on 29/03/2024.
 */
public class TestDB {


    private static final Logger logger = LogManager.getLogger(TestDB.class);
    private static final Gson gson = new Gson();

    public static void main(String[] args) throws Exception {
        Bson filter = Filters.and(
                Filters.eq("fleetId", "thuan"),
                Filters.eq("vehicleType", "Volvo-XC60")
        );
        VehicleType vehicleType = processRequest(filter, VehicleType.class);
        System.out.println("vehicle = " + gson.toJson(vehicleType));
    }

    private static <T> T processRequest(Bson filter, Class<T> clazz) {
        try {
            MongoDatabase database = getMongoDB();
            MongoCollection<Document> collection = database.getCollection(clazz.getSimpleName(), Document.class);
            Document document = collection.find(filter).first();
            return gson.fromJson(gson.toJson(document), clazz);
        } catch (Exception ex) {
            System.out.println(" - processRequest exception" + CommonUtils.getError(ex));
            return null;
        }
    }


    private static volatile MongoClient client = null;
    private static Object flag = "FLAG";
    public static MongoDatabase getMongoDB() throws Exception{

        CodecProvider pojoCodecProvider = PojoCodecProvider.builder().automatic(true).build();
        CodecRegistry pojoCodecRegistry = CodecRegistries.fromRegistries(
                fromCodecs(new StringArrayCodec()),
                fromCodecs(new ObjectArrayCodec()),
                fromCodecs(new DoubleArrayCodec()),
                fromCodecs(new ObjectCodec()),
                fromCodecs(new NullCodec()),
                fromCodecs(new IntegerCodec()),
                getDefaultCodecRegistry(),
                CodecRegistries.fromProviders(pojoCodecProvider));

        if (client == null) {
            synchronized (flag) {
                if (client == null) {
                    try {
                        String options = "maxPoolSize=200&waitQueueMultiple=10&waitQueueTimeoutMS=2000";
                        String authen = "quppayment:QUpInMyHeart@";
                        options += "&w=1&readPreference=primary";
                        String connectURL = "mongodb://"+authen+"192.168.2.82:27017,192.168.2.84:27017,192.168.2.86:27017/?authSource=admin&authMechanism=SCRAM-SHA-1&replicaSet=mongodb-rs&"+options;
                        //client =  MongoClients.create(new ConnectionString(connectURL));

                        ConnectionString connection = new ConnectionString(connectURL);
                        MongoClientSettings settings = MongoClientSettings.builder()
                                .codecRegistry(pojoCodecRegistry)
                                .applyConnectionString(connection).build();
                        client =  MongoClients.create(settings);

                    } catch (Exception e) {
                        // Handle errors for Exception
                        throw e;
                    }
                }
            }
        }
        return client.getDatabase("qupworld_beta").withCodecRegistry(pojoCodecRegistry);

        /*CodecProvider pojoCodecProvider = PojoCodecProvider.builder().automatic(true).build();
        CodecRegistry pojoCodecRegistry = fromRegistries(getDefaultCodecRegistry(),
                fromCodecs(new StringArrayCodec()),
                fromCodecs(new ObjectArrayCodec()),
                fromCodecs(new DoubleArrayCodec()),
                fromCodecs(new NullCodec()),
                fromCodecs(new IntegerCodec()),
                fromCodecs(new ObjectCodec()),
                fromProviders(pojoCodecProvider)
            );
        return client.getDatabase("qupworld_beta").withCodecRegistry(pojoCodecRegistry);*/

        /*ConnectionString connection = new ConnectionString(uri);
        CodecProvider pojoCodecProvider = PojoCodecProvider.builder().automatic(true).build();
        CodecRegistry pojoCodecRegistry = CodecRegistries.fromRegistries(
                MongoClientSettings.getDefaultCodecRegistry(),
                CodecRegistries.fromProviders(pojoCodecProvider));
        MongoClientSettings options = MongoClientSettings.builder()
                .codecRegistry(pojoCodecRegistry)
                .applyConnectionString(connection).build();
        @SuppressWarnings("null")
        SimpleMongoClientDatabaseFactory factory = new SimpleMongoClientDatabaseFactory(
                MongoClients.create(options), getDatabaseName());
        return factory;*/
    }
}
