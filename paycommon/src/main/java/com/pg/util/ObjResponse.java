package com.pg.util;
import com.google.gson.Gson;

/**
 * Created by qup on 7/18/16.
 */
public class ObjResponse {
    public int returnCode;
    public Object response;
    public Object message;
    public String error;

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

}
