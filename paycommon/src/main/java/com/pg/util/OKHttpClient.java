package com.pg.util;

import okhttp3.*;

import java.io.IOException;
/**
 * Created by qup on 11/05/2020.
 */
public class OKHttpClient {

    public static final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    OkHttpClient client = new OkHttpClient();

    public String post(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(json, JSON);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }catch (Exception ex){
            return "";
        }
    }
}
