package com.pg.util;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Calendar;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 *      <p/>
 *      Description
 */

public class ValidCreditCard {

    public static final int INVALID          = -1;
    public static final int VISA             = 0;
    public static final int MASTERCARD       = 1;
    public static final int AMERICAN_EXPRESS = 2;
    public static final int EN_ROUTE         = 3;
    public static final int DINERS_CLUB      = 4;
    public static final int DISCOVER         = 5;
    public static final int UNIONPAY         = 6;
    public static final int JCB              = 7;
    public static final int VERVE            = 8;

    private static final String [] cardNames =
            {   "Visa" ,
                    "MasterCard",
                    "AmericanExpress",
                    "En Route",
                    "Diner's CLub/Carte Blanche",
                    "Discover",
                    "UnionPay",
                    "JCB",
                    "Verve"
            };

    /**
     * Valid a Credit Card number
     */
    public static boolean validCredit(String number)
            {
        if ( (getCardID(number)) != -1)
            return validCCNumber(number);
        return false;
    }

    /**
     * Get the Card type
     * returns the credit card type
     *      INVALID          = -1;
     *      VISA             = 0;
     *      MASTERCARD       = 1;
     *      AMERICAN_EXPRESS = 2;
     *      EN_ROUTE         = 3;
     *      DINERS_CLUB      = 4;
     *      DISCOVER         = 5;
     *      UNIONPAY         = 6;
     *      JCB              = 7;
     *      VERVE            = 8;
     */
    public static int getCardID(String number) {
        int valid = INVALID;

        String digit1 = number.substring(0,1);
        String digit2 = number.substring(0,2);
        String digit3 = number.substring(0,3);
        String digit4 = number.substring(0,4);
        String digit6 = number.substring(0,6);

        if (isNumber(number)) {
            /* ----
            ** VISA  prefix=4
            ** ----  length=13 or 16  or 19 (can be 15 too!?! maybe)
            */
            if (digit1.equals("4"))  {
                if (number.length() == 13 || number.length() == 16 || number.length() == 19)
                    valid = VISA;
            }
            /* ----------
            ** MASTERCARD  prefix= 51 ... 55
            ** ----------  length= 16
            */
            else if (digit2.compareTo("51")>=0 && digit2.compareTo("55")<=0) {
                if (number.length() == 16)
                    valid = MASTERCARD;
            }
            /* ----
            ** AMEX  prefix=34 or 37
            ** ----  length=15
            */
            else if (digit2.equals("34") || digit2.equals("37")) {
                if (number.length() == 15)
                    valid = AMERICAN_EXPRESS;
            }
            /* -----
            ** ENROU prefix=2014 or 2149
            ** ----- length=15
            */
            else if (digit4.equals("2014") || digit4.equals("2149")) {
                if (number.length() == 15)
                    valid = EN_ROUTE;
            }
            /* -----
            ** DCLUB prefix=300 ... 305 or 36 or 38
            ** ----- length=14
            */
            else if (digit2.equals("36") || digit2.equals("38") ||
                    (digit3.compareTo("300")>=0 && digit3.compareTo("305")<=0)) {
                if (number.length() == 14)
                    valid = DINERS_CLUB;
            }
            /* ----
            ** DISCOVER card prefix = 60
            ** --------      lenght = 16
            */
            else if (digit2.equals("65") || digit4.equals("6011") ||
                    (digit3.compareTo("644")>=0 && digit3.compareTo("649")<=0) ||
                    (digit6.compareTo("622126")>=0 && digit6.compareTo("622925")<=0)) {
                if (number.length() == 16)
                    valid = DISCOVER;
            }
             /* ----
            ** UNIONPAY card prefix = 62,88
            ** --------      lenght = 16
            */
            else if (digit2.equals("62") || digit2.equals("88")) {
                if (number.length() == 16)
                    valid = UNIONPAY;
            }
            /* -----
            ** JCB prefix=3528 ... 3589
            ** ----- length=15 or 16 or 19
            */
            else if (digit4.compareTo("3528")>=0 && digit4.compareTo("3589")<=0) {
                if (number.length() == 15 || number.length() == 16 || number.length() == 19)
                    valid = JCB;
            }
            /* -----
            ** VERVE prefix 506099–506198, 650002–650027
            ** ----- length= 16 or 19
            */
            else if ((digit6.compareTo("506099")>=0 && digit6.compareTo("506198")<=0) ||
                    (digit6.compareTo("650002")>=0 && digit6.compareTo("650027")<=0)) {
                if (number.length() == 16 || number.length() == 19)
                    valid = VERVE;
            }
        }
        return valid;

    }

    public static String getCardType(String number) {
        int id = getCardID(number);
        return (id > -1 && id < cardNames.length ? cardNames[id] : "");
    }

    public static String getCardTypeInFormat(String number) {
        String cardType = getCardType(number);
        if (cardType.equals("Visa"))
            cardType = "VISA";
        else if (cardType.equals("MasterCard"))
            cardType = "MASTERCARD";
        else if (cardType.equals("AmericanExpress"))
            cardType = "AMEX";
        else if (cardType.equals("Discover"))
            cardType = "DISCOVER";
        else if (cardType.equals("UnionPay"))
            cardType = "UNIONPAY";
        return cardType;
    }

    public static boolean isNumber(String n) {
        try  {
            double d = Double.valueOf(n).doubleValue();
            return true;
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String getCardName(int id) {
        return (id > -1 && id < cardNames.length ? cardNames[id] : "");
    }

    public static boolean validCCNumber(String n) {
        try {
            /*
            ** known as the LUHN Formula (mod10)
            */
            int j = n.length();

            String [] s1 = new String[j];
            for (int i=0; i < n.length(); i++) s1[i] = "" + n.charAt(i);

            int checksum = 0;

            for (int i=s1.length-1; i >= 0; i-= 2) {
                int k = 0;

                if (i > 0) {
                    k = Integer.valueOf(s1[i-1]).intValue() * 2;
                    if (k > 9) {
                        String s = "" + k;
                        k = Integer.valueOf(s.substring(0,1)).intValue() +
                                Integer.valueOf(s.substring(1)).intValue();
                    }
                    checksum += Integer.valueOf(s1[i]).intValue() + k;
                }
                else
                    checksum += Integer.valueOf(s1[0]).intValue();
            }
            return ((checksum % 10) == 0);
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /*
    ** For testing purpose
    **
    **   java CCUtils [credit card number] or java CCUtils
    **
    */
    public static void main(String args[]) throws Exception {
        String aCard = "";

        if (args.length > 0)
            aCard = args[0];
        else {
            BufferedReader input =
                    new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Card number : ");
            aCard = input.readLine();
        }
        if (getCardID(aCard) > -1) {
            System.out.println("This card is supported.");
            System.out.println("This a " + getCardName(getCardID(aCard)));
            System.out.println
                    ("The card number " + aCard + " is "
                            + (validCredit(aCard)?" good.":" bad."));
        }
        else
            System.out.println("This card is invalid or unsupported!");
    }

    public static String getCardName(String cardType){

        String cardName = "";
        try{
            System.out.println("cardType: " + cardType);
            if(cardType.equals("V"))
                cardName = cardNames[0];
            else if(cardType.equals("M"))
                cardName = cardNames[1];
            else if(cardType.equals("A"))
                cardName = cardNames[2];
            else if(cardType.equals("DS"))
                cardName = cardNames[5];
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return cardName;
    }

    public static boolean checkCardNumber(String cardNumber) {
        if (cardNumber.length() < 4) {
            return false;
        }
        if (!ValidCreditCard.validCredit(cardNumber)) {
            return false;
        }
        // cardNumber data ok, return 200
        return true;
    }

    public static boolean checkExpiredDate(String expiredDate) throws JsonProcessingException {
        try {
            String[]  arr = expiredDate.split("/");
            int month = Integer.parseInt(arr[0].trim());
            int year = Integer.parseInt(arr[1].trim());
            Calendar now = Calendar.getInstance();
            int currentMonth = now.get(Calendar.MONTH) + 1;
            int currentYear = now.get((Calendar.YEAR));
            if ( month > 12 ||
                 year < currentYear ||
                 (year == currentYear && month <= currentMonth) ||
                 year > (currentYear + 15)
               ) {   // limit expired year is 15 years after
                return false;
            }
        } catch(Exception ex) {
            return false;
        }

        // expiredDate data ok, return 200
        return true;
    }

    public static boolean checkCVV(String cvv) throws JsonProcessingException {
        try {
            if (cvv.length() < 3) {
                return false;
            }
            Integer.parseInt(cvv.trim());
        } catch(Exception ex) {
            return false;
        }
        // cvv data ok
        return true;
    }

    public static boolean checkPostalCode(String postalCode) throws JsonProcessingException {
        if (postalCode.length() < 3 || postalCode.length() > 10) {
            return false;
        }
        // postalCode data ok
        return true;
    }
}
