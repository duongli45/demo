package com.pg.util;

import java.util.Arrays;
import java.util.List;

/**
 * Created by thuanho on 06/01/2021.
 */
public class PaymentMethod {

    // PAYMENT METHOD
    public static final String CREDIT = "credit";
    public static final String CREDIT_MDISPATCHER = "mDispatcherCard";
    public static final String CREDIT_CORPORATE = "corporateCard";

    public static final String CASH = "cash";
    public static final String FLEETCARD = "fleetCard";
    public static final String DIRECTBILLING = "directBilling";
    public static final String PREPAID = "prePaid";
    public static final String APPLEPAY = "applePay";
    public static final String AIRPAY = "airpay";
    public static final String CARD_EXTERNAL = "creditCardExternal";
    public static final String PAXWALLET = "paxWallet";
    public static final String TNG_WALLET = "tngeWallet";
    public static final String MOMO = "momo";
    public static final String CASH_SENDER = "cashBySender";
    public static final String CASH_RECIPIENT = "cashByRecipient";
    public static final String NGANLUONG = "nganluong";
    public static final String BOOST = "boosteWallet";

    public static final String CASH_3RD = "cash3rd";
    public static final String FLEETCARD_3RD = "fleetCard3rd";
    public static final String DIRECTBILLING_3RD = "directBilling3rd";
    public static final String CORPORATE_3RD = "corporateCard3rd";

    public static final List<String> CREDIT_PAYMENT = Arrays.asList(CREDIT, CREDIT_MDISPATCHER, CREDIT_CORPORATE);
    public static final List<String> CASH_PAYMENT = Arrays.asList(CASH, CASH_SENDER, CASH_RECIPIENT);
}
