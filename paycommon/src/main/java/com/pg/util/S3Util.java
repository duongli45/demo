package com.pg.util;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Calendar;

/**
 * Created by duyphan on 12/13/16.
 */
public class S3Util {

    final static Logger logger = LogManager.getLogger(S3Util.class);

    private BasicAWSCredentials credentials;
    private AmazonS3 s3;
    private String bucketName;
    private String requestId;

    public S3Util(String requestId, String accessKey, String secretKey, String bucketName, String region){
        try{
            this.requestId = requestId;
            this.bucketName = bucketName;

            credentials= new BasicAWSCredentials(accessKey, secretKey);

            s3 = AmazonS3ClientBuilder
                    .standard()
                    .withCredentials(new AWSStaticCredentialsProvider(credentials))
                    .withRegion(region)
                    .build();
        }catch(Exception ex){
            Gson gson = new Gson();
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            logger.debug(requestId + " - exception while creating awss3client : " + gson.toJson(errors.toString()));
        }
    }

    public Boolean removeUrlS3(String url){
        /**
         * key should be unique. an whatever key you set will be used to in url path to access the pic.
         */
        try{
            s3.deleteObject(new DeleteObjectRequest(bucketName, url));
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }

    }
    public String uploadToS3(String base64Data,String bookId,String type){

        try{
            byte[] byteArr = org.apache.commons.codec.binary.Base64.decodeBase64((base64Data.substring(base64Data.indexOf(",")+1)).getBytes());

            InputStream inputStream = new ByteArrayInputStream(byteArr);
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(byteArr.length);
            metadata.setContentType("image/png");
            metadata.setCacheControl("public, max-age=31536000");
            Calendar calendar = Calendar.getInstance();

            String url = "images/signature/" + bookId +String.valueOf(calendar.getTimeInMillis()) + "."+type;
            try{
                s3.putObject(new PutObjectRequest(this.bucketName,  url, inputStream, metadata).withCannedAcl(CannedAccessControlList.PublicRead));
            } catch (Exception ex){
                Gson gson = new Gson();
                StringWriter errors = new StringWriter();
                ex.printStackTrace(new PrintWriter(errors));
                String error = gson.toJson(errors.toString());
                logger.debug(requestId + " - uploadToS3 exception : " + error);
                url = "";
            } finally {
                inputStream.close();
            }
            return url;
        } catch (Exception ex){
            Gson gson = new Gson();
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            String error = gson.toJson(errors.toString());
            logger.debug(requestId + " - uploadToS3 exception : " + error);
            return "";
        }

    }

}
