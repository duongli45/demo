package com.pg.util;

import okhttp3.MediaType;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 *      <p/>
 *      Description
 */

public class KeysUtil {

    public static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.##");
    public static final DecimalFormat DECIMAL_FORMAT0 = new DecimalFormat("0.00");
    public static final SimpleDateFormat SDF_DMYHMS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final SimpleDateFormat SDF_DMYHMSTZ = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    public static final SimpleDateFormat SDF_MONGO = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
    public static final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    // Define server link for web service
    public static final String SERVER_URI = "http://localhost:8080/qupservice";
    public static final String BRAINTREE = "Braintree";
    public static final String STRIPE = "Stripe";
    public static final String BLUEFIN = "Bluefin";
    public static final String AUTHORIZE = "Authorize";
    public static final String MERCURY = "Mercury";
    public static final String EASYCARD = "EasyCard";
    public static final String PAYPAL = "Paypal";
    public static final String JETPAY = "JetPay";
    public static final String PEACH = "PeachPayments";
    public static final String CIELO = "Cielo";
    public static final String PAYU = "PayU";
    public static final String CREDITCORP = "CreditCorp";
    public static final String FAC = "FirstAtlanticCommerce";
    public static final String PAYFORT = "Payfort";
    public static final String OMISE = "Omise";
    public static final String PAYCORP = "PayCorp";
    public static final String AVIS = "Avis";
    public static final String MASAPAY = "MasaPay";
    public static final String YEEPAY = "Yeepay";
    public static final String ADYEN = "Adyen";
    public static final String CXPAY = "CXPay";
    public static final String EXPRESSPAY = "Expresspay";
    public static final String FLUTTERWAVE = "Flutterwave";
    public static final String CONEKTA = "Conekta";
    public static final String PAYEASE = "PayEase";
    public static final String SENANGPAY = "Senangpay";
    public static final String GCASH = "GCash";
    public static final String MOLPAY = "MOLPay";
    public static final String PAYDOLLAR = "PayDollar";
    public static final String TnGWallet = "TnGeWallet";
    public static final String TNG = "TnG";
    public static final String FIRSTDATA = "FirstData";
    public static final String AIRPAY = "AirPay";
    public static final String MOMO = "MoMo";
    public static final String NGANLUONG = "NganLuong";
    public static final String EGHL = "eGHL";
    public static final String PINGPONG = "PingPong";
    public static final String BOOST = "Boost";
    public static final String VIPPS = "Vipps";
    public static final String DNB = "DNB";
    public static final String TSYS = "TSYS";
    public static final String AUB = "AUB";
    public static final String YENEPAY = "YenePay";
    public static final String ZAINCASH = "ZainCash";
    public static final String PAYMAYA = "PayMaya";
    public static final String TELEBIRR = "Telebirr";
    public static final String KUSHOK = "Kushok";
    public static final String KSHER = "Ksher";
    public static final String ANY_BANK = "AnyBank";
    public static final String PAYTM = "PayTM";
    public static final String PAYDUNYA = "PayDunya";
    public static final String INDIA_DEFAULT = "IndiaDefault";
    public static final String BOG = "BankOfGeorgia";
    public static final String STRIPE_CONNECT = "StripeConnect";
    public static final String MADA = "MADA";
    public static final String PAYWAY = "PayWay";
    public static final String WINGBANK = "WingBank";
    public static final String ECPAY = "ECPay";
    public static final String ONEPAY = "OnePay";
    public static final List<String> GATEWAY_3DS = Arrays.asList(PAYFORT, PAYCORP, YEEPAY, EXPRESSPAY, FLUTTERWAVE,FIRSTDATA, EGHL, TSYS, PAYMAYA, CXPAY, BOG, MADA, PAYWAY, ECPAY, ONEPAY);
    public static final List<String> GATEWAY_WALLET = Arrays.asList(GCASH, MOLPAY, VIPPS, AUB, YENEPAY, ZAINCASH, TELEBIRR, KSHER, PAYTM, PAYDUNYA, BOG, WINGBANK);
    public static final List<String> GATEWAY_SETTLEMENT = Arrays.asList(STRIPE, JETPAY, MOLPAY, "CIB", DNB, ANY_BANK, INDIA_DEFAULT, WINGBANK);
    public static final List<String> GATEWAY_STANDARD = Arrays.asList(KeysUtil.FIRSTDATA, "");
    public static final List<String> WALLET_PREPAID = Arrays.asList(KeysUtil.VIPPS);
    public static final String THIRD_PARTY_BOOKING = "booking.com";
    public static final String THIRD_PARTY_HOLIDAYTAXIS = "HolidayTaxis";
    public static final String THIRD_PARTY_KARHOO = "Karhoo";
    public static final List<String> THIRD_PARTY = Arrays.asList(THIRD_PARTY_BOOKING, THIRD_PARTY_HOLIDAYTAXIS, THIRD_PARTY_KARHOO);
    public static final String CALLBACK_URL = "https://dispatch.qupworld.com";
    public static final String NOTIFICATION_URL = "/paymentNotificationURL";
    public static final String RETURN_URL = "/paymentReturnURL";
    public static final String MOLPAY_URL = "/paymentMolPayNotificationURL";
    public static final String SETTLEMENT_MOLPAY_URL = "/settlementMolPayNotificationURL";
    public static final String TNG_URL = "/paymentTnGNotificationURL";
    public static final String GCASH_URL = "/paymentGCashNotificationURL";
    public static final String PINGPONG_URL = "/paymentPingPongNotificationURL";
    public static final String MOMO_URL = "/paymentMoMoNotificationURL";
    public static final String VIPPS_URL = "/paymentVippsNotificationURL";
    public static final String DNB_URL = "/paymentDNBNotificationURL";

    public static final List<String> methodIncreasePoint =  Arrays.asList("credit", "razerPay", "gCash", "zainCash", "kushok",
            "telebirr", "ksher", "payTM", "payDunya", "bog", "wingbank");

    public static final List<String> FLEET_BINCHECK = Arrays.asList("taxiblack", "avisbudget", "avisyepay", "avispayment");
    public static final List<String> BBL_BINCHECK = Arrays.asList("450535","473014","454623","454626","454627","454628","454629","454631","454632 ","522985 ","544482","544485","544488","377970","377971","622354","520082");
    public static final String UNIONPAY = "UnionPay";

    // Define default keys for payment environment
    public static final String SANDBOX = "sandbox";
    public static final String PRODUCTION = "production";



    public static final String characterFilter = "[^\\p{L}\\p{M}\\p{N}\\p{P}\\p{Z}\\p{Cf}\\p{Cs}\\s]";

    // Define module locale
    public static final String APPTYPE_PASSENGER = "passenger";
    public static final String APPTYPE_DRIVER = "driver";
    public static final String APPTYPE_PARNER = "partner";
    public static final String APPTYPE_MDISPARCHER = "mDispatcher";
    public static final String TIME_BOOK_ASAP = "asap";
    public static final String TIME_BOOK_NOW = "Now";
    public static final String CORPORATE_ADMIN = "corporateAdmin";

    public static final String COMMISION_AMOUNT = "Amount";

    public static final String PLATFORM_IOS = "iOS";
    public static final String PLATFORM_ANDROID = "android";
    public static final String PLATFORM_WEB = "web";

    public static  final String PAY_OUT = "Payout";
    public static  final String CHARGE = "Charge";

    //Book From key

    public  static  final String web_booking = "Web booking";
    public  static  final String partner = "Partner";
    public  static  final String mDispatcher = "mDispatcher";
    public  static  final String command_center = "CC";
    public  static  final String car_hailing = "Car-hailing";
    public  static  final String kiosk = "Kiosk";
    public  static  final String pax = "paxApp";
    public  static  final String api = "API";
    public  static  final String dash_board = "dmc";
    public  static  final String corp_board = "corp";
    public  static  final String street_sharing = "streetSharing";
    public  static  final String PWA = "PWA";

    public static final String noDriver = "noDriver";
    public static final String canceled = "canceled";
    public static final String reservation = "reservation";
    public static final String notStart = "notStart";
    public static final String timeout = "timeout";

    // PAYMENT METHOD
    public static final String PAYMENT_CARD = "credit";
    public static final String PAYMENT_MDISPATCHER = "mDispatcherCard";
    public static final String PAYMENT_CORPORATE = "corporateCard";

    public static final String PAYMENT_CASH = "cash";
    public static final String PAYMENT_FLEETCARD = "fleetCard";
    public static final String PAYMENT_DIRECTBILLING = "directBilling";
    public static final String PAYMENT_PREPAID = "prePaid";
    public static final String PAYMENT_APPLEPAY = "applePay";
    public static final String PAYMENT_GOOGLEPAY = "googlePay";
    public static final String PAYMENT_AIRPAY = "airpay";
    public static final String PAYMENT_CARD_EXTERNAL = "creditCardExternal";
    public static final String PAYMENT_PAXWALLET = "paxWallet";
    public static final String PAYMENT_TNG_WALLET = "tngeWallet";
    public static final String PAYMENT_MOMO = "momo";
    public static final String PAYMENT_CASH_SENDER = "cashBySender";
    public static final String PAYMENT_CASH_RECIPIENT = "cashByRecipient";
    public static final String PAYMENT_NGANLUONG = "nganluong";
    public static final String PAYMENT_BOOST = "boosteWallet";
    public static final String PAYMENT_VIPPS = "vippseWallet";
    public static final String PAYMENT_EXTERNAL_WALLET = "externalWallet";
    public static final String PAYMENT_COMPLETE_WITHOUT_SERVICE = "completedWithoutService";
    public static final String PAYMENT_DIRECT_INVOICING = "directInvoicing";
    public static final String PAYMENT_PAYMENT_LINK = "paymentLink";

    public static final String PAYMENT_INCIDENT = "incident";
    public static final String PAYMENT_NOSHOW = "noShow";
    public static final String PAYMENT_CANCELED = "canceled";
    public static final String CANCELER_DRIVER = "driver";
    public static final String CANCELER_PASSENGER = "passenger";
    public static final String CANCELER_MDISPATCHER = "mdispatcher";
    public static final String CANCELER_CORPORATE = "CorpAD";

    public static final String PAYMENT_CASH_3RD = "cash3rd";
    public static final String PAYMENT_FLEETCARD_3RD = "fleetCard3rd";
    public static final String PAYMENT_DIRECTBILLING_3RD = "directBilling3rd";
    public static final String PAYMENT_CORPORATE_3RD = "corporateCard3rd";

    public static final List<String> INCOMPLETE_PAYMENT = Arrays.asList(PAYMENT_INCIDENT, PAYMENT_NOSHOW, PAYMENT_CANCELED);
    public static final List<String> CREDIT_PAYMENT = Arrays.asList(KeysUtil.PAYMENT_CARD, KeysUtil.PAYMENT_CORPORATE, KeysUtil.PAYMENT_MDISPATCHER);
    public static final List<String> CASH_PAYMENT = Arrays.asList(KeysUtil.PAYMENT_CASH, KeysUtil.PAYMENT_CASH_SENDER, KeysUtil.PAYMENT_CASH_RECIPIENT);
    public static final List<String> bookNotPax = Arrays.asList("CC", "API","Car-hailing","Web booking","Partner","mDispatcher","Kiosk", "webBooking", "partner", "");
    public static final List<String> NO_DROP_PAYMENT = Arrays.asList("delivery", "parcel");
    // Terminal payment
    public static final String B2BTERMINAL = "B2BTerminal";

    public static final String  UNIT_DISTANCE_IMPERIAL = "mi";

    public static String TOKEN = ""; // store authentication token for submit data to report server
    public static int SIZE = 50;   // define number of record query on database

    // Rate type
    public static final String REGULAR = "Regular";
    public static final String FLAT = "Flat";
    public static final String HOURLY = "Hourly";
    public static final String INTERCITY = "Intercity";
    public static final String DELIVERY = "Delivery";
    public static final String SHARING = "Sharing";
    public static final String GENERAL = "General";
    public static final String SELLPRICE = "sellPrice";
    public static final String BUYPRICE = "buyPrice";

    // String combine key
    public static final String TEMPKEY = "2Au5Ci6Xy5Bo6Qy2";

    // billing status
    public static final String BILLING_PENDING = "Pending";
    public static final String BILLING_DECLINED = "Declined";
    public static final String BILLING_CHARGED = "Charged";

    // income status
    public static final String INCOME_CHARGED = "charged";
    public static final String INCOME_CHARGE_FAILED = "failed";
    public static final String INCOME_REFUNDED = "refunded";
    public static final String INCOME_REFUND_FAILED = "refundFailed";
    public static final String INCOME_REJECTED = "rejected";
    public static final String INCOME_ACCEPTED = "accepted";
    public static final String INCOME_CREDIT = "Credit card";

    // setting email to receipt payment error logs
    public static final String ERROR_MAIL_LIST = "thuan.ho@qupworld.com";

    // withdrawal status
    public static final String WITHDRAWAL_PENDING = "pending";
    public static final String WITHDRAWAL_PROCESSING = "processing";
    public static final String WITHDRAWAL_APPROVED = "approved";
    public static final String WITHDRAWAL_FAILED = "failed";
    public static final String WITHDRAWAL_REJECTED = "rejected";

    public static final String sendMailReceipt = "sendMailReceipt";
    public static final String sendMailTipDetail = "sendMailTipDetail";
    public static final String sendMailReportInternal = "sendMailReportInternal";
    public static final String sendMailTripNotification = "sendMailTripNotification";

    public static final String action_check = "Check";
    public static final String action_add = "Add";
    public static final String action_delete = "Delete";
    public static final String action_add_to_list= "AddToList";
    public static final String URGREN= "URGREN";
    public static final String INFO= "INFO";
    public static final String ERROR= "ERROR";
    public static final String WARNING= "WARNING";

    public static String BOOST_API_TOKEN = ""; // store apiToken from Boost
    public static String VIPPS_AUTH_TOKEN = ""; // store apiToken from Vipps

    public static final List<Integer> transactionFeeMethod = Arrays.asList(2,3,4,7,5,6,12,9,10,23);
}
