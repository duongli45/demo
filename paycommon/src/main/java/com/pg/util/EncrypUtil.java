package com.pg.util;

import java.nio.charset.StandardCharsets;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class EncrypUtil {

    private static String sha512(String rawString, String apiKey) throws Exception {
        final byte[] byteKey = apiKey.getBytes(StandardCharsets.UTF_8);
        Mac sha512Hmac = Mac.getInstance("HmacSHA512");
        SecretKeySpec keySpec = new SecretKeySpec(byteKey, "HmacSHA512");
        sha512Hmac.init(keySpec);
        byte[] macData = sha512Hmac.doFinal(rawString.getBytes(StandardCharsets.UTF_8));
        return java.util.Base64.getEncoder().encodeToString(macData);
    }
}
