package com.pg.util;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.crypto.*;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.X509EncodedKeySpec;
import java.util.Hashtable;
import java.util.Random;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp World.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp World.
 *
 * @author QUp World
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 *      <p/>
 *      Description
 */
public class SecurityUtil {

    final static Logger logger = LogManager.getLogger(SecurityUtil.class);
    private static final String encAlgo = "AES";

    /**
     * return string hash md5
     * @param password
     * @return
     */
    public static String md5(String password){
        try {
            MessageDigest md = MessageDigest.getInstance("md5");
            md.update(password.getBytes());

            byte byteData[] = md.digest();

            //convert the byte to hex format
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch(NoSuchAlgorithmException ex) {
            ex.printStackTrace();
            return "";
        }
    }

    /**
     * decrypt
     * @param encodedString
     * @return
     */
    public static String decrypt(String encodedString){
        String decodedString = "";

        // seperate character
        char seperate = 67;
        String regex = ""+seperate;
        String[] arrEncoded = encodedString.split(regex);
        String split = ""+(char)66;

        for(String iter: arrEncoded){

            String[]arrChar = iter.split(split);

            String cSplit = ""+(char)65;
            String[]arrInfor = arrChar[1].split(cSplit);
            int pLength = arrInfor.length;
            int pIndex = 0;

            for(int i = pLength - 1; i >= 0 ; i--){
                String s= arrInfor[i];
                int c = Integer.parseInt(s);
                pIndex = (pIndex * 249 + c);

            }

            pIndex^= 65535; // 65535

            String[] arrKey = arrChar[0].split(cSplit);
            int length = arrKey.length;
            // get primary number index

            int oNumb = 0; // original numb;
            for(int i = length -1 ; i >= 0 ; i--){
                String s = arrKey[i];
                Integer c = Integer.parseInt(s);
                oNumb = oNumb * 249 + c;

            }
            oNumb^= getPrimaryNumb(pIndex)^ 65535;//65535
            oNumb^= 255; // 255
            char c= (char)oNumb;
            decodedString+= c;

        }
        return decodedString;
    }

    /**
     * encrypt
     * @param inputString
     * @return
     */
    public static String encrypt(String inputString){
        // get 27 random primary number
        // 1 - 26 : for alphabet char
        // 27 for special char
        Hashtable<Integer, Integer> hNumb = new Hashtable<Integer, Integer>();
        for(int i = 0; i < 27; i++){
            boolean isAccept = false;
            while(!isAccept){
                int index = (int)(Math.random()*5000);

                // check if index existed
                if(!hNumb.contains(index)){

                    // revert bit and save to hash table
                    hNumb.put(i, index);

                    // continue
                    isAccept = true;
                }
            }
        }
        int strLength = inputString.length();
        String encodedString = "";

        // seperate character
        char seperate = 67;
        for(int i = 0; i < strLength; i++){

            // calc mapping from character to table primary numb
            int map = -1;
            char c = inputString.charAt(i);
            if('A' <= c && c <= 'Z')
                map = c - 'A';
            else if('a' <= c && c <= 'z')
                map = c - 'a';
            else
                map = 26;

            // inverse character
            c^= 255; // 255
            // xor with inverse primary numb
            int temp = (int) c;
            int pNumb = hNumb.get(map);

            temp^= (getPrimaryNumb(pNumb) ^ 65535);

            // split it and put to string
            while(temp > 0){
                encodedString+= (temp % 249); // 249
                encodedString+= (char)65;
                temp/= 249;
            }
            encodedString+=(char)66;


            pNumb ^= 65535;
            while(pNumb > 0){
                encodedString+= (pNumb % 249);
                encodedString+= (char)65;
                pNumb /= 249;
            }
            // seperate character

            encodedString+=seperate;
        }
        return encodedString;
    }

    public static int getPrimaryNumb(int index){
        if(index >= 5000)
            return -1;

        boolean isOk = false;
        int j = 1;
        int i = 0;
        while(i < 5001)
        {
            isOk = false;
            while(!isOk){
                j++;
                int sqr = (int)Math.sqrt((double)j) + 1;
                isOk = true;
                for(int k = 2; k < sqr; k++){
                    if(j%k == 0){
                        isOk = false;
                        break;
                    }
                }
            }

            if(i == index)
                return j;
            i++;
        }
        return -1;
    }

    public static String encryptProfile(String secretKey, String profile) throws Exception {
        Key key = generateKey(secretKey);
        Cipher c = Cipher.getInstance(encAlgo);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(profile.getBytes());
        String encryptedValue = DatatypeConverter.printBase64Binary(encVal);
        return encryptedValue;
    }

    public static String decryptProfile(String secretKey, String encryptedData) throws Exception {
        Key key = generateKey(secretKey);
        Cipher c = Cipher.getInstance(encAlgo);
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decordedValue = DatatypeConverter.parseBase64Binary(encryptedData);
        byte[] decValue = c.doFinal(decordedValue);
        String decryptedValue = new String(decValue);
        return decryptedValue;
    }
    public static Key generateKey(String secretKey) throws Exception {
        byte[] newKey = secretKey.getBytes();
        //System.out.println(secretKey.getBytes().length);
        Key key = new SecretKeySpec(newKey, encAlgo);
        return key;
    }

    public static void main(String args[]) throws Exception{

        String profile = "4500600000000061c123p12345e12/2020";

        String key = generatorNumber(16);
        System.out.println("key:::"+key+":");
        String ePass = encrypt(key);
        System.out.println("encodeKey:::"+ePass+":");
        System.out.println(":::"+decrypt(ePass));

        System.out.println("encodeProfile:::"+encryptProfile(key, profile)+":");
        System.out.println(":::"+encryptProfile(key, profile));
        System.out.println("randomPw:   "+getRandomPassword());
    }

    public static String generatorNumber(int numChar) {
        String numStr = "";
        Random random = new Random(System.nanoTime());
        for (int i=0; i<numChar; i++){
            int n = Math.abs(random.nextInt(10));
            numStr += String.valueOf(n);
        }
        return numStr;
    }
    public static String getKey() {
        return "gEzzG5O2dDo354cC";
    }
    private static final String NUMBERS = "0123456789";
    private static final String UPPER_ALPHABETS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String LOWER_ALPHABETS = "abcdefghijklmnopqrstuvwxyz";
    private static final int MINLENGTHOFPASSWORD = 16;

    public static String getRandomPassword() {
        StringBuilder password = new StringBuilder();
        int j = 0;
        for (int i = 0; i < MINLENGTHOFPASSWORD; i++) {
            password.append(getRandomPasswordCharacters(j));
            j++;
            if (j > 2) {
                j = 0;
            }
        }
        return password.toString();
    }

    private static String getRandomPasswordCharacters(int pos) {
        Random randomNum = new Random();
        StringBuilder randomChar = new StringBuilder();
        switch (pos) {
            case 0:
                randomChar.append(NUMBERS.charAt(randomNum.nextInt(NUMBERS.length() - 1)));
                break;
            case 1:
                randomChar.append(UPPER_ALPHABETS.charAt(randomNum.nextInt(UPPER_ALPHABETS.length() - 1)));
                break;
            case 2:
                randomChar.append(LOWER_ALPHABETS.charAt(randomNum.nextInt(LOWER_ALPHABETS.length() - 1)));

                break;
        }
        return randomChar.toString();

    }

    public static String encryptThreeDESECB(String src, String key) throws Exception {
        DESedeKeySpec dks = new DESedeKeySpec(key.getBytes("UTF-8"));
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");
        SecretKey securekey = keyFactory.generateSecret(dks);
        Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, securekey);
        byte[] b = cipher.doFinal(src.getBytes());
        return Base64.encodeBase64String(b);
    }

    public static String decryptThreeDESECB(String src, String key) throws Exception {
        byte[] bytesrc = Base64.decodeBase64(src);
        DESedeKeySpec dks = new DESedeKeySpec(key.getBytes("UTF-8"));
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");
        SecretKey securekey = keyFactory.generateSecret(dks);
        Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, securekey);
        byte[] retByte = cipher.doFinal(bytesrc);
        return new String(retByte);
    }

    public static String hashSHA256(String requestId, String hashString) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] bytes = md.digest(hashString.getBytes(StandardCharsets.UTF_8));
            // Convert byte array into signum representation
            BigInteger number = new BigInteger(1, bytes);

            // Convert message digest into hex value
            StringBuilder hexString = new StringBuilder(number.toString(16));

            // Pad with leading zeros
            while (hexString.length() < 32)
            {
                hexString.insert(0, '0');
            }

            String hashValue = hexString.toString();
            if (hashValue.length() < 64)
                hashValue = "0" + hashValue;
            return hashValue;
        } catch (Exception ex) {
            logger.debug(requestId + " - hashSHA256 exception: " + CommonUtils.getError(ex));
        }
        return "";
    }

    /*public static String encryptRSA2048(String requestId, String secretMessage, String base64PublicKey){
        try {
            Cipher encryptCipher = Cipher.getInstance("RSA");
            encryptCipher.init(Cipher.ENCRYPT_MODE, getPublicKey(requestId, base64PublicKey));
            byte[] secretMessageBytes = secretMessage.getBytes(StandardCharsets.UTF_8);

            byte[] encryptedMessageBytes = blockCipher(encryptCipher, secretMessageBytes,Cipher.ENCRYPT_MODE);
            return java.util.Base64.getEncoder().encodeToString(encryptedMessageBytes);
        } catch (Exception ex) {
            logger.debug(requestId + " - encryptRSA2048 exception: " + CommonUtils.getError(ex));
        }
        return "";
    }

    private static PublicKey getPublicKey(String requestId, String base64PublicKey){
        PublicKey publicKey = null;
        try{
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(java.util.Base64.getDecoder().decode(base64PublicKey.getBytes()));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            publicKey = keyFactory.generatePublic(keySpec);
        } catch (Exception ex) {
            logger.debug(requestId + " - getPublicKey exception: " + CommonUtils.getError(ex));
        }
        return publicKey;
    }

    private static byte[] blockCipher(Cipher cipher, byte[] bytes, int mode) throws IllegalBlockSizeException, BadPaddingException {
        // string initialize 2 buffers.
        // scrambled will hold intermediate results
        byte[] scrambled = new byte[0];

        // toReturn will hold the total result
        byte[] toReturn = new byte[0];
        // if we encrypt we use 100 byte long blocks. Decryption requires 128 byte long blocks (because of RSA)
        int length = (mode == Cipher.ENCRYPT_MODE)? 100 : 128;

        // another buffer. this one will hold the bytes that have to be modified in this step
        byte[] buffer = new byte[length];

        for (int i=0; i< bytes.length; i++){

            // if we filled our buffer array we have our block ready for de- or encryption
            if ((i > 0) && (i % length == 0)){
                //execute the operation
                scrambled = cipher.doFinal(buffer);
                // add the result to our total result.
                toReturn = append(toReturn,scrambled);
                // here we calculate the length of the next buffer required
                int newlength = length;

                // if newlength would be longer than remaining bytes in the bytes array we shorten it.
                if (i + length > bytes.length) {
                    newlength = bytes.length - i;
                }
                // clean the buffer array
                buffer = new byte[newlength];
            }
            // copy byte into our buffer.
            buffer[i%length] = bytes[i];
        }

        // this step is needed if we had a trailing buffer. should only happen when encrypting.
        // example: we encrypt 110 bytes. 100 bytes per run means we "forgot" the last 10 bytes. they are in the buffer array
        scrambled = cipher.doFinal(buffer);

        // final step before we can return the modified data.
        toReturn = append(toReturn,scrambled);

        return toReturn;
    }

    private static byte[] append(byte[] prefix, byte[] suffix){
        byte[] toReturn = new byte[prefix.length + suffix.length];
        for (int i=0; i< prefix.length; i++){
            toReturn[i] = prefix[i];
        }
        for (int i=0; i< suffix.length; i++){
            toReturn[i+prefix.length] = suffix[i];
        }
        return toReturn;
    }*/

    public static String encryptByPublicKey(String requestId, String data, String publicKey) {
        return Base64.encodeBase64String(encryptByPublicKey(requestId, data.getBytes(), publicKey));
    }

    private static byte[] encryptByPublicKey(String requestId, byte[] data, String publicKey){
        try {
            String KEY_ALGORITHM = "RSA";
            int MAX_ENCRYPT_BLOCK = 117;

            byte[] keyBytes = Base64.decodeBase64(publicKey);
            X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
            KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
            Key publicK = keyFactory.generatePublic(x509KeySpec);
            Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
            cipher.init(Cipher.ENCRYPT_MODE, publicK);
            int inputLen = data.length;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int offSet = 0;
            byte[] cache;
            int i = 0;
            while (inputLen - offSet > 0) {
                if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
                    cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
                } else {
                    cache = cipher.doFinal(data, offSet, inputLen - offSet);
                }
                out.write(cache, 0, cache.length);
                i++;
                offSet = i * MAX_ENCRYPT_BLOCK;
            }
            byte[] encryptedData = out.toByteArray();
            out.close();
            return encryptedData;
        } catch (Exception ex){
            logger.debug(requestId + " - encryptByPublicKey exception: " + CommonUtils.getError(ex));
            throw new RuntimeException(ex);
        }
    }

    public static String decryptByPublicKey(String requestId, String encryptedData, String publicKey) {
        try{
            return new String(decryptByPublicKey(requestId, Base64.decodeBase64(encryptedData), publicKey));
        }catch(Exception e){
            throw new RuntimeException(e);
        }
    }

    private static byte[] decryptByPublicKey(String requestId, byte[] encryptedData, String publicKey)
            throws Exception {
        try {
            String KEY_ALGORITHM = "RSA";
            Integer KEY_SIZE = 2048;
            int MAX_DECRYPT_BLOCK = KEY_SIZE / 8;

            byte[] keyBytes = Base64.decodeBase64(publicKey);
            X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
            KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
            Key publicK = keyFactory.generatePublic(x509KeySpec);
            Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
            cipher.init(Cipher.DECRYPT_MODE, publicK);
            int inputLen = encryptedData.length;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int offSet = 0;
            byte[] cache;
            int i = 0;
            while (inputLen - offSet > 0) {
                if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
                    cache = cipher.doFinal(encryptedData, offSet, MAX_DECRYPT_BLOCK);
                } else {
                    cache = cipher.doFinal(encryptedData, offSet, inputLen - offSet);
                }
                out.write(cache, 0, cache.length);
                i++;
                offSet = i * MAX_DECRYPT_BLOCK;
            }
            byte[] decryptedData = out.toByteArray();
            out.close();
            return decryptedData;
        } catch (Exception ex) {
            logger.debug(requestId + " - decryptByPublicKey exception: " + CommonUtils.getError(ex));
            throw new RuntimeException(ex);
        }
    }

}
