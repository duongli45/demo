package com.pg.util;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by myasus on 3/5/20.
 */
public class VersionUtil {
    public String getVersion(){
        final Properties properties = new Properties();
        try {
            properties.load(VersionUtil.class.getClassLoader().getResourceAsStream("project.properties"));
            System.out.println(properties.getProperty("version"));
            return properties.getProperty("version");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
