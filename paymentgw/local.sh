GIT_HASH=$1
DOCKER_IMAGE=$2
WORK_SPACE=$3

git fetch --all
git checkout $GIT_HASH
git submodule update --recursive --init --remote --force

mvn clean install
if [ $? -ne 0 ]; then
        exit 1;
fi
scp qupservice/target/qupservice.war super@192.168.2.81:/home/super/QUp/tomcat8/webapps
