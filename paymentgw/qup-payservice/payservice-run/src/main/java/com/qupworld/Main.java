package com.qupworld;

import com.devebot.opflow.OpflowBuilder;
import com.devebot.opflow.OpflowConfig;
import com.devebot.opflow.OpflowConfigValidator;
import com.devebot.opflow.OpflowServerlet;
import com.devebot.opflow.exception.OpflowBootstrapException;
import com.qupworld.config.ServerConfig;
import com.qupworld.service.PayService;
import com.pg.util.KeysUtil;
import com.qupworld.util.SlackAPI;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.autoconfigure.*;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.web.EmbeddedServletContainerAutoConfiguration;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * Created by myasus on 10/31/19.
 */
@SpringBootApplication
@EnableAutoConfiguration(exclude = {
        MongoAutoConfiguration.class,
        MongoDataAutoConfiguration.class,
        DataSourceAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class,
        EmbeddedServletContainerAutoConfiguration.class,
        WebMvcAutoConfiguration.class })
public class Main {
    public static void main(String[] args) throws OpflowBootstrapException {
        SlackAPI slack = new SlackAPI();
        Logger logger = LogManager.getLogger(Main.class);
        String pathconfig = System.getProperty("pathconfig");
        // String pathconfig =
        // "/home/myasus/Workspace/source/GIT/qup-payservice/payservice-run/src/main/resources";
        logger.debug("pathconfig : " + pathconfig);
        if (pathconfig == null) {
            logger.debug("No PathConfig");
            slack.sendSlack("GET CONFIG ERROR", KeysUtil.URGREN, "");
        }
        ServerConfig.getInstance(pathconfig);
        logger.debug("PayService start: ");
        OpflowConfig.Validator validator = OpflowConfigValidator
                .getServerletConfigValidator(Main.class.getResourceAsStream("/worker-schema.json"));

        OpflowServerlet server = null;
        if (pathconfig == null) {
            server = OpflowBuilder.createServerlet("qup-payservice-worker.properties", validator);
        } else {
            String opflowPath = "file://" + pathconfig + "/qup-payservice-worker.properties";
            server = OpflowBuilder.createServerlet(opflowPath, validator);
            logger.debug("Using Opflow From Config Path");
            slack.sendSlack("Using Opflow From Config Path", KeysUtil.INFO, "");
        }

        SpringApplicationBuilder app = new SpringApplicationBuilder(Main.class);
        ConfigurableApplicationContext context = app.web(false).run(args);
        server.instantiateType(PayService.class, context.getBean("payServiceImpl"));
        server.serve();
        logger.debug("[*] Waiting for message. To exit press CTRL+C");
    }
}
