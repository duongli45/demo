package com.qupworld.service;

import com.qupworld.paymentgateway.entities.*;

/**
 * Created by qup on 09/05/2018.
 */
public interface PayService {
    String getVersion();
    public String estimateFare(ETAFareEnt etaFareEnt);
    public String estimateFareWithMultiCarType(ETAFareEnt etaFareEnt);
    public String estimateFareWithAllSupliers(ETAFareEnt etaFareEnt);
    public String estimateFareCarHailing(ETAFareEnt etaFareEnt);
    public String estimateDeliveryFare(ETADeliveryEnt etaFareEnt);
    public String estimateDeliveryFareWithMultiCarType(ETADeliveryEnt etaFareEnt);
    public String estimateFareMeter(ETAFareMeterEnt etaFareMeterEnt);
    public String estimateFareSharing(ETASharingEnt etaSharingEnt);

    public String addPromoCodeUse(PromoEnt promoEnt);
    public String checkPromoCode(PromoEnt promoEnt);
    public String addPromoToList(PromoEnt promoEnt);
    public String applyReferralAndAddPromo(PromoEnt promoEnt);
    public String deletePromoCodeUse(PromoEnt promoEnt);
    public String getRushHour(String requestId, String bookId);
    public String getCancelAmount(CancelBookingEnt cancelEnt);
    public String getGeneralSettingAffiliate(ETAFareEnt etaFareEnt);
    public String checkFlatRoutes(String requestId, CheckFlatRoutesQEnt checkFlatRoutesQEnt);

    public String getPaymentData(UpdateEnt updateEnt);
    public String checkFareAvailable(ETAFareEnt fareEnt);
    public String saveSettlementHistory(SettlementHistoryEnt settlementHistoryEnt);
    public String reportSettlementHistory(ReportSettlementEnt reportSettlementEnt);
    public String updatePaidToDriver(ReportSettlementEnt reportSettlementEnt);
    public String updateAffiliateRate(String requestId, int value, String priceType);
    public String estimateFareForThirdParty(ETAThirdPartyEnt etaFareEnt);
}
