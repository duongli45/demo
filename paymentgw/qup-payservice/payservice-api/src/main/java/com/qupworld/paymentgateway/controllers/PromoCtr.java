package com.qupworld.paymentgateway.controllers;

import com.google.gson.Gson;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.TimezoneUtil;
import com.qupworld.paymentgateway.entities.PromoCodeUse;
import com.qupworld.paymentgateway.entities.PromoEnt;
import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.MongoDaoImpl;
import com.qupworld.paymentgateway.models.SQLDao;
import com.qupworld.paymentgateway.models.SQLDaoImpl;
import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.account.Account;
import com.qupworld.paymentgateway.models.mongo.collections.booking.Recurring;
import com.qupworld.paymentgateway.models.mongo.collections.fleet.*;
import com.qupworld.paymentgateway.models.mongo.collections.paxPromoList.PaxPromoList;
import com.qupworld.paymentgateway.models.mongo.collections.paxReferral.PaxReferral;
import com.qupworld.paymentgateway.models.mongo.collections.paxReferral.RefereePromo;
import com.qupworld.paymentgateway.models.mongo.collections.promotionCode.PromotionCode;
import com.qupworld.paymentgateway.models.mongo.collections.promotionCode.Schedule;
import com.qupworld.paymentgateway.models.mongo.collections.promotionCode.TimeSchedule;
import com.qupworld.paymentgateway.models.mongo.collections.referral.Referral;
import com.qupworld.paymentgateway.models.mongo.collections.referral.ReferralHistories;
import com.qupworld.paymentgateway.models.mongo.collections.zone.Zone;
import com.qupworld.util.*;


import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by myasus on 10/9/19.
 */
public class PromoCtr {

    private MongoDao mongoDao;
    private SQLDao sqlDao;
    private final static Logger logger = LogManager.getLogger(PromoCtr.class);
    List<String> bookPax = Arrays.asList("CC", "API","Car-hailing","Web booking","Partner","mDispatcher","Kiosk", "Boost web link", "webBooking", "partner", "");

    public PromoCtr(){
        sqlDao = new SQLDaoImpl();
        mongoDao = new MongoDaoImpl();
    }

    public String promoCodeAction(PromoEnt promoEnt, String action) {
        ObjResponse objResponse = new ObjResponse();
        objResponse.returnCode = 200;
        try {
            //query with DataBase
            promoEnt.promoCode = promoEnt.promoCode.toUpperCase();

            //action delete promo code use - no need to check valid
            if(action.equals(KeysUtil.action_delete)){
                sqlDao.deletePromocodeuse(promoEnt.promoCode, promoEnt.userId, promoEnt.fleetId, 0);
                logger.debug(promoEnt.requestId + " - " +  "-----delete promo code successful-----");
                return objResponse.toString();
            } else {
                Fleet fleet = mongoDao.getFleetInfor(promoEnt.fleetId);
                PromotionCode promotionCode = mongoDao.getPromoCodeByFleetAndName(promoEnt.fleetId, promoEnt.promoCode);
                if(promotionCode != null){
                    Map<String, Object> objectMap = new HashMap<>();
                    objectMap.put("promoCodeId", promotionCode._id.toString());
                    String zoneId = "";
                    String currencySymbol = "";
                    String currencyISO = promoEnt.currencyISO != null ? promoEnt.currencyISO : "";
                    if(promoEnt.zoneId != null && !promoEnt.zoneId.isEmpty()){
                        zoneId = promoEnt.zoneId;
                    } else {
                        Zone zone = mongoDao.findByFleetIdAndGeo(promoEnt.fleetId, promoEnt.geo);
                        if (zone != null){
                            zoneId = zone._id.toString();
                            if(currencyISO.isEmpty()){
                                currencyISO = zone.currency.iso;
                                currencySymbol = zone.currency.symbol;
                            }
                        }
                    }
                    if(currencySymbol.isEmpty()){
                        for(com.qupworld.paymentgateway.models.mongo.collections.fleet.Currency currency : fleet.currencies) {
                            if(currencyISO.equals(currency.iso))
                                currencySymbol = currency.symbol;
                        }
                    }
                    double value = 0;
                    if(promotionCode.valueByCurrencies != null && !promotionCode.valueByCurrencies.isEmpty()){
                        if(currencyISO.equals(promotionCode.valueByCurrencies.get(0).currencyISO)){
                            value = promotionCode.valueByCurrencies.get(0).value;
                        }else{
                            if(!action.equals(KeysUtil.action_add_to_list)){
                                // Invalid promotion code. It seems that the promo is applied to other zones.
                                logger.debug(promoEnt.requestId + " - " +  "-----Invalid promotion code. It seems that the promo is applied to other zones-----");
                                objResponse.response = objectMap;
                                objResponse.returnCode = 306;
                                return objResponse.toString();
                            }
                        }
                    }
                    Map<String,Object> mapReturn = checkPromoCode(promoEnt, currencyISO, fleet, promotionCode, zoneId, value, action);
                    int checkCode = Integer.parseInt(mapReturn.get("returnCode").toString());
                    if(checkCode != 200){
                        objResponse.response = objectMap;
                        objResponse.returnCode = checkCode;
                        return objResponse.toString();
                    }
                    value = Double.parseDouble(mapReturn.get("value").toString());
                    logger.debug(promoEnt.requestId + " - apply value = " + value);
                    String promoCustomerType = "common";
                    if(promotionCode.newCustomer)
                        promoCustomerType = "new";
                    if (promotionCode.referredCustomers)
                        promoCustomerType = "referral";
                    boolean paidByDriver = paidByDriver(fleet.fleetId, promoEnt.promoCode, promoEnt.userId);
                    //do with action
                    switch (action) {
                        case KeysUtil.action_check:
                            objectMap.put("promotionCode", promotionCode.promotionCode);
                            objectMap.put("valueByCurrencies", promotionCode.valueByCurrencies);
                            objectMap.put("value", value);
                            objectMap.put("type", promotionCode.type);
                            objectMap.put("promoCustomerType", promoCustomerType);
                            objectMap.put("paidByDriver", paidByDriver);
                            objectMap.put("validFrom", TimezoneUtil.formatISODate(TimezoneUtil.convertServerToGMT(promotionCode.validFrom), "GMT"));
                            objectMap.put("validTo", TimezoneUtil.formatISODate(TimezoneUtil.convertServerToGMT(promotionCode.validTo), "GMT"));
                            objectMap.put("notes", promotionCode.notes);
                            objectMap.put("currencyISO", currencyISO);
                            objectMap.put("currencySymbol", currencySymbol);
                            objectMap.put("service", promotionCode.service);
                            objectMap.put("maximumValue", promotionCode.valueLimitPerUse);
                            objectMap.put("termAndCondition", promotionCode.termAndCondition);
                            objectMap.put("quantityLimitPerMonth", promotionCode.quantityLimitPerMonth);
                            objectMap.put("applyVerifiedCustomers", promotionCode.applyVerifiedCustomers);
                            objectMap.put("keepMinFee", promotionCode.keepMinFee);
                            //return to client
                            objResponse.response = objectMap;
                            addPaxPromoList(promoEnt.fleetId, promoEnt.userId, promotionCode);
                            break;
                        case KeysUtil.action_add_to_list:
                            int code = addPaxPromoList(promoEnt.fleetId, promoEnt.userId, promotionCode);
                            if (code == 200) {
                                objectMap.put("promotionCode", promotionCode.promotionCode);
                                objectMap.put("promoCustomerType", promoCustomerType);
                                objectMap.put("paidByDriver", paidByDriver);
                                objectMap.put("type", promotionCode.type);
                                objectMap.put("notes", promotionCode.notes);
                                objectMap.put("value", promotionCode.valueByCurrencies.get(0).value);
                                objectMap.put("valueByCurrencies", promotionCode.valueByCurrencies);
                                objectMap.put("service", promotionCode.service);
                                objectMap.put("maximumValue", promotionCode.valueLimitPerUse);
                                objectMap.put("termAndCondition", promotionCode.termAndCondition);
                                objectMap.put("quantityLimitPerMonth", promotionCode.quantityLimitPerMonth);
                                objectMap.put("applyVerifiedCustomers", promotionCode.applyVerifiedCustomers);
                                objectMap.put("keepMinFee", promotionCode.keepMinFee);
                                objectMap.put("currencyISO", currencyISO);
                                objectMap.put("currencySymbol", currencySymbol);
                                objectMap.put("validFrom", TimezoneUtil.formatISODate(TimezoneUtil.convertServerToGMT(promotionCode.validFrom), "GMT"));
                                objectMap.put("validTo", TimezoneUtil.formatISODate(TimezoneUtil.convertServerToGMT(promotionCode.validTo), "GMT"));
                                objResponse.response = objectMap;
                            } else {
                                objResponse.response = objectMap;
                                objResponse.returnCode = code;
                            }
                            break;
                        case KeysUtil.action_add:
                            if(promoEnt.recurring == null || (!promoEnt.recurring.mode.isEmpty() && promoEnt.recurring.mode.equals("single"))){
                                addPromoCodeUse(promoEnt.bookId, promoEnt.fleetId, promoEnt.userId, promoEnt.promoCode, promotionCode._id.toString(), currencyISO, value);
                                addPaxPromoList(promoEnt.fleetId, promoEnt.userId, promotionCode);
                            } else {
                                List<Date> dates = getDatesInTimeRange(promoEnt.recurring);
                                if(!dates.isEmpty()){
                                    for(Date currentDate: dates){
                                        logger.debug(promoEnt.requestId + " - " +  "add list promo code by date range " + dates.size());
                                        addPromoCodeUse(promoEnt.bookId, promoEnt.fleetId, promoEnt.userId, promoEnt.promoCode, promotionCode._id.toString(), currencyISO, value);
                                        addPaxPromoList(promoEnt.fleetId, promoEnt.userId, promotionCode);
                                    }
                                }
                            }
                            objectMap.put("value", value);
                            objectMap.put("type", promotionCode.type);
                            objectMap.put("promoCustomerType", promoCustomerType);
                            objectMap.put("paidByDriver", paidByDriver);
                            objectMap.put("notes", promotionCode.notes);
                            objectMap.put("currencyISO", currencyISO);
                            objectMap.put("currencySymbol", currencySymbol);
                            objectMap.put("service", promotionCode.service);
                            objectMap.put("maximumValue", promotionCode.valueLimitPerUse);
                            objectMap.put("termAndCondition", promotionCode.termAndCondition);
                            objectMap.put("valueByCurrencies", promotionCode.valueByCurrencies);
                            objectMap.put("validFrom", TimezoneUtil.formatISODate(TimezoneUtil.convertServerToGMT(promotionCode.validFrom), "GMT"));
                            objectMap.put("validTo", TimezoneUtil.formatISODate(TimezoneUtil.convertServerToGMT(promotionCode.validTo), "GMT"));
                            objectMap.put("quantityLimitPerMonth", promotionCode.quantityLimitPerMonth);
                            objectMap.put("applyVerifiedCustomers", promotionCode.applyVerifiedCustomers);
                            objectMap.put("keepMinFee", promotionCode.keepMinFee);
                            objectMap.put("promotionCode", promotionCode.promotionCode);
                            //return to client
                            objResponse.response = objectMap;
                            break;
                    }
                } else {
                    logger.debug(promoEnt.requestId + " - " +  "-----promo code not existed-----");
                    objResponse.returnCode = 304;
                }
            }
        } catch (Exception ex) {
            logger.debug(promoEnt.requestId + " - " +  "PromoCodeException : " + CommonUtils.getError(ex) + "    data : " + promoEnt.toString());
            objResponse.returnCode = 304;
        }
        return objResponse.toString();
    }

    private Map<String,Object> checkPromoCode(PromoEnt promoEnt, String currencyISO,
                               Fleet fleet, PromotionCode promotionCode, String zoneId, double value, String action) throws ParseException {
        // start check valid promo code
        Map<String, Object> mapReturn = new HashMap<>();
        SimpleDateFormat dateF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Zone zone = mongoDao.getZoneById(zoneId);
        String timezone = zone != null &&zone.timezone != null && !zone.timezone.isEmpty() ? zone.timezone : "";
        if (timezone.isEmpty()) {
            EstimateUtil estimateUtil = new EstimateUtil();
            timezone = estimateUtil.getTimeZoneId(promoEnt.geo, "", promoEnt.requestId);
            if (timezone.isEmpty())
                timezone =fleet.timezone;}
        logger.debug(promoEnt.requestId + " - zoneId: " + zoneId + " - timezone: " + timezone);
        Calendar cal = Calendar.getInstance();
        Date currentDate = new Date();
        logger.debug(promoEnt.requestId + " - currentDate request in GMT: " + promoEnt.currentDate);
        /*boolean compareVersion = ConvertUtil.compareVersion(promoEnt.rv, "4.6.4900");*/
        boolean compareVersion = true;
        if (promoEnt.currentDate != null && !promoEnt.currentDate.isEmpty() && !promoEnt.currentDate.equalsIgnoreCase("Now")) {
            try {
                currentDate = TimezoneUtil.offsetTimeZone(dateF.parse(promoEnt.currentDate.replace("Z", "")), "GMT", timezone);
            } catch (Exception ex) {
                logger.debug(promoEnt.requestId + " - " + "ERROR:" + CommonUtils.getError(ex));
                currentDate = TimezoneUtil.offsetTimeZone(sdf.parse(promoEnt.currentDate), "GMT", timezone);
            }
        }
        if (promoEnt.currentDate == null || promoEnt.currentDate.isEmpty() || promoEnt.currentDate.equalsIgnoreCase("Now")) {
            currentDate = TimezoneUtil.offsetTimeZone(currentDate, cal.getTimeZone().getID(), timezone);
        }
        logger.debug(promoEnt.requestId + " - convert currentDate from GMT to pickup zone: " + currentDate);

        if (!promotionCode.isActive) {
            logger.debug(promoEnt.requestId + " - " + "-----promo code inactive-----");
            mapReturn.put("value", value);
            mapReturn.put("returnCode", 304);
            return mapReturn;
        }
        if (promotionCode.zoneId != null && !promotionCode.zoneId.isEmpty() &&
                !zoneId.isEmpty() && !promotionCode.zoneId.contains(zoneId)) {
            logger.debug(promoEnt.requestId + " - " + "zoneId: " + zoneId);
            logger.debug(promoEnt.requestId + " - " + "-----promo code not support in this zone-----");
            mapReturn.put("value", value);
            mapReturn.put("returnCode", 304);
            return mapReturn;
        }

        if (promotionCode.newCustomer) {
            if (promoEnt.bookFrom.equals(KeysUtil.pax) || !bookPax.contains(promoEnt.bookFrom)) {
                if (promoEnt.userId != null && !promoEnt.userId.isEmpty()) {
                    List<PromoCodeUse> promoCodeUses = sqlDao.findPromoCodeUseByPromoCodeAndCustomer(promotionCode._id.toString(), promoEnt.userId);
                    if (promoCodeUses.size() >= 1) {
                        // Promo code has already been used
                        logger.debug(promoEnt.requestId + " - " + "-----Promo code has already been used-----");
                        mapReturn.put("value", value);
                        mapReturn.put("returnCode", 416);
                        return mapReturn;
                    } else {
                        int tickets = sqlDao.findTicketByFleetAndCustomer(promoEnt.fleetId, promoEnt.userId);
                        if (tickets > 0) {
                            // User has completed trip book from app
                            logger.debug(promoEnt.requestId + " - " + "-----User has completed trip book from app-----");
                            mapReturn.put("value", value);
                            mapReturn.put("returnCode", 304);
                            return mapReturn;
                        }
                    }
                }
            } else {
                // Promo code is not valid
                logger.debug(promoEnt.requestId + " - " + "-----Promo code is not valid-----");
                mapReturn.put("value", value);
                mapReturn.put("returnCode", 304);
                return mapReturn;
            }
        }

        if (promotionCode.referredCustomers) {
            if (promoEnt.userId != null && !promoEnt.userId.isEmpty()) {
                ReferralHistories referralHistories = mongoDao.getLinkReferralByRefereeId(promoEnt.userId);
                if (referralHistories == null) {
                    // Invalid promotion code. It seems that the promo only apply for referred customers
                    logger.debug(promoEnt.requestId + " - " + "-----Invalid promotion code. It seems that the promo only apply for referred customers-----");
                    mapReturn.put("value", value);
                    mapReturn.put("returnCode", 424);
                    return mapReturn;
                } else {
                    if (referralHistories.referralType != null && referralHistories.referralType.equals(KeysUtil.APPTYPE_PASSENGER)) {
                        PaxReferral referral = mongoDao.getPaxReferral(promoEnt.fleetId);
                        if (referral != null && referral.refereeGet != null && referral.refereeGet.type != null) {
                            if (!referral.refereeGet.type.equals("promo")) {
                                // Invalid promotion code. It seems that the promo only apply for referred customers
                                logger.debug(promoEnt.requestId + " - " + "-----Invalid promotion code. It seems that the promo only apply for referred customers-----");
                                mapReturn.put("value", value);
                                mapReturn.put("returnCode", 424);
                                return mapReturn;
                            } else if (referral.refereeGet.type.equals("promo") && referral.refereeGet.promos != null &&
                                    !referral.refereeGet.promos.isEmpty()) {
                                boolean valid = false;
                                for (RefereePromo p : referral.refereeGet.promos) {
                                    if (p.promoId.equals(promotionCode._id.toString())) {
                                        valid = true;
                                        break;
                                    }
                                }
                                if (!valid) {
                                    // Invalid promotion code. It seems that the promo only apply for referred customers
                                    logger.debug(promoEnt.requestId + " - " + "-----Invalid promotion code. It seems that the promo only apply for referred customers-----");
                                    mapReturn.put("value", value);
                                    mapReturn.put("returnCode", 424);
                                    return mapReturn;
                                }
                            }
                        }
                    } else {
                        Referral referral = mongoDao.getReferral(promoEnt.fleetId);
                        if (referral != null && referral.firstPackage != null && referral.firstPackage.promos != null &&
                                !referral.firstPackage.promos.isEmpty()) {
                            boolean valid = false;
                            for (RefereePromo p : referral.firstPackage.promos) {
                                if (p.promoId.equals(promotionCode._id.toString())) {
                                    valid = true;
                                    break;
                                }
                            }
                            if (!valid) {
                                // Invalid promotion code. It seems that the promo only apply for referred customers
                                logger.debug(promoEnt.requestId + " - " + "-----Invalid promotion code. It seems that the promo only apply for referred customers-----");
                                mapReturn.put("value", value);
                                mapReturn.put("returnCode", 424);
                                return mapReturn;
                            }
                        }
                    }
                }
            }
            if (fleet.generalSetting != null && fleet.generalSetting.founderFund != null && fleet.generalSetting.founderFund.firstBooking) {
                if (promoEnt.userId != null && !promoEnt.userId.isEmpty()) {
                    int promoUsed = sqlDao.findPromoCodeUseByCustomerId(promoEnt.userId);
                    if (promoUsed > 0) {
                        //Sorry, this promo code can only be used for the first booking
                        logger.debug(promoEnt.requestId + " - " + "-----Sorry, this promo code can only be used for the first booking-----");
                        mapReturn.put("value", value);
                        mapReturn.put("returnCode", 429);
                        return mapReturn;
                    } else {
                        int tickets = sqlDao.findCompletedTicketByFleetAndCustomer(promoEnt.fleetId, promoEnt.userId);
                        if (tickets > 0) {
                            //Sorry, this promo code can only be used for the first booking
                            logger.debug(promoEnt.requestId + " - " + "-----Sorry, this promo code can only be used for the first booking-----");
                            mapReturn.put("value", value);
                            mapReturn.put("returnCode", 429);
                            return mapReturn;
                        }
                    }
                }
            }
        }

        if (promotionCode.promoStatus.equalsIgnoreCase("private") || promotionCode.promoStatus.equalsIgnoreCase("redeemed")) {
            if (promoEnt.userId != null && !promoEnt.userId.isEmpty()) {
                if (!promotionCode.customerId.contains(promoEnt.userId)) {
                    // If promo is private then user must be in the pax list
                    logger.debug(promoEnt.requestId + " - " + "-----If promo is private then user must be in the pax list-----");
                    mapReturn.put("value", value);
                    mapReturn.put("returnCode", 422);
                    return mapReturn;
                }
            } else {
                // If promo is private then user must be in the pax list
                logger.debug(promoEnt.requestId + " - " + "-----If promo is private then user must be in the pax list-----");
                mapReturn.put("value", value);
                mapReturn.put("returnCode", 422);
                return mapReturn;
            }
        }

        if (promotionCode.isCorporateCustomers) {
            String userId = promoEnt.userId != null ? promoEnt.userId : "";
            if (userId.isEmpty()) {
                // Promo code is not valid
                logger.debug(promoEnt.requestId + " - " + "-----Promo code only apply for corporate users-----");
                mapReturn.put("value", value);
                mapReturn.put("returnCode", 304);
                return mapReturn;
            }
            Account customer = mongoDao.getAccount(userId);
            String corporateId = customer != null && customer.passengerInfo != null && customer.passengerInfo.corporateId != null ? customer.passengerInfo.corporateId : "";
            logger.debug(promoEnt.requestId + " - corporateId: " + corporateId);
            int status = customer != null && customer.corporateInfo != null ? customer.corporateInfo.status : 1; // default inreview
            if (corporateId.isEmpty() || status == 1) {
                // Promo code is not valid
                logger.debug(promoEnt.requestId + " - " + "-----Promo code only apply for corporate users-----");
                mapReturn.put("value", value);
                mapReturn.put("returnCode", 304);
                return mapReturn;
            }
            if (promotionCode.corporateId != null && !promotionCode.corporateId.isEmpty() && !promotionCode.corporateId.contains(corporateId)) {
                // Promo code is not valid
                logger.debug(promoEnt.requestId + " - " + "-----Promo code only apply for corporate users-----");
                mapReturn.put("value", value);
                mapReturn.put("returnCode", 304);
                return mapReturn;
            }
        }

        logger.debug(promoEnt.requestId + " - " + "validFrom in server time: " + promotionCode.validFrom);
        logger.debug(promoEnt.requestId + " - " + "validTo in server time: " + promotionCode.validTo);
        Date validFromGMT = TimezoneUtil.offsetTimeZone(promotionCode.validFrom, cal.getTimeZone().getID(), "GMT");
        Date validToGMT = TimezoneUtil.offsetTimeZone(promotionCode.validTo, cal.getTimeZone().getID(), "GMT");
        logger.debug(promoEnt.requestId + " - " + "convert validFrom to GMT: " + validFromGMT);
        logger.debug(promoEnt.requestId + " - " + "convert validTo to GMT: " + validToGMT);
        Date validFrom = TimezoneUtil.offsetTimeZone(validFromGMT, "GMT", fleet.timezone);
        Date validTo = TimezoneUtil.offsetTimeZone(validToGMT, "GMT", fleet.timezone);
        logger.debug(promoEnt.requestId + " - " + "convert validFrom to fleet time: " + validFrom);
        logger.debug(promoEnt.requestId + " - " + "convert validTo to fleet time: " + validTo);

        logger.debug(promoEnt.requestId + " >> " + "date to compare: " + sdf1.format(currentDate));
        logger.debug(promoEnt.requestId + " >> " + "validFrom to compare: " + sdf1.format(validFrom));
        logger.debug(promoEnt.requestId + " >> " + "validTo to compare: " + sdf1.format(validTo));
        if (promotionCode.quantity.isLimited) {
            int promoUsed = sqlDao.findPromoCodeUseByPromoCode(promotionCode._id.toString());
            if (promoUsed >= promotionCode.quantity.value) {
                //Promo code doesn't reach limited released quantity, if turned on
                logger.debug(promoEnt.requestId + " - " + "-----Promo code doesn't reach limited released quantity, if turned on-----");
                mapReturn.put("value", value);
                mapReturn.put("returnCode", 421);
                return mapReturn;
            }
        }

        if (promotionCode.totalUsesLimit.isLimited && promoEnt.userId != null && !promoEnt.userId.isEmpty()) {
            List<PromoCodeUse> promoCodeUses = sqlDao.findPromoCodeUseByPromoCodeAndCustomer(promotionCode._id.toString(), promoEnt.userId);
            if (promoCodeUses.size() >= promotionCode.totalUsesLimit.value) {
                //Promo code doesn't reach limited total uses/user, if turned on
                logger.debug(promoEnt.requestId + " - " + "-----Promo code doesn't reach limited total uses/user, if turned on-----");
                mapReturn.put("value", value);
                mapReturn.put("returnCode", 420);
                return mapReturn;
            }
        }
        String fromDate = sdf1.format(TimezoneUtil.offsetTimeZone(TimezoneUtil.atStartOfDay(TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), fleet.timezone)), fleet.timezone, "GMT"));
        String toDate = sdf1.format(TimezoneUtil.offsetTimeZone(TimezoneUtil.atEndOfDay(TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), fleet.timezone)), fleet.timezone, "GMT"));
        logger.debug(promoEnt.requestId + " - fromDate: " + fromDate);
        logger.debug(promoEnt.requestId + " - toDate: " + toDate);
        if (promotionCode.quantityLimitPerDay != null && promotionCode.quantityLimitPerDay.isLimited) {
            int promoUsed = sqlDao.findPromoCodeUseByPromoCodeAndDate(promotionCode._id.toString(), fromDate, toDate);
            logger.debug(promoEnt.requestId + " - promoUsed: " + promoUsed);
            if (promoUsed >= promotionCode.quantityLimitPerDay.value) {
                //The promo code has been fully redeemed for that day.
                logger.debug(promoEnt.requestId + " - " + "-----The promo code has been fully redeemed for that day.-----");
                mapReturn.put("value", value);
                mapReturn.put("returnCode", 425);
                return mapReturn;
            }
        }

        if (promotionCode.totalUsesLimitPerDay != null && promotionCode.totalUsesLimitPerDay.isLimited && promoEnt.userId != null && !promoEnt.userId.isEmpty()) {
            int promoCodeUses = sqlDao.findPromoCodeUseByPromoCodeAndCustomerAndDate(promotionCode._id.toString(), promoEnt.userId, fromDate, toDate);
            logger.debug(promoEnt.requestId + " - promoCodeUses: " + promoCodeUses);
            if (promoCodeUses >= promotionCode.totalUsesLimitPerDay.value) {
                //Promo code doesn't reach limited total uses/user, if turned on
                logger.debug(promoEnt.requestId + " - " + "-----Sorry, you have reached your limited promo uses per day.-----");
                mapReturn.put("value", value);
                mapReturn.put("returnCode", 426);
                return mapReturn;
            }
        }

        if (promotionCode.quantityLimitPerMonth != null && promotionCode.quantityLimitPerMonth.isLimited && promoEnt.userId != null && !promoEnt.userId.isEmpty()) {
            String startMonth = sdf1.format(TimezoneUtil.offsetTimeZone(TimezoneUtil.atStartOfMonth(TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), fleet.timezone)), fleet.timezone, "GMT"));
            String endMonth = sdf1.format(TimezoneUtil.offsetTimeZone(TimezoneUtil.atEndOfMonth(TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), fleet.timezone)), fleet.timezone, "GMT"));
            logger.debug(promoEnt.requestId + " - startMonth: " + startMonth);
            logger.debug(promoEnt.requestId + " - endMonth: " + endMonth);
            int promoCodeUses = sqlDao.findPromoCodeUseByPromoCodeAndCustomerAndDate(promotionCode._id.toString(), promoEnt.userId, startMonth, endMonth);
            logger.debug(promoEnt.requestId + " - promoCodeUses: " + promoCodeUses);
            if (promoCodeUses >= promotionCode.quantityLimitPerMonth.value) {
                //Promo code doesn't reach limited total uses/user, if turned on
                logger.debug(promoEnt.requestId + " - " + "-----You have used up your monthly promotion codes.-----");
                mapReturn.put("value", value);
                mapReturn.put("returnCode", 428);
                return mapReturn;
            }
        }

        //check quantity per year
        if (promotionCode.quantityLimitPerYear != null && promotionCode.quantityLimitPerYear.isLimited && promoEnt.userId != null && !promoEnt.userId.isEmpty()) {
            String startYear = sdf1.format(TimezoneUtil.offsetTimeZone(TimezoneUtil.atStartOfYear(TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), fleet.timezone)), fleet.timezone, "GMT"));
            String endYear = sdf1.format(TimezoneUtil.offsetTimeZone(TimezoneUtil.atEndOfYear(TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), fleet.timezone)), fleet.timezone, "GMT"));
            logger.debug(promoEnt.requestId + " - startYear: " + startYear);
            logger.debug(promoEnt.requestId + " - endYear: " + endYear);
            int promoCodeUses = sqlDao.findPromoCodeUseByPromoCodeAndCustomerAndDate(promotionCode._id.toString(), promoEnt.userId, startYear, endYear);
            logger.debug(promoEnt.requestId + " - promoCodeUses: " + promoCodeUses);
            if (promoCodeUses >= promotionCode.quantityLimitPerYear.value) {
                //You have used up your yearly promotion codes.
                logger.debug(promoEnt.requestId + " - " + "-----You have used up your yearly promotion codes.-----");
                if (compareVersion && (promoEnt.bookFrom.equals(KeysUtil.pax) || !bookPax.contains(promoEnt.bookFrom)) || bookPax.contains(promoEnt.bookFrom)) {
                    mapReturn.put("value", value);
                    mapReturn.put("returnCode", 430);
                    return mapReturn;
                } else {
                    mapReturn.put("value", value);
                    mapReturn.put("returnCode", 304);
                    return mapReturn;
                }
            }
        }

        //check schedule
        if (promotionCode.scheduleEnable) {
            Gson gson = new Gson();
            logger.debug(promoEnt.requestId + " - check schedule with timezone: " + timezone);
            logger.debug(promoEnt.requestId + " - currentDate: " + currentDate);
            logger.debug(promoEnt.requestId + " - schedules: " + gson.toJson(promotionCode.schedules));
            boolean scheduleChecked = checkSchedule(promoEnt.requestId, promotionCode.schedules, currentDate, fleet.timezone);
            logger.debug(promoEnt.requestId + " - scheduleChecked: " + scheduleChecked);
            if (!scheduleChecked) {
                //The promotion code isn't eligible to use at selected time.
                logger.debug(promoEnt.requestId + " - " + "-----The promotion code isn't eligible to use at selected time.-----");
                if (compareVersion && (promoEnt.bookFrom.equals(KeysUtil.pax) || !bookPax.contains(promoEnt.bookFrom)) || bookPax.contains(promoEnt.bookFrom)) {
                    mapReturn.put("value", value);
                    mapReturn.put("returnCode", 433);
                    return mapReturn;
                } else {
                    mapReturn.put("value", value);
                    mapReturn.put("returnCode", 304);
                    return mapReturn;
                }
            }
        }

        if (!action.equals(KeysUtil.action_add_to_list)) {
            //check service type
            if (promoEnt.serviceType != null) {
                if (promoEnt.serviceType == 0 && promotionCode.service != null && promotionCode.service.transport != null && !promotionCode.service.transport) {
                    logger.debug(promoEnt.requestId + " - " + "-----The promotion code isn't applied for transport service.-----");
                    mapReturn.put("value", value);
                    mapReturn.put("returnCode", 304);
                    return mapReturn;
                }
                if (promoEnt.serviceType == 1 && promotionCode.service != null && promotionCode.service.intercity != null && !promotionCode.service.intercity) {
                    logger.debug(promoEnt.requestId + " - " + "-----The promotion code isn't applied for intercity service.-----");
                    mapReturn.put("value", value);
                    mapReturn.put("returnCode", 304);
                    return mapReturn;
                }
                if (promoEnt.serviceType == 2 && promotionCode.service != null && promotionCode.service.parcel != null && !promotionCode.service.parcel) {
                    logger.debug(promoEnt.requestId + " - " + "-----The promotion code isn't applied for parcel service.-----");
                    mapReturn.put("value", value);
                    mapReturn.put("returnCode", 304);
                    return mapReturn;
                }
                if (promoEnt.serviceType == 3 && promotionCode.service != null && promotionCode.service.food != null && !promotionCode.service.food) {
                    logger.debug(promoEnt.requestId + " - " + "-----The promotion code isn't applied for food service.-----");
                    mapReturn.put("value", value);
                    mapReturn.put("returnCode", 304);
                    return mapReturn;
                }
                if (promoEnt.serviceType == 4 && promotionCode.service != null && promotionCode.service.mart != null && !promotionCode.service.mart) {
                    logger.debug(promoEnt.requestId + " - " + "-----The promotion code isn't applied for mart service.-----");
                    mapReturn.put("value", value);
                    mapReturn.put("returnCode", 304);
                    return mapReturn;
                }
                if (promoEnt.serviceType == 5 && promotionCode.service != null && promotionCode.service.shuttle != null && !promotionCode.service.shuttle) {
                    logger.debug(promoEnt.requestId + " - " + "-----The promotion code isn't applied for shuttle service.-----");
                    mapReturn.put("value", value);
                    mapReturn.put("returnCode", 304);
                    return mapReturn;
                }
            }
            if (promotionCode.applyVerifiedCustomers) {
                if (promoEnt.userId != null && !promoEnt.userId.isEmpty()) {
                    Account account = mongoDao.getAccount(promoEnt.userId);
                    if (account == null || account.profile == null || account.profile.profileStatus == 0) {
                        //Account doesn't verified
                        logger.debug(promoEnt.requestId + " - " + "-----Verify your account to use this promotion code.-----");
                        mapReturn.put("value", value);
                        mapReturn.put("returnCode", 427);
                        return mapReturn;
                    }
                } else {
                    logger.debug(promoEnt.requestId + " - " + "-----Verify your account to use this promotion code.-----");
                    mapReturn.put("value", value);
                    mapReturn.put("returnCode", 427);
                    return mapReturn;
                }
            }

            if (promotionCode.budgetLimit.isLimited && promoEnt.userId != null && !promoEnt.userId.isEmpty()) {
                List<PromoCodeUse> promoCodeUses = sqlDao.findPromoCodeUseByPromoCodeAndCustomer(promotionCode._id.toString(), promoEnt.userId);
                Gson gson = new Gson();
                logger.debug(promoEnt.requestId + " - promoCodeUses: " + gson.toJson(promoCodeUses));
                double totalUse = 0;
                double totalApplied = 0;
                for (PromoCodeUse promoCodeUse : promoCodeUses) {
                    totalUse += promoCodeUse.usedValue;
                    totalApplied += promoCodeUse.usedValue;
                }
                logger.debug(promoEnt.requestId + " - totalUse: " + totalUse);
                double useValue = value;
                // check type percentage
                if (!promotionCode.type.equalsIgnoreCase("amount") && promoEnt.etaFare != null) {
                    useValue = CommonUtils.getRoundValue(promoEnt.etaFare * value / 100);
                }
                logger.debug(promoEnt.requestId + " - useValue: " + useValue);
                if(promoEnt.basicFare>0){
                    if(useValue> promoEnt.basicFare){
                        totalUse = totalUse + promoEnt.basicFare - useValue;
                    }
                } else {
                    totalUse = totalUse + useValue;
                }
                logger.debug(promoEnt.requestId + " - useValue: " + useValue);
                logger.debug(promoEnt.requestId + " - totalUse: " + totalUse);
                if (fleet.multiCurrencies) {
                    for (AmountByCurrency valueByCurrency : promotionCode.budgetLimit.valueByCurrencies) {
                        if (currencyISO.equals(valueByCurrency.currencyISO)) {
                            if (totalUse > valueByCurrency.value) {
                                //Promo code doesn't reach budget limitation for a pax, if turned on
                                logger.debug(promoEnt.requestId + " - " + "-----Promo code doesn't reach budget limitation for a pax, if turned on-----");
                                mapReturn.put("value", value);
                                mapReturn.put("returnCode", 423);
                                return mapReturn;
                            }
                        }
                    }
                } else if (totalUse > promotionCode.budgetLimit.valueByCurrencies.get(0).value) {
                    if (promotionCode.applyLimitFinished) {
                        double budgetLimit = promotionCode.budgetLimit.valueByCurrencies.get(0).value;
                        double remainValue = CommonUtils.getRoundValue(budgetLimit - totalApplied);
                        logger.debug(promoEnt.requestId + " - budgetLimit: " + budgetLimit);
                        logger.debug(promoEnt.requestId + " - totalApplied: " + totalApplied);
                        logger.debug(promoEnt.requestId + " => remainValue: " + remainValue);
                        if (remainValue < value) {
                            value = remainValue;
                        }
                        logger.debug(promoEnt.requestId + " => promo value: " + value);
                        if (value <= 0) {
                            logger.debug(promoEnt.requestId + " - " + "-----Promo code doesn't reach budget limitation for a pax, if turned on-----");
                            mapReturn.put("value", value);
                            mapReturn.put("returnCode", 423);
                            return mapReturn;
                        }
                    } else {
                        //Promo code doesn't reach budget limitation for a pax, if turned on
                        logger.debug(promoEnt.requestId + " - " + "-----Promo code doesn't reach budget limitation for a pax, if turned on-----");
                        mapReturn.put("value", value);
                        mapReturn.put("returnCode", 423);
                        return mapReturn;
                    }
                }
            }

            if (promotionCode.budgetLimitDay != null && promotionCode.budgetLimitDay.isLimited && promoEnt.userId != null && !promoEnt.userId.isEmpty()) {
                double totalUse = sqlDao.findPromoCodeUseByPromoCodeAndCustomerOnRange(promotionCode._id.toString(), promoEnt.userId, fromDate, toDate);
                double totalApplied = totalUse;
                // add value of this time
                double useValue = value;
                // check type percentage
                if (!promotionCode.type.equalsIgnoreCase("amount") && promoEnt.etaFare != null) {
                    useValue = CommonUtils.getRoundValue(promoEnt.etaFare * value / 100);
                }
                logger.debug(promoEnt.requestId + " - useValue: " + useValue);
                totalUse = totalUse + useValue;
                logger.debug(promoEnt.requestId + " - totalUse: " + totalUse);
                if (totalUse > promotionCode.budgetLimitDay.value) {
                    if (promotionCode.applyLimitFinished) {
                        double budgetLimit = promotionCode.budgetLimitDay.value;
                        double remainValue = CommonUtils.getRoundValue(budgetLimit - totalApplied);
                        if (remainValue < value)
                            value = remainValue;
                        if (value <= 0) {
                            logger.debug(promoEnt.requestId + " - " + "-----Promo code doesn't reach budget limitation for a pax per day, if turned on-----");
                            mapReturn.put("value", value);
                            mapReturn.put("returnCode", 423);
                            return mapReturn;
                        }
                    } else {
                        logger.debug(promoEnt.requestId + " - " + "-----Promo code doesn't reach budget limitation for a pax per day, if turned on-----");
                        mapReturn.put("value", value);
                        mapReturn.put("returnCode", 423);
                        return mapReturn;
                    }
                }
            }

            if (promotionCode.budgetLimitMonth != null && promotionCode.budgetLimitMonth.isLimited && promoEnt.userId != null && !promoEnt.userId.isEmpty()) {
                String startMonth = sdf1.format(TimezoneUtil.offsetTimeZone(TimezoneUtil.atStartOfMonth(TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), fleet.timezone)), fleet.timezone, "GMT"));
                String endMonth = sdf1.format(TimezoneUtil.offsetTimeZone(TimezoneUtil.atEndOfMonth(TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), fleet.timezone)), fleet.timezone, "GMT"));
                double totalUse = sqlDao.findPromoCodeUseByPromoCodeAndCustomerOnRange(promotionCode._id.toString(), promoEnt.userId, startMonth, endMonth);
                double totalApplied = totalUse;
                // add value of this time
                double useValue = value;
                // check type percentage
                if (!promotionCode.type.equalsIgnoreCase("amount") && promoEnt.etaFare != null) {
                    useValue = CommonUtils.getRoundValue(promoEnt.etaFare * value / 100);
                }
                logger.debug(promoEnt.requestId + " - useValue: " + useValue);
                totalUse = totalUse + useValue;
                logger.debug(promoEnt.requestId + " - totalUse: " + totalUse);
                if (totalUse > promotionCode.budgetLimitMonth.value) {
                    if (promotionCode.applyLimitFinished) {
                        double budgetLimit = promotionCode.budgetLimitMonth.value;
                        double remainValue = CommonUtils.getRoundValue(budgetLimit - totalApplied);
                        if (remainValue < value)
                            value = remainValue;
                        if (value <= 0) {
                            logger.debug(promoEnt.requestId + " - " + "-----Promo code doesn't reach budget limitation for a pax per month, if turned on-----");
                            mapReturn.put("value", value);
                            mapReturn.put("returnCode", 423);
                            return mapReturn;
                        }
                    } else {
                        logger.debug(promoEnt.requestId + " - " + "-----Promo code doesn't reach budget limitation for a pax per month, if turned on-----");
                        mapReturn.put("value", value);
                        mapReturn.put("returnCode", 423);
                        return mapReturn;
                    }
                }
            }

            if (promotionCode.budgetLimitYear != null && promotionCode.budgetLimitYear.isLimited && promoEnt.userId != null && !promoEnt.userId.isEmpty()) {
                String startYear = sdf1.format(TimezoneUtil.offsetTimeZone(TimezoneUtil.atStartOfYear(TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), fleet.timezone)), fleet.timezone, "GMT"));
                String endYear = sdf1.format(TimezoneUtil.offsetTimeZone(TimezoneUtil.atEndOfYear(TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), fleet.timezone)), fleet.timezone, "GMT"));
                double totalUse = sqlDao.findPromoCodeUseByPromoCodeAndCustomerOnRange(promotionCode._id.toString(), promoEnt.userId, startYear, endYear);
                double totalApplied = totalUse;
                // add value of this time
                double useValue = value;
                // check type percentage
                if (!promotionCode.type.equalsIgnoreCase("amount") && promoEnt.etaFare != null) {
                    useValue = CommonUtils.getRoundValue(promoEnt.etaFare * value / 100);
                }
                logger.debug(promoEnt.requestId + " - useValue: " + useValue);
                totalUse = totalUse + useValue;
                logger.debug(promoEnt.requestId + " - totalUse: " + totalUse);
                if (totalUse > promotionCode.budgetLimitYear.value) {
                    if (promotionCode.applyLimitFinished) {
                        double budgetLimit = promotionCode.budgetLimitYear.value;
                        double remainValue = CommonUtils.getRoundValue(budgetLimit - totalApplied);
                        if (remainValue < value)
                            value = remainValue;
                        if (value <= 0) {
                            logger.debug(promoEnt.requestId + " - " + "-----Promo code doesn't reach budget limitation for a pax, if turned on-----");
                            mapReturn.put("value", value);
                            mapReturn.put("returnCode", 423);
                            return mapReturn;
                        }
                    } else {
                        logger.debug(promoEnt.requestId + " - " + "-----Promo code doesn't reach budget limitation for a pax, if turned on-----");
                        mapReturn.put("value", value);
                        mapReturn.put("returnCode", 423);
                        return mapReturn;
                    }
                }
            }

            if (compareVersion && (promoEnt.bookFrom.equals(KeysUtil.pax) || !bookPax.contains(promoEnt.bookFrom)) || bookPax.contains(promoEnt.bookFrom)) {
                //check payment method
                if (promotionCode.paymentMethodsApply != null && !promotionCode.paymentMethodsApply.isEmpty() && promoEnt.paymentType != null) {
                    if (promoEnt.paymentType == 16 || promoEnt.paymentType == 17)
                        promoEnt.paymentType = 1;
                    if (promoEnt.paymentType == 13 && promoEnt.bookFrom.matches("(.*)Boost(.*)"))
                        promoEnt.paymentType = 19;
                    //convert old setting
                    List<String> pmConvert = new ArrayList<>();
                    for (Object pm : promotionCode.paymentMethodsApply) {
                        try {
                            pmConvert.add(convertPaymentType(Integer.valueOf(String.valueOf(pm).trim()), null));
                        } catch (NumberFormatException nf) {
                            logger.debug(promoEnt.requestId + " - new setting! no need to convert");
                            pmConvert.add(String.valueOf(pm).trim());
                        }
                    }
                    logger.debug(promoEnt.requestId + " - promotionCode.paymentMethodsApply: " + pmConvert);
                    logger.debug(promoEnt.requestId + " - primaryPartialMethod: " + promoEnt.primaryPartialMethod);
                    if (promoEnt.primaryPartialMethod == -1) {
                        if (!pmConvert.contains(convertPaymentType(promoEnt.paymentType, promoEnt.gateway))) {
                            //The promotion code can't be used for %s payment.
                            logger.debug(promoEnt.requestId + " - " + "-----The promotion code can't be used for %s payment.-----");
                            mapReturn.put("value", value);
                            mapReturn.put("returnCode", 431);
                            return mapReturn;
                        }
                    } else {
                        String primaryPartialMethod = convertPaymentType(promoEnt.primaryPartialMethod, null);
                        // always apply if allow promo with primary partial method
                        if (!pmConvert.contains(primaryPartialMethod)) {
                            // check if allow promo with request method
                            if (!pmConvert.contains(convertPaymentType(promoEnt.paymentType, promoEnt.gateway))) {
                                //The promotion code can't be used for %s payment.
                                logger.debug(promoEnt.requestId + " - " + "-----The promotion code can't be used for %s payment.-----");
                                mapReturn.put("value", value);
                                mapReturn.put("returnCode", 431);
                                return mapReturn;
                            }
                        }
                    }
                }

                //check minimum estimate fare
                if (promotionCode.minimumEstFareApply != null && promotionCode.minimumEstFareApply.isLimited && promoEnt.etaFare != null && promoEnt.etaFare > 0) {
                    if (fleet.multiCurrencies) {
                        for (AmountByCurrency valueByCurrency : promotionCode.minimumEstFareApply.valueByCurrencies) {
                            if (currencyISO.equals(valueByCurrency.currencyISO) && promoEnt.etaFare < valueByCurrency.value) {
                                //This fare amount isn't eligible for the promotion code.
                                logger.debug(promoEnt.requestId + " - " + "-----This fare amount isn't eligible for the promotion code.-----");
                                mapReturn.put("value", value);
                                mapReturn.put("returnCode", 432);
                                return mapReturn;
                            }
                        }
                    } else if (promoEnt.etaFare < promotionCode.minimumEstFareApply.valueByCurrencies.get(0).value) {
                        //This fare amount isn't eligible for the promotion code.
                        logger.debug(promoEnt.requestId + " - " + "-----This fare amount isn't eligible for the promotion code.-----");
                        mapReturn.put("value", value);
                        mapReturn.put("returnCode", 432);
                        return mapReturn;
                    }
                }
            }
        }

        if ((promoEnt.recurring == null || (!promoEnt.recurring.mode.isEmpty() && promoEnt.recurring.mode.equals("single")))) {
            if (currentDate.before(validFrom)) {
                //Promo code is applied in the valid from and valid to
                logger.debug(promoEnt.requestId + " - " + "-----Promo code is applied in the valid from and valid to-----");
                mapReturn.put("value", value);
                mapReturn.put("returnCode", 413);
                return mapReturn;
            }
            if (currentDate.after(validTo)) {
                //Promo code is applied in the valid from and valid to
                logger.debug(promoEnt.requestId + " - " + "-----Promo code is applied in the valid from and valid to-----");
                mapReturn.put("value", value);
                mapReturn.put("returnCode", 413);
                return mapReturn;
            }
        } else if (promoEnt.recurring.mode.equals("recurring")) {
            List<Date> dates = getDatesInTimeRange(promoEnt.recurring);
            logger.debug(promoEnt.requestId + " - " + "check promo code by date range " + dates.size());
            if (!dates.isEmpty()) {
                for (Date curDate : dates) {
                    currentDate = curDate;
                    logger.debug(promoEnt.requestId + " - " + "currentDate " + dateF.format(currentDate));
                    if (currentDate.before(validFrom)) {
                        //Promo code is applied in the valid from and valid to
                        logger.debug(promoEnt.requestId + " - " + "-----Promo code is applied in the valid from and valid to-----");
                        mapReturn.put("value", value);
                        mapReturn.put("returnCode", 413);
                        return mapReturn;
                    }
                    if (currentDate.after(validTo)) {
                        //Promo code is applied in the valid from and valid to
                        logger.debug(promoEnt.requestId + " - " + "-----Promo code is applied in the valid from and valid to-----");
                        mapReturn.put("value", value);
                        mapReturn.put("returnCode", 413);
                        return mapReturn;
                    }
                }
            }
        }
        // end check valid promo code
        mapReturn.put("value", value);
        mapReturn.put("returnCode", 200);
        return mapReturn;
    }

    private int addPaxPromoList(String fleetId, String userId, PromotionCode promotionCode){
        int returnCode = 200;
        PaxPromoList paxPromoLists = mongoDao.findByFleetIdAndPromoCodeIdAndUserId(fleetId, promotionCode._id.toString(), userId);
        //add to list promo code of pax
        if(paxPromoLists == null){
            String promoCustomerType = "common";
            if(promotionCode.newCustomer)
                promoCustomerType = "new";
            if (promotionCode.referredCustomers)
                promoCustomerType = "referral";
            PaxPromoList promoList = new PaxPromoList();
            promoList.fleetId = fleetId;
            promoList.promoCodeId = promotionCode._id.toString();
            promoList.promotionCode = promotionCode.promotionCode;
            promoList.type = promotionCode.type;
            promoList.value = promotionCode.valueByCurrencies.get(0).value;
            promoList.validFrom = promotionCode.validFrom;
            promoList.validTo = promotionCode.validTo;
            promoList.notes = promotionCode.notes;
            promoList.promoCustomerType = promoCustomerType;
            promoList.userId = userId;
            promoList.valueByCurrencies = promotionCode.valueByCurrencies;
            promoList.applyVerifiedCustomers = promotionCode.applyVerifiedCustomers;
            promoList.termAndCondition = promotionCode.termAndCondition;
            mongoDao.addPaxPromoList(promoList);
        }else {
            returnCode = 305;
        }
        return returnCode;
    }
    
    private void addPromoCodeUse(String bookId, String fleetId, String userId, String code, String promoId,
                                 String currencyISO, double value){
        // check if booking contain character
        String numberId = bookId.replaceAll("[a-zA-Z]", "").trim();
        PromoCodeUse promoCodeUse = new PromoCodeUse();
        promoCodeUse.ticketId = new Long(numberId);
        promoCodeUse.fleetId = fleetId;
        promoCodeUse.customerId = userId;
        promoCodeUse.promoCode = code;
        promoCodeUse.timesUsed = 1;
        promoCodeUse.available = 0;
        promoCodeUse.promoCodeId = promoId;
        promoCodeUse.usedValue = value;
        promoCodeUse.currencyISO = currencyISO;
        promoCodeUse.createdDate = new Timestamp(TimezoneUtil.convertServerToGMT(new Date()).getTime());
        sqlDao.addPromoCodeUse(promoCodeUse);
    }

    private int addReferralHistories(String fleetId, Account psg, Account drv, String referralCode){
        int returnCode = 200;
        ReferralHistories rh = mongoDao.getLinkReferralByRefereeId(psg.userId);
        if(rh == null){
            rh = new ReferralHistories();
            rh.fleetId = fleetId;
            rh.createdDate = new Date();
            rh.refereeId = psg.userId;
            rh.refereeName = psg.fullName;
            rh.refereePhone = psg.phone;
            rh.referralCode = referralCode;
            rh.referralType = "driver";
            rh.userId = drv.userId;
            rh.phone = drv.phone;
            rh.fullName = drv.fullName;
            rh.companyId = drv.driverInfo.company.companyId;
            rh.companyName = drv.driverInfo.company.name;
            mongoDao.addReferralHistories(rh);
        }else {
            returnCode = 305;
        }
        return returnCode;
    }

    public String applyReferralAndAddPromo(PromoEnt promoEnt) {
        ObjResponse objResponse = new ObjResponse();
        objResponse.returnCode = 200;
        try {
            //query with DataBase
            PromotionCode promotionCode = mongoDao.getPromoCodeByFleetAndName(promoEnt.fleetId, promoEnt.promoCode);
            Account driver = mongoDao.getAccount(promoEnt.userId);
            for(String phone:promoEnt.customers){
                Account psg = mongoDao.getAccountByPhoneAndFleetAndType(phone, promoEnt.fleetId, KeysUtil.APPTYPE_PASSENGER);
                if(psg != null){
                    int code = addReferralHistories(promoEnt.fleetId, psg, driver, promoEnt.referralCode);
                    if(code == 200){
                        addPaxPromoList(promoEnt.fleetId, psg.userId, promotionCode);
                        logger.debug(promoEnt.requestId + " - applied referral code for user: " + psg.userId + " phone: " + psg.phone);
                    }
                }
            }
        } catch (Exception ex) {
            logger.debug(promoEnt.requestId + " - " +  "PromoCodeException : " + CommonUtils.getError(ex));
            objResponse.returnCode = 304;
        }
        return objResponse.toString();
    }

    private boolean checkSchedule(String requestId, List<Schedule> schedules, Date currentDate, String timezone){
        boolean checked = false;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate);
        int dayOfWeek = getDayOfWeek(currentDate);
        logger.debug(requestId + " - dayOfWeek : " + dayOfWeek);
        for(Schedule schedule: schedules){
            if(schedule.dayOfWeek == dayOfWeek){
                //check time
                Gson gson = new Gson();
                for(TimeSchedule time: schedule.times){
                    logger.debug(requestId + " - time schedule : " + gson.toJson(time));
                    Date startDate1 = TimezoneUtil.offsetTimeZone(getDateStartTimeRange(TimezoneUtil.offsetTimeZone(currentDate, calendar.getTimeZone().getID(), timezone), time.startTime.hour + ":" + time.startTime.minute), timezone, calendar.getTimeZone().getID());
                    Date startDate = getDateStartTimeRange(currentDate, time.startTime.hour + ":" + time.startTime.minute);
                    logger.debug(requestId + " - old startDate : " + startDate1);
                    logger.debug(requestId + " - new startDate : " + startDate);
                    Date endDate1 = TimezoneUtil.offsetTimeZone(getDateEndTimeRange(TimezoneUtil.offsetTimeZone(currentDate, calendar.getTimeZone().getID(), timezone), time.endTime.hour + ":" + time.endTime.minute), timezone, calendar.getTimeZone().getID());
                    Date endDate = getDateEndTimeRange(currentDate, time.endTime.hour + ":" + time.endTime.minute);
                    logger.debug(requestId + " - old endDate1 : " + endDate1);
                    logger.debug(requestId + " - new endDate : " + endDate);
                    if((currentDate.after(startDate) || currentDate.equals(startDate)) && (currentDate.before(endDate) || currentDate.equals(endDate))){
                        checked = true;
                        break;
                    }
                }
                break;
            }
        }
        return checked;
    }

    private boolean paidByDriver(String fleetId, String promoCode, String customerId) {
        ReferralHistories referralHistories = mongoDao.getLinkReferralByRefereeId(customerId);
        String referrerType = (referralHistories != null && referralHistories.referralType != null) ? referralHistories.referralType : KeysUtil.APPTYPE_DRIVER; // default = driver for old ReferralHistories
        if (referrerType.equals(KeysUtil.APPTYPE_DRIVER)) {
            Referral referral = mongoDao.getReferral(fleetId);
            if(referral!= null && referral.paidByDriver && referral.firstPackage != null && referral.firstPackage.promos != null &&
                    !referral.firstPackage.promos.isEmpty()){
                for(RefereePromo p:referral.firstPackage.promos){
                    if(p.promoCode.equals(promoCode)){
                        return true;
                    }
                }
            }
        } else if (referrerType.equals(KeysUtil.APPTYPE_PASSENGER)) {
            PaxReferral paxReferral = mongoDao.getPaxReferral(fleetId);
            if (paxReferral != null && paxReferral.refererGet != null) {
                String type = paxReferral.refereeGet.type != null ? paxReferral.refereeGet.type : "";
                if (type.equals("promo") && paxReferral.refereeGet.promos != null) {
                    for(RefereePromo p:paxReferral.refereeGet.promos){
                        String code = p.promoCode != null ? p.promoCode : "";
                        if (code.equalsIgnoreCase(promoCode)) {
                            return paxReferral.refereeGet.paidByDriver;
                        }
                    }
                }
            }
            return false;
        }
        return false;
    }

    private Date getDateStartTimeRange(Date date, String time){
        //create Calendar instance
        int timeHour = 0;
        int timeMinute = 0;
        String[] timeHFrom = time.split(":");
        if (timeHFrom.length > 1) {
            timeHour = Integer.valueOf(timeHFrom[0]);
            timeMinute = Integer.valueOf(timeHFrom[1]);
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, timeHour);
        calendar.set(Calendar.MINUTE, timeMinute);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }

    private Date getDateEndTimeRange(Date date, String time){
        //create Calendar instance
        int timeHour = 0;
        int timeMinute = 0;
        String[] timeHFrom = time.split(":");
        if (timeHFrom.length > 1) {
            timeHour = Integer.valueOf(timeHFrom[0]);
            timeMinute = Integer.valueOf(timeHFrom[1]);
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, timeHour);
        calendar.set(Calendar.MINUTE, timeMinute);
        calendar.set(Calendar.SECOND, 59);
        return calendar.getTime();
    }

    private List<Date> getDatesInTimeRange(Recurring recurring) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        long MILLIS_IN_A_DAY = 1000 * 60 * 60 * 24;

        int timeHour = 0;
        int timeMinute = 0;
        String[] timeHFrom = recurring.pickupTime.split(":");
        if (timeHFrom.length > 1) {
            timeHour = Integer.valueOf(timeHFrom[0]);
            timeMinute = Integer.valueOf(timeHFrom[1]);
        }
        List<Date> dates = new ArrayList<Date>();
        if (recurring.selectedDates != null && !recurring.selectedDates.isEmpty()) {
            for (String date : recurring.selectedDates) {
                Date formattedDate = sdf.parse(date);
                Calendar calendar = new GregorianCalendar();
                calendar.setTime(formattedDate);
                calendar.set(Calendar.HOUR_OF_DAY,timeHour);
                calendar.set(Calendar.MINUTE,timeMinute);
                dates.add(calendar.getTime());
            }
        } else {
            Date fromDate = sdf.parse(recurring.fromDate);
            Date toDate = sdf.parse(recurring.toDate);
            toDate = new Date(toDate.getTime() + MILLIS_IN_A_DAY);
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(fromDate);

            while (calendar.getTime().before(toDate))
            {
                calendar.set(Calendar.HOUR_OF_DAY,timeHour);
                calendar.set(Calendar.MINUTE,timeMinute);
                Date result = calendar.getTime();
                if(recurring.weekDays.mon && calendar.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
                    dates.add(result);
                else if(recurring.weekDays.tue && calendar.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY)
                    dates.add(result);
                else if(recurring.weekDays.wed && calendar.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY)
                    dates.add(result);
                else if(recurring.weekDays.thu && calendar.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY)
                    dates.add(result);
                else if(recurring.weekDays.fri && calendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY)
                    dates.add(result);
                else if(recurring.weekDays.sat && calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
                    dates.add(result);
                else if(recurring.weekDays.sun && calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
                    dates.add(result);
                calendar.add(Calendar.DATE, 1);
            }
        }
        return dates;
    }

    public static int getDayOfWeek(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        //create an array of days
        int[] strDays = new int[]{
                0,
                1,
                2,
                3,
                4,
                5,
                6
        };
        return strDays[calendar.get(Calendar.DAY_OF_WEEK) - 1];
    }

    public String convertPaymentType(int paymentType, String gateway){
        String paymentMethod = "";
        switch (paymentType) {
            case 1:
                paymentMethod = "cash";
                break;
            case 2:
                paymentMethod = "credit";
                break;
            case 3:
                paymentMethod = "mDispatcher";
                break;
            case 4:
                paymentMethod = "corpCredit";
                break;
            case 5:
                paymentMethod = "directBilling";
                break;
            case 6:
                paymentMethod = "fleetCard";
                break;
            case 7:
                paymentMethod = "prepaid";
                break;
            case 8:
                paymentMethod = "QRCode";
                break;
            case 9:
                paymentMethod = "ApplePay";
                break;
            case 13:
                paymentMethod = "PaxWallet";
                break;
            case 14:
                paymentMethod = "TnGeWallet";
                break;
            case 19:
                paymentMethod = "boosteWallet";
                break;
            case 20:
                paymentMethod = "Vipps";
                break;
            case 21:
                if(gateway == null)
                    gateway = "ZainCash";
                if(gateway.equals("ZainCash")){
                    paymentMethod = "exZainCash";
                }
                if(gateway.equals("GCash")){
                    paymentMethod = "exGCash";
                }
                break;
            case 23:
                paymentMethod = "GooglePay";
                break;
        }

        return paymentMethod;
    }
}
