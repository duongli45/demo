package com.qupworld.paymentgateway.controllers;

import com.google.gson.Gson;
import com.pg.util.CommonUtils;
import com.pg.util.ConvertUtil;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.TimezoneUtil;
import com.qupworld.paymentgateway.entities.*;
import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.MongoDaoImpl;
import com.qupworld.paymentgateway.models.RedisDao;
import com.qupworld.paymentgateway.models.RedisImpl;
import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.CommissionServices;
import com.qupworld.paymentgateway.models.mongo.collections.FleetService;
import com.qupworld.paymentgateway.models.mongo.collections.account.Account;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateAssignedRate.AffiliateAssignedRate;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareGeneral.RateGeneral;
import com.qupworld.paymentgateway.models.mongo.collections.affiliationCarType.AffiliationCarType;
import com.qupworld.paymentgateway.models.mongo.collections.booking.Booking;
import com.qupworld.paymentgateway.models.mongo.collections.booking.Fare;
import com.qupworld.paymentgateway.models.mongo.collections.booking.Request;
import com.qupworld.paymentgateway.models.mongo.collections.company.Company;
import com.qupworld.paymentgateway.models.mongo.collections.corporate.Corporate;
import com.qupworld.paymentgateway.models.mongo.collections.dynamicFare.DynamicFare;
import com.qupworld.paymentgateway.models.mongo.collections.fleet.Fleet;
import com.qupworld.paymentgateway.models.mongo.collections.fleetfare.DifferentZone;
import com.qupworld.paymentgateway.models.mongo.collections.fleetfare.FleetFare;
import com.qupworld.paymentgateway.models.mongo.collections.packageRate.FeesWithCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.packageRate.PackageRate;
import com.qupworld.paymentgateway.models.mongo.collections.pricingplan.PricingPlan;
import com.qupworld.paymentgateway.models.mongo.collections.serviceFee.ServiceFee;
import com.qupworld.paymentgateway.models.mongo.collections.vehicleType.CorpRate;
import com.qupworld.paymentgateway.models.mongo.collections.vehicleType.Rate;
import com.qupworld.paymentgateway.models.mongo.collections.vehicleType.VehicleType;
import com.qupworld.paymentgateway.models.mongo.collections.zone.Zone;
import com.qupworld.util.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by myasus on 10/9/19.
 */
public class EstimateCtr {
    private MongoDao mongoDao;
    private RedisDao redisDao;
    private EstimateUtil estimateUtil;
    private Gson gson = new Gson();
    private final SimpleDateFormat sdfMongo = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
    private final static Logger logger = LogManager.getLogger(EstimateCtr.class);

    public EstimateCtr() {
        mongoDao = new MongoDaoImpl();
        redisDao = new RedisImpl();
        estimateUtil = new EstimateUtil();
    }

    public FareReturn estimateFare(ETAFareEnt etaFareEnt, String timeZoneId, boolean removeCache) throws IOException {
        logger.debug(etaFareEnt.requestId + " - estimateFare: " + gson.toJson(etaFareEnt));
        FareReturn fareReturn = new FareReturn();
        try {
            // check data estimate fare
            String fleetId = etaFareEnt.fleetId;
            double distance = etaFareEnt.distance;
            String zipCodeFrom = etaFareEnt.zipCodeFrom != null ? etaFareEnt.zipCodeFrom : "";
            String zipCodeTo = etaFareEnt.zipCodeTo != null ? etaFareEnt.zipCodeTo : "";
            // double durationTime = etaFareEnt.duration;
            List<Double> pickup = etaFareEnt.pickup;
            List<Double> destination = etaFareEnt.destination;
            String bookFrom = etaFareEnt.bookFrom;
            int bookType = etaFareEnt.bookType;
            int typeRate = etaFareEnt.typeRate;
            String pickupTime = etaFareEnt.pickupTime;
            int meetDriver = etaFareEnt.meetDriver != null ? etaFareEnt.meetDriver : -1;
            String userId = etaFareEnt.userId != null ? etaFareEnt.userId : "";
            String phone = etaFareEnt.phone != null ? etaFareEnt.phone : "";
            String city = etaFareEnt.city;
            String promoCode = etaFareEnt.promoCode != null ? etaFareEnt.promoCode : "";
            String corporateId = etaFareEnt.corporateId != null ? etaFareEnt.corporateId : "";
            String packageRateId = etaFareEnt.packageRateId != null ? etaFareEnt.packageRateId : "";
            int actualFare = etaFareEnt.actualFare;
            double tip = etaFareEnt.tip != null ? etaFareEnt.tip : -1;
            String currencyISO = "";
            String bookingType = "Now";
            if (!pickupTime.equals("Now")) {
                bookingType = "Reservation";
            }
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            SimpleDateFormat dateF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

            // get timeZoneId from dispatch server
            if (timeZoneId.isEmpty()) {
                timeZoneId = etaFareEnt.timezone != null ? etaFareEnt.timezone : "";
                if(timeZoneId.isEmpty() && etaFareEnt.city != null && !etaFareEnt.city.isEmpty()){
                    timeZoneId = estimateUtil.getTimeZoneId(pickup, city, etaFareEnt.requestId);
                }
            }

            // query with DataBase
            Fleet fleet = mongoDao.getFleetInfor(fleetId);
            FleetFare fleetFare = mongoDao.getFleetFare(fleetId);
            Account account = null;
            if (!userId.isEmpty())
                account = mongoDao.getAccount(userId);// neu account co thi co can check tiep o duoi
            if (KeysUtil.mDispatcher.equals(bookFrom) || KeysUtil.kiosk.equals(bookFrom)) {
                if (!phone.isEmpty())
                    account = mongoDao.getAccountByPhoneAndFleetAndType(phone, fleetId, KeysUtil.APPTYPE_PASSENGER);
            }

            // check rate by corporate if exists different Rate will be apply this rate
            boolean rateByCorp = false;
            if (!corporateId.isEmpty()) {
                Corporate corporate = mongoDao.getCorporate(corporateId);
                if (corporate != null && corporate.pricing != null && corporate.pricing.differentRate != null
                        && corporate.pricing.differentRate)
                    rateByCorp = corporate.pricing.differentRate;
            }

            // format date before calculator fare
            if (timeZoneId.isEmpty()) {
                timeZoneId = fleet.timezone;
            }
            if (pickupTime.equals("Now") || pickupTime.isEmpty())
                pickupTime = ConvertUtil.getUTCISOTime(timeZoneId);
            Date pickupDate = null;
            Date desDate = null;
            int numOfDays = 0;
            if (bookType == 3 || typeRate == 1) {
                etaFareEnt.duration = etaFareEnt.duration * 60;
                logger.debug(etaFareEnt.requestId + " - etaFareEnt.duration: " + etaFareEnt.duration);
                /*
                 * if(etaFareEnt.extraDestination != null &&
                 * !etaFareEnt.extraDestination.isEmpty()){
                 * etaFareEnt.extraDestination.get(0).duration =
                 * etaFareEnt.extraDestination.get(0).duration*60;
                 * logger.debug(etaFareEnt.requestId + " - extraDestination.duration: " +
                 * etaFareEnt.extraDestination.get(0).duration);
                 * }
                 */
            }
            if (!pickupTime.equals("Reservation")) {
                try {
                    pickupDate = TimezoneUtil.offsetTimeZone(sdf.parse(pickupTime), timeZoneId, "GMT");
                } catch (Exception e) {
                    logger.debug(etaFareEnt.requestId + " - " + "format pickup time to yyyy-MM-dd HH:mm error!!!!");
                    pickupDate = dateF.parse(etaFareEnt.pickupTime.replace("Z", ""));
                }
                desDate = ConvertUtil.getCustomizeDate(pickupDate, (int) etaFareEnt.duration);
                numOfDays = ConvertUtil.getDaysBetween(pickupDate, desDate, timeZoneId);
            }

            String unitDistance = fleet.unitDistance;
            if (unitDistance.equalsIgnoreCase(KeysUtil.UNIT_DISTANCE_IMPERIAL)) {
                distance = ConvertUtil.round((distance / 1609.344), 2);
            } else {
                distance = ConvertUtil.round((distance / 1000), 2);
            }

            String zoneId = "";
            String fareHourlyId = "";
            String fareFlatId = "";
            String fareNormalId = "";
            String intercityRateId = "";
            Zone zone = mongoDao.findByFleetIdAndGeo(fleetId, pickup);
            if (zone != null) {
                zoneId = zone._id.toString();
                if (zone.currency != null) {
                    currencyISO = zone.currency.iso;
                }
            }
            logger.debug(etaFareEnt.requestId + " - zoneId: " + zoneId);
            VehicleType vehicleType = null;
            String vehicleTypeId = etaFareEnt.vehicleTypeId != null ? etaFareEnt.vehicleTypeId : "";
            String vehicleTypeObjectId = "";
            if (!vehicleTypeId.isEmpty()) {
                vehicleType = mongoDao.getVehicleTypeByFleetAndName(etaFareEnt.fleetId, vehicleTypeId);
                vehicleTypeObjectId = vehicleType != null ? vehicleType._id.toString() : "";
            }
            logger.debug(etaFareEnt.requestId + " - vehicleType: " + gson.toJson(vehicleType));
            zoneId = "6279cb9b60b2c192a82bd7a9";
            if (vehicleType != null && !zoneId.isEmpty()) {
                if (rateByCorp) {
                    if (vehicleType.corpRates != null && !vehicleType.corpRates.isEmpty()) {
                        for (CorpRate corpRate : vehicleType.corpRates) {
                            if (corpRate.corporateId.equals(corporateId)) {
                                for (Rate rate : corpRate.rates) {
                                    if (rate.rateType.equals(KeysUtil.REGULAR) && rate.zoneId.equals(zoneId))
                                        fareNormalId = rate.rateId;
                                    if (rate.rateType.equals(KeysUtil.FLAT))
                                        fareFlatId = rate.rateId;
                                    if (rate.rateType.equals(KeysUtil.HOURLY) && rate.zoneId.equals(zoneId))
                                        fareHourlyId = rate.rateId;
                                    if (rate.rateType.equals(KeysUtil.INTERCITY) && rate.zoneId.equals(zoneId))
                                        intercityRateId = rate.rateId;
                                }
                                break;
                            }
                        }
                    }
                } else {
                    if (vehicleType.rates != null && !vehicleType.rates.isEmpty()) {
                        for (Rate rate : vehicleType.rates) {
                            if (rate.rateType.equals(KeysUtil.REGULAR) && rate.zoneId.equals(zoneId))
                                fareNormalId = rate.rateId;
                            if (rate.rateType.equals(KeysUtil.FLAT))
                                fareFlatId = rate.rateId;
                            if (rate.rateType.equals(KeysUtil.HOURLY) && rate.zoneId.equals(zoneId))
                                fareHourlyId = rate.rateId;
                            if (rate.rateType.equals(KeysUtil.INTERCITY))
                                intercityRateId = rate.rateId;
                        }
                    }
                }
            }
            numOfDays = numOfDays + 1;
            logger.debug(etaFareEnt.requestId + " - " + "fareNormalId: " + fareNormalId);
            logger.debug(etaFareEnt.requestId + " - " + "fareFlatId: " + fareFlatId);
            logger.debug(etaFareEnt.requestId + " - " + "fareHourlyId: " + fareHourlyId);
            logger.debug(etaFareEnt.requestId + " - " + "intercityRateId: " + intercityRateId);
            List<PackageRate> packageRates = mongoDao.findPackageRateByFareHourlyId(fareHourlyId);
            if ((etaFareEnt.extraDestination == null || etaFareEnt.extraDestination.isEmpty()) &&
                    fareNormalId.isEmpty() && fareFlatId.isEmpty() && bookType == 0 && typeRate == 0) {
                return null;
            } else if (fareHourlyId.isEmpty() && (bookType == 3 || typeRate == 1)) {
                return null;
            } else if (intercityRateId.isEmpty() && typeRate == 3) {
                return null;
            } else if ((bookType == 4 || typeRate == 2) && destination == null && !etaFareEnt.pegasus) {
                return null;
            } else {
                logger.debug(etaFareEnt.requestId + " - rv: " + etaFareEnt.rv);
                boolean compareVersion = ConvertUtil.compareVersion(etaFareEnt.rv, "4.6.2410");
                if (etaFareEnt.pegasus) {
                    // set default type rate if type rate = -1
                    if (typeRate == -1) {
                        if (!fareFlatId.isEmpty() || !fareNormalId.isEmpty())
                            typeRate = 0;
                        else if (!fareHourlyId.isEmpty())
                            typeRate = 1;
                        if (typeRate == 0 && destination == null || typeRate == -1)
                            // type rate == 0 && destination == null - don't calculator fare
                            return null;
                        /*
                         * //get additional service
                         * List<String> services = new ArrayList<>();
                         * List<AdditionalServices> additionalServices =
                         * mongoDao.findByVehicleIdAndServiceType(vehicleType._id.toString(),
                         * "Compulsory");
                         * if(!additionalServices.isEmpty()){
                         * services.addAll(additionalServices.stream().map(s ->
                         * s._id.toString()).collect(Collectors.toList()));
                         * etaFareEnt.services = services;
                         * }
                         */
                    }
                    // get compulsory service
                    if (etaFareEnt.services == null || etaFareEnt.services.isEmpty()) {
                        etaFareEnt.services = estimateUtil.getCompulsoryServices(vehicleType._id.toString(), fleet,
                                fleetFare, zoneId, bookFrom, etaFareEnt.requestId);
                    }
                }

                boolean recurring = etaFareEnt.recurring != null && etaFareEnt.recurring.equals("recurring");

                if (fleet.multiPointCalFareMode != null && fleet.multiPointCalFareMode.equals("perPoint") &&
                        etaFareEnt.extraDestination != null && !etaFareEnt.extraDestination.isEmpty()) {
                    logger.debug(etaFareEnt.requestId + " - Calculator fare with extra destination!");
                    fareReturn = estimateUtil.calFareWithExtraLocation(fleet, fleetFare, account, etaFareEnt, userId,
                            corporateId, bookingType, pickupDate, desDate, fareNormalId, fareFlatId, currencyISO,
                            zoneId,
                            timeZoneId, etaFareEnt.pegasus, vehicleTypeObjectId);
                } else if (etaFareEnt.typeRate == 3) {
                    logger.debug(etaFareEnt.requestId + " - Calculator fare intercity!");
                    fareReturn = estimateUtil.calFareIntercity(fleet, fleetFare, etaFareEnt, zoneId, vehicleTypeObjectId);
                } else {
                    logger.debug(etaFareEnt.requestId + " - " + "type  : " + typeRate);
                    int pricingType = etaFareEnt.pricingType != null ? etaFareEnt.pricingType : 0;
                    String bookId = etaFareEnt.bookId != null ? etaFareEnt.bookId : "";
                    fareReturn = estimateUtil.calculatorFare(fleet, fleetFare, account, userId, corporateId,
                            packageRateId,
                            promoCode, bookType, typeRate, etaFareEnt.duration, distance, numOfDays, bookingType,
                            bookFrom,
                            meetDriver, tip, pickup, destination, zipCodeFrom, zipCodeTo, pickupDate, desDate,
                            actualFare,
                            fareNormalId, fareFlatId, currencyISO, zoneId, etaFareEnt.requestId, etaFareEnt.services,
                            timeZoneId, compareVersion, etaFareEnt.pegasus, recurring, etaFareEnt.paymentMethod,
                            etaFareEnt.jobType, vehicleTypeId, vehicleTypeObjectId, pricingType, bookId);
                }

                // check if booking is customized driver earning
                String bookId = etaFareEnt.bookId != null ? etaFareEnt.bookId : "";
                if (fareReturn != null) {
                    double editedDriverEarning = 0.0;
                    double originalDriverEarning = 0.0;
                    String driverEarningType = "default";
                    if (!bookId.isEmpty()) {
                        Booking booking = mongoDao.getBooking(bookId);
                        if (booking == null) {
                            booking = mongoDao.getBookingCompleted(bookId);
                        }
                        if (booking.request.estimate != null && booking.request.estimate.fare != null) {
                            Fare fare = booking.request.estimate.fare;
                            driverEarningType = fare.driverEarningType != null ? fare.driverEarningType : "default";
                            if (!driverEarningType.equals("default")) {
                                editedDriverEarning = fare.editedDriverEarning;
                                originalDriverEarning = fare.originalDriverEarning;
                            }
                        }
                    }
                    if (!driverEarningType.equals("default")) {
                        fareReturn.driverEarningType = driverEarningType;
                        fareReturn.editedDriverEarning = editedDriverEarning;
                        fareReturn.originalDriverEarning = originalDriverEarning;
                    } else {
                        String commissionType = "";
                        double commission = 0.0;
                        // check fleet commission and company commission
                        String defaultFleetCommissionType = fleetFare.defaultFleetCommissionType != null
                                ? fleetFare.defaultFleetCommissionType
                                : "";
                        String jobType = etaFareEnt.jobType != null ? etaFareEnt.jobType : "";
                        String serviceType = etaFareEnt.serviceType != null ? etaFareEnt.serviceType : jobType;
                        logger.debug(etaFareEnt.requestId + " - serviceType: " + serviceType);
                        if (defaultFleetCommissionType.equals("sameZones")) {
                            List<CommissionServices> services = fleetFare.defaultFleetCommissionValue.sameZones;
                            if (services != null && !services.isEmpty()) {
                                for (CommissionServices commissionServices : services) {
                                    if (serviceType.equals(commissionServices.serviceType)) {
                                        commissionType = commissionServices.type;
                                        commission = commissionServices.value;
                                        break;
                                    }
                                }
                            }
                        } else if (defaultFleetCommissionType.equals("differentZones")) {
                            List<DifferentZone> differentZones = fleetFare.defaultFleetCommissionValue.differentZones;
                            if (differentZones != null && !differentZones.isEmpty()) {
                                for (DifferentZone differentZone : differentZones) {
                                    if (zoneId.equals(differentZone.zoneId)) {
                                        List<CommissionServices> services = differentZone.value;
                                        if (services != null && !services.isEmpty()) {
                                            for (CommissionServices commissionServices : services) {
                                                if (serviceType.equals(commissionServices.serviceType)) {
                                                    commissionType = commissionServices.type;
                                                    commission = commissionServices.value;
                                                    break;
                                                }
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        logger.debug(etaFareEnt.requestId + " - commission type: " + commissionType + " - commission: "
                                + commission);
                        double fleetCommission = commissionType.equalsIgnoreCase("amount") ? commission
                                : commission * fareReturn.subTotal / 100;

                        // check company commission
                        double companyCommission = 0.0;
                        if (vehicleType != null) {
                            String companyId = etaFareEnt.companyId != null ? etaFareEnt.companyId : "all";
                            if (!companyId.equals("all")) {
                                Company company = mongoDao.getCompanyById(companyId);
                                if (company != null && company.commissionValue > 0) {
                                    companyCommission = company.commissionValue * (fareReturn.subTotal - fleetCommission) / 100;
                                }
                            }
                            logger.debug(etaFareEnt.requestId + " - companyCommission value: " + companyCommission);
                        }

                        fareReturn.driverEarningType = driverEarningType;
                        fareReturn.editedDriverEarning = editedDriverEarning;
                        fareReturn.originalDriverEarning = CommonUtils
                                .getRoundValue(fareReturn.subTotal - fleetCommission - companyCommission);
                    }
                }
                logger.debug(etaFareEnt.requestId + " - fareReturn:" + gson.toJson(fareReturn));
                if (fareReturn == null)
                    return null;

                if ((bookType == 3 || typeRate == 1) && !fareHourlyId.isEmpty()
                        && ((etaFareEnt.extraDestination == null || etaFareEnt.extraDestination.isEmpty()) ||
                                (etaFareEnt.extraDestination != null && fleet.multiPointCalFareMode != null
                                        && !fleet.multiPointCalFareMode.equals("perPoint")))) {
                    logger.debug(etaFareEnt.requestId + " - " + "check best price: ");
                    String cache = redisDao.getTmp(etaFareEnt.requestId, etaFareEnt.requestId+"packages:"+fareHourlyId);
                    JSONArray jsonArray = new JSONArray();
                    if (!cache.isEmpty()) {
                        jsonArray = gson.fromJson(cache, JSONArray.class);
                    } else {
                        double bestPrice = 0;
                        if (!packageRates.isEmpty()) {
                            for (PackageRate packageRate : packageRates) {
                                int pricingType = etaFareEnt.pricingType != null ? etaFareEnt.pricingType : 0;
                                FareReturn hourlyFare = estimateUtil.calculatorFare(fleet, fleetFare, account, userId,
                                    corporateId,
                                        packageRate._id.toString(), promoCode, bookType, typeRate, etaFareEnt.duration,
                                    distance, numOfDays,
                                        bookingType, bookFrom, meetDriver, tip, pickup, destination, zipCodeFrom, zipCodeTo,
                                    pickupDate,
                                        desDate, actualFare, fareNormalId, fareFlatId, currencyISO, zoneId,
                                        etaFareEnt.requestId, etaFareEnt.services, timeZoneId, compareVersion,
                                    etaFareEnt.pegasus, recurring, etaFareEnt.paymentMethod, etaFareEnt.jobType,
                                    vehicleTypeId, vehicleTypeObjectId, pricingType, bookId);
                                if (hourlyFare == null)
                                    return null;

                                JSONArray feesByCurrencies = new JSONArray();
                                for (FeesWithCurrency feesWithCurrency : packageRate.feesByCurrencies) {
                                    JSONObject feesByCurrency = new JSONObject();
                                    feesByCurrency.put("currencyISO", feesWithCurrency.currencyISO);
                                    feesByCurrency.put("basedFee", feesWithCurrency.basedFee);
                                    feesByCurrency.put("extraDuration", feesWithCurrency.extraDuration);
                                    feesByCurrency.put("extraDistance", feesWithCurrency.extraDistance);
                                    feesByCurrencies.add(feesByCurrency);
                                }

                                JSONObject object = new JSONObject();
                                object.put("_id", packageRate._id.toString());
                                object.put("name", packageRate.name);
                                object.put("type", packageRate.type);
                                object.put("duration", packageRate.duration);
                                object.put("coveredDistance", packageRate.coveredDistance);
                                object.put("feesByCurrencies", feesByCurrencies);
                                object.put("value", hourlyFare.etaFare);
                                object.put("extraDistanceFee", hourlyFare.extraDistanceFee);
                                object.put("extraDurationFee", hourlyFare.extraDurationFee);
                                if (packageRate.type.equals("hour")) {
                                    object.put("extraDurationType",
                                        packageRate.extraDurationType != null ? packageRate.extraDurationType : "hour");
                                } else {
                                    object.put("extraDurationType", "day");
                                }

                                jsonArray.add(object);
                                if (etaFareEnt.pegasus && typeRate == -1 || packageRateId.isEmpty()) {
                                    if (bestPrice == 0)
                                        bestPrice = hourlyFare.etaFare;
                                    if (hourlyFare.etaFare <= bestPrice) {
                                        bestPrice = hourlyFare.etaFare;
                                        fareReturn = hourlyFare;
                                    }
                                    logger.debug(etaFareEnt.requestId + " - " + "return : " + gson.toJson(fareReturn));
                                }
                            }

                        }
                        redisDao.cacheData(etaFareEnt.requestId, etaFareEnt.requestId+"packages:"+fareHourlyId, jsonArray.toJSONString(), 1);
                    }
                    fareReturn.packages = jsonArray;
                }
                fareReturn.vehicleType = vehicleTypeId;
                fareReturn.actualFare = vehicleType.actualFare;
                if (etaFareEnt.typeRate == 2 || etaFareEnt.bookType == 4)
                    fareReturn.typeRate = 3; // round trip
                else if (etaFareEnt.typeRate == 3)
                    fareReturn.typeRate = 4; // intercity
                else if (fareReturn.normalFare)
                    fareReturn.typeRate = 0; // regular rate
                else if (fareReturn.route != null && !fareReturn.route.isEmpty())
                    fareReturn.typeRate = 1; // flat rate
                else if (fareReturn.basicFare > 0)
                    fareReturn.typeRate = 2; // hourly rate
                else
                    fareReturn.typeRate = 0;
                if (etaFareEnt.extraDestination != null && !etaFareEnt.extraDestination.isEmpty()) {
                    fareReturn.typeRate = 0;
                }

                DecimalFormat df = new DecimalFormat("#.##");
                // Check & Return setting price adjustable
                if (fareReturn.typeRate == 0) {
                    boolean enableAdjust = false;
                    int minAdjust = 0;
                    int maxAdjust = 0;
                    JSONObject priceAdjustable = new JSONObject();
                    PricingPlan pricingPlan = mongoDao.getPricingPlan(fleetId);
                    if (pricingPlan != null && pricingPlan.priceAdjustable != null
                            && pricingPlan.priceAdjustable.enable) {
                        priceAdjustable.put("emoji", pricingPlan.priceAdjustable.emoji);
                        priceAdjustable.put("weblink", pricingPlan.priceAdjustable.weblink);
                        com.qupworld.paymentgateway.models.mongo.collections.fareNormal.FareNormal fareNormal = mongoDao
                                .getFareNormalById(fareNormalId);
                        if (fareNormal != null && fareNormal.priceAdjustment != null) {
                            enableAdjust = fareNormal.priceAdjustment.isActive;
                            minAdjust = fareNormal.priceAdjustment.minimumPercent;
                            maxAdjust = fareNormal.priceAdjustment.maximumPercent;
                            priceAdjustable.put("minimumPercent", minAdjust);
                            priceAdjustable.put("maximumPercent", maxAdjust);
                            priceAdjustable.put("valueByCurrencies", fareNormal.priceAdjustment.valueByCurrencies);
                        }
                    }
                    fareReturn.validPriceAdjustable = true;
                    double etaFare = fareReturn.etaFare;
                    double addOnPrice = Double.valueOf(df.format(etaFareEnt.addOnPrice));
                    double minAdj = estimateUtil.roundingValue(
                            Double.valueOf(df.format(etaFare - minAdjust * 0.01 * etaFare)), fleet.rounding,
                            currencyISO);
                    double maxAdj = estimateUtil.roundingValue(
                            Double.valueOf(df.format(etaFare + maxAdjust * 0.01 * etaFare)), fleet.rounding,
                            currencyISO);
                    logger.debug(etaFareEnt.requestId + " - " + "min price adjustable : " + minAdj);
                    logger.debug(etaFareEnt.requestId + " - " + "max price adjustable : " + maxAdj);
                    logger.debug(etaFareEnt.requestId + " - " + "etaFare : " + etaFare);
                    double fareAdjust = estimateUtil.roundingValue(Double.valueOf(df.format(addOnPrice + etaFare)),
                            fleet.rounding, currencyISO);
                    logger.debug(etaFareEnt.requestId + " - " + "fareAdjust : " + fareAdjust);
                    if (etaFareEnt.addOnPrice != 0 && (minAdj > fareAdjust || maxAdj < fareAdjust))
                        fareReturn.validPriceAdjustable = false;
                    priceAdjustable.put("enable", enableAdjust);
                    fareReturn.priceAdjustable = priceAdjustable;
                    fareReturn.subTotal = Double.valueOf(df.format(fareReturn.subTotal + addOnPrice));
                    fareReturn.etaFare = Double.valueOf(df.format(etaFare + addOnPrice));
                    fareReturn.totalWithoutPromo = Double.valueOf(df.format(fareReturn.totalWithoutPromo + addOnPrice));
                    fareReturn.addOnPrice = addOnPrice;

                    // recheck short trip without dynamicSurcharge
                    boolean shortTrip = estimateUtil.checkWaiveOffCommission(fleet, zoneId,
                            fareReturn.totalWithoutPromo,
                            fareReturn.rushHourFee, currencyISO, pickupDate, timeZoneId, "transport",
                            etaFareEnt.requestId);
                    fareReturn.shortTrip = shortTrip;
                }
                double fleetMarkup = Double.valueOf(df.format(etaFareEnt.fleetMarkup));
                fareReturn.fleetMarkup = fleetMarkup;

                // re-calculator if have edit fare
                logger.debug(etaFareEnt.requestId + " - etaFareEnt.editFare: " + etaFareEnt.editFare);
                if (etaFareEnt.editFare != null) {
                    fareReturn = estimateUtil.checkEditFare(etaFareEnt.requestId, fleet, fareReturn,
                            etaFareEnt.editFare, userId, corporateId,
                            bookFrom, tip);
                    logger.debug(etaFareEnt.requestId + " - fareReturn: " + gson.toJson(fareReturn));
                    // recheck short trip without dynamicSurcharge
                    boolean shortTrip = estimateUtil.checkWaiveOffCommission(fleet, zoneId,
                            fareReturn.totalWithoutPromo,
                            fareReturn.rushHourFee, currencyISO, pickupDate, timeZoneId, "transport",
                            etaFareEnt.requestId);
                    fareReturn.shortTrip = shortTrip;
                }
            }

        } catch (Exception ex) {
            logger.debug(etaFareEnt.requestId + " - " + "estimateFareException : " + CommonUtils.getError(ex)
                    + "    data : " + etaFareEnt.toString());
        }
        if (removeCache) {
            // clear Redis cache before return
            redisDao.removeTmp(etaFareEnt.requestId, etaFareEnt.requestId+"rushHourFeeType");
            redisDao.removeTmp(etaFareEnt.requestId, etaFareEnt.requestId+"rushHourFeeValue");
            redisDao.removeTmp(etaFareEnt.requestId, etaFareEnt.requestId+"packages");
        }
        return fareReturn;
    }

    public ObjResponse estimateDeliveryFareWithMultiCarType(ETADeliveryEnt etaDeliveryEnt) throws IOException {
        ObjResponse etaResponse = new ObjResponse();
        List<DeliveryResponse> deliveryReturns = new ArrayList<>();
        for (RateDetails rateDetails : etaDeliveryEnt.rateDetails) {
            String vehicleTypeId = rateDetails.vehicleTypeId != null ? rateDetails.vehicleTypeId : "";
            logger.debug(etaDeliveryEnt.requestId + " - vehicleTypeId: " + vehicleTypeId);
            if (vehicleTypeId.isEmpty()) {
                deliveryReturns.add(null);
            } else {
                // initial new ETA entity to update menuId and vehicleTypeId
                ETADeliveryEnt etaEntity = gson.fromJson(gson.toJson(etaDeliveryEnt), ETADeliveryEnt.class);
                // check menu-id bound with vehicle
                String fleetId = etaDeliveryEnt.fleetId;
                String requestMenuId = etaDeliveryEnt.menuId != null ? etaDeliveryEnt.menuId : "";
                logger.debug(etaDeliveryEnt.requestId + " - requestMenuId: " + requestMenuId);
                if (!vehicleTypeId.isEmpty()) {
                    VehicleType vehicleType = mongoDao.getVehicleTypeByFleetAndName(fleetId, vehicleTypeId);
                    String menuId = vehicleType.menuId != null ? vehicleType.menuId : "";
                    logger.debug(etaDeliveryEnt.requestId + " - menuId: " + menuId);
                    if (!requestMenuId.isEmpty() && !requestMenuId.equals("bfc-menuid")
                            && !menuId.equals(requestMenuId)) {
                        logger.debug(etaDeliveryEnt.requestId + " - vehicleTypeId is not valid! - return null !");
                        deliveryReturns.add(null);
                        continue;
                    }
                    if (requestMenuId.isEmpty() && requestMenuId.equals("bfc-menuid")) {
                        etaEntity.menuId = menuId;
                    }
                    etaEntity.vehicleTypeId = rateDetails.vehicleTypeId;
                    DeliveryResponse deliveryResponse = estimateDeliveryFare(etaEntity);
                    logger.debug(etaDeliveryEnt.requestId + " - deliveryResponse: " + gson.toJson(deliveryResponse));
                    deliveryReturns.add(deliveryResponse);
                }
            }
        }
        etaResponse.returnCode = deliveryReturns.isEmpty() ? 406 : 200;
        etaResponse.response = deliveryReturns;
        return etaResponse;
    }

    public DeliveryResponse estimateDeliveryFare(ETADeliveryEnt etaFareEnt) throws IOException {
        DeliveryResponse deliveryResponse = new DeliveryResponse();
        try {
            // check data estimate fare
            String fleetId = etaFareEnt.fleetId;
            String vehicleTypeId = etaFareEnt.vehicleTypeId;

            // query with DataBase
            Fleet fleet = mongoDao.getFleetInfor(fleetId);
            FleetFare fleetFare = mongoDao.getFleetFare(fleetId);
            String zoneId = "";
            String deliveryRateId = "";
            String deliveryFlatRateId = "";
            // check rate by corporate if exists different Rate will be apply this rate
            boolean rateByCorp = false;
            logger.debug(etaFareEnt.requestId + " - corporateId: " + etaFareEnt.corporateId);
            if (etaFareEnt.corporateId != null && !etaFareEnt.corporateId.isEmpty()) {
                Corporate corporate = mongoDao.getCorporate(etaFareEnt.corporateId);
                if (corporate != null && corporate.pricing != null && corporate.pricing.differentRate != null
                        && corporate.pricing.differentRate)
                    rateByCorp = corporate.pricing.differentRate;
            }
            logger.debug(etaFareEnt.requestId + " - pricing.differentRate: " + rateByCorp);
            List<Double> geo = null;
            if (etaFareEnt.zoneId != null && !etaFareEnt.zoneId.isEmpty()) {
                zoneId = etaFareEnt.zoneId;
            } else {
                // type delivery
                if (etaFareEnt.deliveryType == 0 && etaFareEnt.pickup != null && etaFareEnt.pickup.geo != null)
                    geo = etaFareEnt.pickup.geo;
                // type shop for you, food, mart
                else if (etaFareEnt.merchants != null && !etaFareEnt.merchants.isEmpty() &&
                        etaFareEnt.merchants.get(0).geo != null)
                    geo = etaFareEnt.merchants.get(0).geo;
                Zone zone = mongoDao.findByFleetIdAndGeo(fleetId, geo);
                if (zone != null) {
                    zoneId = zone._id.toString();
                    if ((etaFareEnt.currencyISO == null || etaFareEnt.currencyISO.isEmpty()) && zone.currency != null) {
                        etaFareEnt.currencyISO = zone.currency.iso;
                    }
                }
            }

            // get timeZoneId from dispatch server
            String timeZoneId = etaFareEnt.timezone != null ? etaFareEnt.timezone :"";
            if(timeZoneId.isEmpty()
                    && etaFareEnt.city != null && !etaFareEnt.city.isEmpty()) {
                timeZoneId = estimateUtil.getTimeZoneId(geo, etaFareEnt.city, etaFareEnt.requestId);
            }

            // format date before calculator fare
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            SimpleDateFormat dateF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            if (timeZoneId.isEmpty()) {
                timeZoneId = fleet.timezone;
            }
            if (etaFareEnt.pickupTime.equals("Now") || etaFareEnt.pickupTime.isEmpty())
                etaFareEnt.pickupTime = ConvertUtil.getUTCISOTime(timeZoneId);
            Date pickupDate = null;
            if (!etaFareEnt.pickupTime.equals("Reservation")) {
                try {
                    pickupDate = TimezoneUtil.offsetTimeZone(sdf.parse(etaFareEnt.pickupTime), timeZoneId, "GMT");
                } catch (Exception e) {
                    logger.debug(etaFareEnt.requestId + " - " + "format pickup time to yyyy-MM-dd HH:mm error!!!!");
                    pickupDate = dateF.parse(etaFareEnt.pickupTime.replace("Z", ""));
                }
            }

            VehicleType vehicleType = null;
            if (vehicleTypeId != null && !vehicleTypeId.isEmpty()) {
                vehicleType = mongoDao.getVehicleTypeByFleetAndName(fleetId, vehicleTypeId);
            } else if (etaFareEnt.deliveryType == 2 || etaFareEnt.deliveryType == 3) {
                List<VehicleType> vehicleTypes = mongoDao.getVehicleTypesByFleet(fleetId);
                for (VehicleType vhcType : vehicleTypes) {
                    if (etaFareEnt.deliveryType == 2 && vhcType.isActive && vhcType.food) {
                        vehicleType = vhcType;
                        break;
                    }
                    if (etaFareEnt.deliveryType == 3 && vhcType.isActive && vhcType.mart) {
                        vehicleType = vhcType;
                        break;
                    }
                }
            }
            logger.debug(etaFareEnt.requestId + " - vehicleType: " + gson.toJson(vehicleType));
            logger.debug(etaFareEnt.requestId + " - zoneId: " + zoneId);
            if (vehicleType != null && !zoneId.isEmpty()) {
                if (rateByCorp) {
                    if (vehicleType.corpRates != null && !vehicleType.corpRates.isEmpty()) {
                        for (CorpRate corpRate : vehicleType.corpRates) {
                            logger.debug(etaFareEnt.requestId + " - corpRate: " + gson.toJson(corpRate));
                            if (corpRate.corporateId.equals(etaFareEnt.corporateId)) {
                                for (Rate rate : corpRate.rates) {
                                    if (rate.rateType.equals(KeysUtil.DELIVERY) && rate.zoneId != null
                                            && rate.zoneId.equals(zoneId))
                                        deliveryRateId = rate.rateId;
                                    if (rate.rateType.equals(KeysUtil.FLAT))
                                        deliveryFlatRateId = rate.rateId;
                                }
                                logger.debug(etaFareEnt.requestId + " - get deliveryRateId from corporate: "
                                        + deliveryRateId);
                                logger.debug(etaFareEnt.requestId + " - get deliveryFlatRateId from corporate: "
                                        + deliveryFlatRateId);
                                break;
                            }
                        }
                    }
                } else {
                    if (vehicleType.rates != null && !vehicleType.rates.isEmpty()) {
                        for (Rate rate : vehicleType.rates) {
                            if (rate.rateType.equals(KeysUtil.FLAT)) {
                                deliveryFlatRateId = rate.rateId;
                            } else if (rate.rateType.equals(KeysUtil.DELIVERY) && rate.zoneId != null
                                    && rate.zoneId.equals(zoneId))
                                deliveryRateId = rate.rateId;
                        }
                    }
                }
            }
            logger.debug(etaFareEnt.requestId + " - " + "deliveryFlatRateId: " + deliveryFlatRateId);
            logger.debug(etaFareEnt.requestId + " - " + "deliveryRateId: " + deliveryRateId);
            if (deliveryRateId.isEmpty() && deliveryFlatRateId.isEmpty()) {
                return null;
            } else {
                logger.debug(etaFareEnt.requestId + " - Calculator fare delivery!");
                deliveryResponse = estimateUtil.calFareDelivery(fleet, fleetFare, etaFareEnt, deliveryFlatRateId,
                        deliveryRateId, zoneId, pickupDate, timeZoneId);
                if (deliveryResponse == null)
                    return null;
                deliveryResponse.vehicleType = vehicleTypeId;
            }
        } catch (Exception ex) {
            logger.debug(etaFareEnt.requestId + " - " + "estimateDeliveryFareException : " + CommonUtils.getError(ex)
                    + "    data : " + etaFareEnt);
        }
        return deliveryResponse;
    }

    public FareReturn estimateFareForAffiliate(ETAFareEnt etaFareEnt) throws IOException {
        FareReturn fareReturn = new FareReturn();
        try {
            // convert data estimate fare
            String corporateId = etaFareEnt.corporateId != null ? etaFareEnt.corporateId : "";
            String bookingType = "Now";
            if (!etaFareEnt.pickupTime.equals("Now")) {
                bookingType = "Reservation";
            }

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            SimpleDateFormat dateF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

            // get timeZoneId from dispatch server
            String timeZoneId = etaFareEnt.timezone != null ? etaFareEnt.timezone : "";
            if (timeZoneId.isEmpty() && etaFareEnt.city != null && !etaFareEnt.city.isEmpty()) {
                timeZoneId = estimateUtil.getTimeZoneId(etaFareEnt.pickup, etaFareEnt.city, etaFareEnt.requestId);
            }

            // query with DataBase
            Fleet fleet = mongoDao.getFleetInfor(etaFareEnt.fleetId);

            // format date before calculator fare
            if (timeZoneId.equals("")) {
                timeZoneId = fleet.timezone;
            }
            if (etaFareEnt.pickupTime.equals("Now") || etaFareEnt.pickupTime.equals(""))
                etaFareEnt.pickupTime = ConvertUtil.getUTCISOTime(timeZoneId);
            Date pickupDate = null;
            Date desDate = null;
            int numOfDays = 0;
            if (etaFareEnt.bookType == 3) {
                etaFareEnt.duration = etaFareEnt.duration * 60;
            }
            if (!etaFareEnt.pickupTime.equals("Reservation")) {
                try {
                    pickupDate = TimezoneUtil.offsetTimeZone(sdf.parse(etaFareEnt.pickupTime), timeZoneId, "GMT");
                } catch (Exception e) {
                    logger.debug(etaFareEnt.requestId + " - " + "format pickup time to yyyy-MM-dd HH:mm error!!!!");
                    pickupDate = dateF.parse(etaFareEnt.pickupTime.replace("Z", ""));
                }
                desDate = ConvertUtil.getCustomizeDate(pickupDate, (int) etaFareEnt.duration);
                numOfDays = ConvertUtil.getDaysBetween(pickupDate, desDate, timeZoneId) + 1;
            }

            // etaFareEnt.distance = ConvertUtil.round((etaFareEnt.distance / 1000), 2);

            String zoneId = "";
            String fareHourlyId = "";
            String fareFlatId = "";
            String fareNormalId = "";
            String vhcTypeId = "";
            String currencyISO = fleet.currencyISOValidation;
            RateGeneral rateGeneral = null;
            AffiliationCarType vehicleType = null;
            if (etaFareEnt.vehicleTypeId != null && !etaFareEnt.vehicleTypeId.isEmpty()) {
                vehicleType = mongoDao.getAffiliationCarTypeByName(etaFareEnt.vehicleTypeId);
                if (vehicleType != null)
                    vhcTypeId = vehicleType._id.toString();
            }
            List<Zone> zone = mongoDao.findByGeoAffiliate(etaFareEnt.pickup, etaFareEnt.fleetId, vhcTypeId);
            if (zone != null && !zone.isEmpty()) {
                zoneId = zone.get(0)._id.toString();
            }
            logger.debug(etaFareEnt.requestId + " - " + "zoneId: " + zoneId);
            if (!vhcTypeId.isEmpty() && !zoneId.isEmpty()) {
                List<AffiliateAssignedRate> listAffiliateAssignedRate = mongoDao.getListAffiliateAssignedRate(vhcTypeId,
                        zoneId);

                AffiliateAssignedRate assignedRateGeneral = listAffiliateAssignedRate.stream()
                        .filter(assignedRate -> assignedRate.type.equalsIgnoreCase(KeysUtil.GENERAL))
                        .findFirst().orElse(null);
                if (assignedRateGeneral != null) {
                    rateGeneral = mongoDao.getAffiliateRateGeneral(assignedRateGeneral.rate.id);
                    currencyISO = rateGeneral.rateInfo.currency.iso;
                }

                AffiliateAssignedRate assignedRateNormal = listAffiliateAssignedRate.stream()
                        .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase(fleet.fleetId) &&
                                assignedRate.type.equalsIgnoreCase(KeysUtil.REGULAR)
                                && assignedRate.priceType.equalsIgnoreCase(KeysUtil.SELLPRICE))
                        .findFirst().orElse(null);
                if (assignedRateNormal == null) {
                    assignedRateNormal = listAffiliateAssignedRate.stream()
                            .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase("") &&
                                    assignedRate.type.equalsIgnoreCase(KeysUtil.REGULAR)
                                    && assignedRate.priceType.equalsIgnoreCase(KeysUtil.SELLPRICE))
                            .findFirst().orElse(null);
                }
                if (assignedRateNormal != null) {
                    fareNormalId = assignedRateNormal.rate.id;
                }

                AffiliateAssignedRate assignedRateHourly = listAffiliateAssignedRate.stream()
                        .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase(fleet.fleetId) &&
                                assignedRate.type.equalsIgnoreCase(KeysUtil.HOURLY)
                                && assignedRate.priceType.equalsIgnoreCase(KeysUtil.SELLPRICE))
                        .findFirst().orElse(null);
                if (assignedRateHourly == null) {
                    assignedRateHourly = listAffiliateAssignedRate.stream()
                            .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase("") &&
                                    assignedRate.type.equalsIgnoreCase(KeysUtil.HOURLY)
                                    && assignedRate.priceType.equalsIgnoreCase(KeysUtil.SELLPRICE))
                            .findFirst().orElse(null);
                }
                if (assignedRateHourly != null) {
                    fareHourlyId = assignedRateHourly.rate.id;
                }

                AffiliateAssignedRate assignedRateFlat = listAffiliateAssignedRate.stream()
                        .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase(fleet.fleetId) &&
                                assignedRate.priceType.equalsIgnoreCase(KeysUtil.SELLPRICE)
                                && assignedRate.type.equalsIgnoreCase(KeysUtil.FLAT))
                        .findFirst().orElse(null);
                if (assignedRateFlat == null) {
                    assignedRateFlat = listAffiliateAssignedRate.stream()
                            .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase("") &&
                                    assignedRate.priceType.equalsIgnoreCase(KeysUtil.SELLPRICE)
                                    && assignedRate.type.equalsIgnoreCase(KeysUtil.FLAT))
                            .findFirst().orElse(null);
                }
                if (assignedRateFlat != null) {
                    fareFlatId = assignedRateFlat.rate.id;
                }
            }
            logger.debug(etaFareEnt.requestId + " - " + "fareFlatId: " + fareFlatId);
            logger.debug(etaFareEnt.requestId + " - " + "fareHourlyId: " + fareHourlyId);
            logger.debug(etaFareEnt.requestId + " - " + "fareNormalId: " + fareNormalId);
            logger.debug(etaFareEnt.requestId + " - " + "rv: " + etaFareEnt.rv);
            boolean compareVersion = ConvertUtil.compareVersion(etaFareEnt.rv, "4.6.2410");
            if (fareHourlyId.isEmpty())
                etaFareEnt.packageRateId = "";
            fareReturn = estimateUtil.calFareAffiliate(fleet, rateGeneral, etaFareEnt, bookingType, pickupDate, desDate,
                    fareNormalId, fareFlatId, numOfDays, currencyISO, timeZoneId,
                    etaFareEnt.requestId, compareVersion, KeysUtil.SELLPRICE);
            fareReturn.vehicleType = etaFareEnt.vehicleTypeId;
            fareReturn.timeZoneId = timeZoneId;
            fareReturn.currencyISO = currencyISO;

            // estimate fare price buy
            if (etaFareEnt.suplierFleetId != null && !etaFareEnt.suplierFleetId.isEmpty()) {
                logger.debug(etaFareEnt.requestId + " - " + "suplierFleetId: " + etaFareEnt.suplierFleetId);
                String suplierFareHourlyId = "";
                String suplierFareFlatId = "";
                String suplierFareNormalId = "";
                String zoneSuplierId = "";
                RateGeneral rateGeneralSuplier = null;
                Fleet suplierFleet = mongoDao.getFleetInfor(etaFareEnt.suplierFleetId);
                if (etaFareEnt.zoneId != null && !etaFareEnt.zoneId.isEmpty()) {
                    zoneSuplierId = etaFareEnt.zoneId;
                } else {
                    Zone zoneSuplier = mongoDao.findByGeoSuplier(etaFareEnt.pickup, etaFareEnt.suplierFleetId,
                            vhcTypeId);
                    if (zoneSuplier != null)
                        zoneSuplierId = zoneSuplier._id.toString();
                }
                if (!vhcTypeId.isEmpty() && !zoneSuplierId.isEmpty()) {
                    List<AffiliateAssignedRate> listAffiliateAssignedRate = mongoDao
                            .getListAffiliateAssignedRate(vhcTypeId, zoneSuplierId);

                    AffiliateAssignedRate assignedRateGeneral = listAffiliateAssignedRate.stream()
                            .filter(assignedRate -> assignedRate.type.equalsIgnoreCase(KeysUtil.GENERAL))
                            .findFirst().orElse(null);
                    if (assignedRateGeneral != null) {
                        rateGeneralSuplier = mongoDao.getAffiliateRateGeneral(assignedRateGeneral.rate.id);
                        currencyISO = rateGeneralSuplier.rateInfo.currency.iso;
                    }

                    AffiliateAssignedRate assignedRateNormal = listAffiliateAssignedRate.stream()
                            .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase(etaFareEnt.suplierFleetId) &&
                                    assignedRate.type.equalsIgnoreCase(KeysUtil.REGULAR)
                                    && assignedRate.priceType.equalsIgnoreCase(KeysUtil.BUYPRICE))
                            .findFirst().orElse(null);
                    if (assignedRateNormal == null) {
                        assignedRateNormal = listAffiliateAssignedRate.stream()
                                .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase("") &&
                                        assignedRate.type.equalsIgnoreCase(KeysUtil.REGULAR)
                                        && assignedRate.priceType.equalsIgnoreCase(KeysUtil.BUYPRICE))
                                .findFirst().orElse(null);
                    }
                    if (assignedRateNormal != null) {
                        suplierFareNormalId = assignedRateNormal.rate.id;
                    }

                    AffiliateAssignedRate assignedRateHourly = listAffiliateAssignedRate.stream()
                            .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase(etaFareEnt.suplierFleetId) &&
                                    assignedRate.type.equalsIgnoreCase(KeysUtil.HOURLY)
                                    && assignedRate.priceType.equalsIgnoreCase(KeysUtil.BUYPRICE))
                            .findFirst().orElse(null);
                    if (assignedRateHourly == null) {
                        assignedRateHourly = listAffiliateAssignedRate.stream()
                                .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase("") &&
                                        assignedRate.type.equalsIgnoreCase(KeysUtil.HOURLY)
                                        && assignedRate.priceType.equalsIgnoreCase(KeysUtil.BUYPRICE))
                                .findFirst().orElse(null);
                    }
                    if (assignedRateHourly != null) {
                        suplierFareHourlyId = assignedRateHourly.rate.id;
                    }

                    AffiliateAssignedRate assignedRateFlat = listAffiliateAssignedRate.stream()
                            .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase(etaFareEnt.suplierFleetId) &&
                                    assignedRate.priceType.equalsIgnoreCase(KeysUtil.BUYPRICE)
                                    && assignedRate.type.equalsIgnoreCase(KeysUtil.FLAT))
                            .findFirst().orElse(null);
                    if (assignedRateFlat == null) {
                        assignedRateFlat = listAffiliateAssignedRate.stream()
                                .filter(assignedRate -> assignedRate.fleetId.equalsIgnoreCase("") &&
                                        assignedRate.priceType.equalsIgnoreCase(KeysUtil.BUYPRICE)
                                        && assignedRate.type.equalsIgnoreCase(KeysUtil.FLAT))
                                .findFirst().orElse(null);
                    }
                    if (assignedRateFlat != null) {
                        suplierFareFlatId = assignedRateFlat.rate.id;
                    }
                }

                logger.debug(etaFareEnt.requestId + " - " + "suplierFareFlatId: " + suplierFareFlatId);
                logger.debug(etaFareEnt.requestId + " - " + "suplierFareHourlyId: " + suplierFareHourlyId);
                logger.debug(etaFareEnt.requestId + " - " + "suplierFareNormalId: " + suplierFareNormalId);
                if (suplierFareHourlyId.isEmpty())
                    etaFareEnt.packageRateId = "";
                FareReturn fareReturnBuy = estimateUtil.calFareAffiliate(suplierFleet, rateGeneralSuplier, etaFareEnt,
                        bookingType, pickupDate, desDate, suplierFareNormalId, suplierFareFlatId, numOfDays,
                        currencyISO,
                        timeZoneId, etaFareEnt.requestId, compareVersion, KeysUtil.BUYPRICE);
                JSONObject object = new JSONObject();
                object.put("basicFare", fareReturnBuy.basicFare);
                object.put("etaFare", fareReturnBuy.etaFare);
                object.put("currencyISO", fareReturnBuy.currencyISO);
                object.put("airportFee", fareReturnBuy.airportFee);
                object.put("meetDriverFee", fareReturnBuy.meetDriverFee);
                object.put("rushHourFee", fareReturnBuy.rushHourFee);
                object.put("techFee", fareReturnBuy.techFee);
                object.put("bookingFee", fareReturnBuy.bookingFee);
                object.put("tollFee", fareReturnBuy.tollFee);
                object.put("parkingFee", fareReturnBuy.parkingFee);
                object.put("gasFee", fareReturnBuy.gasFee);
                object.put("tax", fareReturnBuy.tax);
                object.put("tip", fareReturnBuy.tip);
                object.put("minFare", fareReturnBuy.minFare);
                object.put("normalFare", fareReturnBuy.normalFare);
                object.put("route", fareReturnBuy.route);
                object.put("vehicleType", fareReturnBuy.vehicleType);
                object.put("min", fareReturnBuy.min);
                object.put("otherFees", fareReturnBuy.otherFees);
                object.put("promoAmount", fareReturnBuy.promoAmount);
                object.put("bookingFeeActive", fareReturnBuy.bookingFeeActive);
                object.put("serviceFee", fareReturnBuy.serviceFee);
                object.put("packages", fareReturnBuy.packages);
                object.put("totalWithoutPromo", fareReturnBuy.totalWithoutPromo);
                fareReturn.estimateFareBuy = object;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(etaFareEnt.requestId + " - " + "estimateFareException : " + CommonUtils.getError(ex)
                    + "    data : " + etaFareEnt.toString());
        }
        return fareReturn;
    }

    public ObjResponse estimateFareWithMultiCarType(ETAFareEnt etaFareEnt) throws IOException {
        ObjResponse etaResponse = new ObjResponse();
        List<FareReturn> fareReturns = new ArrayList<>();
        String bookId = etaFareEnt.bookId != null ? etaFareEnt.bookId : "";
        double originPromoAmount = 0.0;
        if (!bookId.isEmpty()) {
            Booking booking = mongoDao.getBooking(bookId);
            if (booking != null) {
                originPromoAmount = booking.request.estimate.fare.originPromoAmount;
                String promoCode = booking.request.promo != null ? booking.request.promo : "";
                if (originPromoAmount == 0 && !promoCode.isEmpty()) {
                    originPromoAmount = booking.request.estimate.fare.promoAmount;
                }
            }
        }
        // get timeZoneId from dispatch server
        String timeZoneId = "";
        for (RateDetails rateDetails : etaFareEnt.rateDetails) {
            if (etaFareEnt.isEmptyString(rateDetails.vehicleTypeId)) {
                FareReturn fareReturn = new FareReturn();
                fareReturn.vehicleType = rateDetails.vehicleTypeId;
                fareReturns.add(fareReturn);
            } else {
                try {
                    ETAFareEnt rqETA = new ETAFareEnt();
                    rqETA.fleetId = etaFareEnt.fleetId;
                    rqETA.bookId = etaFareEnt.bookId;
                    rqETA.distance = rateDetails.distance;
                    rqETA.vehicleTypeId = rateDetails.vehicleTypeId;
                    rqETA.zipCodeFrom = etaFareEnt.zipCodeFrom;
                    rqETA.zipCodeTo = etaFareEnt.zipCodeTo;
                    rqETA.duration = rateDetails.duration;
                    rqETA.pickup = etaFareEnt.pickup;
                    rqETA.destination = etaFareEnt.destination;
                    rqETA.bookFrom = etaFareEnt.bookFrom;
                    rqETA.bookType = rateDetails.bookType;
                    rqETA.pickupTime = rateDetails.pickupTime;
                    rqETA.meetDriver = etaFareEnt.meetDriver;
                    rqETA.userId = etaFareEnt.userId;
                    rqETA.city = etaFareEnt.city;
                    rqETA.timezone = etaFareEnt.timezone;
                    rqETA.tip = etaFareEnt.tip != null ? etaFareEnt.tip : -1;
                    rqETA.promoCode = etaFareEnt.promoCode;
                    rqETA.phone = etaFareEnt.phone;
                    rqETA.corporateId = etaFareEnt.corporateId;
                    rqETA.packageRateId = rateDetails.packageRateId;
                    rqETA.services = rateDetails.services;
                    rqETA.typeRate = rateDetails.typeRate != null ? rateDetails.typeRate : -1;
                    rqETA.extraDestination = rateDetails.extraDestination;
                    rqETA.tripType = etaFareEnt.tripType;
                    rqETA.paymentMethod = etaFareEnt.paymentMethod;
                    if (rqETA.bookType == 3 || rqETA.typeRate == 1) {
                        rqETA.duration = rateDetails.duration / 60;
                        if (rqETA.extraDestination != null && !rqETA.extraDestination.isEmpty()) {
                            rqETA.extraDestination.get(0).duration = rqETA.extraDestination.get(0).duration / 60;
                        }
                    }
                    rqETA.seat = rateDetails.seat;
                    rqETA.luggage = rateDetails.luggage;
                    rqETA.intercityRouteId = rateDetails.intercityRouteId;
                    rqETA.routeNumber = rateDetails.routeNumber;
                    rqETA.pricingType = etaFareEnt.pricingType;
                    rqETA.jobType = etaFareEnt.jobType != null ? etaFareEnt.jobType : "";
                    rqETA.serviceType = etaFareEnt.serviceType != null ? etaFareEnt.serviceType : "";
                    rqETA.companyId = etaFareEnt.companyId;
                    rqETA.requestId = etaFareEnt.requestId;
                    rqETA.rv = "4.6.2600";
                    rqETA.pegasus = true;
                    if (timeZoneId.isEmpty()) {
                        timeZoneId = etaFareEnt.timezone != null ? etaFareEnt.timezone : "";
                        if(timeZoneId.isEmpty()
                                && etaFareEnt.city != null && !etaFareEnt.city.isEmpty()) {
                            timeZoneId = estimateUtil.getTimeZoneId(etaFareEnt.pickup, etaFareEnt.city,
                                    etaFareEnt.requestId);

                        }
                    }

                    FareReturn fareReturn;
                    if (rqETA.pricingType != null && rqETA.pricingType == 1) {
                        logger.debug(etaFareEnt.requestId + " - convert car type affiliate to local");
                        if (!rateDetails.vehicleTypeId.isEmpty()) {
                            AffiliationCarType vehicleType = mongoDao
                                    .getAffiliationCarTypeByName(rateDetails.vehicleTypeId);
                            if (vehicleType != null) {
                                // get list car types local has associated with affiliate car type
                                List<String> listCarTypes = mongoDao.getListCarTypeByAffiliate(etaFareEnt.fleetId,
                                        vehicleType._id.toString());
                                if (!listCarTypes.isEmpty()) {
                                    for (String vehicleTypeId : listCarTypes) {
                                        logger.debug(etaFareEnt.requestId + " - vehicleTypeId: " + vehicleTypeId);
                                        rqETA.vehicleTypeId = vehicleTypeId;
                                        fareReturn = estimateFare(rqETA, timeZoneId, false);
                                        // add data for hydra
                                        fareReturn.qupPreferredAmount = estimateUtil.getQUpPreferred(
                                                etaFareEnt.requestId, fareReturn.etaFare, fareReturn.typeRate,
                                                etaFareEnt.fleetId);
                                        fareReturn.qupSellPrice = CommonUtils
                                                .getRoundValue(fareReturn.etaFare + fareReturn.qupPreferredAmount);
                                        fareReturn.totalWoPromoSellPrice = CommonUtils.getRoundValue(
                                                fareReturn.totalWithoutPromo + fareReturn.qupPreferredAmount);
                                        fareReturn.sellPriceMarkup = CommonUtils
                                                .getRoundValue(fareReturn.qupSellPrice + etaFareEnt.fleetMarkup);
                                        fareReturn.originPromoAmount = originPromoAmount;

                                        // revert original vehicle type after converted for affiliate
                                        fareReturn.vehicleType = rateDetails.vehicleTypeId;
                                        // return local vehicle associated with affiliate type
                                        fareReturn.vehicleTypeLocal = vehicleTypeId;
                                        fareReturn.localFleetId = etaFareEnt.fleetId;

                                        logger.debug(
                                                etaFareEnt.requestId + " - fareReturn: " + gson.toJson(fareReturn));
                                        fareReturns.add(fareReturn);
                                    }
                                } else {
                                    logger.debug(etaFareEnt.requestId
                                            + " - there is no local car type associated with affiliate type");
                                }
                            }
                        }
                    } else {
                        fareReturn = estimateFare(rqETA, timeZoneId, false);
                        // check if booking from third party
                        boolean isThirdParty = false;
                        if (!bookId.isEmpty()) {
                            Booking booking = mongoDao.getBooking(bookId);
                            if (booking != null) {
                                String bookingReference = booking.externalInfo != null
                                        && booking.externalInfo.bookingReference != null
                                                ? booking.externalInfo.bookingReference
                                                : "";
                                isThirdParty = !bookingReference.isEmpty();
                            }
                        } else if (KeysUtil.THIRD_PARTY.contains(etaFareEnt.bookFrom)) {
                            isThirdParty = true;
                        }
                        if (isThirdParty) {
                            logger.debug(etaFareEnt.requestId
                                    + " - book from third party => calculate driver earning for booking");
                            String commissionType = "";
                            double commission = 0.0;
                            // check fleet commission and company commission
                            FleetFare fleetFare = mongoDao.getFleetFare(etaFareEnt.fleetId);
                            String defaultFleetCommissionType = fleetFare.defaultFleetCommissionType != null
                                    ? fleetFare.defaultFleetCommissionType
                                    : "sameZones";
                            logger.debug(etaFareEnt.requestId + " - defaultFleetCommissionType type: "
                                    + defaultFleetCommissionType);
                            if (defaultFleetCommissionType.equals("sameZones")) {
                                List<CommissionServices> services = fleetFare.defaultFleetCommissionValue.sameZones;
                                if (services != null && !services.isEmpty()) {
                                    for (CommissionServices commissionServices : services) {
                                        if (commissionServices.serviceType.equals("transport")) {
                                            commissionType = commissionServices.type;
                                            commission = commissionServices.value;
                                            break;
                                        }
                                    }
                                }
                            } else {
                                List<DifferentZone> differentZones = fleetFare.defaultFleetCommissionValue.differentZones;
                                String zoneId = etaFareEnt.zoneId != null ? etaFareEnt.zoneId : "";
                                if (differentZones != null && !differentZones.isEmpty()) {
                                    for (DifferentZone differentZone : differentZones) {
                                        if (zoneId.equals(differentZone.zoneId)) {
                                            List<CommissionServices> services = differentZone.value;
                                            if (services != null && !services.isEmpty()) {
                                                for (CommissionServices commissionServices : services) {
                                                    if (commissionServices.serviceType.equals("transport")) {
                                                        commissionType = commissionServices.type;
                                                        commission = commissionServices.value;
                                                        break;
                                                    }
                                                }
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                            logger.debug(etaFareEnt.requestId + " - commission type: " + commissionType
                                    + " - commission: " + commission);
                            double fleetCommission = commissionType.equalsIgnoreCase("amount") ? commission
                                    : commission * fareReturn.subTotal / 100;

                            fareReturn.driverEarningType = "default";
                            fareReturn.editedDriverEarning = 0.0;
                            fareReturn.originalDriverEarning = CommonUtils
                                    .getRoundValue(fareReturn.subTotal - fleetCommission);
                        }
                        logger.debug(etaFareEnt.requestId + " - fareReturn: " + gson.toJson(fareReturn));
                        fareReturns.add(fareReturn);
                    }
                } catch (Exception ex) {
                    logger.debug(etaFareEnt.requestId + " - exception: " + CommonUtils.getError(ex));
                }
            }
        }
        // clear Redis cache before return
        redisDao.removeTmp(etaFareEnt.requestId, etaFareEnt.requestId+"rushHourFeeType");
        redisDao.removeTmp(etaFareEnt.requestId, etaFareEnt.requestId+"rushHourFeeValue");
        redisDao.removeTmp(etaFareEnt.requestId, etaFareEnt.requestId+"packages");
        int returnCode = fareReturns.isEmpty() ? 406 : 200;
        etaResponse.returnCode = returnCode;
        etaResponse.response = fareReturns;
        return etaResponse;
    }

    public ObjResponse estimateFareWithAllSupliers(ETAFareEnt etaFareEnt) throws IOException {
        ObjResponse etaResponse = new ObjResponse();
        if (etaFareEnt.supliers != null && !etaFareEnt.supliers.isEmpty()) {
            // get timeZoneId from dispatch server
            String timeZoneId = "";
            JSONArray jsonArray = new JSONArray();
            for (String suplier : etaFareEnt.supliers) {
                JSONObject object = new JSONObject();
                ETAFareEnt rqETA = new ETAFareEnt();
                rqETA.fleetId = etaFareEnt.fleetId;
                rqETA.suplierFleetId = suplier;
                rqETA.distance = etaFareEnt.distance;
                rqETA.vehicleTypeId = etaFareEnt.vehicleTypeId;
                rqETA.zipCodeFrom = etaFareEnt.zipCodeFrom;
                rqETA.zipCodeTo = etaFareEnt.zipCodeTo;
                rqETA.duration = etaFareEnt.duration;
                rqETA.pickup = etaFareEnt.pickup;
                rqETA.destination = etaFareEnt.destination;
                rqETA.bookFrom = etaFareEnt.bookFrom;
                rqETA.bookType = etaFareEnt.bookType;
                rqETA.pickupTime = etaFareEnt.pickupTime;
                rqETA.meetDriver = etaFareEnt.meetDriver;
                rqETA.userId = etaFareEnt.userId;
                rqETA.city = etaFareEnt.city;
                rqETA.timezone = etaFareEnt.timezone;
                rqETA.tip = etaFareEnt.tip != null ? etaFareEnt.tip : -1;
                rqETA.promoCode = etaFareEnt.promoCode;
                rqETA.phone = etaFareEnt.phone;
                rqETA.corporateId = etaFareEnt.corporateId;
                rqETA.packageRateId = etaFareEnt.packageRateId;
                rqETA.services = etaFareEnt.services;
                rqETA.typeRate = etaFareEnt.typeRate;
                if (rqETA.bookType == 3 || rqETA.typeRate == 1) {
                    rqETA.duration = etaFareEnt.duration / 60;
                }
                rqETA.pricingType = etaFareEnt.pricingType;
                rqETA.requestId = etaFareEnt.requestId;
                rqETA.rv = "4.6.3500";
                rqETA.pegasus = true;
                // FareReturn fareReturn = estimateFareForAffiliate(rqETA);
                if (timeZoneId.isEmpty()) {
                    timeZoneId = etaFareEnt.timezone != null ? etaFareEnt.timezone : "";
                    if(timeZoneId.isEmpty()
                            && etaFareEnt.city != null && !etaFareEnt.city.isEmpty()) {
                        timeZoneId = estimateUtil.getTimeZoneId(etaFareEnt.pickup, etaFareEnt.city,
                                etaFareEnt.requestId);

                    }
                }
                FareReturn fareReturn = estimateFare(rqETA, timeZoneId, false);
                object.put("fleetId", suplier);
                if (fareReturn != null) {
                    String type = "regular";
                    JSONObject jsonObject = fareReturn.estimateFareBuy;
                    if (etaFareEnt.typeRate == 2 || etaFareEnt.bookType == 4)
                        type = "roundTrip"; // round trip
                    else if (etaFareEnt.typeRate == 1 || etaFareEnt.bookType == 3)
                        type = "hourly"; // hourly rate
                    else if (Boolean.valueOf(jsonObject.get("normalFare").toString()))
                        type = "regular"; // regular rate
                    else if (!jsonObject.get("route").toString().isEmpty())
                        type = "flat"; // flat rate
                    object.put("fare", jsonObject.get("etaFare"));
                    object.put("type", type);
                    jsonArray.add(object);
                }
            }
            // clear Redis cache before return
            redisDao.removeTmp(etaFareEnt.requestId, etaFareEnt.requestId+"rushHourFeeType");
            redisDao.removeTmp(etaFareEnt.requestId, etaFareEnt.requestId+"rushHourFeeValue");
            redisDao.removeTmp(etaFareEnt.requestId, etaFareEnt.requestId+"packages");

            etaResponse.returnCode = 200;
            etaResponse.response = jsonArray;
            return etaResponse;
        } else {
            etaResponse.returnCode = 306;
            return etaResponse;
        }
    }

    public ObjResponse estimateFareCarHailing(ETAFareEnt etaFareEnt) throws IOException {
        ObjResponse etaResponse = new ObjResponse();
        try {
            FareReturn fareReturn = new FareReturn();
            // check data estimate fare
            double distance = etaFareEnt.distance;
            String pickupTime = etaFareEnt.pickupTime;
            String currencyISO = "";

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            SimpleDateFormat dateF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

            // get timeZoneId from dispatch server
            String timeZoneId = etaFareEnt.timezone != null ? etaFareEnt.timezone : "";
            if(timeZoneId.isEmpty()
                    && etaFareEnt.city != null && !etaFareEnt.city.isEmpty()) {
                timeZoneId = estimateUtil.getTimeZoneId(etaFareEnt.pickup, etaFareEnt.city, etaFareEnt.requestId);
            }

            // query with DataBase
            Fleet fleet = mongoDao.getFleetInfor(etaFareEnt.fleetId);
            FleetFare fleetFare = mongoDao.getFleetFare(etaFareEnt.fleetId);
            Account account = null;
            if (etaFareEnt.userId != null && !etaFareEnt.userId.isEmpty())
                account = mongoDao.getAccount(etaFareEnt.userId);

            // format date before calculator fare
            if (timeZoneId.isEmpty()) {
                timeZoneId = fleet.timezone;
            }
            if (pickupTime.equals("Now") || pickupTime.isEmpty())
                pickupTime = ConvertUtil.getUTCISOTime(timeZoneId);
            Date pickupDate = null;
            Date desDate = null;
            try {
                pickupDate = TimezoneUtil.offsetTimeZone(sdf.parse(pickupTime), timeZoneId, "GMT");
            } catch (Exception e) {
                logger.debug(etaFareEnt.requestId + " - " + "format pickup time to yyyy-MM-dd HH:mm error!!!!");
                pickupDate = dateF.parse(etaFareEnt.pickupTime.replace("Z", ""));
            }
            desDate = ConvertUtil.getCustomizeDate(pickupDate, (int) etaFareEnt.duration);

            String unitDistance = fleet.unitDistance;
            if (unitDistance.equalsIgnoreCase(KeysUtil.UNIT_DISTANCE_IMPERIAL)) {
                distance = ConvertUtil.round((distance / 1609.344), 2);
            } else {
                distance = ConvertUtil.round((distance / 1000), 2);
            }

            String zoneId = "";
            String fareFlatId = "";
            String fareNormalId = "";
            Zone zone = mongoDao.findByFleetIdAndGeo(etaFareEnt.fleetId, etaFareEnt.pickup);
            if (zone != null) {
                zoneId = zone._id.toString();
                if (zone.currency != null) {
                    currencyISO = zone.currency.iso;
                }
            }

            VehicleType vehicleType = null;
            if (!etaFareEnt.vehicleTypeId.isEmpty()) {
                vehicleType = mongoDao.getVehicleTypeByFleetAndName(etaFareEnt.fleetId, etaFareEnt.vehicleTypeId);
            }
            if (vehicleType != null && !zoneId.isEmpty()) {
                if (vehicleType.rates != null && !vehicleType.rates.isEmpty()) {
                    for (Rate rate : vehicleType.rates) {
                        if (rate.rateType.equals(KeysUtil.REGULAR) && rate.zoneId.equals(zoneId))
                            fareNormalId = rate.rateId;
                        if (rate.rateType.equals(KeysUtil.FLAT))
                            fareFlatId = rate.rateId;
                    }
                }
            }
            logger.debug(etaFareEnt.requestId + " - " + "fareNormalId: " + fareNormalId);
            logger.debug(etaFareEnt.requestId + " - " + "fareFlatId: " + fareFlatId);
            if (fareNormalId.isEmpty() && fareFlatId.isEmpty()) {
                etaResponse.returnCode = 306;
                return etaResponse;
            } else {
                // set default type for car-hailing booking
                String jobType = "carHailing";
                String vehicleTypeId = etaFareEnt.vehicleTypeId != null ? etaFareEnt.vehicleTypeId : "";
                fareReturn = estimateUtil.calFareCarHailing(fleet, fleetFare, account, etaFareEnt.bookType,
                        etaFareEnt.duration,
                        distance, etaFareEnt.pickup, etaFareEnt.destination, etaFareEnt.zipCodeFrom,
                        etaFareEnt.zipCodeTo,
                        pickupDate, desDate, fareNormalId, fareFlatId, currencyISO, zoneId, etaFareEnt.requestId,
                        etaFareEnt.services, timeZoneId,
                        jobType, etaFareEnt.typeRate, vehicleTypeId);
                fareReturn.vehicleType = etaFareEnt.vehicleTypeId;
                if (fareReturn.normalFare)
                    fareReturn.typeRate = 0; // regular rate
                else if (fareReturn.route != null && !fareReturn.route.isEmpty())
                    fareReturn.typeRate = 1; // flat rate
                etaResponse.returnCode = 200;
                etaResponse.response = fareReturn;
                return etaResponse;
            }
        } catch (Exception ex) {
            logger.debug(etaFareEnt.requestId + " - " + "estimateFareCarHailingException : " + CommonUtils.getError(ex)
                    + "    data : " + etaFareEnt.toString());
            etaResponse.returnCode = 306;
            return etaResponse;
        }
    }

    public ObjResponse estimateFareMeter(ETAFareMeterEnt etaFareMeterEnt) throws IOException {
        ObjResponse etaResponse = new ObjResponse();
        try {
            FareReturn fareReturn = new FareReturn();
            Calendar cal = Calendar.getInstance();
            // check bookId
            Booking booking = mongoDao.getBooking(etaFareMeterEnt.bookId);
            if (booking != null) {
                // get data estimate fare
                double distance = etaFareMeterEnt.distance;
                String bookingType = "Now";
                if (booking.reservation) {
                    bookingType = "Reservation";
                }
                String corporateId = booking.corporateInfo != null && booking.corporateInfo.corporateId != null
                        ? booking.corporateInfo.corporateId
                        : "";
                String zoneId = booking.request.pickup.zoneId;
                boolean recurring = booking.recurring != null && booking.recurring.mode.equals("recurring");
                String mDispatcherId = booking.mDispatcherInfo != null ? booking.mDispatcherInfo.userId : "";
                Request request = booking.request;
                String vehicleTypeRequest = "";
                List<String> services = new ArrayList<>();
                int meetDriver = -1;
                boolean editedFare = false;
                boolean markupPrice = false;
                boolean addOnPrice = false;
                String flatRouteId = null;
                boolean isReverseRoute = false;
                Fare estimateFare = null;
                if (request != null) {
                    vehicleTypeRequest = request.vehicleTypeRequest;
                    if (request.moreInfo != null && request.moreInfo.flightInfo != null)
                        meetDriver = booking.request.moreInfo.flightInfo.type;
                    if (request.services != null && !request.services.isEmpty()) {
                        services = booking.request.services.stream().filter(s -> s.active).map(s -> s.serviceId)
                                .collect(Collectors.toList());
                    } else if (request.fleetServices != null && !request.fleetServices.isEmpty()) {
                        services = booking.request.fleetServices.stream().filter(s -> s.active).map(s -> s.serviceId)
                                .collect(Collectors.toList());
                    }
                    if (request.estimate != null) {
                        editedFare = request.estimate.isFareEdited;
                        if (request.estimate.fare != null) {
                            estimateFare = request.estimate.fare;
                            markupPrice = estimateFare.markupDifference != 0;
                            addOnPrice = estimateFare.addOnPrice != 0;
                            flatRouteId = estimateFare.routeId;
                            isReverseRoute = estimateFare.reverseRoute;
                        }
                    }
                }
                // check edited fare by Operator or markup price
                if (editedFare || markupPrice || addOnPrice) {
                    // return edited fare
                    fareReturn.basicFare = estimateFare.basicFare;
                    fareReturn.normalFare = estimateFare.normalFare != null && estimateFare.normalFare;
                    fareReturn.currencyISO = booking.currencyISO;
                    fareReturn.route = estimateFare.route;
                    fareReturn.routeId = estimateFare.routeId;
                    fareReturn.reverseRoute = estimateFare.reverseRoute;
                    fareReturn.airportFee = estimateFare.airportFee;
                    fareReturn.rushHourFee = estimateFare.rushHourFee;
                    fareReturn.dynamicSurcharge = estimateFare.dynamicSurcharge;
                    fareReturn.surchargeParameter = estimateFare.surchargeParameter;
                    fareReturn.meetDriverFee = estimateFare.meetDriverFee;
                    fareReturn.techFee = estimateFare.techFee;
                    fareReturn.otherFees = estimateFare.otherFees;
                    fareReturn.minFare = estimateFare.minFare;
                    fareReturn.min = estimateFare.min;
                    fareReturn.bookingFee = estimateFare.bookingFee;
                    fareReturn.tollFee = estimateFare.tollFee;
                    fareReturn.parkingFee = estimateFare.parkingFee;
                    fareReturn.gasFee = estimateFare.gasFee;
                    fareReturn.tax = estimateFare.tax;
                    fareReturn.tip = estimateFare.tip;
                    fareReturn.promoAmount = estimateFare.promoAmount;
                    fareReturn.serviceFee = estimateFare.serviceFee;
                    fareReturn.dynamicFare = estimateFare.dynamicFare;
                    fareReturn.subTotal = estimateFare.subTotal;
                    fareReturn.promoCode = estimateFare.promoCode;
                    fareReturn.taxSetting = estimateFare.taxSetting;
                    fareReturn.etaFare = estimateFare.etaFare;
                    fareReturn.shortTrip = estimateFare.shortTrip;
                    fareReturn.typeRate = estimateFare.typeRate;
                    etaResponse.returnCode = 200;
                    etaResponse.response = fareReturn;
                    return etaResponse;
                }
                // query with DataBase
                Fleet fleet = mongoDao.getFleetInfor(booking.fleetId);
                FleetFare fleetFare = mongoDao.getFleetFare(booking.fleetId);
                // check rate by corporate if exists different Rate will be apply this rate
                boolean rateByCorp = false;
                if (!corporateId.isEmpty()) {
                    Corporate corporate = mongoDao.getCorporate(corporateId);
                    if (corporate != null && corporate.pricing != null && corporate.pricing.differentRate != null
                            && corporate.pricing.differentRate)
                        rateByCorp = corporate.pricing.differentRate;
                }
                Account account = mongoDao.getAccount(booking.psgInfo.userId);
                String unitDistance = fleet.unitDistance;
                if (unitDistance.equalsIgnoreCase(KeysUtil.UNIT_DISTANCE_IMPERIAL)) {
                    distance = ConvertUtil.round((distance / 1609.344), 2);
                } else {
                    distance = ConvertUtil.round((distance / 1000), 2);
                }
                Date pickupDate = null;
                Date desDate = null;
                if (booking.time != null) {
                    if (booking.time.pickUpTime != null) {
                        pickupDate = TimezoneUtil.offsetTimeZone(booking.time.pickUpTime,
                                cal.getTimeZone().getID(), "GMT");
                        logger.debug(etaFareMeterEnt.requestId + " - " + "pickupDate: " + pickupDate);
                        desDate = ConvertUtil.getCustomizeDate(pickupDate, (int) etaFareMeterEnt.duration);
                    }
                }
                int numOfDays = 0;
                String fareNormalId = "";
                VehicleType vehicleType = null;
                String vehicleTypeId = "";
                if (!vehicleTypeRequest.isEmpty()) {
                    vehicleType = mongoDao.getVehicleTypeByFleetAndName(booking.fleetId, vehicleTypeRequest);
                    vehicleTypeId = vehicleType != null ? vehicleType._id.toString() : "";
                }
                if (vehicleType != null && !zoneId.isEmpty()) {
                    if (rateByCorp) {
                        if (vehicleType.corpRates != null && !vehicleType.corpRates.isEmpty()) {
                            for (CorpRate corpRate : vehicleType.corpRates) {
                                if (corpRate.corporateId.equals(corporateId)) {
                                    for (Rate rate : corpRate.rates) {
                                        if (rate.rateType.equals(KeysUtil.REGULAR) && rate.zoneId.equals(zoneId))
                                            fareNormalId = rate.rateId;
                                    }
                                    break;
                                }
                            }
                        }
                    } else {
                        if (vehicleType.rates != null && !vehicleType.rates.isEmpty()) {
                            for (Rate rate : vehicleType.rates) {
                                if (rate.rateType.equals(KeysUtil.REGULAR) && rate.zoneId.equals(zoneId))
                                    fareNormalId = rate.rateId;
                            }
                        }
                    }
                }
                logger.debug(etaFareMeterEnt.requestId + " - " + "fareNormalId: " + fareNormalId);
                double tip = request.tip != null ? request.tip : 0;
                int type = request.type != null ? request.type : 0;
                fareReturn = estimateUtil.calFareByMeter(booking, fleet, fleetFare, account, mDispatcherId, corporateId,
                        request.packageRateId, request.promo, type, request.typeRate, etaFareMeterEnt.duration,
                        distance, numOfDays, bookingType, booking.bookFrom, meetDriver, tip, request.pickup.geo,
                        etaFareMeterEnt.destination, pickupDate, desDate, fareNormalId, flatRouteId, isReverseRoute,
                        booking.currencyISO, zoneId, etaFareMeterEnt.requestId, services, request.pickup.timezone,
                        recurring, vehicleTypeId);
                fareReturn.vehicleType = booking.request.vehicleTypeRequest;
                if (booking.request.typeRate == 2 || booking.request.type == 4)
                    fareReturn.typeRate = 3; // round trip
                else if (fareReturn.normalFare)
                    fareReturn.typeRate = 0; // regular rate
                else if (fareReturn.route != null && !fareReturn.route.isEmpty())
                    fareReturn.typeRate = 1; // flat rate
                else if (fareReturn.basicFare > 0)
                    fareReturn.typeRate = 2; // hourly rate
                else
                    fareReturn.typeRate = 0;

                etaResponse.returnCode = 200;
                etaResponse.response = fareReturn;
                return etaResponse;
            } else {
                logger.debug(etaFareMeterEnt.requestId + " - " + "bookId not exists !!!");
                etaResponse.returnCode = 306;
                return etaResponse;
            }
        } catch (Exception ex) {
            logger.debug(etaFareMeterEnt.requestId + " - " + "estimateFareCarHailingException : "
                    + CommonUtils.getError(ex) + "    data : " + etaFareMeterEnt.toString());
            etaResponse.returnCode = 306;
            return etaResponse;
        }
    }

    public ObjResponse estimateFareSharing(ETASharingEnt sharingEnt) throws IOException {
        ObjResponse etaResponse = new ObjResponse();
        try {
            ETAFareSharingResponse response = new ETAFareSharingResponse();
            String fareSharingId = "";

            // check bookId
            Booking booking = mongoDao.getBooking(sharingEnt.bookId);
            if (booking != null && booking.streetSharing != null) {
                // get data estimate fare
                int order = sharingEnt.currentWaypoint.order;
                double distance = sharingEnt.currentWaypoint.distance;
                double duration = sharingEnt.currentWaypoint.duration;
                String zoneId = "";
                Request request = booking.request;
                String vehicleTypeRequest = "";
                if (request != null) {
                    zoneId = booking.request.pickup.zoneId;
                    vehicleTypeRequest = request.vehicleTypeRequest;
                }

                // query with DataBase
                Fleet fleet = mongoDao.getFleetInfor(booking.fleetId);
                String unitDistance = fleet.unitDistance;
                if (unitDistance.equalsIgnoreCase(KeysUtil.UNIT_DISTANCE_IMPERIAL)) {
                    distance = ConvertUtil.round((distance / 1609.344), 2);
                } else {
                    distance = ConvertUtil.round((distance / 1000), 2);
                }
                VehicleType vehicleType = null;
                if (!vehicleTypeRequest.isEmpty()) {
                    vehicleType = mongoDao.getVehicleTypeByFleetAndName(booking.fleetId, vehicleTypeRequest);
                }
                if (vehicleType != null && !zoneId.isEmpty()) {
                    if (vehicleType.rates != null && !vehicleType.rates.isEmpty()) {
                        for (Rate rate : vehicleType.rates) {
                            if (rate.rateType.equals(KeysUtil.SHARING) && rate.zoneId.equals(zoneId))
                                fareSharingId = rate.rateId;
                        }
                    }
                }
                logger.debug(sharingEnt.requestId + " - " + "fareSharingId: " + fareSharingId);
                response = estimateUtil.calFareSharing(fleet, booking.streetSharing, fleet.rounding,
                        sharingEnt.dropOffOrder,
                        order, duration, distance, fareSharingId, booking.currencyISO, zoneId, sharingEnt.requestId);
                etaResponse.returnCode = 200;
                etaResponse.response = response;
                return etaResponse;
            } else if (sharingEnt.vehicleType != null && !sharingEnt.vehicleType.isEmpty() &&
                    sharingEnt.fleetId != null && !sharingEnt.fleetId.isEmpty() &&
                    sharingEnt.currentLocation != null && !sharingEnt.currentLocation.isEmpty()) {
                logger.debug(sharingEnt.requestId + " - " + "get starting fee !!!");
                String zoneId = "";
                String currencyISO = "";
                Fleet fleet = mongoDao.getFleetInfor(sharingEnt.fleetId);
                Zone zone = mongoDao.findByFleetIdAndGeo(sharingEnt.fleetId, sharingEnt.currentLocation);
                if (zone != null) {
                    zoneId = zone._id.toString();
                    if (zone.currency != null) {
                        currencyISO = zone.currency.iso;
                    }
                }
                VehicleType vehicleType = mongoDao.getVehicleTypeByFleetAndName(sharingEnt.fleetId,
                        sharingEnt.vehicleType);
                if (vehicleType != null && !zoneId.isEmpty()) {
                    if (vehicleType.rates != null && !vehicleType.rates.isEmpty()) {
                        for (Rate rate : vehicleType.rates) {
                            if (rate.rateType.equals(KeysUtil.SHARING) && rate.zoneId.equals(zoneId))
                                fareSharingId = rate.rateId;
                        }
                    }
                    logger.debug(sharingEnt.requestId + " - " + "fareSharingId: " + fareSharingId);
                    List<Object> dataFare = estimateUtil.getBaseFeeSharing(true, sharingEnt.numOfPax, 0, 0,
                            fareSharingId, currencyISO, sharingEnt.requestId);
                    double feePerPax = Double.parseDouble(dataFare.get(0).toString());
                    double minimum = Double.parseDouble(dataFare.get(1).toString());
                    // get tech fee
                    List<Object> getTechFee = estimateUtil.getTechFeeValueByZone(fleet, KeysUtil.street_sharing,
                            currencyISO, zoneId);
                    double techFee = Double.parseDouble(getTechFee.get(0).toString());
                    logger.debug(sharingEnt.requestId + " - " + "techFee: " + techFee);

                    ETAFareSharingResponse sharingResponse = new ETAFareSharingResponse();
                    double totalFare = 0;
                    List<Passenger> passengers = new ArrayList<>();
                    for (int i = 1; i <= sharingEnt.numOfPax; i++) {
                        Passenger p = new Passenger();
                        p.order = i;
                        p.baseFare = feePerPax;
                        p.techFee = techFee;
                        if (p.baseFare < minimum)
                            p.baseFare = minimum;
                        p.totalFare = p.baseFare + techFee;
                        totalFare += p.totalFare;
                        passengers.add(p);
                    }
                    sharingResponse.passengers = passengers;
                    sharingResponse.totalFare = totalFare;
                    sharingResponse.currencyISO = currencyISO;
                    etaResponse.response = sharingResponse;
                    etaResponse.returnCode = 200;
                    return etaResponse;
                } else {
                    logger.debug(sharingEnt.requestId + " - " + "vehicle type not exists !!!");
                    etaResponse.returnCode = 306;
                    return etaResponse;
                }
            } else {
                logger.debug(sharingEnt.requestId + " - " + "data request failed - " + gson.toJson(sharingEnt));
                etaResponse.returnCode = 306;
                return etaResponse;
            }
        } catch (Exception ex) {
            logger.debug(sharingEnt.requestId + " - " + "estimateFareSharingException : " + CommonUtils.getError(ex)
                    + "    data : " + gson.toJson(sharingEnt));
            etaResponse.returnCode = 306;
            return etaResponse;
        }
    }

    private FareReturn createFareReturnForThirdParty(ETAThirdPartyEnt etaFareEnt) {
        String requestId = etaFareEnt.requestId;
        try {
            FareReturn fareReturn = new FareReturn();
            double basicFare = etaFareEnt.amount;
            fareReturn.basicFare = CommonUtils.getRoundValue(basicFare);
            fareReturn.normalFare = true;
            fareReturn.currencyISO = etaFareEnt.currencyISO;
            fareReturn.route = "";
            fareReturn.routeId = "";
            fareReturn.reverseRoute = false;
            fareReturn.airportFee = 0.0;
            fareReturn.rushHourFee = 0.0;
            fareReturn.dynamicSurcharge = 0.0;
            fareReturn.surchargeParameter = 0.0;
            fareReturn.meetDriverFee = 0.0;
            fareReturn.techFee = 0.0;
            fareReturn.otherFees = 0.0;
            fareReturn.minFare = 0.0;
            fareReturn.min = false;
            fareReturn.extraDistanceFee = 0.0;
            fareReturn.extraDurationFee = 0.0;
            fareReturn.bookingFee = 0.0;
            fareReturn.tollFee = 0.0;
            fareReturn.parkingFee = 0.0;
            fareReturn.gasFee = 0.0;
            fareReturn.bookingFeeActive = false;
            fareReturn.tax = 0.0;
            fareReturn.tip = 0.0;
            fareReturn.serviceFee = 0.0;
            fareReturn.fleetCommissionFromFleetServiceFee = 0.0;
            fareReturn.driverCommissionFromFleetServiceFee = 0.0;
            fareReturn.dynamicFare = 0.0;
            fareReturn.dynamicType = "factor";
            fareReturn.promoCode = "";
            fareReturn.taxSetting = 0.0;
            fareReturn.promoAmount = 0.0;
            fareReturn.subTotal = CommonUtils.getRoundValue(basicFare);
            fareReturn.unroundedTotalAmt = basicFare;
            fareReturn.etaFare = basicFare;
            fareReturn.markupEstimate = 0.0;
            fareReturn.markupEstimateAmount = fareReturn.unroundedTotalAmt;
            fareReturn.shortTrip = false;
            fareReturn.creditTransactionFeePercent = 0.0;
            fareReturn.creditTransactionFeeAmount = 0.0;
            fareReturn.vehicleType = etaFareEnt.vehicleTypeId != null ? etaFareEnt.vehicleTypeId : "";
            fareReturn.actualFare = false;
            fareReturn.typeRate = 0;

            String commissionType = "";
            double commission = 0.0;
            // check fleet commission and company commission
            FleetFare fleetFare = mongoDao.getFleetFare(etaFareEnt.fleetId);
            String defaultFleetCommissionType = fleetFare.defaultFleetCommissionType != null
                    ? fleetFare.defaultFleetCommissionType
                    : "sameZones";
            logger.debug(requestId + " - defaultFleetCommissionType type: " + defaultFleetCommissionType);
            if (defaultFleetCommissionType.equals("sameZones")) {
                List<CommissionServices> services = fleetFare.defaultFleetCommissionValue.sameZones;
                if (services != null && !services.isEmpty()) {
                    for (CommissionServices commissionServices : services) {
                        if (commissionServices.serviceType.equals("transport")) {
                            commissionType = commissionServices.type;
                            commission = commissionServices.value;
                            break;
                        }
                    }
                }
            } else {
                List<DifferentZone> differentZones = fleetFare.defaultFleetCommissionValue.differentZones;
                String zoneId = etaFareEnt.zoneId != null ? etaFareEnt.zoneId : "";
                if (differentZones != null && !differentZones.isEmpty()) {
                    for (DifferentZone differentZone : differentZones) {
                        if (zoneId.equals(differentZone.zoneId)) {
                            List<CommissionServices> services = differentZone.value;
                            if (services != null && !services.isEmpty()) {
                                for (CommissionServices commissionServices : services) {
                                    if (commissionServices.serviceType.equals("transport")) {
                                        commissionType = commissionServices.type;
                                        commission = commissionServices.value;
                                        break;
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
            }
            logger.debug(requestId + " - commission type: " + commissionType + " - commission: " + commission);
            double fleetCommission = commissionType.equalsIgnoreCase("amount") ? commission
                    : commission * fareReturn.subTotal / 100;

            fareReturn.driverEarningType = "default";
            fareReturn.editedDriverEarning = 0.0;
            fareReturn.originalDriverEarning = CommonUtils.getRoundValue(fareReturn.subTotal - fleetCommission);
            return fareReturn;
        } catch (Exception ex) {
            logger.debug(requestId + " - commission type: " + CommonUtils.getError(ex));
            return null;
        }
    }

    public String estimateFareForThirdParty(ETAThirdPartyEnt etaFareEnt) {
        String requestId = etaFareEnt.requestId;
        ObjResponse etaResponse = new ObjResponse();
        try {
            FareReturn fareReturn = createFareReturnForThirdParty(etaFareEnt);
            if (fareReturn == null) {
                etaResponse.returnCode = 525;
            } else {
                etaResponse.returnCode = 200;
                etaResponse.response = fareReturn;
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - " + "estimateFareException : " + CommonUtils.getError(ex) + "    data : "
                    + etaFareEnt.toString());
            etaResponse.returnCode = 525;
        }
        return etaResponse.toString();
    }
}
