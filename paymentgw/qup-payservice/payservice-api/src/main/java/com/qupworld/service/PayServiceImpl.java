package com.qupworld.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.qupworld.paymentgateway.controllers.EstimateCtr;
import com.qupworld.paymentgateway.controllers.PaymentServiceCtr;
import com.qupworld.paymentgateway.controllers.PromoCtr;
import com.qupworld.paymentgateway.entities.*;
import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.MongoDaoImpl;
import com.qupworld.paymentgateway.models.mongo.collections.affiliationCarType.AffiliationCarType;
import com.qupworld.paymentgateway.models.mongo.collections.booking.Booking;
import com.pg.util.CommonUtils;
import com.qupworld.util.EstimateUtil;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;
import java.util.UUID;

/**
 * Created by qup on 09/05/2018.
 */
@Service
public class PayServiceImpl implements PayService {
    private final static Logger logger = LogManager.getLogger(PayServiceImpl.class);
    private Gson gson = new Gson();
    PromoCtr promoCtr = new PromoCtr();
    EstimateCtr estimateCtr = new EstimateCtr();
    PaymentServiceCtr paymentServiceCtr = new PaymentServiceCtr();

    @Override
    public String getVersion() {
        return "4.6.7103";
    }

    @Override
    public String estimateFare(ETAFareEnt etaFareEnt) {
        try {
            etaFareEnt.setRequestId(etaFareEnt.requestId);
            logger.debug(etaFareEnt.requestId + " - " + "estimateFare : " + gson.toJson(etaFareEnt));
            ObjResponse etaResponse = new ObjResponse();
            int returnCode = etaFareEnt.validateData();
            if (returnCode != 200) {
                etaResponse.returnCode = returnCode;
                logger.debug(etaFareEnt.requestId + " - " + "estimateFareResponse : " + etaResponse.toString());
                return etaResponse.toString();
            }
            FareReturn fareReturn;
            // check pricing type: 0 - local | 1 - affiliate
            if (etaFareEnt.pricingType != null && etaFareEnt.pricingType == 1) {
                //fareReturn = estimateCtr.estimateFareForAffiliate(etaFareEnt);
                fareReturn = estimateCtr.estimateFare(etaFareEnt, "", true);
                // add data for hydra
                EstimateUtil estimateUtil = new EstimateUtil();
                fareReturn.qupPreferredAmount = estimateUtil.getQUpPreferred(etaFareEnt.requestId, fareReturn.etaFare, fareReturn.typeRate, etaFareEnt.fleetId);
                fareReturn.qupSellPrice = CommonUtils.getRoundValue(fareReturn.etaFare + fareReturn.qupPreferredAmount);
                fareReturn.totalWoPromoSellPrice = CommonUtils.getRoundValue(fareReturn.totalWithoutPromo + fareReturn.qupPreferredAmount);
                fareReturn.sellPriceMarkup = CommonUtils.getRoundValue(fareReturn.qupSellPrice + etaFareEnt.fleetMarkup);
                String bookId = etaFareEnt.bookId != null ? etaFareEnt.bookId : "";
                double originPromoAmount = 0.0;
                if (!bookId.isEmpty()) {
                    MongoDao mongoDao = new MongoDaoImpl();
                    Booking booking = mongoDao.getBooking(bookId);
                    if (booking != null) {
                        originPromoAmount = booking.request.estimate.fare.originPromoAmount;
                        String promoCode = booking.request.promo != null ? booking.request.promo : "";
                        if (originPromoAmount == 0 && !promoCode.isEmpty()) {
                            originPromoAmount = booking.request.estimate.fare.promoAmount;
                        }
                    }
                }
                fareReturn.originPromoAmount = originPromoAmount;
                // revert original vehicle type after converted for affiliate
                fareReturn.vehicleType = etaFareEnt.vehicleTypeId;

                logger.debug(etaFareEnt.requestId + " - fareReturn: " + gson.toJson(fareReturn));
            } else {
                fareReturn = estimateCtr.estimateFare(etaFareEnt, "", true);
            }
            if (fareReturn != null) {
                if (fareReturn.supportExtraLocation != null && !fareReturn.supportExtraLocation){
                    fareReturn = new FareReturn();
                    etaResponse.returnCode = 407;
                }
                else if (fareReturn.validPriceAdjustable != null && !fareReturn.validPriceAdjustable){
                    fareReturn = new FareReturn();
                    etaResponse.returnCode = 408;
                }
                else
                    etaResponse.returnCode = 200;
            } else {
                /*String bookFrom = etaFareEnt.bookFrom != null ? etaFareEnt.bookFrom : "";
                if (bookFrom.equalsIgnoreCase("CC")) {
                    etaResponse.returnCode = 200;
                } else {
                    etaResponse.returnCode = 306;
                }*/
                etaResponse.returnCode = 306;
                fareReturn = new FareReturn();
            }
            etaResponse.response = fareReturn;
            logger.debug(etaFareEnt.requestId + " - " + "estimateFareResponse : " + etaResponse.toString());
            return etaResponse.toString();
        } catch (Exception ex) {
            logger.debug(etaFareEnt.requestId + " - " + "estimateFareResponse : " + CommonUtils.getError(ex));
        }
        return null;
    }

    @Override
    public String estimateFareWithMultiCarType(ETAFareEnt etaFareEnt) {
        ObjResponse etaResponse = new ObjResponse();
        try {
            etaFareEnt.setRequestId(etaFareEnt.requestId);
            if (etaFareEnt.rateDetails == null || etaFareEnt.rateDetails.isEmpty() || etaFareEnt.isEmptyString(etaFareEnt.fleetId)
                    || etaFareEnt.isEmptyString(etaFareEnt.bookFrom)) {

                etaResponse.returnCode = 406;
                return etaResponse.toString();
            }
            ObjResponse response = estimateCtr.estimateFareWithMultiCarType(etaFareEnt);
            return response.toString();
        } catch (Exception ex) {
            etaResponse.returnCode = 406;
        }
        return etaResponse.toString();
    }

    @Override
    public String estimateFareWithAllSupliers(ETAFareEnt etaFareEnt) {
        ObjResponse etaResponse = new ObjResponse();
        try {
            etaFareEnt.setRequestId(etaFareEnt.requestId);
            logger.debug(etaFareEnt.requestId + " - " + "estimateFareWithAllSupliers : " + gson.toJson(etaFareEnt));

            int returnCode = etaFareEnt.validateData();
            if (returnCode != 200) {
                etaResponse.returnCode = returnCode;
                logger.debug(etaFareEnt.requestId + " - " + "estimateFareWithAllSupliersResponse : " + etaResponse.toString());
                return etaResponse.toString();
            }
            ObjResponse response = estimateCtr.estimateFareWithAllSupliers(etaFareEnt);
            return response.toString();
        } catch (IOException e) {
            etaResponse.returnCode = 406;
            logger.debug(etaFareEnt.requestId + " - " + "estimateFareWithAllSupliersResponse : " + CommonUtils.getError(e));

        }
        logger.debug(etaFareEnt.requestId + " - " + "estimateFareWithAllSupliersResponse : " + etaResponse.toString());
        return etaResponse.toString();
    }

    @Override
    public String estimateFareCarHailing(ETAFareEnt etaFareEnt) {
        ObjResponse etaResponse = new ObjResponse();
        try {
            etaFareEnt.setRequestId(etaFareEnt.requestId);
            logger.debug(etaFareEnt.requestId + " - " + "estimateFareCarHailing : " + gson.toJson(etaFareEnt));

            int returnCode = etaFareEnt.validateData();
            if (returnCode != 200) {
                etaResponse.returnCode = returnCode;
                logger.debug(etaFareEnt.requestId + " - " + "estimateFareCarHailingResponse : " + etaResponse.toString());
                return etaResponse.toString();
            }
            ObjResponse response = estimateCtr.estimateFareCarHailing(etaFareEnt);
            return response.toString();
        } catch (IOException e) {
            etaResponse.returnCode = 406;
            logger.debug(etaFareEnt.requestId + " - " + "estimateFareCarHailingResponse : " + CommonUtils.getError(e));

        }
        logger.debug(etaFareEnt.requestId + " - " + "estimateFareCarHailingResponse : " + etaResponse.toString());
        return etaResponse.toString();
    }

    @Override
    public String estimateDeliveryFare(ETADeliveryEnt etaFareEnt) {
        try {
            etaFareEnt.setRequestId(etaFareEnt.requestId);
            logger.debug(etaFareEnt.requestId + " - " + "estimateDeliveryFare : " + gson.toJson(etaFareEnt));
            ObjResponse etaResponse = new ObjResponse();
            int returnCode = etaFareEnt.validateData();
            if (returnCode != 200) {
                etaResponse.returnCode = returnCode;
                logger.debug(etaFareEnt.requestId + " - " + "estimateDeliveryFareResponse : " + etaResponse.toString());
                return etaResponse.toString();
            }
            DeliveryResponse response = estimateCtr.estimateDeliveryFare(etaFareEnt);
            if (response != null) {
                etaResponse.returnCode = 200;
                etaResponse.response = response;
            } else {
                etaResponse.returnCode = 306;
                etaResponse.response = new DeliveryResponse();
            }
            logger.debug(etaFareEnt.requestId + " - " + "estimateDeliveryFareResponse : " + etaResponse.toString());
            return etaResponse.toString();
        } catch (Exception ex) {
            logger.debug(etaFareEnt.requestId + " - " + "estimateDeliveryFareResponse : " + CommonUtils.getError(ex));
        }
        return null;
    }

    @Override
    public String estimateDeliveryFareWithMultiCarType(ETADeliveryEnt etaFareEnt) {
        try {
            etaFareEnt.setRequestId(etaFareEnt.requestId);
            logger.debug(etaFareEnt.requestId + " - " + "estimateDeliveryFareWithMultiCarType : " + gson.toJson(etaFareEnt));
            ObjResponse etaResponse = new ObjResponse();

            etaResponse = estimateCtr.estimateDeliveryFareWithMultiCarType(etaFareEnt);
            logger.debug(etaFareEnt.requestId + " - " + "estimateDeliveryFareWithMultiCarTypeResponse : " + etaResponse.toString());
            return etaResponse.toString();
        } catch (Exception ex) {
            logger.debug(etaFareEnt.requestId + " - " + "estimateDeliveryFareWithMultiCarTypeResponse : " + CommonUtils.getError(ex));
        }
        return null;
    }

    @Override
    public String estimateFareMeter(ETAFareMeterEnt etaFareMeterEnt) {
        ObjResponse etaResponse = new ObjResponse();
        try {
            etaFareMeterEnt.setRequestId(etaFareMeterEnt.requestId);
            logger.debug(etaFareMeterEnt.requestId + " - " + "estimateFareMeter : " + gson.toJson(etaFareMeterEnt));

            int returnCode = etaFareMeterEnt.validateData();
            if (returnCode != 200) {
                etaResponse.returnCode = returnCode;
                logger.debug(etaFareMeterEnt.requestId + " - " + "estimateFareMeterResponse : " + etaResponse.toString());
                return etaResponse.toString();
            }
            ObjResponse response = estimateCtr.estimateFareMeter(etaFareMeterEnt);
            return response.toString();
        } catch (IOException e) {
            etaResponse.returnCode = 406;
            logger.debug(etaFareMeterEnt.requestId + " - " + "estimateFareMeterResponse : " + CommonUtils.getError(e));

        }
        logger.debug(etaFareMeterEnt.requestId + " - " + "estimateFareMeterResponse : " + etaResponse.toString());
        return etaResponse.toString();
    }

    @Override
    public String estimateFareSharing(ETASharingEnt sharingEnt) {
        ObjResponse etaResponse = new ObjResponse();
        try {
            sharingEnt.setRequestId(sharingEnt.requestId);
            logger.debug(sharingEnt.requestId + " - " + "estimateFareSharing : " + gson.toJson(sharingEnt));

            int returnCode = sharingEnt.validateData();
            if (returnCode != 200) {
                etaResponse.returnCode = returnCode;
                logger.debug(sharingEnt.requestId + " - " + "estimateFareSharingResponse : " + etaResponse.toString());
                return etaResponse.toString();
            }
            ObjResponse response = estimateCtr.estimateFareSharing(sharingEnt);
            return response.toString();
        } catch (IOException e) {
            etaResponse.returnCode = 406;
            logger.debug(sharingEnt.requestId + " - " + "estimateFareSharingResponse : " + CommonUtils.getError(e));

        }
        logger.debug(sharingEnt.requestId + " - " + "estimateFareSharingResponse : " + etaResponse.toString());
        return etaResponse.toString();
    }

    @Override
    public String addPromoCodeUse(PromoEnt promoEnt) {
        ObjResponse objResponse = new ObjResponse();
        try {
            promoEnt.setRequestId(promoEnt.requestId);
            logger.debug(promoEnt.requestId + " - " + "addPromoCodeUse : " + gson.toJson(promoEnt));
            int returnCode = 0;
            returnCode = promoEnt.validateData();
            if (returnCode != 200) {
                objResponse.returnCode = returnCode;
                logger.debug(promoEnt.requestId + " - " + "addPromoCodeUseResponse : " + objResponse.toString());
                return objResponse.toString();
            }

            String responseStr = promoCtr.promoCodeAction(promoEnt, KeysUtil.action_add);
            logger.debug(promoEnt.requestId + " - " + "addPromoCodeUseResponse : " + responseStr);
            return responseStr;
        } catch (JsonProcessingException e) {
            objResponse.returnCode = 406;
            e.printStackTrace();
            logger.debug(promoEnt.requestId + " - " + "addPromoCodeUseResponse : " + CommonUtils.getError(e));
        }
        return objResponse.toString();
    }

    @Override
    public String checkPromoCode(PromoEnt promoEnt) {
        ObjResponse objResponse = new ObjResponse();
        try {
            promoEnt.setRequestId(promoEnt.requestId);
            logger.debug(promoEnt.requestId + " - " + "checkPromoCode : " + gson.toJson(promoEnt));
            int returnCode = promoEnt.validateData();
            if (returnCode != 200) {
                objResponse.returnCode = returnCode;
                logger.debug(promoEnt.requestId + " - " + "checkPromoCodeResponse : " + objResponse.toString());
                return objResponse.toString();
            }
            String responseStr = promoCtr.promoCodeAction(promoEnt, KeysUtil.action_check);
            logger.debug(promoEnt.requestId + " - " + "checkPromoCodeResponse : " + responseStr);
            return responseStr;
        } catch (JsonProcessingException e) {
            objResponse.returnCode = 406;
            logger.debug(promoEnt.requestId + " - " + "checkPromoCodeResponse : " + CommonUtils.getError(e));
        }
        return objResponse.toString();
    }

    @Override
    public String addPromoToList(PromoEnt promoEnt) {
        ObjResponse objResponse = new ObjResponse();
        try {
            promoEnt.setRequestId(promoEnt.requestId);
            logger.debug(promoEnt.requestId + " - " + "addPromoToList : " + gson.toJson(promoEnt));
            int returnCode = promoEnt.validateData();
            if (returnCode != 200) {
                objResponse.returnCode = returnCode;
                logger.debug(promoEnt.requestId + " - " + "addPromoToListResponse : " + objResponse.toString());
                return objResponse.toString();
            }
            String responseStr = promoCtr.promoCodeAction(promoEnt, KeysUtil.action_add_to_list);
            logger.debug(promoEnt.requestId + " - " + "addPromoToListResponse : " + responseStr);
            return responseStr;
        } catch (JsonProcessingException e) {
            objResponse.returnCode = 406;
            logger.debug(promoEnt.requestId + " - " + "addPromoToListResponse : " + CommonUtils.getError(e));
        }
        return objResponse.toString();

    }

    @Override
    public String applyReferralAndAddPromo(PromoEnt promoEnt) {
        return promoCtr.applyReferralAndAddPromo(promoEnt);
    }

    @Override
    public String deletePromoCodeUse(PromoEnt promoEnt) {
        ObjResponse objResponse = new ObjResponse();
        try {
            promoEnt.setRequestId(promoEnt.requestId);
            logger.debug(promoEnt.requestId + " - " + "deletePromoCodeUse : " + gson.toJson(promoEnt));

            int returnCode = promoEnt.validateData();
            if (returnCode != 200 || promoEnt.userId == null || promoEnt.userId.isEmpty()) {
                objResponse.returnCode = returnCode;
                logger.debug(promoEnt.requestId + " - " + "deletePromoCodeUseResponse : " + objResponse.toString());
                return objResponse.toString();
            }
            String responseStr = promoCtr.promoCodeAction(promoEnt, KeysUtil.action_delete);
            logger.debug(promoEnt.requestId + " - " + "deletePromoCodeUseResponse : " + responseStr);
            return responseStr;
        } catch (JsonProcessingException e) {
            objResponse.returnCode = 406;
            logger.debug(promoEnt.requestId + " - " + "deletePromoCodeUseResponse : " + CommonUtils.getError(e));
        }
        return objResponse.toString();
    }

    @Override
    public String getRushHour(String requestId, String bookId) {
        ObjResponse objResponse = new ObjResponse();
        try {
            if (requestId == null || requestId.isEmpty())
                requestId = UUID.randomUUID().toString();
            logger.debug(requestId + " - " + "getRushHour : " + bookId);
            String responseStr = paymentServiceCtr.getRushHour(requestId, bookId);
            ;
            logger.debug(requestId + " - " + "getRushHourResponse : " + responseStr);
            return responseStr;
        } catch (IOException e) {
            e.printStackTrace();
            objResponse.returnCode = 406;
            e.printStackTrace();
        }
        return objResponse.toString();

    }

    @Override
    public String getCancelAmount(CancelBookingEnt cancelBookingEnt) {
        ObjResponse objResponse = new ObjResponse();
        try {
            cancelBookingEnt.setRequestId(cancelBookingEnt.requestId);
            logger.debug(cancelBookingEnt.requestId + " - " + "getCancelAmount : " + gson.toJson(cancelBookingEnt));
            ObjResponse cancelResponse = new ObjResponse();

            int returnCode = cancelBookingEnt.validateData();
            if (returnCode != 200) {
                cancelResponse.returnCode = returnCode;
                logger.debug(cancelBookingEnt.requestId + " - " + "getCancelAmountResponse : " + cancelResponse.toString());
                return cancelResponse.toString();
            }
            String responseStr = paymentServiceCtr.getCancelAmount(cancelBookingEnt);

            logger.debug(cancelBookingEnt.requestId + " - " + "getCancelAmountResponse : " + responseStr);
            return responseStr;
        } catch (JsonProcessingException e) {
            objResponse.returnCode = 406;
            logger.debug(cancelBookingEnt.requestId + " - " + "getCancelAmountResponse : " + CommonUtils.getError(e));
        }
        return objResponse.toString();

    }

    @Override
    public String getGeneralSettingAffiliate(ETAFareEnt etaFareEnt) {
        ObjResponse objResponse = new ObjResponse();
        try {
            etaFareEnt.setRequestId(etaFareEnt.requestId);
            logger.debug(etaFareEnt.requestId + " - " + "getGeneralSettingAffiliate : " + gson.toJson(etaFareEnt));
            String responseStr = paymentServiceCtr.getGeneralSettingAffiliate(etaFareEnt);
            logger.debug(etaFareEnt.requestId + " - " + "getGeneralSettingAffiliateResponse : " + responseStr);
            return responseStr;
        } catch (JsonProcessingException e) {
            objResponse.returnCode = 406;
            logger.debug(etaFareEnt.requestId + " - " + "getGeneralSettingAffiliateResponse : " + CommonUtils.getError(e));
        } catch (IOException e) {
            e.printStackTrace();
            objResponse.returnCode = 406;
            logger.debug(etaFareEnt.requestId + " - " + "getGeneralSettingAffiliateResponse : " + CommonUtils.getError(e));
        }
        return objResponse.toString();
    }

    @Override
    public String checkFlatRoutes(String s, CheckFlatRoutesQEnt checkFlatRoutesQEnt) {
        ObjResponse objResponse = new ObjResponse();
        try {
            if (s == null || s.isEmpty())
                s = UUID.randomUUID().toString();
            logger.debug(s + " - " + "checkFlatRoutes : " + gson.toJson(checkFlatRoutesQEnt));
            String responseStr = paymentServiceCtr.checkFlatRoutes(checkFlatRoutesQEnt.list, s);
            logger.debug(s + " - " + "checkFlatRoutes : " + responseStr);
            return responseStr;
        } catch (IOException e) {
            objResponse.returnCode = 406;
            logger.debug(s + " - " + "checkFlatRoutes : " + CommonUtils.getError(e));
        }
        return objResponse.toString();
    }

    @Override
    public String getPaymentData(UpdateEnt updateEnt) {
        updateEnt.setRequestId(updateEnt.requestId);
        logger.debug(updateEnt.requestId + " - " + "getPaymentData : " + updateEnt.bookId);
        ObjResponse objResponse = new ObjResponse();
        if (updateEnt.bookId == null || updateEnt.bookId.isEmpty()) {
            objResponse.returnCode = 406;
            logger.debug(updateEnt.requestId + " - " + "getPaymentDataResponse : " + objResponse.toString());
            return objResponse.toString();
        }
        String responseStr = paymentServiceCtr.getPaymentData(updateEnt.bookId, updateEnt.requestId);
        logger.debug(updateEnt.requestId + " - " + "getPaymentDataResponse : " + responseStr);
        return responseStr;

    }

    @Override
    public String checkFareAvailable(ETAFareEnt etaFareEnt) {
        ObjResponse objResponse = new ObjResponse();
        try {
            etaFareEnt.setRequestId(etaFareEnt.requestId);
            logger.debug(etaFareEnt.requestId + " - " + "checkFareAvailable : " + etaFareEnt.toString());
            String responseStr = paymentServiceCtr.checkFareAvailable(etaFareEnt.requestId, etaFareEnt);
            logger.debug(etaFareEnt.requestId + " - " + "checkFareAvailable response : " + responseStr);
            return responseStr;
        } catch (IOException e) {
            objResponse.returnCode = 406;
            logger.debug("checkFareAvailable response : " + CommonUtils.getError(e));
        }
        return objResponse.toString();

    }

    @Override
    public String saveSettlementHistory(SettlementHistoryEnt settlementHistoryEnt) {
        settlementHistoryEnt.setRequestId(settlementHistoryEnt.requestId);
        logger.debug(settlementHistoryEnt.requestId + " - " + "saveSettlementHistory : " + gson.toJson(settlementHistoryEnt));
        String responseStr = paymentServiceCtr.saveSettlementHistory(settlementHistoryEnt);
        logger.debug(settlementHistoryEnt.requestId + " - " + "saveSettlementHistoryResponse : " + responseStr);
        return responseStr;

    }

    @Override
    public String reportSettlementHistory(ReportSettlementEnt reportSettlementEnt) {
        reportSettlementEnt.setRequestId(reportSettlementEnt.requestId);
        logger.debug(reportSettlementEnt.requestId + " - " + "reportSettlementHistory : " + gson.toJson(reportSettlementEnt));
        String responseStr = null;
        try {
            responseStr = paymentServiceCtr.reportSettlementHistory(reportSettlementEnt);
        } catch (ParseException e) {
            logger.debug(reportSettlementEnt.requestId + " - " + "reportSettlementHistoryResponse : " + CommonUtils.getError(e));
        }
        logger.debug(reportSettlementEnt.requestId + " - " + "reportSettlementHistoryResponse : " + responseStr);
        return responseStr;

    }

    @Override
    public String updatePaidToDriver(ReportSettlementEnt reportSettlementEnt) {
        reportSettlementEnt.setRequestId(reportSettlementEnt.requestId);
        logger.debug(reportSettlementEnt.requestId + " - " + "reportSettlementHistory : " + gson.toJson(reportSettlementEnt));
        String responseStr = null;
        try {
            responseStr = paymentServiceCtr.updatePaidToDriver(reportSettlementEnt);
        } catch (ParseException e) {
            logger.debug(reportSettlementEnt.requestId + " - " + "reportSettlementHistoryResponse : " + CommonUtils.getError(e));
        }
        logger.debug(reportSettlementEnt.requestId + " - " + "reportSettlementHistoryResponse : " + responseStr);
        return responseStr;
    }

    @Override
    public String updateAffiliateRate(String requestId, int value, String priceType) {
        logger.debug("updateAffiliateRate - value : " + value);
        String responseStr = null;
        try {
            responseStr = paymentServiceCtr.updateAffiliateRate(value, priceType);
        } catch (ParseException e) {
            logger.debug("updateAffiliateRateResponse : " + CommonUtils.getError(e));
        }
        logger.debug("updateAffiliateRateResponse : " + responseStr);
        return responseStr;

    }

    @Override
    public String estimateFareForThirdParty(ETAThirdPartyEnt etaFareEnt) {
        return estimateCtr.estimateFareForThirdParty(etaFareEnt);
    }

}
