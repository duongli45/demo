package pg.util;
import com.google.gson.Gson;

/**
 */
public class ObjResponse {
    public int returnCode;
    public Object response;
    public Object message;
    public String error;

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

}
