package pg.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by hoang.nguyen on 11/25/16.
 */
public class ConvertUtil {
    public static double round(double value, int places) {
        if (places < 0) {
            return 0;
        }
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static int getMeters(int number, String unit) {
        if (unit.equals("mi")) {
            Double meters = number * 1609.344;
            return meters.intValue();
        }
        return number * 1000;
    }

    public static Date getCustomizeDate(Date date, int seconds) throws ParseException {
        //create Calendar instance
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.SECOND, seconds);
        return calendar.getTime();
    }

    public static Date customizeDateByCurrentYear(Date date, int year) throws ParseException {
        //create Calendar instance
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.YEAR, year);
        return calendar.getTime();
    }

    public static Date customizeDateByHour(Date date, String time) throws ParseException {
        //create Calendar instance
        int timeHour = 0;
        int timeMinute = 0;
        String[] timeHFrom = time.split(":");
        if (timeHFrom.length > 1) {
            timeHour = Integer.valueOf(timeHFrom[0]);
            timeMinute = Integer.valueOf(timeHFrom[1]);
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, timeHour);
        calendar.set(Calendar.MINUTE, timeMinute);
        return calendar.getTime();
    }

    public static Integer getDurationTime(String fromDate, String toDate) throws ParseException {
        SimpleDateFormat sdfMongo = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
        Date date1 = sdfMongo.parse(fromDate);
        Date date2 = sdfMongo.parse(toDate);
        long duration  = date2.getTime() - date1.getTime();

        long diffInSeconds = TimeUnit.MILLISECONDS.toSeconds(duration);

        return (int)diffInSeconds;
    }

    public static int getDaysBetween(Date d1, Date d2, String timeZone){
        //TimeZone tz = TimeZone.getTimeZone(timeZone);
        //convert date from GMT to tz
        Date date1 = TimezoneUtil.offsetTimeZone(d1,"GMT", timeZone);
        Date date2 = TimezoneUtil.offsetTimeZone(d2,"GMT", timeZone);
        Calendar calendarD1 = Calendar.getInstance();
        Calendar calendarD2 = Calendar.getInstance();
        calendarD1.setTime(date1);
        calendarD2.setTime(date2);
        return calendarD2.get(Calendar.DAY_OF_YEAR) - calendarD1.get(Calendar.DAY_OF_YEAR);
    }

    public static Integer getMinutesTime(Date date) throws ParseException {
        //create Calendar instance
        /*DateFormat formatToDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = formatToDate.parse(time);*/
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int hourOfDay  = calendar.get(Calendar.HOUR_OF_DAY); // 24 hour clock
        int minute     = calendar.get(Calendar.MINUTE);
        int minutes = (hourOfDay*60)+minute;

        return minutes;
    }

    public static String getDay(Date date) throws ParseException {
        //create Calendar instance
       /* DateFormat formatToDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = formatToDate.parse(time);*/
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        //create an array of days
        String[] strDays = new String[]{
                "Sun",
                "Mon",
                "Tue",
                "Wed",
                "Thu",
                "Fri",
                "Sat"
        };
        return strDays[calendar.get(Calendar.DAY_OF_WEEK) - 1];
    }

    public static String getNextDay(String day) throws ParseException {
        String nextDay ="";
        if(day.equals("Mon"))
            nextDay = "Tue";
        if(day.equals("Tue"))
            nextDay = "Wed";
        if(day.equals("Wed"))
            nextDay = "Thu";
        if(day.equals("Thu"))
            nextDay = "Fri";
        if(day.equals("Fri"))
            nextDay = "Sat";
        if(day.equals("Sat"))
            nextDay = "Sun";
        if(day.equals("Sun"))
            nextDay = "Mon";
        return nextDay;
    }

    public static  String getUTCISOTime(String timeZoneId) {
        TimeZone tz = TimeZone.getTimeZone(timeZoneId);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        df.setTimeZone(tz);
        return df.format(new Date());
    }

    public static boolean compareVersion(String v1, String v2){
       try{
           String[] currentTemp = v1.split("\\.");
           String[] minRevTemp = v2.split("\\.");
           int[] currentRev = new int[currentTemp.length];
           int[] minRev = new int[minRevTemp.length];
           for (int i = 0; i < currentTemp.length; i++) {
               currentRev[i] = Integer.parseInt(currentTemp[i]);
               minRev[i] = Integer.parseInt(minRevTemp[i]);
           }
           if (currentRev[0] > minRev[0]) {
               return true;
           } else if (currentRev[0] == minRev[0]) {
               if (currentRev[1] > minRev[1]) {
                   return true;
               } else if (currentRev[1] == minRev[1]) {
                   if (currentRev[2] >= minRev[2]) {
                       return true;
                   } else {
                       return false;
                   }
               } else {
                   return false;
               }
           } else {
               return false;
           }
       }catch (Exception ex){
           return false;
       }
    }

    public static double extendedRound(double number, String mode, int zeroes) {
        RoundingMode roundingMode = RoundingMode.CEILING;
        if(mode.equals("round")){
            roundingMode = RoundingMode.HALF_UP;
        }else if(mode.equals("roundUp")){
            roundingMode = RoundingMode.UP;
        }else if(mode.equals("roundDown")){
            roundingMode = RoundingMode.DOWN;
        }
        BigDecimal bd = BigDecimal.valueOf(number).setScale(zeroes, roundingMode);
        return bd.doubleValue();
    }

    public static void main(String[] args) {
        double d = 60.71;
        BigDecimal bd = BigDecimal.valueOf(d).setScale(2, RoundingMode.UP);
        d = bd.doubleValue();
        System.out.println("d:   " + d);
        System.out.println("test poll SCM job");
    }

}
