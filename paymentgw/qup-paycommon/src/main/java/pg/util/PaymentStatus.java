package pg.util;

import java.util.Arrays;
import java.util.List;

/**
 */
public class PaymentStatus {

    public static final String INCIDENT = "incident";
    public static final String NOSHOW = "noShow";
    public static final String CANCELED = "canceled";

    public static final String CANCELER_DRIVER = "driver";
    public static final String CANCELER_PASSENGER = "passenger";
    public static final String CANCELER_MDISPATCHER = "mdispatcher";
    public static final String CANCELER_CORPORATE = "CorpAD";

    public static final List<String> INCOMPLETE_PAYMENT = Arrays.asList(INCIDENT, NOSHOW, CANCELED);
}
