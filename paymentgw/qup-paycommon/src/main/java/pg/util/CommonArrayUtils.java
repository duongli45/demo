package pg.util;

import java.util.Arrays;

/**
 */
public class CommonArrayUtils {

    public static void main(String[] args) throws Exception {
        boolean contain = Arrays.stream(CASH_PAYMENT).anyMatch("cash"::equals);
        System.out.println("contain: " + contain);
    }

    public static final String[] CASH_PAYMENT = {
            KeysUtil.PAYMENT_CASH,
            KeysUtil.PAYMENT_CASH_SENDER,
            KeysUtil.PAYMENT_CASH_RECIPIENT
    };

    public static final String[] CREDIT_PAYMENT = {
            KeysUtil.PAYMENT_CARD,
            KeysUtil.PAYMENT_CORPORATE,
            KeysUtil.PAYMENT_MDISPATCHER
    };

    public static final String[] INCOMPLETE_PAYMENT = {
            KeysUtil.PAYMENT_INCIDENT,
            KeysUtil.PAYMENT_NOSHOW,
            KeysUtil.PAYMENT_CANCELED
        };

    public static final String[] BOOK_NOT_PAX = {
            "CC",
            "API",
            "Car-hailing",
            "Web booking",
            "Partner",
            "mDispatcher",
            "Kiosk",
            "webBooking",
            "partner",
            ""
        };

    public static final String[] TRANSACTION_STATUS = {

        };

    public static final String[] PAX_WALLET_TYPE = {
            "credit" ,
            "ride",
            "tipAfterCompleted",
            "debtWriteOff",
            "editBalance",
            "refund",
            "cashExcess",
            "driverTopUpForPax",
            "topUpMOLPay",
            "topUpReferrer",
            "topUpReferee",
            "itemValue",
            "redeemVoucher",
            "topUpMoMo",
            "topUpAUB",
            "topUpYenePay",
            "topUpWallet"
        };

    public static final String[] DRIVER_WALLET_TYPE = {
            "topUp" ,
            "bookingDeduction",
            "editBalance",
            "topUpGCash",
            "cashWallet",
            "topUpMOLPay",
            "cashExcess",
            "driverTopUpForPax",
            "fawry",
            "bookingPromo",
            "referralEarning",
            "topUpAUB",
            "topUpYenePay",
            "topUpWallet",
            "insurance",
            "sentToDriver",
            "receivedFromDriver",
            "sentToCustomer",
            "cancellationPenalty",
            "customerDebt",
            "merchantCommission"
        };

    public static final String[] DRIVER_CASH_WALLET_TYPE = {
            "withdraw" ,
            "tipAfterCompleted",
            "bookingEarning",
            "referralEarning",
            "itemValue",
            "updatedBalance",
            "payout",
            "payToDriver",
            "transferToCreditWallet",
            "promo",
            "partialEarning"
    };

    public static final String[] MERCHANT_WALLET_TYPE = {
            "editBalance",
            "orderDeduction"
    };

    public static final String[] MERCHANT_CASH_WALLET_TYPE = {
            "editBalance",
            "earning",
            "itemValue",
            "payout"
    };

    public static final String[] TOPUP_GATEWAY_NAME = {
            KeysUtil.MOLPAY
        };

    public static final String[] WALLET_NAME = {
            KeysUtil.TnGWallet,
            KeysUtil.GCASH
        };

    public static final String[] GATEWAY_3DS = {
            KeysUtil.PAYFORT,
            KeysUtil.PAYCORP,
            KeysUtil.YEEPAY,
            KeysUtil.FLUTTERWAVE,
            KeysUtil.FIRSTDATA,
            KeysUtil.EGHL
        };

    public static final String[] GATEWAY_PREAUTHORIZED = {
            KeysUtil.STRIPE,
            KeysUtil.FIRSTDATA,
            KeysUtil.BRAINTREE,
            KeysUtil.AUTHORIZE,
            KeysUtil.CREDITCORP,
            KeysUtil.FAC,
            KeysUtil.JETPAY,
            KeysUtil.AVIS, //PingPong
            KeysUtil.CONEKTA,
            KeysUtil.PEACH
        };

    public static final String[] GATEWAY_SETTLEMENT = {
            KeysUtil.STRIPE,
            KeysUtil.JETPAY,
            KeysUtil.MOLPAY
        };




}
