package pg.util;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.SecureRandom;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 */
@SuppressWarnings("unchecked")
public class CommonUtils {

    //final static Logger logger = Logger.getLogger(CommonUtils.class);
    private static final Logger logger = LogManager.getLogger(CommonUtils.class);

    public static String getError(Exception ex) {
        Gson gson = new Gson();
        StringWriter errors = new StringWriter();
        ex.printStackTrace(new PrintWriter(errors));
        return gson.toJson(errors.toString());
    }

    public static String getValue(String data, String key) {
        try {
            Gson gson = new Gson();
            ObjResponse jsonData = gson.fromJson(data, ObjResponse.class);

            Map<String, Object> mapResponse = (Map<String, Object>) jsonData.response;
            return mapResponse.get(key) != null ? mapResponse.get(key).toString() : "";
        } catch (Exception ignore) {}
        return "";
    }

    public static String getOrderId() {
        Calendar cal = Calendar.getInstance();
        Date currentTime = TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), "GMT");
        SimpleDateFormat formatOrder = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        return formatOrder.format(currentTime) + (int)(Math.random() * 1000);
    }

    public static String removeSpecialCharacter(String data){
        //remove emoji
        data = data.replaceAll(KeysUtil.characterFilter,"");
        //remove special character
        data = Normalizer.normalize(data, Normalizer.Form.NFD);
        data = data.replaceAll("[^\\p{ASCII}]", "");
        data = data.replaceAll("\\p{M}", "");
        return data;
    }

    public static String sendJupiter(String requestId, String body, String url, String userName, String pw){
        String result = "";
        try {
            HttpResponse<String> httpResponse = Unirest.post(url)
                    .basicAuth(userName, pw)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("x-request-id", requestId)
                    .body(body)
                    .asString();
            result = httpResponse.getBody();
        } catch (Exception ex) {
            logger.debug(requestId + " - sendJupiter exception: " + CommonUtils.getError(ex));
        }
        return result;
    }

    public static int sendPointSystem(String requestId, String body, String url, String userName, String pw){
        int returnCode = 0;
        try {
            HttpResponse<String> httpResponse = Unirest.post(url)
                    .basicAuth(userName, pw)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("x-request-id", requestId)
                    .body(body)
                    .asString();
            String result = httpResponse.getBody();
            logger.debug(requestId + " - result: " + result);
            returnCode = httpResponse.getStatus();
        } catch (Exception ex) {
            logger.debug(requestId + " - sendPointSystem exception: " + CommonUtils.getError(ex));
        }
        return returnCode;
    }

    public static String sendReport(String requestId, String body, String url, String userName, String pw){
        String result = "";
        try {
            HttpResponse<String> httpResponse = Unirest.post(url)
                    .basicAuth(userName, pw)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("X-Trace-Request-Id", requestId)
                    .body(body)
                    .asString();
            result = httpResponse.getBody();
        } catch (Exception ex) {
            logger.debug(requestId + " - sendReport exception: " + CommonUtils.getError(ex));
        }
        return result;
    }

    public static String sendPostRequest(String requestId, String body, String url, String userName, String pw){
        String result = "";
        try {
            HttpResponse<String> httpResponse = Unirest.post(url)
                    .basicAuth(userName, pw)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("x-request-id", requestId)
                    .body(body)
                    .asString();
            result = httpResponse.getBody();
        } catch (Exception ex) {
            logger.debug(requestId + " - sendPostRequest exception: " + CommonUtils.getError(ex));
        }
        return result;
    }

    public static int getReturnCode(String string) {
        try {
            if (!string.isEmpty()) {
                JSONParser parser = new JSONParser();
                JSONObject obj = (JSONObject) parser.parse(string);
                return Integer.parseInt(obj.get("returnCode").toString());
            }
        } catch(Exception ex) {
            ex.getMessage();
        }
        return 0;
    }

    public static boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }

    public static String getJobType(String jobType, String bookFrom, boolean intercity, boolean delivery) {
        if (bookFrom.equalsIgnoreCase("car-hailing")) {
            jobType = "rideHailing";
        } else if (intercity) {
            jobType = "intercity";
        } else if (delivery) {
            if (!jobType.equals("food") && !jobType.equals("mart")) {
                jobType = "parcel";
            }
        } else {
            if (!jobType.equals("streetSharing") && !jobType.equals("shuttle")) {
                jobType = "transport";
            }
        }
        return jobType;
    }

    public static double getRoundValue(double origin) {
        BigDecimal bigDecimal = new BigDecimal(origin);
        return bigDecimal.setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public static double getRoundFormat(double origin) {
        String format3 = String.format("%.3f", origin);
        double step3 =  Double.parseDouble(format3);
        String format = String.format("%.2f", step3);
        return Double.parseDouble(format);
    }

    public static double getRoundUp(double origin) {
        BigDecimal bigDecimal = new BigDecimal(origin);
        return bigDecimal.setScale(2, RoundingMode.UP).doubleValue();
    }

    public static double getRoundDown(double origin) {
        BigDecimal bigDecimal = new BigDecimal(origin);
        return bigDecimal.setScale(2, RoundingMode.DOWN).doubleValue();
    }

    public static String randomString(int len){
        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        SecureRandom rnd = new SecureRandom();
        StringBuilder sb = new StringBuilder(len);
        for(int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }
}
