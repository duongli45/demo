package pg.util;

import org.apache.log4j.Logger;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 */

public class TimezoneUtil {
    final static Logger logger = Logger.getLogger(TimezoneUtil.class);
    /**
     * Get list timezone base on TimeZone library
     * @return List timezone with formatted name and filter by locations
     */
    public static List< Map<String,String> > getTimezoneIdList() {
        String[] temp = TimeZone.getAvailableIDs();
        List<String> timezoneList = new ArrayList<String>();
        for (String tz : temp) {
            timezoneList.add(tz);
        }

        // sort by name
        /*Collections.sort(timezoneList);*/

        // filter by locations
        String filterList = "Canada|Mexico|Chile|Cuba|Brazil|Japan|Turkey|Mideast|Africa|America|Asia|Atlantic|Australia|Europe|Indian|Pacific";
        Pattern p = Pattern.compile("^(" + filterList + ").*");
        List< Map<String,String> > listMap = new ArrayList< Map<String,String> >();
        for (String tz : timezoneList) {
            Matcher m = p.matcher(tz);
            if (m.find()) {
                Map<String,String> map = new HashMap<String,String>();
                map.put("timezone", TimeZone.getTimeZone(tz).getID());
                // format name and remove "_" symbol
                map.put("display", displayTimeZone(TimeZone.getTimeZone(tz)).replace("_", " "));

                listMap.add(map);
            }
        }
        return listMap;
    }

    /**
     * Get list timezone base on TimeZone library
     * @return List timezone with formatted name and filter by locations
     */
    public static String getTimezoneId(String timeZone) {
        String[] temp = TimeZone.getAvailableIDs();
        List<String> timezoneList = new ArrayList<String>();
        for (String tz : temp) {
            timezoneList.add(tz);
        }
        String timeZoneId = "";
        // filter by locations
        String filterList = "Canada|Mexico|Chile|Cuba|Brazil|Japan|Turkey|Mideast|Africa|America|Asia|Atlantic|Australia|Europe|Indian|Pacific";
        Pattern p = Pattern.compile("^(" + filterList + ").*");
        for (String tz : timezoneList) {
            Matcher m = p.matcher(tz);
            if (m.find()) {
                String displayTimezone = displayTimeZone(TimeZone.getTimeZone(tz)).replace("_", " ");
                if (displayTimezone.contains(timeZone)){
                    timeZoneId = displayTimezone;
                }
            }
        }
        return timeZoneId;
    }

    /**
     * Format display name of timezone
     * @param tz Id string of timezone
     * @return Formatted string of timezone - E.g. (GMT+7:00) Asia/Saigon
     */
    public static String displayTimeZone(TimeZone tz) {

        long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
        long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset())
                - TimeUnit.HOURS.toMinutes(hours);
        // avoid -4:-30 issue
        minutes = Math.abs(minutes);

        String result = "";
        if (hours > 0) {
            result = String.format("(GMT+%d:%02d) %s", hours, minutes, tz.getID());
        } else if (hours < 0){
            result = String.format("(GMT%d:%02d) %s", hours, minutes, tz.getID());
        } else {
            result = String.format("(GMT %d:%02d) %s", hours, minutes, tz.getID());
        }

        return result;
    }

    /**
     * Test class
     * @param args args
     */
    public static void main(String[] args) {
        String time = "Pacific/Midway";
        String timeZone = getTimezoneId(time);
        System.out.println("timeZone is: " + timeZone);


        String date1 = formatISODate(new Date(), Calendar.getInstance().getTimeZone().getID());
        System.out.println("date: " + date1);

        String date2 = formatISODate(new Date(), "GMT");
        System.out.println("date: " + date2);
        System.out.println("start year: " + atStartOfYear(new Date()));
        System.out.println("end year: " + atEndOfYear(new Date()));
    }

    public static Date offsetTimeZone(Date date, String fromTZ, String toTZ){

        //construct FROM and TO Timezone instances
        TimeZone fromTimeZone = TimeZone.getTimeZone(fromTZ);
        TimeZone toTimeZone = TimeZone.getTimeZone(toTZ);

        //Get a Calendar instance using the default time zone and locale.
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(toTZ));

        // Set the calendar's time with the given date
        calendar.setTimeZone(fromTimeZone);
        calendar.setTime(date);

        //System.out.println("--------------------TimeUtile: "+calendar.getTime());

        // FROM timezone to UTC
        calendar.add(Calendar.MILLISECOND, fromTimeZone.getRawOffset()*(-1));
        if(fromTimeZone.inDaylightTime(calendar.getTime())){
            /*System.out.println("fromTimeZone in Daylight");*/
            calendar.add(Calendar.MILLISECOND, calendar.getTimeZone().getDSTSavings() *(-1));
            calendar.getTimeZone().getDisplayName();
        }
        //System.out.println("-----------------------FROM timezone to UTC: "+calendar.getTime() + " ---- " + fromTimeZone.getRawOffset()*(-1));

        // UTC to Timezone
        calendar.add(Calendar.MILLISECOND, toTimeZone.getRawOffset());
        if(toTimeZone.inDaylightTime(calendar.getTime())){
            /*System.out.println("toTimeZone in Daylight");*/
            calendar.add(Calendar.MILLISECOND, toTimeZone.getDSTSavings());
        }
        //System.out.println("-------------------------UTC to Timezone: "+calendar.getTime() + " ---- " + toTimeZone.getRawOffset());
        return calendar.getTime();
    }

    /***
     * get date send mail
     * @return date format GMT
     */
    public static String getDateSendMail(){
        //Lay gio GMT
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss");
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        String timezoneServer = cal.getTimeZone().getID();
        Date temp = offsetTimeZone(date, timezoneServer, "GMT");
        String mailDateStr = sdf.format(temp);
        String mailDate = mailDateStr.substring(0, mailDateStr.length()-8)+"at "+mailDateStr.substring(mailDateStr.length()-8, mailDateStr.length())+" (GMT)";
        return mailDate;
    }

    public static String getDateRangeOfWeek(int week) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.WEEK_OF_YEAR, week);
        cal.set(Calendar.YEAR, 2014);

        cal.set(Calendar.DAY_OF_WEEK, 1);
        String firstDate = sdf.format(cal.getTime());
        cal.set(Calendar.DAY_OF_WEEK, 7);
        String lastDate = sdf.format(cal.getTime());
        return firstDate + " - " + lastDate;

    }

    public static String changeWeekToDateRange(int week) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.WEEK_OF_YEAR, week);
        cal.set(Calendar.YEAR, 2014);

        cal.set(Calendar.DAY_OF_WEEK, 1);
        String firstDate = sdf.format(cal.getTime());
        cal.set(Calendar.DAY_OF_WEEK, 7);
        String lastDate = sdf.format(cal.getTime());
        return firstDate + "-" + lastDate;
    }

    public static String changeDateToDateRange(String date) {
        return date + "-" + date;

    }


    public static String formatISODate(Date date, String timezone) {
        SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss.SSS" );
        String time = df.format(date);

        SimpleDateFormat dfz = new SimpleDateFormat( "Z" );
        dfz.setTimeZone(TimeZone.getTimeZone(timezone));
        String zone = dfz.format(date);

        String combine = time + zone;
        return combine.replace("+0000", "Z");

    }

    public static Date convertServerToGMT(Date date) {
        Calendar cal = Calendar.getInstance();
        return TimezoneUtil.offsetTimeZone(date, cal.getTimeZone().getID(), "GMT");
    }

    public static String getPeriodTime(String requestId, String pickUpTime) throws Exception{
        SimpleDateFormat sdfMongo = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("HH-mm-ss dd-MM-yyyy");
        Calendar cal = Calendar.getInstance();
        Date pickupDate = TimezoneUtil.offsetTimeZone(sdfMongo.parse(pickUpTime), cal.getTimeZone().getID(), "GMT");
        Date currentDate = TimezoneUtil.convertServerToGMT(new Date());
        logger.debug(requestId + " - " + "from = " + sdf.format(currentDate) + " to = " + sdf.format(pickupDate));
        long diff = pickupDate.getTime() - currentDate.getTime();
        String result = "";
        if (diff > 0) {
            long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);
            logger.debug(requestId + " - " + "minutes = " + minutes);
            long diffDays = minutes / (24 * 60);
            logger.debug(requestId + " - " + "diffDays = " + diffDays);
            long remain = minutes - (24 * 60)*diffDays;
            //logger.debug(requestId + " - " + "remain = " + remain);
            long diffHours = remain/60;
            logger.debug(requestId + " - " + "diffHours = " + diffHours);
            long diffMinutes = remain - diffHours*60;
            logger.debug(requestId + " - " + "diffMinutes = " + diffMinutes);
            result = String.valueOf(diffDays);
            result += diffDays > 1 ? " days " : " day ";
            result += String.valueOf(diffHours);
            result += diffHours > 1 ? " hours " : " hour ";
            result += String.valueOf(diffMinutes);
            result += diffMinutes > 1 ? " minutes" : " minute";
        }
        return result;
    }

    public static Timestamp getGMTTimestamp(){
        Calendar cal = Calendar.getInstance();

        Date resultTime = TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), "GMT");
        Timestamp result = new Timestamp(resultTime.getTime());

        Date compareTime = TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), "GMT");
        Timestamp compare = new Timestamp(compareTime.getTime());

        return result.compareTo(compare) <= 0 ? result : compare;
    }

    public static Date atStartOfDay(Date date) {
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime startOfDay = localDateTime.with(LocalTime.MIN);
        return localDateTimeToDate(startOfDay);
    }

    public static Date atEndOfDay(Date date) {
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime endOfDay = localDateTime.with(LocalTime.MAX);
        return localDateTimeToDate(endOfDay);
    }

    public static Date atStartOfMonth(Date date){
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime start = localDateTime.withDayOfMonth(1).with(LocalTime.MIN);
        return localDateTimeToDate(start);
    }

    public static Date atEndOfMonth(Date date){
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime end = localDateTime.plusMonths(1).withDayOfMonth(1).minusDays(1).with(LocalTime.MAX);
        return localDateTimeToDate(end);
    }

    public static Date atStartOfYear(Date date){
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime start = localDateTime.withDayOfYear(1).with(LocalTime.MIN);
        return localDateTimeToDate(start);
    }

    public static Date atEndOfYear(Date date){
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime end = localDateTime.plusYears(1).withDayOfYear(1).minusDays(1).with(LocalTime.MAX);
        return localDateTimeToDate(end);
    }

    private static LocalDateTime dateToLocalDateTime(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    static Date localDateTimeToDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
}
