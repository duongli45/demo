GIT_HASH=$1
DOCKER_IMAGE=$2
WORK_SPACE=$3

git fetch --all
git checkout $GIT_HASH
git submodule update --recursive --init --remote --force

rm -rf $WORK_SPACE/ecs-scripts/docker/payment/qupservice.war
mvn clean install
if [ $? -ne 0 ]; then
        exit 1;
fi
mv $WORK_SPACE/qup-paymentgw/qupservice/target/qupservice.war $WORK_SPACE/ecs-scripts/docker/payment/qupservice.war
chmod +x $WORK_SPACE/ecs-scripts/build/build_payment.sh
$WORK_SPACE/ecs-scripts/build/build_payment.sh --git=$GIT_HASH --docker=$DOCKER_IMAGE --force -nc