package com.qupworld.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class PeachPaymentsConnectionUtil {

    private static final Logger _log = LogManager.getLogger(PeachPaymentsConnectionUtil.class);
    private DecimalFormat DF = new DecimalFormat("0.00");
    private String requestId;

    public PeachPaymentsConnectionUtil(String requestId) {
        this.requestId = requestId;
    }

    public Map<String,Object> createCreditToken(String serverUrl, Map<String,String> mapData, boolean enableCheckZipCode) throws Exception {
        serverUrl = serverUrl + "v1/payments";
        URL url = new URL(serverUrl);
        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);

        String query = ""
                + "authentication.userId=$userId"
                + "&authentication.password=$password"
                + "&authentication.entityId=$entityId"
                + "&merchantTransactionId=$merchantTransactionId"
                + "&amount=$amount"
                + "&currency=$currency"
                + "&paymentBrand=$cardType"
                + "&paymentType=DB"
                + "&card.number=$cardNumber"
                + "&card.holder=$cardHolder"
                + "&card.expiryMonth=$expiredMonth"
                + "&card.expiryYear=$expiredYear"
                + "&card.cvv=$cvv"
                + "&createRegistration=true";

        query = query.replace("$userId", mapData.get("userId"));
        query = query.replace("$password", mapData.get("password"));
        query = query.replace("$entityId", mapData.get("entityId"));
        query = query.replace("$merchantTransactionId", "Q" + RandomStringUtils.randomAlphanumeric(6).toUpperCase());
        query = query.replace("$currency", mapData.get("currency"));
        query = query.replace("$cardType", mapData.get("cardType"));
        query = query.replace("$cardHolder", mapData.get("cardHolder"));
        query = query.replace("$cardNumber", mapData.get("cardNumber"));
        query = query.replace("$expiredMonth", mapData.get("expiredMonth"));
        query = query.replace("$expiredYear", mapData.get("expiredYear"));
        query = query.replace("$cvv", mapData.get("cvv"));
        query = query.replace("$zipCode", mapData.get("zipCode"));
        query = query.replace("$amount", DF.format(1.00));

        String cardNumber = mapData.get("cardNumber") != null ? mapData.get("cardNumber") : "";
        cardNumber = cardNumber.length() > 4 ? "XXXXXXXXXXXXXXXX" + cardNumber.substring(cardNumber.length() - 4) : "XXXXXXXXXXXXXXXX";
        String log = query.replace(mapData.get("cardNumber"), cardNumber);
        _log.debug(requestId + " - query: " + log);

        DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
        wr.writeBytes(query);
        wr.flush();
        wr.close();

        return getResponse(conn, "createCreditToken");
    }

    public Map<String,Object> createPaymentWithCreditToken(String serverUrl, Map<String,String> mapData, String token, double amount) throws Exception {
        serverUrl = serverUrl + "v1/registrations/" + token + "/payments";
        URL url = new URL(serverUrl);
        String query = ""
                + "authentication.userId=$userId"
                + "&authentication.password=$password"
                + "&authentication.entityId=$entityId"
                + "&merchantTransactionId=$merchantTransactionId"
                + "&amount=$amount"
                + "&currency=$currency"
                + "&paymentType=DB";

        query = query.replace("$userId", mapData.get("userId"));
        query = query.replace("$password", mapData.get("password"));
        query = query.replace("$entityId", mapData.get("entityId"));
        query = query.replace("$merchantTransactionId", "Q" + mapData.get("bookId"));
        query = query.replace("$currency", mapData.get("currency"));
        query = query.replace("$amount", DF.format(amount));
        _log.debug(requestId + " - query: " + query);

        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-length", String.valueOf(query.length()));
        con.setDoOutput(true);
        con.setDoInput(true);

        DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.flush();
        output.close();

        return getResponse(con, "createPaymentWithCreditToken");
    }

    public Map<String,Object> preauthPayment(String serverUrl, Map<String,String> mapData, String token, double amount) throws Exception {
        serverUrl = serverUrl + "v1/registrations/" + token + "/payments";
        URL url = new URL(serverUrl);
        String query = ""
                + "authentication.userId=$userId"
                + "&authentication.password=$password"
                + "&authentication.entityId=$entityId"
                + "&merchantTransactionId=$merchantTransactionId"
                + "&amount=$amount"
                + "&currency=$currency"
                + "&paymentType=PA";

        query = query.replace("$userId", mapData.get("userId"));
        query = query.replace("$password", mapData.get("password"));
        query = query.replace("$entityId", mapData.get("entityId"));
        query = query.replace("$merchantTransactionId", "Q" + mapData.get("bookId"));
        query = query.replace("$currency", mapData.get("currency"));
        query = query.replace("$amount", DF.format(amount));
        _log.debug(requestId + " - query: " + query);

        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-length", String.valueOf(query.length()));
        con.setDoOutput(true);
        con.setDoInput(true);

        DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.flush();
        output.close();

        return getResponse(con, "preauthPayment");
    }

    public Map<String,Object> preauthCapture(String serverUrl, Map<String,String> mapData, String authId, double amount) throws Exception {
        serverUrl = serverUrl + "v1/payments/" + authId;
        URL url = new URL(serverUrl);
        String query = ""
                + "authentication.userId=$userId"
                + "&authentication.password=$password"
                + "&authentication.entityId=$entityId"
                + "&amount=$amount"
                + "&currency=$currency"
                + "&paymentType=CP";

        query = query.replace("$userId", mapData.get("userId"));
        query = query.replace("$password", mapData.get("password"));
        query = query.replace("$entityId", mapData.get("entityId"));
        query = query.replace("$currency", mapData.get("currency"));
        query = query.replace("$amount", DF.format(amount));
        _log.debug(requestId + " - query: " + query);

        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-length", String.valueOf(query.length()));
        con.setDoOutput(true);
        con.setDoInput(true);

        DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.flush();
        output.close();

        return getResponse(con, "preauthCapture");
    }

    public Map<String,Object> voidPeachPaymentsTransaction(String type, String serverUrl, Map<String,String> mapData, String transId, double amount) throws Exception {
        serverUrl = serverUrl + "v1/payments/" + transId;
        URL url = new URL(serverUrl);
        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);

        String query = ""
                + "authentication.userId=$userId"
                + "&authentication.password=$password"
                + "&authentication.entityId=$entityId"
                + "&amount=$amount"
                + "&currency=$currency"
                + "&paymentType=RF";

        query = query.replace("$userId", mapData.get("userId"));
        query = query.replace("$password", mapData.get("password"));
        query = query.replace("$entityId", mapData.get("entityId"));
        query = query.replace("$merchantTransactionId", "RF" + mapData.get("bookId"));
        query = query.replace("$currency", mapData.get("currency"));
        query = query.replace("$amount", DF.format(amount));

        DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
        wr.writeBytes(query);
        wr.flush();
        wr.close();

        return getResponse(conn, "voidPeachPaymentsTransaction");
    }

    public Map<String,Object> createPaymentWithCardInfo(String serverUrl, Map<String,String> mapData, boolean enableCheckZipCode, double amount) throws Exception {
        serverUrl = serverUrl + "v1/payments";
        URL url = new URL(serverUrl);
        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);

        String query = ""
                + "authentication.userId=$userId"
                + "&authentication.password=$password"
                + "&authentication.entityId=$entityId"
                + "&merchantTransactionId=$merchantTransactionId"
                + "&amount=$amount"
                + "&currency=$currency"
                + "&paymentBrand=$cardType"
                + "&paymentType=DB"
                + "&card.number=$cardNumber"
                + "&card.holder=$cardHolder"
                + "&card.expiryMonth=$expiredMonth"
                + "&card.expiryYear=$expiredYear"
                + "&card.cvv=$cvv";

        query = query.replace("$userId", mapData.get("userId"));
        query = query.replace("$password", mapData.get("password"));
        query = query.replace("$entityId", mapData.get("entityId"));
        query = query.replace("$merchantTransactionId", "Q" + mapData.get("bookId"));
        query = query.replace("$currency", mapData.get("currency"));
        query = query.replace("$cardType", mapData.get("cardType"));
        query = query.replace("$cardHolder", mapData.get("cardHolder"));
        query = query.replace("$cardNumber", mapData.get("cardNumber"));
        query = query.replace("$expiredMonth", mapData.get("expiredMonth"));
        query = query.replace("$expiredYear", mapData.get("expiredYear"));
        query = query.replace("$cvv", mapData.get("cvv"));
        query = query.replace("$zipCode", mapData.get("zipCode"));
        query = query.replace("$amount", DF.format(amount));

        DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
        wr.writeBytes(query);
        wr.flush();
        wr.close();

        return getResponse(conn, "createPaymentWithCardInfo");
    }

    @SuppressWarnings("unchecked")
    private Map<String,Object> getResponse(HttpsURLConnection con, String type){
        Map<String,Object> response = new HashMap<>();
        Map<String, String> mapResponse = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        try {
            _log.debug(requestId + " - responseCode: " + con.getResponseCode());
            InputStream is;
            if(con.getResponseCode() < 400){
                is = con.getInputStream();
            } else {
                is = con.getErrorStream();
            }
            StringBuilder inputStringBuilder = new StringBuilder();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String line = bufferedReader.readLine();
            while(line != null){
                inputStringBuilder.append(line);
                inputStringBuilder.append('\n');
                line = bufferedReader.readLine();
            }
            String data = inputStringBuilder.toString();
            _log.debug(requestId + " - response: " + data);
            Map<String,Object> map = mapper.readValue(data, new TypeReference<HashMap<String,Object>>(){});
            Map<String, String> mapResult = (Map<String, String>) map.get("result");
            String code = mapResult.get("code") != null ? mapResult.get("code") : "";
            String description = mapResult.get("description") != null ? mapResult.get("description") : "";
            int returnCode = 437;
            String id = "";
            String token = "";
            switch (code) {
                case "000.000.000" :
                    returnCode = 200;
                    id = map.get("id") != null ? map.get("id").toString() : "";
                    token = map.get("registrationId") != null ? map.get("registrationId").toString() : "";
                    break;
                case "000.100.110" :
                    returnCode = 200;
                    id = map.get("id") != null ? map.get("id").toString() : "";
                    token = map.get("registrationId") != null ? map.get("registrationId").toString() : "";
                    break;
                case "000.100.111" :
                    returnCode = 200;
                    id = map.get("id") != null ? map.get("id").toString() : "";
                    token = map.get("registrationId") != null ? map.get("registrationId").toString() : "";
                    break;
                case "000.100.203" :
                    returnCode = 2001;
                    break;
                case "000.100.210" :
                    returnCode = 2048;
                    break;
                case "000.100.225" :
                    returnCode = 437;  //// Credit Not Processed
                    break;
                case "000.400.107" :
                    returnCode = 443;
                    break;
                case "000.400.108" :
                    returnCode = 437; /// Cardholder Not Found - card number provided is not found in the ranges of the issuer
                    break;
                case "000.500.000" :
                    returnCode = 200;
                    id = map.get("id") != null ? map.get("id").toString() : "";
                    token = map.get("registrationId") != null ? map.get("registrationId").toString() : "";
                    break;
                case "000.500.100" :
                    returnCode = 200;
                    id = map.get("id") != null ? map.get("id").toString() : "";
                    token = map.get("registrationId") != null ? map.get("registrationId").toString() : "";
                    break;
                case "000.600.000" :
                    returnCode = 200;
                    id = map.get("id") != null ? map.get("id").toString() : "";
                    token = map.get("registrationId") != null ? map.get("registrationId").toString() : "";
                    break;
                case "100.100.100" :
                    returnCode = 2061;
                    break;
                case "100.100.101" :
                    returnCode = 2005;
                    break;
                case "100.100.303" :
                    returnCode = 2004;
                    break;
                case "100.100.600" :
                    returnCode = 432;
                    break;
                case "100.100.601" :
                    returnCode = 432;
                    break;
                case "100.100.700" :
                    returnCode = 421;
                    break;
                case "100.100.701" :
                    returnCode = 2014;
                    break;
                case "100.370.100" :
                    returnCode = 2044;
                    break;
                case "100.380.100" :
                    returnCode = 2044;
                    break;
                case "100.400.020" :
                    returnCode = 2044;
                    break;
                case "100.400.081" :
                    returnCode = 2044;
                    break;
                case "100.550.301" :
                    returnCode = 2048;
                    break;
                case "100.550.303" :
                    returnCode = 438;
                    break;
                case "100.550.310" :
                    returnCode = 2002;
                    break;
                case "100.550.311" :
                    returnCode = 2002;
                    break;
                case "700.100.300" :
                    returnCode = 2048;
                    break;
                case "800.100.100" :
                    returnCode = 2044;
                    break;
                case "800.100.150" :
                    returnCode = 2044;
                    break;
                case "800.100.151" :
                    returnCode = 421;
                    break;
                case "800.100.153" :
                    returnCode = 432;
                    break;
                case "800.100.155" :
                    returnCode = 2002;
                    break;
                case "800.100.157" :
                    returnCode = 422;
                    break;
                case "800.100.158" :
                    returnCode = 2014;
                    break;
                case "800.100.159" :
                    returnCode = 2013;
                    break;
                case "800.100.160" :
                    returnCode = 2057;
                    break;
                case "800.100.161" :
                    returnCode = 2003;
                    break;
                case "800.100.162" :
                    returnCode = 2002;
                    break;
                case "800.100.165" :
                    returnCode = 2012;
                    break;
                case "800.100.168" :
                    returnCode = 2057;
                    break;
                case "800.100.170" :
                    returnCode = 2015;
                    break;
                case "800.100.171" :
                    returnCode = 2047;
                    break;
                case "800.100.174" :
                    returnCode = 2048;
                    break;
                case "800.100.192" :
                    returnCode = 432;
                    break;
                case "800.400.100" :
                    returnCode = 445;
                    break;
                case "800.400.110" :
                    returnCode = 2014;
                    break;
                case "800.800.202" :
                    returnCode = 429;
                    break;
                case "800.800.800" :
                    returnCode = 2043;
                    break;
            }

            mapResponse.put("id", id);
            mapResponse.put("token", token);
            mapResponse.put("code", code);
            mapResponse.put("message", description);

            response.put("response", mapResponse);
            response.put("returnCode", returnCode);
        } catch(Exception ex) {
            ex.printStackTrace();

            response.put("returnCode", 437);
        }
        return response;
    }
}
