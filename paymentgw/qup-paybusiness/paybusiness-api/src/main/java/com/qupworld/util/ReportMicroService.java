package com.qupworld.util;
import com.qupworld.paymentgateway.models.RedisDao;
import com.qupworld.paymentgateway.models.RedisImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


class CallReportMicroServiceThread implements Runnable {
    public  String message;
    public  String name;
    Logger logger = LogManager.getLogger(CallReportMicroServiceThread.class);

    public void reportMQ(String message,String name) {
        try{
            RedisDao redisDao = new RedisImpl();
            logger.debug("callLogToReport " + name +" :"+ message);
            redisDao.reportMQ(message,name);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            this.reportMQ(message,name);

        } catch(Exception ex) {

            logger.debug("callLogToReport Error: "+ ex.toString());
        }

    }

}
public class ReportMicroService {
    private ExecutorService threadExecutor;
    private final RedisDao redisDao;

    public ReportMicroService() {
        redisDao = new RedisImpl();
        threadExecutor = Executors.newSingleThreadExecutor();
    }

    public synchronized void close() {
        threadExecutor.shutdown();
        try {
            threadExecutor.awaitTermination(1000l, TimeUnit.MILLISECONDS);
        } catch (InterruptedException ie) {}
        finally {
            threadExecutor = null;
        }
    }

    public synchronized void reportMQ(final String message, final String name){
        try {
            if (threadExecutor != null) {
//                CallReportMicroServiceThread callSlackThread = new CallReportMicroServiceThread();
//                callSlackThread.message = message;
//                callSlackThread.name = name;
                threadExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        redisDao.reportMQ(message,name);
                    }
                });
            }
        }catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public void getReportMQ(){
        try {
            RedisDao redisDao = new RedisImpl();
            String reportData = redisDao.getReportMQ();
            SlackAPI slack = new SlackAPI();
            slack.sendSlack(reportData,"REPORT","REPORT PAYSERVICE API");
        }catch(Exception ex) {
            ex.printStackTrace();
        }

    }

    public void resetReportMQ(){
        try {
            RedisDao redisDao = new RedisImpl();
             redisDao.deleteReportMQ();
        }catch(Exception ex) {
            ex.printStackTrace();
        }

    }
}