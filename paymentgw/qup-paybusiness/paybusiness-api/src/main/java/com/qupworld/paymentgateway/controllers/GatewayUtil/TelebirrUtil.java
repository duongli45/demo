package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.qupworld.config.ServerConfig;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.SecurityUtil;
import okhttp3.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class TelebirrUtil {

    final static Logger logger = LogManager.getLogger(TelebirrUtil.class);

    private String requestId;
    private String MERCHANT_NAME = "";
    private String SHORT_CODE = "";
    private String APP_ID = "";
    private String APP_KEY = "";
    private String PUBLIC_KEY = "";
    private String REQUEST_URL;
    private final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    public TelebirrUtil(String requestId, String merchantName, String shortCode, String appId, String appKey, String publicKey, String requestUrl) {
        this.requestId = requestId;
        MERCHANT_NAME = merchantName;
        SHORT_CODE = shortCode;
        APP_ID = appId;
        APP_KEY = appKey;
        PUBLIC_KEY = publicKey;
        REQUEST_URL = requestUrl;
    }

    public String getCheckoutURL(double amount, String orderId, String reason) throws Exception{
        ObjResponse response = new ObjResponse();
        try {
            String nonce = UUID.randomUUID().toString();
            String notifyURL = ServerConfig.hook_server + "/notification/" + KeysUtil.TELEBIRR + "?urlAction="+orderId;
            String returnUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.TELEBIRR + "?urlAction=return";
            long timestamp = Calendar.getInstance().getTimeInMillis();
            String origin = "appId=" + APP_ID +
                    "&appKey=" + APP_KEY +
                    "&nonce=" + nonce +
                    "&notifyUrl=" + notifyURL +
                    "&outTradeNo=" + orderId +
                    "&receiveName=" + MERCHANT_NAME +
                    "&returnUrl=" + returnUrl +
                    "&shortCode=" + SHORT_CODE +
                    "&subject=" + reason +
                    "&timeoutExpress=30" +
                    "&timestamp=" + timestamp +
                    "&totalAmount=" + KeysUtil.DECIMAL_FORMAT0.format(amount);
            logger.debug(requestId + " - origin = " + origin);

            JSONObject objData = new JSONObject();
            objData.put("appId", APP_ID);
            objData.put("nonce", nonce);
            objData.put("notifyUrl", notifyURL);
            objData.put("outTradeNo", orderId);
            objData.put("receiveName", MERCHANT_NAME);
            objData.put("returnUrl", returnUrl);
            objData.put("shortCode", SHORT_CODE);
            objData.put("subject", reason);
            objData.put("timeoutExpress", "30");
            objData.put("timestamp", timestamp);
            objData.put("totalAmount", KeysUtil.DECIMAL_FORMAT0.format(amount));
            logger.debug(requestId + " - objData = " + objData.toJSONString());

            String hashSHA = SecurityUtil.hashSHA256(requestId, origin);
            String sign = hashSHA.toUpperCase();
            String ussdjson = objData.toJSONString();
            String ussd = SecurityUtil.encryptByPublicKey(requestId, ussdjson, PUBLIC_KEY);

            JSONObject objBody = new JSONObject();
            objBody.put("appid", APP_ID);
            objBody.put("sign", sign);
            objBody.put("ussd", ussd);
            logger.debug(requestId + " - objBody = " + objBody.toJSONString());

            RequestBody body = RequestBody.create(objBody.toJSONString(), JSON);
            Request request = new Request.Builder()
                    .addHeader("Content-Type", "application/json")
                    .url(REQUEST_URL + "/toTradeWebPay")
                    .post(body)
                    .build();

            OkHttpClient client = new OkHttpClient.Builder().build();
            Response gwResponse = client.newCall(request).execute();
            String responseString = gwResponse.body() != null ? gwResponse.body().string() : "";
            logger.debug(requestId + " - response = " + responseString);
            org.json.JSONObject jsonResponse = new org.json.JSONObject(responseString);
            int code = jsonResponse.has("code") ? jsonResponse.getInt("code") : -1;
            String message = "";
            if (code == 0 || code == 200) {
                org.json.JSONObject jsonData = jsonResponse.has("data") ? jsonResponse.getJSONObject("data") : null;
                if (jsonData != null) {
                    String url = jsonData.has("toPayUrl") ? jsonData.getString("toPayUrl") : "";
                    if (!url.isEmpty()) {
                        Map<String, Object> mapResponse = new HashMap<>();
                        mapResponse.put("3ds_url", url);
                        mapResponse.put("type", "link");
                        mapResponse.put("transactionId", orderId);
                        mapResponse.put("isDeeplink", false);
                        response.response = mapResponse;
                        response.returnCode = 200;
                        return response.toString();
                    }
                }
            } else {
                message = jsonResponse.has("message") ? jsonResponse.getString("message") : "";
            }
            if (!message.isEmpty()) {
                Map<String, Object> mapResponse = new HashMap<>();
                mapResponse.put("message", message);
                response.response = mapResponse;
            }
            response.returnCode = 525;
            return response.toString();
        } catch (Exception ex) {
            logger.debug(requestId + " - getCheckoutURL exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response.toString();
        }
    }

    public String decryptData(String data) {
        ObjResponse response = new ObjResponse();
        try {
            Gson gsonEscaping = new GsonBuilder().disableHtmlEscaping().create();
            String escape = gsonEscaping.toJson(data);

            String jsonData = SecurityUtil.decryptByPublicKey(requestId, escape, PUBLIC_KEY);
            Gson gson = new Gson();
            JSONObject objJson = gson.fromJson(jsonData, JSONObject.class);
            if (objJson != null) {
                response.returnCode = 200;
                response.response = objJson;
                return response.toString();
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - decryptData exception: " + CommonUtils.getError(ex));
        }
        response.returnCode = 525;
        return response.toString();
    }
}
