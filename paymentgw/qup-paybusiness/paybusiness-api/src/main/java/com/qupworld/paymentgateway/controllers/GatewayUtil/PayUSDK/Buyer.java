
package com.qupworld.paymentgateway.controllers.GatewayUtil.PayUSDK;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Buyer {

    public String merchantBuyerId;
    public String fullName;
    public String emailAddress;
    public String contactPhone;
    public String dniNumber;
    public ShippingAddress shippingAddress;

}
