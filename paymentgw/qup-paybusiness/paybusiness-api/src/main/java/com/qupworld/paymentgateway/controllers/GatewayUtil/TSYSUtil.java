package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.google.gson.Gson;
import com.qupworld.config.ServerConfig;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
public class TSYSUtil {

    private String requestId;
    private String API_KEY;
    private String COMPANY_NUMBER;
    private String MERCHANT_NUMBER;
    private String MERCHANT_TERMINAL_NUMBER;
    private String REQUEST_URL;
    private static final Logger logger = LogManager.getLogger(TSYSUtil.class);
    private static final Gson gson = new Gson();
    private DecimalFormat DECIMAL_FORMAT = new DecimalFormat("00000000000");

    // define path
    private static final String SECURE_NEW_SESSION = "/securefields/session/new";
    private static final String INFO_USER = "/recur/InfoUser";
    private static final String PRE_AUTH_WITH_TOKEN = "/preAuthorizationWithToken";
    private static final String PURCHASE_WITH_TOKEN = "/purchaseWithToken";
    private static final String COMPLETE = "/completion";
    private static final String VOID = "/void";
    private static final String ACK  = "/ack";

    public TSYSUtil(String _requestId, String apiKey, String companyNumber, String merchantNumber, String terminalNumber, String requestUrl) {
        requestId = _requestId;
        API_KEY = apiKey;
        COMPANY_NUMBER = companyNumber;
        MERCHANT_NUMBER = merchantNumber;
        MERCHANT_TERMINAL_NUMBER = terminalNumber != null ? terminalNumber.trim() : "";
        if (MERCHANT_TERMINAL_NUMBER.isEmpty()) MERCHANT_TERMINAL_NUMBER = "     ";
        REQUEST_URL = requestUrl;
    }

    public static void main(String[] args) throws Exception {
        TSYSUtil tsysUtil = new TSYSUtil(UUID.randomUUID().toString(), "OE5E72FJ2L3V75ON4TX1F15B44BBK079A9",
                "00673", "22700078", "     ", "https://test.api.payfacto.com/v1");
        /*String result = tsysUtil.addUser();

        JSONObject json = gson.fromJson(result, JSONObject.class);
        String token = json.get("token") != null ? json.get("token").toString() : "";
        System.out.println(" - token = " + token);

        tsysUtil.getSession(token);*/

        tsysUtil.getSecureFieldSession("CUS12345", "", true);
    }

    public String getSecureFieldSession(String customerNumber, String sessionId, boolean showAddress) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,Object> mapResponse = new HashMap<>();
        try {
            Map<String, String> params = new HashMap<>();
            params.put("CompanyNumber", COMPANY_NUMBER);
            params.put("MerchantNumber", MERCHANT_NUMBER);
            params.put("CustomerNumber", customerNumber);
            params.put("OperatorID", "SYSTEM00");

            String result = sendRequest(params, SECURE_NEW_SESSION);
            logger.debug(requestId + " - getSession result: " + result);
            JSONObject json = gson.fromJson(result, JSONObject.class);
            String secureID = json.get("secureID") != null ? json.get("secureID").toString() : "";
            logger.debug(requestId + " - secureID: " + secureID);
            if (secureID.trim().isEmpty()) {
                String errorDescription = json.get("errorDescription") != null ? json.get("errorDescription").toString().trim() : "";
                if (!errorDescription.isEmpty()) {
                    mapResponse.put("message", errorDescription);
                }
                objResponse.returnCode = 525;
                objResponse.response = mapResponse;
                return  objResponse.toString();
            }
            mapResponse.put("secureID", secureID);
            mapResponse.put("sessionId", sessionId);
            mapResponse.put("showAddress", showAddress);
            objResponse.response = mapResponse;
            objResponse.returnCode = 200;
        } catch (Exception ex) {
            logger.debug(requestId + " - getSecureFieldsSession exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
        }
        return objResponse.toString();
    }

    public String initiateCreditForm(String customerNumber, String sessionId, boolean showAddress) {
        ObjResponse objResponse = new ObjResponse();
        String url;
        try {
            String getSecureResult = getSecureFieldSession(customerNumber, sessionId, showAddress);
            String secureID = CommonUtils.getValue(getSecureResult, "secureID");
            if (secureID.isEmpty()) {
                // get secureID has failed, return
                return getSecureResult;
            }
            url = ServerConfig.webform_url + "?secureID="+secureID+"&sessionId="+sessionId+"&showAddress="+showAddress;
        } catch(Exception ex) {
            logger.debug(requestId + " - initiateCreditForm exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            return objResponse.toString();
        }

        Map<String,String> mapResponse = new HashMap<>();
        mapResponse.put("3ds_url", url);
        mapResponse.put("type", "link");
        mapResponse.put("transactionId", sessionId);
        objResponse.response = mapResponse;
        objResponse.returnCode = 200;
        return objResponse.toString();

    }

    public String createCreditToken(String customerNumber, String token) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        try {
            Map<String, String> params = new HashMap<>();
            params.put("CompanyNumber", COMPANY_NUMBER);
            params.put("MerchantNumber", MERCHANT_NUMBER);
            params.put("CustomerNumber", customerNumber);
            params.put("Token", token);
            params.put("OperatorID", "SYSTEM00");

            String result = sendRequest(params, INFO_USER);
            logger.debug(requestId + " - createCreditToken result: " + result);
            JSONObject jsonObject = gson.fromJson(result, JSONObject.class);
            String status = jsonObject.get("status") != null ? jsonObject.get("status").toString().trim() : "";
            logger.debug(requestId + " - status: " + status);
            if (status.trim().isEmpty()) {
                String errorDescription = jsonObject.get("errorDescription") != null ? jsonObject.get("errorDescription").toString().trim() : "";
                if (!errorDescription.isEmpty()) {
                    mapResponse.put("message", errorDescription);
                }
                objResponse.returnCode = 525;
                return  objResponse.toString();
            }
            String cardType = jsonObject.get("cardType") != null ? jsonObject.get("cardType").toString().trim() : "";
            String cardNumber = jsonObject.get("cardNumber") != null ? jsonObject.get("cardNumber").toString().trim() : "";
            cardNumber = cardNumber.trim();
            logger.debug(requestId + " - card: " + cardNumber + "-" + cardType);
            String cardTypeValue = "";
            switch (cardType) {
                case "A" :
                    cardTypeValue = "AmericanExpress";
                    break;
                case "M" :
                    cardTypeValue = "MasterCard";
                    break;
                case "I" :
                    cardTypeValue = "Diners";
                    break;
                case "V" :
                    cardTypeValue = "Visa";
                    break;
                case "O" :
                    cardTypeValue = "Discover";
                    break;
            }
            mapResponse.put("cardType", cardTypeValue);
            mapResponse.put("last4", cardNumber.length() > 0 ? cardNumber.substring(cardNumber.length() - 4) : "");
            objResponse.response = mapResponse;
            objResponse.returnCode = 200;
            return objResponse.toString();
        } catch(Exception ex) {
            logger.debug(requestId + " - createCreditToken exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            return objResponse.toString();
        }
    }

    public String createPaymentWithCreditToken(double amount, String token, String currencyISO) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        try {
            String[] arrToken = token.split(KeysUtil.TEMPKEY);
            Map<String, String> params = new HashMap<>();
            params.put("CompanyNumber", COMPANY_NUMBER);
            params.put("MerchantNumber", MERCHANT_NUMBER);
            params.put("CustomerNumber", arrToken[1]);
            params.put("Amount", DECIMAL_FORMAT.format(amount*100));
            params.put("InvoiceNumber", getInvoice());
            params.put("InputType", "I");
            params.put("Token", arrToken[0]);
            params.put("MerchantTerminalNumber", MERCHANT_TERMINAL_NUMBER);
            params.put("LanguageCode", "E");
            params.put("CurrencyCode", currencyISO);
            params.put("OperatorID", "SYSTEM00");

            String result = sendRequest(params, PURCHASE_WITH_TOKEN);
            logger.debug(requestId + " - createPaymentWithCreditToken result: " + result);
            JSONObject jsonObject = gson.fromJson(result, JSONObject.class);
            String returnCode = jsonObject.get("returnCode") != null ? jsonObject.get("returnCode").toString().trim() : "";
            logger.debug(requestId + " - returnCode: " + returnCode);
            if (returnCode.trim().isEmpty() || !returnCode.trim().equals("00")) {
                String errorDescription = jsonObject.get("errorDescription") != null ? jsonObject.get("errorDescription").toString().trim() : "";
                if (!errorDescription.isEmpty()) {
                    mapResponse.put("message", errorDescription);
                }
                objResponse.returnCode = 525;
                return  objResponse.toString();
            }

            String transactionNumber = jsonObject.get("transactionNumber") != null ? jsonObject.get("transactionNumber").toString().trim() : "";
            String cardType = jsonObject.get("cardType") != null ? jsonObject.get("cardType").toString().trim() : "";
            String authorizationNumber = jsonObject.get("authorizationNumber") != null ? jsonObject.get("authorizationNumber").toString().trim() : "";
            String cardNumber = jsonObject.get("cardNumber") != null ? jsonObject.get("cardNumber").toString().trim() : "";
            Map<String, String> paramsACK = new HashMap<>();
            paramsACK.put("CompanyNumber", COMPANY_NUMBER);
            paramsACK.put("MerchantNumber", MERCHANT_NUMBER);
            paramsACK.put("TransactionNumber", transactionNumber);
            String resultACK = sendRequest(paramsACK, ACK);
            logger.debug(requestId + " - ACK result: " + resultACK);
            JSONObject jsonObjectACK = gson.fromJson(resultACK, JSONObject.class);
            String returnCodeACK = jsonObjectACK.get("returnCode") != null ? jsonObjectACK.get("returnCode").toString().trim() : "";
            logger.debug(requestId + " - returnCodeACK: " + returnCodeACK);
            if (returnCodeACK.isEmpty() || !returnCodeACK.equals("true")) {
                objResponse.returnCode = 525;
                return objResponse.toString();
            }

            mapResponse.put("message", "Success!" );
            mapResponse.put("transId", transactionNumber);
            mapResponse.put("cardType", cardType);
            mapResponse.put("authCode", authorizationNumber);
            mapResponse.put("cardMask", cardNumber);
            objResponse.response = mapResponse;
            objResponse.returnCode = 200;
            return objResponse.toString();
        } catch(Exception ex) {
            logger.debug(requestId + " - createPaymentWithCreditToken exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            return objResponse.toString();
        }
    }

    public String preAuthPayment(double amount, String token, String currencyISO) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        try {
            String[] arrToken = token.split(KeysUtil.TEMPKEY);
            String invoice = getInvoice();
            Map<String, String> params = new HashMap<>();
            params.put("CompanyNumber", COMPANY_NUMBER);
            params.put("MerchantNumber", MERCHANT_NUMBER);
            params.put("CustomerNumber", arrToken[1]);
            params.put("Amount", DECIMAL_FORMAT.format(amount*100));
            params.put("InvoiceNumber", invoice);
            params.put("InputType", "I");
            params.put("Token", arrToken[0]);
            params.put("MerchantTerminalNumber", MERCHANT_TERMINAL_NUMBER);
            params.put("LanguageCode", "E");
            params.put("CurrencyCode", currencyISO);
            params.put("OperatorID", "SYSTEM00");

            String result = sendRequest(params, PRE_AUTH_WITH_TOKEN);
            logger.debug(requestId + " - preAuthPayment result: " + result);
            JSONObject jsonObject = gson.fromJson(result, JSONObject.class);
            String returnCode = jsonObject.get("returnCode") != null ? jsonObject.get("returnCode").toString().trim() : "";
            logger.debug(requestId + " - returnCode: " + returnCode);
            if (returnCode.trim().isEmpty() || !returnCode.trim().equals("00")) {
                String errorDescription = jsonObject.get("errorDescription") != null ? jsonObject.get("errorDescription").toString().trim() : "";
                if (!errorDescription.isEmpty()) {
                    mapResponse.put("message", errorDescription);
                }
                objResponse.returnCode = 525;
                return  objResponse.toString();
            }

            String transactionNumber = jsonObject.get("transactionNumber") != null ? jsonObject.get("transactionNumber").toString().trim() : "";
            String authorizationNumber = jsonObject.get("authorizationNumber") != null ? jsonObject.get("authorizationNumber").toString().trim() : "";
            Map<String, String> paramsACK = new HashMap<>();
            paramsACK.put("CompanyNumber", COMPANY_NUMBER);
            paramsACK.put("MerchantNumber", MERCHANT_NUMBER);
            paramsACK.put("TransactionNumber", transactionNumber);
            String resultACK = sendRequest(paramsACK, ACK);
            logger.debug(requestId + " - ACK result: " + resultACK);
            JSONObject jsonObjectACK = gson.fromJson(resultACK, JSONObject.class);
            String returnCodeACK = jsonObjectACK.get("returnCode") != null ? jsonObjectACK.get("returnCode").toString().trim() : "";
            logger.debug(requestId + " - returnCodeACK: " + returnCodeACK);
            if (returnCodeACK.isEmpty() || !returnCodeACK.equals("true")) {
                objResponse.returnCode = 525;
                return objResponse.toString();
            }

            mapResponse.put("authId", invoice + KeysUtil.TEMPKEY + transactionNumber + KeysUtil.TEMPKEY + authorizationNumber);
            mapResponse.put("authAmount", String.valueOf(amount));
            mapResponse.put("authCode", authorizationNumber);
            mapResponse.put("authToken", token);
            mapResponse.put("allowCapture", "true");

            objResponse.response = mapResponse;
            objResponse.returnCode = 200;
            return objResponse.toString();
        } catch(Exception ex) {
            logger.debug(requestId + " - preAuthPayment exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            return objResponse.toString();
        }
    }

    public String preAuthCapture(double amount, String authId, String token) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        try {
            String[] arrToken = token.split(KeysUtil.TEMPKEY);
            String[] origin = authId.split(KeysUtil.TEMPKEY);

            Map<String, String> params = new HashMap<>();
            params.put("CompanyNumber", COMPANY_NUMBER);
            params.put("MerchantNumber", MERCHANT_NUMBER);
            params.put("CustomerNumber", arrToken[1]);
            params.put("Amount", DECIMAL_FORMAT.format(amount*100));
            params.put("InvoiceNumber", getInvoice());
            params.put("OriginalInvoiceNumber", origin[0]);
            params.put("OriginalTransactionNumber", origin[1]);
            params.put("OriginalAuthorizationNumber", origin[2]);
            params.put("TrxOption", "isRecurring");

            String result = sendRequest(params, COMPLETE);
            logger.debug(requestId + " - preAuthCapture result: " + result);
            JSONObject jsonObject = gson.fromJson(result, JSONObject.class);
            String returnCode = jsonObject.get("returnCode") != null ? jsonObject.get("returnCode").toString().trim() : "";
            logger.debug(requestId + " - returnCode: " + returnCode);
            if (returnCode.trim().isEmpty() || !returnCode.trim().equals("00")) {
                String errorDescription = jsonObject.get("errorDescription") != null ? jsonObject.get("errorDescription").toString().trim() : "";
                if (!errorDescription.isEmpty()) {
                    mapResponse.put("message", errorDescription);
                }
                objResponse.returnCode = 525;
                return  objResponse.toString();
            }
            String cardType = jsonObject.get("cardType") != null ? jsonObject.get("cardType").toString().trim() : "";
            String transactionNumber = jsonObject.get("transactionNumber") != null ? jsonObject.get("transactionNumber").toString().trim() : "";
            String authorizationNumber = jsonObject.get("authorizationNumber") != null ? jsonObject.get("authorizationNumber").toString().trim() : "";

            Map<String, String> paramsACK = new HashMap<>();
            paramsACK.put("CompanyNumber", COMPANY_NUMBER);
            paramsACK.put("MerchantNumber", MERCHANT_NUMBER);
            paramsACK.put("TransactionNumber", transactionNumber);
            String resultACK = sendRequest(paramsACK, ACK);
            logger.debug(requestId + " - ACK result: " + resultACK);
            JSONObject jsonObjectACK = gson.fromJson(resultACK, JSONObject.class);
            String returnCodeACK = jsonObjectACK.get("returnCode") != null ? jsonObjectACK.get("returnCode").toString().trim() : "";
            logger.debug(requestId + " - returnCodeACK: " + returnCodeACK);
            if (returnCodeACK.isEmpty() || !returnCodeACK.equals("true")) {
                objResponse.returnCode = 525;
                return objResponse.toString();
            }

            mapResponse.put("message", "Success!" );
            mapResponse.put("transId", transactionNumber);
            mapResponse.put("cardType", cardType);
            mapResponse.put("authCode", authorizationNumber);
            mapResponse.put("cardMask", "");
            objResponse.response = mapResponse;
            objResponse.returnCode = 200;
            return objResponse.toString();
        } catch(Exception ex) {
            logger.debug(requestId + " - preAuthCapture exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            return objResponse.toString();
        }
    }

    public void voidTransaction(String authId, String token) {
        try {
            // Regarding canceling a preauthorization, the right process to follow is to send a 0.00$ completion on the initial preauthorization
            String result = preAuthCapture(0.0, authId, token);
            logger.debug(requestId + " - voidTransaction result: " + result);
        } catch(Exception ex) {
            logger.debug(requestId + " - voidTransaction exception: " + CommonUtils.getError(ex));
        }
    }

    private String getInvoice() throws Exception {
        int len = 12;
        String text = "0123456789";
        SecureRandom rnd = new SecureRandom();
        StringBuilder sb = new StringBuilder(len);
        for(int i = 0; i < len; i++)
            sb.append(text.charAt(rnd.nextInt(text.length())));
        return sb.toString();
    }

    private String sendRequest(Map<String, String> params, String path) {
        try {
            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            nvps.add(new BasicNameValuePair("auth-api-key", API_KEY));

            StringBuffer paramToEncode = new StringBuffer();
            boolean isFirst = true;
            if (params != null) {
                for (String key : params.keySet()) {
                    logger.debug(requestId + " - "+key+": " + params.get(key));
                    if (! isFirst) {
                        paramToEncode.append("&");
                    }
                    paramToEncode.append( key + "=" + String.valueOf(params.get(key)));
                    isFirst = false;
                }
            }
            nvps.add(new BasicNameValuePair("payload", Base64.getEncoder().encodeToString( paramToEncode.toString().getBytes())));

            HttpClient client = HttpClientBuilder.create().disableRedirectHandling().build();

            HttpContext httpContext = new BasicHttpContext();
            CookieStore cookieStore = new BasicCookieStore();
            httpContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);

            int CONNECTION_TIMEOUT_MS = 180 * 1000; // Timeout in millis.
            RequestConfig requestConfig = RequestConfig.custom()
                    .setConnectionRequestTimeout(CONNECTION_TIMEOUT_MS)
                    .setConnectTimeout(CONNECTION_TIMEOUT_MS)
                    .setSocketTimeout(CONNECTION_TIMEOUT_MS)
                    .build();

            HttpPost postRequest = new HttpPost(REQUEST_URL + path);
            postRequest.setConfig(requestConfig);
            postRequest.setHeader(HttpHeaders.ACCEPT, "application/json");
            postRequest.setEntity(new UrlEncodedFormEntity(nvps));
            HttpResponse httpResponse = client.execute(postRequest, httpContext);


            BufferedReader in = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));

            StringBuilder response = new StringBuilder();
            String line;

            while ((line = in.readLine()) != null) {
                response.append(line);
            }

            in.close();
            return response.toString();
        } catch (Exception ex) {
            logger.debug(requestId + " - sendRequest exception: " + CommonUtils.getError(ex));
            return "";
        }
    }

}
