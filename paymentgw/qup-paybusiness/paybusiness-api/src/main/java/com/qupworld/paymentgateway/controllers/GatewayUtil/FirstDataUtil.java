package com.qupworld.paymentgateway.controllers.GatewayUtil;
import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.entities.CreditEnt;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class FirstDataUtil {
    final static Logger logger = LogManager.getLogger(FirstDataUtil.class);
    private String basePath;
    private String apiSecret;
    private String apiKey;
    private String storeId;
    private String paymentStoreId;
    public FirstDataUtil(String _apiSecret, String _apiKey, String _storeId, String _paymentStoreId, String _basePath) {
        basePath = _basePath;
        storeId = _storeId;
        paymentStoreId = _paymentStoreId;
        apiSecret = _apiSecret;
        apiKey = _apiKey;
    }


    public Map<String,String> createToken3DSP3(String cvv, String MD, String payerAuthenticationResponse, String ipgTransactionId, String requestId)  {
        Map<String, String> mapResponse = new HashMap<>();
        mapResponse.put("transactionStatus", "ERROR"  );

        try {
            Gson gson = new Gson();
            Map<String, Object> request = new HashMap<>();
            request.put("authenticationType", "Secure3D10AuthenticationUpdateRequest");
            request.put("securityCode", cvv);
            request.put("payerAuthenticationResponse", payerAuthenticationResponse);
            request.put("merchantData", MD);
            request.put("storeId", storeId);
            String bodyStr =  gson.toJson(request);
            org.json.JSONObject objectResponse = callServerWithPatch(bodyStr,"payments/"+ipgTransactionId);
            logger.debug(requestId + " - Request " + bodyStr);
            logger.debug(requestId + " - Response " + objectResponse.toString());
            if((Integer)objectResponse.get("statusCode") == 200 &&  objectResponse.getJSONObject("response").get("transactionStatus").toString().equals("APPROVED")) {
                mapResponse.put("transactionStatus", "OK"  );

            }else {
                String errorMS = objectResponse.getJSONObject("response").getJSONObject("error").get("message").toString();
                mapResponse.put("message", errorMS  );
            }
        } catch(Exception ex) {
            ex.printStackTrace();

        }
        return mapResponse;
    }
    private String createHTMLForm(String acsURL,String merchantData, String payerAuthenticationRequest,String qupUrl){
        String url = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<body>\n" +
                "\n" +
                "<p>Processing...</p>\n" +
                "\n" +
                " <form method=\"post\" action=\"%acsURL\" name=\"f1\" >   <!-- ACSURL --> \n" +
                "\n" +
                "<table>\n" +
                "\t<tbody>\n" +
                "\t<!-- Merchant Ddata Value -->\n" +
                "\t<input type=\"hidden\" name=\"MD\" value=\"%merchantData\"/>\n" +
                "\t<!-- PARES Value -->\n" +
                "\t<input type=\"hidden\" name=\"PaReq\" value=\"%payerAuthenticationRequest\"/>\n" +
                "\t\t<!-- Safe Charge URL -->\n" +
                "\t\t<input type=\"hidden\" name=\"TermUrl\" value=\n" +
                "\t\t\"%termURL\"/> \n" +
                "\t\n" +
                "\t</tbody>\n" +
                "</table>\n" +
                "<script type=\"text/javascript\">            \t\n" +
                "document.f1.submit();\n" +
                "</script>\n" +
                "</form>\n" +
                "</body>\n" +
                "</html>";
        url = url.replaceFirst("%acsURL", acsURL);
        url = url.replaceFirst("%merchantData", merchantData);
        url = url.replaceFirst("%payerAuthenticationRequest", payerAuthenticationRequest);
        url = url.replaceFirst("%termURL", qupUrl);
        return url;
    }
    public Map<String,String> createCreditToken3DS( CreditEnt credit)  {
        org.json.JSONObject objectResponse = null;
        String htmlForm = "";
        String merchantData = "";
        String ipgTransactionId = "";
        String token = "";
        try {
            Gson gson = new Gson();
            Map<String, Object> transactionAmount = new HashMap<String, Object>();
            transactionAmount.put("total", 1.0);
            transactionAmount.put("currency", "INR");

            String[] arr = credit.expiredDate.split("/");
            Map<String, Object> expiryDate = new HashMap<String, Object>();
            expiryDate.put("month", arr[0]);
            expiryDate.put("year",  arr[1].substring(2, 4));

            Map<String, Object> paymentCard = new HashMap<String, Object>();
            paymentCard.put("expiryDate", expiryDate);
            paymentCard.put("number", credit.cardNumber);
            paymentCard.put("securityCode", credit.cvv);

            Map<String, Object> paymentMethod = new HashMap<String, Object>();
            paymentMethod.put("paymentCard",paymentCard);
            Map<String, Object> authenticationRequest = new HashMap<String, Object>();
            authenticationRequest.put("authenticationType","Secure3D10AuthenticationRequest");

            Map<String, Object> createToken = new HashMap<String, Object>();

            createToken.put("reusable", true);
            createToken.put("declineDuplicates", false);


            Map<String, Object> request = new HashMap<String, Object>();
            request.put("requestType", "PaymentCardSaleTransaction");
            request.put("paymentMethod", paymentMethod);
            request.put("transactionAmount", transactionAmount);
            request.put("authenticationRequest",authenticationRequest);
            request.put("createToken",createToken);
            request.put("storeId",storeId);

            String bodyStr =  gson.toJson(request);
            objectResponse = callServer(bodyStr,"payments");
            logger.debug(credit.requestId + " - Request " + bodyStr.replace(credit.cardNumber,"XXXXXX"));
            logger.debug(credit.requestId + " - Response " + objectResponse.toString());
            if((Integer)objectResponse.get("statusCode") == 200 &&  objectResponse.getJSONObject("response").get("transactionStatus").toString().equals("WAITING")) {
                token = objectResponse.getJSONObject("response").getJSONObject("paymentToken").get("value").toString();
//                token = "ABC";
                String cardType = objectResponse.getJSONObject("response").getJSONObject("paymentToken").get("brand").toString();
                String cardMasked = "XXXXXXXX"+objectResponse.getJSONObject("response").getJSONObject("paymentMethodDetails").getJSONObject("paymentCard").get("last4").toString();
                String qupUrl = ServerConfig.dispatch_server+KeysUtil.NOTIFICATION_URL+"?token="+token+"&cardType="+cardType+"&cardMasked="+cardMasked;
                String acsURL = objectResponse.getJSONObject("response").getJSONObject("authenticationResponse").getJSONObject("params").get("acsURL").toString();
                String payerAuthenticationRequest = objectResponse.getJSONObject("response").getJSONObject("authenticationResponse").getJSONObject("params").get("payerAuthenticationRequest").toString();
                merchantData = objectResponse.getJSONObject("response").getJSONObject("authenticationResponse").getJSONObject("params").get("merchantData").toString();
                ipgTransactionId = objectResponse.getJSONObject("response").get("ipgTransactionId").toString();
                htmlForm = createHTMLForm(acsURL,merchantData,payerAuthenticationRequest, qupUrl);
                logger.debug(credit.requestId + " - qupUrl: " + qupUrl);
                logger.debug(credit.requestId + " - htmlForm: " + htmlForm);
            }else {
                String errorMS = objectResponse.getJSONObject("response").getJSONObject("error").get("message").toString();
                String errorCode = objectResponse.getJSONObject("response").getJSONObject("error").get("code").toString();

                Map<String,String> map = new HashMap<>();
                map.put("errorMS", errorMS);
                map.put("errorCode", errorCode);
                return map;
            }

        } catch(Exception ex) {
            ex.printStackTrace();

        }
        Map<String,String> map = new HashMap<>();
        map.put("htmlForm", htmlForm);
        map.put("merchantData", merchantData);
        map.put("ipgTransactionId", ipgTransactionId);
        map.put("token", token);
        return map;
    }

    public String initiateCreditForm(CreditEnt creditEnt) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        Map<String,String> data3DS = createCreditToken3DS(creditEnt);
        String value = data3DS.get("errorMS");
        if (value != null) {
            return returnPaymentFailed(creditEnt.requestId, "", 437,value, value);
        }

        objResponse.returnCode = 200;
        mapResponse.put("3ds_url", data3DS.get("htmlForm"));
        mapResponse.put("merchantData", data3DS.get("merchantData"));
        mapResponse.put("ipgTransactionId", data3DS.get("ipgTransactionId"));
        mapResponse.put("token", data3DS.get("token"));
        mapResponse.put("type", "form");
        objResponse.response = mapResponse;
        return objResponse.toString();
    }

    public String createPaymentWithCreditToken(String token,double total,String currencyISO,String requestId){
        try {
            DecimalFormat df = new DecimalFormat("0.00");
            total =  Double.valueOf(df.format(total));
            org.json.JSONObject payByToken = payByToken(token, total, currencyISO, "PaymentTokenSaleTransaction",requestId);
            logger.debug(requestId + " - payByToken: " + payByToken.toString());
            /*{
                "response": {
                    "country": "Vietnam",
                    "orderId": "R-679a95ea-3c99-4364-bc71-343bc87ccdf7",
                    "transactionStatus": "APPROVED",
                    "clientRequestId": "9546418e-c0e9-4ef7-8cd7-ad219b21bc28",
                    "terminalId": "87032761",
                    "transactionTime": 1611128039,
                    "transactionOrigin": "ECOM",
                    "processor": {
                        "authorizationCode": "223300",
                        "responseMessage": "Function performed error-free",
                        "avsResponse": {
                            "streetMatch": "NO_INPUT_DATA",
                            "postalCodeMatch": "NO_INPUT_DATA"
                        },
                        "responseCode": "00"
                    },
                    "transactionType": "SALE",
                    "schemeTransactionId": "301020272392660",
                    "merchantId": "470000087032761",
                    "paymentMethodDetails": {
                        "paymentCard": {
                            "expiryDate": {
                                "month": "11",
                                "year": "2021"
                            },
                            "last4": "0780",
                            "bin": "412975",
                            "cardFunction": "CREDIT",
                            "brand": "VISA"
                        },
                        "paymentMethodType": "PAYMENT_CARD"
                    },
                    "approvedAmount": {
                        "total": 2,
                        "components": {
                            "subtotal": 2
                        },
                        "currency": "INR"
                    },
                    "ipgTransactionId": "70264774954",
                    "apiTraceId": "rrt-0afc241aed6a2ca15-b-wo-19937-349788746-1"
                },
                "statusCode": 200
            }*/
            if ((Integer) payByToken.get("statusCode") == 200 && payByToken.getJSONObject("response").get("transactionStatus").toString().equals("APPROVED")) {
                String last4 = payByToken.getJSONObject("response").getJSONObject("paymentMethodDetails").getJSONObject("paymentCard").get("last4").toString();
                String cardType = payByToken.getJSONObject("response").getJSONObject("paymentMethodDetails").getJSONObject("paymentCard").get("brand").toString();
                String authorizationCode = payByToken.getJSONObject("response").getJSONObject("processor").get("authorizationCode").toString();
                String transId = payByToken.getJSONObject("response").get("ipgTransactionId").toString();
                ObjResponse preAuthResponse = new ObjResponse();
                Map<String, String> mapResponse = new HashMap<String, String>();
                mapResponse.put("message", "Success!");
                mapResponse.put("transId", transId);
                mapResponse.put("cardType", cardType);
                mapResponse.put("authCode", authorizationCode);
                mapResponse.put("cardMask", "XXXXXXXX"+last4);
                preAuthResponse.response = mapResponse;
                preAuthResponse.returnCode = 200;
                return preAuthResponse.toString();
            }else {
                /*{
                    "response": {
                        "transactionType": "SALE",
                        "responseType": "GatewayDeclined",
                        "orderId": "R-d5ec1264-eb9d-446e-9b8a-1d7e3183fb5b",
                        "transactionStatus": "VALIDATION_FAILED",
                        "clientRequestId": "b25c7d07-ae06-4660-aa1b-71abf1310ba3",
                        "paymentMethodDetails": {
                            "paymentCard": {
                                "expiryDate": {
                                    "month": "09",
                                    "year": "2022"
                                },
                                "last4": "1001",
                                "bin": "377116",
                                "cardFunction": "CREDIT",
                                "brand": "AMEX"
                            },
                            "paymentMethodType": "PAYMENT_CARD"
                        },
                        "ipgTransactionId": "70264906472",
                        "error": {
                            "code": "5002",
                            "message": "The merchant is not setup to support the requested service."
                        },
                        "transactionOrigin": "ECOM",
                        "apiTraceId": "rrt-034d9f7b49bb306a5-c-wo-6686-351394757-1"
                    },
                    "statusCode": 409
                }*/
                String transId = "";
                String errorMS = "";
                if (payByToken.getJSONObject("response") != null) {
                    org.json.JSONObject objResponse = payByToken.getJSONObject("response");
                    transId = objResponse.get("ipgTransactionId") != null ? objResponse.get("ipgTransactionId").toString() : "";
                    if (transId.isEmpty()) {
                        transId = objResponse.get("orderId") != null ? objResponse.get("orderId").toString() : "";
                    }
                    errorMS = objResponse.getJSONObject("error").get("message").toString();
                }
                if (errorMS != null) {
                    return returnPaymentFailed(requestId, transId, 437,errorMS, errorMS);
                }
                return returnPaymentFailed(requestId, transId, 437,"", payByToken.toString());
            }
        }catch (Exception ex) {
            ex.printStackTrace();
            return returnPaymentFailed(requestId, token, 437, getError(ex), ex.getMessage());

        }

    }

    private org.json.JSONObject payByToken( String token,double total,String currencyISO,String requestType, String requestId)  {
        org.json.JSONObject objectResponse = null;
        try {
            DecimalFormat df = new DecimalFormat("0.00");
            total =  Double.valueOf(df.format(total));
            Gson gson = new Gson();
            Map<String, Object> transactionAmount = new HashMap<String, Object>();
            transactionAmount.put("total", total);
            transactionAmount.put("currency", currencyISO);

            Map<String, Object> paymentToken = new HashMap<String, Object>();
            paymentToken.put("value", token);
            paymentToken.put("function", "CREDIT");
            paymentToken.put("tokenOriginStoreId", storeId);
            Map<String, Object> installmentOptions = new HashMap<String, Object>();
            installmentOptions.put("recurringType", "STANDING_INSTRUCTION");

            Map<String, Object> order = new HashMap<String, Object>();
            order.put("installmentOptions", installmentOptions);


            Map<String, Object> paymentMethod = new HashMap<String, Object>();
            paymentMethod.put("paymentToken", paymentToken);

            Map<String, Object> request = new HashMap<String, Object>();
            request.put("requestType", requestType);
            request.put("paymentMethod", paymentMethod);
            request.put("transactionAmount", transactionAmount);
            request.put("order", order);
            request.put("storeId",paymentStoreId);

            String bodyStr =  gson.toJson(request);
            logger.debug(requestId + " - Request " + bodyStr);
            objectResponse = callServer(bodyStr,"payments");

            return objectResponse;
        } catch(Exception ex) {
            ex.printStackTrace();

        }
        return objectResponse;
    }
    private org.json.JSONObject catpture( String orderId,double total,String currencyISO,String requestId)  {
        org.json.JSONObject objectResponse = null;
        try {
            DecimalFormat df = new DecimalFormat("0.00");
            total =  Double.valueOf(df.format(total));
            Gson gson = new Gson();
            Map<String, Object> transactionAmount = new HashMap<String, Object>();
            transactionAmount.put("total", total);
            transactionAmount.put("currency", currencyISO);

            Map<String, Object> request = new HashMap<String, Object>();
            request.put("requestType", "PostAuthTransaction");
            request.put("transactionAmount", transactionAmount);
            String bodyStr =  gson.toJson(request);
            objectResponse = callServer(bodyStr,"orders/"+orderId);
            logger.debug(requestId + " - Request " + bodyStr);
            logger.debug(requestId + " - Response " + objectResponse.toString());

            return objectResponse;
        } catch(Exception ex) {
            ex.printStackTrace();

        }
        return objectResponse;
    }
    public String preauthPayment(String token, double total,String currencyISO,String requestId){
        try {
            DecimalFormat df = new DecimalFormat("0.00");
            total =  Double.valueOf(df.format(total));
        logger.debug(requestId + " - preAuthCapture: token = " + token +"total: "+total);
        org.json.JSONObject objectResponse = payByToken(token,total,currencyISO,"PaymentTokenPreAuthTransaction",requestId);
            logger.debug(requestId + " - Response " + objectResponse.toString());
        if((Integer)objectResponse.get("statusCode") == 200 &&  objectResponse.getJSONObject("response").get("transactionStatus").toString().equals("APPROVED")) {
            String orderId = objectResponse.getJSONObject("response").get("orderId").toString();
            ObjResponse preAuthResponse = new ObjResponse();
            Map<String, String> mapResponse = new HashMap<String, String>();
            mapResponse.put("authId", orderId);
            mapResponse.put("authAmount", String.valueOf(total));
            mapResponse.put("authCode", "");
            mapResponse.put("authToken", token);
            mapResponse.put("allowCapture", "true");

            preAuthResponse.response = mapResponse;
            preAuthResponse.returnCode = 200;

            String result = preAuthResponse.toString();
            return result;
        }else {
            String errorMS = objectResponse.getJSONObject("response").getJSONObject("error").get("message").toString();
            if (errorMS != null) {
                return returnPaymentFailed(requestId, "", 437,errorMS, errorMS);
            }
            return returnPaymentFailed(requestId, token, 437,"", objectResponse.toString());

        }


        } catch(Exception ex) {
            ex.printStackTrace();
            return returnPaymentFailed(requestId, token, 437, getError(ex), ex.getMessage());
        }

    }
    public String preAuthCapture(String orderId, double total,String currencyISO,String requestId) {
        try {
            DecimalFormat df = new DecimalFormat("0.00");
            total =  Double.valueOf(df.format(total));
            logger.debug(requestId + " - preAuthCapture: orderId = " + orderId);
            org.json.JSONObject objectResponse = catpture(orderId,total,currencyISO,requestId);
            if((Integer)objectResponse.get("statusCode") == 200 &&  objectResponse.getJSONObject("response").get("transactionStatus").toString().equals("APPROVED")) {
                String last4 = objectResponse.getJSONObject("response").getJSONObject("paymentMethodDetails").getJSONObject("paymentCard").get("last4").toString();
                String cardType = objectResponse.getJSONObject("response").getJSONObject("paymentMethodDetails").getJSONObject("paymentCard").get("brand").toString();
                String authorizationCode = objectResponse.getJSONObject("response").getJSONObject("processor").get("authorizationCode").toString();
                String transId = objectResponse.getJSONObject("response").get("ipgTransactionId").toString();
                ObjResponse preAuthResponse = new ObjResponse();
                Map<String, String> mapResponse = new HashMap<String, String>();
                mapResponse.put("message", "Success!");
                mapResponse.put("transId", transId);
                mapResponse.put("cardType", cardType);
                mapResponse.put("authCode", authorizationCode);
                mapResponse.put("cardMask", "XXXXXXXX"+last4);
                preAuthResponse.response = mapResponse;
                preAuthResponse.returnCode = 200;
                String result = preAuthResponse.toString();
                return result;

            }else {
                String errorMS = objectResponse.getJSONObject("response").getJSONObject("error").get("message").toString();
                if (errorMS != null) {
                    return returnPaymentFailed(requestId, "", 437,errorMS, errorMS);
                }
                return returnPaymentFailed(requestId, orderId, 437,"", objectResponse.toString());

            }
        } catch(Exception ex) {
            ex.printStackTrace();
            return returnPaymentFailed(requestId, orderId, 437, getError(ex), ex.getMessage());
        }

    }
    private org.json.JSONObject callServerWithPatch(String bodyStr,String url)  {
        org.json.JSONObject objectResponse = new org.json.JSONObject();
        try {
            String requestId = UUID.randomUUID().toString();
            Date date = new Date();
            long timeMilli = date.getTime();
            String messageSignature = createMessageSignature(requestId,String.valueOf(timeMilli),bodyStr);
            logger.debug(basePath+"/"+url);
            HttpResponse<JsonNode> jsonResponse  = Unirest.patch(basePath+"/"+url)
                    .header("Content-Type", "application/json")
                    .header("Client-Request-Id", requestId)
                    .header("Api-Key", apiKey)
                    .header("Timestamp", String.valueOf(timeMilli))
                    .header("Message-Signature", messageSignature)
                    .body(bodyStr)
                    .asJson();
            org.json.JSONObject body = jsonResponse.getBody().getObject();
            int statusCode = jsonResponse.getStatus();
            objectResponse.put("statusCode",statusCode);
            objectResponse.put("response",body);
        }  catch (Exception e) {
            e.printStackTrace();
        }
        return  objectResponse;
    }
    private org.json.JSONObject callServer(String bodyStr,String url)  {
        org.json.JSONObject objectResponse = new org.json.JSONObject();
        try {
            String requestId = UUID.randomUUID().toString();
            Date date = new Date();
            long timeMilli = date.getTime();
            String messageSignature = createMessageSignature(requestId,String.valueOf(timeMilli),bodyStr);
            logger.debug(basePath+"/"+url);
            HttpResponse<JsonNode> jsonResponse  = Unirest.post(basePath+"/"+url)
                    .header("Content-Type", "application/json")
                    .header("Client-Request-Id", requestId)
                    .header("Api-Key", apiKey)
                    .header("Timestamp", String.valueOf(timeMilli))
                    .header("Message-Signature", messageSignature)
                    .body(bodyStr)
                    .asJson();
           org.json.JSONObject body = jsonResponse.getBody().getObject();
           int statusCode = jsonResponse.getStatus();
            objectResponse.put("statusCode",statusCode);
            objectResponse.put("response",body);
        }  catch (Exception e) {
                e.printStackTrace();
        }
        return  objectResponse;
    }
    private String createMessageSignature(String requestId, String timeStam, String playload){
        final HmacUtils hmacHelper = new HmacUtils(HmacAlgorithms.HMAC_SHA_256, apiSecret);
        final Hex hexHelper = new Hex();
        final String msg = apiKey + requestId + timeStam + playload;
        final byte[] raw = hmacHelper.hmac(msg);
        final byte[] hex = hexHelper.encode(raw);
        final String messageSignature = Base64.encodeBase64String(hex);
        return  messageSignature;
    }
    public String voidPreAuth() {
        return "";
    }
    public String voidPaymentsTransaction(){
        return "";
    }
    public String returnPaymentFailed(String requestId, String chargeId, int code, String message, String gwMessage) {
        ObjResponse payTokenResponse = new ObjResponse();
        // payment error, void completed transaction
        if (!chargeId.equals("")) {
          //  voidStripeTransaction("", fleetId, bookId, secretKey, chargeId);
        }

        // add transaction pay with Stripe
        Map<String, String> mapResponse = new HashMap<>();
        logger.debug(requestId + " - response: " + message);
        //mapResponse.put("response", message);
        mapResponse.put("message", gwMessage);
        payTokenResponse.returnCode = code;
        payTokenResponse.response = mapResponse;
        return payTokenResponse.toString();
    }
    private String getError(Exception ex) {
        return GatewayUtil.getError(ex);
    }
}
