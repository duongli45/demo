
package com.qupworld.paymentgateway.controllers.GatewayUtil.PayUSDK;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class ShippingAddress_ {

    public String street1;
    public String street2;
    public String city;
    public String state;
    public String country;
    public String postalCode;
    public String phone;

}
