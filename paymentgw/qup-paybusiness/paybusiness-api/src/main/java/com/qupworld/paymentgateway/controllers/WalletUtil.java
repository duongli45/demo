package com.qupworld.paymentgateway.controllers;

import com.google.gson.Gson;
import com.qupworld.paymentgateway.controllers.GatewayUtil.GatewayUtil;
import com.qupworld.paymentgateway.entities.WalletTransaction;
import com.qupworld.paymentgateway.entities.WalletTransfer;
import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.MongoDaoImpl;
import com.qupworld.paymentgateway.models.SQLDao;
import com.qupworld.paymentgateway.models.SQLDaoImpl;
import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.account.Account;
import com.qupworld.paymentgateway.models.mongo.collections.wallet.DriverWallet;
import com.pg.util.TimezoneUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
public class WalletUtil {

    private Gson gson;
    public SQLDao sqlDao;
    public MongoDao mongoDao;
    private String requestId;
    private final Logger logger = LogManager.getLogger(WalletUtil.class);

    public WalletUtil(String requestId){
        gson = new Gson();
        sqlDao = new SQLDaoImpl();
        mongoDao = new MongoDaoImpl();
        this.requestId = requestId;
    }

    public double getCurrentBalance(String userId, String type, String currencyISO) throws ParseException {
        // check new wallet
        boolean update = false;
        DriverWallet driverWallet = mongoDao.getDriverWallet(userId);
        if (driverWallet == null) {
            update = true;
        } else {
            // driverWallet not null, check credit wallet
            if (driverWallet.creditWallet == null) {
                update = true;
            } else {
                // credit wallet not null, check currencyISO
                String currentCurrency = driverWallet.creditWallet.currencyISO != null ? driverWallet.creditWallet.currencyISO : "";
                if (currentCurrency.isEmpty()) {
                    update = true;
                }
            }
        }
        if (update && !currencyISO.isEmpty()) {
            Account account = mongoDao.getAccount(userId);
            if (account.driverInfo != null && account.driverInfo.creditBalances!= null && account.driverInfo.creditBalances.size() > 0) {
                for (AmountByCurrency creditBalance : account.driverInfo.creditBalances) {
                    if (creditBalance.currencyISO != null && creditBalance.currencyISO.equals(currencyISO)) {
                        if(driverWallet == null){
                            //add new driver wallet
                            driverWallet = new DriverWallet();
                            driverWallet.fleetId = account.fleetId;
                            driverWallet.userId = account.userId;
                            driverWallet.creditWallet = creditBalance;
                            mongoDao.addDriverWallet(driverWallet);
                        } else if(driverWallet.creditWallet == null) {
                            //driver wallet exists - update credit wallet
                            mongoDao.updateDriverWallet(account.userId, creditBalance);
                        }

                        WalletTransfer deposit = new WalletTransfer();
                        deposit.fleetId = account.fleetId;
                        deposit.driverId = driverWallet.userId;
                        deposit.amount = creditBalance.value;
                        deposit.currencyISO = currencyISO;
                        deposit.referenceId = "initial-" + GatewayUtil.getOrderId();
                        WalletUtil walletUtil = new WalletUtil(requestId);
                        walletUtil.updateCreditWallet(deposit, true);
                    }
                }
            } else {
                // new driver - did not initiated credit wallet
                // generate wallet with balance = 0 for both Account.DriverInfo and DriverWallet
                AmountByCurrency balance = new AmountByCurrency();
                balance.value = 0.0;
                balance.currencyISO = currencyISO;
                List<AmountByCurrency> creditBalances = new ArrayList<>();
                creditBalances.add(balance);

                //add new driver wallet
                driverWallet = new DriverWallet();
                driverWallet.fleetId = account.fleetId;
                driverWallet.userId = account.userId;
                driverWallet.creditWallet = balance;
                mongoDao.addDriverWallet(driverWallet);

                // update new balance
                mongoDao.updateCreditWallet(account.userId, creditBalances, balance);
            }
        }
        BigDecimal driverBalance = sqlDao.getWalletBalance(requestId, userId, type, currencyISO);
        logger.debug(requestId + " - " + type +" balance of user "+ userId+ " from DB: " + driverBalance.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
        return driverBalance.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    void updateCreditWallet(WalletTransfer walletTransfer, boolean isDeposit) {
        WalletTransaction walletTransaction = new WalletTransaction();
        walletTransaction.fleetId = walletTransfer.fleetId;
        walletTransaction.userId = walletTransfer.driverId;
        walletTransaction.type = "credit";
        walletTransaction.transactionId = walletTransfer.transactionId;
        walletTransaction.description = walletTransfer.transferType;
        if (isDeposit) {
            walletTransaction.cashIn = walletTransfer.amount;
            walletTransaction.cashOut = 0.0;
        } else {
            walletTransaction.cashIn = 0.0;
            walletTransaction.cashOut = walletTransfer.amount;
        }
        walletTransaction.currencyISO = walletTransfer.currencyISO;
        walletTransaction.createdDate = TimezoneUtil.getGMTTimestamp();

        doAddDB(walletTransaction);
    }

    public void updateCashWallet(WalletTransfer walletTransfer, boolean isDeposit) {
        WalletTransaction walletTransaction = new WalletTransaction();
        walletTransaction.fleetId = walletTransfer.fleetId;
        walletTransaction.userId = walletTransfer.driverId;
        walletTransaction.type = "cash";
        walletTransaction.transactionId = walletTransfer.transactionId;
        walletTransaction.description = walletTransfer.transferType;
        if (isDeposit) {
            walletTransaction.cashIn = walletTransfer.amount;
            walletTransaction.cashOut = 0.0;
        } else {
            walletTransaction.cashIn = 0.0;
            walletTransaction.cashOut = walletTransfer.amount;
        }
        walletTransaction.currencyISO = walletTransfer.currencyISO;
        walletTransaction.createdDate = TimezoneUtil.getGMTTimestamp();

        doAddDB(walletTransaction);
    }

    private void doAddDB(WalletTransaction walletTransaction) {
        if (walletTransaction.cashIn > 0 || walletTransaction.cashOut > 0) {
            logger.debug(requestId + " - doAddDB: " + gson.toJson(walletTransaction));
            long addIncome = sqlDao.addWalletTransaction(walletTransaction);
            if (addIncome == 0) {
                // something went wrong, try to add again
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException ignore) {
                }
                addIncome = sqlDao.addWalletTransaction(walletTransaction);
                if (addIncome == 0) {
                    logger.debug(requestId + " - add WalletTransaction error!!!");
                    // send log to email for checking later
                    GatewayUtil.sendMailWalletTransaction(requestId, gson.toJson(walletTransaction));
                }
            }
        }
    }

}
