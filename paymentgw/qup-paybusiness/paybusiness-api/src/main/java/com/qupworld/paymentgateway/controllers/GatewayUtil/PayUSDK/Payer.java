
package com.qupworld.paymentgateway.controllers.GatewayUtil.PayUSDK;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Payer {

    public String merchantPayerId;
    public String fullName;
    public String emailAddress;
    public String birthdate;
    public String contactPhone;
    public String dniNumber;
    public BillingAddress billingAddress;

}
