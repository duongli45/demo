
package com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AuthorizeRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AuthorizeRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BillingDetails" type="{http://schemas.firstatlanticcommerce.com/gateway/data}BillingDetails" minOccurs="0"/>
 *         &lt;element name="CardDetails" type="{http://schemas.firstatlanticcommerce.com/gateway/data}CardDetails"/>
 *         &lt;element name="RecurringDetails" type="{http://schemas.firstatlanticcommerce.com/gateway/data}RecurringDetails" minOccurs="0"/>
 *         &lt;element name="ThreeDSecureDetails" type="{http://schemas.firstatlanticcommerce.com/gateway/data}ThreeDSecureDetails" minOccurs="0"/>
 *         &lt;element name="TransactionDetails" type="{http://schemas.firstatlanticcommerce.com/gateway/data}TransactionDetails"/>
 *         &lt;element name="ShippingDetails" type="{http://schemas.firstatlanticcommerce.com/gateway/data}ShippingDetails" minOccurs="0"/>
 *         &lt;element name="FraudDetails" type="{http://schemas.firstatlanticcommerce.com/gateway/data}FraudDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AuthorizeRequest", propOrder = {
    "billingDetails",
    "cardDetails",
    "recurringDetails",
    "threeDSecureDetails",
    "transactionDetails",
    "shippingDetails",
    "fraudDetails"
})
public class AuthorizeRequest {

    @XmlElement(name = "BillingDetails", nillable = true)
    protected BillingDetails billingDetails;
    @XmlElement(name = "CardDetails", required = true, nillable = true)
    protected CardDetails cardDetails;
    @XmlElement(name = "RecurringDetails", nillable = true)
    protected RecurringDetails recurringDetails;
    @XmlElement(name = "ThreeDSecureDetails", nillable = true)
    protected ThreeDSecureDetails threeDSecureDetails;
    @XmlElement(name = "TransactionDetails", required = true, nillable = true)
    protected TransactionDetails transactionDetails;
    @XmlElement(name = "ShippingDetails", nillable = true)
    protected ShippingDetails shippingDetails;
    @XmlElement(name = "FraudDetails", nillable = true)
    protected FraudDetails fraudDetails;

    /**
     * Gets the value of the billingDetails property.
     * 
     * @return
     *     possible object is
     *     {@link BillingDetails }
     *     
     */
    public BillingDetails getBillingDetails() {
        return billingDetails;
    }

    /**
     * Sets the value of the billingDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingDetails }
     *     
     */
    public void setBillingDetails(BillingDetails value) {
        this.billingDetails = value;
    }

    /**
     * Gets the value of the cardDetails property.
     * 
     * @return
     *     possible object is
     *     {@link CardDetails }
     *     
     */
    public CardDetails getCardDetails() {
        return cardDetails;
    }

    /**
     * Sets the value of the cardDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardDetails }
     *     
     */
    public void setCardDetails(CardDetails value) {
        this.cardDetails = value;
    }

    /**
     * Gets the value of the recurringDetails property.
     * 
     * @return
     *     possible object is
     *     {@link RecurringDetails }
     *     
     */
    public RecurringDetails getRecurringDetails() {
        return recurringDetails;
    }

    /**
     * Sets the value of the recurringDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link RecurringDetails }
     *     
     */
    public void setRecurringDetails(RecurringDetails value) {
        this.recurringDetails = value;
    }

    /**
     * Gets the value of the threeDSecureDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ThreeDSecureDetails }
     *     
     */
    public ThreeDSecureDetails getThreeDSecureDetails() {
        return threeDSecureDetails;
    }

    /**
     * Sets the value of the threeDSecureDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ThreeDSecureDetails }
     *     
     */
    public void setThreeDSecureDetails(ThreeDSecureDetails value) {
        this.threeDSecureDetails = value;
    }

    /**
     * Gets the value of the transactionDetails property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionDetails }
     *     
     */
    public TransactionDetails getTransactionDetails() {
        return transactionDetails;
    }

    /**
     * Sets the value of the transactionDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionDetails }
     *     
     */
    public void setTransactionDetails(TransactionDetails value) {
        this.transactionDetails = value;
    }

    /**
     * Gets the value of the shippingDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ShippingDetails }
     *     
     */
    public ShippingDetails getShippingDetails() {
        return shippingDetails;
    }

    /**
     * Sets the value of the shippingDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShippingDetails }
     *     
     */
    public void setShippingDetails(ShippingDetails value) {
        this.shippingDetails = value;
    }

    /**
     * Gets the value of the fraudDetails property.
     * 
     * @return
     *     possible object is
     *     {@link FraudDetails }
     *     
     */
    public FraudDetails getFraudDetails() {
        return fraudDetails;
    }

    /**
     * Sets the value of the fraudDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link FraudDetails }
     *     
     */
    public void setFraudDetails(FraudDetails value) {
        this.fraudDetails = value;
    }

}
