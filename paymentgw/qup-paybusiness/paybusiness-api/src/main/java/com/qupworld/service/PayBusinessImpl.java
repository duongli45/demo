package com.qupworld.service;

import com.google.gson.Gson;
import com.qupworld.paymentgateway.controllers.CreditCardCtr;
import com.qupworld.paymentgateway.controllers.PaymentCtr;
import com.qupworld.paymentgateway.entities.*;
import com.qupworld.paymentgateway.entities.FawryFormEnt.*;
import com.qupworld.paymentgateway.entities.TopupAPI.*;
import com.qupworld.paymentgateway.entities.TopupEnt;
import com.qupworld.paymentgateway.entities.Vipps.VippsNotifyEnt;
import com.qupworld.paymentgateway.models.RedisDao;
import com.qupworld.paymentgateway.models.RedisImpl;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.stripe.model.Event;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;

/**
 * Created by qup on 09/05/2018.
 */
public class PayBusinessImpl implements PayBusiness{
    private final static Logger logger = LogManager.getLogger(PayBusinessImpl.class);
    private Gson gson = new Gson();

    private PaymentCtr paymentCtr = new PaymentCtr();
    private CreditCardCtr creditCardCtr = new CreditCardCtr();

    public PayBusinessImpl(){

    }

    @Override
    public String getVersion() {
        return "4.6.7103";
    }

    @Override
    public String cronjobWebhookStripe() {
        return paymentCtr.trackWebhookStripe();
    }

    @Override
    public String checkWebhookStripe(String bookId, String requestId) {
        return paymentCtr.checkWebhookStripe(bookId,requestId);
    }
    @Override
    public String acceptBooking(HydraBookingEnt hydraBookingEnt) {
        return paymentCtr.supplierAcceptBooking(hydraBookingEnt);
    }

    @Override
    public String hashPmtNotifyRq(FawryEnt pmtNotifyRq, String requestId) {
        return paymentCtr.hashPmtNotifyRq(pmtNotifyRq,requestId);
    }

    @Override
    public String notificationFawry( FawryEnt fawryPayEnt, String requestId) {
        return paymentCtr.notificationFawry(fawryPayEnt, requestId);
    }
    @Override
    public String addCreditToken(CreditEnt creditForm) {
        return creditCardCtr.createToken(creditForm, "user", "", false);
    }

    @Override
    public String addCreditTokenForCorporate(CreditEnt creditForm) {
        return creditCardCtr.createToken(creditForm, "corporate", "", false);
    }

    @Override
    public String addCreditTokenForCustomer(CreditEnt creditForm) {
        return creditCardCtr.createToken(creditForm, "customer", "", false);
    }

    @Override
    public String addCreditTokenForFleetOwner(CreditEnt creditForm) {
        return creditCardCtr.createToken(creditForm, "fleetOwner", "", false);
    }

    @Override
    public String addSignature(SignatureForm signatureForm) {
        return paymentCtr.addSignature(signatureForm);
    }

    @Override
    public String addTicket(AddTicketEnt addTicketEnt) {
        return paymentCtr.addTicket(addTicketEnt);
    }

    @Override
    public String applePayAsyncPayEase(ApplePayAsyncEnt applePayAsyncEnt) {
        return paymentCtr.applePayAsyncPayEase(applePayAsyncEnt);
    }

    @Override
    public String approveWithdrawal(WalletTransferEnt walletTransferEnt, String action) {
        return paymentCtr.handleWithdrawal(walletTransferEnt, KeysUtil.WITHDRAWAL_APPROVED);
    }

    @Override
    public String cancelBooking(CancelBookingEnt cancelBookingEnt) {
        return paymentCtr.cancelBooking(cancelBookingEnt);
    }

    @Override
    public String cancelPaymentInterWithBookId(String bookId, String requestId) {
        return paymentCtr.cancelPaymentInterWithBookId(bookId,requestId);
    }

    @Override
    public String chargeFleetOwner(ChargeFleetOwner chargeFleetOwner) {
        return paymentCtr.chargeFleetOwner(chargeFleetOwner);
    }

    @Override
    public String chargeIntercityBooking(PaymentEnt paymentEnt) {
        return paymentCtr.chargeIntercityBooking(paymentEnt);
    }

    @Override
    public String checkBBLCorporate(String fleetId, String corporateId) {
        return paymentCtr.checkBBLCorporate(fleetId, corporateId);
    }

    @Override
    public String checkStripeACHToken(ACHEnt achEnt) {
        return paymentCtr.checkACHToken(achEnt);
    }

    @Override
    public String checkUnionpayCard(String fleetId, String cardNumber) {
        return creditCardCtr.checkUnionpayCard(fleetId, cardNumber);
    }

    @Override
    public String clearCustomerOutstanding(OutstandingEnt outstandingEnt) {
        return paymentCtr.clearCustomerOutstanding(outstandingEnt);
    }

    @Override
    public String completeBookingWithoutCharging(PaymentEnt paymentEnt) {
        return paymentCtr.completeOfflinePayment(paymentEnt);
    }

    @Override
    public String completedWithoutPayment(IncidentEnt incidentEnt) {
        return paymentCtr.completeIncident(incidentEnt);
    }

    @Override
    public String completeIntercityTrip(IntercityTripCompleteEnt completeEnt) {
        return paymentCtr.completeIntercityTrip(completeEnt);
    }

    @Override
    public String confirmPreAuth(String paymentInterId, String requestId) {
        return paymentCtr.confirmPreAuth(paymentInterId,requestId, "");
    }

    @Override
    public String confirmStripeData(String bookId) {
        return paymentCtr.confirmStripeData(bookId);
    }

    @Override
    public String createACHToken(ACHEnt achEnt, String type) {
        return paymentCtr.manageBankProfile(achEnt, "add");
    }

    @Override
    public String createCreditTokenForDriver(CreditEnt creditForm) {
        return creditCardCtr.createToken(creditForm, "user", "", false);
    }

    @Override
    public String createWebhookStripe(WebhookCreateEnt webhookCreateEnt) {
        return paymentCtr.createWebhook(webhookCreateEnt);
    }

    @Override
    public String deleteCorporateToken(CreditDeleteEnt creditDeleteEnt) {
        return creditCardCtr.deleteCreditCorporate(creditDeleteEnt);
    }

    @Override
    public String deleteCreditToken(CreditDeleteEnt creditDeleteEnt) {
        return creditCardCtr.deleteToken(creditDeleteEnt);
    }

    @Override
    public String deleteCreditTokenForFleetOwner(CreditDeleteEnt creditDeleteEnt) {
        return creditCardCtr.deleteCreditTokenForFleetOwner(creditDeleteEnt);
    }

    @Override
    public String generateBillingReport(String requestId, int month, int year){
        return paymentCtr.generateBillingReport(requestId, month, year);
    }

    @Override
    public String getClientSecret(ClientSecretEnt clientSecretEnt) {
        return creditCardCtr.getClientSecret(clientSecretEnt);
    }

    @Override
    public String getCreditBalances(CreditBalancesEnt creditBalancesEnt) {
        return paymentCtr.getCreditBalances(creditBalancesEnt);
    }

    @Override
    public String getEncrypted(String data) {
        return paymentCtr.getEncrypted(data);
    }

    @Override
    public String getFileId(String requestId, String fleetId, String gateway, String fileUrl) {
        return paymentCtr.getFileId(requestId, fleetId,gateway,fileUrl);
    }

    @Override
    public String getFirstPayDetail(String requestId, String bookId) {
        return paymentCtr.getFirstPayDetail(requestId, bookId);
    }

    @Override
    public String getOutstandingAmount(OutstandingEnt outstandingEnt) {
        return paymentCtr.getOutstandingAmount(outstandingEnt);
    }

    @Override
    public String getOutstandingTicket(OutstandingEnt outstandingEnt) {
        return paymentCtr.getOutstandingTicket(outstandingEnt);
    }

    @Override
    public String getPaxWallet(UserWalletEnt userWalletEnt) {
        return paymentCtr.getPaxWallet(userWalletEnt);
    }

    @Override
    public String getRequiredFields(ACHEnt achEnt) {
        return paymentCtr.getRequiredFields(achEnt);
    }

    @Override
    public String getReferralEarning(ReferralTransactionEnt referralTransactionEnt) {
        return paymentCtr.getReferralEarning(referralTransactionEnt);
    }

    @Override
    public String getTicket(AddTicketEnt addTicketEnt) {
        return paymentCtr.getTicket(addTicketEnt);
    }

    @Override
    public String getWalletInfo(UserWalletEnt userWalletEnt) {
        return paymentCtr.getWalletInfo(userWalletEnt);
    }

    @Override
    public String getWebhookStripe(WebhookCreateEnt webhookCreateEnt) {
        return paymentCtr.getWebhookStripe(webhookCreateEnt);
    }


    @Override
    public String getWithdrawal(WalletWithdrawalEnt walletWithdrawalEnt) {
        return paymentCtr.getWithdrawal(walletWithdrawalEnt);
    }

    @Override
    public String markAsChargeBillingReport(FleetSubscriptionEnt fleetSubscriptionEnt) {
        return paymentCtr.markAsChargeBillingReport(fleetSubscriptionEnt);
    }

    @Override
    public String migrateTicket(MigrateEnt migrateEnt) {
        return paymentCtr.migrateTicket(migrateEnt);
    }

    @Override
    public String noShow3rd(CancelBookingEnt cancelBookingEnt) {
        return paymentCtr.noShow3rd(cancelBookingEnt);
    }

    @Override
    public String payByApplePay(PaymentEnt paymentEnt) {
        return paymentCtr.payByApplePay(paymentEnt);
    }

    @Override
    public String payByCash(PaymentEnt paymentEnt) {
        return paymentCtr.completeOfflinePayment(paymentEnt);
    }

    @Override
    public String payByCreditExternal(PaymentEnt paymentEnt) {
        return paymentCtr.completeOfflinePayment(paymentEnt);
    }

    @Override
    public String payByCreditToken(PaymentEnt paymentEnt) {
        return paymentCtr.payByToken(paymentEnt);
    }

    @Override
    public String payByDirectBilling(PaymentEnt paymentEnt) {
        return paymentCtr.completeOfflinePayment(paymentEnt);
    }

    @Override
    public String payByFleetCard(PaymentEnt paymentEnt) {
        return paymentCtr.completeOfflinePayment(paymentEnt);
    }

    @Override
    public String payByInputCard(PaymentEnt paymentEnt) {
        return paymentCtr.payByInputCard(paymentEnt);
    }

    @Override
    public String payByPaxWallet(PaymentEnt paymentEnt) {
        return paymentCtr.completeOfflinePayment(paymentEnt);
    }

    @Override
    public String payByPrePaid(PaymentEnt paymentEnt) {
        return paymentCtr.completeOfflinePayment(paymentEnt);
    }

    @Override
    public String payByWallet(PaymentEnt paymentEnt) {
        return paymentCtr.payByWallet(paymentEnt);
    }

    @Override
    public String payfortDirectTransaction(PayfortResponseEnt payfortResponseEnt,String requestId ) {
        String responseStr = "";
        if (payfortResponseEnt.command.toUpperCase().equals("AUTHORIZATION"))
            responseStr = creditCardCtr.completeFirstTransaction3DS(requestId, gson.toJson(payfortResponseEnt), KeysUtil.PAYFORT);
        else if (payfortResponseEnt.command.toUpperCase().equals("PURCHASE")) {
            responseStr = paymentCtr.completePayfort3DSTransaction(gson.toJson(payfortResponseEnt), KeysUtil.PAYFORT, requestId);
        }
        return responseStr;
    }

    @Override
    public String payFrom3rd(PaymentEnt paymentEnt) {
        return paymentCtr.payFrom3rd(paymentEnt);
    }

    @Override
    public String payFromCommandCenter(PaymentEnt paymentEnt) {
        return paymentCtr.payFromCommandCenter(paymentEnt);
    }

    @Override
    public String paymentAudit(PaymentAudit paymentAudit) {
        return paymentCtr.paymentAudit(paymentAudit);
    }

    @Override
    public String paymentDetail(String bookId, String requestId) {
        return paymentCtr.paymentDetail(bookId, requestId);
    }

    @Override
    public String paymentGCashNotificationURL(TnGNotification tnGNotification) {
        return paymentCtr.paymentGCashNotificationURL(tnGNotification);
    }

    @Override
    public String paymentMolPayNotificationURL(PaymentNotificationEnt paymentNotificationEnt) {
        return paymentCtr.paymentMolPayNotificationURL(paymentNotificationEnt);
    }

    @Override
    public String paymentNotification(PaymentNotificationEnt paymentNotificationEnt) {
        // detect gateway
        String gateway = paymentNotificationEnt.gateway != null ? paymentNotificationEnt.gateway : "";
        if (gateway.isEmpty()) {
            String response = paymentNotificationEnt.Response != null ? paymentNotificationEnt.Response : "";
            if (!response.isEmpty())
                gateway = KeysUtil.FAC;
        }
        if (gateway.isEmpty()) {
            String fort_id = paymentNotificationEnt.fort_id != null ? paymentNotificationEnt.fort_id : "";
            if (!fort_id.isEmpty())
                gateway = KeysUtil.PAYFORT;
        }
        if (gateway.isEmpty()) {
            String tranID = paymentNotificationEnt.tranID != null ? paymentNotificationEnt.tranID : "";
            if (!tranID.isEmpty())
                gateway = KeysUtil.MOLPAY;
        }

        if (gateway.isEmpty()) {
            String gCashMerchantTransId = paymentNotificationEnt.gCashMerchantTransId != null ? paymentNotificationEnt.gCashMerchantTransId : "";
            if (!gCashMerchantTransId.isEmpty())
                gateway = KeysUtil.GCASH;
        }

        if (gateway.isEmpty()) {
            String PaRes = paymentNotificationEnt.PaRes != null ? paymentNotificationEnt.PaRes : "";
            if (!PaRes.isEmpty())
                gateway = KeysUtil.FIRSTDATA;
        }

        if (gateway.isEmpty()) {
            String ServiceID = paymentNotificationEnt.ServiceID != null ? paymentNotificationEnt.ServiceID : "";
            if (!ServiceID.isEmpty())
                gateway = KeysUtil.EGHL;
        }
        logger.debug(paymentNotificationEnt.requestId + " - gateway: " + gateway);
        if (gateway.equals(KeysUtil.TSYS)) {
            // register card with TSYS
            return creditCardCtr.paymentTSYSNotificationURL(paymentNotificationEnt);
        }
        if (gateway.equals(KeysUtil.PAYMAYA)) {
            // register card with PayMaya
            return creditCardCtr.paymentPayMayaNotificationURL(paymentNotificationEnt);
        }
        if (gateway.equals(KeysUtil.CXPAY)) {
            // check if calling from caching API
            String urlAction = paymentNotificationEnt.urlAction != null ? paymentNotificationEnt.urlAction : "";
            if (urlAction.equals("cache")) {
                return creditCardCtr.cacheCXPayData(paymentNotificationEnt);
            }
            // register card with CXPay
            return creditCardCtr.paymentCXPayNotificationURL(paymentNotificationEnt);
        }
        if (gateway.equals(KeysUtil.BOG)) {
            String orderId = paymentNotificationEnt.order_id != null ? paymentNotificationEnt.order_id : "";
            RedisDao redisDao = new RedisImpl();
            String type = redisDao.getTmp(paymentNotificationEnt.requestId, KeysUtil.BOG+orderId+"type");
            logger.debug(paymentNotificationEnt.requestId + " - type: " + type);
            if (type != null && type.equals("Register")) {
                return creditCardCtr.paymentBOGNotificationURL(paymentNotificationEnt);
            }
        }
        if (gateway.equals(KeysUtil.MADA)) {
            // check action if notification for register card or payment
            String urlAction = paymentNotificationEnt.urlAction != null ? paymentNotificationEnt.urlAction : "";
            if (urlAction.equals("token")) {
                return creditCardCtr.paymentMADANotificationURL(paymentNotificationEnt);
            }
        }
        if (gateway.equals(KeysUtil.FAC)) {
            String gwOrderid = paymentNotificationEnt.gwOrderid != null ? paymentNotificationEnt.gwOrderid : "";
            if (gwOrderid.contains("REGISTER")) {
                creditCardCtr.paymentFACNotificationURL(paymentNotificationEnt);
                try {
                    Thread.sleep(3000);
                }catch (InterruptedException ignore){}
                return returnWebContent(paymentNotificationEnt.requestId, KeysUtil.FAC, paymentNotificationEnt.urlAction);
            }
        }
        if (gateway.equals(KeysUtil.PAYWAY)) {
            // check action if notification for register card or payment
            String urlAction = paymentNotificationEnt.urlAction != null ? paymentNotificationEnt.urlAction : "";
            if (urlAction.equals("token")) {
                return creditCardCtr.paymentPayWayNotificationURL(paymentNotificationEnt);
            } else if (urlAction.equals("success")) {
                try {
                    Thread.sleep(3000);
                }catch (InterruptedException ignore){}
                return returnWebContent(paymentNotificationEnt.requestId, KeysUtil.PAYWAY, paymentNotificationEnt.urlAction);
            }
        }
        if (gateway.equals(KeysUtil.ECPAY)) {
            // check action if notification for register card or payment
            String urlAction = paymentNotificationEnt.urlAction != null ? paymentNotificationEnt.urlAction : "";
            if (urlAction.equals("token")) {
                return creditCardCtr.paymentECPayNotificationURL(paymentNotificationEnt);
            } else if (urlAction.equals("backToStore") || urlAction.equals("registerCompleted")) {
                return returnWebContent(paymentNotificationEnt.requestId, KeysUtil.ECPAY, paymentNotificationEnt.urlAction);
            }
        }
        if (gateway.equals("CACHE")) {
            return creditCardCtr.cacheBillingData(paymentNotificationEnt);
        }
        return paymentCtr.paymentNotification(paymentNotificationEnt);
    }

    @Override
    public String paymentPingPongNotificationURL(PaymentNotificationEnt paymentNotificationEnt) {
        return paymentCtr.paymentPingPongNotificationURL(paymentNotificationEnt);
    }

    @Override
    public String paymentSenangpayNotificationURL(PaymentNotificationEnt paymentNotificationEnt) {
        return creditCardCtr.paymentSenangpayNotificationURL(paymentNotificationEnt);
    }

    @Override
    public String paymentTnGNotificationURL(TnGNotification tnGNotification) {
        return paymentCtr.paymentTnGNotificationURL(tnGNotification);
    }

    @Override
    public String payOutstanding(OutstandingEnt outstandingEnt) {
        return paymentCtr.payOutstanding(outstandingEnt);
    }

    @Override
    public String payToDriver(PayToDriverEnt payToDriverEnt) {
        return paymentCtr.payToDriver(payToDriverEnt);
    }

    @Override
    public String preAuthPayment(PreAuthEnt preAuthEnt) {
        return paymentCtr.preAuthPayment(preAuthEnt);
    }

    @Override
    public String preChargeBooking(HydraBookingEnt hydraBookingEnt) {
        return paymentCtr.preChargeBooking(hydraBookingEnt);
    }

    @Override
    public String rejectWithdrawal(WalletTransferEnt walletTransferEnt, String action) {
        return paymentCtr.handleWithdrawal(walletTransferEnt, KeysUtil.WITHDRAWAL_REJECTED);
    }

    @Override
    public String removeBankInfo(ACHEnt achEnt) {
        return paymentCtr.removeBankInfo(achEnt);
    }

    @Override
    public String retryPayment(PaymentEnt paymentEnt) {
        return paymentCtr.retryPayment(paymentEnt);
    }

    @Override
    public String setDefaultCardForFleetOwner(CreditDeleteEnt creditDeleteEnt) {
        return creditCardCtr.setDefaultCardForFleetOwner(creditDeleteEnt);
    }

    @Override
    public String settlementMolPayNotificationURL(SettlementNotificationEnt settlementNotificationEnt) {
        return paymentCtr.settlementMolPayNotificationURL(settlementNotificationEnt);
    }

    @Override
    public String storePaymentData(PaymentEnt paymentEnt) {
        return paymentCtr.storePaymentData(paymentEnt);
    }

    @Override
    public String switchToBBLCorporate(String fleetId, String userId, String requestId) {
        return paymentCtr.updateCorporateInfo(fleetId, userId, requestId);
    }

    @Override
    public String synWebhookStripe(WebhookCreateEnt webhookCreateEnt) {
        return paymentCtr.synWebhookStripe(webhookCreateEnt);
    }

    @Override
    public String ticketDetail(String requestId, String bookId, String appType) {
        return paymentCtr.ticketDetail(requestId, bookId, appType);
    }

    @Override
    public String tipAfterPayment(TipDriverEnt tipDriverEnt) {
        return paymentCtr.tipAfterPayment(tipDriverEnt);
    }

    @Override
    public String topupByWallet(TopupByWalletEnt topupByWalletEnt) {
        return paymentCtr.topupByWallet(topupByWalletEnt);
    }

    @Override
    public String topupDriverBalance(TopupDriverBalanceEnt topupDriverBalanceEnt) {
        return paymentCtr.topupDriverBalance(topupDriverBalanceEnt);
    }

    @Override
    public String topupDriverBalanceFromCC(TopupDriverBalanceEnt topupDriverBalanceEnt) {
        return paymentCtr.topupDriverBalance(topupDriverBalanceEnt);
    }

    @Override
    public String topUpPaxWallet(TopupPaxWalletEnt topupPaxWalletEnt) {
        return paymentCtr.topUpPaxWallet(topupPaxWalletEnt);
    }

    @Override
    public String topupPrepaid(TopupEnt topupEnt) {
        return paymentCtr.topupPrepaid(topupEnt);
    }

    @Override
    public String topUpRefereeWallet(TopupPaxWalletEnt topupEnt) {
        return paymentCtr.topUpRefereeWallet(topupEnt);
    }

    @Override
    public String unlockBooking(CancelBookingEnt cancelBookingEnt) {
        return paymentCtr.unlockBooking(cancelBookingEnt);
    }

    @Override
    public String updateBankProfile(ACHEnt achEnt, String type) {
        return paymentCtr.manageBankProfile(achEnt, "update");
    }

    @Override
    public String updateBillingReport(FleetSubscriptionEnt subscriptionEnt) {
        return paymentCtr.updateBillingReport(subscriptionEnt);
    }

    @Override
    public String updateCompletedBooking(UpdateEnt updateEnt) {
        return paymentCtr.updateCompletedBooking(updateEnt);
    }

    @Override
    public String updateDriverBalance(UpdateBalanceEnt updateBalanceEnt) {
        return paymentCtr.updateDriverBalance(updateBalanceEnt);
    }

    @Override
    public String updatePaymentStatus(PaymentEnt paymentEnt) {
        return paymentCtr.updatePaymentStatus(paymentEnt);
    }

    @Override
    public String voidPreAuthBeforePayment(VoidEnt voidEnt) {
        return paymentCtr.voidPreAuthBeforePayment(voidEnt);
    }

    @Override
    public String webhookStripe(String eventstr, String requestId) {
        try {
            Event event = gson.fromJson(eventstr, Event.class);
            // Handle the event
            List<String> setupIntentHooks = Arrays.asList("setup_intent.succeeded", "setup_intent.setup_failed", "setup_intent.requires_action", "setup_intent.created", "setup_intent.canceled");
            if(setupIntentHooks.contains(event.getType())) {
                return creditCardCtr.webhookStripe(event,requestId);
            } else {
                return paymentCtr.webhookStripe(event,requestId);
            }


        } catch (Exception ex) {
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            String strError = gson.toJson(errors.toString());

            ObjResponse error = new ObjResponse();
            error.returnCode = 306;
            error.message = strError;
            return error.toString();
        }
    }

    @Override
    public String withdrawalRequest(WalletTransferEnt walletTransferEnt) {
        return paymentCtr.withdrawalRequest(walletTransferEnt);
    }

    @Override
    public String updateDriverCashBalance(WalletTransferEnt walletTransferEnt) {
        return paymentCtr.updateDriverCashBalance(walletTransferEnt);
    }

    @Override
    public String driverTopupPax(DriverTopupPaxEnt driverTopupPaxEnt){
        // init default value
        JSONObject objPayment = new JSONObject();
        objPayment.put("bookId", "");
        objPayment.put("bookingFare", 0.0);
        objPayment.put("givenAmount", 0.0);
        return paymentCtr.driverTopupPax(driverTopupPaxEnt, objPayment.toJSONString(), "api");
    }

    @Override
    public String resetCashWallet(UpdateBalanceEnt updateBalanceEnt) {
        return paymentCtr.resetCashWallet(updateBalanceEnt);
    }

    @Override
    public String syncReferralTransaction(SyncEnt syncEnt) {
        return paymentCtr.syncReferralTransaction(syncEnt);
    }

    @Override
    public String resetCreditBalance(SyncEnt syncEnt) {
        return paymentCtr.resetCreditBalance(syncEnt);
    }

    @Override
    public String checkPendingPayment(PaymentChecking paymentChecking) {
        return paymentCtr.checkPendingPayment(paymentChecking);
    }

    @Override
    public String getPayoutDriverReport(PayoutReportEnt payoutReportEnt) {
        return paymentCtr.getPayoutDriverReport(payoutReportEnt);
    }

    @Override
    public String payoutToDriver(PayoutEnt payoutEnt) {
        return paymentCtr.payoutToDriver(payoutEnt);
    }


    @Override
    public String prePaidByWallet(PrePaidByWalletEnt prePaidEnt) {
        return paymentCtr.prePaidByWallet(prePaidEnt);
    }

    @Override
    public String cancelWalletBooking(CancelBookingEnt cancelBookingEnt) {
        return paymentCtr.cancelWalletBooking(cancelBookingEnt);
    }

    @Override
    public String removeCacheData(ClearCacheEnt clearCacheEnt) {
        return paymentCtr.removeCacheData(clearCacheEnt);
    }

    @Override
    public String resetExpiredBonus(SyncEnt syncEnt) {
        return paymentCtr.resetExpiredBonus(syncEnt);
    }

    @Override
    public String topUpTransportDriver(SyncEnt syncEnt) {
        return paymentCtr.topUpTransportDriver(syncEnt);
    }

    @Override
    public String updateInvalidPayout(PayoutReportEnt payoutReportEnt) {
        return paymentCtr.updateInvalidPayout(payoutReportEnt);
    }

    @Override
    public String redeemToPaxWallet(RedeemVoucherEnt redeemVoucherEnt) {
        return paymentCtr.redeemToPaxWallet(redeemVoucherEnt);
    }

    @Override
    public String topupDriverCredit(SyncEnt syncEnt) {
        return paymentCtr.topupDriverCredit(syncEnt);
    }

    @Override
    public String paymentVippsNotificationURL(VippsNotifyEnt vippsNotifyEnt) {
        return paymentCtr.paymentVippsNotificationURL(vippsNotifyEnt);
    }

    @Override
    public String paymentDNBNotificationURL(PaymentNotificationEnt notificationEnt) {
        return paymentCtr.paymentDNBNotificationURL(notificationEnt);
    }

    @Override
    public String getPayoutMerchantReport(PayoutReportEnt payoutReportEnt) {
        return paymentCtr.getPayoutMerchantReport(payoutReportEnt);
    }

    @Override
    public String payoutToMerchant(PayoutEnt payoutEnt) {
        return paymentCtr.payoutToMerchant(payoutEnt);
    }

    @Override
    public String getSecureFieldSession(CreditEnt creditEnt) {
        return paymentCtr.getSecureFieldSession(creditEnt);
    }

    @Override
    public String paymentTSYSNotificationURL(PaymentNotificationEnt paymentNotificationEnt) {
        return creditCardCtr.paymentTSYSNotificationURL(paymentNotificationEnt);
    }

    @Override
    public String merchantPendingAmount(PayoutReportEnt payoutReportEnt) {
        return paymentCtr.merchantPendingAmount(payoutReportEnt);
    }

    @Override
    public String getBankRequiredFields(ACHEnt achEnt) {
        return paymentCtr.getBankRequiredFields(achEnt);
    }

    @Override
    public String createBankToken(ACHEnt achEnt) {
        return paymentCtr.manageBankProfile(achEnt, "add");
    }

    @Override
    public String calGrossEarning(List<ETAGrossEnt> etaGrossEnts, String requestId) {
        return paymentCtr.calGrossEarning(etaGrossEnts, requestId);
    }

    @Override
    public String completeBooking(PaymentEnt paymentEnt) {
        return paymentCtr.completeBooking(paymentEnt);
    }

    @Override
    public String updateTransaction(SyncEnt syncEnt) {
        return paymentCtr.updateTransaction(syncEnt);
    }

    @Override
    public String topUpBonusToDriverWallet(SyncEnt syncEnt) {
        return paymentCtr.topUpBonusToDriverWallet(syncEnt);
    }

    @Override
    public String removeExpiredBonus(SyncEnt syncEnt) {
        return paymentCtr.removeExpiredBonus(syncEnt);
    }

    @Override
    public String syncBalance(SyncEnt syncEnt) {
        return paymentCtr.syncBalance(syncEnt);
    }

    @Override
    public String completeStreetSharingBooking(PaymentEnt paymentEnt) {
        return paymentCtr.completeStreetSharingBooking(paymentEnt);
    }

    @Override
    public String getCheckoutPage(CreditEnt creditEnt) {
        return creditCardCtr.getCheckoutPage(creditEnt);
    }

    @Override
    public String verifyUser(String requestId, VerifyEnt verifyEnt, String auth) {
        return paymentCtr.verifyUser(requestId, verifyEnt, auth);
    }

    @Override
    public String topupByAPI(String requestId, com.qupworld.paymentgateway.entities.TopupAPI.TopupEnt topupEnt, String auth) {
        return paymentCtr.topupByAPI(requestId, topupEnt, auth);
    }

    @Override
    public String registerAuthentication(RegisterAuthEnt registerAuthEnt) {
        return paymentCtr.registerAuthentication(registerAuthEnt);
    }

    @Override
    public String updatePaymentMethod(UpdateEnt updateEnt) {
        return paymentCtr.updatePaymentMethod(updateEnt);
    }

    @Override
    public String syncPaymentData(SyncPaymentEnt syncPaymentEnt) {
        return paymentCtr.syncPaymentData(syncPaymentEnt);
    }

    @Override
    public String resetNegativeBalanceForDriver(SyncEnt syncEnt) {
        return paymentCtr.resetNegativeBalanceForDriver(syncEnt);
    }

    @Override
    public String migrateCreditCard(MigrateCreditEnt migrateCreditEnt) {
        return paymentCtr.migrateCreditCard(migrateCreditEnt);
    }

    @Override
    public String updateMerchantWalletBalance(MerchantWalletTransactionEnt merchantWalletTransactionEnt) {
        return paymentCtr.updateMerchantWalletBalance(merchantWalletTransactionEnt);
    }

    @Override
    public String removeOldCards(MigrateCreditEnt migrateCreditEnt) {
        return paymentCtr.removeOldCards(migrateCreditEnt);
    }

    @Override
    public String driverTopupDriver(DriverTopupDriverEnt driverTopupDriverEnt) {
        return paymentCtr.driverTopupDriver(driverTopupDriverEnt);
    }

    @Override
    public String chargeDriverCancelBooking(CancelBookingEnt cancelBookingEnt) {
        return paymentCtr.chargeDriverCancelBooking(cancelBookingEnt);
    }

    @Override
    public String stripeConnectOnboarding(StripeConnectEnt stripeConnectEnt) {
        return paymentCtr.stripeConnectOnboarding(stripeConnectEnt);
    }

    @Override
    public String stripeConnectLogin(StripeConnectEnt stripeConnectEnt) {
        return paymentCtr.stripeConnectLogin(stripeConnectEnt);
    }

    @Override
    public String disconnectStripe(StripeConnectEnt stripeConnectEnt) {
        return paymentCtr.disconnectStripe(stripeConnectEnt);
    }

    @Override
    public String returnWebContent(String requestId, String gateway, String action) {
        return paymentCtr.returnWebContent(requestId, gateway, action);
    }

    @Override
    public String completePartialPayment(PaymentEnt paymentEnt) {
        return paymentCtr.completePartialPayment(paymentEnt);
    }

    @Override
    public String verifyReservationBooking(PaymentEnt paymentEnt) {
        return paymentCtr.verifyReservationBooking(paymentEnt);
    }

    @Override
    public String completeHydraBooking(PaymentEnt paymentEnt) {
        return paymentCtr.completeHydraBooking(paymentEnt);
    }

    @Override
    public String preAuthForHydraReservation(PreAuthEnt preAuthEnt) {
        return paymentCtr.preAuthForHydraReservation(preAuthEnt);
    }

    @Override
    public String getHydraPendingPayoutReport(HydraPayoutEnt hydraPayoutEnt) {
        return paymentCtr.getHydraPendingPayoutReport(hydraPayoutEnt);
    }

    @Override
    public String getPendingDetailByFleet(HydraPayoutEnt hydraPayoutEnt) {
        return paymentCtr.getPendingDetailByFleet(hydraPayoutEnt);
    }

    @Override
    public String hydraManualPayout(HydraPayoutEnt hydraPayoutEnt) {
        return paymentCtr.hydraManualPayout(hydraPayoutEnt);
    }

    @Override
    public String clearPendingHydra(HydraPendingEnt hydraPendingEnt) {
        return paymentCtr.clearPendingHydra(hydraPendingEnt);
    }

    @Override
    public String getOutstandingHydra(OutstandingEnt outstandingEnt) {
        return paymentCtr.getOutstandingHydra(outstandingEnt);
    }

    @Override
    public String clearPendingHydraForPWA(HydraPendingEnt hydraPendingEnt) {
        return paymentCtr.clearPendingHydraForPWA(hydraPendingEnt);
    }

    @Override
    public String supplierRejectBooking(CancelBookingEnt cancelEnt) {
        return paymentCtr.rejectHydraBooking(cancelEnt);
    }

    @Override
    public String webhookApple(AppleNotifyEnt appleNotifyEnt) {
        return paymentCtr.webhookApple(appleNotifyEnt);
    }

    @Override
    public String checkConnectStatus(StripeConnectEnt stripeConnectEnt) {
        return paymentCtr.checkConnectStatus(stripeConnectEnt);
    }

    @Override
    public String getStripeWalletData(PaymentEnt paymentEnt) {
        return paymentCtr.getStripeWalletData(paymentEnt);
    }

    @Override
    public String getConfigAndConnectionStatus(String requestId) {
        return paymentCtr.getConfigAndConnectionStatus(requestId);
    }

    @Override
    public String getPaymentLink(PaymentEnt paymentEnt) {
        return paymentCtr.getPaymentLink(paymentEnt);
    }

    @Override
    public String getCheckoutSession(InvoicePayEnt invoicePayEnt) {
        return paymentCtr.getCheckoutSession(invoicePayEnt);
    }

    @Override
    public String chargeCustomerForInvoice(InvoicePayEnt invoicePayEnt) {
        return paymentCtr.chargeCustomerForInvoice(invoicePayEnt);
    }

    @Override
    public String getPaymentIntentForInvoice(InvoicePayEnt invoicePayEnt) {
        return paymentCtr.getPaymentIntentForInvoice(invoicePayEnt);
    }

    @Override
    public String getPaymentMethodForInvoice(InvoicePayEnt invoicePayEnt) {
        return paymentCtr.getPaymentMethodForInvoice(invoicePayEnt);
    }

    @Override
    public String prePaidOrPostPaid(PrePaidEnt prePaidEnt) {
        return paymentCtr.prePaidOrPostPaid(prePaidEnt);
    }

    @Override
    public String getPaymentActivities(PrePaidEnt prePaidEnt) {
        return paymentCtr.getPaymentActivities(prePaidEnt);
    }

    @Override
    public String getPaymentLinkForPrepaidOrPostpaid(PrePaidEnt prePaidEnt) {
        return paymentCtr.getPaymentLinkForPrepaidOrPostpaid(prePaidEnt);
    }

    @Override
    public String deactivatePrepaidPaymentLink(PrePaidEnt prePaidEnt) {
        return paymentCtr.deactivatePrepaidPaymentLink(prePaidEnt);
    }

    @Override
    public String deactivatePaymentLinkForBooking(PrePaidEnt prePaidEnt) {
        return paymentCtr.deactivatePaymentLinkForBooking(prePaidEnt);
    }
}
