package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.qupworld.config.ServerConfig;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import okhttp3.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class YenePayUtil {

    final static Logger logger = LogManager.getLogger(YenePayUtil.class);

    private String requestId;
    private String MERCHANT_ID = "";
    private String PDT_TOKEN = "";
    private String REQUEST_URL;
    private final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    public YenePayUtil(String requestId, String merchantId, String pdtToken, String requestUrl) {
        this.requestId = requestId;
        MERCHANT_ID = merchantId;
        PDT_TOKEN = pdtToken;
        REQUEST_URL = requestUrl;
    }

    public String getCheckoutURL(double amount, String orderId, String reason) throws Exception{
        ObjResponse response = new ObjResponse();
        try {
            JSONObject objBody = new JSONObject();

            String returnUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.YENEPAY;
            logger.debug(requestId + " - orderId = " + orderId);
            objBody.put("process", "Express");
            objBody.put("merchantOrderId", orderId);
            objBody.put("merchantId", MERCHANT_ID);
            objBody.put("successUrl", returnUrl+"?urlAction=success");
            objBody.put("cancelUrl", returnUrl+"?urlAction=cancel");
            objBody.put("failureUrl", returnUrl+"?urlAction=failure");
            objBody.put("ipnUrl", returnUrl+"?urlAction=ipn");
            objBody.put("expiresAfter", 30);
            objBody.put("totalItemsDeliveryFee", 0);

            JSONArray arrItem = new JSONArray();
            JSONObject objItem = new JSONObject();
            objItem.put("itemId", GatewayUtil.getOrderId());
            objItem.put("itemName", reason);
            objItem.put("unitPrice", Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(amount)));
            objItem.put("quantity", 1);

            arrItem.add(objItem);
            objBody.put("items", arrItem);

            RequestBody body = RequestBody.create(objBody.toJSONString(), JSON);
            Request request = new Request.Builder()
                    .addHeader("Content-Type", "application/json")
                    .url(REQUEST_URL + "/urlgenerate/getcheckouturl/")
                    .post(body)
                    .build();

            OkHttpClient client = new OkHttpClient.Builder().build();
            Response gwResponse = client.newCall(request).execute();
            String responseString = gwResponse.body() != null ? gwResponse.body().string() : "";
            logger.debug(requestId + " - response = " + responseString);
            /*
            {"result":"https://test.yenepay.com/Home/Process/?Process=Express&CancelUrl=http://eventapis.local.qup.vn/api/webhook/notification/YenePay?urlAction=cancel&SuccessUrl=http://eventapis.local.qup.vn/api/webhook/notification/YenePay?urlAction=success&FailureUrl=http://eventapis.local.qup.vn/api/webhook/notification/YenePay?urlAction=failure&IPNUrl=http://eventapis.local.qup.vn/api/webhook/notification/YenePay?urlAction=ipn&MerchantId=SB1146&MerchantOrderId=20210914025751956&ExpiresAfter=30&ItemId=20210914025751989&ItemName=booking&Quantity=1&UnitPrice=9&DeliveryFee=0"}
            */
            org.json.JSONObject objResponse = new org.json.JSONObject(responseString);
            String result = objResponse.has("result") ? objResponse.getString("result") : "";
            if (!result.isEmpty()) {
                Map<String, String> mapResponse = new HashMap<>();
                mapResponse.put("3ds_url", result);
                mapResponse.put("type", "link");
                response.response = mapResponse;
                response.returnCode = 200;
                return response.toString();
            }
            response.returnCode = 525;
            return response.toString();
        } catch (Exception ex) {
            logger.debug(requestId + " - getCheckoutURL exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response.toString();
        }
    }

    public String queryStatus(String orderId, String transactionId) throws Exception{
        ObjResponse response = new ObjResponse();
        try {
            JSONObject objBody = new JSONObject();
            logger.debug(requestId + " - orderId = " + orderId);
            logger.debug(requestId + " - transactionId = " + transactionId);
            objBody.put("requestType", "PDT");
            objBody.put("pdtToken", PDT_TOKEN);
            objBody.put("transactionId", transactionId);
            objBody.put("merchantOrderId", orderId);

            RequestBody body = RequestBody.create(objBody.toJSONString(), JSON);
            Request request = new Request.Builder()
                    .addHeader("Content-Type", "application/json")
                    .url(REQUEST_URL + "/verify/pdt/")
                    .post(body)
                    .build();

            OkHttpClient client = new OkHttpClient.Builder().build();
            Response gwResponse = client.newCall(request).execute();
            String responseString = gwResponse.body() != null ? gwResponse.body().string() : "";
            logger.debug(requestId + " - response = " + responseString);
            /*
            "result=SUCCESS&TotalAmount=9.00&BuyerId=c35149a0-22f6-48e7-9a55-ddabcdf636f4&MerchantOrderId=20210913042324978&MerchantCode=SB1146&MerchantId=2335176b-4364-40d4-bd0c-299c7637228f&TransactionCode=TQHO7JSA&TransactionId=7e8c3f4f-6b0c-47cc-b6b7-b45f74504ad9&Status=Paid&Currency=ETB"
            */
            String[] arrKeyValues = responseString.replaceAll("\"","").split("&");
            Map<String,String> mapData = new HashMap<>();
            Arrays.stream(arrKeyValues).forEach((String item) -> {
                String[] arrItem = item.split("=");
                mapData.put(arrItem[0], arrItem[1]);
                logger.debug(requestId + " - "+ arrItem[0] +" = " + arrItem[1]);
            });

            String status = mapData.get("Status") != null ? mapData.get("Status") : "";
            int returnCode = 525;
            if (status.equals("Paid")) {
                returnCode = 200;
            }
            response.returnCode = returnCode;
            return response.toString();
        } catch (Exception ex) {
            logger.debug(requestId + " - queryStatus exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response.toString();
        }
    }
}
