package com.qupworld.service;

import com.devebot.opflow.OpflowBuilder;
import com.devebot.opflow.OpflowPubsubHandler;
import com.devebot.opflow.exception.OpflowBootstrapException;
import com.pg.util.KeysUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

/**
 * Created by qup on 18/05/2018.
 */
public class MailService {

  static final Logger logger = LogManager.getLogger(MailService.class);
  public static MailService instance = null;

  public OpflowPubsubHandler handler;

  public static MailService getInstance() {
    if (instance == null) {
      synchronized (MailService.class) {
        if (instance == null) {
          instance = new MailService();
        }
      }
    }
    return instance;
  }

  private MailService() {
    try {
      checkHandler();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void sendMailReport(String requestId, String subject, String body) {
    try {
      logger.debug(requestId + " - start sendMailReport");
      logger.debug(requestId + " - subject: " + subject);
      logger.debug(requestId + " - body: " + body);
      JSONObject object = new JSONObject();
      object.put("subject", subject);
      object.put("body", body);
      object.put("requestId", requestId);
      object.put("mailType", KeysUtil.sendMailReportInternal);
      checkHandler();
      handler.publish(object.toString());
      logger.debug(requestId + " - done !");
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void sendMailReportToSpecificEmail(
    String requestId,
    String subject,
    String body
  ) {
    try {
      logger.debug(requestId + " - start sendMailReport");
      logger.debug(requestId + " - subject: " + subject);
      logger.debug(requestId + " - body: " + body);
      JSONObject object = new JSONObject();
      object.put("subject", subject);
      object.put("body", body);
      object.put("toEmail", KeysUtil.ERROR_MAIL_LIST.split(","));
      object.put("requestId", requestId);
      object.put("mailType", KeysUtil.sendMailReportInternal);
      checkHandler();
      handler.publish(object.toString());
      logger.debug(requestId + " - done !");
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void sendMailPayoutCompleted(
    String requestId,
    String payTo,
    String fleetId,
    String fleetName,
    String payoutEndTime,
    int totalSuccess,
    int totalFailed,
    double totalPayout,
    String[] emails,
    org.json.JSONArray jsonError
  ) {
    try {
      logger.debug(requestId + " - start sendMailPayoutCompleted");
      System.out.println(requestId + " - start sendMailPayoutCompleted");
      String subject =
        fleetName + " - Payout completed [" + payoutEndTime + "]";
      String body = "Hi team,<br>";
      if (payTo.equals("driver")) {
        body +=
          "Driver payout on [" + payoutEndTime + "] has been completed:<br>";
        body += "- Pay successfully: " + totalSuccess + " driver(s)<br>";
        body += "- Failed: " + totalFailed + " driver(s)<br>";
      } else if (payTo.equals("merchant")) {
        body +=
          "Merchant payout on [" + payoutEndTime + "] has been completed:<br>";
        body += "- Pay successfully: " + totalSuccess + " merchant(s)<br>";
        body += "- Failed: " + totalFailed + " merchant(s)<br>";
      }

      if (jsonError.length() > 0) {
        body += "<br> Error details: <br>";
        for (int i = 0; i < jsonError.length(); i++) {
          JSONObject obj = jsonError.getJSONObject(i);
          body +=
            "- " +
            obj.getString("info") +
            ": " +
            obj.getString("message") +
            "<br>";
        }
        body += "<br>";
      }

      body += "- Total payout successfully: " + totalPayout + "<br>";
      body += "<br>";
      body += "Regards,";
      body += fleetName + " team";
      System.out.println(requestId + " - body: " + body);
      JSONObject object = new JSONObject();
      object.put("subject", subject);
      object.put("body", body);
      object.put("toEmail", emails);
      object.put("fleetId", fleetId);
      object.put("requestId", requestId);
      object.put("mailType", "sendMailPayoutCompleted");
      checkHandler();
      handler.publish(object.toString());
      logger.debug(requestId + " - done !");
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void sendMailReceiptDetail(
    String requestId,
    String bookId,
    String email
  ) {
    try {
      logger.debug(requestId + " - start sendMailReceiptDetail");
      logger.debug(requestId + " - email: " + email);
      logger.debug(requestId + " - bookId: " + bookId);
      JSONObject object = new JSONObject();
      object.put("bookId", bookId);
      object.put("manual", false);
      object.put("email", email);
      object.put("requestId", requestId);
      object.put("mailType", KeysUtil.sendMailReceipt);
      checkHandler();
      handler.publish(object.toString());
      logger.debug(requestId + " - done !");
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void sendMailTipDetail(
    String requestId,
    String bookId,
    String email,
    double tipAfterCompleted
  ) {
    try {
      logger.debug(requestId + " - start sendMailTipDetail");
      logger.debug(requestId + " - email: " + email);
      logger.debug(requestId + " - bookId: " + bookId);
      logger.debug(requestId + " - tipAfterCompleted: " + tipAfterCompleted);
      JSONObject object = new JSONObject();
      object.put("bookId", bookId);
      object.put("tipAfterCompleted", tipAfterCompleted);
      object.put("email", email);
      object.put("requestId", requestId);
      object.put("mailType", KeysUtil.sendMailTipDetail);
      checkHandler();
      handler.publish(object.toString());
      logger.debug(requestId + " - done !");
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void sendMailManualPayoutHydra(
    String requestId,
    String fleetId,
    String payoutId,
    double payoutAmount,
    String currencyISO,
    String[] emails
  ) {
    try {
      logger.debug(requestId + " - start sendMailManualPayoutHydra");
      System.out.println(requestId + " - start sendMailManualPayoutHydra");
      JSONObject object = new JSONObject();
      object.put("toEmail", emails);
      object.put("fleetId", fleetId);
      object.put("requestId", requestId);
      object.put("mailType", "sendMailManualPayoutHydra");
      object.put("payoutID", payoutId);
      object.put("payoutAmount", payoutAmount);
      object.put("currencyISO", currencyISO);
      checkHandler();
      handler.publish(object.toString());
      logger.debug(requestId + " - done !");
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void sendMailCollectPendingHydra(
    String requestId,
    String psgFleetId,
    String bookId,
    double pendingAmount,
    String currencyISO,
    String[] emails,
    String paxName,
    String cardType,
    String cardMasked,
    String action
  ) {
    try {
      logger.debug(requestId + " - start sendMailCollectPendingHydra");
      System.out.println(requestId + " - start sendMailCollectPendingHydra");
      JSONObject object = new JSONObject();
      object.put("requestId", requestId);
      object.put("fleetId", psgFleetId);
      object.put("toEmail", emails);
      object.put("mailType", "sendMailCollectPendingHydra");
      object.put("bookId", bookId);
      object.put("name", paxName);
      object.put("pendingAmount", pendingAmount);
      object.put("currencyISO", currencyISO);
      object.put("cardType", cardType);
      object.put("cardMasked", cardMasked);
      object.put("action", action);

      // re-generate service whether null pointer exception
      checkHandler();
      handler.publish(object.toString());
      logger.debug(requestId + " - done !");
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /*public void sendMailPaidInvoice(
          String requestId,
          String fleetId,
          long invoiceId,
          String email
  ) {
    try {
      logger.debug(requestId + " - start sendMailPaidInvoice");
      logger.debug(requestId + " - invoiceId: " + invoiceId);
      JSONObject object = new JSONObject();
      object.put("requestId", requestId);
      object.put("fleetId", fleetId);
      object.put("invoiceId", invoiceId);
      object.put("email", email);
      object.put("mailType", "sendMailPaidInvoice");
      checkHandler();
      handler.publish(object.toString());
      logger.debug(requestId + " - done !");
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }*/

  private void checkHandler() throws OpflowBootstrapException {
    if (handler == null) {
      handler = OpflowBuilder.createPubsubHandler();
    }
  }

  public void close() {
    handler.close();
  }
}
