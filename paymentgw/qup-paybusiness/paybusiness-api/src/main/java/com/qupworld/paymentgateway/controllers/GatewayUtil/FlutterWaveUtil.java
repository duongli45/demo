package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.models.RedisDao;
import com.qupworld.paymentgateway.models.RedisImpl;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.SecurityUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class FlutterWaveUtil {

    final static Logger logger = LogManager.getLogger(FlutterWaveUtil.class);
    final static String CHARGE_SANDBOX = "https://ravesandboxapi.flutterwave.com/flwv3-pug/getpaidx/api/charge";
    final static String CHARGE_TOKEN_SANDBOX = "https://ravesandboxapi.flutterwave.com/flwv3-pug/getpaidx/api/tokenized/charge";
    final static String VALIDATE_SANDBOX = "https://ravesandboxapi.flutterwave.com/flwv3-pug/getpaidx/api/validatecharge";
    final static String REFUND_AUTH_SANDBOX = "https://ravesandboxapi.flutterwave.com/flwv3-pug/getpaidx/api/refundorvoid";
    final static String REFUND_SANDBOX = "https://ravesandboxapi.flutterwave.com/gpx/merchant/transactions/refund";
    final static String VERIFY_SANDBOX = "https://ravesandboxapi.flutterwave.com/flwv3-pug/getpaidx/api/v2/verify";

    final static String CHARGE_PRODUCTION = "https://api.ravepay.co/flwv3-pug/getpaidx/api/charge";
    final static String CHARGE_TOKEN_PRODUCTION = "https://api.ravepay.co/flwv3-pug/getpaidx/api/tokenized/charge";
    final static String VALIDATE_PRODUCTION = "https://api.ravepay.co/flwv3-pug/getpaidx/api/validatecharge";
    final static String REFUND_AUTH_PRODUCTION = "https://api.ravepay.co/flwv3-pug/getpaidx/api/refundorvoid";
    final static String REFUND_PRODUCTION = "https://api.ravepay.co/gpx/merchant/transactions/refund";
    final static String VERIFY_PRODUCTION = "https://api.ravepay.co/flwv3-pug/getpaidx/api/v2/verify";

    static String pubKeyNormal3DS = "FLWPUBK-33f42516f7ff1bd6bbb49254ff4cb3df-X";
    static String secKeyNormal3DS = "FLWSECK-54bf778ee24d3c8ef928577dfa35e0b2-X";

    //static Map<String, Object> storeRequest = new HashMap<>();
    static String REQUESTID = "";

    RedisDao redisDao;

    public FlutterWaveUtil(String requestId, String pubKeyNormal, String secKeyNormal) {
        pubKeyNormal3DS = pubKeyNormal;
        secKeyNormal3DS = secKeyNormal;
        REQUESTID = requestId;
        redisDao = new RedisImpl();
    }

    public String initiateCreditForm(Map<String,String> mapData, double amount, String customerEmail, String customerPhone, String customerIP,
                                     String countryName, boolean isSandbox, String currencyISO) throws IOException {
        ObjResponse objResponse = new ObjResponse();
        logger.debug(REQUESTID + " -- initiateCreditForm --");
        try {
            String submitURL = isSandbox ? CHARGE_SANDBOX : CHARGE_PRODUCTION;

            String encryptedKey = getKey(secKeyNormal3DS);

            String[] date = mapData.get("expiredDate").split("/");
            /*List<String> currencyISOs = Arrays.asList("NGN", "GHS", "KES", "ZAR");
            if (currencyISOs.contains(currencyISO) && amount == 1.0)
                amount = 10.0;*/
            amount = 1.0;
            currencyISO = "USD";

            String countryCode = GatewayUtil.getCountry2Code(countryName);
            if (countryCode.equals("SN"))
                countryCode = "NG";
            Map<String, Object> mapRequest = new HashMap<>();
            mapRequest.put("PBFPubKey", pubKeyNormal3DS);
            mapRequest.put("cardno", mapData.get("cardNumber"));
            mapRequest.put("currency", currencyISO);
            mapRequest.put("country", countryCode);
            mapRequest.put("cvv", mapData.get("cvv"));
            mapRequest.put("amount", KeysUtil.DECIMAL_FORMAT.format(amount));
            mapRequest.put("expiryyear", date[1].substring(2));
            mapRequest.put("expirymonth", date[0]);
            mapRequest.put("email", customerEmail);
            mapRequest.put("IP", customerIP);
            mapRequest.put("phonenumber", customerPhone.substring(1)); // remove '+' symbol
            mapRequest.put("txRef", GatewayUtil.getOrderId());
            mapRequest.put("redirect_url", ServerConfig.dispatch_server + KeysUtil.NOTIFICATION_URL);

            for (String key : mapRequest.keySet()) {
                if (key.equals("cardno")) {
                    String cardNo = mapRequest.get(key).toString();
                    logger.debug(REQUESTID + " - " + key + ": " + "XXXXXXXXXXXX" +cardNo.substring(cardNo.length() - 4));
                } else if (key.equals("cvv")) {
                    logger.debug(REQUESTID + " - " + key + ": XXX");
                } else {
                    logger.debug(REQUESTID + " - " + key + ": " + mapRequest.get(key));
                }

            }

            Gson gson = new Gson();
            String requestParams = gson.toJson(mapRequest);
            String client = encryptData(requestParams, encryptedKey);

            String body = "{ \"PBFPubKey\": \"" + pubKeyNormal3DS + "\", \"alg\": \"3DES-24\", \"client\": \"" + client + "\" }";
            logger.debug(REQUESTID + " - submitURL: " + submitURL);
            logger.debug(REQUESTID + " - body: " + body);
            HttpResponse<JsonNode> json = Unirest.post(submitURL)
                    .header("Content-Type", "application/json; charset=utf-8; charset=utf-8")
                    .body(body)
                    .asJson();

            //do something with response
            logger.debug(REQUESTID + " - response: " + json.getBody().getObject());
            JSONObject jsonObj = json.getBody().getObject();

            if (jsonObj.getString("status").equals("success")) {
                JSONObject objData = jsonObj.getJSONObject("data");
                if (objData != null) {
                    String authMode = "";
                    if (objData.has("suggested_auth")) {
                        authMode = objData.getString("suggested_auth");
                    }
                    logger.debug(REQUESTID + " - suggested_auth: " + authMode);
                    if (authMode.isEmpty() && objData.has("authurl")) { // not request PIN, get authURL
                        String authURL = objData.getString("authurl");
                        Map<String,String> mapResponse = new HashMap<>();
                        mapResponse.put("3ds_url", authURL);
                        mapResponse.put("type", "link");
                        objResponse.returnCode = 200;
                        objResponse.response = mapResponse;
                        return objResponse.toString();

                    } else { // request PIN, return correct error code
                        // store map request
                        ObjectMapper mapper = new ObjectMapper();
                        String initData = mapper.writeValueAsString(mapRequest);
                        String encrypt = SecurityUtil.encryptProfile(SecurityUtil.getKey(), initData);
                        logger.debug("initData = " + encrypt);
                        redisDao.removeTmp(REQUESTID, customerPhone);
                        redisDao.addTmpData(REQUESTID, customerPhone, encrypt);

                        if (authMode.equals("PIN")) {
                            objResponse.returnCode = 468;
                            return objResponse.toString();
                        } else if (authMode.equals("NOAUTH_INTERNATIONAL") || authMode.equals("AVS_VBVSECURECODE")) {
                            return initiateCreditFormWithAVS(mapData, authMode, customerPhone, isSandbox);
                        }
                    }
                }
            } else if (jsonObj.getString("status").equals("error")) {
                objResponse.returnCode = 437;
                String message = jsonObj.has("message") ? jsonObj.getString("message") : "";
                if (!message.isEmpty()) {
                    Map<String,String> mapMessage = new HashMap<>();
                    mapMessage.put("message", message);
                    objResponse.response = mapMessage;
                    objResponse.message = message;
                }
                return objResponse.toString();
            }
            // return error if not run to correct case
            objResponse.returnCode = 437;
            objResponse.message = json.getBody().getObject();
            return objResponse.toString();
        } catch(Exception ex) {
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }
    }

    public String createCreditToken(String chargeId, boolean isSandbox, String cardHolder, String customerPhone){
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            String submitURL = isSandbox ? VERIFY_SANDBOX : VERIFY_PRODUCTION;
            String body = "{ " +
                    "\"txref\": \"" + chargeId + "\", " +
                    //"\"normalize\": \"1\", " +
                    "\"SECKEY\": \"" + secKeyNormal3DS + "\"" +
                    "}";
            logger.debug(REQUESTID + " - submitURL: " + submitURL);
            logger.debug(REQUESTID + " - body: " + body);
            HttpResponse<JsonNode> json = Unirest.post(submitURL)
                    .header("Content-Type", "application/json; charset=utf-8; charset=utf-8")
                    .body(body)
                    .asJson();

            //do something with response
            logger.debug(REQUESTID + " - response: " + json.getBody().getObject());
            String status = json.getBody().getObject().getString("status");
            String message = json.getBody().getObject().getString("message");

            if (status.equals("success") ) {
                // remove stored request
                redisDao.removeTmp(REQUESTID, customerPhone);

                JSONObject data = json.getBody().getObject().getJSONObject("data");
                String chargeResponse = data.has("chargecode") ? data.getString("chargecode") : "";
                String flwref = data.has("flwref") ? data.getString("flwref") : "";
                String chargeResponseMessage = "";
                if (chargeResponse.isEmpty()) {
                    JSONObject flwMeta = data.getJSONObject("flwMeta");
                    chargeResponse = flwMeta.has("chargeResponse") ? flwMeta.getString("chargeResponse") : "";
                    chargeResponseMessage = flwMeta.has("chargeResponseMessage") ? flwMeta.getString("chargeResponseMessage") : "";
                }
                if (chargeResponse.equals("00") || chargeResponse.equals("0")) {
                    JSONObject card = data.getJSONObject("card");
                    if (card != null) {
                        org.json.JSONArray jsonArray = card.getJSONArray("card_tokens");
                        if (jsonArray != null && jsonArray.length() > 0) {
                            JSONObject objToken = jsonArray.getJSONObject(0);
                            if (objToken != null) {
                                String token = objToken.has("embedtoken") ? objToken.getString("embedtoken") : "";
                                String cardMasked = card.has("last4digits") ? "XXXXXXXXXXXX" + card.getString("last4digits") : "";
                                String cardType = card.has("type") ? card.getString("type") : "";
                                if (cardType.isEmpty()) {
                                    String cardBIN = card.has("cardBIN") ? card.getString("cardBIN") : "";
                                    cardType = getCardType(cardBIN);
                                }
                                Map<String, String> mapResponse = new HashMap<>();
                                mapResponse.put("message", "");
                                mapResponse.put("creditCard", cardMasked);
                                mapResponse.put("qrCode", "");
                                mapResponse.put("token", token);
                                mapResponse.put("cardType", cardType);
                                mapResponse.put("cardHolder", cardHolder);
                                createTokenResponse.returnCode = 200;
                                createTokenResponse.response = mapResponse;

                                // call refund before return to client
                                if (!flwref.isEmpty())
                                    refund(flwref, isSandbox);

                                return createTokenResponse.toString();
                            }
                        }
                    }
                    // return error if card or objToken is null
                    createTokenResponse.returnCode = 437;
                    createTokenResponse.response = "card or token is null" + " --- " + json.getBody().getObject();
                } else {
                    String chargeMessage = data.has("chargemessage") ? data.getString("chargemessage") : "";
                    if (chargeMessage.isEmpty()) {
                        chargeMessage = chargeResponseMessage;
                    }
                    if (!chargeMessage.isEmpty()) {
                        Map<String, String> response = new HashMap<>();
                        response.put("message", chargeMessage);
                        createTokenResponse.response = response;
                    }
                    createTokenResponse.returnCode = 437;
                    createTokenResponse.message = message;
                    return createTokenResponse.toString();
                }

                return createTokenResponse.toString();
            } else {
                // error - get error message and return
                if (!message.isEmpty()) {
                    Map<String, String> response = new HashMap<>();
                    response.put("message", message);
                    createTokenResponse.response = response;
                }
                createTokenResponse.returnCode = 437;
                createTokenResponse.message = message;
                return createTokenResponse.toString();
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }
    }

    public String doPaymentWithCardInfo(String chargeId, boolean isSandbox, String cardType, String customerPhone){
        ObjResponse objResponse = new ObjResponse();
        try {
            String submitURL = isSandbox ? VERIFY_SANDBOX : VERIFY_PRODUCTION;
            String body = "{ " +
                    "\"flw_ref\": \"" + chargeId + "\", " +
                    "\"normalize\": \"1\", " +
                    "\"SECKEY\": \"" + secKeyNormal3DS + "\"" +
                    "}";
            logger.debug("body: " + body);
            HttpResponse<JsonNode> json = Unirest.post(submitURL)
                    .header("Content-Type", "application/json; charset=utf-8; charset=utf-8")
                    .body(body)
                    .asJson();

            //do something with response
            logger.debug("response: " + json.getBody().getObject());
            String status = json.getBody().getObject().getString("status");
            JSONObject data = json.getBody().getObject().getJSONObject("data");

            JSONObject flwMeta = data.getJSONObject("flwMeta");
            String chargeResponse = flwMeta.has("VBVRESPONSECODE") ? flwMeta.getString("VBVRESPONSECODE") : "";
            String message = flwMeta.has("VBVRESPONSEMESSAGE") ? flwMeta.getString("VBVRESPONSEMESSAGE") : "";
            if (status.equals("success") && ( chargeResponse.equals("00") || chargeResponse.equals("0") )) {
                // mode 3DS - remove stored request
                redisDao.removeTmp(REQUESTID, customerPhone);

                String transToken = data.has("flw_ref") ? data.getString("flw_ref") : "";
                String authCode = "";

                Map<String,String> mapResult = new HashMap<>();
                mapResult.put("transId", transToken);
                mapResult.put("authCode", authCode);
                mapResult.put("cardType", cardType);
                objResponse.returnCode = 200;
                objResponse.response = mapResult;
                return objResponse.toString();
            } else if (!message.isEmpty()) {// error - get error message and return
                Map<String,String> response = new HashMap<>();
                response.put("message", message);
                objResponse.returnCode = 437;
                objResponse.response = response;
                objResponse.message = message;
                return objResponse.toString();
            } else {
                objResponse.returnCode = getTransactionCode(status, chargeResponse);
                objResponse.response = json.getBody().getObject();
                return objResponse.toString();
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }
    }

    public String initiateCreditFormWithPIN(String pin, String customerPhone, boolean isSandbox) throws IOException {
        ObjResponse objResponse = new ObjResponse();
        logger.debug(REQUESTID + " -- initiateCreditFormWithPIN --");
        try {
            String submitURL = isSandbox ? CHARGE_SANDBOX : CHARGE_PRODUCTION;

            String encryptedKey = getKey(secKeyNormal3DS);

            // mode PIN - get stored request and add PIN
            String initData = redisDao.getTmp(REQUESTID, customerPhone);
            String decrypt = SecurityUtil.decryptProfile(SecurityUtil.getKey(), initData);

            ObjectMapper mapperResult = new ObjectMapper();
            Map<String, Object> mapRequest = mapperResult.readValue(decrypt, new TypeReference<HashMap<String, Object>>() {
            });

            mapRequest.put("suggested_auth", "pin");
            mapRequest.put("pin", pin);

            // remove stored request
            mapRequest.remove(customerPhone);

            for (String key : mapRequest.keySet()) {
                if (key.equals("cardno")) {
                    String cardNo = mapRequest.get(key).toString();
                    logger.debug(REQUESTID + " - " + key + ": " + "XXXXXXXXXXXX" +cardNo.substring(cardNo.length() - 4));
                } else if (key.equals("cvv")) {
                    logger.debug(REQUESTID + " - " + key + ": XXX");
                } else {
                    logger.debug(REQUESTID + " - " + key + ": " + mapRequest.get(key));
                }
            }

            Gson gson = new Gson();
            String requestParams = gson.toJson(mapRequest);
            String client = encryptData(requestParams, encryptedKey);

            String body = "{ \"PBFPubKey\": \"" + pubKeyNormal3DS + "\", \"alg\": \"3DES-24\", \"client\": \"" + client + "\" }";
            logger.debug(REQUESTID + " - submitURL: " + submitURL);
            logger.debug(REQUESTID + " - body: " + body);
            HttpResponse<JsonNode> json = Unirest.post(submitURL)
                    .header("Content-Type", "application/json; charset=utf-8; charset=utf-8")
                    .body(body)
                    .asJson();
            logger.debug(REQUESTID + " - response: " + json.getBody().getObject());

            JSONObject jsonObj = json.getBody().getObject();
            if (jsonObj.getString("status").equals("success")) {
                JSONObject objData = jsonObj.getJSONObject("data");
                if (objData != null) {
                    String txRef = objData.getString("txRef");
                    String flwRef = objData.getString("flwRef");
                    String chargeResponseCode = objData.has("chargeResponseCode") ? objData.getString("chargeResponseCode") : "";
                    if (chargeResponseCode.equals("00") || chargeResponseCode.equals("0")) {
                        return createCreditToken(txRef, isSandbox, "", customerPhone);
                    } else {
                        String chargeResponseMessage = objData.has("chargeResponseMessage") ? objData.getString("chargeResponseMessage") : "";
                        if (chargeResponseMessage.toLowerCase().contains("otp")) {
                            objResponse.returnCode = 469;
                            objResponse.response = flwRef;
                            return objResponse.toString();
                        }
                    }
                }
            } else {
                // error - check case invalid PIN
                String message = jsonObj.has("message") ? jsonObj.getString("message") : "";
                if (message.toLowerCase().contains("pin")) {
                    objResponse.returnCode = 471;
                    return objResponse.toString();
                } else if (message.toLowerCase().contains("declined")) {
                    Map<String,String> mapResponse = new HashMap<>();
                    mapResponse.put("message", message);
                    objResponse.returnCode = 436;
                    objResponse.response = mapResponse;
                    return objResponse.toString();
                } else {
                    Map<String,String> mapResponse = new HashMap<>();
                    mapResponse.put("message", message);
                    objResponse.returnCode = 437;
                    objResponse.response = mapResponse;
                    objResponse.message = message;
                    return objResponse.toString();
                }
            }
            // return error if not run to correct case
            objResponse.returnCode = 437;
            objResponse.message = json.getBody().getObject();
            return objResponse.toString();
        } catch(Exception ex) {
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }
    }

    public String initiateCreditFormWithAVS(Map<String,String> mapData, String authMode, String customerPhone, boolean isSandbox) throws IOException {
        ObjResponse objResponse = new ObjResponse();
        logger.debug(REQUESTID + " -- initiateCreditFormWithAVS --");
        try {
            String submitURL = isSandbox ? CHARGE_SANDBOX : CHARGE_PRODUCTION;

            String encryptedKey = getKey(secKeyNormal3DS);

            // mode AVS_VBVSECURECODE"/ "NOAUTH_INTERNATIONAL - re-submit with AVS data
            String initData = redisDao.getTmp(REQUESTID, customerPhone);
            String decrypt = SecurityUtil.decryptProfile(SecurityUtil.getKey(), initData);

            ObjectMapper mapperResult = new ObjectMapper();
            Map<String, Object> mapRequest = mapperResult.readValue(decrypt, new TypeReference<HashMap<String, Object>>() {
            });

            mapRequest.put("suggested_auth", authMode);
            String billingaddress = mapData.get("street") != null ? mapData.get("street") : "";
            String billingcity = mapData.get("city") != null ? mapData.get("city") : "";
            String billingstate = mapData.get("state") != null ? mapData.get("state") : "";
            String billingzip = mapData.get("postalCode") != null ? mapData.get("postalCode") : "";
            String billingcountry = mapData.get("country") != null ? mapData.get("country") : "";
            mapRequest.put("billingaddress", billingaddress);
            mapRequest.put("billingcity", billingcity);
            mapRequest.put("billingstate", billingstate);
            mapRequest.put("billingzip", billingzip);
            mapRequest.put("billingcountry", billingcountry);

            // remove stored request
            mapRequest.remove(customerPhone);

            for (String key : mapRequest.keySet()) {
                if (key.equals("cardno")) {
                    String cardNo = mapRequest.get(key).toString();
                    logger.debug(REQUESTID + " - " + key + ": " + "XXXXXXXXXXXX" +cardNo.substring(cardNo.length() - 4));
                } else if (key.equals("cvv")) {
                    logger.debug(REQUESTID + " - " + key + ": XXX");
                } else {
                    logger.debug(REQUESTID + " - " + key + ": " + mapRequest.get(key));
                }
            }

            Gson gson = new Gson();
            String requestParams = gson.toJson(mapRequest);
            String client = encryptData(requestParams, encryptedKey);

            String body = "{ \"PBFPubKey\": \"" + pubKeyNormal3DS + "\", \"alg\": \"3DES-24\", \"client\": \"" + client + "\" }";
            logger.debug(REQUESTID + " - submitURL: " + submitURL);
            logger.debug(REQUESTID + " - body: " + body);
            HttpResponse<JsonNode> json = Unirest.post(submitURL)
                    .header("Content-Type", "application/json; charset=utf-8; charset=utf-8")
                    .body(body)
                    .asJson();
            logger.debug(REQUESTID + " - response: " + json.getBody().getObject());

            JSONObject jsonObj = json.getBody().getObject();
            if (jsonObj.getString("status").equals("success")) {
                JSONObject objData = jsonObj.getJSONObject("data");
                if (objData != null) {
                    String txRef = objData.getString("txRef");
                    String chargeResponseCode = objData.has("chargeResponseCode") ? objData.getString("chargeResponseCode") : "";
                    if (chargeResponseCode.equals("00") || chargeResponseCode.equals("0")) {
                        return createCreditToken(txRef, isSandbox, "", customerPhone);
                    } else if (chargeResponseCode.equals("02")) {
                        String authModelUsed = objData.has("authModelUsed") ? objData.getString("authModelUsed") : "";
                        if (authModelUsed.equals("VBVSECURECODE")) {
                            String authURL = objData.getString("authurl");
                            Map<String,String> mapResponse = new HashMap<>();
                            mapResponse.put("3ds_url", authURL);
                            mapResponse.put("type", "link");
                            objResponse.returnCode = 200;
                            objResponse.response = mapResponse;
                            return objResponse.toString();
                        }
                    } else {
                        String chargeResponseMessage = objData.has("chargeResponseMessage") ? objData.getString("chargeResponseMessage") : "";
                        if (chargeResponseMessage.toLowerCase().contains("otp")) {
                            objResponse.returnCode = 469;
                            objResponse.response = txRef;
                            return objResponse.toString();
                        }
                    }
                }
            } else {
                // error - check case invalid PIN
                String message = jsonObj.has("message") ? jsonObj.getString("message") : "";
                if (message.toLowerCase().contains("pin")) {
                    objResponse.returnCode = 471;
                    return objResponse.toString();
                } else if (message.toLowerCase().contains("declined")) {
                    Map<String,String> mapResponse = new HashMap<>();
                    mapResponse.put("message", message);
                    objResponse.returnCode = 436;
                    objResponse.response = mapResponse;
                    return objResponse.toString();
                } else {
                    Map<String,String> mapResponse = new HashMap<>();
                    mapResponse.put("message", message);
                    objResponse.returnCode = 437;
                    objResponse.response = mapResponse;
                    objResponse.message = message;
                    return objResponse.toString();
                }
            }
            // return error if not run to correct case
            objResponse.returnCode = 437;
            objResponse.message = json.getBody().getObject();
            return objResponse.toString();
        } catch(Exception ex) {
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }
    }

    public String validateTransactionWithOTP(String flwRef, String otp, boolean isSandbox) throws Exception {
        ObjResponse objResponse = new ObjResponse();
        try {
            String submitURL = isSandbox ? VALIDATE_SANDBOX : VALIDATE_PRODUCTION;

            String body = "{ \"PBFPubKey\": \"" + pubKeyNormal3DS + "\", \"transaction_reference\": \"" + flwRef + "\", \"otp\": \"" + otp + "\"}";
            logger.debug(REQUESTID + " - submitURL: " + submitURL);
            logger.debug(REQUESTID + " - body: " + body);
            HttpResponse<JsonNode> json = Unirest.post(submitURL)
                    .header("Content-Type", "application/json; charset=utf-8; charset=utf-8")
                    .body(body)
                    .asJson();
            logger.debug(REQUESTID + " - response: " + json.getBody().getObject());

            JSONObject jsonObj = json.getBody().getObject();
            if (jsonObj.getString("status").equals("success")) {
                JSONObject objData = jsonObj.getJSONObject("data");
                JSONObject objTx = objData.getJSONObject("tx");
                if (objTx != null) {
                    String chargeResponseCode = objTx.getString("chargeResponseCode") != null ? objTx.getString("chargeResponseCode") : "";
                    String txRef = "";
                    if (chargeResponseCode.equals("00")) {
                        txRef = objTx.getString("txRef") != null ? objTx.getString("txRef") : "";
                        objResponse.returnCode = 200;
                        objResponse.response = txRef;
                        return objResponse.toString();
                    } else {
                        String message = objTx.getString("chargeResponseMessage") != null ? objTx.getString("chargeResponseMessage") : "";
                        if (!message.isEmpty()) {
                            Map<String,String> mapResponse = new HashMap<>();
                            mapResponse.put("message", message);
                            objResponse.response = mapResponse;
                            objResponse.message = message;
                        }
                        objResponse.returnCode = 437;
                        return objResponse.toString();
                    }

                }
            } else {
                // error - check case invalid OTP
                String message = jsonObj.has("message") ? jsonObj.getString("message") : "";
                if (message.toUpperCase().contains("VERIFIED")) {
                    objResponse.returnCode = 200;
                    objResponse.response = flwRef;
                    return objResponse.toString();
                } else if (message.toLowerCase().contains("otp")) {
                    objResponse.returnCode = 472;
                    return objResponse.toString();
                } else if (message.toLowerCase().contains("declined")) {
                    Map<String,String> mapResponse = new HashMap<>();
                    mapResponse.put("message", message);
                    objResponse.returnCode = 436;
                    objResponse.response = mapResponse;
                    return objResponse.toString();
                } else {
                    Map<String,String> mapResponse = new HashMap<>();
                    mapResponse.put("message", message);
                    objResponse.returnCode = 437;
                    objResponse.response = mapResponse;
                    objResponse.message = message;
                    return objResponse.toString();
                }
            }
            // return error if not run to correct case
            objResponse.returnCode = 437;
            objResponse.message = json.getBody().getObject();
            return objResponse.toString();
        } catch(Exception ex) {
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }
    }

    public String createPaymentWithCreditToken(String token, double amount, String customerEmail, String customerPhone, String customerIP,
                                               boolean isSandbox, String currencyISO, String countryName, String cardType) {
        ObjResponse objResponse = new ObjResponse();
        try {
            String submitURL = isSandbox ? CHARGE_TOKEN_SANDBOX : CHARGE_TOKEN_PRODUCTION;
            String body = "{ " +
                    "\"SECKEY\": \"" + secKeyNormal3DS + "\", " +
                    "\"token\": \"" + token + "\", " +
                    "\"currency\": \"" + currencyISO + "\", " +
                    "\"country\": \"" + GatewayUtil.getCountry2Code(countryName) + "\", " +
                    "\"amount\": \"" + KeysUtil.DECIMAL_FORMAT.format(amount) + "\", " +
                    "\"email\": \"" + customerEmail + "\", " +
                    "\"IP\": \"" + customerIP + "\", " +
                    "\"phonenumber\": \"" + customerPhone + "\", " +
                    "\"txRef\": \"" + GatewayUtil.getOrderId() + "\", " +
                    "\"redirect_url\": \"" + ServerConfig.dispatch_server + KeysUtil.NOTIFICATION_URL + "\"" +
                    "}";
            logger.debug(REQUESTID + " - submitURL: " + submitURL);
            logger.debug(REQUESTID + " - body: " + body);
            HttpResponse<JsonNode> json = Unirest.post(submitURL)
                    .header("Content-Type", "application/json; charset=utf-8; charset=utf-8")
                    .body(body)
                    .asJson();
            logger.debug(REQUESTID + " - response: " + json.getBody().getObject());

            String status = json.getBody().getObject().getString("status");
            JSONObject data = json.getBody().getObject().getJSONObject("data");
            if (status.equals("success")) {
                if (data != null) {
                    objResponse.returnCode = 200;
                    String transToken = data.has("flwRef") ? data.getString("flwRef") : "";
                    String authCode = "";
                    JSONObject chargeToken = data.getJSONObject("chargeToken");
                    String embed_token = chargeToken.getString("embed_token");

                    Map<String, String> mapResult = new HashMap<>();
                    mapResult.put("transId", transToken);
                    mapResult.put("authCode", authCode);
                    mapResult.put("cardType", cardType);
                    mapResult.put("embed_token", embed_token);
                    objResponse.response = mapResult;
                } else {
                    // return error if response data is not correct
                    objResponse.returnCode = 437;
                    objResponse.response = "card or token is null" + " --- " + json.getBody().getObject();
                }
                return objResponse.toString();
            } else if (json.getBody().getObject().toString().contains("That token has expired")) {
                // return token expired error
                objResponse.returnCode = 99998;
                return objResponse.toString();
            } else if (status.equals("error")) {
                objResponse.returnCode = 437;
                String message = json.getBody().getObject().has("message") ? json.getBody().getObject().getString("message") : "";
                if (!message.isEmpty()) {
                    Map<String,String> mapMessage = new HashMap<>();
                    mapMessage.put("message", message);
                    objResponse.response = mapMessage;
                    objResponse.message = message;
                }
                return objResponse.toString();
            } else {
                objResponse.returnCode = getTransactionCode(status, "");
                objResponse.response = json.getBody().getObject();
                return objResponse.toString();
            }


        } catch(Exception ex) {
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }
    }

    public int getTransactionCode(String status, String chargeResponse) {
        int errorCode = 437;
        switch (chargeResponse) {
            case "1" :
                errorCode = 200; // Approved.
                break;

            case "2" :
                errorCode = 200; // Approved.
                break;
        }

        return errorCode;
    }


    public void refund(String chargeId, boolean isSandbox) throws Exception {
        try {
            String submitURL = isSandbox ? REFUND_SANDBOX : REFUND_PRODUCTION;
            String body = "{ " +
                    "\"ref\": \"" + chargeId + "\", " +
                    "\"seckey\": \"" + secKeyNormal3DS + "\"" +
                    "}";
            logger.debug(REQUESTID + " - submitURL: " + submitURL);
            logger.debug(REQUESTID + " - body: " + body);
            HttpResponse<JsonNode> json = Unirest.post(submitURL)
                    .header("Content-Type", "application/json; charset=utf-8; charset=utf-8")
                    .body(body)
                    .asJson();

            //do something with response
            logger.debug(REQUESTID + " - response: " + json.getBody().getObject());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** ENCRYPTED METHOD **/

    // Method to turn bytes in hex
    public static String toHexStr(byte[] bytes){

        StringBuilder builder = new StringBuilder();

        for(int i = 0; i < bytes.length; i++ ){
            builder.append(String.format("%02x", bytes[i]));
        }

        return builder.toString();
    }

    // this is the getKey function that generates an encryption Key for you by passing your Secret Key as a parameter.

    public static String getKey(String seedKey) {
        try {
            MessageDigest md = MessageDigest.getInstance("md5");
            byte[] hashedString = md.digest(seedKey.getBytes("utf-8"));
            byte[] subHashString = toHexStr(Arrays.copyOfRange(hashedString, hashedString.length - 12, hashedString.length)).getBytes("utf-8");
            String subSeedKey = seedKey.replace("FLWSECK-", "");
            subSeedKey = subSeedKey.substring(0, 12);
            byte[] combineArray = new byte[24];
            System.arraycopy(subSeedKey.getBytes(), 0, combineArray, 0, 12);
            System.arraycopy(subHashString, subHashString.length - 12, combineArray, 12, 12);
            return new String(combineArray);
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    // This is the encryption function that encrypts your payload by passing the stringified format and your encryption Key.

    public static String encryptData(String message, String _encryptionKey)  {
        try {
            final byte[] digestOfPassword = _encryptionKey.getBytes("utf-8");
            final byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);

            final SecretKey key = new SecretKeySpec( keyBytes , "DESede");
            final Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            final byte[] plainTextBytes = message.getBytes("utf-8");
            final byte[] cipherText = cipher.doFinal(plainTextBytes);
            return Base64.getEncoder().encodeToString(cipherText);

        } catch (Exception e) {

            e.printStackTrace();
            return "";
        }
    }
    /** ENCRYPTED METHOD **/

    private static final String [] cardNames =
            {   "Visa" ,
                    "MasterCard",
                    "AmericanExpress",
                    "En Route",
                    "Diner's CLub/Carte Blanche",
                    "Discover",
                    "UnionPay",
                    "JCB",
                    "Verve"
            };


    public static void main(String[] args) throws Exception{

        String requestId = UUID.randomUUID().toString();
        String pubKeyNormal = "FLWPUBK-aad3e1bbd6a8aa8b30103a11d9b77fc9-X";
        String secKeyNormal = "FLWSECK-102d6671de347c2d5b4b027412b7a46c-X";

        FlutterWaveUtil flutterWaveUtil = new FlutterWaveUtil(requestId, pubKeyNormal, secKeyNormal);

        String chargeId = "20190806072307450";
        flutterWaveUtil.refund(chargeId, false);

    }

    public static String getCardType(String binNumber) {
        String cardType = "";
        String digit1 = binNumber.substring(0,1);
        String digit2 = binNumber.substring(0,2);
        String digit3 = binNumber.substring(0,3);
        String digit4 = binNumber.substring(0,4);
        String digit6 = binNumber.substring(0,6);

        /* ----
        ** VISA  prefix=4
        */
        if (digit1.equals("4"))  {
            cardType = cardNames[0];
        }
        /* ----------
        ** MASTERCARD  prefix= 51 ... 55
        */
        else if (digit2.compareTo("51")>=0 && digit2.compareTo("55")<=0) {
            cardType = cardNames[1];
        }
        /* ----
        ** AMEX  prefix=34 or 37
        */
        else if (digit2.equals("34") || digit2.equals("37")) {
            cardType = cardNames[2];
        }
        /* -----
        ** ENROU prefix=2014 or 2149
        */
        else if (digit4.equals("2014") || digit4.equals("2149")) {
            cardType = cardNames[3];
        }
        /* -----
        ** DCLUB prefix=300 ... 305 or 36 or 38
        */
        else if (digit2.equals("36") || digit2.equals("38") ||
                (digit3.compareTo("300")>=0 && digit3.compareTo("305")<=0)) {
            cardType = cardNames[4];
        }
        /* ----
        ** DISCOVER card prefix = 60
        */
        else if (digit2.equals("65") || digit4.equals("6011") ||
                (digit3.compareTo("644")>=0 && digit3.compareTo("649")<=0) ||
                (digit6.compareTo("622126")>=0 && digit6.compareTo("622925")<=0)) {
            cardType = cardNames[5];
        }
        /* ----
        ** UNIONPAY card prefix = 62,88
        */
        else if (digit2.equals("62") || digit2.equals("88")) {
            cardType = cardNames[6];
        }
        /* -----
        ** JCB prefix=3528 ... 3589
        */
        else if (digit4.compareTo("3528")>=0 && digit4.compareTo("3589")<=0) {
            cardType = cardNames[7];
        }
        /* -----
        ** VERVE prefix 506099–506198, 650002–650027
        */
        else if ((digit6.compareTo("506099")>=0 && digit6.compareTo("506198")<=0) ||
                (digit6.compareTo("650002")>=0 && digit6.compareTo("650027")<=0)) {
            cardType = cardNames[8];
        }
        return cardType;
    }
}
