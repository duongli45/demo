
package com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IPGeoLocationResponse }
     * 
     */
    public IPGeoLocationResponse createIPGeoLocationResponse() {
        return new IPGeoLocationResponse();
    }

    /**
     * Create an instance of {@link AuthorizeResponse }
     * 
     */
    public AuthorizeResponse createAuthorizeResponse() {
        return new AuthorizeResponse();
    }

    /**
     * Create an instance of {@link TransactionModificationResponse }
     * 
     */
    public TransactionModificationResponse createTransactionModificationResponse() {
        return new TransactionModificationResponse();
    }

    /**
     * Create an instance of {@link IPGeoLocation }
     * 
     */
    public IPGeoLocation createIPGeoLocation() {
        return new IPGeoLocation();
    }

    /**
     * Create an instance of {@link Authorize3DSResponse }
     * 
     */
    public Authorize3DSResponse createAuthorize3DSResponse() {
        return new Authorize3DSResponse();
    }

    /**
     * Create an instance of {@link TransactionModification }
     * 
     */
    public TransactionModification createTransactionModification() {
        return new TransactionModification();
    }

    /**
     * Create an instance of {@link Authorize3DS }
     * 
     */
    public Authorize3DS createAuthorize3DS() {
        return new Authorize3DS();
    }

    /**
     * Create an instance of {@link TransactionStatus }
     * 
     */
    public TransactionStatus createTransactionStatus() {
        return new TransactionStatus();
    }

    /**
     * Create an instance of {@link TransactionStatusResponse }
     * 
     */
    public TransactionStatusResponse createTransactionStatusResponse() {
        return new TransactionStatusResponse();
    }

    /**
     * Create an instance of {@link Authorize }
     * 
     */
    public Authorize createAuthorize() {
        return new Authorize();
    }

}
