package com.qupworld.paymentgateway.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.qupworld.paymentgateway.controllers.GatewayUtil.StripeIntentUtil;
import com.qupworld.paymentgateway.controllers.GatewayUtil.StripeUtil;
import com.qupworld.paymentgateway.controllers.threading.HydraPayoutThread;
import com.qupworld.paymentgateway.entities.PaymentEnt;
import com.qupworld.paymentgateway.entities.PreAuthEnt;
import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.MongoDaoImpl;
import com.qupworld.paymentgateway.models.RedisDao;
import com.qupworld.paymentgateway.models.RedisImpl;
import com.qupworld.paymentgateway.models.mongo.collections.AffiliateSetting.AffiliateSetting;
import com.qupworld.paymentgateway.models.mongo.collections.booking.Booking;
import com.qupworld.paymentgateway.models.mongo.collections.fleet.Fleet;
import com.qupworld.paymentgateway.models.mongo.collections.gateway.GatewayStripe;
import com.pg.util.CommonUtils;
import com.pg.util.ObjResponse;
import com.pg.util.TimezoneUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
public class HydraUtil {

    private Gson gson;
    private MongoDao mongoDao;
    private RedisDao redisDao;
    private PaymentUtil paymentUtil;
    final static Logger logger = LogManager.getLogger(HydraUtil.class);

    public HydraUtil() {
        gson = new Gson();
        mongoDao = new MongoDaoImpl();
        paymentUtil = new PaymentUtil();
        redisDao = new RedisImpl();
    }

    public String preAuthForHydra(PreAuthEnt preAuthEnt) {
        ObjResponse preAuthResponse = new ObjResponse();
        String requestId = preAuthEnt.requestId;
        try {
            // update amount with setting for Hydra
            AffiliateSetting affiliateSetting = mongoDao.getAffiliateSetting();
            double authAmount = 1.0;
            String currencyISO = "USD";
            if (!preAuthEnt.reservation) {
                double buffer = affiliateSetting.generalSetting.preAuthorization.buffer;
                authAmount = (preAuthEnt.total * buffer)/100;
                currencyISO = preAuthEnt.currencyISO;
            }

            // update request entity with new amount
            preAuthEnt.total = authAmount;
            // cache data for 3DS Payment
            logger.debug(requestId + "SCALOG: CACHE AUTH " + preAuthEnt.bookId);
            StripeIntentUtil sUtil = new  StripeIntentUtil();
            Gson gson = (new GsonBuilder()).serializeNulls().create();
            String strPayData = gson.toJson(preAuthEnt);
            sUtil.addAuthenRequired(preAuthEnt.bookId,strPayData);
            sUtil.addAuthenRequired("queue-"+preAuthEnt.bookId, preAuthEnt.fleetId);

            StripeUtil stripeUtil = new StripeUtil(requestId);
            String bookId = preAuthEnt.bookId != null ? preAuthEnt.bookId : "PRE_AUTH";
            String secretKey = paymentUtil.getStripeSystemAccount(preAuthEnt.fleetId);
            String result = stripeUtil.preAuthPayment(preAuthEnt.fleetId ,bookId, secretKey, preAuthEnt.crossToken, authAmount, currencyISO);
            logger.debug(requestId + " - preAuth result: " + result);
            if (result.isEmpty()) {
                // gateway do not support pre-auth
                preAuthResponse.returnCode = 453;   // message: Do not support
                return preAuthResponse.toString();
            }
            if (paymentUtil.getReturnCode(result) != 200) {
                // ignore code 3DS
                if (paymentUtil.getReturnCode(result) != 113){
                    // pre-auth failed - limit retry
                    paymentUtil.addFailedCard(preAuthEnt.fleetId, "", preAuthEnt.localToken, preAuthEnt.requestId);   // save token to Failed card - card type = "", last4 = token
                }
            }
            return result;
        } catch (Exception ex) {
            logger.debug(preAuthEnt.requestId + " - exception: " + CommonUtils.getError(ex));
            preAuthResponse.returnCode = 525;
            return preAuthResponse.toString();
        }
    }

    public String captureForHydra(PaymentEnt paymentEnt, Booking booking) {
        ObjResponse captureResponse = new ObjResponse();
        String requestId = paymentEnt.requestId;
        try {
            StripeUtil stripeUtil = new StripeUtil(requestId);
            String authId = booking.preAuthorized.authId != null ? booking.preAuthorized.authId : "";
            double authAmount = booking.preAuthorized.authAmount;
            String authCode = booking.preAuthorized.authCode != null ? booking.preAuthorized.authCode : "";
            String authToken = booking.preAuthorized.authToken != null ? booking.preAuthorized.authToken : "";
            logger.debug(paymentEnt.requestId + " - authAmount: " + authAmount);
            double total = paymentEnt.totalAffiliate;
            logger.debug(paymentEnt.requestId + " - DO CAPTURE for " + paymentEnt.totalAffiliate);
            String secretKey = paymentUtil.getStripeSystemAccount(paymentEnt.fleetId);
            double totalCaptured;
            if (total <= authAmount) {
                String captureResult = stripeUtil.preAuthCapture(paymentEnt.fleetId, paymentEnt.bookId, secretKey,
                        authToken, total, authId, booking.currencyISO);
                if (CommonUtils.getReturnCode(captureResult) != 200) {
                    // capture failed - try to charge credit card
                    String chargeResult = stripeUtil.createPaymentWithCreditToken(paymentEnt.fleetId, paymentEnt.bookId,
                            booking.currencyISO, total, authToken, secretKey);
                    if (CommonUtils.getReturnCode(chargeResult) != 200) {
                        totalCaptured = 0;
                    } else {
                        totalCaptured = total;
                    }
                } else {
                    totalCaptured = total;
                }
            } else {
                double remainAmount = total - authAmount;
                logger.debug(paymentEnt.requestId + " - remainAmount: " + remainAmount);
                //auth remain
                logger.debug(paymentEnt.requestId + " - DO AUTHORIZE for " + remainAmount);
                String authRemainResult = stripeUtil.preAuthPayment(booking.fleetId, paymentEnt.bookId, secretKey, authToken, remainAmount, booking.currencyISO);
                int authRemainCode = paymentUtil.getReturnCode(authRemainResult);
                if (authRemainCode == 200) {
                    JSONObject objectAuthRemain = paymentUtil.toObjectResponse(authRemainResult);
                    JSONObject responseAuthRemain = (JSONObject) objectAuthRemain.get("response");
                    String authRemainId = responseAuthRemain.get("authId") != null ? responseAuthRemain.get("authId").toString() : "";
                    // capture the pre-auth amount
                    String captureResult = stripeUtil.preAuthCapture(booking.fleetId, paymentEnt.bookId, secretKey,
                            authToken, authAmount, authId, booking.currencyISO);
                    if (paymentUtil.getReturnCode(captureResult) == 200) {
                        // capture remain
                        String captureRemainResult = stripeUtil.preAuthCapture(booking.fleetId, paymentEnt.bookId, secretKey,
                                authToken, remainAmount, authRemainId, booking.currencyISO);
                        if (paymentUtil.getReturnCode(captureRemainResult) != 200) {
                            // capture remain was failed
                            totalCaptured = authAmount;
                        } else {
                            totalCaptured = total;
                        }
                    } else {
                        // release remain
                        stripeUtil.voidStripeTransaction(requestId, booking.fleetId, booking.bookId, secretKey, authRemainId);
                        totalCaptured = 0;
                    }
                } else {
                    // pre-auth remain has failed - capture pre-auth amount
                    String captureResult = stripeUtil.preAuthCapture(paymentEnt.fleetId, paymentEnt.bookId, secretKey,
                            authToken, authAmount, authId, booking.currencyISO);
                    if (paymentUtil.getReturnCode(captureResult) == 200) {
                        totalCaptured = authAmount;
                    } else {
                        totalCaptured = 0;
                    }
                }
            }
            Map<String,Object> mapResponse = new HashMap<>();
            mapResponse.put("totalCaptured", totalCaptured);
            mapResponse.put("authCode", authCode);
            mapResponse.put("transId", authId);
            captureResponse.response = mapResponse;
            captureResponse.returnCode = 200;
            return captureResponse.toString();
        } catch (Exception ex) {
            logger.debug(requestId + " - captureForHydra exception: " + CommonUtils.getError(ex));
            captureResponse.returnCode = 525;
            return captureResponse.toString();
        }
    }

    public String chargeForHydra(String requestId, Booking booking, double total) {
        ObjResponse captureResponse = new ObjResponse();
        try {
            StripeUtil stripeUtil = new StripeUtil(requestId);
            String authToken = booking.preAuthorized.authToken != null ? booking.preAuthorized.authToken : "";
            logger.debug(requestId + " - CHARGE for " + total);
            String secretKey = paymentUtil.getStripeSystemAccount(booking.fleetId);
            return stripeUtil.createPaymentWithCreditToken(booking.fleetId, booking.bookId,
                    booking.currencyISO, total, authToken, secretKey);
        } catch (Exception ex) {
            logger.debug(requestId + " - chargeForHydra exception: " + CommonUtils.getError(ex));
            captureResponse.returnCode = 525;
            return captureResponse.toString();
        }
    }

    public String verifyReservationBooking(PaymentEnt paymentEnt) {
        ObjResponse preAuthResponse = new ObjResponse();
        String requestId = paymentEnt.requestId;
        try {
            Booking booking = mongoDao.getBooking(paymentEnt.bookId);
            String authId = booking.preAuthorized.authId != null ? booking.preAuthorized.authId : "";
            double authAmount = booking.preAuthorized.authAmount;
            String authCode = booking.preAuthorized.authCode != null ? booking.preAuthorized.authCode : "";
            String authToken = booking.preAuthorized.authToken != null ? booking.preAuthorized.authToken : "";
            logger.debug(requestId + " - authAmount: " + authAmount);
            logger.debug(requestId + " - authCode: " + authCode);
            logger.debug(requestId + " - authToken: " + authToken);
            String secretKey = paymentUtil.getStripeSystemAccount(booking.fleetId);
            StripeUtil stripeUtil = new StripeUtil(requestId);
            String result = stripeUtil.preAuthCapture(booking.fleetId, paymentEnt.bookId, secretKey,
                    authToken, authAmount, authId, booking.currencyISO);
            logger.debug(requestId + " - captureResult: " + result);
            if (CommonUtils.getReturnCode(result) != 200) {
                // capture fail - try to charge the booking again
                result = stripeUtil.createPaymentWithCreditToken(booking.fleetId, booking.bookId, booking.currencyISO, authAmount, authToken, secretKey);
                logger.debug(requestId + " - chargeResult: " + result);
            }
            return result;
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            preAuthResponse.returnCode = 525;
            return preAuthResponse.toString();
        }
    }

    public String payoutToFleet(String requestId, String fleetId, String chargeId, Booking booking, double amount) {
        ObjResponse response = new ObjResponse();
        try {
            Fleet fleet = mongoDao.getFleetInfor(fleetId);
            String token = fleet.stripeConnect != null && fleet.stripeConnect.connectToken != null ? fleet.stripeConnect.connectToken : "";
            String status = fleet.stripeConnect != null && fleet.stripeConnect.connectStatus != null ? fleet.stripeConnect.connectStatus : "";
            if (!token.isEmpty() && status.equals("activated")) {
                String secretKey;
                if (booking.isFarmOut) {
                    GatewayStripe gatewayStripe = mongoDao.getGatewayStripeFleet(booking.request.psgFleetId);
                    secretKey = gatewayStripe != null && gatewayStripe.secretKey != null ? gatewayStripe.secretKey : "";
                } else {
                    secretKey = paymentUtil.getStripeSystemAccount(fleetId);
                }
                StripeUtil stripeUtil = new StripeUtil(requestId);
                String result = stripeUtil.doTransfer(secretKey, chargeId, booking.bookId, token, amount, "USD");
                if (CommonUtils.getReturnCode(result) == 200) {
                    logger.debug(requestId + " - transfer succeed - send history to Report");
                    HydraPayoutThread hydraPayoutThread = new HydraPayoutThread();
                    hydraPayoutThread.requestId = requestId;
                    hydraPayoutThread.payoutId = CommonUtils.getOrderId();
                    hydraPayoutThread.payoutType = "auto";
                    hydraPayoutThread.dateTime = TimezoneUtil.getGMTTimestamp();
                    hydraPayoutThread.accountHolderName = "";
                    hydraPayoutThread.bankName = "";
                    hydraPayoutThread.accountNumber = "";
                    hydraPayoutThread.totalPayout = amount;
                    hydraPayoutThread.notes = "";
                    hydraPayoutThread.fleetId = fleetId;
                    hydraPayoutThread.bookId = booking.bookId;
                    hydraPayoutThread.transactionId = CommonUtils.getValue(result, "transferId");
                    hydraPayoutThread.operatorName = "";
                    hydraPayoutThread.fleetName = fleet.name;
                    hydraPayoutThread.email = fleet.email != null ? fleet.email : "";
                    hydraPayoutThread.currencyISO = "USD";
                    hydraPayoutThread.start();
                }
                return result;
            } else {
                Map<String,Object> mapResponse = new HashMap<>();
                mapResponse.put("token", token);
                mapResponse.put("status", status);
                mapResponse.put("message", "Connect token is empty or not activated yet");
                response.response = mapResponse;
                response.returnCode = 525;
                return response.toString();
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - payoutToFleet exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response.toString();
        }
    }
}


