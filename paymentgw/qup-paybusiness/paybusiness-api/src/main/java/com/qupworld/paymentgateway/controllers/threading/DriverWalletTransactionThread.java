package com.qupworld.paymentgateway.controllers.threading;

import com.google.gson.Gson;
import com.qupworld.service.QUpReportPublisher;
import com.pg.util.CommonUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by qup on 23/03/2020.
 */
@SuppressWarnings("unchecked")
public class DriverWalletTransactionThread extends Thread {
    public String id;
    public String reportBody;
    public String requestId;
    Gson gson = new Gson();
    final static Logger logger = LogManager.getLogger(DriverWalletTransactionThread.class);
    @Override
    public void run() {
        try {
            QUpReportPublisher reportPublisher = QUpReportPublisher.getInstance();
            reportPublisher.sendOneItem(requestId, String.valueOf(id), "DriverWalletTransaction", reportBody);
        } catch(Exception ex) {
            logger.debug(requestId + " - report DriverWalletTransaction: "+ CommonUtils.getError(ex));
        }
    }

}
