package com.qupworld.paymentgateway.controllers.GatewayUtil.FAC;

import java.security.MessageDigest;

/**
 * Created by qup on 11/21/16.
 */
public class Signature {
    public static String SignAuthorize(String password, String merchantId,
                              String acquirerId, String orderNumber,
                              String amount, String currency){
        try {
            String text = password.trim() + merchantId.trim() +
                    acquirerId.trim() + orderNumber.trim() +
                    amount.trim() + currency.trim();
            MessageDigest md;
            md = MessageDigest.getInstance("SHA-1");
            byte[] sha1hash = new byte[40];
            md.update(text.getBytes("iso-8859-1"), 0, text.length());
            sha1hash = md.digest();
            return Base64Encoder.encode(sha1hash);
        } catch(Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }
}
