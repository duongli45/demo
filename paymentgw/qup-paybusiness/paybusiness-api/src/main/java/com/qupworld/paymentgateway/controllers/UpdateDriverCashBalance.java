package com.qupworld.paymentgateway.controllers;

import com.google.gson.Gson;
import com.qupworld.paymentgateway.entities.WalletTransfer;
import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.RedisDao;
import com.qupworld.paymentgateway.models.mongo.collections.wallet.DriverWallet;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UpdateDriverCashBalance {

    final static Logger logger = LogManager.getLogger(UpdateDriverCashBalance.class);
    public String requestId;
    public WalletTransfer walletTransfer;
    public MongoDao mongoDao;
    public RedisDao redisDao;

    private Gson gson = new Gson();
    public double pendingBalance = 0.0;
    public boolean isDeposit = false;

    public double doUpdate() {
        String driverId = walletTransfer.driverId;
        try {
            // check if there is another update process
            boolean isCache = redisDao.addUniqueData(requestId, "cash-" + driverId, "cache" + driverId);
            while (!isCache) {
                logger.debug(requestId + " - there is another update process ...");
                // if not success means this orderId has been cached for the previous request
                // wait 3 seconds then try again
                Thread.sleep(3000);
                isCache = redisDao.addUniqueData(requestId, "cash-" + driverId, "cache" + driverId);
            }

            WalletUtil walletUtil = new WalletUtil(requestId);
            double currentBalance = walletUtil.getCurrentBalance(walletTransfer.driverId, "cash", walletTransfer.currencyISO);
            logger.debug(requestId + " - balance before update: " + currentBalance);
            // update wallet
            double amount = KeysUtil.DECIMAL_FORMAT.parse(KeysUtil.DECIMAL_FORMAT.format(walletTransfer.amount)).doubleValue();
            double newBalance;
            if (isDeposit) {
                newBalance = currentBalance + amount;
            } else {
                newBalance = currentBalance - amount;
            }
            // update balance
            mongoDao.updateCashWallet(walletTransfer.driverId, newBalance, pendingBalance, walletTransfer.currencyISO);
            walletUtil.updateCashWallet(walletTransfer, isDeposit);

            DriverWallet driverWallet = mongoDao.getDriverWallet(walletTransfer.driverId);
            logger.debug(requestId + " - balance after update: " + gson.toJson(driverWallet));
            redisDao.removeUniqueData(requestId, "cash-" + driverId);
            return newBalance;
        } catch (Exception ex) {
            logger.debug(requestId + " - UpdateDriverCashBalance exception: " + CommonUtils.getError(ex));
            redisDao.removeUniqueData(requestId, "cash-" + driverId);
            return 0.0;
        }
    }
}
