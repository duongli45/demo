/*
 * Copyright 2015 Willian Oki
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.qupworld.util;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.json.simple.JSONObject;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import java.math.BigInteger;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * @author Willian Oki &lt;willian.oki@gmail.com&gt;
 */
@SuppressWarnings("unchecked")
public final class AdyenCSEUtil {
    private static final String CSE_VERSION = "1_0_0";
    private static final String CSE_SEPARATOR = "$";
    private static final String CSE_PREFIX = "payments-adyen-api_";
    private static final SecureRandom CSE_RANDOM = new SecureRandom();
    private static final Logger logger = LogManager.getLogger(AdyenCSEUtil.class);
    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    private AdyenCSEUtil() {
        // utility class
    }

    private static SecretKey aesKey(int keySize) throws NoSuchAlgorithmException {
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        kgen.init(keySize);
        return kgen.generateKey();
    }

    private static synchronized byte[] iv(SecureRandom random, int ivSize) {
        byte[] iv = new byte[ivSize];
        random.nextBytes(iv);
        return iv;
    }

    public static void main(String[] args) throws Exception{
        Cipher aesCipher = AdyenCSEUtil.aesCipher();
        String cseKeyText = "10001|A1585A8BF19487788A59ECAEEBDF05C2829129CC628F6BCD1E0B266BDBEF107C6A11A404AD" +
                "64217429C49F57D1FFDE51E7E3122EE73A8C54D5837BBCDFFE431EDDDDA574C3325636BAFD4CBD944DC6666FFA26" +
                "EE931E058C0278F71927A8B5ABC6F292C781C7AF41182F693FF936F5441DF1517F8EF751DA00F834FEEEF4CB8162" +
                "41AD481B5C596B66595D9370844D4EC9558B398A56E4C616916D6BB379B8FB8ADB1CAA71CC7BE018985D1A8AFBEC" +
                "1EF52CD8FEEA393E135320424DF99E854BAE84F33DE69F468A99C9CFD72A374391692DB7B39DBDB84B3FF78CC9CD" +
                "DCD031DB0FFDB105D179C622EF67931B4980E95F4C07D473EA7DA1DD03F2FB3744C1F5";
        Cipher rsaCipher = AdyenCSEUtil.rsaCipher(cseKeyText);

        String cardNumber = "4111111111111111";
        String holderName = "Any name";
        String cvc = "737";
        String expiryMonth = "10";
        String expiryYear = "2020";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.sss'Z'");

        Date date = offsetTimeZone(new Date(), Calendar.getInstance().getTimeZone().getID(), "GMT");
        String generationtime = sdf.format(date);

        JSONObject object = new JSONObject();
        object.put("number", cardNumber);
        object.put("holderName", holderName);
        object.put("cvc", cvc);
        object.put("expiryMonth", expiryMonth);
        object.put("expiryYear", expiryYear);
        object.put("generationtime", generationtime);
        String plainText = object.toJSONString();
        logger.debug("plainText: " + plainText);
        String result = AdyenCSEUtil.encrypt(aesCipher, rsaCipher, plainText);
        logger.debug("result: " + result);
    }

    public static String getCSEString(String cseKey, String cardNumber, String holderName, String cvc, String expiryMonth, String expiryYear) {
        try {
            Cipher aesCipher = AdyenCSEUtil.aesCipher();
            Cipher rsaCipher = AdyenCSEUtil.rsaCipher(cseKey);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.sss'Z'");
            Date date = offsetTimeZone(new Date(), Calendar.getInstance().getTimeZone().getID(), "GMT");
            String generationtime = sdf.format(date);

            JSONObject object = new JSONObject();
            object.put("number", cardNumber);
            object.put("holderName", holderName);
            object.put("cvc", cvc);
            object.put("expiryMonth", expiryMonth);
            object.put("expiryYear", expiryYear);
            object.put("generationtime", generationtime);
            String plainText = object.toJSONString();
            logger.debug("plainText: " + plainText.replace(cardNumber, "XXXXXXXXXXXX" + cardNumber.substring(cardNumber.length() -4)));
            String result = AdyenCSEUtil.encrypt(aesCipher, rsaCipher, plainText);
            logger.debug("result: " + result);
            return result;
        } catch(Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

    public static Date offsetTimeZone(Date date, String fromTZ, String toTZ){

        //construct FROM and TO Timezone instances
        TimeZone fromTimeZone = TimeZone.getTimeZone(fromTZ);
        TimeZone toTimeZone = TimeZone.getTimeZone(toTZ);

        //Get a Calendar instance using the default time zone and locale.
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(toTZ));

        // Set the calendar's time with the given date
        calendar.setTimeZone(fromTimeZone);
        calendar.setTime(date);

        //System.out.println("--------------------TimeUtile: "+calendar.getTime());

        // FROM timezone to UTC
        calendar.add(Calendar.MILLISECOND, fromTimeZone.getRawOffset()*(-1));
        if(fromTimeZone.inDaylightTime(calendar.getTime())){
            /*System.out.println("fromTimeZone in Daylight");*/
            calendar.add(Calendar.MILLISECOND, calendar.getTimeZone().getDSTSavings() *(-1));
            calendar.getTimeZone().getDisplayName();
        }
        //System.out.println("-----------------------FROM timezone to UTC: "+calendar.getTime() + " ---- " + fromTimeZone.getRawOffset()*(-1));

        // UTC to Timezone
        calendar.add(Calendar.MILLISECOND, toTimeZone.getRawOffset());
        if(toTimeZone.inDaylightTime(calendar.getTime())){
            /*System.out.println("toTimeZone in Daylight");*/
            calendar.add(Calendar.MILLISECOND, toTimeZone.getDSTSavings());
        }
        //System.out.println("-------------------------UTC to Timezone: "+calendar.getTime() + " ---- " + toTimeZone.getRawOffset());
        return calendar.getTime();
    }

    static String encrypt(final Cipher aesCipher, final Cipher rsaCipher, final String plainText) throws BadPaddingException, IllegalBlockSizeException,
            NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException {
        SecretKey aesKey = aesKey(256);
        byte[] iv = iv(CSE_RANDOM, 12);
        aesCipher.init(Cipher.ENCRYPT_MODE, aesKey, new IvParameterSpec(iv));
        byte[] encrypted = aesCipher.doFinal(plainText.getBytes());

        byte[] result = new byte[iv.length + encrypted.length];
        System.arraycopy(iv, 0, result, 0, iv.length);
        System.arraycopy(encrypted, 0, result, iv.length, encrypted.length);

        byte[] encryptedAESKey;
        try {
            encryptedAESKey = rsaCipher.doFinal(aesKey.getEncoded());
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new InvalidKeyException(e.getMessage());
        }
        return String.format("%s%s%s%s%s%s", CSE_PREFIX, CSE_VERSION, CSE_SEPARATOR, Base64.encodeBase64String(encryptedAESKey), CSE_SEPARATOR,
                Base64.encodeBase64String(result));
    }

    public static Cipher aesCipher() throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException {
        return Cipher.getInstance("AES/CCM/NoPadding", "BC");
    }

    public static Cipher rsaCipher(final String cseKeyText) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, InvalidKeySpecException, IllegalArgumentException {
        String[] cseKeyParts = cseKeyText.split("\\|");
        if (cseKeyParts.length != 2) {
            throw new InvalidKeyException("Invalid CSE Key: " + cseKeyText);
        }
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");

        BigInteger keyComponent1, keyComponent2;
        try {
            keyComponent1 = new BigInteger(cseKeyParts[1].toLowerCase(Locale.getDefault()), 16);
            keyComponent2 = new BigInteger(cseKeyParts[0].toLowerCase(Locale.getDefault()), 16);
        } catch (NumberFormatException e) {
            throw new InvalidKeyException("Invalid CSE Key: " + cseKeyText);
        }
        RSAPublicKeySpec pubKeySpec = new RSAPublicKeySpec(keyComponent1, keyComponent2);
        PublicKey pubKey = keyFactory.generatePublic(pubKeySpec);

        Cipher result = Cipher.getInstance("RSA/None/PKCS1Padding");
        result.init(Cipher.ENCRYPT_MODE, pubKey);
        return result;
    }
}
