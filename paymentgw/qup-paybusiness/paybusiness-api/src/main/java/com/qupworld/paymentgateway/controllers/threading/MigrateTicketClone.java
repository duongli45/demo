package com.qupworld.paymentgateway.controllers.threading;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.SQLDao;
import com.qupworld.paymentgateway.models.mongo.collections.account.Account;
import com.qupworld.paymentgateway.models.mysql.tables.records.QupTicketextendRecord;
import com.qupworld.paymentgateway.models.mysql.tables.records.QupTicketextraRecord;
import com.pg.util.KeysUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by qup on 23/03/2020.
 */
@SuppressWarnings("unchecked")
public class MigrateTicketClone extends Thread {
    final static Logger logger = LogManager.getLogger(MigrateTicketClone.class);
    public JSONArray records;
    public List<String> listKeys;
    public SQLDao sqlDao;
    public String requestId;
    public MongoDao mongoDao;

    public MigrateTicketClone(JSONArray records, List<String> listKeys, SQLDao sqlDao, MongoDao mongoDao, String requestId) {
        this.records = records;
        this.listKeys = listKeys;
        this.sqlDao = sqlDao;
        this.requestId = requestId;
        this.mongoDao = mongoDao;
    }

    @Override
    public void run() {
        try {
            GsonBuilder gb = new GsonBuilder();
            gb.serializeNulls();
            Gson gson = gb.create();
            JSONParser parser = new JSONParser();
            String body = "[";
            for (Object record : records) {
                //logger.debug(requestId + " - record: " + gson.toJson(record));
                JSONArray object = (JSONArray) record;
                Map<String, Object> mapData = new HashMap<>();
                String bookId = "";
                for (int k = 0; k < listKeys.size(); k++) {
                    switch (k) {
                        case 3:
                            bookId = object.get(k).toString();
                            mapData.put(listKeys.get(k), object.get(k));
                            break;
                        case 28:
                            String comment = object.get(k) != null ? object.get(k).toString().replace("\"", "").replace("(", "").replace(")", "") : "";
                            mapData.put(listKeys.get(k), comment);
                            break;
                        case 10:
                            String destination = object.get(k) != null ? object.get(k).toString().replace("\"", "").replace("(", "").replace(")", "") : "";
                            mapData.put(listKeys.get(k), destination);
                            break;
                        case 23:
                            String pickup = object.get(k) != null ? object.get(k).toString().replace("\"", "").replace("(", "").replace(")", "") : "";
                            mapData.put(listKeys.get(k), pickup);
                            break;
                        case 9:
                            String customerName = object.get(k) != null ? object.get(k).toString().replace("\"", "").replace("(", "").replace(")", "") : "";
                            mapData.put(listKeys.get(k), customerName);
                            break;
                        case 13:
                            String driverName = object.get(k) != null ? object.get(k).toString().replace("\"", "").replace("(", "").replace(")", "") : "";
                            mapData.put(listKeys.get(k), driverName);
                            break;
                        case 88:
                            String userName = object.get(k) != null ? object.get(k).toString().replace("\"", "").replace("(", "").replace(")", "") : "";
                            mapData.put(listKeys.get(k), userName);
                            break;
                        default:
                            mapData.put(listKeys.get(k), object.get(k));
                    }
                }

                QupTicketextendRecord qupTicketextendRecord = sqlDao.getTicketExtend(requestId, bookId);
                if(qupTicketextendRecord != null) {
                    ResultSet rsExtend = qupTicketextendRecord.intoResultSet();
                    while (rsExtend.next()) {
                        mapData.put("tollFee", rsExtend.getDouble("double0"));
                        mapData.put("otherFeesDetails", rsExtend.getString("text0"));
                        mapData.put("gatewayId", rsExtend.getString("varchar0"));
                        mapData.put("signatureUrl", rsExtend.getString("varchar1"));
                        mapData.put("rideSharing", rsExtend.getInt("boolean0"));
                        mapData.put("basicFareCalculator", rsExtend.getDouble("double1"));
                        mapData.put("driverDeduction", rsExtend.getDouble("double8"));
                        mapData.put("fareProvider", rsExtend.getDouble("double2"));
                        mapData.put("tipProvider", rsExtend.getDouble("double3"));
                        mapData.put("taxValueProvider", rsExtend.getDouble("double4"));
                        mapData.put("subTotalProvider", rsExtend.getDouble("double5"));
                        mapData.put("totalProvider", rsExtend.getDouble("double6"));
                        mapData.put("qupCommission", rsExtend.getDouble("double7"));
                        mapData.put("minimumProvider", rsExtend.getDouble("double9"));
                        mapData.put("fleetCommission", rsExtend.getDouble("double10"));
                        mapData.put("netProfit", rsExtend.getDouble("double11"));
                        mapData.put("bookType", rsExtend.getInt("int0"));
                        mapData.put("driverUserName", rsExtend.getString("text5"));
                        mapData.put("additionalServices", gson.fromJson(rsExtend.getString("text3"), JSONArray.class));
                        mapData.put("extraDestination", gson.fromJson(rsExtend.getString("text4"), JSONArray.class));
                        mapData.put("reasonCode", rsExtend.getInt("int1"));
                        mapData.put("isPending", rsExtend.getBoolean("boolean2"));
                        mapData.put("shortTrip", rsExtend.getBoolean("boolean3"));
                        mapData.put("intercity", rsExtend.getBoolean("boolean4"));
                        mapData.put("tripId", rsExtend.getString("varchar9"));
                        JSONObject mapCorporate = gson.fromJson(rsExtend.getString("text6"), JSONObject.class);
                        if(mapCorporate != null){
                            mapData.put("corpId", mapCorporate.get("corpId"));
                            mapData.put("corpDivision", mapCorporate.get("corpDivision"));
                            mapData.put("costCentre", mapCorporate.get("costCentre"));
                            mapData.put("corpPO", mapCorporate.get("corpPO"));
                            mapData.put("managerName", mapCorporate.get("managerName"));
                            mapData.put("managerEmail", mapCorporate.get("managerEmail"));
                            mapData.put("title", mapCorporate.get("title"));
                            mapData.put("department", mapCorporate.get("department"));
                        } else {
                            mapData.put("corpId", "");
                            mapData.put("corpDivision", "");
                            mapData.put("costCentre", "");
                            mapData.put("corpPO", "");
                            mapData.put("managerName", "");
                            mapData.put("managerEmail", "");
                            mapData.put("title", "");
                            mapData.put("department", "");
                        }
                        JSONObject jsonSetting = gson.fromJson(rsExtend.getString("text1"), JSONObject.class);
                        if(jsonSetting != null){
                            mapData.put("techFeeActive", jsonSetting.get("techFeeActive"));
                            mapData.put("tipActive", jsonSetting.get("tipActive"));
                            mapData.put("taxActive", jsonSetting.get("taxActive"));
                            mapData.put("meetDriverActive", jsonSetting.get("meetDriverActive"));
                            mapData.put("heavyTrafficActive", jsonSetting.get("heavyTrafficActive"));
                            mapData.put("airportActive", jsonSetting.get("airportActive"));
                            mapData.put("otherFeeActive", jsonSetting.get("otherFeeActive"));
                            mapData.put("rushHourActive", jsonSetting.get("rushHourActive"));
                            mapData.put("tollFeeActive", jsonSetting.get("tollFeeActive"));
                            mapData.put("serviceActive", jsonSetting.get("serviceActive"));
                            mapData.put("sos", jsonSetting.get("sos"));
                            mapData.put("firstRqPaymentType", jsonSetting.get("firstRqPaymentType"));
                            mapData.put("typeRate", jsonSetting.get("typeRate"));
                        } else {
                            mapData.put("techFeeActive", false);
                            mapData.put("tipActive", false);
                            mapData.put("taxActive", false);
                            mapData.put("meetDriverActive", false);
                            mapData.put("heavyTrafficActive", false);
                            mapData.put("airportActive", false);
                            mapData.put("otherFeeActive", false);
                            mapData.put("rushHourActive", false);
                            mapData.put("tollFeeActive", false);
                            mapData.put("serviceActive", false);
                            mapData.put("sos", new ArrayList<>());
                            mapData.put("firstRqPaymentType", null);
                            mapData.put("typeRate", 0);
                        }
                        JSONObject jsonSharing = gson.fromJson(rsExtend.getString("text2"), JSONObject.class);
                        if(jsonSharing != null){
                            mapData.put("isRideSharing", jsonSharing.get("rideSharing"));
                            mapData.put("sharedBooks", jsonSharing.get("sharedBooks"));
                        } else {
                            mapData.put("isRideSharing", false);
                            mapData.put("sharedBooks", new ArrayList<>());
                        }
                        JSONObject jsonReferral = gson.fromJson(rsExtend.getString("varchar4"), JSONObject.class);
                        if(jsonReferral != null){
                            mapData.put("referralCode", jsonReferral.get("referralCode"));
                            mapData.put("referralEarning", jsonReferral.get("referralEarning"));
                            mapData.put("totalEarning", jsonReferral.get("totalEarning"));
                        } else {
                            mapData.put("referralCode", "");
                            mapData.put("referralEarning", 0.0);
                            mapData.put("totalEarning", 0.0);
                        }

                        String driverVehicleInfo = rsExtend.getString("varchar6") != null ? rsExtend.getString("varchar6") : "";
                        if (!driverVehicleInfo.isEmpty()) {
                            try {
                                JSONObject objInfo = gson.fromJson(driverVehicleInfo, JSONObject.class);
                                if (objInfo != null) {
                                    mapData.put("driverVehicleID", objInfo.get("driverVehicleID"));
                                    mapData.put("driverVehicleType", objInfo.get("driverVehicleType"));
                                }
                            } catch (Exception ignore) {}

                        }
                        JSONObject jsonDriver = gson.fromJson(rsExtend.getString("varchar8"), JSONObject.class);
                        if(jsonDriver != null){
                            mapData.put("driverNumberType", jsonDriver.get("idType"));
                        } else {
                            mapData.put("driverNumberType", false);
                        }
                    }
                }

                QupTicketextraRecord qupTicketextraRecord = sqlDao.getTicketExtra(requestId, bookId);
                String adjustPrice = "";
                if(qupTicketextraRecord != null) {
                    ResultSet rsExtra = qupTicketextraRecord.intoResultSet();
                    while (rsExtra.next()) {
                        JSONObject jsonPassenger = gson.fromJson(rsExtra.getString("text1"), JSONObject.class);
                        if(jsonPassenger != null){
                            mapData.put("passengerAvatar", jsonPassenger.get("passengerAvatar"));
                            mapData.put("isVipPassenger", jsonPassenger.get("isVipPassenger"));
                        } else {
                            mapData.put("passengerAvatar", "");
                            mapData.put("isVipPassenger", false);
                        }

                        mapData.put("intercityInfo", rsExtra.getString("text2"));
                        JSONObject jsonBooking = gson.fromJson(rsExtra.getString("text0"), JSONObject.class);
                        if(jsonBooking != null){
                            mapData.put("pickupAddressDetails", jsonBooking.get("pickupAddressDetails") != null ? jsonBooking.get("pickupAddressDetails") : "");
                            mapData.put("destinationAddressDetails", jsonBooking.get("destinationAddressDetails") != null ? jsonBooking.get("destinationAddressDetails") : "");
                        } else {
                            mapData.put("pickupAddressDetails", "");
                            mapData.put("destinationAddressDetails", "");
                        }

                        adjustPrice = rsExtra.getString("text4");
                    }
                }

                // add adjust data
                double addOnPrice = 0.0;
                double oldSubTotal = 0.0;
                if (!adjustPrice.isEmpty()) {
                    JSONObject objAdjust = gson.fromJson(adjustPrice, JSONObject.class);
                    addOnPrice = Double.parseDouble(objAdjust.get("addOnPrice").toString());
                    oldSubTotal = Double.parseDouble(objAdjust.get("oldSubTotal").toString());
                }
                mapData.put("addOnPrice", addOnPrice);
                mapData.put("oldSubTotal", oldSubTotal);

                // get driver avatar from Mongo
                try {
                    Account driver = mongoDao.getAccount(mapData.get("driverId").toString());
                    if (driver != null) {
                        mapData.put("avatar", driver.avatar != null ? driver.avatar : "");
                    }
                } catch (Exception ignore) {

                }

                // return outstanding amount
                String paymentStatus = "full";
                double originalOutstandingAmount = 0.0;
                double currentOutstandingAmount = 0.0;
                boolean isPending = Boolean.parseBoolean(mapData.get("isPending").toString());
                double total = Double.parseDouble(mapData.get("total").toString());
                double totalCharged = Double.parseDouble(mapData.get("totalCharged").toString());
                if (isPending) {
                    paymentStatus = "pending";
                    originalOutstandingAmount = total;
                    currentOutstandingAmount = total;
                    // reset total charged amount
                    totalCharged = 0.0;
                } else if (total > totalCharged) {
                    paymentStatus = "partial";
                    originalOutstandingAmount = total - totalCharged;
                    currentOutstandingAmount = total - totalCharged;
                }
                mapData.put("paymentStatus", paymentStatus);
                mapData.put("originalOutstandingAmount", originalOutstandingAmount);
                mapData.put("currentOutstandingAmount", currentOutstandingAmount);
                // reset total charged amount
                mapData.remove("totalCharged");
                mapData.put("totalCharged", totalCharged);

                String ticket = gson.toJson(mapData).replace("\\", "");
                ticket = ticket.replace(".0\"", "\"");
                body = body + "{ \"id\" : \"" + bookId + "\" , \"type\" : \"Ticket\" , \"body\": " + ticket + " },";
            }
            // remove last comma
            body = body.substring(0, body.length() - 1);
            // add close square bracket
            body = body + "]";
            logger.debug(requestId + " - body: " + body);

            /*HttpResponse<JsonNode> json = Unirest.post(ServerConfig.server_report)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("x-access-token", KeysUtil.TOKEN)
                    .body(body)
                    .asJson();
            logger.debug(requestId + " - MigrateTicket: " + json.getBody().toString());*/
            HttpResponse<String> responseString = Unirest.post(ServerConfig.server_report)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("x-access-token", KeysUtil.TOKEN)
                    .header("x-trace-request-id", requestId)
                    .body(body)
                    .asString();
            String result = responseString.getBody();
            logger.debug(requestId + " - resultStr: " + result);
            //{"success":false,"authenticators":["httpauth","jwt","token"],"message":"All of authenticators were failed"}
            //[{"success":true,...}]
            boolean success;
            try {
                JSONArray storeObj = gson.fromJson(result, JSONArray.class);
                JSONObject object = gson.fromJson(gson.toJson(storeObj.get(0)), JSONObject.class);
                success = object.get("success") != null && Boolean.valueOf(object.get("success").toString());
            } catch (Exception ex) {
                JSONObject object = gson.fromJson(result, JSONObject.class);
                logger.debug(requestId + " - object: " + object.toString());
                success = object.get("success") != null && Boolean.valueOf(object.get("success").toString());
            }
            if (!success) {
                // re-authenticate
                String authBody = "{ \"username\" : \"" + ServerConfig.auth_username + "\" , \"password\" : \"" + ServerConfig.auth_password + "\" }";
                HttpResponse<JsonNode> jsonAuth = Unirest.post(ServerConfig.server_auth)
                        .header("Content-Type", "application/json; charset=utf-8")
                        .body(authBody)
                        .asJson();
                JSONObject objectData = (JSONObject) parser.parse(jsonAuth.getBody().toString());
                KeysUtil.TOKEN = objectData.get("token").toString();
                // submit with new token
                /*json = Unirest.post(ServerConfig.server_report)
                        .header("Content-Type", "application/json; charset=utf-8")
                        .header("x-access-token", KeysUtil.TOKEN)
                        .body(body)
                        .asJson();*/
                responseString = Unirest.post(ServerConfig.server_report)
                        .header("Content-Type", "application/json; charset=utf-8")
                        .header("x-access-token", KeysUtil.TOKEN)
                        .header("x-trace-request-id", requestId)
                        .body(body)
                        .asString();
                logger.debug(requestId + " - MigrateTicket: " + responseString.getBody());
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            logger.debug(requestId + " - MigrateTicket: "+ errors.toString());
        }

    }

}
