package com.qupworld.paymentgateway.controllers.threading;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.controllers.GatewayUtil.GatewayUtil;
import com.qupworld.paymentgateway.controllers.InboxUtil;
import com.qupworld.paymentgateway.controllers.PaymentUtil;
import com.qupworld.paymentgateway.controllers.UpdateDriverCashBalance;
import com.qupworld.paymentgateway.controllers.WalletUtil;
import com.qupworld.paymentgateway.entities.PayoutHistoryEnt;
import com.qupworld.paymentgateway.entities.WalletTransfer;
import com.qupworld.paymentgateway.models.*;
import com.qupworld.paymentgateway.models.mongo.collections.Logs.Logs;
import com.qupworld.paymentgateway.models.mongo.collections.account.Account;
import com.qupworld.paymentgateway.models.mongo.collections.bankInfo.BankInfo;
import com.qupworld.service.MailService;
import com.qupworld.service.QUpReportPublisher;
import com.pg.util.CommonArrayUtils;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.TimezoneUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class PayoutDriverThread extends Thread {

    final static Logger logger = LogManager.getLogger(PayoutDriverThread.class);
    public List<String> listDriver;
    public String payoutDate;
    public String payoutId;
    public String fleetId;
    public String fleetName;
    public String country;
    public String timezone;
    public String currencyISO;
    public String operatorId;
    public String operatorName;
    public String operatorUserName;
    public String email;
    public String companyId;
    public double minPayoutAmount;
    public double holdAmount;
    public int totalDriver;
    public String sessionId;
    public String requestId;
    public String payoutTarget;
    public org.json.JSONArray jsonError;

    @Override
    public void run() {
        MongoDao mongoDao = new MongoDaoImpl();
        SQLDao sqlDao = new SQLDaoImpl();
        RedisDao redisDao = new RedisImpl();
        try {
            Gson gson = new GsonBuilder().serializeNulls().create();
            WalletUtil walletUtil = new WalletUtil(requestId);

            JSONArray arrSuccessReport = new JSONArray(); // contain success payout to send Report

            List<String> arrSuccess = new ArrayList<>();
            double totalPayoutAmount = 0.00;
            for (String driverId : listDriver) {
                try {
                    logger.debug(requestId + " - payout for driver " + driverId);
                    // get current balance before update db
                    // update to Report
                    Account account = mongoDao.getAccount(driverId);
                    if (account != null && account.driverInfo != null) {
                        // check bank number then ignore if not valid
                        String accountNumber = account.driverInfo.accountNumber != null ? account.driverInfo.accountNumber : "";
                        //String textInBank = accountNumber.replaceAll("[0-9 ]", "").trim();
                        String textInBank = accountNumber.replaceAll("[^a-zA-Z]", "").trim();
                        if (payoutTarget.equals(KeysUtil.MOLPAY) && textInBank.length() > 0) {
                            logger.debug(requestId + " - driver " + driverId + " is error: textInBank: " + textInBank);
                            continue;
                        }
                        double currentBalance = walletUtil.getCurrentBalance(driverId, "cash", currencyISO);
                        logger.debug(requestId + " - driverId: " + driverId + " - balance before update: " + currentBalance);
                        if (currentBalance >= minPayoutAmount) {
                            double payoutAmount = Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(currentBalance - holdAmount));
                            if (payoutAmount > 0) {
                                WalletTransfer walletTransfer = new WalletTransfer();
                                walletTransfer.fleetId = fleetId;
                                walletTransfer.driverId = driverId;
                                walletTransfer.amount = payoutAmount;
                                walletTransfer.transferType = CommonArrayUtils.DRIVER_CASH_WALLET_TYPE[6];
                                walletTransfer.referenceId = "payout-" + GatewayUtil.getOrderId();
                                walletTransfer.transactionId = payoutId;
                                walletTransfer.currencyISO = currencyISO;
                                UpdateDriverCashBalance updateCash = new UpdateDriverCashBalance();
                                updateCash.requestId = requestId;
                                updateCash.walletTransfer = walletTransfer;
                                updateCash.mongoDao = mongoDao;
                                updateCash.redisDao = redisDao;
                                updateCash.isDeposit = false;
                                updateCash.pendingBalance = 0.0;
                                double newBalance = updateCash.doUpdate();

                                String driverName = account.firstName + " " + account.lastName;
                                driverName = driverName.trim(); // remove spaces
                                String phone = account.phone != null ? account.phone : "";
                                String email = account.email != null ? account.email : "";
                                String companyId = "";
                                String companyName = "";
                                if (account.driverInfo.company != null) {
                                    companyId = account.driverInfo.company.companyId != null ? account.driverInfo.company.companyId : "";
                                    companyName = account.driverInfo.company.name != null ? account.driverInfo.company.name : "";
                                }
                                String driverType = account.driverInfo.driverType != null ? account.driverInfo.driverType : "";
                                String driverNumber = account.driverInfo.drvId != null ? account.driverInfo.drvId : "";
                                String driverNumberType = account.driverInfo.idType != null ? account.driverInfo.idType : "";
                                String bankAccountHolder = account.driverInfo.nameOfAccount != null ? account.driverInfo.nameOfAccount : "";
                                String bankName = "";
                                String bankCode = "";
                                if (payoutTarget.equals(KeysUtil.MOLPAY)) {
                                    bankCode = account.driverInfo.routingNumber != null ? account.driverInfo.routingNumber : "";
                                    if (!bankCode.isEmpty()) {
                                        BankInfo bankInfo = mongoDao.getBankInfoByBankCode(KeysUtil.MOLPAY, bankCode, country);
                                        if (bankInfo != null) {
                                            bankName = bankInfo.bankName != null ? bankInfo.bankName : "";
                                        } else {
                                            // invalid bank
                                            bankCode = "";
                                        }
                                    }
                                } else {
                                    // try to get bank name from profile
                                    bankName = account.driverInfo != null && account.driverInfo.nameOfBank != null ? account.driverInfo.nameOfBank : "";
                                }
                                String IFSCCode = "";
                                if (payoutTarget.equals(KeysUtil.INDIA_DEFAULT)) {
                                    IFSCCode = account.driverInfo.IFSCCode != null ? account.driverInfo.IFSCCode : "";
                                }
                                boolean isBankAccountOwner = account.driverInfo.isBankAccountOwner == null || account.driverInfo.isBankAccountOwner;
                                String beneficiaryIDIC = account.driverInfo.beneficiaryIDIC != null ? account.driverInfo.beneficiaryIDIC : "";
                                int bankRelationship = account.driverInfo.bankRelationship != null ? account.driverInfo.bankRelationship : 0;
                                String relationshipOtherName = account.driverInfo.relationshipOtherName != null ? account.driverInfo.relationshipOtherName : "";

                                PayoutHistoryEnt payoutHistoryEnt = new PayoutHistoryEnt();
                                payoutHistoryEnt.fleetId = fleetId;
                                payoutHistoryEnt.payTo = "driver";
                                payoutHistoryEnt.driverId = driverId;
                                payoutHistoryEnt.driverName = driverName;
                                payoutHistoryEnt.phone = phone;
                                payoutHistoryEnt.email = email;
                                payoutHistoryEnt.companyId = companyId;
                                payoutHistoryEnt.companyName = companyName;
                                payoutHistoryEnt.driverType = driverType;
                                payoutHistoryEnt.driverNumber = driverNumber;
                                payoutHistoryEnt.driverNumberType = driverNumberType;
                                payoutHistoryEnt.operatorId = operatorId;
                                payoutHistoryEnt.operatorName = operatorName;
                                payoutHistoryEnt.transactionType = bankName.toLowerCase().contains("rhb") ? "Rhb" : "Ibg";
                                payoutHistoryEnt.bopIndicator = "Resident";
                                payoutHistoryEnt.paidAmount = payoutAmount;
                                payoutHistoryEnt.newBalance = newBalance;
                                payoutHistoryEnt.currencyISO = currencyISO;
                                payoutHistoryEnt.bankAccountHolder = bankAccountHolder;
                                payoutHistoryEnt.accountNumber = accountNumber;
                                payoutHistoryEnt.bankName = bankName;
                                payoutHistoryEnt.bankCode = bankCode;
                                payoutHistoryEnt.IFSCCode = IFSCCode;
                                payoutHistoryEnt.payoutId = payoutId;
                                payoutHistoryEnt.payoutDate = new Timestamp(KeysUtil.SDF_DMYHMSTZ.parse(payoutDate).getTime());
                                payoutHistoryEnt.createdDate = TimezoneUtil.getGMTTimestamp();
                                payoutHistoryEnt.isBankAccountOwner = isBankAccountOwner;
                                payoutHistoryEnt.beneficiaryIDIC = beneficiaryIDIC;
                                payoutHistoryEnt.bankRelationship = bankRelationship;
                                payoutHistoryEnt.relationshipOtherName = relationshipOtherName;

                                SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
                                String transactionId = sdf.format(TimezoneUtil.offsetTimeZone(payoutHistoryEnt.payoutDate, "GMT", timezone));
                                payoutHistoryEnt.transactionId = transactionId;

                                // Save to MySQL
                                long idHistory = sqlDao.addPayoutHistory(payoutHistoryEnt);
                                if (idHistory > 0) {
                                    // generate data send to Report
                                    payoutHistoryEnt.transactionId = transactionId + "-" + String.valueOf(idHistory);
                                    String payoutHistoryStr = gson.toJson(payoutHistoryEnt);
                                    JSONObject payoutHistoryJSON = gson.fromJson(payoutHistoryStr, JSONObject.class);
                                    payoutHistoryJSON.remove("createdDate");
                                    payoutHistoryJSON.put("createdDate", TimezoneUtil.formatISODate(payoutHistoryEnt.createdDate, "GMT"));
                                    payoutHistoryJSON.remove("payoutDate");
                                    payoutHistoryJSON.put("payoutDate", TimezoneUtil.formatISODate(payoutHistoryEnt.payoutDate, "GMT"));

                                    JSONObject objReport = new JSONObject();
                                    objReport.put("type", "PayoutHistory");
                                    objReport.put("id", String.valueOf(idHistory));
                                    objReport.put("body", payoutHistoryJSON);
                                    arrSuccessReport.add(objReport);
                                    arrSuccess.add(driverId);
                                    totalPayoutAmount += payoutAmount;

                                    //send inbox driver payout
                                    System.out.println(requestId + " - " + "sendInboxDriverPayout");
                                    InboxUtil inboxUtil = new InboxUtil(requestId);
                                    inboxUtil.action = "sendInboxDriverPayout";
                                    inboxUtil.userId = walletTransfer.driverId;
                                    inboxUtil.currencyISO = walletTransfer.currencyISO;
                                    inboxUtil.referenceId = walletTransfer.referenceId;
                                    inboxUtil.amount = walletTransfer.amount;
                                    inboxUtil.bankAccount = accountNumber.length() > 4 ? accountNumber.substring(accountNumber.length() - 4) : accountNumber;
                                    inboxUtil.reason = "Payout";
                                    inboxUtil.start();
                                } else {
                                    logger.debug(requestId + " - driver " + driverId + " is error: cannot update database");
                                }
                            } else {
                                logger.debug(requestId + " - driver " + driverId + " is error: payoutAmount <= 0");
                            }
                        } else {
                            logger.debug(requestId + " - driver " + driverId + " is error: cash balance < payoutAmount");
                        }
                    } else {
                        logger.debug(requestId + " - driver " + driverId + " is error: account is not valid");
                    }
                } catch (Exception ex) {
                    StringWriter errors = new StringWriter();
                    ex.printStackTrace(new PrintWriter(errors));
                    logger.debug(requestId + " - payout for driver " + driverId + " exception: " + errors.toString());
                }
            }

            // in case payout for some specific merchants in the list
            // remove drivers were paid successfully out of cache
            Date date = KeysUtil.SDF_DMYHMSTZ.parse(payoutDate);
            String cacheKey = String.valueOf(date.getTime());
            String cachedData = redisDao.getTmp(requestId, cacheKey+"-listIds");
            List<String> cachedDriver = Arrays.asList(cachedData.split(","));
            List<String> newCache = new ArrayList<>();
            for (String driverId : cachedDriver) {
                 if (!arrSuccess.contains(driverId))
                     newCache.add(driverId);
            }
            redisDao.overrideTmpData(requestId, cacheKey+"-listIds", String.join(",", newCache));

            // clear cache
            if (!sessionId.isEmpty()) {
                redisDao.removeTmp(requestId, sessionId + "-payoutToDriverObj");
                redisDao.removeTmp(requestId, sessionId + "-basketId");
                redisDao.removeTmp(requestId, sessionId + "-paymentIds");
            }
            // send success payout to Report
            if (arrSuccessReport.size() > 0) {
                // separate to multiple threads
                int count = arrSuccessReport.size()/100;
                int rest = arrSuccessReport.size()%100;
                if (rest > 0) count += 1;

                for (int i = 0; i < count; i++) {
                    JSONArray arrReport = new JSONArray();
                    for (int j = 0; j < 100; j++) {
                        int index = i*100 + j;
                        if (index < arrSuccessReport.size()) {
                            arrReport.add(arrSuccessReport.get(index));
                        } else {
                            break;
                        }
                    }
                    QUpReportPublisher reportPublisher = QUpReportPublisher.getInstance();
                    reportPublisher.sendListItems(requestId, arrReport.toJSONString());
                }

                // send notify to Jupiter > CC
                JSONObject objData = new JSONObject();
                objData.put("fleetId", fleetId);
                objData.put("status", "success");
                objData.put("type", "driver");
                objData.put("listId", arrSuccess);
                logger.debug(requestId + " - URL: " + PaymentUtil.payout_dnb);
                logger.debug(requestId + " - data: " + objData.toJSONString());
                String jupiterResponse = CommonUtils.sendJupiter(requestId, objData.toJSONString(), PaymentUtil.payout_dnb,
                        ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
                logger.debug(requestId + " - jupiterResponse: " + jupiterResponse);
            }

            //convert payout date to fleet timezone before send mail
            String pd = KeysUtil.SDF_DMYHMS.format(TimezoneUtil.offsetTimeZone(KeysUtil.SDF_DMYHMSTZ.parse(payoutDate), "GMT", timezone));

            // send mail payout result
            try {
                int totalFailed = totalDriver - arrSuccess.size();
                MailService mailService = MailService.getInstance();
                mailService.sendMailPayoutCompleted(requestId, "driver", fleetId, fleetName , pd, arrSuccess.size(), totalFailed,
                        Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(totalPayoutAmount)), email.split(";"), jsonError);
            } catch(Exception e) {
                e.printStackTrace();
            }

            //save operator logs
            try {
                Currency currency = Currency.getInstance(currencyISO);
                DecimalFormat nf = (DecimalFormat) NumberFormat.getCurrencyInstance(Locale.US);
                nf.setCurrency(currency);
                Logs logs = new Logs();
                logs._id = new ObjectId();
                logs.fleetId = fleetId;
                logs.userId = operatorId;
                logs.fullName = operatorName;
                logs.userName = operatorUserName;
                logs.module = "Driver payout";
                logs.action = "Pay";
                logs.description = "Payout to: " + listDriver.size() + " drivers. " +
                        "Min payout: " + nf.format(minPayoutAmount) + ". " +
                        "Hold amount: " + nf.format(holdAmount) + ". " +
                        "Total payout: " + nf.format(Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(totalPayoutAmount))) + ". " +
                        "Payout end time: " + pd + ".";
                logs.companyId = companyId;
                logs.createdDate = new Date();
                logs.latestUpdate = new Date();
                mongoDao.addLogs(logs);

                String json = gson.toJson(logs);
                JSONObject jsonObject = (JSONObject) new JSONParser().parse(json);
                Calendar calendar = Calendar.getInstance();
                jsonObject.put("createdDate",
                        TimezoneUtil.formatISODate(TimezoneUtil.offsetTimeZone(logs.createdDate, calendar.getTimeZone().getID(), "GMT"), "GMT"));
                jsonObject.put("latestUpdate",
                        TimezoneUtil.formatISODate(TimezoneUtil.offsetTimeZone(logs.createdDate, calendar.getTimeZone().getID(), "GMT"), "GMT"));
                jsonObject.remove("_id");

                //send data logs to report module
                QUpReportPublisher reportPublisher = QUpReportPublisher.getInstance();
                reportPublisher.sendOneItem(requestId, logs._id.toString(), "Logs", jsonObject.toJSONString());
            }catch (Exception e){
                e.printStackTrace();
            }


        } catch(Exception ex) {
            redisDao.unlockPayout(requestId, fleetId);
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            logger.debug(requestId + " - PayoutDriverThread: "+ errors.toString());
        } finally {
            // remove lock payout after completed
            redisDao.unlockPayout(requestId, fleetId);
        }

    }

}
