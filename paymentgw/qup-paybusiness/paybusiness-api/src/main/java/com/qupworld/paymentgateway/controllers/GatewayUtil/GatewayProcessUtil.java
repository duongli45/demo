package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.google.gson.Gson;
import com.qupworld.paymentgateway.controllers.PaymentUtil;
import com.qupworld.paymentgateway.entities.CreditEnt;
import com.qupworld.paymentgateway.entities.PaymentEnt;
import com.qupworld.paymentgateway.models.*;
import com.qupworld.paymentgateway.models.mongo.collections.Credit;
import com.qupworld.paymentgateway.models.mongo.collections.account.Account;
import com.qupworld.paymentgateway.models.mongo.collections.booking.Booking;
import com.qupworld.paymentgateway.models.mongo.collections.corporate.Corporate;
import com.qupworld.paymentgateway.models.mongo.collections.gateway.*;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by thuanho on 07/07/2022.
 */
public class GatewayProcessUtil {

    final static Logger logger = LogManager.getLogger(GatewayProcessUtil.class);
    private String requestId;
    private MongoDao mongoDao;
    private Gson gson;
    private PaymentUtil paymentUtil;
    private PaymentEnt paymentEnt;

    public GatewayProcessUtil(String requestId, PaymentEnt paymentEnt) {
        this.requestId = requestId;
        mongoDao = new MongoDaoImpl();
        gson = new Gson();
        paymentUtil = new PaymentUtil();
        this.paymentEnt = paymentEnt;
    }

    public String payToken(String gateway, String fleetId, String bookId,
                           double amount, String token, String currencyISO, JSONObject objData) {
        ObjResponse response = new ObjResponse();
        response.returnCode = 525;
        try {
            String result;
            Booking booking = mongoDao.getBooking(bookId);
            switch (gateway) {
                case KeysUtil.STRIPE : {
                    StripeUtil stripeUtil = new StripeUtil(requestId);
                    GatewayStripe gatewayStripe = mongoDao.getGatewayStripeFleet(fleetId);
                    result = stripeUtil.createPaymentWithCreditToken(fleetId, bookId, currencyISO, amount, token, gatewayStripe.secretKey);
                }
                break;
                case KeysUtil.PINGPONG:{
                    //Get gateway Information
                    GatewayPingPong gatewayFleet = mongoDao.getGatewayPingPongFleet(fleetId);
                    GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.PINGPONG, gatewayFleet.environment);
                    Boolean isSandbox = gatewayFleet.environment.equalsIgnoreCase(KeysUtil.SANDBOX);
                    PingPongUtil pingPongUtil = new PingPongUtil(gatewayFleet.clientId, gatewayFleet.accId, gatewayFleet.salt, isSandbox, gatewayURL.url, paymentEnt.requestId);
                    String billAddress = "";
                    String billCity = "";
                    String billState = "";
                    String billPostalCode = "";
                    String billCountry = "";
                    String cardMask = "";
                    String payerId = objData.get("payerId").toString();
                    if (!payerId.isEmpty()) {
                        List<Credit> credits;
                        Account account = mongoDao.getAccount(payerId);
                        if (account != null) {
                            credits = account.credits;
                        } else {
                            Corporate corporate = mongoDao.getCorporate(payerId);
                            if (corporate != null) {
                                credits = corporate.credits;
                            } else {
                                credits = null;
                            }
                        }
                        if (credits != null && !credits.isEmpty()) {
                            for (Credit credit : credits) {
                                if (token.equals(credit.localToken)) {
                                    cardMask = credit.cardMask != null ? credit.cardMask : "";
                                    billAddress = credit.street != null ? credit.street : "";
                                    billCity = credit.city != null ? credit.city : "";
                                    billState = credit.state != null ? credit.state : "";
                                    billPostalCode = credit.zipCode != null ? credit.zipCode : "";
                                    billCountry = credit.country != null ? credit.country : "";
                                    break;
                                }
                            }
                        }
                    }

                    String type = "SALE";
                    Map<String,String> industryData = new HashMap<>();
                    String vehicleType = booking.drvInfo != null && booking.drvInfo.vehicleType != null ? booking.drvInfo.vehicleType : "Chauffeur";
                    String destinationAddress = booking.request.destination != null && booking.request.destination.address != null ?
                            booking.request.destination.address : "";
                    double distance = booking.droppedOffInfo != null && booking.droppedOffInfo.distanceTour != null ?
                            booking.droppedOffInfo.distanceTour: 0.0;
                    industryData.put("carModel", vehicleType);
                    industryData.put("destinationAddress", destinationAddress);
                    industryData.put("mileage", String.valueOf(distance));
                    String cardType = objData.get("cardType").toString();
                    String payerPhone = objData.get("payerPhone").toString();
                    String payerEmail = objData.get("payerEmail").toString();
                    String payerFirstName = objData.get("payerFirstName").toString();
                    String payerLastName = objData.get("payerLastName").toString();
                    result = pingPongUtil.payByToken(type, paymentEnt.bookId, paymentEnt.total, currencyISO, token, cardMask, cardType,
                            payerId, payerFirstName, payerLastName, payerPhone, payerEmail,
                            billAddress, billCity, billState, billPostalCode, billCountry, industryData);
                }
                break;
                case KeysUtil.YEEPAY: {
                    //Get gateway Information
                    GatewayYeepay gatewayFleet = mongoDao.getGatewayYeepayFleet(fleetId);
                    YeepayUtil yeepayUtil = new YeepayUtil();
                    GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.YEEPAY, gatewayFleet.environment);
                    CreditEnt creditUser = new CreditEnt();
                    String phone = "";
                    String cardMask = "";
                    String payerId = objData.get("payerId").toString();
                    if (!payerId.isEmpty()) {
                        List<Credit> credits;
                        Account account = mongoDao.getAccount(payerId);
                        if (account != null) {
                            credits = account.credits;
                            phone = account.phone;
                        } else {
                            Corporate corporate = mongoDao.getCorporate(payerId);
                            if (corporate != null) {
                                credits = corporate.credits;
                                phone = corporate.adminAccount != null && corporate.adminAccount.phone != null ? corporate.adminAccount.phone : "";
                            } else {
                                credits = null;
                            }
                        }
                        if (credits != null && !credits.isEmpty()) {
                            for (Credit credit : credits) {
                                if (token.equals(credit.localToken)) {
                                    String creditPhone = credit.creditPhone != null ? credit.creditPhone : "";
                                    if (!creditPhone.isEmpty())
                                        phone = creditPhone;
                                    cardMask = credit.cardMask != null ? credit.cardMask : "";
                                    break;
                                }
                            }
                        }
                    }
                    creditUser.phone = phone;
                    creditUser.cardNumber = cardMask;
                    result = yeepayUtil.createPaymentWithCreditToken(paymentEnt.requestId, paymentEnt.bookId, creditUser, paymentEnt.total, gatewayFleet.merchantId, gatewayFleet.key, gatewayFleet.terminalId, gatewayURL.url, currencyISO);

                }
                break;
                case KeysUtil.PAYWAY: {
                    String getConfigInfo = mongoDao.getConfigInfo(fleetId, "Gateway"+KeysUtil.PAYWAY);
                    GatewayPayWay gatewayFleet = gson.fromJson(getConfigInfo, GatewayPayWay.class);
                    GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.PAYWAY, gatewayFleet.environment);
                    PayWayUtil payWayUtil = new PayWayUtil(requestId, gatewayFleet.merchantId, gatewayFleet.apiKey, gatewayFleet.publicKey, gatewayFleet.skipAuthenAmount, gatewayURL.url);
                    String orderId = GatewayUtil.getOrderId();
                    Map<String,String> mapData = new HashMap<>();
                    mapData.put("firstName", objData.get("firstName").toString());
                    mapData.put("lastName", objData.get("lastName").toString());
                    mapData.put("cardType", objData.get("cardType").toString());
                    mapData.put("cardMask", objData.get("cardMask").toString());
                    result = payWayUtil.payByToken(orderId, paymentEnt.bookId, "remain-partial", token, amount, booking.currencyISO, mapData);
                }
                break;
                default: {
                    ObjResponse failureResponse = new ObjResponse();
                    failureResponse.returnCode = 453;
                    result = failureResponse.toString();
                }
            }
            logger.debug(requestId + " - preAuthCapture result: " + result);
            return result;
        } catch (Exception ex) {
            logger.debug(requestId + " - payToken exception: " + CommonUtils.getError(ex));
        }
        return response.toString();
    }

    public String preAuthPayment(String gateway, String fleetId, String bookId,
                                 double amount, String token, String currencyISO, JSONObject objData) {
        ObjResponse response = new ObjResponse();
        response.returnCode = 525;
        try {
            String result;
            switch (gateway) {
                case KeysUtil.STRIPE : {
                    GatewayStripe gatewayStripeFleet = mongoDao.getGatewayStripeFleet(fleetId);
                    StripeUtil stripeUtil = new StripeUtil(requestId);
                    result = stripeUtil.preAuthPayment(fleetId ,bookId, gatewayStripeFleet.secretKey, token, amount, currencyISO);
                }
                break;
                case KeysUtil.PAYWAY : {
                    String getConfigInfo = mongoDao.getConfigInfo(fleetId, "Gateway"+KeysUtil.PAYWAY);
                    GatewayPayWay gatewayFleet = gson.fromJson(getConfigInfo, GatewayPayWay.class);
                    GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.PAYWAY, gatewayFleet.environment);
                    PayWayUtil payWayUtil = new PayWayUtil(requestId, gatewayFleet.merchantId, gatewayFleet.apiKey, gatewayFleet.publicKey, gatewayFleet.skipAuthenAmount, gatewayURL.url);
                    Map<String,String> mapData = new HashMap<>();
                    mapData.put("firstName", objData.get("firstName").toString());
                    mapData.put("lastName", objData.get("lastName").toString());
                    mapData.put("cardType", objData.get("cardType").toString());
                    mapData.put("cardMask", objData.get("cardMask").toString());
                    String orderId = GatewayUtil.getOrderId();
                    result = payWayUtil.payByToken(orderId, bookId, "extra", token, amount, currencyISO, mapData);
                }
                break;
                default: {
                    ObjResponse failureResponse = new ObjResponse();
                    failureResponse.returnCode = 453;
                    result = failureResponse.toString();
                }
            }
            logger.debug(requestId + " - preAuthPayment result: " + result);
            return result;
        } catch (Exception ex) {
            logger.debug(requestId + " - preAuthPayment exception: " + CommonUtils.getError(ex));
        }
        return response.toString();
    }

    public String preAuthCapture(String gateway, String fleetId, String bookId,
                                 String token, double captureValue, String authId, String currencyISO, JSONObject objData) {
        ObjResponse response = new ObjResponse();
        response.returnCode = 525;
        try {
            String result;
            switch (gateway) {
                case KeysUtil.STRIPE : {
                    GatewayStripe gatewayStripe = mongoDao.getGatewayStripeFleet(fleetId);
                    StripeUtil stripeUtil = new StripeUtil(requestId);
                    result = stripeUtil.preAuthCapture(fleetId, bookId, gatewayStripe.secretKey, token, captureValue, authId, currencyISO);
                }
                break;
                case KeysUtil.PAYWAY : {
                    String getConfigInfo = mongoDao.getConfigInfo(fleetId, "Gateway"+KeysUtil.PAYWAY);
                    GatewayPayWay gatewayFleet = gson.fromJson(getConfigInfo, GatewayPayWay.class);
                    GatewayURL gatewayURL = mongoDao.getGatewayURL(KeysUtil.PAYWAY, gatewayFleet.environment);
                    PayWayUtil payWayUtil = new PayWayUtil(requestId, gatewayFleet.merchantId, gatewayFleet.apiKey, gatewayFleet.publicKey, gatewayFleet.skipAuthenAmount, gatewayURL.url);
                    Map<String,String> mapData = new HashMap<>();
                    mapData.put("firstName", objData.get("firstName").toString());
                    mapData.put("lastName", objData.get("lastName").toString());
                    mapData.put("cardType", objData.get("cardType").toString());
                    mapData.put("cardMask", objData.get("cardMask").toString());
                    result = payWayUtil.preAuthCapture(authId, captureValue, mapData);
                }
                break;
                default: {
                    ObjResponse failureResponse = new ObjResponse();
                    failureResponse.returnCode = 453;
                    result = failureResponse.toString();
                }
            }
            logger.debug(requestId + " - preAuthCapture result: " + result);
            return result;
        } catch (Exception ex) {
            logger.debug(requestId + " - preAuthCapture exception: " + CommonUtils.getError(ex));
        }
        return response.toString();
    }

    public String voidTransaction(String gateway, String fleetId, String bookId, String authId, double amount, String currencyISO, boolean partialVoid) {
        ObjResponse response = new ObjResponse();
        response.returnCode = 525;
        try {
            String result;
            switch (gateway) {
                case KeysUtil.STRIPE : {
                    GatewayStripe gatewayStripe = mongoDao.getGatewayStripeFleet(fleetId);
                    StripeUtil stripeUtil = new StripeUtil(requestId);
                    if (partialVoid)
                        result = stripeUtil.voidStripeTransaction(requestId, fleetId, bookId, gatewayStripe.secretKey, authId);
                    else {
                        result = stripeUtil.voidStripeTransaction(requestId, fleetId, bookId, currencyISO, amount, gatewayStripe.secretKey, authId);
                    }
                }
                break;
                default: {
                    ObjResponse failureResponse = new ObjResponse();
                    failureResponse.returnCode = 453;
                    result = failureResponse.toString();
                }
            }
            logger.debug(requestId + " - preAuthCapture result: " + result);
            return result;
        } catch (Exception ex) {
            logger.debug(requestId + " - preAuthCapture exception: " + CommonUtils.getError(ex));
        }
        return response.toString();
    }

    public boolean checkPreAuthStatus(String gateway, String fleetId, String authId) {
        try {
            switch (gateway) {
                case KeysUtil.STRIPE : {
                    GatewayStripe gatewayStripe = mongoDao.getGatewayStripeFleet(fleetId);
                    StripeUtil stripeUtil = new StripeUtil(requestId);
                    String status = stripeUtil.getTransactionStatus(requestId, gatewayStripe.secretKey, authId);
                    return status.equals("requires_capture");
                }
                default: {
                    return true;
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - checkPreAuthStatus exception: " + CommonUtils.getError(ex));
            return true;
        }
    }
}
