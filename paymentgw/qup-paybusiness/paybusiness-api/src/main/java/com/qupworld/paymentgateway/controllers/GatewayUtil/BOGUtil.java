package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.qupworld.config.ServerConfig;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import okhttp3.*;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by thuanho on 24/08/2022.
 */
public class BOGUtil {

    private final static Logger logger = LogManager.getLogger(BOGUtil.class);
    private String requestId;
    private String CLIENT_ID;
    private String CLIENT_SECRET;
    private String SERVER_URL;
    private String PAY_URL;
    private String AUTH_URL;
    private boolean IS_SANDBOX;

    public static void main(String[] args) throws Exception {
        /*BOGUtil bogUtil = new BOGUtil("", "30242", "62af2bafc31511293f6c5cba8f30c31d",
                "https://ipay.ge/opay/api/v1", "/checkout/orders", "/oauth2/token", KeysUtil.SANDBOX);*/

        /*String authen = bogUtil.getAuthen();
        logger.debug(" - authen: " + authen);*/

        /*String getURL = bogUtil.getCheckoutURL(1.0, CommonUtils.getOrderId(), "Booking", "GEL");
        logger.debug(" - getURL: " + getURL);*/

        //String transId = "45d400d3adde867fc05aaf1db2f28bf8c17548e9";
        /*String transId = "15f783820233491563bea2dc4005b2e6d2263ceb";
        String status = bogUtil.getPaymentStatus(transId);
        logger.debug(" - status: " + status);*/

        BOGUtil bogUtil = new BOGUtil("", "30242", "62af2bafc31511293f6c5cba8f30c31d",
                "https://ipay.ge/opay/api/v1", "/checkout/orders", "/oauth2/token", "production");
        String orderId = "aead4e68cd122744e55f0c0fecd4a2a31b1166f4";
        bogUtil.refund(orderId);
    }


    public BOGUtil(String _requestId, String clientId, String clientSecret, String serverURL, String payURL, String authURL, String environment) {
        requestId = _requestId;
        CLIENT_ID = clientId;
        CLIENT_SECRET = clientSecret;
        SERVER_URL = serverURL;
        PAY_URL = payURL;
        AUTH_URL = authURL;
        IS_SANDBOX = environment.equals(KeysUtil.SANDBOX);
    }

    public String getCheckoutURL(double amount, String orderId, String type, String currencyISO) {
        ObjResponse response = new ObjResponse();
        response.returnCode = 525;
        try {
            logger.debug(requestId + " - orderId: " + orderId);
            String callbackUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.BOG;

            JSONObject requestParams = new JSONObject();
            String intent = type.equals("Register") ? "AUTHORIZE" : "CAPTURE";
            requestParams.put("intent", intent);
            requestParams.put("redirect_url", callbackUrl);
            requestParams.put("capture_method", "AUTOMATIC");
            requestParams.put("locale", "en-US");

            JSONArray arrPurchase = new JSONArray();
            JSONObject jsonPurchase = new JSONObject();
            jsonPurchase.put("industry_type", "ECOMMERCE");
            JSONObject jsonAmount = new JSONObject();
            jsonAmount.put("currency_code", currencyISO);
            jsonAmount.put("value", KeysUtil.DECIMAL_FORMAT0.format(amount));
            jsonPurchase.put("amount", jsonAmount);
            arrPurchase.put(jsonPurchase);
            requestParams.put("purchase_units", arrPurchase);

            JSONArray arrItem = new JSONArray();
            JSONObject jsonItem = new JSONObject();
            jsonItem.put("amount", KeysUtil.DECIMAL_FORMAT0.format(amount));
            jsonItem.put("description", type);
            jsonItem.put("product_id", orderId);
            arrItem.put(jsonItem);
            requestParams.put("items", arrItem);

            String post_data = requestParams.toString();
            logger.debug(requestId + " - post_data: " + post_data);

            String requestURL = SERVER_URL + PAY_URL;
            logger.debug(requestId + " - requestURL: " + requestURL);
            URL url = new URL(requestURL);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Basic " + getAuthen());
            connection.setDoOutput(true);

            DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
            requestWriter.writeBytes(post_data);
            requestWriter.close();
            String responseData = "";
            InputStream is = connection.getInputStream();
            BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
            if ((responseData = responseReader.readLine()) != null) {
                logger.debug(requestId + " - response: " + responseData);
            }
            responseReader.close();
            /*
            {
                "status": "CREATED",
                "payment_hash": "9ffd692260c4e28b25b77682dd36beb87c3aa9d9",
                "links": [
                    {
                        "href": "https://ipay.ge/opay/api/v1/checkout/orders/16b0dafcfeeb23c38036aa666367c30295098e44",
                        "rel": "self",
                        "method": "GET"
                    },
                    {
                        "href": "https://ipay.ge/?order_id=16b0dafcfeeb23c38036aa666367c30295098e44&locale=en-US",
                        "rel": "approve",
                        "method": "REDIRECT"
                    }
                ],
                "order_id": "16b0dafcfeeb23c38036aa666367c30295098e44"
            }
             */
            JSONObject jsonResponse = new JSONObject(responseData);
            String transactionId = jsonResponse.has("order_id") ? jsonResponse.getString("order_id") : "";
            String checkoutURL = "";
            JSONArray arrLink = jsonResponse.has("links") ? jsonResponse.getJSONArray("links") : new JSONArray();
            for (int i = 0; i < arrLink.length(); i++) {
                JSONObject jsonLink = arrLink.getJSONObject(i);
                String method = jsonLink.has("method") ? jsonLink.getString("method") : "";
                if (method.equals("REDIRECT")) {
                    checkoutURL = jsonLink.has("href") ? jsonLink.getString("href") : "";
                }
            }
            if (!checkoutURL.isEmpty()) {
                response.returnCode = 200;
                Map<String, String> mapResponse = new HashMap<>();
                mapResponse.put("3ds_url", checkoutURL);
                mapResponse.put("type", "link");
                mapResponse.put("transactionId", transactionId);
                response.response = mapResponse;
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }
        return response.toString();
    }

    public String getPaymentStatus(String transactionId) {
        ObjResponse response = new ObjResponse();
        response.returnCode = 525;
        try {
            logger.debug(requestId + " - transactionId: " + transactionId);

            String requestURL = SERVER_URL + PAY_URL + "/" + transactionId;
            logger.debug(requestId + " - requestURL: " + requestURL);
            URL url = new URL(requestURL);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Basic " + getAuthen());

            String responseData = "";
            InputStream is = connection.getInputStream();
            BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
            if ((responseData = responseReader.readLine()) != null) {
                logger.debug(requestId + " - response: " + responseData);
            }
            responseReader.close();
            /*
            {
                "id": "45d400d3adde867fc05aaf1db2f28bf8c17548e9",
                "status": "PERFORMED",
                "intent": "CAPTURE",
                "payer": {
                    "name": null,
                    "email_address": null,
                    "payer_id": null
                },
                "purchaseUnit": {
                    "amount": {
                        "value": "1.00",
                        "currency_code": "GEL"
                    },
                    "payee": {
                        "addres": "berbukisq.8,mansarda100",
                        "contact": "995599887444",
                        "email_address": "imedadarsalia@gmail.com"
                    },
                    "payments": [
                        {
                            "captures": [
                                {
                                    "id": "23112",
                                    "status": "PERFORMED",
                                    "amount": {
                                        "value": "0.10",
                                        "currency_code": "GEL"
                                    },
                                    "final_capture": "true",
                                    "create_time": "Wed Aug 17 20:01:51 GET 2022",
                                    "update_time": "Wed Aug 17 20:01:51 GET 2022"
                                }
                            ]
                        }
                    ],
                    "shop_order_id": "3123581662"
                },
                "createTime": null,
                "updateTime": null,
                "errorHistory": []
            }
             */
            JSONObject jsonResponse = new JSONObject(responseData);
            String resultStatus = jsonResponse.has("status") ? jsonResponse.getString("status") : ""; //PERFORMED
            logger.debug(requestId + " - resultStatus: " + resultStatus);
            Map<String,String> mapResponse = new HashMap<>();
            mapResponse.put("status", resultStatus.toUpperCase());
            response.response = mapResponse;
            response.returnCode = 200;
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }

        if (IS_SANDBOX) {
            // mock data for sandbox environment
            Map<String, String> mapResponse = new HashMap<>();
            mapResponse.put("status", "PERFORMED");
            response.response = mapResponse;
            response.returnCode = 200;
        }
        return response.toString();
    }

    public String getPaymentData(String transactionId, boolean isRegister) {
        ObjResponse response = new ObjResponse();
        response.returnCode = 525;
        try {
            logger.debug(requestId + " - transactionId: " + transactionId);
            if (IS_SANDBOX && isRegister) {
                // mock data for sandbox environment
                transactionId = "8980264ba08c35742fc763f276b1a7acc03872ac";
                logger.debug(requestId + " - SANDBOX mode - use test transactionId: " + transactionId);
            }
            String requestURL = SERVER_URL +  "/checkout/payment/" + transactionId;
            logger.debug(requestId + " - requestURL: " + requestURL);
            URL url = new URL(requestURL);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Basic " + getAuthen());

            String responseData = "";
            InputStream is = connection.getInputStream();
            BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
            if ((responseData = responseReader.readLine()) != null) {
                logger.debug(requestId + " - response: " + responseData);
            }
            responseReader.close();
            /*
            {
              "status": "success",
              "pan": "415479xxxxxx2461",
              "rrn": null,
              "order_id": "8980264ba08c35742fc763f276b1a7acc03872ac",
              "pre_auth_status": null,
              "payment_hash": "748848595ab8e02c204566240c30d5ca1a6eb63e",
              "ipay_payment_id": "2250397",
              "status_description": "PERFORMED",
              "shop_order_id": null,
              "payment_method": "GC_CARD",
              "card_type": "VISA",
              "transaction_id": "8WUU4D7DCB5NQSAE",
              "multi_merchant_transaction_id": "8X3K075CH0FE",
              "error_description": null
            }
             */
            JSONObject jsonResponse = new JSONObject(responseData);
            String status = jsonResponse.has("status") ? jsonResponse.getString("status") : ""; //success | error | in_progress
            logger.debug(requestId + " - status: " + status);
            String last4 = "";
            String cardType = "";
            String token = "";
            if (status.equals("success")) {
                String pan = jsonResponse.has("pan") ? jsonResponse.getString("pan") : "";
                last4 = pan.length() > 4 ? pan.substring(pan.length() - 4) : "";
                cardType = jsonResponse.has("card_type") ? jsonResponse.getString("card_type") : "";
                token = jsonResponse.has("order_id") ? jsonResponse.getString("order_id") : "";

                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("status", status);
                mapResponse.put("last4", last4);
                mapResponse.put("cardType", formatCardType(cardType));
                mapResponse.put("token", token);
                response.response = mapResponse;
                response.returnCode = 200;
            } else {
                String message = jsonResponse.has("status_description") ? jsonResponse.getString("status_description") : "";
                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("message", message);
                response.response = mapResponse;
                response.returnCode = 437;
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }
        return response.toString();
    }

    public String createPaymentWithCreditToken(String orderId, double amount, String token, String currencyISO) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();
        try {
            logger.debug(requestId + " - orderId: " + orderId);
            JSONObject requestParams = new JSONObject();
            requestParams.put("order_id", token);
            JSONObject jsonAmount = new JSONObject();
            jsonAmount.put("currency_code", currencyISO);
            jsonAmount.put("value", KeysUtil.DECIMAL_FORMAT0.format(amount));
            requestParams.put("amount", jsonAmount);
            requestParams.put("shop_order_id", orderId);

            String post_data = requestParams.toString();
            logger.debug(requestId + " - post_data: " + post_data);

            String requestURL = SERVER_URL + "/checkout/payment/subscription";
            logger.debug(requestId + " - requestURL: " + requestURL);
            URL url = new URL(requestURL);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Basic " + getAuthen());
            connection.setDoOutput(true);

            DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
            requestWriter.writeBytes(post_data);
            requestWriter.close();
            String responseData = "";
            InputStream is = connection.getInputStream();
            BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
            if ((responseData = responseReader.readLine()) != null) {
                logger.debug(requestId + " - response: " + responseData);
            }
            responseReader.close();
            /*
            {
              "status": "in_progress",
              "payment_hash": "dababce386d224383427d773cbfa8a63b15a4329",
              "order_id": "84b791008eadaaa593673cf55ffa4f8aab519285"
            }
             */
            JSONObject jsonResponse = new JSONObject(responseData);
            String transId = jsonResponse.has("order_id") ? jsonResponse.getString("order_id") : "";
            String status = jsonResponse.has("status") ? jsonResponse.getString("status") : "";
            logger.debug(requestId + " - status: " + status);
            int returnCode = 525;
            if (status.equals("success") || status.equals("in_progress")) {
                if (status.equals("in_progress")) {
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException ignore) {}
                }
                returnCode = 200;
                String authCode = "";
                String paymentData = getPaymentData(transId, false);
                String last4 = CommonUtils.getValue(paymentData, "last4");
                String cardType = CommonUtils.getValue(paymentData, "cardType");

                mapResponse.put("message", "Success!");
                mapResponse.put("transId", transId);
                mapResponse.put("cardType", formatCardType(cardType));
                mapResponse.put("authCode", authCode);
                mapResponse.put("cardMask", "XXXXXXXXXXXX" + last4);
                objResponse.response = mapResponse;
            }
            objResponse.returnCode = returnCode;
            return objResponse.toString();
        } catch(Exception ex) {
            logger.debug(requestId + " - createPaymentWithCreditToken exception: " + CommonUtils.getError(ex));
            objResponse.returnCode = 525;
            return objResponse.toString();
        }
    }

    public void refund(String transactionId) {
        try {
            logger.debug(requestId + " - transactionId: " + transactionId);
            ArrayList<NameValuePair> postParameters = new ArrayList<>();
            postParameters.add(new BasicNameValuePair("order_id", transactionId));

            String requestURL = SERVER_URL + "/checkout/refund";
            logger.debug(requestId + " - requestURL: " + requestURL);

            HttpPost httpRequest = new HttpPost(requestURL);
            httpRequest.setHeader("Authorization", "Basic " + getAuthen());
            httpRequest.addHeader("Content-Type", "application/x-www-form-urlencoded");
            httpRequest.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));

            HttpClient client = HttpClientBuilder.create().build();
            HttpResponse httpResponse = client.execute(httpRequest);
            String response = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
            logger.debug(requestId + " - response: " + response);
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }
    }

    private String getAuthen() throws Exception {
        String requestURL = SERVER_URL + AUTH_URL;
        ArrayList<NameValuePair> postParameters = new ArrayList<>();
        postParameters.add(new BasicNameValuePair("grant_type", "client_credentials"));
        String credential = Credentials.basic(CLIENT_ID, CLIENT_SECRET);
        HttpClient client = HttpClientBuilder.create().build();
        logger.debug(requestId + " - requestURL: " + requestURL);
        HttpPost httpRequest = new HttpPost(requestURL);
        httpRequest.setHeader("Authorization", credential);
        httpRequest.addHeader("Content-Type", "application/x-www-form-urlencoded");
        httpRequest.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));

        HttpResponse httpResponse = client.execute(httpRequest);
        String response = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
        logger.debug(requestId + " - response: " + response);
        JSONObject jsondata = new JSONObject(response);
        return jsondata.getString("access_token");
    }

    private String formatCardType(String cardType) {
        if (cardType.equalsIgnoreCase("VISA"))
            return "VISA";
        else if (cardType.equalsIgnoreCase("MC"))
            return "MASTERCARD";
        else if (cardType.equalsIgnoreCase("AMEX"))
            return "AMEX";

        return cardType;
    }
}
