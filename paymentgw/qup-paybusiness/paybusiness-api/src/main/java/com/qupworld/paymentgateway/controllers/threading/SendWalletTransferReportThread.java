package com.qupworld.paymentgateway.controllers.threading;

import com.qupworld.config.ServerConfig;
import com.qupworld.service.QUpReportPublisher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by qup on 23/03/2020.
 */
@SuppressWarnings("unchecked")
public class SendWalletTransferReportThread extends Thread {
    public String id;
    public String reportBody;
    public String requestId;
    final static Logger logger = LogManager.getLogger(SendWalletTransferReportThread.class);
    @Override
    public void run() {
        try {
            if (ServerConfig.server_report_enable) {
                QUpReportPublisher reportPublisher = QUpReportPublisher.getInstance();
                reportPublisher.sendOneItem(requestId, String.valueOf(id), "WalletTransfer", reportBody);
            }
        } catch(Exception ex) {
            //ex.printStackTrace();
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            logger.debug(requestId + " - SendWalletTransfer store: "+ errors.toString());
        }
    }

}
