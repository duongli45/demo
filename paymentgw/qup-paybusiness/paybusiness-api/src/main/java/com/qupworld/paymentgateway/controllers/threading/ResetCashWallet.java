package com.qupworld.paymentgateway.controllers.threading;

import com.qupworld.paymentgateway.controllers.WalletUtil;
import com.qupworld.paymentgateway.entities.WalletTransfer;
import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.MongoDaoImpl;
import com.qupworld.paymentgateway.models.mongo.collections.wallet.DriverWallet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

/**
 * Created by qup on 23/03/2020.
 */
@SuppressWarnings("unchecked")
public class ResetCashWallet extends Thread {
    final static Logger logger = LogManager.getLogger(ResetCashWallet.class);
    public String fleetId;
    public String requestId;
    @Override
    public void run() {
        try {
            MongoDao mongoDao = new MongoDaoImpl();
            List<DriverWallet> accounts = mongoDao.getCashWalletOfDriver(fleetId);
            WalletUtil walletUtil = new WalletUtil(requestId);
            for (DriverWallet driverWallet : accounts) {
                try {
                    double currentBalance = walletUtil.getCurrentBalance(driverWallet.userId, "cash", driverWallet.cashWallet.currencyISO);
                    logger.debug(requestId + " - driverId: " + driverWallet.userId + " - balance: " + currentBalance + " - mongo: " + driverWallet.cashWallet.currentBalance);

                    WalletTransfer walletTransfer = new WalletTransfer();
                    walletTransfer.fleetId = fleetId;
                    walletTransfer.driverId = driverWallet.userId;
                    walletTransfer.amount = currentBalance;
                    walletTransfer.transferType = "reset";
                    walletTransfer.transactionId = requestId;
                    walletTransfer.currencyISO = driverWallet.cashWallet.currencyISO;
                    walletTransfer.newBalance = 0.0;
                    walletUtil.updateCashWallet(walletTransfer, false);

                    mongoDao.updateCashWallet(driverWallet.userId, 0.0, 0.0, driverWallet.cashWallet.currencyISO);
                } catch (Exception ex) {
                    logger.debug(requestId + " - driverId: " + driverWallet.userId + " - ex: " + ex.getMessage());
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            logger.debug(requestId + " - ResetCashWallet: "+ errors.toString());
        }

    }

}
