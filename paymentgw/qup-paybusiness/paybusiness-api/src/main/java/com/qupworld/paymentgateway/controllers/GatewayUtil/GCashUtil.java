package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.alipay.ap.integration.sdk.openapi.DefaultOpenApiClient;
import com.alipay.ap.integration.sdk.openapi.OpenApiClient;
import com.alipay.ap.integration.sdk.openapi.domain.model.EnvInfo;
import com.alipay.ap.integration.sdk.openapi.domain.model.Money;
import com.alipay.ap.integration.sdk.openapi.domain.model.acquiring.InputUserInfo;
import com.alipay.ap.integration.sdk.openapi.domain.model.acquiring.NotificationUrl;
import com.alipay.ap.integration.sdk.openapi.domain.model.acquiring.Order;
import com.alipay.ap.integration.sdk.openapi.request.GcashAcquiringOrderCreateRequest;
import com.alipay.ap.integration.sdk.openapi.request.GcashAcquiringOrderQueryRequest;
import com.alipay.ap.integration.sdk.openapi.request.GcashAcquiringOrderRefundRequest;
import com.alipay.ap.integration.sdk.openapi.response.GcashAcquiringOrderCreateResponse;
import com.alipay.ap.integration.sdk.openapi.response.GcashAcquiringOrderQueryResponse;
import com.alipay.ap.integration.sdk.openapi.response.GcashAcquiringOrderRefundResponse;
import com.google.gson.Gson;
import com.qupworld.config.ServerConfig;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.TimezoneUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.security.*;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class GCashUtil {

    public String REQUEST_URL;
    public String CLIENT_ID;
    public String CLIENT_SECRET;
    public String MERCHANT_ID;
    public String PRODUCT_CODE;
    public String GCASH_PRIVATE_KEY;
    public String GCASH_PUBLIC_KEY;
    public String SERVER_PRIVATE_KEY;
    public String SERVER_PUBLIC_KEY;
    public boolean IS_SANDBOX;
    public final String MCC = "9950";
    final static Logger logger = LogManager.getLogger(GCashUtil.class);

    public GCashUtil(boolean isSandbox, String requestUrl, String clientId, String clientSecret, String merchantId, String productCode) {
        REQUEST_URL = requestUrl;
        CLIENT_ID = clientId;
        CLIENT_SECRET = clientSecret;
        MERCHANT_ID = merchantId;
        PRODUCT_CODE = productCode;
        IS_SANDBOX = isSandbox;
        // temporary fix public key
        if (isSandbox)
            SERVER_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgo22hZx9N2EXHh8ZspL/iHYcuL1tSdQ974nTkJHRlKRWNzyQqO6TSYouY+nA0y/WD2Pz2wwKwTDBFW82DNekYgFeKZ2bCrsG3Twi7tn3DbfFGKzXfBzXYv6sggL/1Ej2/ABzMVWIaAo8t9Wo5A+/12nOLQIwxsHXZTUrbCJeZYUAfRQFLyIp2/PxVeVd0bLhtEAAj8aJq8RlSlPYdUIgk+CJ+WVdUs7rhMd09MArgAFwB3iRPbBS4gf7QEswRaBDQ3fTJIYE6xTr53geuv9MAO6B2x0Qt0ctmrdGtjLdsoq2KWmlPXo/GS6uJadwQ+xR9JbudS4eZYKNYTf6zGsvVwIDAQAB";
        else
            SERVER_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsWcpXRPXOH3m0sytTeuPoFMCSWwg+ZJVc+K2krCJqRjWsiCtaGx6jUgRvL+eTJVIAvjwxpH3ftmnUjORCaD12Oam7L37c5tst5PHHyDC+ChPsdjLf8I6tl6bNUzdN08Cse+q09CIoaRo4KwO5FQupdqZjIXILogr14dMPgELCcxnqOgZ4bKqgVT2QH97f4Mx0uw+bWadOtMnmMkRtR1WEVULn7SYG7DMHTEEAsFhrX9fVDvoYBvBM5AIH+0nD2ZYOqR63VfgUxaiGk5d3BztK5UNfA+WjeclkKPVI7ED1NPdiojlUXrUchwMGYI+GPTHM8UkqAWtcDxobHrhnfMmoQIDAQAB";
    }

    public String initiateCreditForm(String requestId, String orderId, String fleetId, String driverPhone, String osType, String appVersion, double amount, String currencyISO,
                                     String userType, String orderTitle) {
        ObjResponse objResponse = new ObjResponse();
        try {
            GCASH_PRIVATE_KEY = getGCashPrivateKey();
            /*SERVER_PUBLIC_KEY = getServerPublicKey();*/
            OpenApiClient openApiClient = new DefaultOpenApiClient(REQUEST_URL, CLIENT_ID, CLIENT_SECRET, GCASH_PRIVATE_KEY, SERVER_PUBLIC_KEY);

            GcashAcquiringOrderCreateRequest gcashAcquiringOrderCreateRequest = new GcashAcquiringOrderCreateRequest();
            gcashAcquiringOrderCreateRequest.setMerchantId(MERCHANT_ID);
            gcashAcquiringOrderCreateRequest.setProductCode(PRODUCT_CODE);
            gcashAcquiringOrderCreateRequest.setMcc(MCC);
            gcashAcquiringOrderCreateRequest.setSubMerchantId("");
            gcashAcquiringOrderCreateRequest.setSubMerchantName("HirNa");

            // create order
            Order order = new Order();
            order.setMerchantTransId(orderId);
            order.setMerchantTransType("Payment");
            order.setOrderMemo(driverPhone);
            order.setOrderTitle(orderTitle);

            InputUserInfo buyer = new InputUserInfo();
            buyer.setExternalUserType(userType);
            buyer.setExternalUserId(driverPhone);
            order.setBuyer(buyer);

            InputUserInfo seller = new InputUserInfo();
            seller.setExternalUserType("HIRNA");
            seller.setExternalUserId(fleetId);
            order.setSeller(seller);

            Date createdTime = TimezoneUtil.convertServerToGMT(new Date());
            SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ssZ" );
            String time = df.format(createdTime);
            order.setCreatedTime(df.parse(time));

            // create order
            Money payAmount = new Money();
            payAmount.setValue((long) GatewayUtil.getIntAmount(amount, currencyISO));
            payAmount.setCurrency(currencyISO);
            order.setOrderAmount(payAmount);

            gcashAcquiringOrderCreateRequest.setOrder(order);

            // create URLs
            // ex url = "https://dispatch.lab.qup.vn/paymentNotificationURL";
            /*String url = ServerConfig.dispatch_server + KeysUtil.NOTIFICATION_URL + "?gCashMerchantTransId=" + orderId;
            String gcashNotifyUrl = ServerConfig.dispatch_server + KeysUtil.GCASH_URL;
            if (ServerConfig.dispatch_server.contains("http://dispatch.local.qup.vn"))
                gcashNotifyUrl = "http://113.160.232.234:1337" + KeysUtil.GCASH_URL;*/
            String url = ServerConfig.hook_server + "/notification/" + KeysUtil.GCASH;
            String gcashNotifyUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.GCASH + "?gCashMerchantTransId=" + orderId;
            List<NotificationUrl> notificationUrls = new ArrayList<>();
            NotificationUrl returnUrl = new NotificationUrl();
            returnUrl.setType("PAY_RETURN");
            returnUrl.setUrl(url+"?urlAction=return");
            notificationUrls.add(returnUrl);

            NotificationUrl cancelUrl = new NotificationUrl();
            cancelUrl.setType("CANCEL_RETURN");
            cancelUrl.setUrl(url+"?urlAction=cancel");
            notificationUrls.add(cancelUrl);

            NotificationUrl notifyUrl = new NotificationUrl();
            notifyUrl.setType("NOTIFICATION");
            notifyUrl.setUrl(gcashNotifyUrl);
            notificationUrls.add(notifyUrl);

            gcashAcquiringOrderCreateRequest.setNotificationUrls(notificationUrls);

            // create EnvInfo
            EnvInfo envInfo = new EnvInfo();
            envInfo.setOrderTerminalType("APP");
            envInfo.setTerminalType("APP");
            if (osType != null && !osType.isEmpty())
                envInfo.setOsType(osType);
            if (appVersion != null && !appVersion.isEmpty())
                envInfo.setAppVersion(appVersion);
            gcashAcquiringOrderCreateRequest.setEnvInfo(envInfo);

            Gson gson = new Gson();
            logger.debug(requestId + " - initiateCreditForm request = " + gson.toJson(gcashAcquiringOrderCreateRequest));

            // submit
            GcashAcquiringOrderCreateResponse alipayplusAcquiringOrderCreateOrderResponse =
                    openApiClient.execute(gcashAcquiringOrderCreateRequest);
            String acquirementId = alipayplusAcquiringOrderCreateOrderResponse.getAcquirementId();
            logger.debug(requestId + " - initiateCreditForm response = " + gson.toJson(alipayplusAcquiringOrderCreateOrderResponse));

            if (alipayplusAcquiringOrderCreateOrderResponse.getResultInfo() != null) {
                String status = alipayplusAcquiringOrderCreateOrderResponse.getResultInfo().getResultStatus() != null ? alipayplusAcquiringOrderCreateOrderResponse.getResultInfo().getResultStatus() : "";
                Map<String,Object> mapResponse = new HashMap<>();
                if (status.equals("S")) {
                    String checkoutUrl = alipayplusAcquiringOrderCreateOrderResponse.getCheckoutUrl();
                    logger.debug(requestId + " - checkoutUrl = " + checkoutUrl);
                    mapResponse.put("3ds_url", checkoutUrl);
                    mapResponse.put("merchantTransId", orderId);
                    mapResponse.put("acquirementId", acquirementId);
                    mapResponse.put("transactionId", acquirementId);
                    mapResponse.put("type", "link");
                    mapResponse.put("isDeeplink", false);
                    objResponse.returnCode = 200;
                    objResponse.response = mapResponse;
                    return objResponse.toString();
                } else {
                    String resultCodeId = alipayplusAcquiringOrderCreateOrderResponse.getResultInfo().getResultCodeId() != null ? alipayplusAcquiringOrderCreateOrderResponse.getResultInfo().getResultCodeId() : "";
                    String resultCode = alipayplusAcquiringOrderCreateOrderResponse.getResultInfo().getResultCode() != null ? alipayplusAcquiringOrderCreateOrderResponse.getResultInfo().getResultCode() : "";
                    mapResponse.put("message", resultCode);
                    objResponse.returnCode = 525;
                    objResponse.response = mapResponse;
                    return objResponse.toString();
                }
            } else {
                objResponse.returnCode = 525;
                return objResponse.toString();
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 525);
        }
    }

    public String completePayment(String requestId, String acquirementId, String merchantTransId, double amount, String currencyISO) {
        ObjResponse objResponse = new ObjResponse();
        try {
            GCASH_PRIVATE_KEY = getGCashPrivateKey();
            /*SERVER_PUBLIC_KEY = getServerPublicKey();*/
            OpenApiClient openApiClient = new DefaultOpenApiClient(REQUEST_URL, CLIENT_ID, CLIENT_SECRET, GCASH_PRIVATE_KEY, SERVER_PUBLIC_KEY);

            GcashAcquiringOrderQueryRequest gcashAcquiringOrderQueryRequest = new GcashAcquiringOrderQueryRequest();
            gcashAcquiringOrderQueryRequest.setMerchantId(MERCHANT_ID);
            gcashAcquiringOrderQueryRequest.setAcquirementId(acquirementId);
            gcashAcquiringOrderQueryRequest.setTransactionId(merchantTransId);

            String merchantRequestId = GatewayUtil.getOrderId();
            gcashAcquiringOrderQueryRequest.setMerchantTransId(merchantRequestId);
            Gson gson = new Gson();
            logger.debug(requestId + " - completePayment request = " + gson.toJson(gcashAcquiringOrderQueryRequest));

            GcashAcquiringOrderQueryResponse response = openApiClient.execute(gcashAcquiringOrderQueryRequest);
            logger.debug(requestId + " - completePayment response = " + gson.toJson(response));

            int result = 525;
            if (response.getStatusDetail() != null) {
                String acquirementStatus = response.getStatusDetail().getAcquirementStatus() != null ? response.getStatusDetail().getAcquirementStatus() : "";
                if (acquirementStatus.equalsIgnoreCase("SUCCESS")) {
                    String transactionId = response.getTransactionId();
                    result = 200;
                    // return payment data with for refund process if necessary
                    Map<String,Object> mapResponse = new HashMap<>();
                    mapResponse.put("acquirementId", acquirementId);
                    mapResponse.put("transactionId", transactionId);
                    mapResponse.put("amount", amount);
                    mapResponse.put("currencyISO", currencyISO);
                    objResponse.response = mapResponse;
                } else {
                    result = 467; //The payment process has been cancelled. Please try again
                }
            }

            objResponse.returnCode = result;
            return objResponse.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 525);
        }

    }


    public String getSignature(String requestId, String body) {
        logger.debug(requestId + " - getSignature body: " + body);
        String signature = "";
        try {
            KeyPair pair = getKeyPairFromKeyStore();
            //Let's sign our message
            signature = sign(body, pair.getPrivate());

        } catch (Exception ex) {
            logger.debug(requestId + " - getSignature error: " + GatewayUtil.getError(ex));
        }
        return signature;
    }

    public static String sign(String plainText, PrivateKey privateKey) throws Exception {
        Signature privateSignature = Signature.getInstance("SHA256withRSA");
        privateSignature.initSign(privateKey);
        privateSignature.update(plainText.getBytes(UTF_8));

        byte[] signature = privateSignature.sign();

        return Base64.getEncoder().encodeToString(signature);
    }

    public static KeyPair getKeyPairFromKeyStore() throws Exception {
        //Generated with:
        //  keytool -genkeypair -alias mykey -storepass s3cr3t -keypass s3cr3t -keyalg RSA -keystore keystore.jks

        InputStream ins = GCashUtil.class.getResourceAsStream("/keystore.jks");

        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        keyStore.load(ins, "QUp@1234".toCharArray());   //Keystore password
        KeyStore.PasswordProtection keyPassword =       //Key password
                new KeyStore.PasswordProtection("QUp@1234".toCharArray());

        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry("QUpKey", keyPassword);

        java.security.cert.Certificate cert = keyStore.getCertificate("QUpKey");
        PublicKey publicKey = cert.getPublicKey();
        PrivateKey privateKey = privateKeyEntry.getPrivateKey();

        return new KeyPair(publicKey, privateKey);
    }

    public void refundOrder(String requestId, String acquirementId, double amount, String currencyISO) {
        try {
            GCASH_PRIVATE_KEY = getGCashPrivateKey();
            SERVER_PUBLIC_KEY = getServerPublicKey();
            OpenApiClient openApiClient = new DefaultOpenApiClient(REQUEST_URL, CLIENT_ID, CLIENT_SECRET, GCASH_PRIVATE_KEY, SERVER_PUBLIC_KEY);

            String merchantTransId = GatewayUtil.getOrderId();

            GcashAcquiringOrderRefundRequest gcashAcquiringOrderRefundRequest = new GcashAcquiringOrderRefundRequest();
            gcashAcquiringOrderRefundRequest.setMerchantId(MERCHANT_ID);
            gcashAcquiringOrderRefundRequest.setAcquirementId(acquirementId);
            gcashAcquiringOrderRefundRequest.setRequestId(merchantTransId);
            gcashAcquiringOrderRefundRequest.setRefundReason("Refund topup");

            Money refundAmount = new Money();
            refundAmount.setValue((long) GatewayUtil.getIntAmount(amount, currencyISO));
            refundAmount.setCurrency(currencyISO);
            gcashAcquiringOrderRefundRequest.setRefundAmount(refundAmount);
            Gson gson = new Gson();
            logger.debug(requestId + " - refundOrder request = " + gson.toJson(gcashAcquiringOrderRefundRequest));
            GcashAcquiringOrderRefundResponse response = openApiClient.execute(gcashAcquiringOrderRefundRequest);
            logger.debug(requestId + " - refundOrder response = " + gson.toJson(response));
        } catch (Exception ex) {

        }
    }

    public String getGCashPrivateKey() throws Exception {
        InputStream in;
        if (IS_SANDBOX) {
            in = GCashUtil.class.getResourceAsStream("/pri-key.pem");
        } else {
            in = GCashUtil.class.getResourceAsStream("/private_key_production.pem");
        }
        byte[] keyBytes = new byte[in.available()];
        in.read(keyBytes);
        in.close();

        String priKey = new String(keyBytes, "UTF-8");
        priKey = priKey.replaceAll("(-+BEGIN PRIVATE KEY-+\\r?\\n|-+END PRIVATE KEY-+\\r?\\n?)", "");
        priKey = priKey.replaceAll("\\r\\n|\\r|\\n", "");
        /*logger.debug("priKey: " + priKey);*/
        return priKey;
    }

    public static String getServerPublicKey() throws Exception {
        InputStream ins = GCashUtil.class.getResourceAsStream("/keystore.jks");

        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        keyStore.load(ins, "QUp@1234".toCharArray());   //Keystore password

        java.security.cert.Certificate cert = keyStore.getCertificate("QUpKey");
        PublicKey publicKey = cert.getPublicKey();
        Base64.Encoder encoder = java.util.Base64.getMimeEncoder();
        String pubKey = encoder.encodeToString(publicKey.getEncoded());
        pubKey = pubKey.replaceAll("\\r\\n|\\r|\\n", "");
        logger.debug("pubKey: " + pubKey);
        return pubKey;
    }

    public static void main(String[] args) throws Exception{
        String clientId = "2018103116394800000000";
        String clientSecret = "2018101611204819271136Oaea";
        String merchantId = "217020000000125080855";
        String productCode = "51051000101000100001";
        GCashUtil gCashUtil = new GCashUtil(true, "", clientId, clientSecret, merchantId, productCode);

        /*String requestId = UUID.randomUUID().toString();
        String acquirementId = "20190819121212800110170312200224965";
        double amount = 5611.34;
        String currencyISO = "PHP";
        gCashUtil.refundOrder(requestId, acquirementId, amount, currencyISO);*/

        String body = "{\"head\":{\"clientId\":\"2018103116394800000000\",\"function\":\"gcash.acquiring.order.finish.notify\",\"respTime\":\"2019-10-14T10:36:27+0700\",\"version\":\"2.0\",\"reqMsgId\":\"fc0c4db1-4f03-447c-a932-c18533fa1158\"},\"body\":{\"resultInfo\":{\"resultStatus\":\"S\",\"resultCode\":\"SUCCESS\",\"resultCodeId\":\"00000000\",\"resultMsg\":\"success\"}}}";
        String signature = gCashUtil.getSignature("", body);
        System.out.println("signature  = " + signature);

    }
    public static String getServerPublicKeyProduction() throws Exception {
        InputStream ins = GCashUtil.class.getResourceAsStream("/keystoreProduction.jks");

        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        keyStore.load(ins, "Payment@QUp1234".toCharArray());   //Keystore password

        java.security.cert.Certificate cert = keyStore.getCertificate("QUpKey");
        PublicKey publicKey = cert.getPublicKey();
        Base64.Encoder encoder = java.util.Base64.getMimeEncoder();
        String pubKey = encoder.encodeToString(publicKey.getEncoded());
        //pubKey = pubKey.replaceAll("\\r\\n|\\r|\\n", "");
        logger.debug("pubKey: " + pubKey);
        return pubKey;
    }

    /***********************************************************
     * Generate random order ID
     * @return Get random between 0-100000
     ************************************************************/

    public static int getRandomOrderId(){
        int result = (int)(Math.random()*1000000);
        return result;
    }
}
