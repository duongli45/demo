package com.qupworld.util;

import com.pg.util.KeysUtil;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 *      <p/>
 *      Description
 */
@SuppressWarnings("unchecked")
public class TicketUtil {

    public static String getPaidBy(String transactionStatus, String canceller, String gateway, boolean splitPayment, String stripeMethod) {
        String paidBy = "";
        switch (transactionStatus) {
            case KeysUtil.PAYMENT_CASH:
                paidBy = splitPayment ? "Wallet, Cash" : "Cash";
                break;
            case KeysUtil.PAYMENT_FLEETCARD:
                paidBy = splitPayment ? "Wallet, External card" : "External card";
                break;
            case KeysUtil.PAYMENT_CARD:
                paidBy = splitPayment ? "Wallet, Personal card" : "Personal card";
                break;
            case KeysUtil.PAYMENT_CARD_EXTERNAL:
                paidBy = splitPayment ? "Wallet, Credit card" : "Credit card";
                break;
            case KeysUtil.PAYMENT_PREPAID:
                paidBy = splitPayment ? "Wallet, Corporate prepaid" : "Corporate prepaid";
                break;
            case KeysUtil.PAYMENT_DIRECTBILLING:
                paidBy = splitPayment ? "Wallet, Direct invoicing" : "Direct invoicing";
                break;
            case KeysUtil.PAYMENT_CORPORATE:
                paidBy = splitPayment ? "Wallet, Corporate card" : "Corporate card";
                break;
            case KeysUtil.PAYMENT_MDISPATCHER:
                paidBy = splitPayment ? "Wallet, mDispatcher card" : "mDispatcher card";
                break;
            case KeysUtil.PAYMENT_INCIDENT:
                paidBy = "Incident";
                break;
            case KeysUtil.PAYMENT_NOSHOW:
                paidBy = "No show";
                break;
            case KeysUtil.PAYMENT_APPLEPAY:
                paidBy = splitPayment ? "Wallet, Apple Pay" : "Apple Pay";
                break;
            case KeysUtil.PAYMENT_PAXWALLET:
                paidBy = "Wallet";
                break;
            case KeysUtil.PAYMENT_TNG_WALLET:
                paidBy = "TnG eWallet";
                break;
            case KeysUtil.PAYMENT_CASH_SENDER:
                paidBy = "Cash by sender";
                break;
            case KeysUtil.PAYMENT_CASH_RECIPIENT:
                paidBy = "Cash by recipient";
                break;
            case KeysUtil.PAYMENT_BOOST:
                paidBy = "Boost eWallet";
                break;
            case KeysUtil.PAYMENT_VIPPS:
                paidBy = "Vipps";
                break;
            case KeysUtil.PAYMENT_EXTERNAL_WALLET:
                paidBy = gateway;
                break;
            case KeysUtil.PAYMENT_COMPLETE_WITHOUT_SERVICE:
                paidBy = "Complete without service";
                break;
            case KeysUtil.PAYMENT_GOOGLEPAY:
                paidBy = splitPayment ? "Wallet, Google Pay" : "Google Pay";
                break;
            case KeysUtil.PAYMENT_PAYMENT_LINK:
                paidBy = "Payment Link (" + convertPaymentMethod(stripeMethod) + ")";
                break;
            case KeysUtil.PAYMENT_CANCELED:
                paidBy = "Canceled by CC";
                switch (canceller) {
                    case "driver":
                        paidBy = "Canceled by driver";
                        break;
                    case "passenger":
                        paidBy = "Canceled by passenger";
                        break;
                    case "Partner":
                        paidBy = "Canceled by partner";
                        break;
                    case "mDispatcher":
                        paidBy = "Canceled by mDispatcher";
                        break;
                    case "CorpAD":
                        paidBy = "Canceled by Corporate Admin";
                        break;
                    case "API":
                        paidBy = "Canceled by API";
                        break;
                    case "webBooking":
                        paidBy = "Canceled by web booking";
                        break;
                    case "timeout":
                        paidBy = "Canceled by timeout";
                        break;
                    case KeysUtil.dash_board:
                        paidBy = "Canceled by Booking board";
                        break;
                    case KeysUtil.corp_board:
                        paidBy = "Canceled by Booking board";
                        break;
                    case "rejected":
                        paidBy = "Canceled by rejected";
                        break;
                    case "merchant":
                        paidBy = "Canceled by merchant";
                        break;
                }
                break;
            default:
                paidBy = transactionStatus;
        }
        return paidBy;
    }

    public static int getPaymentType(String transactionStatus) {
        int paymentType = 0;
        switch (transactionStatus) {
            case KeysUtil.PAYMENT_CASH:
                paymentType = 0;
                break;
            case KeysUtil.PAYMENT_CARD:
                paymentType = 1;
                break;
           /* case KeysUtil.PAYMENT_CASH: // input
                paymentType = 2;
                break;*/
            case KeysUtil.PAYMENT_FLEETCARD:
                paymentType = 3;
                break;
            /*case KeysUtil.PAYMENT_CASH: // swipe
                paymentType = 4;
                break;*/
            case KeysUtil.PAYMENT_MDISPATCHER:
                paymentType = 5;
                break;
            case KeysUtil.PAYMENT_DIRECTBILLING:
                paymentType = 6;
                break;
            case KeysUtil.PAYMENT_CORPORATE:
                paymentType = 7;
                break;
            case KeysUtil.PAYMENT_PREPAID:
                paymentType = 8;
                break;
            case KeysUtil.PAYMENT_APPLEPAY:
                paymentType = 10;
                break;
            case KeysUtil.PAYMENT_AIRPAY:
                paymentType = 11;
                break;
            case KeysUtil.PAYMENT_CARD_EXTERNAL:
                paymentType = 12;
                break;
            case KeysUtil.PAYMENT_PAXWALLET:
                paymentType = 13;
                break;
            case KeysUtil.PAYMENT_TNG_WALLET:
                paymentType = 14;
                break;
            case KeysUtil.PAYMENT_MOMO:
                paymentType = 15;
                break;
            case KeysUtil.PAYMENT_CASH_SENDER:
                paymentType = 16;
                break;
            case KeysUtil.PAYMENT_CASH_RECIPIENT:
                paymentType = 17;
                break;
            case KeysUtil.PAYMENT_BOOST:
                paymentType = 19;
                break;
            case KeysUtil.PAYMENT_VIPPS:
                paymentType = 20;
                break;
            case KeysUtil.PAYMENT_EXTERNAL_WALLET:
                paymentType = 21;
                break;
            case KeysUtil.PAYMENT_GOOGLEPAY:
                paymentType = 23;
                break;
            case KeysUtil.PAYMENT_PAYMENT_LINK:
                paymentType = 24;
                break;

        }
        return paymentType;
    }

    private static String convertPaymentMethod(String method) {
        String converted = method;
        switch (method) {
            case "card":
                converted = "Credit Card";
                break;
            case "affirm" :
                converted = "Affirm";
                break;
            case "promptpay" :
                converted = "PromptPay";
                break;
            case "bacs_debit" :
                converted = "BACS Debit";
                break;
            case "bancontact" :
                converted = "Bancontact";
                break;
            case "blik" :
                converted = "Blik";
                break;
            case "boleto" :
                converted = "Boleto";
                break;
            case "cashapp" :
                converted = "Cash App";
                break;
            case "eps" :
                converted = "EPS";
                break;
            case "fpx" :
                converted = "FPX";
                break;
            case "giropay" :
                converted = "Giropay";
                break;
            case "grabpay" :
                converted = "GrabPay";
                break;
            case "ideal" :
                converted = "iDEAL";
                break;
            case "klarna" :
                converted = "Klarna";
                break;
            case "konbini" :
                converted = "Konbini";
                break;
            case "link" :
                converted = "Link";
                break;
            case "oxxo" :
                converted = "OXXO";
                break;
            case "p24" :
                converted = "Przelewy24";
                break;
            case "paynow" :
                converted = "PayNow";
                break;
            case "paypal" :
                converted = "PayPal";
                break;
            case "pix" :
                converted = "PIX";
                break;
            case "afterpay_clearpay" :
                converted = "Afterpay Clearpay";
                break;
            case "alipay" :
                converted = "Alipay";
                break;
            case "au_becs_debit" :
                converted = "AU BECS Debit";
                break;
            case "sepa_debit" :
                converted = "SEPA Debit";
                break;
            case "sofort" :
                converted = "SOFORT";
                break;
            case "us_bank_account" :
                converted = "US Bank Account";
                break;
            case "wechat_pay" :
                converted = "WeChat Pay";
                break;
            case "apple_pay" :
                converted = "Apple Pay";
                break;
            case "google_pay" :
                converted = "Google Pay";
                break;
        }
        return converted;
    }
}