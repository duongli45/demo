package com.qupworld.paymentgateway.controllers.threading;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.qupworld.config.ServerConfig;
import com.pg.util.KeysUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;

/**
 * Created by qup on 23/03/2020.
 */
@SuppressWarnings("unchecked")
public class DeleteTokenThread extends Thread {
    private static String removelogs = ServerConfig.logs_server+"/api/service/removeLogs";
    Logger logger = LogManager.getLogger(DeleteTokenThread.class);
    public String token;
    public String gateway;
    public String requestId;
    public Boolean deleteToken(String token) {
        HttpResponse<JsonNode> jsonResponse = null;
        try {
            String body = "{\"logId\":\""+token+"\",\"type\":\"\"}";
            jsonResponse = Unirest.post(removelogs)
                    .basicAuth(ServerConfig.logs_username, ServerConfig.logs_password)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .body(body)
                    .asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        org.json.JSONObject objectResponse = jsonResponse.getBody().getObject();
        if (objectResponse != null && objectResponse.get("returnCode") != null && Integer.parseInt(objectResponse.get("returnCode").toString()) == 200) {
            return true;
        }
        return false;
    }
    public Boolean deleteCardStore(String token){
        HttpResponse<JsonNode> jsonResponse = null;
        try {
            String body = "{\"logId\":\""+token+"\",\"type\":\"Company\"}";
            jsonResponse = Unirest.post(removelogs)
                    .basicAuth(ServerConfig.logs_username, ServerConfig.logs_password)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .body(body)
                    .asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        org.json.JSONObject objectResponse = jsonResponse.getBody().getObject();
        if (objectResponse != null && objectResponse.get("returnCode") != null && Integer.parseInt(objectResponse.get("returnCode").toString()) == 200) {
            return true;
        }
        return false;
    }
    @Override
    public void run() {
        logger.debug(requestId + " - DeleteTokenThread");
        try {
            List<String> gatewayCacheCard = Arrays.asList(KeysUtil.CREDITCORP, KeysUtil.YEEPAY);
            if (gatewayCacheCard.contains(gateway)) {
                this.deleteToken(token);
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            logger.debug(requestId + " - DeleteTokenThread Error: "+ errors.toString());
        }

    }

}
