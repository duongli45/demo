package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.google.common.io.BaseEncoding;
import com.qupworld.paymentgateway.entities.CreditEnt;
import com.qupworld.util.AdyenCSEUtil;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.ValidCreditCard;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by qup on 6/30/17.
 */
@SuppressWarnings("unchecked")
public class AdyenUtil {

    final static Logger logger = LogManager.getLogger(AdyenUtil.class);
    final static String AUTHORIZE_SANDBOX = "https://pal-test.adyen.com/pal/servlet/Payment/v25/authorise";
    final static String AUTHORIZE_PRODUCTION = "https://pal-live.adyen.com/pal/servlet/Payment/v25/authorise";
    final static String CAPTURE_SANDBOX = "https://pal-test.adyen.com/pal/servlet/Payment/v25/capture";
    final static String CAPTURE_PRODUCTION = "https://pal-live.adyen.com/pal/servlet/Payment/v25/capture";
    final static String REFUND_SANDBOX = "https://pal-test.adyen.com/pal/servlet/Payment/v25/refund";
    final static String REFUND_PRODUCTION = "https://pal-live.adyen.com/pal/servlet/Payment/v25/refund";
    final static String HPP_SANDBOX = "https://test.adyen.com/hpp/pay.shtml";
    final static String HPP_PRODUCTION = "https://live.adyen.com/hpp/pay.shtml";
    final static String RETRIEVE_SANDBOX = "https://pal-test.adyen.com/pal/servlet/Recurring/v25/listRecurringDetails";
    final static String RETRIEVE_PRODUCTION = "https://pal-live.adyen.com/pal/servlet/Recurring/v25/listRecurringDetails";

    static String MERCHANT_ACCOUNT = "";
    static String WS_USER = "";
    static String WS_PASSWORD = "";
    static String HMAC_KEY = "";
    static String SKIN = "";
    static String CSE_KEY = "";
    public AdyenUtil(String merchantAccount, String wsUser, String wsPassword, String hmacKey, String skin, String cseKey) {
        MERCHANT_ACCOUNT = merchantAccount;
        WS_USER = wsUser;
        WS_PASSWORD = wsPassword;
        HMAC_KEY = hmacKey;
        SKIN = skin;
        CSE_KEY = cseKey;
    }

    public String doPaymentWithCardInfo(String requestId, String bookId, double amount, String merchantAccount, String wsUser, String wsPassword, String cseKey, boolean isSandbox, String currencyISO,
            String customerEmail, String customerIP, String cardNumber, String cardHolder, String cvv, String expiredDate) {
        ObjResponse response = new ObjResponse();
        try {
            String authorizeURL = isSandbox ? AUTHORIZE_SANDBOX : AUTHORIZE_PRODUCTION;
            String captureURL = isSandbox ? CAPTURE_SANDBOX : CAPTURE_PRODUCTION;
            int intAmount = 0;
            if (isZeroDecimal(currencyISO)) {
                intAmount = (int) amount;
            } else {
                DecimalFormat df = new DecimalFormat("#");
                intAmount = Integer.parseInt(df.format(amount*100));
            }
            CredentialsProvider provider = new BasicCredentialsProvider();
            UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(wsUser, wsPassword);
            provider.setCredentials(AuthScope.ANY, credentials);

            HttpClient client = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();

            String shopperRef = GatewayUtil.getOrderId();

            // Create authorize request
            JSONObject paymentRequest = new JSONObject();
            paymentRequest.put("merchantAccount", merchantAccount);
            paymentRequest.put("reference", shopperRef);
            paymentRequest.put("shopperIP", customerIP);
            paymentRequest.put("shopperEmail", customerEmail);
            paymentRequest.put("fraudOffset", "0");

            // Set amount
            JSONObject payAmount = new JSONObject();
            payAmount.put("currency", currencyISO);
            payAmount.put("value", intAmount);
            paymentRequest.put("amount", payAmount);

            // Set card - replaced by encrypted data
            String[] expiry = expiredDate.split("/");
            /*JSONObject card = new JSONObject();
            card.put("expiryMonth", expired[0]);
            card.put("expiryYear", expired[1]);
            card.put("holderName", cardHolder);
            card.put("number", cardNumber);
            card.put("cvc", cvv);
            paymentRequest.put("card", card);*/

            JSONObject additionalData = new JSONObject();
            String encrypted = AdyenCSEUtil.getCSEString(cseKey, cardNumber, cardHolder, cvv, expiry[0], expiry[1]);
            additionalData.put("card.encrypted.json", encrypted);
            paymentRequest.put("additionalData", additionalData);

            /*JSONObject billingAddress = new JSONObject();
            billingAddress.put("street", "Simon Carmiggeltstraat");
            billingAddress.put("houseNumberOrName", "6-50");
            billingAddress.put("postalCode", "1011 DJ");
            billingAddress.put("city", "Amsterdam");
            billingAddress.put("stateOrProvince", "");
            billingAddress.put("country", "NL");
            card.put("billingAddress", billingAddress);*/



            /**
             * Send the HTTP request with the specified variables in JSON.
             */
            HttpPost httpRequest = new HttpPost(authorizeURL);
            httpRequest.addHeader("Content-Type", "application/json");
            httpRequest.setEntity(new StringEntity(paymentRequest.toString(), "UTF-8"));
            logger.debug(requestId + " - Payment request " + paymentRequest.toString());

            HttpResponse httpResponse = client.execute(httpRequest);
            String paymentResponse = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
            logger.debug(requestId + " - Payment result: " + paymentResponse);

            // Parse JSON response
            JSONParser parser = new JSONParser();
            JSONObject paymentResult;

            try {
                paymentResult = (JSONObject) parser.parse(paymentResponse);
            } catch (Exception ex) {
                ex.printStackTrace();
                paymentResult = null;
            }

            if (paymentResult == null) {
                GatewayUtil.sendMail(requestId, KeysUtil.ADYEN, paymentRequest.toString(), paymentResponse, "bookId " + bookId);
                response.returnCode = 437;
                return response.toString();

            } else if (paymentResult.get("status") != null && !paymentResult.get("status").toString().equals("200")) {
                GatewayUtil.sendMail(requestId, KeysUtil.ADYEN, paymentRequest.toString(), paymentResponse, "bookId " + bookId);

                String code = paymentResult.get("errorCode") != null ? paymentResult.get("errorCode").toString() : "";
                response.returnCode = getErrorCodeFromErrorCode(code);
                return response.toString();
            } else if (paymentResult.get("resultCode") != null && paymentResult.get("resultCode").toString().equalsIgnoreCase("Refused")) {
                GatewayUtil.sendMail(requestId, KeysUtil.ADYEN, paymentRequest.toString(), paymentResponse, "bookId " + bookId);

                String errorMessage = paymentResult.get("refusalReason") != null ? paymentResult.get("refusalReason").toString() : "";
                response.returnCode = getErrorCodeFromMessage(errorMessage);
                return response.toString();
            }
            String pspReference = paymentResult.get("pspReference").toString();

            // Create capture request
            JSONObject captureRequest = new JSONObject();
            captureRequest.put("merchantAccount", merchantAccount);
            captureRequest.put("modificationAmount", payAmount);

            captureRequest.put("value", intAmount);
            captureRequest.put("currency", currencyISO);
            captureRequest.put("originalReference", pspReference);

            /**
             * Send the HTTP request with the specified variables in JSON.
             */
            HttpPost httpCaptureRequest = new HttpPost(captureURL);
            httpCaptureRequest.addHeader("Content-Type", "application/json");
            httpCaptureRequest.setEntity(new StringEntity(captureRequest.toString(), "UTF-8"));
            logger.debug(requestId + " - Capture request: " + captureRequest.toString());

            HttpResponse httpCaptureResponse = client.execute(httpCaptureRequest);
            String paymentCaptureResponse = EntityUtils.toString(httpCaptureResponse.getEntity(), "UTF-8");
            logger.debug(requestId + " - Capture result: " + paymentCaptureResponse);

            // Parse JSON response
            JSONObject captureResult;

            try {
                captureResult = (JSONObject) parser.parse(paymentCaptureResponse);
            } catch (Exception ex) {
                ex.printStackTrace();
                captureResult = null;
            }

            logger.debug(requestId + " - Payment Result: " + captureResult);
            if (captureResult != null && captureResult.get("response").equals("[capture-received]")) {
                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("message", "Success!");
                mapResponse.put("transId", captureResult.get("pspReference").toString());
                mapResponse.put("cardType", ValidCreditCard.getCardType(cardNumber));
                mapResponse.put("authCode", paymentResult.get("authCode").toString());
                mapResponse.put("cardMask", "XXXXXXXXXXXX" + cardNumber.substring(cardNumber.length() - 4));

                response.returnCode = 200;
                response.response = mapResponse;
            } else {
                GatewayUtil.sendMail(requestId, KeysUtil.ADYEN, captureRequest.toString(), paymentCaptureResponse, "bookId " + bookId);
                response.returnCode = 437;
            }

        } catch(Exception ex) {
            GatewayUtil.sendMail(requestId, KeysUtil.ADYEN, ex, "bookId " + bookId);
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }

        return response.toString();
    }

    public String createCreditTokenCSE(CreditEnt creditForm, boolean isSandbox, String currencyISO, String customerEmail, String customerIP) {
        ObjResponse response = new ObjResponse();
        try {
            String authorizeURL = isSandbox ? AUTHORIZE_SANDBOX : AUTHORIZE_PRODUCTION;
            String captureURL = isSandbox ? CAPTURE_SANDBOX : CAPTURE_PRODUCTION;
            int intAmount = 0;
            if (isZeroDecimal(currencyISO)) {
                intAmount = 10;
            } else {
                intAmount = 1000;
            }
            CredentialsProvider provider = new BasicCredentialsProvider();
            UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(WS_USER, WS_PASSWORD);
            provider.setCredentials(AuthScope.ANY, credentials);

            HttpClient client = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();

            String shopperRef = GatewayUtil.getOrderId();

            // Create authorize request
            JSONObject paymentRequest = new JSONObject();
            paymentRequest.put("merchantAccount", MERCHANT_ACCOUNT);
            paymentRequest.put("reference", shopperRef);
            paymentRequest.put("shopperIP", customerIP);
            paymentRequest.put("shopperEmail", customerEmail);
            paymentRequest.put("shopperReference", shopperRef);
            paymentRequest.put("fraudOffset", "0");

            // Set amount
            JSONObject payAmount = new JSONObject();
            payAmount.put("currency", currencyISO);
            payAmount.put("value", intAmount);
            paymentRequest.put("amount", payAmount);

            JSONObject additionalData = new JSONObject();
            byte[] decodedBytes = Base64.getDecoder().decode(creditForm.data3ds);
            String decodedString = new String(decodedBytes);
            logger.debug(creditForm.requestId + " - decodedString: " + decodedString);

            additionalData.put("card.encrypted.json", decodedString);
            paymentRequest.put("additionalData", additionalData);

            // Set recurring
            JSONObject recurring = new JSONObject();
            recurring.put("contract", "RECURRING,ONECLICK");
            paymentRequest.put("recurring", recurring);

            /**
             * Send the HTTP request with the specified variables in JSON.
             */
            HttpPost httpRequest = new HttpPost(authorizeURL);
            httpRequest.addHeader("Content-Type", "application/json");
            httpRequest.setEntity(new StringEntity(paymentRequest.toString(), "UTF-8"));
            logger.debug(creditForm.requestId + " - Payment request: " + paymentRequest.toString());

            HttpResponse httpResponse = client.execute(httpRequest);
            String paymentResponse = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
            logger.debug(creditForm.requestId + " - Payment response: " + paymentResponse);
            logger.debug(creditForm.requestId + " - Code: " + httpResponse.getStatusLine().getStatusCode());

            // Parse JSON response
            JSONParser parser = new JSONParser();
            JSONObject paymentResult;

            try {
                paymentResult = (JSONObject) parser.parse(paymentResponse);
            } catch (Exception ex) {
                ex.printStackTrace();
                paymentResult = null;
            }

            String pspReference = "";
            if (paymentResult != null) {
                int httpCode = httpResponse.getStatusLine().getStatusCode();
                String errorCode = paymentResult.get("errorCode") != null ? paymentResult.get("errorCode").toString() : "";
                String resultCode = paymentResult.get("resultCode") != null ? paymentResult.get("resultCode").toString() : "";
                String refusalReason = paymentResult.get("refusalReason") != null ? paymentResult.get("refusalReason").toString() : "";
                pspReference = paymentResult.get("pspReference")!= null ? paymentResult.get("pspReference").toString() : "";
                logger.debug(creditForm.requestId + " - errorCode: " + errorCode);
                logger.debug(creditForm.requestId + " - resultCode: " + resultCode);
                logger.debug(creditForm.requestId + " - refusalReason: " + refusalReason);
                logger.debug(creditForm.requestId + " - pspReference: " + pspReference);
                if (200 != httpCode) {
                    GatewayUtil.sendMail(creditForm.requestId, KeysUtil.ADYEN, paymentRequest.toString(), "HTTP Code: " + httpCode + " - " + paymentResponse, "phone " + creditForm.phone);
                    response.returnCode = getErrorCodeFromErrorCode(errorCode);
                    return response.toString();
                }
                if ("Refused".equals(resultCode)) {
                    GatewayUtil.sendMail(creditForm.requestId, KeysUtil.ADYEN, paymentRequest.toString(), paymentResponse, "phone " + creditForm.phone);
                    response.returnCode = getErrorCodeFromMessage(refusalReason);
                    return response.toString();
                }
                if (pspReference.isEmpty()) {
                    GatewayUtil.sendMail(creditForm.requestId, KeysUtil.ADYEN, paymentRequest.toString(), paymentResponse, "phone " + creditForm.phone);
                    response.returnCode = 437;
                    return response.toString();
                }
            } else {
                GatewayUtil.sendMail(creditForm.requestId, KeysUtil.ADYEN, paymentRequest.toString(), paymentResponse, "phone " + creditForm.phone);
                response.returnCode = 437;
                return response.toString();
            }

            // Create capture request
            JSONObject captureRequest = new JSONObject();
            captureRequest.put("merchantAccount", MERCHANT_ACCOUNT);
            captureRequest.put("modificationAmount", payAmount);

            captureRequest.put("value", intAmount);
            captureRequest.put("currency", currencyISO);
            captureRequest.put("originalReference", pspReference);

            /**
             * Send the HTTP request with the specified variables in JSON.
             */
            HttpPost httpCaptureRequest = new HttpPost(captureURL);
            httpCaptureRequest.addHeader("Content-Type", "application/json");
            httpCaptureRequest.setEntity(new StringEntity(captureRequest.toString(), "UTF-8"));
            logger.debug(creditForm.requestId + " - Capture request: " + captureRequest.toString());

            HttpResponse httpCaptureResponse = client.execute(httpCaptureRequest);
            String paymentCaptureResponse = EntityUtils.toString(httpCaptureResponse.getEntity(), "UTF-8");
            logger.debug(creditForm.requestId + " - Capture response: " + paymentCaptureResponse);

            // Parse JSON response
            JSONObject captureResult;

            try {
                captureResult = (JSONObject) parser.parse(paymentCaptureResponse);
            } catch (Exception ex) {
                ex.printStackTrace();
                captureResult = null;
            }

            logger.debug(creditForm.requestId + " - Payment Result: " + captureResult);
            if (captureResult != null && "[capture-received]".equals(captureResult.get("response"))) {
                String cardMasked = "XXXXXXXXXXXX" + creditForm.cardNumber.substring(creditForm.cardNumber.length() - 4);
                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("message", "");
                mapResponse.put("creditCard", cardMasked);
                mapResponse.put("qrCode", "");
                mapResponse.put("token", shopperRef + KeysUtil.TEMPKEY + creditForm.cvv);
                mapResponse.put("cardType", ValidCreditCard.getCardType(creditForm.cardNumber));

                response.returnCode = 200;
                response.response = mapResponse;

                // void transaction
                refundTransaction(creditForm.requestId, pspReference, payAmount, isSandbox);
            } else {
                GatewayUtil.sendMail(creditForm.requestId, KeysUtil.ADYEN, captureRequest.toString(), paymentCaptureResponse, "phone " + creditForm.phone);
                response.returnCode = 437;
            }

        } catch(Exception ex) {
            GatewayUtil.sendMail(creditForm.requestId, KeysUtil.ADYEN, ex, "phone " + creditForm.phone);
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }

        return response.toString();
    }

    public String createCreditTokenHPP(String requestId, boolean isSandbox, String currencyISO, String pspReference, String shopperRef, String phone) {
        ObjResponse response = new ObjResponse();
        try {
            String captureURL = isSandbox ? CAPTURE_SANDBOX : CAPTURE_PRODUCTION;
            int intAmount = 0;
            if (isZeroDecimal(currencyISO)) {
                intAmount = 10;
            } else {
                intAmount = 1000;
            }
            CredentialsProvider provider = new BasicCredentialsProvider();
            UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(WS_USER, WS_PASSWORD);
            provider.setCredentials(AuthScope.ANY, credentials);

            HttpClient client = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();

            // Create capture request
            JSONObject payAmount = new JSONObject();
            payAmount.put("currency", currencyISO);
            payAmount.put("value", intAmount);

            JSONObject captureRequest = new JSONObject();
            captureRequest.put("merchantAccount", MERCHANT_ACCOUNT);
            captureRequest.put("modificationAmount", payAmount);

            captureRequest.put("value", intAmount);
            captureRequest.put("currency", currencyISO);
            captureRequest.put("originalReference", pspReference);

            /**
             * Send the HTTP request with the specified variables in JSON.
             */
            HttpPost httpCaptureRequest = new HttpPost(captureURL);
            httpCaptureRequest.addHeader("Content-Type", "application/json");
            httpCaptureRequest.setEntity(new StringEntity(captureRequest.toString(), "UTF-8"));
            logger.debug(requestId + " - Capture request: " + captureRequest.toJSONString());

            HttpResponse httpCaptureResponse = client.execute(httpCaptureRequest);
            String paymentCaptureResponse = EntityUtils.toString(httpCaptureResponse.getEntity(), "UTF-8");
            logger.debug(requestId + " - Capture response: " + paymentCaptureResponse);

            // Parse JSON response
            JSONObject captureResult;
            JSONParser parser = new JSONParser();
            try {
                captureResult = (JSONObject) parser.parse(paymentCaptureResponse);
            } catch (Exception ex) {
                ex.printStackTrace();
                captureResult = null;
            }
            logger.debug(requestId + " - Payment Result: " + captureResult);

            // Create retrieve request to get card details
            String cardMasked = "";
            String cardType = "";
            JSONObject retrieveRequest = new JSONObject();
            retrieveRequest.put("merchantAccount", MERCHANT_ACCOUNT);
            retrieveRequest.put("shopperReference", shopperRef);
            String retrieveURL = isSandbox ? RETRIEVE_SANDBOX : RETRIEVE_PRODUCTION;
            HttpPost httpRetrieveRequest = new HttpPost(retrieveURL);
            httpRetrieveRequest.addHeader("Content-Type", "application/json");
            httpRetrieveRequest.setEntity(new StringEntity(retrieveRequest.toString(), "UTF-8"));
            logger.debug(requestId + " - Retrieve request: " + retrieveRequest.toString());

            HttpResponse httpRetrieveResponse = client.execute(httpRetrieveRequest);
            String paymentRetrieveResponse = EntityUtils.toString(httpRetrieveResponse.getEntity(), "UTF-8");
            logger.debug(requestId + " - Retrieve response: " + paymentRetrieveResponse);

            // Parse JSON response
            JSONObject retrieveResult;
            try {
                retrieveResult = (JSONObject) parser.parse(paymentRetrieveResponse);
            } catch (Exception ex) {
                ex.printStackTrace();
                retrieveResult = null;
            }
            logger.debug(requestId + " - Payment Result: " + retrieveResult);
            String recurringDetailReference = "";
            if (retrieveResult != null) {
                JSONArray array = (JSONArray) retrieveResult.get("details");
                if (array != null && array.size() > 0) {
                    JSONObject detail = (JSONObject) array.get(0);
                    JSONObject recurringDetail = (JSONObject) detail.get("RecurringDetail");
                    if (recurringDetail != null) {
                        recurringDetailReference = recurringDetail.get("recurringDetailReference") != null ? recurringDetail.get("recurringDetailReference").toString() : "";
                        String variant = recurringDetail.get("variant") != null ? recurringDetail.get("variant").toString() : "";
                        switch (variant.toUpperCase()) {
                            case "VISA" :
                                cardType = "VISA";
                                break;
                            case "MC" :
                                cardType = "MASTERCARD";
                                break;
                            case "AMEX" :
                                cardType = "AMEX";
                                break;
                            case "JCB" :
                                cardType = "JCB";
                                break;
                            case "DISCOVER" :
                                cardType = "DISCOVER";
                                break;
                        }
                        JSONObject card = (JSONObject) recurringDetail.get("card");
                        if (card != null) {
                            cardMasked = "XXXXXXXXXXXX" + card.get("number").toString();
                        }
                    }
                } else {
                    // empty data, something went wrong, return error to add card again
                    GatewayUtil.sendMail(requestId, KeysUtil.ADYEN, retrieveRequest.toString(), paymentRetrieveResponse, "phone " + phone);
                    response.returnCode = 437;
                    return response.toString();
                }
            }
            logger.debug("cardMasked: " + cardMasked);
            logger.debug("cardType: " + cardType);

            if (captureResult != null && "[capture-received]".equals(captureResult.get("response"))) {
                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("message", "");
                mapResponse.put("creditCard", cardMasked);
                mapResponse.put("qrCode", "");
                mapResponse.put("token", shopperRef + KeysUtil.TEMPKEY + recurringDetailReference);
                mapResponse.put("cardType", cardType);
                response.returnCode = 200;
                response.response = mapResponse;

                // void transaction
                //refundTransaction(pspReference, payAmount, isSandbox);
            } else {
                GatewayUtil.sendMail(requestId, KeysUtil.ADYEN, captureRequest.toString(), paymentCaptureResponse, "phone " + phone);
                response.returnCode = 437;
            }

        } catch(Exception ex) {
            GatewayUtil.sendMail(requestId, KeysUtil.ADYEN, ex, "phone " + phone);
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }

        return response.toString();
    }

    public String createPaymentWithCreditToken(String requestId, String reference, double amount, String shopperReference, String selectedRecurring, boolean isSandbox, String currencyISO,
                                    String payerEmail, String payerIP, String cardType) {
        ObjResponse response = new ObjResponse();
        try {
            String authorizeURL = isSandbox ? AUTHORIZE_SANDBOX : AUTHORIZE_PRODUCTION;
            String captureURL = isSandbox ? CAPTURE_SANDBOX : CAPTURE_PRODUCTION;
            int intAmount = 0;
            if (isZeroDecimal(currencyISO)) {
                intAmount = (int) amount;
            } else {
                DecimalFormat df = new DecimalFormat("#");
                intAmount = Integer.parseInt(df.format(amount*100));
            }

            CredentialsProvider provider = new BasicCredentialsProvider();
            UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(WS_USER, WS_PASSWORD);
            provider.setCredentials(AuthScope.ANY, credentials);

            HttpClient client = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();

            // Create authorize request
            JSONObject paymentRequest = new JSONObject();
            paymentRequest.put("merchantAccount", MERCHANT_ACCOUNT);
            paymentRequest.put("reference", reference);
            paymentRequest.put("shopperIP", payerIP);
            paymentRequest.put("shopperEmail", payerEmail);
            paymentRequest.put("shopperReference", shopperReference);
            paymentRequest.put("fraudOffset", "0");

            // Set amount
            JSONObject payAmount = new JSONObject();
            payAmount.put("currency", currencyISO);
            payAmount.put("value", intAmount);
            paymentRequest.put("amount", payAmount);

            // Set recurring contract
            paymentRequest.put("selectedRecurringDetailReference", selectedRecurring);
            paymentRequest.put("shopperInteraction", "ContAuth");

            JSONObject recurring = new JSONObject();
            recurring.put("contract", "RECURRING");
            paymentRequest.put("recurring", recurring);

            // CVC is only required for OneClick card payments
            /*JSONObject card = new JSONObject();
            card.put("cvc", cvv);
            paymentRequest.put("card", card);*/

            /**
             * Send the HTTP request with the specified variables in JSON.
             */
            HttpPost httpRequest = new HttpPost(authorizeURL);
            httpRequest.addHeader("Content-Type", "application/json");
            httpRequest.setEntity(new StringEntity(paymentRequest.toString(), "UTF-8"));
            logger.debug(requestId + " - Payment request: " + paymentRequest.toString());

            HttpResponse httpResponse = client.execute(httpRequest);
            String paymentResponse = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
            logger.debug(requestId + " - Payment result: " + paymentResponse);
            logger.debug(requestId + " - Code: " + httpResponse.getStatusLine().getStatusCode());

            // Parse JSON response
            JSONParser parser = new JSONParser();
            JSONObject paymentResult;

            try {
                paymentResult = (JSONObject) parser.parse(paymentResponse);
            } catch (Exception ex) {
                ex.printStackTrace();
                paymentResult = null;
            }

            String pspReference = "";
            if (paymentResult != null) {
                int httpCode = httpResponse.getStatusLine().getStatusCode();
                String errorCode = paymentResult.get("errorCode") != null ? paymentResult.get("errorCode").toString() : "";
                String resultCode = paymentResult.get("resultCode") != null ? paymentResult.get("resultCode").toString() : "";
                String refusalReason = paymentResult.get("refusalReason") != null ? paymentResult.get("refusalReason").toString() : "";
                pspReference = paymentResult.get("pspReference")!= null ? paymentResult.get("pspReference").toString() : "";
                logger.debug(requestId + " - errorCode: " + errorCode);
                logger.debug(requestId + " - resultCode: " + resultCode);
                logger.debug(requestId + " - refusalReason: " + refusalReason);
                logger.debug(requestId + " - pspReference: " + pspReference);
                if (200 != httpCode) {
                    GatewayUtil.sendMail(requestId, KeysUtil.ADYEN, paymentRequest.toString(), "HTTP Code: " + httpCode + " - " + paymentResponse, "bookId " + reference);
                    response.returnCode = getErrorCodeFromErrorCode(errorCode);
                    return response.toString();
                }
                if ("Refused".equals(resultCode)) {
                    GatewayUtil.sendMail(requestId, KeysUtil.ADYEN, paymentRequest.toString(), paymentResponse, "bookId " + reference);
                    response.returnCode = getErrorCodeFromMessage(refusalReason);
                    return response.toString();
                }
                if (pspReference.isEmpty()) {
                    GatewayUtil.sendMail(requestId, KeysUtil.ADYEN, paymentRequest.toString(), paymentResponse, "bookId " + reference);
                    response.returnCode = 437;
                    return response.toString();
                }
            } else {
                GatewayUtil.sendMail(requestId, KeysUtil.ADYEN, paymentRequest.toString(), paymentResponse, "bookId " + reference);
                response.returnCode = 437;
                return response.toString();
            }

            // Create capture request
            JSONObject captureRequest = new JSONObject();
            captureRequest.put("merchantAccount", MERCHANT_ACCOUNT);
            captureRequest.put("modificationAmount", payAmount);

            captureRequest.put("value", intAmount);
            captureRequest.put("currency", currencyISO);
            captureRequest.put("originalReference", pspReference);

            /**
             * Send the HTTP request with the specified variables in JSON.
             */
            HttpPost httpCaptureRequest = new HttpPost(captureURL);
            httpCaptureRequest.addHeader("Content-Type", "application/json");
            httpCaptureRequest.setEntity(new StringEntity(captureRequest.toString(), "UTF-8"));
            logger.debug(requestId + " - Capture request: " + captureRequest.toString());

            HttpResponse httpCaptureResponse = client.execute(httpCaptureRequest);
            String paymentCaptureResponse = EntityUtils.toString(httpCaptureResponse.getEntity(), "UTF-8");
            logger.debug(requestId + " - Capture result: " + paymentCaptureResponse);

            // Parse JSON response
            JSONObject captureResult;
            try {
                captureResult = (JSONObject) parser.parse(paymentCaptureResponse);
            } catch (Exception ex) {
                ex.printStackTrace();
                captureResult = null;
            }

            logger.debug(requestId + " - Payment Result: " + captureResult);
            if (captureResult != null && "[capture-received]".equals(captureResult.get("response"))) {
                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("message", "Success!");
                mapResponse.put("transId", captureResult.get("pspReference").toString());
                mapResponse.put("cardType", cardType);
                mapResponse.put("authCode", paymentResult.get("authCode")!= null ? paymentResult.get("authCode").toString() : "'");
                mapResponse.put("cardMask", "");

                response.returnCode = 200;
                response.response = mapResponse;
            } else {
                GatewayUtil.sendMail(requestId, KeysUtil.ADYEN, captureRequest.toString(), paymentCaptureResponse, "bookId " + reference);
                response.returnCode = 437;
            }
        } catch(Exception ex) {
            GatewayUtil.sendMail(requestId, KeysUtil.ADYEN, ex, "bookId " + reference);
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }

        return response.toString();
    }

    private int getErrorCodeFromMessage(String errorMsg) {
        int errorCode = 437; // default
        // https://docs.adyen.com/developers/ecommerce-integration/detailed-ecommerce-guide/response-handling
        switch (errorMsg) {
            case "3d-secure: Authentication failed" :
                errorCode = 458; // Authentication failed
                break;
            case "Acquirer Fraud" :
                errorCode = 2014; // Processor Declined - Fraud Suspected
                break;
            case "Blocked Card" :
                errorCode = 2038; // Processor Declined. The customer's bank is unwilling to accept the transaction, need to contact their bank for more details.
                break;
            case "CVC Declined" :
                errorCode = 2010; // Card Issuer Declined CVV
                break;
            case "Refused" :
                errorCode = 436; // This card has been declined by payment provider. Please contact the provider for more information
                break;
            case "Declined Non Generic" :
                errorCode = 436; // This card has been declined by payment provider. Please contact the provider for more information
                break;
            case "Expired Card" :
                errorCode = 2004; // Expired Card
                break;
            case "FRAUD" :
                errorCode = 2014; // Processor Declined - Fraud Suspected
                break;
            case "FRAUD-CANCELLED" :
                errorCode = 2014; // Processor Declined - Fraud Suspected
                break;
            case "Invalid Card Number" :
                errorCode = 2005; // Invalid Credit Card Number
                break;
            case "Issuer Suspected Fraud" :
                errorCode = 2014; // Processor Declined - Fraud Suspected
                break;
            case "Issuer Unavailable" :
                errorCode = 2043; // Error - Do Not Retry, Call Issuer
                break;
            case "Not enough balance" :
                errorCode = 2001; // Insufficient Funds. The account did not have sufficient funds to cover the transaction amount.
                break;
            case "Restricted Card" :
                errorCode = 2057; // Issuer or Cardholder has put a restriction on the card
                break;
            case "Transaction Not Permitted" :
                errorCode = 2015; // Transaction Not Allowed
                break;

        }

        return errorCode;
    }

    private int getErrorCodeFromErrorCode(String code) {
        int errorCode = 437; // default
        // https://docs.adyen.com/developers/ecommerce-integration/detailed-ecommerce-guide/response-handling
        switch (code) {
            case "101" :
                errorCode = 2005; // Invalid Credit Card Number
                break;
            case "103" :
                errorCode = 432; // CVV is invalid
                break;
            case "108" :
                errorCode = 2005; // Invalid Credit Card Number
                break;
            case "129" :
                errorCode = 422; // Expired date is invalid
                break;
            case "153" :
                errorCode = 432; // CVV is invalid
                break;
            case "701" :
                errorCode = 3000; // Processor Network Unavailable - Try Again
                break;
            case "800" :
                errorCode = 411; // Invalid data
                break;
            case "802" :
                errorCode = 411; // Invalid data
                break;

        }

        return errorCode;
    }

    private boolean isZeroDecimal(String currencyISO) {
        List<String> fullList = Arrays.asList("BIF", "DJF", "JPY", "KRW", "PYG", "VND", "XAF", "XPF", "CLP", "GNF", "KMF", "MGA", "RWF", "VUV", "XOF");
        return fullList.contains(currencyISO);
    }

    private void refundTransaction(String requestId, String pspReference, JSONObject payAmount, boolean isSandbox) {
        logger.debug(requestId + " - refundTransaction: " + pspReference);
        try {
            JSONObject refundRequest = new JSONObject();
            refundRequest.put("merchantAccount", MERCHANT_ACCOUNT);
            refundRequest.put("modificationAmount", payAmount);
            refundRequest.put("originalReference", pspReference);

            CredentialsProvider provider = new BasicCredentialsProvider();
            UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(WS_USER, WS_PASSWORD);
            provider.setCredentials(AuthScope.ANY, credentials);

            HttpClient client = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();

            /**
             * Send the HTTP request with the specified variables in JSON.
             */
            String refundURL = isSandbox ? REFUND_SANDBOX : REFUND_PRODUCTION;
            HttpPost httpRefundRequest = new HttpPost(refundURL);
            httpRefundRequest.addHeader("Content-Type", "application/json");
            httpRefundRequest.setEntity(new StringEntity(refundRequest.toString(), "UTF-8"));

            HttpResponse httpRefudnResponse = client.execute(httpRefundRequest);
            String paymentRefundResponse = EntityUtils.toString(httpRefudnResponse.getEntity(), "UTF-8");
            logger.debug(requestId + " - Refund Result: " + paymentRefundResponse);
        } catch(Exception ex) {
            ex.printStackTrace();
        }

    }

    public String initiateCreditForm(String requestId, String phone, String customerEmail, boolean isSandbox, String currencyISO){
        ObjResponse initiateCreditFormResponse = new ObjResponse();
        try {
            // Generate dates
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, 1);
            Date sessionDate = calendar.getTime(); // current date + 1 day
            calendar.add(Calendar.DATE, 2);
            Date shippingDate = calendar.getTime(); // current date + 3 days

            // Define variables
            phone = phone.replace("+", " ").replace(" ", "");
            String merchantReference = GatewayUtil.getOrderId();
            if (!phone.isEmpty())
                merchantReference = phone + "-" + GatewayUtil.getOrderId();
            String paymentAmount = "100";
            if (currencyISO.equalsIgnoreCase("INR"))
                paymentAmount = "1000";
            String shipBeforeDate = new SimpleDateFormat("yyyy-MM-dd").format(shippingDate);
            String skinCode = SKIN;
            String merchantAccount = MERCHANT_ACCOUNT;
            String sessionValidity = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX").format(sessionDate);
            String shopperLocale = "en_US";

            String allowedMethods = "";
            String blockedMethods = "";
            String offset = "";
            String recurringContract = "ONECLICK,RECURRING";

            logger.debug(requestId + " - paymentAmount = " + paymentAmount);
            logger.debug(requestId + " - currencyCode = " + currencyISO);
            logger.debug(requestId + " - shipBeforeDate = " + shipBeforeDate);
            logger.debug(requestId + " - merchantReference = " + merchantReference);
            logger.debug(requestId + " - skinCode = " + skinCode);
            logger.debug(requestId + " - merchantAccount = " + merchantAccount);
            logger.debug(requestId + " - sessionValidity = " + sessionValidity);
            logger.debug(requestId + " - shopperEmail = " + customerEmail);
            logger.debug(requestId + " - shopperReference = " + merchantReference);
            logger.debug(requestId + " - allowedMethods = " + allowedMethods);
            logger.debug(requestId + " - blockedMethods = " + blockedMethods);
            logger.debug(requestId + " - offset = " + offset);
            logger.debug(requestId + " - recurringContract = " + recurringContract);

            String merchantSig = getSig(merchantReference, sessionValidity, shipBeforeDate, paymentAmount,
                    currencyISO, customerEmail, shopperLocale, merchantReference, offset, recurringContract);

            String hppURL = isSandbox ? HPP_SANDBOX : HPP_PRODUCTION;

            String paymentUrl = hppURL
                    + "?merchantAccount=" + URLEncoder.encode(merchantAccount, "UTF-8")
                    + "&merchantReference=" + URLEncoder.encode(merchantReference, "UTF-8")
                    + "&sessionValidity=" + URLEncoder.encode(sessionValidity, "UTF-8")
                    + "&shipBeforeDate=" + URLEncoder.encode(shipBeforeDate, "UTF-8")
                    + "&currencyCode=" + URLEncoder.encode(currencyISO, "UTF-8")
                    + "&paymentAmount=" + URLEncoder.encode(paymentAmount, "UTF-8")
                    + "&shopperEmail=" + URLEncoder.encode(customerEmail, "UTF-8")
                    + "&skinCode=" + URLEncoder.encode(skinCode, "UTF-8")
                    + "&shopperLocale=" + URLEncoder.encode(shopperLocale, "UTF-8")
                    + "&shopperReference=" + URLEncoder.encode(merchantReference, "UTF-8")
                    + "&offset=" + URLEncoder.encode(offset, "UTF-8")
                    + "&recurringContract=" + URLEncoder.encode(recurringContract, "UTF-8")
                    + "&merchantSig=" + URLEncoder.encode(merchantSig, "UTF-8");
            logger.debug(requestId + " - paymentUrl: " + paymentUrl);

            Map<String,String> mapResponse = new HashMap<>();
            mapResponse.put("3ds_url", paymentUrl);
            mapResponse.put("type", "link");
            initiateCreditFormResponse.returnCode = 200;
            initiateCreditFormResponse.response = mapResponse;
            return initiateCreditFormResponse.toString();
        } catch(Exception ex) {
            GatewayUtil.sendMail(requestId, KeysUtil.ADYEN, ex, "phone " + phone);
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }

    }

    private static String getSig(String merchantReference, String sessionValidity, String shipBeforeDate, String paymentAmount,
                                 String currencyCode, String shopperEmail, String shopperLocale, String shopperReference, String offset, String recurringContract) throws Exception{

        Map<String, String> pairs = new HashMap<>();
        pairs.put("merchantReference", merchantReference);
        pairs.put("merchantAccount", MERCHANT_ACCOUNT);
        pairs.put("sessionValidity", sessionValidity);
        pairs.put("shipBeforeDate", shipBeforeDate);
        pairs.put("paymentAmount", paymentAmount);
        pairs.put("currencyCode", currencyCode);
        pairs.put("shopperEmail", shopperEmail);
        pairs.put("shopperLocale", shopperLocale);
        pairs.put("shopperReference", shopperReference);
        pairs.put("skinCode", SKIN);
        pairs.put("offset", offset);
        pairs.put("recurringContract", recurringContract);

        SortedMap<String, String> sortedPairs = new TreeMap<>(pairs);
        SortedMap<String, String> escapedPairs =
                sortedPairs.entrySet().stream()
                        .collect(Collectors.toMap(
                                e -> e.getKey(),
                                e -> (e.getValue() == null) ? "" : e.getValue().replace("\\", "\\\\").replace(":", "\\:"),
                                (k, v) -> k,
                                TreeMap::new
                        ));
        String signingString = Stream.concat(escapedPairs.keySet().stream(), escapedPairs.values().stream())
                .collect(Collectors.joining(":"));
        logger.debug("signingString = " + signingString);

        // import from com.google.common.io.BaseEncoding;
        byte[] binaryHmacKey = BaseEncoding.base16().decode(HMAC_KEY);

        // Create an HMAC SHA-256 key from the raw key bytes
        SecretKeySpec signingKey = new SecretKeySpec(binaryHmacKey, "HmacSHA256");

        // Get an HMAC SHA-256 Mac instance and initialize with the signing key
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(signingKey);

        // calculate the hmac on the binary representation of the signing string
        byte[] binaryHmac = mac.doFinal(signingString.getBytes(Charset.forName("UTF8")));

        return Base64.getEncoder().encodeToString(binaryHmac);
    }
}
