package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.braintreegateway.*;
import com.google.gson.Gson;
import com.qupworld.paymentgateway.entities.CreditEnt;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by qup on 8/2/16.
 */
@SuppressWarnings("unchecked")
public class BraintreeUtil {
    final static Logger logger = LogManager.getLogger(BraintreeUtil.class);
    private String requestId;
    private Gson gson;
    public BraintreeUtil(String _requestId) {
        requestId = _requestId;
        gson = new Gson();
    }

    public String createCreditToken(CreditEnt credit, String braintreeId, Map<String, String> account) throws IOException {
        logger.debug(requestId + " - account :" + account.toString());
        ObjResponse response = new ObjResponse();

        BraintreeGateway gateway = getBraintreeGateway(account);
        // check setting info
        try {
            Customer customer = gateway.customer().find(braintreeId);
            logger.debug(requestId + " - customer " + customer.getId() + " ... found!");
        } catch (Exception ex) {
            logger.debug(requestId + " - get customer ERROR: " + ex.getMessage());
            logger.debug(requestId + " - customer " + braintreeId + " ... not found!");
            try {
                CustomerRequest request = new CustomerRequest()
                        .id(braintreeId)
                        .firstName(credit.cardHolder);
                Result<Customer> result = gateway.customer().create(request);

                logger.debug(requestId + " - add customer " + result.getTarget().getId() + " success!");
            } catch (Exception e) {
                ex.printStackTrace();
            }
        }
        Map<String,String> mapResult = new HashMap<>();
        try {
            if (!isEmptyString(credit.postalCode)) {
                AddressRequest addressRequest = new AddressRequest().postalCode(credit.postalCode);
                Result<Address> addressResult = gateway.address().create(braintreeId, addressRequest);
                if (addressResult.isSuccess()) {
                    String addressId = addressResult.getTarget().getId();
                    CreditCardRequest request = new CreditCardRequest()
                            .customerId(braintreeId)
                            .number(credit.cardNumber)
                            .expirationDate(credit.expiredDate)
                            .cardholderName(credit.cardHolder)
                            .cvv(credit.cvv)
                            .billingAddressId(addressId)
                            .options()
                            .makeDefault(true)
                            .verifyCard(true)
                            .done();

                    String cardLog = "XXXXXXXXXXXX" + credit.cardNumber.substring(credit.cardNumber.length() - 4);
                    String log = gson.toJson(request).replace(credit.cardNumber, cardLog);
                    logger.debug(requestId + " - request: " + log);
                    Result<CreditCard> result = gateway.creditCard().create(request);
                    logger.debug(requestId + " - response " + gson.toJson(result));
                    if (result.isSuccess()) {
                        String token = result.getTarget().getToken();
                        String cardType = result.getTarget().getCardType();
                        String cardMasked = result.getTarget().getMaskedNumber();

                        mapResult.put("message", addressResult.getMessage());
                        mapResult.put("creditCard", cardMasked);
                        mapResult.put("qrCode", "");
                        mapResult.put("token", token);
                        mapResult.put("cardType", cardType);

                        response.response = mapResult;
                        response.returnCode = 200;
                        return  response.toString();

                        //return CreditCardUtil.createCreditCard(token, cardType, cardMasked, expiredMonth, expiredYear, holderName, KeysUtil.PAYMENT_BRAINTREE, userId, groupId, companyId);

                    } else {

                        return returnCreditCardFailed(result);
                    }
                } else {
                    response.returnCode = 406;
                    return  response.toString();
                }

            } else {
                CreditCardRequest request = new CreditCardRequest()
                        .customerId(braintreeId)
                        .number(credit.cardNumber)
                        .expirationDate(credit.expiredDate)
                        .cardholderName(credit.cardHolder)
                        .cvv(credit.cvv)
                        .options()
                        .makeDefault(true)
                        .verifyCard(true)
                        .done();
                String cardLog = "XXXXXXXXXXXX" + credit.cardNumber.substring(credit.cardNumber.length() - 4);
                String log = gson.toJson(request).replace(credit.cardNumber, cardLog);
                logger.debug(requestId + " - request: " + log);
                Result<CreditCard> result = gateway.creditCard().create(request);
                logger.debug(requestId + " - response " + gson.toJson(result));
                if (result.isSuccess()) {
                    String token = result.getTarget().getToken();
                    String cardType = result.getTarget().getCardType();
                    String cardMasked = result.getTarget().getMaskedNumber();

                    mapResult.put("message", result.getMessage());
                    mapResult.put("creditCard", cardMasked);
                    mapResult.put("qrCode", "");
                    mapResult.put("token", token);
                    mapResult.put("cardType", cardType);

                    response.response = mapResult;
                    response.returnCode = 200;
                    return  response.toString();

                    //return CreditCardUtil.createCreditCard(token, cardType, cardMasked, expiredMonth, expiredYear, holderName, KeysUtil.PAYMENT_BRAINTREE, userId, groupId, companyId);

                } else {

                    return returnCreditCardFailed(result);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            response.returnCode = 406;
            response.response = GatewayUtil.getError(ex);
            return  response.toString();
        }
    }

    private static BraintreeGateway getBraintreeGateway(Map<String, String> account) {
        try {
            String env = account.get("environment");
            Environment environment = null;
            if (env.equalsIgnoreCase(KeysUtil.SANDBOX))
                environment = Environment.SANDBOX;
            else if (env.equalsIgnoreCase(KeysUtil.PRODUCTION))
                environment = Environment.PRODUCTION;
            return new BraintreeGateway(environment, account.get("merchantID"), account.get("publicKey"), account.get("privateKey"));
        } catch (Exception ex) {
            ex.printStackTrace();

            return null;
        }
    }

    public boolean isEmptyString(String s) {
        return (s == null || s.trim().isEmpty());
    }

    private static String returnCreditCardFailed(Result<CreditCard> result)  {
        ObjResponse response = new ObjResponse();
        String resultMessage = result.getMessage();
        String avsCode = "";
        String cvvCode = "";
        try {
            avsCode = result.getCreditCardVerification().getAvsPostalCodeResponseCode() != null ? result.getCreditCardVerification().getAvsPostalCodeResponseCode() : "";
            cvvCode = result.getCreditCardVerification().getCvvResponseCode() != null ? result.getCreditCardVerification().getCvvResponseCode() : "";
        } catch (Exception ex) {
            logger.debug("get return code ERROR: " + ex.getMessage());
        }
        String errorCode = getReturnCode(resultMessage, avsCode, cvvCode);
        if (errorCode.equals(""))
            errorCode = result.getCreditCardVerification().getProcessorResponseCode();

        // return default code if not match any case
        if (errorCode.equals(""))
            errorCode = "437";
        Map<String,String> mapResponse = new HashMap<>();
        mapResponse.put("message", resultMessage);
        response.returnCode = Integer.parseInt(errorCode);
        response.response = mapResponse;
        return response.toString();
    }
    private static String getReturnCode(String resultMessage, String avsResponse, String cvvResponse) {
        logger.debug("resultMessage: " + resultMessage);
        String errorCode = "";
        avsResponse = avsResponse.trim();
        cvvResponse = cvvResponse.trim();
        if (resultMessage.contains("avs") || resultMessage.contains("AVS") || resultMessage.contains("Avs")
                || resultMessage.contains("Postal code") || resultMessage.contains("Postal Code")) {
            errorCode = "429";
        } else if (resultMessage.contains("cvv") || resultMessage.contains("CVV") || resultMessage.contains("Cvv")) {
            errorCode = "432";
        } else if (resultMessage.contains("Credit card") || resultMessage.contains("credit card") || resultMessage.contains("Credit Card")) {
            errorCode = "421";
        } else if (resultMessage.contains("Expiration") || resultMessage.contains("expiration")
                || resultMessage.contains("Expired") || resultMessage.contains("expired")) {
            errorCode = "422";
        }

        logger.debug("avsCode: " + avsResponse);
        if (avsResponse.equalsIgnoreCase("N")) {
            errorCode = "430";
        } else if (avsResponse.equalsIgnoreCase("U")) {
            errorCode = "431";
        } else if (avsResponse.equalsIgnoreCase("I")) {
            errorCode = "429";
        }

        logger.debug("cvvCode: " + cvvResponse);
        if (cvvResponse.equalsIgnoreCase("N")) {
            errorCode = "433";
        } else if (cvvResponse.equalsIgnoreCase("U")) {
            errorCode = "434";
        } else if (cvvResponse.equalsIgnoreCase("S")) {
            errorCode = "435";
        } else if (cvvResponse.equalsIgnoreCase("I")) {
            errorCode = "432";
        }

        return errorCode;
    }

    private String returnPaymentFailed(Result<Transaction> result, String fleetId, double amount, String bookId, String token) {
        ObjResponse response = new ObjResponse();
        String resultMessage = result.getMessage();
        String avsCode = "";
        String cvvCode = "";
        try {
            avsCode = result.getCreditCardVerification().getAvsPostalCodeResponseCode() != null ? result.getCreditCardVerification().getAvsPostalCodeResponseCode() : "";
            cvvCode = result.getCreditCardVerification().getCvvResponseCode() != null ? result.getCreditCardVerification().getCvvResponseCode() : "";
        } catch (Exception ex) {
            logger.debug("get return code ERROR: " + ex.getMessage());
        }
        String errorCode = getReturnCode(resultMessage, avsCode, cvvCode);
        if (errorCode.equals(""))
            errorCode = result.getTransaction().getProcessorResponseCode();

        // return default code if not match any case
        if (errorCode.equals(""))
            errorCode = "437";

        // add log history
        String braintreeTransId = "";
        if (result.getTransaction() != null)
            braintreeTransId = result.getTransaction().getId();
        String cardMask = "";
        String cardType = "";
        String authorizationCode = "";
        String status = "";
        String gatewayRejectionReason = resultMessage;

        Map<String,String> mapResponse = new HashMap<>();
        mapResponse.put("message", resultMessage);
        response.returnCode = Integer.parseInt(errorCode);
        response.response = mapResponse;
        return response.toString();
    }
    public String createPaymentWithCreditToken(double amount, String token, String bookId, Map<String,String> account){
        logger.debug(requestId + " - createPaymentWithCreditToken " + token + " ...");
        ObjResponse response = new ObjResponse();
        String braintreeTransId = "";
        DecimalFormat df = new DecimalFormat("#.##");
        // if payment local zone, remove MerchantAccountId from QUp
        TransactionRequest request = new TransactionRequest()
                .amount(new BigDecimal(df.format(amount)))
                .paymentMethodToken(token)
                .options()
                .submitForSettlement(true)
                .done();
        if (!account.get("merchantAccountID").equals("")) {
            request = new TransactionRequest()
                    .amount(new BigDecimal(df.format(amount)))
                    .paymentMethodToken(token)
                    .merchantAccountId(account.get("merchantAccountID"))
                    .options()
                    .submitForSettlement(true)
                    .done();
        }
        logger.debug(requestId + " - request: " + gson.toJson(request));
        BraintreeGateway gateway = getBraintreeGateway(account);
        Result<Transaction> result = gateway.transaction().sale(request);
        logger.debug(requestId + " - response: " + gson.toJson(result));
        Map<String,String> mapResponse = new HashMap<>();
        if (result.isSuccess()) {
            // add log history
            try {
                Transaction transaction = result.getTarget();
                braintreeTransId = transaction.getId();
                String cardMask = transaction.getCreditCard().getMaskedNumber();
                String cardType = transaction.getCreditCard().getCardType();
                String authorizationCode = transaction.getProcessorAuthorizationCode();
                String status =  transaction.getStatus().name();
                String gatewayRejectionReason = "";
                if (transaction.getGatewayRejectionReason()!=null)
                    gatewayRejectionReason = transaction.getGatewayRejectionReason().toString();

                mapResponse.put("message", "Success!");
                mapResponse.put("transId", braintreeTransId);
                mapResponse.put("cardType", cardType);
                mapResponse.put("authCode", authorizationCode);
                mapResponse.put("cardMask", cardMask);

                response.response = mapResponse;
                response.returnCode = 200;
                return response.toString();
            } catch (Exception ex) {
                ex.printStackTrace();

                // get error, void completed transaction
                if (!braintreeTransId.equals("")) {
                    voidBraintreeTransaction(account, braintreeTransId);
                }

                response.returnCode = 437;
                return response.toString();
            }

        } else {

            return returnPaymentFailed(result, account.get("fleetId"), amount, bookId, token);

        }
    }

    public String voidBraintreeTransaction(Map<String, String> account, String braintreeId) {
        ObjResponse response = new ObjResponse();
        try {
            String transId = "";
            BraintreeGateway gateway = getBraintreeGateway(account);
            if (!braintreeId.equals("")) {
                Result<Transaction> result = gateway.transaction().voidTransaction(braintreeId);
                if (result.isSuccess()) {
                    response.returnCode = 200;
                } else {
                    response.returnCode = 437;
                }

                if (result.getTransaction() != null)
                    transId = result.getTransaction().getId();
            }

            response.returnCode = 200;
            logger.debug(requestId + " - braintreeId: " + braintreeId + " has been voided !!!! RefundId: " + transId);
        } catch(Exception ex) {
            ex.printStackTrace();
            logger.debug(requestId + " - voidBraintreeTransaction ERROR: " + GatewayUtil.getError(ex));
            response.returnCode = 437;
        }
        return response.toString();
    }

    public String preAuthPayment(double amount, String token, String bookId, Map<String,String> account) {
        ObjResponse response = new ObjResponse();
        String braintreeTransId = "";
        try {
            DecimalFormat df = new DecimalFormat("#.##");
            // if payment local zone, remove MerchantAccountId from QUp
            TransactionRequest request = new TransactionRequest()
                    .amount(new BigDecimal(df.format(amount)))
                    .paymentMethodToken(token)
                    .options()
                    .done();
            logger.debug(requestId + " - request: " + gson.toJson(request));
            BraintreeGateway gateway = getBraintreeGateway(account);
            Result<Transaction> result = gateway.transaction().sale(request);
            logger.debug(requestId + " - response: " + gson.toJson(result));
            if (result.isSuccess()) {
                // add log history
                Transaction transaction = result.getTarget();
                braintreeTransId = transaction.getId();
                String cardMask = transaction.getCreditCard().getMaskedNumber();
                String cardType = transaction.getCreditCard().getCardType();
                String authorizationCode = transaction.getProcessorAuthorizationCode();
                String status =  transaction.getStatus().name();
                String gatewayRejectionReason = "";
                if (transaction.getGatewayRejectionReason()!=null)
                    gatewayRejectionReason = transaction.getGatewayRejectionReason().toString();

                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("authId", braintreeTransId);
                mapResponse.put("authAmount", String.valueOf(amount));
                mapResponse.put("authCode", authorizationCode);
                mapResponse.put("authToken", token);
                mapResponse.put("allowCapture", "true");

                response.response = mapResponse;
                response.returnCode = 200;
                return response.toString();

            } else {

                return returnPaymentFailed(result, account.get("fleetId"), amount, bookId, token);

            }
        } catch(Exception ex) {
            ex.printStackTrace();

            // get error, void completed transaction
            if (!braintreeTransId.equals("")) {
                voidBraintreeTransaction(account, braintreeTransId);
            }
            response.returnCode = 437;
            response.response = GatewayUtil.getError(ex);
            return response.toString();
        }
    }

    public String preAuthCapture(String bookId, String token, double amount, String authId, Map<String,String> account) {
        ObjResponse response = new ObjResponse();
        String braintreeTransId = "";
        try {
            DecimalFormat df = new DecimalFormat("#.##");
            BraintreeGateway gateway = getBraintreeGateway(account);
            Result<Transaction> result = gateway.transaction().submitForSettlement(authId, new BigDecimal(df.format(amount)));
            logger.debug(requestId + " - result: " + gson.toJson(result));
            if (result.isSuccess()) {
                // add log history
                Transaction transaction = result.getTarget();
                braintreeTransId = transaction.getId();
                String cardMask = transaction.getCreditCard().getMaskedNumber();
                String cardType = transaction.getCreditCard().getCardType();
                String authorizationCode = transaction.getProcessorAuthorizationCode();
                String status =  transaction.getStatus().name();
                String gatewayRejectionReason = "";
                if (transaction.getGatewayRejectionReason()!=null)
                    gatewayRejectionReason = transaction.getGatewayRejectionReason().toString();

                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("message", "Success!");
                mapResponse.put("transId", braintreeTransId);
                mapResponse.put("cardType", cardType);
                mapResponse.put("authCode", authorizationCode);
                mapResponse.put("cardMask", cardMask);

                response.response = mapResponse;
                response.returnCode = 200;
                return response.toString();
            } else {

                return returnPaymentFailed(result, account.get("fleetId"), amount, bookId, token);

            }
        } catch(Exception ex) {
            ex.printStackTrace();

            // get error, void completed transaction
            if (!braintreeTransId.equals("")) {
                    voidBraintreeTransaction(account, braintreeTransId);
            }

            response.returnCode = 437;
            response.response = GatewayUtil.getError(ex);
            return response.toString();
        }
    }
}
