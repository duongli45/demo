package com.qupworld.paymentgateway.controllers.threading;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.controllers.GatewayUtil.GatewayUtil;
import com.qupworld.paymentgateway.controllers.InboxUtil;
import com.qupworld.paymentgateway.controllers.MerchantWalletUtil;
import com.qupworld.paymentgateway.controllers.PaymentUtil;
import com.qupworld.paymentgateway.controllers.UpdateMerchantWalletBalance;
import com.qupworld.paymentgateway.entities.MerchantTransactionEnt;
import com.qupworld.paymentgateway.entities.PayoutHistoryEnt;
import com.qupworld.paymentgateway.models.*;
import com.qupworld.paymentgateway.models.mongo.collections.Logs.Logs;
import com.qupworld.paymentgateway.models.mongo.collections.bankInfo.BankInfo;
import com.qupworld.paymentgateway.models.mongo.collections.menuMerchant.MenuMerchant;
import com.qupworld.paymentgateway.models.mongo.collections.menuMerchantUser.MenuMerchantUser;
import com.qupworld.paymentgateway.models.mongo.collections.pricingplan.PricingPlan;
import com.qupworld.service.MailService;
import com.qupworld.service.QUpReportPublisher;
import com.pg.util.CommonArrayUtils;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.TimezoneUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class PayoutMerchantThread extends Thread {

    final static Logger logger = LogManager.getLogger(PayoutMerchantThread.class);
    public List<String> listMerchant;
    public String payoutDate;
    public String payoutId;
    public String fleetId;
    public String fleetName;
    public String country;
    public String timezone;
    public String currencyISO;
    public String operatorId;
    public String operatorName;
    public String operatorUserName;
    public String email;
    public String companyId;
    public double minPayoutAmount;
    public double holdAmount;
    public int totalMerchant;
    public String sessionId;
    public String requestId;
    public String payoutTarget;
    public org.json.JSONArray jsonError;

    @Override
    public void run() {
        MongoDao mongoDao = new MongoDaoImpl();
        SQLDao sqlDao = new SQLDaoImpl();
        RedisDao redisDao = new RedisImpl();
        try {
            Gson gson = new GsonBuilder().serializeNulls().create();
            JSONArray arrSuccessReport = new JSONArray(); // contain success payout to send Report
            List<String> arrSuccess = new ArrayList<>();
            double totalPayoutAmount = 0.00;
            for (String merchantId : listMerchant) {
                try {
                    logger.debug(requestId + " - payout for merchant " + merchantId);
                    // get current balance before update db
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                    String queryDate = sdf.format(KeysUtil.SDF_DMYHMSTZ.parse(payoutDate));
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
                    LocalDateTime dateTime = LocalDateTime.parse(queryDate, formatter);
                    List<String> merchantQuery = Collections.singletonList(merchantId);
                    PricingPlan pricingPlan = mongoDao.getPricingPlan(fleetId);
                    // if enable cash wallet then payout from cash balance
                    boolean enableCashWallet = pricingPlan.merchantCashWallet != null && pricingPlan.merchantCashWallet.enable;
                    double pendingAmount;
                    if (enableCashWallet) {
                        pendingAmount = sqlDao.getMerchantWalletBalance(requestId, fleetId, merchantId, "cash", currencyISO);
                    } else {
                        pendingAmount = sqlDao.getPendingAmount(fleetId, merchantQuery, currencyISO, Timestamp.valueOf(dateTime));
                    }
                    logger.debug(requestId + " - merchantId: " + merchantId + " - pendingAmount before update: " + pendingAmount);
                    if (pendingAmount > 0) {
                        double payoutAmount = Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(pendingAmount - holdAmount));
                        logger.debug(requestId + " - payoutAmount: " + payoutAmount);
                        if (payoutAmount > 0) {
                            double balanceAfterUpdate = Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(pendingAmount - payoutAmount));

                            // update to Report
                            MenuMerchant merchant = mongoDao.getMerchant(merchantId);
                            String adminId = merchant.admin != null ? merchant.admin.toString() : "";
                            MenuMerchantUser menuMerchantUser = null;
                            if (!adminId.isEmpty()) {
                                menuMerchantUser = mongoDao.getMenuMerchantUser(adminId);
                            }
                            String merchantName = "";
                            String phone = "";
                            String email = "";
                            String bankAccountHolder = "";
                            String accountNumber = "";
                            String driverNumber = "";
                            String driverNumberType = "";
                            String bankName = "";
                            String bankCode = "";
                            String IFSCCode = "";
                            if (merchant != null && merchant.bankInfo != null && menuMerchantUser != null) {
                                merchantName = merchant.name != null ? merchant.name : "";
                                phone = menuMerchantUser.phone != null && menuMerchantUser.phone.full != null ? menuMerchantUser.phone.full : "";
                                bankAccountHolder = merchant.bankInfo.accountHolder != null ? merchant.bankInfo.accountHolder : "";
                                accountNumber = merchant.bankInfo.accountNumber != null ? merchant.bankInfo.accountNumber : "";
                                if (payoutTarget.equals(KeysUtil.DNB)) {
                                    accountNumber = merchant.bankInfo.ibanNumber != null ? merchant.bankInfo.ibanNumber : "";
                                }
                                if (payoutTarget.equals(KeysUtil.MOLPAY)) {
                                    /*"bankInfo" : {
                                        "accountHolder" : "QA Test",
                                        "accountNumber" : "123456789",
                                        "bankName" : "BOFAMY2X",
                                        "IDnumber" : "12346548971",
                                        "IDtype" : "OLDNRIC",
                                        "bankCode" : "PEMBMYK1"
                                    },*/
                                    bankCode = merchant.bankInfo.bankCode != null ? merchant.bankInfo.bankCode : "";
                                    driverNumber = merchant.bankInfo.IDnumber != null ? merchant.bankInfo.IDnumber : "";
                                    driverNumberType = merchant.bankInfo.IDtype != null ? merchant.bankInfo.IDtype : "";
                                    if (!bankCode.isEmpty()) {
                                        BankInfo bankInfo = mongoDao.getBankInfoByBankCode(KeysUtil.MOLPAY, bankCode, country);
                                        if (bankInfo != null) {
                                            bankName = bankInfo.bankName != null ? bankInfo.bankName : "";
                                        } else {
                                            // invalid bank
                                            bankCode = "";
                                        }
                                    }
                                } else {
                                    // try to get bank name from profile
                                    bankName = merchant.bankInfo != null && merchant.bankInfo.bankName != null ? merchant.bankInfo.bankName : "";
                                }
                                if (payoutTarget.equals(KeysUtil.INDIA_DEFAULT)) {
                                    IFSCCode = merchant.bankInfo.IFSCCode != null ? merchant.bankInfo.IFSCCode : "";
                                }
                                if (payoutTarget.equals(KeysUtil.STRIPE)) {
                                    /*"bankInfo" : {
                                        "accountHolder" : "Texas 06",
                                        "accountNumber" : "000123456789",
                                        "address" : "1033 Young St, Dallas, TX 75202, Hoa Kỳ",
                                        "bankToken" : "acct_1JI3T04hTKRJ0leK",
                                        "birthDay" : "05/02/1995",
                                        "city" : "Dallas",
                                        "isBankAccountOwner" : true,
                                        "postalCode" : "75202",
                                        "routingNumber" : "110000000",
                                        "ssn" : "123456789",
                                        "state" : "TX",
                                        "verificationDocumentBack" : "https://lab-gojo.s3.ap-southeast-1.amazonaws.com/images/gojotexas/merchant/1627442954861_untitle_2_d_png",
                                        "verificationDocumentFront" : "https://lab-gojo.s3.ap-southeast-1.amazonaws.com/images/gojotexas/merchant/1627442954890_add_error_jpg",
                                        "isBankVerified" : true
                                    },*/
                                    String countryCode = GatewayUtil.getCountry2Code(country);
                                    List<String> useAccountNumber = Arrays.asList("US", "CA", "GB");
                                    if (!useAccountNumber.contains(countryCode)) {
                                        accountNumber = merchant.bankInfo.ibanNumber != null ? merchant.bankInfo.ibanNumber : "";
                                    }
                                }
                            }

                            // check bank number then ignore if not valid
                            //String textInBank = accountNumber.replaceAll("[0-9 ]", "").trim();
                            String textInBank = accountNumber.replaceAll("[^a-zA-Z]", "").trim();
                            if (payoutTarget.equals(KeysUtil.MOLPAY) && textInBank.length() > 0) {
                                logger.debug(requestId + " - payout merchantId " + merchantId + " is error: textInBank: " + textInBank);
                                continue;
                            }

                            PayoutHistoryEnt payoutHistoryEnt = new PayoutHistoryEnt();
                            payoutHistoryEnt.fleetId = fleetId;
                            payoutHistoryEnt.payTo = "merchant";
                            payoutHistoryEnt.merchantId = merchantId;
                            payoutHistoryEnt.merchantName = merchantName;
                            payoutHistoryEnt.driverId = "";
                            payoutHistoryEnt.driverName = "";
                            payoutHistoryEnt.phone = phone;
                            payoutHistoryEnt.email = email;
                            payoutHistoryEnt.companyId = "";
                            payoutHistoryEnt.companyName = "";
                            payoutHistoryEnt.driverType = "";
                            payoutHistoryEnt.driverNumber = driverNumber;
                            payoutHistoryEnt.driverNumberType = driverNumberType;
                            payoutHistoryEnt.operatorId = operatorId;
                            payoutHistoryEnt.operatorName = operatorName;
                            payoutHistoryEnt.transactionType = "";
                            payoutHistoryEnt.bopIndicator = "";
                            payoutHistoryEnt.paidAmount = payoutAmount;
                            payoutHistoryEnt.newBalance = balanceAfterUpdate;
                            payoutHistoryEnt.currencyISO = currencyISO;
                            payoutHistoryEnt.bankAccountHolder = bankAccountHolder;
                            payoutHistoryEnt.accountNumber = accountNumber;
                            payoutHistoryEnt.bankName = bankName;
                            payoutHistoryEnt.IFSCCode = IFSCCode;
                            payoutHistoryEnt.bankCode = bankCode;
                            payoutHistoryEnt.payoutId = payoutId;
                            payoutHistoryEnt.payoutDate = new Timestamp(KeysUtil.SDF_DMYHMSTZ.parse(payoutDate).getTime());
                            payoutHistoryEnt.createdDate = TimezoneUtil.getGMTTimestamp();
                            payoutHistoryEnt.isBankAccountOwner = true;
                            payoutHistoryEnt.beneficiaryIDIC = "";
                            payoutHistoryEnt.bankRelationship = 0;
                            payoutHistoryEnt.relationshipOtherName = "";

                            SimpleDateFormat sdfTransaction = new SimpleDateFormat("yyMMddHHmmss");
                            String transactionId = sdfTransaction.format(TimezoneUtil.offsetTimeZone(payoutHistoryEnt.payoutDate, "GMT", timezone));
                            payoutHistoryEnt.transactionId = transactionId;

                            // update merchant transaction before update history
                            int update = sqlDao.updateMerchantTransaction(fleetId, merchantQuery, currencyISO, Timestamp.valueOf(dateTime));
                            logger.debug(requestId + " - updateMerchantTransaction: " + update);
                            if (update == 1) {
                                // Save to MySQL
                                long idHistory = sqlDao.addPayoutHistory(payoutHistoryEnt);
                                if (idHistory > 0) {
                                    // generate data send to Report
                                    payoutHistoryEnt.transactionId = transactionId + "-" + String.valueOf(idHistory);
                                    String payoutHistoryStr = gson.toJson(payoutHistoryEnt);
                                    JSONObject payoutHistoryJSON = gson.fromJson(payoutHistoryStr, JSONObject.class);
                                    payoutHistoryJSON.remove("createdDate");
                                    payoutHistoryJSON.put("createdDate", TimezoneUtil.formatISODate(payoutHistoryEnt.createdDate, "GMT"));
                                    payoutHistoryJSON.remove("payoutDate");
                                    payoutHistoryJSON.put("payoutDate", TimezoneUtil.formatISODate(payoutHistoryEnt.payoutDate, "GMT"));

                                    JSONObject objReport = new JSONObject();
                                    objReport.put("type", "PayoutHistory");
                                    objReport.put("id", String.valueOf(idHistory));
                                    objReport.put("body", payoutHistoryJSON);
                                    arrSuccessReport.add(objReport);
                                    arrSuccess.add(merchantId);
                                    totalPayoutAmount += payoutAmount;

                                    //send inbox merchant payout
                                    InboxUtil inboxUtil = new InboxUtil(requestId);
                                    inboxUtil.action = "sendInboxMerchantPayout";
                                    inboxUtil.userId = merchant.admin != null ? merchant.admin.toString() : "";
                                    inboxUtil.currencyISO = currencyISO;
                                    inboxUtil.referenceId = payoutId;
                                    inboxUtil.amount = payoutAmount;
                                    inboxUtil.bankAccount = accountNumber.length() > 4 ? accountNumber.substring(accountNumber.length() - 4) : accountNumber;
                                    inboxUtil.start();

                                    // check if enable cash wallet
                                    if (enableCashWallet) {
                                        // deduct amount from cash wallet
                                        MerchantWalletUtil merchantWalletUtil = new MerchantWalletUtil(requestId);
                                        double currentBalance = merchantWalletUtil.getCurrentBalance(fleetId, merchantId, "cash", currencyISO);
                                        logger.debug(requestId + " - currentBalance: " + currentBalance);
                                        double newBalance = Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(currentBalance - payoutAmount));
                                        logger.debug(requestId + " - newBalance: " + newBalance);
                                        UpdateMerchantWalletBalance updateMerchantWalletBalance = new UpdateMerchantWalletBalance();
                                        updateMerchantWalletBalance.walletType = "cash";
                                        updateMerchantWalletBalance.fleetId = fleetId;
                                        updateMerchantWalletBalance.merchantId = merchantId;
                                        updateMerchantWalletBalance.currentBalance = currentBalance;
                                        updateMerchantWalletBalance.amount = payoutAmount * (-1);
                                        updateMerchantWalletBalance.newBalance = newBalance;
                                        updateMerchantWalletBalance.currencyISO = currencyISO;
                                        updateMerchantWalletBalance.mongoDao = mongoDao;
                                        updateMerchantWalletBalance.sqlDao = sqlDao;
                                        updateMerchantWalletBalance.reason = "";
                                        updateMerchantWalletBalance.transactionType = CommonArrayUtils.MERCHANT_CASH_WALLET_TYPE[3];
                                        updateMerchantWalletBalance.transactionId = CommonUtils.getOrderId();
                                        updateMerchantWalletBalance.operatorId = "";
                                        updateMerchantWalletBalance.requestId = requestId;
                                        String updateResult = updateMerchantWalletBalance.doUpdate();
                                        logger.debug(requestId + " - updateResult: " + updateResult);
                                    } else {
                                        // else add hold amount transaction for the next payout
                                        MerchantTransactionEnt merchantTransactionEnt = new MerchantTransactionEnt();
                                        merchantTransactionEnt.fleetId = fleetId;
                                        merchantTransactionEnt.merchantId = merchantId;
                                        merchantTransactionEnt.type = "holdAmount";
                                        merchantTransactionEnt.transactionId = sdfTransaction.format(payoutHistoryEnt.payoutDate);
                                        merchantTransactionEnt.pendingAmount = holdAmount;
                                        merchantTransactionEnt.currencyISO = currencyISO;
                                        merchantTransactionEnt.createdDate = payoutHistoryEnt.createdDate;
                                        long id = sqlDao.addMerchantTransaction(merchantTransactionEnt);
                                        logger.debug(requestId + " - merchantTransactionId for holdAmount = " + id);
                                    }
                                } else {
                                    logger.debug(requestId + " - payout merchantId " + merchantId + " is error: cannot update payout history");
                                }
                            } else {
                                logger.debug(requestId + " - payout merchantId " + merchantId + " is error: cannot update merchant transaction");
                            }
                        } else {
                            logger.debug(requestId + " - payout merchantId " + merchantId + " is error: negative payout amount");
                        }

                    } else {
                        logger.debug(requestId + " - payout merchantId " + merchantId + " is error: negative pending amount");
                    }
                } catch (Exception ex) {
                    StringWriter errors = new StringWriter();
                    ex.printStackTrace(new PrintWriter(errors));
                    logger.debug(requestId + " - payout for merchant " + merchantId + " exception: " + errors.toString());
                }
            }

            // in case payout for some specific merchants in the list
            // remove merchants were paid successfully out of cache
            Date date = KeysUtil.SDF_DMYHMSTZ.parse(payoutDate);
            String cacheKey = String.valueOf(date.getTime());
            String cachedData = redisDao.getTmp(requestId, cacheKey+"-listIds");
            List<String> cachedDriver = Arrays.asList(cachedData.split(","));
            List<String> newCache = new ArrayList<>();
            for (String driverId : cachedDriver) {
                 if (!arrSuccess.contains(driverId))
                     newCache.add(driverId);
            }
            redisDao.overrideTmpData(requestId, cacheKey+"-listIds", String.join(",", newCache));

            // clear cache
            if (!sessionId.isEmpty()) {
                redisDao.removeTmp(requestId, sessionId + "-payoutObj");
                redisDao.removeTmp(requestId, sessionId + "-basketId");
                redisDao.removeTmp(requestId, sessionId + "-paymentIds");
            }

            // send success payout to Report
            if (arrSuccessReport.size() > 0) {
                // separate to multiple threads
                int count = arrSuccessReport.size()/100;
                int rest = arrSuccessReport.size()%100;
                if (rest > 0) count += 1;

                for (int i = 0; i < count; i++) {
                    JSONArray arrReport = new JSONArray();
                    for (int j = 0; j < 100; j++) {
                        int index = i*100 + j;
                        if (index < arrSuccessReport.size()) {
                            arrReport.add(arrSuccessReport.get(index));
                        } else {
                            break;
                        }
                    }
                    QUpReportPublisher reportPublisher = QUpReportPublisher.getInstance();
                    reportPublisher.sendListItems(requestId, arrReport.toJSONString());
                }

                // send notify to Jupiter > CC
                JSONObject objData = new JSONObject();
                objData.put("fleetId", fleetId);
                objData.put("status", "success");
                objData.put("type", "merchant");
                objData.put("listId", arrSuccess);
                logger.debug(requestId + " - URL: " + PaymentUtil.payout_dnb);
                logger.debug(requestId + " - data: " + objData.toJSONString());
                String jupiterResponse = CommonUtils.sendJupiter(requestId, objData.toJSONString(), PaymentUtil.payout_dnb,
                        ServerConfig.dispatch_userName, ServerConfig.dispatch_password);
                logger.debug(requestId + " - jupiterResponse: " + jupiterResponse);
            }

            //convert payout date to fleet timezone before send mail
            String pd = KeysUtil.SDF_DMYHMS.format(TimezoneUtil.offsetTimeZone(KeysUtil.SDF_DMYHMSTZ.parse(payoutDate), "GMT", timezone));

            // send mail payout result
            try {
                int totalFailed = totalMerchant - arrSuccess.size();
                MailService mailService = MailService.getInstance();
                mailService.sendMailPayoutCompleted(requestId, "merchant", fleetId, fleetName , pd, arrSuccess.size(), totalFailed,
                        Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(totalPayoutAmount)), email.split(";"), jsonError);
            } catch(Exception e) {
                e.printStackTrace();
            }

            //save operator logs
            try {
                Currency currency = Currency.getInstance(currencyISO);
                DecimalFormat nf = (DecimalFormat) NumberFormat.getCurrencyInstance(Locale.US);
                nf.setCurrency(currency);
                Logs logs = new Logs();
                logs._id = new ObjectId();
                logs.fleetId = fleetId;
                logs.userId = operatorId;
                logs.fullName = operatorName;
                logs.userName = operatorUserName;
                logs.module = "Merchant payout";
                logs.action = "Pay";
                logs.description = "Payout to: " + listMerchant.size() + " merchant. " +
                        "Min payout: " + nf.format(minPayoutAmount) + ". " +
                        "Hold amount: " + nf.format(holdAmount) + ". " +
                        "Total payout: " + nf.format(Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(totalPayoutAmount))) + ". " +
                        "Payout end time: " + pd + ".";
                logs.companyId = companyId;
                logs.createdDate = new Date();
                logs.latestUpdate = new Date();
                mongoDao.addLogs(logs);

                String json = gson.toJson(logs);
                JSONObject jsonObject = (JSONObject) new JSONParser().parse(json);
                Calendar calendar = Calendar.getInstance();
                jsonObject.put("createdDate",
                        TimezoneUtil.formatISODate(TimezoneUtil.offsetTimeZone(logs.createdDate, calendar.getTimeZone().getID(), "GMT"), "GMT"));
                jsonObject.put("latestUpdate",
                        TimezoneUtil.formatISODate(TimezoneUtil.offsetTimeZone(logs.createdDate, calendar.getTimeZone().getID(), "GMT"), "GMT"));
                jsonObject.remove("_id");

                //send data logs to report module
                QUpReportPublisher reportPublisher = QUpReportPublisher.getInstance();
                reportPublisher.sendOneItem(requestId, logs._id.toString(), "Logs", jsonObject.toJSONString());
            }catch (Exception e){
                e.printStackTrace();
            }


        } catch(Exception ex) {
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            logger.debug(requestId + " - PayoutMerchantThread: "+ errors.toString());
        } finally {
            // remove lock payout after completed
            redisDao.unlockPayout(requestId, fleetId + "-merchant");
        }

    }

}
