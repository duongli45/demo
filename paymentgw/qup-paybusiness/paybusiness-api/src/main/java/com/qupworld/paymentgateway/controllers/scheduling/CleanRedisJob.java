package com.qupworld.paymentgateway.controllers.scheduling;

import com.qupworld.paymentgateway.models.RedisDao;
import com.qupworld.paymentgateway.models.RedisImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;

/**
 * Created by qup on 2/28/17.
 */
public class CleanRedisJob implements Job {

    final static Logger logger = LogManager.getLogger(CleanRedisJob.class);
    public void execute(JobExecutionContext context)
            throws JobExecutionException {
        String requestId = UUID.randomUUID().toString();
        logger.debug(requestId + "- query and clean ticket on Redis....");
        RedisDao redisDao = new RedisImpl();
        try {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -2);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String date = sdf.format(cal.getTime());

            // remove ticket
            redisDao.removeCompletedBook(requestId, date);

            // remove audit
            redisDao.removeAudit(requestId, date);

            for (int i = 0; i < 24; i++) {
                String dateWithHour = date + "-" + i;
                if (i < 10)
                    dateWithHour = date + "-0" + i;
                redisDao.removeAudit(requestId, dateWithHour);
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }

    }
}
