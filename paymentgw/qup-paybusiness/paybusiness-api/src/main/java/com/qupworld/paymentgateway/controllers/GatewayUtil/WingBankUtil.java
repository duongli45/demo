package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.Base64Encoder;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.SecurityUtil;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.KeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by thuanho on 27/06/2023.
 */
public class WingBankUtil {

    final static Logger logger = LogManager.getLogger(WingBankUtil.class);
    private String requestId = "";
    private String USER_ID;
    private String PASSWORD;
    private String API_KEY;
    private String REQUEST_URL;
    
    public WingBankUtil(String _requestId, String userId, String password, String apiKey, String requestURL) {
        this.requestId = _requestId;
        this.USER_ID = userId;
        this.PASSWORD = password;
        this.API_KEY = apiKey;
        this.REQUEST_URL = requestURL;
    }

    public String getCheckoutURL(double amount, String orderId, String fleetName, String currencyISO) {
        ObjResponse response = new ObjResponse();
        response.returnCode = 525;
        try {
            logger.debug(requestId + " - orderId: " + orderId);
            String authenToken = getAuthenToken();
            String topupURL = "/api/v4/generatedeeplink";

            String returnUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.WINGBANK + "?urlAction=return";
            String cancelUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.WINGBANK + "?urlAction=cancel";

            String amountStr = KeysUtil.DECIMAL_FORMAT0.format(amount);
            String schema_url = "payment://wingbank";

            JSONObject jsonPayload = new JSONObject();
            jsonPayload.put("order_reference_no", orderId);
            jsonPayload.put("amount", amountStr);
            jsonPayload.put("currency", currencyISO);
            jsonPayload.put("merchant_name", USER_ID);
            jsonPayload.put("merchant_id", fleetName);
            jsonPayload.put("item_name", "Top-up");
            jsonPayload.put("success_callback_url", returnUrl);
            jsonPayload.put("fail_callback_url", cancelUrl);
            jsonPayload.put("schema_url", schema_url);

            //SHA256(BASE64(AES256(BASE64(amount#currency#merchant_id#order_reference_no#schema_url))))
            String keyOrder = amountStr + "#" + currencyISO + "#" + fleetName + "#" + orderId + "#" + schema_url;
            logger.debug(requestId + " - keyOrder: " + keyOrder);
            String encodeBase4 =  Base64.getEncoder().encodeToString(keyOrder.getBytes());
            String token = authenToken.replace("-", "");
            logger.debug(requestId + " - token: " + token);
            String encryptAes256 = encryptSHA(encodeBase4, token);
            logger.debug(requestId + " - encryptAes256: " + encryptAes256);
            String encodedBase664 = Base64.getEncoder().encodeToString(encryptAes256.getBytes());
            String hash = SecurityUtil.hashSHA256(requestId, encodedBase664).toUpperCase();
            logger.debug(requestId + " - hash: " + hash);
            jsonPayload.put("txn_hash", hash);

            JSONObject jsonProduct = new JSONObject();
            jsonProduct.put("product_id", orderId);
            jsonProduct.put("product_type", "top-up");
            jsonProduct.put("product_qty", 1);
            jsonProduct.put("product_unit_price", amountStr);
            jsonProduct.put("currency", currencyISO);

            JSONArray arrProduct = new JSONArray();
            arrProduct.put(jsonProduct);
            jsonPayload.put("product_detail", arrProduct);

            String requestUrl = REQUEST_URL + topupURL;
            logger.debug(requestId + " - requestUrl: " + requestUrl);
            logger.debug(requestId + " - objBody: " + jsonPayload.toString());
            RequestBody body = RequestBody.create(jsonPayload.toString(), KeysUtil.JSON);
            Request request = new Request.Builder()
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", "Bearer " + authenToken)
                    .url(requestUrl)
                    .post(body)
                    .build();

            OkHttpClient client = new OkHttpClient.Builder().build();
            Response paymentResponse = client.newCall(request).execute();
            String responseString = paymentResponse.body().string();
            logger.debug(requestId + " - response = " + responseString);
            /*
            {
              "error_code": "200",
              "amount": "10.20",
              "currency": "USD",
              "error_text": "Success Pre Order",
              "pre_order_id": "ORDER000138684",
              "merchant_name": "partner.tta",
              "merchant_id": "TTA",
              "item_name": "Top-up",
              "redirect_url": "wingbankapp://payment?orderId=ORDER000138684&amount=10.20&currency=USD&merchant_id=00182&merchant_name=partner.tta&serviceType=BILLPAYON&ref_id=20230626072835359"
            }
            */
            JSONObject jsonResponse = new JSONObject(responseString);
            String error_code = jsonResponse.has("error_code") ? jsonResponse.getString("error_code") : "";
            if (error_code.equals("200")) {
                /*String checkoutURL = apiURL + "&txnToken="+txnToken;
                checkoutURL = checkoutURL.replace("initiateTransaction", "showPaymentPage");*/
                String checkoutURL = jsonResponse.has("redirect_url") ? jsonResponse.getString("redirect_url") : "";
                if (!checkoutURL.isEmpty()) {
                    response.returnCode = 200;
                    Map<String, Object> mapResponse = new HashMap<>();
                    mapResponse.put("3ds_url", checkoutURL);
                    mapResponse.put("type", "link");
                    mapResponse.put("transactionId", orderId);
                    mapResponse.put("isDeeplink", true);
                    response.response = mapResponse;
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }

        return response.toString();
    }

    public String getPaymentStatus(String orderId) {
        ObjResponse response = new ObjResponse();
        response.returnCode = 525;
        try {
            logger.debug(requestId + " - orderId: " + orderId);
            String authenToken = getAuthenToken();
            String statusURL = "/api/v1/checktxnstatus";

            JSONObject jsonPayload = new JSONObject();
            jsonPayload.put("order_reference_no", orderId);
            jsonPayload.put("merchant_name", USER_ID);

            String requestUrl = REQUEST_URL + statusURL;
            logger.debug(requestId + " - requestUrl: " + requestUrl);
            logger.debug(requestId + " - objBody: " + jsonPayload.toString());
            RequestBody body = RequestBody.create(jsonPayload.toString(), KeysUtil.JSON);
            Request request = new Request.Builder()
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", "Bearer " + authenToken)
                    .url(requestUrl)
                    .post(body)
                    .build();

            OkHttpClient client = new OkHttpClient.Builder().build();
            Response paymentResponse = client.newCall(request).execute();
            String responseString = paymentResponse.body().string();
            logger.debug(requestId + " - response = " + responseString);
            /*
            {
              "error_code": "200",
              "amount": "1",
              "currency": "USD",
              "status": "TS",
              "error_text": "Success",
              "pre_order_id": "ORDER000127821",
              "ref_number": "cc3ad5ca50c4418fbb42",
              "partner_id": "prefund.tadatopup",
              "transaction_id": "BRM117721"
            }
            */
            JSONObject jsonResponse = new JSONObject(responseString);
            String resultStatus = jsonResponse.has("error_text") ? jsonResponse.getString("error_text") : ""; //Success
            logger.debug(requestId + " - resultStatus: " + resultStatus);
            Map<String,String> mapResponse = new HashMap<>();
            mapResponse.put("status", resultStatus.toUpperCase());
            response.response = mapResponse;
            response.returnCode = 200;
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
        }

        return response.toString();
    }

    public String decryptWithKey(String originalText, String strKey)  {
        String data = "";
        try {
            final MessageDigest md = MessageDigest.getInstance("SHA-256");
            final byte[] digestOfPassword = md.digest(strKey.getBytes("utf-8"));
            final SecretKey key = new SecretKeySpec(digestOfPassword, "AES");
            final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(new byte[16]));
            final byte[] plainTextBytes = Base64Encoder.decode(originalText);
            final byte[] encodeTextBytes = cipher.doFinal(plainTextBytes);
            data = new String(encodeTextBytes);
            /*
            {
              "callback_url": "https://eventapis-local.qup.vn/api/webhook/notification/WingBank",
              "transaction_id": "OOE226460",
              "transaction_date": "179-06-23 11:51:55",
              "amount": "15.0",
              "total_amount": " 61,500",
              "order_reference_no": "PW20230628045132693232",
              "fee": "USD 0.00",
              "rest_api_key": "6d3f7e7ef9b24f4bcce78e7519160ce20c53cbd0f1da35dec78373c2eb8322f1",
              "currency": "USD",
              "account": "00400232"
            }
             */
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException |
                IllegalBlockSizeException |
                InvalidKeyException | BadPaddingException | NoSuchPaddingException |
                InvalidAlgorithmParameterException ex) {
            logger.debug(requestId + " - Error while decryptWithKey: " + CommonUtils.getError(ex));
        }
        return data;
    }

    public String doPayout(String account, double amount) {
        ObjResponse response = new ObjResponse();
        try {
            logger.debug(requestId + " - account: " + account);
            logger.debug(requestId + " - amount: " + amount);
            String authenToken = getAuthenToken();
            String payoutURL = "/api/v1/sendmoney/wing";

            JSONObject objBody = new JSONObject();

            objBody.put("amount", amount);
            objBody.put("receiver_account", account);
            objBody.put("ext_transaction_id", GatewayUtil.getOrderId());

            String requestUrl = REQUEST_URL + payoutURL;
            logger.debug(requestId + " - requestUrl: " + requestUrl);
            logger.debug(requestId + " - objBody: " + objBody.toString());
            RequestBody body = RequestBody.create(objBody.toString(), KeysUtil.JSON);
            Request request = new Request.Builder()
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", "Bearer " + authenToken)
                    .url(requestUrl)
                    .post(body)
                    .build();

            OkHttpClient client = new OkHttpClient.Builder().build();
            Response payoutResponse = client.newCall(request).execute();
            String responseString = payoutResponse.body().string();
            logger.debug(requestId + " - response = " + responseString);
            /*
            {
              "currency": "USD",
              "amount": "USD 10.00",
              "fee": "USD 0.05",
              "total": "USD 10.05",
              "exchange_rate": "KHR 40,870",
              "transaction_id": "0002393243390001"
            }
            {
              "error_code": "E0204",
              "message": "ERROR: Not enough wallet balance to do transactions."
            }
             */
            JSONObject jsonData = new JSONObject(responseString);
            String transaction_id = jsonData.has("transaction_id") ? jsonData.getString("transaction_id") : "";
            String message = jsonData.has("message") ? jsonData.getString("message") : "";
            Map<String, String> mapResponse = new HashMap<>();
            if (!transaction_id.isEmpty()) {
                mapResponse.put("transId", transaction_id);
                response.response = mapResponse;
                response.returnCode = 200;
            } else {
                mapResponse.put("message", message);
                response.response = mapResponse;
                response.returnCode = 525;
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - doPayout exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
        }
        return response.toString();
    }

    private String getAuthenToken() throws Exception {
        String authenURL = "/oauth/token";

        ArrayList<NameValuePair> postParameters = new ArrayList<>();
        postParameters.add(new BasicNameValuePair("username", USER_ID));
        postParameters.add(new BasicNameValuePair("password", PASSWORD));
        postParameters.add(new BasicNameValuePair("client_id", "third_party"));
        postParameters.add(new BasicNameValuePair("client_secret", "16681c9ff419d8ecc7cfe479eb02a7a"));
        postParameters.add(new BasicNameValuePair("grant_type", "password"));
        HttpClient client = HttpClientBuilder.create().build();
        String requestUrl = REQUEST_URL + authenURL;
        logger.debug(requestId + " - requestUrl: " + requestUrl);
        HttpPost httpRequest = new HttpPost(requestUrl);
        httpRequest.addHeader("Content-Type", "application/x-www-form-urlencoded");
        httpRequest.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));

        HttpResponse httpResponse = client.execute(httpRequest);
        String response = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
        logger.debug(requestId + " - response: " + response);
        JSONObject json = new JSONObject(response);
        return json.has("access_token") ? json.getString("access_token") : "";
    }

    private String encryptSHA(String strToEncrypt, String token) {
        try {
            String SALT = "WINGBANK";
            byte[] iv = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            IvParameterSpec ivspec = new IvParameterSpec(iv);

            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            KeySpec spec = new PBEKeySpec(token.toCharArray(), SALT.getBytes(), 65536, 256);
            SecretKey key = factory.generateSecret(spec);

            SecretKeySpec secretKey = new SecretKeySpec(key.getEncoded(), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivspec);
            return Base64.getEncoder()
                    .encodeToString(cipher.doFinal(strToEncrypt.getBytes(StandardCharsets.UTF_8)));


        } catch (Exception ex) {
            logger.debug(requestId + " - Error while encrypting: " + CommonUtils.getError(ex));
        }
        return "";
    }
}
