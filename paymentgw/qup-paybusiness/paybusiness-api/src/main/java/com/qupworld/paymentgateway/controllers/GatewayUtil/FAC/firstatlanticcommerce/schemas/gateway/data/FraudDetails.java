
package com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FraudDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FraudDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AVSResponseCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AuthResponseCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CVVResponseCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SessionId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FraudDetails", propOrder = {
    "avsResponseCode",
    "authResponseCode",
    "cvvResponseCode",
    "sessionId"
})
public class FraudDetails {

    @XmlElement(name = "AVSResponseCode", required = true, nillable = true)
    protected String avsResponseCode;
    @XmlElement(name = "AuthResponseCode", required = true, nillable = true)
    protected String authResponseCode;
    @XmlElement(name = "CVVResponseCode", required = true, nillable = true)
    protected String cvvResponseCode;
    @XmlElement(name = "SessionId", required = true, nillable = true)
    protected String sessionId;

    /**
     * Gets the value of the avsResponseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAVSResponseCode() {
        return avsResponseCode;
    }

    /**
     * Sets the value of the avsResponseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAVSResponseCode(String value) {
        this.avsResponseCode = value;
    }

    /**
     * Gets the value of the authResponseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthResponseCode() {
        return authResponseCode;
    }

    /**
     * Sets the value of the authResponseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthResponseCode(String value) {
        this.authResponseCode = value;
    }

    /**
     * Gets the value of the cvvResponseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCVVResponseCode() {
        return cvvResponseCode;
    }

    /**
     * Sets the value of the cvvResponseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCVVResponseCode(String value) {
        this.cvvResponseCode = value;
    }

    /**
     * Gets the value of the sessionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Sets the value of the sessionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionId(String value) {
        this.sessionId = value;
    }

}
