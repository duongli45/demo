package com.qupworld.paymentgateway.controllers.threading;

import com.qupworld.paymentgateway.controllers.UpdateDriverCreditBalance;
import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.SQLDao;
import com.pg.util.CommonArrayUtils;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.TimezoneUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class RemoveExpiredBonusThread extends Thread {

    final static Logger logger = LogManager.getLogger(RemoveExpiredBonusThread.class);
    public String fleetId;
    public String currencyISO;
    public String driverId;
    public double amount;
    public List<String> listDriverTypes;
    public String reason;
    public String topUpDate;
    public MongoDao mongoDao;
    public SQLDao sqlDao;
    public String requestId;

    @Override
    public void run() {
        try {
            if (!driverId.isEmpty()) {
                doUpdate(driverId);

            } else {
                int i = 0;
                int size = 100;
                boolean valid = true;
                while (valid) {
                    List<String> listDriver = mongoDao.getActiveDriversByType(requestId, fleetId, listDriverTypes, i * size, size);
                    int querySize = listDriver.size();
                    for (String userId : listDriver) {
                        doUpdate(userId);
                    }

                    if (querySize < size) {
                        valid = false;
                    }
                    i++;
                }
            }

        } catch(Exception ex) {
            logger.debug(requestId + " - RemoveExpiredBonusThread exception: " + CommonUtils.getError(ex));
        }

    }

    private void doUpdate(String userId) {
        try {
            Date topUpDateParse = KeysUtil.SDF_DMYHMS.parse(topUpDate);
            Date convertDate = TimezoneUtil.offsetTimeZone(topUpDateParse, Calendar.getInstance().getTimeZone().getID(), "GMT");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime dateTime = LocalDateTime.parse(KeysUtil.SDF_DMYHMS.format(convertDate), formatter);
            if (sqlDao.isToppedUpBonus(userId, amount, currencyISO, Timestamp.valueOf(dateTime))) {
                logger.debug(requestId + " - doUpdate for driver " + userId);
                List<Double> usage = sqlDao.getUsageWalletBalance(userId, currencyISO, Timestamp.valueOf(dateTime));

                double cashIn = Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(usage.get(0)));
                double cashOut = Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(usage.get(1)));
                double currentBalance = Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(usage.get(2)));
                logger.debug(requestId + " - cashIn (topUpMOLPay, cashWallet, editBalance): " + cashIn);
                logger.debug(requestId + " - cashOut (bookingDeduction, editBalance): " + cashOut);
                logger.debug(requestId + " - currentBalance: " + currentBalance);
                // ignore driver has cash out >= topped up amount
                if (cashOut < amount) {
                    double remainAmount = amount - cashOut;
                    logger.debug(requestId + " - remainAmount: " + remainAmount);
                    // remove remain amount from current balance
                    updateBalance(userId, -remainAmount);
                }
            } else {
                logger.debug(requestId + " - " + userId + " did not topped-up "+ amount+". Ignore!");
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - doUpdate exception: " + CommonUtils.getError(ex));
        }
    }

    private void updateBalance(String driverId, double amount) {
        UpdateDriverCreditBalance updateBalance = new UpdateDriverCreditBalance();
        updateBalance.type = CommonArrayUtils.DRIVER_WALLET_TYPE[2];
        updateBalance.fleetId = fleetId;
        updateBalance.driverId = driverId;
        updateBalance.amount = amount;
        updateBalance.currencyISO = currencyISO;
        updateBalance.mongoDao = mongoDao;
        updateBalance.sqlDao = sqlDao;
        updateBalance.reason = reason;
        updateBalance.additionalData.put("operatorId", "");
        updateBalance.additionalData.put("operatorName", "");
        updateBalance.requestId = requestId;
        String result = updateBalance.doUpdate();
        logger.debug(requestId + " - updateBalance for driver "+ driverId +" result = " + result);
    }

}
