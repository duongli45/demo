package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.google.gson.Gson;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.controllers.GatewayUtil.YeePay.CallbackUtils;
import com.qupworld.paymentgateway.controllers.GatewayUtil.YeePay.DigestUtilFacade;
import com.qupworld.paymentgateway.entities.CreditEnt;
import com.qupworld.service.MailService;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.TimezoneUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class YeepayUtil {

    private final static Logger logger = LogManager.getLogger(YeepayUtil.class);
    private final static DecimalFormat DF = new DecimalFormat("0.00");
    private SimpleDateFormat formatOrder = new SimpleDateFormat("yyyyMMddHHmmssSSS");
    private final String [] HmacOrder =
        {
            // EposSale
            "p0_Cmd,p1_MerId,p2_Order,p3_Amt,p4_Cur,p5_Pid,p8_Url,pa_CredType,pb_CredCode,pe_BuyerTel,pf_BuyerName,pt_ActId,pa2_ExpireYear,pa3_ExpireMonth,pa4_CVV,prisk_TerminalCode,prisk_Param" ,
            "r0_Cmd,r1_Code,r2_TrxId,r6_Order,ro_BankOrderId",
            // EposVerifySale
            "p0_Cmd,p1_MerId,p2_Order,pb_VerifyCode",
            "r0_Cmd,r1_Code,r2_TrxId,r6_Order,ro_BankOrderId",
            // EposBindList
            "p0_Cmd,p1_MerId,pe_Identityid,pe_Identitytype",
            "r0_Cmd,r1_Code,re_Identityid,re_Identityty,re_BindList",
            // EposBindPayNew
            "p0_Cmd,p1_MerId,p2_Order,p3_Amt,p4_Cur,p5_Pid,p8_Url,pe_Identityid,pe_Identitytype,pe_BindId,prisk_TerminalCode,prisk_Param",
            "r0_Cmd,r1_Code,r2_TrxId,r6_Order,ro_BankOrderId,rp_NeedFactor",
            // EposFactorAndVerifyCode
            "p0_Cmd,p1_MerId,p2_Order,pa_CredType,pb_CredCode,pt_ActId,pe_BuyerTel,pf_BuyerName,pa2_ExpireYear,pa3_ExpireMonth,pa4_CVV",
            "r0_Cmd,r1_Code,r2_TrxId,r6_Order,ro_BankOrderId",
            // RefundOrd
            "p0_Cmd,p1_MerId,p2_Order,pb_TrxId,p3_Amt,p4_Cur,p5_Desc",
            "r0_Cmd,r1_Code,r2_TrxId,r3_Amt,r4_Cur"
        };

    private Gson gson = new Gson();

    public String submitSaleToGetVerifyCode(String requestId, String type, String bookId, double amount, String merchantId, String hmacKey, String terminalId, String requestUrl, String currencyISO, String customerPhone,
                                            String creditType, String creditCode, String cardNumber, String cardHolder, String cvv, String expiredDate) throws IOException {
        ObjResponse response = new ObjResponse();
        try {
            String p8_Url = "localhost";
            String pd_FrpId = "";
            String[] expired = expiredDate.split("/");
            String prisk_Param = "";

            String orderId = type.equals("payInput") ? bookId : getOrderId();
            String pid = type.equals("addToken") ? "registration-" + customerPhone : "taxi";

            Map<String,String> hm=new HashMap<>();
            hm.put("p0_Cmd", "EposSale");
            hm.put("p2_Order", orderId);
            hm.put("p3_Amt", DF.format(amount));
            hm.put("p4_Cur", currencyISO);
            hm.put("p5_Pid", pid);
            hm.put("p8_Url", p8_Url);
            hm.put("pa_CredType", creditType.toUpperCase());
            hm.put("pb_CredCode", creditCode);
            hm.put("pd_FrpId", pd_FrpId);
            hm.put("pe_BuyerTel", customerPhone);
            hm.put("pf_BuyerName", cardHolder);
            hm.put("pt_ActId", cardNumber);
            hm.put("pa2_ExpireYear", expired[1]);
            hm.put("pa3_ExpireMonth", expired[0]);
            hm.put("pa4_CVV", cvv);
            hm.put("prisk_TerminalCode", terminalId);
            hm.put("prisk_Param", prisk_Param);
            hm.put("pr_NeedResponse", "1");
            hm.put("pe_IsBind", "1");
            hm.put("pe_Identitytype", "4"); // 4 is user's phone number
            hm.put("pe_Identityid", customerPhone);

            String log = gson.toJson(hm);
            log = log.replace(cardNumber, "XXXXXXXXXXXX" + cardNumber.substring(cardNumber.length() - 4));
            logger.debug(requestId + " - params: " + log);

            Map<String,String> result = trxRequest(requestId, hm, "POST", "gbk", merchantId, hmacKey, requestUrl);
            int r1_Code = Integer.parseInt(result.get("r1_Code"));
            if (r1_Code == 1) {
                response.returnCode = 200;
                response.response = result;
            } else if (r1_Code == 81100 || r1_Code == 81201 || r1_Code == 81203) {
                response.returnCode = 457; // SMS code is required
                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("orderId", orderId);
                response.response = mapResponse;
            } else {
                String request = gson.toJson(hm);
                request = request.replace(cardNumber, "XXXXXXXXXXXX" + cardNumber.substring(cardNumber.length() - 4));
                GatewayUtil.sendMail(requestId, KeysUtil.YEEPAY, request, gson.toJson(result), "phone " + customerPhone);

                response.returnCode = getErrorCode(r1_Code);
                String message = getErrorMessage(r1_Code);
                if (!message.isEmpty()) {
                    result.put("message", message);
                }
                response.response = result;
            }
            return response.toString();
        } catch(Exception ex) {
            GatewayUtil.sendMail(requestId, KeysUtil.YEEPAY, ex, "phone " + customerPhone);

            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }
    }

    public String doPaymentWithCardInfo(String requestId, String bookId, double amount, String merchantId, String hmacKey, String terminalId, String requestUrl, String currencyISO, String customerPhone,
                                            String creditType, String creditCode, String cardNumber, String cardHolder, String cvv, String expiredDate) throws IOException {
        ObjResponse response = new ObjResponse();
        try {
            String p8_Url = "localhost";
            String pd_FrpId = "";
            String[] expired = expiredDate.split("/");
            String prisk_Param = "";

            Map<String,String> hm=new HashMap<>();
            hm.put("p0_Cmd", "EposSale");
            hm.put("p2_Order", bookId);
            hm.put("p3_Amt", DF.format(amount));
            hm.put("p4_Cur", currencyISO);
            hm.put("p5_Pid", "taxi");
            hm.put("p8_Url", p8_Url);
            hm.put("pa_CredType", creditType.toUpperCase());
            hm.put("pb_CredCode", creditCode);
            hm.put("pd_FrpId", pd_FrpId);
            hm.put("pe_BuyerTel", customerPhone);
            hm.put("pf_BuyerName", cardHolder);
            hm.put("pt_ActId", cardNumber);
            hm.put("pa2_ExpireYear", expired[1]);
            hm.put("pa3_ExpireMonth", expired[0]);
            hm.put("pa4_CVV", cvv);
            hm.put("prisk_TerminalCode", terminalId);
            hm.put("prisk_Param", prisk_Param);
            hm.put("pr_NeedResponse", "1");
            hm.put("pe_IsBind", "1");

            Map<String,String> result = trxRequest(requestId, hm, "POST", "gbk", merchantId, hmacKey, requestUrl);
            int r1_Code = Integer.parseInt(result.get("r1_Code"));
            if (r1_Code == 1) {
                response.returnCode = 200;
            } else if (r1_Code == 81100 || r1_Code == 81201 || r1_Code == 81203) {
                response.returnCode = 457; // SMS code is required
            } else {
                String request = gson.toJson(hm);
                request = request.replace(cardNumber, "XXXXXXXXXXXX" + cardNumber.substring(cardNumber.length() - 4));
                GatewayUtil.sendMail(requestId, KeysUtil.YEEPAY, request, gson.toJson(result), "phone " + customerPhone);

                response.returnCode = getErrorCode(r1_Code);
                String message = getErrorMessage(r1_Code);
                if (!message.isEmpty()) {
                    result.put("message", message);
                }
            }
            response.response = result;
            return response.toString();
        } catch(Exception ex) {
            GatewayUtil.sendMail(requestId, KeysUtil.YEEPAY, ex, "phone " + customerPhone);

            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }
    }

    public ObjResponse verifyTransaction(String logKey, String requestId, String merchantId, String hmacKey, String requestUrl, String verifyCode, String orderId) throws IOException {
        ObjResponse response = new ObjResponse();
        try {
            Map<String,String> hm=new HashMap<>();
            hm.put("p0_Cmd", "EposVerifySale");
            hm.put("p2_Order", orderId);
            hm.put("pb_VerifyCode", verifyCode);

            Map<String,String> result = trxRequest(requestId, hm, "POST", "gbk", merchantId, hmacKey, requestUrl);
            int r1_Code = Integer.parseInt(result.get("r1_Code"));
            if (r1_Code == 1) {
                response.returnCode = 200;
            } else {
                GatewayUtil.sendMail(requestId, KeysUtil.YEEPAY, gson.toJson(hm), gson.toJson(result), logKey);
                response.returnCode = getErrorCode(r1_Code);
                String message = getErrorMessage(r1_Code);
                if (!message.isEmpty()) {
                    result.put("message", message);
                }
            }
            response.response = result;
            return response;
        } catch(Exception ex) {
            GatewayUtil.sendMail(requestId, KeysUtil.YEEPAY, ex, logKey);

            ex.printStackTrace();
            response.returnCode = 437;
            return response;
        }
    }

    public String createCreditTokenWithSMS(CreditEnt creditForm, String merchantId, String hmacKey, String customerPhone, String requestUrl,
                                           String r2_TrxId, String currencyISO) throws IOException {
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            String cardMasked = "XXXXXXXXXXXX" + creditForm.cardNumber.substring(creditForm.cardNumber.length() - 4);
            // get token from Yeepay
            String token;
            ObjResponse bindResponse = getBindId(creditForm.requestId, creditForm.cardNumber, merchantId, hmacKey, customerPhone, requestUrl);
            if (bindResponse.returnCode == 200) {
                token = bindResponse.response.toString();
            } else {
                return bindResponse.toString();
            }
            Map<String,String> mapResponse = new HashMap<>();
            mapResponse.put("message", "");
            mapResponse.put("creditCard", cardMasked);
            mapResponse.put("qrCode", "");
            mapResponse.put("token", token);
            mapResponse.put("cardType", KeysUtil.UNIONPAY);

            createTokenResponse.returnCode = 200;
            createTokenResponse.response = mapResponse;
            try {
                if (!token.isEmpty()) {
                    voidTransaction(creditForm.requestId, r2_TrxId, 1.00,  merchantId, hmacKey, requestUrl, currencyISO);
                }
            } catch(Exception ex) {
                ex.printStackTrace();
            }
            return createTokenResponse.toString();
        } catch(Exception ex) {
            GatewayUtil.sendMail(creditForm.requestId, KeysUtil.YEEPAY, ex, "phone " + customerPhone);

            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }
    }

    public String createPaymentWithCreditToken(String requestId, String bookId, CreditEnt creditUser, double total, String merchantId, String hmacKey, String terminalId, String requestUrl, String currencyISO) {
        ObjResponse payTokenResponse = new ObjResponse();
        try {
            if (creditUser != null) {
                logger.debug(requestId + " - creditUser: " + creditUser.toString());
                String customerPhone = creditUser.phone != null ? creditUser.phone : "";
                if (customerPhone.length() > 11) {
                    customerPhone = customerPhone.substring(customerPhone.length() - 11);
                }
                // try to get bindId and pay again
                ObjResponse bindResponse = getBindId(requestId, creditUser.cardNumber, merchantId, hmacKey, customerPhone, requestUrl);
                if (bindResponse.returnCode == 200) {
                    String bindId = bindResponse.response.toString();
                    ObjResponse payBind = eposBindPay(requestId, bookId, total, merchantId, hmacKey, terminalId, customerPhone, bindId, requestUrl, currencyISO);
                    if (payBind.returnCode == 200) {
                        return payBind.toString();
                    } else if (payBind.returnCode == 82110 || payBind.returnCode == 82111 || payBind.returnCode == 82112) {
                        // call EposFactorAndVerifyCode
                        String orderId = payBind.response.toString();
                        ObjResponse factorAndVerify = eposFactorAndVerifyCode(bookId, requestId, creditUser, orderId, merchantId, hmacKey, customerPhone, requestUrl);
                        if (factorAndVerify.returnCode == 200) {
                            // call EposVerifySale again with empty pb_VerifyCode
                            ObjResponse verifyEmptyCode = verifyTransaction("bookId " + bookId, requestId, merchantId, hmacKey, requestUrl, "", orderId);
                            if (verifyEmptyCode.returnCode == 200) {
                                return factorAndVerify.toString(); // return this because it includes transId from Yeepay
                            } else {
                                return verifyEmptyCode.toString();
                            }
                        } else {
                            return factorAndVerify.toString();
                        }
                    } else {
                        return payBind.toString();
                    }
                } else {
                    return bindResponse.toString();
                }
            } else {
                payTokenResponse.returnCode = 437;
                payTokenResponse.response = "cannot get card data";
                return payTokenResponse.toString();
            }
        } catch(Exception ex) {
            GatewayUtil.sendMail(requestId, KeysUtil.YEEPAY, ex, "bookId " + bookId);

            ex.printStackTrace();
            payTokenResponse.returnCode = 437;
            return payTokenResponse.toString();
        }
    }
    private ObjResponse getBindId(String requestId, String cardNumber, String merchantId, String hmacKey, String phone, String requestUrl) throws Exception{
        ObjResponse response = new ObjResponse();
        try {
            Map<String,String> hm=new HashMap<>();
            hm.put("p0_Cmd", "EposBindList");
            hm.put("pe_Identityid", phone);
            hm.put("pe_Identitytype", "4"); // type = 4 is using phone number
            String last4 = cardNumber.substring(cardNumber.length() - 4);
            Map<String,String> result = trxRequest(requestId, hm, "POST", "gbk", merchantId, hmacKey, requestUrl);
            int r1_Code = Integer.parseInt(result.get("r1_Code"));
            if (r1_Code == 1) {
                JSONArray jsonMainArr = new JSONArray(result.get("re_BindList"));
                for (int i = 0; i < jsonMainArr.length(); i++) {
                    JSONObject obj = (JSONObject)jsonMainArr.get(i);
                    String cardMasked = obj.getString("cardNo") != null ? obj.getString("cardNo") : "";
                    if (!cardMasked.isEmpty()) {
                        String masked4 = cardMasked.substring(cardMasked.length() - 4);
                        if (masked4.equals(last4)) {
                            response.returnCode = 200;
                            response.response = obj.getString("bindId");
                        }
                    }
                }
            } else {
                String request = gson.toJson(hm);
                request = request.replace(cardNumber, "XXXXXXXXXXXX" + cardNumber.substring(cardNumber.length() - 4));
                GatewayUtil.sendMail(requestId, KeysUtil.YEEPAY, request, gson.toJson(result), "phone " + phone);

                response.returnCode = getErrorCode(r1_Code);
                String message = getErrorMessage(r1_Code);
                if (!message.isEmpty()) {
                    result.put("message", message);
                }
                response.response = result;
            }
        } catch(Exception ex) {
            GatewayUtil.sendMail(requestId, KeysUtil.YEEPAY, ex, "phone " + phone);
            ex.printStackTrace();
            response.returnCode = 437;
        }
        return response;

    }

    private ObjResponse eposBindPay(String requestId, String bookId, double total, String merchantId, String hmacKey, String terminalId, String customerPhone, String token, String requestUrl, String currencyISO) throws Exception{
        ObjResponse response = new ObjResponse();
        try {
            String p5_Pid="taxi";
            String p8_Url="localhost";
            String prisk_Param = "";

            Map<String,String> hm = new HashMap<>();
            hm.put("p0_Cmd", "EposBindPayNew");
            hm.put("p2_Order", bookId);
            hm.put("p3_Amt", DF.format(total));
            hm.put("p4_Cur", currencyISO);
            hm.put("p5_Pid", p5_Pid);
            hm.put("p8_Url", p8_Url);
            hm.put("pe_Identityid", customerPhone);
            hm.put("pe_Identitytype", "4");// type = 4 is using phone number
            hm.put("pe_BindId", token);
            hm.put("prisk_TerminalCode", terminalId);
            hm.put("prisk_Param", prisk_Param);
            hm.put("pr_NeedResponse","1");

            Map<String,String> result = trxRequest(requestId, hm, "POST", "gbk", merchantId, hmacKey, requestUrl);
            int r1_Code = Integer.parseInt(result.get("r1_Code"));
            if (r1_Code == 1) {
                Map<String, String> mapResponse = new HashMap<String, String>();
                mapResponse.put("message", "Success!");
                mapResponse.put("transId", result.get("r2_TrxId"));
                mapResponse.put("cardType", "");
                mapResponse.put("authCode", "");
                mapResponse.put("cardMask", "");

                response.response = mapResponse;
                response.returnCode = 200;
            } else if (r1_Code == 82110 || r1_Code == 82111 || r1_Code == 82112){
                response.returnCode = r1_Code;
                response.response = bookId;
            } else {
                GatewayUtil.sendMail(requestId, KeysUtil.YEEPAY, gson.toJson(hm), gson.toJson(result), "bookId " + bookId);

                response.returnCode = getErrorCode(r1_Code);
                String message = getErrorMessage(r1_Code);
                if (!message.isEmpty()) {
                    result.put("message", message);
                }
                response.response = result;
            }
            return response;
        } catch(Exception ex) {
            GatewayUtil.sendMail(requestId, KeysUtil.YEEPAY, ex, "bookId " + bookId);
            ex.printStackTrace();
            response.returnCode = 437;
            return response;
        }

    }

    private ObjResponse eposFactorAndVerifyCode(String bookId, String requestId, CreditEnt creditUser, String orderId, String merchantId, String hmacKey, String phone, String requestUrl) throws Exception{
        ObjResponse response = new ObjResponse();
        try {
            String[] expired = creditUser.expiredDate.split("/");

            Map<String,String> hm = new HashMap<>();
            hm.put("p0_Cmd", "EposFactorAndVerifyCode");
            hm.put("p2_Order", orderId);
            hm.put("pa_CredType", creditUser.creditType.toUpperCase());
            hm.put("pb_CredCode", creditUser.creditCode);
            hm.put("pt_ActId", creditUser.cardNumber);
            hm.put("pe_BuyerTel", phone);
            hm.put("pf_BuyerName", creditUser.cardHolder);
            hm.put("pa2_ExpireYear", expired[1]);
            hm.put("pa3_ExpireMonth", expired[0]);
            hm.put("pa4_CVV", creditUser.cvv);

            String log = gson.toJson(hm);
            log = log.replace(creditUser.cardNumber, "XXXXXXXXXXXX" + creditUser.cardNumber.substring(creditUser.cardNumber.length() - 4));
            logger.debug(requestId + " - params: " + log);

            Map<String,String> result = trxRequest(requestId, hm, "POST", "gbk", merchantId, hmacKey, requestUrl);
            int r1_Code = Integer.parseInt(result.get("r1_Code"));
            if (r1_Code == 1) {
                Map<String, String> mapResponse = new HashMap<String, String>();
                mapResponse.put("message", "Success!");
                mapResponse.put("transId", result.get("r2_TrxId"));
                mapResponse.put("cardType", "");
                mapResponse.put("authCode", "");
                mapResponse.put("cardMask", "");

                response.response = mapResponse;
                response.returnCode = 200;
            } else {
                String request = gson.toJson(hm);
                request = request.replace(creditUser.cardNumber, "XXXXXXXXXXXX" + creditUser.cardNumber.substring(creditUser.cardNumber.length() - 4));
                GatewayUtil.sendMail(requestId, KeysUtil.YEEPAY, request, gson.toJson(result), "bookId " + bookId);

                response.returnCode = getErrorCode(r1_Code);
                String message = getErrorMessage(r1_Code);
                if (!message.isEmpty()) {
                    result.put("message", message);
                }
                response.response = result;
            }
            return response;
        } catch(Exception ex) {
            GatewayUtil.sendMail(requestId, KeysUtil.YEEPAY, ex, "bookId " + bookId);

            ex.printStackTrace();
            response.returnCode = 437;
            return response;
        }
    }

    public static void main(String[] args) throws Exception{
        /*String r2_TrxId = "218695609512192J";
        double amount = 1293.20;
        String merchantId = "10015038799";
        String terminalId = "";
        String hmackey = "nY309X171V5YoFC4zr6q1tSxfqercQM3PM9p00941Ccc67kH9IN1co62fg32";
        boolean isSandbox = false;
        String currencyISO = "CNY";

        YeepayUtil yeepayUtil = new YeepayUtil();
        yeepayUtil.voidTransaction("", r2_TrxId, amount, merchantId, hmackey, terminalId, isSandbox, currencyISO);*/
    }

    public void voidTransaction(String requestId, String r2_TrxId, double amount,  String merchantId, String hmacKey, String requestUrl, String currencyISO){
        logger.debug(requestId + " - voidTransaction " + r2_TrxId + " ..." );
        try {
            Map<String,String> hm = new HashMap<>();
            hm.put("p0_Cmd", "RefundOrd");
            hm.put("p2_Order", getOrderId());
            hm.put("pb_TrxId", r2_TrxId);
            hm.put("p3_Amt", DF.format(amount));
            hm.put("p4_Cur", currencyISO);
            hm.put("p5_Desc", "Refund completed transaction");

            Map<String,String> result = trxRequest(requestId, hm, "POST", "gbk", merchantId, hmacKey, requestUrl);
            int r1_Code = Integer.parseInt(result.get("r1_Code"));
            if (r1_Code == 1) {

                logger.debug(requestId + " - chargeId: " + r2_TrxId + " has been refunded !!!! RefundId: " + result.get("r2_TrxId"));

            } else {
                logger.debug(requestId + " - RefundYeepayTransaction ERROR: " + result);
            }
        } catch(Exception ex) {
            GatewayUtil.sendMail(r2_TrxId, KeysUtil.YEEPAY, ex, "r2_TrxId " + r2_TrxId);

            ex.printStackTrace();
            logger.debug(requestId + " - RefundYeepayTransaction ERROR: " + getError(ex));
        }
    }

    private Map<String,String> trxRequest(String requestId, Map<String, String> params, String method, String charSet,
                                          String merchantId, String hmacKey, String requestUrl) throws Exception{

        String cmd = params.get("p0_Cmd");
        if(isEmpty(cmd)) throw new Exception("p0_Cmd is null");

        params.putIfAbsent("p1_MerId", merchantId);

        // get Hmac Order
        String requestHmacOrder = "";
        String responseHmacOrder = "";
        switch (cmd) {
            case "EposSale": {
                requestHmacOrder = HmacOrder[0];
                responseHmacOrder = HmacOrder[1];
            }
            break;
            case "EposVerifySale": {
                requestHmacOrder = HmacOrder[2];
                responseHmacOrder = HmacOrder[3];
            }
            break;
            case "EposBindList": {
                requestHmacOrder = HmacOrder[4];
                responseHmacOrder = HmacOrder[5];
            }
            break;
            case "EposBindPayNew": {
                requestHmacOrder = HmacOrder[6];
                responseHmacOrder = HmacOrder[7];
            }
            break;
            case "EposFactorAndVerifyCode": {
                requestHmacOrder = HmacOrder[8];
                responseHmacOrder = HmacOrder[9];
            }

            break;
            case "RefundOrd": {
                requestHmacOrder = HmacOrder[10];
                responseHmacOrder = HmacOrder[11];
            }
            break;

        }

        return	trxRequest(requestId, requestUrl, merchantId, hmacKey, params, requestHmacOrder, responseHmacOrder, method, charSet);
    }

    private Map<String,String> trxRequest(String requestId, String eposReqUrl, String merchantID, String hmacKey, Map<String, String> params, String requestHmacOrder, String responseHmacOrder, String method,
                                          String charSet) throws Exception{
        if(params == null) throw new Exception("params is null");

        if(isEmpty(hmacKey) || isEmpty(eposReqUrl) ||isEmpty(merchantID)) throw new Exception("keyValue or eposReqUrl or merchantID is null");

        if(isEmpty(requestHmacOrder)) throw new Exception("requestHmacOrder is null");

        if(isEmpty(responseHmacOrder)) throw new Exception("responseHmacOrder is null");

        /**请求参数hmac签名*/
        params = DigestUtilFacade.addHmac(requestHmacOrder.split(","), params, hmacKey);

        /**发送交易请求*/
        Map<String,String> resultMap= httpRequest(requestId, eposReqUrl,params,method,charSet);

        /**返回数据验签*/
        if(!DigestUtilFacade.checkHmac(responseHmacOrder.split(","), resultMap, hmacKey)) {
            MailService mailService = MailService.getInstance();
            String subject = "["+ KeysUtil.YEEPAY +"] " + ServerConfig.mail_server.toUpperCase() + " payment log for error checkHmac";
            String bodyDetails = "- RequestId: " + requestId + "<br>&nbsp;<br>";
            bodyDetails = bodyDetails + "- responseHmacOrder: <br>" + responseHmacOrder;
            bodyDetails = bodyDetails + "- resultMap: <br>" + gson.toJson(resultMap);
            bodyDetails = bodyDetails + "- hmacKey: <br>" + hmacKey;
            mailService.sendMailReportToSpecificEmail(requestId, subject, bodyDetails);

            throw new Exception("checkHmac is error");
        }

        return resultMap;

    }

    private Map<String,String> httpRequest(String requestId, String url, Map<String,String> params,String method,
                                                 String charSet) throws Exception{
        if (!gson.toJson(params).contains("registration") && !gson.toJson(params).contains("EposFactorAndVerifyCode"))
            logger.debug(requestId + " - params: " + gson.toJson(params));
        String result = CallbackUtils.httpRequest(url, params, method, charSet);
        logger.debug(requestId + " - response: " + result);
        if(isEmpty(result)) throw new Exception("result is null");

        Map<String,String> resultMap = CallbackUtils.queryString(result,"\n");
        if(resultMap==null) throw new Exception("resultMap is null");

        return resultMap;
    }

    public static boolean isEmpty(String str){
        return str == null || "".equals(str.replace(" ", ""));
    }

    private String getOrderId() {
        Calendar cal = Calendar.getInstance();
        Date currentTime = TimezoneUtil.offsetTimeZone(new Date(), cal.getTimeZone().getID(), "GMT");
        return formatOrder.format(currentTime);
    }

    private String getError(Exception ex) {
        return GatewayUtil.getError(ex);
    }

    private int getErrorCode(int code) {
        int returnCode = 437;
        switch (code) {
            case 18:
            case 88050:
                returnCode = 2001; // Insufficient Funds. The account did not have sufficient funds to cover the transaction amount.
                break;
            case 90:
                returnCode = 459; // Bank cards do not support
                break;
            case 3003:
                returnCode = 2014; // Processor Declined - Fraud Suspected
                break;
            case 3006:
                returnCode = 443; // No data from issuer/banknet switch
                break;
            case 3008:
                returnCode = 2005; // Invalid Credit Card Number
                break;
            case 3009:
                returnCode = 443; // No data from issuer/banknet switch
                break;
            case 8015:
                returnCode = 2024; // Card Type Not Enabled
                break;
            case 8026:
            case 88051:
                returnCode = 2002; // Limit Exceeded
                break;
            case 8036:
                returnCode = 2005; // Invalid Credit Card Number
                break;
            case 8037:
                returnCode = 461; // This card is not supported
                break;
            case 8038:
                returnCode = 422; // Expired date is invalid
                break;
            case 8039:
                returnCode = 462; // Card holder name invalid
                break;
            case 8040:
                returnCode = 432; // CVV is invalid
                break;
            case 81206:
                returnCode = 463; // Verification code is invalid
                break;
            case 81207:
                returnCode = 463; // Verification code is invalid
                break;
            case 81208:
                returnCode = 463; // Verification code is invalid
                break;
            case 88011:
                returnCode = 2038; // Processor Declined. The customer's bank is unwilling to accept the transaction, need to contact their bank for more details.
                break;
            case 88013:
                returnCode = 2038; // Processor Declined. The customer's bank is unwilling to accept the transaction, need to contact their bank for more details.
                break;
            case 88020:
                returnCode = 2014; // Processor Declined - Fraud Suspected
                break;
            case 88021:
                returnCode = 2012; // Processor Declined - Possible Lost Card
                break;
            case 88022:
                returnCode = 2004; // Expired Card
                break;
            case 81205:
                returnCode = 464; // The Bank does not support generating and sending the verification code
                break;
            case 88010:
                returnCode = 464; // The Bank does not support generating and sending the verification code
                break;

        }

        return returnCode;
    }

    private String getErrorMessage(int code) {
        String message = "";
        switch (code) {
            case 18:
            case 88050:
                message = "Card balance or insufficient credit limit, please make sure the card has enough amount of transaction";
                break;
            case 88051:
                message = "Transaction amount exceeding limits, payment failed, if traded, please choose a different bank card payments";
                break;
            case 81105:
                message = "Failed to send SMS";
                break;
            case 88071:
                message = "Banking system maintenance, please try again later";
                break;
            case 88033:
                message = "Cardholder identification information is incorrect, make sure to resubmit the transaction";
                break;
            case 88035:
                message = "Card information is inaccurate, please confirm to resubmit the transaction";
                break;
            case 88016:
                message = "The card is not open electronic payment function or card information is wrong";
                break;
            case 88032:
                message = "Bank card account's name is incorrect, make sure to resubmit the transaction";
                break;
            case 88063:
                message = "The transaction failed. Please try again later";
                break;
            case 88004:
                message = "Merchants have not yet opened this banking business";
                break;


        }

        return message;
    }


}
