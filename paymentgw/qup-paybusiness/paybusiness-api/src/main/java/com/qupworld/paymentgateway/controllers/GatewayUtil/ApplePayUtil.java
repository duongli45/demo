package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.qupworld.paymentgateway.controllers.GatewayUtil.ApplePay.MerchantX509Cert;
import com.qupworld.paymentgateway.entities.ApplePayAsyncEnt;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.File;
import java.net.URL;

/**
 * Created by qup on 30/01/2018.
 */
@SuppressWarnings("unchecked")
public class ApplePayUtil {

    final static Logger logger = LogManager.getLogger(ApplePayUtil.class);

    private final static String REFUND_SANDBOX = "https://paytest.yizhifubj.com/merchant/refund/ref_ack_submit.jsp";
    private final static String REFUND_PRODUCTION = "https://pay.yizhifubj.com/merchant/refund/ref_ack_submit.jsp";
    private final static String QUERY_SANDBOX = "https://paytest.yizhifubj.com/merchant/order/order_ack_oid_list.jsp";
    private final static String QUERY_PRODUCTION = "https://api.yizhifubj.com/merchant/order/order_ack_oid_list.jsp";
    private final static String RECON_SANDBOX = "http://merservice.yizhifubj.com/merchant/download/";
    private final static String RECON_PRODUCTION = "http://merservice.yizhifubj.com/merchant/download/";
    private final static String RPID_SANDBOX = "39159";
    private final static String RPID_PRODUCTION = "59790";
    private static String requestId;

    public ApplePayUtil(String _requestId) {
        requestId = _requestId;
    }

    public static void main(String[] args) throws Exception{
        //// VERIFY
        /*String v_oid = "20180418-14001-8233";
        String v_pstatus = "1";
        String v_amount = "0.01";
        String v_moneytype = "0";
        String v_sign = "0";
        String v_count = "1";
        logger.debug("v_oid: " + v_oid);
        logger.debug("v_pstatus: " + v_pstatus);
        logger.debug("v_amount: " + v_amount);
        logger.debug("v_moneytype: " + v_moneytype);
        logger.debug("v_sign: " + v_sign);
        logger.debug("v_count: " + v_count);
        String src = v_oid+v_pstatus+v_amount+v_moneytype+v_count;
        String certPath = readResource("payease_cfca.cer", Charsets.UTF_8);//public key file route provided by PayEase
        logger.debug("certPath: " + certPath);
        boolean verifyResult = MerchantX509Cert.verifySign(src, v_sign, certPath);
        logger.debug("verifyResult: " + verifyResult);*/

        /// REFUND
        /*double amount = 0.01;
        String v_mid = "14001";
        String v_oid = "20180418-14001-6268";
        String v_refamount = KeysUtil.DECIMAL_FORMAT.format(amount);
        String v_refreason = "Refund order " + v_oid;
        String v_refoprt = RPID_PRODUCTION;
        String src = v_mid + v_oid + v_refamount + v_refoprt;
        String pfxFile = readResource("AVIS_CFCA.pfx", Charsets.UTF_8);//public key file route provided by AVIS
        logger.debug("pfxFile: " + pfxFile);
        String pfxPassword = "1234";
        String aliasName = "{2FA328EF-4ED8-4A8A-8F3C-223B4AD21AFA}";
        String aliasPassword = "1234";
        String v_sign= MerchantX509Cert.sign(src, pfxFile, aliasName, pfxPassword, aliasPassword);
        logger.debug("v_sign: " + v_sign);

        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(REFUND_SANDBOX)
                .queryParam("v_mid", v_mid)
                .queryParam("v_oid", v_oid)
                .queryParam("v_refamount", v_refamount)
                .queryParam("v_refreason", v_refreason)
                .queryParam("v_refoprt", v_refoprt)
                .queryParam("v_mac", v_sign)
                ;
        Response response = webTarget.request().get();
        String replyString = response.readEntity(String.class);
        logger.debug("=== response: " + replyString);*/


        /// CHECK
        String v_mid = "14001";
        String v_oid = "20180418-14001-6268";
        String src = v_mid + v_oid;
        String pfxFile = readResource("avis_cfca.pfx");//public key file route provided by AVIS
        logger.debug("pfxFile: " + pfxFile);
        String pfxPassword = "1234";
        String aliasName = "{2FA328EF-4ED8-4A8A-8F3C-223B4AD21AFA}";
        String aliasPassword = "1234";
        String v_sign= MerchantX509Cert.sign(src, pfxFile, aliasName, pfxPassword, aliasPassword);
        logger.debug("v_sign: " + v_sign);

        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(QUERY_SANDBOX)
                .queryParam("v_mid", v_mid)
                .queryParam("v_oid", v_oid)
                .queryParam("v_mac", v_sign)
                ;

        Response response = webTarget.request().get();
        String replyString = response.readEntity(String.class);

        logger.debug("=== response: " + replyString);


    }

    public static String readResource(final String fileName) throws Exception {
        ClassLoader classLoader = ApplePayUtil.class.getClassLoader();
        File file = null;
        if (classLoader != null) {
            URL resource = classLoader.getResource(fileName);
            if (resource != null) {
                file = new File(resource.getFile());
                return file.getPath();
            }
        }
        return "";
    }

    public String applePayAsyncPayEase(ApplePayAsyncEnt applePayAsyncEnt) {
        try {
            String v_oid = applePayAsyncEnt.v_oid != null ? applePayAsyncEnt.v_oid : "";
            String v_pstatus = applePayAsyncEnt.v_pstatus != null ? applePayAsyncEnt.v_pstatus : "";
            String v_amount = applePayAsyncEnt.v_amount != null ? applePayAsyncEnt.v_amount : "";
            String v_moneytype = applePayAsyncEnt.v_moneytype != null ? applePayAsyncEnt.v_moneytype : "";
            String v_count = applePayAsyncEnt.v_count != null ? applePayAsyncEnt.v_count : "";
            String v_sign = applePayAsyncEnt.v_sign != null ? applePayAsyncEnt.v_sign : "";

            logger.debug(requestId + " - v_oid: " + v_oid);
            logger.debug(requestId + " - v_pstatus: " + v_pstatus);
            logger.debug(requestId + " - v_amount: " + v_amount);
            logger.debug(requestId + " - v_moneytype: " + v_moneytype);
            logger.debug(requestId + " - v_count: " + v_count);
            logger.debug(requestId + " - v_sign: " + v_sign);
            String src = v_oid+v_pstatus+v_amount+v_moneytype+v_count;
            String certPath = readResource("payease_cfca.cer"); //public key file route provided by PayEase
            logger.debug("certPath: " + certPath);
            boolean verifyResult = MerchantX509Cert.verifySign(src, v_sign, certPath);
            logger.debug(requestId + " - verifyResult: " + verifyResult);
            if(verifyResult) {
                return "sent";
            } else {
                return "error";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return "error";
        }
    }

    public String refund(String merchantId, String v_oid, double amount, boolean isSandbox) {
        ObjResponse response = new ObjResponse();
        try {
            String v_refreason = "Refund order " + v_oid;

            String endPoint = isSandbox ? REFUND_SANDBOX: REFUND_PRODUCTION;
            String v_refoprt = isSandbox ? RPID_SANDBOX : RPID_PRODUCTION;
            String v_refamount = KeysUtil.DECIMAL_FORMAT.format(amount);
            String src = merchantId + v_oid + v_refamount + v_refoprt;
            String pfxFile = readResource("avis_cfca.pfx");//public key file route provided by AVIS
            logger.debug("pfxFile: " + pfxFile);
            String pfxPassword = "1234";
            String aliasName = "{2FA328EF-4ED8-4A8A-8F3C-223B4AD21AFA}";
            String aliasPassword = "1234";
            String v_sign= MerchantX509Cert.sign(src, pfxFile, aliasName, pfxPassword, aliasPassword);
            logger.debug("v_sign: " + v_sign);

            Client client = ClientBuilder.newClient();
            WebTarget webTarget = client.target(endPoint)
                    .queryParam("v_mid", merchantId)
                    .queryParam("v_oid", v_oid)
                    .queryParam("v_refamount", v_refamount)
                    .queryParam("v_refreason", v_refreason)
                    .queryParam("v_refoprt", v_refoprt)
                    .queryParam("v_mac", v_sign)
                    ;
            Response wsResponse = webTarget.request().get();
            String replyString = wsResponse.readEntity(String.class);
            logger.debug(requestId + " - Refund response: " + replyString);

            response.returnCode = 200;
            response.response = replyString;
            return response.toString();
        } catch(Exception ex) {
            logger.debug("Error: " + ex.getMessage());
        }
        return "";
    }
}
