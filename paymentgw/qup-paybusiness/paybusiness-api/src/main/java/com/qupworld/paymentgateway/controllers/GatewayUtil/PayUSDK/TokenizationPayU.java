
package com.qupworld.paymentgateway.controllers.GatewayUtil.PayUSDK;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class TokenizationPayU {

    public String language;
    public String command;
    public Merchant merchant;
    public CreditCardToken creditCardToken;

}
