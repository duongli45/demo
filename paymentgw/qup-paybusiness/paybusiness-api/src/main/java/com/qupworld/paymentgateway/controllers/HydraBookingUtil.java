package com.qupworld.paymentgateway.controllers;

import com.google.gson.Gson;
import com.qupworld.paymentgateway.controllers.GatewayUtil.GatewayUtil;
import com.qupworld.paymentgateway.controllers.GatewayUtil.StripeUtil;
import com.qupworld.paymentgateway.entities.HydraBookingEnt;
import com.qupworld.paymentgateway.entities.HydraIncome;
import com.qupworld.paymentgateway.entities.PaymentEnt;
import com.qupworld.paymentgateway.models.*;
import com.qupworld.paymentgateway.models.mongo.collections.Credit;
import com.qupworld.paymentgateway.models.mongo.collections.booking.Booking;
import com.qupworld.paymentgateway.models.mongo.collections.corpItinerary.CorpItinerary;
import com.qupworld.paymentgateway.models.mongo.collections.fleet.Fleet;
import com.qupworld.paymentgateway.models.mongo.collections.gateway.GatewayStripe;
import com.qupworld.service.QUpReportPublisher;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.TimezoneUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by qup on 24/12/2018.
 */
@SuppressWarnings("unchecked")
public class HydraBookingUtil {

    Gson gson;
    MongoDao mongoDao;
    SQLDao sqlDao;
    RedisDao redisDao;
    PaymentUtil paymentUtil;
    final static Logger logger = LogManager.getLogger(HydraBookingUtil.class);

    public HydraBookingUtil(){
        gson = new Gson();
        sqlDao = new SQLDaoImpl();
        mongoDao = new MongoDaoImpl();
        redisDao = new RedisImpl();
        paymentUtil = new PaymentUtil();
    }

    public String chargeDMCFleet(HydraBookingEnt hydraBookingEnt, Booking booking, Fleet dmcFleet, Fleet supplierFleet, double amount) {
        ObjResponse objResponse = new ObjResponse();

        // get default card of DMC
        String token = "";
        for (Credit credit : dmcFleet.credits) {
            if (credit.defaultCard) {
                token = credit.crossToken != null ? credit.crossToken.trim() : "";
            }
        }
        if (token.isEmpty()) {
            objResponse.returnCode = 482;
            Map<String,String> map = new HashMap<>();
            map.put("message", booking.bookFrom.toUpperCase() + " fleet does not register any default credit card");
            objResponse.response = map;
            return objResponse.toString();
        }
        StripeUtil stripeUtil = new StripeUtil(hydraBookingEnt.requestId);
        // get QUp Stripe account
        GatewayStripe gateway;
        if (!dmcFleet.affiliateEnvironment) {
            gateway = mongoDao.getGatewayStripeSystem(true);//true is sandbox
        } else {
            gateway = mongoDao.getGatewayStripeSystem(false);//false is product
        }
        logger.debug(hydraBookingEnt.requestId + " - token: " + token);
        String payResult = stripeUtil.createPaymentWithCreditToken(booking.fleetId, hydraBookingEnt.bookId,
                booking.currencyISO, amount, token, gateway.secretKey);
        logger.debug(hydraBookingEnt.requestId + " - chargeDMCFleet: " + payResult);
        JSONObject object = paymentUtil.toObjectResponse(payResult);
        int returnCode = Integer.parseInt(object.get("returnCode").toString());
        String transId = "";
        String status;
        if (returnCode == 200) {
            JSONObject response = (JSONObject)object.get("response");
            transId = response.get("transId") != null ? response.get("transId").toString() : "";
            status = KeysUtil.INCOME_CHARGED;
        } else {
            status = KeysUtil.INCOME_CHARGE_FAILED;
        }

        // update to database
        HydraIncome hydraIncome = new HydraIncome();
        hydraIncome.bookId = hydraBookingEnt.bookId;
        hydraIncome.dmcFleetId = dmcFleet.fleetId;
        hydraIncome.dmcFleetName = dmcFleet.name;
        if (supplierFleet != null) {
            hydraIncome.supplierFleetId = supplierFleet.fleetId;
            hydraIncome.supplierFleetName = supplierFleet.name;
        }
        hydraIncome.amount = amount;
        hydraIncome.currencyISO = booking.currencyISO;
        hydraIncome.currencySymbol = booking.currencySymbol;
        hydraIncome.reason = "Charge " + booking.bookFrom.toUpperCase() +" fleet for booking " + hydraBookingEnt.bookId;
        HydraIncome firstCharge = sqlDao.getHydraIncomeByBookId(hydraBookingEnt.bookId, KeysUtil.INCOME_CHARGED);
        if (firstCharge != null) {
            hydraIncome.reason = "Re-charge " + booking.bookFrom.toUpperCase() +" fleet for booking " + hydraBookingEnt.bookId;
        }
        hydraIncome.createdDate = TimezoneUtil.getGMTTimestamp();
        hydraIncome.paidDate = TimezoneUtil.getGMTTimestamp();
        hydraIncome.happenedTime = TimezoneUtil.getGMTTimestamp();
        hydraIncome.chargeId = transId;
        hydraIncome.status = status;

        String itineraryId = "";
        String eventId = "";
        String eventName = "";
        String companyId = "";
        String companyName = "";
        int travelerType = 0;
        if (booking.itinerary != null) {
            itineraryId = booking.itinerary._id.toString();
            if (booking.itinerary.event != null) {
                eventId = booking.itinerary.event._id.toString();
                eventName = booking.itinerary.event.name != null ? booking.itinerary.event.name : "";
                travelerType = booking.itinerary.customerType;
            }
            if (booking.itinerary.organizationInfo != null) {
                companyId = booking.itinerary.organizationInfo._id.toString();
                companyName = booking.itinerary.organizationInfo.name != null ? booking.itinerary.organizationInfo.name : "";
            }
        }
        hydraIncome.itineraryId = itineraryId;
        hydraIncome.eventId = eventId;
        hydraIncome.eventName = eventName;
        hydraIncome.travelerType = travelerType;
        hydraIncome.companyId = companyId;
        hydraIncome.companyName = companyName;
        String pickup = "";
        String timeZonePickup = "";
        String destination = "";
        String timeZoneDestination = "";
        if (booking.request.pickup != null) {
            pickup = booking.request.pickup.address != null ? CommonUtils.removeSpecialCharacter(booking.request.pickup.address) : "";
            if (pickup.length() > 255)
                pickup = pickup.substring(0,255);
            timeZonePickup = booking.request.pickup.timezone != null ? booking.request.pickup.timezone : "";
        }
        if (booking.request.destination != null) {
            destination = booking.request.destination.address != null ? CommonUtils.removeSpecialCharacter(booking.request.destination.address) : "";
            if (destination.length() > 255)
                destination = destination.substring(0,255);
            timeZoneDestination = booking.request.destination.timezone != null ? booking.request.destination.timezone : "";
        }
        hydraIncome.pickup = pickup;
        hydraIncome.timeZonePickUp = timeZonePickup;
        hydraIncome.destination = destination;
        hydraIncome.timeZoneDestination = timeZoneDestination;
        Timestamp pickupTime = null;
        try {
            if (booking.time != null) {
                if (booking.time.pickUpTime != null) {
                    Date pickupDate = TimezoneUtil.offsetTimeZone(booking.time.pickUpTime, Calendar.getInstance().getTimeZone().getID(), "GMT");
                    pickupTime = new Timestamp(pickupDate.getTime());
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        hydraIncome.pickupTime = pickupTime;

        int result = sendReportHydraIncome(hydraBookingEnt.requestId, hydraIncome, true);
        if (result != 200) {
            // send log to email
            logger.debug(hydraBookingEnt.requestId + " - hydra income: " + gson.toJson(hydraIncome));
            GatewayUtil.sendMailHydraBooking(hydraBookingEnt.requestId, "charge", booking.bookId, gson.toJson(hydraIncome));

            // void charged transaction
            if (!transId.equals(""))
                stripeUtil.voidStripeTransaction("", dmcFleet.fleetId, hydraBookingEnt.bookId, gateway.secretKey, transId);

            objResponse.returnCode = 437;
            Map<String,String> map = new HashMap<>();
            map.put("message", "Cannot add transaction to MySQL database");
            objResponse.response = map;
            return objResponse.toString();
        }
        return payResult;
    }

    public String chargeCorpItinerary(HydraBookingEnt hydraBookingEnt, Booking booking, Fleet dmcFleet, Fleet supplierFleet, double amount) {
        ObjResponse objResponse = new ObjResponse();
        String token = "";
        if (booking.bookFrom.equals(KeysUtil.corp_board) && booking.itinerary != null) {
            String itineraryId = booking.itinerary._id.toString();
            CorpItinerary corpItinerary = mongoDao.getCorpItinerary(itineraryId);
            if (corpItinerary != null && corpItinerary.corpCreditCard != null) {
                token = corpItinerary.corpCreditCard.crossToken != null ? corpItinerary.corpCreditCard.crossToken : "";
            }
        }

        if (token.isEmpty()) {
            objResponse.returnCode = 482;
            Map<String,String> map = new HashMap<>();
            map.put("message", "corp itinerary does not have any credit card");
            objResponse.response = map;
            return objResponse.toString();
        }
        StripeUtil stripeUtil = new StripeUtil(hydraBookingEnt.requestId);
        // get QUp Stripe account
        GatewayStripe gateway;
        if (!dmcFleet.affiliateEnvironment) {
            gateway = mongoDao.getGatewayStripeSystem(true);//true is sandbox
        } else {
            gateway = mongoDao.getGatewayStripeSystem(false);//false is product
        }
        logger.debug(hydraBookingEnt.requestId + " - token: " + token);
        String payResult = stripeUtil.createPaymentWithCreditToken(booking.fleetId, hydraBookingEnt.bookId,
                booking.currencyISO, amount, token, gateway.secretKey);
        logger.debug(hydraBookingEnt.requestId + " - chargeCorpItinerary: " + payResult);
        JSONObject object = paymentUtil.toObjectResponse(payResult);
        int returnCode = Integer.parseInt(object.get("returnCode").toString());
        String transId = "";
        String status;
        if (returnCode == 200) {
            JSONObject response = (JSONObject)object.get("response");
            transId = response.get("transId") != null ? response.get("transId").toString() : "";
            status = KeysUtil.INCOME_CHARGED;
        } else {
            status = KeysUtil.INCOME_CHARGE_FAILED;
        }

        // update to database
        HydraIncome hydraIncome = new HydraIncome();
        hydraIncome.bookId = hydraBookingEnt.bookId;
        hydraIncome.dmcFleetId = dmcFleet.fleetId;
        hydraIncome.dmcFleetName = dmcFleet.name;
        if (supplierFleet != null) {
            hydraIncome.supplierFleetId = supplierFleet.fleetId;
            hydraIncome.supplierFleetName = supplierFleet.name;
        }
        hydraIncome.amount = amount;
        hydraIncome.currencyISO = booking.currencyISO;
        hydraIncome.currencySymbol = booking.currencySymbol;
        hydraIncome.reason = "Charge " + booking.bookFrom.toUpperCase() +" fleet for booking " + hydraBookingEnt.bookId;
        HydraIncome firstCharge = sqlDao.getHydraIncomeByBookId(hydraBookingEnt.bookId, KeysUtil.INCOME_CHARGED);
        if (firstCharge != null) {
            hydraIncome.reason = "Re-charge " + booking.bookFrom.toUpperCase() +" fleet for booking " + hydraBookingEnt.bookId;
        }
        hydraIncome.createdDate = TimezoneUtil.getGMTTimestamp();
        hydraIncome.paidDate = TimezoneUtil.getGMTTimestamp();
        hydraIncome.happenedTime = TimezoneUtil.getGMTTimestamp();
        hydraIncome.chargeId = transId;
        hydraIncome.status = status;

        String itineraryId = "";
        String eventId = "";
        String eventName = "";
        String companyId = "";
        String companyName = "";
        int travelerType = 0;
        if (booking.itinerary != null) {
            itineraryId = booking.itinerary._id.toString();
            if (booking.itinerary.event != null) {
                eventId = booking.itinerary.event._id.toString();
                eventName = booking.itinerary.event.name != null ? booking.itinerary.event.name : "";
                travelerType = booking.itinerary.customerType;
            }
            if (booking.itinerary.organizationInfo != null) {
                companyId = booking.itinerary.organizationInfo._id.toString();
                companyName = booking.itinerary.organizationInfo.name != null ? booking.itinerary.organizationInfo.name : "";
            }
        }
        hydraIncome.itineraryId = itineraryId;
        hydraIncome.eventId = eventId;
        hydraIncome.eventName = eventName;
        hydraIncome.travelerType = travelerType;
        hydraIncome.companyId = companyId;
        hydraIncome.companyName = companyName;
        String pickup = "";
        String timeZonePickup = "";
        String destination = "";
        String timeZoneDestination = "";
        if (booking.request.pickup != null) {
            pickup = booking.request.pickup.address != null ? CommonUtils.removeSpecialCharacter(booking.request.pickup.address) : "";
            if (pickup.length() > 255)
                pickup = pickup.substring(0,255);
            timeZonePickup = booking.request.pickup.timezone != null ? booking.request.pickup.timezone : "";
        }
        if (booking.request.destination != null) {
            destination = booking.request.destination.address != null ? CommonUtils.removeSpecialCharacter(booking.request.destination.address) : "";
            if (destination.length() > 255)
                destination = destination.substring(0,255);
            timeZoneDestination = booking.request.destination.timezone != null ? booking.request.destination.timezone : "";
        }
        hydraIncome.pickup = pickup;
        hydraIncome.timeZonePickUp = timeZonePickup;
        hydraIncome.destination = destination;
        hydraIncome.timeZoneDestination = timeZoneDestination;
        Timestamp pickupTime = null;
        try {
            if (booking.time != null) {
                if (booking.time.pickUpTime != null) {
                    Date pickupDate = TimezoneUtil.offsetTimeZone(booking.time.pickUpTime, Calendar.getInstance().getTimeZone().getID(), "GMT");
                    pickupTime = new Timestamp(pickupDate.getTime());
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        hydraIncome.pickupTime = pickupTime;

        int result = sendReportHydraIncome(hydraBookingEnt.requestId, hydraIncome, false);
        if (result != 200) {
            // send log to email
            logger.debug(hydraBookingEnt.requestId + " - hydra income: " + gson.toJson(hydraIncome));
            GatewayUtil.sendMailHydraBooking(hydraBookingEnt.requestId, "charge", booking.bookId, gson.toJson(hydraIncome));

            // void charged transaction
            if (!transId.equals(""))
                stripeUtil.voidStripeTransaction("", dmcFleet.fleetId, hydraBookingEnt.bookId, gateway.secretKey, transId);

            objResponse.returnCode = 437;
            Map<String,String> map = new HashMap<>();
            map.put("message", "Cannot add transaction to MySQL database");
            objResponse.response = map;
            return objResponse.toString();
        }
        return payResult;
    }

    public int sendReportHydraIncome(String requestId, HydraIncome hydraIncome, boolean dmcBooking) {
        int returnCode = 0;
        long addIncome = sqlDao.addHydraIncome(hydraIncome);
        if (addIncome == 0) {
            // something went wrong, try to add again
            try {
                Thread.sleep(5000);
            } catch(InterruptedException ignore) {}
            addIncome = sqlDao.addHydraIncome(hydraIncome);
        }

        if (addIncome != 0) {
            if (dmcBooking) {
                // send to server Report
                String jsonData = gson.toJson(hydraIncome);
                try {
                    JSONParser parser = new JSONParser();
                    JSONObject jsonObject = (JSONObject) parser.parse(jsonData);
                    if (hydraIncome.createdDate != null) {
                        jsonObject.remove("createdDate");
                        jsonObject.put("createdDate", KeysUtil.SDF_DMYHMS.format(hydraIncome.createdDate));
                    }
                    if (hydraIncome.paidDate != null) {
                        jsonObject.remove("paidDate");
                        jsonObject.put("paidDate", KeysUtil.SDF_DMYHMS.format(hydraIncome.paidDate));
                    }
                    if (hydraIncome.completedTime != null) {
                        jsonObject.remove("completedTime");
                        jsonObject.put("completedTime", KeysUtil.SDF_DMYHMS.format(hydraIncome.completedTime));
                    }
                    if (hydraIncome.happenedTime != null) {
                        jsonObject.remove("happenedTime");
                        jsonObject.put("happenedTime", KeysUtil.SDF_DMYHMS.format(hydraIncome.happenedTime));
                    }
                    if (hydraIncome.pickupTime != null) {
                        jsonObject.remove("pickupTime");
                        jsonObject.put("pickupTime", KeysUtil.SDF_DMYHMS.format(hydraIncome.pickupTime));
                    }
                    jsonData = jsonObject.toJSONString();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                // send to Reporting
                // ignore status accepted which is stored for retrieve from MySQL only
                if (!hydraIncome.status.equals(KeysUtil.INCOME_ACCEPTED)) {
                    QUpReportPublisher reportPublisher = QUpReportPublisher.getInstance();
                    reportPublisher.sendOneItem(requestId, String.valueOf(addIncome), "HydraIncome", jsonData);
                }
            }
            returnCode = 200;
        }
        return returnCode;
    }

    public void refundDMCFleet(PaymentEnt paymentEnt, Booking booking, Fleet dmcFleet, HydraIncome hydraIncome, double refundAmount, boolean isPartialRefund) {

        GatewayStripe gateway;
        if (!dmcFleet.affiliateEnvironment) {
            gateway = mongoDao.getGatewayStripeSystem(true);//true is sandbox
        } else {
            gateway = mongoDao.getGatewayStripeSystem(false);//false is product
        }
        StripeUtil stripeUtil = new StripeUtil(paymentEnt.requestId);
        String result;
        if (isPartialRefund) {
            if (refundAmount <= 0) {
                // do not refund - create success result
                ObjResponse voidResponse = new ObjResponse();
                Map<String, String> mapResponse = new HashMap<String, String>();
                mapResponse.put("refundId", "");
                voidResponse.response = mapResponse;
                voidResponse.returnCode = 200;
                result = voidResponse.toString();
            } else {
                result = stripeUtil.voidStripeTransaction(paymentEnt.requestId, dmcFleet.fleetId, booking.bookId, hydraIncome.currencyISO,
                        refundAmount, gateway.secretKey, hydraIncome.chargeId);
            }

        } else {
            result = stripeUtil.voidStripeTransaction(paymentEnt.requestId, dmcFleet.fleetId, booking.bookId, gateway.secretKey, hydraIncome.chargeId);
        }
        logger.debug(paymentEnt.requestId + " - refundDMCFleet: " + result);
        JSONObject resultObject = paymentUtil.toObjectResponse(result);
        int returnCode = 0;
        if (resultObject != null && resultObject.get("returnCode") != null) {
            returnCode = Integer.parseInt(resultObject.get("returnCode").toString());
        }

        // add status income
        HydraIncome refundIncome = new HydraIncome();
        refundIncome.bookId = booking.bookId;
        refundIncome.dmcFleetId = hydraIncome.dmcFleetId;
        refundIncome.dmcFleetName = hydraIncome.dmcFleetName;
        refundIncome.supplierFleetId = hydraIncome.supplierFleetId;
        refundIncome.supplierFleetName = hydraIncome.supplierFleetName;
        refundIncome.refundAmount = refundAmount;
        refundIncome.currencyISO = hydraIncome.currencyISO;
        refundIncome.reason = "Refund " + booking.bookFrom.toUpperCase() + " fleet for cancel booking " + paymentEnt.bookId;

        refundIncome.createdDate = TimezoneUtil.getGMTTimestamp();
        refundIncome.refundedDate = TimezoneUtil.getGMTTimestamp();

        if (returnCode == 200) {
            JSONObject response = (JSONObject)resultObject.get("response");
            refundIncome.chargeId = response.get("refundId") != null ? response.get("refundId").toString() : "";
            refundIncome.status = KeysUtil.INCOME_REFUNDED;
            // update to booking
            mongoDao.updateAffiliateStatus(booking.bookId, 3); // 3 = refunded
        } else {
            refundIncome.status = KeysUtil.INCOME_REFUND_FAILED;
        }

        // update to database and send to report
        long addIncome = sqlDao.addHydraIncome(refundIncome);
        if (addIncome == 0) {
            // something went wrong, try to add again
            try {
                Thread.sleep(5000);
            } catch(InterruptedException ignore) {}
            addIncome = sqlDao.addHydraIncome(refundIncome);
        }

        if (addIncome != 0) {
            // send to server Report
            String jsonData = gson.toJson(refundIncome);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                JSONParser parser = new JSONParser();
                JSONObject jsonObject = (JSONObject)parser.parse(jsonData);
                jsonObject.remove("createdDate");
                jsonObject.put("createdDate", sdf.format(refundIncome.createdDate));
                jsonObject.remove("refundedDate");
                jsonObject.put("refundedDate", sdf.format(refundIncome.refundedDate));

                jsonData = jsonObject.toJSONString();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            // send to Reporting
            QUpReportPublisher reportPublisher = QUpReportPublisher.getInstance();
            reportPublisher.sendOneItem(paymentEnt.requestId, String.valueOf(addIncome), "HydraIncome", jsonData);
        } else {
            logger.debug(paymentEnt.requestId + " - Hydra income: " + gson.toJson(refundIncome));
            // send log to email for checking later
            GatewayUtil.sendMailHydraBooking(paymentEnt.requestId, "refund", booking.bookId, gson.toJson(refundIncome));
        }
    }

}
