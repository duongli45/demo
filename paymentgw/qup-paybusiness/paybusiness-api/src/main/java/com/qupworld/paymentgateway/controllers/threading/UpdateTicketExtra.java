package com.qupworld.paymentgateway.controllers.threading;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.qupworld.paymentgateway.models.SQLDao;
import com.qupworld.paymentgateway.models.SQLDaoImpl;
import com.pg.util.KeysUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by qup on 23/03/2020.
 */
@SuppressWarnings("unchecked")
public class UpdateTicketExtra extends Thread {
    final static Logger logger = LogManager.getLogger(UpdateTicketExtra.class);
    public String FLEETID = "";
    public String requestId = "";
    public UpdateTicketExtra(String fleetId) {
        this.FLEETID = fleetId;
        this.requestId = UUID.randomUUID().toString();
    }

    @Override
    public void run() {
        try {

            SQLDao sqlDao = new SQLDaoImpl();
            boolean valid = true;
            int count = KeysUtil.SIZE;
            int i = 0;
            GsonBuilder gb = new GsonBuilder();
            gb.serializeNulls();
            Gson gson = gb.create();
            int size = 0;
            logger.debug(requestId + " - --- UPDATE TICKET EXTRA ...");
            while (valid) {
                try {
                    if ((i * count) % 2000 == 0) {
                        logger.debug(requestId + " - total = " + i * count + " sleep ...");
                        Thread.sleep(2000);
                    }
                    String allTickets = sqlDao.getTickets(i, FLEETID);
                    JSONParser parser = new JSONParser();
                    JSONObject jsonObject = (JSONObject) parser.parse(allTickets);
                    JSONArray records = (JSONArray) jsonObject.get("records");
                    JSONArray fields = (JSONArray) jsonObject.get("fields");
                    size = records.size();

                    List<String> listKeys = new ArrayList<>();
                    for (Object field : fields) {
                        JSONObject object = (JSONObject) field;
                        listKeys.add(object.get("name").toString());
                    }

                    logger.debug(requestId + " - i = " + i + " - size: " + records.size());
                    if (size > 0) {
                        for (Object record : records) {
                            JSONArray object = (JSONArray) record;
                            String bookId = object.get(listKeys.indexOf("bookId")) != null ? object.get(listKeys.indexOf("bookId")).toString() : "";
                            String referralObj = object.get(listKeys.indexOf("varchar4")) != null ? object.get(listKeys.indexOf("varchar4")).toString() : "";
                            String tipAfterRideObj = object.get(listKeys.indexOf("varchar7")) != null ? object.get(listKeys.indexOf("varchar7")).toString() : "";
                            double referalEarning = 0.0;
                            double tipAfterRide = 0.0;

                            JSONObject mapReferral = gson.fromJson(referralObj, JSONObject.class);
                            JSONObject mapTipAfterRide = gson.fromJson(tipAfterRideObj, JSONObject.class);
                            if(mapReferral != null){
                                referalEarning = Double.valueOf(mapReferral.get("referralEarning").toString());
                            }
                            if(mapTipAfterRide != null){
                                tipAfterRide = Double.valueOf(mapTipAfterRide.get("driverTipAfterCompleted").toString());
                            }

                            sqlDao.addTicketExtra(bookId, referalEarning, tipAfterRide, 0 );
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    logger.debug(requestId + " - error: " + ex.getMessage());
                } finally {
                    if (size < count) {
                        valid = false;
                    } else {
                        i++;
                    }
                }
            }
            logger.debug(requestId + " - total ticket = " + (i * count + size));
            logger.debug(requestId + " - --- DONE !!!");
        } catch(Exception ex) {
            ex.printStackTrace();
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            logger.debug(requestId + " - MigrateTicket: "+ errors.toString());
        }
    }
}
