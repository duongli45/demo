package com.qupworld.paymentgateway.controllers.GatewayUtil.ApplePay;

/**
 * Created by thuanho on 20/05/2022.
 */
import com.alibaba.fastjson15.JSONObject;
import com.upay.sdk.Constants;
import com.upay.sdk.SignPublisher;
import com.upay.sdk.exception.UnknownException;
import java.lang.reflect.Field;

public class BuilderSupport {

    public JSONObject build(String merchantId) {
        JSONObject json = new JSONObject(true);
        json.put(Constants.MERCHANT_ID, merchantId);
        return json;
    }

    protected JSONObject assembleBuild() {
        JSONObject json;
        Class<?> clazz = getClass();
        try {
            json = findField(clazz);
        } catch (Exception e) {
            throw new UnknownException(e);
        }
        return json;
    }

    private void putField(Class clazz, JSONObject json) throws IllegalAccessException {
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            Object obj = field.get(this);
            if (obj != null)
                json.put(field.getName(), obj);
        }
    }

    private JSONObject findField(Class clazz) throws IllegalAccessException {
        JSONObject json = new JSONObject();
        for (Class<Object> parent = clazz.getSuperclass(); parent != null && parent != Object.class; parent = (Class)parent.getSuperclass())
            putField(parent, json);
        putField(clazz, json);
        return json;
    }

    protected String orderGenerateHmac() {
        String hmacSource;
        try {
            hmacSource = SignPublisher.generateHmacSource(this);
        } catch (Exception e) {
            throw new UnknownException(e);
        }
        return hmacSource;
    }

}
