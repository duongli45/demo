package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.Base64Encoder;
import com.qupworld.paymentgateway.entities.CreditEnt;
import com.qupworld.paymentgateway.entities.PaymentEnt;
import com.pg.util.CommonUtils;
import com.pg.util.ObjResponse;
import com.pg.util.ValidCreditCard;
import org.apache.commons.codec.binary.Hex;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class SenangpayUtil {

    private static String MERCHANTACCOUNT = "806153239634275";
    private static String WS_USER = "806153239634275";
    private static String WS_PASSWORD = "";
    private static String SECRET_KEY = "3-412";
    private static String REQUEST_ID = "";
    private static String TOKEN_URL = "";
    private static String PAYMENT_URL = "";

    final static Logger logger = LogManager.getLogger(SenangpayUtil.class);

    public SenangpayUtil(String requestId, String merchantAccount, String wsUser, String wsPassword, String secretKey, String tokenURL, String paymentURL) {
        REQUEST_ID = requestId;
        MERCHANTACCOUNT = merchantAccount;
        WS_USER = wsUser;
        WS_PASSWORD = wsPassword;
        SECRET_KEY = secretKey;
        TOKEN_URL = tokenURL;
        PAYMENT_URL = paymentURL;
    }

    public String createCreditToken(CreditEnt creditEnt, String customerName, String customerEmail, String customerPhone) throws IOException {
        ObjResponse createTokenResponse = new ObjResponse();
        try {
            customerName = customerName.trim();
            customerEmail = customerEmail.trim();
            customerPhone = customerPhone.trim();
            String[] date = creditEnt.expiredDate.split("/");
            ArrayList<NameValuePair> postParameters = new ArrayList<>();
            postParameters.add(new BasicNameValuePair("name", customerName));
            postParameters.add(new BasicNameValuePair("email", customerEmail));
            postParameters.add(new BasicNameValuePair("phone", customerPhone));
            postParameters.add(new BasicNameValuePair("cc_number", creditEnt.cardNumber));
            postParameters.add(new BasicNameValuePair("cc_exp", date[0] + date[1].substring(2,4)));
            postParameters.add(new BasicNameValuePair("cc_cvv", creditEnt.cvv));
            for (NameValuePair param : postParameters) {
                logger.debug(REQUEST_ID + " - " + param.getName() + ": " + param.getValue().replace(creditEnt.cardNumber, "XXXXXXXXXXXX" + creditEnt.cardNumber.substring(creditEnt.cardNumber.length() - 4)));
            }
            String encoding = Base64Encoder.encodeString(WS_USER + ":" + WS_PASSWORD);
            HttpClient client = HttpClientBuilder.create().build();

            HttpPost httpRequest = new HttpPost(TOKEN_URL);
            httpRequest.setHeader("Authorization", "Basic " + encoding);
            httpRequest.addHeader("Content-Type", "application/x-www-form-urlencoded");
            httpRequest.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));

            HttpResponse httpResponse = client.execute(httpRequest);
            String paymentResponse = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
            logger.debug(REQUEST_ID + " - Credit Result: " + paymentResponse);
            JSONParser parser = new JSONParser();
            JSONObject response = (JSONObject) parser.parse(paymentResponse);
            int status = Integer.parseInt(response.get("status").toString());
            if (status == 1) {
                Map<String, String> mapResponse = new HashMap<>();
                mapResponse.put("creditCard", "XXXXXXXXXXXX" + creditEnt.cardNumber.substring(creditEnt.cardNumber.length() - 4));
                mapResponse.put("token", response.get("token").toString());
                mapResponse.put("cardType", ValidCreditCard.getCardType(creditEnt.cardNumber));

                createTokenResponse.returnCode = 200;
                createTokenResponse.response = mapResponse;
            } else {
                Map<String,String> mapResponse = new HashMap<>();
                String message = response.get("msg") != null ? response.get("msg").toString() : "";
                mapResponse.put("message", message);
                if (message.toLowerCase().contains("declined"))
                    createTokenResponse.returnCode = 436;
                else {
                    createTokenResponse.returnCode = getErrorCode(status);
                }
                createTokenResponse.response = mapResponse;
            }
            return createTokenResponse.toString();
        } catch (Exception ex) {
            createTokenResponse.returnCode = 437;
            createTokenResponse.response = getError(ex);
        }
        return createTokenResponse.toString();
    }

    public String initiateCreditForm(String name, String phone, String email) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();

        String htmlForm = "";
        String orderId = GatewayUtil.getOrderId();
        try {
            String hashString = MERCHANTACCOUNT + orderId;
            logger.debug("hashString = " + hashString);
            String hash = hmacDigest(hashString, SECRET_KEY);
            logger.debug("hash = " + hash);

            htmlForm = GatewayUtil.getFile(REQUEST_ID, "senangpay.html");
            htmlForm = htmlForm.replace("action_page", TOKEN_URL + MERCHANTACCOUNT);
            htmlForm = htmlForm.replace("order_value", orderId);
            htmlForm = htmlForm.replace("name_value", name);
            htmlForm = htmlForm.replace("email_value", email);
            htmlForm = htmlForm.replace("phone_value", phone);
            htmlForm = htmlForm.replace("hash_value", hash);
            htmlForm = htmlForm.replace("\"","'").replace("\n","").replace("\r", "");
            logger.debug(REQUEST_ID + " - htmlForm = " + htmlForm);
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        objResponse.returnCode = 200;
        mapResponse.put("3ds_url", htmlForm);
        mapResponse.put("type", "form");
        mapResponse.put("transactionId", orderId);
        objResponse.response = mapResponse;
        return objResponse.toString();
    }

    public String createPaymentWithCreditToken(String bookId, double amount, String token, String payerName, String payerEmail, String payerPhone,
                                               String cardType, String cardMask, String requestId){
        ObjResponse payResponse = new ObjResponse();
        String chargeId;
        try {
            payerName = payerName.trim();
            payerEmail = payerEmail.trim();
            payerPhone = payerPhone.trim();
            DecimalFormat df = new DecimalFormat("#");
            int intAmount = Integer.parseInt(df.format(amount*100));
            logger.debug(requestId + " - intAmount: " + intAmount);
            String detail = "Pay for bookId " + bookId;
            if (bookId.equals("topup")) {
                bookId = "topup-" + GatewayUtil.getOrderId();
                detail = "MyCar Credit Wallet Top Up for " + payerPhone;
            }
            String hashString = MERCHANTACCOUNT + payerName + payerEmail + payerPhone + detail + bookId + intAmount;
            logger.debug(requestId + " - hashString = " + hashString);
            String hash = encode(SECRET_KEY, hashString);
            logger.debug(requestId + " - hash = " + hash);
            if (requestId.isEmpty())
                requestId = payerName;
            ArrayList<NameValuePair> postParameters = new ArrayList<>();
            postParameters.add(new BasicNameValuePair("order_id", bookId));
            postParameters.add(new BasicNameValuePair("name", payerName));
            postParameters.add(new BasicNameValuePair("phone", payerPhone));
            postParameters.add(new BasicNameValuePair("email", payerEmail));
            postParameters.add(new BasicNameValuePair("detail", detail));
            postParameters.add(new BasicNameValuePair("amount", String.valueOf(intAmount)));
            postParameters.add(new BasicNameValuePair("token", token));
            postParameters.add(new BasicNameValuePair("hash", hash));
            for (NameValuePair param : postParameters) {
                logger.debug(requestId + " - " + param.getName() + ": " + param.getValue());
            }
            String encoding = Base64Encoder.encodeString(WS_USER + ":" + WS_PASSWORD);
            HttpClient client = HttpClientBuilder.create().build();

            HttpPost httpRequest = new HttpPost(PAYMENT_URL);
            httpRequest.setHeader("Authorization", "Basic " + encoding);
            httpRequest.addHeader("Content-Type", "application/x-www-form-urlencoded");
            httpRequest.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));

            HttpResponse httpResponse = client.execute(httpRequest);
            String paymentResponse = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
            logger.debug(requestId + " - Payment Result: " + paymentResponse);
            JSONParser parser = new JSONParser();
            JSONObject response = (JSONObject) parser.parse(paymentResponse);
            int status = Integer.parseInt(response.get("status").toString());
            if (status == 1) {
                chargeId = response.get("transaction_id") != null ? response.get("transaction_id").toString() : "";
                Map<String, String> mapResponse = new HashMap<>();
                mapResponse.put("message", "Success!");
                mapResponse.put("transId", chargeId);
                mapResponse.put("cardType", cardType);
                mapResponse.put("authCode", "");
                mapResponse.put("cardMask", cardMask.length() == 4 ? "XXXXXXXXXXXX" + cardMask : cardMask);
                payResponse.response = mapResponse;
                payResponse.returnCode = 200;
            } else {
                Map<String,String> mapResponse = new HashMap<>();
                String message = response.get("msg") != null ? response.get("msg").toString() : "";
                mapResponse.put("message", message);
                if (message.toLowerCase().contains("declined")) {
                    payResponse.returnCode = 436;
                }else
                    payResponse.returnCode = getErrorCode(status);
                payResponse.response = mapResponse;
            }
            return payResponse.toString();
        } catch(Exception ex) {
            return GatewayUtil.returnError(ex, 437);
        }

    }

    public String paymentWithInputCard(PaymentEnt paymentEnt, String bookId, double amount, String customerName,
                                       String customerEmail, String customerPhone) {
        ObjResponse payResponse = new ObjResponse();
        String chargeId;
        try {
            customerName = customerName.trim();
            customerEmail = customerEmail.trim();
            customerPhone = customerPhone.trim();
            DecimalFormat df = new DecimalFormat("#");
            int intAmount = Integer.parseInt(df.format(amount*100));
            logger.debug("intAmount: " + intAmount);
            String detail = "Pay for bookId " + bookId;
            String hashString = MERCHANTACCOUNT + customerName + customerEmail + customerPhone + detail + bookId + intAmount;
            logger.debug("hashString = " + hashString);
            String hash = encode(SECRET_KEY, hashString);
            logger.debug("hash = " + hash);
            String requestId = paymentEnt.requestId != null ? paymentEnt.requestId : "";
            if (requestId.isEmpty())
                requestId = customerName;
            String[] date = paymentEnt.expiredDate.split("/");
            ArrayList<NameValuePair> postParameters = new ArrayList<>();
            postParameters.add(new BasicNameValuePair("order_id", bookId));
            postParameters.add(new BasicNameValuePair("name", customerName));
            postParameters.add(new BasicNameValuePair("phone", customerPhone));
            postParameters.add(new BasicNameValuePair("email", customerEmail));
            postParameters.add(new BasicNameValuePair("detail", detail));
            postParameters.add(new BasicNameValuePair("amount", String.valueOf(intAmount)));
            postParameters.add(new BasicNameValuePair("cc_number", paymentEnt.cardNumber));
            postParameters.add(new BasicNameValuePair("cc_exp", date[0] + date[1].substring(2,4)));
            postParameters.add(new BasicNameValuePair("cc_cvv", paymentEnt.cvv));
            postParameters.add(new BasicNameValuePair("hash", hash));
            for (NameValuePair param : postParameters) {
                logger.debug(requestId + " - " + param.getName() + ": " + param.getValue().replace(paymentEnt.cardNumber, "XXXXXXXXXXXX" + paymentEnt.cardNumber.substring(paymentEnt.cardNumber.length() - 4)));
            }
            String encoding = Base64Encoder.encodeString(WS_USER + ":" + WS_PASSWORD);
            HttpClient client = HttpClientBuilder.create().build();

            HttpPost httpRequest = new HttpPost(PAYMENT_URL);
            httpRequest.setHeader("Authorization", "Basic " + encoding);
            httpRequest.addHeader("Content-Type", "application/x-www-form-urlencoded");
            httpRequest.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));

            HttpResponse httpResponse = client.execute(httpRequest);
            String paymentResponse = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
            logger.debug(requestId + " - Payment Result: " + paymentResponse);
            JSONParser parser = new JSONParser();
            JSONObject response = (JSONObject) parser.parse(paymentResponse);
            int status = Integer.parseInt(response.get("status").toString());
            if (status == 1) {
                chargeId = response.get("transaction_id") != null ? response.get("transaction_id").toString() : "";
                Map<String, String> mapResponse = new HashMap<>();
                mapResponse.put("message", "Success!");
                mapResponse.put("transId", chargeId);
                mapResponse.put("cardType", ValidCreditCard.getCardType(paymentEnt.cardNumber));
                mapResponse.put("authCode", "");
                mapResponse.put("cardMask", "XXXXXXXXXXXX" + paymentEnt.cardNumber.substring(paymentEnt.cardNumber.length() - 4));
                payResponse.response = mapResponse;
                payResponse.returnCode = 200;
            } else {
                Map<String,String> mapResponse = new HashMap<>();
                String message = response.get("msg") != null ? response.get("msg").toString() : "";
                mapResponse.put("message", message);
                if (message.toLowerCase().contains("declined")) {
                    payResponse.returnCode = 436;
                }else
                    payResponse.returnCode = getErrorCode(status);
                payResponse.response = mapResponse;
            }
            return payResponse.toString();
        } catch(Exception ex) {
            return GatewayUtil.returnError(ex, 437);
        }
    }

    private String getError(Exception ex) {
        return GatewayUtil.getError(ex);
    }

    private int getErrorCode(int code) {
        int returnCode;
        switch (code) {
            case 0: //error
                returnCode = 437;
                break;
            default:
                returnCode = 437;
                break;

        }
        return returnCode;
    }

    private static String encode(String key, String data) throws Exception {
        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
        SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
        sha256_HMAC.init(secret_key);

        return Hex.encodeHexString(sha256_HMAC.doFinal(data.getBytes("UTF-8")));
    }

    private static String hmacDigest(String msg, String keyString) {
        String digest = null;
        try {
            SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), "HmacSHA256");
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(key);

            byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));

            StringBuilder hash = new StringBuilder();
            for (byte aByte : bytes) {
                String hex = Integer.toHexString(0xFF & aByte);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (Exception ex) {
            logger.debug(CommonUtils.getError(ex));
        }
        return digest;
    }
}
