
package com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransactionModificationResult" type="{http://schemas.firstatlanticcommerce.com/gateway/data}TransactionModificationResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transactionModificationResult"
})
@XmlRootElement(name = "TransactionModificationResponse")
public class TransactionModificationResponse {

    @XmlElement(name = "TransactionModificationResult", nillable = true)
    protected com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.TransactionModificationResponse transactionModificationResult;

    /**
     * Gets the value of the transactionModificationResult property.
     * 
     * @return
     *     possible object is
     *     {@link com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.TransactionModificationResponse }
     *     
     */
    public com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.TransactionModificationResponse getTransactionModificationResult() {
        return transactionModificationResult;
    }

    /**
     * Sets the value of the transactionModificationResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.TransactionModificationResponse }
     *     
     */
    public void setTransactionModificationResult(com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.TransactionModificationResponse value) {
        this.transactionModificationResult = value;
    }

}
