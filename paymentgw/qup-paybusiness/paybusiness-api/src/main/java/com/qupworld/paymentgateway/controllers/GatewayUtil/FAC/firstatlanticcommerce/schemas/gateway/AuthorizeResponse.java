
package com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AuthorizeResult" type="{http://schemas.firstatlanticcommerce.com/gateway/data}AuthorizeResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "authorizeResult"
})
@XmlRootElement(name = "AuthorizeResponse")
public class AuthorizeResponse {

    @XmlElement(name = "AuthorizeResult", nillable = true)
    protected com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.AuthorizeResponse authorizeResult;

    /**
     * Gets the value of the authorizeResult property.
     * 
     * @return
     *     possible object is
     *     {@link com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.AuthorizeResponse }
     *     
     */
    public com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.AuthorizeResponse getAuthorizeResult() {
        return authorizeResult;
    }

    /**
     * Sets the value of the authorizeResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.AuthorizeResponse }
     *     
     */
    public void setAuthorizeResult(com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.AuthorizeResponse value) {
        this.authorizeResult = value;
    }

}
