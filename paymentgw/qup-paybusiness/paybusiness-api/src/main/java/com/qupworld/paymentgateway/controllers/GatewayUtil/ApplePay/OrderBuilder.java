package com.qupworld.paymentgateway.controllers.GatewayUtil.ApplePay;

import com.alibaba.fastjson15.JSONObject;
import com.qupworld.paymentgateway.entities.ApplePay.AppleToken;
import com.upay.sdk.CipherWrapper;
import com.upay.sdk.ConfigurationUtils;
import com.upay.sdk.RSAUtils;
import com.upay.sdk.SignUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by thuanho on 20/05/2022.
 */
public class OrderBuilder extends BuilderSupport {

    private String merchantId;

    private String orderAmount;

    private String orderCurrency;

    private String requestId;

    private String notifyUrl;

    private String callbackUrl;

    private String remark;

    private String paymentModeCode;

    private String authCode;

    private String openId;

    private Payer payer;

    private String cashierVersion;

    private String forUse;

    private String merchantUserId;

    private String bindCardId;

    private String clientIp;

    private String timeout;

    private String splitMark;

    private String splitRule;

    private String partnerId;

    private List<ProductDetail> productDetails = new LinkedList<>();

    private String isBackUsersSign;

    private String appId;

    private String apCardNo;

    private String apUserData;

    private String apTransData;

    private String reportSerialNo;

    private String productInfo;

    private String projectId;

    private String hmac;

    /*private AppleToken paymentToken;*/

    private String paymentToken;

    public OrderBuilder setCashierVersion(String cashierVersion) {
        this.cashierVersion = cashierVersion;
        return this;
    }

    public OrderBuilder setForUse(String forUse) {
        this.forUse = forUse;
        return this;
    }

    public OrderBuilder setMerchantUserId(String merchantUserId) {
        this.merchantUserId = merchantUserId;
        return this;
    }

    public OrderBuilder setBindCardId(String bindCardId) {
        this.bindCardId = bindCardId;
        return this;
    }

    public OrderBuilder(String merchantId) {
        this.setMerchantId(merchantId);
    }

    public OrderBuilder setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
        return this;
    }

    public OrderBuilder setOrderCurrency(String orderCurrency) {
        this.orderCurrency = orderCurrency;
        return this;
    }

    public OrderBuilder setRequestId(String requestId) {
        this.requestId = requestId;
        return this;
    }

    public OrderBuilder setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
        return this;
    }

    public OrderBuilder setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
        return this;
    }

    public OrderBuilder setRemark(String remark) {
        this.remark = remark;
        return this;
    }

    public OrderBuilder setPaymentModeCode(String paymentModeCode) {
        this.paymentModeCode = paymentModeCode;
        return this;
    }

    public OrderBuilder setAuthCode(String authCode) {
        this.authCode = authCode;
        return this;
    }

    public OrderBuilder setOpenId(String openId) {
        this.openId = openId;
        return this;
    }

    public OrderBuilder setPayer(Payer payer) {
        this.payer = payer;
        return this;
    }

    public OrderBuilder addProductDetail(ProductDetail productDetail) {
        this.productDetails.add(productDetail);
        return this;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public void setSplitMark(String splitMark) {
        this.splitMark = splitMark;
    }

    public void setSplitRule(String splitRule) {
        this.splitRule = splitRule;
    }

    public void setIsBackUsersSign(String isBackUsersSign) {
        this.isBackUsersSign = isBackUsersSign;
    }

    public String getPartnerId() {
        return this.partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getAppId() {
        return this.appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    private String generateHmac() {
        StringBuilder hmacSource = new StringBuilder();
        hmacSource.append(StringUtils.defaultString(this.getMerchantId()))
                .append((String) ObjectUtils.defaultIfNull(this.getOrderAmount(), ""))
                .append(StringUtils.defaultString(this.getOrderCurrency(), ""))
                .append(StringUtils.defaultString(this.getRequestId(), ""))
                .append(StringUtils.defaultString(this.getNotifyUrl(), ""))
                .append(StringUtils.defaultString(this.getCallbackUrl(), ""))
                .append(StringUtils.defaultString(this.remark, ""))
                .append(StringUtils.defaultString(this.paymentModeCode, ""));
        if (this.productDetails != null)
            for (ProductDetail productDetail : this.productDetails)
                hmacSource.append(StringUtils.defaultString(productDetail.getName()))
                        .append(ObjectUtils.defaultIfNull(productDetail.getQuantity(), ""))
                        .append(ObjectUtils.defaultIfNull(productDetail.getAmount(), ""))
                        .append(StringUtils.defaultString(productDetail.getReceiver()))
                        .append(StringUtils.defaultString(productDetail.getDescription()));
        if (this.getPayer() != null)
            hmacSource.append(StringUtils.defaultString(this.getPayer().getName()))
                    .append(StringUtils.defaultString(this.getPayer().getPhoneNum()))
                    .append(StringUtils.defaultString(this.getPayer().getIdNum()))
                    .append(StringUtils.defaultString(this.getPayer().getBankCardNum()))
                    .append(StringUtils.defaultString(this.getPayer().getEmail()));
        hmacSource.append(StringUtils.defaultString(this.cashierVersion, ""));
        hmacSource.append(StringUtils.defaultString(this.forUse, ""));
        hmacSource.append(StringUtils.defaultString(this.merchantUserId, ""));
        hmacSource.append(StringUtils.defaultString(this.bindCardId, ""));
        hmacSource.append(StringUtils.defaultString(this.getClientIp(), ""));
        hmacSource.append(StringUtils.defaultString(this.timeout, ""));
        hmacSource.append(StringUtils.defaultString(this.splitMark, ""));
        hmacSource.append(StringUtils.defaultString(this.splitRule, ""));
        hmacSource.append(StringUtils.defaultString(this.authCode, ""));
        hmacSource.append(StringUtils.defaultString(this.openId, ""));
        hmacSource.append(StringUtils.defaultString(this.isBackUsersSign, ""));
        hmacSource.append(StringUtils.defaultString(this.appId, ""));
        hmacSource.append(StringUtils.defaultString(this.partnerId, ""));
        System.out.println("hmacSource = " + hmacSource.toString());
        return SignUtils.signMd5(hmacSource.toString(), ConfigurationUtils.getHmacKey(StringUtils.isBlank(this.partnerId) ? this.getMerchantId() : this.partnerId));
    }

    public JSONObject encryptBuild() {
        JSONObject json = assembleBuild();
        json.put("hmac", orderGenerateHmac());
        if (StringUtils.isNotBlank(this.partnerId))
            return CipherWrapper.bothEncryptWrap(this.getMerchantId(), this.partnerId, json);
        return CipherWrapper.encryptWrap(this.getMerchantId(), json);
    }

    protected String orderGenerateHmac() {
        return SignUtils.signMd5(super.orderGenerateHmac(), ConfigurationUtils.getHmacKey(StringUtils.isBlank(this.partnerId) ? this.getMerchantId() : this.partnerId));
    }

    public JSONObject bothEncryptBuild() {
        JSONObject json = assembleBuild();
        try {
            json.put("hmac", bothOrderGenerateHmac());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (StringUtils.isNotBlank(this.partnerId))
            return CipherWrapper.bothEncryptWrap(this.getMerchantId(), this.partnerId, json);
        return CipherWrapper.bothEncryptWrap(this.getMerchantId(), json);
    }

    protected byte[] bothOrderGenerateHmac() throws Exception {
        return RSAUtils.encryptSHA(super.orderGenerateHmac().getBytes("UTF-8"));
    }

    public void setReportSerialNo(String reportSerialNo) {
        this.reportSerialNo = reportSerialNo;
    }

    public String getProductInfo() {
        return this.productInfo;
    }

    public void setProductInfo(String productInfo) {
        this.productInfo = productInfo;
    }

    public String getProjectId() {
        return this.projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public String getOrderCurrency() {
        return orderCurrency;
    }

    public String getRequestId() {
        return requestId;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public Payer getPayer() {
        return payer;
    }

    public String getClientIp() {
        return clientIp;
    }

    public String getHmac() {
        return hmac;
    }

    public void setHmac(String hmac) {
        this.hmac = hmac;
    }

    public String getPaymentModeCode() {
        return this.paymentModeCode;
    }

    public String getPaymentToken() {
        return paymentToken;
    }

    public void setPaymentToken(String paymentToken) {
        this.paymentToken = paymentToken;
    }

    public String getApCardNo() {
        return apCardNo;
    }

    public void setApCardNo(String apCardNo) {
        this.apCardNo = apCardNo;
    }

    public String getApUserData() {
        return apUserData;
    }

    public void setApUserData(String apUserData) {
        this.apUserData = apUserData;
    }

    public String getApTransData() {
        return apTransData;
    }

    public void setApTransData(String apTransData) {
        this.apTransData = apTransData;
    }

    /*public AppleToken getPaymentToken() {
        return paymentToken;
    }

    public void setPaymentToken(AppleToken paymentToken) {
        this.paymentToken = paymentToken;
    }*/
}
