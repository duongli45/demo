package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.entities.CreditEnt;
import com.qupworld.paymentgateway.entities.PaymentEnt;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import io.conekta.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class ConektaUtil {

    private static final Logger logger = LogManager.getLogger(ConektaUtil.class);
    private String requestId;
    private Gson gson;
    public ConektaUtil(String _requestId) {
        requestId = _requestId;
        gson = new Gson();
    }

    public static void main(String[] args) throws Exception{
        /*JSONObject customerJSON = new JSONObject();
        customerJSON.put("name", "test name");
        customerJSON.put("email", "mail@gmail.com");

        JSONArray paymentArray = new JSONArray();
        JSONObject paymentJSON = new JSONObject();
        paymentJSON.put("name", "card name");
        paymentJSON.put("number", "4111111111111111");
        paymentJSON.put("cvc", "123");        
        paymentJSON.put("exp_month", "01");
        paymentJSON.put("exp_year", "2020");
        paymentJSON.put("type", "card");
        paymentArray.put(paymentJSON);
        customerJSON.put("payment_sources", paymentArray);

        Conekta.setApiKey("key_nRscUMMEFzMgyrsP3PG9qw");
        Customer customer = Customer.create(customerJSON);
        logger.debug("customer: " + customer);*/


        /*JSONObject orderJSON = new JSONObject();
        orderJSON.put("currency", "MXN");

        JSONObject customerJSON = new JSONObject();
        customerJSON.put("customer_id", "cus_2iBFuYjokGS4kdMXb");
        *//*JSONObject fraudCustomerJSON = new JSONObject();
        fraudCustomerJSON.put("paid_transactions", 1);
        customerJSON.put("antifraud_info" , fraudCustomerJSON);*//*
        orderJSON.put("customer_info", customerJSON);

        JSONArray itemArray = new JSONArray();
        JSONObject itemJSON = new JSONObject();
        itemJSON.put("name", "bookId1");
        itemJSON.put("unit_price", 2100);
        itemJSON.put("quantity", 1);
        JSONObject fraudItemJSON = new JSONObject();
        fraudItemJSON.put("trip_id", "bookId1");
        fraudItemJSON.put("driver_id", "driverId1");
        fraudItemJSON.put("ticket_class", "transport");
        fraudItemJSON.put("pickup_latlon", "23.4323456,-123.1234567");
        fraudItemJSON.put("dropoff_latlon", "23.4323456,-123.1234567");
        itemJSON.put("antifraud_info" , fraudItemJSON);
        itemArray.put(itemJSON);
        orderJSON.put("line_items", itemArray);

        JSONArray chargeArray = new JSONArray();
        JSONObject chargeJSON = new JSONObject();
        JSONObject paymentMethodJSON = new JSONObject();
        paymentMethodJSON.put("type", "default");
        chargeJSON.put("payment_method", paymentMethodJSON);
        chargeArray.put(chargeJSON);
        orderJSON.put("charges", chargeArray);
        logger.debug("orderJSON: " + orderJSON.toString());

        Conekta.setApiKey("key_nRscUMMEFzMgyrsP3PG9qw");
        try {
            Order order = Order.create(orderJSON);
            String orderId = order.getId();
            logger.debug("order: " + order);
            logger.debug("orderId: " + orderId);
        } catch (ErrorList ex) {
            ex.printStackTrace();
            Gson gson = new Gson();
            logger.debug("error: " + gson.toJson(ex));
            logger.debug(" ");
            logger.debug("==== error message: " + ex.details.get(0).debug_message);

        }*/

        JSONObject orderJSON = new JSONObject();
        orderJSON.put("currency", "MXN");

        JSONObject customerJSON = new JSONObject();
        customerJSON.put("name", "customer name");
        customerJSON.put("phone", "+5213353319758");
        customerJSON.put("email", "mail@gmail.com");
        /*JSONObject fraudCustomerJSON = new JSONObject();
        fraudCustomerJSON.put("paid_transactions", 1);
        customerJSON.put("antifraud_info" , fraudCustomerJSON);*/
        orderJSON.put("customer_info", customerJSON);

        JSONArray itemArray = new JSONArray();
        JSONObject itemJSON = new JSONObject();
        itemJSON.put("name", "bookId1");
        itemJSON.put("unit_price", 120);
        itemJSON.put("quantity", 1);
        JSONObject fraudItemJSON = new JSONObject();
        fraudItemJSON.put("trip_id", "bookId1");
        fraudItemJSON.put("driver_id", "driverId1");
        fraudItemJSON.put("ticket_class", "transport");
        fraudItemJSON.put("pickup_latlon", "23.4323456,-123.1234567");
        fraudItemJSON.put("dropoff_latlon", "23.4323456,-123.1234567");
        itemJSON.put("antifraud_info" , fraudItemJSON);
        itemArray.put(itemJSON);
        orderJSON.put("line_items", itemArray);

        JSONArray chargeArray = new JSONArray();
        JSONObject chargeJSON = new JSONObject();
        JSONObject paymentMethodJSON = new JSONObject();
        paymentMethodJSON.put("type", "card");
        paymentMethodJSON.put("name", "card name");
        paymentMethodJSON.put("number", "4111111111111111");
        paymentMethodJSON.put("cvc", "123");
        paymentMethodJSON.put("exp_month", "01");
        paymentMethodJSON.put("exp_year", "2020");
        chargeJSON.put("payment_method", paymentMethodJSON);
        chargeArray.put(chargeJSON);
        orderJSON.put("charges", chargeArray);
        logger.debug("orderJSON: " + orderJSON.toString());

        Conekta.setApiKey("key_nRscUMMEFzMgyrsP3PG9qw");
        try {
            Order order = Order.create(orderJSON);
            String orderId = order.getId();
            logger.debug("order: " + order);
            logger.debug("orderId: " + orderId);
        } catch (ErrorList ex) {
            ex.printStackTrace();
            Gson gson = new Gson();
            logger.debug("error: " + gson.toJson(ex));
            logger.debug(" ");
            logger.debug("==== error message: " + ex.details.get(0).debug_message);

        }


    }

    public String initiateCreditForm(String apiKey) {
        ObjResponse objResponse = new ObjResponse();
        Map<String,String> mapResponse = new HashMap<>();

        String htmlForm = "";
        try {
            String actionUrl = KeysUtil.CALLBACK_URL + KeysUtil.NOTIFICATION_URL;

            htmlForm = GatewayUtil.getFile(requestId, "conekta.html");
            htmlForm = htmlForm.replace("action_page", actionUrl);
            htmlForm = htmlForm.replace("apiKey", apiKey);
            htmlForm = htmlForm.replace("\"","'").replace("\n","").replace("\r", "");
            logger.debug(requestId + " - htmlForm :" + htmlForm);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        objResponse.returnCode = 200;
        mapResponse.put("3ds_url", htmlForm);
        mapResponse.put("type", "form");
        objResponse.response = mapResponse;
        return objResponse.toString();
    }

    public String createCreditToken(String apiKey, String secretKey, CreditEnt credit, String fullName, String email, String currencyISO) throws IOException {
        ObjResponse response = new ObjResponse();
        try {
            String token_id = "";
            JSONObject tokenJSON = new JSONObject();
            tokenJSON.put("key", apiKey);

            String[] date = credit.expiredDate.split("/");
            JSONObject cardJSON = new JSONObject();
            cardJSON.put("name", credit.cardHolder);
            cardJSON.put("number", credit.cardNumber);
            cardJSON.put("cvc", credit.cvv);
            cardJSON.put("exp_month",date[0]);
            cardJSON.put("exp_year", date[1]);
            tokenJSON.put("cardData", cardJSON);

            String browserboxURL = ServerConfig.webview_url + "/browserbox/conekta";
            logger.debug(requestId + " - browserboxURL: " + browserboxURL);
            String data = tokenJSON.toString();
            HttpResponse<String> stringResponse = null;
            try {
                stringResponse = Unirest.put(browserboxURL)
                        //.basicAuth(ServerConfig.logs_username, ServerConfig.logs_password)
                        .header("Content-Type", "application/json; charset=utf-8")
                        .body(data)
                        .asString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                // try to get data again after 5 seconds
                try {
                    Thread.sleep(5000);
                    stringResponse = null;
                    stringResponse = Unirest.put(browserboxURL)
                            //.basicAuth(ServerConfig.logs_username, ServerConfig.logs_password)
                            .header("Content-Type", "application/json; charset=utf-8")
                            .body(data)
                            .asString();
                } catch(Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (stringResponse != null) {
                String result = stringResponse.getBody().replace("\\","").replace("\"{","{").replace("}\"","}");
                logger.debug(requestId + " - result: " + result);
                org.json.simple.JSONObject objResult = (org.json.simple.JSONObject)new JSONParser().parse(result);
                org.json.simple.JSONObject tokenResponse = (org.json.simple.JSONObject)objResult.get("result");
                String object = tokenResponse.get("object") != null ? tokenResponse.get("object").toString() : "";
                if (object.equals("token")) {
                    token_id = tokenResponse.get("id") != null ? tokenResponse.get("id").toString() : "";
                } else if (object.equals("error")) {
                    String message_to_purchaser = tokenResponse.get("message_to_purchaser") != null ? tokenResponse.get("message_to_purchaser").toString() : "";
                    if (!message_to_purchaser.isEmpty()) {
                        Map<String, String> mapResponse = new HashMap<>();
                        mapResponse.put("message", message_to_purchaser);
                        response.response = mapResponse;
                    }
                    response.returnCode = 437;
                    return response.toString();
                }
            } else {
                logger.debug(requestId + " - stringResponse: null");
            }
            logger.debug(requestId + " - token_id: " + token_id);

            JSONObject customerJSON = new JSONObject();
            customerJSON.put("name", fullName);
            customerJSON.put("email", email);

            JSONArray paymentArray = new JSONArray();
            JSONObject paymentJSON = new JSONObject();
            paymentJSON.put("type", "card");
            paymentJSON.put("token_id", token_id);
            paymentArray.put(paymentJSON);
            customerJSON.put("payment_sources", paymentArray);

            String customerLog = customerJSON.toString().replace(credit.cardNumber,"XXXX" + credit.cardNumber.substring(credit.cardNumber.length() - 4));
            logger.debug(requestId + " - customerJSON: " + customerLog);
            Conekta.setApiKey(secretKey);
            Customer customer = Customer.create(customerJSON);
            logger.debug(requestId + " - customer: " + customer);

            PaymentSource paymentSource = (PaymentSource)customer.payment_sources.get(0);
            String token = customer.getId();
            Map<String,String> mapResponse = new HashMap<>();
            mapResponse.put("message", "success");
            /*mapResponse.put("creditCard", "XXXXXXXXXXXX" + paymentSource.last4);*/
            mapResponse.put("creditCard", "XXXXXXXXXXXX");
            mapResponse.put("qrCode", "");
            mapResponse.put("token", token);
            /*mapResponse.put("cardType", formatCardType(paymentSource.brand));*/
            mapResponse.put("cardType", "");

            response.response = mapResponse;
            response.returnCode = 200;
            return  response.toString();
        } catch (ErrorList ex) {
            ex.printStackTrace();
            response.returnCode = 437;
            if (ex.details!= null && !ex.details.isEmpty()) {
                Map<String,String> mapResult = new HashMap<>();
                mapResult.put("message", ex.details.get(0).message + " - " + ex.details.get(0).debug_message);
                response.response = mapResult;
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }

        return response.toString();
    }

    public String createCreditToken(String apiKey, String cardToken, String fullName, String email) throws IOException {
        ObjResponse response = new ObjResponse();
        try {
            JSONObject customerJSON = new JSONObject();
            customerJSON.put("name", fullName);
            customerJSON.put("email", email);

            JSONArray paymentArray = new JSONArray();
            JSONObject paymentJSON = new JSONObject();
            paymentJSON.put("type", "card");
            paymentJSON.put("token_id", cardToken);
            paymentArray.put(paymentJSON);
            customerJSON.put("payment_sources", paymentArray);
            /*JSONObject fraudCustomerJSON = new JSONObject();
            Calendar calendar = Calendar.getInstance();
            logger.debug("account_created_at = " + KeysUtil.SDF_DMYHMS.format(calendar.getTime()));
            fraudCustomerJSON.put("account_created_at", calendar.getTimeInMillis());
            calendar.add(Calendar.DATE, 1);
            logger.debug("first_paid_at = " + KeysUtil.SDF_DMYHMS.format(calendar.getTime()));
            fraudCustomerJSON.put("first_paid_at", calendar.getTimeInMillis());
            customerJSON.put("antifraud_info" , fraudCustomerJSON);*/

            logger.debug(requestId + " - customerJSON: " + customerJSON.toString());
            Conekta.setApiKey(apiKey);
            Customer customer = Customer.create(customerJSON);
            logger.debug(requestId + " - customer: " + gson.toJson(customer));

            PaymentSource paymentSource = (PaymentSource)customer.payment_sources.get(0);
            String token = customer.getId();
            Map<String,String> mapResponse = new HashMap<>();
            mapResponse.put("message", "success");
            /*mapResponse.put("creditCard", "XXXXXXXXXXXX" + paymentSource.last4);*/
            mapResponse.put("creditCard", "XXXXXXXXXXXX");
            mapResponse.put("qrCode", "");
            mapResponse.put("token", token);
            /*mapResponse.put("cardType", formatCardType(paymentSource.brand));*/
            mapResponse.put("cardType", "");

            response.response = mapResponse;
            response.returnCode = 200;
            return  response.toString();
        } catch (ErrorList ex) {
            ex.printStackTrace();
            response.returnCode = 437;
            if (ex.details!= null && !ex.details.isEmpty()) {
                Map<String,String> mapResult = new HashMap<>();
                mapResult.put("message", ex.details.get(0).message + " - " + ex.details.get(0).debug_message);
                response.response = mapResult;
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }

        return response.toString();
    }

    public String createPaymentWithCreditToken(String requestId, double amount, String token, Map<String,String> bookingData, Map<String,String> addressData, String apiKey, String currencyISO){
        logger.debug(requestId + " - createPaymentWithCreditToken " + token + " ...");
        DecimalFormat df = new DecimalFormat("#");
        int intAmount = Integer.parseInt(df.format(amount*100));

        ObjResponse response = new ObjResponse();
        String transId = "";
        JSONObject orderJSON = new JSONObject();
        orderJSON.put("currency", currencyISO);

        JSONObject customerJSON = new JSONObject();
        customerJSON.put("customer_id", token);
        /*JSONObject fraudCustomerJSON = new JSONObject();
        fraudCustomerJSON.put("paid_transactions", 1);
        customerJSON.put("antifraud_info" , fraudCustomerJSON);*/
        orderJSON.put("customer_info", customerJSON);

        JSONArray itemArray = new JSONArray();
        JSONObject itemJSON = new JSONObject();
        itemJSON.put("name", bookingData.get("bookId"));
        itemJSON.put("unit_price", intAmount);
        itemJSON.put("quantity", 1);
        JSONObject fraudItemJSON = new JSONObject();
        fraudItemJSON.put("trip_id", bookingData.get("bookId"));
        fraudItemJSON.put("driver_id", bookingData.get("driverId"));
        fraudItemJSON.put("ticket_class", "transport");
        fraudItemJSON.put("pickup_latlon", bookingData.get("pickup"));
        fraudItemJSON.put("dropoff_latlon", bookingData.get("dropoff"));
        itemJSON.put("antifraud_info" , fraudItemJSON);
        itemArray.put(itemJSON);
        orderJSON.put("line_items", itemArray);

        JSONArray chargeArray = new JSONArray();
        JSONObject chargeJSON = new JSONObject();
        JSONObject paymentMethodJSON = new JSONObject();
        paymentMethodJSON.put("type", "default");
        chargeJSON.put("payment_method", paymentMethodJSON);
        chargeArray.put(chargeJSON);
        orderJSON.put("charges", chargeArray);

        JSONObject addressJSON = new JSONObject();
        String street1 = addressData.get("street1") != null ? addressData.get("street1") : "";
        if (!street1.isEmpty())
            addressJSON.put("street1", street1);

        String city = addressData.get("city") != null ? addressData.get("city") : "";
        if (!city.isEmpty())
            addressJSON.put("city", city);

        String state = addressData.get("state") != null ? addressData.get("state") : "";
        if (!state.isEmpty())
            addressJSON.put("state", state);

        String postal_code = addressData.get("postal_code") != null ? addressData.get("postal_code") : "";
        if (!postal_code.isEmpty())
            addressJSON.put("postal_code", postal_code);

        String country = addressData.get("country") != null ? addressData.get("country") : "";
        if (!country.isEmpty())
            addressJSON.put("country", country);

        JSONObject shippingJSON = new JSONObject();
        shippingJSON.put("address", addressJSON);

        if (!street1.isEmpty()) {
            orderJSON.put("shipping_contact", shippingJSON);
        }

        logger.debug(requestId + " - orderJSON: " + orderJSON.toString());

        Conekta.setApiKey(apiKey);
        try {
            Order order = Order.create(orderJSON);
            logger.debug(requestId + " - order: " + gson.toJson(order));
            logger.debug(requestId + " - payment_status: " + order.payment_status);
            if (order.payment_status.equals("paid")) {
                Charge charge = (Charge)order.charges.get(0);
                transId = order.getId();
                ConektaObject paymentMethod = charge.payment_method;
                String cardMask = "XXXXXXXXXXXX" + paymentMethod.getVal("last4").toString();
                String cardType = paymentMethod.getVal("brand").toString();
                String authorizationCode = paymentMethod.getVal("auth_code").toString();

                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("message", "Success!");
                mapResponse.put("transId", transId);
                mapResponse.put("cardType", formatCardType(cardType));
                mapResponse.put("authCode", authorizationCode);
                mapResponse.put("cardMask", cardMask);

                response.response = mapResponse;
                response.returnCode = 200;
                return response.toString();
            } else {
                response.returnCode = 437;
                return response.toString();
            }
        } catch (ErrorList ex) {
            ex.printStackTrace();
            response.returnCode = 437;
            if (ex.details!= null && !ex.details.isEmpty()) {
                Map<String,String> mapResult = new HashMap<>();
                mapResult.put("message", ex.details.get(0).message + " - " + ex.details.get(0).debug_message);
                response.response = mapResult;
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }

        return response.toString();

    }

    public String preAuthPayment(String requestId, double amount, String token, Map<String,String> bookingData, Map<String,String> addressData, String apiKey, String currencyISO) {
        logger.debug(requestId + " PRE-AUTH PAYMENT for bookId:" + bookingData.get("bookId") + " - token: " + token + " ...");
        DecimalFormat df = new DecimalFormat("#");
        int intAmount = Integer.parseInt(df.format(amount*100));

        ObjResponse response = new ObjResponse();
        String transId = "";
        JSONObject orderJSON = new JSONObject();
        orderJSON.put("currency", currencyISO);
        orderJSON.put("pre_authorize", true);

        JSONObject customerJSON = new JSONObject();
        customerJSON.put("customer_id", token);
        /*JSONObject fraudCustomerJSON = new JSONObject();
        fraudCustomerJSON.put("paid_transactions", 1);
        customerJSON.put("antifraud_info" , fraudCustomerJSON);*/
        orderJSON.put("customer_info", customerJSON);

        JSONArray itemArray = new JSONArray();
        JSONObject itemJSON = new JSONObject();
        itemJSON.put("name", bookingData.get("bookId"));
        itemJSON.put("unit_price", intAmount);
        itemJSON.put("quantity", 1);
        JSONObject fraudItemJSON = new JSONObject();
        fraudItemJSON.put("trip_id", bookingData.get("bookId"));
        fraudItemJSON.put("driver_id", bookingData.get("driverId"));
        fraudItemJSON.put("ticket_class", "transport");
        fraudItemJSON.put("pickup_latlon", bookingData.get("pickup"));
        fraudItemJSON.put("dropoff_latlon", bookingData.get("dropoff"));
        itemJSON.put("antifraud_info" , fraudItemJSON);
        itemArray.put(itemJSON);
        orderJSON.put("line_items", itemArray);

        JSONArray chargeArray = new JSONArray();
        JSONObject chargeJSON = new JSONObject();
        JSONObject paymentMethodJSON = new JSONObject();
        paymentMethodJSON.put("type", "default");
        chargeJSON.put("payment_method", paymentMethodJSON);
        chargeArray.put(chargeJSON);
        orderJSON.put("charges", chargeArray);

        JSONObject addressJSON = new JSONObject();
        String street1 = addressData.get("street1") != null ? addressData.get("street1") : "";
        if (!street1.isEmpty())
            addressJSON.put("street1", street1);

        String city = addressData.get("city") != null ? addressData.get("city") : "";
        if (!city.isEmpty())
            addressJSON.put("city", city);

        String state = addressData.get("state") != null ? addressData.get("state") : "";
        if (!state.isEmpty())
            addressJSON.put("state", state);

        String postal_code = addressData.get("postal_code") != null ? addressData.get("postal_code") : "";
        if (!postal_code.isEmpty())
            addressJSON.put("postal_code", postal_code);

        String country = addressData.get("country") != null ? addressData.get("country") : "";
        if (!country.isEmpty())
            addressJSON.put("country", country);

        JSONObject shippingJSON = new JSONObject();
        shippingJSON.put("address", addressJSON);

        if (!street1.isEmpty()) {
            orderJSON.put("shipping_contact", shippingJSON);
        }

        logger.debug(requestId + " - orderJSON: " + orderJSON.toString());

        Conekta.setApiKey(apiKey);
        try {
            Order order = Order.create(orderJSON);
            logger.debug(requestId + " - order: " + gson.toJson(order));
            logger.debug(requestId + " - payment_status: " + order.payment_status);
            if (order.payment_status.equals("pre_authorized")) {
                Charge charge = (Charge)order.charges.get(0);
                transId = order.getId();
                ConektaObject paymentMethod = charge.payment_method;
                String authorizationCode = paymentMethod.getVal("auth_code").toString();

                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("authId", transId);
                mapResponse.put("authAmount", String.valueOf(amount));
                mapResponse.put("authCode", authorizationCode);
                mapResponse.put("authToken", token);
                mapResponse.put("allowCapture", "true");

                response.response = mapResponse;
                response.returnCode = 200;
                return response.toString();
            } else {
                response.returnCode = 437;
                return response.toString();
            }
        } catch (ErrorList ex) {
            ex.printStackTrace();
            response.returnCode = 437;
            if (ex.details!= null && !ex.details.isEmpty()) {
                Map<String,String> mapResult = new HashMap<>();
                mapResult.put("message", ex.details.get(0).message + " - " + ex.details.get(0).debug_message);
                response.response = mapResult;
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }

        return response.toString();

    }

    public void voidPreauthTransaction(String requestId, String bookId, double amount, String authId, String apiKey) {
        logger.debug(requestId + " - voidPreauthTransaction for bookId " + bookId + " ...");
        Conekta.setApiKey(apiKey);
        try {
            /*DecimalFormat df = new DecimalFormat("#");
            int intAmount = Integer.parseInt(df.format(amount*100));

            JSONObject validRefund = new JSONObject();
            validRefund.put("amount", intAmount);
            validRefund.put("reason", "Refund authorized transaction");

            Order refund = Order.find(authId);
            refund.refund(validRefund);

            Thread.sleep(1000);

            Order order = Order.find(authId);
            logger.debug(requestId + " - order: " + order);
            logger.debug(requestId + " - payment_status: " + order.payment_status);*/

            String url = "https://api.conekta.io/orders/"+ authId +"/void";
            com.mashape.unirest.http.HttpResponse<String> json = Unirest.post(url)
                    .header("Accept", "application/vnd.conekta-v2.0.0+json")
                    .header("Content-Type", "application/json; charset=utf-8")
                    .basicAuth(apiKey,"")
                    .header("user", apiKey)
                    .asString();
            logger.debug(requestId + " - voidPreauthTransaction response: " + json.getBody().toString());
            try {
                Gson gson = new Gson();
                Order order = gson.fromJson(json.getBody().toString(), Order.class);
                logger.debug(requestId + " - payment_status: " + order.payment_status);
            } catch (Exception ignore) {}

        } catch(Exception ex) {
            ex.printStackTrace();
            logger.debug(requestId + " - message: " + GatewayUtil.getError(ex));
        }

    }

    public String preAuthCapture(String requestId, String bookId, String authId, String apiKey){
        logger.debug(requestId + " - PRE-AUTH CAPTURE for bookId:" + bookId + " ...");
        ObjResponse preAuthResponse = new ObjResponse();

        Conekta.setApiKey(apiKey);
        try {
            Order capture = Order.find(authId);
            capture.capture();
            Thread.sleep(1000);

            Order order = Order.find(authId);
            logger.debug(requestId + " - order: " + order);
            logger.debug(requestId + " - payment_status: " + order.payment_status);
            if (order.payment_status.equals("paid")) {
                Charge charge = (Charge)order.charges.get(0);
                String transId = order.getId();
                ConektaObject paymentMethod = charge.payment_method;
                String cardMask = "XXXXXXXXXXXX" + paymentMethod.getVal("last4").toString();
                String cardType = paymentMethod.getVal("brand").toString();
                String authorizationCode = paymentMethod.getVal("auth_code").toString();

                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("message", "Success!");
                mapResponse.put("transId", transId);
                mapResponse.put("cardType", formatCardType(cardType));
                mapResponse.put("authCode", authorizationCode);
                mapResponse.put("cardMask", cardMask);

                preAuthResponse.response = mapResponse;
                preAuthResponse.returnCode = 200;
                return preAuthResponse.toString();
            } else {
                preAuthResponse.returnCode = 437;
                return preAuthResponse.toString();
            }
        } catch (ErrorList ex) {
            ex.printStackTrace();
            preAuthResponse.returnCode = 437;
            if (ex.details!= null && !ex.details.isEmpty()) {
                Map<String,String> mapResult = new HashMap<>();
                mapResult.put("message", ex.details.get(0).message + " - " + ex.details.get(0).debug_message);
                preAuthResponse.response = mapResult;
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }

        return preAuthResponse.toString();
    }

    public String paymentWithInputCard(PaymentEnt paymentEnt, double amount, Map<String,String> bookingData, String apiKey, String currencyISO){
        logger.debug(requestId+ " - paymentWithInputCard ...");
        DecimalFormat df = new DecimalFormat("#");
        int intAmount = Integer.parseInt(df.format(amount*100));

        ObjResponse response = new ObjResponse();
        String transId = "";
        JSONObject orderJSON = new JSONObject();
        orderJSON.put("currency", currencyISO);

        JSONObject customerJSON = new JSONObject();
        customerJSON.put("name", bookingData.get("customerName"));
        customerJSON.put("phone", bookingData.get("customerPhone"));
        customerJSON.put("email", bookingData.get("customerEmail"));
        /*JSONObject fraudCustomerJSON = new JSONObject();
        fraudCustomerJSON.put("paid_transactions", 1);
        customerJSON.put("antifraud_info" , fraudCustomerJSON);*/
        orderJSON.put("customer_info", customerJSON);

        JSONArray itemArray = new JSONArray();
        JSONObject itemJSON = new JSONObject();
        itemJSON.put("name", bookingData.get("bookId"));
        itemJSON.put("unit_price", intAmount);
        itemJSON.put("quantity", 1);
        JSONObject fraudItemJSON = new JSONObject();
        fraudItemJSON.put("trip_id", bookingData.get("bookId"));
        fraudItemJSON.put("driver_id", bookingData.get("driverId"));
        fraudItemJSON.put("ticket_class", "transport");
        fraudItemJSON.put("pickup_latlon", bookingData.get("pickup"));
        fraudItemJSON.put("dropoff_latlon", bookingData.get("dropoff"));
        itemJSON.put("antifraud_info" , fraudItemJSON);
        itemArray.put(itemJSON);
        orderJSON.put("line_items", itemArray);

        JSONArray chargeArray = new JSONArray();
        JSONObject chargeJSON = new JSONObject();
        JSONObject paymentMethodJSON = new JSONObject();
        paymentMethodJSON.put("type", "card");
        paymentMethodJSON.put("name", paymentEnt.cardHolder);
        paymentMethodJSON.put("number", paymentEnt.cardNumber);
        paymentMethodJSON.put("cvc", paymentEnt.cvv);
        String[] arrExp = paymentEnt.expiredDate.split("/");
        paymentMethodJSON.put("exp_month", arrExp[0]);
        paymentMethodJSON.put("exp_year", arrExp[1]);
        chargeJSON.put("payment_method", paymentMethodJSON);
        chargeArray.put(chargeJSON);
        orderJSON.put("charges", chargeArray);
        String orderLog = orderJSON.toString().replace(paymentEnt.cardNumber,"XXXX" + paymentEnt.cardNumber.substring(paymentEnt.cardNumber.length() - 4));
        logger.debug(requestId + " - orderJSON: " + orderLog);

        Conekta.setApiKey(apiKey);
        try {
            Order order = Order.create(orderJSON);
            logger.debug(requestId + " - order: " + order);
            logger.debug(requestId + " - payment_status: " + order.payment_status);
            if (order.payment_status.equals("paid")) {
                Charge charge = (Charge)order.charges.get(0);
                transId = order.getId();
                ConektaObject paymentMethod = charge.payment_method;
                String cardMask = "XXXXXXXXXXXX" + paymentMethod.getVal("last4").toString();
                String cardType = paymentMethod.getVal("brand").toString();
                String authorizationCode = paymentMethod.getVal("auth_code").toString();

                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("message", "Success!");
                mapResponse.put("transId", transId);
                mapResponse.put("cardType", formatCardType(cardType));
                mapResponse.put("authCode", authorizationCode);
                mapResponse.put("cardMask", cardMask);

                response.response = mapResponse;
                response.returnCode = 200;
                return response.toString();
            }
            String orderId = order.getId();
            logger.debug(requestId + " - order: " + order);
            logger.debug(requestId + " - orderId: " + orderId);
        } catch (ErrorList ex) {
            ex.printStackTrace();
            response.returnCode = 437;
            if (ex.details!= null && !ex.details.isEmpty()) {
                Map<String,String> mapResult = new HashMap<>();
                mapResult.put("message", ex.details.get(0).message + " - " + ex.details.get(0).debug_message);
                response.response = mapResult;
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            return GatewayUtil.returnError(ex, 437);
        }

        return response.toString();

    }

    public static String formatCardType(String cardType) {
        if (cardType.equalsIgnoreCase("VISA"))
            return "VISA";
        else if (cardType.equalsIgnoreCase("MC"))
            return "MASTERCARD";
        else if (cardType.equalsIgnoreCase("AMERICAN_EXPRESS"))
            return "AMEX";
        else if (cardType.equals("DISCOVER"))
            return "DISCOVER";

        return cardType;
    }
}
