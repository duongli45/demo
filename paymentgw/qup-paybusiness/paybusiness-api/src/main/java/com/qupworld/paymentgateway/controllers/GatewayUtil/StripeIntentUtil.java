package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.google.gson.Gson;
import com.qupworld.paymentgateway.models.RedisDao;
import com.qupworld.paymentgateway.models.RedisImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by qup on 8/14/19.
 */
@SuppressWarnings("unchecked")
public class StripeIntentUtil {
    Gson gson;
    RedisDao redisDao;
    final SimpleDateFormat sdfMongo = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
    final static Logger logger = LogManager.getLogger(StripeIntentUtil.class);

    public StripeIntentUtil(){
        gson = new Gson();
        redisDao = new RedisImpl();
    }
    public boolean addPaySCAData(String requestId,String bookId, String paymentInterId,String fleetId){
        Gson gson = new Gson();
        Map<String, String> data = new HashMap<String, String>();
        data.put("paymentInterId", paymentInterId);
        data.put("fleetId", fleetId);
        String json = gson.toJson(data);
        addTrackResultSCA(requestId,bookId);
        return redisDao.addPaySCAData(requestId, bookId,json);

    }

    public void removePaySCAData(String requestId,String bookId){
        redisDao.removePaySCAData(requestId, bookId);

    }
    public String getPaySCAData(String requestId, String bookId) {
        return redisDao.getPaySCAData(requestId, bookId);

    }
    public Boolean addTrackResultSCA(String requestId, String bookId) {
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-DD hh:mm:ss");
        String strDate = dateFormat.format(date);
        return redisDao.addTrackResultSCA(requestId, bookId, strDate);

    }
    public void removeTrackResultSCAByBookId(String requestId, String bookId){
        try {
            List<String> arrayPaymentIterId = this.getTrackResultSCA(requestId);
            arrayPaymentIterId.forEach((element) -> {
                String[] datas = element.split("-SCA-");
                if (datas.length == 2 && datas[0].equals(bookId)) {
                    this.removeTrackResultSCA("", datas[0], datas[1]);
                }
            });
        }catch (Exception ex){
            logger.info(ex);
        }
    }
    public void removeTrackResultSCA(String requestId,String bookId, String time) {
         redisDao.removeTrackResultSCA(requestId,bookId,time);

    }
    public List<String> getTrackResultSCA(String requestId) {
        return redisDao.getTrackResultSCA(requestId);

    }
    public Boolean addAuthenRequired(String bookId, String result) {
        return redisDao.addAuthData("", bookId, result);

    }
    public Boolean updateAuthData(String bookId, String result) {
        redisDao.updateAuthData("", "-cacheWebHook-"+bookId, result);
        return redisDao.updateAuthData("", bookId, result);

    }
    public void removeAuthenRequired(String bookId) {
         redisDao.removeAuthData("", bookId);
    }
    public String getAuthenRequired(String bookId) {
            return  redisDao.getAuthData("",bookId);
    }
    public void addStripeWebhook(String requestId, String pmId, String data){
        redisDao.addStripeWebhook(requestId,pmId,data);
    }
    public String getStripeWebhook(String requestId, String pmId){
        return redisDao.getStripeWebhook(requestId,pmId);
    }
}
