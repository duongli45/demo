package com.qupworld.paymentgateway.controllers;

import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.SQLDao;
import com.qupworld.paymentgateway.models.mongo.collections.account.UpdateBalanceId;
import com.qupworld.paymentgateway.models.mongo.collections.fleet.Fleet;
import com.pg.util.CommonArrayUtils;
import com.pg.util.CommonUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Created by qup on 15/08/2019.
 */
@SuppressWarnings("unchecked")
public class ThreadUpdateBalance extends Thread{

    public String fleetId;
    public Double amount;
    public String currencyISO;
    public String currencySymbol;
    public String reason;
    public String requestId;
    public Fleet fleet;
    public MongoDao mongoDao;
    public SQLDao sqlDao;

    final static Logger logger = LogManager.getLogger(ThreadUpdateBalance.class);
    @Override
    public void run() {
        try {
            String key = fleetId + "-" + currencyISO;
            List<UpdateBalanceId> listIds = mongoDao.getIdUpdateBalance(key);
            int i = 0;
            for (UpdateBalanceId updateBalanceId : listIds) {
                if (i%200 == 0) {
                    Thread.sleep(2000);
                }
                UpdateDriverCreditBalance updateBalance = new UpdateDriverCreditBalance();
                updateBalance.type = CommonArrayUtils.DRIVER_WALLET_TYPE[2];
                updateBalance.fleetId = fleet.fleetId;
                updateBalance.driverId = updateBalanceId.driverId;
                updateBalance.amount = amount;
                updateBalance.currencyISO = currencyISO;
                updateBalance.mongoDao = mongoDao;
                updateBalance.sqlDao = sqlDao;
                updateBalance.reason = reason;
                updateBalance.additionalData.put("operatorId", "");
                updateBalance.additionalData.put("operatorName", "");
                updateBalance.requestId = requestId;
                String result = updateBalance.doUpdate();
                logger.debug(requestId + " - update balance for driverId " + updateBalanceId.driverId + " - result: " + result);
                // done - remove catch
                mongoDao.removeUpdateBalanceId(key, updateBalanceId.driverId);
                i++;
            }

        } catch(Exception ex) {
            logger.debug(requestId + " - ThreadUpdateBalance exception: "+ CommonUtils.getError(ex));
        }

    }
}
