package com.qupworld.paymentgateway.controllers.scheduling;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.TimeZone;

/**
 * Created by qup on 2/28/17.
 */
public class CronJobListener implements ServletContextListener{

    private static final String FLAG = "milestone";

    private static volatile Scheduler scheduler = null;

    public static Scheduler getScheduler() throws SchedulerException {
        if (scheduler == null) {
            synchronized (FLAG) {
                if (scheduler == null) {
                    scheduler = new StdSchedulerFactory().getScheduler();
                }
            }
        }
        return scheduler;
    }

    @Override
    public void contextInitialized(ServletContextEvent servletContext) {
        System.out.println("Context Initialized");

        try {
            // Setup the Job class and the Job group
            JobDetail jobCleanRedis = JobBuilder.newJob(CleanRedisJob.class).withIdentity(
                    "CronQuartzCleanRedisJob", "Group").build();

            // Create a Trigger that fires at 1 AM everyday
            Trigger triggerCleanRedis = TriggerBuilder.newTrigger()
                    .withIdentity("TriggerCleanRedis", "Group")
                    //.withSchedule(CronScheduleBuilder.cronSchedule("0/30 * * * * ?"))
                    .withSchedule(CronScheduleBuilder.cronSchedule("0 0 1 * * ?"))
                    .build();

            // Setup the Job class and the Job group
            JobDetail jobAudit = JobBuilder.newJob(AuditReportJob.class).withIdentity(
                    "CronQuartzAuditJob", "Group").build();

            // Create a Trigger that fires at 2 AM everyday
            Trigger triggerAudit = TriggerBuilder.newTrigger()
                    .withIdentity("TriggerAudit", "Group")
                    //.withSchedule(CronScheduleBuilder.cronSchedule("0 0/5 * * * ?"))
                    .withSchedule(CronScheduleBuilder.cronSchedule("0 0 2 * * ?"))
                    .build();

            // Setup the Job class and the Job group
            JobDetail jobRedeemExpired = JobBuilder.newJob(RedeemExpiredJob.class).withIdentity(
                    "CronQuartzRedeemExpiredJob", "Group").build();

            // Create a Trigger that fires at 0 AM everyday
            Trigger triggerRedeemExpired = TriggerBuilder.newTrigger()
                    .withIdentity("TriggerRedeemExpired", "Group")
                    //.withSchedule(CronScheduleBuilder.cronSchedule("0 0/5 * * * ?"))
                    .withSchedule(CronScheduleBuilder.cronSchedule("0 0 0 * * ?").inTimeZone(TimeZone.getTimeZone("GMT")))
                    .build();

            // Setup the Job and Trigger with Scheduler & schedule jobs
            getScheduler().start();
            getScheduler().scheduleJob(jobCleanRedis, triggerCleanRedis);
            getScheduler().scheduleJob(jobAudit, triggerAudit);
            getScheduler().scheduleJob(jobRedeemExpired, triggerRedeemExpired);
        }
        catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContext) {
        System.out.println("Context Destroyed Scheduler .... ");
        try
        {
            getScheduler().shutdown(true);
            StdSchedulerFactory.getDefaultScheduler().shutdown(true);
            System.out.println("Context Destroyed Scheduler DONE !!!!");
        }
        catch (Exception e)
        {
            System.out.println("============== scheduler.shutdown ERROR");
            e.printStackTrace();
        }
    }
}
