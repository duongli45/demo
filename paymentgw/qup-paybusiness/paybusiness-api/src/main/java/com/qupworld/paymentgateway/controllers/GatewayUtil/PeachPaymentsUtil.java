package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.google.gson.Gson;
import com.qupworld.paymentgateway.entities.CreditEnt;
import com.pg.util.ObjResponse;
import com.qupworld.util.PeachPaymentsConnectionUtil;
import com.pg.util.ValidCreditCard;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class PeachPaymentsUtil {
    private static final Logger logger = LogManager.getLogger(PeachPaymentsUtil.class);
    private PeachPaymentsConnectionUtil peachPaymentsConnectionUtil;
    private DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.##");
    private String requestId;
    private Gson gson;
    public PeachPaymentsUtil(String requestId) {
        peachPaymentsConnectionUtil = new PeachPaymentsConnectionUtil(requestId);
        this.requestId = requestId;
        gson = new Gson();
    }

    public String createCreditToken(CreditEnt credit, Map<String, String> account, String currencyISO) throws IOException {
        ObjResponse objResponse = new ObjResponse();
        boolean isVoid = false;
        // check setting info
        String transId = "";
        String approval = "";
        try {
            Boolean enableCheckZipCode = false;
            if (!isEmptyString(credit.postalCode)) {
                enableCheckZipCode = true;
            }
            String[] date = credit.expiredDate.split("/");

            Map<String,String> mapData = new HashMap<>();
            mapData.put("userId", account.get("userId"));
            mapData.put("password", account.get("password"));
            mapData.put("entityId", account.get("clientId"));
            mapData.put("currency", currencyISO);
            mapData.put("cardType", getCardType(credit.cardNumber));
            mapData.put("cardHolder", credit.cardHolder);
            mapData.put("cardNumber", credit.cardNumber);
            mapData.put("expiredMonth", date[0]);
            mapData.put("expiredYear", date[1]);
            mapData.put("cvv", credit.cvv);
            mapData.put("zipCode", credit.postalCode);
            String cardLog = "XXXXXXXXXXXX" + credit.cardNumber.substring(credit.cardNumber.length() - 4);
            String log = gson.toJson(mapData).replace(credit.cardNumber, cardLog);
            logger.debug(requestId + " - request: " + log);
            Map<String, Object> transactionResponse = peachPaymentsConnectionUtil.createCreditToken(account.get("serverUrl"), mapData, enableCheckZipCode);
            logger.debug(requestId + " - response: " + gson.toJson(transactionResponse));
            // if verify CVV and AVS has failed, void this transaction
            String returnCode = transactionResponse.get("returnCode") != null ? transactionResponse.get("returnCode").toString() : "";
            if (!transId.equals("")) {
                isVoid = true;
                voidPeachPaymentsTransaction(account, "DB", transId, approval, 1, currencyISO);
            }
            if (!returnCode.equals("200")) {
                Map<String,String> response = (Map<String,String>)transactionResponse.get("response");
                String message = response.get("message") != null ? response.get("message") : "";
                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("message", message);
                objResponse.returnCode = Integer.parseInt(returnCode);
                objResponse.response = mapResponse;
                return objResponse.toString();
            }

            // validate AVS and CVV2 is passed, retrieve data and return
            Map<String,String> mapResponse = (Map<String,String>)transactionResponse.get("response");
            String actionCode = mapResponse.get("code") != null ? mapResponse.get("code") : "";
            String transactionCode = "200";
            transId = mapResponse.get("id") != null ? mapResponse.get("id") : "";
            logger.debug(requestId + " - actionCode: " + actionCode + " --- transactionCode: " + transactionCode);
            String cardMasked = "XXXXXXXX" + credit.cardNumber.substring(credit.cardNumber.length() - 4, credit.cardNumber.length());
            String token = mapResponse.get("token") != null ? mapResponse.get("token") : "";
            Map<String,String> mapResult = new HashMap<>();
            mapResult.put("message", "");
            mapResult.put("creditCard", cardMasked);
            mapResult.put("qrCode", "");
            mapResult.put("token", token);
            mapResult.put("cardType", ValidCreditCard.getCardType(credit.cardNumber));

            objResponse.response = mapResult;
            objResponse.returnCode = 200;
            return objResponse.toString();

        } catch(Exception ex) {
            ex.printStackTrace();

            // get error, void completed transaction
            if (!transId.equals("") && !isVoid) {
                voidPeachPaymentsTransaction(account, "DB", transId, approval, 1, currencyISO);
            }
            objResponse.returnCode = 437;
            objResponse.response = getError(ex);
            return objResponse.toString();
        }

    }

    private String getCardType(String cardNumber) {
        String cardType = "";
        if (ValidCreditCard.getCardType(cardNumber).equals("Visa"))
            cardType = "VISA";
        else if (ValidCreditCard.getCardType(cardNumber).equals("MasterCard"))
            cardType = "MASTER";
        else if (ValidCreditCard.getCardType(cardNumber).equals("AmericanExpress"))
            cardType = "AMEX";
        else if (ValidCreditCard.getCardType(cardNumber).equals("Discover"))
            cardType = "DISCOVER";

        return cardType;
    }

    private boolean isEmptyString(String s) {
        return (s == null || s.trim().isEmpty());
    }
    public void voidPeachPaymentsTransaction(Map<String, String> account, String type, String transId, String bookId, double amount, String currencyISO) {
        logger.debug(requestId + " - voidPeachPaymentsTransaction for transId " + transId);
        ObjResponse objResponse = new ObjResponse();
        try {
            Map<String,String> mapData = new HashMap<>();
            mapData.put("userId", account.get("userId"));
            mapData.put("password", account.get("password"));
            mapData.put("entityId", account.get("clientId"));
            mapData.put("bookId", bookId);
            mapData.put("currency", currencyISO);

            Map<String, Object> response = peachPaymentsConnectionUtil.voidPeachPaymentsTransaction(type, account.get("serverUrl"), mapData, transId, amount);
            objResponse.returnCode = 200;
            Map<String,String> mapResponse = (Map<String,String>)response.get("response");
            String refundId = mapResponse.get("id") != null ? mapResponse.get("id") : "";
            logger.debug(requestId + " - transId: " + transId + " has been voided !!!! RefundId: " + refundId);
        } catch(Exception ex) {
            ex.printStackTrace();
            logger.debug(requestId + " - voidPeachPaymentsTransaction ERROR: " + getError(ex));
            objResponse.returnCode = 304;
        }
    }

    public String createPaymentWithCreditToken(double amount, String token, String bookId, String cardType, Map<String,String> account, String currencyISO) {
        logger.debug(requestId + " - createPaymentWithCreditToken " + token + " ...");
        ObjResponse objResponse = new ObjResponse();
        String transId = "";
        try {
            Map<String,String> mapData = new HashMap<>();
            mapData.put("userId", account.get("userId"));
            mapData.put("password", account.get("password"));
            mapData.put("entityId", account.get("clientId"));
            mapData.put("bookId", bookId);
            mapData.put("currency", currencyISO);
            mapData.put("amount", DECIMAL_FORMAT.format(amount));

            Map<String, Object> transactionResponse = peachPaymentsConnectionUtil.createPaymentWithCreditToken(account.get("serverUrl"), mapData, token, amount);

            // validate AVS and CVV2 is passed, retrieve data and return
            String returnCode = transactionResponse.get("returnCode") != null ? transactionResponse.get("returnCode").toString() : "";
            if (!returnCode.equals("200")) {
                voidPeachPaymentsTransaction(account, "DB", transId, bookId, amount, currencyISO);
                Map<String,String> response = (Map<String,String>)transactionResponse.get("response");
                String message = response.get("message") != null ? response.get("message") : "";
                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("message", message);
                objResponse.returnCode = Integer.parseInt(returnCode);
                objResponse.response = mapResponse;
                return objResponse.toString();
            }

            // validate AVS and CVV2 is passed, retrieve data and return
            Map<String,String> mapResponse = (Map<String,String>)transactionResponse.get("response");
            String actionCode = mapResponse.get("code") != null ? mapResponse.get("code") : "";
            transId = mapResponse.get("id") != null ? mapResponse.get("id") : "";
            String authorizationCode = "";

            Map<String,String> mapResult = new HashMap<>();
            mapResult.put("message", "Success!" );
            mapResult.put("transId", transId);
            mapResult.put("cardType", cardType);
            mapResult.put("authCode", authorizationCode);
            mapResult.put("cardMask", "");
            objResponse.response = mapResult;
            objResponse.returnCode = 200;
            return objResponse.toString();
        } catch(Exception ex) {
            ex.printStackTrace();

            // get error, void completed transaction
            if (!transId.equals("")) {
                voidPeachPaymentsTransaction(account, "DB", transId, bookId, amount, currencyISO);
            }
            return returnPaymentFailed(transId, "DB", "token",
                    account.get("fleetId"), bookId, amount,
                    "437", getError(ex), false);
        }
    }

    public String preauthPayment(double amount, String token, String bookId, Map<String,String> account, String currencyISO) {
        logger.debug(requestId + " - preauthPayment " + token + " ...");
        ObjResponse objResponse = new ObjResponse();
        String transId = "";
        try {
            Map<String,String> mapData = new HashMap<>();
            mapData.put("userId", account.get("userId"));
            mapData.put("password", account.get("password"));
            mapData.put("entityId", account.get("clientId"));
            mapData.put("bookId", bookId);
            mapData.put("currency", currencyISO);
            mapData.put("amount", DECIMAL_FORMAT.format(amount));

            Map<String, Object> transactionResponse = peachPaymentsConnectionUtil.preauthPayment(account.get("serverUrl"), mapData, token, amount);

            // validate AVS and CVV2 is passed, retrieve data and return
            String returnCode = transactionResponse.get("returnCode") != null ? transactionResponse.get("returnCode").toString() : "";
            if (!returnCode.equals("200")) {
                voidPeachPaymentsTransaction(account, "DB", transId, bookId, amount, currencyISO);
                Map<String,String> response = (Map<String,String>)transactionResponse.get("response");
                String message = response.get("message") != null ? response.get("message") : "";
                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("message", message);
                objResponse.returnCode = Integer.parseInt(returnCode);
                objResponse.response = mapResponse;
                return objResponse.toString();
            }

            // validate AVS and CVV2 is passed, retrieve data and return
            Map<String,String> mapResponse = (Map<String,String>)transactionResponse.get("response");
            transId = mapResponse.get("id") != null ? mapResponse.get("id") : "";
            String authorizationCode = "";

            Map<String,String> mapResult = new HashMap<>();
            mapResult.put("authId", transId);
            mapResult.put("authAmount", String.valueOf(amount));
            mapResult.put("authCode", authorizationCode);
            mapResult.put("authToken", token);
            mapResult.put("allowCapture", "true");
            objResponse.response = mapResult;
            objResponse.returnCode = 200;
            return objResponse.toString();
        } catch(Exception ex) {
            ex.printStackTrace();

            // get error, void completed transaction
            if (!transId.equals("")) {
                voidPeachPaymentsTransaction(account, "PA", transId, bookId, amount, currencyISO);
            }
            return returnPaymentFailed(transId, "PA", "token",
                    account.get("fleetId"), bookId, amount,
                    "437", getError(ex), false);
        }
    }

    public String preauthCapture(double amount, String authId, String bookId, Map<String,String> account, String cardType, String currencyISO) {
        logger.debug(requestId + " - preauthCapture " + authId + " ...");
        ObjResponse objResponse = new ObjResponse();
        String transId = "";
        String approval = "";
        try {
            Map<String,String> mapData = new HashMap<>();
            mapData.put("userId", account.get("userId"));
            mapData.put("password", account.get("password"));
            mapData.put("entityId", account.get("clientId"));
            mapData.put("bookId", bookId);
            mapData.put("currency", currencyISO);
            mapData.put("amount", DECIMAL_FORMAT.format(amount));

            Map<String, Object> transactionResponse = peachPaymentsConnectionUtil.preauthCapture(account.get("serverUrl"), mapData, authId, amount);

            // validate AVS and CVV2 is passed, retrieve data and return
            String returnCode = transactionResponse.get("returnCode") != null ? transactionResponse.get("returnCode").toString() : "";
            if (!returnCode.equals("200")) {
                voidPeachPaymentsTransaction(account, "DB", transId, bookId, amount, currencyISO);
                Map<String,String> response = (Map<String,String>)transactionResponse.get("response");
                String message = response.get("message") != null ? response.get("message") : "";
                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("message", message);
                objResponse.returnCode = Integer.parseInt(returnCode);
                objResponse.response = mapResponse;
                return objResponse.toString();
            }

            // validate AVS and CVV2 is passed, retrieve data and return
            Map<String,String> mapResponse = (Map<String,String>)transactionResponse.get("response");
            String actionCode = mapResponse.get("code") != null ? mapResponse.get("code") : "";
            transId = mapResponse.get("id") != null ? mapResponse.get("id") : "";
            String authorizationCode = "";

            Map<String,String> mapResult = new HashMap<>();
            mapResult.put("message", "Success!" );
            mapResult.put("transId", transId);
            mapResult.put("cardType", cardType);
            mapResult.put("authCode", authorizationCode);
            mapResult.put("cardMask", "");
            objResponse.response = mapResult;
            objResponse.returnCode = 200;
            return objResponse.toString();
        } catch(Exception ex) {
            ex.printStackTrace();

            // get error, void completed transaction
            if (!transId.equals("")) {
                voidPeachPaymentsTransaction(account, "PA", transId, bookId, amount, currencyISO);
            }
            return returnPaymentFailed(transId, "PA", "token",
                    account.get("fleetId"), bookId, amount,
                    "437", getError(ex), false);
        }
    }

    private String returnPaymentFailed(String transId, String transType, String paymentType,
                                       String fleetId, String bookId, double amount,
                                       String errorCode, String errorMessage, boolean isPreAuth) {
        ObjResponse objResponse = new ObjResponse();
        objResponse.returnCode = Integer.parseInt(errorCode);
        objResponse.response = errorMessage;
        return objResponse.toString();
    }

    public String doPaymentWithCardInfo(String bookId, double amount,
                                        String cardNumber, String expiredDate, String cvv, String cardHolder, String postalCode, Map<String,String> account, String currencyISO) throws IOException {
        logger.debug("--- doPaymentWithCardInfo for card " + cardNumber.substring(cardNumber.length() - 4, cardNumber.length()) + " ...");
        ObjResponse objResponse = new ObjResponse();
        String transId = "";
        String approval = "";
        try {
            Boolean enableCheckZipCode = false;
            if (!isEmptyString(postalCode)) {
                enableCheckZipCode = true;
            }
            String[] date = expiredDate.split("/");

            Map<String,String> mapData = new HashMap<>();
            mapData.put("userId", account.get("userId"));
            mapData.put("password", account.get("password"));
            mapData.put("entityId", account.get("clientId"));
            mapData.put("bookId", bookId);
            mapData.put("currency", currencyISO);
            mapData.put("cardType", getCardType(cardNumber));
            mapData.put("cardHolder", cardHolder);
            mapData.put("cardNumber", cardNumber);
            mapData.put("expiredMonth", date[0]);
            mapData.put("expiredYear", date[1]);
            mapData.put("cvv", cvv);
            mapData.put("zipCode", postalCode);

            Map<String, Object> transactionResponse = peachPaymentsConnectionUtil.createPaymentWithCardInfo(account.get("serverUrl"), mapData, enableCheckZipCode, amount);

            // if verify CVV and AVS has failed, void this transaction
            String returnCode = transactionResponse.get("returnCode") != null ? transactionResponse.get("returnCode").toString() : "";
            if (!returnCode.equals("200")) {
                voidPeachPaymentsTransaction(account, "DB", transId, bookId, amount, currencyISO);
                Map<String,String> response = (Map<String,String>)transactionResponse.get("response");
                String message = response.get("message") != null ? response.get("message") : "";
                Map<String,String> mapResponse = new HashMap<>();
                mapResponse.put("message", message);
                objResponse.returnCode = Integer.parseInt(returnCode);
                objResponse.response = mapResponse;
                return objResponse.toString();
            }

            // validate AVS and CVV2 is passed, retrieve data and return
            Map<String,String> mapResponse = (Map<String,String>)transactionResponse.get("response");
            String actionCode = mapResponse.get("code") != null ? mapResponse.get("code") : "";
            transId = mapResponse.get("id") != null ? mapResponse.get("id") : "";
            String cardMasked = "XXXXXXXX" + cardNumber.substring(cardNumber.length() - 4, cardNumber.length());
            String cardType = ValidCreditCard.getCardType(cardNumber);

            Map<String,String> mapResult = new HashMap<>();
            mapResult.put("message", "Success!" );
            mapResult.put("transId", transId);
            mapResult.put("cardType", cardType);
            mapResult.put("authCode", "");
            mapResult.put("cardMask", cardMasked);
            objResponse.response = mapResponse;
            objResponse.returnCode = 200;
            return objResponse.toString();
        } catch(Exception ex) {
            ex.printStackTrace();

            // get error, void completed transaction
            if (!transId.equals("")) {
                voidPeachPaymentsTransaction(account, "DB", transId, bookId, amount, currencyISO);
            }
            return returnPaymentFailed(transId, "SALE", "token",
                    account.get("fleetId"), bookId, amount,
                    "437", getError(ex), false);
        }
    }
    private String getError(Exception ex) {
        return GatewayUtil.getError(ex);
    }
}
