package com.qupworld.paymentgateway.controllers.scheduling;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.controllers.GatewayUtil.GatewayUtil;
import com.qupworld.paymentgateway.controllers.threading.MigrateTicketAuditThread;
import com.qupworld.paymentgateway.models.RedisDao;
import com.qupworld.paymentgateway.models.RedisImpl;
import com.qupworld.service.MailService;
import com.pg.util.KeysUtil;
import com.pg.util.TimezoneUtil;
import com.pg.util.VersionUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.net.InetAddress;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by qup on 2/28/17.
 */
@SuppressWarnings("unchecked")
public class AuditReportJob implements Job {

    private final Logger logger = LogManager.getLogger(AuditReportJob.class);
    private final String AUDIT_URL = "/api/v1/report/admin-compare-ticket";
    private final String BOOKING_URL = "/api/v1/object/Ticket/";
    private VersionUtil versionUtil = new VersionUtil();

    //static SQLDao sqlDao = new SQLDaoImpl();
    private DecimalFormat df = new DecimalFormat("#.##");
    private RedisDao redisDao = new RedisImpl();
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat sdfSaveRedisWithHour = new SimpleDateFormat("yyyy-MM-dd-HH");

    private Map<Integer, Integer> diffBook = new HashMap<>();
    private Map<Integer, Double> diffTotal = new HashMap<>();
    private Map<Integer, String> diffStart = new HashMap<>();
    private Map<Integer, String> diffEnd = new HashMap<>();

    private boolean isSentMailReport = true;
    private int reportTotalBookings = 0;
    private double reportTotalCharged = 0.0;
    private int redisTotalBookings = 0;
    private double redisTotalCharged = 0.0;

    public void paymentAudit(String requestId, String from, String to) {
        startAudit(requestId, from, to);
        /*try {
            execute(null);
        } catch (JobExecutionException e) {
            e.printStackTrace();
        }*/
    }
    public void execute(JobExecutionContext context)
            throws JobExecutionException {
        String requestId = UUID.randomUUID().toString();
        logger.debug(requestId + " ===== audit payment results ....");
        try {

            SimpleDateFormat simpleSDF = new SimpleDateFormat("yyyy-MM-dd");

            Calendar calFrom = Calendar.getInstance();
            calFrom.add(Calendar.DATE, -1);
            String from = simpleSDF.format(calFrom.getTime()) + " 00:00:00";
            Date fromServer = KeysUtil.SDF_DMYHMS.parse(from);
            Date fromGMT = TimezoneUtil.convertServerToGMT(fromServer);

            Calendar calTo = Calendar.getInstance();
            calTo.add(Calendar.DATE, -1);
            String to = simpleSDF.format(calTo.getTime()) + " 23:59:59";
            Date toServer = KeysUtil.SDF_DMYHMS.parse(to);
            Date toGMT = TimezoneUtil.convertServerToGMT(toServer);

            // startAudit(requestId, KeysUtil.SDF_DMYHMS.format(fromGMT), KeysUtil.SDF_DMYHMS.format(toGMT));
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    private void startAudit(String requestId, String from, String to) {
        logger.debug(requestId + " >>>>> start audit payment from "+ from +" to " + to);
        try {
            isSentMailReport = false;
            InetAddress ip = InetAddress.getLocalHost();
            //boolean isMatch = doAudit(requestId, from, to, false, 0);

            // set limit query time
            Calendar end = Calendar.getInstance();
            end.setTime(KeysUtil.SDF_DMYHMS.parse(to));

            // set time range in 1 hour
            Calendar startRange = Calendar.getInstance();
            startRange.setTime(KeysUtil.SDF_DMYHMS.parse(from));

            Calendar endRange = Calendar.getInstance();
            endRange.setTime(KeysUtil.SDF_DMYHMS.parse(from));
            endRange.add(Calendar.HOUR, 1);
            endRange.add(Calendar.SECOND, -1);

            Map<Integer, Boolean> listData = new HashMap<>();
            int countUnMatched = 0; // count number of un-matched time range
            int i = 0;
            while (!endRange.after(end)) {
                logger.debug("");
                boolean isMatch = doAudit(requestId, KeysUtil.SDF_DMYHMS.format(startRange.getTime()), KeysUtil.SDF_DMYHMS.format(endRange.getTime()), i);
                logger.debug(requestId + " -- isMatch: " + isMatch);
                if (!isMatch) {
                    countUnMatched++;
                }
                listData.put(i, isMatch);
                // reset
                startRange.setTime(KeysUtil.SDF_DMYHMS.parse(from));
                endRange.setTime(KeysUtil.SDF_DMYHMS.parse(from));
                // increase time range
                i++;
                startRange.add(Calendar.HOUR, i);
                endRange.add(Calendar.HOUR, i + 1);
                endRange.add(Calendar.SECOND, -1);
            }

            logger.debug(requestId + " ====== listUnMatch: " + countUnMatched);

            String subject;
            StringBuilder body;
            if (countUnMatched > 0) {
                // not match - show details result
                subject = "[QUp_Mail_Report] [" + ServerConfig.mail_server.toUpperCase() + "] [Payment] [FAILURE]" +" Audit Payment date " + KeysUtil.SDF_DMYHMS.format(new Date());

                body = new StringBuilder("Server name: " + ServerConfig.mail_server.toUpperCase() + "<br>");
                body.append("Host name: ").append(ip.getHostName()).append("<br>");
                body.append("Version: ").append(versionUtil.getVersion()).append("<br>");
                body.append("Time: ").append(KeysUtil.SDF_DMYHMS.format(new Date())).append("<br>");
                body.append("Trace ID: ").append(requestId).append("<br>");
                body.append("Message: [FAILURE]" + " Audit Payment date ").append(KeysUtil.SDF_DMYHMS.format(new Date())).append("<br>");
                body.append("Data: <br>");
                body.append("\treportTotalBookings: ").append(reportTotalBookings).append(" ---- reportTotalCharged: ").append(KeysUtil.DECIMAL_FORMAT.format(reportTotalCharged)).append("<br>");
                body.append("\tredisTotalBookings: ").append(redisTotalBookings).append(" ---- redisTotalCharged: ").append(KeysUtil.DECIMAL_FORMAT.format(redisTotalCharged)).append("<br>");
                body.append("Error: Payment data is not matching !!! <br>");
                body.append("====== listUnMatch: ").append(countUnMatched).append("<br>&nbsp;<br>");
                startRange.setTime(KeysUtil.SDF_DMYHMS.parse(from));
                endRange.setTime(KeysUtil.SDF_DMYHMS.parse(from));
                endRange.add(Calendar.HOUR, 1);
                endRange.add(Calendar.SECOND, -1);
                for (int j = 0; j < listData.size(); j++) {
                    startRange.add(Calendar.HOUR, j);
                    endRange.add(Calendar.HOUR, j + 1);
                    // get time change from list
                    boolean match = listData.get(j);
                    if (!match) {
                        logger.debug(requestId + " startRange: " + KeysUtil.SDF_DMYHMS.format(startRange.getTime()) + " - endRange: " + KeysUtil.SDF_DMYHMS.format(endRange.getTime()));
                        body.append("--- startRange: ").append(diffStart.get(j)).append(" - endRange: ").append(diffEnd.get(j)).append("<br>");
                        body.append("differ booking: ").append(diffBook.get(j)).append(" - differ total: ").append(diffTotal.get(j)).append("<br>");
                        String result = getDetailUnmatched(requestId,
                                KeysUtil.SDF_DMYHMS.format(startRange.getTime()), KeysUtil.SDF_DMYHMS.format(endRange.getTime()),
                                diffBook.get(j), diffTotal.get(j));
                        body.append(result).append("<br>");
                        body.append("<br>");
                    }
                }
            } else {
                // matching completely
                subject = "[QUp_Mail_Report] [" + ServerConfig.mail_server.toUpperCase() + "] [Payment] [OK]" +" Audit Payment date " + KeysUtil.SDF_DMYHMS.format(new Date());

                body = new StringBuilder("Server name: " + ServerConfig.mail_server.toUpperCase() + "<br>");
                body.append("Host name: ").append(ip.getHostName()).append("<br>");
                body.append("Version: ").append(versionUtil.getVersion()).append("<br>");
                body.append("Time: ").append(KeysUtil.SDF_DMYHMS.format(new Date())).append("<br>");
                body.append("Trace ID: ").append(requestId).append("<br>");
                body.append("Message: [OK]" + " Audit Payment date ").append(KeysUtil.SDF_DMYHMS.format(new Date())).append("<br>");
                body.append("Data: <br>");
                body.append("\treportTotalBookings: ").append(reportTotalBookings).append(" ---- reportTotalCharged: ").append(KeysUtil.DECIMAL_FORMAT.format(reportTotalCharged)).append("<br>");
                body.append("\tredisTotalBookings: ").append(redisTotalBookings).append(" ---- redisTotalCharged: ").append(KeysUtil.DECIMAL_FORMAT.format(redisTotalCharged)).append("<br>");
                body.append("====== Payment data matching completely !!!");
            }

            //send email
            if (!isSentMailReport) {
                MailService mailService = MailService.getInstance();
                mailService.sendMailReport(requestId, subject, body.toString());
            }

        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    private boolean doAudit(String requestId, String from, String to, int index) throws Exception{
        //logger.debug(requestId + " - doAudit from: " + from + " - to: " + to);
        int reportTotalBookingsRange = 0;
        double reportTotalChargedRange = 0.0;
        int redisTotalBookingsRange = 0;
        double redisTotalChargedRange = 0.0;

        InetAddress ip = InetAddress.getLocalHost();

        /// REPORT
        JSONObject reportResult = checkReportData(requestId, from, to);
        if (reportResult == null) {
            // try to request again
            reportResult = checkReportData(requestId, from, to);
        }
        if (reportResult != null) {
            reportTotalBookingsRange = reportResult.getInt("totalBookings");
            reportTotalChargedRange = Double.valueOf(df.format(reportResult.getDouble("totalCharged")));
            logger.debug(requestId + " bookings: " + reportTotalBookingsRange + " ---- total: " + reportTotalChargedRange);

            // count total book and sum total charge
            reportTotalBookings += reportTotalBookingsRange;
            reportTotalCharged += reportTotalChargedRange;
        } else {
            logger.debug(requestId + " --- cannot get data from Reporting - Send mail warning");
            String subject = "[QUp_Mail_Report] [" + ServerConfig.mail_server.toUpperCase() + "] [Payment] [FAILURE]" +" Audit Payment date " + KeysUtil.SDF_DMYHMS.format(new Date());

            String body = "Server name: "+ ServerConfig.mail_server.toUpperCase() + "<br>";
            body += "Host name: " + ip.getHostName() + "<br>";
            body += "Version: " + versionUtil.getVersion() + "<br>";
            body += "Time: " + KeysUtil.SDF_DMYHMS.format(new Date()) + "<br>";
            body += "Trace ID: " + requestId + "<br>";
            body += "Message: [FAILURE]" +" Audit Payment date " + KeysUtil.SDF_DMYHMS.format(new Date()) + "<br>";
            body += "Data: <br>";
            body += "\treportTotalBookings: " + reportTotalBookings + " ---- reportTotalCharged: " + KeysUtil.DECIMAL_FORMAT.format(reportTotalCharged) + "<br>";
            body += "\tredisTotalBookings: " + redisTotalBookings + " ---- redisTotalCharged: " + KeysUtil.DECIMAL_FORMAT.format(redisTotalCharged) + "<br>";
            body += "Error: Could not get data from Reporting. Please check the connection to Reporting server!";
            MailService mailService = MailService.getInstance();

            mailService.sendMailReport(requestId, subject, body);
            isSentMailReport = true;
        }

        /// REDIS
        String date = sdfSaveRedisWithHour.format(sdf.parse(from));
        logger.debug(requestId + " - REDIS: " + date);
        redisTotalBookingsRange = redisDao.countTicket(requestId, date);
        redisTotalChargedRange = Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(redisDao.sumTotalTicket(requestId, date)));
        logger.debug(requestId + " bookings: " + redisTotalBookingsRange + " ---- total: " + redisTotalChargedRange);
        // count total book and sum total charge
        redisTotalBookings += redisTotalBookingsRange;
        redisTotalCharged += redisTotalChargedRange;

        // start compare
        int bookDiff = redisTotalBookingsRange - reportTotalBookingsRange;
        double totalDiff = redisTotalChargedRange - reportTotalChargedRange;
        if (bookDiff != 0 || totalDiff != 0) {
            logger.debug(requestId + " - booking differ = " + bookDiff);
            logger.debug(requestId + " - total differ = " + totalDiff);
            diffBook.put(index, bookDiff);
            diffTotal.put(index, Double.parseDouble(KeysUtil.DECIMAL_FORMAT.format(totalDiff)));
            diffStart.put(index, KeysUtil.SDF_DMYHMS.format(KeysUtil.SDF_DMYHMS.parse(from)));
            diffEnd.put(index, KeysUtil.SDF_DMYHMS.format(KeysUtil.SDF_DMYHMS.parse(to)));
            return false;
        } else {
            return true;
        }
    }

    private JSONObject checkReportData(String requestId, String from, String to) throws Exception{
        logger.debug(requestId + " - REPORT from: " + from + " - to: " + to);
        JSONObject summary = null;
        try {
            if (ServerConfig.server_report_enable) {
                String body = "{\"criteria\":{\"fromDate\":\"" + from + "\", \"toDate\":\"" + to + "\"}}";
                // submit with old token
                HttpResponse<JsonNode> json = Unirest.post(ServerConfig.audit_report_url + AUDIT_URL)
                        .header("Content-Type", "application/json; charset=utf-8; charset=utf-8")
                        .header("x-access-token", KeysUtil.TOKEN)
                        .header("x-trace-request-id", requestId)
                        .body(body)
                        .asJson();
                summary = json.getBody().getObject().has("summary") ? json.getBody().getObject().getJSONObject("summary") : null;
                // generate new authentication token and retry for the 1st time
                if (summary == null) {
                    String authBody = "{ \"username\" : \"" + ServerConfig.auth_username + "\" , \"password\" : \"" + ServerConfig.auth_password + "\" }";
                    HttpResponse<JsonNode> jsonAuth = Unirest.post(ServerConfig.server_auth)
                            .header("Content-Type", "application/json; charset=utf-8")
                            .body(authBody)
                            .asJson();
                    KeysUtil.TOKEN = jsonAuth.getBody().getObject().has("token") ? jsonAuth.getBody().getObject().get("token").toString() : "";
                    // submit with new token
                    json = Unirest.post(ServerConfig.audit_report_url + AUDIT_URL)
                            .header("Content-Type", "application/json; charset=utf-8")
                            .header("x-access-token", KeysUtil.TOKEN)
                            .header("x-trace-request-id", requestId)
                            .body(body)
                            .asJson();
                    summary = json.getBody().getObject().has("summary") ? json.getBody().getObject().getJSONObject("summary") : null;
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " --- checkReportData exception: " + GatewayUtil.getError(ex));
        }
        return summary;
    }

    private String getDetailUnmatched(String requestId, String from, String to, int differBook, double differTotal)  throws Exception{
        if (differBook < 0) {
            // total booking on Report larger than Redis - show warning message
            return "Missing data on Redis - Please check !!!";
        } else {
            String date = sdfSaveRedisWithHour.format(sdf.parse(from));
            List<String> bookIds = redisDao.getListBooks(requestId, date);
            List<String> missing = new ArrayList<>();
            for (String bookId : bookIds) {
                if (!checkBooking(requestId, bookId)){
                    missing.add(bookId);
                }
            }
            logger.debug(requestId + " - missing " + missing.size()+ " booking!!!!");
            if (ServerConfig.enable_sync) {
                MigrateTicketAuditThread migrateTicketAuditThread = new MigrateTicketAuditThread();
                migrateTicketAuditThread.BOOKIDS = missing;
                migrateTicketAuditThread.requestId = requestId;
                migrateTicketAuditThread.start();
            }

            return String.join(",", missing);
        }
    }

    private boolean checkBooking(String requestId, String bookId) throws Exception{
        //logger.debug(requestId + " - checkBooking " + bookId);
        boolean isExist = true;
        try {
            if (ServerConfig.server_report_enable) {
                // submit with old token
                HttpResponse<JsonNode> json = Unirest.get(ServerConfig.audit_report_url + BOOKING_URL + bookId)
                        .header("Content-Type", "application/json; charset=utf-8; charset=utf-8")
                        .header("x-access-token", KeysUtil.TOKEN)
                        .header("x-trace-request-id", requestId)
                        .asJson();
                String code = json.getBody().getObject().has("code") ? String.valueOf(json.getBody().getObject().get("code")) : "";
                // generate new authentication token and retry for the 1st time
                if (code.equals("404")) {
                    isExist = false;
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " --- checkReportData exception: " + GatewayUtil.getError(ex));
        }
        return isExist;
    }

}
