
package com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Authorize3DSRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Authorize3DSRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BillingDetails" type="{http://schemas.firstatlanticcommerce.com/gateway/data}BillingDetails" minOccurs="0"/>
 *         &lt;element name="CardDetails" type="{http://schemas.firstatlanticcommerce.com/gateway/data}CardDetails" minOccurs="0"/>
 *         &lt;element name="MerchantResponseURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionDetails" type="{http://schemas.firstatlanticcommerce.com/gateway/data}TransactionDetails" minOccurs="0"/>
 *         &lt;element name="RecurringDetails" type="{http://schemas.firstatlanticcommerce.com/gateway/data}RecurringDetails" minOccurs="0"/>
 *         &lt;element name="FraudDetails" type="{http://schemas.firstatlanticcommerce.com/gateway/data}FraudDetails" minOccurs="0"/>
 *         &lt;element name="ShippingDetails" type="{http://schemas.firstatlanticcommerce.com/gateway/data}ShippingDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Authorize3DSRequest", propOrder = {
    "billingDetails",
    "cardDetails",
    "merchantResponseURL",
    "transactionDetails",
    "recurringDetails",
    "fraudDetails",
    "shippingDetails"
})
public class Authorize3DSRequest {

    @XmlElement(name = "BillingDetails", nillable = true)
    protected BillingDetails billingDetails;
    @XmlElement(name = "CardDetails", nillable = true)
    protected CardDetails cardDetails;
    @XmlElement(name = "MerchantResponseURL", nillable = true)
    protected String merchantResponseURL;
    @XmlElement(name = "TransactionDetails", nillable = true)
    protected TransactionDetails transactionDetails;
    @XmlElement(name = "RecurringDetails", nillable = true)
    protected RecurringDetails recurringDetails;
    @XmlElement(name = "FraudDetails", nillable = true)
    protected FraudDetails fraudDetails;
    @XmlElement(name = "ShippingDetails", nillable = true)
    protected ShippingDetails shippingDetails;

    /**
     * Gets the value of the billingDetails property.
     * 
     * @return
     *     possible object is
     *     {@link BillingDetails }
     *     
     */
    public BillingDetails getBillingDetails() {
        return billingDetails;
    }

    /**
     * Sets the value of the billingDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingDetails }
     *     
     */
    public void setBillingDetails(BillingDetails value) {
        this.billingDetails = value;
    }

    /**
     * Gets the value of the cardDetails property.
     * 
     * @return
     *     possible object is
     *     {@link CardDetails }
     *     
     */
    public CardDetails getCardDetails() {
        return cardDetails;
    }

    /**
     * Sets the value of the cardDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardDetails }
     *     
     */
    public void setCardDetails(CardDetails value) {
        this.cardDetails = value;
    }

    /**
     * Gets the value of the merchantResponseURL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMerchantResponseURL() {
        return merchantResponseURL;
    }

    /**
     * Sets the value of the merchantResponseURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMerchantResponseURL(String value) {
        this.merchantResponseURL = value;
    }

    /**
     * Gets the value of the transactionDetails property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionDetails }
     *     
     */
    public TransactionDetails getTransactionDetails() {
        return transactionDetails;
    }

    /**
     * Sets the value of the transactionDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionDetails }
     *     
     */
    public void setTransactionDetails(TransactionDetails value) {
        this.transactionDetails = value;
    }

    /**
     * Gets the value of the recurringDetails property.
     * 
     * @return
     *     possible object is
     *     {@link RecurringDetails }
     *     
     */
    public RecurringDetails getRecurringDetails() {
        return recurringDetails;
    }

    /**
     * Sets the value of the recurringDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link RecurringDetails }
     *     
     */
    public void setRecurringDetails(RecurringDetails value) {
        this.recurringDetails = value;
    }

    /**
     * Gets the value of the fraudDetails property.
     * 
     * @return
     *     possible object is
     *     {@link FraudDetails }
     *     
     */
    public FraudDetails getFraudDetails() {
        return fraudDetails;
    }

    /**
     * Sets the value of the fraudDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link FraudDetails }
     *     
     */
    public void setFraudDetails(FraudDetails value) {
        this.fraudDetails = value;
    }

    /**
     * Gets the value of the shippingDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ShippingDetails }
     *     
     */
    public ShippingDetails getShippingDetails() {
        return shippingDetails;
    }

    /**
     * Sets the value of the shippingDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShippingDetails }
     *     
     */
    public void setShippingDetails(ShippingDetails value) {
        this.shippingDetails = value;
    }

}
