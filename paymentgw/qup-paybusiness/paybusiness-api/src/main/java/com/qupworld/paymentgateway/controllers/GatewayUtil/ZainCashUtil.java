package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.qupworld.config.ServerConfig;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import okhttp3.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class ZainCashUtil {

    final static Logger logger = LogManager.getLogger(ZainCashUtil.class);

    private String requestId;
    private String MSISDN = "";
    private String MERCHANT_ID = "";
    private String SECRET_KEY = "";
    private String REQUEST_URL;
    private final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    public ZainCashUtil(String requestId, String msisdn, String merchantId, String secretKey, String requestUrl) {
        this.requestId = requestId;
        MSISDN = msisdn;
        MERCHANT_ID = merchantId;
        SECRET_KEY = secretKey;
        REQUEST_URL = requestUrl;
    }

    public String getCheckoutURL(double amount, String orderId, String reason) throws Exception{
        ObjResponse response = new ObjResponse();
        try {
            String returnUrl = ServerConfig.hook_server + "/notification/" + KeysUtil.ZAINCASH;
            logger.debug(requestId + " - orderId = " + orderId);

            DecimalFormat df = new DecimalFormat("#");
            int intAmount = Integer.parseInt(df.format(amount));

            long time = System.currentTimeMillis();
            Map<String, Object> payloadClaims = new HashMap<>();
            payloadClaims.put("amount", intAmount);
            payloadClaims.put("serviceType", reason);
            payloadClaims.put("msisdn", MSISDN);
            payloadClaims.put("orderId", orderId);
            payloadClaims.put("redirectUrl", returnUrl + "?gwOrderid="+orderId);
            payloadClaims.put("iat", time);
            payloadClaims.put("exp", time + 60 * 60 * 4);

            String token = createJWSSignature(payloadClaims);

            JSONObject objBody = new JSONObject();
            objBody.put("token", token);
            objBody.put("merchantId", MERCHANT_ID);
            objBody.put("lang", "en");
            logger.debug(requestId + " - objBody = " + objBody.toJSONString());

            RequestBody body = RequestBody.create(objBody.toJSONString(), JSON);
            Request request = new Request.Builder()
                    .addHeader("Content-Type", "application/json")
                    .url(REQUEST_URL + "/init")
                    .post(body)
                    .build();

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60,TimeUnit.SECONDS)
                    .writeTimeout(60,TimeUnit.SECONDS)
                    .build();
            Response gwResponse = client.newCall(request).execute();
            String responseString = gwResponse.body() != null ? gwResponse.body().string() : "";
            logger.debug(requestId + " - response = " + responseString);
            org.json.JSONObject objResponse = new org.json.JSONObject(responseString);
            String id = objResponse.has("id") ? objResponse.getString("id") : "";
            logger.debug(requestId + " - id = " + id);
            if (!id.isEmpty()) {
                Map<String, Object> mapResponse = new HashMap<>();
                mapResponse.put("3ds_url", REQUEST_URL + "/pay?id=" + id);
                mapResponse.put("type", "link");
                mapResponse.put("transactionId", id);
                mapResponse.put("isDeeplink", false);
                response.response = mapResponse;
                response.returnCode = 200;
                return response.toString();
            }
            response.returnCode = 525;
            return response.toString();
        } catch (Exception ex) {
            logger.debug(requestId + " - getCheckoutURL exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response.toString();
        }
    }

    public String getPaymentStatus(String transactionId) throws Exception {
        ObjResponse response = new ObjResponse();
        try {
            long time = System.currentTimeMillis();
            Map<String, Object> payloadClaims = new HashMap<>();
            payloadClaims.put("id", transactionId);
            payloadClaims.put("msisdn", MSISDN);
            payloadClaims.put("iat", time);
            payloadClaims.put("exp", time + 60 * 60 * 4);

            String token = createJWSSignature(payloadClaims);

            JSONObject objBody = new JSONObject();
            objBody.put("token", token);
            objBody.put("merchantId", MERCHANT_ID);
            objBody.put("lang", "en");
            logger.debug(requestId + " - objBody = " + objBody.toJSONString());

            RequestBody body = RequestBody.create(objBody.toJSONString(), JSON);
            Request request = new Request.Builder()
                    .addHeader("Content-Type", "application/json")
                    .url(REQUEST_URL + "/get")
                    .post(body)
                    .build();

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60,TimeUnit.SECONDS)
                    .writeTimeout(60,TimeUnit.SECONDS)
                    .build();
            Response gwResponse = client.newCall(request).execute();
            String responseString = gwResponse.body() != null ? gwResponse.body().string() : "";
            logger.debug(requestId + " - response = " + responseString);
            org.json.JSONObject objResponse = new org.json.JSONObject(responseString);
            String status = objResponse.has("status") ? objResponse.getString("status") : "";
            logger.debug(requestId + " - status = " + status);
            Map<String, String> mapResponse = new HashMap<>();
            mapResponse.put("status", status);
            response.response = mapResponse;
            response.returnCode = 200;
            return response.toString();
        } catch (Exception ex) {
            logger.debug(requestId + " - getPaymentStatus exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response.toString();
        }
    }

    private String createJWSSignature(Map<String, Object> payloadClaims) throws Exception {
        String jws = "";
        try {
            Algorithm algorithm = Algorithm.HMAC256(SECRET_KEY);
            jws = JWT.create()
                    //.withIssuer("auth0")
                    .withPayload(payloadClaims)
                    .sign(algorithm);
        } catch (JWTCreationException ex){
            logger.debug(requestId + " - createJWSSignature exception: " + CommonUtils.getError(ex));
        }
        return jws;
    }

    public static void main(String[] args) throws Exception{

        MediaType JSON = MediaType.get("application/json; charset=utf-8");

        String id = "6152e63635ca4df245691cb1";
        String merchantId = "5ffacf6612b5777c6d44266f";
        String msisdn = "9647835077893";

        long time = System.currentTimeMillis();
        String url = "https://test.zaincash.iq/transaction/get";
        Map<String, Object> payloadClaims = new HashMap<>();
        payloadClaims.put("id", id);
        payloadClaims.put("msisdn", msisdn);
        payloadClaims.put("iat", time);
        payloadClaims.put("exp", time + 60 * 60 * 4);

        String secretKey = "$2y$10$hBbAZo2GfSSvyqAyV2SaqOfYewgYpfR1O19gIh4SqyGWdmySZYPuS";

        String requestURL = "https://test.zaincash.iq/transaction";
        ZainCashUtil zainCashUtil = new ZainCashUtil("", msisdn, merchantId, secretKey, requestURL);

        String token = zainCashUtil.createJWSSignature(payloadClaims);

        JSONObject objBody = new JSONObject();

        objBody.put("token", token);
        objBody.put("merchantId", merchantId);
        objBody.put("lang", "en");
        RequestBody body = RequestBody.create(objBody.toJSONString(), JSON);
        Request request = new Request.Builder()
                .addHeader("Content-Type", "application/json")
                .url(url)
                .post(body)
                .build();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60,TimeUnit.SECONDS)
                .writeTimeout(60,TimeUnit.SECONDS)
                .build();
        Response gwResponse = client.newCall(request).execute();
        String responseString = gwResponse.body() != null ? gwResponse.body().string() : "";
        logger.debug(" - response = " + responseString);

        /*String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdGF0dXMiOiJmYWlsZWQiLCJtc2c" +
                "iOiJzb2FwX2Nvbm5lY3Rpb25fZXJyb3IiLCJvcmRlcmlkIjoiRFcyMDIxMDkyODA5Mzk1NjEzMiIsImlkIjoiNjE1MmUyZWQzNW" +
                "NhNGRmMjQ1NjkxY2IwIiwiaWF0IjoxNjMyODIyMTA0LCJleHAiOjE2MzI4MjU3MDR9.wUOnb-O4-02eVxQ57wC4o9CrTCeveIEIB3uOAYst3Ps";

        String transactionId = "";
        boolean paymentSucceed = false;
        try {
            DecodedJWT jwt = JWT.decode(token);
            transactionId = jwt.getClaim("id") != null ? jwt.getClaim("id").asString() : "";
            String orderId = jwt.getClaim("orderid") != null ? jwt.getClaim("orderid").asString() : "";
            logger.debug(" - orderId: " + orderId);
            String status = jwt.getClaim("status") != null ? jwt.getClaim("status").asString() : "";
            logger.debug(" - status: " + status);
            String msg = jwt.getClaim("msg") != null ? jwt.getClaim("msg").asString() : "";
            logger.debug(" - msg: " + msg);
            if (status.equalsIgnoreCase("success")) {
                paymentSucceed = true;
            }
        } catch (JWTDecodeException ex){
            logger.debug(" - decode token exception: " + CommonUtils.getError(ex));
        }
        logger.debug(" - transactionId: " + transactionId);
        logger.debug(" - paymentSucceed: " + paymentSucceed);*/
    }
}
