package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.BaseEncoding;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.controllers.GatewayUtil.ApplePay.*;
import com.qupworld.paymentgateway.controllers.GatewayUtil.ApplePay.dto.PaymentData;
import com.qupworld.paymentgateway.entities.ApplePay.AppleToken;
import com.qupworld.paymentgateway.models.RedisDao;
import com.qupworld.paymentgateway.models.RedisImpl;
import com.pg.util.CommonUtils;
import com.pg.util.ObjResponse;
import com.upay.sdk.AESUtils;
import com.upay.sdk.Base64;
import com.upay.sdk.CipherConfigure;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import javax.crypto.Cipher;
import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.InetAddress;
import java.net.URL;
import java.security.*;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by thuanho on 20/05/2022.
 */
public class ApplePayPayease {

    private final static Logger logger = LogManager.getLogger(ApplePayPayease.class);

    public static final String CHAR_ENCODING = "UTF-8";
    public static final String AES_ALGORITHM = "AES/ECB/PKCS5Padding";
    public static final String RSA_ALGORITHM = "RSA/ECB/PKCS1Padding";

    private String requestId;
    private Gson gson;
    private String MERCHANT_ID;
    private String REQUEST_URL;
    private String CER_URL;
    private String PUBLIC_KEY_URL;
    private String PRIVATE_KEY_URL;
    private ObjectMapper jsonMapper = new ObjectMapper();
    private RedisDao redisDao = new RedisImpl();

    public ApplePayPayease(String _requestId, String merchantId, String cerUrl, String publicKeyUrl, String privateKeyUrl, String requestUrl) {
        requestId = _requestId;
        gson = new GsonBuilder().disableHtmlEscaping().create();
        MERCHANT_ID = merchantId;
        CER_URL = cerUrl;
        PUBLIC_KEY_URL = publicKeyUrl;
        PRIVATE_KEY_URL = privateKeyUrl;
        REQUEST_URL = requestUrl;
    }

    public String doPayment(String fleetId, String bookId, AppleToken paymentToken, double amount, String currencyISO) {
        ObjResponse response = new ObjResponse();
        int returnCode = 525;
        Map<String,String> mapResponse = new HashMap<>();
        mapResponse.put("bookId", bookId);
        try {
            String notifyUrl = ServerConfig.hook_server + "/webhookApple?provider=PayEase&command=notify";
            String callbackUrl = ServerConfig.hook_server + "/webhookApple?provider=PayEase&command=callback";

            // get payment info from paymentToken
            JSONObject jsonPayment = decryptPaymentToken(paymentToken);
            /* Sample
            {
              "applicationPrimaryAccountNumber": "626361XXXXXX0476",
              "applicationExpirationDate": "320101",
              "currencyCode": "156",
              "transactionAmount": 0,
              "deviceManufacturerIdentifier": "062010030273",
              "paymentDataType": "EMV",
              "paymentData": {
                "emvData": "nyYINYbGr65IYhSfNgIARIECBLeDcMs2IEKtAfgt9H5RqoA1DCOeTz6DCfyGGgu3BAoddnVlgb4dzGZaxPeCqYX2hmRu79MjD22VMCM2YZ3YYy7JUEX1zKOdBtUFsVg4zPW6xg7wpZDDnhp/aZqJ1d564PUgDoDSZ7SrZCP69io4sJBO6i0="
              }
            }
             */
            String apCardNo = jsonPayment.has("applicationPrimaryAccountNumber") ? jsonPayment.getString("applicationPrimaryAccountNumber") : "";
            JSONObject paymentData = jsonPayment.has("paymentData") ? jsonPayment.getJSONObject("paymentData") : null;
            String apTransData = paymentData != null && paymentData.has("emvData") ? paymentData.getString("emvData") : "";
            String apUserData = paymentData != null && paymentData.has("encryptedPINData") ? paymentData.getString("encryptedPINData") : "";

            // replace requestId
            String orderId = bookId + "-" + String.valueOf((int) (Math.random() * 1000000));
            DecimalFormat df = new DecimalFormat("#");
            int intAmount = Integer.parseInt(df.format(amount * 100));
            InetAddress IP = InetAddress.getLocalHost();
            OrderBuilder builder = new OrderBuilder(MERCHANT_ID);
            builder.setRequestId(orderId)
                    .setOrderAmount(String.valueOf(intAmount))
                    .setOrderCurrency(currencyISO)
                    .setNotifyUrl(notifyUrl)
                    .setCallbackUrl(callbackUrl)
                    .setClientIp(IP.getHostAddress());
            builder.setApCardNo(apCardNo);
            builder.setApUserData(apUserData);
            builder.setApTransData(apTransData);
            builder.setPaymentModeCode("APPLE_PAY");

            ProductDetail productDetail = new ProductDetail();
            productDetail.setName("Booking");
            productDetail.setQuantity(new Long("1"));
            productDetail.setAmount(new Long(String.valueOf(intAmount)));
            builder.addProductDetail(productDetail);

            /*
            1. If none Apple Pay, payer object need to include these
            "payer": {
                "bankCardNum": "6222021****5702", "email": "cc@cc.com",
                "idNum": "1*******6",
                "idType": "IDCARD",
                "name": "付款人姓名",
                "phoneNum": "139*****7"
            },
            similar YEEPAY, that was government required
            but for Apple Pay
            payer just {}
             */
            Payer payer = new Payer();
            builder.setPayer(payer);

            // 1. Sort the first letter of the key name:
            String encryptRaw = builder.getApCardNo() + "#"
                    + builder.getApTransData() + "#";
            if (!apUserData.isEmpty()) {
                encryptRaw += builder.getApUserData() + "#";
            }
            encryptRaw = encryptRaw
                    + builder.getCallbackUrl() + "#"
                    + builder.getClientIp() + "#"
                    + builder.getMerchantId() + "#"
                    + builder.getNotifyUrl() + "#"
                    + builder.getOrderAmount() + "#"
                    + builder.getOrderCurrency() + "#"
                    + builder.getPaymentModeCode() + "#"
                    + productDetail.getAmount() + "#"
                    + productDetail.getName() + "#"
                    + productDetail.getQuantity() + "#"
                    + builder.getRequestId() + "#";
            if (apCardNo.length() > 5) {
                String logEncrypt = encryptRaw.replace(apCardNo, apCardNo.substring(0,5) + "XXXXXX" + apCardNo.substring(apCardNo.length() - 4));
                logger.debug(requestId + " - encryptRaw = " + logEncrypt);
            } else {
                logger.debug(requestId + " - encryptRaw = " + encryptRaw);
            }

            // 2. SHA1 signature:
            MessageDigest sha = MessageDigest.getInstance("SHA");
            sha.update(encryptRaw.getBytes());
            byte[] encryptSHA = sha.digest();
            //logger.debug(requestId + " - encrypted string  = " + DigestUtil.toHex(encryptSHA));

            // 3. CFCA private key signature:
            String encryptCFCA = cfcaSignByPrivateKey(encryptSHA, getPrivateKey());
            /*logger.debug(requestId + " - encryptCFCA = " + encryptCFCA);*/

            // 4. Automatically generate a 16-digit aes key
            String aesKey = RandomStringUtils.randomAlphanumeric(16).toUpperCase();
            /*logger.debug(requestId + " - aesKey = " + aesKey);*/

            // 5. AES encryption:
            builder.setHmac(encryptCFCA);
            String logBuilder = gson.toJson(builder).replace(apCardNo, apCardNo.substring(0,5) + "XXXXXX" + apCardNo.substring(apCardNo.length() - 4));
            logger.debug(requestId + " - builder with HMAC = " + logBuilder);

            String encryptAES = AESUtils.encryptToBase64(gson.toJson(builder), aesKey);
            /*logger.debug(requestId + " - encryptAES = " + encryptAES);*/

            // 6. CFCA public key encryption
            String encryptAESKey = cfcaEncryptByPublicKey(aesKey, getPublicKey());
            /*logger.debug(requestId + " - encryptAESKey = " + encryptAESKey);*/

            StringBuilder sb = new StringBuilder();
            URL destURL = new URL(REQUEST_URL);
            MyTrustManager.trustAllHttpsCertificates();
            HttpsURLConnection urlConn = (HttpsURLConnection) destURL.openConnection();

            urlConn.setRequestProperty("Content-Type", "application/vnd.5upay-v3.0+json");
            urlConn.setRequestProperty("encryptKey", encryptAESKey);
            urlConn.setRequestProperty("merchantId", MERCHANT_ID);
            urlConn.setRequestProperty("requestId", orderId);

            urlConn.setDoOutput(true);
            urlConn.setDoInput(true);
            urlConn.setAllowUserInteraction(false);
            urlConn.setUseCaches(false);
            urlConn.setRequestMethod("POST");
            urlConn.setConnectTimeout(10*60*1000);
            urlConn.setReadTimeout(10*60*1000);

            String charSet = "GBK";
            OutputStream os = urlConn.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os, charSet);
            osw.write(encryptAES);
            osw.flush();
            osw.close();
            BufferedInputStream is = new BufferedInputStream(
                    urlConn.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is,
                    charSet));
            String temp = null;
            while ((temp = br.readLine()) != null) {
                sb.append(temp);
                sb.append("\n");
            }

            String encryptKey = urlConn.getHeaderField("encryptKey");
            String merchantId = urlConn.getHeaderField("merchantId");
            com.alibaba.fastjson15.JSONObject json = com.alibaba.fastjson15.JSONObject.parseObject(sb.toString());
            json.put("encryptKey", encryptKey);
            json.put("merchantId", merchantId);

            String returnData = json.toJSONString();
            logger.debug(requestId + " - returnData = " + returnData);

            String AESKey = decryptByPrivateKey(encryptKey, getPrivateKey());
            logger.debug(requestId + " - AESKey = " + AESKey);

            String data = json.getString("data");
            String realData = AESUtils.decryptFromBase64(data, AESKey);
            logger.debug(requestId + " - realData = " + realData);

            JSONObject objResponse = new JSONObject(realData);
            String status = objResponse.get("status") != null ? objResponse.getString("status") : "";
            if (status.equals("SUCCESS")) {
                // cache fleetId to retrieve after receiving notification
                redisDao.cacheData(requestId, orderId+"fleetId", fleetId, 1);

                String transactionId = objResponse.get("paymentOrderId") != null ? objResponse.getString("paymentOrderId") : "";
                mapResponse.put("transactionId", transactionId);
                returnCode = 200;

                // NOT YET complete - waiting for notification from PayEase in 10 second
                String notifyStatus = "";
                int i = 0;
                while (notifyStatus.isEmpty() && i < 10) {
                    logger.debug(requestId + " - waiting for notify status of orderId " + orderId);
                    notifyStatus =  redisDao.getTmp(requestId, orderId);
                    logger.debug(requestId + " - notifyStatus: " + notifyStatus);
                    if (!notifyStatus.isEmpty()) {
                        break;
                    } else {
                        Thread.sleep(1000);
                        i++;
                    }
                }

                if (notifyStatus.equals("SUCCESS")) {
                    returnCode = 200;
                } else {
                    returnCode = 525;
                }
                redisDao.removeTmp(requestId, orderId);
            } else {
                String error = objResponse.get("error") != null ? objResponse.getString("error") : "";
                if (!error.isEmpty()) {
                    mapResponse.put("message", error);
                }
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - doPayment exception: " + CommonUtils.getError(ex));
        }
        response.returnCode = returnCode;
        response.response = mapResponse;
        return response.toString();
    }

    public String doDecrypt(String encryptKey, String data) {
        String status = "";
        try {
            String AESKey = decryptByPrivateKey(encryptKey, getPrivateKey());
            logger.debug(requestId + " - AESKey = " + AESKey);

            String realData = AESUtils.decryptFromBase64(data, AESKey);
            logger.debug(requestId + " - realData = " + realData);

            JSONObject objResponse = new JSONObject(realData);
            status = objResponse.get("status") != null ? objResponse.getString("status") : "";
        } catch (Exception ex) {
            logger.debug(requestId + " - doPayment exception: " + CommonUtils.getError(ex));
        }
        return status;
    }

    private byte[] getPrivateKey(String privateKeyFile) throws Exception {
        DataInputStream dis = new DataInputStream(new FileInputStream(this.getClass().getClassLoader().getResource(privateKeyFile).getFile()));
        byte[] priKeyBytes = new byte[dis.available()];
        dis.readFully(priKeyBytes);
        dis.close();

        String temp = new String(priKeyBytes);
        temp = temp.replace("-----BEGIN PRIVATE KEY-----", "").replace("-----END PRIVATE KEY-----", "");//.replaceAll("\r", "").replaceAll("\n", "");
        return decryptBASE64(temp);
    }
    public JSONObject decryptPaymentToken(AppleToken paymentToken) throws Exception {
        PaymentData paymentData = jsonMapper.readValue(gson.toJson(paymentToken), PaymentData.class);

        byte[] symmetricKeyBytes = ApplePayCryptoUtil.restoreSymmetricKey(
                decryptBASE64(paymentData.getHeader().getWrappedKey()), getPrivateKey("privatePem.pem"));

        System.out.println(BaseEncoding.base16().encode(symmetricKeyBytes));

        byte[] decryptedDataBytes = ApplePayCryptoUtil.decryptData(symmetricKeyBytes, decryptBASE64(paymentData.getData()));
        String result = new String(decryptedDataBytes, "UTF-8");
        return new JSONObject(result);
    }

    private String storeFile(String requestId, String fileUrl, String fileName) {
        String tempFile = "";
        try {
            tempFile = "/tmp/" + fileName;
            File f = new File(tempFile);
            if(f.exists() && !f.isDirectory()) {
                logger.debug(requestId + " - " + fileName + " is existing !!!");
                return tempFile;
            } else {
                // write file to /tmp
                BufferedInputStream inputStream = new BufferedInputStream(new URL(fileUrl).openStream());
                FileOutputStream fileOS = new FileOutputStream(tempFile);
                byte data[] = new byte[1024];
                int byteContent;
                while ((byteContent = inputStream.read(data, 0, 1024)) != -1) {
                    fileOS.write(data, 0, byteContent);
                }
                logger.debug(requestId + " - create file " + tempFile + " done !!!");
            }
        } catch(Exception ex) {
            logger.debug(requestId + " - write tmp file error !!!" + CommonUtils.getError(ex));
        }
        return tempFile;
    }

    public static void main(String[] args) throws Exception {
        String requestId = UUID.randomUUID().toString();
        String merchantId = "899832096";
        String cerUrl = "https://gojo-global-static.s3.ap-southeast-1.amazonaws.com/avis/payease_public.cer";
        String publicKeyUrl = "https://gojo-global-static.s3.ap-southeast-1.amazonaws.com/avis/payease_public.key";
        String privateKeyUrl = "https://gojo-global-static.s3.ap-southeast-1.amazonaws.com/avis/payease_private_pkcs8.key";
        String requestUrl = "https://apis.5upay.com/onlinePay/order";
        ApplePayPayease applePayPayease = new ApplePayPayease(requestId, merchantId, cerUrl, publicKeyUrl, privateKeyUrl, requestUrl);

        String orderId = CommonUtils.getOrderId();
        AppleToken paymentToken = null;
        double amount = 0.1;
        String currencyISO = "CNY";
        String result = applePayPayease.doPayment("", orderId, paymentToken, amount, currencyISO);
        logger.debug(requestId + " - result: " + result);

        /*String json = "{\"version\":\"RSA_v1\",\"data\":\"7ue7itS5i0WtjqcHY3Pesk+TPGqcqtsu7I+zCKxO/e2a5GeA3KKLXVk0ccb8kVDoJulUFvtFcn0MPJ7NKF0wBAnS8+lgrVsep2S1vwhqYMpi+1HR/2bLolCjtmgBWvYp2XN+q4k+Cp1JUG6vV2MKpMVt3wOLLtU+RCWWYQHTg2PcnpGHpLqT2XKh3Uf6F4PLbYl6rfIDLA1cD5/teMJSCTA1Nmt/zNQtT+1GgFmCIRdR/t90AMPxToxzTY4UlvTSODq88ShJeIi+7IKjSysMZ03JizHH8dg+y/KpSvbAdLsUMWMoyM8daM4gsvE5pnbuazQ3fPgvTjU3NBxgGaRpSgvcflw6I68M5VStbpiT9wuZKyV+UNUZtNTVhL7jYJN6xcYAeu0kXG41+Ig88ClV1x0gE+uSv1RcTZZJ7MoUs4jv2pI5wH8j2GwlSmnBF0NRfXHMCYhVlCaf7yzgU98LqA3obVop/AnyziGg7sgeueI6jYeqBZi2zHQOT7hpIg9GngYGXuepwc7CMrAOOPzUOYPo3R2JV3kMP0h8u0zihFXbykR7GDVtvKUf08Oi\",\"signature\":\"MIAGCSqGSIb3DQEHAqCAMIACAQExDzANBglghkgBZQMEAgEFADCABgkqhkiG9w0BBwEAAKCAMIIEtjCCBFugAwIBAgIIbWQxaST07TAwCgYIKoZIzj0EAwIwejEuMCwGA1UEAwwlQXBwbGUgQXBwbGljYXRpb24gSW50ZWdyYXRpb24gQ0EgLSBHMzEmMCQGA1UECwwdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMB4XDTE5MDUzMTA0NTQxM1oXDTI0MDUyOTA0NTQxM1owZzEtMCsGA1UEAwwkZWNjLXNtcC1icm9rZXItc2lnbl9VQzQtUFJPRF9LcnlwdG9uMRQwEgYDVQQLDAtpT1MgU3lzdGVtczETMBEGA1UECgwKQXBwbGUgSW5jLjELMAkGA1UEBhMCVVMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDBRNEIluA8efJ+qjjcC6oRldDBPqTNcTSqivO24ZqXz9Pudacvfw4REUByWBWx5LRMm5GQwp2k5Vp4qWHUko3X09LMnqaq7x+44pRi0u3pAFgm6fZmY7QrnorQKIAFFvHn4+leubvdIcHbSQJ8xzrXSvf9dGjiZNig9PZWz7zJOW+AmzZx5oSSA4jbQe9/kvC+KNAKz6zS7Wq4yNOLk6Axel7QzW+66VD8Rrdo7A2tDoqQJ1Y6VKdDBZHlTa26ZTnBueXEIHRXAk55ZsLjTFVly1WbBIP8kZIONxdDrdLvGQe4sqX4V6GVvoJr7tHSI+SA+rPfY9B5qj7iXwNAVuUlAgMBAAGjggIRMIICDTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFCPyScRPk+TvJ+bE9ihsP6K7/S5LMEUGCCsGAQUFBwEBBDkwNzA1BggrBgEFBQcwAYYpaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwNC1hcHBsZWFpY2EzMDMwggEdBgNVHSAEggEUMIIBEDCCAQwGCSqGSIb3Y2QFATCB/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMDQGA1UdHwQtMCswKaAnoCWGI2h0dHA6Ly9jcmwuYXBwbGUuY29tL2FwcGxlYWljYTMuY3JsMB0GA1UdDgQWBBS19Y4mQcuxFO3LCXUoceHL7vlCpjAOBgNVHQ8BAf8EBAMCB4AwDwYJKoZIhvdjZAYdBAIFADAKBggqhkjOPQQDAgNJADBGAiEAySGKxkWz67DMHCgYBhAcmFjL+XYZSKD8PJ+ospYZHIYCIQDJq5zdZJ+A9/hBrwSL5jc1j6YQStoXGTRjvbs7D0PQBTCCAu4wggJ1oAMCAQICCEltL786mNqXMAoGCCqGSM49BAMCMGcxGzAZBgNVBAMMEkFwcGxlIFJvb3QgQ0EgLSBHMzEmMCQGA1UECwwdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMB4XDTE0MDUwNjIzNDYzMFoXDTI5MDUwNjIzNDYzMFowejEuMCwGA1UEAwwlQXBwbGUgQXBwbGljYXRpb24gSW50ZWdyYXRpb24gQ0EgLSBHMzEmMCQGA1UECwwdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE8BcRhBnXZIXVGl4lgQd26ICi7957rk3gjfxLk+EzVtVmWzWuItCXdg0iTnu6CP12F86Iy3a7ZnC+yOgphP9URaOB9zCB9DBGBggrBgEFBQcBAQQ6MDgwNgYIKwYBBQUHMAGGKmh0dHA6Ly9vY3NwLmFwcGxlLmNvbS9vY3NwMDQtYXBwbGVyb290Y2FnMzAdBgNVHQ4EFgQUI/JJxE+T5O8n5sT2KGw/orv9LkswDwYDVR0TAQH/BAUwAwEB/zAfBgNVHSMEGDAWgBS7sN6hWDOImqSKmd6+veuv2sskqzA3BgNVHR8EMDAuMCygKqAohiZodHRwOi8vY3JsLmFwcGxlLmNvbS9hcHBsZXJvb3RjYWczLmNybDAOBgNVHQ8BAf8EBAMCAQYwEAYKKoZIhvdjZAYCDgQCBQAwCgYIKoZIzj0EAwIDZwAwZAIwOs9yg1EWmbGG+zXDVspiv/QX7dkPdU2ijr7xnIFeQreJ+Jj3m1mfmNVBDY+d6cL+AjAyLdVEIbCjBXdsXfM4O5Bn/Rd8LCFtlk/GcmmCEm9U+Hp9G5nLmwmJIWEGmQ8Jkh0AADGCAk0wggJJAgEBMIGGMHoxLjAsBgNVBAMMJUFwcGxlIEFwcGxpY2F0aW9uIEludGVncmF0aW9uIENBIC0gRzMxJjAkBgNVBAsMHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUwIIbWQxaST07TAwDQYJYIZIAWUDBAIBBQCggZgwGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMjIwNTMxMDIwNjU3WjAtBgkqhkiG9w0BCTQxIDAeMA0GCWCGSAFlAwQCAQUAoQ0GCSqGSIb3DQEBCwUAMC8GCSqGSIb3DQEJBDEiBCAEKc67oeT1u+gGFrZAFdyGqXAcafoqe2inF9lVtBOWaTANBgkqhkiG9w0BAQsFAASCAQB2GbTwKZJBHwytMOKYiBV0yTALdheBNSwvo//i0AZmzRr/dHYqCvAmAtI4yK3jWKkMgPCxy8usEIA+M2jUZoH/1vLqrZ68Ir4ddDVkdaNZFdnQUhv1VqfyXPFr0xw2QW5HO97pW/yuocWUFT4Mgqluc0KiaEzXmuiyfqmfb/0pvgbgvTIrt451IvSe5WEsdTHEviVustL2sSk1HufbqnV08xH/ltL9MQfx4EurRJczPHAmQhSrRW4tF3xtGtu7fEHhJ+XAAj77dt1MdsydGPQ6sVNwVUXNDxBOh+bMC9EDe9gPdbnzhrxV0Tb/+ciUPfuUZy2Hk340WolkKn7Je6geAAAAAAAA\",\"header\":{\"publicKeyHash\":\"IFpxwKG2Wye+DxFOLRujNkMKm9nbSUGMN/tjIk7gTEU=\",\"transactionId\":\"ae1e809f9c3432f2a193136dc796ea010d597270e021c17e909435b2a72ac893\",\"wrappedKey\":\"F72MiP7Lw/smMzl8TSZDrc5feInvlz6PZP9nxsiLwkOSDE8dthV/jbyrrrFeXXVYjlY8zlYjk6vSQbQLlGydq1DdNmVtYh5p0a1802LuuBu+TZOgpXrkUD0i5Q1A8C6Xzgo98OZAj+OaKQ0I6B8uCrnhvKSg9ePbpLGShFPUAQzJumZQp8dSPwm5dFqAXVgi8s73TTGNHre988CI8jrFRiiW6iK32U9zW7+w0E9pDoU2Z+k3h9efUcTRPSiHQ8g29eemxNaJOmgICeF507Jp8P8/kw+vCJQkw7/nhLGE6zn65kaVhWtoLs8ny4PRKrDEx/Qlqp7mMjpPdXq0vMMmEA==\"}}";
        JSONObject object = new JSONObject(json);
        String version = object.getString("version");
        System.out.println(version);*/

    }


    private String getPrivateKey() {
        String privateKey = "";
        try{
            String getFile = storeFile(requestId, PRIVATE_KEY_URL, "payease_private_pkcs8.key");
            InputStream inputStream = new FileInputStream(getFile);
            StringBuilder resultStringBuilder = new StringBuilder();
            try (BufferedReader br
                         = new BufferedReader(new InputStreamReader(inputStream))) {
                String line;
                while ((line = br.readLine()) != null) {
                    resultStringBuilder.append(line).append("\n");
                }
            }
            String data = resultStringBuilder.toString().trim();
            privateKey = data.replaceAll("\\n", "").replace("-----BEGIN PRIVATE KEY-----", "").replace("-----END PRIVATE KEY-----", "");
        }catch(Exception exception){
            exception.printStackTrace();
        }
        return privateKey;
    }

    private String getPublicKey() {
        String publicKey = "";
        try{
            String getFile = storeFile(requestId, PUBLIC_KEY_URL, "payease_public.key");
            InputStream inputStream = new FileInputStream(getFile);
            StringBuilder resultStringBuilder = new StringBuilder();
            try (BufferedReader br
                         = new BufferedReader(new InputStreamReader(inputStream))) {
                String line;
                while ((line = br.readLine()) != null) {
                    resultStringBuilder.append(line).append("\n");
                }
            }
            String data = resultStringBuilder.toString().trim();
            publicKey = data.replaceAll("\\n", "").replace("-----BEGIN PUBLIC KEY-----", "").replace("-----END PUBLIC KEY-----", "");
        }catch(Exception exception){
            exception.printStackTrace();
        }
        return publicKey;
    }

    public static String cfcaSignByPrivateKey(byte[] data, String privateKey)
            throws Exception {
        byte[] keyBytes = decryptBASE64(privateKey);
        PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PrivateKey privateKey2 = keyFactory.generatePrivate(pkcs8EncodedKeySpec);
        Signature signature = Signature.getInstance("MD5withRSA");
        signature.initSign(privateKey2);
        signature.update(data);
        return new String(Base64.encodeBase64(signature.sign()));
    }

    public static byte[] decryptBASE64(String key)
            throws Exception {
        return Base64.decodeBase64(key.getBytes(CipherConfigure.CHAR_ENCODING));
    }


    private String cfcaEncryptByPublicKey(String source, String publicKey)
            throws Exception {
        //Key key = getPublicKeyByString(publicKey);
        Key key = getPublicKeyByString();
        /** 得到Cipher对象来实现对源数据的RSA加密 */
        Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] b = source.getBytes(CHAR_ENCODING);
        /** 执行加密操作 */
        byte[] b1 = cipher.doFinal(b);
        return new String(Base64.encodeBase64(b1), CHAR_ENCODING);
    }

    private static Key getPublicKeyByString(String key) throws Exception {
        byte[] keyBytes = decryptBASE64(key);
        X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        Key publicKey = keyFactory.generatePublic(x509EncodedKeySpec);
        return publicKey;
    }

    private Key getPublicKeyByString() throws Exception {
        try{
            String getFile = storeFile(requestId, CER_URL, "payease_public.cer");
            InputStream inputStream = new FileInputStream(getFile);
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            X509Certificate cert =
                    (X509Certificate)cf.generateCertificate(inputStream);
            RSAPublicKey pubkey = (RSAPublicKey) cert.getPublicKey();
            return pubkey;
        }catch(Exception exception){
            exception.printStackTrace();
        }
        return null;
    }

    private String decryptByPrivateKey(String cryptograph, String privateKey)
            throws Exception {
        Key key = getPrivateKeyByString(privateKey);
        /** 得到Cipher对象对已用公钥加密的数据进行RSA解密 */
        Cipher cipher = Cipher.getInstance(CipherConfigure.RSA_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] b1 = Base64.decodeBase64(cryptograph.getBytes(CipherConfigure.CHAR_ENCODING));
        /** 执行解密操作 */
        byte[] b = cipher.doFinal(b1);
        return new String(b);
    }
    private Key getPrivateKeyByString(String key) throws Exception {
        byte[] keyBytes = decryptBASE64(key);
        PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        Key privateKey = keyFactory.generatePrivate(pkcs8EncodedKeySpec);
        return privateKey;
    }
}
