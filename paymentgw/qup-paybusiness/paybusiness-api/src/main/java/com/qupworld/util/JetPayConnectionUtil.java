package com.qupworld.util;

import com.google.gson.Gson;
import com.qupworld.util.jetpay.TransactionResponse;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class JetPayConnectionUtil {

    private static final Logger _log = LogManager.getLogger(JetPayConnectionUtil.class);
    private String requestId;
    public JetPayConnectionUtil(String requestId) {
        this.requestId = requestId;
    }
    public TransactionResponse createCreditToken(String serverUrl, Map<String,String> mapData, boolean enableCheckZipCode) throws Exception {
        // implement TSL 1.2
        SSLContext sslctx = SSLContext.getInstance("TLSv1.2");
        sslctx.init(null, null, null);
        HttpsURLConnection.setDefaultSSLSocketFactory(sslctx.getSocketFactory());


        URL url = new URL(serverUrl);
        String query = "<JetPay Version='2.2'>" +
                "<TransactionType>AUTHONLY</TransactionType>" +
                "<TerminalID>$terminalId</TerminalID>" +
                "<TransactionID>$transactionId</TransactionID>" +
                "<CardNum Tokenize='true' CardPresent='false'>$cardNumber</CardNum>" +
                "<CardExpMonth>$expiredMonth</CardExpMonth>" +
                "<CardExpYear>$expiredYear</CardExpYear>" +
                "<CardName>$cardHolder</CardName>" +
                "<CVV2>$cvv</CVV2>" +
                "<TotalAmount>100</TotalAmount>" +
                /*"<ReaderUsed>KEYPAD</ReaderUsed>" +*/
                "<Origin>INTERNET</Origin>" +
                "<IndustryInfo Type='ECOMMERCE'></IndustryInfo>" +
                "<Application Version='1.0'>qupdispatcher</Application>" +
                "<Device Version='1.0'>qupdispatcher</Device>" +
                "<Library Version='1.5'>VirtPOS SDK</Library>" +
                "<Gateway>JetPay</Gateway>" +
                "<DeveloperID>pdHdpT</DeveloperID>";

        query = query + "<Billing>";
        if (enableCheckZipCode) {
            query = query +
                    "<Address>$address</Address>" +
                    "<City>$city</City>" +
                    "<PostalCode>$zipCode</PostalCode>" +
                    "<Country>$country</Country>";

            if (mapData.get("state") != null && !mapData.get("state").isEmpty()) {
                query = query +
                        "<StateProv>$state</StateProv>";
            }
        }
        if (mapData.get("phone") != null && !mapData.get("phone").isEmpty()) {
            query = query +
                    "<Phone>$phone</Phone>";
        }
        if (mapData.get("customerEmail") != null && !mapData.get("customerEmail").isEmpty()) {
            query = query +
                    "<Email>$customerEmail</Email>";
        }
        query = query + "</Billing>";
        if (mapData.get("customerIP") != null && !mapData.get("customerIP").isEmpty()) {
            query = query +
                    "<UserIPAddress>$customerIP</UserIPAddress>";
        }
        query = query + "</JetPay>";

        String transactionId = RandomStringUtils.randomAlphanumeric(18).toUpperCase();  // required string length
        query = query.replace("$terminalId", mapData.get("terminalId"));
        query = query.replace("$transactionId", transactionId);
        query = query.replace("$cardHolder", mapData.get("cardHolder"));
        query = query.replace("$cardNumber", mapData.get("cardNumber"));
        query = query.replace("$expiredMonth", mapData.get("expiredMonth"));
        query = query.replace("$expiredYear", mapData.get("expiredYear"));
        query = query.replace("$cvv", mapData.get("cvv"));
        query = query.replace("$address", mapData.get("street"));
        query = query.replace("$city", mapData.get("city"));
        query = query.replace("$state", mapData.get("state"));
        query = query.replace("$zipCode", mapData.get("zipCode"));
        query = query.replace("$country", mapData.get("country"));
        query = query.replace("$phone", mapData.get("phone"));
        query = query.replace("$customerEmail", mapData.get("customerEmail"));
        query = query.replace("$customerIP", mapData.get("customerIP"));

        String cardNumber = mapData.get("cardNumber");
        String queryLog = query.replace(cardNumber, "XXXXXXXX" + cardNumber.substring(cardNumber.length() - 4));
        _log.debug(requestId + " - query: " + queryLog);

        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/xml");
        con.setRequestProperty(HttpHeaders.CONTENT_LENGTH, String.valueOf(query.length()));
        con.setDoOutput(true);
        con.setDoInput(true);

        DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.close();

        return getResponse(con);
    }

    public TransactionResponse createPaymentWithCreditToken(String serverUrl, Map<String,String> mapData) throws Exception {
        // implement TSL 1.2
        SSLContext sslctx = SSLContext.getInstance("TLSv1.2");
        sslctx.init(null, null, null);
        HttpsURLConnection.setDefaultSSLSocketFactory(sslctx.getSocketFactory());

        URL url = new URL(serverUrl);
        String query = "<JetPay Version='2.2'>" +
                "<TransactionType>SALE</TransactionType>" +
                "<TerminalID>$terminalId</TerminalID>" +
                "<TransactionID>$transactionId</TransactionID>" +
                "<Token>$token</Token>" +
                "<TotalAmount>$amount</TotalAmount>" +
                "<Origin>INTERNET</Origin>" +
                "<IndustryInfo Type='ECOMMERCE'></IndustryInfo>" +
                "<Application Version='1.0'>qupdispatcher</Application>" +
                "<Device Version='1.0'>qupdispatcher</Device>" +
                "<Library Version='1.5'>VirtPOS SDK</Library>" +
                "<Gateway>JetPay</Gateway>" +
                "<DeveloperID>pdHdpT</DeveloperID>";

        query = query + "<Billing>";
        if (mapData.get("street") != null && !mapData.get("street").isEmpty()) {
            query = query +
                    "<Address>$address</Address>";
        }
        if (mapData.get("city") != null && !mapData.get("city").isEmpty()) {
            query = query +
                    "<City>$city</City>";
        }
        if (mapData.get("state") != null && !mapData.get("state").isEmpty()) {
            query = query +
                    "<StateProv>$state</StateProv>";
        }
        if (mapData.get("zipCode") != null && !mapData.get("zipCode").isEmpty()) {
            query = query +
                    "<PostalCode>$zipCode</PostalCode>";
        }
        if (mapData.get("country") != null && !mapData.get("country").isEmpty()) {
            query = query +
                    "<Country>$country</Country>";
        }
        if (mapData.get("phone") != null && !mapData.get("phone").isEmpty()) {
            query = query +
                    "<Phone>$phone</Phone>";
        }
        if (mapData.get("customerEmail") != null && !mapData.get("customerEmail").isEmpty()) {
            query = query +
                    "<Email>$customerEmail</Email>";
        }
        query = query + "</Billing>";
        if (mapData.get("customerIP") != null && !mapData.get("customerIP").isEmpty()) {
            query = query +
                    "<UserIPAddress>$customerIP</UserIPAddress>";
        }
        query = query + "</JetPay>";

        String transactionId = RandomStringUtils.randomAlphanumeric(18).toUpperCase();  // required string length
        query = query.replace("$terminalId", mapData.get("terminalId"));
        query = query.replace("$transactionId", transactionId);
        query = query.replace("$token", mapData.get("token"));
        query = query.replace("$amount", mapData.get("amount"));
        query = query.replace("$address", mapData.get("street"));
        query = query.replace("$city", mapData.get("city"));
        query = query.replace("$state", mapData.get("state"));
        query = query.replace("$zipCode", mapData.get("zipCode"));
        query = query.replace("$country", mapData.get("country"));
        query = query.replace("$phone", mapData.get("phone"));
        query = query.replace("$customerEmail", mapData.get("customerEmail"));
        query = query.replace("$customerIP", mapData.get("customerIP"));
        _log.debug(requestId + " - query: " + query);

        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/xml");
        con.setRequestProperty(HttpHeaders.CONTENT_LENGTH, String.valueOf(query.length()));
        con.setDoOutput(true);
        con.setDoInput(true);

        DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.close();

        return getResponse(con);
    }

    public TransactionResponse voiJetpayTransaction(String serverUrl, Map<String,String> mapData) throws Exception {
        // implement TSL 1.2
        SSLContext sslctx = SSLContext.getInstance("TLSv1.2");
        sslctx.init(null, null, null);
        HttpsURLConnection.setDefaultSSLSocketFactory(sslctx.getSocketFactory());

        URL url = new URL(serverUrl);
        String transactionId = RandomStringUtils.randomAlphanumeric(18).toUpperCase();  // required string length
        String query = "<JetPay Version='2.2'>" +
                "<TransactionType>VOID</TransactionType>" +
                "<TerminalID>$terminalId</TerminalID>" +
                "<TransactionID>$transactionId</TransactionID>" +
                "<UniqueID>$uniqueId</UniqueID>" +
                "<Origin>INTERNET</Origin>" +
                "<IndustryInfo Type='ECOMMERCE'></IndustryInfo>" +
                "<Application Version='1.0'>qupdispatcher</Application>" +
                "<Device Version='1.0'>qupdispatcher</Device>" +
                "<Library Version='1.5'>VirtPOS SDK</Library>" +
                "<Gateway>JetPay</Gateway>" +
                "<DeveloperID>pdHdpT</DeveloperID>" +
                "</JetPay>";

        query = query.replace("$terminalId", mapData.get("terminalId"));
        query = query.replace("$transactionId", transactionId);
        query = query.replace("$uniqueId", mapData.get("uniqueId"));
        _log.debug(requestId + " - query: " + query);

        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/xml");
        con.setRequestProperty(HttpHeaders.CONTENT_LENGTH, String.valueOf(query.length()));
        con.setDoOutput(true);
        con.setDoInput(true);

        DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.close();

        return getResponse(con);
    }

    public TransactionResponse createPaymentWithCardInfo(String serverUrl, Map<String,String> mapData, boolean enableCheckZipCode) throws Exception {
        // implement TSL 1.2
        SSLContext sslctx = SSLContext.getInstance("TLSv1.2");
        sslctx.init(null, null, null);
        HttpsURLConnection.setDefaultSSLSocketFactory(sslctx.getSocketFactory());
        URL url = new URL(serverUrl);
        String query = "<JetPay Version='2.2'>" +
                "<TransactionType>SALE</TransactionType>" +
                "<TerminalID>$terminalId</TerminalID>" +
                "<TransactionID>$transactionId</TransactionID>" +
                "<CardNum CardPresent='false'>$cardNumber</CardNum>" +
                "<CardExpMonth>$expiredMonth</CardExpMonth>" +
                "<CardExpYear>$expiredYear</CardExpYear>" +
                "<TotalAmount>$amount</TotalAmount>" +
                "<CardName>$cardHolder</CardName>" +
                "<CVV2>$cvv</CVV2>" +
                /*"<ReaderUsed>KEYPAD</ReaderUsed>" +*/
                "<Origin>INTERNET</Origin>" +
                "<IndustryInfo Type='ECOMMERCE'></IndustryInfo>" +
                "<Application Version='1.0'>qupdispatcher</Application>" +
                "<Device Version='1.0'>qupdispatcher</Device>" +
                "<Library Version='1.5'>VirtPOS SDK</Library>" +
                "<Gateway>JetPay</Gateway>" +
                "<DeveloperID>pdHdpT</DeveloperID>";

        query = query + "<Billing>";
        if (enableCheckZipCode) {
            query = query +
                    "<Address>$address</Address>" +
                    "<City>$city</City>" +
                    "<PostalCode>$zipCode</PostalCode>" +
                    "<Country>$country</Country>";

            if (mapData.get("state") != null && !mapData.get("state").isEmpty()) {
                query = query +
                        "<StateProv>$state</StateProv>";
            }
        }
        if (mapData.get("phone") != null && !mapData.get("phone").isEmpty()) {
            query = query +
                    "<Phone>$phone</Phone>";
        }
        if (mapData.get("customerEmail") != null && !mapData.get("customerEmail").isEmpty()) {
            query = query +
                    "<Email>$customerEmail</Email>";
        }
        query = query + "</Billing>";
        if (mapData.get("customerIP") != null && !mapData.get("customerIP").isEmpty()) {
            query = query +
                    "<UserIPAddress>$customerIP</UserIPAddress>";
        }
        query = query + "</JetPay>";

        String transactionId = RandomStringUtils.randomAlphanumeric(18).toUpperCase();  // required string length
        query = query.replace("$terminalId", mapData.get("terminalId"));
        query = query.replace("$transactionId", transactionId);
        query = query.replace("$cardHolder", mapData.get("cardHolder"));
        query = query.replace("$cardNumber", mapData.get("cardNumber"));
        query = query.replace("$expiredMonth", mapData.get("expiredMonth"));
        query = query.replace("$expiredYear", mapData.get("expiredYear"));
        query = query.replace("$cvv", mapData.get("cvv"));
        query = query.replace("$address", mapData.get("street"));
        query = query.replace("$city", mapData.get("city"));
        query = query.replace("$state", mapData.get("state"));
        query = query.replace("$zipCode", mapData.get("zipCode"));
        query = query.replace("$country", mapData.get("country"));
        query = query.replace("$phone", mapData.get("phone"));
        query = query.replace("$customerEmail", mapData.get("customerEmail"));
        query = query.replace("$customerIP", mapData.get("customerIP"));
        query = query.replace("$amount", mapData.get("amount"));

        String cardNumber = mapData.get("cardNumber");
        String queryLog = query.replace(cardNumber, "XXXXXXXX" + cardNumber.substring(cardNumber.length() - 4));
        _log.debug(requestId + " - query: " + queryLog);

        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/xml");
        con.setRequestProperty(HttpHeaders.CONTENT_LENGTH, String.valueOf(query.length()));
        con.setDoOutput(true);
        con.setDoInput(true);

        DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.close();

        return getResponse(con);
    }

    public TransactionResponse createACHToken(String serverUrl, Map<String,String> mapData) throws Exception {
        // implement TSL 1.2
        SSLContext sslctx = SSLContext.getInstance("TLSv1.2");
        sslctx.init(null, null, null);
        HttpsURLConnection.setDefaultSSLSocketFactory(sslctx.getSocketFactory());

        URL url = new URL(serverUrl);
        String query = "<JetPay Version='2.2'>" +
                "<TransactionType>TOKENIZE</TransactionType>" +
                "<TerminalID>$terminalId</TerminalID>" +
                "<TransactionID>$transactionId</TransactionID>" +
                "<CardName>$cardName</CardName>" +
                "<ACH>" +
                "<AccountNumber>$accountNumber</AccountNumber>" +
                "<ABA>$routingNumber</ABA>" +
                "<CheckNumber>$checkNumber</CheckNumber>" +
                "</ACH>" +
                "<Application Version='1.0'>qupdispatcher</Application>" +
                "<Device Version='1.0'>qupdispatcher</Device>" +
                "<Library Version='1.5'>VirtPOS SDK</Library>" +
                "<Gateway>JetPay</Gateway>" +
                "<DeveloperID>pdHdpT</DeveloperID>" +
                "</JetPay>";

        String transactionId = RandomStringUtils.randomAlphanumeric(18).toUpperCase();  // required string length
        query = query.replace("$terminalId", mapData.get("terminalId"));
        query = query.replace("$transactionId", transactionId);
        query = query.replace("$cardName", mapData.get("cardName"));
        query = query.replace("$accountNumber", mapData.get("accountNumber"));
        query = query.replace("$routingNumber", mapData.get("routingNumber"));
        query = query.replace("$checkNumber", mapData.get("checkNumber"));
        _log.debug(requestId + " - query: " + query);
        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/xml");
        con.setRequestProperty(HttpHeaders.CONTENT_LENGTH, String.valueOf(query.length()));
        con.setDoOutput(true);
        con.setDoInput(true);

        DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.close();

        return getResponse(con);
    }

    public TransactionResponse payToBankAccount(String serverUrl, Map<String,String> mapData) throws Exception {
        // implement TSL 1.2
        SSLContext sslctx = SSLContext.getInstance("TLSv1.2");
        sslctx.init(null, null, null);
        HttpsURLConnection.setDefaultSSLSocketFactory(sslctx.getSocketFactory());

        URL url = new URL(serverUrl);
        String query = "<JetPay Version='2.2'>" +
                "<TransactionType>REVERSAL</TransactionType>" +
                "<TerminalID>$terminalId</TerminalID>" +
                "<TransactionID>$transactionId</TransactionID>" +
                "<Origin>INTERNET</Origin>" +
                "<CardName>$cardName</CardName>" +
                "<TotalAmount>$amount</TotalAmount>" +
                "<Token>$token</Token>" +
                "<ACH>" +
                "<CheckNumber>$checkNumber</CheckNumber>" +
                "</ACH>" +
                "<Application Version='1.0'>qupdispatcher</Application>" +
                "<Device Version='1.0'>qupdispatcher</Device>" +
                "<Library Version='1.5'>VirtPOS SDK</Library>" +
                "<Gateway>JetPay</Gateway>" +
                "<DeveloperID>pdHdpT</DeveloperID>" +
                "</JetPay>";

        String transactionId = RandomStringUtils.randomAlphanumeric(18).toUpperCase();  // required string length
        query = query.replace("$transactionId", transactionId);
        query = query.replace("$terminalId", mapData.get("terminalId"));
        query = query.replace("$cardName", mapData.get("cardName"));
        query = query.replace("$amount", mapData.get("amount"));
        query = query.replace("$token", mapData.get("token"));
        query = query.replace("$checkNumber", mapData.get("checkNumber"));
        _log.debug(requestId + " - query: " + query);
        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/xml");
        con.setRequestProperty(HttpHeaders.CONTENT_LENGTH, String.valueOf(query.length()));
        con.setDoOutput(true);
        con.setDoInput(true);

        DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.close();

        return getResponse(con);
    }

    private TransactionResponse getResponse(HttpsURLConnection con){
        TransactionResponse transactionResponse= new TransactionResponse();
        try {
            if(HttpsURLConnection.HTTP_OK == con.getResponseCode()){
                BufferedReader br = null;
                StringBuilder sb = new StringBuilder();
                String line;
                try {
                    br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    if (br != null) {
                        try {
                            br.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                _log.debug(requestId + " - response: " + sb.toString());
                // convert XML string to XML Document
                DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(sb.toString()));
                Document doc = db.parse(is);
                doc.getDocumentElement().normalize();

                // read data from XML Document
                NodeList nList = doc.getElementsByTagName(doc.getDocumentElement().getNodeName());
                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node nNode = nList.item(temp);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        if (eElement.getElementsByTagName("ActionCode").item(0) != null)
                            transactionResponse.setActionCode(eElement.getElementsByTagName("ActionCode").item(0).getTextContent());
                        if (eElement.getElementsByTagName("AddressMatch").item(0) != null)
                            transactionResponse.setAddressMatch(eElement.getElementsByTagName("AddressMatch").item(0).getTextContent());
                        if (eElement.getElementsByTagName("AdditionalInfo").item(0) != null)
                            transactionResponse.setAdditionalInfo(eElement.getElementsByTagName("AdditionalInfo").item(0).getTextContent());
                        if (eElement.getElementsByTagName("Approval").item(0) != null)
                            transactionResponse.setApproval(eElement.getElementsByTagName("Approval").item(0).getTextContent());
                        if (eElement.getElementsByTagName("AuthorizedAmount").item(0) != null)
                            transactionResponse.setAuthorizedAmount(eElement.getElementsByTagName("AuthorizedAmount").item(0).getTextContent());
                        if (eElement.getElementsByTagName("AVS").item(0) != null)
                            transactionResponse.setAvs(eElement.getElementsByTagName("AVS").item(0).getTextContent());
                        if (eElement.getElementsByTagName("BalanceAmount").item(0) != null)
                            transactionResponse.setBalanceAmount(eElement.getElementsByTagName("BalanceAmount").item(0).getTextContent());
                        if (eElement.getElementsByTagName("BatchID").item(0) != null)
                            transactionResponse.setBatchID(eElement.getElementsByTagName("BatchID").item(0).getTextContent());
                        if (eElement.getElementsByTagName("CVV2").item(0) != null)
                            transactionResponse.setCvv2(eElement.getElementsByTagName("CVV2").item(0).getTextContent());
                        if (eElement.getElementsByTagName("ErrMsg").item(0) != null)
                            transactionResponse.setErrMsg(eElement.getElementsByTagName("ErrMsg").item(0).getTextContent());
                        if (eElement.getElementsByTagName("PurchaseCardType").item(0) != null)
                            transactionResponse.setPurchaseCardType(eElement.getElementsByTagName("PurchaseCardType").item(0).getTextContent());
                        if (eElement.getElementsByTagName("RawResponseCode").item(0) != null)
                            transactionResponse.setRawResponseCode(eElement.getElementsByTagName("RawResponseCode").item(0).getTextContent());
                        if (eElement.getElementsByTagName("ResponseText").item(0) != null)
                            transactionResponse.setResponseText(eElement.getElementsByTagName("ResponseText").item(0).getTextContent());
                        if (eElement.getElementsByTagName("RRN").item(0) != null)
                            transactionResponse.setRrn(eElement.getElementsByTagName("RRN").item(0).getTextContent());
                        if (eElement.getElementsByTagName("Token").item(0) != null)
                            transactionResponse.setToken(eElement.getElementsByTagName("Token").item(0).getTextContent());
                        if (eElement.getElementsByTagName("TransactionID").item(0) != null)
                            transactionResponse.setTransactionID(eElement.getElementsByTagName("TransactionID").item(0).getTextContent());
                        if (eElement.getElementsByTagName("UniqueID").item(0) != null)
                            transactionResponse.setUniqueID(eElement.getElementsByTagName("UniqueID").item(0).getTextContent());
                        if (eElement.getElementsByTagName("VerificationResult").item(0) != null)
                            transactionResponse.setVerificationResult(eElement.getElementsByTagName("VerificationResult").item(0).getTextContent());
                        if (eElement.getElementsByTagName("ZipMatch").item(0) != null)
                            transactionResponse.setZipMatch(eElement.getElementsByTagName("ZipMatch").item(0).getTextContent());

                    }
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        Gson gson = new Gson();
        _log.debug(requestId + " - transactionResponse: " + gson.toJson(transactionResponse));
        return transactionResponse;
    }

    public Map<String,Object> payByTerminal(Map<String,String> mapData) throws Exception {
        Map<String,Object> response = new HashMap<>();
        Map<String, String> mapResponse = new HashMap<>();

        String query = "" +
                "<request>" +
                "   <PaymentType>Card</PaymentType>" + // no need to put PaymentType
                "   <TransType>Sale</TransType>" +
                "   <Amount>$amount</Amount>" +
                "   <Tip>$tip</Tip>" +
                "   <Frequency>OneTime</Frequency>" +
                "   <InvNum></InvNum>" +
                "   <RefId>$refId</RefId>" +
                "   <RegisterId>$registerId</RegisterId>" +
                "   <AuthKey>$authKey</AuthKey>" +
                "   <PrintReceipt>Customer</PrintReceipt>" +
                "</request>";


        query = query.replace("$cardType", mapData.get("cardType"));
        query = query.replace("$registerId", mapData.get("registerId"));
        query = query.replace("$authKey", mapData.get("authKey"));
        query = query.replace("$amount", mapData.get("amount"));
        query = query.replace("$tip", mapData.get("tip"));
        query = query.replace("$refId", mapData.get("refId"));

        String serverUrl = mapData.get("serverUrl") + "?";
        List<NameValuePair> params = new LinkedList<>();
        params.add(new BasicNameValuePair("TerminalTransaction", query.replace(" ", "")));
        String paramString = URLEncodedUtils.format(params, "utf-8");

        serverUrl += paramString;
        _log.debug(requestId + " - query: " + query);

        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(serverUrl);

        // add header
        request.setHeader(HttpHeaders.USER_AGENT, "Mozilla/5.0");

        // set timeout
        int timeout = 5 * 60 * 1000;
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(timeout).setSocketTimeout(timeout).build();
        request.setConfig(requestConfig);

        HttpResponse httpResponse = client.execute(request);
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(httpResponse.getEntity().getContent()));

        StringBuilder result = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        _log.debug(requestId + " - result: " + result.toString());
        String xmlString = result.toString();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        String returnCode = "";
        String message = "";
        String authCode = "";
        String extData = "";
        String TID = "";
        try {
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse( new InputSource( new StringReader( xmlString ) ) );

            doc.getDocumentElement().normalize();
            //System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

            NodeList nodeList = doc.getChildNodes();
            for (int temp = 0; temp < nodeList.getLength(); temp++) {
                Node nNode = nodeList.item(temp);
                //System.out.println("\nCurrent Element :" + nNode.getNodeName());
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    if (eElement.getElementsByTagName("ResultCode") != null && eElement.getElementsByTagName("ResultCode").item(0) != null) {
                        returnCode = eElement.getElementsByTagName("ResultCode").item(0).getTextContent();
                    }
                    if (eElement.getElementsByTagName("RespMSG") != null && eElement.getElementsByTagName("RespMSG").item(0) != null) {
                        message = eElement.getElementsByTagName("RespMSG").item(0).getTextContent();
                    }
                    if (eElement.getElementsByTagName("AuthCode") != null && eElement.getElementsByTagName("AuthCode").item(0) != null) {
                        authCode = eElement.getElementsByTagName("AuthCode").item(0).getTextContent();
                    }
                    if (eElement.getElementsByTagName("ExtData") != null && eElement.getElementsByTagName("ExtData").item(0) != null) {
                        extData = eElement.getElementsByTagName("ExtData").item(0).getTextContent();
                    }
                    if (eElement.getElementsByTagName("TID") != null && eElement.getElementsByTagName("TID").item(0) != null) {
                        TID = eElement.getElementsByTagName("TID").item(0).getTextContent();
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (message == null) message = "";
        String cardType = "";
        String cardMask = "";
        if (!returnCode.equals("0")) {
            if (message.equals("Duplicate%20Reference%20Id")) {
                // payment has completed on the Gateway server - mark booking is completed
                returnCode = "200";

            } else {
                returnCode = "449";
                if (message.equals(""))
                    message = "An error occurred while processing your card. Try again in a little bit.";
            }

        } else {
            returnCode = "200";
            Map<String, String> map = new HashMap<>();
            String[] test1 = extData.split(",");

            for (String s : test1) {
                String[] t = s.split("=");
                if (t.length > 1)
                    map.put(t[0], t[1]);
            }

            cardMask = "XXXXXXXXXXXX" + map.get("AcntLast4");
            cardType = map.get("CardType");
        }

        mapResponse.put("authCode", authCode);
        mapResponse.put("cardType", cardType);
        mapResponse.put("cardMask", cardMask);
        mapResponse.put("transId", TID);

        response.put("response", mapResponse);
        response.put("message", message.replace("%20", " "));
        response.put("returnCode", returnCode);

        return response;
    }

    public TransactionResponse preAuthPayment(String terminalId, String serverUrl, String token, String amount, Map<String,String> mapData) throws Exception {
        // implement TSL 1.2
        SSLContext sslctx = SSLContext.getInstance("TLSv1.2");
        sslctx.init(null, null, null);
        HttpsURLConnection.setDefaultSSLSocketFactory(sslctx.getSocketFactory());

        URL url = new URL(serverUrl);
        String query = "<JetPay Version='2.2'>" +
                "<TransactionType>AUTHONLY</TransactionType>" +
                "<TerminalID>$terminalId</TerminalID>" +
                "<TransactionID>$transactionId</TransactionID>" +
                "<Token>$token</Token>" +
                "<TotalAmount>$amount</TotalAmount>" +
                "<Origin>INTERNET</Origin>" +
                "<IndustryInfo Type='ECOMMERCE'></IndustryInfo>" +
                "<Application Version='1.0'>qupdispatcher</Application>" +
                "<Device Version='1.0'>qupdispatcher</Device>" +
                "<Library Version='1.5'>VirtPOS SDK</Library>" +
                "<Gateway>JetPay</Gateway>" +
                "<DeveloperID>pdHdpT</DeveloperID>";

        query = query + "<Billing>";
        if (mapData.get("street") != null && !mapData.get("street").isEmpty()) {
            query = query +
                    "<Address>$address</Address>";
        }
        if (mapData.get("city") != null && !mapData.get("city").isEmpty()) {
            query = query +
                    "<City>$city</City>";
        }
        if (mapData.get("state") != null && !mapData.get("state").isEmpty()) {
            query = query +
                    "<StateProv>$state</StateProv>";
        }
        if (mapData.get("zipCode") != null && !mapData.get("zipCode").isEmpty()) {
            query = query +
                    "<PostalCode>$zipCode</PostalCode>";
        }
        if (mapData.get("country") != null && !mapData.get("country").isEmpty()) {
            query = query +
                    "<Country>$country</Country>";
        }
        if (mapData.get("phone") != null && !mapData.get("phone").isEmpty()) {
            query = query +
                    "<Phone>$phone</Phone>";
        }
        if (mapData.get("customerEmail") != null && !mapData.get("customerEmail").isEmpty()) {
            query = query +
                    "<Email>$customerEmail</Email>";
        }
        query = query + "</Billing>";
        if (mapData.get("customerIP") != null && !mapData.get("customerIP").isEmpty()) {
            query = query +
                    "<UserIPAddress>$customerIP</UserIPAddress>";
        }
        query = query + "</JetPay>";

        String transactionId = RandomStringUtils.randomAlphanumeric(18).toUpperCase();  // required string length
        query = query.replace("$terminalId", terminalId);
        query = query.replace("$transactionId", transactionId);
        query = query.replace("$token", token);
        query = query.replace("$amount", amount);
        query = query.replace("$address", mapData.get("street"));
        query = query.replace("$city", mapData.get("city"));
        query = query.replace("$state", mapData.get("state"));
        query = query.replace("$zipCode", mapData.get("zipCode"));
        query = query.replace("$country", mapData.get("country"));
        query = query.replace("$phone", mapData.get("phone"));
        query = query.replace("$customerEmail", mapData.get("customerEmail"));
        query = query.replace("$customerIP", mapData.get("customerIP"));
        _log.debug(requestId + " - query: " + query);
        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/xml");
        con.setRequestProperty(HttpHeaders.CONTENT_LENGTH, String.valueOf(query.length()));
        con.setDoOutput(true);
        con.setDoInput(true);

        DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.close();

        return getResponse(con);
    }

    public TransactionResponse preAuthCapture(String transactionId, String terminalId, String serverUrl) throws Exception {
        // implement TSL 1.2
        SSLContext sslctx = SSLContext.getInstance("TLSv1.2");
        sslctx.init(null, null, null);
        HttpsURLConnection.setDefaultSSLSocketFactory(sslctx.getSocketFactory());

        URL url = new URL(serverUrl);
        String query = "<JetPay Version='2.2'>" +
                "<TransactionType>CAPT</TransactionType>" +
                "<TerminalID>$terminalId</TerminalID>" +
                "<TransactionID>$transactionId</TransactionID>" +
                "<Origin>INTERNET</Origin>" +
                "<IndustryInfo Type='ECOMMERCE'></IndustryInfo>" +
                "<Application Version='1.0'>qupdispatcher</Application>" +
                "<Device Version='1.0'>qupdispatcher</Device>" +
                "<Library Version='1.5'>VirtPOS SDK</Library>" +
                "<Gateway>JetPay</Gateway>" +
                "<DeveloperID>pdHdpT</DeveloperID>" +
                "</JetPay>";

        query = query.replace("$terminalId", terminalId);
        query = query.replace("$transactionId", transactionId);
        _log.debug(requestId + " - query: " + query);

        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/xml");
        con.setRequestProperty(HttpHeaders.CONTENT_LENGTH, String.valueOf(query.length()));
        con.setDoOutput(true);
        con.setDoInput(true);

        DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.close();

        return getResponse(con);
    }

}
