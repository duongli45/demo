package com.qupworld.paymentgateway.controllers.GatewayUtil;

import com.qupworld.config.ServerConfig;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.pg.util.TimezoneUtil;
import okhttp3.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.simple.JSONObject;

import javax.net.ssl.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class VippsUtil {

    private String REQUEST_URL;
    private String AUTH_URL;
    private String CLIENT_ID;
    private String CLIENT_SECRET;
    private String MERCHANT_SERIAL_NUMBER;
    private String PRIMARY_SUBSCRIPTION;
    private String SECOND_SUBSCRIPTION;
    private String requestId;
    private final static Logger logger = LogManager.getLogger(VippsUtil.class);
    private final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    public VippsUtil(String requestId, String clientId, String clientSecret, String merchantSerialNumber, String primarySubscription, String secondarySubscription,
                     String requestUrl, String authUrl) {
        this.requestId = requestId;
        REQUEST_URL = requestUrl;
        AUTH_URL = authUrl;
        CLIENT_ID = clientId;
        CLIENT_SECRET = clientSecret;
        MERCHANT_SERIAL_NUMBER = merchantSerialNumber;
        PRIMARY_SUBSCRIPTION = primarySubscription;
        SECOND_SUBSCRIPTION = secondarySubscription;
    }

    public String getCheckoutURL(String orderId, String phoneNumber, double amount, boolean walletAppInstalled) {
        ObjResponse response = doGetCheckoutURL(orderId, phoneNumber, amount, walletAppInstalled);
        if (response.returnCode == 401) {
            // try to generate new token then submit payment again
            getAuthToken();
            response = doGetCheckoutURL(orderId, phoneNumber, amount, walletAppInstalled);
            return response.toString();
        } else { // return code = 200/525
            return response.toString();
        }
    }

    public String checkOrderStatus(String orderId) {
        ObjResponse response = doCheckOrderStatus(orderId);
        if (response.returnCode == 401) {
            // try to generate new token then submit payment again
            getAuthToken();
            response = doCheckOrderStatus(orderId);
            return response.toString();
        } else { // return code = 200/525
            return response.toString();
        }
    }

    public String captureTransaction(String orderId, double amount) {
        ObjResponse response = doCaptureTransaction(orderId, amount);
        if (response.returnCode == 401) {
            // try to generate new token then submit payment again
            getAuthToken();
            response = doCaptureTransaction(orderId, amount);
            return response.toString();
        } else { // return code = 200/525
            return response.toString();
        }
    }

    public String cancelTransaction(String orderId) {
        ObjResponse response = doCancelTransaction(orderId);
        if (response.returnCode == 401) {
            // try to generate new token then submit payment again
            getAuthToken();
            response = doCancelTransaction(orderId);
            return response.toString();
        } else { // return code = 200/525
            return response.toString();
        }
    }

    public String refundTransaction(String orderId, double amount) {
        ObjResponse response = doRefundTransaction(orderId, amount);
        if (response.returnCode == 401) {
            // try to generate new token then submit payment again
            getAuthToken();
            response = doRefundTransaction(orderId, amount);
            return response.toString();
        } else { // return code = 200/525
            return response.toString();
        }
    }

    private ObjResponse doGetCheckoutURL(String orderId, String phoneNumber, double amount, boolean appInstalled) {
        ObjResponse response = new ObjResponse();
        try {
            JSONObject objBody = new JSONObject();

            JSONObject objCustomer = new JSONObject();
            objCustomer.put("mobileNumber", phoneNumber);
            objBody.put("customerInfo", objCustomer);

            String callbackPrefix = ServerConfig.hook_server + KeysUtil.VIPPS_URL;
            /*String fallBack = ServerConfig.dispatch_server + KeysUtil.NOTIFICATION_URL;*/
            /*String fallBack = "https://link.gojo.global/paxapp";*/
            String fallBack = "https://link.gojo.global/paxapp?action=prepaid_success&from=vipps";
            /*if (ServerConfig.dispatch_server.equals("http://dispatch.local.qup.vn")) {
                callbackPrefix = "http://113.160.232.234:1337" + KeysUtil.VIPPS_URL;
                *//*fallBack = "http://113.160.232.234:1337" + KeysUtil.NOTIFICATION_URL;*//*
            }*/

            JSONObject objMerchant = new JSONObject();
            objMerchant.put("callbackPrefix", callbackPrefix);
            objMerchant.put("fallBack", fallBack);
            objMerchant.put("isApp", appInstalled);
            objMerchant.put("merchantSerialNumber", MERCHANT_SERIAL_NUMBER);
            objMerchant.put("paymentType", "eComm Regular Payment");
            objBody.put("merchantInfo", objMerchant);

            DecimalFormat df = new DecimalFormat("#");

            JSONObject objTransaction = new JSONObject();
            objTransaction.put("amount", Integer.parseInt(df.format(amount *100)));
            objTransaction.put("orderId", orderId);
            objTransaction.put("timeStamp", KeysUtil.SDF_DMYHMSTZ.format(TimezoneUtil.getGMTTimestamp()));
            objTransaction.put("transactionText", "Order #" + orderId);
            objTransaction.put("skipLandingPage", false);
            objBody.put("transaction", objTransaction);
            logger.debug(requestId + " - URL = " + REQUEST_URL);
            logger.debug(requestId + " - request = " + objBody.toJSONString());

            RequestBody body = RequestBody.create(objBody.toJSONString(), JSON);
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .addHeader("Authorization", KeysUtil.VIPPS_AUTH_TOKEN)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Ocp-Apim-Subscription-Key", PRIMARY_SUBSCRIPTION)
                    .addHeader("Merchant-Serial-Number", MERCHANT_SERIAL_NUMBER)
                    .url(REQUEST_URL)
                    .post(body)
                    .build();
            Response vippsResponse = client.newCall(request).execute();
            String responseString = vippsResponse.body().string();
            logger.debug(requestId + " - response = " + responseString);
            try {
                org.json.JSONObject jsonResponse = new org.json.JSONObject(responseString);
                /*{
                    "orderId": "gojo-order-2021041201",
                    "url": "vippsmt://?token=eyJraWQ..."
                }*/
                String url = jsonResponse.has("url") ? jsonResponse.getString("url") : "";
                if (!url.isEmpty()) {
                    Map<String,Object> mapResponse = new HashMap<>();
                    mapResponse.put("3ds_url", url);
                    mapResponse.put("deepLinkUrl", url);
                    mapResponse.put("type", "link");
                    mapResponse.put("transactionId", orderId);
                    mapResponse.put("waitingNotify", true);

                    response.returnCode = 200;
                    response.response = mapResponse;
                    return response;
                } else {
                    return getError(responseString);
                }
            } catch (Exception ignore) {
                return getError(responseString);
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - getCheckoutURL exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response;
        }
    }

    private ObjResponse doCheckOrderStatus(String orderId) {
        ObjResponse response = new ObjResponse();
        try {
            String url = REQUEST_URL + "/" + orderId +"/details";
            OkHttpClient client = getUnsafeOkHttpClient();
            Request request = new Request.Builder()
                    .addHeader("orderId", orderId)
                    .addHeader("Authorization", KeysUtil.VIPPS_AUTH_TOKEN)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Ocp-Apim-Subscription-Key", PRIMARY_SUBSCRIPTION)
                    .addHeader("Merchant-Serial-Number", MERCHANT_SERIAL_NUMBER)
                    .url(url)
                    .get()
                    .build();
            logger.debug(requestId + " - URL = " + url);
            Response vippsResponse = client.newCall(request).execute();
            String responseString = vippsResponse.body().string();
            logger.debug(requestId + " - response = " + responseString);
            try {
                org.json.JSONObject jsonResponse = new org.json.JSONObject(responseString);
                /*{
                    "orderId": "gojo-order-2021041202",
                    "transactionSummary": {
                        "capturedAmount": 0,
                        "remainingAmountToCapture": 2000,
                        "refundedAmount": 0,
                        "remainingAmountToRefund": 0,
                        "bankIdentificationNumber": 528022
                    },
                    "transactionLogHistory": [
                        {
                            "amount": 2000,
                            "transactionText": "Order #2021041202",
                            "transactionId": "5002554886",
                            "timeStamp": "2021-04-12T09:35:35.570Z",
                            "operation": "RESERVE",
                            "requestId": "",
                            "operationSuccess": true
                        },
                        {
                            "amount": 2000,
                            "transactionText": "Order #2021041202",
                            "transactionId": "5002554886",
                            "timeStamp": "2021-04-12T09:35:29.098Z",
                            "operation": "INITIATE",
                            "operationSuccess": true
                        }
                    ]
                }*/
                orderId = jsonResponse.has("orderId") ? jsonResponse.getString("orderId") : "";
                if (!orderId.isEmpty()) {
                    org.json.JSONArray arrHistory = jsonResponse.has("transactionLogHistory") ? jsonResponse.getJSONArray("transactionLogHistory") : null;
                    if (arrHistory != null && arrHistory.length() > 0) {
                        org.json.JSONObject objHistory = arrHistory.getJSONObject(0);
                        String finalStatus = objHistory.has("operation") ? objHistory.getString("operation") : "";
                        String transactionId = objHistory.has("transactionId") ? objHistory.getString("transactionId") : "";
                        Map<String,String> mapResponse = new HashMap<>();
                        mapResponse.put("status", finalStatus);
                        mapResponse.put("transactionId", transactionId);
                        response.returnCode = 200;
                        response.response = mapResponse;
                    } else {
                        response.returnCode = 525;
                    }
                    return response;
                } else {
                    return getError(responseString);
                }
            } catch (Exception ignore) {
                return getError(responseString);
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - checkOrderStatus exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response;
        }
    }

    private ObjResponse doCaptureTransaction(String orderId, double amount) {
        ObjResponse response = new ObjResponse();
        try {
            JSONObject objBody = new JSONObject();

            JSONObject objMerchant = new JSONObject();
            objMerchant.put("merchantSerialNumber", MERCHANT_SERIAL_NUMBER);
            objBody.put("merchantInfo", objMerchant);

            DecimalFormat df = new DecimalFormat("#");

            JSONObject objTransaction = new JSONObject();
            if (amount > 0) {
                objTransaction.put("amount", Integer.parseInt(df.format(amount * 100)));
            }
            objTransaction.put("transactionText", "Order #" + orderId);
            objBody.put("transaction", objTransaction);

            String url = REQUEST_URL + "/" + orderId + "/capture";
            RequestBody body = RequestBody.create(objBody.toJSONString(), JSON);
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .addHeader("orderId", orderId)
                    .addHeader("Authorization", KeysUtil.VIPPS_AUTH_TOKEN)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Ocp-Apim-Subscription-Key", PRIMARY_SUBSCRIPTION)
                    .addHeader("X-Request-Id", UUID.randomUUID().toString())
                    .url(url)
                    .post(body)
                    .build();
            logger.debug(requestId + " - URL = " + url);
            logger.debug(requestId + " - request = " + objBody.toJSONString());
            Response vippsResponse = client.newCall(request).execute();
            String responseString = vippsResponse.body().string();
            logger.debug(requestId + " - response = " + responseString);
            try {
                org.json.JSONObject jsonResponse = new org.json.JSONObject(responseString);
                /*{
                    "orderId": "gojo-order-2021041202",
                    "transactionInfo": {
                        "amount": 2000,
                        "timeStamp": "2021-04-12T09:38:31.813Z",
                        "transactionText": "Order #2021041202",
                        "status": "Captured",
                        "transactionId": "2850000122"
                    },
                    "transactionSummary": {
                        "capturedAmount": 2000,
                        "remainingAmountToCapture": 0,
                        "refundedAmount": 0,
                        "remainingAmountToRefund": 2000
                    }
                }*/
                orderId = jsonResponse.has("orderId") ? jsonResponse.getString("orderId") : "";
                if (!orderId.isEmpty()) {
                    org.json.JSONObject objInfo = jsonResponse.has("transactionInfo") ? jsonResponse.getJSONObject("transactionInfo") : null;
                    String status = objInfo.has("status") ? objInfo.getString("status") : "";
                    if ("Captured".equalsIgnoreCase(status)) {
                        Map<String,String> mapResponse = new HashMap<>();
                        String transactionId = objInfo.has("transactionId") ? objInfo.getString("transactionId") : "";
                        mapResponse.put("transactionId", transactionId);
                        response.returnCode = 200;
                        response.response = mapResponse;
                    } else {
                        response.returnCode = 525;
                    }
                    return response;
                } else {
                    return getError(responseString);
                }
            } catch (Exception ignore) {
                return getError(responseString);
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - captureTransaction exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response;
        }
    }

    private ObjResponse doCancelTransaction(String orderId) {
        ObjResponse response = new ObjResponse();
        try {
            JSONObject objBody = new JSONObject();

            JSONObject objMerchant = new JSONObject();
            objMerchant.put("merchantSerialNumber", MERCHANT_SERIAL_NUMBER);
            objBody.put("merchantInfo", objMerchant);

            JSONObject objTransaction = new JSONObject();
            objTransaction.put("transactionText", "Cancel order #" + orderId);
            objBody.put("transaction", objTransaction);

            String url = REQUEST_URL + "/"+ orderId + "/cancel";
            RequestBody body = RequestBody.create(objBody.toJSONString(), JSON);
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .addHeader("orderId", orderId)
                    .addHeader("Authorization", KeysUtil.VIPPS_AUTH_TOKEN)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Ocp-Apim-Subscription-Key", PRIMARY_SUBSCRIPTION)
                    .url(url)
                    .put(body)
                    .build();
            logger.debug(requestId + " - URL = " + url);
            logger.debug(requestId + " - request = " + objBody.toJSONString());
            Response vippsResponse = client.newCall(request).execute();
            String responseString = vippsResponse.body().string();
            logger.debug(requestId + " - response = " + responseString);
            try {
                org.json.JSONObject jsonResponse = new org.json.JSONObject(responseString);
                /*{
                    "orderId": "gojo-order-2021041501",
                    "transactionInfo": {
                        "amount": 2100,
                        "transactionText": "Cancel order #2021041501",
                        "status": "Cancelled",
                        "transactionId": "2471000103",
                        "timeStamp": "2021-04-15T08:46:55.790Z"
                    },
                    "transactionSummary": {
                        "capturedAmount": 0,
                        "remainingAmountToCapture": 0,
                        "refundedAmount": 0,
                        "remainingAmountToRefund": 0
                    }
                }*/
                orderId = jsonResponse.has("orderId") ? jsonResponse.getString("orderId") : "";
                if (!orderId.isEmpty()) {
                    org.json.JSONObject objInfo = jsonResponse.has("transactionInfo") ? jsonResponse.getJSONObject("transactionInfo") : null;
                    String status = objInfo.has("status") ? objInfo.getString("status") : "";
                    if ("Cancelled".equalsIgnoreCase(status)) {
                        Map<String,String> mapResponse = new HashMap<>();
                        String transactionId = objInfo.has("transactionId") ? objInfo.getString("transactionId") : "";
                        mapResponse.put("transactionId", transactionId);
                        response.returnCode = 200;
                        response.response = mapResponse;
                    } else {
                        response.returnCode = 525;
                    }
                    return response;
                } else {
                    return getError(responseString);
                }
            } catch (Exception ignore) {
                return getError(responseString);
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - cancelTransaction exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response;
        }
    }

    private ObjResponse doRefundTransaction(String orderId, double amount) {
        ObjResponse response = new ObjResponse();
        try {
            JSONObject objBody = new JSONObject();

            JSONObject objMerchant = new JSONObject();
            objMerchant.put("merchantSerialNumber", MERCHANT_SERIAL_NUMBER);
            objBody.put("merchantInfo", objMerchant);

            DecimalFormat df = new DecimalFormat("#");

            JSONObject objTransaction = new JSONObject();
            if (amount > 0) {
                objTransaction.put("amount", Integer.parseInt(df.format(amount * 100)));
            }
            objTransaction.put("transactionText", "Cancel order #" + orderId);
            objBody.put("transaction", objTransaction);

            String url = REQUEST_URL + "/"+ orderId + "/refund";
            RequestBody body = RequestBody.create(objBody.toJSONString(), JSON);
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .addHeader("orderId", orderId)
                    .addHeader("Authorization", KeysUtil.VIPPS_AUTH_TOKEN)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Ocp-Apim-Subscription-Key", PRIMARY_SUBSCRIPTION)
                    .addHeader("X-Request-Id", UUID.randomUUID().toString())
                    .url(url)
                    .post(body)
                    .build();
            logger.debug(requestId + " - URL = " + url);
            logger.debug(requestId + " - request = " + objBody.toJSONString());
            Response vippsResponse = client.newCall(request).execute();
            String responseString = vippsResponse.body().string();
            logger.debug(requestId + " - response = " + responseString);
            try {
                org.json.JSONObject jsonResponse = new org.json.JSONObject(responseString);
                /*{
                    "orderId": "gojo-order-2021041202",
                    "transaction": {
                        "amount": 2000,
                        "transactionText": "Cancel order #2021041202",
                        "status": "Refund",
                        "transactionId": "2804000122",
                        "timeStamp": "2021-04-12T09:43:21.057Z"
                    },
                    "transactionSummary": {
                        "capturedAmount": 2000,
                        "remainingAmountToCapture": 0,
                        "refundedAmount": 2000,
                        "remainingAmountToRefund": 0
                    }
                }*/
                orderId = jsonResponse.has("orderId") ? jsonResponse.getString("orderId") : "";
                if (!orderId.isEmpty()) {
                    org.json.JSONObject objInfo = jsonResponse.has("transactionInfo") ? jsonResponse.getJSONObject("transactionInfo") : null;
                    String status = objInfo.has("status") ? objInfo.getString("status") : "";
                    if ("Refund".equalsIgnoreCase(status)) {
                        Map<String,String> mapResponse = new HashMap<>();
                        String transactionId = objInfo.has("transactionId") ? objInfo.getString("transactionId") : "";
                        mapResponse.put("transactionId", transactionId);
                        response.returnCode = 200;
                        response.response = mapResponse;
                    } else {
                        response.returnCode = 525;
                    }
                    return response;
                } else {
                    return getError(responseString);
                }
            } catch (Exception ignore) {
                return getError(responseString);
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - refundTransaction exception: " + CommonUtils.getError(ex));
            response.returnCode = 525;
            return response;
        }
    }

    private void getAuthToken() {
        try {
            JSONObject objBody = new JSONObject();
            RequestBody body = RequestBody.create(objBody.toJSONString(), JSON);
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .addHeader("client_id", CLIENT_ID)
                    .addHeader("client_secret", CLIENT_SECRET)
                    .addHeader("Ocp-Apim-Subscription-Key", PRIMARY_SUBSCRIPTION)
                    .url(AUTH_URL)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            /*
                {
                    "token_type": "Bearer",
                    "expires_in": "3599",
                    "ext_expires_in": "3599",
                    "expires_on": "1617947782",
                    "not_before": "1617943882",
                    "resource": "00000002-0000-0000-c000-000000000000",
                    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6Im5PbzNaRHJPRFhFSzFqS1doWHNsSFJfS1hFZyIsImtpZCI6Im5PbzNaRHJPRFhFSzFqS1doWHNsSFJfS1hFZyJ9.eyJhdWQiOiIwMDAwMDAwMi0wMDAwLTAwMDAtYzAwMC0wMDAwMDAwMDAwMDAiLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC9lNTExNjUyNi01MWRjLTRjMTQtYjA4Ni1hNWNiNDcxNmJjNGIvIiwiaWF0IjoxNjE3OTQzODgyLCJuYmYiOjE2MTc5NDM4ODIsImV4cCI6MTYxNzk0Nzc4MiwiYWlvIjoiRTJaZ1lManBlV2pDeWlidmlNKzNKbnhPL2lzNkNRQT0iLCJhcHBpZCI6IjE4Y2E4M2FlLWUxMGUtNDZiZC1iZGMyLTYxMzczYzEyYzZkNSIsImFwcGlkYWNyIjoiMSIsImlkcCI6Imh0dHBzOi8vc3RzLndpbmRvd3MubmV0L2U1MTE2NTI2LTUxZGMtNGMxNC1iMDg2LWE1Y2I0NzE2YmM0Yi8iLCJyaCI6IjAuQVNBQUptVVI1ZHhSRkV5d2hxWExSeGE4UzY2RHloZ080YjFHdmNKaE56d1N4dFVnQUFBLiIsInRlbmFudF9yZWdpb25fc2NvcGUiOiJFVSIsInRpZCI6ImU1MTE2NTI2LTUxZGMtNGMxNC1iMDg2LWE1Y2I0NzE2YmM0YiIsInV0aSI6Ik5lSU0taTNIZ2stUWhFMWk5bjBIQUEiLCJ2ZXIiOiIxLjAifQ.n7N2qyXkIDFfGSf_L6ctDDrXHBlvV6cB6HburnsC8OngaRjFVoLo_yEZEUh9We0SwYr7SAG5u_BqTYshK2U7REhLubTDQq6NweRG7mS2udSW7eZZvqr92quBVq6oEb4oagQUm5cd2xyUQDlEyTFFpeGYSLqmTgpBM6ETEQXeGjT719MgbZfty-J-LkL6pwR-JluThPiT3uq1YbK3jnM_SMCcwH0m9MIwsgrw3g55B9K0GKoG3gPd27n-OzlluNZoDY4xzuX-4V0fFGIuOvDov1WAZF2uxNFROF9VgziB1HAD48KPOjkVUi0a3rdataKcm_bLKoblVfRgat6Otve5gA"
                }
            */
            org.json.JSONObject jsonResponse = new org.json.JSONObject(response.body().string());
            //logger.debug(requestId + " - response = " + jsonResponse.toString());
            KeysUtil.VIPPS_AUTH_TOKEN = jsonResponse.has("access_token") ? jsonResponse.getString("access_token") : "";
        } catch (Exception ex) {
            logger.debug(requestId + " - getAuthToken exception: " + CommonUtils.getError(ex));
        }
    }

    private ObjResponse getError(String responseString) {
        ObjResponse response = new ObjResponse();
        response.returnCode = 525;
        try {
            org.json.JSONObject jsonResponse = new org.json.JSONObject(responseString);
            /*{
                "responseInfo": {
                    "responseCode": 401.0,
                    "responseMessage": "Something went wrong, please try again later."
                },
                    "result": {
                    "message": "Something went wrong, please try again later."
                }
            }*/
            org.json.JSONObject objInfo = jsonResponse.has("responseInfo") ? jsonResponse.getJSONObject("responseInfo") : null;
            int returnCode = 525;
            String responseMessage = "";
            if (objInfo != null) {
                if (objInfo.has("responseCode"))
                    returnCode = objInfo.getInt("responseCode");
                if (objInfo.has(responseMessage))
                    responseMessage = objInfo.getString("responseMessage");
            }

            response.returnCode = returnCode == 401 ? returnCode : 525;
            if (!responseMessage.isEmpty()) {
                Map<String, String> mapResponse = new HashMap<>();
                mapResponse.put("message", responseMessage);
                response.response = mapResponse;
            }
            return response;
        } catch (Exception ignore) {
            JSONArray arrError = new JSONArray(responseString);
            /*[
                {
                    "errorGroup": "Merchant",
                    "errorCode": "35",
                    "errorMessage": "Requested Order not found",
                    "contextId": "02760aa5-bc41-44f5-89f6-b248447f12eb"
                }
            ]*/
            if (arrError.length() > 0) {
                org.json.JSONObject error = arrError.getJSONObject(0);
                String errorMessage = error.has("errorMessage") ? error.getString("errorMessage") : "";
                if (!errorMessage.isEmpty()) {
                    Map<String, String> mapResponse = new HashMap<>();
                    mapResponse.put("message", errorMessage);
                    response.response = mapResponse;
                }
            }
        }
        return response;
    }

    private static final String[] TLS_PROTOCOLS = new String[]{ "TLSv1.1", "TLSv1.2" };

    /*private Socket enableTLSOnSocket(final Socket socket) {
        if (socket instanceof SSLSocket) {
            ((SSLSocket) socket).setEnabledProtocols(TLS_PROTOCOLS);
        }
        return socket;
    }

    public MySSLSocketFactory(final KeyManager[] keyManagers, final TrustManager trustManager) throws KeyManagementException, NoSuchAlgorithmException {
        final SSLContext sslContext = SSLContext.getInstance(TLS);
        sslContext.init(keyManagers, new TrustManager[]{ trustManager }, null);
        // ...
    }*/

    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                                       String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                                       String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new X509Certificate[0];
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            return new OkHttpClient.Builder()
                    .sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0])
                    .hostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }
                    }).build();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
