package com.qupworld.paymentgateway.controllers.threading;

import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.mongo.collections.Credit;
import com.qupworld.paymentgateway.models.mongo.collections.account.Account;
import com.pg.util.CommonUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */
@SuppressWarnings("unchecked")
public class RemoveCreditCardsThread extends Thread {

    private final static Logger logger = LogManager.getLogger(RemoveCreditCardsThread.class);
    public String fleetId;
    public String currentGateway;
    public MongoDao mongoDao;
    public String requestId;

    private int total = 0;
    @Override
    public void run() {
        try {
            int i = 0;
            int size = 100;
            boolean valid = true;
            while (valid) {
                List<String> listPax = mongoDao.getAllPassengerRegisteredCard(requestId, fleetId, i * size, size);
                int querySize = listPax.size();
                for (String userId : listPax) {
                    try {
                        Account account = mongoDao.getAccount(userId);
                        List<Credit> credits = account.credits;
                        if (credits != null && !credits.isEmpty()) {
                            logger.debug(requestId + " === remove card for userId: "+ userId);
                            List<Credit> newList = new ArrayList<>();
                            for (Credit credit : credits) {
                                if (currentGateway.equals(credit.gateway)) {
                                    logger.debug(requestId + " - save token = " + credit.localToken);
                                    newList.add(credit);
                                } else {
                                    logger.debug(requestId + " - remove old token = " + credit.localToken + " from " + credit.gateway);
                                    total += 1;
                                }
                            }
                            boolean updated = mongoDao.updateCreditCards(userId, newList);
                            if (!updated) {
                                Thread.sleep(1000);
                                updated = mongoDao.updateCreditCards(userId, newList);
                            }
                            logger.debug(requestId + " - updated = " + updated);
                            logger.debug(requestId + " === remove for userId: "+ userId + " is done!");
                        }

                    } catch (Exception ex) {
                        logger.debug(requestId + " === remove for userId: "+ userId +" exception: "+ CommonUtils.getError(ex));
                    }
                }

                if (querySize < size) {
                    valid = false;
                }
                i++;
            }
            logger.debug(requestId + " - RemoveCreditCardsThread COMPLETED !!!");
            logger.debug(requestId + " - total removed: " + total);
        } catch(Exception ex) {
            logger.debug(requestId + " - remove exception: "+ CommonUtils.getError(ex));
        }
    }
}
