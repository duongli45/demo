
package com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransactionStatusResult" type="{http://schemas.firstatlanticcommerce.com/gateway/data}TransactionStatusResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transactionStatusResult"
})
@XmlRootElement(name = "TransactionStatusResponse")
public class TransactionStatusResponse {

    @XmlElement(name = "TransactionStatusResult", nillable = true)
    protected com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.TransactionStatusResponse transactionStatusResult;

    /**
     * Gets the value of the transactionStatusResult property.
     * 
     * @return
     *     possible object is
     *     {@link com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.TransactionStatusResponse }
     *     
     */
    public com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.TransactionStatusResponse getTransactionStatusResult() {
        return transactionStatusResult;
    }

    /**
     * Sets the value of the transactionStatusResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.TransactionStatusResponse }
     *     
     */
    public void setTransactionStatusResult(com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data.TransactionStatusResponse value) {
        this.transactionStatusResult = value;
    }

}
