package com.qupworld.paymentgateway.controllers.threading;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.MongoDaoImpl;
import com.qupworld.paymentgateway.models.SQLDao;
import com.qupworld.paymentgateway.models.SQLDaoImpl;
import com.pg.util.KeysUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by qup on 23/03/2020.
 */
@SuppressWarnings("unchecked")
public class MigrateTicket extends Thread {
    final static Logger logger = LogManager.getLogger(MigrateTicket.class);
    public String FLEETID = "";
    public String FROM = "";
    public String TO = "";
    public String TYPE = "";
    public String requestId = "";
    public List<String> bookIds = new ArrayList<>();
    public MigrateTicket(String requestId, String fleetId, String from, String to, String type, List<String> bookIds) {
        this.FLEETID = fleetId;
        this.FROM = from;
        this.TO = to;
        this.TYPE = type.toUpperCase();
        this.requestId = requestId;
        this.bookIds = bookIds;
    }

    @Override
    public void run() {
        try {

            SQLDao sqlDao = new SQLDaoImpl();
            MongoDao mongoDao = new MongoDaoImpl();
            boolean valid = true;
            int count = KeysUtil.SIZE;
            int i = 0;
            List<String> listKeys = new ArrayList<>();

            List<String> types = Arrays.asList(TYPE.split(","));
            GsonBuilder gb = new GsonBuilder();
            gb.serializeNulls();
            Gson gson = gb.create();
            int size = 0;
            if (TYPE.equals("TICKET")) {
                logger.debug(requestId + " - --- MIGRATE TICKET ...");
                while (valid) {
                    try {
                        if ((i * count) % 2000 == 0) {
                            logger.debug(requestId + " - total = " + i * count + " sleep ...");
                            Thread.sleep(2000);
                        }
                        String allTickets = "";
                        if (bookIds.isEmpty()){
                            allTickets = sqlDao.getAllTicket(i, FLEETID, FROM, TO);
                        }else {
                            allTickets = sqlDao.getAllTicket(bookIds);
                        }
                        JSONParser parser = new JSONParser();
                        JSONObject jsonObject = (JSONObject) parser.parse(allTickets);

                        JSONArray fields = (JSONArray) jsonObject.get("fields");
                        if (listKeys.size() == 0) {
                            int index = 0;
                            for (Object field : fields) {
                                JSONObject object = (JSONObject) field;
                                if (object.get("name").toString().equals("total"))
                                    logger.debug(requestId + " - index" + index);
                                listKeys.add(object.get("name").toString());
                                index++;
                            }
                        }
                        JSONArray records = (JSONArray) jsonObject.get("records");
                        size = records.size();
                        logger.debug(requestId + " - i = " + i + " - size: " + records.size());
                        if (size > 0) {
                            MigrateTicketClone clone = new MigrateTicketClone(records, listKeys, sqlDao, mongoDao, requestId);
                            clone.run();

                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        logger.debug(requestId + " - error: " + ex.getMessage());
                    } finally {
                        if (size < count) {
                            valid = false;
                        } else {
                            i++;
                        }
                    }
                }
                logger.debug(requestId + " - total ticket = " + (i * count + size));
                logger.debug(requestId + " - --- DONE !!!");
            }

            List<String> listTypes = Arrays.asList("PROMOCODEUSE", "PREPAID", "DRIVERBALANCE", "SETTLEMENTHISTORY");
            if (listTypes.contains(TYPE)) {
                logger.debug(requestId + " - --- MIGRATE "+ TYPE +" ...");
                valid = true;
                i = 0;
                listKeys = new ArrayList<>();
                size = 0;
                while (valid) {
                    try {
                        if ((i * count) % 500 == 0) {
                            logger.debug(requestId + " - total = " + i * count + " sleep ...");
                            Thread.sleep(1000);
                        }
                        String allRecords = sqlDao.getAllRecords(TYPE, i, FLEETID, FROM, TO);
                        JSONParser parser = new JSONParser();
                        JSONObject jsonObject = (JSONObject) parser.parse(allRecords);

                        JSONArray fields = (JSONArray) jsonObject.get("fields");
                        if (listKeys.size() == 0) {
                            for (Object field : fields) {
                                JSONObject object = (JSONObject) field;
                                listKeys.add(object.get("name").toString());
                            }
                        }

                        JSONArray records = (JSONArray) jsonObject.get("records");
                        size = records.size();
                        logger.debug(requestId + " - i = " + i + " - size: " + records.size());
                        if (size > 0) {
                            String type = "";
                            switch (TYPE) {
                                case "PROMOCODEUSE" : {
                                    type = "PromoCodeUse";
                                }
                                break;
                                case "PREPAID" : {
                                    type = "PrepaidTransaction";
                                }
                                break;
                                case "DRIVERBALANCE" : {
                                    type = "DriverCreditBalance";
                                }
                                break;
                                case "SETTLEMENTHISTORY" : {
                                    type = "SettlementHistory";
                                }
                                break;
                            }

                            JSONArray allConvertedRecords = new JSONArray();
                            for (Object record : records) {
                                JSONObject objBody = new JSONObject();
                                JSONArray object = (JSONArray) record;
                                String id = "";
                                JSONObject obj = new JSONObject();
                                for (int k = 0; k < listKeys.size(); k++) {
                                    if (k == 0)
                                        id = object.get(k).toString();
                                    obj.put(listKeys.get(k), object.get(k));
                                }
                                objBody.put("id", id);
                                objBody.put("type", type);
                                objBody.put("body", obj);
                                allConvertedRecords.add(objBody);
                            }

                            String body = gson.toJson(allConvertedRecords).replace(".0\"", "\"");
                            logger.debug(requestId + " - body: " + body);

                            String authBody = "{ \"username\" : \"" + ServerConfig.auth_username + "\" , \"password\" : \"" + ServerConfig.auth_password + "\" }";
                            HttpResponse<JsonNode> jsonAuth = Unirest.post(ServerConfig.server_auth)
                                    .header("Content-Type", "application/json; charset=utf-8")
                                    .body(authBody)
                                    .asJson();
                            JSONObject object = (JSONObject) parser.parse(jsonAuth.getBody().toString());
                            String token = object.get("token").toString();
                            HttpResponse<JsonNode> json = Unirest.post(ServerConfig.server_report)
                                    .header("Content-Type", "application/json; charset=utf-8")
                                    .header("x-access-token", token)
                                    .header("x-trace-request-id", requestId)
                                    .body(body)
                                    .asJson();
                            logger.debug(requestId + " - Migrate "+TYPE+": " + json.getBody().toString());
                        }
                    } catch (Exception ex) {
                        logger.debug(requestId + " - error: " + ex.getMessage());
                    } finally {
                        if (size < count) {
                            valid = false;
                        } else {
                            i++;
                        }
                    }
                }
                logger.debug(requestId + " - total records === " + (i * count + size));
                logger.debug(requestId + " - --- DONE !!!");
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            logger.debug(requestId + " - MigrateTicket: "+ errors.toString());
        }

    }

}
