
package com.qupworld.paymentgateway.controllers.GatewayUtil.PayUSDK;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class CreditCardToken {

    public String payerId;
    public String name;
    public String identificationNumber;
    public String paymentMethod;
    public String number;
    public String expirationDate;

}
