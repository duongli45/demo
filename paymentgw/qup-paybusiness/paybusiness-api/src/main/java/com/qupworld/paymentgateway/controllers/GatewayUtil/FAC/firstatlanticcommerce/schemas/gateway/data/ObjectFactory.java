
package com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Authorize3DSRequest_QNAME = new QName("http://schemas.firstatlanticcommerce.com/gateway/data", "Authorize3DSRequest");
    private final static QName _BinCheckResults_QNAME = new QName("http://schemas.firstatlanticcommerce.com/gateway/data", "BinCheckResults");
    private final static QName _TransactionModificationRequest_QNAME = new QName("http://schemas.firstatlanticcommerce.com/gateway/data", "TransactionModificationRequest");
    private final static QName _AuthorizeResponse_QNAME = new QName("http://schemas.firstatlanticcommerce.com/gateway/data", "AuthorizeResponse");
    private final static QName _TransactionModificationResponse_QNAME = new QName("http://schemas.firstatlanticcommerce.com/gateway/data", "TransactionModificationResponse");
    private final static QName _BillingDetails_QNAME = new QName("http://schemas.firstatlanticcommerce.com/gateway/data", "BillingDetails");
    private final static QName _TransactionDetails_QNAME = new QName("http://schemas.firstatlanticcommerce.com/gateway/data", "TransactionDetails");
    private final static QName _TransactionStatusResponse_QNAME = new QName("http://schemas.firstatlanticcommerce.com/gateway/data", "TransactionStatusResponse");
    private final static QName _ThreeDSecureDetails_QNAME = new QName("http://schemas.firstatlanticcommerce.com/gateway/data", "ThreeDSecureDetails");
    private final static QName _ShippingDetails_QNAME = new QName("http://schemas.firstatlanticcommerce.com/gateway/data", "ShippingDetails");
    private final static QName _Authorize3DSResponse_QNAME = new QName("http://schemas.firstatlanticcommerce.com/gateway/data", "Authorize3DSResponse");
    private final static QName _FraudControlResults_QNAME = new QName("http://schemas.firstatlanticcommerce.com/gateway/data", "FraudControlResults");
    private final static QName _CardDetails_QNAME = new QName("http://schemas.firstatlanticcommerce.com/gateway/data", "CardDetails");
    private final static QName _TransactionStatusRequest_QNAME = new QName("http://schemas.firstatlanticcommerce.com/gateway/data", "TransactionStatusRequest");
    private final static QName _CreditCardTransactionResults_QNAME = new QName("http://schemas.firstatlanticcommerce.com/gateway/data", "CreditCardTransactionResults");
    private final static QName _FraudDetails_QNAME = new QName("http://schemas.firstatlanticcommerce.com/gateway/data", "FraudDetails");
    private final static QName _RecurringDetails_QNAME = new QName("http://schemas.firstatlanticcommerce.com/gateway/data", "RecurringDetails");
    private final static QName _IPGeoLocationResults_QNAME = new QName("http://schemas.firstatlanticcommerce.com/gateway/data", "IPGeoLocationResults");
    private final static QName _IPGeoLocationRequest_QNAME = new QName("http://schemas.firstatlanticcommerce.com/gateway/data", "IPGeoLocationRequest");
    private final static QName _AuthorizeRequest_QNAME = new QName("http://schemas.firstatlanticcommerce.com/gateway/data", "AuthorizeRequest");
    private final static QName _IPGeoLocationResponse_QNAME = new QName("http://schemas.firstatlanticcommerce.com/gateway/data", "IPGeoLocationResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.qupworld.paymentgateway.controllers.GatewayUtil.FAC.firstatlanticcommerce.schemas.gateway.data
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IPGeoLocationResponse }
     * 
     */
    public IPGeoLocationResponse createIPGeoLocationResponse() {
        return new IPGeoLocationResponse();
    }

    /**
     * Create an instance of {@link AuthorizeResponse }
     * 
     */
    public AuthorizeResponse createAuthorizeResponse() {
        return new AuthorizeResponse();
    }

    /**
     * Create an instance of {@link TransactionModificationResponse }
     * 
     */
    public TransactionModificationResponse createTransactionModificationResponse() {
        return new TransactionModificationResponse();
    }

    /**
     * Create an instance of {@link IPGeoLocationRequest }
     * 
     */
    public IPGeoLocationRequest createIPGeoLocationRequest() {
        return new IPGeoLocationRequest();
    }

    /**
     * Create an instance of {@link Authorize3DSResponse }
     * 
     */
    public Authorize3DSResponse createAuthorize3DSResponse() {
        return new Authorize3DSResponse();
    }

    /**
     * Create an instance of {@link TransactionModificationRequest }
     * 
     */
    public TransactionModificationRequest createTransactionModificationRequest() {
        return new TransactionModificationRequest();
    }

    /**
     * Create an instance of {@link Authorize3DSRequest }
     * 
     */
    public Authorize3DSRequest createAuthorize3DSRequest() {
        return new Authorize3DSRequest();
    }

    /**
     * Create an instance of {@link TransactionStatusRequest }
     * 
     */
    public TransactionStatusRequest createTransactionStatusRequest() {
        return new TransactionStatusRequest();
    }

    /**
     * Create an instance of {@link TransactionStatusResponse }
     * 
     */
    public TransactionStatusResponse createTransactionStatusResponse() {
        return new TransactionStatusResponse();
    }

    /**
     * Create an instance of {@link AuthorizeRequest }
     * 
     */
    public AuthorizeRequest createAuthorizeRequest() {
        return new AuthorizeRequest();
    }

    /**
     * Create an instance of {@link FraudControlResults }
     * 
     */
    public FraudControlResults createFraudControlResults() {
        return new FraudControlResults();
    }

    /**
     * Create an instance of {@link ShippingDetails }
     * 
     */
    public ShippingDetails createShippingDetails() {
        return new ShippingDetails();
    }

    /**
     * Create an instance of {@link ThreeDSecureDetails }
     * 
     */
    public ThreeDSecureDetails createThreeDSecureDetails() {
        return new ThreeDSecureDetails();
    }

    /**
     * Create an instance of {@link BillingDetails }
     * 
     */
    public BillingDetails createBillingDetails() {
        return new BillingDetails();
    }

    /**
     * Create an instance of {@link TransactionDetails }
     * 
     */
    public TransactionDetails createTransactionDetails() {
        return new TransactionDetails();
    }

    /**
     * Create an instance of {@link IPGeoLocationResults }
     * 
     */
    public IPGeoLocationResults createIPGeoLocationResults() {
        return new IPGeoLocationResults();
    }

    /**
     * Create an instance of {@link RecurringDetails }
     * 
     */
    public RecurringDetails createRecurringDetails() {
        return new RecurringDetails();
    }

    /**
     * Create an instance of {@link FraudDetails }
     * 
     */
    public FraudDetails createFraudDetails() {
        return new FraudDetails();
    }

    /**
     * Create an instance of {@link CreditCardTransactionResults }
     * 
     */
    public CreditCardTransactionResults createCreditCardTransactionResults() {
        return new CreditCardTransactionResults();
    }

    /**
     * Create an instance of {@link BinCheckResults }
     * 
     */
    public BinCheckResults createBinCheckResults() {
        return new BinCheckResults();
    }

    /**
     * Create an instance of {@link CardDetails }
     * 
     */
    public CardDetails createCardDetails() {
        return new CardDetails();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Authorize3DSRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.firstatlanticcommerce.com/gateway/data", name = "Authorize3DSRequest")
    public JAXBElement<Authorize3DSRequest> createAuthorize3DSRequest(Authorize3DSRequest value) {
        return new JAXBElement<Authorize3DSRequest>(_Authorize3DSRequest_QNAME, Authorize3DSRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BinCheckResults }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.firstatlanticcommerce.com/gateway/data", name = "BinCheckResults")
    public JAXBElement<BinCheckResults> createBinCheckResults(BinCheckResults value) {
        return new JAXBElement<BinCheckResults>(_BinCheckResults_QNAME, BinCheckResults.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionModificationRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.firstatlanticcommerce.com/gateway/data", name = "TransactionModificationRequest")
    public JAXBElement<TransactionModificationRequest> createTransactionModificationRequest(TransactionModificationRequest value) {
        return new JAXBElement<TransactionModificationRequest>(_TransactionModificationRequest_QNAME, TransactionModificationRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthorizeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.firstatlanticcommerce.com/gateway/data", name = "AuthorizeResponse")
    public JAXBElement<AuthorizeResponse> createAuthorizeResponse(AuthorizeResponse value) {
        return new JAXBElement<AuthorizeResponse>(_AuthorizeResponse_QNAME, AuthorizeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionModificationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.firstatlanticcommerce.com/gateway/data", name = "TransactionModificationResponse")
    public JAXBElement<TransactionModificationResponse> createTransactionModificationResponse(TransactionModificationResponse value) {
        return new JAXBElement<TransactionModificationResponse>(_TransactionModificationResponse_QNAME, TransactionModificationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BillingDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.firstatlanticcommerce.com/gateway/data", name = "BillingDetails")
    public JAXBElement<BillingDetails> createBillingDetails(BillingDetails value) {
        return new JAXBElement<BillingDetails>(_BillingDetails_QNAME, BillingDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.firstatlanticcommerce.com/gateway/data", name = "TransactionDetails")
    public JAXBElement<TransactionDetails> createTransactionDetails(TransactionDetails value) {
        return new JAXBElement<TransactionDetails>(_TransactionDetails_QNAME, TransactionDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.firstatlanticcommerce.com/gateway/data", name = "TransactionStatusResponse")
    public JAXBElement<TransactionStatusResponse> createTransactionStatusResponse(TransactionStatusResponse value) {
        return new JAXBElement<TransactionStatusResponse>(_TransactionStatusResponse_QNAME, TransactionStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ThreeDSecureDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.firstatlanticcommerce.com/gateway/data", name = "ThreeDSecureDetails")
    public JAXBElement<ThreeDSecureDetails> createThreeDSecureDetails(ThreeDSecureDetails value) {
        return new JAXBElement<ThreeDSecureDetails>(_ThreeDSecureDetails_QNAME, ThreeDSecureDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShippingDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.firstatlanticcommerce.com/gateway/data", name = "ShippingDetails")
    public JAXBElement<ShippingDetails> createShippingDetails(ShippingDetails value) {
        return new JAXBElement<ShippingDetails>(_ShippingDetails_QNAME, ShippingDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Authorize3DSResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.firstatlanticcommerce.com/gateway/data", name = "Authorize3DSResponse")
    public JAXBElement<Authorize3DSResponse> createAuthorize3DSResponse(Authorize3DSResponse value) {
        return new JAXBElement<Authorize3DSResponse>(_Authorize3DSResponse_QNAME, Authorize3DSResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FraudControlResults }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.firstatlanticcommerce.com/gateway/data", name = "FraudControlResults")
    public JAXBElement<FraudControlResults> createFraudControlResults(FraudControlResults value) {
        return new JAXBElement<FraudControlResults>(_FraudControlResults_QNAME, FraudControlResults.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.firstatlanticcommerce.com/gateway/data", name = "CardDetails")
    public JAXBElement<CardDetails> createCardDetails(CardDetails value) {
        return new JAXBElement<CardDetails>(_CardDetails_QNAME, CardDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionStatusRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.firstatlanticcommerce.com/gateway/data", name = "TransactionStatusRequest")
    public JAXBElement<TransactionStatusRequest> createTransactionStatusRequest(TransactionStatusRequest value) {
        return new JAXBElement<TransactionStatusRequest>(_TransactionStatusRequest_QNAME, TransactionStatusRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreditCardTransactionResults }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.firstatlanticcommerce.com/gateway/data", name = "CreditCardTransactionResults")
    public JAXBElement<CreditCardTransactionResults> createCreditCardTransactionResults(CreditCardTransactionResults value) {
        return new JAXBElement<CreditCardTransactionResults>(_CreditCardTransactionResults_QNAME, CreditCardTransactionResults.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FraudDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.firstatlanticcommerce.com/gateway/data", name = "FraudDetails")
    public JAXBElement<FraudDetails> createFraudDetails(FraudDetails value) {
        return new JAXBElement<FraudDetails>(_FraudDetails_QNAME, FraudDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecurringDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.firstatlanticcommerce.com/gateway/data", name = "RecurringDetails")
    public JAXBElement<RecurringDetails> createRecurringDetails(RecurringDetails value) {
        return new JAXBElement<RecurringDetails>(_RecurringDetails_QNAME, RecurringDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IPGeoLocationResults }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.firstatlanticcommerce.com/gateway/data", name = "IPGeoLocationResults")
    public JAXBElement<IPGeoLocationResults> createIPGeoLocationResults(IPGeoLocationResults value) {
        return new JAXBElement<IPGeoLocationResults>(_IPGeoLocationResults_QNAME, IPGeoLocationResults.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IPGeoLocationRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.firstatlanticcommerce.com/gateway/data", name = "IPGeoLocationRequest")
    public JAXBElement<IPGeoLocationRequest> createIPGeoLocationRequest(IPGeoLocationRequest value) {
        return new JAXBElement<IPGeoLocationRequest>(_IPGeoLocationRequest_QNAME, IPGeoLocationRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthorizeRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.firstatlanticcommerce.com/gateway/data", name = "AuthorizeRequest")
    public JAXBElement<AuthorizeRequest> createAuthorizeRequest(AuthorizeRequest value) {
        return new JAXBElement<AuthorizeRequest>(_AuthorizeRequest_QNAME, AuthorizeRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IPGeoLocationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.firstatlanticcommerce.com/gateway/data", name = "IPGeoLocationResponse")
    public JAXBElement<IPGeoLocationResponse> createIPGeoLocationResponse(IPGeoLocationResponse value) {
        return new JAXBElement<IPGeoLocationResponse>(_IPGeoLocationResponse_QNAME, IPGeoLocationResponse.class, null, value);
    }

}
