package com.qupworld.paymentgateway.controllers.threading;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.entities.PaxWalletTransaction;
import com.qupworld.service.QUpReportPublisher;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by qup on 23/03/2020.
 */
@SuppressWarnings("unchecked")
public class UpdatePaxWalletThread extends Thread {
    public PaxWalletTransaction paxWalletTransaction;
    public String requestId;
    final static Logger logger = LogManager.getLogger(UpdatePaxWalletThread.class);
    @Override
    public void run() {
        String createdDate =  KeysUtil.SDF_DMYHMSTZ.format(paxWalletTransaction.createdDate);
        try {
            if (ServerConfig.server_report_enable) {
                Gson gson = new GsonBuilder().serializeNulls().create();
                String paxWalletTransactionStr = gson.toJson(paxWalletTransaction);
                JsonElement jsonElement = gson.fromJson(paxWalletTransactionStr, JsonElement.class);
                JsonObject jo = jsonElement.getAsJsonObject();
                jo.addProperty("createdDate",createdDate);

                QUpReportPublisher reportPublisher = QUpReportPublisher.getInstance();
                reportPublisher.sendOneItem(requestId, String.valueOf(paxWalletTransaction.id), "PaxWalletTransaction", gson.toJson(jo));
            }
        } catch(Exception ex) {
            logger.debug(requestId + " - report PaxWalletTransaction: "+ CommonUtils.getError(ex));
        }
    }
}
