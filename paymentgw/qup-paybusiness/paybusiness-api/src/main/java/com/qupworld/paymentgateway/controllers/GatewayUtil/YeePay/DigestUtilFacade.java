package com.qupworld.paymentgateway.controllers.GatewayUtil.YeePay;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;

@SuppressWarnings("unchecked")
public class DigestUtilFacade {
	// 在map中添加hmac
	public static Map<String,String> addHmac(String[] HmacOrder, Map<String,String> map, String keyValue){
		map = formatMap(HmacOrder, map);
		String sbold = DigestUtil.getHmacSBOld(HmacOrder, map);
		
		String hmac = "";
		String str = sbold; 
		hmac = DigestUtil.hmacSign(sbold, keyValue);
		map.put("hmac", hmac);
		return map;
	}
	// 格式化参数Map，如HmacOrder中有的参数名在Map中没有此键值对的话，在Map中添加键值对
	public static Map<String,String> formatMap(String[] HmacOrder, Map<String,String> map){
		String key = "";
		String value = "";
		for(int i = 0; i < HmacOrder.length; i++){
			key = HmacOrder[i];
			value = map.get(key);
			if(value == null){
				map.put(key, "");
			}
		}
		return map;
	}
	// 检查map中的hmac与在map中的以HmacOrder为键的值所组成的hmac是否一致
	public static boolean checkHmac(String[] HmacOrder,Map<String,String> map, String keyValue){
		boolean returnBoolean = false;
		Object hmacObj = map.get("hmac");
		String hmac = (hmacObj == null) ? "" : (String)hmacObj ;
		String sbold = DigestUtil.getHmacSBOld(HmacOrder, map);
		String newHmac = "";
//		System.out.println("sbold:"+sbold);
		try {
			sbold = URLDecoder.decode(sbold,"GBK");
			if(map.get("r0_Cmd") !=null && 
					(map.get("r0_Cmd").equals("AppleOrder")||map.get("r0_Cmd").equals("EposBindList"))){
				sbold = sbold.replace(" ", "+");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
//		System.out.println("newsbold:"+sbold);
		newHmac = DigestUtil.hmacSign(sbold, keyValue);
//		System.out.println("newHmac:"+newHmac);
		if(hmac.equals(newHmac)){
			returnBoolean = true;
		}
		return returnBoolean; 
	}

}
