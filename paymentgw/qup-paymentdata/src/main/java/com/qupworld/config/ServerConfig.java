package com.qupworld.config;

import com.devebot.opflow.supports.OpflowConfigUtil;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by qup on 7/28/16.
 */
public class ServerConfig {

  public static String fawry_api_key;
  // Define server link for report Store
  public static String server_auth;
  public static String auth_username;
  public static String auth_password;
  public static String server_report;
  public static String server_report_dashboard;
  public static String server_report_dmc;
  public static Boolean server_report_enable;
  public static String audit_report_url;

  // Define server link for logs
  public static String logs_server;
  public static String logs_username;
  public static String logs_password;

  // Define default keys for payment gateway

  public static boolean mongo_enable_auth;
  public static boolean mongo_oplog = false;
  public static String mongo_database_name;
  public static int mongo_port;
  public static String mongo_host;
  public static String mongo_list_host;
  public static String mongo_userName;
  public static String mongo_password;
  public static String mongo_replicaSet;
  public static String mongo_authSource;
  public static String mongo_authMechanism;
  public static String mongo_writeConcern;
  public static String mongo_readPreference;
  public static int mongo_maxPoolSize;
  public static int mongo_waitQueueMultiple;
  public static int mongo_waitQueueTimeoutMS;

  // my SQL WRITE instance
  public static String sql_database_name;
  public static int sql_port;
  public static String sql_host;
  public static String sql_user;
  public static String sql_password;
  public static int sql_setMinIdle;
  public static int sql_setMaxIdle;
  public static int sql_setMaxActive;
  public static int sql_setMaxWait;

  // my SQL READ instance
  public static boolean enable_read_write;
  public static String mysqlConnectOptions;
  public static boolean enable_sync;
  public static String sql_read_database_name;
  public static int sql_read_port;
  public static String sql_read_host;
  public static String sql_read_user;
  public static String sql_read_password;
  public static int sql_read_setMinIdle;
  public static int sql_read_setMaxIdle;
  public static int sql_read_setMaxActive;
  public static int sql_read_setMaxWait;

  // Redis
  public static String redis_host;
  public static int redis_port;
  public static int redis_number;
  public static int redis_number_retry;

  // Authentication
  // Dispatcher server
  public static String dispatcher_username;
  public static String dispatcher_password;

  // CC server
  public static String qup_service;
  public static String cc_username;
  public static String cc_password;
  public static String cc_service_username;
  public static String cc_service_password;

  // MP - ASAP Promotion Service
  public static Boolean txgTrip_enable;
  public static String txgTrip_fleets;

  // Dispatch service
  public static String dispatch_server;
  public static String dispatch_userName;
  public static String dispatch_password;
  public static ServerConfig instance = null;

  // Maps provider service
  public static String maps_provider_server;
  public static String maps_provider_userName;
  public static String maps_provider_password;

  // Menu API service
  public static String menu_api;
  public static String menu_username;
  public static String menu_password;

  // default timeZone
  public static String default_timezone = null;

  // S3
  public static String accessKey;
  public static String secretKey;
  public static String bucketName;
  public static String serverName;
  public static String region;

  // Config URL to Browserbox
  public static String webview_url;

  // for test
  public static String runningMode = "test";
  public static boolean enableMock = false;

  // for mail service
  public static String mail_config_dir;
  public static String mail_server;
  public static String mail_log_gateway;

  // cronjob
  public static String cronjob_clean_redis;
  public static String cronjob_payment_audit;
  public static Boolean queue_mode;
  public static Boolean queue_business_mode;
  public static String slack_url;

  public static String hook_server;
  public static String proxy_server;
  public static String webform_url;
  public static String webform_paymaya;
  public static String webform_cxpay;
  public static String webform_register;
  public static String webform_powertranz;

  public static String point_system_url;
  public static String point_system_user;
  public static String point_system_pass;

  // slack notify
  public static String notify_url;
  public static String notify_token;
  public static String notify_channel;

  // invoice
  public static String invoice_server;
  public static String invoice_userName;
  public static String invoice_password;

  public ServerConfig() {
    // Exists only to defeat instantiation.
  }

  public static ServerConfig getInstance(String pathconfig) {
    if (instance == null) {
      instance = new ServerConfig();
      Properties prop = new Properties();
      InputStream input = null;
      try {
        input = Thread
            .currentThread()
            .getContextClassLoader()
            .getResourceAsStream("configpayment.properties");
        // load a properties file from class path, inside static method
        prop.load(input);
        server_report = getEnviromentField("server_report", prop);
        System.out.println("ServerConfig: server_report: " + server_report);
        server_report_dashboard = getEnviromentField("server_report_dashboard", prop);
        server_report_dmc = getEnviromentField("server_report_dmc", prop);
        server_report_enable = Boolean.parseBoolean(
            getEnviromentField("server_report_enable", prop));
        server_auth = getEnviromentField("server_auth", prop);
        auth_username = getEnviromentField("auth_username", prop);
        auth_password = getEnviromentField("auth_password", prop);
        audit_report_url = getEnviromentField("audit_report_url", prop);

        logs_server = getEnviromentField("logs_server", prop);
        logs_username = getEnviromentField("logs_username", prop);
        logs_password = getEnviromentField("logs_password", prop);

        sql_database_name = getEnviromentField("sql_database_name", prop);
        sql_port = Integer.parseInt(
            !getEnviromentField("sql_port", prop).isEmpty()
                ? getEnviromentField("sql_port", prop)
                : "0");
        sql_host = getEnviromentField("sql_host", prop);
        sql_user = getEnviromentField("sql_user", prop);
        sql_password = getEnviromentField("sql_password", prop);
        sql_setMinIdle = Integer.parseInt(
            !getEnviromentField("sql_setMinIdle", prop).isEmpty()
                ? getEnviromentField("sql_setMinIdle", prop)
                : "0");
        sql_setMaxIdle = Integer.parseInt(
            !getEnviromentField("sql_setMaxIdle", prop).isEmpty()
                ? getEnviromentField("sql_setMaxIdle", prop)
                : "0");
        sql_setMaxActive = Integer.parseInt(
            !getEnviromentField("sql_setMaxActive", prop).isEmpty()
                ? getEnviromentField("sql_setMaxActive", prop)
                : "0");
        sql_setMaxWait = Integer.parseInt(
            !getEnviromentField("sql_setMaxWait", prop).isEmpty()
                ? getEnviromentField("sql_setMaxWait", prop)
                : "0");

        enable_read_write = Boolean.parseBoolean(getEnviromentField("enable_read_write", prop));
        mysqlConnectOptions = getEnviromentField("mysqlConnectOptions", prop);
        enable_sync = Boolean.parseBoolean(getEnviromentField("enable_sync", prop));
        sql_read_database_name = getEnviromentField("sql_read_database_name", prop);
        sql_read_port = Integer.parseInt(
            !getEnviromentField("sql_read_port", prop).isEmpty()
                ? getEnviromentField("sql_read_port", prop)
                : "0");
        sql_read_host = getEnviromentField("sql_read_host", prop);
        sql_read_user = getEnviromentField("sql_read_user", prop);
        sql_read_password = getEnviromentField("sql_read_password", prop);
        sql_read_setMinIdle = Integer.parseInt(
            !getEnviromentField("sql_read_setMinIdle", prop).isEmpty()
                ? getEnviromentField("sql_read_setMinIdle", prop)
                : "0");
        sql_read_setMaxIdle = Integer.parseInt(
            !getEnviromentField("sql_read_setMaxIdle", prop).isEmpty()
                ? getEnviromentField("sql_read_setMaxIdle", prop)
                : "0");
        sql_read_setMaxActive = Integer.parseInt(
            !getEnviromentField("sql_read_setMaxActive", prop).isEmpty()
                ? getEnviromentField("sql_read_setMaxActive", prop)
                : "0");
        sql_read_setMaxWait = Integer.parseInt(
            !getEnviromentField("sql_read_setMaxWait", prop).isEmpty()
                ? getEnviromentField("sql_read_setMaxWait", prop)
                : "0");

        mongo_enable_auth = Boolean.parseBoolean(getEnviromentField("mongo_enable_auth", prop));
        mongo_oplog = Boolean.parseBoolean(getEnviromentField("mongo_oplog", prop));
        mongo_database_name = getEnviromentField("mongo_database_name", prop);
        mongo_port = Integer.parseInt(
            !getEnviromentField("mongo_port", prop).isEmpty()
                ? getEnviromentField("mongo_port", prop)
                : "0");
        mongo_host = getEnviromentField("mongo_host", prop);
        mongo_list_host = getEnviromentField("mongo_list_host", prop);
        mongo_userName = getEnviromentField("mongo_userName", prop);
        mongo_password = getEnviromentField("mongo_password", prop);
        mongo_replicaSet = getEnviromentField("mongo_replicaSet", prop);
        mongo_authSource = getEnviromentField("mongo_authSource", prop);
        mongo_authMechanism = getEnviromentField("mongo_authMechanism", prop);
        mongo_writeConcern = getEnviromentField("mongo_writeConcern", prop);
        mongo_readPreference = getEnviromentField("mongo_readPreference", prop);
        mongo_maxPoolSize = Integer.parseInt(
            !getEnviromentField("mongo_maxPoolSize", prop).isEmpty()
                ? getEnviromentField("mongo_maxPoolSize", prop)
                : "0");
        mongo_waitQueueMultiple = Integer.parseInt(
            !getEnviromentField("mongo_waitQueueMultiple", prop).isEmpty()
                ? getEnviromentField("mongo_waitQueueMultiple", prop)
                : "0");
        mongo_waitQueueTimeoutMS = Integer.parseInt(
            !getEnviromentField("mongo_waitQueueTimeoutMS", prop).isEmpty()
                ? getEnviromentField("mongo_waitQueueTimeoutMS", prop)
                : "0");

        redis_host = getEnviromentField("redis_host", prop);
        redis_port = Integer.parseInt(
            !getEnviromentField("redis_port", prop).isEmpty()
                ? getEnviromentField("redis_port", prop)
                : "0");
        redis_number = Integer.parseInt(
            !getEnviromentField("redis_number", prop).isEmpty()
                ? getEnviromentField("redis_number", prop)
                : "0");
        redis_number_retry = Integer.parseInt(
            !getEnviromentField("redis_number_retry", prop).isEmpty()
                ? getEnviromentField("redis_number_retry", prop)
                : "3");
        dispatcher_username = getEnviromentField("dispatcher_username", prop);
        dispatcher_password = getEnviromentField("dispatcher_password", prop);

        qup_service = getEnviromentField("qup_service", prop);
        cc_username = getEnviromentField("cc_username", prop);
        cc_password = getEnviromentField("cc_password", prop);
        cc_service_username = getEnviromentField("cc_service_username", prop);
        cc_service_password = getEnviromentField("cc_service_password", prop);

        txgTrip_enable = Boolean.parseBoolean(getEnviromentField("txgStore.enable", prop));
        txgTrip_fleets = getEnviromentField("txgStore.fleets", prop);

        // get dispatcher properties
        dispatch_server = getEnviromentField("dispatch_server", prop);
        dispatch_userName = getEnviromentField("dispatch_userName", prop);
        dispatch_password = getEnviromentField("dispatch_password", prop);

        accessKey = getEnviromentField("accessKey", prop);
        secretKey = getEnviromentField("secretKey", prop);
        bucketName = getEnviromentField("bucketName", prop);
        serverName = getEnviromentField("serverName", prop);
        region = getEnviromentField("region", prop);

        webview_url = getEnviromentField("webview_url", prop);

        mail_config_dir = getEnviromentField("mail_config_dir", prop);
        mail_server = getEnviromentField("mail_server", prop);
        mail_log_gateway = getEnviromentField("mail_log_gateway", prop);

        cronjob_clean_redis = getEnviromentField("cronjob_clean_redis", prop);
        cronjob_payment_audit = getEnviromentField("cronjob_payment_audit", prop);

        default_timezone = getEnviromentField("default_timezone", prop);
        maps_provider_server = "";
        maps_provider_userName = "";
        maps_provider_password = "";
        queue_mode = Boolean.parseBoolean(getEnviromentField("queue_mode", prop));
        queue_business_mode = Boolean.parseBoolean(getEnviromentField("queue_business_mode", prop));
        slack_url = getEnviromentField("slack_url", prop);
        menu_api = getEnviromentField("menu_api", prop);
        menu_username = getEnviromentField("menu_username", prop);
        menu_password = getEnviromentField("menu_password", prop);
        fawry_api_key = getEnviromentField("fawry_api_key", prop);

        hook_server = getEnviromentField("hook_server", prop);
        proxy_server = getEnviromentField("proxy_server", prop);
        webform_url = getEnviromentField("webform_url", prop);
        webform_paymaya = getEnviromentField("webform_paymaya", prop);
        webform_cxpay = getEnviromentField("webform_cxpay", prop);
        webform_register = getEnviromentField("webform_register", prop);
        webform_powertranz = getEnviromentField("webform_powertranz", prop);
        point_system_url = getEnviromentField("point_system_url", prop);
        point_system_user = getEnviromentField("point_system_user", prop);
        point_system_pass = getEnviromentField("point_system_pass", prop);
        notify_url = getEnviromentField("notify_url", prop);
        notify_token = getEnviromentField("notify_token", prop);
        notify_channel = getEnviromentField("notify_channel", prop);
        mysqlConnectOptions = getEnviromentField("mysqlConnectOptions",prop);
        invoice_server = getEnviromentField("invoice_server", prop);
        invoice_userName = getEnviromentField("invoice_userName", prop);
        invoice_password = getEnviromentField("invoice_password", prop);
      } catch (Exception ex) {
        ex.printStackTrace();
        instance = null;
      } finally {
        if (input != null) {
          try {
            input.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }
    }
    return instance;

  }

  private static String getEnviromentField(String propertyKey, Properties prop) {
    String val = OpflowConfigUtil.getConfigValue(propertyKey, null);
    if (val == null) {
      val = prop.getProperty(propertyKey) != null ? prop.getProperty(propertyKey) : "";
    }
    return val;
  }
}
