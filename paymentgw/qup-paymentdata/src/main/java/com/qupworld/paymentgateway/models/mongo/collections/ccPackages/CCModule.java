
package com.qupworld.paymentgateway.models.mongo.collections.ccPackages;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class CCModule {

    public boolean enable;
    public String name;
    public String key;
}
