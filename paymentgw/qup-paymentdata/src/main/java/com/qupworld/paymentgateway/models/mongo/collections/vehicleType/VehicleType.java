
package com.qupworld.paymentgateway.models.mongo.collections.vehicleType;

import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class VehicleType {

    public ObjectId _id;
    public String fleetId;
    public String vehicleType;
    public String icon;
    public String iconSlider;
    public String iconOnMap;
    public String image;
    public String group;
    public List<String> dispatch = new ArrayList<String>();
    public List<String> zoneId = new ArrayList<String>();
    public Integer no;
    public String appDisplayName;
    public Boolean showCarTypeIcon;
    public Boolean destinationRequired;
    public Boolean actualFare;
    public Boolean editBasicFare;
    public Boolean editTax;
    public Boolean editDestinationPax;
    public Boolean editDestinationDrv;
    public Boolean hideDestinationReservation;
    public Boolean hideDestinationOnDemand;
    public Integer maxPassengers;
    public Integer maxLuggages;
    public Double googleETAFactor;
    public Boolean bookNow;
    public Boolean bookLater;
    public Boolean isActive;
    public Boolean pobTimer;
    public List<String> affiliateCarType = new ArrayList<String>();
    public DrvApp drvApp;
    public List<Rate> rates;
    public List<CorpRate> corpRates;
    public FareNormal fareNormal;
    public FareFlat fareFlat;
    public FareHourly fareHourly;
    public boolean food;
    public boolean mart;
    public double markupEstimate;
    public boolean dispatchRideSharing;
    public String menuId;
}
