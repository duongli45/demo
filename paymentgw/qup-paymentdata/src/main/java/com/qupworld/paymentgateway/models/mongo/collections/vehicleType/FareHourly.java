
package com.qupworld.paymentgateway.models.mongo.collections.vehicleType;

import org.bson.types.ObjectId;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class FareHourly {

    public String _id;
    public String name;
    public Boolean isActive;

}
