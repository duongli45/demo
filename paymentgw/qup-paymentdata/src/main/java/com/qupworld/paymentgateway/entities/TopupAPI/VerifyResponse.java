package com.qupworld.paymentgateway.entities.TopupAPI;

/**
 * Created by thuanho on 13/01/2022.
 */
public class VerifyResponse {

    public String secureToken;
    public String phoneNumber;
    public String maskedUser;
}
