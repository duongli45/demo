package com.qupworld.paymentgateway.models.mongo.collections.menuMerchant;

/**
 * Created by thuanho on 22/07/2021.
 */
public class BankInfo {

    public String accountHolder;
    public String accountNumber;
    public String bankName;
    public String bankCode;
    public String ibanNumber;
    public String routingNumber;
    public String sortCode;
    public String IFSCCode;
    public String institutionNumber;
    public String transitNumber;
    public String address;
    public String city;
    public String postalCode;
    public String state;
    public String birthDay;
    public String ssn;
    public String IDnumber;
    public String IDtype;
    public String frontFileId;
    public String verificationDocumentFront;
    public String backFileId;
    public String verificationDocumentBack;
    public boolean isBankAccountOwner;
    public String beneficiaryIDIC;
    public int bankRelationship;
    public String relationshipOtherName;
    public boolean isBankVerified;
    public String bankToken;
    public boolean invalidBank;
}
