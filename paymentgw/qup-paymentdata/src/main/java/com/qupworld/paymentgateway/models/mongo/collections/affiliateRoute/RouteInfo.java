package com.qupworld.paymentgateway.models.mongo.collections.affiliateRoute;

import com.qupworld.paymentgateway.models.mongo.collections.fleet.Currency;

/**
 * Created by hoangnguyen on 7/26/17.
 */
public class RouteInfo {
    public String name;
    public String routeType;
    public Currency currency;
}
