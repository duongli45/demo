
package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GatewayAppotaPay {

    public String fleetId;
    public String environment;
    public String partnerCode;
    public String apiKey;
    public String secretKey;
}
