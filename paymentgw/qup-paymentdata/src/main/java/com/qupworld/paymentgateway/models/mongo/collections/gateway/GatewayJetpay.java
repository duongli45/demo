
package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import org.bson.types.ObjectId;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GatewayJetpay {

    public ObjectId _id;
    public String _class;
    public String fleetId;
    public Boolean isSystem;
    public String environment;
    public String terminalId;
    public String serverURL;
    public String terminalURL;
    public Boolean isActive;

}
