
package com.qupworld.paymentgateway.models.mongo.collections.corporate;

import com.qupworld.paymentgateway.models.mongo.collections.Credit;
import org.bson.types.ObjectId;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Corporate {

    public ObjectId _id;
    public String fleetId;
    public CompanyInfo companyInfo;
    public AdminAccount adminAccount;
    public Commission commission;
    public List<Credit> credits = new ArrayList<Credit>();
    public List<JSONObject> paymentMethods = new ArrayList<>();
    public String registeredFrom;
    public Boolean isActive;
    public Boolean travellerSignature;
    public Boolean trackingLog;
    public Boolean rating;
    public Pricing pricing;
    public boolean isAirline;
    public boolean isCommissionBookingApp;
}
