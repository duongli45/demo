package com.qupworld.paymentgateway.models.mongo.collections.intercityRoute;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import java.util.List;

/**
 * Created by myasus on 4/14/20.
 */
public class LimitSeatLuggage {
    public int warningMinimumSeats;
    public int freeLuggagePerSeat;
    public List<AmountByCurrency> feePerExtraLuggage;
    public boolean allowStartLessMinRequiredSeats;
    public boolean allowToAddExtraLuggage;
}