package com.qupworld.paymentgateway.entities;

import javax.annotation.Generated;
import java.util.List;

/**
 * Created by hoangnguyen on 3/21/19.
 */
@Generated("org.jsonschema2pojo")
public class SettlementReturn {
    public String companyId;
    public String driverId;
    public String driverName;
    public double totalUnsettledAmount;
    public int totalUnsettledTransactions;
    public List<String> books;
}
