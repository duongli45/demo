
package com.qupworld.paymentgateway.models.mongo.collections.account;

import org.bson.types.ObjectId;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class CarType {

    public String name;
    public String make;
    public String model;
    public Integer year;
    public String licensePlateNumber;

}
