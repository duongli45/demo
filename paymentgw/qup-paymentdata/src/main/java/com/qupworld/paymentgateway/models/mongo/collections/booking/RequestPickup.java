
package com.qupworld.paymentgateway.models.mongo.collections.booking;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class RequestPickup {

    public String address;
    public String businessName;
    public String offset;
    public String timezone;
    public String zipCode;
    public String from;
    public String zoneId;
    public List<Double> geo = new ArrayList<Double>();
    public AddressDetails addressDetails;
}
