package com.qupworld.paymentgateway.models.mongo.collections.booking;

/**
 * Created by myasus on 9/6/21.
 */
public class Passenger {
    public String userId;
    public String fullName;
    public String phone;
}