package com.qupworld.paymentgateway.entities;

import java.sql.Timestamp;

/**
 * Created by thuanho on 06/05/2022.
 */
public class MerchantWalletTransaction {

    public long id;
    public String fleetId;
    public String merchantId;
    public String walletType;
    public String transactionType;
    public String transactionId;
    public String bookId;
    public String reason;
    public double amount;
    public double newBalance;
    public String currencyISO;
    public String currencySymbol;
    public String operatorId;
    public String operatorName;
    public String operatorUserName;
    public String merchantName;
    public String merchantUserName;
    public String merchantUserPhone;
    public String merchantUserEmail;
    public Timestamp createdDate;
}
