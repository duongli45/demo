
package com.qupworld.paymentgateway.entities;

import java.util.UUID;

public class MoMoEnt {

    public String type;
    public String partnerCode;
    public String accessKey;
    public String requestId = "";
    public String amount;
    public String orderId;
    public String orderInfo;
    public String orderType;
    public String transId;
    public int errorCode;
    public String message;
    public String localMessage;
    public String payType;
    public String responseTime;
    public String extraData;
    public String signature;

    // for log
    public String jupiterRequestId = "";

    public void setRequestId(String jupiterRequestId) {
        if (jupiterRequestId == null || jupiterRequestId.isEmpty())
            jupiterRequestId = UUID.randomUUID().toString();
        this.jupiterRequestId = jupiterRequestId;
    }

}
