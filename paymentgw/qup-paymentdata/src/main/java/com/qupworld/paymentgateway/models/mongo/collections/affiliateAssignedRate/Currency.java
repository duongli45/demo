package com.qupworld.paymentgateway.models.mongo.collections.affiliateAssignedRate;

/**
 * Created by hoangnguyen on 4/19/17.
 */
public class Currency {
    public String iso;
    public String symbol;
}
