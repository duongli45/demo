package com.qupworld.paymentgateway.models.mongo.collections.promotionCode;

import org.bson.types.ObjectId;

import javax.annotation.Generated;

/**
 * Created by hoangnguyen on 11/24/16.
 */
@Generated("org.jsonschema2pojo")
public class Campaign {
    public String name;
    public ObjectId _id;
    public boolean isActive;
}
