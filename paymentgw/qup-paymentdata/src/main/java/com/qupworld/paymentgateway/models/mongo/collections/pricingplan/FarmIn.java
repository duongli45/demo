
package com.qupworld.paymentgateway.models.mongo.collections.pricingplan;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class FarmIn {

    public Double qupAmount;
    public Double qupPercentage;

}
