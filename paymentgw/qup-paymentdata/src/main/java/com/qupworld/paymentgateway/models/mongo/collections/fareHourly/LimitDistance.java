package com.qupworld.paymentgateway.models.mongo.collections.fareHourly;

import javax.annotation.Generated;

/**
 * Created by hoang.nguyen on 11/28/16.
 */
@Generated("org.jsonschema2pojo")
public class LimitDistance {
    public boolean limited;
    public double covered;
    public double extra;
}
