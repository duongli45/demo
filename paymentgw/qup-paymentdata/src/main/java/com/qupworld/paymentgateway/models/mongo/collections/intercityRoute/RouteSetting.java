package com.qupworld.paymentgateway.models.mongo.collections.intercityRoute;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.CancellationPolicy;

import java.util.List;

/**
 * Created by myasus on 4/14/20.
 */
public class RouteSetting {
    public List<AmountByCurrency> pricePerSeat;
    public double distance;
    public double duration;
    public boolean enable;
    public boolean scheduleEnable;
    public boolean hideRouteEnable;
    public boolean requestTimeEnable;
    public List<AmountByCurrency> extraFee;
    public List<Schedule> schedules;
    public CancelPolicy cancellationPolicy;
    public NoShowPolicy noShowPolicy;
    public List<AmountByCurrency> cancellationPenalty;
}
