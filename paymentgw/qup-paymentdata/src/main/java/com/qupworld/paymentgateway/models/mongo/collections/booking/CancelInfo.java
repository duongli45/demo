
package com.qupworld.paymentgateway.models.mongo.collections.booking;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class CancelInfo {

    public Policies policies;
    public String reason;
    public Integer reasonCode;
    public String cancellerId;
    public String canceller;

}
