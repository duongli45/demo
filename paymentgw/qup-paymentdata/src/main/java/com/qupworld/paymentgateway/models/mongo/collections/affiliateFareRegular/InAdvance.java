
package com.qupworld.paymentgateway.models.mongo.collections.affiliateFareRegular;


public class InAdvance {

    public Boolean isActive;
    public Double beforeAdvanceTime;
    public Double afterAdvanceTime;

}
