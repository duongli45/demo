
package com.qupworld.paymentgateway.models.mongo.collections.pricingplan;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Domestic {

    public Boolean allowRequest;
    public Double qupPayoutAmount;
    public Double qupPayoutPercentage;
    public Boolean allowAccept;
    public Double qupChargeAmount;
    public Double qupChargePercentage;
    public Boolean nearestPriority;
    public Boolean nearestAll;
}
