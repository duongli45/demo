package com.qupworld.paymentgateway.models.mongo.collections.fleet;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import java.util.List;

/**
 * Created by thuanho on 31/07/2023.
 */
public class OutstandingPayment {

    public boolean enable;
    public boolean applyForBusinessProfile;
    public List<AmountByCurrency> amountByCurrencies;
}
