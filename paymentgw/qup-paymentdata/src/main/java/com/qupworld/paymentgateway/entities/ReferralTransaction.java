package com.qupworld.paymentgateway.entities;

import java.sql.Timestamp;

/**
 * Created by thuanho on 11/08/2020.
 */
public class ReferralTransaction {

    public String fleetId;
    public String userId;
    public String companyId;
    public String companyName;
    public String bookId;
    public String phone;
    public String fullName;
    public String driverNumber;
    public String transactionStatus;
    public String referralCode;
    public String currencyISO;
    public double cashIn;
    public double cashOut;
    public double balance;
    public int transferred;
    public Timestamp createdTime;
}
