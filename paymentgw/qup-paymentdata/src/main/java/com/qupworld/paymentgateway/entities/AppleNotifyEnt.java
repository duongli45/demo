package com.qupworld.paymentgateway.entities;

import java.util.UUID;

/**
 * Created by thuanho on 08/05/2023.
 */
public class AppleNotifyEnt {

    public String host;
    public String merchantid;
    public String requestid;
    public String encryptkey;
    public String text;

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }
}

