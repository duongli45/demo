package com.qupworld.paymentgateway.entities;

import java.sql.Timestamp;

/**
 * Created by thuanho on 23/11/2022.
 */
public class HydraPending {

    public String fleetId;
    public String psgFleetId;
    public String bookId;
    public Timestamp completedTime;
    public String userId;
    public int payoutToProviderFleet;
    public int payoutToHomeFleet;
    public double convertSupplierPayout;
    public double convertBuyerPayout;
    public double convertSupplierPenalty;
    public double hydraOutstandingAmount;
    public String hydraPaymentStatus;
    public double exchangeRate;
    public double chargeHomeFleetAmount;
    public String networkType = "";
    public String paymentMethod = "";
}