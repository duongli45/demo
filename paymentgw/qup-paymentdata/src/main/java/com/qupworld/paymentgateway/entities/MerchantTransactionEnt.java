package com.qupworld.paymentgateway.entities;

import java.sql.Timestamp;

/**
 * Created by thuanho on 24/11/2020.
 */
public class MerchantTransactionEnt {

    public String fleetId;
    public String merchantId;
    public String type;
    public String transactionId;
    public double pendingAmount;
    public String currencyISO;
    public Timestamp createdDate;

}

