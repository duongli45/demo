package com.qupworld.paymentgateway.entities;

import javax.annotation.Generated;
import java.sql.Timestamp;

/**
 * Created by qup on 08/03/2019.
 */
@Generated("org.jsonschema2pojo")
public class PaxWalletTransaction {

    public Long id;
    public String fleetId;
    public String userId;
    public String transactionType; // ride/credit/TnG/Stripe
    public String description;
    public String reason;
    public String bookId;
    public String cardNumber;
    public String transactionId;
    public String channelLog;
    public Double amount;
    public Double newBalance;
    public Double bookingFare;
    public Double givenAmount;
    public String currencyISO;
    public String currencySymbol;
    public String customerName;
    public String phoneNumber;
    public String refereeName;
    public String refereePhone;
    public String referralCode;
    public Timestamp createdDate;
    public String serviceType;
    public String walletName;

}
