
package com.qupworld.paymentgateway.models.mongo.collections.flatRoutes;

import javax.annotation.Generated;
import java.util.Set;

@Generated("org.jsonschema2pojo")
public class ZoneData {

    public String name;
    public String type;
    public Set<double[][]> coordinates;
}
