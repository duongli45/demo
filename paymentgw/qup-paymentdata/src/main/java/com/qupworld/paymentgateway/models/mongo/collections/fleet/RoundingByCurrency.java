package com.qupworld.paymentgateway.models.mongo.collections.fleet;

/**
 * Created by myasus on 11/18/19.
 */
public class RoundingByCurrency {
    public String currencyISO;
    public String mode;
    public int digits;
}
