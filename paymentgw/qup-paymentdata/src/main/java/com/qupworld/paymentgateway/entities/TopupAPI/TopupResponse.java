package com.qupworld.paymentgateway.entities.TopupAPI;

/**
 * Created by thuanho on 13/01/2022.
 */
public class TopupResponse {

    public String transactionId;
    public String completedTime;
    public double topupAmount;

}
