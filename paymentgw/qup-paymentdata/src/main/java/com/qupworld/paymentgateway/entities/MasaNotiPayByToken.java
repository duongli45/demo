
package com.qupworld.paymentgateway.entities;


import java.util.UUID;

public class MasaNotiPayByToken {

    public String version;
    public String charset;
    public String language;
    public String signType;
    public String serviceType;
    public String agreementRequestNo;
    public String masapayAgreementNo;
    public String debitSeqNo;
    public String merchantOrderNo;
    public String masapayDebitOrderNo;
    public String currencyCode;
    public String orderAmount;
    public String resultCode;
    public String errCode;
    public String errMsg;
    public String riskLevel;
    public String riskDesc;
    public String ext1;
    public String ext2;
    public String signMsg;

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

}
