package com.qupworld.paymentgateway.models.mongo.collections.booking;

/**
 * Created by thuanho on 08/03/2023.
 */
public class FinishedInfo {

    public String finishedBy; // driver/operator
    public String userId = "";
    public String reason = "";
    public String reasonImage = "";
    public int reasonCode = 0;
}