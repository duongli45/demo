
package com.qupworld.paymentgateway.models.mongo.collections.account;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class FrequentAddress {

    public String address;
    public List<Double> geo = new ArrayList<Double>();
    public String zipCode;
    public String businessName;
    public String city;

}
