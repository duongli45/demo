
package com.qupworld.paymentgateway.models.mongo.collections.flatRoutes;

import org.bson.types.ObjectId;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class FlatRoutes {

    public ObjectId _id;
    public String fleetId;
    public String fareFlatId;
    public String routeName;
    public String routeType;
    public String strFrom;
    public String strTo;
    public Boolean isActive;
    public SingleTrip singleTrip;
    public RoundTrip roundTrip;
    public Zone zone;
    public ZipCode zipCode;
}
