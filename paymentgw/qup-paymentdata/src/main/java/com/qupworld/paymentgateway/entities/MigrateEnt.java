package com.qupworld.paymentgateway.entities;

import java.util.List;
import java.util.UUID;

/**
 * Created by qup on 10/18/16.
 */
public class MigrateEnt {

    public String fleetId;
    public String fromDate;
    public String toDate;
    public String type;
    public List<String> bookId;

    //// MIGRATE CREDIT

    // add card to JetPay
    public String environment;
    public String terminalId;
    public String currencyISO;

    // index of Mysql
    public int start;
    public int end;

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }
}
