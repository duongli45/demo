package com.qupworld.paymentgateway.models.mongo.collections.fleet;

/**
 * Created by myasus on 11/13/19.
 */
public class WaiveOffByZone {
    public String zoneId;
    public String currencyISO;
    public boolean applySurcharge;
    public double amount;
}
