package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.List;
import java.util.UUID;

/**
 * Created by hoang.nguyen on 11/23/16.
 */
public class ETAFareEnt implements PaymentIdentifiable {
    public String bookId;
    public String fleetId;
    public double distance;
    public String vehicleTypeId;
    public String zipCodeFrom;
    public String zipCodeTo;
    public double duration;
    public List<Double> pickup;
    public List<Double> destination;
    public String bookFrom;
    public int bookType;
    public String pickupTime;
    public Integer meetDriver;
    public String userId;
    public String city;
    public String timezone;
    public Double tip;
    public String promoCode;
    public String phone;
    public int actualFare;
    public String corporateId;
    public String packageRateId;
    public List<String> services;
    public int typeRate;
    public String rv;
    public List<RateDetails> rateDetails;
    public String zoneId;
    public String suplierFleetId;
    public List<String> supliers;
    public List<ExtraLocation> extraDestination;
    public int seat;
    public int luggage;
    public String intercityRouteId;
    public int routeNumber;
    public String tripType;  //scheduled | requested
    public double addOnPrice;
    public double markupDifference;
    //for pegasus
    public boolean pegasus;
    public String recurring;
    public int paymentMethod;
    public String jobType;
    public double fleetMarkup;
    public String serviceType; // main purpose is to determine the booking is transport or rideHailing
    public String companyId;

    //for affiliate
    public Integer pricingType; // 0: local, 1: affiliate

    //for edit fare
    public EditFare editFare;

    // for third party
    public double thirdPartyAmount;
    public String currencyISO;

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    public int validateData() throws JsonProcessingException {
        if(this.isEmptyString(this.fleetId)||
                this.isEmptyString(this.vehicleTypeId)||
                this.isEmptyString(this.bookFrom)){
            return 406;
        }
        return 200;
    }

    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }

    @Override
    public String getRequestId() {
        return requestId;
    }
}
