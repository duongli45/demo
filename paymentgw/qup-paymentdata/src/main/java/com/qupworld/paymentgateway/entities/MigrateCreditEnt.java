package com.qupworld.paymentgateway.entities;

import java.util.List;
import java.util.UUID;

/**
 * Created by thuanho on 05/05/2022.
 */
public class MigrateCreditEnt {

    public String fleetId;
    public String currentGateway;
    public String dataURL;
    public List<String> listUsers;

    // for log
    public String requestId = "";

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }
}
