package com.qupworld.paymentgateway.models.mongo.collections.pricingplan;

/**
 * Created by hoangnguyen on 3/14/19.
 */
public class BankingInfo {
    public boolean enable;
    public boolean allowDrvUpdateBanking;
    public int verifyAccount; // 0 - show info only | 1 - force to input, verify info with bank
}
