
package com.qupworld.paymentgateway.models.mongo.collections;

import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class UsersSystem {

    public ObjectId _id;
    public String firstName;
    public String lastName;
    public String userName;
    public String password;
    public String phone;
    public String email;
    public String address;
    public String sPID;
    public Boolean isAdmin;
    public String token;
    public Boolean forgetPassword;
    public String agentId;
    public Boolean viewCharge;
    public List<Credit> credits = new ArrayList<Credit>();
    public Boolean defaultPw;
    public Boolean isActive;
    public Integer v;

}
