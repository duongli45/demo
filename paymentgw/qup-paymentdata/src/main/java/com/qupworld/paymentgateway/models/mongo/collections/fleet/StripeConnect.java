
package com.qupworld.paymentgateway.models.mongo.collections.fleet;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class StripeConnect {

    public String connectToken;
    public String connectStatus;

}
