package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.qupworld.paymentgateway.models.mongo.collections.booking.Recurring;

import java.util.List;
import java.util.UUID;

/**
 * Created by hoangnguyen on 7/18/17.
 */
public class PromoEnt implements PaymentIdentifiable {
    public String fleetId;
    public String bookId;
    public String userId;
    public String promoCode;
    public List<Double> geo;
    public String currentDate;
    public String bookFrom;
    public String currencyISO;
    public String zoneId;
    public Integer serviceType; // 0: transport, 1: intercity, 2: parcel, 3: food, 4: mart
    public Double basicFare = 0.0;
    public Double etaFare;
    public Integer paymentType;
    public String rv;
    public String gateway;
    public Recurring recurring;
    public int primaryPartialMethod = -1;

    //for run script
    public String referralCode;
    public List<String> customers;

    public int validateData() throws JsonProcessingException {
        if(this.isEmptyString(this.fleetId)|| this.isEmptyString(this.promoCode)){
            return 406;
        }
        return 200;
    }

    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    @Override
    public String toString() {
        return "PromoEnt{" +
                "fleetId='" + fleetId + '\'' +
                ", userId='" + userId + '\'' +
                ", promoCode='" + promoCode + '\'' +
                ", geo=" + geo +
                ", currentDate='" + currentDate + '\'' +
                ", bookFrom='" + bookFrom + '\'' +
                ", currencyISO='" + currencyISO + '\'' +
                ", zoneId='" + zoneId + '\'' +
                ", serviceType=" + serviceType +
                ", basicFare=" + basicFare +
                ", etaFare=" + etaFare +
                ", paymentType=" + paymentType +
                ", rv='" + rv + '\'' +
                ", gateway='" + gateway + '\'' +
                ", recurring=" + recurring +
                ", referralCode='" + referralCode + '\'' +
                ", customers=" + customers +
                ", requestId='" + requestId + '\'' +
                '}';
    }

    @Override
    public String getRequestId() {
        return requestId;
    }
}
