
package com.qupworld.paymentgateway.models.mongo.collections.affiliateFareGeneral;

import com.qupworld.paymentgateway.models.mongo.collections.fleetfare.RushHour;
import org.bson.types.ObjectId;

import java.util.List;

public class RateGeneral {
    public ObjectId _id;
    public String type;
    public Boolean tollFee;
    public boolean parkingFee;
    public boolean gasFee;
    public Boolean isActive;
    public List<RushHour> rushHours;
    public Boolean rushHour;
    public TechFee techFee;
    public Tip tip;
    public Tax tax;
    public HeavyTraffic heavyTraffic;
    public OtherFees otherFees;
    public MeetDriver meetDriver;
    public Airport airport;
    public RateInfo rateInfo;

}
