package com.qupworld.paymentgateway.models.mongo.collections.phoneZip;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by qup on 12/6/16.
 */
@Generated("org.jsonschema2pojo")
public class PhoneZip {

    public String country;
    public String stateName;
    public int stateLength;
    public String phoneCodes;
    public int phoneLength;
    public String zipStart;
}
