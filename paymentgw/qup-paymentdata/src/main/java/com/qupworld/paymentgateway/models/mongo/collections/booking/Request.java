
package com.qupworld.paymentgateway.models.mongo.collections.booking;

import com.qupworld.paymentgateway.models.mongo.collections.FleetService;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Request {

    public String pickUpTime;
    public Double tip;
    public String vehicleTypeRequest;
    public String psgFleetId;
    public Integer paymentType;
    public Integer type;
    public int typeRate;
    public Boolean rideSharing;
    public Integer hourly;
    public String packageRateId;
    public String packageRateName;
    public Estimate estimate;
    public MoreInfo moreInfo;
    public String operatorNote;
    public String note;
    public String notes;
    public List<String> vehicleType = new ArrayList<String>();
    public Destination destination;
    public Origin origin;
    public RequestPickup pickup;
    public String promo;
    public String promoValue;
    public List<Service> services;
    public boolean serviceActive;
    public List<ExtraDestination> extraDestination;
    public boolean hideFare;
    public Instructions instructions;
    public String vehicleImageRequest;
    public String promoCustomerType;
    public String gateway;
    public String walletName;
    public String iconUrl;

    public int paxNumber;
    public int luggageNumber;
    public List<FleetService> fleetServices;
    public int primaryPartialMethod = -1;
}
