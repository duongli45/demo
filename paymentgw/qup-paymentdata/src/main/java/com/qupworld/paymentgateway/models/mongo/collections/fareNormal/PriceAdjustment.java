package com.qupworld.paymentgateway.models.mongo.collections.fareNormal;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import java.util.List;

/**
 * Created by myasus on 3/15/21.
 */
public class PriceAdjustment {
    public boolean isActive;
    public int minimumPercent;
    public int maximumPercent;
    public List<AmountByCurrency> valueByCurrencies;

}
