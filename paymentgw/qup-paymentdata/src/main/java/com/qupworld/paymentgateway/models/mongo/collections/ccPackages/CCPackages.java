package com.qupworld.paymentgateway.models.mongo.collections.ccPackages;

import javax.annotation.Generated;
import java.util.List;

/**
 * Created by thuanho on 18/06/2021.
 */
@Generated("org.jsonschema2pojo")
public class CCPackages {

    public String fleetId;
    public String name;
    public List<CCModule> modules;

}
