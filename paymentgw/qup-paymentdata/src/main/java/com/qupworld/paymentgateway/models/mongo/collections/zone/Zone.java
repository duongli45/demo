package com.qupworld.paymentgateway.models.mongo.collections.zone;

import com.qupworld.paymentgateway.models.mongo.collections.fleet.Currency;
import org.bson.types.ObjectId;

import javax.annotation.Generated;
import java.util.List;

/**
 * Created by hoangnguyen on 3/21/17.
 */
@Generated("org.jsonschema2pojo")
public class Zone {

    public ObjectId _id;
    public String fleetId;
    public String zoneName;
    public Boolean assignAffiliate;
    public Boolean activate;
    public Boolean isActive;
    public Currency currency;
    //public Geo geo;
    public List<String> affiliateCarType;
    public List<String> affiliateNow;
    public List<String> affiliateLater;
    public String timezone;

}
