
package com.qupworld.paymentgateway.models.mongo.collections.updateHistory;

import org.bson.types.ObjectId;

import javax.annotation.Generated;
import java.util.Date;

@Generated("org.jsonschema2pojo")
public class UpdateHistory {

    public ObjectId _id;
    public String fleetId;
    public String bookId;
    public Long ticketId;
    public Double oldAmount;
    public Double newAmount;
    public Double newFare;
    public String updateBy;
    public String updateFrom;
    public Date createdDate;
    public Date latestDate;
    public String currencyISO;
    public String reasonUpdate;
}
