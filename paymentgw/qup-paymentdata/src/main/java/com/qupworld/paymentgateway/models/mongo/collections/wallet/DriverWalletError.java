package com.qupworld.paymentgateway.models.mongo.collections.wallet;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.BalanceByCurrency;

import javax.annotation.Generated;
import java.util.List;

/**
 * Created by qup on 13/03/2019.
 */
@Generated("org.jsonschema2pojo")
public class DriverWalletError {

    public String fleetId;
    public String userId;
    //public List<AmountByCurrency> creditBalances;
    public BalanceByCurrency cashWallet;
}
