
package com.qupworld.paymentgateway.models.mongo.collections.affiliateFareGeneral;


public class TechFee {

    public Boolean enable;
    public Double commandCenter;
    public Double paxApp;
    public Double webBooking;
    public Double bookingDashboard;

}
