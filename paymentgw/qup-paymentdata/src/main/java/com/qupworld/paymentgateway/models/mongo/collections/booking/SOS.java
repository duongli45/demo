package com.qupworld.paymentgateway.models.mongo.collections.booking;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by hoangnguyen on 5/9/18.
 */
public class SOS {
    public String fromApp;
    public String time;
    public List<Double> geo = new ArrayList<Double>();
}
