package com.qupworld.paymentgateway.models.mongo.collections.booking;

/**
 * Created by qup on 6/22/17.
 */
public class CompletedInfo {

    public Double totalFare;
    public Double totalBuyFare;
    public int paymentType;
    public int pointsEarned;
    public String paymentStatus;
    public Double total;
    public double totalCharged;
    public double tax;
    public double subTotal;

}
