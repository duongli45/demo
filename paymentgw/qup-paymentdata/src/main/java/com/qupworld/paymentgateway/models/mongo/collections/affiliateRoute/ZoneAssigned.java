package com.qupworld.paymentgateway.models.mongo.collections.affiliateRoute;

import java.util.Set;

/**
 * Created by hoangnguyen on 7/26/17.
 */
public class ZoneAssigned {
    public String zoneId;
    public String name;
    public String type;
    public Set<double[][]> coordinates;
}
