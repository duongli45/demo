package com.qupworld.paymentgateway.models.mongo.collections.affiliateAssignedRate;

import org.bson.types.ObjectId;

/**
 * Created by hoangnguyen on 4/19/17.
 */
public class Rate {
    public String id;
    public String name;
}
