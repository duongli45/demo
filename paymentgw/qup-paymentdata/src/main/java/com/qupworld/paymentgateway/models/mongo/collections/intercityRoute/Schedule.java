package com.qupworld.paymentgateway.models.mongo.collections.intercityRoute;

import java.util.List;

/**
 * Created by myasus on 4/14/20.
 */
public class Schedule {
    public String date;
    public List<Time> times;
}
