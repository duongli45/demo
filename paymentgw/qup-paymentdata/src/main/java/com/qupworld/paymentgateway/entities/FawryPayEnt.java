package com.qupworld.paymentgateway.entities;

import java.sql.Timestamp;

public class FawryPayEnt {
    public String phone;
    public String fleetId;
    public String userId;
    public String type; // cash, credit
    public String transactionId;
    public String description;
    public Double cashIn;
    public Double cashOut;
    public String currencyISO;
    public Timestamp createdDate;
}
