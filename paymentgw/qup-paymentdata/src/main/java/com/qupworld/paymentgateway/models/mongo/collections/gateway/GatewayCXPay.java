
package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GatewayCXPay {

    public String fleetId;
    public String environment;
    public String userName;
    public String password;
    public String apiKey;
    public Boolean isActive;
}
