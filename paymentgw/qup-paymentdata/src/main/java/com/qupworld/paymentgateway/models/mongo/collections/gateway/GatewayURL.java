
package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GatewayURL {

    public String gateway;
    public String environment;
    public String url;
    public String tokenURL;
    public String payURL;
    public String authURL;
    public String serverURL;

    public String boostPayment;
    public String boostAck;
    public String boostRef;
    public String boostVoid;
    public String boostRefund;

}
