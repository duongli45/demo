package com.qupworld.paymentgateway.models.mongo.collections.operations;

/**
 * Created by qup on 7/25/17.
 */
import javax.annotation.Generated;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class Operations {

    public String driverId;
    public Company company;
    public List<String> zoneId;
    public List<String> balanceCurrencies;

}
