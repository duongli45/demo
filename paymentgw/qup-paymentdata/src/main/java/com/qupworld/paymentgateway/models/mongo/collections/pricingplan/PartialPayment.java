
package com.qupworld.paymentgateway.models.mongo.collections.pricingplan;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class PartialPayment {

    public boolean enable;
    public boolean allowPaxCreateBookingEstimateGtBalance;
    public boolean returnChangeInCash;
    public boolean returnChangeToWallet;
}
