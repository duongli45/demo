package com.qupworld.paymentgateway.models.mongo.collections.fareSharing;

import java.util.List;

/**
 * Created by myasus on 12/13/21.
 */
public class FareSharing {
    public String name;
    public String fleetId;
    public List<FeesByCurrency> feesByCurrencies;
}
