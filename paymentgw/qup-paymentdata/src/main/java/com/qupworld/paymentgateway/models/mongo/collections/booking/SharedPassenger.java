package com.qupworld.paymentgateway.models.mongo.collections.booking;

/**
 * Created by myasus on 12/13/21.
 */
public class SharedPassenger {
    public int order;
    public String status; //['engaged', 'droppedOff']
    public double baseFare;
    public double techFee;
    public double totalFare;
    public double totalDistance;
    public double totalDuration;
    public int puPointNumber;
    public int doPointNumber;
    public boolean isStarted;
}
