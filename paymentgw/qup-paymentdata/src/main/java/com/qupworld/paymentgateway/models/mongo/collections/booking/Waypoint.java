package com.qupworld.paymentgateway.models.mongo.collections.booking;

/**
 * Created by myasus on 12/13/21.
 */
public class Waypoint {
    public int order;
    public double distance;
    public double duration;
    public double fare;
    public String time;
    public RequestPickup address;
}
