package com.qupworld.paymentgateway.models.mongo.collections.voucher;

import org.bson.types.ObjectId;

/**
 * Created by thuanho on 12/01/2021.
 */
public class Campaign {

    public ObjectId _id;
    public String name;
    public boolean isActive;
}
