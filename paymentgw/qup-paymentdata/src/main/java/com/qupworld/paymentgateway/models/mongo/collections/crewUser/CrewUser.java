package com.qupworld.paymentgateway.models.mongo.collections.crewUser;

import javax.annotation.Generated;
import java.util.Date;

/**
 * Created by myasus on 5/5/21.
 */
@Generated("org.jsonschema2pojo")
public class CrewUser {
    public String fleetId;
    public String airlineId;
    public String userName;
    public String firstName;
    public String lastName;
    public String phone;
    public String fullName;
    public String fullNameSort;
    public String email;
    public String assistantEmail;
    public String dateOfBirth;
    public String jobTitle;
    public String department;
    public String managerName;
    public String password;
    public Date createdDate;
    public Date latestUpdate;
    public Address address;
    public boolean isActive;
}
