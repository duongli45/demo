
package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GatewayTSYS {

    public String fleetId;
    public String environment;
    public String apiKey;
    public String companyNumber;
    public String merchantNumber;
    public String merchantTerminalNumber;
}
