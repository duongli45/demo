package com.qupworld.paymentgateway.entities;

/**
 * Created by qup on 3/21/17.
 */
public class MOLPayNotificationEnt {

    public String urlAction;
    public String gwOrderid;
    public String amount;
    public String currency;
    public String status;
    public String nbcb;
    public String skey;
    public String tranID;
    public String domain;
    public String paydate;
    public String orderid;
    public String appcode;
    public String error_code;
    public String error_desc;
    public String channel;
    public String extraP;

    // pay to bank notification
    public String payeeID;
    public String operator;
    public String VrfKey;
    public String StatCode;
    public String reference;
    public String mass_id;

    // for log
    public String requestId = "";

}
