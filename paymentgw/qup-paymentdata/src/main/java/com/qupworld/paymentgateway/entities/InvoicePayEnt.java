package com.qupworld.paymentgateway.entities;

import java.util.UUID;

public class InvoicePayEnt {

    public String fleetId;
    public long invoiceId;
    public String chargeNote;
    public String paymentMethod;
    public String token;
    public double amount;
    public String operatorId;
    public String paymentIntentId;

    // for log
    public String requestId = "";

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }
}
