package com.qupworld.paymentgateway.models.mongo.collections.menuMerchant;

import org.bson.types.ObjectId;

import java.util.List;

/**
 * Created by myasus on 5/21/21.
 */
public class MenuMerchant {
    public ObjectId _id;
    public String fleetId;
    public String name;
    public String customerPhone;
    public String commissionType;
    public double commission;
    public List<Commission> commissions;
    public boolean isPreferred;
    public String accountNumber;
    public String accountHolder;
    public boolean invalidBank;
    public ObjectId admin;

    public BankInfo bankInfo;

}
