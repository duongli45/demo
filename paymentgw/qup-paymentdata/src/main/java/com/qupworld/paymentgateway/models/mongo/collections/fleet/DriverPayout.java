
package com.qupworld.paymentgateway.models.mongo.collections.fleet;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class DriverPayout {

    public String gateway;//RazerPay, CIB, credit
    public Boolean showRelationship;
    public boolean payManual;
    public boolean activeConnectAccount;

}
