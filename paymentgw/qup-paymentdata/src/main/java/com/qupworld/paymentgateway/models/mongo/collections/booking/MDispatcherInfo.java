
package com.qupworld.paymentgateway.models.mongo.collections.booking;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class MDispatcherInfo {

    public String phone;
    public String userId;
    public String firstName;
    public String lastName;
    public CreditInfo creditInfo;

}
