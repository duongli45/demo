package com.qupworld.paymentgateway.models.mongo.collections.affiliateFareFlat;

import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareGeneral.RateInfo;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareRegular.CancellationPolicy;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareRegular.NoShowPolicy;
import org.bson.types.ObjectId;

/**
 * Created by hoangnguyen on 7/26/17.
 */
public class AffiliateFlat {
    public ObjectId _id;
    public String type;
    public Boolean isActive;
    public NoShowPolicy noShowPolicy;
    public CancellationPolicy cancellationPolicy;
    public RateInfo rateInfo;
}
