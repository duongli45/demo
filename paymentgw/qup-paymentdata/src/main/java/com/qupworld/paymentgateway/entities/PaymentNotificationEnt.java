package com.qupworld.paymentgateway.entities;
import com.qupworld.paymentgateway.entities.GCash.GCashRequest;
import com.qupworld.paymentgateway.entities.PayWay.PayWayReturnParams;

import java.util.UUID;

/**
 * Copyright (c) 2015 QUp World Inc. All Rights Reserved.
 * <p/>
 * This document contains proprietary and confidential information of QUp.
 * It may not be used for any other purposes, reproduced in whole or in part, nor passed to any organization or person
 * without the specific permission in writing of the Technical Director, QUp.
 *
 * @author QUp
 * @see http://qupworld.com/terms
 * @see http://qupworld.com/privacy
 * <p/>
 * Description
 */

public class PaymentNotificationEnt {

    public String urlAction;
    public String urlaction;
    public String gwOrderid;
    public String gworderid;
    public String operation;

    // PayWay
    public String tran_id;
    public String apv;
    public PayWayReturnParams return_params;

    // Payfort
    public String amount;
    public String response_code;
    public String card_number;
    public String signature;
    public String merchant_identifier;
    public String access_code;
    public String expiry_date;
    public String payment_option;
    public String customer_ip;
    public String language;
    public String eci;
    public String fort_id;
    public String command;
    public String response_message;
    public String merchant_reference;
    public String authorization_code;
    public String customer_email;
    public String token_name;
    public String currency;
    public String remember_me;
    public String status;

    // FAC
    public String MerID;
    public String AcqID;
    public String OrderID;
    public String ResponseCode;
    public String ReasonCode;
    public String ReasonCodeDesc;
    public String TokenizedPAN;
    public String ReferenceNo;
    public String PaddedCardNo;
    public String AuthCode;
    public String CVV2Result;
    public String BillToPostCode;
    public String OriginalResponseCode;
    public String Signature;
    public String SignatureMethod;
    public String Response;
    // POWERTRANZ
    public String Approved;
    public String AuthorizationCode;
    public String TransactionIdentifier;
    public String RRN;
    public String CardBrand;
    public String CardSuffix;
    public String IsoResponseCode;
    public String ResponseMessage;
    public String PanToken;
    public String OrderIdentifier;

    // GCASH
    public String gCashMerchantTransId;
    public GCashRequest request;
    /*public String signature;*/

    // MOLPay
    /*public String amount;*/
    /*public String currency;*/
    /*public String status;*/
    public String nbcb;
    public String skey;
    public String tranID;
    public String domain;
    public String paydate;
    public String orderid;
    public String appcode;
    public String error_code;
    public String error_desc;
    public String channel;
    public String extraP;
    // pay to bank notification
    public String payeeID;
    public String operator;
    public String VrfKey;
    public String StatCode;
    public String reference;
    public String mass_id;
    public String Channel;

    // TnG
    public String TnGMerchantTransId;
    public String acquirementId;
    public String merchantTransId;
    public String finishedTime;
    public String createdTime;
    public String merchantId;
    public String orderAmount;
    public String acquirementStatus;
    public String extendInfo;

    // Senangpay
    /*public String status;*/
    public String status_id;
    public String order_id;
    public String token;
    public String cc_num;
    public String cc_type;
    public String msg;
    public String hash;

    // FirstData
    public String MD;
    public String PaRes;

    // eGHL
    public String TransactionType;
    public String PymtMethod;
    public String IssuingBank;
    public String ServiceID;
    public String PaymentID;
    public String OrderNumber;
    public String Amount;
    public String CurrencyCode;
    public String HashValue2;
    public String TxnID;
    public String TxnStatus;
    public String TxnMessage;
    public String Token;
    public String CardHolder;
    public String CardNoMask;
    public String CardType;

    // PingPong
    public String notificationUrl;
    /*public String amount;*/
    public String clientId;
    public String code;
    public String threeDSecure;
    public String sign;
    public String description;
    public String transactionTime;
    public String transactionId;
    public String paymentType;
    public String outFlowId;
    public String merchantTransactionId;
    public String accId;
    public String signType;
    /*public String currency;*/
    /*public String status;*/

    // Boost
    /*public String amount;*/
    /*public String merchantId;*/
    /*public String transactionTime;*/
    public String onlineRefNum;
    public String transactionType;
    public String customerLast4DigitMSISDN;
    public String transactionStatus;
    public String boostRefNum;
    public String checksum;
    public String message;

    // DNB
    public String dnbSessionId;

    // receive gateway on the request
    public String gateway;

    // TSYS
    public String sessionId;
    public String secureID;
    public String STATUS;

    // YenePay
    public String TotalAmount;
    public String MerchantOrderId;
    public String TransactionId;
    public String Status;

    // PayMaya
    public String id;

    // Telebirr
    public PaymentNotificationData data;
    public String text;

    // Ksher
    public String mch_order_no;
    public String instance;
    public String type;

    // CXPAY
    public String token_id;

    // PayTM
    public String ORDERID;

    // TBC
    public String PaymentId;

    // MADA
    public String TrackId;
    public String TranId;
    public String cardBrand;
    public String maskedPAN;
    public String cardToken;

    // WingBank
    public String contents;

    // Appota Pay
    public int errorCode;
    public String partnerCode;
    public String apiKey;
    public String orderId;
    public String bankCode;
    public String paymentMethod;
    public String appotapayTransId;
    public String transactionTs;
    public String extraData;
    public String tokenResult;

    // ECPay
    public String MerchantTradeNo;
    public String RtnCode;
    public String RtnMsg;
    public String card4no;
    public String card6no;
    public String TradeNo;
    public String auth_code;
    public String gwsr;

    // for log
    public String requestId = "";

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }
}
