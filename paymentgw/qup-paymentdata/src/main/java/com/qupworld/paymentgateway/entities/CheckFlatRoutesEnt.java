package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.List;

/**
 * Created by myasus on 8/3/19.
 */
public class CheckFlatRoutesEnt {
    public List<Double> pickup;
    public List<Double> destination;
    public String zipCodeFrom;
    public String zipCodeTo;
    public List<String> flatRateId;

    public int validateData() throws JsonProcessingException {
        if(this.pickup == null || this.pickup.isEmpty() ||
                this.destination == null || this.destination.isEmpty() ||
                this.flatRateId == null || this.flatRateId.isEmpty()){
            return 406;
        }
        return 200;
    }

}
