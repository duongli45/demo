package com.qupworld.paymentgateway.models.mongo.collections.gateway;

public class GatewayFirstData {

    public String fleetId;
    public Boolean isSystem;
    public String environment;
    public String apiSecret;
    public String apiKey;
    public String storeId;
    public String paymentStoreId;
    public Boolean isActive;

}