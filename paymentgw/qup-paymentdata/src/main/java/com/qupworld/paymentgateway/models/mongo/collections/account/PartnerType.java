
package com.qupworld.paymentgateway.models.mongo.collections.account;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class PartnerType {

    public String partnerTypeId;
    public String name;
    public String nameSort;
    public Boolean isActive;

}
