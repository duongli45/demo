package com.qupworld.paymentgateway.entities;


import java.util.List;

/**
 * Created by myasus on 6/11/20.
 */
public class LocationDetails {
    public String merchantId;
    public List<Double> geo;
    public double distance;
    public double duration;
    //public List<MenuData> menuData;
    public String menuData;
    public int order;
}
