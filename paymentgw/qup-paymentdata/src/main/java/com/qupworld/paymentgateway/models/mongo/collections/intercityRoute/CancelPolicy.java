
package com.qupworld.paymentgateway.models.mongo.collections.intercityRoute;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import javax.annotation.Generated;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class CancelPolicy {

    public boolean isActive;
    //public Double value;
    public List<AmountByCurrency> valueByCurrencies;
    public String payToDrv; // fixAmount | commission
    public boolean enableTax;
    public boolean enableTechFee;
    public List<AmountByCurrency> drvGetAmtByCurrencies;
}
