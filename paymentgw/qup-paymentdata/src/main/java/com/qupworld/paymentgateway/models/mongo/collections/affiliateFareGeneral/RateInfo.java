
package com.qupworld.paymentgateway.models.mongo.collections.affiliateFareGeneral;


public class RateInfo {

    public String name;
    public Currency currency;
    public String priceType;
    public String unitDistance;

}
