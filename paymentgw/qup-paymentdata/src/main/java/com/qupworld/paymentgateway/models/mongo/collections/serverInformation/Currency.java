
package com.qupworld.paymentgateway.models.mongo.collections.serverInformation;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Currency {

    public String symbol;
    public String iso;
    public Double exchangeRate;
    public Double exchangeRateEUR;
    public Double exchangeRateGBP;
    public Boolean isActive;

}
