
package com.qupworld.paymentgateway.models.mongo.collections.flatRoutes;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Zone {

    public ZoneData fromZone;
    public ZoneData toZone;
}
