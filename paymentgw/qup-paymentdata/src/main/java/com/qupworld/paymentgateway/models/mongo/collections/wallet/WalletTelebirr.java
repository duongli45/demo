package com.qupworld.paymentgateway.models.mongo.collections.wallet;

import javax.annotation.Generated;

/**
 * Created by thuanho on 22/02/2021.
 */
@Generated("org.jsonschema2pojo")
public class WalletTelebirr {

    public String fleetId;
    public String environment;
    public String merchantName;
    public String shortCode;
    public String appId;
    public String appKey;
    public String publicKey;
}
