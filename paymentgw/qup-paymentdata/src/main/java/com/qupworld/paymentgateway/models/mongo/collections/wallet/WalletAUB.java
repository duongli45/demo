package com.qupworld.paymentgateway.models.mongo.collections.wallet;

import javax.annotation.Generated;

/**
 * Created by thuanho on 22/02/2021.
 */
@Generated("org.jsonschema2pojo")
public class WalletAUB {

    public String fleetId;
    public String environment;
    public String merchantId;
    public String aubPublic;
    public String aubPrivate;
    public String qupPublic;
    public String qupPrivate;
}