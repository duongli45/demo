
package com.qupworld.paymentgateway.models.mongo.collections.fleetfare;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import javax.annotation.Generated;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class OtherFee {

    public Boolean isEdit;
    public Boolean addNote;
    //public Double value;
    public List<AmountByCurrency> valueByCurrencies;
    public Boolean limitDriverInputActive;
    public List<AmountByCurrency> otherFeeDriverCanInput;
}
