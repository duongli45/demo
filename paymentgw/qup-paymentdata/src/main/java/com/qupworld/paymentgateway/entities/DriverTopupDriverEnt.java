package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.UUID;

/**
 * Created by myasus on 8/27/20.
 */
public class DriverTopupDriverEnt {
    public String fleetId;
    public String senderId;
    public String receiverId;
    public double amount;
    public String currencyISO;
    public String reason;
    // for log
    public String requestId;


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    public int validateData() throws JsonProcessingException {
        if (this.isEmptyString(this.fleetId) || this.isEmptyString(this.senderId) || this.isEmptyString(this.receiverId)
                || this.isEmptyString(this.currencyISO) || amount <= 0.0) {
            return 406;
        }
        return 200;
    }

    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }
}
