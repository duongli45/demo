
package com.qupworld.paymentgateway.models.mongo.collections.booking;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Location {

    public Double lon;
    public Double lat;

}
