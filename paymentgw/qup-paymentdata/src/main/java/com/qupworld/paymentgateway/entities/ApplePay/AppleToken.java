package com.qupworld.paymentgateway.entities.ApplePay;

/**
 * Created by thuanho on 24/05/2022.
 */
public class AppleToken {

    public String version;
    public String data;
    public String signature;
    public TokenHeader header;
}
