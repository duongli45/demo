
package com.qupworld.paymentgateway.entities.FawryFormEnt;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "pmtId",
    "creationDt"
})
public class PmtRevInfo {

    @JsonProperty("pmtId")
    private String pmtId;
    @JsonProperty("creationDt")
    private String creationDt;

    @JsonProperty("pmtId")
    public String getPmtId() {
        return pmtId;
    }

    @JsonProperty("pmtId")
    public void setPmtId(String pmtId) {
        this.pmtId = pmtId;
    }

    @JsonProperty("creationDt")
    public String getCreationDt() {
        return creationDt;
    }

    @JsonProperty("creationDt")
    public void setCreationDt(String creationDt) {
        this.creationDt = creationDt;
    }

}
