
package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.List;
import java.util.UUID;

public class PreAuthEnt {

    public String fleetId = "";
    public String bookId = "";
    public String localToken = "";
    public String crossToken = "";
    public double total = 0.0;
    public int paymentType = 0;
    public boolean reservation = false;
    public int primaryPartialMethod;

    // this currency based on pickup location, same as the currency which is stored on the booking
    // therefore the preAuth and capture always use the same currency
    public String currencyISO = "";

    // use to get card information from user
    public String userId = "";

    // for multi gateway
    public List<Double> geoLocation;
    public String zoneId;

    // to determine the booking is using promo code in case total = 0
    public String destination;

    // support guest checkout
    public String sessionId;

    // support guest checkout
    public int bookType;
    public int typeRate;

    // for log
    public String requestId = "";

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    /**
     * Valid Data
     */
    public int validateData() throws JsonProcessingException {
        if (this.isEmptyString(this.fleetId) ||
                (this.isEmptyString(this.localToken) && this.isEmptyString(this.crossToken)) ) {
            return 406;
        }
        return 200;
    }
    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }
}
