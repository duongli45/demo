package com.qupworld.paymentgateway.models.mongo.collections;

/**
 * Created by thuanho on 19/08/2022.
 */
public class PayDrvForCashBooking {

    public String type; // default = commission
    public double value;
}
