package com.qupworld.paymentgateway.models.mongo.collections;

import javax.annotation.Generated;
import java.util.List;

/**
 * Created by thuanho on 18/03/2022.
 */
@Generated("org.jsonschema2pojo")
public class FleetService {

    public String serviceId;
    public String serviceName;
    public String serviceType; // "Compulsory" | "Optional"
    public List<AmountByCurrency> serviceFeeByCurrencies;
    public FleetCommission fleetCommission;
    public List<String> vehicleType;
    public boolean isActive;
    public boolean active;
    public double fee;
    public String bookingType;
    public boolean applyTax;
    public String serviceFeeType; // amount , percent
    public double serviceFeePercent;
}
