
package com.qupworld.paymentgateway.entities;


public class GetPaymentDataForPayEnt implements PaymentIdentifiable {

    public String bookId;
    public String fleetId;
    public Double distance;
    public Double distanceGG;
    public String requestId;

    @Override
    public String getRequestId() {
        return requestId;
    }
}
