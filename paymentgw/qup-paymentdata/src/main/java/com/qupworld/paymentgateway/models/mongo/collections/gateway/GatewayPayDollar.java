
package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GatewayPayDollar {

    public String fleetId;
    public Boolean isSystem;
    public String environment;
    public String merchantId;
    public String loginName;
    public String loginPassword;
    public String tokenKey;
    public String tokenSalt;
    public Boolean isActive;

}
