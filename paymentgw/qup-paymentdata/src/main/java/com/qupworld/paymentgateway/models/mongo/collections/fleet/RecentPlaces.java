package com.qupworld.paymentgateway.models.mongo.collections.fleet;
import java.util.List;
/**
 * Created by qup on 7/12/16.
 */
public class RecentPlaces {
    List<Place> pickup;
    List<Place> destination;

    public List<Place> getPickup() {
        return pickup;
    }

    public void setPickup(List<Place> pickup) {
        this.pickup = pickup;
    }

    public List<Place> getDestination() {
        return destination;
    }

    public void setDestination(List<Place> destination) {
        this.destination = destination;
    }
}