package com.qupworld.paymentgateway.models.mongo.collections.booking;

/**
 * Created by thuanho on 21/05/2023.
 */
public class ExternalInfo {

    public String bookingReference;
    public String customerReference;
    public String searchResultId;
    public String status;
    public String vehicleType;
    public String thirdParty = "";
    public double price;
}

