package com.qupworld.paymentgateway.entities;

import java.sql.Timestamp;

/**
 * Created by thuanho on 24/11/2020.
 */
public class PayoutHistoryEnt {

    public String fleetId;
    public String driverId;
    public String driverName;
    public String phone;
    public String email;
    public String companyId;
    public String companyName;
    public String driverType;
    public String driverNumber;
    public String driverNumberType;
    public String operatorId;
    public String operatorName;
    public String transactionId;
    public String transactionType;
    public String bopIndicator;
    public double paidAmount;
    public double newBalance;
    public String currencyISO;
    public String bankAccountHolder;
    public String accountNumber;
    public String bankName;
    public String IFSCCode;
    public String bankCode;
    public String payoutId;
    public Timestamp payoutDate;
    public Timestamp createdDate;
    public boolean isBankAccountOwner;
    public String beneficiaryIDIC;
    public int bankRelationship;
    public String relationshipOtherName;
    public String payTo;
    public String merchantId;
    public String merchantName;

}

