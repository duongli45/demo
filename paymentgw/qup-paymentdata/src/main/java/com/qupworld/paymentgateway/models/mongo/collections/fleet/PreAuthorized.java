
package com.qupworld.paymentgateway.models.mongo.collections.fleet;


import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import javax.annotation.Generated;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class PreAuthorized {

    public Boolean isActive;
    public Integer holdPreAuth;
    public Double buffer;
    public List<AmountByCurrency> amountByCurrencies;
    public boolean checkWalletBalance;
    public double walletBuffer;
    public boolean additionalPreauth;
    public double additionalThreshold;

}
