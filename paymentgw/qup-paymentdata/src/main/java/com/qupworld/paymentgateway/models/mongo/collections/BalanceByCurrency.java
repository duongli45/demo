package com.qupworld.paymentgateway.models.mongo.collections;

import javax.annotation.Generated;

/**
 * Created by thuanho on 09/03/2019.
 */
@Generated("org.jsonschema2pojo")
public class BalanceByCurrency {

    public String currencyISO;
    public double currentBalance;
    public double pendingBalance;
    public double availableBalance;
}
