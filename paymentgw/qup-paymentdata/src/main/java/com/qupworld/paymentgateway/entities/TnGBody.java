package com.qupworld.paymentgateway.entities;

/**
 * Created by qup on 06/08/2019.
 */
public class TnGBody {

    public String acquirementId = "";
    public String orderAmount = "";
    public String merchantId = "";
    public String merchantTransId = "";
    public String finishedTime = "";
    public String createdTime = "";
    public String acquirementStatus = "";
}
