package com.qupworld.paymentgateway.models.mongo.collections.fleet;

import com.qupworld.paymentgateway.models.mongo.collections.CreditFee;

import java.util.List;

/**
 * Created by hoangnguyen on 4/2/18.
 */
public class ConfigGateway {
    public String gateway;
    public String environment;
    public boolean requireEmail;
    public boolean requireName;
    public Boolean requirePhone;
    public Boolean supportWeb;
    public List<String> zones;
    public ZipCode zipCode;
    public CreditFee transactionFee;
}
