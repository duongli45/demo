package com.qupworld.paymentgateway.entities;

import java.sql.Timestamp;

/**
 * Created by qup on 26/11/2018.
 */
public class FleetSubscription {

    public Integer fleetSubscriptionId;
    // save fleet info
    public String fleetId;
    public String fleetName;
    public String fleetType;
    public String solutionPartner;

    // save monthly fee from setting
    public Double monthlyFee;

    // save driver fee
    public Integer activeDriver;
    public Integer freeDriver;
    public Integer chargeDriver;
    public Integer chargeDriverOri;
    public Double costPerDriver;
    public Double costPerDriverOri;
    public Double driverFee;
    public Double driverFeeOri;

    // save vehicle fee
    public Integer activeVehicle;
    public Integer freeVehicle;
    public Integer chargeVehicle;
    public Integer chargeVehicleOri;
    public Double costPerVehicle;
    public Double costPerVehicleOri;
    public Double vehicleFee;
    public Double vehicleFeeOri;

    // save transaction fee amount
    public Integer totalTransaction;
    public Integer freeTransaction;
    public Integer chargeTransaction;
    public Integer chargeTransactionOri;
    public Double costPerTransaction;
    public Double costPerTransactionOri;
    public Double transactionFeeAmount;
    public Double transactionFeeAmountOri;
    // save transaction fee percentage
    public Double totalBaseFare;
    public Double percentageSetting;
    public Double transactionFeePercent;
    public Double exchangeRate;
    public String fleetCurrencyISO;
    public String fleetCurrencySymbol;
    public Double transactionFeePercentExchange;
    public Double transactionFeePercentExchangeOri;
    public Double totalTransactionFee;
    public Double totalTransactionFeeOri;

    // save other fee
    public Double otherFee;
    public Double otherFeeOri;

    // save adjust desc
    public String adjustDriverDesc;
    public String adjustVehicleDesc;
    public String adjustTransactionAmountDesc;
    public String adjustTransactionPercentDesc;
    public String adjustOtherDesc;

    // save charge info
    public String invoiceNumber;
    public String invoiceDate;
    public String transactionId;
    public Double subTotalQupFee;
    public Double subTotalQupFeeOri;
    public String status;
    public Double taxSetting;
    public Double tax;
    public Double taxOri;
    public Double totalQupFee;
    public Double totalQupFeeOri;
    public String chargeCurrencyISO;
    public String chargeCurrencySymbol;
    public Integer month;
    public Integer year;
    public Timestamp createdDate;
    public Timestamp completedDate;
    public Timestamp transactionDate;


}
