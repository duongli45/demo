package com.qupworld.paymentgateway.models.mongo.collections.booking;

import javax.annotation.Generated;

/**
 * Created by hoang.nguyen on 1/12/17.
 */
@Generated("org.jsonschema2pojo")
public class Origin {
    public Destination destination;
    public Integer paymentType;
    public double etaDistance;
}
