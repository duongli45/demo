package com.qupworld.paymentgateway.entities;

import java.sql.Timestamp;

/**
 * Created by qup on 08/03/2019.
 */
public class WalletTransferReturn {

    public String fleetId = "";
    public String referenceId = "";
    public String driverId = "";
    public String driverName = "";
    public String driverPhone = "";
    public String driverUserName = "";
    public String companyId = "";
    public String companyName = "";
    public String operatorId = "";
    public String operatorName = "";
    public String description = "";
    public String transferType = ""; // withdraw
    public String receiverAccount = ""; // last 4 number of bank account
    public String reason = ""; // in case reject a request
    public Double amount = 0.0;
    public Double currentBalance = 0.0;
    public String currencyISO = "";
    public String currencySymbol = "";
    public String status = ""; //pending, approved, rejected
    public String createdDate = "";
    public String completedDate = "";
    public String destination; //'creditWallet', 'bankAccount', 'WingBank'
}
