package com.qupworld.paymentgateway.models.mongo.collections.booking;

/**
 * Created by hoangnguyen on 8/10/18.
 */
public class Representative {
    public String firstName;
    public String lastName;
    public String phone;
    public String email;
}
