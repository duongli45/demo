
package com.qupworld.paymentgateway.models.mongo.collections.account;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.CommissionServices;
import com.qupworld.paymentgateway.models.mongo.collections.booking.OfferedQueue;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class DriverInfo {

    public String driverType;
    public String companyInfo;
    public Company company;
    public String driverLicenseNumber;
    public String driverLicenseState;
    public String driverLicenseExp;
    public String dateOfBirth;
    public String document;
    public String link;
    public String notes;
    public CarType carType;
    public Integer liked;
    public Integer disLiked;
    //public Integer commission;
    public String commissionType;
    public Boolean isActivate;
    public Boolean isBankVerified;
    public String achToken;
    public String nameOfBank;
    public String nameOfAccount;
    public String routingNumber;
    public String accountNumber;
    public String bankAddress;
    public String bankCity;
    public String bankState;
    public String bankZip;
    public String ssn;
    public String checkNumber;
    public String legalToken;
    public List<Object> listRoles = new ArrayList<Object>();
    public List<OfferedQueue> offeredQueue = new ArrayList<OfferedQueue>();
    public String policyNumber;
    public String effectiveDate;
    public String expiredDate;
    public String licensePlate;
    public String drvId;
    public String idType;
    public String vehicleId;
    public String statusView;
    public Boolean forceMeter;
    public String terminalId;
    public String authKey;
    public List<AmountByCurrency> commissionByCurrencies;
    public List<AmountByCurrency> creditBalances;
    public Boolean isBankAccountOwner;
    public String beneficiaryIDIC;
    public Integer bankRelationship;
    public String relationshipOtherName;
    public String sortCode;
    public String IFSCCode;
    public String commissionDriverType;
    public List<CommissionServices> commissionDriverValue;

    // verification file
    public String stripeFileId;
    public String verificationDocument;
    public String verificationDocumentBack;
    public String verificationDocumentBackName;

    // additional file
    public String stripeAdditionalFileId;
    public String additionalDocument;
    public String additionalDocumentBack;
    public String additionalDocumentBackName;

    public String stripeConnectStatus;
    public String commissionCompanyType; // 'default', 'customize'
    public List<CommissionServices> commissionCompanyValue;

}
