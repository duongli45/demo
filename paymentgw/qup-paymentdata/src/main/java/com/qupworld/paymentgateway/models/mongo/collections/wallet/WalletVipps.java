package com.qupworld.paymentgateway.models.mongo.collections.wallet;

import javax.annotation.Generated;

/**
 * Created by thuanho on 22/02/2021.
 */
@Generated("org.jsonschema2pojo")
public class WalletVipps {

    public String fleetId;
    public String environment;
    public String merchantSerialNumber;
    public String clientId;
    public String clientSecret;
    public String primarySubscription;
    public String secondarySubscription;
}