package com.qupworld.paymentgateway.models.mongo.collections.vehicleType;

import org.bson.types.ObjectId;

import javax.annotation.Generated;

/**
 * Created by hoangnguyen on 3/21/17.
 */
@Generated("org.jsonschema2pojo")
public class Rate {

    public ObjectId _id;
    public String rateId;
    public String zoneId;
    public String rateType; // Regular || Flat || Hourly
    public boolean rateActive;
}
