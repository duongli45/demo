package com.qupworld.paymentgateway.models.mongo.collections.intercityRoute;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import java.util.List;

/**
 * Created by thuanho on 12/10/2021.
 */
public class NoShowPolicy {

    public boolean isActive;
    //public Double value;
    public List<AmountByCurrency> valueByCurrencies;
    public String payToDrv; // fixAmount | commission
    public boolean enableTax;
    public boolean enableTechFee;
    public List<AmountByCurrency> drvGetAmtByCurrencies;
}
