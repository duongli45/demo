package com.qupworld.paymentgateway.models.mongo.collections.affiliationCarType;

import org.bson.types.ObjectId;

/**
 * Created by hoangnguyen on 4/24/17.
 */
public class AffiliationCarType {
    public ObjectId _id;
    public String name;
    public String iconOnMap;
    public String iconSlider;
    public Boolean showOnMap;
    public Boolean destinationRequired;
    public Boolean isActive;
    public Integer no;
}
