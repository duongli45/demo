
package com.qupworld.paymentgateway.models.mongo.collections.serviceFee;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.FleetService;
import com.qupworld.paymentgateway.models.mongo.collections.TechFeeByCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.fleetfare.*;
import org.bson.types.ObjectId;

import javax.annotation.Generated;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class ServiceFee {

    public ObjectId _id;
    public String fleetId;
    public String zoneId;
    public Boolean rushHourActive;
    public Boolean heavyTrafficActive;
    public Boolean otherFeeActive;
    public Boolean taxActive;
    public Boolean tollFeeActive;
    public Boolean tollFeeLimitDriverInputActive;
    public List<AmountByCurrency> tollFeeDriverCanInput;
    public int tollFeePayTo; // 1 - fleet | 0 - driver
    public Double tax;
    public MeetDriver meetDriver;
    public OtherFee otherFee;
    public List<RushHour> rushHours;
    public Boolean isActive;
    public List<AmountByCurrency> heavyTrafficByCurrencies;
    public boolean techFeeActive;
    public List<TechFeeByCurrency> techFeeByCurrencies;
    public boolean fleetServiceActive;
    public List<FleetService> fleetServices;
    public boolean parkingFeeActive;
    public boolean parkingFeeLimitDriverInputActive;
    public List<AmountByCurrency> parkingFeeDriverCanInput;
    public int parkingFeePayTo; // 1 - fleet | 0 - driver
    public boolean gasFeeActive;
    public boolean gasFeeLimitDriverInputActive;
    public List<AmountByCurrency> gasFeeDriverCanInput;
    public int gasFeePayTo; // 1 - fleet | 0 - driver
}
