
package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.UUID;

public class ACHEnt {

    // NEW FLOW
    public String accountHolder;
    public String accountNumber;
    public String address;
    public String backFileId;
    public String backAddFileId;
    public String bankName;
    public String birthDay;
    public String city;
    public String email;
    public String fleetId;
    public String frontFileId;
    public String frontAddFileId;
    public String ibanNumber;
    public String IDnumber;
    public String IDtype;
    public String institutionNumber;
    public String legalToken;
    public String merchantId;
    public String phone;
    public String postalCode;
    public String routingNumber;
    public String sortCode;
    public String IFSCCode;
    public String ssn;
    public String state;
    public String transitNumber;
    public String verificationDocumentFront;
    public String verificationDocumentBack;
    public String additionalDocumentFront;
    public String additionalDocumentBack;
    public String stripeFileIdFront;
    public String stripeFileIdBack;
    public String stripeAdditionalFileIdFront;
    public String stripeAdditionalFileIdBack;
    public boolean verifyStatus;

    // OLD FLOW
    public String gateway;
    public String type; // driver/merchant
    public String driverId;
    public String bankNumber;
    public String firstName;
    public String lastName;
    public String accountName;
    public boolean isBankAccountOwner;
    public String beneficiaryIDIC;
    public int bankRelationship;
    public String relationshipOtherName;

    // STRIPE
    public String token;
    public String zipCode;
    public String stripeFileId;
    public String verificationDocument;
    public String stripeAdditionalFileId;
    public String additionalDocument;

    // key pair for new file
    /*public String frontFileId;
    public String verificationDocumentFront;
    public String backFileId;
    public String verificationDocumentBack;
    public String frontAddFileId;
    public String additionalDocumentFront;
    public String backAddFileId;
    public String additionalDocumentBack;*/


    // MOLPay
    public String driverIc;
    public String idType;
    public String bankCode;

    // Jetpay
    public String checkNumber;

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    /**
     * Valid request data
     */
    public int validateData() throws JsonProcessingException {
        if (this.isEmptyString(this.fleetId)) {
            return 406;
        }
        return 200;
    }

    public int validateBankToken() throws JsonProcessingException {
        if (this.isEmptyString(this.fleetId) || this.isEmptyString(this.type)) {
            return 406;
        }
        if ("merchant".equals(this.type) && this.isEmptyString(this.merchantId)) {
            return 406;
        }
        if ("driver".equals(this.type) && this.isEmptyString(this.driverId)) {
            return 406;
        }
        return 200;
    }
    private boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }
}
