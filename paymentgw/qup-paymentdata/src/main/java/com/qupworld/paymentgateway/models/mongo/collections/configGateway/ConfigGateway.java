
package com.qupworld.paymentgateway.models.mongo.collections.configGateway;

import com.qupworld.paymentgateway.models.mongo.collections.CreditFee;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class ConfigGateway {

    public String fleetId;
    public String description;
    public String gateway;
    public String environment;
    public Boolean singleGateway;
    public Boolean requireEmail;
    public Boolean requireName;
    public Boolean requirePhone;
    public Boolean supportWeb;
    public List<String> zones = new ArrayList<String>();
    public Date createdDate;
    public Boolean isActive;
    public ZipCode zipCode;
    public CreditFee transactionFee;
    
}
