
package com.qupworld.paymentgateway.models.mongo.collections.account;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Browser {

    public Object major;
    public Object version;
    public Object name;

}
