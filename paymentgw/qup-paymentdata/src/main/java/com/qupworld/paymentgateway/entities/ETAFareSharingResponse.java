package com.qupworld.paymentgateway.entities;

import java.util.List;

/**
 * Created by myasus on 12/13/21.
 */
public class ETAFareSharingResponse {
    public String currencyISO;
    public double totalFare;
    public double minimumFare;
    public double techFee;
    public Waypoint waypoint;
    public List<Passenger> passengers;
}
