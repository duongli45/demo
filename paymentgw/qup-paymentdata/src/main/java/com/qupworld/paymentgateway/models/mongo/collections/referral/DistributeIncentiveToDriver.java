package com.qupworld.paymentgateway.models.mongo.collections.referral;

import java.util.Date;

/**
 * Created by thuanho on 11/08/2020.
 */
public class DistributeIncentiveToDriver {

    public Integer period;
    public Date startDate;
    public Date endDate;
}