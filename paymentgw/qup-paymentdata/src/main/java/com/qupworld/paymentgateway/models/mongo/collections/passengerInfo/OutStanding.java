package com.qupworld.paymentgateway.models.mongo.collections.passengerInfo;

import javax.annotation.Generated;

/**
 * Created by qup on 23/04/2019.
 */
@Generated("org.jsonschema2pojo")
public class OutStanding {

    public Double amount;
    public String currencyISO;
    public String currencySymbol;

}
