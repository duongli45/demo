package com.qupworld.paymentgateway.models.mongo.collections.fleet;

import java.util.List;

/**
 * Created by hoangnguyen on 4/2/18.
 */
public class CreditConfig {
    public boolean enable;
    public boolean multiGateway;
    public List<ConfigGateway> configGateway;
}
