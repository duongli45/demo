package com.qupworld.paymentgateway.models;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.entities.*;
import com.qupworld.paymentgateway.models.mongo.collections.FleetService;
import com.qupworld.paymentgateway.models.mysql.tables.records.*;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.vdurmont.emoji.EmojiParser;
import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.*;
import org.jooq.conf.MappedSchema;
import org.jooq.conf.RenderMapping;
import org.jooq.conf.Settings;
import org.jooq.impl.DSL;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.qupworld.paymentgateway.models.mysql.tables.QupDrivercreditbalance.QUP_DRIVERCREDITBALANCE;
import static com.qupworld.paymentgateway.models.mysql.tables.QupFleetsubscription.QUP_FLEETSUBSCRIPTION;
import static com.qupworld.paymentgateway.models.mysql.tables.QupHydraincome.QUP_HYDRAINCOME;
import static com.qupworld.paymentgateway.models.mysql.tables.QupIntercitytrip.QUP_INTERCITYTRIP;
import static com.qupworld.paymentgateway.models.mysql.tables.QupMerchanttransaction.QUP_MERCHANTTRANSACTION;
import static com.qupworld.paymentgateway.models.mysql.tables.QupPaxwallettransaction.QUP_PAXWALLETTRANSACTION;
import static com.qupworld.paymentgateway.models.mysql.tables.QupPayouthistory.QUP_PAYOUTHISTORY;
import static com.qupworld.paymentgateway.models.mysql.tables.QupPrepaidtransaction.QUP_PREPAIDTRANSACTION;
import static com.qupworld.paymentgateway.models.mysql.tables.QupPromocodeuse.QUP_PROMOCODEUSE;
import static com.qupworld.paymentgateway.models.mysql.tables.QupReferraltransaction.QUP_REFERRALTRANSACTION;
import static com.qupworld.paymentgateway.models.mysql.tables.QupSettlementhistory.QUP_SETTLEMENTHISTORY;
import static com.qupworld.paymentgateway.models.mysql.tables.QupTicket.QUP_TICKET;
import static com.qupworld.paymentgateway.models.mysql.tables.QupTicketextend.QUP_TICKETEXTEND;
import static com.qupworld.paymentgateway.models.mysql.tables.QupTicketextra.QUP_TICKETEXTRA;
import static com.qupworld.paymentgateway.models.mysql.tables.QupWalletbalance.QUP_WALLETBALANCE;
import static com.qupworld.paymentgateway.models.mysql.tables.QupWallettransaction.QUP_WALLETTRANSACTION;
import static com.qupworld.paymentgateway.models.mysql.tables.QupWallettransfer.QUP_WALLETTRANSFER;
import static com.qupworld.paymentgateway.models.mysql.tables.QupMerchantwalletbalance.QUP_MERCHANTWALLETBALANCE;
import static com.qupworld.paymentgateway.models.mysql.tables.QupMerchantwallettransaction.QUP_MERCHANTWALLETTRANSACTION;
import static com.qupworld.paymentgateway.models.mysql.tables.QupHydrapayout.QUP_HYDRAPAYOUT;
import static com.qupworld.paymentgateway.models.mysql.tables.QupHydrapending.QUP_HYDRAPENDING;
import static com.qupworld.paymentgateway.models.mysql.tables.QupTransactiondetails.QUP_TRANSACTIONDETAILS;

import static org.jooq.impl.DSL.*;

/**
 * Created by qup on 7/13/16.
 * Data Access Layer
 */
@SuppressWarnings("unchecked")
public class SQLDaoImpl implements SQLDao {

    // final static Logger logger = Logger.getLogger(SQLDaoImpl.class);
    final static Logger logger = LogManager.getLogger(SQLDaoImpl.class);
    Gson gson = new Gson();
    private static DataSource dataSource = null;
    private static GenericObjectPool connectionPool = null;
    private String SCHEMA = "qupworld";

    private static DataSource dataSourceRead = null;
    private static GenericObjectPool connectionPoolRead = null;
    private String SCHEMA_READ = "qupworld";

    public DataSource setUp() throws Exception {
        try {
            String url = "jdbc:mysql://" + ServerConfig.sql_host + ":" + ServerConfig.sql_port + "/"
                    + ServerConfig.sql_database_name + "?useUnicode=yes&characterEncoding=UTF-8&autoReconnect=true";
            if (ServerConfig.mysqlConnectOptions != null && !ServerConfig.mysqlConnectOptions.isEmpty()) {
                url = url + ServerConfig.mysqlConnectOptions;
            }
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connectionPool = new GenericObjectPool();
            connectionPool.setMinIdle(ServerConfig.sql_setMinIdle);
            connectionPool.setMaxIdle(ServerConfig.sql_setMaxIdle);
            connectionPool.setMaxActive(ServerConfig.sql_setMaxActive);
            connectionPool.setMaxWait(ServerConfig.sql_setMaxWait);
            ConnectionFactory cf = new DriverManagerConnectionFactory(
                    url,
                    ServerConfig.sql_user,
                    ServerConfig.sql_password);

            PoolableConnectionFactory pcf = new PoolableConnectionFactory(cf, connectionPool,
                    null, null, false, true);
            return new PoolingDataSource(connectionPool);
        } catch (Exception ex) {
            notifyToSlack(ex);
            return null;
        }
    }

    public void close() throws Exception {
        if (dataSource != null && dataSource.getConnection() != null) {
            dataSource.getConnection().close();
        }
        if (connectionPool != null) {
            connectionPool.close();
        }
    }

    public static SQLDaoImpl getInstance() {
        return new SQLDaoImpl();
    }

    public DataSource setUpReadConnection() throws Exception {
        try {
            String url = "jdbc:mysql://" + ServerConfig.sql_read_host + ":" + ServerConfig.sql_read_port + "/"
                    + ServerConfig.sql_read_database_name
                    + "?useUnicode=yes&characterEncoding=UTF-8&autoReconnect=true";
            if (ServerConfig.mysqlConnectOptions != null && !ServerConfig.mysqlConnectOptions.isEmpty()) {
                url = url + ServerConfig.mysqlConnectOptions;
            }
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connectionPoolRead = new GenericObjectPool();
            connectionPoolRead.setMinIdle(ServerConfig.sql_read_setMinIdle);
            connectionPoolRead.setMaxIdle(ServerConfig.sql_read_setMaxIdle);
            connectionPoolRead.setMaxActive(ServerConfig.sql_read_setMaxActive);
            connectionPoolRead.setMaxWait(ServerConfig.sql_read_setMaxWait);
            ConnectionFactory cf = new DriverManagerConnectionFactory(
                    url,
                    ServerConfig.sql_read_user,
                    ServerConfig.sql_read_password);

            PoolableConnectionFactory pcf = new PoolableConnectionFactory(cf, connectionPoolRead,
                    null, null, false, true);
            return new PoolingDataSource(connectionPoolRead);
        } catch (Exception ex) {
            notifyToSlack(ex);
            return null;
        }
    }

    private static Connection getConnection(String requestId) throws Exception {
        /*
         * if (ServerConfig.enable_read_write) {
         * System.out.println("calling to getConnection ...");
         * }
         */
        if (dataSource == null) {
            SQLDaoImpl demo = new SQLDaoImpl();
            dataSource = demo.setUp();
        }
        logger.debug(requestId + " - connectionPool active = " + connectionPool.getNumActive());
        logger.debug(requestId + " - connectionPool idle = " + connectionPool.getNumIdle());
        Connection cnn = null;
        try {
            cnn = dataSource.getConnection();
            if (cnn == null || cnn.isClosed()) {
                cnn = dataSource.getConnection();
            }
            if (cnn != null) {
                Gson gson = new Gson();
                logger.debug(requestId + " - connection = " + gson.toJson(cnn.getClientInfo()));
            }
        } catch (Exception ex) {
            notifyToSlack(ex);

            logger.debug(requestId + " - SQL connection error --- retry to get connection from server again");
            if (cnn != null) {
                cnn.close();
            }
            cnn = dataSource.getConnection();
        }
        return cnn;
    }

    public static Connection getReadConnection(String requestId) throws Exception {
        if (ServerConfig.enable_read_write) {
            if (dataSourceRead == null) {
                SQLDaoImpl demo = new SQLDaoImpl();
                dataSourceRead = demo.setUpReadConnection();
            }
            logger.debug(requestId + " - getReadConnection active = " + connectionPoolRead.getNumActive());
            logger.debug(requestId + " - getReadConnection idle = " + connectionPoolRead.getNumIdle());
            Connection cnn = null;
            try {
                cnn = dataSourceRead.getConnection();
                if (cnn == null || cnn.isClosed()) {
                    cnn = dataSourceRead.getConnection();
                }
                if (cnn != null) {
                    Gson gson = new Gson();
                    logger.debug(requestId + " - connection = " + gson.toJson(cnn.getClientInfo()));
                }
            } catch (Exception ex) {
                notifyToSlack(ex);

                logger.debug(requestId + " - SQL connection error --- retry to get connection from server again");
                if (cnn != null) {
                    cnn.close();
                }
                cnn = dataSourceRead.getConnection();
            }
            return cnn;
        } else {
            return getConnection(requestId);
        }
    }

    private Settings getSettings(String mode) {
        try {
            if ("read".equals(mode)) {
                /*
                 * if (SCHEMA_READ.equals("")) {
                 * InputStream input =
                 * Thread.currentThread().getContextClassLoader().getResourceAsStream(
                 * "my.properties");
                 * if (input != null) {
                 * Properties prop = new Properties();
                 * prop.load(input);
                 * SCHEMA_READ = prop.getProperty("local.dbname") != null ?
                 * prop.getProperty("local.dbname"):"qupworld";
                 * }
                 * }
                 */
                return new Settings()
                        .withReturnAllOnUpdatableRecord(true)
                        .withRenderMapping(new RenderMapping()
                                .withSchemata(
                                        new MappedSchema().withInput(SCHEMA_READ)
                                                .withOutput(ServerConfig.sql_read_database_name)));
            } else {
                /*
                 * if (SCHEMA.equals("")) {
                 * InputStream input =
                 * Thread.currentThread().getContextClassLoader().getResourceAsStream(
                 * "my.properties");
                 * if (input != null) {
                 * Properties prop = new Properties();
                 * prop.load(input);
                 * SCHEMA = prop.getProperty("local.dbname") != null ?
                 * prop.getProperty("local.dbname"):"qupworld";
                 * }
                 * }
                 */
                return new Settings()
                        .withReturnAllOnUpdatableRecord(true)
                        .withRenderMapping(new RenderMapping()
                                .withSchemata(
                                        new MappedSchema().withInput(SCHEMA)
                                                .withOutput(ServerConfig.sql_database_name)));
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public String getTransaction() {
        Connection cnnRead = null;
        ArrayList<Object> objectArrayList = new ArrayList<Object>();
        try {
            cnnRead = getReadConnection("");
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            Result<Record> result = contextRead.select().from(QUP_TICKET).fetch();
            for (Record r : result) {
                String transactionNumber = r.getValue(QUP_TICKET.BOOKID);
                double total = r.getValue(QUP_TICKET.TOTAL);
                Map m1 = new HashMap();
                m1.put("transactionNumber", transactionNumber);
                m1.put("total", total);
                objectArrayList.add(m1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return gson.toJson(objectArrayList);
    }

    public JSONObject getConfigAndConnectionStatus() {
        JSONObject map = new JSONObject();

        JSONObject readConnection = new JSONObject();
        if (connectionPoolRead != null) {
            readConnection.put("maxActive", connectionPoolRead.getMaxActive());
            readConnection.put("maxIdle", connectionPoolRead.getMaxIdle());
            readConnection.put("maxWait", connectionPoolRead.getMaxWait());
            readConnection.put("numberActive", connectionPoolRead.getNumActive());
            readConnection.put("numberIdle", connectionPoolRead.getNumIdle());
        }
        map.put("read", readConnection);

        JSONObject writeConnection = new JSONObject();
        if (connectionPool != null) {
            writeConnection.put("maxActive", connectionPool.getMaxActive());
            writeConnection.put("maxIdle", connectionPool.getMaxIdle());
            writeConnection.put("maxWait", connectionPool.getMaxWait());
            writeConnection.put("numberActive", connectionPool.getNumActive());
            writeConnection.put("numberIdle", connectionPool.getNumIdle());
        }
        map.put("write", writeConnection);
        return map;
    }

    public TicketReturn getTicket(final String bookId) {
        Connection cnnRead = null;
        QupTicketRecord qupTicketRecord = null;
        QupTicketextendRecord qupTicketextendRecord = null;
        QupTicketextraRecord qupTicketextraRecord = null;
        try {
            cnnRead = getReadConnection(bookId);

            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            qupTicketRecord = contextRead.selectFrom(QUP_TICKET)
                    .where(QUP_TICKET.BOOKID.eq(bookId))
                    .orderBy(QUP_TICKET.ID.desc())
                    .fetchOne();

            qupTicketextendRecord = contextRead.selectFrom(QUP_TICKETEXTEND)
                    .where(QUP_TICKETEXTEND.BOOKID.eq(bookId))
                    .orderBy(QUP_TICKETEXTEND.ID.desc())
                    .fetchOne();

            qupTicketextraRecord = contextRead.selectFrom(QUP_TICKETEXTRA)
                    .where(QUP_TICKETEXTRA.BOOKID.eq(bookId))
                    .orderBy(QUP_TICKETEXTRA.ID.desc())
                    .fetchOne();

            if (qupTicketRecord != null) {
                return convertTicket(qupTicketRecord, qupTicketextendRecord, qupTicketextraRecord);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void deleteTicket(String bookId) {
        Connection cnn = null;
        try {
            cnn = getConnection(bookId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));

            context.delete(QUP_TICKET)
                    .where(QUP_TICKET.BOOKID.eq(bookId))
                    .returning()
                    .fetchOne();

            context.delete(QUP_TICKETEXTEND)
                    .where(QUP_TICKETEXTEND.BOOKID.eq(bookId))
                    .returning()
                    .fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public QupTicketextendRecord addTicketExtend(final TicketEnt ticketEnt) {
        Connection cnn = null;
        Connection cnnRead = null;
        QupTicketextendRecord record = null;
        try {
            cnn = getConnection(ticketEnt.bookId);
            cnnRead = getReadConnection(ticketEnt.bookId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));

            QupTicketextendRecord qupTicketextendRecord = contextRead.selectFrom(QUP_TICKETEXTEND)
                    .where(QUP_TICKETEXTEND.BOOKID.eq(ticketEnt.bookId))
                    .orderBy(QUP_TICKETEXTEND.ID.desc())
                    .fetchOne();
            if (qupTicketextendRecord == null)
                record = context.insertInto(QUP_TICKETEXTEND)
                        .set(QUP_TICKETEXTEND.BOOKID, ticketEnt.bookId)
                        .set(QUP_TICKETEXTEND.DOUBLE0, ticketEnt.tollFee)
                        .set(QUP_TICKETEXTEND.DOUBLE1, ticketEnt.calBasicFare)
                        .set(QUP_TICKETEXTEND.TEXT0, ticketEnt.otherFeesDetails)
                        .set(QUP_TICKETEXTEND.TEXT7,
                                ticketEnt.additionalFare != null ? ticketEnt.additionalFare.toJSONString() : "")
                        .set(QUP_TICKETEXTEND.BOOLEAN0, ticketEnt.rideSharing ? 1 : 0)
                        .set(QUP_TICKETEXTEND.VARCHAR6,
                                ticketEnt.driverVehicleInfo != null ? ticketEnt.driverVehicleInfo : "")
                        .set(QUP_TICKETEXTEND.TEXT10, String.valueOf(ticketEnt.authAmount))
                        .set(QUP_TICKETEXTEND.BOOLEAN4, ticketEnt.intercity ? 1 : 0)
                        .set(QUP_TICKETEXTEND.BOOLEAN5, ticketEnt.delivery ? 1 : 0)
                        .set(QUP_TICKETEXTEND.VARCHAR9, ticketEnt.tripId)
                        .set(QUP_TICKETEXTEND.BOOLEAN6, ticketEnt.isMinimumTotal ? 1 : 0)
                        .set(QUP_TICKETEXTEND.DATETIME1, ticketEnt.expectedPickupTime)
                        .returning()
                        .fetchOne();
            else
                return qupTicketextendRecord;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return record;
    }

    public QupTicketextraRecord addTicketExtra(String bookId, double referralEarning, double tipAfterRide,
            double unroundedTotalAmt) {
        TicketEnt ticketEnt = new TicketEnt();
        ticketEnt.bookId = bookId;
        ticketEnt.unroundedTotalAmt = unroundedTotalAmt;
        ticketEnt.fleetServices = new ArrayList<>();
        return addTicketExtra(ticketEnt, referralEarning, tipAfterRide);
    }

    private QupTicketextraRecord addTicketExtra(TicketEnt ticketEnt, double referralEarning, double tipAfterRide) {
        Connection cnn = null;
        Connection cnnRead = null;
        QupTicketextraRecord record = null;
        try {
            cnn = getConnection(ticketEnt.bookId);
            cnnRead = getReadConnection(ticketEnt.bookId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));

            QupTicketextraRecord qupTicketextraRecord = contextRead.selectFrom(QUP_TICKETEXTRA)
                    .where(QUP_TICKETEXTRA.BOOKID.eq(ticketEnt.bookId))
                    .orderBy(QUP_TICKETEXTRA.ID.desc())
                    .fetchOne();
            if (qupTicketextraRecord == null) {
                JSONArray jsonArray = new JSONArray();
                JSONObject jsonServiceFee = new JSONObject();
                if (!ticketEnt.fleetServices.isEmpty()) {
                    for (FleetService fleetService : ticketEnt.fleetServices) {
                        jsonArray.add(gson.toJson(fleetService));
                    }
                    jsonServiceFee.put("fleetServiceFee", ticketEnt.serviceFee);
                }

                JSONObject jsonHydra = new JSONObject();
                jsonHydra.put("qupPreferredAmount", ticketEnt.qupPreferredAmount);
                jsonHydra.put("qupBuyPrice", ticketEnt.qupBuyPrice);
                jsonHydra.put("fleetMarkup", ticketEnt.fleetMarkup);
                jsonHydra.put("qupSellPrice", CommonUtils
                        .getRoundValue(ticketEnt.qupBuyPrice + ticketEnt.qupPreferredAmount + ticketEnt.fleetMarkup));
                jsonHydra.put("hydraPaymentMethod",
                        ticketEnt.hydraPaymentMethod != null ? ticketEnt.hydraPaymentMethod : "");
                jsonHydra.put("isFarmOut", ticketEnt.isFarmOut);
                jsonHydra.put("sellPriceMarkup", ticketEnt.sellPriceMarkup);
                record = context.insertInto(QUP_TICKETEXTRA)
                        .set(QUP_TICKETEXTRA.BOOKID, ticketEnt.bookId)
                        .set(QUP_TICKETEXTRA.DOUBLE0, referralEarning)
                        .set(QUP_TICKETEXTRA.DOUBLE1, tipAfterRide)
                        .set(QUP_TICKETEXTRA.DOUBLE2, ticketEnt.unroundedTotalAmt)
                        .set(QUP_TICKETEXTRA.TEXT2, ticketEnt.intercityInfo != null ? ticketEnt.intercityInfo : "")
                        .set(QUP_TICKETEXTRA.DOUBLE6, ticketEnt.remainingAmount)
                        .set(QUP_TICKETEXTRA.DOUBLE7, ticketEnt.itemValue)
                        .set(QUP_TICKETEXTRA.DOUBLE18, ticketEnt.customerDebt)
                        .set(QUP_TICKETEXTRA.TEXT5, jsonArray.toJSONString())
                        .set(QUP_TICKETEXTRA.TEXT6, jsonServiceFee.toJSONString())
                        .set(QUP_TICKETEXTRA.TEXT8, jsonHydra.toJSONString())
                        .set(QUP_TICKETEXTRA.DOUBLE10, ticketEnt.creditTransactionFee)
                        .set(QUP_TICKETEXTRA.DOUBLE19, ticketEnt.merchantCommission)
                        .set(QUP_TICKETEXTRA.DOUBLE19, ticketEnt.merchantCommission)
                                .returning()
                        .fetchOne();
            } else {
                return qupTicketextraRecord;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return record;
    }

    public QupTicketRecord addTicket(final TicketEnt ticketEnt) {
        Connection cnn = null;
        QupTicketRecord record = null;
        try {
            cnn = getConnection(ticketEnt.bookId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            String currencyISO = ticketEnt.currencyISO;
            String currencySymbol = ticketEnt.currencySymbol;
            record = context.insertInto(QUP_TICKET)
                    .set(QUP_TICKET.BOOKID, ticketEnt.bookId)
                    .set(QUP_TICKET.STATUS, 0)
                    .set(QUP_TICKET.FLEETID, ticketEnt.fleetId)
                    .set(QUP_TICKET.PSGFLEETID, ticketEnt.psgFleetId)
                    .set(QUP_TICKET.BOOKFROM, ticketEnt.bookFrom)
                    .set(QUP_TICKET.PLATFORM, ticketEnt.platform)
                    .set(QUP_TICKET.PLATENUMBER, ticketEnt.plateNumber)
                    .set(QUP_TICKET.USERNAME, ticketEnt.userName)
                    .set(QUP_TICKET.AGENTID, ticketEnt.agentId)
                    .set(QUP_TICKET.DISPATCH, ticketEnt.dispatch)
                    .set(QUP_TICKET.QUEUEID, ticketEnt.queueId)
                    .set(QUP_TICKET.QUEUENAME, ticketEnt.queueName)
                    .set(QUP_TICKET.DISPATCHERID, ticketEnt.dispatcherId)
                    .set(QUP_TICKET.MDISPATCHERID, ticketEnt.mDispatcherId)
                    .set(QUP_TICKET.MDISPATCHERNAME, ticketEnt.mDispatcherName)
                    .set(QUP_TICKET.MDISPATCHERTYPEID, ticketEnt.mDispatcherTypeId)
                    .set(QUP_TICKET.MDISPATCHERTYPENAME, ticketEnt.mDispatcherTypeName)
                    .set(QUP_TICKET.CUSTOMERID, ticketEnt.customerId)
                    .set(QUP_TICKET.CUSTOMERNAME, ticketEnt.customerName)
                    .set(QUP_TICKET.CUSTOMERPHONE, ticketEnt.customerPhone)
                    .set(QUP_TICKET.PSGEMAIL, ticketEnt.psgEmail)
                    .set(QUP_TICKET.DRIVERID, ticketEnt.driverId)
                    .set(QUP_TICKET.DRIVERNAME, ticketEnt.driverName)
                    .set(QUP_TICKET.DRIVERNUMBER, ticketEnt.driverNumber)
                    .set(QUP_TICKET.DRIVERLICENSENUMBER, ticketEnt.driverLicenseNumber)
                    .set(QUP_TICKET.PHONE, ticketEnt.phone)
                    .set(QUP_TICKET.VEHICLETYPE, ticketEnt.vehicleType)
                    .set(QUP_TICKET.VEHICLEID, ticketEnt.vehicleId)
                    .set(QUP_TICKET.VHCID, ticketEnt.vhcId)
                    .set(QUP_TICKET.PERMISSION, ticketEnt.permission)
                    .set(QUP_TICKET.PICKUP, ticketEnt.pickup)
                    .set(QUP_TICKET.DESTINATION, ticketEnt.destination)
                    .set(QUP_TICKET.PRICINGTYPE, ticketEnt.pricingType)
                    .set(QUP_TICKET.CURRENCYSYMBOL, currencySymbol)
                    .set(QUP_TICKET.CURRENCYSYMBOLCHARGED, currencySymbol)
                    .set(QUP_TICKET.CURRENCYISO, currencyISO)
                    .set(QUP_TICKET.CURRENCYISOCHARGED, currencyISO)
                    .set(QUP_TICKET.DISTANCETOUR, ticketEnt.distanceTour)
                    .set(QUP_TICKET.PICKUPTIME, ticketEnt.pickupTime)
                    .set(QUP_TICKET.CREATEDTIME, ticketEnt.createdTime)
                    .set(QUP_TICKET.COMPLETEDTIME, ticketEnt.completedTime)
                    .set(QUP_TICKET.DROPPEDOFFTIME, ticketEnt.droppedOffTime)
                    .set(QUP_TICKET.ENGAGEDTIME, ticketEnt.engagedTime)
                    .set(QUP_TICKET.TIMEZONEPICKUP, ticketEnt.timeZonePickup)
                    .set(QUP_TICKET.PICKUPLON, ticketEnt.pickupLon)
                    .set(QUP_TICKET.PICKUPLAT, ticketEnt.pickupLat)
                    .set(QUP_TICKET.TIMEZONEDESTINATION, ticketEnt.timeZoneDestination)
                    .set(QUP_TICKET.DESTINATIONLON, ticketEnt.destinationLon)
                    .set(QUP_TICKET.DESTINATIONLAT, ticketEnt.destinationLat)
                    .set(QUP_TICKET.TYPEBOOKING, ticketEnt.typeBooking)
                    .set(QUP_TICKET.CORPORATEID, ticketEnt.corporateId)
                    .set(QUP_TICKET.CORPORATENAME, ticketEnt.corporateName)
                    .set(QUP_TICKET.CLIENTCASEMATTER, ticketEnt.clientCaseMatter)
                    .set(QUP_TICKET.CHARGECODE, ticketEnt.chargeCode)
                    .set(QUP_TICKET.RANK, ticketEnt.rank)
                    .set(QUP_TICKET.RECEIPTCOMMENT, ticketEnt.receiptComment)
                    // initiate payment data
                    .set(QUP_TICKET.TOTAL, ticketEnt.total)
                    .set(QUP_TICKET.SUBTOTAL, ticketEnt.subTotal)
                    .set(QUP_TICKET.TECHFEE, ticketEnt.techFee)
                    .set(QUP_TICKET.OLDBASICFARE, ticketEnt.fare)
                    .set(QUP_TICKET.FARE, ticketEnt.fare)
                    .set(QUP_TICKET.TIP, ticketEnt.tip)
                    .set(QUP_TICKET.TAX, ticketEnt.tax)
                    .set(QUP_TICKET.PROMOCODE, ticketEnt.promoCode)
                    .set(QUP_TICKET.PROMOAMOUNT, ticketEnt.promoAmount)
                    .set(QUP_TICKET.MEETDRIVERFEE, ticketEnt.meetDriverFee)
                    .set(QUP_TICKET.HEAVYTRAFFIC, ticketEnt.heavyTraffic)
                    .set(QUP_TICKET.AIRPORTSURCHARGE, ticketEnt.airportSurcharge)
                    .set(QUP_TICKET.PARTNERCOMMISSION, ticketEnt.partnerCommission)
                    .set(QUP_TICKET.OTHERFEES, ticketEnt.otherFees)
                    .set(QUP_TICKET.RUSHHOUR, ticketEnt.rushHour)
                    .set(QUP_TICKET.ISMINIMUM, ticketEnt.isMinimum)
                    .set(QUP_TICKET.CANCELLER, ticketEnt.canceller)
                    .set(QUP_TICKET.CANCELLERNAME, ticketEnt.cancellerName)
                    .set(QUP_TICKET.TRANSACTIONSTATUS, ticketEnt.transactionStatus)
                    .set(QUP_TICKET.APPROVALCODE, ticketEnt.approvalCode)
                    .set(QUP_TICKET.COMPANYID, ticketEnt.companyId)
                    .set(QUP_TICKET.COMPANYNAME, ticketEnt.companyName)
                    .set(QUP_TICKET.LOCATIONFROM, ticketEnt.locationFrom)
                    .set(QUP_TICKET.TOKEN, ticketEnt.token)
                    .set(QUP_TICKET.PAYMENTTYPE, ticketEnt.paymentType)
                    .set(QUP_TICKET.BOOKINGFEEACTIVE, ticketEnt.bookingFeeActive)
                    .set(QUP_TICKET.QUPCOMMAMOUNT, ticketEnt.serviceFee)
                    .set(QUP_TICKET.QUPCOMMPCT, ticketEnt.dynamicSurcharge)
                    .set(QUP_TICKET.QUPCOMMTOTAL, ticketEnt.surchargeParameter)
                    .set(QUP_TICKET.BALANCEFARMOUT, ticketEnt.dynamicFare)
                    // SET DEFAULT VALUES
                    .set(QUP_TICKET.ISLOCALZONE, true)
                    .set(QUP_TICKET.TIMEVALUE, 0)
                    .set(QUP_TICKET.COMM, 0.0)
                    .set(QUP_TICKET.DEDUCTIONS, 0.0)
                    .set(QUP_TICKET.NETEARNING, 0.0)
                    .set(QUP_TICKET.TOTALFARE, 0.0)
                    .set(QUP_TICKET.CCHANDLE, 0.0)
                    .set(QUP_TICKET.PAYWITHFLEETACCOUNT, true)
                    .set(QUP_TICKET.BALANCE, 0.0)
                    .set(QUP_TICKET.QUPFEEAMOUNT, 0.0)
                    .set(QUP_TICKET.QUPFEEPERCENT, 0.0)
                    .set(QUP_TICKET.AUTHAMOUNT, 0.0)
                    .set(QUP_TICKET.CHARGEDRIVER, 0)
                    .set(QUP_TICKET.ISTODRIVER, false)
                    .set(QUP_TICKET.PAIDTODRIVER, 0)
                    .set(QUP_TICKET.TECHFEEVALUE, 0.0)
                    .set(QUP_TICKET.MDISPATCHERCOMMISSION, 0.0)
                    .set(QUP_TICKET.TAXVALUE, 0.0)
                    .set(QUP_TICKET.RIDEPAYMENT, 0.0)
                    .set(QUP_TICKET.GROSSEARNING, 0.0)
                    .returning()
                    .fetchOne();
            if (record != null) {
                return record;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public TicketReturn addTicketData(final TicketEnt ticketEnt) {
        TicketReturn ticketReturn = null;
        try {
            QupTicketRecord qupTicketRecord = this.addTicket(ticketEnt);// add to ticket table
            QupTicketextendRecord qupTicketextendRecord = this.addTicketExtend(ticketEnt);// add to ticketExtend table
            QupTicketextraRecord qupTicketextraRecord = this.addTicketExtra(ticketEnt, 0.0, 0.0);
            if (qupTicketRecord != null && qupTicketextendRecord != null && qupTicketextraRecord != null)
                ticketReturn = convertTicket(qupTicketRecord, qupTicketextendRecord, qupTicketextraRecord);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ticketReturn;
    }

    private TicketReturn convertTicket(QupTicketRecord qupTicketRecord, QupTicketextendRecord qupTicketextend,
            QupTicketextraRecord qupTicketextraRecord) throws SQLException {
        TicketReturn ticket = new TicketReturn();
        ResultSet rs = qupTicketRecord.intoResultSet();
        while (rs.next()) {
            ticket.id = rs.getLong("id");
            ticket.fleetId = rs.getString("fleetId");
            ticket.fleetName = rs.getString("fleetName");
            ticket.bookId = rs.getString("bookId");
            ticket.bookFrom = rs.getString("bookFrom");
            ticket.customerId = rs.getString("customerId");
            ticket.customerName = rs.getString("customerName");
            ticket.psgEmail = rs.getString("PsgEmail");
            ticket.driverId = rs.getString("driverId");
            ticket.driverName = rs.getString("driverName");
            ticket.pickup = rs.getString("pickup");
            ticket.destination = rs.getString("destination");
            ticket.plateNumber = rs.getString("plateNumber");
            ticket.isMinimum = rs.getInt("isMinimum");
            ticket.receiptComment = rs.getString("receiptComment");
            ticket.paymentType = rs.getInt("paymentType");
            ticket.status = rs.getInt("status");
            ticket.transactionStatus = rs.getString("transactionStatus");
            ticket.paidBy = rs.getString("paidBy");
            ticket.total = rs.getDouble("total");
            ticket.totalFare = rs.getDouble("totalFare");
            ticket.subTotal = rs.getDouble("subTotal");
            ticket.techFee = rs.getDouble("techFee");
            ticket.oldBasicFare = rs.getDouble("oldBasicFare");
            ticket.fare = rs.getDouble("fare");
            ticket.tip = rs.getDouble("tip");
            ticket.tax = rs.getDouble("tax");
            ticket.promoCode = rs.getString("promoCode");
            ticket.promoAmount = rs.getDouble("promoAmount");
            ticket.meetDriverFee = rs.getDouble("meetDriverFee");
            ticket.heavyTraffic = rs.getDouble("heavyTraffic");
            ticket.airportSurcharge = rs.getDouble("airportSurcharge");
            ticket.partnerCommission = rs.getDouble("mDispatcherCommission");
            ticket.otherFees = rs.getDouble("otherFees");
            ticket.rushHour = rs.getDouble("rushHour");
            ticket.token = rs.getString("token");
            ticket.currencyISO = rs.getString("currencyISO");
            ticket.currencySymbol = rs.getString("currencySymbol");
            ticket.currencyISOCharged = rs.getString("currencyISOCharged");
            ticket.currencySymbolCharged = rs.getString("currencySymbolCharged");
            ticket.totalCharged = rs.getDouble("totalCharged");
            ticket.pricingType = rs.getInt("pricingType");
            ticket.pickupTime = rs.getTimestamp("pickupTime");
            ticket.completedTime = rs.getTimestamp("completedTime");
            ticket.approvalCode = rs.getString("approvalCode");
            ticket.cardType = rs.getString("cardType");
            ticket.distanceTour = rs.getDouble("distanceTour");
            ticket.timeZoneDestination = rs.getString("timeZoneDestination");
            ticket.bookingFeeActive = rs.getInt("bookingFeeActive");
            ticket.mDispatcherId = rs.getString("mDispatcherId");
            ticket.corporateId = rs.getString("corporateId");
            ticket.mDispatcherCommission = rs.getDouble("mDispatcherCommission");
            ticket.totalCharged = rs.getDouble("totalCharged");
            ticket.subTotalCharged = rs.getDouble("subTotalCharged");
            ticket.psgFleetId = rs.getString("psgFleetId");
            ticket.exchangeRate = rs.getDouble("exchangeRate");
            ticket.totalExchange = rs.getDouble("totalExchange");
            ticket.subTotalExchange = rs.getDouble("subTotalExchange");
            ticket.serviceFee = rs.getDouble("qupCommAmount");
            ticket.dynamicSurcharge = rs.getDouble("qupCommPct");
            ticket.surchargeParameter = rs.getDouble("qupCommTotal");
            ticket.dynamicFare = rs.getDouble("balanceFarmOut");
            ticket.canceller = rs.getString("canceller");
            ticket.typeBooking = rs.getString("typeBooking");
            ticket.grossEarning = rs.getDouble("grossEarning");
            ticket.netEarning = rs.getDouble("netEarning");
            ticket.paidToDriver = rs.getInt("paidToDriver") == 1;
        }
        if (qupTicketextend != null) {
            ResultSet rsExtend = qupTicketextend.intoResultSet();
            while (rsExtend.next()) {
                ticket.tollFee = rsExtend.getDouble("double0");
                ticket.calBasicFare = rsExtend.getDouble("double1");
                ticket.otherFeesDetails = rsExtend.getString("text0");
                ticket.rideSharing = rsExtend.getInt("boolean0") > 0;
                ticket.avis3rd = rsExtend.getInt("boolean1") > 0;
                ticket.bookingFeeType = rsExtend.getString("varchar3");
                ticket.paymentTime = rsExtend.getTimestamp("datetime0");
                ticket.expectedPickupTime = rsExtend.getTimestamp("datetime1");
                ticket.isPending = rsExtend.getBoolean("boolean2");
                ticket.authAmount = rsExtend.getString("text10") != null ? Double.valueOf(rsExtend.getString("text10"))
                        : 0;
                ticket.tipAfterPayment = rsExtend.getString("varchar7") != null ? rsExtend.getString("varchar7") : "";
                ticket.intercity = rsExtend.getBoolean("boolean4");
                ticket.delivery = rsExtend.getBoolean("boolean5");
                ticket.tripId = rsExtend.getString("varchar9");
                ticket.isMinimumTotal = rsExtend.getBoolean("boolean6");
                ticket.completedWithoutService = rsExtend.getBoolean("boolean7");
                ticket.chargeCancelPolicy = rsExtend.getBoolean("boolean8");
                ticket.gatewayId = rsExtend.getString("varchar0");
                ticket.cardMasked = rsExtend.getString("varchar5") != null ? rsExtend.getString("varchar5") : "";
                ticket.additionalAuth = rsExtend.getBoolean("boolean9");
                ticket.payFromCC = rsExtend.getBoolean("boolean10");
                ticket.writeOffDebt = rsExtend.getBoolean("boolean11");
            }
        } else {
            ticket.tollFee = 0.0;
            ticket.otherFeesDetails = "";
        }

        if (qupTicketextraRecord != null) {
            ResultSet rsExtra = qupTicketextraRecord.intoResultSet();
            while (rsExtra.next()) {
                ticket.unroundedTotalAmt = rsExtra.getDouble("double2");
                ticket.intercityInfo = rsExtra.getString("text2");
                ticket.originalFare = rsExtra.getDouble("double3");
                ticket.receivedAmount = rsExtra.getDouble("double4");
                ticket.transferredAmount = rsExtra.getDouble("double5");
                ticket.remainingAmount = rsExtra.getDouble("double6");
                ticket.itemValue = rsExtra.getDouble("double7");
                ticket.creditTransactionFee = rsExtra.getDouble("double10");
                ticket.paidToCompany = rsExtra.getInt("int2") == 1;
                String gateway = "";
                String walletName = "";
                String iconUrl = "";
                boolean serviceActive = false;
                String objectUpdateMySql = rsExtra.getString("text0") != null ? rsExtra.getString("text0") : "";
                if (!objectUpdateMySql.isEmpty()) {
                    org.json.JSONObject jsonObject = gson.fromJson(objectUpdateMySql, org.json.JSONObject.class);
                    gateway = jsonObject.has("gateway") ? jsonObject.getString("gateway") : "";
                    walletName = jsonObject.has("walletName") ? jsonObject.getString("walletName") : "";
                    iconUrl = jsonObject.has("iconUrl") ? jsonObject.getString("iconUrl") : "";
                    serviceActive = jsonObject.has("serviceActive")
                            && Boolean.parseBoolean(jsonObject.getString("serviceActive"));
                }
                ticket.gateway = gateway;
                ticket.walletName = walletName;
                ticket.iconUrl = iconUrl;
                ticket.serviceActive = serviceActive;

                String objFleetServices = rsExtra.getString("text5") != null ? rsExtra.getString("text5") : "";
                List<FleetService> fleetServices = new ArrayList<>();
                if (!objFleetServices.isEmpty()) {
                    Gson gson = new Gson();
                    try {
                        JSONArray jsonArray = gson.fromJson(objFleetServices, JSONArray.class);
                        for (int i = 0; i < jsonArray.size(); i++) {
                            FleetService fleetService = gson.fromJson(jsonArray.get(i).toString(), FleetService.class);
                            fleetServices.add(fleetService);
                        }
                    } catch (Exception ex) {
                        logger.debug("convert FleetService error " + CommonUtils.getError(ex));
                    }
                }
                ticket.fleetServices = fleetServices;

                String objFleetServiceFee = rsExtra.getString("text6") != null ? rsExtra.getString("text6") : "";
                double fleetServiceFee = 0.0;
                if (!objFleetServiceFee.isEmpty()) {
                    JSONObject jsonObject = gson.fromJson(objFleetServiceFee, JSONObject.class);
                    fleetServiceFee = jsonObject.get("fleetServiceFee") != null
                            ? Double.parseDouble(jsonObject.get("fleetServiceFee").toString())
                            : 0.0;
                }
                ticket.fleetServiceFee = fleetServiceFee;
                ticket.driverTax = rsExtra.getDouble("double11");

                ticket.cashReceived = rsExtra.getDouble("double4");
                String objPartialPayment = rsExtra.getString("text7") != null ? rsExtra.getString("text7") : "";
                boolean splitPayment = false;
                double paidByWallet = 0.0;
                double paidByOtherMethod = 0.0;
                double transferredChangeByWallet = 0.0;
                double returnedChangeByCash = 0.0;
                int primaryPartialMethod = -1;
                String otherMethod = "";
                String returnChange = "";
                if (!objPartialPayment.isEmpty()) {
                    org.json.JSONObject jsonPartialPayment = new org.json.JSONObject(objPartialPayment);
                    splitPayment = jsonPartialPayment.has("splitPayment")
                            && jsonPartialPayment.getBoolean("splitPayment");
                    paidByWallet = jsonPartialPayment.has("paidByWallet") ? jsonPartialPayment.getDouble("paidByWallet")
                            : 0.0;
                    paidByOtherMethod = jsonPartialPayment.has("paidByOtherMethod")
                            ? jsonPartialPayment.getDouble("paidByOtherMethod")
                            : 0.0;
                    transferredChangeByWallet = jsonPartialPayment.has("transferredChangeByWallet")
                            ? jsonPartialPayment.getDouble("transferredChangeByWallet")
                            : 0.0;
                    returnedChangeByCash = jsonPartialPayment.has("returnedChangeByCash")
                            ? jsonPartialPayment.getDouble("returnedChangeByCash")
                            : 0.0;
                    primaryPartialMethod = jsonPartialPayment.has("primaryPartialMethod")
                            ? jsonPartialPayment.getInt("primaryPartialMethod")
                            : -1;
                    otherMethod = jsonPartialPayment.has("otherMethod") && !jsonPartialPayment.isNull("otherMethod")
                            ? jsonPartialPayment.getString("otherMethod")
                            : "";
                    returnChange = jsonPartialPayment.has("returnChange") && !jsonPartialPayment.isNull("returnChange")
                            ? jsonPartialPayment.getString("returnChange")
                            : "";
                }
                ticket.splitPayment = splitPayment;
                ticket.paidByWallet = paidByWallet;
                ticket.paidByOtherMethod = paidByOtherMethod;
                ticket.transferredChangeByWallet = transferredChangeByWallet;
                ticket.returnedChangeByCash = returnedChangeByCash;
                ticket.primaryPartialMethod = primaryPartialMethod;
                ticket.otherMethod = otherMethod;
                ticket.driverTax = rsExtra.getDouble("double11");
                ticket.returnChange = returnChange;

                String objHydra = rsExtra.getString("text8") != null ? rsExtra.getString("text8") : "";
                double qupPreferredAmount = 0.0;
                double qupBuyPrice = 0.0;
                double qupSellPrice = 0.0;
                double fleetMarkup = 0.0;
                boolean isFarmOut = false;
                String hydraPaymentMethod = "";
                double sellPriceMarkup = 0.0;
                if (!objHydra.isEmpty()) {
                    org.json.JSONObject jsonHydra = new org.json.JSONObject(objHydra);
                    qupPreferredAmount = jsonHydra.has("qupPreferredAmount") ? jsonHydra.getDouble("qupPreferredAmount")
                            : 0.0;
                    qupBuyPrice = jsonHydra.has("qupBuyPrice") ? jsonHydra.getDouble("qupBuyPrice") : 0.0;
                    qupSellPrice = jsonHydra.has("qupSellPrice") ? jsonHydra.getDouble("qupSellPrice") : 0.0;
                    fleetMarkup = jsonHydra.has("fleetMarkup") ? jsonHydra.getDouble("fleetMarkup") : 0.0;
                    hydraPaymentMethod = jsonHydra.has("hydraPaymentMethod") && !jsonHydra.isNull("hydraPaymentMethod")
                            ? jsonHydra.getString("hydraPaymentMethod")
                            : "";
                    isFarmOut = (jsonHydra.has("isFarmOut") && !jsonHydra.isNull("isFarmOut"))
                            && jsonHydra.getBoolean("isFarmOut");
                    sellPriceMarkup = jsonHydra.has("sellPriceMarkup") ? jsonHydra.getDouble("sellPriceMarkup") : 0.0;
                }
                ticket.qupPreferredAmount = qupPreferredAmount;
                ticket.qupBuyPrice = qupBuyPrice;
                ticket.qupSellPrice = qupSellPrice;
                ticket.fleetMarkup = fleetMarkup;
                ticket.hydraPaymentMethod = hydraPaymentMethod;
                ticket.isFarmOut = isFarmOut;
                ticket.sellPriceMarkup = sellPriceMarkup;

                ticket.payoutToProviderFleet = rsExtra.getInt("int0") == 1;
                ticket.payoutToHomeFleet = rsExtra.getInt("int1") == 1;
                ticket.supplierPayout = rsExtra.getDouble("double12");
                ticket.buyerPayout = rsExtra.getDouble("double13");
                ticket.penaltySupplier = rsExtra.getDouble("double14");
                ticket.supplierFailedId = rsExtra.getString("varchar0") != null ? rsExtra.getString("varchar0") : "";
                ticket.buyerFailedId = rsExtra.getString("varchar1") != null ? rsExtra.getString("varchar1") : "";
                ticket.hydraOutstandingAmount = rsExtra.getDouble("double15");
                ticket.stripeMethod = rsExtra.getString("varchar5") != null ? rsExtra.getString("varchar5") : "";
                ticket.customerDebt = rsExtra.getDouble("double18");
                ticket.parkingFee = rsExtra.getDouble("double20");
                ticket.gasFee = rsExtra.getDouble("double21");
            }
        } else {
            ticket.unroundedTotalAmt = ticket.total;
        }

        // return cash changed for cash excess booking
        ticket.changedAmount = 0.0;
        String returnChange = ticket.returnChange != null ? ticket.returnChange : "";
        if (returnChange.equals("wallet")) {
            ticket.changedAmount = CommonUtils
                    .getRoundValue(ticket.receivedAmount - ticket.total + ticket.paidByWallet);
        } else if (ticket.receivedAmount > ticket.totalCharged && !returnChange.equals("cash")) {
            // if not split payment - check if partial payment using cash
            if (ticket.returnedChangeByCash == 0) {
                ticket.changedAmount = Double.parseDouble(
                        KeysUtil.DECIMAL_FORMAT.format(ticket.receivedAmount - ticket.total - ticket.customerDebt));
            }
        }
        return ticket;
    }

    public int addPaymentData(final String bookId, final PaymentEnt paymentEnt, final double unroundedTotalAmt) {
        Connection cnn = null;
        try {
            cnn = getConnection(bookId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));

            // set default value
            if (paymentEnt.totalCharged == 0 && !KeysUtil.INCOMPLETE_PAYMENT.contains(paymentEnt.transactionStatus)
                    && !paymentEnt.isPartialPayment && !paymentEnt.isPending && !paymentEnt.completedWithoutService) {
                paymentEnt.totalCharged = paymentEnt.total;
                paymentEnt.subTotalCharged = paymentEnt.subTotal;
            }
            //// move ra ngoai
            // check editFare, sau nay viet ham chung de chi get 1 lan,kien tra ticket da
            //// tao chua
            double oldBasicFare = 0.0;
            int editFare = 0;
            TicketReturn ticket = this.getTicket(bookId);
            if (ticket != null) {
                oldBasicFare = ticket.oldBasicFare;
                if (paymentEnt.currencyISOCharged.equals("")) {
                    paymentEnt.currencyISOCharged = ticket.currencyISO;
                    paymentEnt.currencySymbolCharged = ticket.currencySymbol;
                }
            }

            if (paymentEnt.transactionStatus.equals(KeysUtil.PAYMENT_INCIDENT)) {
                oldBasicFare = 0;
            }

            NumberFormat formatter = new DecimalFormat("#0.00");
            if (!formatter.format(oldBasicFare).equalsIgnoreCase(formatter.format(paymentEnt.fare))
                    && !paymentEnt.transactionStatus.equals(KeysUtil.PAYMENT_INCIDENT)) {
                editFare = 1;
            }
            ////
            int returnCode = context.update(QUP_TICKET)
                    .set(QUP_TICKET.STATUS, 1)
                    .set(QUP_TICKET.TOTAL, paymentEnt.total)
                    .set(QUP_TICKET.SUBTOTAL, paymentEnt.subTotal)
                    .set(QUP_TICKET.TECHFEE, paymentEnt.techFee)
                    .set(QUP_TICKET.FARE, paymentEnt.fare)
                    .set(QUP_TICKET.OLDBASICFARE, oldBasicFare)
                    .set(QUP_TICKET.EDITFARE, editFare)
                    .set(QUP_TICKET.TIP, paymentEnt.tip)
                    .set(QUP_TICKET.TAX, paymentEnt.tax)
                    .set(QUP_TICKET.PROMOCODE, paymentEnt.promoCode)
                    .set(QUP_TICKET.PROMOAMOUNT, paymentEnt.promoAmount)
                    .set(QUP_TICKET.MEETDRIVERFEE, paymentEnt.meetDriverFee) // this field is set when add ticket, not
                                                                             // update
                    .set(QUP_TICKET.HEAVYTRAFFIC, paymentEnt.heavyTraffic)
                    .set(QUP_TICKET.AIRPORTSURCHARGE, paymentEnt.airportSurcharge)
                    .set(QUP_TICKET.MDISPATCHERCOMMISSION, paymentEnt.partnerCommission)
                    .set(QUP_TICKET.OTHERFEES, paymentEnt.otherFees)
                    .set(QUP_TICKET.RUSHHOUR, paymentEnt.rushHour)
                    .set(QUP_TICKET.ISMINIMUM, paymentEnt.isMinimum)
                    .set(QUP_TICKET.CANCELLER, paymentEnt.canceller)
                    .set(QUP_TICKET.CANCELLERNAME, paymentEnt.cancellerName)
                    .set(QUP_TICKET.TRANSACTIONSTATUS, paymentEnt.transactionStatus)
                    .set(QUP_TICKET.PAIDBY, paymentEnt.paidBy)
                    .set(QUP_TICKET.CARDTYPE, paymentEnt.cardType)
                    .set(QUP_TICKET.APPROVALCODE, paymentEnt.approvalCode)
                    .set(QUP_TICKET.ISLOCALZONE, paymentEnt.isLocalZone)
                    .set(QUP_TICKET.COMPLETEDTIME, paymentEnt.completedTime)
                    .set(QUP_TICKET.CURRENCYISOCHARGED, paymentEnt.currencyISOCharged)
                    .set(QUP_TICKET.CURRENCYSYMBOLCHARGED, paymentEnt.currencySymbolCharged)
                    .set(QUP_TICKET.SUBTOTALCHARGED, paymentEnt.subTotalCharged)
                    .set(QUP_TICKET.TOTALCHARGED, paymentEnt.totalCharged)
                    .set(QUP_TICKET.EXCHANGERATE, paymentEnt.exchangeRate)
                    .set(QUP_TICKET.SUBTOTALEXCHANGE, paymentEnt.subTotalExchange)
                    .set(QUP_TICKET.TOTALEXCHANGE, paymentEnt.totalExchange)
                    .set(QUP_TICKET.ISTODRIVER, paymentEnt.isToDriver)
                    .set(QUP_TICKET.QUPCOMMAMOUNT, paymentEnt.serviceFee)
                    .set(QUP_TICKET.GROSSEARNING, paymentEnt.grossEarning)
                    .where(QUP_TICKET.BOOKID.eq(bookId))

                    .execute();

            String otherFeesDetails = paymentEnt.otherFeesDetails != null
                    ? EmojiParser.removeAllEmojis(paymentEnt.otherFeesDetails)
                    : "";
            context.update(QUP_TICKETEXTEND)
                    .set(QUP_TICKETEXTEND.DOUBLE0, paymentEnt.tollFee)
                    .set(QUP_TICKETEXTEND.DOUBLE1, oldBasicFare)
                    .set(QUP_TICKETEXTEND.TEXT0, otherFeesDetails)
                    .set(QUP_TICKETEXTEND.VARCHAR0, paymentEnt.gatewayId)
                    .set(QUP_TICKETEXTEND.BOOLEAN1, paymentEnt.avis3rd)
                    .set(QUP_TICKETEXTEND.DATETIME0, paymentEnt.paymentTime)
                    .set(QUP_TICKETEXTEND.BOOLEAN2, paymentEnt.isPending ? 1 : 0)
                    .set(QUP_TICKETEXTEND.BOOLEAN7, paymentEnt.completedWithoutService ? 1 : 0)
                    .set(QUP_TICKETEXTEND.BOOLEAN8, paymentEnt.isCharge ? 1 : 0)
                    .set(QUP_TICKETEXTEND.BOOLEAN9, paymentEnt.additionalAuth ? 1 : 0)
                    .set(QUP_TICKETEXTEND.BOOLEAN10, paymentEnt.payFromCC ? 1 : 0)
                    .set(QUP_TICKETEXTEND.BOOLEAN11, paymentEnt.writeOffDebt ? 1 : 0)
                    .where(QUP_TICKETEXTEND.BOOKID.equal(bookId))
                    .returning()
                    .fetchOne();

            // set default value of receivedAmount for normal cash booking
            if (paymentEnt.receivedAmount == 0.0 && paymentEnt.transactionStatus.equals(KeysUtil.PAYMENT_CASH)) {
                if (paymentEnt.splitPayment) {
                    paymentEnt.receivedAmount = paymentEnt.paidByOtherMethod;
                } else {
                    paymentEnt.receivedAmount = paymentEnt.total;
                }
            }

            JSONObject objPartialPayment = new JSONObject();
            objPartialPayment.put("splitPayment", paymentEnt.splitPayment);
            objPartialPayment.put("paidByWallet", paymentEnt.paidByWallet);
            objPartialPayment.put("paidByOtherMethod", paymentEnt.paidByOtherMethod);
            objPartialPayment.put("transferredChangeByWallet", paymentEnt.transferredChangeByWallet);
            objPartialPayment.put("returnedChangeByCash", paymentEnt.returnedChangeByCash);
            objPartialPayment.put("primaryPartialMethod", paymentEnt.primaryPartialMethod);
            objPartialPayment.put("otherMethod", paymentEnt.otherMethod);
            objPartialPayment.put("returnChange", paymentEnt.returnChange);

            JSONObject jsonHydra = new JSONObject();
            jsonHydra.put("qupPreferredAmount", paymentEnt.qupPreferredAmount);
            jsonHydra.put("qupBuyPrice", paymentEnt.qupBuyPrice);
            jsonHydra.put("qupSellPrice",
                    CommonUtils.getRoundValue(paymentEnt.qupBuyPrice + paymentEnt.qupPreferredAmount));
            jsonHydra.put("fleetMarkup", paymentEnt.fleetMarkup);
            jsonHydra.put("hydraPaymentMethod",
                    paymentEnt.hydraPaymentMethod != null ? paymentEnt.hydraPaymentMethod : "");
            jsonHydra.put("isFarmOut", paymentEnt.isFarmOut);
            jsonHydra.put("sellPriceMarkup", paymentEnt.sellPriceMarkup);

            context.update(QUP_TICKETEXTRA)
                    .set(QUP_TICKETEXTRA.DOUBLE2, unroundedTotalAmt)
                    .set(QUP_TICKETEXTRA.DOUBLE3, paymentEnt.originalFare)
                    .set(QUP_TICKETEXTRA.DOUBLE4, paymentEnt.receivedAmount)
                    .set(QUP_TICKETEXTRA.DOUBLE5, paymentEnt.transferredAmount)
                    .set(QUP_TICKETEXTRA.DOUBLE7, paymentEnt.itemValue)
                    .set(QUP_TICKETEXTRA.DOUBLE10, paymentEnt.creditTransactionFee)
                    .set(QUP_TICKETEXTRA.TEXT7, objPartialPayment.toJSONString())
                    .set(QUP_TICKETEXTRA.DOUBLE11, paymentEnt.driverTax)
                    .set(QUP_TICKETEXTRA.TEXT8, jsonHydra.toJSONString())
                    .set(QUP_TICKETEXTRA.VARCHAR2, paymentEnt.hydraPaymentStatus)
                    .set(QUP_TICKETEXTRA.DOUBLE15, paymentEnt.hydraOutstandingAmount)
                    .set(QUP_TICKETEXTRA.VARCHAR3, paymentEnt.finishedBookingStatus)
                    .set(QUP_TICKETEXTRA.VARCHAR5, paymentEnt.stripeMethod)
                    .set(QUP_TICKETEXTRA.DOUBLE20, paymentEnt.parkingFee)
                    .set(QUP_TICKETEXTRA.DOUBLE21, paymentEnt.gasFee)
                    .where(QUP_TICKETEXTRA.BOOKID.equal(bookId))
                    .returning()
                    .fetchOne();

            return returnCode;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public int addPaymentFailedData(final String bookId, final PaymentEnt paymentEnt, String services,
            double remainingAmount) {
        Connection cnn = null;
        try {
            cnn = getConnection(bookId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            ////
            int returnCode = context.update(QUP_TICKET)
                    .set(QUP_TICKET.TOTAL, paymentEnt.total)
                    .set(QUP_TICKET.FARE, paymentEnt.fare)
                    .set(QUP_TICKET.HEAVYTRAFFIC, paymentEnt.heavyTraffic)
                    .set(QUP_TICKET.AIRPORTSURCHARGE, paymentEnt.airportSurcharge)
                    .set(QUP_TICKET.PROMOCODE, paymentEnt.promoCode)
                    .set(QUP_TICKET.PROMOAMOUNT, paymentEnt.promoAmount)
                    .set(QUP_TICKET.TIP, paymentEnt.tip)
                    .set(QUP_TICKET.TECHFEE, paymentEnt.techFee)
                    .set(QUP_TICKET.RUSHHOUR, paymentEnt.rushHour)
                    .set(QUP_TICKET.MDISPATCHERCOMMISSION, paymentEnt.partnerCommission)
                    .set(QUP_TICKET.TAX, paymentEnt.tax)
                    .set(QUP_TICKET.OTHERFEES, paymentEnt.otherFees)
                    .set(QUP_TICKET.SUBTOTAL, paymentEnt.subTotal)
                    .set(QUP_TICKET.ISMINIMUM, paymentEnt.isMinimum)
                    .where(QUP_TICKET.BOOKID.eq(bookId))
                    .execute();

            String otherFeesDetails = paymentEnt.otherFeesDetails != null
                    ? EmojiParser.removeAllEmojis(paymentEnt.otherFeesDetails)
                    : "";
            context.update(QUP_TICKETEXTEND)
                    .set(QUP_TICKETEXTEND.DOUBLE0, paymentEnt.tollFee)
                    .set(QUP_TICKETEXTEND.TEXT0, otherFeesDetails)
                    .set(QUP_TICKETEXTEND.TEXT3, services)
                    .where(QUP_TICKETEXTEND.BOOKID.equal(bookId))
                    .execute();

            context.update(QUP_TICKETEXTRA)
                    .set(QUP_TICKETEXTRA.DOUBLE6, remainingAmount)
                    .set(QUP_TICKETEXTRA.DOUBLE7, paymentEnt.itemValue)
                    .set(QUP_TICKETEXTRA.DOUBLE20, paymentEnt.parkingFee)
                    .set(QUP_TICKETEXTRA.DOUBLE21, paymentEnt.gasFee)
                    .where(QUP_TICKETEXTRA.BOOKID.equal(bookId))
                    .execute();

            return returnCode;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public String getCreditCardsCompany(int i) {
        Connection cnn = null;
        String result = "";
        // try {
        // cnn = this.getConnection();
        // DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings());
        // result = contextRead.selectFrom(TOKENCOMPANY)
        // .limit(KeysUtil.SIZE)
        // .offset(i * KeysUtil.SIZE)
        // .fetch()
        // .formatJSON();
        //
        // context.close();
        // } catch (SQLException e) {
        // e.printStackTrace();
        // } catch (Exception e) {
        // e.printStackTrace();
        // }
        // finally {
        // try {
        // cnn.close();
        // } catch (SQLException e) {
        // e.printStackTrace();
        // }
        // }
        return result;
    }

    public String getCreditCards(int i) {
        Connection cnn = null;
        String result = "";
        // try {
        // cnn = this.getConnection();
        // DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings());
        // result = contextRead.selectFrom(PAYMENTTOKEN)
        // .limit(KeysUtil.SIZE)
        // .offset(i * KeysUtil.SIZE)
        // .fetch()
        // .formatJSON();
        //
        // context.close();
        // } catch (SQLException e) {
        // e.printStackTrace();
        // } catch (Exception e) {
        // e.printStackTrace();
        // }
        // finally {
        // try {
        // cnn.close();
        // } catch (SQLException e) {
        // e.printStackTrace();
        // }
        // }
        return result;
    }

    public String getAllTicket(int i, String fleetId, String from, String to) {
        Connection cnnRead = null;
        String result = "";
        try {
            cnnRead = getReadConnection(fleetId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            if (fleetId.equals("all")) {
                result = contextRead.selectFrom(QUP_TICKET)
                        .where(QUP_TICKET.COMPLETEDTIME.between(Timestamp.valueOf(from), Timestamp.valueOf(to)))
                        .orderBy(QUP_TICKET.ID.desc())
                        .limit(KeysUtil.SIZE)
                        .offset(i * KeysUtil.SIZE)
                        .fetch()
                        .formatJSON();
            } else {
                result = contextRead.selectFrom(QUP_TICKET)
                        .where(QUP_TICKET.FLEETID.eq(fleetId))
                        .and(QUP_TICKET.COMPLETEDTIME.between(Timestamp.valueOf(from), Timestamp.valueOf(to)))
                        .orderBy(QUP_TICKET.ID.desc())
                        .limit(KeysUtil.SIZE)
                        .offset(i * KeysUtil.SIZE)
                        .fetch()
                        .formatJSON();
            }

            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public String getAllTicket(List<String> bookIds) {
        Connection cnnRead = null;
        String result = "";
        try {
            cnnRead = getReadConnection("");
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            result = contextRead.selectFrom(QUP_TICKET)
                    .where(QUP_TICKET.BOOKID.in(bookIds))
                    .fetch()
                    .formatJSON();
            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public double getBalancePrepaid(String prepaidId) {
        Connection cnnRead = null;
        double balance = 0;
        try {
            cnnRead = getReadConnection(prepaidId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            Result<Record> result = contextRead.select().from(QUP_PREPAIDTRANSACTION)
                    .where(QUP_PREPAIDTRANSACTION.PREPAIDID.eq(prepaidId))
                    .fetch();

            for (Record r : result) {
                double amount = r.getValue(QUP_PREPAIDTRANSACTION.AMOUNT);
                if (r.getValue("type").toString().equalsIgnoreCase("deposit")
                        || r.getValue("type").toString().equalsIgnoreCase("editBalance")) {
                    balance += amount;
                } else {
                    balance -= amount;
                }

            }
            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return balance;
    }

    public void removePrepaidTransaction(String bookId) {
        Connection cnn = null;
        try {
            cnn = getConnection(bookId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            context.delete(QUP_PREPAIDTRANSACTION)
                    .where(QUP_PREPAIDTRANSACTION.BOOKID.eq(bookId))
                    .returning()
                    .fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public long addPrepaidTransaction(PrepaidTransaction prepaidTransaction) {
        Connection cnn = null;
        Record record = null;
        try {
            cnn = getConnection(prepaidTransaction.transactionId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            record = context.insertInto(QUP_PREPAIDTRANSACTION,
                    QUP_PREPAIDTRANSACTION.PREPAIDID,
                    QUP_PREPAIDTRANSACTION.TOPUPID,
                    QUP_PREPAIDTRANSACTION.TRANSACTIONID,
                    QUP_PREPAIDTRANSACTION.NAME,
                    QUP_PREPAIDTRANSACTION.COMPANY,
                    QUP_PREPAIDTRANSACTION.PAIDBY,
                    QUP_PREPAIDTRANSACTION.CARDMASK,
                    QUP_PREPAIDTRANSACTION.AMOUNT,
                    QUP_PREPAIDTRANSACTION.TYPE,
                    QUP_PREPAIDTRANSACTION.BOOKID,
                    QUP_PREPAIDTRANSACTION.CREATEDDATE,
                    QUP_PREPAIDTRANSACTION.FLEETID,
                    QUP_PREPAIDTRANSACTION.COMPANYID,
                    QUP_PREPAIDTRANSACTION.CURRENCYISO,
                    QUP_PREPAIDTRANSACTION.VARCHAR0,
                    QUP_PREPAIDTRANSACTION.TEXT0)
                    .values(prepaidTransaction.prepaidId,
                            prepaidTransaction.topUpId,
                            prepaidTransaction.transactionId,
                            prepaidTransaction.name,
                            prepaidTransaction.company,
                            prepaidTransaction.paidBy,
                            prepaidTransaction.cardMask,
                            prepaidTransaction.amount,
                            prepaidTransaction.type,
                            prepaidTransaction.bookId,
                            prepaidTransaction.createdDate,
                            prepaidTransaction.fleetId,
                            prepaidTransaction.companyId,
                            prepaidTransaction.currencyISO,
                            prepaidTransaction.operatorId,
                            prepaidTransaction.reason)
                    .returning()
                    .fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (record != null)
            return record.getValue(QUP_PREPAIDTRANSACTION.ID);
        return 0;
    }

    public String getAllPromoCodeUse(int i, String fleetId, String from, String to) {
        Connection cnnRead = null;
        String result = "";
        try {
            cnnRead = getReadConnection(fleetId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            if (fleetId.equals("all")) {
                result = contextRead.selectFrom(QUP_PROMOCODEUSE)
                        .where(QUP_PROMOCODEUSE.USEDDATE.between(Timestamp.valueOf(from), Timestamp.valueOf(to)))
                        .orderBy(QUP_PROMOCODEUSE.ID.desc())
                        .limit(KeysUtil.SIZE)
                        .offset(i * KeysUtil.SIZE)
                        .fetch()
                        .formatJSON();
            } else {
                result = contextRead.selectFrom(QUP_PROMOCODEUSE)
                        .where(QUP_PROMOCODEUSE.FLEETID.eq(fleetId))
                        .and(QUP_PROMOCODEUSE.USEDDATE.between(Timestamp.valueOf(from), Timestamp.valueOf(to)))
                        .orderBy(QUP_PROMOCODEUSE.ID.desc())
                        .limit(KeysUtil.SIZE)
                        .offset(i * KeysUtil.SIZE)
                        .fetch()
                        .formatJSON();
            }

            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public void deletePromocodeuse(String promoCode, String userId, String fleetId, int available) {
        Connection cnn = null;
        Connection cnnRead = null;
        try {
            cnn = getConnection(promoCode);
            cnnRead = getReadConnection(promoCode);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            Record record = contextRead.select(QUP_PROMOCODEUSE.ID).from(QUP_PROMOCODEUSE)
                    .where(QUP_PROMOCODEUSE.PROMOCODE.eq(promoCode.toUpperCase()))
                    .and(QUP_PROMOCODEUSE.CUSTOMERID.eq(userId))
                    .and(QUP_PROMOCODEUSE.FLEETID.eq(fleetId))
                    .and(QUP_PROMOCODEUSE.AVAILABLE.eq(available))
                    .orderBy(QUP_PROMOCODEUSE.ID)
                    .limit(1)
                    .fetchOne();
            long id = record.getValue(QUP_PROMOCODEUSE.ID);

            context.delete(QUP_PROMOCODEUSE)
                    .where(QUP_PROMOCODEUSE.ID.eq(id))
                    .returning()
                    .fetchOne();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public String getAllPrepaidTransaction(int i, String fleetId, String from, String to) {
        Connection cnnRead = null;
        String result = "";
        try {
            cnnRead = getReadConnection(fleetId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            if (fleetId.equals("all")) {
                result = contextRead.selectFrom(QUP_PREPAIDTRANSACTION)
                        .where(QUP_PREPAIDTRANSACTION.CREATEDDATE.between(Timestamp.valueOf(from),
                                Timestamp.valueOf(to)))
                        .orderBy(QUP_PREPAIDTRANSACTION.ID.desc())
                        .limit(KeysUtil.SIZE)
                        .offset(i * KeysUtil.SIZE)
                        .fetch()
                        .formatJSON();
            } else {
                result = contextRead.selectFrom(QUP_PREPAIDTRANSACTION)
                        .where(QUP_PREPAIDTRANSACTION.FLEETID.eq(fleetId))
                        .and(QUP_PREPAIDTRANSACTION.CREATEDDATE.between(Timestamp.valueOf(from), Timestamp.valueOf(to)))
                        .orderBy(QUP_PREPAIDTRANSACTION.ID.desc())
                        .limit(KeysUtil.SIZE)
                        .offset(i * KeysUtil.SIZE)
                        .fetch()
                        .formatJSON();
            }

            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public QupTicketextendRecord getTicketExtend(String bookId) {
        Connection cnnRead = null;
        QupTicketextendRecord qupTicketextendRecord = null;
        try {
            cnnRead = getReadConnection(bookId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            qupTicketextendRecord = contextRead.selectFrom(QUP_TICKETEXTEND)
                    .where(QUP_TICKETEXTEND.BOOKID.eq(bookId))
                    .fetchOne();

            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return qupTicketextendRecord;
    }

    public QupTicketextendRecord getTicketExtend(String requestId, String bookId) {
        Connection cnnRead = null;
        QupTicketextendRecord qupTicketextendRecord = null;
        try {
            cnnRead = getReadConnection(requestId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            qupTicketextendRecord = contextRead.selectFrom(QUP_TICKETEXTEND)
                    .where(QUP_TICKETEXTEND.BOOKID.eq(bookId))
                    .fetchOne();

            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return qupTicketextendRecord;
    }

    public QupTicketextraRecord getTicketExtra(String bookId) {
        Connection cnnRead = null;
        QupTicketextraRecord qupTicketextraRecord = null;
        try {
            cnnRead = getReadConnection(bookId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            qupTicketextraRecord = contextRead.selectFrom(QUP_TICKETEXTRA)
                    .where(QUP_TICKETEXTRA.BOOKID.eq(bookId))
                    .fetchOne();

            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return qupTicketextraRecord;
    }

    public QupTicketextraRecord getTicketExtra(String requestId, String bookId) {
        Connection cnnRead = null;
        QupTicketextraRecord qupTicketextraRecord = null;
        try {
            cnnRead = getReadConnection(requestId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            qupTicketextraRecord = contextRead.selectFrom(QUP_TICKETEXTRA)
                    .where(QUP_TICKETEXTRA.BOOKID.eq(bookId))
                    .fetchOne();

            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return qupTicketextraRecord;
    }

    public String updateTicketAfterPayment(TicketEnt ticketEnt, Map<String, String> mapTicket, JSONObject mapCorporate,
            JSONObject jsonSetting, JSONObject jsonSharing, JSONObject itinerary, JSONObject OTWDistance,
            JSONObject jsonReferral,
            JSONObject jsonRequestInfo, JSONObject jsonDriver, JSONObject jsonBooking) {
        String requestId = mapTicket.get("requestId") != null ? mapTicket.get("requestId") : ticketEnt.bookId;
        String otwDistance = "";
        if (OTWDistance.get("originDestination") != null) {
            otwDistance = OTWDistance.get("originDestination").toString();
            OTWDistance.remove("originDestination");
        }
        JSONObject objectUpdateMySql = new JSONObject();
        String carTypeService = "rideHailing";
        if (jsonBooking.get("jobType") != null)
            carTypeService = jsonBooking.get("jobType").toString();
        boolean serviceActive = jsonBooking.get("serviceActive") != null
                && Boolean.parseBoolean(jsonBooking.get("serviceActive").toString());
        objectUpdateMySql.put("carTypeService", carTypeService);
        objectUpdateMySql.put("vehicleImageRequest",
                jsonBooking.get("vehicleImageRequest") != null ? jsonBooking.get("vehicleImageRequest").toString()
                        : "");
        objectUpdateMySql.put("packageRateId",
                jsonBooking.get("packageRateId") != null ? jsonBooking.get("packageRateId").toString() : "");
        objectUpdateMySql.put("packageRateName",
                jsonBooking.get("packageRateName") != null ? jsonBooking.get("packageRateName").toString() : "");
        objectUpdateMySql.put("flatRouteId",
                jsonBooking.get("flatRouteId") != null ? jsonBooking.get("flatRouteId").toString() : "");
        objectUpdateMySql.put("flatRouteName",
                jsonBooking.get("flatRouteName") != null ? jsonBooking.get("flatRouteName").toString() : "");
        objectUpdateMySql.put("customerGender",
                jsonBooking.get("customerGender") != null ? jsonBooking.get("customerGender") : -1);
        objectUpdateMySql.put("customerBirthday",
                jsonBooking.get("customerBirthday") != null ? jsonBooking.get("customerBirthday").toString() : "");
        objectUpdateMySql.put("pickupAddressDetails",
                jsonBooking.get("pickupAddressDetails") != null
                        ? gson.fromJson(jsonBooking.get("pickupAddressDetails").toString(), JSONObject.class)
                        : new JSONObject());
        objectUpdateMySql.put("destinationAddressDetails",
                jsonBooking.get("destinationAddressDetails") != null
                        ? gson.fromJson(jsonBooking.get("destinationAddressDetails").toString(), JSONObject.class)
                        : new JSONObject());
        objectUpdateMySql.put("bookingNotes",
                jsonBooking.get("bookingNotes") != null ? jsonBooking.get("bookingNotes").toString() : "");
        objectUpdateMySql.put("isAirline", jsonBooking.get("isAirline") != null ? jsonBooking.get("isAirline") : false);
        objectUpdateMySql.put("flightNumber",
                jsonBooking.get("flightNumber") != null ? jsonBooking.get("flightNumber") : "");
        objectUpdateMySql.put("pointsEarned",
                jsonBooking.get("pointsEarned") != null ? jsonBooking.get("pointsEarned") : 0);
        objectUpdateMySql.put("isFareEdited",
                jsonBooking.get("isFareEdited") != null ? jsonBooking.get("isFareEdited") : false);
        objectUpdateMySql.put("reasonEditFare",
                jsonBooking.get("reasonEditFare") != null ? jsonBooking.get("reasonEditFare") : "");
        objectUpdateMySql.put("markupPrice",
                jsonBooking.get("markupPrice") != null ? jsonBooking.get("markupPrice") : 0.0);
        objectUpdateMySql.put("markupDifference",
                jsonBooking.get("markupDifference") != null ? jsonBooking.get("markupDifference") : 0.0);
        objectUpdateMySql.put("reasonMarkup",
                jsonBooking.get("reasonMarkup") != null ? jsonBooking.get("reasonMarkup") : "");
        objectUpdateMySql.put("gateway",
                jsonBooking.get("gateway") != null ? jsonBooking.get("gateway").toString() : "");
        objectUpdateMySql.put("walletName",
                jsonBooking.get("walletName") != null ? jsonBooking.get("walletName").toString() : "");
        objectUpdateMySql.put("iconUrl",
                jsonBooking.get("iconUrl") != null ? jsonBooking.get("iconUrl").toString() : "");
        objectUpdateMySql.put("dynamicType",
                jsonBooking.get("dynamicType") != null ? jsonBooking.get("dynamicType").toString() : "");
        objectUpdateMySql.put("operatorNote",
                jsonBooking.get("operatorNote") != null ? jsonBooking.get("operatorNote").toString() : "");
        objectUpdateMySql.put("paxNumber", jsonBooking.get("paxNumber") != null ? jsonBooking.get("paxNumber") : 1);
        objectUpdateMySql.put("luggageNumber",
                jsonBooking.get("luggageNumber") != null ? jsonBooking.get("luggageNumber") : 0);
        objectUpdateMySql.put("serviceActive", serviceActive);
        JSONObject deliveryInfo = jsonBooking.get("deliveryInfo") != null
                ? gson.fromJson(jsonBooking.get("deliveryInfo").toString(), JSONObject.class)
                : new JSONObject();
        String reason = ticketEnt.reason != null ? ticketEnt.reason : "";
        JSONArray jsonArray = new JSONArray();
        for (FleetService fleetService : ticketEnt.fleetServices) {
            jsonArray.add(gson.toJson(fleetService));
        }
        int paidToDriver = Boolean.valueOf(mapTicket.get("paidToDriver")) ? 1 : 0;
        if (paidToDriver == 0 && Boolean.valueOf(mapTicket.get("driverSettlement"))) {
            paidToDriver = -1;
        }

        Connection cnn = null;
        try {
            cnn = getConnection(ticketEnt.bookId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            try {
                context.update(QUP_TICKET)
                        .set(QUP_TICKET.TRANSACTIONSTATUS, ticketEnt.transactionStatus)
                        .set(QUP_TICKET.REASON,
                                CommonUtils.removeSpecialCharacter(reason).trim().isEmpty() ? ""
                                        : CommonUtils.removeSpecialCharacter(reason).trim())
                        .set(QUP_TICKET.MDISPATCHERCOMMISSION,
                                Double.parseDouble(mapTicket.get("mDispatcherCommission")))
                        .set(QUP_TICKET.PROMOAMOUNT, Double.parseDouble(mapTicket.get("promoAmount")))
                        .set(QUP_TICKET.NETEARNING, Double.parseDouble(mapTicket.get("netEarning")))
                        .set(QUP_TICKET.TOTALFARE, Double.parseDouble(mapTicket.get("totalFare")))
                        .set(QUP_TICKET.COMM, Double.parseDouble(mapTicket.get("comm")))
                        .set(QUP_TICKET.DEDUCTIONS, Double.parseDouble(mapTicket.get("deductions")))
                        .set(QUP_TICKET.RIDEPAYMENT, Double.parseDouble(mapTicket.get("ridePayment")))
                        .set(QUP_TICKET.GROSSEARNING, Double.parseDouble(mapTicket.get("grossEarning")))
                        .set(QUP_TICKET.TRANSACTIONFEE, Double.parseDouble(mapTicket.get("transactionFee")))
                        .set(QUP_TICKET.FLEETNAME, mapTicket.get("fleetName"))
                        .set(QUP_TICKET.PSGFLEETNAME, mapTicket.get("psgFleetName"))
                        .set(QUP_TICKET.OWNERFLEETID, mapTicket.get("ownerId"))
                        .set(QUP_TICKET.OWNERFLEETNAME, mapTicket.get("ownerName"))
                        .set(QUP_TICKET.PSGFLEETCOMMAMOUNT, Double.parseDouble(mapTicket.get("netEarningProvider")))
                        .set(QUP_TICKET.PSGFLEETCOMMPCT, Double.parseDouble(mapTicket.get("commProvider")))
                        .set(QUP_TICKET.PSGFLEETCOMMTOTAL, Double.parseDouble(mapTicket.get("deductionsProvider")))
                        .set(QUP_TICKET.OWNERFLEETCOMMAMOUNT, Double.parseDouble(mapTicket.get("ridePaymentProvider")))
                        .set(QUP_TICKET.OWNERFLEETCOMMPCT, Double.parseDouble(mapTicket.get("grossEarningProvider")))
                        .set(QUP_TICKET.OWNERFLEETCOMMTOTAL,
                                Double.parseDouble(mapTicket.get("transactionFeeProvider")))
                        .set(QUP_TICKET.EXCHANGERATE, Double.parseDouble(mapTicket.get("exchangeRate")))
                        .set(QUP_TICKET.CLIENTCASEMATTER,
                                jsonBooking.get("clientCaseMatter") != null
                                        ? jsonBooking.get("clientCaseMatter").toString()
                                        : "")
                        .set(QUP_TICKET.CHARGECODE,
                                jsonBooking.get("chargeCode") != null ? jsonBooking.get("chargeCode").toString() : "")
                        .set(QUP_TICKET.CARDTYPE, ticketEnt.cardType)
                        .set(QUP_TICKET.PAIDTODRIVER, paidToDriver)
                        .where(QUP_TICKET.BOOKID.eq(ticketEnt.bookId))
                        .returning()
                        .fetchOne();
            } catch (Exception ex) {
                logger.debug(requestId + " - save QUP_Ticket error: " + CommonUtils.getError(ex));
            }
            try {
                String payoutType = paidToDriver == 1 ? "auto" : "manual";
                context.update(QUP_TICKETEXTEND)
                        .set(QUP_TICKETEXTEND.VARCHAR2, mapTicket.get("commissionType"))
                        .set(QUP_TICKETEXTEND.VARCHAR3, mapTicket.get("mDispatcherCommissionType"))
                        .set(QUP_TICKETEXTEND.VARCHAR4, jsonReferral.toString())
                        .set(QUP_TICKETEXTEND.VARCHAR5,
                                jsonRequestInfo.get("cardMask") != null ? jsonRequestInfo.get("cardMask").toString()
                                        : "")
                        .set(QUP_TICKETEXTEND.VARCHAR8, jsonDriver.toString())
                        .set(QUP_TICKETEXTEND.VARCHAR11, payoutType)
                        .set(QUP_TICKETEXTEND.DOUBLE2, ticketEnt.fareProvider)
                        .set(QUP_TICKETEXTEND.DOUBLE3, ticketEnt.tipProvider)
                        .set(QUP_TICKETEXTEND.DOUBLE4, ticketEnt.taxValueProvider)
                        .set(QUP_TICKETEXTEND.DOUBLE5, ticketEnt.subTotalProvider)
                        .set(QUP_TICKETEXTEND.DOUBLE6, ticketEnt.totalProvider)
                        .set(QUP_TICKETEXTEND.DOUBLE7, ticketEnt.qupCommission)
                        .set(QUP_TICKETEXTEND.DOUBLE8, Double.parseDouble(mapTicket.get("driverDeduction")))
                        .set(QUP_TICKETEXTEND.DOUBLE9, ticketEnt.minimumProvider)
                        .set(QUP_TICKETEXTEND.DOUBLE10, Double.parseDouble(mapTicket.get("fleetCommission")))
                        .set(QUP_TICKETEXTEND.DOUBLE11, Double.parseDouble(mapTicket.get("netProfit")))
                        .set(QUP_TICKETEXTEND.INT0, Integer.parseInt(mapTicket.get("bookType")))
                        .set(QUP_TICKETEXTEND.TEXT1, jsonSetting.toJSONString())
                        .set(QUP_TICKETEXTEND.TEXT2, jsonSharing.toJSONString())
                        .set(QUP_TICKETEXTEND.TEXT3, ticketEnt.services)
                        .set(QUP_TICKETEXTEND.TEXT4, ticketEnt.extraDestination)
                        .set(QUP_TICKETEXTEND.TEXT5, mapTicket.get("drvUsername"))
                        .set(QUP_TICKETEXTEND.TEXT6, mapCorporate.toJSONString())
                        .set(QUP_TICKETEXTEND.TEXT8, itinerary.toJSONString())
                        .set(QUP_TICKETEXTEND.TEXT9, OTWDistance.toJSONString())
                        .set(QUP_TICKETEXTEND.TEXT11, String.valueOf(ticketEnt.ridePayout))
                        .set(QUP_TICKETEXTEND.INT1, ticketEnt.reasonCode)
                        .set(QUP_TICKETEXTEND.INT2,
                                Boolean.valueOf(
                                        mapTicket.get("companyCommission") != null ? mapTicket.get("companyCommission")
                                                : "false") ? 1 : 0)
                        .set(QUP_TICKETEXTEND.BOOLEAN3, Boolean.valueOf(mapTicket.get("shortTrip")) ? 1 : 0)
                        .set(QUP_TICKETEXTEND.VARCHAR10, ticketEnt.walletName)
                        .set(QUP_TICKETEXTEND.BOOLEAN6, mapTicket.get("isMinimumTotal").equals("true") ? 1 : 0)
                        .where(QUP_TICKETEXTEND.BOOKID.eq(ticketEnt.bookId))
                        .returning()
                        .fetchOne();
            } catch (Exception ex) {
                logger.debug(requestId + " - save QUp_TicketExtend error: " + CommonUtils.getError(ex));
            }
            String adjustPrice = mapTicket.get("adjustPrice") != null ? mapTicket.get("adjustPrice") : "";
            JSONObject jsonServiceFee = new JSONObject();
            jsonServiceFee.put("fleetServiceFee",
                    mapTicket.get("fleetServiceFee") != null ? Double.parseDouble(mapTicket.get("fleetServiceFee"))
                            : 0.0);
            jsonServiceFee.put("fleetCommissionFromServiceFee",
                    mapTicket.get("fleetCommissionFromFleetServiceFee") != null
                            ? Double.parseDouble(mapTicket.get("fleetCommissionFromFleetServiceFee"))
                            : 0.0);
            jsonServiceFee.put("driverCommissionFromServiceFee",
                    mapTicket.get("driverCommissionFromFleetServiceFee") != null
                            ? Double.parseDouble(mapTicket.get("driverCommissionFromFleetServiceFee"))
                            : 0.0);

            JSONObject jsonHydra = new JSONObject();
            jsonHydra.put("qupPreferredAmount",
                    mapTicket.get("qupPreferredAmount") != null
                            ? Double.parseDouble(mapTicket.get("qupPreferredAmount"))
                            : 0.0);
            jsonHydra.put("qupBuyPrice",
                    mapTicket.get("qupBuyPrice") != null ? Double.parseDouble(mapTicket.get("qupBuyPrice")) : 0.0);
            jsonHydra.put("qupSellPrice",
                    mapTicket.get("qupSellPrice") != null ? Double.parseDouble(mapTicket.get("qupSellPrice")) : 0.0);
            jsonHydra.put("supplierPayout",
                    mapTicket.get("supplierPayout") != null ? Double.parseDouble(mapTicket.get("supplierPayout"))
                            : 0.0);
            jsonHydra.put("buyerPayout",
                    mapTicket.get("buyerPayout") != null ? Double.parseDouble(mapTicket.get("buyerPayout")) : 0.0);
            jsonHydra.put("qupEarning",
                    mapTicket.get("qupEarning") != null ? Double.parseDouble(mapTicket.get("qupEarning")) : 0.0);
            jsonHydra.put("supplierCommission",
                    mapTicket.get("buyerCommission") != null ? Double.parseDouble(mapTicket.get("supplierCommission"))
                            : 0.0);
            jsonHydra.put("buyerCommission",
                    mapTicket.get("supplierCommission") != null ? Double.parseDouble(mapTicket.get("buyerCommission"))
                            : 0.0);
            jsonHydra.put("profitFromSupplier",
                    mapTicket.get("profitFromSupplier") != null
                            ? Double.parseDouble(mapTicket.get("profitFromSupplier"))
                            : 0.0);
            jsonHydra.put("profitFromBuyer",
                    mapTicket.get("profitFromBuyer") != null ? Double.parseDouble(mapTicket.get("profitFromBuyer"))
                            : 0.0);
            jsonHydra.put("penaltyPolicy",
                    mapTicket.get("penaltyPolicy") != null ? Double.parseDouble(mapTicket.get("penaltyPolicy")) : 0.0);
            jsonHydra.put("penaltyAmount",
                    mapTicket.get("penaltyAmount") != null ? Double.parseDouble(mapTicket.get("penaltyAmount")) : 0.0);
            jsonHydra.put("compensationPolicy",
                    mapTicket.get("compensationPolicy") != null
                            ? Double.parseDouble(mapTicket.get("compensationPolicy"))
                            : 0.0);
            jsonHydra.put("compensationAmount",
                    mapTicket.get("compensationAmount") != null
                            ? Double.parseDouble(mapTicket.get("compensationAmount"))
                            : 0.0);
            jsonHydra.put("fleetMarkup",
                    mapTicket.get("fleetMarkup") != null ? Double.parseDouble(mapTicket.get("fleetMarkup")) : 0.0);
            jsonHydra.put("hydraPaymentMethod",
                    mapTicket.get("hydraPaymentMethod") != null ? mapTicket.get("hydraPaymentMethod") : "");
            jsonHydra.put("isFarmOut",
                    mapTicket.get("isFarmOut") != null && Boolean.parseBoolean(mapTicket.get("isFarmOut")));
            jsonHydra.put("sellPriceMarkup",
                    mapTicket.get("sellPriceMarkup") != null ? Double.parseDouble(mapTicket.get("sellPriceMarkup"))
                            : 0.0);

            JSONObject jsonTransfer = new JSONObject();
            jsonTransfer.put("fleetAmount",
                    mapTicket.get("fleetAmount") != null ? Double.parseDouble(mapTicket.get("fleetAmount")) : 0.0);
            jsonTransfer.put("driverAmount",
                    mapTicket.get("driverAmount") != null ? Double.parseDouble(mapTicket.get("driverAmount")) : 0.0);
            jsonTransfer.put("companyAmount",
                    mapTicket.get("companyAmount") != null ? Double.parseDouble(mapTicket.get("companyAmount")) : 0.0);
            jsonTransfer.put("stripeFee",
                    mapTicket.get("stripeFee") != null ? Double.parseDouble(mapTicket.get("stripeFee")) : 0.0);
            jsonTransfer.put("pendingSettlement",
                    mapTicket.get("pendingSettlement") != null ? Double.parseDouble(mapTicket.get("pendingSettlement"))
                            : 0.0);
            try {
                context.update(QUP_TICKETEXTRA)
                        .set(QUP_TICKETEXTRA.DOUBLE0, Double.valueOf(jsonReferral.get("referralEarning").toString()))
                        .set(QUP_TICKETEXTRA.DOUBLE8, Double.valueOf(mapTicket.get("merchantCommission")))
                        .set(QUP_TICKETEXTRA.DOUBLE11, Double.valueOf(mapTicket.get("driverTax")))
                        .set(QUP_TICKETEXTRA.TEXT0, objectUpdateMySql.toString())
                        .set(QUP_TICKETEXTRA.TEXT1, jsonRequestInfo.toString())
                        .set(QUP_TICKETEXTRA.TEXT3,
                                deliveryInfo != null ? CommonUtils.removeSpecialCharacter(deliveryInfo.toString()) : "")
                        .set(QUP_TICKETEXTRA.TEXT4, adjustPrice)
                        .set(QUP_TICKETEXTRA.TEXT5, jsonArray.toJSONString())
                        .set(QUP_TICKETEXTRA.TEXT6, jsonServiceFee.toJSONString())
                        .set(QUP_TICKETEXTRA.TEXT8, jsonHydra.toJSONString())
                        .set(QUP_TICKETEXTRA.TEXT9, jsonTransfer.toJSONString())
                        .set(QUP_TICKETEXTRA.INT0, mapTicket.get("payoutToProviderFleet").equals("true") ? 1 : 0)
                        .set(QUP_TICKETEXTRA.INT1, mapTicket.get("payoutToHomeFleet").equals("true") ? 1 : 0)
                        .set(QUP_TICKETEXTRA.DOUBLE12,
                                mapTicket.get("convertSupplierPayout") != null
                                        ? Double.parseDouble(mapTicket.get("convertSupplierPayout"))
                                        : 0.0)
                        .set(QUP_TICKETEXTRA.DOUBLE13,
                                mapTicket.get("convertBuyerPayout") != null
                                        ? Double.parseDouble(mapTicket.get("convertBuyerPayout"))
                                        : 0.0)
                        .set(QUP_TICKETEXTRA.DOUBLE14,
                                mapTicket.get("convertSupplierPenalty") != null
                                        ? Double.parseDouble(mapTicket.get("convertSupplierPenalty")) * (-1)
                                        : 0.0)
                        .set(QUP_TICKETEXTRA.VARCHAR0, mapTicket.get("supplierFailedId"))
                        .set(QUP_TICKETEXTRA.VARCHAR1, mapTicket.get("buyerFailedId"))
                        .set(QUP_TICKETEXTRA.INT2, mapTicket.get("paidToCompany").equals("true") ? 1 : 0)
                        .set(QUP_TICKETEXTRA.DOUBLE16,
                                mapTicket.get("companyTax") != null ? Double.parseDouble(mapTicket.get("companyTax"))
                                        : 0.0)
                        .set(QUP_TICKETEXTRA.VARCHAR4,
                                mapTicket.get("driverEarningType") != null ? mapTicket.get("driverEarningType") : "")
                        .set(QUP_TICKETEXTRA.DOUBLE17,
                                mapTicket.get("editedDriverEarning") != null
                                        ? Double.parseDouble(mapTicket.get("editedDriverEarning"))
                                        : 0.0)
                        .set(QUP_TICKETEXTRA.TEXT10,
                                mapTicket.get("externalInfo") != null ? mapTicket.get("externalInfo") : "")
                        .where(QUP_TICKETEXTRA.BOOKID.eq(ticketEnt.bookId))
                        .returning()
                        .fetchOne();
            } catch (Exception ex) {
                logger.debug(requestId + " - save QUp_TicketExtra error: " + CommonUtils.getError(ex));
            }

            // add data for pending payout
            if (ticketEnt.pricingType == 1) {
                String networkType = mapTicket.get("networkType") != null ? mapTicket.get("networkType") : ""; // 'roaming',
                                                                                                               // 'farmOut'
                double exchangeRate = Double.parseDouble(mapTicket.get("exchangeRate"));
                // add penalty data if not empty
                double convertSupplierPenalty = mapTicket.get("convertSupplierPenalty") != null
                        ? Double.parseDouble(mapTicket.get("convertSupplierPenalty")) * (-1)
                        : 0.0;
                if (convertSupplierPenalty < 0) {
                    if (networkType.equals("farmOut")) {
                        // charge penalty for home fleet
                        HydraPending hydraPendingProvider = new HydraPending();
                        hydraPendingProvider.psgFleetId = ticketEnt.psgFleetId;
                        hydraPendingProvider.bookId = ticketEnt.bookId;
                        hydraPendingProvider.completedTime = ticketEnt.completedTime;
                        hydraPendingProvider.payoutToHomeFleet = mapTicket.get("payoutToHomeFleet").equals("true") ? 1
                                : 0;
                        hydraPendingProvider.convertBuyerPayout = convertSupplierPenalty;
                        hydraPendingProvider.exchangeRate = exchangeRate;
                        hydraPendingProvider.networkType = networkType;
                        hydraPendingProvider.paymentMethod = "";
                        long addPending = addHydraPending(hydraPendingProvider);
                        logger.debug(requestId + " - save HydraPending buyer penalty for fleetId "
                                + ticketEnt.psgFleetId + " - booking " + ticketEnt.bookId + ": " + addPending);
                    } else {
                        // charge penalty for provider fleet
                        HydraPending hydraPendingProvider = new HydraPending();
                        hydraPendingProvider.fleetId = ticketEnt.fleetId;
                        hydraPendingProvider.bookId = ticketEnt.bookId;
                        hydraPendingProvider.completedTime = ticketEnt.completedTime;
                        hydraPendingProvider.payoutToProviderFleet = mapTicket.get("payoutToProviderFleet")
                                .equals("true") ? 1 : 0;
                        hydraPendingProvider.convertSupplierPenalty = convertSupplierPenalty;
                        hydraPendingProvider.exchangeRate = exchangeRate;
                        hydraPendingProvider.networkType = networkType;
                        hydraPendingProvider.paymentMethod = "";
                        long addPending = addHydraPending(hydraPendingProvider);
                        logger.debug(requestId + " - save HydraPending supplier penalty for fleetId "
                                + ticketEnt.fleetId + " - booking " + ticketEnt.bookId + ": " + addPending);
                    }
                }

                // add transaction for home fleet
                double convertBuyerPayout = mapTicket.get("convertBuyerPayout") != null
                        ? Double.parseDouble(mapTicket.get("convertBuyerPayout"))
                        : 0.0;
                if (convertBuyerPayout > 0) {
                    // check if need to charge home fleet for farm-out booking
                    if (networkType.equals("farmOut")) {
                        double qupSellPrice = mapTicket.get("qupSellPrice") != null
                                ? Double.parseDouble(mapTicket.get("qupSellPrice"))
                                : 0.0;
                        convertBuyerPayout = convertBuyerPayout - qupSellPrice;
                    }

                    HydraPending hydraPendingHome = new HydraPending();
                    hydraPendingHome.psgFleetId = ticketEnt.psgFleetId;
                    hydraPendingHome.bookId = ticketEnt.bookId;
                    hydraPendingHome.completedTime = ticketEnt.completedTime;
                    hydraPendingHome.payoutToHomeFleet = mapTicket.get("payoutToHomeFleet").equals("true") ? 1 : 0;
                    hydraPendingHome.convertBuyerPayout = convertBuyerPayout;
                    hydraPendingHome.exchangeRate = exchangeRate;
                    hydraPendingHome.networkType = networkType;
                    hydraPendingHome.paymentMethod = mapTicket.get("hydraPaymentMethod") != null
                            ? mapTicket.get("hydraPaymentMethod")
                            : "";
                    long addPendingHome = addHydraPending(hydraPendingHome);
                    logger.debug(requestId + " - save HydraPending home payout for fleetId " + ticketEnt.psgFleetId
                            + " - booking " + ticketEnt.bookId + ": " + addPendingHome);
                }

                // add transaction for provider fleet
                double convertSupplierPayout = mapTicket.get("convertSupplierPayout") != null
                        ? Double.parseDouble(mapTicket.get("convertSupplierPayout"))
                        : 0.0;
                if (convertSupplierPayout > 0) {
                    HydraPending hydraPendingProvider = new HydraPending();
                    hydraPendingProvider.fleetId = ticketEnt.fleetId;
                    hydraPendingProvider.bookId = ticketEnt.bookId;
                    hydraPendingProvider.completedTime = ticketEnt.completedTime;
                    hydraPendingProvider.payoutToProviderFleet = mapTicket.get("payoutToProviderFleet").equals("true")
                            ? 1
                            : 0;
                    hydraPendingProvider.convertSupplierPayout = convertSupplierPayout;
                    hydraPendingProvider.exchangeRate = exchangeRate;
                    hydraPendingProvider.networkType = "farmIn";
                    hydraPendingProvider.paymentMethod = mapTicket.get("hydraPaymentMethod") != null
                            ? mapTicket.get("hydraPaymentMethod")
                            : "";
                    long addPendingProvider = addHydraPending(hydraPendingProvider);
                    logger.debug(requestId + " - save HydraPending supplier payout for provider fleetId "
                            + ticketEnt.fleetId + " - booking " + ticketEnt.bookId + ": " + addPendingProvider);
                }
            }

            String ticket = context.selectFrom(QUP_TICKET)
                    .where(QUP_TICKET.BOOKID.eq(ticketEnt.bookId))
                    .orderBy(QUP_TICKET.ID.desc())
                    .fetch()
                    .formatJSON();

            QupTicketextendRecord qupTicketextendRecord = context.selectFrom(QUP_TICKETEXTEND)
                    .where(QUP_TICKETEXTEND.BOOKID.eq(ticketEnt.bookId))
                    .fetchOne();
            QupTicketextraRecord qupTicketextraRecord = context.selectFrom(QUP_TICKETEXTRA)
                    .where(QUP_TICKETEXTRA.BOOKID.eq(ticketEnt.bookId))
                    .fetchOne();

            if (qupTicketextraRecord != null) {
                ResultSet rsExtra = qupTicketextraRecord.intoResultSet();
                double pendingAmount = 0.0;
                String pendingStatus = "";
                while (rsExtra.next()) {
                    pendingAmount = rsExtra.getDouble("double15");
                    pendingStatus = rsExtra.getString("varchar2") != null ? rsExtra.getString("varchar2") : "";
                }
                if (pendingAmount > 0) {
                    HydraPending hydraPending = new HydraPending();
                    hydraPending.fleetId = ticketEnt.fleetId;
                    hydraPending.psgFleetId = ticketEnt.psgFleetId;
                    hydraPending.bookId = ticketEnt.bookId;
                    hydraPending.completedTime = ticketEnt.completedTime;
                    hydraPending.userId = ticketEnt.customerId;
                    hydraPending.hydraOutstandingAmount = pendingAmount;
                    hydraPending.hydraPaymentStatus = pendingStatus;
                    hydraPending.exchangeRate = Double.parseDouble(mapTicket.get("exchangeRate"));
                    long addPending = addHydraPending(hydraPending);
                    logger.debug(requestId + " - save HydraPending for hydra pending payment " + ticketEnt.bookId + ": "
                            + addPending);
                }
            }

            if (ticket != null) {
                JSONParser parser = new JSONParser();
                JSONObject jsonObject = (JSONObject) parser.parse(ticket);
                JSONArray fields = (JSONArray) jsonObject.get("fields");
                List<String> listKeys = new ArrayList<>();
                int indexOfComment = 0;
                int indexOfDestination = 0;
                int indexOfPickup = 0;
                int indexOfCustomerName = 0;
                int indexOfDriverName = 0;
                int indexOfUserName = 0;
                int indexOfTotal = 0;
                int indexOfTotalCharged = 0;
                if (listKeys.size() == 0) {
                    int index = 0;
                    for (Object field : fields) {
                        JSONObject object = (JSONObject) field;
                        String key = object.get("name") != null ? object.get("name").toString() : "";
                        listKeys.add(key);
                        if (key.equalsIgnoreCase("receiptComment")) {
                            indexOfComment = index;
                        }
                        if (key.equalsIgnoreCase("pickup")) {
                            indexOfPickup = index;
                        }
                        if (key.equalsIgnoreCase("destination")) {
                            indexOfDestination = index;
                        }
                        if (key.equalsIgnoreCase("customerName")) {
                            indexOfCustomerName = index;
                        }
                        if (key.equalsIgnoreCase("driverName")) {
                            indexOfDriverName = index;
                        }
                        if (key.equalsIgnoreCase("userName")) {
                            indexOfUserName = index;
                        }
                        if (key.equalsIgnoreCase("total")) {
                            indexOfTotal = index;
                        }
                        if (key.equalsIgnoreCase("totalCharged")) {
                            indexOfTotalCharged = index;
                        }
                        index++;
                    }
                }
                JSONArray records = (JSONArray) jsonObject.get("records");
                Gson gson = new Gson();
                double total = 0.0;
                double totalCharged = 0.0;
                if (records.size() > 0) {
                    JSONArray object = (JSONArray) records.get(0);
                    Map<String, Object> mapData = new HashMap<>();
                    for (int k = 0; k < listKeys.size(); k++) {
                        if (k == indexOfComment) {
                            mapData.put(listKeys.get(k),
                                    jsonBooking.get("receiptComment") != null ? jsonBooking.get("receiptComment") : "");
                        } else if (k == indexOfPickup) {
                            mapData.put(listKeys.get(k),
                                    jsonBooking.get("pickup") != null ? jsonBooking.get("pickup") : "");
                        } else if (k == indexOfDestination) {
                            mapData.put(listKeys.get(k),
                                    jsonBooking.get("destination") != null ? jsonBooking.get("destination") : "");
                        } else if (k == indexOfCustomerName) {
                            mapData.put(listKeys.get(k),
                                    jsonBooking.get("customerName") != null ? jsonBooking.get("customerName") : "");
                        } else if (k == indexOfDriverName) {
                            mapData.put(listKeys.get(k),
                                    jsonBooking.get("driverName") != null ? jsonBooking.get("driverName") : "");
                        } else if (k == indexOfUserName) {
                            String userName = object.get(k) != null
                                    ? object.get(k).toString().replace("\"", "").replace("(", "").replace(")", "")
                                    : "";
                            mapData.put(listKeys.get(k), userName);
                        } else if (k == indexOfTotal) {
                            total = Double.parseDouble(object.get(k).toString());
                            mapData.put(listKeys.get(k), object.get(k));
                        } else if (k == indexOfTotalCharged) {
                            totalCharged = Double.parseDouble(object.get(k).toString());
                            mapData.put(listKeys.get(k), object.get(k));
                        } else {
                            mapData.put(listKeys.get(k), object.get(k));
                        }
                    }
                    // calculate subTotal after promotion
                    DecimalFormat DF = new DecimalFormat("#.##");
                    double subTotal = DF.parse(mapData.get("subTotal").toString()).doubleValue();
                    double promoAmount = DF.parse(mapData.get("promoAmount").toString()).doubleValue();
                    double subTotalAfterPromotion = subTotal - promoAmount;
                    mapData.put("subTotalAfterPromotion", Double.valueOf(DF.format(subTotalAfterPromotion)));
                    // check DMC booking then remove supplier fleet
                    if (mapData.get("bookFrom").toString().equalsIgnoreCase("dmc")) {
                        String transactionStatus = mapData.get("transactionStatus").toString();
                        if (transactionStatus.equals("canceled")) {
                            boolean remove = false;
                            String canceller = mapData.get("canceller").toString();
                            if (canceller.equals("rejected")) {
                                remove = true;
                            } else if (canceller.equalsIgnoreCase("dmc")) {
                                if (mapData.get("fleetId").equals(mapData.get("psgFleetId"))) {
                                    remove = true;
                                }
                            }
                            if (remove) {
                                mapData.remove("fleetId");
                                mapData.remove("fleetName");

                                mapData.put("fleetId", "");
                                mapData.put("fleetName", "");
                            }
                        }
                    }
                    mapData.remove("reason");
                    mapData.put("reason", reason);
                    mapData.put("surchargeParameter", mapData.get("qupCommTotal"));
                    mapData.put("dynamicFare", mapData.get("balanceFarmOut"));
                    mapData.put("dynamicType",
                            jsonBooking.get("dynamicType") != null ? jsonBooking.get("dynamicType").toString()
                                    : "factor");
                    mapData.put("instructions",
                            jsonBooking.get("instructions") != null ? jsonBooking.get("instructions") : "");
                    mapData.put("supportId", jsonBooking.get("supportId") != null ? jsonBooking.get("supportId") : "");
                    mapData.put("carTypeService", carTypeService);
                    mapData.put("pickupAddressDetails",
                            jsonBooking.get("pickupAddressDetails") != null
                                    ? gson.fromJson(jsonBooking.get("pickupAddressDetails").toString(),
                                            JSONObject.class)
                                    : new JSONObject());
                    mapData.put("destinationAddressDetails",
                            jsonBooking.get("destinationAddressDetails") != null
                                    ? gson.fromJson(jsonBooking.get("destinationAddressDetails").toString(),
                                            JSONObject.class)
                                    : new JSONObject());
                    mapData.put("vehicleImageRequest",
                            jsonBooking.get("vehicleImageRequest") != null
                                    ? jsonBooking.get("vehicleImageRequest").toString()
                                    : "");
                    mapData.put("packageRateId",
                            jsonBooking.get("packageRateId") != null ? jsonBooking.get("packageRateId").toString()
                                    : "");
                    mapData.put("packageRateName",
                            jsonBooking.get("packageRateName") != null ? jsonBooking.get("packageRateName").toString()
                                    : "");
                    mapData.put("flatRouteId",
                            jsonBooking.get("flatRouteId") != null ? jsonBooking.get("flatRouteId").toString() : "");
                    mapData.put("flatRouteName",
                            jsonBooking.get("flatRouteName") != null ? jsonBooking.get("flatRouteName").toString()
                                    : "");
                    mapData.put("customerGender",
                            jsonBooking.get("customerGender") != null ? jsonBooking.get("customerGender") : -1);
                    mapData.put("promoCustomerType",
                            jsonBooking.get("promoCustomerType") != null ? jsonBooking.get("promoCustomerType") : "");
                    mapData.put("isAirline",
                            jsonBooking.get("isAirline") != null ? jsonBooking.get("isAirline") : false);
                    mapData.put("flightNumber",
                            jsonBooking.get("flightNumber") != null ? jsonBooking.get("flightNumber") : "");
                    mapData.put("operatorId",
                            jsonBooking.get("operatorId") != null ? jsonBooking.get("operatorId") : "");
                    mapData.put("operatorName",
                            jsonBooking.get("operatorName") != null ? jsonBooking.get("operatorName") : "");
                    mapData.put("pointsEarned",
                            jsonBooking.get("pointsEarned") != null ? jsonBooking.get("pointsEarned") : 0);
                    mapData.put("deliveryInfo", deliveryInfo);
                    mapData.put("isFareEdited",
                            jsonBooking.get("isFareEdited") != null ? jsonBooking.get("isFareEdited") : false);
                    mapData.put("reasonEditFare",
                            jsonBooking.get("reasonEditFare") != null ? jsonBooking.get("reasonEditFare") : "");
                    mapData.put("merchantSelfManage",
                            jsonBooking.get("merchantSelfManage") != null ? jsonBooking.get("merchantSelfManage")
                                    : false);
                    mapData.put("isMultiPoints",
                            jsonBooking.get("isMultiPoints") != null ? jsonBooking.get("isMultiPoints") : false);
                    mapData.put("recurring",
                            jsonBooking.get("recurring") != null ? jsonBooking.get("recurring").toString() : "");
                    mapData.put("puPoints",
                            jsonBooking.get("puPoints") != null ? jsonBooking.get("puPoints").toString() : "");
                    mapData.put("doPoints",
                            jsonBooking.get("doPoints") != null ? jsonBooking.get("doPoints").toString() : "");
                    mapData.put("markupPrice",
                            jsonBooking.get("markupPrice") != null ? jsonBooking.get("markupPrice") : 0.0);
                    mapData.put("markupDifference",
                            jsonBooking.get("markupDifference") != null ? jsonBooking.get("markupDifference") : 0.0);
                    mapData.put("reasonMarkup",
                            jsonBooking.get("reasonMarkup") != null ? jsonBooking.get("reasonMarkup") : "");
                    mapData.put("operatorNote",
                            jsonBooking.get("operatorNote") != null ? jsonBooking.get("operatorNote").toString() : "");
                    mapData.put("paxNumber", jsonBooking.get("paxNumber") != null ? jsonBooking.get("paxNumber") : 1);
                    mapData.put("luggageNumber",
                            jsonBooking.get("luggageNumber") != null ? jsonBooking.get("luggageNumber") : 0);
                    if (jsonBooking.get("originDestinationGeoLon") != null) {
                        mapData.put("originDestinationGeoLon", jsonBooking.get("originDestinationGeoLon"));
                    }
                    if (jsonBooking.get("originDestinationGeoLat") != null) {
                        mapData.put("originDestinationGeoLat", jsonBooking.get("originDestinationGeoLat"));
                    }
                    String customerBirthday = jsonBooking.get("customerBirthday") != null
                            ? jsonBooking.get("customerBirthday").toString()
                            : "";
                    try {
                        if (!customerBirthday.isEmpty()) {
                            SimpleDateFormat SDF_MDY = new SimpleDateFormat("MM/dd/yyyy");
                            mapData.put("customerBirthday",
                                    KeysUtil.SDF_DMYHMS.format(SDF_MDY.parse(customerBirthday)));
                        } else {
                            mapData.put("customerBirthday", null);
                        }
                    } catch (Exception ex) {
                        logger.debug(requestId + " - parse customer birthday error: " + ex.getMessage());
                        mapData.put("customerBirthday", null);
                    }
                    boolean isPending = false;
                    if (qupTicketextendRecord != null) {
                        ResultSet rsExtend = qupTicketextendRecord.intoResultSet();
                        while (rsExtend.next()) {
                            mapData.put("tollFee", rsExtend.getDouble("double0"));
                            mapData.put("otherFeesDetails", rsExtend.getString("text0"));
                            mapData.put("gatewayId", rsExtend.getString("varchar0"));
                            mapData.put("signatureUrl", rsExtend.getString("varchar1"));
                            mapData.put("rideSharing", rsExtend.getInt("boolean0"));
                            mapData.put("basicFareCalculator", rsExtend.getDouble("double1"));
                            mapData.put("driverDeduction", rsExtend.getDouble("double8"));
                            mapData.put("fareProvider", rsExtend.getDouble("double2"));
                            mapData.put("tipProvider", rsExtend.getDouble("double3"));
                            mapData.put("taxValueProvider", rsExtend.getDouble("double4"));
                            mapData.put("subTotalProvider", rsExtend.getDouble("double5"));
                            mapData.put("totalProvider", rsExtend.getDouble("double6"));
                            mapData.put("ridePayout", ticketEnt.ridePayout);
                            mapData.put("qupCommission", rsExtend.getDouble("double7"));
                            mapData.put("minimumProvider", rsExtend.getDouble("double9"));
                            mapData.put("fleetCommission", rsExtend.getDouble("double10"));
                            mapData.put("netProfit", rsExtend.getDouble("double11"));
                            mapData.put("bookType", rsExtend.getInt("int0"));
                            mapData.put("driverUserName", rsExtend.getString("text5"));
                            mapData.put("additionalFare", gson.fromJson(rsExtend.getString("text7"), JSONObject.class));
                            mapData.put("reasonCode", rsExtend.getInt("int1"));
                            mapData.put("companyCommission", rsExtend.getInt("int2") == 1);
                            isPending = rsExtend.getBoolean("boolean2");
                            mapData.put("isPending", isPending);
                            mapData.put("shortTrip", Boolean.valueOf(mapTicket.get("shortTrip")));
                            mapData.put("paidByDriver", Boolean.valueOf(mapTicket.get("paidByDriver")));
                            mapData.put("isNewIncentiveReferralWithPromo",
                                    Boolean.valueOf(mapTicket.get("isNewIncentiveReferralWithPromo")));
                            mapData.put("driverNumberType", jsonDriver.get("idType"));
                            mapData.put("driverVehicleID", jsonBooking.get("vehicleTypeId"));
                            mapData.put("driverVehicleType", jsonBooking.get("driverVehicleType"));
                            // replace vehicleType and plateNumber and companyName value
                            mapData.remove("vehicleType");
                            mapData.put("vehicleType", jsonBooking.get("vehicleType"));
                            mapData.remove("plateNumber");
                            mapData.put("plateNumber", jsonBooking.get("plateNumber"));
                            mapData.remove("companyName");
                            mapData.put("companyName", jsonBooking.get("companyName"));

                            String paymentTime = null;
                            try {
                                if (rsExtend.getTimestamp("datetime0") != null)
                                    paymentTime = KeysUtil.SDF_DMYHMS.format(rsExtend.getTimestamp("datetime0"));
                            } catch (Exception ignore) {
                            }

                            mapData.put("paymentTime", paymentTime);

                            String expectedPickupTime = null;
                            try {
                                if (rsExtend.getTimestamp("datetime1") != null)
                                    expectedPickupTime = KeysUtil.SDF_DMYHMS.format(rsExtend.getTimestamp("datetime1"));
                            } catch (Exception ignore) {
                            }

                            mapData.put("expectedPickupTime", expectedPickupTime);
                            ticketEnt.intercity = rsExtend.getBoolean("boolean4");
                            ticketEnt.tripId = rsExtend.getString("varchar9") != null ? rsExtend.getString("varchar9")
                                    : "";
                            mapData.put("walletName", rsExtend.getString("varchar10"));
                            mapData.put("isMinimumTotal", rsExtend.getBoolean("boolean6"));
                            mapData.put("completedWithoutService", rsExtend.getBoolean("boolean7"));
                            mapData.put("payoutType",
                                    rsExtend.getString("varchar11") != null ? rsExtend.getString("varchar11") : "");
                            mapData.put("additionalAuth", rsExtend.getBoolean("boolean9"));
                        }
                    }
                    if (qupTicketextraRecord != null) {
                        ResultSet rsExtra = qupTicketextraRecord.intoResultSet();
                        while (rsExtra.next()) {
                            mapData.put("originTotalFare", rsExtra.getDouble("double2"));
                            ticketEnt.intercityInfo = rsExtra.getString("text2");
                            ticketEnt.originalFare = rsExtra.getDouble("double3");
                            ticketEnt.itemValue = rsExtra.getDouble("double7");
                            ticketEnt.creditTransactionFee = rsExtra.getDouble("double10");
                            ticketEnt.creditTransactionFee = rsExtra.getDouble("double10");
                            mapData.put("itemValue", ticketEnt.itemValue);
                            mapData.put("creditTransactionFee", ticketEnt.creditTransactionFee);
                            mapData.put("merchantCommission", rsExtra.getDouble("double8"));
                            mapData.put("driverTax", rsExtra.getDouble("double11"));
                            mapData.put("hydraOutstandingAmount", rsExtra.getDouble("double15"));
                            mapData.put("hydraPaymentStatus",
                                    rsExtra.getString("varchar2") != null ? rsExtra.getString("varchar2") : "full");

                            String objPartialPayment = rsExtra.getString("text7") != null ? rsExtra.getString("text7")
                                    : "";
                            boolean splitPayment = false;
                            double paidByWallet = 0.0;
                            double paidByOtherMethod = 0.0;
                            double transferredChangeByWallet = 0.0;
                            double returnedChangeByCash = 0.0;
                            int primaryPartialMethod = -1;
                            String otherMethod = "";
                            String returnChange = "";
                            if (!objPartialPayment.isEmpty()) {
                                org.json.JSONObject jsonPartialPayment = new org.json.JSONObject(objPartialPayment);
                                splitPayment = jsonPartialPayment.has("splitPayment")
                                        && jsonPartialPayment.getBoolean("splitPayment");
                                paidByWallet = jsonPartialPayment.has("paidByWallet")
                                        ? jsonPartialPayment.getDouble("paidByWallet")
                                        : 0.0;
                                paidByOtherMethod = jsonPartialPayment.has("paidByOtherMethod")
                                        ? jsonPartialPayment.getDouble("paidByOtherMethod")
                                        : 0.0;
                                transferredChangeByWallet = jsonPartialPayment.has("transferredChangeByWallet")
                                        ? jsonPartialPayment.getDouble("transferredChangeByWallet")
                                        : 0.0;
                                returnedChangeByCash = jsonPartialPayment.has("returnedChangeByCash")
                                        ? jsonPartialPayment.getDouble("returnedChangeByCash")
                                        : 0.0;
                                primaryPartialMethod = jsonPartialPayment.has("primaryPartialMethod")
                                        ? jsonPartialPayment.getInt("primaryPartialMethod")
                                        : -1;
                                otherMethod = jsonPartialPayment.has("otherMethod")
                                        && !jsonPartialPayment.isNull("otherMethod")
                                                ? jsonPartialPayment.getString("otherMethod")
                                                : "";
                                returnChange = jsonPartialPayment.has("returnChange")
                                        && !jsonPartialPayment.isNull("returnChange")
                                                ? jsonPartialPayment.getString("returnChange")
                                                : "";
                            } else if (ticketEnt.transactionStatus.equals(KeysUtil.PAYMENT_PAXWALLET)) {
                                paidByWallet = ticketEnt.total;
                            }
                            mapData.put("cashReceived", rsExtra.getDouble("double4"));
                            mapData.put("splitPayment", splitPayment);
                            mapData.put("paidByWallet", paidByWallet);
                            mapData.put("paidByOtherMethod", paidByOtherMethod);
                            mapData.put("transferredChangeByWallet", transferredChangeByWallet);
                            mapData.put("returnedChangeByCash", returnedChangeByCash);
                            mapData.put("primaryPartialMethod", primaryPartialMethod);
                            mapData.put("otherMethod", otherMethod);
                            mapData.put("driverTax", rsExtra.getDouble("double11"));
                            mapData.put("returnChange", returnChange);

                            String objHydra = rsExtra.getString("text8") != null ? rsExtra.getString("text8") : "";
                            double qupPreferredAmount = 0.0;
                            double qupBuyPrice = 0.0;
                            double qupSellPrice = 0.0;
                            double supplierPayout = 0.0;
                            double buyerPayout = 0.0;
                            double qupEarning = 0.0;
                            double supplierCommission = 0.0;
                            double buyerCommission = 0.0;
                            double profitFromSupplier = 0.0;
                            double profitFromBuyer = 0.0;
                            double penaltyPolicy = 0.0;
                            double penaltyAmount = 0.0;
                            double compensationPolicy = 0.0;
                            double compensationAmount = 0.0;
                            double fleetMarkup = 0.0;
                            String hydraPaymentMethod = "";
                            double sellPriceMarkup = 0.0;
                            boolean isFarmOut = false;
                            if (!objHydra.isEmpty()) {
                                org.json.JSONObject jsHydra = new org.json.JSONObject(objHydra);
                                qupPreferredAmount = jsHydra.has("qupPreferredAmount")
                                        ? jsHydra.getDouble("qupPreferredAmount")
                                        : 0.0;
                                qupBuyPrice = jsHydra.has("qupBuyPrice") ? jsHydra.getDouble("qupBuyPrice") : 0.0;
                                qupSellPrice = jsHydra.has("qupSellPrice") ? jsHydra.getDouble("qupSellPrice") : 0.0;
                                supplierPayout = jsHydra.has("supplierPayout") ? jsHydra.getDouble("supplierPayout")
                                        : 0.0;
                                buyerPayout = jsHydra.has("buyerPayout") ? jsHydra.getDouble("buyerPayout") : 0.0;
                                qupEarning = jsHydra.has("qupEarning") ? jsHydra.getDouble("qupEarning") : 0.0;
                                supplierCommission = jsHydra.has("supplierCommission")
                                        ? jsHydra.getDouble("supplierCommission")
                                        : 0.0;
                                buyerCommission = jsHydra.has("buyerCommission") ? jsHydra.getDouble("buyerCommission")
                                        : 0.0;
                                profitFromSupplier = jsHydra.has("profitFromSupplier")
                                        ? jsHydra.getDouble("profitFromSupplier")
                                        : 0.0;
                                profitFromBuyer = jsHydra.has("profitFromBuyer") ? jsHydra.getDouble("profitFromBuyer")
                                        : 0.0;
                                penaltyPolicy = jsHydra.has("penaltyPolicy") ? jsHydra.getDouble("penaltyPolicy") : 0.0;
                                penaltyAmount = jsHydra.has("penaltyAmount") ? jsHydra.getDouble("penaltyAmount") : 0.0;
                                compensationPolicy = jsHydra.has("compensationPolicy")
                                        ? jsHydra.getDouble("compensationPolicy")
                                        : 0.0;
                                compensationAmount = jsHydra.has("compensationAmount")
                                        ? jsHydra.getDouble("compensationAmount")
                                        : 0.0;
                                fleetMarkup = jsHydra.has("fleetMarkup") ? jsHydra.getDouble("fleetMarkup") : 0.0;
                                hydraPaymentMethod = jsHydra.has("hydraPaymentMethod")
                                        && !jsHydra.isNull("hydraPaymentMethod")
                                                ? jsHydra.getString("hydraPaymentMethod")
                                                : "";
                                isFarmOut = (jsHydra.has("isFarmOut") && !jsHydra.isNull("isFarmOut"))
                                        && jsHydra.getBoolean("isFarmOut");
                                sellPriceMarkup = jsHydra.has("sellPriceMarkup") ? jsHydra.getDouble("sellPriceMarkup")
                                        : 0.0;
                            }
                            mapData.put("qupPreferredAmount", qupPreferredAmount);
                            mapData.put("qupBuyPrice", qupBuyPrice);
                            mapData.put("qupSellPrice", qupSellPrice);
                            mapData.put("supplierPayout", supplierPayout);
                            mapData.put("buyerPayout", buyerPayout);
                            mapData.put("qupEarning", qupEarning);
                            mapData.put("supplierCommission", supplierCommission);
                            mapData.put("buyerCommission", buyerCommission);
                            mapData.put("profitFromSupplier", profitFromSupplier);
                            mapData.put("profitFromBuyer", profitFromBuyer);
                            mapData.put("penaltyPolicy", penaltyPolicy);
                            mapData.put("penaltyAmount", penaltyAmount);
                            mapData.put("compensationPolicy", compensationPolicy);
                            mapData.put("compensationAmount", compensationAmount);
                            mapData.put("fleetMarkup", fleetMarkup);
                            mapData.put("payoutToProviderFleet", rsExtra.getInt("int0") == 1);
                            mapData.put("payoutToHomeFleet", rsExtra.getInt("int1") == 1);
                            mapData.put("chargeTotalFromHomeFleet", false); // default is not charge yet
                            mapData.put("hydraPaymentMethod", hydraPaymentMethod);
                            mapData.put("isFarmOut", isFarmOut);
                            mapData.put("sellPriceMarkup", sellPriceMarkup);

                            mapData.put("companyTax", rsExtra.getDouble("double16"));
                            mapData.put("paidToCompany", rsExtra.getInt("int2"));
                            String objTransfer = rsExtra.getString("text9") != null ? rsExtra.getString("text9") : "";
                            double fleetAmount = 0.0;
                            double driverAmount = 0.0;
                            double companyAmount = 0.0;
                            double stripeFee = 0.0;
                            double pendingSettlement = 0.0;
                            if (!objTransfer.isEmpty()) {
                                org.json.JSONObject jsTransfer = new org.json.JSONObject(objTransfer);
                                fleetAmount = jsTransfer.has("fleetAmount") ? jsTransfer.getDouble("fleetAmount") : 0.0;
                                driverAmount = jsTransfer.has("driverAmount") ? jsTransfer.getDouble("driverAmount")
                                        : 0.0;
                                companyAmount = jsTransfer.has("companyAmount") ? jsTransfer.getDouble("companyAmount")
                                        : 0.0;
                                stripeFee = jsTransfer.has("stripeFee") ? jsTransfer.getDouble("stripeFee") : 0.0;
                                pendingSettlement = jsTransfer.has("pendingSettlement")
                                        ? jsTransfer.getDouble("pendingSettlement")
                                        : 0.0;
                            }
                            mapData.put("fleetAmount", fleetAmount);
                            mapData.put("driverAmount", driverAmount);
                            mapData.put("companyAmount", companyAmount);
                            mapData.put("stripeFee", stripeFee);
                            mapData.put("pendingSettlement", pendingSettlement);

                            String bookingStatus = rsExtra.getString("varchar3") != null ? rsExtra.getString("varchar3")
                                    : "";
                            mapData.put("bookingStatus", bookingStatus);

                            String driverEarningType = rsExtra.getString("varchar4") != null
                                    ? rsExtra.getString("varchar4")
                                    : "";
                            mapData.put("driverEarningType", driverEarningType);
                            mapData.put("editedDriverEarning", rsExtra.getDouble("double17"));
                            mapData.put("customerDebt", rsExtra.getDouble("double18"));
                            String bookingReference = "";
                            double requestedTotalAmount = 0.0;
                            String thirdParty = "";
                            String externalInfo = rsExtra.getString("text10") != null ? rsExtra.getString("text10")
                                    : "";
                            if (!externalInfo.isEmpty()) {
                                org.json.JSONObject jsExternalInfo = new org.json.JSONObject(externalInfo);
                                bookingReference = jsExternalInfo.has("bookingReference") ? jsExternalInfo.getString("bookingReference"): "";
                                thirdParty = jsExternalInfo.has("thirdParty") ? jsExternalInfo.getString("thirdParty"): "";
                                requestedTotalAmount = jsExternalInfo.has("price") ? jsExternalInfo.getDouble("price"): 0.0;
                            }
                            mapData.put("thirdParty", thirdParty);
                            mapData.put("bookingReference", bookingReference);
                            mapData.put("requestedTotalAmount", requestedTotalAmount);
                            mapData.put("stripeMethod",
                                    rsExtra.getString("varchar5") != null ? rsExtra.getString("varchar5") : "");
                            mapData.put("parkingFee", rsExtra.getDouble("double20"));
                            mapData.put("gasFee", rsExtra.getDouble("double21"));
                        }
                    }

                    // add adjust data
                    double addOnPrice = 0.0;
                    double oldSubTotal = 0.0;
                    if (!adjustPrice.isEmpty()) {
                        JSONObject objAdjust = gson.fromJson(adjustPrice, JSONObject.class);
                        addOnPrice = Double.parseDouble(objAdjust.get("addOnPrice").toString());
                        oldSubTotal = Double.parseDouble(objAdjust.get("oldSubTotal").toString());
                    }
                    mapData.put("addOnPrice", addOnPrice);
                    mapData.put("oldSubTotal", oldSubTotal);
                    // put corporate traveller data
                    mapData.put("corpId", mapCorporate.get("corpId"));
                    mapData.put("corpDivision", mapCorporate.get("corpDivision"));
                    mapData.put("costCentre", mapCorporate.get("costCentre"));
                    mapData.put("corpPO", mapCorporate.get("corpPO"));
                    mapData.put("managerName", mapCorporate.get("managerName"));
                    mapData.put("managerEmail", mapCorporate.get("managerEmail"));
                    mapData.put("title", mapCorporate.get("title"));
                    mapData.put("department", mapCorporate.get("department"));
                    // put data setting
                    mapData.put("techFeeActive", jsonSetting.get("techFeeActive"));
                    mapData.put("tipActive", jsonSetting.get("tipActive"));
                    mapData.put("taxActive", jsonSetting.get("taxActive"));
                    mapData.put("meetDriverActive", jsonSetting.get("meetDriverActive"));
                    mapData.put("heavyTrafficActive", jsonSetting.get("heavyTrafficActive"));
                    mapData.put("airportActive", jsonSetting.get("airportActive"));
                    mapData.put("otherFeeActive", jsonSetting.get("otherFeeActive"));
                    mapData.put("rushHourActive", jsonSetting.get("rushHourActive"));
                    mapData.put("tollFeeActive", jsonSetting.get("tollFeeActive"));
                    mapData.put("parkingFeeActive", jsonSetting.get("parkingFeeActive"));
                    mapData.put("gasFeeActive", jsonSetting.get("gasFeeActive"));
                    mapData.put("serviceActive", jsonSetting.get("serviceActive"));
                    mapData.put("hideFare", jsonSetting.get("hideFare"));
                    mapData.put("isRideSharing", jsonSharing.get("rideSharing"));
                    mapData.put("sharedBooks", jsonSharing.get("sharedBooks"));
                    mapData.put("itineraryId",
                            itinerary.get("itineraryId") != null ? itinerary.get("itineraryId") : "");
                    mapData.put("eventId", itinerary.get("eventId") != null ? itinerary.get("eventId") : "");
                    mapData.put("eventName", itinerary.get("eventName") != null ? itinerary.get("eventName") : "");
                    mapData.put("totalMarkup",
                            itinerary.get("totalMarkup") != null
                                    ? Double.valueOf(itinerary.get("totalMarkup").toString())
                                    : 0.0);
                    mapData.put("quoted",
                            itinerary.get("quoted") != null ? Double.valueOf(itinerary.get("quoted").toString()) : 0.0);
                    mapData.put("corporateProfit",
                            itinerary.get("corporateProfit") != null
                                    ? Double.valueOf(itinerary.get("corporateProfit").toString())
                                    : 0.0);
                    mapData.put("organizationId",
                            itinerary.get("organizationId") != null ? itinerary.get("organizationId") : "");
                    mapData.put("organizationName",
                            itinerary.get("organizationName") != null ? itinerary.get("organizationName") : "");
                    mapData.put("dmcCustomerInfo",
                            itinerary.get("dmcCustomerInfo") != null ? itinerary.get("dmcCustomerInfo") : "");
                    mapData.put("customerType",
                            itinerary.get("customerType") != null ? itinerary.get("customerType") : "");
                    mapData.put("departmentId",
                            itinerary.get("departmentId") != null ? itinerary.get("departmentId") : "");
                    mapData.put("departmentName",
                            itinerary.get("departmentName") != null ? itinerary.get("departmentName") : "");
                    mapData.put("corpCustomerInfo",
                            itinerary.get("corpCustomerInfo") != null ? itinerary.get("corpCustomerInfo") : "");

                    mapData.put("onTheWayDistance",
                            OTWDistance.get("onTheWayDistance") != null ? OTWDistance.get("onTheWayDistance") : 0);
                    mapData.put("pobDistance",
                            OTWDistance.get("pobDistance") != null ? OTWDistance.get("pobDistance") : 0);
                    mapData.put("originEstDistance",
                            OTWDistance.get("originEstDistance") != null ? OTWDistance.get("originEstDistance") : 0);
                    mapData.put("originDestination", otwDistance);

                    mapData.put("additionalServices", gson.fromJson(ticketEnt.services, JSONArray.class));
                    mapData.put("extraDestination", gson.fromJson(ticketEnt.extraDestination, JSONArray.class));
                    mapData.put("sos", jsonSetting.get("sos"));
                    mapData.put("firstRqPaymentType", jsonSetting.get("firstRqPaymentType"));
                    mapData.put("typeRate", jsonSetting.get("typeRate"));
                    mapData.put("tollFeePayTo", jsonSetting.get("tollFeePayTo"));
                    mapData.put("parkingFeePayTo", jsonSetting.get("parkingFeePayTo"));
                    mapData.put("gasFeePayTo", jsonSetting.get("gasFeePayTo"));
                    mapData.put("meetDriverPayTo", jsonSetting.get("meetDriverPayTo"));
                    mapData.put("airportPayTo", jsonSetting.get("airportPayTo"));
                    mapData.put("avatar", mapTicket.get("avatar") != null ? mapTicket.get("avatar") : "");
                    mapData.put("passengerAvatar", jsonRequestInfo.get("passengerAvatar"));
                    mapData.put("isVipPassenger", jsonRequestInfo.get("isVipPassenger"));
                    mapData.put("pickUpZoneId", jsonRequestInfo.get("pickUpZoneId"));
                    mapData.put("pickUpZoneName", jsonRequestInfo.get("pickUpZoneName"));
                    mapData.put("customerIdNumber", jsonRequestInfo.get("customerIdNumber"));
                    mapData.put("cardMask", jsonRequestInfo.get("cardMask"));
                    mapData.put("driverFirstName", jsonBooking.get("driverFirstName"));
                    mapData.put("driverLastName", jsonBooking.get("driverLastName"));
                    mapData.put("driverType", jsonBooking.get("driverType"));
                    mapData.put("customerFirstName", jsonBooking.get("customerFirstName"));
                    mapData.put("customerLastName", jsonBooking.get("customerLastName"));
                    mapData.put("bookingNotes", jsonBooking.get("bookingNotes"));
                    mapData.put("operatorNote", jsonBooking.get("operatorNote"));

                    // put referral data
                    mapData.put("referralCode", jsonReferral.get("referralCode"));
                    mapData.put("referralEarning", jsonReferral.get("referralEarning"));
                    mapData.put("totalEarning",
                            jsonReferral.get("totalEarning") != null ? jsonReferral.get("totalEarning") : 0.0);
                    mapData.put("referralMaxAmount",
                            jsonReferral.get("referralMaxAmount") != null ? jsonReferral.get("referralMaxAmount")
                                    : 0.0);
                    mapData.put("referralType", jsonReferral.get("referralType"));
                    mapData.put("isReferralMaxAmount",
                            jsonReferral.get("isReferralMaxAmount") != null ? jsonReferral.get("isReferralMaxAmount")
                                    : false);
                    mapData.put("referralPercent",
                            jsonReferral.get("referralPercent") != null ? jsonReferral.get("referralPercent") : 0.0);
                    mapData.put("referralFrom",
                            jsonReferral.get("referralFrom") != null ? jsonReferral.get("referralFrom") : "");
                    mapData.put("newBalance",
                            mapTicket.get("newBalance") != null ? Double.parseDouble(mapTicket.get("newBalance"))
                                    : 0.0);
                    mapData.put("fleetProfit",
                            mapTicket.get("fleetProfit") != null ? Double.parseDouble(mapTicket.get("fleetProfit"))
                                    : 0.0);

                    String bookFrom = mapData.get("bookFrom").toString();
                    // return outstanding amount
                    String paymentStatus = "full";
                    String paidStatus = "paid";
                    double paidAmount = totalCharged;
                    double originalOutstandingAmount = 0.0;
                    double currentOutstandingAmount = 0.0;
                    if (isPending) {
                        paymentStatus = "pending";
                        paidStatus = "pending";
                        paidAmount = 0.0;
                        if (ticketEnt.paymentType != 5) {
                            originalOutstandingAmount = total;
                            currentOutstandingAmount = total;
                        }
                        // reset total charged amount for normal ride
                        // do NOT reset for intercity booking
                        if (!ticketEnt.intercity || (ticketEnt.intercity && ticketEnt.paymentType == 1))
                            totalCharged = 0.0;
                    } else if (total > totalCharged && !KeysUtil.THIRD_PARTY.contains(bookFrom)) {
                        paymentStatus = "partial";
                        paidStatus = "partial";
                        paidAmount = totalCharged;
                        if (ticketEnt.paymentType != 5) {
                            originalOutstandingAmount = total - totalCharged;
                            currentOutstandingAmount = total - totalCharged;
                        }
                    } else if (KeysUtil.INCOMPLETE_PAYMENT.contains(ticketEnt.transactionStatus) && total == 0 && totalCharged > 0) {
                        paidAmount = totalCharged;
                    }
                    // update refund status for intercity booking
                    double refundAmount = 0.0;
                    if (ticketEnt.intercity && KeysUtil.INCOMPLETE_PAYMENT.contains(ticketEnt.transactionStatus)
                            && ticketEnt.paymentType != 1) { // ignore case cancel by cash
                        if (totalCharged <= 0.0) {
                            paymentStatus = "fullRefund";
                            refundAmount = ticketEnt.originalFare;
                        } else {
                            refundAmount = ticketEnt.originalFare - totalCharged;
                            if (refundAmount > 0) {
                                paymentStatus = "partialRefund";
                            }
                        }
                    }
                    logger.debug(requestId + " - transactionStatus: " + ticketEnt.transactionStatus);
                    logger.debug(requestId + " - paymentStatus: " + paymentStatus);
                    logger.debug(requestId + " - refundAmount: " + refundAmount);
                    // put intercity info
                    mapData.put("tripId", ticketEnt.tripId);
                    if (ticketEnt.intercityInfo != null && !ticketEnt.intercityInfo.isEmpty()) {
                        JSONObject obj = gson.fromJson(ticketEnt.intercityInfo, JSONObject.class);
                        mapData.put("routeNumber", obj.get("routeNumber") != null ? obj.get("routeNumber") : 0);
                        mapData.put("routeId", obj.get("routeId") != null ? obj.get("routeId") : "");
                        mapData.put("routeName", obj.get("routeName") != null ? obj.get("routeName") : "");
                        mapData.put("routeFromZone", obj.get("routeFromZone") != null ? obj.get("routeFromZone") : "");
                        mapData.put("routeToZone", obj.get("routeToZone") != null ? obj.get("routeToZone") : "");
                        mapData.put("routeFromZoneId",
                                obj.get("routeFromZoneId") != null ? obj.get("routeFromZoneId") : "");
                        mapData.put("routeToZoneId", obj.get("routeToZoneId") != null ? obj.get("routeToZoneId") : "");
                        mapData.put("tripType", obj.get("tripType") != null ? obj.get("tripType") : "scheduled");
                    }

                    mapData.put("paymentStatus", paymentStatus);
                    mapData.put("paidStatus", paidStatus);
                    mapData.put("paidAmount", paidAmount);
                    mapData.put("originalOutstandingAmount", originalOutstandingAmount);
                    mapData.put("currentOutstandingAmount", currentOutstandingAmount);
                    // reset total charged amount
                    mapData.remove("totalCharged");
                    mapData.put("totalCharged", totalCharged);
                    mapData.put("refundAmount", refundAmount);
                    mapData.put("totalOrder", total + ticketEnt.itemValue);
                    // return wallet info
                    mapData.put("gateway",
                            jsonBooking.get("gateway") != null ? jsonBooking.get("gateway").toString() : "");
                    mapData.put("walletName",
                            jsonBooking.get("walletName") != null ? jsonBooking.get("walletName").toString() : "");
                    mapData.put("iconUrl",
                            jsonBooking.get("iconUrl") != null ? jsonBooking.get("iconUrl").toString() : "");
                    // return fleet service info
                    mapData.put("serviceActive", serviceActive);
                    mapData.put("fleetServiceFee",
                            mapTicket.get("fleetServiceFee") != null
                                    ? Double.parseDouble(mapTicket.get("fleetServiceFee"))
                                    : 0.0);
                    mapData.put("fleetCommissionFromServiceFee",
                            mapTicket.get("fleetCommissionFromFleetServiceFee") != null
                                    ? Double.parseDouble(mapTicket.get("fleetCommissionFromFleetServiceFee"))
                                    : 0.0);
                    mapData.put("driverCommissionFromServiceFee",
                            mapTicket.get("driverCommissionFromFleetServiceFee") != null
                                    ? Double.parseDouble(mapTicket.get("driverCommissionFromFleetServiceFee"))
                                    : 0.0);
                    JSONArray arrFleetServices = new JSONArray();
                    arrFleetServices.addAll(ticketEnt.fleetServices);
                    mapData.put("fleetServices", arrFleetServices);

                    Gson gsonUpdate = new GsonBuilder().serializeNulls().create();
                    return gsonUpdate.toJson(mapData).replace(".0\"", "\"");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    public PromoCodeUse findByCustomerAndAvailable(String promoCode, String fleetId, String customerId, int available) {
        Connection cnnRead = null;
        QupPromocodeuseRecord qupPromocodeuseRecord = null;
        try {
            cnnRead = getReadConnection(promoCode);

            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            List<QupPromocodeuseRecord> list = contextRead.selectFrom(QUP_PROMOCODEUSE)
                    .where(QUP_PROMOCODEUSE.FLEETID.eq(fleetId))
                    .and(QUP_PROMOCODEUSE.CUSTOMERID.eq(customerId))
                    .and(QUP_PROMOCODEUSE.PROMOCODE.eq(promoCode))
                    .and(QUP_PROMOCODEUSE.AVAILABLE.eq(available))
                    .orderBy(QUP_PROMOCODEUSE.ID.desc())
                    .fetch();
            if (list.size() > 0) {
                qupPromocodeuseRecord = list.get(0);
                PromoCodeUse promoCodeUse = new PromoCodeUse();
                promoCodeUse.id = qupPromocodeuseRecord.getId();
                promoCodeUse.available = qupPromocodeuseRecord.getAvailable();
                promoCodeUse.campaignId = qupPromocodeuseRecord.getCampaignid();
                promoCodeUse.campaignName = qupPromocodeuseRecord.getCampaignname();
                promoCodeUse.createdDate = qupPromocodeuseRecord.getCreatedtime();
                promoCodeUse.customerId = qupPromocodeuseRecord.getCustomerid();
                promoCodeUse.customerName = qupPromocodeuseRecord.getCustomername();
                promoCodeUse.fleetId = qupPromocodeuseRecord.getFleetid();
                promoCodeUse.promoCode = qupPromocodeuseRecord.getPromocode();
                promoCodeUse.promoCodeId = qupPromocodeuseRecord.getPromocodeid();
                promoCodeUse.ticketId = qupPromocodeuseRecord.getTicketid();
                promoCodeUse.timesUsed = qupPromocodeuseRecord.getTimesused();
                promoCodeUse.usedDate = qupPromocodeuseRecord.getUseddate();
                promoCodeUse.usedValue = qupPromocodeuseRecord.getUsedvalue();
                promoCodeUse.totalReceipt = qupPromocodeuseRecord.getTotalreceipt();
                promoCodeUse.currencyISO = qupPromocodeuseRecord.getCurrencyiso();
                return promoCodeUse;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public List<PromoCodeUse> findPromoCodeUseByPromoCodeAndCustomer(String promoCodeId, String customerId) {
        Connection cnnRead = null;
        List<PromoCodeUse> promoCodeUses = new ArrayList<>();
        try {
            cnnRead = getReadConnection(promoCodeId);

            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            List<QupPromocodeuseRecord> list = contextRead.selectFrom(QUP_PROMOCODEUSE)
                    .where(QUP_PROMOCODEUSE.PROMOCODEID.eq(promoCodeId))
                    .and(QUP_PROMOCODEUSE.CUSTOMERID.eq(customerId))
                    .fetch();
            if (list.size() > 0) {
                for (QupPromocodeuseRecord qupPromocodeuseRecord : list) {
                    PromoCodeUse promoCodeUse = new PromoCodeUse();
                    promoCodeUse.id = qupPromocodeuseRecord.getId();
                    promoCodeUse.available = qupPromocodeuseRecord.getAvailable();
                    promoCodeUse.campaignId = qupPromocodeuseRecord.getCampaignid();
                    promoCodeUse.campaignName = qupPromocodeuseRecord.getCampaignname();
                    promoCodeUse.createdDate = qupPromocodeuseRecord.getCreatedtime();
                    promoCodeUse.fleetId = qupPromocodeuseRecord.getFleetid();
                    promoCodeUse.customerId = qupPromocodeuseRecord.getCustomerid();
                    promoCodeUse.customerName = qupPromocodeuseRecord.getCustomername();
                    promoCodeUse.promoCode = qupPromocodeuseRecord.getPromocode();
                    promoCodeUse.promoCodeId = qupPromocodeuseRecord.getPromocodeid();
                    promoCodeUse.ticketId = qupPromocodeuseRecord.getTicketid();
                    promoCodeUse.timesUsed = qupPromocodeuseRecord.getTimesused();
                    promoCodeUse.usedDate = qupPromocodeuseRecord.getUseddate();
                    promoCodeUse.usedValue = qupPromocodeuseRecord.getUsedvalue();
                    promoCodeUse.totalReceipt = qupPromocodeuseRecord.getTotalreceipt();
                    promoCodeUse.currencyISO = qupPromocodeuseRecord.getCurrencyiso();
                    promoCodeUses.add(promoCodeUse);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return promoCodeUses;
    }

    public double findPromoCodeUseByPromoCodeAndCustomerOnRange(String promoCodeId, String customerId, String fromDate,
            String toDate) {
        Connection cnnRead = null;
        double sum = 0.0;
        try {
            cnnRead = getReadConnection(promoCodeId);

            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            sum = contextRead.select(DSL.sum(QUP_PROMOCODEUSE.USEDVALUE))
                    .from(QUP_PROMOCODEUSE)
                    .where(QUP_PROMOCODEUSE.PROMOCODEID.eq(promoCodeId))
                    .and(QUP_PROMOCODEUSE.CUSTOMERID.eq(customerId))
                    .and(QUP_PROMOCODEUSE.CREATEDTIME.between(Timestamp.valueOf(fromDate), Timestamp.valueOf(toDate)))
                    .fetchOne(0, double.class);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return sum;
    }

    public int findPromoCodeUseByPromoCodeAndCustomerAndDate(String promoCodeId, String customerId, String fromDate,
            String toDate) {
        Connection cnnRead = null;
        try {
            cnnRead = getReadConnection(promoCodeId);

            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            return contextRead.select(DSL.count())
                    .from(QUP_PROMOCODEUSE)
                    .where(QUP_PROMOCODEUSE.PROMOCODEID.eq(promoCodeId))
                    .and(QUP_PROMOCODEUSE.CUSTOMERID.eq(customerId))
                    .and(QUP_PROMOCODEUSE.CREATEDTIME.between(Timestamp.valueOf(fromDate), Timestamp.valueOf(toDate)))
                    .fetchOne(0, int.class);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public int findPromoCodeUseByCustomerId(String customerId) {
        Connection cnnRead = null;
        try {
            cnnRead = getReadConnection(customerId);

            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            return contextRead.select(DSL.count())
                    .from(QUP_PROMOCODEUSE)
                    .where(QUP_PROMOCODEUSE.CUSTOMERID.eq(customerId))
                    .fetchOne(0, int.class);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public int findPromoCodeUseByPromoCode(String promoCodeId) {
        Connection cnnRead = null;
        try {
            cnnRead = getReadConnection(promoCodeId);

            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            return contextRead.select(DSL.count())
                    .from(QUP_PROMOCODEUSE)
                    .where(QUP_PROMOCODEUSE.PROMOCODEID.eq(promoCodeId))
                    .fetchOne(0, int.class);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public int findPromoCodeUseByPromoCodeAndDate(String promoCodeId, String fromDate, String toDate) {
        Connection cnnRead = null;
        try {
            cnnRead = getReadConnection(promoCodeId);

            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            return contextRead.select(DSL.count())
                    .from(QUP_PROMOCODEUSE)
                    .where(QUP_PROMOCODEUSE.PROMOCODEID.eq(promoCodeId))
                    .and(QUP_PROMOCODEUSE.CREATEDTIME.between(Timestamp.valueOf(fromDate), Timestamp.valueOf(toDate)))
                    .fetchOne(0, int.class);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public int findTicketByFleetAndCustomer(String fleetId, String customerId) {
        Connection cnnRead = null;
        try {
            cnnRead = getReadConnection(customerId);

            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            List<QupTicketRecord> list = contextRead.selectFrom(QUP_TICKET)
                    .where(QUP_TICKET.FLEETID.eq(fleetId))
                    .and(QUP_TICKET.CUSTOMERID.eq(customerId))
                    .and(QUP_TICKET.STATUS.eq(1))
                    .and(QUP_TICKET.TRANSACTIONSTATUS.notIn("canceled", "noShow", "incident"))
                    .and(QUP_TICKET.BOOKFROM.notIn("CC", "Car-hailing", "Web booking", "Partner", "mDispatcher",
                            "Kiosk"))
                    .fetch();
            return list.size();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public int findCompletedTicketByFleetAndCustomer(String fleetId, String customerId) {
        Connection cnnRead = null;
        try {
            cnnRead = getReadConnection(customerId);

            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            List<QupTicketRecord> list = contextRead.selectFrom(QUP_TICKET)
                    .where(QUP_TICKET.FLEETID.eq(fleetId))
                    .and(QUP_TICKET.CUSTOMERID.eq(customerId))
                    .and(QUP_TICKET.STATUS.eq(1))
                    .and(QUP_TICKET.TRANSACTIONSTATUS.notIn("canceled", "noShow", "incident"))
                    .fetch();
            return list.size();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public void updatePromoCodeUse(PromoCodeUse promoCodeUse) {
        Connection cnn = null;
        try {
            cnn = getConnection(promoCodeUse.promoCodeId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            QupPromocodeuseRecord qupPromocodeuseRecord = context.update(QUP_PROMOCODEUSE)
                    .set(QUP_PROMOCODEUSE.AVAILABLE, promoCodeUse.available)
                    .set(QUP_PROMOCODEUSE.CAMPAIGNID, promoCodeUse.campaignId)
                    .set(QUP_PROMOCODEUSE.CAMPAIGNNAME, promoCodeUse.campaignName)
                    .set(QUP_PROMOCODEUSE.CREATEDTIME, promoCodeUse.createdDate)
                    .set(QUP_PROMOCODEUSE.CUSTOMERNAME, promoCodeUse.customerName)
                    .set(QUP_PROMOCODEUSE.PROMOCODEID, promoCodeUse.promoCodeId)
                    .set(QUP_PROMOCODEUSE.TICKETID, promoCodeUse.ticketId)
                    .set(QUP_PROMOCODEUSE.USEDDATE, promoCodeUse.usedDate)
                    .set(QUP_PROMOCODEUSE.USEDVALUE, promoCodeUse.usedValue)
                    .set(QUP_PROMOCODEUSE.TOTALRECEIPT, promoCodeUse.totalReceipt)
                    .where(QUP_PROMOCODEUSE.ID.eq(promoCodeUse.id))
                    .returning()
                    .fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Long updatePromoCodeUse(long ticketId, double usedValue, double totalReceipt) {
        Connection cnn = null;
        try {
            cnn = getConnection(String.valueOf(ticketId));
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            QupPromocodeuseRecord qupPromocodeuseRecord = context.update(QUP_PROMOCODEUSE)
                    .set(QUP_PROMOCODEUSE.USEDVALUE, usedValue)
                    .set(QUP_PROMOCODEUSE.TOTALRECEIPT, totalReceipt)
                    .where(QUP_PROMOCODEUSE.TICKETID.eq(ticketId))
                    .returning()
                    .fetchOne();

            return qupPromocodeuseRecord.getId();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return (long) 0;
    }

    public void addPromoCodeUse(PromoCodeUse promoCodeUse) {
        Connection cnn = null;
        try {
            cnn = getConnection(promoCodeUse.promoCode);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            QupPromocodeuseRecord qupPromocodeuseRecord = context.insertInto(QUP_PROMOCODEUSE)
                    .set(QUP_PROMOCODEUSE.FLEETID, promoCodeUse.fleetId)
                    .set(QUP_PROMOCODEUSE.TICKETID, promoCodeUse.ticketId != null ? promoCodeUse.ticketId : (long) 0)
                    .set(QUP_PROMOCODEUSE.TOTALRECEIPT,
                            promoCodeUse.totalReceipt != null ? promoCodeUse.totalReceipt : 0.0)
                    .set(QUP_PROMOCODEUSE.CUSTOMERID, promoCodeUse.customerId)
                    .set(QUP_PROMOCODEUSE.PROMOCODE, promoCodeUse.promoCode)
                    .set(QUP_PROMOCODEUSE.PROMOCODEID, promoCodeUse.promoCodeId)
                    .set(QUP_PROMOCODEUSE.TIMESUSED, promoCodeUse.timesUsed)
                    .set(QUP_PROMOCODEUSE.AVAILABLE, promoCodeUse.available)
                    .set(QUP_PROMOCODEUSE.USEDVALUE, promoCodeUse.usedValue)
                    .set(QUP_PROMOCODEUSE.USEDDATE, promoCodeUse.usedDate)
                    .set(QUP_PROMOCODEUSE.CURRENCYISO, promoCodeUse.currencyISO)
                    .set(QUP_PROMOCODEUSE.CREATEDTIME, promoCodeUse.createdDate)
                    .returning()
                    .fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public PromoCodeUse getPromoCodeUseByBookId(String bookId) {
        Connection cnnRead = null;
        try {
            // check if booking contain character
            String numberId = bookId.replaceAll("[a-zA-Z]", "").trim();
            cnnRead = getReadConnection(bookId);
            DSLContext context = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            List<QupPromocodeuseRecord> list = contextRead.selectFrom(QUP_PROMOCODEUSE)
                    .where(QUP_PROMOCODEUSE.TICKETID.eq(new Long(numberId)))
                    .fetch();

            if (list.size() > 0) {
                QupPromocodeuseRecord qupPromocodeuseRecord = list.get(0);
                PromoCodeUse promoCodeUse = new PromoCodeUse();
                promoCodeUse.id = qupPromocodeuseRecord.getId();
                promoCodeUse.available = qupPromocodeuseRecord.getAvailable();
                promoCodeUse.campaignId = qupPromocodeuseRecord.getCampaignid();
                promoCodeUse.campaignName = qupPromocodeuseRecord.getCampaignname();
                promoCodeUse.createdDate = qupPromocodeuseRecord.getCreatedtime();
                promoCodeUse.customerId = qupPromocodeuseRecord.getCustomerid();
                promoCodeUse.customerName = qupPromocodeuseRecord.getCustomername();
                promoCodeUse.fleetId = qupPromocodeuseRecord.getFleetid();
                promoCodeUse.promoCode = qupPromocodeuseRecord.getPromocode();
                promoCodeUse.promoCodeId = qupPromocodeuseRecord.getPromocodeid();
                promoCodeUse.ticketId = qupPromocodeuseRecord.getTicketid();
                promoCodeUse.timesUsed = qupPromocodeuseRecord.getTimesused();
                promoCodeUse.usedDate = qupPromocodeuseRecord.getUseddate();
                promoCodeUse.usedValue = qupPromocodeuseRecord.getUsedvalue();
                promoCodeUse.totalReceipt = qupPromocodeuseRecord.getTotalreceipt();
                promoCodeUse.currencyISO = qupPromocodeuseRecord.getCurrencyiso();
                return promoCodeUse;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void deletePromoCodeUse(PromoCodeUse promoCodeUse) {
        Connection cnn = null;
        try {
            cnn = getConnection(String.valueOf(promoCodeUse.id));
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            QupPromocodeuseRecord qupPromocodeuseRecord = context.delete(QUP_PROMOCODEUSE)
                    .where(QUP_PROMOCODEUSE.ID.eq(promoCodeUse.id))
                    .returning()
                    .fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public String getSignature(String bookId) {
        Connection cnnRead = null;
        String url = "";
        try {
            cnnRead = getReadConnection(bookId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            QupTicketextendRecord qupTicketextendRecord = contextRead.selectFrom(QUP_TICKETEXTEND)
                    .where(QUP_TICKETEXTEND.BOOKID.eq(bookId))
                    .orderBy(QUP_TICKETEXTEND.ID.desc())
                    .fetchOne();
            if (qupTicketextendRecord != null && qupTicketextendRecord.getVarchar1() != null) {
                url = qupTicketextendRecord.getVarchar1();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

        return url;
    }

    public String updateSignature(String bookId, String url) {
        Connection cnn = null;
        try {
            cnn = getConnection(bookId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            context.update(QUP_TICKETEXTEND)
                    .set(QUP_TICKETEXTEND.VARCHAR1, url)
                    .where(QUP_TICKETEXTEND.BOOKID.equal(bookId))
                    .returning()
                    .fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return "";
            }

        }
        return url;
    }

    public PrepaidTransaction getPrepaidTransactionByBookId(String bookid) {
        Connection cnnRead = null;
        QupPrepaidtransactionRecord qupPrepaidtransactionRecord = null;
        try {
            cnnRead = getReadConnection(bookid);

            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            List<QupPrepaidtransactionRecord> list = contextRead.selectFrom(QUP_PREPAIDTRANSACTION)
                    .where(QUP_PREPAIDTRANSACTION.BOOKID.eq(bookid))
                    .fetch();
            if (list.size() > 0) {
                qupPrepaidtransactionRecord = list.get(0);
                PrepaidTransaction prepaidTransaction = new PrepaidTransaction();
                prepaidTransaction.id = qupPrepaidtransactionRecord.getId();
                prepaidTransaction.prepaidId = qupPrepaidtransactionRecord.getPrepaidid();
                prepaidTransaction.topUpId = qupPrepaidtransactionRecord.getTopupid();
                prepaidTransaction.transactionId = qupPrepaidtransactionRecord.getTransactionid();
                prepaidTransaction.name = qupPrepaidtransactionRecord.getName();
                prepaidTransaction.company = qupPrepaidtransactionRecord.getCompany();
                prepaidTransaction.paidBy = qupPrepaidtransactionRecord.getPaidby();
                prepaidTransaction.cardMask = qupPrepaidtransactionRecord.getCardmask();
                prepaidTransaction.amount = qupPrepaidtransactionRecord.getAmount();
                prepaidTransaction.type = qupPrepaidtransactionRecord.getType();
                prepaidTransaction.bookId = qupPrepaidtransactionRecord.getBookid();
                prepaidTransaction.createdDate = qupPrepaidtransactionRecord.getCreateddate();
                prepaidTransaction.fleetId = qupPrepaidtransactionRecord.getFleetid();
                prepaidTransaction.companyId = qupPrepaidtransactionRecord.getCompanyid();
                prepaidTransaction.currencyISO = qupPrepaidtransactionRecord.getCurrencyiso();
                return prepaidTransaction;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public boolean isFirstPay(String token) {
        Connection cnnRead = null;
        try {
            cnnRead = getReadConnection(token);

            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            List<QupTicketRecord> list = contextRead.selectFrom(QUP_TICKET)
                    .where(QUP_TICKET.TOKEN.eq(token))
                    .and(QUP_TICKET.TRANSACTIONSTATUS.eq(KeysUtil.PAYMENT_CARD))
                    .fetch();
            if (list.size() > 0) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    public PrepaidTransaction getPrepaidTransactionBytopupId(String topupId) {
        Connection cnnRead = null;
        QupPrepaidtransactionRecord qupPrepaidtransactionRecord = null;
        try {
            cnnRead = getReadConnection(topupId);

            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            List<QupPrepaidtransactionRecord> list = contextRead.selectFrom(QUP_PREPAIDTRANSACTION)
                    .where(QUP_PREPAIDTRANSACTION.TOPUPID.eq(topupId))
                    .fetch();
            if (list.size() > 0) {
                qupPrepaidtransactionRecord = list.get(0);
                PrepaidTransaction prepaidTransaction = new PrepaidTransaction();
                prepaidTransaction.id = qupPrepaidtransactionRecord.getId();
                prepaidTransaction.prepaidId = qupPrepaidtransactionRecord.getPrepaidid();
                prepaidTransaction.topUpId = qupPrepaidtransactionRecord.getTopupid();
                prepaidTransaction.transactionId = qupPrepaidtransactionRecord.getTransactionid();
                prepaidTransaction.name = qupPrepaidtransactionRecord.getName();
                prepaidTransaction.company = qupPrepaidtransactionRecord.getCompany();
                prepaidTransaction.paidBy = qupPrepaidtransactionRecord.getPaidby();
                prepaidTransaction.cardMask = qupPrepaidtransactionRecord.getCardmask();
                prepaidTransaction.amount = qupPrepaidtransactionRecord.getAmount();
                prepaidTransaction.type = qupPrepaidtransactionRecord.getType();
                prepaidTransaction.bookId = qupPrepaidtransactionRecord.getBookid();
                prepaidTransaction.createdDate = qupPrepaidtransactionRecord.getCreateddate();
                prepaidTransaction.fleetId = qupPrepaidtransactionRecord.getFleetid();
                prepaidTransaction.companyId = qupPrepaidtransactionRecord.getCompanyid();
                return prepaidTransaction;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public long addDriverBalanceTransaction(DriverCreditBalance driverCreditBalance) {
        Connection cnn = null;
        Record record = null;
        try {
            cnn = getConnection(driverCreditBalance.driverId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            String walletName = driverCreditBalance.walletName != null ? driverCreditBalance.walletName : "";
            String reason = driverCreditBalance.reason != null ? driverCreditBalance.reason : "";
            if (!walletName.isEmpty()) {
                reason = walletName + "-" + reason;
            }
            reason = CommonUtils.removeSpecialCharacter(reason).trim().isEmpty() ? ""
                    : CommonUtils.removeSpecialCharacter(reason).trim();
            record = context.insertInto(QUP_DRIVERCREDITBALANCE,
                    QUP_DRIVERCREDITBALANCE.FLEETID,
                    QUP_DRIVERCREDITBALANCE.FLEETNAME,
                    QUP_DRIVERCREDITBALANCE.DRIVERID,
                    QUP_DRIVERCREDITBALANCE.DRIVERNAME,
                    QUP_DRIVERCREDITBALANCE.DRIVERPHONE,
                    QUP_DRIVERCREDITBALANCE.DRIVERUSERNAME,
                    QUP_DRIVERCREDITBALANCE.COMPANYID,
                    QUP_DRIVERCREDITBALANCE.COMPANYNAME,
                    QUP_DRIVERCREDITBALANCE.TYPE,
                    QUP_DRIVERCREDITBALANCE.AMOUNT,
                    QUP_DRIVERCREDITBALANCE.CURRENTAMOUNT,
                    QUP_DRIVERCREDITBALANCE.NEWAMOUNT,
                    QUP_DRIVERCREDITBALANCE.REASON,
                    QUP_DRIVERCREDITBALANCE.BOOKID,
                    QUP_DRIVERCREDITBALANCE.OPERATORID,
                    QUP_DRIVERCREDITBALANCE.OPERATORNAME,
                    QUP_DRIVERCREDITBALANCE.CURRENCYISO,
                    QUP_DRIVERCREDITBALANCE.CURRENCYSYMBOL,
                    QUP_DRIVERCREDITBALANCE.CARDMASKED,
                    QUP_DRIVERCREDITBALANCE.CREATEDTIME)
                    .values(driverCreditBalance.fleetId,
                            driverCreditBalance.fleetName,
                            driverCreditBalance.driverId,
                            driverCreditBalance.driverName,
                            driverCreditBalance.driverPhone,
                            driverCreditBalance.driverUserName,
                            driverCreditBalance.companyId,
                            driverCreditBalance.companyName,
                            driverCreditBalance.type,
                            driverCreditBalance.amount,
                            driverCreditBalance.currentAmount,
                            driverCreditBalance.newAmount,
                            reason,
                            driverCreditBalance.bookId,
                            driverCreditBalance.operatorId,
                            driverCreditBalance.operatorName,
                            driverCreditBalance.currencyISO,
                            driverCreditBalance.currencySymbol,
                            driverCreditBalance.cardMasked,
                            driverCreditBalance.createdDate)
                    .returning()
                    .fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (record != null)
            return record.getValue(QUP_DRIVERCREDITBALANCE.ID);
        return 0;
    }

    public long addPaxWalletTransaction(PaxWalletTransaction paxWalletTransaction) {
        Connection cnn = null;
        Record record = null;
        try {
            cnn = getConnection(paxWalletTransaction.userId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            String description = paxWalletTransaction.description;
            String walletName = paxWalletTransaction.walletName != null ? paxWalletTransaction.walletName : "";
            if (!walletName.isEmpty()) {
                description = description.replace("topup", walletName);
            }
            record = context.insertInto(QUP_PAXWALLETTRANSACTION,
                    QUP_PAXWALLETTRANSACTION.FLEETID,
                    QUP_PAXWALLETTRANSACTION.USERID,
                    QUP_PAXWALLETTRANSACTION.TRANSACTIONTYPE,
                    QUP_PAXWALLETTRANSACTION.DESCRIPTION,
                    QUP_PAXWALLETTRANSACTION.AMOUNT,
                    QUP_PAXWALLETTRANSACTION.NEWBALANCE,
                    QUP_PAXWALLETTRANSACTION.CURRENCYISO,
                    QUP_PAXWALLETTRANSACTION.CURRENCYSYMBOL,
                    QUP_PAXWALLETTRANSACTION.CUSTOMERNAME,
                    QUP_PAXWALLETTRANSACTION.PHONENUMBER,
                    QUP_PAXWALLETTRANSACTION.CREATEDDATE)
                    .values(paxWalletTransaction.fleetId,
                            paxWalletTransaction.userId,
                            paxWalletTransaction.transactionType,
                            description,
                            paxWalletTransaction.amount,
                            paxWalletTransaction.newBalance,
                            paxWalletTransaction.currencyISO,
                            paxWalletTransaction.currencySymbol,
                            paxWalletTransaction.customerName,
                            paxWalletTransaction.phoneNumber,
                            paxWalletTransaction.createdDate)
                    .returning()
                    .fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (record != null)
            return record.getValue(QUP_PAXWALLETTRANSACTION.ID);
        return 0;
    }

    public void deleteDriverBalanceTransaction(long transactionId) {
        Connection cnn = null;
        try {
            cnn = getConnection(String.valueOf(transactionId));
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            QupDrivercreditbalanceRecord qupDrivercreditbalanceRecord = context.delete(QUP_DRIVERCREDITBALANCE)
                    .where(QUP_DRIVERCREDITBALANCE.ID.eq(transactionId))
                    .returning()
                    .fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public List<TicketReturn> getTicketByFleetIdAndFilter(String fleetId, String from) {
        List<TicketReturn> ticketReturns = new ArrayList<>();
        Connection cnnRead = null;
        try {
            cnnRead = getReadConnection(fleetId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            List<QupTicketRecord> list = contextRead.selectFrom(QUP_TICKET)
                    .where(QUP_TICKET.FLEETID.eq(fleetId))
                    .and(QUP_TICKET.TRANSACTIONSTATUS.in(KeysUtil.PAYMENT_CARD, KeysUtil.PAYMENT_MDISPATCHER,
                            KeysUtil.PAYMENT_CORPORATE,
                            KeysUtil.PAYMENT_PREPAID, KeysUtil.PAYMENT_APPLEPAY))
                    .and(QUP_TICKET.COMPLETEDTIME.gt(Timestamp.valueOf(from)))
                    .fetch();
            if (list.size() > 0) {
                for (QupTicketRecord qupTicketRecord : list) {
                    TicketReturn ticketReturn = new TicketReturn();
                    ticketReturn.id = qupTicketRecord.getId();
                    ticketReturn.fleetId = qupTicketRecord.getFleetid();
                    ticketReturn.bookId = qupTicketRecord.getBookid();
                    ticketReturn.bookFrom = qupTicketRecord.getBookfrom();
                    ticketReturn.transactionStatus = qupTicketRecord.getTransactionstatus();
                    ticketReturn.currencyISO = qupTicketRecord.getCurrencyiso();
                    ticketReturn.total = qupTicketRecord.getTotal();
                    ticketReturn.totalFare = qupTicketRecord.getTotalfare();
                    ticketReturn.deductions = qupTicketRecord.getDeductions();
                    ticketReturns.add(ticketReturn);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return ticketReturns;
    }

    public void updateTransactionFee(Map<String, String> mapTicket) {
        Connection cnn = null;
        try {
            cnn = getConnection(mapTicket.get("bookId"));
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            context.update(QUP_TICKET)
                    .set(QUP_TICKET.NETEARNING, Double.parseDouble(mapTicket.get("netEarning")))
                    .set(QUP_TICKET.DEDUCTIONS, Double.parseDouble(mapTicket.get("deductions")))
                    .set(QUP_TICKET.TRANSACTIONFEE, Double.parseDouble(mapTicket.get("transactionFee")))
                    .where(QUP_TICKET.BOOKID.eq(mapTicket.get("bookId")))
                    .returning()
                    .fetchOne();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public String getTicketData(String bookId) {
        Connection cnnRead = null;
        try {
            cnnRead = getReadConnection(bookId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));

            String ticket = contextRead.selectFrom(QUP_TICKET)
                    .where(QUP_TICKET.BOOKID.eq(bookId))
                    .orderBy(QUP_TICKET.ID.desc())
                    .fetch()
                    .formatJSON();

            QupTicketextendRecord qupTicketextendRecord = contextRead.selectFrom(QUP_TICKETEXTEND)
                    .where(QUP_TICKETEXTEND.BOOKID.eq(bookId))
                    .fetchOne();

            if (ticket != null) {
                JSONParser parser = new JSONParser();
                JSONObject jsonObject = (JSONObject) parser.parse(ticket);
                JSONArray fields = (JSONArray) jsonObject.get("fields");
                List<String> listKeys = new ArrayList<>();
                if (listKeys.size() == 0) {
                    for (Object field : fields) {
                        JSONObject object = (JSONObject) field;
                        String key = object.get("name") != null ? object.get("name").toString() : "";
                        listKeys.add(key);
                    }
                }
                JSONArray records = (JSONArray) jsonObject.get("records");
                if (records.size() > 0) {
                    JSONArray object = (JSONArray) records.get(0);
                    Map<String, Object> mapData = new HashMap<>();
                    for (int k = 0; k < listKeys.size(); k++) {
                        mapData.put(listKeys.get(k), object.get(k));
                    }
                    if (qupTicketextendRecord != null) {
                        ResultSet rsExtend = qupTicketextendRecord.intoResultSet();
                        while (rsExtend.next()) {
                            mapData.put("tollFee", rsExtend.getDouble("double0"));
                            mapData.put("otherFeesDetails", rsExtend.getString("text0"));
                            mapData.put("gatewayId", rsExtend.getString("varchar0"));
                            mapData.put("signatureUrl", rsExtend.getString("varchar1"));
                            mapData.put("rideSharing", rsExtend.getInt("boolean0"));
                            mapData.put("basicFareCalculator", rsExtend.getDouble("double1"));
                            mapData.put("driverDeduction", rsExtend.getDouble("double8"));
                            mapData.put("fareProvider", rsExtend.getDouble("double2"));
                            mapData.put("tipProvider", rsExtend.getDouble("double3"));
                            mapData.put("taxValueProvider", rsExtend.getDouble("double4"));
                            mapData.put("subTotalProvider", rsExtend.getDouble("double5"));
                            mapData.put("totalProvider", rsExtend.getDouble("double6"));
                            mapData.put("qupCommission", rsExtend.getDouble("double7"));
                            mapData.put("minimumProvider", rsExtend.getDouble("double9"));
                            mapData.put("fleetCommission", rsExtend.getDouble("double10"));
                            mapData.put("netProfit", rsExtend.getDouble("double11"));
                            mapData.put("bookType", rsExtend.getInt("int0"));
                            mapData.put("driverUserName", rsExtend.getString("text5"));
                            mapData.put("additionalFare", gson.fromJson(rsExtend.getString("text7"), JSONObject.class));
                            mapData.put("reasonCode", rsExtend.getInt("int1"));
                            mapData.put("referral", gson.fromJson(rsExtend.getString("varchar4"), JSONObject.class));
                        }
                    }

                    Gson gsonUpdate = new GsonBuilder().serializeNulls().create();
                    return gsonUpdate.toJson(mapData).replace(".0\"", "\"");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void updateCompletedTicket(TicketEnt ticketEnt) {
        Connection cnn = null;
        try {
            cnn = getConnection(ticketEnt.bookId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            context.update(QUP_TICKET)
                    .set(QUP_TICKET.TOTAL, ticketEnt.total)
                    /* .set(QUP_TICKET.TOTALCHARGED, ticketEnt.total) */
                    .set(QUP_TICKET.TOTALFARE, ticketEnt.totalFare)
                    .set(QUP_TICKET.COMM, ticketEnt.comm)
                    .set(QUP_TICKET.DEDUCTIONS, ticketEnt.deductions)
                    .set(QUP_TICKET.NETEARNING, ticketEnt.netEarning)
                    .set(QUP_TICKET.RIDEPAYMENT, ticketEnt.ridePayment)
                    .set(QUP_TICKET.GROSSEARNING, ticketEnt.grossEarning)
                    .set(QUP_TICKET.TRANSACTIONFEE, ticketEnt.transactionFee)
                    .set(QUP_TICKET.SUBTOTAL, ticketEnt.subTotal)
                    .set(QUP_TICKET.SUBTOTALCHARGED, ticketEnt.subTotal)
                    .set(QUP_TICKET.TAX, ticketEnt.tax)
                    .set(QUP_TICKET.TIP, ticketEnt.tip)
                    .set(QUP_TICKET.MDISPATCHERCOMMISSION, ticketEnt.mDispatcherCommission)
                    .set(QUP_TICKET.PROMOAMOUNT, ticketEnt.promoAmount)
                    .where(QUP_TICKET.BOOKID.eq(ticketEnt.bookId))
                    .returning()
                    .fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public String getAllRecords(String type, int i, String fleetId, String from, String to) {
        Connection cnnRead = null;
        String result = "";
        try {
            cnnRead = getReadConnection(type);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            switch (type) {
                case "PROMOCODEUSE": {
                    if (fleetId.equals("all")) {
                        result = contextRead.selectFrom(QUP_PROMOCODEUSE)
                                .where(QUP_PROMOCODEUSE.USEDDATE.between(Timestamp.valueOf(from),
                                        Timestamp.valueOf(to)))
                                .orderBy(QUP_PROMOCODEUSE.ID.desc())
                                .limit(KeysUtil.SIZE)
                                .offset(i * KeysUtil.SIZE)
                                .fetch()
                                .formatJSON();
                    } else {
                        result = contextRead.selectFrom(QUP_PROMOCODEUSE)
                                .where(QUP_PROMOCODEUSE.FLEETID.eq(fleetId))
                                .and(QUP_PROMOCODEUSE.USEDDATE.between(Timestamp.valueOf(from), Timestamp.valueOf(to)))
                                .orderBy(QUP_PROMOCODEUSE.ID.desc())
                                .limit(KeysUtil.SIZE)
                                .offset(i * KeysUtil.SIZE)
                                .fetch()
                                .formatJSON();
                    }
                }
                    break;
                case "PREPAID": {
                    if (fleetId.equals("all")) {
                        result = contextRead.selectFrom(QUP_PREPAIDTRANSACTION)
                                .where(QUP_PREPAIDTRANSACTION.CREATEDDATE.between(Timestamp.valueOf(from),
                                        Timestamp.valueOf(to)))
                                .orderBy(QUP_PREPAIDTRANSACTION.ID.desc())
                                .limit(KeysUtil.SIZE)
                                .offset(i * KeysUtil.SIZE)
                                .fetch()
                                .formatJSON();
                    } else {
                        result = contextRead.selectFrom(QUP_PREPAIDTRANSACTION)
                                .where(QUP_PREPAIDTRANSACTION.FLEETID.eq(fleetId))
                                .and(QUP_PREPAIDTRANSACTION.CREATEDDATE.between(Timestamp.valueOf(from),
                                        Timestamp.valueOf(to)))
                                .orderBy(QUP_PREPAIDTRANSACTION.ID.desc())
                                .limit(KeysUtil.SIZE)
                                .offset(i * KeysUtil.SIZE)
                                .fetch()
                                .formatJSON();
                    }
                }
                    break;
                case "DRIVERBALANCE": {
                    if (fleetId.equals("all")) {
                        result = contextRead.selectFrom(QUP_DRIVERCREDITBALANCE)
                                .where(QUP_DRIVERCREDITBALANCE.CREATEDTIME.between(Timestamp.valueOf(from),
                                        Timestamp.valueOf(to)))
                                .orderBy(QUP_DRIVERCREDITBALANCE.ID.desc())
                                .limit(KeysUtil.SIZE)
                                .offset(i * KeysUtil.SIZE)
                                .fetch()
                                .formatJSON();
                    } else {
                        result = contextRead.selectFrom(QUP_DRIVERCREDITBALANCE)
                                .where(QUP_DRIVERCREDITBALANCE.FLEETID.eq(fleetId))
                                .and(QUP_DRIVERCREDITBALANCE.CREATEDTIME.between(Timestamp.valueOf(from),
                                        Timestamp.valueOf(to)))
                                .orderBy(QUP_DRIVERCREDITBALANCE.ID.desc())
                                .limit(KeysUtil.SIZE)
                                .offset(i * KeysUtil.SIZE)
                                .fetch()
                                .formatJSON();
                    }
                }
                    break;
                case "SETTLEMENTHISTORY": {
                    if (fleetId.equals("all")) {
                        result = contextRead.selectFrom(QUP_SETTLEMENTHISTORY)
                                .where(QUP_SETTLEMENTHISTORY.CREATEDDATE.between(Timestamp.valueOf(from),
                                        Timestamp.valueOf(to)))
                                .orderBy(QUP_SETTLEMENTHISTORY.ID.desc())
                                .limit(KeysUtil.SIZE)
                                .offset(i * KeysUtil.SIZE)
                                .fetch()
                                .formatJSON();
                    } else {
                        result = contextRead.selectFrom(QUP_SETTLEMENTHISTORY)
                                .where(QUP_SETTLEMENTHISTORY.FLEETID.eq(fleetId))
                                .and(QUP_SETTLEMENTHISTORY.CREATEDDATE.between(Timestamp.valueOf(from),
                                        Timestamp.valueOf(to)))
                                .orderBy(QUP_SETTLEMENTHISTORY.ID.desc())
                                .limit(KeysUtil.SIZE)
                                .offset(i * KeysUtil.SIZE)
                                .fetch()
                                .formatJSON();
                    }
                }
                    break;

            }
            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public String updatePaymentStatus(String bookId, String transactionStatus, String paidBy) {
        Connection cnn = null;
        try {
            cnn = getConnection(bookId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            int update = context.update(QUP_TICKET)
                    .set(QUP_TICKET.TRANSACTIONSTATUS, transactionStatus)
                    .set(QUP_TICKET.PAIDBY, paidBy)
                    .where(QUP_TICKET.BOOKID.eq(bookId))
                    .execute();
            if (update == 1) {
                QupTicketRecord qupTicketRecord = context.selectFrom(QUP_TICKET)
                        .where(QUP_TICKET.BOOKID.eq(bookId))
                        .orderBy(QUP_TICKET.ID.desc())
                        .fetchOne();
                return qupTicketRecord.get("completedTime").toString();
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    public ObjResponse getTicketByTimeRange(String from, String to) {
        ObjResponse response = new ObjResponse();
        Connection cnnRead = null;
        Map<String, Object> mapData = new HashMap<>();
        try {
            cnnRead = getReadConnection("");
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            int totalBookings = contextRead.selectCount().from(QUP_TICKET)
                    .where(QUP_TICKET.FLEETID.ne(""))
                    .and(QUP_TICKET.COMPLETEDTIME.ge(Timestamp.valueOf(from)))
                    .and(QUP_TICKET.COMPLETEDTIME.le(Timestamp.valueOf(to)))
                    .fetchOne(0, int.class);

            Result totalCharged = contextRead.select(sum(QUP_TICKET.TOTALCHARGED)).from(QUP_TICKET)
                    .where(QUP_TICKET.FLEETID.ne(""))
                    .and(QUP_TICKET.COMPLETEDTIME.ge(Timestamp.valueOf(from)))
                    .and(QUP_TICKET.COMPLETEDTIME.le(Timestamp.valueOf(to)))
                    .fetch();
            Record object = (Record) totalCharged.get(0);
            Double total = 0.0;
            if (object.getValue("sum") != null) {
                total = Double.parseDouble(object.getValue("sum").toString());
            }

            mapData.put("totalBookings", totalBookings);
            mapData.put("totalCharged", total);
            response.returnCode = 200;
            response.response = mapData;
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        response.returnCode = 407;
        return response;
    }

    public ObjResponse getTicketInTimeRange(String fleetId, String from, String to, String currencyISO) {
        ObjResponse response = new ObjResponse();
        Connection cnnRead = null;
        try {
            cnnRead = getReadConnection(fleetId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            Result<?> join = contextRead
                    .select(QUP_TICKET.BOOKFROM, QUP_TICKET.TRANSACTIONSTATUS, QUP_TICKET.CURRENCYISOCHARGED,
                            QUP_TICKET.PRICINGTYPE, QUP_TICKET.TOTALCHARGED, QUP_TICKET.NETEARNING,
                            QUP_TICKET.PSGFLEETCOMMAMOUNT,
                            QUP_TICKETEXTEND.DOUBLE6)
                    .from(QUP_TICKET)
                    .innerJoin(QUP_TICKETEXTEND)
                    .on(QUP_TICKET.BOOKID.eq(QUP_TICKETEXTEND.BOOKID))
                    .and(QUP_TICKET.FLEETID.eq(fleetId))
                    .and(QUP_TICKET.CURRENCYISOCHARGED.eq(currencyISO))
                    .and(QUP_TICKET.COMPLETEDTIME.greaterOrEqual(Timestamp.valueOf(from)))
                    .and(QUP_TICKET.COMPLETEDTIME.lt(Timestamp.valueOf(to)))
                    .fetch();
            contextRead.close();
            List<Map> listData = new ArrayList<>();
            for (Record r : join) {
                QupTicketRecord ticket = r.into(QUP_TICKET);
                QupTicketextendRecord extend = r.into(QUP_TICKETEXTEND);
                Map<String, Object> mapData = new HashMap<>();
                mapData.put("bookFrom", ticket.getBookfrom());
                mapData.put("transactionStatus", ticket.getTransactionstatus());
                mapData.put("pricingType", ticket.getPricingtype());
                mapData.put("totalCharged", ticket.getTotalcharged());
                mapData.put("totalProvider", extend.getDouble6());
                mapData.put("netEarning", ticket.getNetearning());
                mapData.put("netEarningProvider", ticket.getPsgfleetcommamount());
                listData.add(mapData);
            }
            response.returnCode = 200;
            response.response = listData;
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        response.returnCode = 407;
        return response;
    }

    public long addReportBilling(FleetSubscription fleetSubscription) {
        Connection cnn = null;
        Record record = null;
        try {
            cnn = getConnection(fleetSubscription.fleetId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            record = context.insertInto(QUP_FLEETSUBSCRIPTION,
                    QUP_FLEETSUBSCRIPTION.FLEETID,
                    QUP_FLEETSUBSCRIPTION.FLEETNAME,
                    QUP_FLEETSUBSCRIPTION.FLEETTYPE,
                    QUP_FLEETSUBSCRIPTION.SOLUTIONPARTNER,
                    QUP_FLEETSUBSCRIPTION.MONTHLYFEE,
                    QUP_FLEETSUBSCRIPTION.ACTIVEDRIVER,
                    QUP_FLEETSUBSCRIPTION.FREEDRIVER,
                    QUP_FLEETSUBSCRIPTION.CHARGEDRIVER,
                    QUP_FLEETSUBSCRIPTION.CHARGEDRIVERORI,
                    QUP_FLEETSUBSCRIPTION.COSTPERDRIVER,
                    QUP_FLEETSUBSCRIPTION.COSTPERDRIVERORI,
                    QUP_FLEETSUBSCRIPTION.DRIVERFEE,
                    QUP_FLEETSUBSCRIPTION.DRIVERFEEORI,
                    QUP_FLEETSUBSCRIPTION.ACTIVEVEHICLE,
                    QUP_FLEETSUBSCRIPTION.FREEVEHICLE,
                    QUP_FLEETSUBSCRIPTION.CHARGEVEHICLE,
                    QUP_FLEETSUBSCRIPTION.CHARGEVEHICLEORI,
                    QUP_FLEETSUBSCRIPTION.COSTPERVEHICLE,
                    QUP_FLEETSUBSCRIPTION.COSTPERVEHICLEORI,
                    QUP_FLEETSUBSCRIPTION.VEHICLEFEE,
                    QUP_FLEETSUBSCRIPTION.VEHICLEFEEORI,
                    QUP_FLEETSUBSCRIPTION.TOTALTRANSACTION,
                    QUP_FLEETSUBSCRIPTION.FREETRANSACTION,
                    QUP_FLEETSUBSCRIPTION.CHARGETRANSACTION,
                    QUP_FLEETSUBSCRIPTION.CHARGETRANSACTIONORI,
                    QUP_FLEETSUBSCRIPTION.COSTPERTRANSACTION,
                    QUP_FLEETSUBSCRIPTION.COSTPERTRANSACTIONORI,
                    QUP_FLEETSUBSCRIPTION.TRANSACTIONFEEAMOUNT,
                    QUP_FLEETSUBSCRIPTION.TRANSACTIONFEEAMOUNTORI,
                    QUP_FLEETSUBSCRIPTION.TOTALBASEFARE,
                    QUP_FLEETSUBSCRIPTION.TRANSACTIONFEEPERCENT,
                    QUP_FLEETSUBSCRIPTION.PERCENTAGESETTING,
                    QUP_FLEETSUBSCRIPTION.FLEETCURRENCYISO,
                    QUP_FLEETSUBSCRIPTION.FLEETCURRENCYSYMBOL,
                    QUP_FLEETSUBSCRIPTION.CHARGECURRENCYISO,
                    QUP_FLEETSUBSCRIPTION.CHARGECURRENCYSYMBOL,
                    QUP_FLEETSUBSCRIPTION.EXCHANGERATE,
                    QUP_FLEETSUBSCRIPTION.TRANSACTIONFEEPERCENTEXCHANGE,
                    QUP_FLEETSUBSCRIPTION.TRANSACTIONFEEPERCENTEXCHANGEORI,
                    QUP_FLEETSUBSCRIPTION.TOTALTRANSACTIONFEE,
                    QUP_FLEETSUBSCRIPTION.TOTALTRANSACTIONFEEORI,
                    QUP_FLEETSUBSCRIPTION.OTHERFEE,
                    QUP_FLEETSUBSCRIPTION.OTHERFEEORI,
                    QUP_FLEETSUBSCRIPTION.ADJUSTDRIVERDESC,
                    QUP_FLEETSUBSCRIPTION.ADJUSTVEHICLEDESC,
                    QUP_FLEETSUBSCRIPTION.ADJUSTTRANSACTIONAMOUNTDESC,
                    QUP_FLEETSUBSCRIPTION.ADJUSTTRANSACTIONPERCENTDESC,
                    QUP_FLEETSUBSCRIPTION.ADJUSTOTHERDESC,
                    QUP_FLEETSUBSCRIPTION.INVOICENUMBER,
                    QUP_FLEETSUBSCRIPTION.TRANSACTIONID,
                    QUP_FLEETSUBSCRIPTION.SUBTOTALQUPFEE,
                    QUP_FLEETSUBSCRIPTION.SUBTOTALQUPFEEORI,
                    QUP_FLEETSUBSCRIPTION.TAXSETTING,
                    QUP_FLEETSUBSCRIPTION.TAX,
                    QUP_FLEETSUBSCRIPTION.TAXORI,
                    QUP_FLEETSUBSCRIPTION.TOTALQUPFEE,
                    QUP_FLEETSUBSCRIPTION.TOTALQUPFEEORI,
                    QUP_FLEETSUBSCRIPTION.STATUS,
                    QUP_FLEETSUBSCRIPTION.MONTH,
                    QUP_FLEETSUBSCRIPTION.YEAR,
                    QUP_FLEETSUBSCRIPTION.CREATEDDATE)
                    .values(fleetSubscription.fleetId,
                            fleetSubscription.fleetName,
                            fleetSubscription.fleetType,
                            fleetSubscription.solutionPartner,
                            fleetSubscription.monthlyFee,
                            fleetSubscription.activeDriver,
                            fleetSubscription.freeDriver,
                            fleetSubscription.chargeDriver,
                            fleetSubscription.chargeDriver,
                            fleetSubscription.costPerDriver,
                            fleetSubscription.costPerDriver,
                            fleetSubscription.driverFee,
                            fleetSubscription.driverFee,
                            fleetSubscription.activeVehicle,
                            fleetSubscription.freeVehicle,
                            fleetSubscription.chargeVehicle,
                            fleetSubscription.chargeVehicle,
                            fleetSubscription.costPerVehicle,
                            fleetSubscription.costPerVehicle,
                            fleetSubscription.vehicleFee,
                            fleetSubscription.vehicleFee,
                            fleetSubscription.totalTransaction,
                            fleetSubscription.freeTransaction,
                            fleetSubscription.chargeTransaction,
                            fleetSubscription.chargeTransaction,
                            fleetSubscription.costPerTransaction,
                            fleetSubscription.costPerTransaction,
                            fleetSubscription.transactionFeeAmount,
                            fleetSubscription.transactionFeeAmount,
                            fleetSubscription.totalBaseFare,
                            fleetSubscription.transactionFeePercent,
                            fleetSubscription.percentageSetting,
                            fleetSubscription.fleetCurrencyISO,
                            fleetSubscription.fleetCurrencySymbol,
                            fleetSubscription.chargeCurrencyISO,
                            fleetSubscription.chargeCurrencySymbol,
                            fleetSubscription.exchangeRate,
                            fleetSubscription.transactionFeePercentExchange,
                            fleetSubscription.transactionFeePercentExchange,
                            fleetSubscription.totalTransactionFee,
                            fleetSubscription.totalTransactionFee,
                            fleetSubscription.otherFee,
                            fleetSubscription.otherFee,
                            fleetSubscription.adjustDriverDesc,
                            fleetSubscription.adjustVehicleDesc,
                            fleetSubscription.adjustTransactionAmountDesc,
                            fleetSubscription.adjustTransactionPercentDesc,
                            fleetSubscription.adjustOtherDesc,
                            fleetSubscription.invoiceNumber,
                            fleetSubscription.transactionId,
                            fleetSubscription.subTotalQupFee,
                            fleetSubscription.subTotalQupFee,
                            fleetSubscription.taxSetting,
                            fleetSubscription.tax,
                            fleetSubscription.tax,
                            fleetSubscription.totalQupFee,
                            fleetSubscription.totalQupFee,
                            fleetSubscription.status,
                            fleetSubscription.month,
                            fleetSubscription.year,
                            fleetSubscription.createdDate)
                    .returning()
                    .fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (record != null)
            return record.getValue(QUP_FLEETSUBSCRIPTION.ID);
        return 0;
    }

    public Map<String, String> updateReportBilling(FleetSubscriptionEnt ent) {
        Connection cnn = null;
        try {
            cnn = getConnection(String.valueOf(ent.fleetSubscriptionId));
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            long id = (long) ent.fleetSubscriptionId;
            int returnCode;
            if (ent.invoiceSent == null)
                ent.invoiceSent = false;
            if (!ent.invoiceSent) {
                returnCode = context.update(QUP_FLEETSUBSCRIPTION)
                        .set(QUP_FLEETSUBSCRIPTION.CHARGEDRIVER, ent.chargeDriver)
                        .set(QUP_FLEETSUBSCRIPTION.COSTPERDRIVER, ent.costPerDriver)
                        .set(QUP_FLEETSUBSCRIPTION.DRIVERFEE, ent.driverFee)
                        .set(QUP_FLEETSUBSCRIPTION.CHARGEVEHICLE, ent.chargeVehicle)
                        .set(QUP_FLEETSUBSCRIPTION.COSTPERVEHICLE, ent.costPerVehicle)
                        .set(QUP_FLEETSUBSCRIPTION.VEHICLEFEE, ent.vehicleFee)
                        .set(QUP_FLEETSUBSCRIPTION.CHARGETRANSACTION, ent.chargeTransaction)
                        .set(QUP_FLEETSUBSCRIPTION.COSTPERTRANSACTION, ent.costPerTransaction)
                        .set(QUP_FLEETSUBSCRIPTION.TRANSACTIONFEEAMOUNT, ent.transactionFeeAmount)
                        .set(QUP_FLEETSUBSCRIPTION.TRANSACTIONFEEPERCENTEXCHANGE, ent.transactionFeePercentExchange)
                        .set(QUP_FLEETSUBSCRIPTION.TOTALTRANSACTIONFEE, ent.totalTransactionFee)
                        .set(QUP_FLEETSUBSCRIPTION.OTHERFEE, ent.otherFee)
                        .set(QUP_FLEETSUBSCRIPTION.ADJUSTDRIVERDESC,
                                ent.adjustDriverDesc != null ? ent.adjustDriverDesc.trim() : "")
                        .set(QUP_FLEETSUBSCRIPTION.ADJUSTVEHICLEDESC,
                                ent.adjustVehicleDesc != null ? ent.adjustVehicleDesc.trim() : "")
                        .set(QUP_FLEETSUBSCRIPTION.ADJUSTTRANSACTIONAMOUNTDESC,
                                ent.adjustTransactionAmountDesc != null ? ent.adjustTransactionAmountDesc.trim() : "")
                        .set(QUP_FLEETSUBSCRIPTION.ADJUSTTRANSACTIONPERCENTDESC,
                                ent.adjustTransactionPercentDesc != null ? ent.adjustTransactionPercentDesc.trim() : "")
                        .set(QUP_FLEETSUBSCRIPTION.ADJUSTOTHERDESC,
                                ent.adjustOtherDesc != null ? ent.adjustOtherDesc.trim() : "")
                        .set(QUP_FLEETSUBSCRIPTION.SUBTOTALQUPFEE, ent.subTotalQupFee)
                        .set(QUP_FLEETSUBSCRIPTION.TAX, ent.tax)
                        .set(QUP_FLEETSUBSCRIPTION.TOTALQUPFEE, ent.totalQupFee)
                        .where(QUP_FLEETSUBSCRIPTION.ID.eq(id))
                        .execute();
            } else {
                returnCode = context.update(QUP_FLEETSUBSCRIPTION)
                        .set(QUP_FLEETSUBSCRIPTION.INVOICESENT, ent.invoiceSent ? 1 : 0)
                        .where(QUP_FLEETSUBSCRIPTION.ID.eq(id))
                        .execute();
            }

            if (returnCode == 1) {
                QupFleetsubscriptionRecord record = context.selectFrom(QUP_FLEETSUBSCRIPTION)
                        .where(QUP_FLEETSUBSCRIPTION.ID.eq(id))
                        .fetchOne();
                Map<String, String> map = new HashMap<>();
                map.put("id", String.valueOf(record.getId()));
                map.put("createdDate", record.get("createdDate").toString());
                return map;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return new HashMap<>();
    }

    public void removeBillingData(String fleetId, int month, int year) {
        Connection cnn = null;
        try {
            cnn = getConnection(fleetId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            context.delete(QUP_FLEETSUBSCRIPTION)
                    .where(QUP_FLEETSUBSCRIPTION.FLEETID.eq(fleetId))
                    .and(QUP_FLEETSUBSCRIPTION.MONTH.eq(month))
                    .and(QUP_FLEETSUBSCRIPTION.YEAR.eq(year))
                    .execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public int updateBillingReport(FleetSubscription fleetSubscription) {
        Connection cnn = null;
        try {
            cnn = getConnection(String.valueOf(fleetSubscription.fleetSubscriptionId));
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            long id = (long) fleetSubscription.fleetSubscriptionId;
            return context.update(QUP_FLEETSUBSCRIPTION)
                    .set(QUP_FLEETSUBSCRIPTION.STATUS, fleetSubscription.status)
                    .set(QUP_FLEETSUBSCRIPTION.TRANSACTIONID,
                            fleetSubscription.transactionId != null ? fleetSubscription.transactionId : "'")
                    .set(QUP_FLEETSUBSCRIPTION.COMPLETEDDATE, fleetSubscription.completedDate)
                    .set(QUP_FLEETSUBSCRIPTION.TRANSACTIONDATE, fleetSubscription.transactionDate)
                    .where(QUP_FLEETSUBSCRIPTION.ID.eq(id))
                    .execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public long addHydraIncome(HydraIncome hydraIncome) {
        Connection cnn = null;
        Record record = null;
        try {
            cnn = getConnection(hydraIncome.bookId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            record = context.insertInto(QUP_HYDRAINCOME,
                    QUP_HYDRAINCOME.BOOKID,
                    QUP_HYDRAINCOME.DMCFLEETID,
                    QUP_HYDRAINCOME.DMCFLEETNAME,
                    QUP_HYDRAINCOME.SUPPLIERFLEETID,
                    QUP_HYDRAINCOME.SUPPLIERFLEETNAME,
                    QUP_HYDRAINCOME.AMOUNT,
                    QUP_HYDRAINCOME.CURRENCYISO,
                    QUP_HYDRAINCOME.CURRENCYSYMBOL,
                    QUP_HYDRAINCOME.REASON,
                    QUP_HYDRAINCOME.STATUS,
                    QUP_HYDRAINCOME.CREATEDDATE,
                    QUP_HYDRAINCOME.PAIDDATE,
                    QUP_HYDRAINCOME.CHARGEID,
                    QUP_HYDRAINCOME.DRIVERNAME,
                    QUP_HYDRAINCOME.PASSENGERNAME,
                    QUP_HYDRAINCOME.TRANSACTIONSTATUS,
                    QUP_HYDRAINCOME.PAIDBY,
                    QUP_HYDRAINCOME.CANCELLER,
                    QUP_HYDRAINCOME.PENALTYAMOUNT,
                    QUP_HYDRAINCOME.REFUNDAMOUNT,
                    QUP_HYDRAINCOME.GROSSPROFIT,
                    QUP_HYDRAINCOME.TOTALCOLLECT,
                    QUP_HYDRAINCOME.PENALTYPERCENT,
                    QUP_HYDRAINCOME.PENALTYREASON,
                    QUP_HYDRAINCOME.PENALTYDURATION,
                    QUP_HYDRAINCOME.PENALTYNOTE,
                    QUP_HYDRAINCOME.COMPLETEDTIME,
                    QUP_HYDRAINCOME.HAPPENEDTIME,
                    QUP_HYDRAINCOME.ISFINAL,
                    QUP_HYDRAINCOME.ITINERARYID,
                    QUP_HYDRAINCOME.EVENTID,
                    QUP_HYDRAINCOME.EVENTNAME,
                    QUP_HYDRAINCOME.COMPANYID,
                    QUP_HYDRAINCOME.COMPANYNAME,
                    QUP_HYDRAINCOME.TRAVELERTYPE,
                    QUP_HYDRAINCOME.QUOTED,
                    QUP_HYDRAINCOME.PICKUP,
                    QUP_HYDRAINCOME.TIMEZONEPICKUP,
                    QUP_HYDRAINCOME.DESTINATION,
                    QUP_HYDRAINCOME.TIMEZONEDESTINATION,
                    QUP_HYDRAINCOME.PICKUPTIME)
                    .values(hydraIncome.bookId,
                            hydraIncome.dmcFleetId,
                            hydraIncome.dmcFleetName,
                            hydraIncome.supplierFleetId,
                            hydraIncome.supplierFleetName,
                            hydraIncome.amount,
                            hydraIncome.currencyISO,
                            hydraIncome.currencySymbol,
                            hydraIncome.reason,
                            hydraIncome.status,
                            hydraIncome.createdDate,
                            hydraIncome.paidDate,
                            hydraIncome.chargeId,
                            hydraIncome.driverName,
                            hydraIncome.passengerName,
                            hydraIncome.transactionStatus,
                            hydraIncome.paidBy,
                            hydraIncome.canceller,
                            hydraIncome.penaltyAmount,
                            hydraIncome.refundAmount,
                            hydraIncome.grossProfit,
                            hydraIncome.totalCollect,
                            hydraIncome.penaltyPercent,
                            hydraIncome.penaltyReason,
                            hydraIncome.penaltyDuration,
                            hydraIncome.penaltyNote,
                            hydraIncome.completedTime,
                            hydraIncome.happenedTime,
                            hydraIncome.isFinal,
                            hydraIncome.itineraryId,
                            hydraIncome.eventId,
                            hydraIncome.eventName,
                            hydraIncome.companyId,
                            hydraIncome.companyName,
                            hydraIncome.travelerType,
                            hydraIncome.quoted,
                            hydraIncome.pickup,
                            hydraIncome.timeZonePickUp,
                            hydraIncome.destination,
                            hydraIncome.timeZoneDestination,
                            hydraIncome.pickupTime)
                    .returning()
                    .fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (record != null)
            return record.getValue(QUP_HYDRAINCOME.ID);
        return 0;
    }

    public HydraIncome getHydraIncomeByBookId(String bookId, String status) {
        Connection cnnRead = null;
        QupHydraincomeRecord qupHydraincomeRecord = null;
        try {
            cnnRead = getReadConnection(bookId);

            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            List<QupHydraincomeRecord> list = contextRead.selectFrom(QUP_HYDRAINCOME)
                    .where(QUP_HYDRAINCOME.BOOKID.eq(bookId))
                    .and(QUP_HYDRAINCOME.STATUS.eq(status))
                    .orderBy(QUP_HYDRAINCOME.ID.desc())
                    .fetch();
            if (list.size() > 0) {
                qupHydraincomeRecord = list.get(0);
                HydraIncome hydraIncome = new HydraIncome();
                hydraIncome.dmcFleetId = qupHydraincomeRecord.getDmcfleetid();
                hydraIncome.dmcFleetName = qupHydraincomeRecord.getDmcfleetname();
                hydraIncome.supplierFleetId = qupHydraincomeRecord.getSupplierfleetid();
                hydraIncome.supplierFleetName = qupHydraincomeRecord.getSupplierfleetname();
                hydraIncome.amount = qupHydraincomeRecord.getAmount();
                hydraIncome.status = qupHydraincomeRecord.getStatus();
                hydraIncome.chargeId = qupHydraincomeRecord.getChargeid();
                hydraIncome.currencyISO = qupHydraincomeRecord.getCurrencyiso();
                hydraIncome.currencySymbol = qupHydraincomeRecord.getCurrencysymbol();
                return hydraIncome;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public String getWalletWithdrawal(String fleetId, List<String> company, List<String> driver, String currencyISO,
            int from, int size) {
        Connection cnnRead = null;
        String result = "";
        int count = 0;
        double sum = 0.0;
        ObjResponse response = new ObjResponse();
        Map<String, Object> mapResponse = new HashMap<>();
        try {
            cnnRead = getReadConnection(fleetId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            if (driver.contains("all")) {
                if (company.contains("all")) {
                    result = contextRead.selectFrom(QUP_WALLETTRANSFER)
                            .where(QUP_WALLETTRANSFER.FLEETID.eq(fleetId))
                            .and(QUP_WALLETTRANSFER.CURRENCYISO.eq(currencyISO))
                            .and(QUP_WALLETTRANSFER.STATUS.in(KeysUtil.WITHDRAWAL_PENDING,
                                    KeysUtil.WITHDRAWAL_PROCESSING))
                            .orderBy(QUP_WALLETTRANSFER.ID.desc())
                            .offset(from)
                            .limit(size)
                            .fetch()
                            .formatJSON();

                    count = contextRead.select(DSL.count())
                            .from(QUP_WALLETTRANSFER)
                            .where(QUP_WALLETTRANSFER.FLEETID.eq(fleetId))
                            .and(QUP_WALLETTRANSFER.CURRENCYISO.eq(currencyISO))
                            .and(QUP_WALLETTRANSFER.STATUS.in(KeysUtil.WITHDRAWAL_PENDING,
                                    KeysUtil.WITHDRAWAL_PROCESSING))
                            .fetchOne(0, int.class);

                    sum = contextRead.select(DSL.sum(QUP_WALLETTRANSFER.AMOUNT))
                            .from(QUP_WALLETTRANSFER)
                            .where(QUP_WALLETTRANSFER.FLEETID.eq(fleetId))
                            .and(QUP_WALLETTRANSFER.CURRENCYISO.eq(currencyISO))
                            .and(QUP_WALLETTRANSFER.STATUS.in(KeysUtil.WITHDRAWAL_PENDING,
                                    KeysUtil.WITHDRAWAL_PROCESSING))
                            .fetchOne(0, double.class);
                } else {
                    result = contextRead.selectFrom(QUP_WALLETTRANSFER)
                            .where(QUP_WALLETTRANSFER.FLEETID.eq(fleetId))
                            .and(QUP_WALLETTRANSFER.COMPANYID.in(company))
                            .and(QUP_WALLETTRANSFER.CURRENCYISO.eq(currencyISO))
                            .and(QUP_WALLETTRANSFER.STATUS.in(KeysUtil.WITHDRAWAL_PENDING,
                                    KeysUtil.WITHDRAWAL_PROCESSING))
                            .orderBy(QUP_WALLETTRANSFER.ID.desc())
                            .offset(from)
                            .limit(size)
                            .fetch()
                            .formatJSON();

                    count = contextRead.select(DSL.count())
                            .from(QUP_WALLETTRANSFER)
                            .where(QUP_WALLETTRANSFER.FLEETID.eq(fleetId))
                            .and(QUP_WALLETTRANSFER.COMPANYID.in(company))
                            .and(QUP_WALLETTRANSFER.CURRENCYISO.eq(currencyISO))
                            .and(QUP_WALLETTRANSFER.STATUS.in(KeysUtil.WITHDRAWAL_PENDING,
                                    KeysUtil.WITHDRAWAL_PROCESSING))
                            .fetchOne(0, int.class);

                    sum = contextRead.select(DSL.sum(QUP_WALLETTRANSFER.AMOUNT))
                            .from(QUP_WALLETTRANSFER)
                            .where(QUP_WALLETTRANSFER.FLEETID.eq(fleetId))
                            .and(QUP_WALLETTRANSFER.COMPANYID.in(company))
                            .and(QUP_WALLETTRANSFER.CURRENCYISO.eq(currencyISO))
                            .and(QUP_WALLETTRANSFER.STATUS.in(KeysUtil.WITHDRAWAL_PENDING,
                                    KeysUtil.WITHDRAWAL_PROCESSING))
                            .fetchOne(0, double.class);
                }
            } else {
                result = contextRead.selectFrom(QUP_WALLETTRANSFER)
                        .where(QUP_WALLETTRANSFER.FLEETID.eq(fleetId))
                        .and(QUP_WALLETTRANSFER.DRIVERID.in(driver))
                        .and(QUP_WALLETTRANSFER.CURRENCYISO.eq(currencyISO))
                        .and(QUP_WALLETTRANSFER.STATUS.in(KeysUtil.WITHDRAWAL_PENDING, KeysUtil.WITHDRAWAL_PROCESSING))
                        .orderBy(QUP_WALLETTRANSFER.ID.desc())
                        .offset(from)
                        .limit(size)
                        .fetch()
                        .formatJSON();

                count = contextRead.select(DSL.count())
                        .from(QUP_WALLETTRANSFER)
                        .where(QUP_WALLETTRANSFER.FLEETID.eq(fleetId))
                        .and(QUP_WALLETTRANSFER.DRIVERID.in(driver))
                        .and(QUP_WALLETTRANSFER.CURRENCYISO.eq(currencyISO))
                        .and(QUP_WALLETTRANSFER.STATUS.in(KeysUtil.WITHDRAWAL_PENDING, KeysUtil.WITHDRAWAL_PROCESSING))
                        .fetchOne(0, int.class);

                sum = contextRead.select(DSL.sum(QUP_WALLETTRANSFER.AMOUNT))
                        .from(QUP_WALLETTRANSFER)
                        .where(QUP_WALLETTRANSFER.FLEETID.eq(fleetId))
                        .and(QUP_WALLETTRANSFER.DRIVERID.in(driver))
                        .and(QUP_WALLETTRANSFER.CURRENCYISO.eq(currencyISO))
                        .and(QUP_WALLETTRANSFER.STATUS.in(KeysUtil.WITHDRAWAL_PENDING, KeysUtil.WITHDRAWAL_PROCESSING))
                        .fetchOne(0, double.class);
            }

            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        mapResponse.put("size", count);
        mapResponse.put("total", sum);
        mapResponse.put("result", result);
        response.returnCode = 200;
        response.response = mapResponse;
        return response.toString();
    }

    public long addWalletTransfer(WalletTransfer walletTransfer) {
        Connection cnn = null;
        Record record = null;
        try {
            cnn = getConnection(walletTransfer.driverId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            record = context.insertInto(QUP_WALLETTRANSFER,
                    QUP_WALLETTRANSFER.FLEETID,
                    QUP_WALLETTRANSFER.REFERENCEID,
                    QUP_WALLETTRANSFER.DRIVERID,
                    QUP_WALLETTRANSFER.DRIVERNAME,
                    QUP_WALLETTRANSFER.DRIVERPHONE,
                    QUP_WALLETTRANSFER.DRIVERUSERNAME,
                    QUP_WALLETTRANSFER.COMPANYID,
                    QUP_WALLETTRANSFER.COMPANYNAME,
                    QUP_WALLETTRANSFER.DESCRIPTION,
                    QUP_WALLETTRANSFER.TRANSFERTYPE,
                    QUP_WALLETTRANSFER.RECEIVERACCOUNT,
                    QUP_WALLETTRANSFER.AMOUNT,
                    QUP_WALLETTRANSFER.CURRENTBALANCE,
                    QUP_WALLETTRANSFER.CURRENCYISO,
                    QUP_WALLETTRANSFER.CURRENCYSYMBOL,
                    QUP_WALLETTRANSFER.STATUS,
                    QUP_WALLETTRANSFER.CREATEDDATE,
                    QUP_WALLETTRANSFER.COMPLETEDDATE)
                    .values(walletTransfer.fleetId,
                            walletTransfer.referenceId,
                            walletTransfer.driverId,
                            walletTransfer.driverName,
                            walletTransfer.driverPhone,
                            walletTransfer.driverUserName,
                            walletTransfer.companyId,
                            walletTransfer.companyName,
                            CommonUtils.removeSpecialCharacter(walletTransfer.description).trim().isEmpty() ? ""
                                    : CommonUtils.removeSpecialCharacter(walletTransfer.description).trim(),
                            walletTransfer.transferType,
                            walletTransfer.receiverAccount,
                            walletTransfer.amount,
                            walletTransfer.currentBalance,
                            walletTransfer.currencyISO,
                            walletTransfer.currencySymbol,
                            walletTransfer.status,
                            walletTransfer.createdDate,
                            walletTransfer.completedDate)
                    .returning()
                    .fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (record != null)
            return record.getValue(QUP_WALLETTRANSFER.ID);
        return 0;
    }

    public double updateWalletTransfer(WalletTransfer walletTransfer) {
        Connection cnn = null;
        double balance = 0.0;
        try {
            cnn = getConnection(walletTransfer.transactionId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            long id = (long) walletTransfer.id;
            String reason = walletTransfer.reason != null ? walletTransfer.reason : "";
            int update = context.update(QUP_WALLETTRANSFER)
                    .set(QUP_WALLETTRANSFER.TRANSACTIONID, walletTransfer.transactionId)
                    .set(QUP_WALLETTRANSFER.OPERATORID, walletTransfer.operatorId)
                    .set(QUP_WALLETTRANSFER.OPERATORNAME, walletTransfer.operatorName)
                    .set(QUP_WALLETTRANSFER.REASON,
                            CommonUtils.removeSpecialCharacter(reason).trim().isEmpty() ? ""
                                    : CommonUtils.removeSpecialCharacter(reason).trim())
                    .set(QUP_WALLETTRANSFER.STATUS, walletTransfer.status)
                    .set(QUP_WALLETTRANSFER.COMPLETEDDATE, walletTransfer.completedDate)
                    .where(QUP_WALLETTRANSFER.ID.eq(id))
                    .execute();
            if (update != 0) {
                BigDecimal balanceBD = getWalletBalance(walletTransfer.transactionId, walletTransfer.driverId, "cash",
                        walletTransfer.currencyISO);
                balance = balanceBD.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                context.update(QUP_WALLETTRANSFER)
                        .set(QUP_WALLETTRANSFER.CURRENTBALANCE, balance)
                        .where(QUP_WALLETTRANSFER.ID.eq(id))
                        .execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return balance;
    }

    public WalletTransfer getWalletTransfer(String referenceId) {
        Connection cnnRead = null;
        QupWallettransferRecord qupWallettransferRecord = null;
        try {
            cnnRead = getReadConnection(referenceId);

            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            List<QupWallettransferRecord> list = contextRead.selectFrom(QUP_WALLETTRANSFER)
                    .where(QUP_WALLETTRANSFER.REFERENCEID.eq(referenceId))
                    .fetch();
            if (list.size() > 0) {
                qupWallettransferRecord = list.get(0);
                WalletTransfer walletTransfer = new WalletTransfer();
                walletTransfer.id = qupWallettransferRecord.getId();
                walletTransfer.fleetId = qupWallettransferRecord.getFleetid();
                walletTransfer.referenceId = qupWallettransferRecord.getReferenceid();
                walletTransfer.driverId = qupWallettransferRecord.getDriverid();
                walletTransfer.driverName = qupWallettransferRecord.getDrivername();
                walletTransfer.driverPhone = qupWallettransferRecord.getDriverphone();
                walletTransfer.driverUserName = qupWallettransferRecord.getDriverusername();
                walletTransfer.companyId = qupWallettransferRecord.getCompanyid();
                walletTransfer.companyName = qupWallettransferRecord.getCompanyname();
                walletTransfer.description = qupWallettransferRecord.getDescription();
                walletTransfer.transferType = qupWallettransferRecord.getTransfertype();
                walletTransfer.receiverAccount = qupWallettransferRecord.getReceiveraccount();
                walletTransfer.amount = qupWallettransferRecord.getAmount();
                walletTransfer.currentBalance = qupWallettransferRecord.getCurrentbalance();
                walletTransfer.currencyISO = qupWallettransferRecord.getCurrencyiso();
                walletTransfer.currencySymbol = qupWallettransferRecord.getCurrencysymbol();
                walletTransfer.status = qupWallettransferRecord.getStatus();
                walletTransfer.createdDate = qupWallettransferRecord.getCreateddate();

                return walletTransfer;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public long addWalletTransaction(WalletTransaction walletTransaction) {
        Connection cnn = null;
        Record record = null;
        try {
            cnn = getConnection(walletTransaction.userId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            record = context
                    .insertInto(QUP_WALLETTRANSACTION,
                            QUP_WALLETTRANSACTION.FLEETID,
                            QUP_WALLETTRANSACTION.USERID,
                            QUP_WALLETTRANSACTION.TYPE,
                            QUP_WALLETTRANSACTION.TRANSACTIONID,
                            QUP_WALLETTRANSACTION.DESCRIPTION,
                            QUP_WALLETTRANSACTION.CASHIN,
                            QUP_WALLETTRANSACTION.CASHOUT,
                            QUP_WALLETTRANSACTION.CURRENCYISO,
                            QUP_WALLETTRANSACTION.CREATEDDATE)
                    .values(walletTransaction.fleetId,
                            walletTransaction.userId,
                            walletTransaction.type,
                            walletTransaction.transactionId,
                            walletTransaction.description,
                            walletTransaction.cashIn,
                            walletTransaction.cashOut,
                            walletTransaction.currencyISO,
                            walletTransaction.createdDate)
                    .returning()
                    .fetchOne();

            BigDecimal cashIn = context.select(DSL.sum(QUP_WALLETTRANSACTION.CASHIN))
                    .from(QUP_WALLETTRANSACTION)
                    .where(QUP_WALLETTRANSACTION.FLEETID.eq(walletTransaction.fleetId))
                    .and(QUP_WALLETTRANSACTION.USERID.eq(walletTransaction.userId))
                    .and(QUP_WALLETTRANSACTION.TYPE.eq(walletTransaction.type))
                    .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(walletTransaction.currencyISO))
                    .fetchOne(0, BigDecimal.class);
            if (cashIn == null)
                cashIn = new BigDecimal("0.0");
            BigDecimal cashOut = context.select(DSL.sum(QUP_WALLETTRANSACTION.CASHOUT))
                    .from(QUP_WALLETTRANSACTION)
                    .where(QUP_WALLETTRANSACTION.FLEETID.eq(walletTransaction.fleetId))
                    .and(QUP_WALLETTRANSACTION.USERID.eq(walletTransaction.userId))
                    .and(QUP_WALLETTRANSACTION.TYPE.eq(walletTransaction.type))
                    .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(walletTransaction.currencyISO))
                    .fetchOne(0, BigDecimal.class);
            if (cashOut == null)
                cashOut = new BigDecimal("0.0");
            double balance = cashIn.subtract(cashOut).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();

            // update balance to compare alter
            int update = context.update(QUP_WALLETBALANCE)
                    .set(QUP_WALLETBALANCE.BALANCE, balance)
                    .where(QUP_WALLETBALANCE.FLEETID.eq(walletTransaction.fleetId))
                    .and(QUP_WALLETBALANCE.USERID.eq(walletTransaction.userId))
                    .and(QUP_WALLETBALANCE.TYPE.eq(walletTransaction.type))
                    .and(QUP_WALLETBALANCE.CURRENCYISO.eq(walletTransaction.currencyISO))
                    .execute();
            if (update == 0) {
                context.insertInto(QUP_WALLETBALANCE,
                        QUP_WALLETBALANCE.FLEETID,
                        QUP_WALLETBALANCE.USERID,
                        QUP_WALLETBALANCE.TYPE,
                        QUP_WALLETBALANCE.CURRENCYISO,
                        QUP_WALLETBALANCE.BALANCE)
                        .values(walletTransaction.fleetId,
                                walletTransaction.userId,
                                walletTransaction.type,
                                walletTransaction.currencyISO,
                                balance)
                        .returning()
                        .fetchOne();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (record != null)
            return record.getValue(QUP_WALLETTRANSACTION.ID);
        return 0;
    }

    public BigDecimal getWalletBalance(String requestId, String userId, String type, String currencyISO) {
        Connection cnnRead = null;
        BigDecimal returnValue = new BigDecimal("0.0");
        try {
            cnnRead = getReadConnection(userId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            BigDecimal cashIn = contextRead.select(DSL.sum(QUP_WALLETTRANSACTION.CASHIN))
                    .from(QUP_WALLETTRANSACTION)
                    .where(QUP_WALLETTRANSACTION.USERID.eq(userId))
                    .and(QUP_WALLETTRANSACTION.TYPE.eq(type))
                    .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .fetchOne(0, BigDecimal.class);
            if (cashIn == null)
                cashIn = new BigDecimal("0.0");
            logger.debug(requestId + " - cashIn: " + cashIn);
            BigDecimal cashOut = contextRead.select(DSL.sum(QUP_WALLETTRANSACTION.CASHOUT))
                    .from(QUP_WALLETTRANSACTION)
                    .where(QUP_WALLETTRANSACTION.USERID.eq(userId))
                    .and(QUP_WALLETTRANSACTION.TYPE.eq(type))
                    .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .fetchOne(0, BigDecimal.class);
            if (cashOut == null)
                cashOut = new BigDecimal("0.0");
            logger.debug(requestId + " - cashOut: " + cashOut);
            // get balance to compare
            BigDecimal balanceBig = contextRead.select(DSL.sum(QUP_WALLETBALANCE.BALANCE))
                    .from(QUP_WALLETBALANCE).where(QUP_WALLETBALANCE.USERID.eq(userId))
                    .and(QUP_WALLETBALANCE.TYPE.eq(type))
                    .and(QUP_WALLETBALANCE.CURRENCYISO.eq(currencyISO))
                    .fetchOne(0, BigDecimal.class);

            returnValue = cashIn.subtract(cashOut);
            logger.debug(requestId + " - returnValue: " + returnValue);
            if (balanceBig != null) {
                double value = returnValue.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                double balance = balanceBig.doubleValue();
                logger.debug(requestId + " - balance: " + balance);
                if (value != balance || (value - balance != 0)) {
                    boolean matched = false;
                    logger.debug(requestId + " - balance not match - retry");
                    int i = 0;
                    while (!matched && i < 5) {
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException ignore) {
                        }
                        cashIn = contextRead.select(DSL.sum(QUP_WALLETTRANSACTION.CASHIN))
                                .from(QUP_WALLETTRANSACTION)
                                .where(QUP_WALLETTRANSACTION.USERID.eq(userId))
                                .and(QUP_WALLETTRANSACTION.TYPE.eq(type))
                                .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                                .fetchOne(0, BigDecimal.class);
                        if (cashIn == null)
                            cashIn = new BigDecimal("0.0");
                        cashOut = contextRead.select(DSL.sum(QUP_WALLETTRANSACTION.CASHOUT))
                                .from(QUP_WALLETTRANSACTION)
                                .where(QUP_WALLETTRANSACTION.USERID.eq(userId))
                                .and(QUP_WALLETTRANSACTION.TYPE.eq(type))
                                .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                                .fetchOne(0, BigDecimal.class);
                        if (cashOut == null)
                            cashOut = new BigDecimal("0.0");

                        returnValue = cashIn.subtract(cashOut);
                        if (!Objects.equals(returnValue, balance)) {
                            logger.debug(requestId + " - balance NOT matched after retry");
                        } else {
                            logger.debug(requestId + " - balance MATCHED after retry");
                            matched = true;
                        }
                        i++;
                    }
                }
            }
            contextRead.close();
        } catch (Exception e) {
            logger.debug(requestId + " - userId: " + userId + " + - exception:" + CommonUtils.getError(e));
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                logger.debug(requestId + " - userId: " + userId + " + - exception:" + CommonUtils.getError(e));
            }
        }
        return returnValue;
    }

    public double getWalletBalance(String requestId, String fleetId, String userId, String type, String currencyISO) {
        Connection cnnRead = null;
        Connection cnn = null;
        double balance = 0.0;
        try {
            cnnRead = getReadConnection(userId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            BigDecimal cashIn = contextRead.select(DSL.sum(QUP_WALLETTRANSACTION.CASHIN))
                    .from(QUP_WALLETTRANSACTION)
                    .where(QUP_WALLETTRANSACTION.FLEETID.eq(fleetId))
                    .and(QUP_WALLETTRANSACTION.USERID.eq(userId))
                    .and(QUP_WALLETTRANSACTION.TYPE.eq(type))
                    .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .fetchOne(0, BigDecimal.class);
            if (cashIn == null)
                cashIn = new BigDecimal("0.0");
            BigDecimal cashOut = contextRead.select(DSL.sum(QUP_WALLETTRANSACTION.CASHOUT))
                    .from(QUP_WALLETTRANSACTION)
                    .where(QUP_WALLETTRANSACTION.FLEETID.eq(fleetId))
                    .and(QUP_WALLETTRANSACTION.USERID.eq(userId))
                    .and(QUP_WALLETTRANSACTION.TYPE.eq(type))
                    .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .fetchOne(0, BigDecimal.class);
            if (cashOut == null)
                cashOut = new BigDecimal("0.0");

            QupWalletbalanceRecord record = contextRead.selectFrom(QUP_WALLETBALANCE)
                    .where(QUP_WALLETBALANCE.FLEETID.eq(fleetId))
                    .and(QUP_WALLETBALANCE.USERID.eq(userId))
                    .and(QUP_WALLETBALANCE.TYPE.eq(type))
                    .and(QUP_WALLETBALANCE.CURRENCYISO.eq(currencyISO))
                    .fetchOne();

            balance = cashIn.subtract(cashOut).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            double cacheBalance = 0.0;
            if (record != null) {
                cacheBalance = BigDecimal.valueOf(record.getBalance()).setScale(2, BigDecimal.ROUND_HALF_UP)
                        .doubleValue();
            } else {
                cnn = getConnection(userId);
                DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
                record = context
                        .insertInto(QUP_WALLETBALANCE,
                                QUP_WALLETBALANCE.FLEETID,
                                QUP_WALLETBALANCE.USERID,
                                QUP_WALLETBALANCE.TYPE,
                                QUP_WALLETBALANCE.CURRENCYISO,
                                QUP_WALLETBALANCE.BALANCE)
                        .values(fleetId,
                                userId,
                                type,
                                currencyISO,
                                balance)
                        .returning()
                        .fetchOne();
                balance = record.getBalance();
                context.close();
            }

            if (balance != cacheBalance) {
                Thread.sleep(500);
                // balance not match !!! query balance again
                cashIn = contextRead.select(DSL.sum(QUP_WALLETTRANSACTION.CASHIN))
                        .from(QUP_WALLETTRANSACTION)
                        .where(QUP_WALLETTRANSACTION.FLEETID.eq(fleetId))
                        .and(QUP_WALLETTRANSACTION.USERID.eq(userId))
                        .and(QUP_WALLETTRANSACTION.TYPE.eq(type))
                        .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                        .fetchOne(0, BigDecimal.class);
                if (cashIn == null)
                    cashIn = new BigDecimal("0.0");
                cashOut = contextRead.select(DSL.sum(QUP_WALLETTRANSACTION.CASHOUT))
                        .from(QUP_WALLETTRANSACTION)
                        .where(QUP_WALLETTRANSACTION.FLEETID.eq(fleetId))
                        .and(QUP_WALLETTRANSACTION.USERID.eq(userId))
                        .and(QUP_WALLETTRANSACTION.TYPE.eq(type))
                        .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                        .fetchOne(0, BigDecimal.class);
                if (cashOut == null)
                    cashOut = new BigDecimal("0.0");
                balance = cashIn.subtract(cashOut).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            }

            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
                if (cnn != null) {
                    cnn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return balance;
    }

    public List<Double> getWalletBalanceToDate(String userId, String type, String currencyISO, String date) {
        Connection cnnRead = null;
        double cashIn = 0.0;
        double cashOut = 0.0;
        try {
            cnnRead = getReadConnection(userId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            cashIn = contextRead.select(DSL.sum(QUP_WALLETTRANSACTION.CASHIN))
                    .from(QUP_WALLETTRANSACTION)
                    .where(QUP_WALLETTRANSACTION.USERID.eq(userId))
                    .and(QUP_WALLETTRANSACTION.TYPE.eq(type))
                    .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .and(QUP_WALLETTRANSACTION.CREATEDDATE.le(Timestamp.valueOf(date)))
                    .fetchOne(0, double.class);

            cashOut = contextRead.select(DSL.sum(QUP_WALLETTRANSACTION.CASHOUT))
                    .from(QUP_WALLETTRANSACTION)
                    .where(QUP_WALLETTRANSACTION.USERID.eq(userId))
                    .and(QUP_WALLETTRANSACTION.TYPE.eq(type))
                    .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .and(QUP_WALLETTRANSACTION.CREATEDDATE.le(Timestamp.valueOf(date)))
                    .fetchOne(0, double.class);

            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        List<Double> result = new ArrayList<>();
        result.add(cashIn);
        result.add(cashOut);
        result.add(cashIn - cashOut);
        return result;
    }

    public double getCashInMonth(String userId, String type, String currencyISO, String desc) {
        Connection cnnRead = null;
        double cashIn = 0.0;
        try {
            cnnRead = getReadConnection(userId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            List<QupWallettransactionRecord> list = contextRead.selectFrom(QUP_WALLETTRANSACTION)
                    .where(QUP_WALLETTRANSACTION.USERID.eq(userId))
                    .and(QUP_WALLETTRANSACTION.TYPE.eq(type))
                    .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .and(QUP_WALLETTRANSACTION.DESCRIPTION.eq(desc))
                    .orderBy(QUP_WALLETTRANSACTION.ID.desc())
                    .fetch();
            if (list.size() > 0) {
                QupWallettransactionRecord record = list.get(0);
                cashIn = record.getCashin();
                if (cashIn == 0.0) {
                    // already insert summary record with total = 0.0
                    // return default value to reduce insert again
                    cashIn = 1.0;
                }
            }
            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return cashIn;
    }

    public int deleteCashInMonth(String userId, String type, String currencyISO, String date) {
        Connection cnn = null;
        int result = 0;
        try {
            cnn = getConnection(userId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            result = context.delete(QUP_WALLETTRANSACTION)
                    .where(QUP_WALLETTRANSACTION.USERID.eq(userId))
                    .and(QUP_WALLETTRANSACTION.TYPE.eq(type))
                    .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .and(QUP_WALLETTRANSACTION.CREATEDDATE.le(Timestamp.valueOf(date)))
                    .execute();

            context.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public double getTransferredAmount(String userId, String currencyISO, String destination, String from, String to) {
        Connection cnnRead = null;
        double total = 0.0;
        try {
            cnnRead = getReadConnection(userId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            total = contextRead.select(DSL.sum(QUP_WALLETTRANSFER.AMOUNT))
                    .from(QUP_WALLETTRANSFER)
                    .where(QUP_WALLETTRANSFER.DRIVERID.eq(userId))
                    .and(QUP_WALLETTRANSFER.TRANSFERTYPE.eq("withdraw"))
                    .and(QUP_WALLETTRANSFER.CURRENCYISO.eq(currencyISO))
                    .and(QUP_WALLETTRANSFER.RECEIVERACCOUNT.eq(destination))
                    .and(QUP_WALLETTRANSFER.STATUS.ne(KeysUtil.WITHDRAWAL_REJECTED))
                    .and(QUP_WALLETTRANSFER.CREATEDDATE.between(Timestamp.valueOf(from), Timestamp.valueOf(to)))
                    .fetchOne(0, double.class);

            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return total;
    }

    public SettlementHistory saveSettlementHistory(SettlementHistoryEnt settlementHistoryEnt) {
        Connection cnn = null;
        QupSettlementhistoryRecord qupSettlementhistoryRecord = null;
        try {
            cnn = getConnection(settlementHistoryEnt.fleetId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            qupSettlementhistoryRecord = context.insertInto(QUP_SETTLEMENTHISTORY,
                    QUP_SETTLEMENTHISTORY.FLEETID,
                    QUP_SETTLEMENTHISTORY.DRIVERID,
                    QUP_SETTLEMENTHISTORY.DRIVERNAME,
                    QUP_SETTLEMENTHISTORY.TOTALTRANSACTION,
                    QUP_SETTLEMENTHISTORY.PAIDAMOUNT,
                    QUP_SETTLEMENTHISTORY.COMPANYID,
                    QUP_SETTLEMENTHISTORY.DATERANGEFROM,
                    QUP_SETTLEMENTHISTORY.DATERANGETO,
                    QUP_SETTLEMENTHISTORY.CURRENCYISO,
                    QUP_SETTLEMENTHISTORY.TOTALDISPATCH,
                    QUP_SETTLEMENTHISTORY.CREATEDDATE)
                    .values(settlementHistoryEnt.fleetId,
                            settlementHistoryEnt.driverId,
                            settlementHistoryEnt.driverName,
                            settlementHistoryEnt.totalTransaction,
                            settlementHistoryEnt.paidAmount,
                            settlementHistoryEnt.companyId,
                            Timestamp.valueOf(settlementHistoryEnt.dateRangeFrom),
                            Timestamp.valueOf(settlementHistoryEnt.dateRangeTo),
                            settlementHistoryEnt.currencyISO,
                            settlementHistoryEnt.totalDispatch,
                            Timestamp.valueOf(settlementHistoryEnt.createdDate))
                    .returning()
                    .fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (qupSettlementhistoryRecord != null) {
            SettlementHistory history = new SettlementHistory();
            history.id = qupSettlementhistoryRecord.getId();
            history.fleetId = qupSettlementhistoryRecord.getFleetid();
            history.driverId = qupSettlementhistoryRecord.getDriverid();
            history.driverName = qupSettlementhistoryRecord.getDrivername();
            history.totalTransaction = qupSettlementhistoryRecord.getTotaltransaction();
            history.paidAmount = qupSettlementhistoryRecord.getPaidamount();
            history.companyId = qupSettlementhistoryRecord.getCompanyid();
            history.dateRangeFrom = qupSettlementhistoryRecord.getDaterangefrom();
            history.dateRangeTo = qupSettlementhistoryRecord.getDaterangeto();
            history.currencyISO = qupSettlementhistoryRecord.getCurrencyiso();
            history.totalDispatch = qupSettlementhistoryRecord.getTotaldispatch();
            history.createdDate = qupSettlementhistoryRecord.getCreateddate();
            return history;
        }
        return null;
    }

    public int updatePaidToDriver(ReportSettlementEnt settlementEnt) {
        Connection cnn = null;
        try {
            cnn = getConnection("");
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            int update = context.update(QUP_TICKET)
                    .set(QUP_TICKET.PAIDTODRIVER, 1)
                    .where(QUP_TICKET.BOOKID.in(settlementEnt.books))
                    .execute();
            return update;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public JSONObject reportSettlementEnt(ReportSettlementEnt settlementEnt) {
        Connection cnnRead = null;
        String result = "";
        String summary = "";
        try {
            cnnRead = getReadConnection(settlementEnt.fleetId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            SelectQuery query = contextRead.selectQuery();
            SelectQuery querySummary = contextRead.selectQuery();
            query.addSelect(QUP_TICKET.DRIVERID, QUP_TICKET.DRIVERNAME, QUP_TICKET.PAIDTODRIVER, QUP_TICKET.COMPANYID,
                    groupConcat(QUP_TICKET.BOOKID).as("books"),
                    sum(when(QUP_TICKET.PRICINGTYPE.eq(0), QUP_TICKET.NETEARNING)
                            .otherwise(QUP_TICKET.PSGFLEETCOMMAMOUNT))
                            .plus(sum(when(QUP_TICKET.PRICINGTYPE.eq(0), ifnull(QUP_TICKETEXTRA.DOUBLE1, 0.0))
                                    .otherwise(0.0)))
                            .as("total"),
                    count(QUP_TICKET.ID).as("size"));
            query.addFrom(QUP_TICKET.leftJoin(QUP_TICKETEXTRA).on(QUP_TICKET.BOOKID.eq(QUP_TICKETEXTRA.BOOKID)));
            query.addConditions(QUP_TICKET.DRIVERID.isNotNull(), QUP_TICKET.DRIVERID.notEqual(""),
                    QUP_TICKET.FLEETID.eq(settlementEnt.fleetId), QUP_TICKET.PAIDTODRIVER.eq(-1),
                    QUP_TICKET.STATUS.eq(1),
                    QUP_TICKET.CURRENCYISO.eq(settlementEnt.currency),
                    QUP_TICKET.COMPLETEDTIME.between(Timestamp.valueOf(settlementEnt.fromDate),
                            Timestamp.valueOf(settlementEnt.toDate)));

            querySummary.addSelect(countDistinct(QUP_TICKET.DRIVERID).as("totalDriver"),
                    sum(when(QUP_TICKET.PRICINGTYPE.eq(0), QUP_TICKET.NETEARNING)
                            .otherwise(QUP_TICKET.PSGFLEETCOMMAMOUNT))
                            .plus(sum(when(QUP_TICKET.PRICINGTYPE.eq(0), ifnull(QUP_TICKETEXTRA.DOUBLE1, 0.0))
                                    .otherwise(0.0)))
                            .as("totalUnsettledAmount"),
                    count(QUP_TICKET.ID).as("totalUnsettledTransactions"));
            querySummary.addFrom(QUP_TICKET.leftJoin(QUP_TICKETEXTRA).on(QUP_TICKET.BOOKID.eq(QUP_TICKETEXTRA.BOOKID)));
            querySummary.addConditions(QUP_TICKET.DRIVERID.isNotNull(), QUP_TICKET.DRIVERID.notEqual(""),
                    QUP_TICKET.FLEETID.eq(settlementEnt.fleetId), QUP_TICKET.PAIDTODRIVER.eq(-1),
                    QUP_TICKET.STATUS.eq(1),
                    QUP_TICKET.CURRENCYISO.eq(settlementEnt.currency),
                    QUP_TICKET.COMPLETEDTIME.between(Timestamp.valueOf(settlementEnt.fromDate),
                            Timestamp.valueOf(settlementEnt.toDate)));
            if (settlementEnt.bookingService.equals("localBooking")) {
                query.addConditions(QUP_TICKET.FLEETID.eq(QUP_TICKET.PSGFLEETID));
                querySummary.addConditions(QUP_TICKET.FLEETID.eq(QUP_TICKET.PSGFLEETID));
            } else {
                query.addConditions(QUP_TICKET.FLEETID.ne(QUP_TICKET.PSGFLEETID));
                querySummary.addConditions(QUP_TICKET.FLEETID.ne(QUP_TICKET.PSGFLEETID));
            }
            if (!settlementEnt.companyId.equals("all")) {
                query.addConditions(QUP_TICKET.COMPANYID.eq(settlementEnt.companyId));
                querySummary.addConditions(QUP_TICKET.COMPANYID.eq(settlementEnt.companyId));
            }
            if (!settlementEnt.driverId.equals("all")) {
                query.addConditions(QUP_TICKET.DRIVERID.eq(settlementEnt.driverId));
                querySummary.addConditions(QUP_TICKET.DRIVERID.eq(settlementEnt.driverId));
            }
            if (settlementEnt.paymentMethod != null && !settlementEnt.paymentMethod.isEmpty()
                    && settlementEnt.paymentMethod.indexOf("all") == -1) {
                query.addConditions(QUP_TICKET.TRANSACTIONSTATUS.in(settlementEnt.paymentMethod));
                querySummary.addConditions(QUP_TICKET.TRANSACTIONSTATUS.in(settlementEnt.paymentMethod));
            }
            query.addGroupBy(QUP_TICKET.DRIVERID);
            query.addOrderBy(QUP_TICKET.DRIVERNAME.asc());
            query.addLimit(settlementEnt.size);
            query.addOffset(settlementEnt.from * settlementEnt.size);
            result = query.fetch().formatJSON();
            summary = querySummary.fetch().formatJSON();
            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        JSONObject obj = new JSONObject();
        obj.put("list", result);
        obj.put("summary", summary);
        obj.put("from", settlementEnt.from);
        obj.put("size", settlementEnt.size);
        return obj;
    }

    public String getReportData(String fleetId, String type, String from, String to) {
        Connection cnnRead = null;
        String result = "";
        try {
            cnnRead = getReadConnection(fleetId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            if (type.equals("driver")) {
                result = contextRead.selectDistinct(QUP_TICKET.DRIVERNAME, QUP_TICKET.DRIVERID)
                        .from(QUP_TICKET)
                        .where(QUP_TICKET.FLEETID.eq(fleetId))
                        .and(QUP_TICKET.COMPLETEDTIME.between(Timestamp.valueOf(from), Timestamp.valueOf(to)))
                        .and(QUP_TICKET.DRIVERNAME.ne(""))
                        .and(QUP_TICKET.DRIVERID.ne(""))
                        .and(QUP_TICKET.TRANSACTIONSTATUS.notIn(KeysUtil.INCOMPLETE_PAYMENT))
                        .and(QUP_TICKET.PRICINGTYPE.eq(0))
                        .orderBy(QUP_TICKET.ID.desc())
                        .fetch()
                        .formatJSON();
            } else if (type.equals("vehicle")) {
                result = contextRead.selectDistinct(QUP_TICKET.PLATENUMBER)
                        .from(QUP_TICKET)
                        .where(QUP_TICKET.FLEETID.eq(fleetId))
                        .and(QUP_TICKET.COMPLETEDTIME.between(Timestamp.valueOf(from), Timestamp.valueOf(to)))
                        .and(QUP_TICKET.PLATENUMBER.ne(""))
                        .and(QUP_TICKET.VEHICLEID.ne(""))
                        .and(QUP_TICKET.TRANSACTIONSTATUS.notIn(KeysUtil.INCOMPLETE_PAYMENT))
                        .and(QUP_TICKET.PRICINGTYPE.eq(0))
                        .orderBy(QUP_TICKET.ID.desc())
                        .fetch()
                        .formatJSON();
            }
            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public String getTickets(int i, String fleetId) {
        Connection cnnRead = null;
        String result = "";
        try {
            cnnRead = getReadConnection(fleetId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            result = contextRead.select(QUP_TICKET.BOOKID, QUP_TICKETEXTEND.VARCHAR4, QUP_TICKETEXTEND.VARCHAR7)
                    .from(QUP_TICKET, QUP_TICKETEXTEND)
                    .where(QUP_TICKET.FLEETID.eq(fleetId))
                    .and(QUP_TICKET.BOOKID.eq(QUP_TICKETEXTEND.BOOKID))
                    .orderBy(QUP_TICKET.ID.desc())
                    .limit(KeysUtil.SIZE)
                    .offset(i * KeysUtil.SIZE)
                    .fetch()
                    .formatJSON();
            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public int countTotalTicket(String fleetId, String from, String to) {
        Connection cnnRead = null;
        int size = 0;
        try {
            cnnRead = getReadConnection(fleetId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));

            size = contextRead.select(DSL.count())
                    .from(QUP_TICKET)
                    .where(QUP_TICKET.FLEETID.eq(fleetId))
                    .and(QUP_TICKET.COMPLETEDTIME.between(Timestamp.valueOf(from), Timestamp.valueOf(to)))
                    .and(QUP_TICKET.TRANSACTIONSTATUS.notIn(KeysUtil.INCOMPLETE_PAYMENT))
                    .and(QUP_TICKET.PRICINGTYPE.eq(0))
                    .fetchOne(0, int.class);
            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return size;
    }

    public double getTotalReport(String fleetId, String from, String to, int skip) {
        Connection cnnRead = null;
        double all = 0.0;
        double free = 0.0;
        try {
            cnnRead = getReadConnection(fleetId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            all = contextRead.select(DSL.sum(QUP_TICKET.FARE))
                    .from(QUP_TICKET)
                    .where(QUP_TICKET.FLEETID.eq(fleetId))
                    .and(QUP_TICKET.COMPLETEDTIME.between(Timestamp.valueOf(from), Timestamp.valueOf(to)))
                    /* .and(QUP_TICKET.TRANSACTIONSTATUS.in(KeysUtil.COMPLETED_STATUS)) */
                    .and(QUP_TICKET.PRICINGTYPE.eq(0))
                    .orderBy(QUP_TICKET.ID.asc())
                    .fetchOne(0, double.class);

            Table<?> freeTable = contextRead.select(QUP_TICKET.FARE.as("fare"))
                    .from(QUP_TICKET)
                    .where(QUP_TICKET.FLEETID.eq(fleetId))
                    .and(QUP_TICKET.COMPLETEDTIME.between(Timestamp.valueOf(from), Timestamp.valueOf(to)))
                    /* .and(QUP_TICKET.TRANSACTIONSTATUS.in(KeysUtil.COMPLETED_STATUS)) */
                    .and(QUP_TICKET.PRICINGTYPE.eq(0))
                    .orderBy(QUP_TICKET.ID.asc())
                    .limit(skip)
                    .asTable("freeTable");

            free = contextRead.select(DSL.sum(freeTable.field("fare").cast(double.class)))
                    .from(freeTable)
                    .fetchOne(0, double.class);

            System.out.println("all = " + all);
            System.out.println("free = " + free);

            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return all - free;
    }

    public int updateTipAfterPayment(String bookId, JSONObject tipData) {
        Connection cnn = null;
        try {
            cnn = getConnection(bookId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));

            context.update(QUP_TICKETEXTRA)
                    .set(QUP_TICKETEXTRA.DOUBLE1, Double.valueOf(tipData.get("driverTipAfterCompleted").toString()))
                    .where(QUP_TICKETEXTRA.BOOKID.eq(bookId))
                    .execute();

            return context.update(QUP_TICKETEXTEND)
                    .set(QUP_TICKETEXTEND.VARCHAR7, tipData.toJSONString())
                    .where(QUP_TICKETEXTEND.BOOKID.eq(bookId))
                    .execute();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public ObjResponse getOutstandingTicket(String fleetId, String customerId, String currencyISO) {
        ObjResponse response = new ObjResponse();
        Connection cnnRead = null;
        try {
            Condition pending = QUP_TICKETEXTEND.BOOLEAN2.eq(1);
            Condition partial = QUP_TICKET.TOTAL.gt(QUP_TICKET.TOTALCHARGED);
            Condition combine = pending.or(partial);

            cnnRead = getReadConnection(customerId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            Result<?> join = contextRead.select(QUP_TICKET.BOOKID, QUP_TICKET.TOTAL, QUP_TICKET.TOTALCHARGED,
                    QUP_TICKET.COMPLETEDTIME, QUP_TICKET.CURRENCYISO, QUP_TICKET.TIMEZONEDESTINATION,
                    QUP_TICKETEXTEND.BOOLEAN2)
                    .from(QUP_TICKET)
                    .innerJoin(QUP_TICKETEXTEND)
                    .on(QUP_TICKET.BOOKID.eq(QUP_TICKETEXTEND.BOOKID))
                    .and(QUP_TICKET.FLEETID.eq(fleetId))
                    .and(QUP_TICKET.CUSTOMERID.eq(customerId))
                    .and(QUP_TICKET.CURRENCYISO.eq(currencyISO))
                    .and(QUP_TICKET.STATUS.eq(1))
                    .and(QUP_TICKET.TRANSACTIONSTATUS.ne(KeysUtil.PAYMENT_DIRECTBILLING))
                    .and(QUP_TICKET.PAYMENTTYPE.ne(5))
                    .and(combine)
                    .fetch();
            contextRead.close();
            List<Map> listData = new ArrayList<>();
            for (Record r : join) {
                QupTicketRecord ticket = r.into(QUP_TICKET);
                QupTicketextendRecord extend = r.into(QUP_TICKETEXTEND);
                Map<String, Object> mapData = new HashMap<>();
                mapData.put("bookId", ticket.getBookid());
                mapData.put("total", ticket.getTotal());
                mapData.put("totalCharged", ticket.getTotalcharged());
                mapData.put("completedTime", ticket.getCompletedtime());
                mapData.put("timeZoneDestination", ticket.getTimezonedestination());
                mapData.put("currencyISO", ticket.getCurrencyiso());
                mapData.put("isPending", extend.getBoolean2());
                listData.add(mapData);
            }
            response.returnCode = 200;
            response.response = listData;
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        response.returnCode = 407;
        return response;
    }

    public int updateOutstanding(String bookId, double total) {
        Connection cnn = null;
        try {
            cnn = getConnection(bookId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));

            context.update(QUP_TICKET)
                    .set(QUP_TICKET.TOTALCHARGED, total)
                    .where(QUP_TICKET.BOOKID.eq(bookId))
                    .execute();

            context.update(QUP_TICKETEXTEND)
                    .set(QUP_TICKETEXTEND.BOOLEAN2, 0)
                    .where(QUP_TICKETEXTEND.BOOKID.eq(bookId))
                    .execute();

            return 1;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    private Settings getSettingMigrate(String mode) {
        try {
            if ("read".equals(mode)) {
                return new Settings()
                        .withRenderMapping(new RenderMapping()
                                .withSchemata(
                                        new MappedSchema().withInput(SCHEMA)
                                                .withOutput("qupworld_beta")));
            } else {
                return new Settings()
                        .withRenderMapping(new RenderMapping()
                                .withSchemata(
                                        new MappedSchema().withInput(SCHEMA)
                                                .withOutput("qupworld_empty")));
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public long addIntercityTrip(final IntercityTripEnt intercityTripEnt) {
        Connection cnn = null;
        Record record = null;
        try {
            cnn = getConnection(intercityTripEnt.bookIds);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            record = context.insertInto(QUP_INTERCITYTRIP,
                    QUP_INTERCITYTRIP.TRIPID,
                    QUP_INTERCITYTRIP.FLEETID,
                    QUP_INTERCITYTRIP.NOBOOKINGS,
                    QUP_INTERCITYTRIP.BOOKIDS,
                    QUP_INTERCITYTRIP.ROUTENAME,
                    QUP_INTERCITYTRIP.PICKUPTIME,
                    QUP_INTERCITYTRIP.STATUS,
                    QUP_INTERCITYTRIP.FARE,
                    QUP_INTERCITYTRIP.CURRENCYISO,
                    QUP_INTERCITYTRIP.CURRENCYSYMBOL,
                    QUP_INTERCITYTRIP.CREATEDDATE)
                    .values(intercityTripEnt.tripId,
                            intercityTripEnt.fleetId,
                            intercityTripEnt.noBookings,
                            intercityTripEnt.bookIds,
                            intercityTripEnt.routeName,
                            intercityTripEnt.pickUpTime,
                            intercityTripEnt.status,
                            intercityTripEnt.fare,
                            intercityTripEnt.currencyISO,
                            intercityTripEnt.currencySymbol,
                            intercityTripEnt.createdDate)
                    .returning()
                    .fetchOne();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (record != null)
            return record.getValue(QUP_INTERCITYTRIP.ID);
        return 0;
    }

    public IntercityTripEnt getIntercityTripEnt(String tripId) {
        Connection cnnRead = null;
        QupIntercitytripRecord qupIntercitytripRecord = null;
        try {
            cnnRead = getReadConnection(tripId);

            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            List<QupIntercitytripRecord> list = contextRead.selectFrom(QUP_INTERCITYTRIP)
                    .where(QUP_INTERCITYTRIP.TRIPID.eq(tripId))
                    .fetch();
            if (list.size() > 0) {
                qupIntercitytripRecord = list.get(0);

                IntercityTripEnt tripEnt = new IntercityTripEnt();
                tripEnt.tripId = qupIntercitytripRecord.getTripid();
                tripEnt.fleetId = qupIntercitytripRecord.getFleetid();
                tripEnt.noBookings = qupIntercitytripRecord.getNobookings();
                tripEnt.bookIds = qupIntercitytripRecord.getBookids();
                tripEnt.routeName = qupIntercitytripRecord.getRoutename();
                tripEnt.pickUpTime = qupIntercitytripRecord.getPickuptime();
                tripEnt.status = qupIntercitytripRecord.getStatus();
                tripEnt.fare = qupIntercitytripRecord.getFare();
                tripEnt.currencyISO = qupIntercitytripRecord.getCurrencyiso();
                tripEnt.currencySymbol = qupIntercitytripRecord.getCurrencysymbol();
                tripEnt.createdDate = qupIntercitytripRecord.getCreateddate();

                return tripEnt;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public long addReferralTransaction(ReferralTransaction referralTransaction) {
        Connection cnn = null;
        Record record = null;
        try {
            cnn = getConnection(referralTransaction.bookId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            record = context.insertInto(QUP_REFERRALTRANSACTION,
                    QUP_WALLETTRANSACTION.FLEETID,
                    QUP_REFERRALTRANSACTION.USERID,
                    QUP_REFERRALTRANSACTION.BOOKID,
                    QUP_REFERRALTRANSACTION.TRANSACTIONSTATUS,
                    QUP_REFERRALTRANSACTION.REFERRALCODE,
                    QUP_REFERRALTRANSACTION.CURRENCYISO,
                    QUP_REFERRALTRANSACTION.CASHIN,
                    QUP_REFERRALTRANSACTION.CASHOUT,
                    QUP_REFERRALTRANSACTION.BALANCE,
                    QUP_REFERRALTRANSACTION.TRANSFERRED,
                    QUP_REFERRALTRANSACTION.CREATEDTIME)
                    .values(referralTransaction.fleetId,
                            referralTransaction.userId,
                            referralTransaction.bookId,
                            referralTransaction.transactionStatus,
                            referralTransaction.referralCode,
                            referralTransaction.currencyISO,
                            referralTransaction.cashIn,
                            referralTransaction.cashOut,
                            referralTransaction.balance,
                            referralTransaction.transferred,
                            referralTransaction.createdTime)
                    .returning()
                    .fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (record != null)
            return record.getValue(QUP_REFERRALTRANSACTION.ID);
        return 0;
    }

    public double getReferralEarning(String fleetId, String driverId, String fromDate, String toDate,
            String currencyISO) {
        Connection cnnRead = null;
        double cashIn = 0.0;
        double cashOut = 0.0;
        try {
            cnnRead = getReadConnection(driverId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            cashIn = contextRead.select(DSL.sum(QUP_REFERRALTRANSACTION.CASHIN))
                    .from(QUP_REFERRALTRANSACTION)
                    .where(QUP_REFERRALTRANSACTION.FLEETID.eq(fleetId))
                    .and(QUP_REFERRALTRANSACTION.USERID.eq(driverId))
                    .and(QUP_REFERRALTRANSACTION.CREATEDTIME.greaterOrEqual(Timestamp.valueOf(fromDate)))
                    .and(QUP_REFERRALTRANSACTION.CREATEDTIME.lessOrEqual(Timestamp.valueOf(toDate)))
                    .and(QUP_REFERRALTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .and(QUP_REFERRALTRANSACTION.TRANSFERRED.eq(0))
                    .fetchOne(0, double.class);

            cashOut = contextRead.select(DSL.sum(QUP_REFERRALTRANSACTION.CASHOUT))
                    .from(QUP_REFERRALTRANSACTION)
                    .where(QUP_REFERRALTRANSACTION.FLEETID.eq(fleetId))
                    .and(QUP_REFERRALTRANSACTION.USERID.eq(driverId))
                    .and(QUP_REFERRALTRANSACTION.CREATEDTIME.greaterOrEqual(Timestamp.valueOf(fromDate)))
                    .and(QUP_REFERRALTRANSACTION.CREATEDTIME.lessOrEqual(Timestamp.valueOf(toDate)))
                    .and(QUP_REFERRALTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .and(QUP_REFERRALTRANSACTION.TRANSFERRED.eq(0))
                    .fetchOne(0, double.class);

            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return cashIn - cashOut;
    }

    public List<ReferralTransaction> getReferralTransactions(int i, int size) {
        Connection cnnRead = null;
        List<ReferralTransaction> all = new ArrayList<>();
        try {
            cnnRead = getReadConnection("");

            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            List<QupReferraltransactionRecord> list = contextRead.selectFrom(QUP_REFERRALTRANSACTION)
                    .orderBy(QUP_REFERRALTRANSACTION.ID.desc())
                    .limit(size)
                    .offset(i * size)
                    .fetch();

            for (QupReferraltransactionRecord record : list) {
                ReferralTransaction referralTransaction = new ReferralTransaction();
                referralTransaction.fleetId = record.getFleetid();
                referralTransaction.userId = record.getUserid();
                referralTransaction.bookId = record.getBookid();
                referralTransaction.transactionStatus = record.getTransactionstatus();
                referralTransaction.currencyISO = record.getCurrencyiso();
                referralTransaction.cashIn = record.getCashin();
                referralTransaction.cashOut = record.getCashout();
                referralTransaction.balance = record.getBalance();
                referralTransaction.transferred = record.getTransferred();
                referralTransaction.createdTime = record.getCreatedtime();

                all.add(referralTransaction);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return all;
    }

    public long addPayoutHistory(PayoutHistoryEnt payoutHistoryEnt) {
        Connection cnn = null;
        Record record = null;
        try {
            JSONObject objData = new JSONObject();
            String payoutTo = payoutHistoryEnt.payTo;
            if (payoutTo.equals("")) {
                objData.put("IFSCCode", payoutHistoryEnt.IFSCCode);
            } else {
                objData.put("isBankAccountOwner", payoutHistoryEnt.isBankAccountOwner);
                objData.put("beneficiaryIDIC", payoutHistoryEnt.beneficiaryIDIC);
                objData.put("bankRelationship", payoutHistoryEnt.bankRelationship);
                objData.put("relationshipOtherName", payoutHistoryEnt.relationshipOtherName);
                objData.put("IFSCCode", payoutHistoryEnt.IFSCCode);
            }
            cnn = getConnection(payoutHistoryEnt.driverId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            record = context.insertInto(QUP_PAYOUTHISTORY,
                    QUP_PAYOUTHISTORY.FLEETID,
                    QUP_PAYOUTHISTORY.DRIVERID,
                    QUP_PAYOUTHISTORY.DRIVERNAME,
                    QUP_PAYOUTHISTORY.PHONE,
                    QUP_PAYOUTHISTORY.VARCHAR0,
                    QUP_PAYOUTHISTORY.COMPANYID,
                    QUP_PAYOUTHISTORY.COMPANYNAME,
                    QUP_PAYOUTHISTORY.DRIVERTYPE,
                    QUP_PAYOUTHISTORY.DRIVERNUMBER,
                    QUP_PAYOUTHISTORY.DRIVERNUMBERTYPE,
                    QUP_PAYOUTHISTORY.OPERATORID,
                    QUP_PAYOUTHISTORY.OPERATORNAME,
                    QUP_PAYOUTHISTORY.TRANSACTIONID,
                    QUP_PAYOUTHISTORY.TRANSACTIONTYPE,
                    QUP_PAYOUTHISTORY.BOPINDICATOR,
                    QUP_PAYOUTHISTORY.PAIDAMOUNT,
                    QUP_PAYOUTHISTORY.NEWBALANCE,
                    QUP_PAYOUTHISTORY.CURRENCYISO,
                    QUP_PAYOUTHISTORY.BANKACCOUNTHOLDER,
                    QUP_PAYOUTHISTORY.ACCOUNTNUMBER,
                    QUP_PAYOUTHISTORY.BANKNAME,
                    QUP_PAYOUTHISTORY.PAYOUTID,
                    QUP_PAYOUTHISTORY.PAYOUTDATE,
                    QUP_PAYOUTHISTORY.CREATEDDATE,
                    QUP_PAYOUTHISTORY.VARCHAR0,
                    QUP_PAYOUTHISTORY.VARCHAR1,
                    QUP_PAYOUTHISTORY.VARCHAR2)
                    .values(payoutHistoryEnt.fleetId,
                            payoutHistoryEnt.driverId,
                            payoutHistoryEnt.driverName,
                            payoutHistoryEnt.phone,
                            payoutHistoryEnt.email,
                            payoutHistoryEnt.companyId,
                            payoutHistoryEnt.companyName,
                            payoutHistoryEnt.driverType,
                            payoutHistoryEnt.driverNumber,
                            payoutHistoryEnt.driverNumberType,
                            payoutHistoryEnt.operatorId,
                            payoutHistoryEnt.operatorName,
                            payoutHistoryEnt.transactionId,
                            payoutHistoryEnt.transactionType,
                            payoutHistoryEnt.bopIndicator,
                            payoutHistoryEnt.paidAmount,
                            payoutHistoryEnt.newBalance,
                            payoutHistoryEnt.currencyISO,
                            payoutHistoryEnt.bankAccountHolder,
                            payoutHistoryEnt.accountNumber,
                            payoutHistoryEnt.bankName,
                            payoutHistoryEnt.payoutId,
                            payoutHistoryEnt.payoutDate,
                            payoutHistoryEnt.createdDate,
                            objData.toJSONString(),
                            payoutHistoryEnt.payTo,
                            payoutHistoryEnt.merchantId)
                    .returning()
                    .fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (record != null)
            return record.getValue(QUP_PAYOUTHISTORY.ID);
        return 0;
    }

    public PayoutHistoryEnt getPayoutHistory(String fleetId, String driverId) {
        Connection cnnRead = null;
        PayoutHistoryEnt payoutHistoryEnt = null;
        try {
            cnnRead = getReadConnection(driverId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            List<QupPayouthistoryRecord> list = contextRead.selectFrom(QUP_PAYOUTHISTORY)
                    .where(QUP_PAYOUTHISTORY.FLEETID.eq(fleetId))
                    .and(QUP_PAYOUTHISTORY.DRIVERID.eq(driverId))
                    .orderBy(QUP_PAYOUTHISTORY.ID.desc())
                    .limit(1)
                    .fetch();

            for (QupPayouthistoryRecord record : list) {
                payoutHistoryEnt = new PayoutHistoryEnt();
                payoutHistoryEnt.fleetId = record.getFleetid();
                payoutHistoryEnt.driverId = record.getDriverid();
                payoutHistoryEnt.driverName = record.getDrivername();
                payoutHistoryEnt.phone = record.getPhone();
                payoutHistoryEnt.companyId = record.getCompanyid();
                payoutHistoryEnt.companyName = record.getCompanyname();
                payoutHistoryEnt.driverType = record.getDrivertype();
                payoutHistoryEnt.operatorId = record.getOperatorid();
                payoutHistoryEnt.operatorName = record.getOperatorname();
                payoutHistoryEnt.paidAmount = record.getPaidamount();
                payoutHistoryEnt.currencyISO = record.getCurrencyiso();
                payoutHistoryEnt.payoutId = record.getPayoutid();
                payoutHistoryEnt.payoutDate = record.getPayoutdate();
                payoutHistoryEnt.createdDate = record.getCreateddate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return payoutHistoryEnt;
    }

    public Timestamp getLastPayoutTime(String fleetId, String driverId) {
        Connection cnnRead = null;
        Timestamp lastPayoutTime = null;
        try {
            cnnRead = getReadConnection(driverId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            List<QupPayouthistoryRecord> list = contextRead.selectFrom(QUP_PAYOUTHISTORY)
                    .where(QUP_PAYOUTHISTORY.FLEETID.eq(fleetId))
                    .and(QUP_PAYOUTHISTORY.DRIVERID.eq(driverId))
                    .orderBy(QUP_PAYOUTHISTORY.ID.desc())
                    .limit(1)
                    .fetch();

            for (QupPayouthistoryRecord record : list) {
                lastPayoutTime = record.getPayoutdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return lastPayoutTime;
    }

    public double getTransferredAmount(String userId, String currencyISO, String fromDate, String toDate) {
        Connection cnnRead = null;
        double cashOut = 0.0;
        try {
            cnnRead = getReadConnection(userId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            if (fromDate.isEmpty()) {
                cashOut = contextRead.select(DSL.sum(QUP_WALLETTRANSFER.AMOUNT))
                        .from(QUP_WALLETTRANSFER)
                        .where(QUP_WALLETTRANSFER.DRIVERID.eq(userId))
                        .and(QUP_WALLETTRANSFER.TRANSFERTYPE.eq("withdraw"))
                        .and(QUP_WALLETTRANSFER.CURRENCYISO.eq(currencyISO))
                        .and(QUP_WALLETTRANSFER.RECEIVERACCOUNT.eq("creditWallet"))
                        .and(QUP_WALLETTRANSFER.STATUS.eq("approved"))
                        .and(QUP_WALLETTRANSFER.CREATEDDATE.lessOrEqual(Timestamp.valueOf(toDate)))
                        .fetchOne(0, double.class);
            } else {
                cashOut = contextRead.select(DSL.sum(QUP_WALLETTRANSFER.AMOUNT))
                        .from(QUP_WALLETTRANSFER)
                        .where(QUP_WALLETTRANSFER.DRIVERID.eq(userId))
                        .and(QUP_WALLETTRANSFER.TRANSFERTYPE.eq("withdraw"))
                        .and(QUP_WALLETTRANSFER.CURRENCYISO.eq(currencyISO))
                        .and(QUP_WALLETTRANSFER.RECEIVERACCOUNT.eq("creditWallet"))
                        .and(QUP_WALLETTRANSFER.STATUS.eq("approved"))
                        .and(QUP_WALLETTRANSFER.CREATEDDATE.greaterThan(Timestamp.valueOf(fromDate)))
                        .and(QUP_WALLETTRANSFER.CREATEDDATE.lessOrEqual(Timestamp.valueOf(toDate)))
                        .fetchOne(0, double.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return cashOut;
    }

    public double getTotalBalance(String fleetId, List<String> driverId, String type, String currencyISO) {
        Connection cnnRead = null;
        double cashIn = 0.0;
        double cashOut = 0.0;
        try {
            cnnRead = getReadConnection(fleetId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            System.out.println("===== start query at " + Calendar.getInstance().getTime());

            cashIn = contextRead.select(DSL.sum(QUP_WALLETTRANSACTION.CASHIN))
                    .from(QUP_WALLETTRANSACTION)
                    .where(QUP_WALLETTRANSACTION.FLEETID.eq(fleetId))
                    .and(QUP_WALLETTRANSACTION.USERID.in(driverId))
                    .and(QUP_WALLETTRANSACTION.TYPE.eq(type))
                    .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .fetchOne(0, double.class);

            cashOut = contextRead.select(DSL.sum(QUP_WALLETTRANSACTION.CASHOUT))
                    .from(QUP_WALLETTRANSACTION)
                    .where(QUP_WALLETTRANSACTION.FLEETID.eq(fleetId))
                    .and(QUP_WALLETTRANSACTION.USERID.in(driverId))
                    .and(QUP_WALLETTRANSACTION.TYPE.in(type))
                    .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .fetchOne(0, double.class);

            contextRead.close();
            System.out.println("===== end query at " + Calendar.getInstance().getTime());
            return cashIn - cashOut;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return cashIn - cashOut;
    }

    public long getIdWalletTransaction(String driverId, String type, String currencyISO, String transferType,
            String transactionId) {
        Connection cnnRead = null;
        try {
            cnnRead = getReadConnection(driverId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            QupWallettransactionRecord record = contextRead.selectFrom(QUP_WALLETTRANSACTION)
                    .where(QUP_WALLETTRANSACTION.USERID.in(driverId))
                    .and(QUP_WALLETTRANSACTION.TYPE.eq(type))
                    .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .and(QUP_WALLETTRANSACTION.DESCRIPTION.eq(transferType))
                    .and(QUP_WALLETTRANSACTION.TRANSACTIONID.eq(transactionId))
                    .orderBy(QUP_WALLETTRANSACTION.ID.desc())
                    .fetchOne();
            return record.getId();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public List<Double> getUsageWalletBalance(String userId, String currencyISO) {
        Connection cnnRead = null;
        double cashIn = 0.0;
        double cashOut = 0.0;
        double currentBalance = 0.0;
        try {
            cnnRead = getReadConnection(userId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));

            Condition topUpMOLPay = QUP_WALLETTRANSACTION.TRANSACTIONID.like("topUpMOLPay%");
            Condition cashWallet = QUP_WALLETTRANSACTION.TRANSACTIONID.like("cashWallet%");
            Condition combine = topUpMOLPay.or(cashWallet);

            cashIn = contextRead.select(DSL.sum(QUP_WALLETTRANSACTION.CASHIN))
                    .from(QUP_WALLETTRANSACTION)
                    .where(QUP_WALLETTRANSACTION.USERID.eq(userId))
                    .and(QUP_WALLETTRANSACTION.TYPE.eq("credit"))
                    .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .and(combine)
                    .fetchOne(0, double.class);

            cashOut = contextRead.select(DSL.sum(QUP_WALLETTRANSACTION.CASHOUT))
                    .from(QUP_WALLETTRANSACTION)
                    .where(QUP_WALLETTRANSACTION.USERID.eq(userId))
                    .and(QUP_WALLETTRANSACTION.TYPE.eq("credit"))
                    .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .and(QUP_WALLETTRANSACTION.TRANSACTIONID.like("bookingEarning%"))
                    .fetchOne(0, double.class);

            double currentCashIn = contextRead.select(DSL.sum(QUP_WALLETTRANSACTION.CASHIN))
                    .from(QUP_WALLETTRANSACTION)
                    .where(QUP_WALLETTRANSACTION.USERID.eq(userId))
                    .and(QUP_WALLETTRANSACTION.TYPE.eq("credit"))
                    .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .fetchOne(0, double.class);

            double currentCashOut = contextRead.select(DSL.sum(QUP_WALLETTRANSACTION.CASHOUT))
                    .from(QUP_WALLETTRANSACTION)
                    .where(QUP_WALLETTRANSACTION.USERID.eq(userId))
                    .and(QUP_WALLETTRANSACTION.TYPE.eq("credit"))
                    .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .fetchOne(0, double.class);

            currentBalance = currentCashIn - currentCashOut;
            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        List<Double> list = new ArrayList<>();
        list.add(cashIn);
        list.add(cashOut);
        list.add(currentBalance);
        return list;
    }

    public long addMerchantTransaction(MerchantTransactionEnt merchantTransactionEnt) {
        Connection cnn = null;
        Record record = null;
        try {
            cnn = getConnection(merchantTransactionEnt.merchantId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            record = context.insertInto(QUP_MERCHANTTRANSACTION,
                    QUP_MERCHANTTRANSACTION.FLEETID,
                    QUP_MERCHANTTRANSACTION.MERCHANTID,
                    QUP_MERCHANTTRANSACTION.TYPE,
                    QUP_MERCHANTTRANSACTION.TRANSACTIONID,
                    QUP_MERCHANTTRANSACTION.PENDINGAMOUNT,
                    QUP_MERCHANTTRANSACTION.CURRENCYISO,
                    QUP_MERCHANTTRANSACTION.CREATEDDATE)
                    .values(merchantTransactionEnt.fleetId,
                            merchantTransactionEnt.merchantId,
                            merchantTransactionEnt.type,
                            merchantTransactionEnt.transactionId,
                            merchantTransactionEnt.pendingAmount,
                            merchantTransactionEnt.currencyISO,
                            merchantTransactionEnt.createdDate)
                    .returning()
                    .fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (record != null)
            return record.getValue(QUP_MERCHANTTRANSACTION.ID);
        return 0;
    }

    public double getPendingAmount(String fleetId, List<String> merchantId, String currencyISO, Timestamp payoutDate) {
        Connection cnnRead = null;
        try {
            cnnRead = getReadConnection(fleetId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            double pendingAmount = contextRead.select(DSL.sum(QUP_MERCHANTTRANSACTION.PENDINGAMOUNT))
                    .from(QUP_MERCHANTTRANSACTION)
                    .where(QUP_MERCHANTTRANSACTION.FLEETID.eq(fleetId))
                    .and(QUP_MERCHANTTRANSACTION.MERCHANTID.in(merchantId))
                    .and(QUP_MERCHANTTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .and(QUP_MERCHANTTRANSACTION.PAID.eq(0))
                    .and(QUP_MERCHANTTRANSACTION.CREATEDDATE.lessOrEqual(payoutDate))
                    .fetchOne(0, double.class);

            contextRead.close();
            return pendingAmount;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0.0;
    }

    public int updateMerchantTransaction(String fleetId, List<String> merchantId, String currencyISO,
            Timestamp payoutDate) {
        Connection cnn = null;
        try {
            cnn = getConnection(fleetId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));

            context.update(QUP_MERCHANTTRANSACTION)
                    .set(QUP_MERCHANTTRANSACTION.PAID, 1)
                    .where(QUP_MERCHANTTRANSACTION.FLEETID.eq(fleetId))
                    .and(QUP_MERCHANTTRANSACTION.MERCHANTID.in(merchantId))
                    .and(QUP_MERCHANTTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .and(QUP_MERCHANTTRANSACTION.PAID.eq(0))
                    .and(QUP_MERCHANTTRANSACTION.CREATEDDATE.lessOrEqual(payoutDate))
                    .execute();

            return 1;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public List<Double> getUsageWalletBalance(String userId, String currencyISO, Timestamp topUpDate) {
        Connection cnnRead = null;
        double cashIn = 0.0;
        double cashOut = 0.0;
        double currentBalance = 0.0;
        try {
            cnnRead = getReadConnection(userId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));

            Condition editCashIn = QUP_WALLETTRANSACTION.DESCRIPTION.eq("editBalance");
            Condition topUpMOLPay = QUP_WALLETTRANSACTION.DESCRIPTION.eq("topUpMOLPay");
            Condition cashWallet = QUP_WALLETTRANSACTION.DESCRIPTION.eq("cashWallet");
            Condition combineCashIn = topUpMOLPay.or(cashWallet).or(editCashIn);

            cashIn = contextRead.select(DSL.sum(QUP_WALLETTRANSACTION.CASHIN))
                    .from(QUP_WALLETTRANSACTION)
                    .where(QUP_WALLETTRANSACTION.USERID.eq(userId))
                    .and(QUP_WALLETTRANSACTION.TYPE.eq("credit"))
                    .and(QUP_WALLETTRANSACTION.CASHIN.greaterThan(0.0))
                    .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .and(combineCashIn)
                    .and(QUP_WALLETTRANSACTION.CREATEDDATE.greaterOrEqual(topUpDate))
                    .fetchOne(0, double.class);

            Condition editCashOut = QUP_WALLETTRANSACTION.DESCRIPTION.eq("editBalance");
            Condition bookingDeduction = QUP_WALLETTRANSACTION.DESCRIPTION.eq("bookingDeduction");
            Condition driverTopUpForPax = QUP_WALLETTRANSACTION.DESCRIPTION.eq("driverTopUpForPax");
            Condition cashExcess = QUP_WALLETTRANSACTION.DESCRIPTION.eq("cashExcess");
            Condition combineCashOut = bookingDeduction.or(driverTopUpForPax).or(cashExcess).or(editCashOut);

            cashOut = contextRead.select(DSL.sum(QUP_WALLETTRANSACTION.CASHOUT))
                    .from(QUP_WALLETTRANSACTION)
                    .where(QUP_WALLETTRANSACTION.USERID.eq(userId))
                    .and(QUP_WALLETTRANSACTION.TYPE.eq("credit"))
                    .and(QUP_WALLETTRANSACTION.CASHOUT.greaterThan(0.0))
                    .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .and(combineCashOut)
                    .and(QUP_WALLETTRANSACTION.CREATEDDATE.greaterOrEqual(topUpDate))
                    .fetchOne(0, double.class);

            double currentCashIn = contextRead.select(DSL.sum(QUP_WALLETTRANSACTION.CASHIN))
                    .from(QUP_WALLETTRANSACTION)
                    .where(QUP_WALLETTRANSACTION.USERID.eq(userId))
                    .and(QUP_WALLETTRANSACTION.TYPE.eq("credit"))
                    .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .fetchOne(0, double.class);

            double currentCashOut = contextRead.select(DSL.sum(QUP_WALLETTRANSACTION.CASHOUT))
                    .from(QUP_WALLETTRANSACTION)
                    .where(QUP_WALLETTRANSACTION.USERID.eq(userId))
                    .and(QUP_WALLETTRANSACTION.TYPE.eq("credit"))
                    .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .fetchOne(0, double.class);

            currentBalance = currentCashIn - currentCashOut;
            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        List<Double> list = new ArrayList<>();
        list.add(cashIn);
        list.add(cashOut);
        list.add(currentBalance);
        return list;
    }

    public ObjResponse getListBookId(String fleetId) {
        ObjResponse response = new ObjResponse();
        Connection cnnRead = null;
        try {
            cnnRead = getReadConnection(fleetId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            Result<?> join = contextRead
                    .select(QUP_TICKET.BOOKID, QUP_TICKET.NETEARNING, QUP_TICKET.COMPLETEDTIME, QUP_TICKET.DRIVERID,
                            QUP_TICKET.CURRENCYISO)
                    .from(QUP_TICKET)
                    .innerJoin(QUP_WALLETTRANSACTION)
                    .on(QUP_TICKET.BOOKID.eq(QUP_WALLETTRANSACTION.TRANSACTIONID))
                    .and(QUP_TICKET.FLEETID.eq(fleetId))
                    .and(QUP_WALLETTRANSACTION.TYPE.eq("cash"))
                    .and(QUP_WALLETTRANSACTION.DESCRIPTION.eq("promo"))
                    .and(QUP_WALLETTRANSACTION.CREATEDDATE.greaterOrEqual(Timestamp.valueOf("2021-09-27 00:00:00")))
                    .and(QUP_WALLETTRANSACTION.CREATEDDATE.lessThan(Timestamp.valueOf("2021-10-09 00:00:00")))
                    .and(QUP_TICKET.TRANSACTIONSTATUS.ne("cash"))
                    .fetch();
            contextRead.close();
            List<Map> listData = new ArrayList<>();
            for (Record r : join) {
                QupTicketRecord ticket = r.into(QUP_TICKET);
                Map<String, Object> mapData = new HashMap<>();
                mapData.put("bookId", ticket.getBookid());
                mapData.put("netEarning", ticket.getNetearning());
                mapData.put("completedTime", ticket.getCompletedtime());
                mapData.put("driverId", ticket.getDriverid());
                mapData.put("currencyISO", ticket.getCurrencyiso());
                listData.add(mapData);
            }
            response.returnCode = 200;
            response.response = listData;
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        response.returnCode = 407;
        return response;
    }

    public boolean isToppedUpBonus(String userId, double amount, String currencyISO, Timestamp topUpDate) {
        Connection cnnRead = null;
        try {
            cnnRead = getReadConnection(userId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            List<QupWallettransactionRecord> record = contextRead.selectFrom(QUP_WALLETTRANSACTION)
                    .where(QUP_WALLETTRANSACTION.USERID.eq(userId))
                    .and(QUP_WALLETTRANSACTION.TYPE.eq("credit"))
                    .and(QUP_WALLETTRANSACTION.DESCRIPTION.eq("editBalance"))
                    .and(QUP_WALLETTRANSACTION.CASHIN.eq(amount))
                    .and(QUP_WALLETTRANSACTION.CURRENCYISO.eq(currencyISO))
                    .and(QUP_WALLETTRANSACTION.CREATEDDATE.greaterOrEqual(topUpDate))
                    .fetch();
            if (record != null && record.size() > 0)
                return true;

            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public double getMerchantWalletBalance(String requestId, String fleetId, String merchantId, String walletType,
            String currencyISO) {
        Connection cnnRead = null;
        double returnValue = 0.0;
        try {
            cnnRead = getReadConnection(merchantId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            QupMerchantwalletbalanceRecord record = contextRead.selectFrom(QUP_MERCHANTWALLETBALANCE)
                    .where(QUP_MERCHANTWALLETBALANCE.FLEETID.eq(fleetId))
                    .and(QUP_MERCHANTWALLETBALANCE.MERCHANTID.in(merchantId))
                    .and(QUP_MERCHANTWALLETBALANCE.CURRENCYISO.eq(currencyISO))
                    .and(QUP_MERCHANTWALLETBALANCE.WALLETTYPE.eq(walletType))
                    .orderBy(QUP_MERCHANTWALLETBALANCE.ID.desc())
                    .limit(1)
                    .fetchOne();
            double currentBalance = record != null ? record.getNewbalance() : 0.0;
            logger.debug(requestId + " - currentBalance: " + currentBalance);
            BigDecimal cashIn = contextRead.select(DSL.sum(QUP_MERCHANTWALLETBALANCE.CASHIN))
                    .from(QUP_MERCHANTWALLETBALANCE)
                    .where(QUP_MERCHANTWALLETBALANCE.FLEETID.eq(fleetId))
                    .and(QUP_MERCHANTWALLETBALANCE.MERCHANTID.in(merchantId))
                    .and(QUP_MERCHANTWALLETBALANCE.CURRENCYISO.eq(currencyISO))
                    .and(QUP_MERCHANTWALLETBALANCE.WALLETTYPE.eq(walletType))
                    .fetchOne(0, BigDecimal.class);
            if (cashIn == null)
                cashIn = new BigDecimal("0.0");
            logger.debug(requestId + " - cashIn: " + cashIn.doubleValue());
            BigDecimal cashOut = contextRead.select(DSL.sum(QUP_MERCHANTWALLETBALANCE.CASHOUT))
                    .from(QUP_MERCHANTWALLETBALANCE)
                    .where(QUP_MERCHANTWALLETBALANCE.FLEETID.eq(fleetId))
                    .and(QUP_MERCHANTWALLETBALANCE.MERCHANTID.in(merchantId))
                    .and(QUP_MERCHANTWALLETBALANCE.CURRENCYISO.eq(currencyISO))
                    .and(QUP_MERCHANTWALLETBALANCE.WALLETTYPE.eq(walletType))
                    .fetchOne(0, BigDecimal.class);
            if (cashOut == null)
                cashOut = new BigDecimal("0.0");
            logger.debug(requestId + " - cashOut: " + cashOut.doubleValue());

            returnValue = cashIn.subtract(cashOut).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            logger.debug(requestId + " - returnValue: " + returnValue);
            if (returnValue != currentBalance) {
                boolean matched = false;
                logger.debug(requestId + " - balance not match - retry");
                int i = 0;
                while (!matched && i < 5) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ignore) {
                    }
                    cashIn = contextRead.select(DSL.sum(QUP_MERCHANTWALLETBALANCE.CASHIN))
                            .from(QUP_MERCHANTWALLETBALANCE)
                            .where(QUP_MERCHANTWALLETBALANCE.FLEETID.eq(fleetId))
                            .and(QUP_MERCHANTWALLETBALANCE.MERCHANTID.in(merchantId))
                            .and(QUP_MERCHANTWALLETBALANCE.CURRENCYISO.eq(currencyISO))
                            .and(QUP_MERCHANTWALLETBALANCE.WALLETTYPE.eq(walletType))
                            .fetchOne(0, BigDecimal.class);
                    if (cashIn == null)
                        cashIn = new BigDecimal("0.0");
                    cashOut = contextRead.select(DSL.sum(QUP_MERCHANTWALLETBALANCE.CASHOUT))
                            .from(QUP_MERCHANTWALLETBALANCE)
                            .where(QUP_MERCHANTWALLETBALANCE.FLEETID.eq(fleetId))
                            .and(QUP_MERCHANTWALLETBALANCE.MERCHANTID.in(merchantId))
                            .and(QUP_MERCHANTWALLETBALANCE.CURRENCYISO.eq(currencyISO))
                            .and(QUP_MERCHANTWALLETBALANCE.WALLETTYPE.eq(walletType))
                            .fetchOne(0, BigDecimal.class);
                    if (cashOut == null)
                        cashOut = new BigDecimal("0.0");

                    returnValue = cashIn.subtract(cashOut).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                    if (returnValue != currentBalance) {
                        logger.debug(requestId + " - balance NOT matched after retry");
                    } else {
                        logger.debug(requestId + " - balance MATCHED after retry");
                        matched = true;
                    }
                    i++;
                }
            }

            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return returnValue;
    }

    public long addMerchantWalletBalance(MerchantWalletBalance merchantWalletBalance) {
        Connection cnn = null;
        Record record = null;
        try {
            cnn = getConnection(merchantWalletBalance.merchantId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            record = context.insertInto(QUP_MERCHANTWALLETBALANCE,
                    QUP_MERCHANTWALLETBALANCE.FLEETID,
                    QUP_MERCHANTWALLETBALANCE.MERCHANTID,
                    QUP_MERCHANTWALLETBALANCE.WALLETTYPE,
                    QUP_MERCHANTWALLETBALANCE.TRANSACTIONTYPE,
                    QUP_MERCHANTWALLETBALANCE.TRANSACTIONID,
                    QUP_MERCHANTWALLETBALANCE.CASHIN,
                    QUP_MERCHANTWALLETBALANCE.CASHOUT,
                    QUP_MERCHANTWALLETBALANCE.NEWBALANCE,
                    QUP_MERCHANTWALLETBALANCE.CURRENCYISO,
                    QUP_MERCHANTWALLETBALANCE.CREATEDDATE)
                    .values(merchantWalletBalance.fleetId,
                            merchantWalletBalance.merchantId,
                            merchantWalletBalance.walletType,
                            merchantWalletBalance.transactionType,
                            merchantWalletBalance.transactionId,
                            merchantWalletBalance.cashIn,
                            merchantWalletBalance.cashOut,
                            merchantWalletBalance.newBalance,
                            merchantWalletBalance.currencyISO,
                            merchantWalletBalance.createdDate)
                    .returning()
                    .fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (record != null)
            return record.getValue(QUP_MERCHANTWALLETBALANCE.ID);
        return 0;
    }

    public long addMerchantWalletTransaction(MerchantWalletTransaction merchantWalletTransaction) {
        Connection cnn = null;
        Record record = null;
        try {
            cnn = getConnection(merchantWalletTransaction.merchantId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            String reason = merchantWalletTransaction.reason != null ? merchantWalletTransaction.reason : "";
            reason = CommonUtils.removeSpecialCharacter(reason).trim().isEmpty() ? ""
                    : CommonUtils.removeSpecialCharacter(reason).trim();

            record = context.insertInto(QUP_MERCHANTWALLETTRANSACTION,
                    QUP_MERCHANTWALLETTRANSACTION.FLEETID,
                    QUP_MERCHANTWALLETTRANSACTION.MERCHANTID,
                    QUP_MERCHANTWALLETTRANSACTION.WALLETTYPE,
                    QUP_MERCHANTWALLETTRANSACTION.TRANSACTIONTYPE,
                    QUP_MERCHANTWALLETTRANSACTION.TRANSACTIONID,
                    QUP_MERCHANTWALLETTRANSACTION.BOOKID,
                    QUP_MERCHANTWALLETTRANSACTION.REASON,
                    QUP_MERCHANTWALLETTRANSACTION.AMOUNT,
                    QUP_MERCHANTWALLETTRANSACTION.CURRENCYISO,
                    QUP_MERCHANTWALLETTRANSACTION.OPERATORID,
                    QUP_MERCHANTWALLETTRANSACTION.CREATEDDATE)
                    .values(merchantWalletTransaction.fleetId,
                            merchantWalletTransaction.merchantId,
                            merchantWalletTransaction.walletType,
                            merchantWalletTransaction.transactionType,
                            merchantWalletTransaction.transactionId,
                            merchantWalletTransaction.bookId,
                            reason,
                            merchantWalletTransaction.amount,
                            merchantWalletTransaction.currencyISO,
                            merchantWalletTransaction.operatorId,
                            merchantWalletTransaction.createdDate)
                    .returning()
                    .fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (record != null)
            return record.getValue(QUP_MERCHANTWALLETTRANSACTION.ID);
        return 0;
    }

    public long addHydraPayout(HydraPayout hydraPayout) {
        Connection cnn = null;
        Record record = null;
        try {
            cnn = getConnection(hydraPayout.payoutId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            String notes = hydraPayout.notes != null ? hydraPayout.notes : "";
            notes = CommonUtils.removeSpecialCharacter(notes).trim().isEmpty() ? ""
                    : CommonUtils.removeSpecialCharacter(notes).trim();

            record = context.insertInto(QUP_HYDRAPAYOUT,
                    QUP_HYDRAPAYOUT.PAYOUTID,
                    QUP_HYDRAPAYOUT.DATETIME,
                    QUP_HYDRAPAYOUT.PAYOUTTYPE,
                    QUP_HYDRAPAYOUT.ACCOUNTHOLDERNAME,
                    QUP_HYDRAPAYOUT.BANKNAME,
                    QUP_HYDRAPAYOUT.ACCOUNTNUMBER,
                    QUP_HYDRAPAYOUT.TOTALPAYOUT,
                    QUP_HYDRAPAYOUT.NOTES,
                    QUP_HYDRAPAYOUT.FLEETID,
                    QUP_HYDRAPAYOUT.BOOKID,
                    QUP_HYDRAPAYOUT.TRANSACTIONID,
                    QUP_HYDRAPAYOUT.OPERATORNAME,
                    QUP_HYDRAPAYOUT.FLEETNAME,
                    QUP_HYDRAPAYOUT.TRANSACTIONTYPE)
                    .values(hydraPayout.payoutId,
                            hydraPayout.dateTime,
                            hydraPayout.payoutType,
                            hydraPayout.accountHolderName,
                            hydraPayout.bankName,
                            hydraPayout.accountNumber,
                            hydraPayout.totalPayout,
                            notes,
                            hydraPayout.fleetId,
                            hydraPayout.bookId,
                            hydraPayout.transactionId,
                            hydraPayout.operatorName,
                            hydraPayout.fleetName,
                            hydraPayout.transactionType

                    )
                    .returning()
                    .fetchOne();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (record != null)
            return record.getValue(QUP_HYDRAPAYOUT.ID);
        return 0;
    }

    public long addHydraPending(HydraPending hydraPending) {
        Connection cnn = null;
        Record record = null;
        try {
            cnn = getConnection(hydraPending.bookId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));

            record = context.insertInto(QUP_HYDRAPENDING,
                    QUP_HYDRAPENDING.FLEETID,
                    QUP_HYDRAPENDING.PSGFLEETID,
                    QUP_HYDRAPENDING.BOOKID,
                    QUP_HYDRAPENDING.COMPLETEDTIME,
                    QUP_HYDRAPENDING.USERID,
                    QUP_HYDRAPENDING.PAYOUTTOPROVIDERFLEET,
                    QUP_HYDRAPENDING.PAYOUTTOHOMEFLEET,
                    QUP_HYDRAPENDING.CONVERTSUPPLIERPAYOUT,
                    QUP_HYDRAPENDING.CONVERTBUYERPAYOUT,
                    QUP_HYDRAPENDING.CONVERTSUPPLIERPENALTY,
                    QUP_HYDRAPENDING.HYDRAOUTSTANDINGAMOUNT,
                    QUP_HYDRAPENDING.HYDRAPAYMENTSTATUS,
                    QUP_HYDRAPENDING.DOUBLE0,
                    QUP_HYDRAPENDING.NETWORKTYPE,
                    QUP_HYDRAPENDING.PAYMENTMETHOD)
                    .values(hydraPending.fleetId,
                            hydraPending.psgFleetId,
                            hydraPending.bookId,
                            hydraPending.completedTime,
                            hydraPending.userId,
                            hydraPending.payoutToProviderFleet,
                            hydraPending.payoutToHomeFleet,
                            hydraPending.convertSupplierPayout,
                            hydraPending.convertBuyerPayout,
                            hydraPending.convertSupplierPenalty,
                            hydraPending.hydraOutstandingAmount,
                            hydraPending.hydraPaymentStatus,
                            hydraPending.exchangeRate,
                            hydraPending.networkType,
                            hydraPending.paymentMethod)
                    .returning()
                    .fetchOne();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (record != null)
            return record.getValue(QUP_HYDRAPENDING.ID);
        return 0;
    }

    public HydraPending getHydraPending(String requestId, String fleetId, String userId, String bookId) {
        Connection cnnRead = null;
        try {
            cnnRead = getReadConnection(fleetId);

            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            QupHydrapendingRecord record = contextRead.selectFrom(QUP_HYDRAPENDING)
                    .where(QUP_HYDRAPENDING.BOOKID.eq(bookId))
                    .and(QUP_HYDRAPENDING.QUP_HYDRAPENDING.FLEETID.eq(fleetId))
                    .and(QUP_HYDRAPENDING.QUP_HYDRAPENDING.USERID.eq(userId))
                    .and(QUP_HYDRAPENDING.QUP_HYDRAPENDING.HYDRAOUTSTANDINGAMOUNT.gt(0.0))
                    .fetchOne();
            if (record != null) {
                ResultSet rs = record.intoResultSet();
                while (rs.next()) {
                    HydraPending hydraPending = new HydraPending();
                    hydraPending.hydraOutstandingAmount = rs.getDouble("hydraOutstandingAmount");

                    return hydraPending;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public String getHydraPayoutReport(String requestId, String fleetId) {
        Connection cnnRead = null;
        String result = "";
        try {
            cnnRead = getReadConnection(fleetId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            SelectQuery query = contextRead.selectQuery();

            query.addSelect(QUP_HYDRAPENDING.BOOKID, QUP_HYDRAPENDING.FLEETID, QUP_HYDRAPENDING.PSGFLEETID,
                    QUP_HYDRAPENDING.CONVERTSUPPLIERPENALTY, QUP_HYDRAPENDING.CONVERTSUPPLIERPAYOUT,
                    QUP_HYDRAPENDING.CONVERTBUYERPAYOUT, QUP_HYDRAPENDING.DOUBLE0);

            /*
             * query.addSelect(groupConcat(QUP_HYDRAPENDING.BOOKID).as("books"),
             * sum(when(QUP_HYDRAPENDING.FLEETID.eq(fleetId).and(QUP_HYDRAPENDING.
             * PAYOUTTOPROVIDERFLEET.eq(0)).and(QUP_HYDRAPENDING.CONVERTSUPPLIERPAYOUT.
             * greaterThan(0.0)), QUP_HYDRAPENDING.CONVERTSUPPLIERPAYOUT).otherwise(0.0))
             * .plus(sum(when(QUP_HYDRAPENDING.FLEETID.eq(fleetId).and(QUP_HYDRAPENDING.
             * CONVERTSUPPLIERPENALTY.lessThan(0.0)),
             * QUP_HYDRAPENDING.CONVERTSUPPLIERPENALTY).otherwise(0.0)))
             * .plus(sum(when(QUP_HYDRAPENDING.PSGFLEETID.eq(fleetId).and(QUP_HYDRAPENDING.
             * PAYOUTTOHOMEFLEET.eq(0)).and(QUP_HYDRAPENDING.CONVERTBUYERPAYOUT.greaterThan(
             * 0.0)), QUP_HYDRAPENDING.CONVERTBUYERPAYOUT).otherwise(0.0)))
             * .as("total"));
             */

            query.addFrom(QUP_HYDRAPENDING);
            query.addConditions(
                    (QUP_HYDRAPENDING.FLEETID.eq(fleetId)
                            .and(QUP_HYDRAPENDING.PAYOUTTOPROVIDERFLEET.eq(0))
                            .and(QUP_HYDRAPENDING.CONVERTSUPPLIERPAYOUT.ne(0.0)))
                            .or(QUP_HYDRAPENDING.FLEETID.eq(fleetId)
                                    .and(QUP_HYDRAPENDING.PAYOUTTOPROVIDERFLEET.eq(0))
                                    .and(QUP_HYDRAPENDING.CONVERTSUPPLIERPENALTY.lessThan(0.0)))
                            .or(QUP_HYDRAPENDING.PSGFLEETID.eq(fleetId)
                                    .and(QUP_HYDRAPENDING.PAYOUTTOHOMEFLEET.eq(0))
                                    .and(QUP_HYDRAPENDING.CONVERTBUYERPAYOUT.greaterThan(0.0)))
                            .or(QUP_HYDRAPENDING.PSGFLEETID.eq(fleetId)
                                    .and(QUP_HYDRAPENDING.CHARGETOTALFROMHOMEFLEET.eq(0))
                                    .and(QUP_HYDRAPENDING.CONVERTBUYERPAYOUT.lessThan(0.0))));
            /*
             * logger.debug(requestId + " ---- fleetId: " + fleetId + " - query: " +
             * query.toString());
             */
            result = query.fetch().formatJSON();
            logger.debug(requestId + " - result: " + result);

            contextRead.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public List<HydraPending> getListHydraPendingByBookIds(String requestId, List<String> bookIds) {
        Connection cnnRead = null;
        List<HydraPending> listReturn = new ArrayList<>();
        try {
            cnnRead = getReadConnection("");

            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            List<QupHydrapendingRecord> hydrapendingRecords = contextRead.selectFrom(QUP_HYDRAPENDING)
                    .where(QUP_HYDRAPENDING.BOOKID.in(bookIds))
                    .orderBy(QUP_HYDRAPENDING.ID.desc())
                    .fetch();
            for (QupHydrapendingRecord record : hydrapendingRecords) {
                HydraPending hydraPending = new HydraPending();
                hydraPending.fleetId = record.getFleetid();
                hydraPending.psgFleetId = record.getPsgfleetid();
                hydraPending.bookId = record.getBookid();
                hydraPending.completedTime = record.getCompletedtime();
                hydraPending.convertSupplierPayout = record.getConvertsupplierpayout();
                hydraPending.convertBuyerPayout = record.getConvertbuyerpayout();
                hydraPending.convertSupplierPenalty = record.getConvertsupplierpenalty();
                hydraPending.networkType = record.getNetworktype();
                hydraPending.paymentMethod = record.getPaymentmethod();
                hydraPending.exchangeRate = record.getDouble0();
                listReturn.add(hydraPending);
            }
            return listReturn;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public HydraPayout getHydraPayout(String requestId, String fleetId) {
        Connection cnnRead = null;
        HydraPayout hydraPayout;
        try {
            cnnRead = getReadConnection(fleetId);

            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            List<QupHydrapayoutRecord> hydraPayoutecords = contextRead.selectFrom(QUP_HYDRAPAYOUT)
                    .where(QUP_HYDRAPAYOUT.FLEETID.eq(fleetId))
                    .and(QUP_HYDRAPAYOUT.PAYOUTTYPE.eq("manual"))
                    .orderBy(QUP_HYDRAPAYOUT.ID.desc())
                    .fetch();
            QupHydrapayoutRecord hydraPayoutecord = hydraPayoutecords != null && hydraPayoutecords.size() > 0
                    ? hydraPayoutecords.get(0)
                    : null;
            if (hydraPayoutecord != null) {
                ResultSet rs = hydraPayoutecord.intoResultSet();
                while (rs.next()) {
                    hydraPayout = new HydraPayout();
                    hydraPayout.payoutId = rs.getString("payoutId");
                    hydraPayout.dateTime = rs.getTimestamp("dateTime");
                    hydraPayout.payoutType = rs.getString("payoutType");
                    hydraPayout.accountHolderName = rs.getString("accountHolderName");
                    hydraPayout.bankName = rs.getString("bankName");
                    hydraPayout.accountNumber = rs.getString("accountNumber");
                    hydraPayout.totalPayout = rs.getDouble("totalPayout");
                    hydraPayout.notes = rs.getString("notes");
                    hydraPayout.fleetId = rs.getString("fleetId");
                    hydraPayout.bookId = rs.getString("bookId");
                    hydraPayout.transactionId = rs.getString("transactionId");
                    hydraPayout.operatorName = rs.getString("operatorName");
                    hydraPayout.fleetName = rs.getString("fleetName");
                    hydraPayout.transactionType = rs.getString("transactionType");

                    return hydraPayout;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public List<HydraPending> updateHydraPayout(String requestId, List<String> bookIds, String fleetId) {
        Connection cnn = null;
        List<HydraPending> listReturn = new ArrayList<>();
        try {
            cnn = getConnection(fleetId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            List<QupHydrapendingRecord> list = context.selectFrom(QUP_HYDRAPENDING)
                    .where(QUP_HYDRAPENDING.BOOKID.in(bookIds))
                    .fetch();
            if (list.size() > 0) {
                for (QupHydrapendingRecord record : list) {
                    String bookId = record.getBookid();
                    int update = 0;
                    if (record.getFleetid() != null && record.getFleetid().equals(fleetId)) {
                        if (record.getConvertsupplierpenalty() < 0) {
                            logger.debug(requestId + " - fleet role is supplier - charge penalty");
                            update = context.update(QUP_HYDRAPENDING)
                                    .set(QUP_HYDRAPENDING.PAYOUTTOPROVIDERFLEET, 1)
                                    .where(QUP_HYDRAPENDING.BOOKID.eq(bookId))
                                    .and(QUP_HYDRAPENDING.FLEETID.eq(fleetId))
                                    .execute();
                        } else {
                            if (record.getConvertsupplierpayout() > 0) {
                                logger.debug(requestId + " - fleet role is supplier - payout");
                                update = context.update(QUP_HYDRAPENDING)
                                        .set(QUP_HYDRAPENDING.PAYOUTTOPROVIDERFLEET, 1)
                                        .where(QUP_HYDRAPENDING.BOOKID.eq(bookId))
                                        .and(QUP_HYDRAPENDING.FLEETID.eq(fleetId))
                                        .execute();
                            } else {
                                logger.debug(requestId + " - fleet role is supplier - reject booking");
                                update = context.update(QUP_HYDRAPENDING)
                                        .set(QUP_HYDRAPENDING.PAYOUTTOPROVIDERFLEET, 1)
                                        .where(QUP_HYDRAPENDING.BOOKID.eq(bookId))
                                        .and(QUP_HYDRAPENDING.FLEETID.eq(fleetId))
                                        .execute();
                            }
                        }
                    } else if (record.getPsgfleetid() != null && record.getPsgfleetid().equals(fleetId)) {
                        if (record.getConvertbuyerpayout() > 0) {
                            logger.debug(requestId + " - fleet role is buyer - payout");
                            update = context.update(QUP_HYDRAPENDING)
                                    .set(QUP_HYDRAPENDING.PAYOUTTOHOMEFLEET, 1)
                                    .where(QUP_HYDRAPENDING.BOOKID.eq(bookId))
                                    .and(QUP_HYDRAPENDING.PSGFLEETID.eq(fleetId))
                                    .execute();
                        } else {
                            logger.debug(requestId + " - fleet role is buyer - charge for booking");
                            update = context.update(QUP_HYDRAPENDING)
                                    .set(QUP_HYDRAPENDING.CHARGETOTALFROMHOMEFLEET, 1)
                                    .where(QUP_HYDRAPENDING.BOOKID.eq(bookId))
                                    .and(QUP_HYDRAPENDING.PSGFLEETID.eq(fleetId))
                                    .execute();
                            if (record.getNetworktype().equals("farmOut")) {
                                update = context.update(QUP_HYDRAPENDING)
                                        .set(QUP_HYDRAPENDING.CHARGETOTALFROMHOMEFLEET, 1)
                                        .set(QUP_HYDRAPENDING.PAYOUTTOHOMEFLEET, 1)
                                        .where(QUP_HYDRAPENDING.BOOKID.eq(bookId))
                                        .and(QUP_HYDRAPENDING.PSGFLEETID.eq(fleetId))
                                        .execute();
                            }
                        }
                    }
                    logger.debug(requestId + " - update for booking " + bookId + " with networkType "
                            + record.getNetworktype() + " :" + update);
                    if (update > 0) {
                        HydraPending hydraPending = new HydraPending();
                        hydraPending.fleetId = record.getFleetid();
                        hydraPending.psgFleetId = record.getPsgfleetid();
                        hydraPending.bookId = record.getBookid();
                        hydraPending.completedTime = record.getCompletedtime();
                        hydraPending.convertSupplierPayout = record.getConvertsupplierpayout();
                        hydraPending.convertBuyerPayout = record.getConvertbuyerpayout();
                        hydraPending.convertSupplierPenalty = record.getConvertsupplierpenalty();
                        hydraPending.networkType = record.getNetworktype();
                        hydraPending.paymentMethod = record.getPaymentmethod();
                        hydraPending.exchangeRate = record.getDouble0();
                        listReturn.add(hydraPending);
                    }
                }
            }
            return listReturn;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public int updateHydraPendingPayment(String requestId, String fleetId, String userId, List<String> bookIds,
            String updateInfo) {
        Connection cnn = null;
        try {
            org.json.JSONObject object = new org.json.JSONObject(updateInfo);
            String status = object.has("status") ? object.getString("status") : "";
            if (status.isEmpty())
                return 0;
            cnn = getConnection(fleetId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            if (!fleetId.isEmpty() || !userId.isEmpty()) {
                return context.update(QUP_HYDRAPENDING)
                        .set(QUP_HYDRAPENDING.HYDRAPAYMENTSTATUS, status)
                        .set(QUP_HYDRAPENDING.HYDRAOUTSTANDINGAMOUNT, 0.0)
                        .set(QUP_HYDRAPENDING.COLLECTPENDINGRESULT, updateInfo)
                        .where(QUP_HYDRAPENDING.BOOKID.in(bookIds))
                        .and(QUP_HYDRAPENDING.PSGFLEETID.eq(fleetId))
                        .and(QUP_HYDRAPENDING.USERID.eq(userId))
                        .execute();
            } else {
                return context.update(QUP_HYDRAPENDING)
                        .set(QUP_HYDRAPENDING.HYDRAPAYMENTSTATUS, status)
                        .set(QUP_HYDRAPENDING.HYDRAOUTSTANDINGAMOUNT, 0.0)
                        .set(QUP_HYDRAPENDING.COLLECTPENDINGRESULT, updateInfo)
                        .where(QUP_HYDRAPENDING.BOOKID.in(bookIds))
                        .execute();
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public ObjResponse getOutstandingHydra(String fleetId, String customerId) {
        ObjResponse response = new ObjResponse();
        Connection cnnRead = null;
        try {
            cnnRead = getReadConnection(customerId);
            DSLContext contextRead = DSL.using(cnnRead, SQLDialect.MYSQL, getSettings("read"));
            List<QupHydrapendingRecord> list = contextRead.selectFrom(QUP_HYDRAPENDING)
                    .where(QUP_HYDRAPENDING.PSGFLEETID.eq(fleetId))
                    .and(QUP_HYDRAPENDING.USERID.eq(customerId))
                    .and(QUP_HYDRAPENDING.HYDRAOUTSTANDINGAMOUNT.gt(0.0))
                    .fetch();
            contextRead.close();
            List<Map> listData = new ArrayList<>();
            for (QupHydrapendingRecord record : list) {
                Map<String, Object> mapData = new HashMap<>();
                mapData.put("bookId", record.getBookid());
                mapData.put("fleetId", record.getFleetid());
                mapData.put("amount", record.getHydraoutstandingamount());
                mapData.put("completedTime", record.getCompletedtime());
                mapData.put("status", record.getHydrapaymentstatus());
                mapData.put("exchangeRate", record.getDouble0());
                listData.add(mapData);
            }
            response.returnCode = 200;
            response.response = listData;
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnnRead != null)
                    cnnRead.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        response.returnCode = 407;
        return response;
    }

    public void removeHydraIncome(long id) {
        Connection cnn = null;
        try {
            cnn = getConnection(String.valueOf(id));
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            context.delete(QUP_HYDRAINCOME)
                    .where(QUP_HYDRAINCOME.ID.eq(id))
                    .returning()
                    .fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void notifyToSlack(Exception ex) throws Exception {
        StringBuffer buffer = new StringBuffer();
        buffer.append("```");
        buffer.append("exception: " + ex.getMessage() + "\n");
        buffer.append("<@UGSEF7KN3> ");
        buffer.append("<@UGSHTHE7L> ");
        buffer.append("<@UH3ACCSSH>");
        buffer.append("```");
        JSONObject objBody = new JSONObject();
        objBody.put("channel", ServerConfig.notify_channel);
        objBody.put("text", buffer.toString());
        HttpResponse<String> httpResponse = Unirest.post(ServerConfig.notify_url)
                .header("Authorization", "Bearer " + ServerConfig.notify_token)
                .header("Content-Type", "application/json; charset=utf-8")
                .body(objBody.toJSONString())
                .asString();
        String result = httpResponse.getBody();
        logger.debug("Notify to slack: " + result);
    }

    public long addTransactionDetails(String requestId, TransactionDetails transactionDetails) {
        Connection cnn = null;
        Record record = null;
        try {
            cnn = getConnection(requestId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            record =
                    context.insertInto(QUP_TRANSACTIONDETAILS,
                                    QUP_TRANSACTIONDETAILS.FLEETID,
                                    QUP_TRANSACTIONDETAILS.BOOKID,
                                    QUP_TRANSACTIONDETAILS.TRANSACTIONID,
                                    QUP_TRANSACTIONDETAILS.CREATEDTIME,
                                    QUP_TRANSACTIONDETAILS.AMOUNT,
                                    QUP_TRANSACTIONDETAILS.CARDTYPE,
                                    QUP_TRANSACTIONDETAILS.CARDMASKED,
                                    QUP_TRANSACTIONDETAILS.PAYMENTMETHOD,
                                    QUP_TRANSACTIONDETAILS.CURRENCYISO,
                                    QUP_TRANSACTIONDETAILS.CURRENCYSYMBOL,
                                    QUP_TRANSACTIONDETAILS.CHARGENOTE,
                                    QUP_TRANSACTIONDETAILS.OPERATORID,
                                    QUP_TRANSACTIONDETAILS.PAYMENTLINK,
                                    QUP_TRANSACTIONDETAILS.STATUS
                            )
                            .values(transactionDetails.fleetId,
                                    transactionDetails.bookId,
                                    transactionDetails.transactionId,
                                    transactionDetails.createdTime,
                                    transactionDetails.amount,
                                    transactionDetails.cardType,
                                    transactionDetails.cardMasked,
                                    transactionDetails.paymentMethod,
                                    transactionDetails.currencyISO,
                                    transactionDetails.currencySymbol,
                                    transactionDetails.chargeNote,
                                    transactionDetails.operatorId,
                                    transactionDetails.paymentLink,
                                    transactionDetails.status
                            )
                            .returning()
                            .fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug(requestId + " - exception: " + CommonUtils.getError(e));
        }finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                logger.debug(requestId + " - exception: " + CommonUtils.getError(e));
            }
        }
        if(record != null)
            return record.getValue(QUP_TRANSACTIONDETAILS.ID);
        return 0;
    }

    public List<TransactionDetails> getPaymentDetailsByBookId(String requestId, String fleetId, String bookId) {
        Connection cnn = null;
        try {
            cnn = getConnection(requestId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            return context.selectFrom(QUP_TRANSACTIONDETAILS)
                    .where(QUP_TRANSACTIONDETAILS.FLEETID.eq(fleetId))
                    .and(QUP_TRANSACTIONDETAILS.BOOKID.eq(bookId))
                    .orderBy(QUP_TRANSACTIONDETAILS.CREATEDTIME.desc())
                    .fetchInto(TransactionDetails.class);
        } catch (Exception e) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(e));
        }
        finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                logger.debug(requestId + " - exception: " + CommonUtils.getError(e));
            }
        }
        return null;
    }

    public int updatePaymentDetails(String requestId, TransactionDetails transactionDetails) {
        Connection cnn = null;
        try {
            cnn = getConnection(requestId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            return context.update(QUP_TRANSACTIONDETAILS)
                    .set(QUP_TRANSACTIONDETAILS.STATUS, transactionDetails.status)
                    .set(QUP_TRANSACTIONDETAILS.TRANSACTIONID, transactionDetails.transactionId != null ? transactionDetails.transactionId : "")
                    .set(QUP_TRANSACTIONDETAILS.CARDTYPE, transactionDetails.cardType != null ? transactionDetails.cardType : "")
                    .set(QUP_TRANSACTIONDETAILS.CARDMASKED, transactionDetails.cardMasked != null ? transactionDetails.cardMasked : "")
                    .set(QUP_TRANSACTIONDETAILS.STRIPEMETHOD, transactionDetails.stripeMethod != null ? transactionDetails.stripeMethod : "")
                    .where(QUP_TRANSACTIONDETAILS.ID.eq(transactionDetails.id))
                    .execute();
        } catch (Exception e) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(e));
        }
        finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                logger.debug(requestId + " - exception: " + CommonUtils.getError(e));
            }
        }
        return 0;
    }

    public TransactionDetails getPaymentDetailsByTransactionId(String requestId, String transactionId) {
        Connection cnn = null;
        try {
            cnn = getConnection(requestId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            return context.selectFrom(QUP_TRANSACTIONDETAILS)
                    .where(QUP_TRANSACTIONDETAILS.TRANSACTIONID.eq(transactionId))
                    .fetchInto(TransactionDetails.class).get(0);
        } catch (Exception e) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(e));
        }
        finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                logger.debug(requestId + " - exception: " + CommonUtils.getError(e));
            }
        }
        return null;
    }

    public TransactionDetails getPaymentDetailsById(String requestId, long transactionDetailsId) {
        Connection cnn = null;
        try {
            cnn = getConnection(requestId);
            DSLContext context = DSL.using(cnn, SQLDialect.MYSQL, getSettings(""));
            return context.selectFrom(QUP_TRANSACTIONDETAILS)
                    .where(QUP_TRANSACTIONDETAILS.ID.eq(transactionDetailsId))
                    .fetchInto(TransactionDetails.class).get(0);
        } catch (Exception e) {
            logger.debug(requestId + " - exception: " + CommonUtils.getError(e));
        }
        finally {
            try {
                if (cnn != null)
                    cnn.close();
            } catch (SQLException e) {
                logger.debug(requestId + " - exception: " + CommonUtils.getError(e));
            }
        }
        return null;
    }
}