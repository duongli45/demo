package com.qupworld.paymentgateway.entities.Vipps;

import java.util.UUID;

/**
 * Created by thuanho on 12/01/2021.
 */
public class VippsNotifyEnt {

    public String merchantSerialNumber;
    public String orderId;
    public TransactionInfo transactionInfo;

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }
}
