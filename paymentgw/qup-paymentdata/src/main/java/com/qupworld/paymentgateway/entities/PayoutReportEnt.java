package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.List;
import java.util.UUID;

/**
 * Created by thuanho on 24/11/2020.
 */
public class PayoutReportEnt {

    public String fleetId;
    public String merchantId;
    public String driverId;
    public String companyId;
    public String currency;
    public String payoutOption;// value = all, hasBank, noBank, invalidBank
    public String payoutDate;
    public double minPayout;
    public double holdAmount;
    public int from;
    public int size;
    public List<String> listDriver;
    public String action; // value = add, remove; used to add or remove invalid driver
    public String type; // value = driver/merchant
    public List<String> listMerchant;

    // for log
    public String requestId = "";

    public int validateUpdatePayout() throws JsonProcessingException {
        if (this.isEmptyString(this.fleetId) || this.isEmptyString(this.action)) {
            return 406;
        }

        if (!action.equals("add") && !action.equals("remove")) {
            return 415;
        }
        return 200;
    }

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }
}
