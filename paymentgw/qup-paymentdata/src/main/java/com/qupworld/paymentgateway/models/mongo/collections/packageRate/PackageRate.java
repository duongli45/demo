package com.qupworld.paymentgateway.models.mongo.collections.packageRate;

import org.bson.types.ObjectId;

import javax.annotation.Generated;
import java.util.List;

/**
 * Created by hoang.nguyen on 12/16/16.
 */
@Generated("org.jsonschema2pojo")
public class PackageRate {
    public ObjectId _id;
    public String fleetId;
    public String fareHourlyId;
    public String name;
    public int duration;
    public String type;
    public String extraDurationType; // 'hour', 'minute'
    //public Double basedFee;
    //public double extraDuration;
    public double coveredDistance;
    //public double extraDistance;
    public Boolean isActive;
    public List<FeesWithCurrency> feesByCurrencies;
}
