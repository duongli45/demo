package com.qupworld.paymentgateway.entities;

import java.util.UUID;

/**
 * Created by qup on 9/22/16.
 */
public class ChargeFleetOwner {

    public String fleetId;
    public Integer fleetSubscriptionId;
    public double total;
    public String token;
    public String currencyISO;
    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    /**
     * Valid a Credit Card Data
     */
    public int validateData() {
        if (this.isEmptyString(this.fleetId) || fleetSubscriptionId == 0 || total <= 0) {
            return 406;
        }
        return 200;
    }
    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }
}
