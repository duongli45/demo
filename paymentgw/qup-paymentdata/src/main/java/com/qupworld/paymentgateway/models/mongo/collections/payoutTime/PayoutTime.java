package com.qupworld.paymentgateway.models.mongo.collections.payoutTime;

import org.bson.types.ObjectId;

import java.util.Date;

/**
 * Created by thuanho on 24/11/2020.
 */
public class PayoutTime {

    public ObjectId _id;
    public String fleetId;
    public Date payoutTime;
    public String payTo;
}
