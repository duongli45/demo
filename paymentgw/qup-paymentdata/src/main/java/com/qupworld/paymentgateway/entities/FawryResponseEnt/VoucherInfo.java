
package com.qupworld.paymentgateway.entities.FawryResponseEnt;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VoucherInfo {

    @SerializedName("vouchDesc")
    @Expose
    private String vouchDesc;
    @SerializedName("vouchPIN")
    @Expose
    private String vouchPIN;
    @SerializedName("vouchShrtExpDt")
    @Expose
    private String vouchShrtExpDt;
    @SerializedName("vouchSN")
    @Expose
    private String vouchSN;
    @SerializedName("vouchExpDt")
    @Expose
    private String vouchExpDt;

    public String getVouchDesc() {
        return vouchDesc;
    }

    public void setVouchDesc(String vouchDesc) {
        this.vouchDesc = vouchDesc;
    }

    public String getVouchPIN() {
        return vouchPIN;
    }

    public void setVouchPIN(String vouchPIN) {
        this.vouchPIN = vouchPIN;
    }

    public String getVouchShrtExpDt() {
        return vouchShrtExpDt;
    }

    public void setVouchShrtExpDt(String vouchShrtExpDt) {
        this.vouchShrtExpDt = vouchShrtExpDt;
    }

    public String getVouchSN() {
        return vouchSN;
    }

    public void setVouchSN(String vouchSN) {
        this.vouchSN = vouchSN;
    }

    public String getVouchExpDt() {
        return vouchExpDt;
    }

    public void setVouchExpDt(String vouchExpDt) {
        this.vouchExpDt = vouchExpDt;
    }

}
