package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.List;
import java.util.UUID;

/**
 * Created by qup on 9/11/19.
 */
public class WebhookCreateEnt {
    public String url = "";
    public List<String> fleetIds = null;
    public String fleetId = "";
    public boolean production;

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    public int validateData() throws JsonProcessingException {

        return 200;
    }
    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }
}
