
package com.qupworld.paymentgateway.models.mongo.collections.booking;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class CorporateInfo {

    public String corporateId;
    public String name;
    public String clientCaseMatter;
    public String chargeCode;
    public CreditInfo creditInfo;
    public String corpId;
    public String division;
    public String costCentre;
    public String corpPO;
    public String managerName;
    public String managerEmail;
    public String title;
    public String department;
    public boolean isAirline;
}
