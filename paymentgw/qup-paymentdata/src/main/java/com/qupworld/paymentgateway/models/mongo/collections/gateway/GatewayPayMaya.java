
package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import org.bson.types.ObjectId;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GatewayPayMaya {

    public ObjectId _id;
    public String fleetId;
    public Boolean isSystem;
    public String environment;
    public String publicKey;
    public String secretKey;

}
