
package com.qupworld.paymentgateway.models.mongo.collections.booking;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class CreditInfo {

    public String _id;
    public String cardType = "";
    public String cardMask = "";
    public String cardHolder = "";
    public String localToken = "";
    public String crossToken = "";
    public String localFleetId = "";
    public String gateway = "";
    public String userId; // used for Dispatching send socket to mobile

    // save address info to use later
    public String street = "";
    public String city = "";
    public String state = "";
    public String zipCode = "";
    public String country = "";

    // for Adyen
    public String reference = "";
    public String externalToken = "";

}
