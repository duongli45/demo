
package com.qupworld.paymentgateway.entities.FawryFormEnt;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "bankId",
    "pmtAmt",
    "pmtStatus",
    "pmtMethod",
    "pmtRevInfo",
    "pmtIds",
    "billTypeCode",
    "pmtType",
    "extraBillingAccts",
    "billingAcct",
    "deliveryMethod"
})
public class PmtInfo {

    @JsonProperty("bankId")
    private String bankId;
    @JsonProperty("pmtAmt")
    private PmtAmt pmtAmt;
    @JsonProperty("pmtStatus")
    private String pmtStatus;
    @JsonProperty("pmtMethod")
    private String pmtMethod;
    @JsonProperty("pmtRevInfo")
    private PmtRevInfo pmtRevInfo;
    @JsonProperty("pmtIds")
    private List<PmtId> pmtIds = null;
    @JsonProperty("billTypeCode")
    private Integer billTypeCode;
    @JsonProperty("pmtType")
    private String pmtType;
    @JsonProperty("extraBillingAccts")
    private List<ExtraBillingAcct> extraBillingAccts = null;
    @JsonProperty("billingAcct")
    private String billingAcct;
    @JsonProperty("deliveryMethod")
    private String deliveryMethod;

    @JsonProperty("bankId")
    public String getBankId() {
        return bankId;
    }

    @JsonProperty("bankId")
    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    @JsonProperty("pmtAmt")
    public PmtAmt getPmtAmt() {
        return pmtAmt;
    }

    @JsonProperty("pmtAmt")
    public void setPmtAmt(PmtAmt pmtAmt) {
        this.pmtAmt = pmtAmt;
    }

    @JsonProperty("pmtStatus")
    public String getPmtStatus() {
        return pmtStatus;
    }

    @JsonProperty("pmtStatus")
    public void setPmtStatus(String pmtStatus) {
        this.pmtStatus = pmtStatus;
    }

    @JsonProperty("pmtMethod")
    public String getPmtMethod() {
        return pmtMethod;
    }

    @JsonProperty("pmtMethod")
    public void setPmtMethod(String pmtMethod) {
        this.pmtMethod = pmtMethod;
    }

    @JsonProperty("pmtRevInfo")
    public PmtRevInfo getPmtRevInfo() {
        return pmtRevInfo;
    }

    @JsonProperty("pmtRevInfo")
    public void setPmtRevInfo(PmtRevInfo pmtRevInfo) {
        this.pmtRevInfo = pmtRevInfo;
    }

    @JsonProperty("pmtIds")
    public List<PmtId> getPmtIds() {
        return pmtIds;
    }

    @JsonProperty("pmtIds")
    public void setPmtIds(List<PmtId> pmtIds) {
        this.pmtIds = pmtIds;
    }

    @JsonProperty("billTypeCode")
    public Integer getBillTypeCode() {
        return billTypeCode;
    }

    @JsonProperty("billTypeCode")
    public void setBillTypeCode(Integer billTypeCode) {
        this.billTypeCode = billTypeCode;
    }

    @JsonProperty("pmtType")
    public String getPmtType() {
        return pmtType;
    }

    @JsonProperty("pmtType")
    public void setPmtType(String pmtType) {
        this.pmtType = pmtType;
    }

    @JsonProperty("extraBillingAccts")
    public List<ExtraBillingAcct> getExtraBillingAccts() {
        return extraBillingAccts;
    }

    @JsonProperty("extraBillingAccts")
    public void setExtraBillingAccts(List<ExtraBillingAcct> extraBillingAccts) {
        this.extraBillingAccts = extraBillingAccts;
    }

    @JsonProperty("billingAcct")
    public String getBillingAcct() {
        return billingAcct;
    }

    @JsonProperty("billingAcct")
    public void setBillingAcct(String billingAcct) {
        this.billingAcct = billingAcct;
    }

    @JsonProperty("deliveryMethod")
    public String getDeliveryMethod() {
        return deliveryMethod;
    }

    @JsonProperty("deliveryMethod")
    public void setDeliveryMethod(String deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }

}
