package com.qupworld.paymentgateway.models.mongo.collections.affiliateMarkupPrice;

/**
 * Created by thuanho on 22/10/2022.
 */
public class AffiliateMarkupPrice {

    public String fleetId;
    public MarkupPrice regular;
    public MarkupPrice flat;
    public MarkupPrice hourly;
}

