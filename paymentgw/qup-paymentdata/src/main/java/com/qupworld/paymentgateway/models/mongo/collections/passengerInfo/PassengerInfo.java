package com.qupworld.paymentgateway.models.mongo.collections.passengerInfo;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import javax.annotation.Generated;
import java.util.List;

/**
 * Created by qup on 23/04/2019.
 */
@Generated("org.jsonschema2pojo")
public class PassengerInfo {

    public String fleetId;
    public String userId;
    public List<OutStanding> outStanding;
    public List<AmountByCurrency> paxWallet;
}
