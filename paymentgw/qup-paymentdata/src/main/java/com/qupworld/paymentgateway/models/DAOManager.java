package com.qupworld.paymentgateway.models;

import com.mongodb.*;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.models.mongo.codec.*;
import com.pg.util.CommonUtils;
import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.net.URLEncoder;

import static com.mongodb.MongoClientSettings.getDefaultCodecRegistry;
import static org.bson.codecs.configuration.CodecRegistries.fromCodecs;

/**
 * Created by qup on 7/21/16.
 */
public class DAOManager {
    //Mongo
    private static Object flag = "FLAG";
    private static volatile MongoClient client = null;

    public static MongoDatabase getMongoDB(){
        CodecProvider pojoCodecProvider = PojoCodecProvider.builder().automatic(true).build();
        CodecRegistry pojoCodecRegistry = CodecRegistries.fromRegistries(
                getDefaultCodecRegistry(),
                fromCodecs(new StringArrayCodec()),
                fromCodecs(new ObjectArrayCodec()),
                fromCodecs(new DoubleArrayCodec()),
                fromCodecs(new ObjectCodec()),
                fromCodecs(new NullCodec()),
                fromCodecs(new DoubleCodec()),
                fromCodecs(new IntegerCodec()),
                CodecRegistries.fromProviders(pojoCodecProvider));

        if (client == null) {
            synchronized (flag) {
                if (client == null) {
                    try {
                        String options = "maxPoolSize="+ServerConfig.mongo_maxPoolSize+"&waitQueueMultiple="+ServerConfig.mongo_waitQueueMultiple+"&waitQueueTimeoutMS="+ServerConfig.mongo_waitQueueTimeoutMS;
                        options += "&w="+ServerConfig.mongo_writeConcern+"&readPreference="+ServerConfig.mongo_readPreference;
                        String authen = ServerConfig.mongo_userName+":"+ URLEncoder.encode(ServerConfig.mongo_password, "UTF-8")+"@";
                        String connectURL = "mongodb://" + authen + ServerConfig.mongo_list_host + "/?authSource="+ServerConfig.mongo_authSource+"&authMechanism="+ServerConfig.mongo_authMechanism+"&replicaSet="+ServerConfig.mongo_replicaSet+"&"+options;
                        ConnectionString connection = new ConnectionString(connectURL);
                        MongoClientSettings settings = MongoClientSettings.builder()
                                .codecRegistry(pojoCodecRegistry)
                                .applyConnectionString(connection).build();
                        client =  MongoClients.create(settings);
                    } catch (Exception e) {
                        System.out.println("getMongoDB exception: " + CommonUtils.getError(e));
                    }
                }
            }
        }
        return client.getDatabase(ServerConfig.mongo_database_name).withCodecRegistry(pojoCodecRegistry);
    }

    private static class DAOManagerSingleton {
        // Use ThreadLocal for one instance per thread.
        public static ThreadLocal<DAOManager> INSTANCE;
        static
        {
            ThreadLocal<DAOManager> dm;
            try {
                dm = new ThreadLocal<DAOManager>() {
                    @Override protected DAOManager initialValue() {
                        try {
                            return new DAOManager();
                        } catch (Exception e) {
                            e.printStackTrace();
                            return null;
                        }
                    }
                };
            } catch (Exception e) {
                e.printStackTrace();
                dm = null;
            }
            INSTANCE = dm;
        }
    }

    /**
     * A private Constructor prevents any other class from instantiating.
     *
     */
    public static DAOManager getInstance() {
        return new DAOManager();
    }

    public void close(){
        client.close();
    }
}

