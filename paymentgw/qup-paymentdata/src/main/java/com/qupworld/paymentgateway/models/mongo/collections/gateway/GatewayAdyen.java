
package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import org.bson.types.ObjectId;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GatewayAdyen {

    public ObjectId _id;
    public String fleetId;
    public Boolean isSystem;
    public String environment;
    public String mode; // HPP or CSE
    public String merchantAccount;
    public String username;
    public String password;
    public String cseKey;
    public String hmacKey;
    public String skinCode;
    public Boolean isActive;

}
