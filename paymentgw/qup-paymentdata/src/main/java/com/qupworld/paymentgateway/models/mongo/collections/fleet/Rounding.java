package com.qupworld.paymentgateway.models.mongo.collections.fleet;

import java.util.List;

/**
 * Created by myasus on 11/7/19.
 */
public class Rounding {
    public boolean enable;
    public List<RoundingByCurrency> roundingByCurrencies;
}
