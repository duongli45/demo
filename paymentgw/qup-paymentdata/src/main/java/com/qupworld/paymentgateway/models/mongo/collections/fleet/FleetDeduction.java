
package com.qupworld.paymentgateway.models.mongo.collections.fleet;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class FleetDeduction {

    public Boolean enableTax;
    public String separateTax;
    public TransactionFee transactionFee;
    public boolean commissionBasedOnTotalFare;

}
