
package com.qupworld.paymentgateway.models.mongo.collections.fleetfare;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;
import com.qupworld.paymentgateway.models.mongo.collections.FleetService;
import org.bson.types.ObjectId;

import javax.annotation.Generated;
import java.util.Date;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class FleetFare {

    public ObjectId _id;
    public String _class;
    public String fleetId;
    //public Double heavyTrafficSurcharge;
    public Double tips;
    public CancellationPolicy cancellationPolicy;
    public NoShow noShow;
    public Double commission;
    public Boolean airportActive;
    public Boolean rushHourActive;
    public Boolean heavyTrafficActive;
    public Boolean otherFeeActive;
    public Boolean taxActive;
    public Boolean tipActive;
    public Boolean tollFeeActive;
    public Boolean tollFeeLimitDriverInputActive;
    public List<AmountByCurrency> tollFeeDriverCanInput;
    public int tollFeePayTo; // 1 - fleet | 0 - driver
    public boolean parkingFeeActive;
    public boolean parkingFeeLimitDriverInputActive;
    public List<AmountByCurrency> parkingFeeDriverCanInput;
    public int parkingFeePayTo; // 1 - fleet | 0 - driver
    public boolean gasFeeActive;
    public boolean gasFeeLimitDriverInputActive;
    public List<AmountByCurrency> gasFeeDriverCanInput;
    public int gasFeePayTo; // 1 - fleet | 0 - driver
    public Double tax;
    public Airport airport;
    public MeetDriver meetDriver;
    public OtherFee otherFee;
    public List<RushHour> rushHours;
    public Boolean isActive;
    public Date latestUpdate;
    public Integer v;
    public List<AmountByCurrency> heavyTrafficByCurrencies;
    public String applyType;
    public String defaultFleetCommissionType; // ['sameZones', 'differentZones']
    public FleetCommissionValue defaultFleetCommissionValue;
    public boolean fleetServiceActive;
    public List<FleetService> fleetServices;
    public DriverCancelPolicy driverCancelPolicy;
    public boolean additionalServicesActive;
}
