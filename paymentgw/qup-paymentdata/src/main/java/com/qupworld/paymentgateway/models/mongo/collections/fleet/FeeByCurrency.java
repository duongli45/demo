
package com.qupworld.paymentgateway.models.mongo.collections.fleet;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class FeeByCurrency {

    public String currencyISO;
    public Double creditCardAmount;
    public Double creditCardPercent;
    public Double directBillingAmount;
    public Double directBillingPercent;
    public Double externalCardAmount;
    public Double externalCardPercent;
    public Double b2BTerminalAmount;
    public Double b2BTerminalPercent;

}
