package com.qupworld.paymentgateway.entities.PayWay;

/**
 * Created by thuanho on 29/09/2023.
 */
public class PayWayCredit {

    public String status;
    public String pwt;
    public String mask_pan;
    public String card_type;
}
