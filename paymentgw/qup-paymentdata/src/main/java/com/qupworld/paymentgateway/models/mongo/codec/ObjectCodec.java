package com.qupworld.paymentgateway.models.mongo.codec;

import org.bson.BsonReader;
import org.bson.BsonType;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

import java.util.ArrayList;
import java.util.List;

public class ObjectCodec implements Codec<Object> {

    @Override
    public void encode(BsonWriter writer, Object value, EncoderContext encoderContext) {
        //writer.writeStartArray();
        if (value == null) {
            writer.writeNull();
        } else if (value instanceof String) {
            writer.writeString((String) value);
        } else if (value instanceof Integer) {
            writer.writeInt32((Integer) value);
        } else if (value instanceof Double) {
            writer.writeDouble((Double) value);
            /*} else if (obj instanceof JSONObject) {
                writer.write*/
        }
        //writer.writeEndArray();
    }

    @Override
    public Object decode(BsonReader reader, DecoderContext decoderContext) {
        switch (reader.getCurrentBsonType()) {
            case NULL:
                return null;
            case STRING:
                return reader.readString();
            case INT32:
                return reader.readInt32();
            case DOUBLE:
                return reader.readDouble();
            // Add cases for other types as needed
            default:
                //reader.readEndArray();
                throw new IllegalArgumentException("Unsupported BSON type: " + reader.getCurrentBsonType());
        }
    }

    @Override
    public Class<Object> getEncoderClass() {
        return Object.class;
    }

}
