package com.qupworld.paymentgateway.entities;

import java.util.List;
import java.util.UUID;

/**
 * Created by thuanho on 28/09/2020.
 */
public class SyncEnt {

    public String fleetId;
    public double balance;
    public List<String> listDriverTypes;
    public String driverId;
    public String passengerId;
    public String reason;
    public String currencyISO;

    // for ResetExpiredBonusThread
    public String topUpDate;
    // for update wallet balance

    public String walletType = "";

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }
}
