package com.qupworld.paymentgateway.models.mongo.collections.AffiliateSetting;

/**
 * Created by thuanho on 20/10/2022.
 */
public class CancellationPolicy {

    public OnDemand onDemand;
    public Reservation reservation;
    public NoShow noShow;
}
