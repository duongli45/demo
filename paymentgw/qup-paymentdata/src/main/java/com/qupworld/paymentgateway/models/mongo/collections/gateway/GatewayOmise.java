
package com.qupworld.paymentgateway.models.mongo.collections.gateway;


import org.bson.types.ObjectId;

public class GatewayOmise {

    public ObjectId _id;
    public String fleetId;
    public Boolean isSystem;
    public String environment;
    public String publicKey;
    public String secretKey;
    public Boolean isActive;

}
