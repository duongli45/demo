package com.qupworld.paymentgateway.entities;

import java.util.UUID;

/**
 * Created by qup on 06/08/2019.
 */
public class TnGNotification {

    public TnGRequest request;
    public String signature;

    // for log
    public String requestId = "";

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }
}
