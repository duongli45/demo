package com.qupworld.paymentgateway.models.mongo.collections.booking;

/**
 * Created by hoangnguyen on 12/12/17.
 */
public class Service {
    public String serviceId;
    public String name;
    public String type;
    public double fee;
    public boolean active;
}
