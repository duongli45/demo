package com.qupworld.paymentgateway.entities;

import java.sql.Timestamp;

/**
 * Created by qup on 20/04/2020.
 */
public class IntercityTripEnt {

    public String tripId;
    public String fleetId;
    public Integer noBookings;
    public String bookIds;
    public String routeName;
    public Timestamp pickUpTime;
    public String status;
    public Double fare;
    public String currencyISO;
    public String currencySymbol;
    public Timestamp createdDate;
}
