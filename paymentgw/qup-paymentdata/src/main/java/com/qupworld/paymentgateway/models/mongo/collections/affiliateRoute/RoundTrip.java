package com.qupworld.paymentgateway.models.mongo.collections.affiliateRoute;

/**
 * Created by hoangnguyen on 7/26/17.
 */
public class RoundTrip {
    public Double roundTripFee;
    public Boolean enableRoundTrip;
    public Limitation limitation;
}
