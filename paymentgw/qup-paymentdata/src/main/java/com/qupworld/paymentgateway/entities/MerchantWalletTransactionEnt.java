package com.qupworld.paymentgateway.entities;

import java.util.UUID;

/**
 * Created by thuanho on 06/05/2022.
 */
public class MerchantWalletTransactionEnt {

    public String fleetId;
    public String merchantId;
    public String walletType;
    public double amount;
    public String currencyISO;
    public String from;
    public String operatorId;
    public String reason;

    // for log
    public String requestId = "";

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }
}
