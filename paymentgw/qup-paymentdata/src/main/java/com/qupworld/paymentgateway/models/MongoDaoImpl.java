package com.qupworld.paymentgateway.models;

import com.google.gson.Gson;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.model.Updates;
import com.mongodb.client.model.geojson.Point;
import com.mongodb.client.model.geojson.Position;
import com.mongodb.client.result.InsertOneResult;
import com.mongodb.client.result.UpdateResult;
import com.qupworld.paymentgateway.entities.ACHEnt;
import com.qupworld.paymentgateway.entities.CreditDeleteEnt;
import com.qupworld.paymentgateway.entities.CreditEnt;
import com.qupworld.paymentgateway.entities.Process;
import com.qupworld.paymentgateway.models.mongo.collections.AffiliateSetting.AffiliateSetting;
import com.qupworld.paymentgateway.models.mongo.collections.*;
import com.qupworld.paymentgateway.models.mongo.collections.Logs.Logs;
import com.qupworld.paymentgateway.models.mongo.collections.account.Account;
import com.qupworld.paymentgateway.models.mongo.collections.account.UpdateBalanceId;
import com.qupworld.paymentgateway.models.mongo.collections.additionalServices.AdditionalServices;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateAssignedRate.AffiliateAssignedRate;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareFlat.AffiliateFlat;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareGeneral.RateGeneral;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareHourly.AffiliateHourly;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateFareRegular.RateRegular;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateMarkupPrice.AffiliateMarkupPrice;
import com.qupworld.paymentgateway.models.mongo.collections.affiliatePackages.AffiliatePackages;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateProcess.AffiliateProcess;
import com.qupworld.paymentgateway.models.mongo.collections.affiliateRoute.AffiliateRoute;
import com.qupworld.paymentgateway.models.mongo.collections.affiliationCarType.AffiliationCarType;
import com.qupworld.paymentgateway.models.mongo.collections.airportFee.AirportFee;
import com.qupworld.paymentgateway.models.mongo.collections.airportZone.AirportZone;
import com.qupworld.paymentgateway.models.mongo.collections.bankInfo.BankInfo;
import com.qupworld.paymentgateway.models.mongo.collections.booking.Booking;
import com.qupworld.paymentgateway.models.mongo.collections.bookingStatistic.BookingStatistic;
import com.qupworld.paymentgateway.models.mongo.collections.ccPackages.CCPackages;
import com.qupworld.paymentgateway.models.mongo.collections.company.Company;
import com.qupworld.paymentgateway.models.mongo.collections.configGateway.ConfigGateway;
import com.qupworld.paymentgateway.models.mongo.collections.corpItinerary.CorpItinerary;
import com.qupworld.paymentgateway.models.mongo.collections.corporate.Corporate;
import com.qupworld.paymentgateway.models.mongo.collections.driverFields.DriverFields;
import com.qupworld.paymentgateway.models.mongo.collections.dynamicFare.DynamicFare;
import com.qupworld.paymentgateway.models.mongo.collections.dynamicSurcharge.DynamicSurcharge;
import com.qupworld.paymentgateway.models.mongo.collections.failedCard.FailedCard;
import com.qupworld.paymentgateway.models.mongo.collections.fareDelivery.FareDelivery;
import com.qupworld.paymentgateway.models.mongo.collections.fareFlat.FareFlat;
import com.qupworld.paymentgateway.models.mongo.collections.fareHourly.FareHourly;
import com.qupworld.paymentgateway.models.mongo.collections.fareNormal.FareNormal;
import com.qupworld.paymentgateway.models.mongo.collections.fareSharing.FareSharing;
import com.qupworld.paymentgateway.models.mongo.collections.flatRoutes.FlatRoutes;
import com.qupworld.paymentgateway.models.mongo.collections.fleet.Fleet;
import com.qupworld.paymentgateway.models.mongo.collections.fleetfare.FleetFare;
import com.qupworld.paymentgateway.models.mongo.collections.gateway.*;
import com.qupworld.paymentgateway.models.mongo.collections.intercityRoute.IntercityRoute;
import com.qupworld.paymentgateway.models.mongo.collections.intercityTrip.Trip;
import com.qupworld.paymentgateway.models.mongo.collections.invoice.Invoice;
import com.qupworld.paymentgateway.models.mongo.collections.logToken.LogToken;
import com.qupworld.paymentgateway.models.mongo.collections.menuMerchant.MenuMerchant;
import com.qupworld.paymentgateway.models.mongo.collections.menuMerchantUser.MenuMerchantUser;
import com.qupworld.paymentgateway.models.mongo.collections.operations.Operations;
import com.qupworld.paymentgateway.models.mongo.collections.packageRate.PackageRate;
import com.qupworld.paymentgateway.models.mongo.collections.passengerInfo.OutStanding;
import com.qupworld.paymentgateway.models.mongo.collections.passengerInfo.PassengerInfo;
import com.qupworld.paymentgateway.models.mongo.collections.paxPromoList.PaxPromoList;
import com.qupworld.paymentgateway.models.mongo.collections.paxReferral.PaxReferral;
import com.qupworld.paymentgateway.models.mongo.collections.payoutTime.PayoutTime;
import com.qupworld.paymentgateway.models.mongo.collections.phoneZip.PhoneZip;
import com.qupworld.paymentgateway.models.mongo.collections.pricingplan.PricingPlan;
import com.qupworld.paymentgateway.models.mongo.collections.profile.Profile;
import com.qupworld.paymentgateway.models.mongo.collections.promotionCode.PromotionCode;
import com.qupworld.paymentgateway.models.mongo.collections.referral.Referral;
import com.qupworld.paymentgateway.models.mongo.collections.referral.ReferralHistories;
import com.qupworld.paymentgateway.models.mongo.collections.referral.ReferralInfo;
import com.qupworld.paymentgateway.models.mongo.collections.serverInformation.ServerInformation;
import com.qupworld.paymentgateway.models.mongo.collections.serviceFee.ServiceFee;
import com.qupworld.paymentgateway.models.mongo.collections.tripNotification.TripNotification;
import com.qupworld.paymentgateway.models.mongo.collections.updateHistory.UpdateHistory;
import com.qupworld.paymentgateway.models.mongo.collections.vehicleType.VehicleType;
import com.qupworld.paymentgateway.models.mongo.collections.voucher.VoucherCode;
import com.qupworld.paymentgateway.models.mongo.collections.wallet.*;
import com.qupworld.paymentgateway.models.mongo.collections.zone.Zone;
import com.qupworld.paymentgateway.models.mongo.collections.zonePayment.ZonePayment;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.TimezoneUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.JSONObject;
import org.json.simple.JSONArray;

import java.util.*;

/**
 * Created by qup on 7/13/16.
 * Data Access Layer
 */
public class MongoDaoImpl implements MongoDao {


    final static Logger logger = LogManager.getLogger(MongoDaoImpl.class);
    private MongoDatabase database;
    private Gson gson;
    public MongoDaoImpl() {
        try {
            database = DAOManager.getMongoDB();
//            database = TestConnect.getMongoDB();
            gson = new Gson();
        } catch (Exception e) {
            // Handle errors for Exception
            e.printStackTrace();
        }

    }
    public Fleet getFleetInfor (String fleetId){
        MongoCollection<Fleet> collection = database.getCollection("Fleet", Fleet.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }
    public GatewayJetpay getGatewayJetpayFleet(String fleetId){
        MongoCollection<GatewayJetpay> collection = database.getCollection("GatewayJetpay", GatewayJetpay.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }
    public GatewayJetpay getGatewayJetpaySystem(boolean isSanbox){
        MongoCollection<GatewayJetpay> collection = database.getCollection("GatewayJetpay", GatewayJetpay.class);
        Bson filter;
        if (isSanbox) {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "sandbox")
            );
        } else {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "production")
            );
        }
        return collection.find(filter).first();
    }
    public GatewayCielo getGatewayCieloFleet(String fleetId){
        MongoCollection<GatewayCielo> collection = database.getCollection("GatewayCielo", GatewayCielo.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }
    public GatewayCielo getGatewayCieloSystem(boolean isSanbox){
        MongoCollection<GatewayCielo> collection = database.getCollection("GatewayCielo", GatewayCielo.class);
        Bson filter;
        if (isSanbox) {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "sandbox")
            );
        } else {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "production")
            );
        }
        return collection.find(filter).first();
    }
    public GatewayPeachPayments getGatewayPeachPaymentsFleet(String fleetId){
        MongoCollection<GatewayPeachPayments> collection = database.getCollection("GatewayPeachPayments", GatewayPeachPayments.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }
    public GatewayPeachPayments getGatewayPeachPaymentsSystem(boolean isSanbox){
        MongoCollection<GatewayPeachPayments> collection = database.getCollection("GatewayPeachPayments", GatewayPeachPayments.class);
        Bson filter;
        if (isSanbox) {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "sandbox")
            );
        } else {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "production")
            );
        }
        return collection.find(filter).first();
    }
    public GatewayAuthorize getGatewayAuthorizeFleet(String fleetId){
        MongoCollection<GatewayAuthorize> collection = database.getCollection("GatewayAuthorize", GatewayAuthorize.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }
    public GatewayAuthorize getGatewayAuthorizeSystem(boolean isSanbox){
        MongoCollection<GatewayAuthorize> collection = database.getCollection("GatewayAuthorize", GatewayAuthorize.class);
        Bson filter;
        if (isSanbox) {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "sandbox")
            );
        } else {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "production")
            );
        }
        return collection.find(filter).first();
    }
    public GatewayBluefin getGatewayBluefinFleet(String fleetId){
        MongoCollection<GatewayBluefin> collection = database.getCollection("GatewayBluefin", GatewayBluefin.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }
    public GatewayBluefin getGatewayBluefinSystem(boolean isSanbox){
        MongoCollection<GatewayBluefin> collection = database.getCollection("GatewayBluefin", GatewayBluefin.class);
        Bson filter;
        if (isSanbox) {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "sandbox")
            );
        } else {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "production")
            );
        }
        return collection.find(filter).first();
    }
    public GatewayCreditCorp getGatewayCreditCorpFleet(String fleetId){
        MongoCollection<GatewayCreditCorp> collection = database.getCollection("GatewayCreditCorp", GatewayCreditCorp.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }
    public GatewayCXPay getGatewayCXPayFleet(String fleetId) {
        MongoCollection<GatewayCXPay> collection = database.getCollection("GatewayCXPay", GatewayCXPay.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }
    public GatewayFirstAtlanticCommerce getGatewayFACFleet(String fleetId) {
        MongoCollection<GatewayFirstAtlanticCommerce> collection = database.getCollection("GatewayFirstAtlanticCommerce", GatewayFirstAtlanticCommerce.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }
    public GatewayPayfort getGatewayPayfortFleet(String fleetId) {
        MongoCollection<GatewayPayfort> collection = database.getCollection("GatewayPayfort", GatewayPayfort.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }
    public GatewayOmise getGatewayOmiseFleet(String fleetId){
        MongoCollection<GatewayOmise> collection = database.getCollection("GatewayOmise", GatewayOmise.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }
    public GatewayPayCorp getGatewayPayCorpFleet(String fleetId) {
        MongoCollection<GatewayPayCorp> collection = database.getCollection("GatewayPayCorp", GatewayPayCorp.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }
    public GatewayMasapay getGatewayMasapayFleet(String fleetId){
        MongoCollection<GatewayMasapay> collection = database.getCollection("GatewayMasapay", GatewayMasapay.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }
    public GatewayPingPong getGatewayPingPongFleet(String fleetId){
        MongoCollection<GatewayPingPong> collection = database.getCollection("GatewayPingPong", GatewayPingPong.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }
    public GatewayPayU getGatewayPayUFleet(String fleetId){
        MongoCollection<GatewayPayU> collection = database.getCollection("GatewayPayU", GatewayPayU.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }
    public GatewayExpresspay getGatewayExpresspayFleet(String fleetId) {
        MongoCollection<GatewayExpresspay> collection = database.getCollection("GatewayExpresspay", GatewayExpresspay.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }
    public GatewayFlutterwave getGatewayFlutterwaveFleet(String fleetId) {
        MongoCollection<GatewayFlutterwave> collection = database.getCollection("GatewayFlutterwave", GatewayFlutterwave.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }
    public  GatewayStripe getGatewayStripeFleet(String fleetId){
        MongoCollection<GatewayStripe> collection = database.getCollection("GatewayStripe", GatewayStripe.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }
    public  GatewayStripe getGatewayStripeSystem(boolean isSanbox){
        MongoCollection<GatewayStripe> collection = database.getCollection("GatewayStripe", GatewayStripe.class);
        Bson filter;
        if (isSanbox) {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "sandbox")
            );
        } else {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "production")
            );
        }
        return collection.find(filter).first();
    }

    public  GatewayBraintree getGatewayBraintreeFleet(String fleetId){
        MongoCollection<GatewayBraintree> collection = database.getCollection("GatewayBraintree", GatewayBraintree.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }
    public  GatewayBraintree getGatewayBraintreeSystem(boolean isSanbox){
        MongoCollection<GatewayBraintree> collection = database.getCollection("GatewayBraintree", GatewayBraintree.class);
        Bson filter;
        if (isSanbox) {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "sandbox")
            );
        } else {
            filter = Filters.and(
                    Filters.eq("isSystem", true),
                    Filters.eq("environment", "production")
            );
        }
        return collection.find(filter).first();
    }


    public GatewayYeepay getGatewayYeepayFleet(String fleetId) {
        MongoCollection<GatewayYeepay> collection = database.getCollection("GatewayYeepay", GatewayYeepay.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public GatewayAdyen getGatewayAdyenFleet(String fleetId) {
        MongoCollection<GatewayAdyen> collection = database.getCollection("GatewayAdyen", GatewayAdyen.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public GatewayConekta getGatewayConektaFleet(String fleetId) {
        MongoCollection<GatewayConekta> collection = database.getCollection("GatewayConekta", GatewayConekta.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public GatewaySenangpay getGatewaySenangpayFleet(String fleetId) {
        MongoCollection<GatewaySenangpay> collection = database.getCollection("GatewaySenangpay", GatewaySenangpay.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public GatewayPayDollar getGatewayPayDollarFleet(String fleetId) {
        MongoCollection<GatewayPayDollar> collection = database.getCollection("GatewayPayDollar", GatewayPayDollar.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public GatewayEGHL getGatewayPayEGHLFleet(String fleetId) {
        MongoCollection<GatewayEGHL> collection = database.getCollection("GatewayEGHL", GatewayEGHL.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public  boolean getPayToFleet(String fleetId){
        MongoCollection<PricingPlan> collection = database.getCollection("PricingPlan", PricingPlan.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        PricingPlan pricingPlan =  collection.find(filter).first();
        return  Boolean.parseBoolean(String.valueOf(pricingPlan.withinZone.ccPayToFleet));
    }
    public  PricingPlan getPricingPlan(String fleetId){
        MongoCollection<PricingPlan> collection = database.getCollection("PricingPlan", PricingPlan.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }
    public  boolean addCreditCard(String credit, CreditEnt cardInfor, String type){
        Bson update = Updates.push("credits", gson.fromJson(credit, org.json.simple.JSONObject.class));
        if (type.equals("user") || type.equals("customer")) {
            MongoCollection<Account> collection = database.getCollection("Account", Account.class);
            Bson filter = Filters.eq("userId", cardInfor.userId);

            // Defines options that configure the operation to return a document in its post-operation state
            /*FindOneAndUpdateOptions options = new FindOneAndUpdateOptions()
                    .returnDocument(ReturnDocument.AFTER);*/
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        }
        if (type.equals("corporate")){
            MongoCollection<Corporate> collection = database.getCollection("Corporate", Corporate.class);
            Bson filter = Filters.eq("_id", new ObjectId(cardInfor.userId));
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        }
        if (type.equals("fleetOwner")){
            MongoCollection<Fleet> collection = database.getCollection("Fleet", Fleet.class);
            Bson filter = Filters.eq("fleetId", cardInfor.fleetId);
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        }
        return false;
    }

    public Credit getCreditByToken(String userId,String localToken,String crossToken, String gateway){
        MongoCollection<Account> collection = database.getCollection("Account", Account.class);
        Bson filter = Filters.eq("userId", userId);
        Account account = collection.find(filter).first();
        if (account != null && account.credits != null && !account.credits.isEmpty()) {
            account.credits.stream().filter(credit -> credit.localToken.equals(localToken) && credit.gateway.equals(gateway))
                    .findFirst().get();
        }
        return null;
    }
    public boolean deleteCreditByToke(CreditDeleteEnt creditDelete,String gateway){
        MongoCollection<Account> collection = database.getCollection("Account", Account.class);
        Bson filter = Filters.eq("userId", creditDelete.userId);
        org.json.simple.JSONObject objCredit = new org.json.simple.JSONObject();
        objCredit.put("localToken", creditDelete.localToken);
        objCredit.put("crossToken", creditDelete.crossToken);
        objCredit.put("gateway", gateway);
        Bson update = Updates.pull("credits", objCredit);
        // Defines options that configure the operation to return a document in its post-operation state
            /*FindOneAndUpdateOptions options = new FindOneAndUpdateOptions()
                    .returnDocument(ReturnDocument.AFTER);*/
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public Credit getCreditCorporateByToken(CreditDeleteEnt creditDelete, String gateway){
        MongoCollection<Corporate> collection = database.getCollection("Corporate", Corporate.class);
        Bson filter = Filters.eq("_id", new ObjectId(creditDelete.userId));
        Corporate account = collection.find(filter).first();
        if (account != null && account.credits != null && !account.credits.isEmpty()) {
            account.credits.stream().filter(credit -> credit.localToken.equals(creditDelete.localToken) && credit.crossToken.equals(creditDelete.crossToken) && credit.gateway.equals(gateway))
                    .findFirst().get();
        }
        return null;
    }
    public boolean deleteCreditCorporate(CreditDeleteEnt creditDelete,String gateway){
        MongoCollection<Corporate> collection = database.getCollection("Corporate", Corporate.class);
        Bson filter = Filters.eq("_id", new ObjectId(creditDelete.userId));
        org.json.simple.JSONObject objCredit = new org.json.simple.JSONObject();
        objCredit.put("localToken", creditDelete.localToken);
        objCredit.put("crossToken", creditDelete.crossToken);
        objCredit.put("gateway", gateway);
        Bson update = Updates.pull("credits", objCredit);
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }
    public boolean deleteCreditFleetOwner(CreditDeleteEnt creditDelete,String gateway){
        MongoCollection<Fleet> collection = database.getCollection("Fleet", Fleet.class);
        Bson filter = Filters.eq("fleetId", creditDelete.fleetId);
        org.json.simple.JSONObject objCredit = new org.json.simple.JSONObject();
        objCredit.put("localToken", creditDelete.localToken);
        objCredit.put("crossToken", creditDelete.crossToken);
        objCredit.put("gateway", gateway);
        Bson update = Updates.pull("credits", objCredit);
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }
    public List<Credit> getCreditByUserId(String userId){
        MongoCollection<Account> collection = database.getCollection("Account", Account.class);
        Bson filter = Filters.eq("userId", userId);
        return collection.find(filter).first().credits;
    }

    public  boolean updateCreditCards(String userId, List<Credit> credis){
        MongoCollection<Account> collection = database.getCollection("Account", Account.class);
        Bson filter = Filters.eq("userId", userId);
        Bson update = Updates.set("credits", credis);

        // Updates all documents and prints the number of matched and modified documents
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public void updateCreditCards(String cardOwner, String id, List<Credit> credits){
        Bson update = Updates.set("credits", credits);
        if (cardOwner.equals("user") || cardOwner.equals("customer") || cardOwner.equals("passenger") || cardOwner.equals("mDispatcher")
                || cardOwner.equals("driver")) {
            MongoCollection<Account> collection = database.getCollection("Account", Account.class);
            Bson filter = Filters.eq("userId", id);
            collection.updateOne(filter, update);
        }
        if (cardOwner.equals("corporate")){
            MongoCollection<Corporate> collection = database.getCollection("Corporate", Corporate.class);
            Bson filter = Filters.eq("_id", new ObjectId(id));
            collection.updateOne(filter, update);
        }
        if (cardOwner.equals("fleetOwner")){
            MongoCollection<Fleet> collection = database.getCollection("Fleet", Fleet.class);
            Bson filter = Filters.eq("fleetId", id);
            collection.updateOne(filter, update);
        }
    }
    public  boolean addProfile(Profile profile){
        MongoCollection<Profile> collection = database.getCollection("Profile", Profile.class);
        InsertOneResult result = collection.insertOne(profile);
        return result.wasAcknowledged();
    }
    public Booking getBooking(String bookId){
        MongoCollection<Booking> collection = database.getCollection("Booking", Booking.class);
        Bson filter = Filters.eq("bookId", bookId);
        return collection.find(filter).first();
    }

    public Booking getBookingCompleted(String bookId){
        MongoCollection<Booking> collection = database.getCollection("BookingCompleted", Booking.class);
        Bson filter = Filters.eq("bookId", bookId);
        return collection.find(filter).first();
    }

    public FleetFare getFleetFare(String fleetId){
        MongoCollection<FleetFare> collection = database.getCollection("FleetFare", FleetFare.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }
    public Account getAccount(String id){
        if (id != null && !id.isEmpty()) {
            MongoCollection<Account> collection = database.getCollection("Account", Account.class);
            Bson filter = Filters.eq("_id", new ObjectId(id));
            return collection.find(filter).first();
        } else {
            return null;
        }
    }

    public Account getAccountByPhoneAndFleetAndType(String phone, String fleetId, String type){
        MongoCollection<Account> collection = database.getCollection("Account", Account.class);
        Bson filter = Filters.and(
                Filters.eq("phone", phone),
                Filters.eq("fleetId", fleetId),
                Filters.eq("appType", type)
        );
        return collection.find(filter).first();
    }

    public QueuingArea getQueuingArea(String id){
        try {
            MongoCollection<QueuingArea> collection = database.getCollection("QueuingArea", QueuingArea.class);
            Bson filter = Filters.eq("_id", new ObjectId(id));
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public Corporate getCorporate(String id) {
        try {
            MongoCollection<Corporate> collection = database.getCollection("Corporate", Corporate.class);
            Bson filter = Filters.eq("_id", new ObjectId(id));
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }
    public boolean updateCorporatePrePaid(String id,double balance){
        MongoCollection<Corporate> collection = database.getCollection("Corporate", Corporate.class);
        Bson filter = Filters.and(
                Filters.eq("_id", new ObjectId(id)),
                Filters.eq("paymentMethods.type", "prepaid")
        );
        Bson update = Updates.set("paymentMethods.$.balance", balance);
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }
    public UsersSystem getUsersSystem(String id) {
        try {
            MongoCollection<UsersSystem> collection = database.getCollection("UsersSystem", UsersSystem.class);
            Bson filter = Filters.eq("_id", new ObjectId(id));
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public Vehicle getVehicle(String id) {
        try {
            MongoCollection<Vehicle> collection = database.getCollection("Vehicle", Vehicle.class);
            Bson filter = Filters.eq("_id", new ObjectId(id));
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public Roles getRoles(String id) {
        try{
            MongoCollection<Roles> collection = database.getCollection("Roles", Roles.class);
            Bson filter = Filters.eq("_id", new ObjectId(id));
            return collection.find(filter).first();
        }catch (Exception ex){
            return null;
        }
    }

    public ServerInformation getServerInformation() {
        MongoCollection<ServerInformation> collection = database.getCollection("ServerInformation", ServerInformation.class);
        return collection.find().first();
    }

    public VehicleType getVehicleTypeByFleetAndName(String fleetId, String name){
        try {
            MongoCollection<VehicleType> collection = database.getCollection("VehicleType", VehicleType.class);
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("vehicleType", name)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }
    public List<VehicleType> getVehicleTypesByFleet(String fleetId){
        try {
            MongoCollection<VehicleType> collection = database.getCollection("VehicleType", VehicleType.class);
            Bson filter = Filters.eq("fleetId", fleetId);
            MongoCursor<VehicleType> cursor  = collection.find(filter).iterator();
            List<VehicleType> vehicleTypes = new ArrayList<>();
            while(cursor.hasNext()) {
                VehicleType type = cursor.next();
                vehicleTypes.add(type);
            }
            return vehicleTypes;
        } catch(Exception ex) {
            return null;
        }
    }
    public VehicleType getVehicleTypeByFleetAndNameAndRate(String fleetId, String name, String rateType, String zoneId){
        try {
            MongoCollection<VehicleType> collection = database.getCollection("VehicleType", VehicleType.class);
            Bson filterRate = Filters.and(
                    Filters.eq("rateType", rateType),
                    Filters.eq("zoneId", zoneId)
            );
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("vehicleType", name),
                    Filters.elemMatch("rates", filterRate)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public FareHourly getFareHourlyById(String fareHourlyId) {
        try {
            MongoCollection<FareHourly> collection = database.getCollection("FareHourly", FareHourly.class);
            Bson filter = Filters.eq("_id", new ObjectId(fareHourlyId));
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public FareFlat getFareFlatById(String fareFlatId) {
        try {
            MongoCollection<FareFlat> collection = database.getCollection("FareFlat", FareFlat.class);
            Bson filter = Filters.eq("_id", new ObjectId(fareFlatId));
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public FareNormal getFareNormalById(String fareNormalId) {
        try {
            MongoCollection<FareNormal> collection = database.getCollection("FareNormal", FareNormal.class);
            Bson filter = Filters.eq("_id", new ObjectId(fareNormalId));
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public FareDelivery getFareDeliveryById(String fareDeliveryId) {
        try {
            MongoCollection<FareDelivery> collection = database.getCollection("FareDelivery", FareDelivery.class);
            Bson filter = Filters.eq("_id", new ObjectId(fareDeliveryId));
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public FareSharing getFareSharingById(String fareSharingId) {
        try {
            MongoCollection<FareSharing> collection = database.getCollection("FareSharing", FareSharing.class);
            Bson filter = Filters.eq("_id", new ObjectId(fareSharingId));
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }


    public PackageRate getPackageRateById(String packageId) {
        try {
            MongoCollection<PackageRate> collection = database.getCollection("PackageRate", PackageRate.class);
            Bson filter = Filters.eq("_id", new ObjectId(packageId));
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public List<PackageRate> findPackageRateByFareHourlyId(String fareHourlyId) {
        try {
            MongoCollection<PackageRate> collection = database.getCollection("PackageRate", PackageRate.class);
            Bson filter = Filters.eq("fareHourlyId", fareHourlyId);
            Bson sort = Sorts.ascending("createdDate");
            MongoCursor<PackageRate> iterator = collection.find(filter).sort(sort).iterator();
            List<PackageRate> packageRates = new ArrayList<>();
            while(iterator.hasNext()) {
                PackageRate rate = iterator.next();
                packageRates.add(rate);
            }
            return packageRates;
        } catch(Exception ex) {
            return null;
        }
    }

    public RateRegular getAffiliateRate(String rateId) {
        try {
            MongoCollection<RateRegular> collection = database.getCollection("AffiliateRate", RateRegular.class);
            Bson filter = Filters.eq("_id", new ObjectId(rateId));
            return collection.find(filter).first();
            /*MongoCollection collection = jongo.getCollection("AffiliateRate");
            return collection.findOne(Oid.withOid(rateId)).as(RateRegular.class);*/
        } catch(Exception ex) {
            return null;
        }
    }
    public RateGeneral getAffiliateRateGeneral(String rateId){
        try {
            MongoCollection<RateGeneral> collection = database.getCollection("AffiliateRate", RateGeneral.class);
            Bson filter = Filters.eq("_id", new ObjectId(rateId));
            return collection.find(filter).first();
            /*MongoCollection collection = jongo.getCollection("AffiliateRate");
            return collection.findOne(Oid.withOid(rateId)).as(RateGeneral.class);*/
        } catch(Exception ex) {
            return null;
        }
    }
    public AffiliationCarType getAffiliationCarTypeByName(String name) {
        try {
            MongoCollection<AffiliationCarType> collection = database.getCollection("AffiliationCarType", AffiliationCarType.class);
            Bson filter = Filters.eq("name", name);
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }
    public AffiliationCarType getAffiliationCarType(String id) {
        try {
            MongoCollection<AffiliationCarType> collection = database.getCollection("AffiliationCarType", AffiliationCarType.class);
            Bson filter = Filters.eq("_id", new ObjectId(id));
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public List<AffiliateAssignedRate> getListAffiliateAssignedRate(String carTypeId, String zoneId) {
        try {
            MongoCollection<AffiliateAssignedRate> collection = database.getCollection("AffiliateAssignedRate", AffiliateAssignedRate.class);
            Bson filter = Filters.and(
                    Filters.eq("carTypeId", carTypeId),
                    Filters.eq("zone.id", zoneId)
            );
            MongoCursor<AffiliateAssignedRate> iterator = collection.find(filter).iterator();
            List<AffiliateAssignedRate> listAssignedrate = new ArrayList<>();
            while(iterator.hasNext()) {
                AffiliateAssignedRate assignedRate = iterator.next();
                listAssignedrate.add(assignedRate);
            }
            return listAssignedrate;
        } catch(Exception ex) {
            return null;
        }
    }

    public List<AffiliateAssignedRate> findAffiliateFlatRateByCarType(String carTypeId) {
        try {
            MongoCollection<AffiliateAssignedRate> collection = database.getCollection("AffiliateAssignedRate", AffiliateAssignedRate.class);
            Bson filter = Filters.and(
                    Filters.eq("carTypeId", carTypeId),
                    Filters.eq("type", "Flat")
            );
            MongoCursor<AffiliateAssignedRate> iterator = collection.find(filter).iterator();
            List<AffiliateAssignedRate> listAssignedrate = new ArrayList<>();
            while(iterator.hasNext()) {
                AffiliateAssignedRate assignedRate = iterator.next();
                listAssignedrate.add(assignedRate);
            }
            return listAssignedrate;
        } catch(Exception ex) {
            return null;
        }
    }

    public List<AffiliateAssignedRate> findAffiliateHourlyRateByCarTypeAndFleetId(String carTypeId, String fleetId) {
        try {
            MongoCollection<AffiliateAssignedRate> collection = database.getCollection("AffiliateAssignedRate", AffiliateAssignedRate.class);
            Bson filter = Filters.and(
                    Filters.eq("carTypeId", carTypeId),
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("type", "Hourly")
            );
            MongoCursor<AffiliateAssignedRate> iterator = collection.find(filter).iterator();
            List<AffiliateAssignedRate> listAssignedrate = new ArrayList<>();
            while(iterator.hasNext()) {
                AffiliateAssignedRate assignedRate = iterator.next();
                listAssignedrate.add(assignedRate);
            }
            return listAssignedrate;
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateAssignedRate findRateByCarTypeIdAndZoneIdAndType(String carTypeId, String zoneId, String type, String rateType) {
        try {
            MongoCollection<AffiliateAssignedRate> collection = database.getCollection("AffiliateAssignedRate", AffiliateAssignedRate.class);
            Bson filter = Filters.and(
                    Filters.eq("fleetId", ""),
                    Filters.eq("carTypeId", carTypeId),
                    Filters.eq("zone.id", zoneId),
                    Filters.eq("type", type),
                    Filters.eq("priceType", rateType)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateAssignedRate findRateByCarTypeIdAndZoneIdAndTypeAndFleetId(String carTypeId, String zoneId, String fleetId, String type, String rateType) {
        try {
            MongoCollection<AffiliateAssignedRate> collection = database.getCollection("AffiliateAssignedRate", AffiliateAssignedRate.class);
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("carTypeId", carTypeId),
                    Filters.eq("zone.id", zoneId),
                    Filters.eq("type", type),
                    Filters.eq("priceType", rateType)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateHourly findAffiliateHourlyById (String fareHourlyId) {
        try {
            MongoCollection<AffiliateHourly> collection = database.getCollection("AffiliateHourly", AffiliateHourly.class);
            Bson filter = Filters.eq("_id", new ObjectId(fareHourlyId));
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateFlat findAffiliateFlatById(String flatId) {
        try {
            MongoCollection<AffiliateFlat> collection = database.getCollection("AffiliateRate", AffiliateFlat.class);
            Bson filter = Filters.eq("_id", new ObjectId(flatId));
            return collection.find(filter).first();
            /*MongoCollection collection = jongo.getCollection("AffiliateRate");
            return collection.findOne(Oid.withOid(flatId)).as(AffiliateFlat.class);*/
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliatePackages getPackageRateByIdAffiliate(String packageId) {
        try {
            MongoCollection<AffiliatePackages> collection = database.getCollection("AffiliatePackages", AffiliatePackages.class);
            Bson filter = Filters.eq("_id", new ObjectId(packageId));
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateRoute getAffiliateRouteById(String routeId) {
        try {
            MongoCollection<AffiliateRoute> collection = database.getCollection("AffiliateRoute", AffiliateRoute.class);
            Bson filter = Filters.eq("_id", new ObjectId(routeId));
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateRoute getByGeoAffiliate(String fareFlatId, List<Double> pickup, List<Double> destination) {
        try {
            MongoCollection<AffiliateRoute> collection = database.getCollection("AffiliateRoute", AffiliateRoute.class);
            Point geoPickup = new Point(new Position(pickup.get(0),pickup.get(1)));
            Point geoDestination = new Point(new Position(destination.get(0),destination.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("flatRateId", fareFlatId),
                    Filters.geoIntersects("zone.fromZone", geoPickup),
                    Filters.geoIntersects("zone.toZone", geoDestination)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateRoute getByGeoReturnAffiliate(String fareFlatId, List<Double> pickup, List<Double> destination) {
        try {
            MongoCollection<AffiliateRoute> collection = database.getCollection("AffiliateRoute", AffiliateRoute.class);
            Point geoPickup = new Point(new Position(pickup.get(0),pickup.get(1)));
            Point geoDestination = new Point(new Position(destination.get(0),destination.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("flatRateId", fareFlatId),
                    Filters.eq("singleTrip.enableReturnRoute", true),
                    Filters.geoIntersects("zone.fromZone", geoDestination),
                    Filters.geoIntersects("zone.toZone", geoPickup)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateRoute getByGeoRoundTripAffiliate(String fareFlatId, List<Double> pickup, List<Double> destination) {
        try {
            MongoCollection<AffiliateRoute> collection = database.getCollection("AffiliateRoute", AffiliateRoute.class);
            Point geoPickup = new Point(new Position(pickup.get(0),pickup.get(1)));
            Point geoDestination = new Point(new Position(destination.get(0),destination.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("flatRateId", fareFlatId),
                    Filters.eq("roundTrip.enableRoundTrip", true),
                    Filters.geoIntersects("zone.fromZone", geoPickup),
                    Filters.geoIntersects("zone.toZone", geoDestination)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateRoute getByGeoRoundTripReturnAffiliate(String fareFlatId, List<Double> pickup, List<Double> destination) {
        try {
            MongoCollection<AffiliateRoute> collection = database.getCollection("AffiliateRoute", AffiliateRoute.class);
            Point geoPickup = new Point(new Position(pickup.get(0),pickup.get(1)));
            Point geoDestination = new Point(new Position(destination.get(0),destination.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("flatRateId", fareFlatId),
                    Filters.eq("roundTrip.enableRoundTrip", true),
                    Filters.eq("singleTrip.enableReturnRoute", true),
                    Filters.geoIntersects("zone.fromZone", geoDestination),
                    Filters.geoIntersects("zone.toZone", geoPickup)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateRoute getByZipCodeAffiliate(String fareFlatId, String zipCodeFrom, String zipCodeTo) {
        try {
            List<String> zipFrom = Collections.singletonList(zipCodeFrom);
            List<String> zipTo = Collections.singletonList(zipCodeTo);
            MongoCollection<AffiliateRoute> collection = database.getCollection("AffiliateRoute", AffiliateRoute.class);
            Bson filter = Filters.and(
                    Filters.eq("flatRateId", fareFlatId),
                    Filters.in("zipCode.fromZipCode", zipFrom),
                    Filters.in("zipCode.toZipCode", zipTo)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateRoute getByZipCodeReturnAffiliate(String fareFlatId, String zipCodeFrom, String zipCodeTo) {
        try {
            List<String> zipFrom = Collections.singletonList(zipCodeFrom);
            List<String> zipTo = Collections.singletonList(zipCodeTo);
            MongoCollection<AffiliateRoute> collection = database.getCollection("AffiliateRoute", AffiliateRoute.class);
            Bson filter = Filters.and(
                    Filters.eq("flatRateId", fareFlatId),
                    Filters.eq("singleTrip.enableReturnRoute", true),
                    Filters.in("zipCode.fromZipCode", zipTo),
                    Filters.in("zipCode.toZipCode", zipFrom)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateRoute getByZipCodeRoundTripAffiliate(String fareFlatId, String zipCodeFrom, String zipCodeTo) {
        try {
            List<String> zipFrom = Collections.singletonList(zipCodeFrom);
            List<String> zipTo = Collections.singletonList(zipCodeTo);
            MongoCollection<AffiliateRoute> collection = database.getCollection("AffiliateRoute", AffiliateRoute.class);
            Bson filter = Filters.and(
                    Filters.eq("flatRateId", fareFlatId),
                    Filters.eq("roundTrip.enableRoundTrip", true),
                    Filters.in("zipCode.fromZipCode", zipFrom),
                    Filters.in("zipCode.toZipCode", zipTo)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateRoute getByZipCodeRoundTripReturnAffiliate(String fareFlatId, String zipCodeFrom, String zipCodeTo) {
        try {
            List<String> zipFrom = Collections.singletonList(zipCodeFrom);
            List<String> zipTo = Collections.singletonList(zipCodeTo);
            MongoCollection<AffiliateRoute> collection = database.getCollection("AffiliateRoute", AffiliateRoute.class);
            Bson filter = Filters.and(
                    Filters.eq("flatRateId", fareFlatId),
                    Filters.eq("singleTrip.enableReturnRoute", true),
                    Filters.eq("roundTrip.enableRoundTrip", true),
                    Filters.in("zipCode.fromZipCode", zipTo),
                    Filters.in("zipCode.toZipCode", zipFrom)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public long countFailedCard(String fleetId, String cardId) {
        MongoCollection<FailedCard> collection = database.getCollection("FailedCard", FailedCard.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("cardId", cardId)
        );
        return collection.countDocuments(filter);
    }
    public String addFailedCard(FailedCard failedCard) {
        MongoCollection<FailedCard> collection = database.getCollection("FailedCard", FailedCard.class);
        InsertOneResult result = collection.insertOne(failedCard);
        return result.getInsertedId().toString();

    }

    public boolean updateCreditCardsOfCorporate(String corporateId, List<Credit> credits){
        MongoCollection<Corporate> collection = database.getCollection("Corporate", Corporate.class);
        Bson filter = Filters.eq("_id", new ObjectId(corporateId));
        Bson update = Updates.set("credits", credits);

        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public Zone findByFleetIdAndGeo(String fleetId, List<Double> pickup) {
        try {
            MongoCollection<Zone> collection = database.getCollection("Zone", Zone.class);
            Point point = new Point(new Position(pickup.get(0),pickup.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("isActive", true),
                    Filters.geoIntersects("geo", point)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    public List<Zone> findByGeoAffiliate(List<Double> pickup, String fleetId, String vehicleTypeId) {
        List<Zone> zones = new ArrayList<>();
        try {
            List<String> vehicle = Collections.singletonList(vehicleTypeId);
            MongoCollection<Zone> collection = database.getCollection("Zone", Zone.class);
            Point geometry = new Point(new Position(pickup.get(0),pickup.get(1)));
            Bson filter = Filters.and(
                    Filters.ne("fleetId", fleetId),
                    Filters.eq("isActive", true),
                    Filters.eq("assignAffiliate", true),
                    Filters.in("affiliateCarType", vehicle),
                    Filters.geoIntersects("geo", geometry)
            );
            MongoCursor<Zone> cursor = collection.find(filter).iterator();
            while(cursor.hasNext()) {
                zones.add(cursor.next());
            }
        } catch(Exception ex) {

        }
        return zones;
    }

    public Zone findByGeoSuplier(List<Double> pickup, String fleetId, String vehicleTypeId) {
        try {
            List<String> vehicle = Collections.singletonList(vehicleTypeId);
            MongoCollection<Zone> collection = database.getCollection("Zone", Zone.class);
            Point geometry = new Point(new Position(pickup.get(0),pickup.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("isActive", true),
                    Filters.eq("assignAffiliate", true),
                    Filters.in("affiliateCarType", vehicle),
                    Filters.geoIntersects("geo", geometry)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public FlatRoutes getByGeo(String fareFlatId, List<Double> pickup, List<Double> destination) {
        try {
            MongoCollection<FlatRoutes> collection = database.getCollection("FlatRoutes", FlatRoutes.class);
            Point geoPickup = new Point(new Position(pickup.get(0),pickup.get(1)));
            Point geoDestination = new Point(new Position(destination.get(0),destination.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("flatRateId", fareFlatId),
                    Filters.geoIntersects("zone.fromZone", geoPickup),
                    Filters.geoIntersects("zone.toZone", geoDestination)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public FlatRoutes getRouteById(String routeId) {
        try {
            MongoCollection<FlatRoutes> collection = database.getCollection("FlatRoutes", FlatRoutes.class);
            Bson filter = Filters.eq("_id", new ObjectId(routeId));
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public FlatRoutes getByGeoReturn(String fareFlatId, List<Double> pickup, List<Double> destination) {
        try {
            MongoCollection<FlatRoutes> collection = database.getCollection("FlatRoutes", FlatRoutes.class);
            Point geoPickup = new Point(new Position(pickup.get(0),pickup.get(1)));
            Point geoDestination = new Point(new Position(destination.get(0),destination.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("fareFlatId", fareFlatId),
                    Filters.eq("singleTrip.enableReturnRoute", true),
                    Filters.geoIntersects("zone.fromZone", geoDestination),
                    Filters.geoIntersects("zone.toZone", geoPickup)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public FlatRoutes getByGeoRoundTrip(String fareFlatId, List<Double> pickup, List<Double> destination) {
        try {
            MongoCollection<FlatRoutes> collection = database.getCollection("FlatRoutes", FlatRoutes.class);
            Point geoPickup = new Point(new Position(pickup.get(0),pickup.get(1)));
            Point geoDestination = new Point(new Position(destination.get(0),destination.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("flatRateId", fareFlatId),
                    Filters.eq("roundTrip.enableRoundTrip", true),
                    Filters.geoIntersects("zone.fromZone", geoPickup),
                    Filters.geoIntersects("zone.toZone", geoDestination)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public FlatRoutes getByGeoRoundTripReturn(String fareFlatId, List<Double> pickup, List<Double> destination) {
        try {
            MongoCollection<FlatRoutes> collection = database.getCollection("FlatRoutes", FlatRoutes.class);
            Point geoPickup = new Point(new Position(pickup.get(0),pickup.get(1)));
            Point geoDestination = new Point(new Position(destination.get(0),destination.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("fareFlatId", fareFlatId),
                    Filters.eq("roundTrip.enableRoundTrip", true),
                    Filters.eq("singleTrip.enableReturnRoute", true),
                    Filters.geoIntersects("zone.fromZone", geoDestination),
                    Filters.geoIntersects("zone.toZone", geoPickup)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public FlatRoutes getByZipCode(String fareFlatId, String zipCodeFrom, String zipCodeTo) {
        try {
            List<String> zipFrom = Collections.singletonList(zipCodeFrom);
            List<String> zipTo = Collections.singletonList(zipCodeTo);
            MongoCollection<FlatRoutes> collection = database.getCollection("FlatRoutes", FlatRoutes.class);
            Bson filter = Filters.and(
                    Filters.eq("fareFlatId", fareFlatId),
                    Filters.in("zipCode.fromZipCode", zipFrom),
                    Filters.in("zipCode.toZipCode", zipTo)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public FlatRoutes getByZipCodeReturn(String fareFlatId, String zipCodeFrom, String zipCodeTo) {
        try {
            List<String> zipFrom = Collections.singletonList(zipCodeFrom);
            List<String> zipTo = Collections.singletonList(zipCodeTo);
            MongoCollection<FlatRoutes> collection = database.getCollection("FlatRoutes", FlatRoutes.class);
            Bson filter = Filters.and(
                    Filters.eq("fareFlatId", fareFlatId),
                    Filters.eq("singleTrip.enableReturnRoute", true),
                    Filters.in("zipCode.fromZipCode", zipTo),
                    Filters.in("zipCode.toZipCode", zipFrom)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public FlatRoutes getByZipCodeRoundTrip(String fareFlatId, String zipCodeFrom, String zipCodeTo) {
        try {
            List<String> zipFrom = Collections.singletonList(zipCodeFrom);
            List<String> zipTo = Collections.singletonList(zipCodeTo);
            MongoCollection<FlatRoutes> collection = database.getCollection("FlatRoutes", FlatRoutes.class);
            Bson filter = Filters.and(
                    Filters.eq("fareFlatId", fareFlatId),
                    Filters.eq("roundTrip.enableRoundTrip", true),
                    Filters.in("zipCode.fromZipCode", zipFrom),
                    Filters.in("zipCode.toZipCode", zipTo)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public FlatRoutes getByZipCodeRoundTripReturn(String fareFlatId, String zipCodeFrom, String zipCodeTo) {
        try {
            List<String> zipFrom = Collections.singletonList(zipCodeFrom);
            List<String> zipTo = Collections.singletonList(zipCodeTo);
            MongoCollection<FlatRoutes> collection = database.getCollection("FlatRoutes", FlatRoutes.class);
            Bson filter = Filters.and(
                    Filters.eq("fareFlatId", fareFlatId),
                    Filters.eq("singleTrip.enableReturnRoute", true),
                    Filters.eq("roundTrip.enableRoundTrip", true),
                    Filters.in("zipCode.fromZipCode", zipTo),
                    Filters.in("zipCode.toZipCode", zipFrom)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    @Override
    public IntercityRoute getIntercityRouteByGeo(String intercityRateId, List<Double> pickup, List<Double> destination) {
        try {
            MongoCollection<IntercityRoute> collection = database.getCollection("IntercityRoute", IntercityRoute.class);
            Point geoPickup = new Point(new Position(pickup.get(0),pickup.get(1)));
            Point geoDestination = new Point(new Position(destination.get(0),destination.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("intercityRateId", intercityRateId),
                    Filters.geoIntersects("firstLocation", geoDestination),
                    Filters.geoIntersects("secondLocation", geoPickup)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    @Override
    public IntercityRoute getIntercityRouteRouteById(String intercityRouteId) {
        try {
            MongoCollection<IntercityRoute> collection = database.getCollection("IntercityRoute", IntercityRoute.class);
            Bson filter = Filters.eq("_id", new ObjectId(intercityRouteId));
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    @Override
    public IntercityRoute getIntercityRouteByGeoReturn(String intercityRateId, List<Double> pickup, List<Double> destination) {
        try {
            MongoCollection<IntercityRoute> collection = database.getCollection("IntercityRoute", IntercityRoute.class);
            Point geoPickup = new Point(new Position(pickup.get(0),pickup.get(1)));
            Point geoDestination = new Point(new Position(destination.get(0),destination.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("intercityRateId", intercityRateId),
                    Filters.eq("roundTwoEnable", true),
                    Filters.geoIntersects("firstLocation", geoDestination),
                    Filters.geoIntersects("secondLocation", geoPickup)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public AirportZone checkDestinationByGeo(List<Double> destination) {
        try {
            MongoCollection<AirportZone> collection = database.getCollection("AirportZone", AirportZone.class);
            Point geoDestination = new Point(new Position(destination.get(0),destination.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("isActive", true),
                    Filters.geoIntersects("geo", geoDestination)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public PromotionCode getPromoCodeByFleetAndName(String fleetId, String promoCode) {
        MongoCollection<PromotionCode> collection = database.getCollection("PromotionCode", PromotionCode.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("promotionCode", promoCode.toUpperCase())
        );
        return collection.find(filter).first();
    }

    public PromotionCode getPromoCodeByFleetAndNameAndZone(String fleetId, String promoCode, String zoneId) {
        MongoCollection<PromotionCode> collection = database.getCollection("PromotionCode", PromotionCode.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("promotionCode", promoCode.toUpperCase()),
                Filters.in("zoneId", zoneId)
        );
        return collection.find(filter).first();
    }

    public PromotionCode getPromoCodeByFleetAndNameAndCustomer(String fleetId, String promoCode, String customerId) {
        MongoCollection<PromotionCode> collection = database.getCollection("PromotionCode", PromotionCode.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("promotionCode", promoCode.toUpperCase()),
                Filters.in("customerId", customerId)
        );
        return collection.find(filter).first();
    }

    public TripNotification getTripNotificationByCustomer(String fleetId, String customerId, Date date) {
        try{
            MongoCollection<TripNotification> collection = database.getCollection("TripNotification", TripNotification.class);
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("customerId", customerId),
                    Filters.eq("date", date)
            );
            return collection.find(filter).first();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public void addTripNotification(TripNotification tripNotification) {
        MongoCollection<TripNotification> collection = database.getCollection("TripNotification", TripNotification.class);
        InsertOneResult result = collection.insertOne(tripNotification);
    }

    public void updateTripNotification(TripNotification tripNotification) {
        MongoCollection<TripNotification> collection = database.getCollection("TripNotification", TripNotification.class);
        Bson filter = Filters.eq("_id", tripNotification._id);
        collection.replaceOne(filter, tripNotification);
    }

    public List<PhoneZip> getByPhone(String phone) {
        MongoCollection<PhoneZip> collection = database.getCollection("PhoneZip", PhoneZip.class);
        Bson filter = Filters.in("phone", phone);
        MongoCursor<PhoneZip> iterator  = collection.find(filter).iterator();
        List<PhoneZip> listPhoneZips = new ArrayList<>();
        while(iterator.hasNext()) {
            PhoneZip phoneZip = iterator.next();
            listPhoneZips.add(phoneZip);
        }
        return listPhoneZips;
    }

    public PromotionCode findByFleetIdAndPromoCode(String fleetId, String promoCode) {
        try{
            MongoCollection<PromotionCode> collection = database.getCollection("PromotionCode", PromotionCode.class);
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("promotionCode", promoCode.toUpperCase())
            );
            return collection.find(filter).first();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public PaxPromoList findByFleetIdAndPromoCodeIdAndUserId(String fleetId, String promoCodeId, String customerId) {
        try{
            MongoCollection<PaxPromoList> collection = database.getCollection("PaxPromoList", PaxPromoList.class);
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("promoCodeId", promoCodeId),
                    Filters.eq("userId", customerId)
            );
            return collection.find(filter).first();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public void addPaxPromoList(PaxPromoList paxPromoList) {
        MongoCollection<PaxPromoList> collection = database.getCollection("PaxPromoList", PaxPromoList.class);
        InsertOneResult result = collection.insertOne(paxPromoList);
    }

    public void addReferralHistories(ReferralHistories histories) {
        MongoCollection<ReferralHistories> collection = database.getCollection("ReferralHistories", ReferralHistories.class);
        InsertOneResult result = collection.insertOne(histories);
    }

    public void removePaxPromoByPromoCodeId(String promoCodeId){
        MongoCollection<PaxPromoList> collection = database.getCollection("PaxPromoList", PaxPromoList.class);
        Bson filter = Filters.in("promoCodeId", promoCodeId);
        collection.deleteMany(filter);
    }

    public void deletePaxPromoList(String promoCodeId, String userId){
        MongoCollection<PaxPromoList> collection = database.getCollection("PaxPromoList", PaxPromoList.class);
        Bson filter = Filters.and(
                Filters.eq("promoCodeId", promoCodeId),
                Filters.eq("userId", userId)
        );
        collection.deleteMany(filter);
    }

    public Operations getOperation(String driverId) {
        try {
            MongoCollection<Operations> collection = database.getCollection("Operations", Operations.class);
            Bson filter = Filters.in("driverId", driverId);
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public SolutionPartner findSolutionPartnerById (String SID) {
        try {
            MongoCollection<SolutionPartner> collection = database.getCollection("SolutionPartner", SolutionPartner.class);
            Bson filter = Filters.eq("_id", new ObjectId(SID));
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public String addUpdateHistory(UpdateHistory updateHistory) {
        updateHistory._id = new ObjectId();
        MongoCollection<UpdateHistory> collection = database.getCollection("UpdateHistory", UpdateHistory.class);
        InsertOneResult result = collection.insertOne(updateHistory);
        return result.getInsertedId().asObjectId().getValue().toString();
    }

    public AdditionalServices findByServiceId(String serviceId) {
        try{
            MongoCollection<AdditionalServices> collection = database.getCollection("AdditionalServices", AdditionalServices.class);
            Bson filter = Filters.eq("_id", new ObjectId(serviceId));
            return collection.find(filter).first();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public List<AdditionalServices> findByVehicleIdAndServiceType(String vehicleId, String serviceType) {
        List<AdditionalServices> additionalServices = new ArrayList<>();
        try{
            MongoCollection<AdditionalServices> collection = database.getCollection("AdditionalServices", AdditionalServices.class);
            Bson filter = Filters.and(
                    Filters.in("vehicleType", vehicleId),
                    Filters.eq("serviceType", serviceType)
            );
            MongoCursor<AdditionalServices> cursor  = collection.find(filter).iterator();
            while(cursor.hasNext()) {
                AdditionalServices obj = cursor.next();
                additionalServices.add(obj);
            }
            return additionalServices;
        }catch (Exception e){
            e.printStackTrace();
            return additionalServices;
        }
    }

    public Boolean checkBinData(String cardNumber) {
        try {
            MongoCollection<UnionpayBin> collection = database.getCollection("UnionpayBin", UnionpayBin.class);

            Bson filter1 = Filters.and(
                    Filters.eq("binLength", 5),
                    Filters.eq("binNumber", cardNumber.substring(0, 5))
            );
            Bson filter2 = Filters.and(
                    Filters.eq("binLength", 6),
                    Filters.eq("binNumber", cardNumber.substring(0, 6))
            );
            Bson filter3 = Filters.and(
                    Filters.eq("binLength", 7),
                    Filters.eq("binNumber", cardNumber.substring(0, 7))
            );
            Bson filterOr = Filters.or(filter1, filter2, filter3);
            Bson filterLength = Filters.eq("accountLength", cardNumber.length());
            Bson filter = Filters.and(filterLength, filterOr);
            UnionpayBin unionpayBin = collection.find(filter).first();
            return unionpayBin != null;
        } catch(Exception ex) {
            return false;
        }
    }

    public String getTokenFromAccount(String cardOwner, String id, String last4){
        String query = "";
        if (cardOwner.equals("user") || cardOwner.equals("customer") || cardOwner.equals("passenger") || cardOwner.equals("mDispatcher")
                || cardOwner.equals("driver")) {
            MongoCollection<Account> collection = database.getCollection("Account", Account.class);
            Bson filter = Filters.eq("userId", id);
            Account account = collection.find(filter).first();
            if (account != null && !account.credits.isEmpty()) {
                for (Credit credit : account.credits) {
                    if (credit.cardMask.substring(credit.cardMask.length() - 4).equals(last4))
                        return credit.localToken;
                }
            }
        }
        if (cardOwner.equals("corporate")){
            MongoCollection<Corporate> collection = database.getCollection("Corporate", Corporate.class);
            Bson filter = Filters.eq("_id", new ObjectId(id));
            Corporate account = collection.find(filter).first();
            if (account != null && !account.credits.isEmpty()) {
                for (Credit credit : account.credits) {
                    if (credit.cardMask.substring(credit.cardMask.length() - 4).equals(last4))
                        return credit.localToken;
                }
            }
        }
        if (cardOwner.equals("fleetOwner")){
            MongoCollection<UsersSystem> collection = database.getCollection("UsersSystem", UsersSystem.class);
            Bson filter = Filters.eq("_id", new ObjectId(id));
            UsersSystem account = collection.find(filter).first();
            if (account != null && !account.credits.isEmpty()) {
                for (Credit credit : account.credits) {
                    if (credit.cardMask.substring(credit.cardMask.length() - 4).equals(last4))
                        return credit.localToken;
                }
            }
        }
        return "";
    }

    public Fleet getFleetFromPayEaseMerchant(String merchantId){
        MongoCollection<Fleet> collection = database.getCollection("Fleet", Fleet.class);
        Bson filter = Filters.and(
                Filters.eq("paymentClearanceHouses.type", "ApplePay"),
                Filters.eq("paymentClearanceHouses.merchantId", merchantId)
        );
        return collection.find(filter).first();
    }

    public void logPayEaseAsync(LogThirdParties logThirdParties) {
        MongoCollection<LogThirdParties> collection = database.getCollection("LogThirdParties", LogThirdParties.class);
        InsertOneResult result = collection.insertOne(logThirdParties);

    }

    public Zone getZoneById(String zoneId) {
        try{
            MongoCollection<Zone> collection = database.getCollection("Zone", Zone.class);
            Bson filter = Filters.eq("_id", new ObjectId(zoneId));
            return collection.find(filter).first();
        }catch (Exception e){
            e.getMessage();
            return null;
        }
    }

    public ConfigGateway getSingleGateway(String fleetId) {
        MongoCollection<ConfigGateway> collection = database.getCollection("ConfigGateway", ConfigGateway.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("singleGateway", true),
                Filters.eq("isActive", true)
        );
        return collection.find(filter).first();
    }

    public ConfigGateway getGatewayByZone(String fleetId, String zoneId) {
        MongoCollection<ConfigGateway> collection = database.getCollection("ConfigGateway", ConfigGateway.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.in("zones", zoneId),
                Filters.eq("singleGateway", false),
                Filters.eq("isActive", true)
        );
        return collection.find(filter).first();
    }

    public boolean updatePaymentMethod(String bookId, int paymentType){
        MongoCollection<Booking> collection = database.getCollection("BookingCompleted", Booking.class);
        Bson filter = Filters.eq("bookId", bookId);
        Bson update = Updates.set("completedInfo.paymentType", paymentType);
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }


    public List<PhoneZip> getPhoneZipWithoutState(String country) {
        MongoCollection<PhoneZip> collection = database.getCollection("PhoneZip", PhoneZip.class);
        Bson filter = Filters.eq("country", country);
        MongoCursor<PhoneZip> cursor  = collection.find(filter).iterator();
        List<PhoneZip> listPhoneZip = new ArrayList<>();
        while (cursor.hasNext()) {
            try {
                PhoneZip phoneZip = cursor.next();
                listPhoneZip.add(phoneZip);
            } catch(Exception ex) {
                System.out.println("Exception : " + ex.getMessage());
            }
        }
        return listPhoneZip;
    }

    public boolean updateDriverBalance(String userId, List<String> listCurrencies){
        MongoCollection<Operations> collection = database.getCollection("Operations", Operations.class);
        Bson filter = Filters.eq("driverId", userId);
        Bson update = Updates.combine(Updates.set("balanceCurrencies", listCurrencies), Updates.currentDate("latestUpdate"));
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public void addBookingStatistic(BookingStatistic bookingStatistic) {
        MongoCollection<BookingStatistic> collection = database.getCollection("BookingStatistic", BookingStatistic.class);
        InsertOneResult result = collection.insertOne(bookingStatistic);
    }

    public boolean updateCorporateInfo(String userId, Corporate corporate, String createdDate, int status){
        try {
            MongoCollection<Account> collection = database.getCollection("Account", Account.class);
            Bson filter = Filters.eq("userId", userId);
            Bson update = Updates.combine(
                    Updates.set("passengerInfo.corporateId", corporate._id.toString()),
                    Updates.set("passengerInfo.corporateName", corporate.companyInfo.name),
                    Updates.set("passengerInfo.corporateNameSort", corporate.companyInfo.name.toLowerCase()),
                    Updates.currentDate("passengerInfo.createdCorporate"),
                    Updates.set("corporateInfo.status", status));
            UpdateResult result = collection.updateMany(filter, update);
            return result.wasAcknowledged();
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public ReferralHistories getLinkReferralByRefereeId(String refereeId) {
        MongoCollection<ReferralHistories> collection = database.getCollection("ReferralHistories", ReferralHistories.class);
        Bson filter = Filters.eq("refereeId", refereeId);
        return collection.find(filter).first();
    }

    public List<ReferralHistories> getLinkReferralByUserId(String userId) {
        MongoCollection<ReferralHistories> collection = database.getCollection("ReferralHistories", ReferralHistories.class);
        Bson filter = Filters.and(
                Filters.eq("userId", userId),
                Filters.exists("firstBookingDate")
        );
        Bson sort = Sorts.ascending("firstBookingDate");
        MongoCursor<ReferralHistories> cursor = collection.find(filter).sort(sort).iterator();
        List<ReferralHistories> listReferral = new ArrayList<>();
        while(cursor.hasNext()) {
            listReferral.add(cursor.next());
        }
        return listReferral;
    }

    public Referral getReferral(String fleetId) {
        MongoCollection<Referral> collection = database.getCollection("Referral", Referral.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public PaxReferral getPaxReferral(String fleetId) {
        MongoCollection<PaxReferral> collection = database.getCollection("PaxReferral", PaxReferral.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public ReferralInfo getReferralInfo(String fleetId, String userId) {
        MongoCollection<ReferralInfo> collection = database.getCollection("ReferralInfo", ReferralInfo.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("userId", userId)
        );
        return collection.find(filter).first();
    }

    public boolean updateReferralHistories(String refereeId, JSONObject updateObj){
        try {
            MongoCollection<ReferralHistories> collection = database.getCollection("ReferralHistories", ReferralHistories.class);
            Bson filter = Filters.eq("refereeId", refereeId);
            ReferralHistories referralHistories = collection.find(filter).first();
            if (referralHistories != null) {
                if (referralHistories.firstBookingDate == null) {
                    Bson update = Updates.combine(
                            Updates.set("firstBookingDate", KeysUtil.SDF_DMYHMSTZ.parse(updateObj.getString("firstBookingDate"))),
                            Updates.set("firstBookingDateGMT", KeysUtil.SDF_DMYHMSTZ.parse(updateObj.getString("firstBookingDateGMT"))),
                            Updates.set("firstBooking", updateObj.getString("firstBooking")),
                            Updates.set("firstBookingCurrency", updateObj.getString("firstBookingCurrency")),
                            Updates.set("firstEarning", updateObj.getDouble("firstEarning"))
                        );
                    UpdateResult result = collection.updateOne(filter, update);
                    return result.wasAcknowledged();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public AffiliateProcess findAffiliateProcessById(String id){
        try{
            MongoCollection<AffiliateProcess> collection = database.getCollection("AffiliateProcess", AffiliateProcess.class);
            Bson filter = Filters.eq("_id", new ObjectId(id));
            return collection.find(filter).first();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public AffiliateProcess findAffiliateProcessTopByOrderByCreatedDateDesc(){
        try{
            MongoCollection<AffiliateProcess> collection = database.getCollection("AffiliateProcess", AffiliateProcess.class);
            Bson sorts = Sorts.ascending("createdDate");
            MongoCursor<AffiliateProcess> cursor = collection.find().iterator();
            return cursor.next();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public boolean updateAffiliateStatus(String bookId, int status){
        MongoCollection<Booking> collection = database.getCollection("Booking", Booking.class);
        Bson filter = Filters.eq("bookId", bookId);
        Bson update = Updates.set("affiliateStatus", status);
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public boolean updatePromoCode(String bookId, String promoCode){
        MongoCollection<Booking> collectionBooking = database.getCollection("Booking", Booking.class);
        MongoCollection<Booking> collectionBookingCompleted = database.getCollection("BookingCompleted", Booking.class);
        Bson filter = Filters.eq("bookId", bookId);
        Bson update = Updates.set("request.promo", promoCode);
        UpdateResult result = collectionBooking.updateOne(filter, update);
        UpdateResult resultCompleted = collectionBookingCompleted.updateOne(filter, update);
        return resultCompleted.wasAcknowledged();
    }
    public boolean updateTotalBuy(String bookId, double value, String transactionStatus){
        MongoCollection<Booking> collection = database.getCollection("BookingCompleted", Booking.class);
        Bson filter = Filters.eq("bookId", bookId);
        Bson update = Updates.set("completedInfo.totalBuyFare", value);
        if(transactionStatus.equals("canceled") || transactionStatus.equals("noShow")){
            update = Updates.set("cancelInfo.policies.valueBuy", value);
        }
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public boolean updateAccountEmail(String userId, String email){
        MongoCollection<Account> collection = database.getCollection("Account", Account.class);
        Bson filter = Filters.eq("userId", userId);
        Bson update = Updates.set("email", email);
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public boolean updateBankStatus(String userId){
        MongoCollection<Account> collection = database.getCollection("Account", Account.class);
        Bson filter = Filters.eq("userId", userId);
        Bson update = Updates.set("driverInfo.isBankVerified", true);
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public boolean updateBankProfile(ACHEnt achEnt, boolean isVerified, String gateway) {
        MongoCollection<Account> collection = database.getCollection("Account", Account.class);
        Bson filter = Filters.eq("userId", achEnt.driverId);
        String token = achEnt.token != null ? achEnt.token : "";
        String bankName = achEnt.bankName != null ? achEnt.bankName : "";
        String firstName = achEnt.firstName != null ? achEnt.firstName : "";
        String lastName = achEnt.lastName != null ? achEnt.lastName : "";
        String accountName = firstName + " " + lastName;
        if (accountName.trim().isEmpty()) {
            accountName = achEnt.accountName != null ? achEnt.accountName : "";
        }
        String routingNumber = achEnt.routingNumber != null ? achEnt.routingNumber : "";
        String bankNumber = achEnt.bankNumber != null ? achEnt.bankNumber : "";
        String birthDay = achEnt.birthDay != null ? achEnt.birthDay : "";
        String address = achEnt.address != null ? achEnt.address : "";
        String city = achEnt.city != null ? achEnt.city : "";
        String state = achEnt.state != null ? achEnt.state : "";
        String zipCode = achEnt.zipCode != null ? achEnt.zipCode : "";
        String ssn = achEnt.ssn != null ? achEnt.ssn : "";
        String checkNumber = achEnt.checkNumber != null ? achEnt.checkNumber : "";
        String verificationDocument = achEnt.verificationDocument != null ? achEnt.verificationDocument : "";
        String additionalDocument = achEnt.additionalDocument != null ? achEnt.additionalDocument : "";
        String legalToken = achEnt.legalToken != null ? achEnt.legalToken : "";
        String driverIc = achEnt.driverIc != null ? achEnt.driverIc : "";
        String idType = achEnt.idType != null ? achEnt.idType : "";
        boolean isBankAccountOwner = achEnt.isBankAccountOwner;
        String beneficiaryIDIC = achEnt.beneficiaryIDIC != null ? achEnt.beneficiaryIDIC : "";
        int bankRelationship = achEnt.bankRelationship;
        String relationshipOtherName = achEnt.relationshipOtherName != null ? achEnt.relationshipOtherName : "";
        String sortCode = achEnt.sortCode != null ? achEnt.sortCode : "";
        String IFSCCode = achEnt.IFSCCode != null ? achEnt.IFSCCode : "";
        String verificationDocumentBack = achEnt.verificationDocumentBack != null ? achEnt.verificationDocumentBack : "";
        String additionalDocumentBack = achEnt.additionalDocumentBack != null ? achEnt.additionalDocumentBack : "";
        String frontFileId = achEnt.frontFileId != null ? achEnt.frontFileId : "";
        String frontAddFileId = achEnt.frontAddFileId != null ? achEnt.frontAddFileId : "";
        String backFileId = achEnt.backFileId != null ? achEnt.backFileId : "";
        String backAddFileId = achEnt.backAddFileId != null ? achEnt.backAddFileId : "";
        boolean verifyStatus = achEnt.verifyStatus;
        Bson update;
        if (gateway.equals(KeysUtil.STRIPE) && isVerified) {
            update = Updates.combine(
                    Updates.set("driverInfo.dateOfBirth", birthDay),
                    Updates.set("driverInfo.bankAddress", address),
                    Updates.set("driverInfo.bankCity", city),
                    Updates.set("driverInfo.bankState", state),
                    Updates.set("driverInfo.bankZip", zipCode)
                );
        } else {
            update = Updates.combine(
                    Updates.set("driverInfo.achToken", token),
                    Updates.set("driverInfo.nameOfBank", bankName),
                    Updates.set("driverInfo.nameOfAccount", accountName),
                    Updates.set("driverInfo.routingNumber", routingNumber),
                    Updates.set("driverInfo.accountNumber", bankNumber),
                    Updates.set("driverInfo.dateOfBirth", birthDay),
                    Updates.set("driverInfo.bankAddress", address),
                    Updates.set("driverInfo.bankCity", city),
                    Updates.set("driverInfo.bankState", state),
                    Updates.set("driverInfo.bankZip", zipCode),
                    Updates.set("driverInfo.ssn", ssn),
                    Updates.set("driverInfo.checkNumber", checkNumber),
                    Updates.set("driverInfo.legalToken", legalToken),
                    Updates.set("driverInfo.drvId", driverIc),
                    Updates.set("driverInfo.idType", idType),
                    Updates.set("driverInfo.isBankAccountOwner", isBankAccountOwner),
                    Updates.set("driverInfo.beneficiaryIDIC", beneficiaryIDIC),
                    Updates.set("driverInfo.bankRelationship", bankRelationship),
                    Updates.set("driverInfo.relationshipOtherName", relationshipOtherName),
                    Updates.set("driverInfo.sortCode", sortCode),
                    Updates.set("driverInfo.IFSCCode", IFSCCode),
                    Updates.set("driverInfo.verificationDocument", verificationDocument),
                    Updates.set("driverInfo.additionalDocument", additionalDocument),
                    Updates.set("driverInfo.verificationDocumentBack", verificationDocumentBack),
                    Updates.set("driverInfo.additionalDocumentBack", additionalDocumentBack),
                    Updates.set("driverInfo.frontFileId", frontFileId),
                    Updates.set("driverInfo.frontAddFileId", frontAddFileId),
                    Updates.set("driverInfo.backFileId", backFileId),
                    Updates.set("driverInfo.backAddFileId", backAddFileId),
                    Updates.set("driverInfo.isBankVerified", verifyStatus)
                );
        }
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public boolean removeBankProfile(String driverId) {
        MongoCollection<Account> collection = database.getCollection("Account", Account.class);
        Bson filter = Filters.eq("userId", driverId);
        Bson update = Updates.combine(
                Updates.set("driverInfo.achToken", ""),
                Updates.set("driverInfo.nameOfBank", ""),
                Updates.set("driverInfo.nameOfAccount", ""),
                Updates.set("driverInfo.routingNumber", ""),
                Updates.set("driverInfo.accountNumber", ""),
                Updates.set("driverInfo.ssn", ""),
                Updates.set("driverInfo.checkNumber", ""),
                Updates.set("driverInfo.verificationDocument", ""),
                Updates.set("driverInfo.additionalDocument", ""),
                Updates.set("driverInfo.legalToken", ""),
                Updates.set("driverInfo.isBankVerified", false)
            );
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public DriverWallet getDriverWallet(String userId) {
        MongoCollection<DriverWallet> collection = database.getCollection("DriverWallet", DriverWallet.class);
        Bson filter = Filters.eq("userId", userId);
        try {
            return collection.find(filter).first();
        } catch(Exception ex) {
            System.out.println("==== Error mapping DriverWallet for userId " + userId);
            try {
                MongoCollection<DriverWalletError> collectionError = database.getCollection("DriverWallet", DriverWalletError.class);
                DriverWalletError errorObject = collectionError.find(filter).first();
                DriverWallet driverWallet = new DriverWallet();
                driverWallet.fleetId = errorObject.fleetId;
                driverWallet.userId = errorObject.userId;

                if (errorObject.cashWallet != null) {
                    AmountByCurrency creditWallet = new AmountByCurrency();
                    creditWallet.value = 0.0;
                    creditWallet.currencyISO = errorObject.cashWallet.currencyISO;
                    driverWallet.creditWallet = creditWallet;
                    driverWallet.cashWallet = errorObject.cashWallet;

                    // update to repair data model
                    Bson update = Updates.set("creditWallet", creditWallet);
                    collection.updateOne(filter, update);
                } else {
                    Fleet fleet = getFleetInfor(errorObject.fleetId);
                    com.qupworld.paymentgateway.models.mongo.collections.fleet.Currency currency = fleet.currencies.get(0);
                    AmountByCurrency creditWallet = new AmountByCurrency();
                    creditWallet.value = 0.0;
                    creditWallet.currencyISO = currency.iso;
                    driverWallet.creditWallet = creditWallet;

                    BalanceByCurrency cashWallet = new BalanceByCurrency();
                    cashWallet.currentBalance = 0.0;
                    cashWallet.pendingBalance = 0.0;
                    cashWallet.availableBalance = 0.0;
                    cashWallet.currencyISO = currency.iso;
                    driverWallet.cashWallet = cashWallet;

                    // update to repair data model
                    Bson updateCredit = Updates.set("creditWallet", creditWallet);
                    Bson updateCash = Updates.set("cashWallet", cashWallet);
                    collection.updateOne(filter, updateCredit);
                    collection.updateOne(filter, updateCash);
                }
                // return correct data
                return driverWallet;
            } catch (Exception e) {
                return null;
            }
        }
    }

    public boolean updateCashWallet(String userId, double currentBalance, double pendingBalance, String currencyISO) {
        MongoCollection<DriverWallet> collection = database.getCollection("DriverWallet", DriverWallet.class);
        Bson filter = Filters.eq("userId", userId);
        Bson update = Updates.combine(
                Updates.set("cashWallet.currentBalance", currentBalance),
                Updates.set("cashWallet.pendingBalance", pendingBalance),
                Updates.set("cashWallet.currencyISO", currencyISO),
                Updates.currentDate("latestUpdate"));
        if (pendingBalance == -1) {
            update = Updates.combine(
                    Updates.set("cashWallet.currentBalance", currentBalance),
                    Updates.set("cashWallet.currencyISO", currencyISO),
                    Updates.currentDate("latestUpdate"));
        }
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public boolean updateCreditWallet(String userId, List<AmountByCurrency> creditBalances, AmountByCurrency creditWallet) {
        MongoCollection<Account> collectionAccount = database.getCollection("Account", Account.class);
        Bson filter = Filters.eq("userId", userId);
        Bson updateAccount = Updates.combine(
                Updates.set("driverInfo.creditBalances", creditBalances),
                Updates.currentDate("latestUpdate"));
        UpdateResult result = collectionAccount.updateOne(filter, updateAccount);
        if (result.wasAcknowledged()) {
            MongoCollection<DriverWallet> collectionWallet = database.getCollection("DriverWallet", DriverWallet.class);
            Bson updateWallet = Updates.combine(
                    Updates.set("creditWallet", creditWallet),
                    Updates.currentDate("latestUpdate"));
            UpdateResult resultWallet = collectionWallet.updateOne(filter, updateWallet);
            return resultWallet.wasAcknowledged();
        } else {
            return result.wasAcknowledged();
        }
    }

    public void addDriverWallet(DriverWallet driverWallet) {
        MongoCollection<DriverWallet> collection = database.getCollection("DriverWallet", DriverWallet.class);
        try {
            collection.insertOne(driverWallet);
        } catch (Exception ignore) {}

    }

    public void updateDriverWallet(String userId, AmountByCurrency creditWallet) {
        MongoCollection<DriverWallet> collection = database.getCollection("DriverWallet", DriverWallet.class);
        Bson filter = Filters.eq("userId", userId);
        Bson update = Updates.combine(
                Updates.set("creditWallet", creditWallet),
                Updates.currentDate("latestUpdate"));
        UpdateResult result = collection.updateOne(filter, update);
    }

    public ServiceFee getServiceFeeByFleetIdAndZoneId(String fleetId, String zoneId){
        try {
            MongoCollection<ServiceFee> collection = database.getCollection("ServiceFee", ServiceFee.class);
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("zoneId", zoneId)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public DynamicSurcharge getDynamicSurcharge(String fleetId, String zoneId, List<Double> pickup){
        try {
            MongoCollection<DynamicSurcharge> collection = database.getCollection("DynamicSurcharge", DynamicSurcharge.class);
            Point geometry = new Point(new Position(pickup.get(0),pickup.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("zoneId", zoneId),
                    Filters.geoIntersects("geo", geometry)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public DynamicFare getDynamicFare(String fleetId, String zoneId, List<Double> pickup){
        try {
            MongoCollection<DynamicFare> collection = database.getCollection("DynamicFare", DynamicFare.class);
            Point geometry = new Point(new Position(pickup.get(0),pickup.get(1)));
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("zoneId", zoneId),
                    Filters.geoIntersects("geo", geometry)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public PassengerInfo getPassengerInfo(String fleetId, String userId) {
        try {
            MongoCollection<PassengerInfo> collection = database.getCollection("PassengerInfo", PassengerInfo.class);
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("userId", userId)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public void updatePaxWallet(String fleetId, String userId, List<AmountByCurrency> wallets) {
        try {
            MongoCollection<PassengerInfo> collection = database.getCollection("PassengerInfo", PassengerInfo.class);
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("userId", userId)
            );
            Bson update = Updates.combine(
                    Updates.set("paxWallet", wallets),
                    Updates.currentDate("latestUpdate"));

            UpdateResult result = collection.updateOne(filter, update);
        } catch(Exception ex) {
            logger.debug("updatePaxWallet error: " + CommonUtils.getError(ex));
        }
    }

    public void addPassengerInfo(PassengerInfo passengerInfo) {
        MongoCollection<PassengerInfo> collection = database.getCollection("PassengerInfo", PassengerInfo.class);
        try {
            collection.insertOne(passengerInfo);
        } catch (Exception ignore) {}

    }

    public void updateOustanding(String fleetId, String userId, List<OutStanding> outStandings) {
        try {
            MongoCollection<PassengerInfo> collection = database.getCollection("PassengerInfo", PassengerInfo.class);
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("userId", userId)
            );
            Bson update = Updates.combine(
                    Updates.set("outStanding", outStandings),
                    Updates.currentDate("latestUpdate"));

            UpdateResult result = collection.updateOne(filter, update);
        } catch(Exception ignore) {}
    }

    public AirportFee getAirportFee(String fleetId, String airportZoneId){
        try {
            MongoCollection<AirportFee> collection = database.getCollection("AirportFee", AirportFee.class);
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("airportZoneId", airportZoneId)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public List<String> getAllFleetIds() {
        List<String> listIds = new ArrayList<>();
        try {
            MongoCollection<Fleet> collection = database.getCollection("Fleet", Fleet.class);
            Bson filter = Filters.and(
                    Filters.eq("isActive", true),
                    Filters.eq("demoInfo.demoFleet", true)
            );
            MongoCursor<Fleet> cursor = collection.find(filter).iterator();
            List<String> listData = new ArrayList<>();
            while(cursor.hasNext()) {
                Fleet item = cursor.next();
                listData.add(item.fleetId);
            }
        } catch(Exception ignore) {}
        return listIds;
    }

    public CorpItinerary getCorpItinerary(String itineraryId) {
        try {
            MongoCollection<CorpItinerary> collection = database.getCollection("CorpItinerary", CorpItinerary.class);
            Bson filter = Filters.eq("itineraryId", itineraryId);
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    //for update affiliate rate
    public List<RateRegular> getAllAffiliateRegularByPriceType(String priceType){
        MongoCollection<RateRegular> collection = database.getCollection("AffiliateRate", RateRegular.class);
        Bson filter = Filters.and(
                Filters.eq("rateInfo.priceType", priceType),
                Filters.eq("type", "Regular")
        );
        MongoCursor<RateRegular> cursor = collection.find(filter).iterator();
        List<RateRegular> listData = new ArrayList<>();
        while(cursor.hasNext()) {
            listData.add(cursor.next());
        }
        return listData;
    }

    public void updateAffiliateRegular(RateRegular regular){
        MongoCollection<RateRegular> collection = database.getCollection("AffiliateRate", RateRegular.class);
        Bson filter = Filters.eq("_id", regular._id);
        collection.replaceOne(filter, regular);
    }

    public List<AffiliateFlat> getAllAffiliateFlatByPriceType(String priceType){
        MongoCollection<AffiliateFlat> collection = database.getCollection("AffiliateFlat", AffiliateFlat.class);
        Bson filter = Filters.and(
                Filters.eq("rateInfo.priceType", priceType),
                Filters.eq("type", "Flat")
        );
        MongoCursor<AffiliateFlat> cursor = collection.find(filter).iterator();
        List<AffiliateFlat> listData = new ArrayList<>();
        while(cursor.hasNext()) {
            listData.add(cursor.next());
        }
        return listData;
    }

    public List<AffiliateRoute> getAllAffiliateRouteByFlatId(String flatId){
        MongoCollection<AffiliateRoute> collection = database.getCollection("AffiliateRoute", AffiliateRoute.class);
        Bson filter = Filters.eq("flatRateId", flatId);
        MongoCursor<AffiliateRoute> cursor = collection.find(filter).iterator();
        List<AffiliateRoute> listData = new ArrayList<>();
        while(cursor.hasNext()) {
            listData.add(cursor.next());
        }
        return listData;
    }

    public void updateAffiliateRoute(AffiliateRoute route){
        MongoCollection<AffiliateRoute> collection = database.getCollection("AffiliateRoute", AffiliateRoute.class);
        Bson filter = Filters.eq("_id", route._id);
        collection.replaceOne(filter, route);
    }

    public List<String> getListDriverId(String fleetId, List<String> listZone) {
        MongoCollection<Account> collection = database.getCollection("Account", Account.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("appType", "driver"),
                Filters.eq("isActive", true),
                Filters.in("driverInfo.zoneId", listZone)
        );
        Bson sort = Sorts.ascending("createdDate");
        int i = 0;
        int size = 50;
        List<String> listAccount = new ArrayList<>();
        while (i >= 0) {
            MongoCursor<Account> cursor = collection.find(filter).skip(i*size).limit(size).sort(sort).iterator();
            while(cursor.hasNext()) {
                Account account = cursor.next();
                listAccount.add(account.userId);
            }
            if (listAccount.size() == size || listAccount.size() == (i+1)*size) {
                i++;
            } else {
                i = -1;
            }
        }

        return listAccount;
    }

    public List<UpdateBalanceId> getIdUpdateBalance(String key) {
        MongoCollection<UpdateBalanceId> collection = database.getCollection("UpdateBalanceId", UpdateBalanceId.class);
        Bson filter = Filters.eq("key", key);
        MongoCursor<UpdateBalanceId> cursor = collection.find(filter).iterator();
        List<UpdateBalanceId> listData = new ArrayList<>();
        while(cursor.hasNext()) {
            listData.add(cursor.next());
        }
        return listData;
    }

    public void addUpdateBalanceId(UpdateBalanceId updateBalanceId) {
        MongoCollection<UpdateBalanceId> collection = database.getCollection("UpdateBalanceId", UpdateBalanceId.class);
        try {
            collection.insertOne(updateBalanceId);
        } catch (Exception ignore) {}
    }

    public void removeUpdateBalanceId(String key, String driverId){
        MongoCollection<UpdateBalanceId> collection = database.getCollection("UpdateBalanceId", UpdateBalanceId.class);
        Bson filter = Filters.and(
                Filters.eq("key", key),
                Filters.eq("driverId", driverId)
        );
        try {
            collection.deleteOne(filter);
        } catch (Exception ignore) {}
    }

    public List<String> getListZone(String fleetId, String currencyISO) {
        MongoCollection<Zone> collection = database.getCollection("Zone", Zone.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("currency.iso", currencyISO)
        );
        MongoCursor<Zone> cursor = collection.find(filter).iterator();
        List<String> listData = new ArrayList<>();
        while(cursor.hasNext()) {
            Zone item = cursor.next();
            listData.add(item._id.toString());
        }
        return listData;
    }

    public List<BankInfo> getBankInfo(String gateway, String environment, String country) {
        MongoCollection<BankInfo> collection = database.getCollection("BankInfo", BankInfo.class);
        Bson filter = Filters.and(
                Filters.eq("gateway", gateway),
                Filters.eq("environment", environment),
                Filters.eq("country", country)
        );
        Bson sorts = Sorts.ascending("bankName");
        MongoCursor<BankInfo> cursor = collection.find(filter).sort(sorts).iterator();
        List<BankInfo> listBank = new ArrayList<>();
        while(cursor.hasNext()) {
            listBank.add(cursor.next());
        }
        return listBank;
    }

    public List<BankInfo> getBankInfo(String gateway, String country) {
        MongoCollection<BankInfo> collection = database.getCollection("BankInfo", BankInfo.class);
        Bson filter = Filters.and(
                Filters.eq("gateway", gateway),
                Filters.eq("country", country)
        );
        Bson sorts = Sorts.ascending("bankName");
        MongoCursor<BankInfo> cursor = collection.find(filter).sort(sorts).iterator();
        List<BankInfo> listBank = new ArrayList<>();
        while(cursor.hasNext()) {
            listBank.add(cursor.next());
        }
        return listBank;
    }

    public BankInfo getBankInfoByBankCode(String gateway, String bankCode, String country) {
        try {
            MongoCollection<BankInfo> collection = database.getCollection("BankInfo", BankInfo.class);
            Bson filter = Filters.and(
                    Filters.eq("gateway", gateway),
                    Filters.eq("bankCode", bankCode),
                    Filters.eq("country", country)
            );
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }
    public Process getSettingPayment (String fleetId){
        MongoCollection<Process> collection = database.getCollection("Process", Process.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public GatewayFirstData getGatewayFirstData(String fleetId) {
        MongoCollection<GatewayFirstData> collection = database.getCollection("GatewayFirstData", GatewayFirstData.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public List<DriverWallet> getAllDriverWallets(int count, int size) {
        MongoCollection<DriverWallet> collection = database.getCollection("DriverWallet", DriverWallet.class);
        List<DriverWallet> listDW = new ArrayList<>();
        MongoCursor<DriverWallet> cursor = collection.find().skip(count*size).limit(size).iterator();
        while(cursor.hasNext()) {
            listDW.add(cursor.next());
        }
        return listDW;
    }

    public Trip getCorpIntercityTrip(String tripId) {
        try {
            MongoCollection<Trip> collection = database.getCollection("Trip", Trip.class);
            Bson filter = Filters.eq("tripId", tripId);
            return collection.find(filter).first();
        } catch(Exception ex) {
            return null;
        }
    }

    public List<DriverWallet> getCashWalletOfDriver(String fleetId) {
        MongoCollection<DriverWallet> collection = database.getCollection("DriverWallet", DriverWallet.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.gt("cashWallet.currentBalance", 0.0)
        );
        List<DriverWallet> listDW = new ArrayList<>();
        MongoCursor<DriverWallet> cursor = collection.find(filter).iterator();
        while(cursor.hasNext()) {
            listDW.add(cursor.next());
        }
        return listDW;
    }

    public GatewayURL getGatewayURL(String gateway, String environment) {
        MongoCollection<GatewayURL> collection = database.getCollection("GatewayURL", GatewayURL.class);
        Bson filter = Filters.and(
                Filters.eq("gateway", gateway),
                Filters.eq("environment", environment)
        );
        return collection.find(filter).first();
    }

    public List<String> getDriverWithCreditBalanceLessThan(String fleetId, double balance, int from, int size){
        List<String> driverIds = new ArrayList<>();
        try {
            MongoCollection<Account> collection = database.getCollection("Account", Account.class);
            Bson filterOr = Filters.or(
                    Filters.exists("driverInfo.creditBalances", false),
                    Filters.size("driverInfo.creditBalances", 0),
                    Filters.lt("driverInfo.creditBalances.value", balance)
            );
            Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("appType", "driver"),
                Filters.eq("isActive", true),
                filterOr
            );
            MongoCursor<Account> cursor = collection.find(filter).skip(from).limit(size).iterator();
            while(cursor.hasNext()) {
                Account item = cursor.next();
                driverIds.add(item.userId);
            }
        } catch(Exception ignore) {}
        return driverIds;
    }

    public Account getAccountFromBoostId(String fleetId, String boostId, String type) {
        MongoCollection<Account> collection = database.getCollection("Account", Account.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("boostId", boostId),
                Filters.eq("appType", type)
                );
        return collection.find(filter).first();
    }

    public Booking getActiveBookingFromBoostId(String fleetId, String boostId) {
        MongoCollection<Booking> collection = database.getCollection("Booking", Booking.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("psgInfo.boostId", boostId)
        );
        return collection.find(filter).first();
    }

    public String addPayoutTime(PayoutTime payoutTime) {
        MongoCollection<PayoutTime> collection = database.getCollection("PayoutTime", PayoutTime.class);
        payoutTime._id = new ObjectId();
        collection.insertOne(payoutTime);
        return payoutTime._id.toString();
    }

    public PayoutTime getPayoutTime(String fleetId) {
        try {
            MongoCollection<PayoutTime> collection = database.getCollection("PayoutTime", PayoutTime.class);
            Bson filter = Filters.eq("fleetId", fleetId);
            Bson sort = Sorts.descending("payoutTime");
            return collection.find(filter).sort(sort).first();
        } catch (Exception ex) {
            return null;
        }
    }

    public org.json.simple.JSONObject getListDriver(String fleetId, String gateway, String companyId, String payoutOption, double minAmount, String currencyISO, int skip, int size) {
        MongoCollection<Account> collection = database.getCollection("Account", Account.class);

        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("appType", "driver"),
                Filters.eq("isActive", true)
        );

        if (!companyId.equals("all")) {
            filter = Filters.and(
                    filter,
                    Filters.eq("driverInfo.company.companyId", companyId)
            );
        }

        if (payoutOption.equals("invalidBank")) {
            filter = Filters.and(
                    filter,
                    Filters.eq("driverInfo.invalidBank", true)
            );
        } else {
            List<String> requireRouting = Arrays.asList(KeysUtil.MOLPAY, KeysUtil.STRIPE);
            List<String> requireBankName = Arrays.asList(KeysUtil.ANY_BANK, KeysUtil.INDIA_DEFAULT);
            if (payoutOption.equals("hasBank")) {
                Bson invalidBank = Filters.or(
                        Filters.exists("driverInfo.invalidBank", false),
                        Filters.eq("driverInfo.invalidBank", false)
                );
                Bson nameOfAccount = Filters.and(
                        Filters.exists("driverInfo.nameOfAccount", true),
                        Filters.ne("driverInfo.nameOfAccount", "")
                );
                Bson accountNumber = Filters.and(
                        Filters.exists("driverInfo.accountNumber", true),
                        Filters.ne("driverInfo.accountNumber", "")
                );
                filter = Filters.and(filter, invalidBank, nameOfAccount, accountNumber);

                if (requireRouting.contains(gateway)) {
                    Bson routingNumber = Filters.and(
                            Filters.exists("driverInfo.routingNumber", true),
                            Filters.ne("driverInfo.routingNumber", "")
                    );
                    filter = Filters.and(filter, routingNumber);
                }

                if (requireBankName.contains(gateway)) {
                    Bson nameOfBank = Filters.and(
                            Filters.exists("driverInfo.nameOfBank", true),
                            Filters.ne("driverInfo.nameOfBank", "")
                    );
                    filter = Filters.and(filter, nameOfBank);
                }
                if (gateway.equals(KeysUtil.INDIA_DEFAULT)) {
                    Bson IFSCCode = Filters.and(
                            Filters.exists("driverInfo.IFSCCode", true),
                            Filters.ne("driverInfo.IFSCCode", "")
                    );
                    filter = Filters.and(filter, IFSCCode);
                }
            } else if (payoutOption.equals("noBank")) {
                Bson noBank = Filters.or(
                        Filters.exists("driverInfo.nameOfAccount", false),
                        Filters.eq("driverInfo.nameOfAccount", ""),
                        Filters.exists("driverInfo.accountNumber", false),
                        Filters.eq("driverInfo.accountNumber", "")
                );
                if (requireRouting.contains(gateway)) {
                    Bson routingNumber = Filters.or(
                            Filters.exists("driverInfo.routingNumber", false),
                            Filters.eq("driverInfo.routingNumber", "")
                    );
                    noBank = Filters.or(noBank, routingNumber);
                }
                if (requireBankName.contains(gateway)) {
                    Bson nameOfBank = Filters.or(
                            Filters.exists("driverInfo.nameOfBank", false),
                            Filters.eq("driverInfo.nameOfBank", "")
                    );
                    noBank = Filters.or(noBank, nameOfBank);
                }
                if (gateway.equals(KeysUtil.INDIA_DEFAULT)) {
                    Bson IFSCCode = Filters.or(
                            Filters.exists("driverInfo.IFSCCode", false),
                            Filters.eq("driverInfo.IFSCCode", "")
                    );
                    noBank = Filters.or(noBank, IFSCCode);
                }
                filter = Filters.and(filter, noBank);
            }
        }
        Bson sort = Sorts.ascending("createdDate");
        Bson projections = Projections.fields(Projections.include("userId"), Projections.excludeId());
        MongoCursor<Account> cursor = collection.find(filter).skip(skip).limit(size).projection(projections).sort(sort).iterator();
        List<String> listDriverId = new ArrayList<>();
        while(cursor.hasNext()) {
            Account item = cursor.next();
            listDriverId.add(item.userId);
        }
        return returnListDriver(fleetId, listDriverId, currencyISO);
    }

    private org.json.simple.JSONObject returnListDriver(String fleetId, List<String> listDriverId, String currencyISO) {
        // get cash balance of all driver in the list
        // for some driver who did not initiate DriverWallet, so need to manually add to the arrDriver list
        // initiate list driver who already has credit wallet
        List<String> driverHasDriverWallet = new ArrayList<>();
        MongoCollection<DriverWallet> collection = database.getCollection("DriverWallet", DriverWallet.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.in("userId", listDriverId),
                Filters.eq("cashWallet.currencyISO", currencyISO)
        );
        Bson projections = Projections.fields(Projections.include("userId", "cashWallet.currentBalance"), Projections.excludeId());
        MongoCursor<DriverWallet> cursor = collection.find(filter).projection(projections).iterator();
        JSONArray arrDriver = new JSONArray();
        while(cursor.hasNext()) {
            DriverWallet item = cursor.next();
            driverHasDriverWallet.add(item.userId);
            org.json.simple.JSONObject objData = new org.json.simple.JSONObject();
            objData.put("driverId", item.userId);
            objData.put("cashBalance", item.cashWallet.currentBalance);
            arrDriver.add(objData);
        }
        if (listDriverId.size() > driverHasDriverWallet.size()) {
            // there are some driver did not initiate DriverWallet
            // add a default value
            for (String driverId : listDriverId) {
                if (!driverHasDriverWallet.contains(driverId)) {
                    org.json.simple.JSONObject objData = new org.json.simple.JSONObject();
                    objData.put("driverId", driverId);
                    objData.put("cashBalance", 0.0);
                    arrDriver.add(objData);
                }
            }
        }

        org.json.simple.JSONObject objReturn = new org.json.simple.JSONObject();
        objReturn.put("size", listDriverId.size());
        objReturn.put("data", arrDriver);
        return objReturn;
    }

    public void addLogs(Logs logs) {
        MongoCollection<Logs> collection = database.getCollection("Logs", Logs.class);
        try {
            collection.insertOne(logs);
        } catch (Exception ignore) {}

    }

    public List<String> getActiveDrivers(String fleetId, boolean delivery, int skip, int size) {
        List<String> listType = Arrays.asList("Delivery by Car", "Delivery by Bike");
        MongoCollection<Account> collection = database.getCollection("Account", Account.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("appType", "driver"),
                Filters.nin("driverInfo.carType.name", listType),
                Filters.eq("isActive", true)
        );
        if (delivery) {
            filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("appType", "driver"),
                    Filters.in("driverInfo.carType.name", listType),
                    Filters.eq("isActive", true)
            );
        }
        Bson sort = Sorts.descending("createdDate");
        Bson projections = Projections.fields(Projections.include("userId"), Projections.exclude("_id"));
        MongoCursor<Account> cursor = collection.find(filter).skip(skip).limit(size).sort(sort).projection(projections).iterator();
        List<String> listData = new ArrayList<>();
        while(cursor.hasNext()) {
            Account item = cursor.next();
            listData.add(item.userId);
        }
        return listData;
    }

    public List<String> getActiveTransportDrivers(String fleetId, int skip, int size) {
        MongoCollection<Account> collection = database.getCollection("Account", Account.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("appType", "driver"),
                Filters.eq("driverInfo.driverType", "Fleet Owner"),
                Filters.eq("isActive", true)
        );
        Bson sort = Sorts.descending("createdDate");
        Bson projections = Projections.fields(Projections.include("userId"), Projections.exclude("_id"));
        MongoCursor<Account> cursor = collection.find(filter).skip(skip).limit(size).sort(sort).projection(projections).iterator();
        List<String> listData = new ArrayList<>();
        while(cursor.hasNext()) {
            Account item = cursor.next();
            listData.add(item.userId);
        }
        return listData;
    }

    public boolean updateBankStatus(String requestId, String fleetId, String userId, boolean status) {
        try {
            MongoCollection<Account> collection = database.getCollection("Account", Account.class);
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("userId", userId)
            );
            Bson update = Updates.combine(
                    Updates.set("driverInfo.invalidBank", status),
                    Updates.currentDate("latestUpdate")
            );
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        } catch (Exception ex) {
            logger.debug(requestId + " - updateBankStatus error: " + CommonUtils.getError(ex));
            return false;
        }
    }

    public VoucherCode getVoucherCode(String fleetId, String voucherCode) {
        MongoCollection<VoucherCode> collection = database.getCollection("VoucherCode", VoucherCode.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("voucherCode", voucherCode)
        );
        return collection.find(filter).first();
    }

    public boolean updateVoucherCodeExpired() {
        MongoCollection<VoucherCode> collection = database.getCollection("VoucherCode", VoucherCode.class);
        Bson filter = Filters.and(
                Filters.eq("isUsed", ""),
                Filters.eq("isActive", true),
                Filters.lte("validTo", TimezoneUtil.offsetTimeZone(new Date(), Calendar.getInstance().getTimeZone().getID(), "GMT"))
        );

        Bson update = Updates.combine(
                Updates.set("isUsed", "expired"),
                Updates.currentDate("latestUpdate")
        );
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public boolean updateVoucherCode(String fleetId, String voucherCode, String status) {
        MongoCollection<VoucherCode> collection = database.getCollection("VoucherCode", VoucherCode.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("voucherCode", voucherCode)
        );
        Bson update = Updates.combine(
                Updates.set("isUsed", status),
                Updates.currentDate("latestUpdate")
        );
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public List<String> getAllActiveDrivers(String fleetId, int skip, int size) {
        MongoCollection<Account> collection = database.getCollection("Account", Account.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("appType", "driver"),
                Filters.eq("isActive", true)
        );
        Bson sort = Sorts.descending("createdDate");
        Bson projections = Projections.fields(Projections.include("userId"), Projections.exclude("_id"));
        MongoCursor<Account> cursor = collection.find(filter).skip(skip).limit(size).sort(sort).projection(projections).iterator();
        List<String> listData = new ArrayList<>();
        while(cursor.hasNext()) {
            Account item = cursor.next();
            listData.add(item.userId);
        }
        return listData;
    }

    public ConfigWallet getConfigWallet(String fleetId, String gateway) {
        MongoCollection<ConfigWallet> collection = database.getCollection("ConfigWallet", ConfigWallet.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("gateway", gateway)
        );
        return collection.find(filter).first();
    }

    public PaymentChannel getPaymentChannel(String fleetId, String gateway, String type, String channel) {
        MongoCollection<PaymentChannel> collection = database.getCollection("PaymentChannel", PaymentChannel.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("gateway", gateway),
                Filters.eq("type", type),
                Filters.eq("channel", channel)
        );
        return collection.find(filter).first();
    }

    public ConfigWallet getPaymentChannel(String fleetId, String gateway) {
        MongoCollection<ConfigWallet> collection = database.getCollection("ConfigWallet", ConfigWallet.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("gateway", gateway),
                Filters.eq("isActive", true)
        );
        return collection.find(filter).first();
    }

    public WalletMOLPay getWalletMOLPay(String fleetId) {
        MongoCollection<WalletMOLPay> collection = database.getCollection("WalletMOLPay", WalletMOLPay.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public WalletGCash getWalletGCash(String fleetId) {
        MongoCollection<WalletGCash> collection = database.getCollection("WalletGCash", WalletGCash.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public WalletTnG getWalletTnG(String fleetId) {
        MongoCollection<WalletTnG> collection = database.getCollection("WalletTnG", WalletTnG.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public WalletBoost getWalletBoost(String fleetId) {
        MongoCollection<WalletBoost> collection = database.getCollection("WalletBoost", WalletBoost.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public WalletMoMo getWalletMoMo(String fleetId) {
        MongoCollection<WalletMoMo> collection = database.getCollection("WalletMoMo", WalletMoMo.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public WalletVipps getWalletVipps(String fleetId) {
        MongoCollection<WalletVipps> collection = database.getCollection("WalletVipps", WalletVipps.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public MenuMerchant getMerchant(String merchantId){
        MongoCollection<MenuMerchant> collection = database.getCollection("MenuMerchant", MenuMerchant.class);
        Bson filter = Filters.eq("_id", new ObjectId(merchantId));
        return collection.find(filter).first();
    }

    public GatewayDNB getGatewayDNB(String fleetId) {
        MongoCollection<GatewayDNB> collection = database.getCollection("GatewayDNB", GatewayDNB.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public CCPackages getCCPackages(String fleetId) {
        MongoCollection<CCPackages> collection = database.getCollection("CCPackages", CCPackages.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public List<String> getListMerchant(String fleetId, String gateway, String payoutOption, String currencyISO, int skip, int size) {
        MongoCollection<MenuMerchant> collection = database.getCollection("MenuMerchant", MenuMerchant.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("available", true),
                Filters.eq("isPreferred", true)
        );
        if (payoutOption.equals("invalidBank")) {
            filter = Filters.and(
                    filter,
                    Filters.eq("bankInfo.invalidBank", true)
            );
        } else {
            if (gateway.equals(KeysUtil.MOLPAY)) {
                if (payoutOption.equals("hasBank")) {
                    /*"bankInfo" : {
                        "accountHolder" : "QA Test",
                        "accountNumber" : "123456789",
                        "bankName" : "BOFAMY2X",
                        "IDnumber" : "12346548971",
                        "IDtype" : "OLDNRIC",
                        "bankCode" : "PEMBMYK1"
                    },*/
                    Bson validBank = Filters.or(
                            Filters.exists("bankInfo.invalidBank", false),
                            Filters.eq("bankInfo.invalidBank", false)
                    );
                    Bson nameOfAccount = Filters.and(
                            Filters.exists("bankInfo.accountHolder", true),
                            Filters.ne("bankInfo.accountHolder", "")
                    );
                    Bson accountNumber = Filters.and(
                            Filters.exists("bankInfo.accountNumber", true),
                            Filters.ne("bankInfo.accountNumber", "")
                    );
                    Bson IDnumber = Filters.and(
                            Filters.exists("bankInfo.IDnumber", true),
                            Filters.ne("bankInfo.IDnumber", "")
                    );
                    Bson routingNumber = Filters.and(
                            Filters.exists("bankInfo.bankCode", true),
                            Filters.ne("bankInfo.bankCode", "")
                    );
                    filter = Filters.and(filter, validBank, nameOfAccount, accountNumber, IDnumber, routingNumber);
                } else if (payoutOption.equals("noBank")) {
                    Bson noBank = Filters.or(
                            Filters.exists("bankInfo.accountHolder", false),
                            Filters.eq("bankInfo.accountHolder", ""),
                            Filters.exists("bankInfo.accountNumber", false),
                            Filters.eq("bankInfo.accountNumber", ""),
                            Filters.exists("bankInfo.IDnumber", false),
                            Filters.eq("bankInfo.IDnumber", ""),
                            Filters.exists("bankInfo.bankCode", false),
                            Filters.eq("bankInfo.accountHolder", "")
                    );
                    filter = Filters.and(filter, noBank);
                }
            }
            else if (gateway.equals(KeysUtil.STRIPE)) {
                if (payoutOption.equals("hasBank")) {
                    /*"bankInfo" : {
                        "accountHolder" : "Texas 06",
                        "accountNumber" : "000123456789",
                        "address" : "1033 Young St, Dallas, TX 75202, Hoa Kỳ",
                        "bankToken" : "acct_1JI3T04hTKRJ0leK",
                        "birthDay" : "05/02/1995",
                        "city" : "Dallas",
                        "isBankAccountOwner" : true,
                        "postalCode" : "75202",
                        "routingNumber" : "110000000",
                        "ssn" : "123456789",
                        "state" : "TX",
                        "verificationDocumentBack" : "https://lab-gojo.s3.ap-southeast-1.amazonaws.com/images/gojotexas/merchant/1627442954861_untitle_2_d_png",
                        "verificationDocumentFront" : "https://lab-gojo.s3.ap-southeast-1.amazonaws.com/images/gojotexas/merchant/1627442954890_add_error_jpg",
                        "isBankVerified" : true
                    },*/
                    Bson validBank = Filters.or(
                            Filters.exists("bankInfo.invalidBank", false),
                            Filters.eq("bankInfo.invalidBank", false)
                    );
                    Bson accountNumber = Filters.and(
                            Filters.exists("bankInfo.bankToken", true),
                            Filters.ne("bankInfo.bankToken", "")
                    );
                    filter = Filters.and(filter, validBank, accountNumber);
                } else if (payoutOption.equals("noBank")) {
                    Bson noBank = Filters.or(
                            Filters.exists("bankInfo.bankToken", false),
                            Filters.eq("bankInfo.bankToken", "")
                    );
                    filter = Filters.and(filter, noBank);
                }
            }
            else if (gateway.equals(KeysUtil.ANY_BANK)) {
                if (payoutOption.equals("hasBank")) {
                    Bson validBank = Filters.or(
                            Filters.exists("bankInfo.invalidBank", false),
                            Filters.eq("bankInfo.invalidBank", false)
                    );
                    Bson bankName = Filters.and(
                            Filters.exists("bankInfo.bankName", true),
                            Filters.ne("bankInfo.bankName", "")
                    );
                    Bson accountHolder = Filters.and(
                            Filters.exists("bankInfo.accountHolder", true),
                            Filters.ne("bankInfo.accountHolder", "")
                    );
                    Bson accountNumber = Filters.and(
                            Filters.exists("bankInfo.accountNumber", true),
                            Filters.ne("bankInfo.accountNumber", "")
                    );
                    filter = Filters.and(validBank, bankName, accountNumber, accountHolder);
                } else if (payoutOption.equals("noBank")) {
                    Bson noBank = Filters.or(
                            Filters.exists("bankInfo.bankName", false),
                            Filters.eq("bankInfo.bankName", ""),
                            Filters.exists("bankInfo.accountHolder", false),
                            Filters.eq("bankInfo.accountHolder", ""),
                            Filters.exists("bankInfo.accountNumber", false),
                            Filters.eq("bankInfo.accountNumber", "")
                    );
                    filter = Filters.and(filter, noBank);
                }
            }
            else if (gateway.equals(KeysUtil.INDIA_DEFAULT)) {
                if (payoutOption.equals("hasBank")) {
                    Bson validBank = Filters.or(
                            Filters.exists("bankInfo.invalidBank", false),
                            Filters.eq("bankInfo.invalidBank", false)
                    );
                    Bson bankName = Filters.and(
                            Filters.exists("bankInfo.bankName", true),
                            Filters.ne("bankInfo.bankName", "")
                    );
                    Bson accountHolder = Filters.and(
                            Filters.exists("bankInfo.accountHolder", true),
                            Filters.ne("bankInfo.accountHolder", "")
                    );
                    Bson accountNumber = Filters.and(
                            Filters.exists("bankInfo.accountNumber", true),
                            Filters.ne("bankInfo.accountNumber", "")
                    );
                    Bson IFSCCode = Filters.and(
                            Filters.exists("bankInfo.IFSCCode", true),
                            Filters.ne("bankInfo.IFSCCode", "")
                    );
                    filter = Filters.and(filter, validBank, bankName, accountHolder, accountNumber, IFSCCode);
                } else if (payoutOption.equals("noBank")) {
                    Bson noBank = Filters.or(
                            Filters.exists("bankInfo.bankName", false),
                            Filters.eq("bankInfo.bankName", ""),
                            Filters.exists("bankInfo.accountHolder", false),
                            Filters.eq("bankInfo.accountHolder", ""),
                            Filters.exists("bankInfo.accountNumber", false),
                            Filters.eq("bankInfo.accountNumber", ""),
                            Filters.exists("bankInfo.IFSCCode", false),
                            Filters.eq("bankInfo.IFSCCode", "")
                    );
                    filter = Filters.and(filter, noBank);
                }
            }
        }
        Bson projection = Projections.fields(Projections.include("userId"), Projections.excludeId());
        Bson sort = Sorts.ascending("createdDate");
        MongoCursor<MenuMerchant> cursor  = collection.find(filter).projection(projection).sort(sort).skip(skip).limit(size).iterator();
        List<String> listMerchantId = new ArrayList<>();
        while(cursor.hasNext()) {
            MenuMerchant menuMerchant = cursor.next();
            listMerchantId.add(menuMerchant._id.toString());
        }
        return listMerchantId;
    }

    public boolean updateBankStatusForMerchant(String requestId, String fleetId, String merchantId, boolean status) {
        try {
            MongoCollection<MenuMerchant> collection = database.getCollection("MenuMerchant", MenuMerchant.class);
            Bson filter = Filters.eq("_id", new ObjectId(merchantId));
            Bson update = Updates.combine(
                    Updates.set("bankInfo.invalidBank", status),
                    Updates.currentDate("updated_at"));
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        } catch (Exception ex) {
            logger.debug(requestId + " - updateBankStatusForMerchant error: " + CommonUtils.getError(ex));
            return false;
        }
    }

    public GatewayTSYS getGatewayTSYS(String fleetId) {
        MongoCollection<GatewayTSYS> collection = database.getCollection("GatewayTSYS", GatewayTSYS.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public GatewayMADA getGatewayMADA(String fleetId) {
        MongoCollection<GatewayMADA> collection = database.getCollection("GatewayMADA", GatewayMADA.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }


    public MenuMerchantUser getMenuMerchantUser(String adminId) {
        MongoCollection<MenuMerchantUser> collection = database.getCollection("MenuMerchantUser", MenuMerchantUser.class);
        Bson filter = Filters.eq("_id", new ObjectId(adminId));
        return collection.find(filter).first();
    }

    public DriverFields getDriverFields(String fleetId, String fieldKey) {
        MongoCollection<DriverFields> collection = database.getCollection("DriverFields", DriverFields.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("fieldKey", fieldKey)
        );
        return collection.find(filter).first();
    }

    public boolean updateBankVerifyForMerchant(String requestId, String merchantId){
        try {
            MongoCollection<MenuMerchant> collection = database.getCollection("MenuMerchant", MenuMerchant.class);
            Bson filter = Filters.eq("_id", new ObjectId(merchantId));
            Bson update = Updates.combine(
                    Updates.set("bankInfo.isBankVerified", true),
                    Updates.currentDate("updated_at"));
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        } catch (Exception ex) {
            logger.debug(requestId + " - updateBankVerifyForMerchant error: " + CommonUtils.getError(ex));
            return false;
        }
    }

    public List<String> getActiveDriversByType(String requestId, String fleetId, List<String> driverTypes, int skip, int size) {
        MongoCollection<Account> collection = database.getCollection("Account", Account.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("appType", "driver"),
                Filters.in("driverInfo.driverType", driverTypes),
                Filters.eq("isActive", true)
        );
        Bson sort = Sorts.descending("createdDate");
        Bson projections = Projections.fields(Projections.include("userId"), Projections.exclude("_id"));
        MongoCursor<Account> cursor = collection.find(filter).skip(skip).limit(size).sort(sort).projection(projections).iterator();
        List<String> listData = new ArrayList<>();
        while(cursor.hasNext()) {
            Account item = cursor.next();
            listData.add(item.userId);
        }
        return listData;
    }

    public WalletYenePay getWalletYenePay(String fleetId) {
        MongoCollection<WalletYenePay> collection = database.getCollection("WalletYenePay", WalletYenePay.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public WalletZainCash getWalletZainCash(String fleetId) {
        MongoCollection<WalletZainCash> collection = database.getCollection("WalletZainCash", WalletZainCash.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public GatewayPayMaya getGatewayPayMaya(String fleetId) {
        MongoCollection<GatewayPayMaya> collection = database.getCollection("GatewayPayMaya", GatewayPayMaya.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public String getConfigInfo(String fleetId, String tableName) {
        MongoCollection<Document> collection = database.getCollection(tableName);
        Bson filter = Filters.eq("fleetId", fleetId);
        Document doc = collection.find(filter).first();
        return gson.toJson(doc);
    }

    public List<String> getAllPassengerRegisteredCard(String requestId, String fleetId, int skip, int size) {
        MongoCollection<Account> collection = database.getCollection("Account", Account.class);
        Bson filterCredit = Filters.and(
                Filters.exists("credits"),
                Filters.not(Filters.size("credits", 0))
        );
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("appType", "passenger"),
                Filters.eq("isActive", true),
                filterCredit
        );
        Bson sort = Sorts.descending("createdDate");
        Bson projections = Projections.fields(Projections.include("userId"), Projections.exclude("_id"));
        MongoCursor<Account> cursor = collection.find(filter).skip(skip).limit(size).sort(sort).projection(projections).iterator();
        List<String> listData = new ArrayList<>();
        while(cursor.hasNext()) {
            Account item = cursor.next();
            listData.add(item.userId);
        }
        return listData;
    }

    public MerchantWallet getMerchantWallet(String fleetId, String merchantId) {
        MongoCollection<MerchantWallet> collection = database.getCollection("MerchantWallet", MerchantWallet.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("merchantId", merchantId)
        );
        try {
            return collection.find(filter).first();
        } catch(Exception ex) {
            System.out.println("==== Error mapping MerchantWallet for merchantId " + merchantId);
            return null;
        }
    }

    public void addMerchantWallet(MerchantWallet merchantWallet) {
        MongoCollection<MerchantWallet> collection = database.getCollection("MerchantWallet", MerchantWallet.class);
        try {
            collection.insertOne(merchantWallet);
        } catch (Exception ignore) {}

    }

    public boolean updateMerchantWallet(String fleetId, String merchantId, String walletType, AmountByCurrency creditWallet, BalanceByCurrency cashWallet) {
        MongoCollection<MerchantWallet> collection = database.getCollection("MerchantWallet", MerchantWallet.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("merchantId", merchantId)
        );
        UpdateResult result;
        switch (walletType) {
            case "credit": {
                Bson update = Updates.combine(
                        Updates.set("creditWallet", creditWallet),
                        Updates.currentDate("latestUpdate")
                );
                result = collection.updateOne(filter, update);
                break;
            }
            case "cash": {
                Bson update = Updates.combine(
                        Updates.set("cashWallet", cashWallet),
                        Updates.currentDate("latestUpdate")
                );
                result = collection.updateOne(filter, update);
                break;
            }
            default:
                result = null;
                break;
        }
        return result != null && result.wasAcknowledged();
    }

    public GatewayBankOfGeorgia getGatewayBOG(String fleetId) {
        MongoCollection<GatewayBankOfGeorgia> collection = database.getCollection("GatewayBankOfGeorgia", GatewayBankOfGeorgia.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public boolean updateStripeConnectStatusOperation(String requestId, String userId, String status){
        try {
            MongoCollection<Operations> collection = database.getCollection("Operations", Operations.class);
            Bson filter = Filters.eq("driverId", userId);
            Bson update = Updates.combine(
                    Updates.set("stripeConnectStatus", status),
                    Updates.currentDate("latestUpdate")
            );
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        } catch (Exception ex) {
            logger.debug(requestId + " - updateStripeConnectStatusOperation error: " + CommonUtils.getError(ex));
            return false;
        }
    }

    public boolean updateStripeConnectStatus(String requestId, String fleetId, String userId, String token, String status) {
        try {
            MongoCollection<Account> collection = database.getCollection("Account", Account.class);
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("userId", userId)
            );
            Bson update = Updates.combine(
                    Updates.set("driverInfo.stripeConnectStatus", status),
                    Updates.set("driverInfo.achToken", token),
                    Updates.set("driverInfo.isBankVerified", true),
                    Updates.currentDate("latestUpdate")
            );
            if (status.equals("activated")) {
                update = Updates.combine(
                        Updates.set("driverInfo.signupStep", "stripeConnect"),
                        Updates.set("driverInfo.stripeConnectStatus", status),
                        Updates.set("driverInfo.achToken", token),
                        Updates.currentDate("latestUpdate")
                );
            }
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        } catch (Exception ex) {
            logger.debug(requestId + " - updateStripeConnectStatus error: " + CommonUtils.getError(ex));
            return false;
        }
    }

    public boolean disconnectStripe(String requestId, String fleetId, String userId, boolean updateStatus) {
        try {
            MongoCollection<Account> collection = database.getCollection("Account", Account.class);
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.eq("userId", userId)
            );
            Bson update = Updates.combine(
                    Updates.set("driverInfo.stripeConnectStatus", ""),
                    Updates.set("driverInfo.achToken", ""),
                    Updates.currentDate("latestUpdate")
            );
            UpdateResult result = collection.updateOne(filter, update);
            if (result.wasAcknowledged() && updateStatus) {
                Bson updateAccountStatus = Updates.combine(
                        Updates.set("isActive", false),
                        Updates.set("driverInfo.isActivate", false),
                        Updates.set("driverInfo.statusReview", "inProgress"),
                        Updates.currentDate("latestUpdate")
                );
                UpdateResult resultUpdate = collection.updateOne(filter, updateAccountStatus);
                return resultUpdate.wasAcknowledged();
            } else {
                return result.wasAcknowledged();
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - disconnectStripe error: " + CommonUtils.getError(ex));
            return false;
        }
    }

    public boolean updateStripeConnectStatusForFleet(String requestId, String fleetId, String token, String status) {
        try {
            MongoCollection<Fleet> collection = database.getCollection("Fleet", Fleet.class);
            Bson filter = Filters.eq("fleetId", fleetId);
            Bson update = Updates.combine(
                    Updates.set("stripeConnect.connectStatus", status),
                    Updates.set("stripeConnect.connectToken", token),
                    Updates.currentDate("latestUpdate")
            );
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        } catch (Exception ex) {
            logger.debug(requestId + " - updateStripeConnectStatusForFleet error: " + CommonUtils.getError(ex));
            return false;
        }
    }

    public boolean disconnectStripeForFleet(String requestId, String fleetId) {
        try {
            MongoCollection<Fleet> collection = database.getCollection("Fleet", Fleet.class);
            Bson filter = Filters.eq("fleetId", fleetId);
            Bson update = Updates.combine(
                    Updates.set("stripeConnect.connectStatus", ""),
                    Updates.set("stripeConnect.connectToken", ""),
                    Updates.currentDate("latestUpdate")
            );
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        } catch (Exception ex) {
            logger.debug(requestId + " - disconnectStripeForFleet error: " + CommonUtils.getError(ex));
            return false;
        }
    }

    public boolean updateStripeConnectStatusForCompany(String requestId, String fleetId, String companyId, String token, String status) {
        try {
            MongoCollection<Company> collection = database.getCollection("Company", Company.class);
            Bson filter = Filters.eq("_id", new ObjectId(companyId));
            Bson update = Updates.combine(
                    Updates.set("connectStatus", status),
                    Updates.set("connectToken", token),
                    Updates.currentDate("latestUpdate")
            );
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        } catch (Exception ex) {
            logger.debug(requestId + " - updateStripeConnectStatusForCompany error: " + CommonUtils.getError(ex));
            return false;
        }
    }

    public boolean disconnectStripeForCompany(String requestId, String fleetId, String companyId) {
        try {
            MongoCollection<Company> collection = database.getCollection("Company", Company.class);
            Bson filter = Filters.eq("_id", new ObjectId(companyId));
            Bson update = Updates.combine(
                    Updates.set("connectStatus", ""),
                    Updates.set("connectToken", ""),
                    Updates.currentDate("latestUpdate")
            );
            UpdateResult result = collection.updateOne(filter, update);
            return result.wasAcknowledged();
        } catch (Exception ex) {
            logger.debug(requestId + " - disconnectStripeForCompany error: " + CommonUtils.getError(ex));
            return false;
        }
    }

    public List<String> getListCarTypeByAffiliate(String fleetId, String affiliateCarTypeId) {
        try {
            MongoCollection<VehicleType> collection = database.getCollection("VehicleType", VehicleType.class);
            Bson filter = Filters.and(
                    Filters.eq("fleetId", fleetId),
                    Filters.in("affiliateCarType", affiliateCarTypeId),
                    Filters.in("isActive", true)
            );
            MongoCursor<VehicleType> cursor = collection.find(filter).iterator();
            List<String> listCarTypeName = new ArrayList<>();
            while(cursor.hasNext()) {
                VehicleType vehicleType = cursor.next();
                listCarTypeName.add(vehicleType.vehicleType);
            }
            return listCarTypeName;
        } catch(Exception ex) {
            return null;
        }
    }

    public AffiliateSetting getAffiliateSetting() {
        MongoCollection<AffiliateSetting> collection = database.getCollection("AffiliateSetting", AffiliateSetting.class);
        return collection.find().first();
    }

    public AffiliateMarkupPrice getAffiliateMarkupPrice(String fleetId) {
        MongoCollection<AffiliateMarkupPrice> collection = database.getCollection("AffiliateMarkupPrice", AffiliateMarkupPrice.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public List<String> getListFleetToPayout(String requestId, String payoutOption, int skip, int size) {
        MongoCollection<PricingPlan> collection = database.getCollection("PricingPlan", PricingPlan.class);
        Bson hydra = Filters.or(
            Filters.eq("affiliate.dispatching", true),
            Filters.eq("affiliate.receiveOndemandBooking", true),
            Filters.eq("affiliate.receiveReservationBooking", true)
        );

        Bson filter;
        if (payoutOption.equals("hasBank")) {
            //finalQuery.add(new BasicDBObject("affiliate.isActiveBankAccount", true));
            Bson bankName = Filters.and(
                    Filters.exists("affiliate.bankName", true),
                    Filters.ne("affiliate.bankName", "")
            );
            Bson holderName = Filters.and(
                    Filters.exists("affiliate.holderName", true),
                    Filters.ne("affiliate.holderName", "")
            );
            Bson accountNumber = Filters.and(
                    Filters.exists("affiliate.accountNumber", true),
                    Filters.ne("affiliate.accountNumber", "")
            );
            filter = Filters.and(hydra, bankName, holderName, accountNumber);
        } else if (payoutOption.equals("noBank")) {
            Bson noBank = Filters.or(
                Filters.exists("affiliate.bankName", false),
                Filters.eq("affiliate.bankName", ""),
                Filters.exists("affiliate.holderName", false),
                Filters.eq("affiliate.holderName", ""),
                Filters.exists("affiliate.accountNumber", false),
                Filters.eq("affiliate.accountNumber", "")
            );
            filter = Filters.and(hydra, noBank);
        } else {
            filter = Filters.empty();
        }
        Bson projection = Projections.fields(Projections.include("userId"), Projections.excludeId());
        Bson sort = Sorts.ascending("createdDate");
        MongoCursor<PricingPlan> cursor  = collection.find(filter).projection(projection).sort(sort).skip(skip).limit(size).iterator();
        List<String> listFleetId = new ArrayList<>();
        while(cursor.hasNext()) {
            PricingPlan pricingPlan = cursor.next();
            listFleetId.add(pricingPlan.fleetId);
        }
        return listFleetId;
    }

    public Company getCompanyById(String companyId) {
        try{
            MongoCollection<Company> collection = database.getCollection("Company", Company.class);
            Bson filter = Filters.eq("_id", new ObjectId(companyId));
            return collection.find(filter).first();
        }catch (Exception e){
            e.getMessage();
            return null;
        }
    }

    public GatewayPayWay getGatewayPayWay(String fleetId) {
        MongoCollection<GatewayPayWay> collection = database.getCollection("GatewayPayWay", GatewayPayWay.class);
        Bson filter = Filters.eq("fleetId", fleetId);
        return collection.find(filter).first();
    }

    public List<String> getWingBankAPIKey() {
        try {
            MongoCollection<WalletWingBank> collection = database.getCollection("WalletWingBank", WalletWingBank.class);
            MongoCursor<WalletWingBank> cursor = collection.find().iterator();
            List<String> listKey = new ArrayList<>();
            while(cursor.hasNext()) {
                WalletWingBank walletWingBank = cursor.next();
                String apiKey = walletWingBank.apiKey != null ? walletWingBank.apiKey : "";
                if (!apiKey.isEmpty() && !listKey.contains(apiKey))
                    listKey.add(apiKey);
            }
            return listKey;
        } catch(Exception ex) {
            return null;
        }
    }

    public String getDroppedOffBooking(String fleetId, String userId) {
        MongoCollection<Booking> collection = database.getCollection("Booking", Booking.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("status", "droppedOff"),
                Filters.eq("psgInfo.userId", userId)
        );
        try {
            Booking booking = collection.find(filter).first();
            return booking != null ? booking.bookId : "";
        } catch(Exception ex) {
            return "";
        }
    }

    public void addLogToken(LogToken logToken) {
        MongoCollection<LogToken> collection = database.getCollection("LogToken", LogToken.class);
        try {
            collection.insertOne(logToken);
        } catch (Exception ignore) {}
    }

    public LogToken getLogToken(String fleetId, String logId) {
        MongoCollection<LogToken> collection = database.getCollection("LogToken", LogToken.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("logId", logId)
        );
        return collection.find(filter).first();
    }

    public void deleteLogToken(String fleetId, String logId) {
        MongoCollection<LogToken> collection = database.getCollection("LogToken", LogToken.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("logId", logId)
        );
        collection.deleteOne(filter);
    }

    public List<String> getCorporateByFleet(String requestId, String fleetId) {
        MongoCollection<Corporate> collection = database.getCollection("Corporate", Corporate.class);
        Bson filterCredit = Filters.and(
                Filters.exists("credits"),
                Filters.not(Filters.size("credits", 0))
        );
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("isActive", true),
                filterCredit
        );
        MongoCursor<Corporate> cursor = collection.find(filter).iterator();
        List<String> listData = new ArrayList<>();
        while(cursor.hasNext()) {
            Corporate item = cursor.next();
            listData.add(item._id.toString());
        }
        return listData;
    }

    public ZonePayment getZonePayment(String fleetId, String zoneId) {
        MongoCollection<ZonePayment> collection = database.getCollection("ZonePayment", ZonePayment.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("zoneId", zoneId)
        );
        return collection.find(filter).first();
    }

    public Booking getBookingByInvoiceId(String fleetId, long invoiceId) {
        MongoCollection<Booking> collection = database.getCollection("Booking", Booking.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("invoiceId", invoiceId)
        );
        Booking booking = collection.find(filter).first();
        if (booking == null) {
            collection = database.getCollection("BookingCompleted", Booking.class);
            booking = collection.find(filter).first();
        }
        return booking;
    }

    public Invoice getInvoice(String fleetId, long invoiceId) {
        MongoCollection<Invoice> collection = database.getCollection("Invoice", Invoice.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("invoiceId", invoiceId)

        );
        return collection.find(filter).first();
    }

    public List<String> getListBookingOfInvoice(String fleetId, long invoiceId) {
        MongoCollection<Booking> collection = database.getCollection("BookingCompleted", Booking.class);
        Bson filter = Filters.and(
                Filters.eq("fleetId", fleetId),
                Filters.eq("invoiceId", invoiceId)

        );
        Bson projections = Projections.fields(Projections.include("bookId"), Projections.exclude("_id"));
        MongoCursor<Booking> cursor = collection.find(filter).projection(projections).iterator();
        List<String> listBookId = new ArrayList<>();
        while(cursor.hasNext()) {
            Booking booking = cursor.next();
            listBookId.add(booking.bookId);
        }
        return listBookId;
    }

    public boolean updateInvoice(String requestId, long invoiceId, JSONObject updateObj){
        try {
            MongoCollection<Invoice> collection = database.getCollection("Invoice", Invoice.class);
            Bson filter = Filters.eq("invoiceId", invoiceId);
            Invoice invoice = collection.find(filter).first();
            if (invoice != null) {
                Bson update = Updates.combine(
                        Updates.set("status", updateObj.getInt("status")),
                        Updates.set("transactionId", updateObj.getString("transactionId")),
                        Updates.set("paymentMethod", updateObj.getString("paymentMethod")),
                        Updates.set("paidAmount", updateObj.getDouble("paidAmount")),
                        Updates.set("cardType", updateObj.getString("cardType")),
                        Updates.set("cardMasked", updateObj.getString("cardMasked")),
                        Updates.set("chargeNote", updateObj.getString("chargeNote")),
                        Updates.set("paidTime", KeysUtil.SDF_DMYHMSTZ.parse(updateObj.getString("paidTime"))),
                        Updates.currentDate("latestUpdate"));
                UpdateResult result = collection.updateOne(filter, update);
                return result.wasAcknowledged();
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - updateInvoice exception: " + CommonUtils.getError(ex));
        }
        return false;
    }

    public boolean updatePaymentLinkToBooking(String bookId, boolean hasPendingPaymentLink){
        MongoCollection<Booking> collection = database.getCollection("BookingCompleted", Booking.class);
        Bson filter = Filters.eq("bookId", bookId);
        Bson update = Updates.set("hasPendingPaymentLink", hasPendingPaymentLink);
        UpdateResult result = collection.updateOne(filter, update);
        return result.wasAcknowledged();
    }

    public Fleet findByFleetToken(String fleetToken){
        MongoCollection<Fleet> collection = database.getCollection("Fleet", Fleet.class);
        Bson filter = Filters.eq("fleetToken", fleetToken);
        return collection.find(filter).first();
    }
}
