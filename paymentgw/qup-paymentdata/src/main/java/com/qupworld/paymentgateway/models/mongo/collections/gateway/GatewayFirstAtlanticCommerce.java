
package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import org.bson.types.ObjectId;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GatewayFirstAtlanticCommerce {

    public ObjectId _id;
    public String _class;
    public String fleetId;
    public Boolean isSystem;
    public String environment;
    public String merchantId;
    public String acquirerId;
    public String password;
    public Boolean isActive;
    public boolean powerTranz;
    public String powerTranzId;
    public String powerTranzPassword;

}
