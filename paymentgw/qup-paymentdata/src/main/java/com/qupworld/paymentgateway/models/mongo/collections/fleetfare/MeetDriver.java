
package com.qupworld.paymentgateway.models.mongo.collections.fleetfare;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import javax.annotation.Generated;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class MeetDriver {

    public int payTo; // 0 - Driver | 1 - Fleet
    public List<AmountByCurrency> onCurbByCurrencies;
    public List<AmountByCurrency> meetDrvByCurrencies;
}
