
package com.qupworld.paymentgateway.entities;

import javax.annotation.Generated;
import java.sql.Timestamp;

@Generated("org.jsonschema2pojo")
public class PrepaidTransaction {

    public Long id;
    public String prepaidId;
    public String topUpId;
    public String transactionId;
    public String name;
    public String company;
    public String paidBy;
    public String cardMask;
    public double amount;
    public String type;
    public String bookId;
    public Timestamp createdDate;
    public String fleetId;
    public String companyId;
    public String currencyISO;
    public String reason;
    public String operatorId;
    public String operatorName;
    public String operatorUserName;
    public double newAmount;
}
