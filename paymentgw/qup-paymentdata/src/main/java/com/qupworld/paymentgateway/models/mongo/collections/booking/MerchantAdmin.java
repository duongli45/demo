package com.qupworld.paymentgateway.models.mongo.collections.booking;

/**
 * Created by myasus on 5/31/21.
 */
public class MerchantAdmin {
    public String userId;
    public String phone;
    public String email;
    public String firstName;
    public String lastName;
    public String fullName;
}
