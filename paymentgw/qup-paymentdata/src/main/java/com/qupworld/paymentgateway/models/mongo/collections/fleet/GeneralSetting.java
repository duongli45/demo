package com.qupworld.paymentgateway.models.mongo.collections.fleet;

import javax.annotation.Generated;

/**
 * Created by hoang.nguyen on 12/2/16.
 */
@Generated("org.jsonschema2pojo")
public class GeneralSetting {
    public boolean switchMapProvider;
    public boolean soundNotify;
    public Boolean advanceInfoCorp;
    public boolean dynamicSurcharge;
    public boolean dynamicFare;
    public boolean allowCongfigSettingForEachZone;
    public boolean hideFareOnPaxAppForAllCorporateBookings;
    public FounderFund founderFund;
    public boolean editTotalCue;
    public boolean differentFleetCommission;
    public boolean driverInsurance;
    public boolean differentSurchargeForDifferentService;
    public boolean cancellationCashBooking;
    public boolean companyCommission;
}
