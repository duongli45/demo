
package com.qupworld.paymentgateway.entities;

import javax.annotation.Generated;
import java.sql.Timestamp;

@Generated("org.jsonschema2pojo")
public class DriverCreditBalance {

    public Long id;
    public String fleetId;
    public String fleetName;
    public String driverId;
    public String driverName;
    public String driverPhone;
    public String driverUserName;
    public String companyId;
    public String companyName;
    public String type;
    public Double amount;
    public Double currentAmount;
    public Double newAmount;
    public String reason;
    public String bookId;
    public String operatorId;
    public String operatorName;
    public String currencyISO;
    public String currencySymbol;
    public String cardMasked;
    public Timestamp createdDate;
    public String chargedType;
    public Double chargedAmount;
    public Double differentPercent;
    public Double bookingFare;
    public Double receivedAmount;
    public String channelLog;
    public String customerName;
    public String customerPhone;
    public String walletName;
    public String transactionId;
    public String receiverPhone;
    public String receiverName;
    public String receiverId;
    public String senderPhone;
    public String senderName;
    public String senderId;
}
