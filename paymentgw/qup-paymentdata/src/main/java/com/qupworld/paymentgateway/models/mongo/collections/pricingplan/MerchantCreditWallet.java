
package com.qupworld.paymentgateway.models.mongo.collections.pricingplan;


import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import javax.annotation.Generated;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class MerchantCreditWallet {

    public boolean enable;
    public List<AmountByCurrency> valueByCurrencies;
}
