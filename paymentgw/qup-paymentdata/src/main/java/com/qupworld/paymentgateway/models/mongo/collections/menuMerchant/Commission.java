package com.qupworld.paymentgateway.models.mongo.collections.menuMerchant;

/**
 * Created by myasus on 6/1/21.
 */
public class Commission {
    public String iso;
    public String symbol;
    public double value;
}
