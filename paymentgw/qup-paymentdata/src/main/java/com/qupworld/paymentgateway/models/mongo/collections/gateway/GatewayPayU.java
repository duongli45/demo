
package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GatewayPayU {

    public String fleetId;
    public String paymentCountry;
    public String environment;
    public String apiKey;
    public String apiLogin;
    public String merchantId;
    public String accountId;
    public Boolean isActive;

}
