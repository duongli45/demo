package com.qupworld.paymentgateway.models.mongo.collections.account;

public class Address {
    public String street;
    public String city;
    public String state;
    public String zipcode;
    public String country;
}