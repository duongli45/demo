
package com.qupworld.paymentgateway.models.mongo.collections.pricingplan;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Subscription {

    public Double minimumFee;
    public String type;
    public Integer freeDriver;
    public Double costDriver;
    public Integer freeTransaction;
    public Double costTransaction;
    public Double percentageFare;
    public Double tax;

}
