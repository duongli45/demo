package com.qupworld.paymentgateway.models.mongo.collections.wallet;

import javax.annotation.Generated;

/**
 * Created by thuanho on 22/02/2021.
 */
@Generated("org.jsonschema2pojo")
public class ConfigWallet {

    public String fleetId;
    public String description;
    public String gateway;
    public String environment;
    public boolean drvApp;
    public boolean paxApp;
    public ChargeExtra chargeExtra;
    public boolean isActive;

}
