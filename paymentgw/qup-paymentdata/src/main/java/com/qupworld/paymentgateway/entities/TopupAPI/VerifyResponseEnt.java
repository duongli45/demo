package com.qupworld.paymentgateway.entities.TopupAPI;

import com.google.gson.Gson;

/**
 * Created by thuanho on 13/01/2022.
 */
public class VerifyResponseEnt {

    public int returnCode;
    public String message;
    public VerifyResponse response;

    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
