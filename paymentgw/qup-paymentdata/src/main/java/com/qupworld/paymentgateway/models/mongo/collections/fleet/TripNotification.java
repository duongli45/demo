package com.qupworld.paymentgateway.models.mongo.collections.fleet;

/**
 * Created by myasus on 5/12/20.
 */
public class TripNotification {
    public boolean enable;
    public double spendingOver;
    public int trips;
}
