package com.qupworld.paymentgateway.models.mongo.collections;

/**
 * Created by thuanho on 16/02/2022.
 */
public class CommissionServices {

    public String serviceType; //  ['transport', 'streetHailing', 'streetSharing', 'shuttle', 'intercity', 'parcel', 'food', 'mart']
    public double value;
    public String type; // percent/amount
    public double insurance;
}
