package com.qupworld.paymentgateway.models.mongo.collections.affiliateRoute;

/**
 * Created by hoangnguyen on 7/26/17.
 */
public class Limitation {
    public Boolean limited;
    public Double coveredDistance;
    public Double extraDistance;
    public Double coveredTime;
    public Double extraTime;
    public String unitDistance;
}
