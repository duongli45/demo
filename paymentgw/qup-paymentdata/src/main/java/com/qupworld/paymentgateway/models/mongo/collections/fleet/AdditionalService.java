
package com.qupworld.paymentgateway.models.mongo.collections.fleet;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class AdditionalService {

    public FromAirport fromAirport;
    public ToAirportNew toAirportNew;
    public Boolean toAirport;
    public Boolean hourly;
    public Boolean isActive;

}
