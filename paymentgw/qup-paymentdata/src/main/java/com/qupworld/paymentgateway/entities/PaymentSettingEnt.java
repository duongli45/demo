package com.qupworld.paymentgateway.entities;

/**
 * Created by hoangnguyen on 10/16/17.
 */
public class PaymentSettingEnt {
    public String fleetId;
    public String bookId;
    public String currencyISO;
    public String currencySymbol;
    public String bookFrom;
    public String token;
    public String couponType;
    public String commissionType;
    public String promoCode;
    public String techFeeType;
    public String partnerId;
    public boolean taxActive;
    public boolean tipActive;
    public boolean heavyTrafficActive;
    public boolean otherFeeActive;
    public boolean airportActive;
    public boolean rushHourActive;
    public boolean meetDriverActive;
    public boolean techFeeActive;
    public boolean editBasicFare;
    public boolean editTax;
    public boolean editTip;
    public boolean editOtherFees;
    public double taxValue;
    public double rushHour;
    public double commissionValue;
    public double meetDriverFee;
    public double heavyTraffic;
    public double airportSurcharge;
    public double techFeeValue;
    public double couponValue;
    public double maximumValue;
    public double minimum;
    public double otherFees;
    public boolean actualFare;
    public int pricingType;
    public int paymentType;
    public boolean tollFeeActive;
    public boolean parkingFeeActive;
    public boolean gasFeeActive;
    public double tollFee;
    public double parkingFee;
    public double gasFee;
    public Double surchargeFee;
    public String surchargeType;
}

