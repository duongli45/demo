package com.qupworld.paymentgateway.models.mongo.collections.booking;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hoangnguyen on 3/6/18.
 */
public class ExtraDestination {
    public String from;
    public String address;
    public String timezone;
    public String offset;
    public String zipCode;
    public List<Double> geo = new ArrayList<>();
    public AddressDetails addressDetails;
}
