package com.qupworld.paymentgateway.models.mongo.collections.corporate;

/**
 * Created by hoang.nguyen on 2/16/17.
 */
public class Pricing {
    public Boolean defaultPricing;
    public Boolean discountByPercentage;
    public Boolean differentRate;
    public Double value;
    public boolean markUpByPercentage;
    public double markUpValue;
}
