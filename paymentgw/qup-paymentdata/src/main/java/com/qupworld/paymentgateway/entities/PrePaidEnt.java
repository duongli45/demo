package com.qupworld.paymentgateway.entities;

import java.util.UUID;

public class PrePaidEnt {

    public String fleetId;
    public String bookId;
    public String chargeNote;
    public String paymentMethod;
    public String token;
    public double amount;
    public String operatorId;
    public String cardType;
    public String cardOwner;
    public String paymentLinkId;
    public String stripeMethod;

    // used to update transaction from webhook
    public long id;

    public String type; // prePaid, postPaid

    // for log
    public String requestId = "";

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }
}
