package com.qupworld.paymentgateway.entities.gateway.PayWay;

/**
 * Created by thuanho on 14/06/2023.
 */
public class PayWayCardStatus {
    public int status;
    public String pwt;
    public String mask_pan;
    public String card_type;
}
