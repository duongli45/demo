
package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GatewayECPay {

    public String fleetId;
    public String environment;
    public String merchantId;
    public String hashKey;
    public String hashIV;
    public String creditCheckCode;
}
