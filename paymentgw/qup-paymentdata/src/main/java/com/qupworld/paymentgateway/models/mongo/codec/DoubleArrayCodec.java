package com.qupworld.paymentgateway.models.mongo.codec;

import org.bson.BsonReader;
import org.bson.BsonType;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

import java.util.ArrayList;
import java.util.List;

public class DoubleArrayCodec implements Codec<Double[]> {

    @Override
    public void encode(BsonWriter writer, Double[] value, EncoderContext encoderContext) {
        writer.writeStartArray();
        for (Double d : value) {
            writer.writeDouble(d);
        }
        writer.writeEndArray();
    }

    @Override
    public Double[] decode(BsonReader reader, DecoderContext decoderContext) {
        reader.readStartArray();
        List<Double> list = new ArrayList<>();
        while (reader.readBsonType() != BsonType.END_OF_DOCUMENT) {
            list.add(reader.readDouble());
        }
        reader.readEndArray();

        return list.toArray(new Double[0]);
    }

    @Override
    public Class<Double[]> getEncoderClass() {
        return Double[].class;
    }
}