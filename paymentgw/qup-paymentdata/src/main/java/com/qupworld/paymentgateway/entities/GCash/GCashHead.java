package com.qupworld.paymentgateway.entities.GCash;

/**
 * Created by qup on 06/08/2019.
 */
public class GCashHead {

    public String function = "";
    public String clientId = "";
    public String version = "";
    public String reqTime = "";
    public String reqMsgId = "";
}
