package com.qupworld.paymentgateway.models.mongo.collections.bookingStatistic;

/**
 * Created by qup on 06/09/2018.
 */
public class Statistic {

    public Integer completed;
    public Integer noShow;
    public Integer canceled;
    public Integer incident;
    public Integer finished;
}
