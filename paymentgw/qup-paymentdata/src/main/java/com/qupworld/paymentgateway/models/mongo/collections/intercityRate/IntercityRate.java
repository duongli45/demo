package com.qupworld.paymentgateway.models.mongo.collections.intercityRate;

import org.bson.types.ObjectId;

/**
 * Created by myasus on 4/14/20.
 */
public class IntercityRate {
    public ObjectId _id;
    public String name;
    public String fleetId;
    public Boolean isDefault;
    public Boolean isActive;
}
