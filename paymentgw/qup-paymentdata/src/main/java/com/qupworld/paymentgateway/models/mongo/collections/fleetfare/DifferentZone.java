package com.qupworld.paymentgateway.models.mongo.collections.fleetfare;

import com.qupworld.paymentgateway.models.mongo.collections.CommissionServices;

import java.util.List;

/**
 * Created by thuanho on 16/02/2022.
 */
public class DifferentZone {

    public String zoneId;
    public List<CommissionServices> value;
}
