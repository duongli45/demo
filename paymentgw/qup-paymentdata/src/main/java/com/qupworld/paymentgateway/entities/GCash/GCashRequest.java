package com.qupworld.paymentgateway.entities.GCash;

/**
 * Created by qup on 07/08/2019.
 */
public class GCashRequest {

    public GCashHead head;
    public GCashBody body;
}
