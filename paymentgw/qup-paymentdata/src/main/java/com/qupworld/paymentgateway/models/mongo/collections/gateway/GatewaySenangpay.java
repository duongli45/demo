
package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GatewaySenangpay {

    public String _class;
    public String fleetId;
    public Boolean isSystem;
    public String environment;
    public String merchantAccount;
    public String wsUser;
    public String wsPassword;
    public String secretKey;
    public Boolean isActive;

}
