
package com.qupworld.paymentgateway.models.mongo.collections.corporate;


import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import javax.annotation.Generated;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class Commission {

    public Boolean active;
    public String type;
    //public Double value;
    public List<AmountByCurrency> commissionByCurrencies;
}
