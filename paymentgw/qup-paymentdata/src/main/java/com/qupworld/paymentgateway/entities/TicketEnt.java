
package com.qupworld.paymentgateway.entities;

import com.qupworld.paymentgateway.models.mongo.collections.FleetService;
import org.json.simple.JSONObject;

import javax.annotation.Generated;
import java.sql.Timestamp;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class TicketEnt {

    public Long id = new Long(0);
    //// PAYMENT DATA
    public String bookId;
    /**
     * pay by cash:         'paymentType': 0
     * pay by credit token: 'paymentType': 1
     * pay by input credit: 'paymentType': 2
     * pay by fleet card:   'paymentType': 3
     * pay by swipe card:   'paymentType': 4
     * pay by mDis card:    'paymentType': 1 (in mongoDB: 5)
     * pay by CorporateCard:'paymentType': 1 (in mongoDB: 7)
     * pay by DirectBilling:'paymentType': 6
     * pay by B2BTerminal:  'paymentType': 3
     */
    public int paymentType = 0;
    public double total = 0.0;
    public double subTotal = 0.0;
    public double techFee = 0.0;
    public double fare = 0.0;
    public double oldBasicFare = 0.0;
    public double calBasicFare = 0.0;
    public double tip = 0.0;
    public double tax = 0.0;
    public String promoCode = "";
    public double promoAmount = 0.0;
    public double meetDriverFee = 0.0;
    public double heavyTraffic = 0.0;
    public double airportSurcharge = 0.0;
    public double partnerCommission = 0.0;
    public double otherFees = 0.0;
    public double rushHour = 0.0;
    public int isMinimum = 0;
    public String canceller = "";
    public String cancellerName = "";
    public int status = 0;
    public String transactionStatus = "";
    public String paidBy = "";
    public String approvalCode = ""; // returned from gateway
    public String cardType = "";
    public double tollFee = 0.0;
    public double parkingFee = 0.0;
    public double gasFee = 0.0;
    public double serviceFee = 0.0;
    public double dynamicSurcharge = 0.0;
    public double surchargeParameter = 0.0;
    public double dynamicFare = 0.0;
    public double creditTransactionFee = 0.0;
    public double authAmount = 0.0;
    public String otherFeesDetails = "";
    public double unroundedTotalAmt = 0.0;
    public double receivedAmount = 0.0;
    public double transferredAmount = 0.0;
    public double remainingAmount = 0.0;
    public double itemValue = 0.0;
    public double etaFare = 0.0;
    public double merchantCommission = 0.0;

    //// HYDRA
    public double fareProvider = 0.0;
    public double tipProvider = 0.0;
    public double taxValueProvider = 0.0;
    public double subTotalProvider= 0.0;
    public double totalProvider = 0.0;
    public double qupCommission = 0.0;
    public double minimumProvider = 0.0;
    public double ridePayout = 0.0;



    //// TICKET DATA
    public String fleetId = "";
    public String psgFleetId = "";
    public String bookFrom = "";
    public String platform = "";
    public String plateNumber = "";
    public String userName = ""; // operator name
    public String agentId = "";  // operator agentId
    public int dispatch = 0;
    public String queueId = "";
    public String queueName = "";
    public String dispatcherId = "";
    public String mDispatcherId = "";
    public String mDispatcherName = "";
    public String mDispatcherTypeId = "";
    public String mDispatcherTypeName = "";
    public String customerId = "";
    public String customerName = "";
    public String customerPhone = "";
    public String psgEmail = "";
    public int rank = 0;
    public String driverId = "";
    public String driverName = "";
    public String driverNumber = "";
    public String driverNumberType = "";
    public String driverLicenseNumber = "";
    public String phone = "";
    public String vehicleType = "";
    public String vehicleId = "";
    public String driverVehicleInfo = "";
    public String vhcId = "";
    public String permission = "";
    public String pickup = "";
    public String destination = "";
    public int pricingType = 0;
    public String currencySymbol = "";
    public String currencyISO = "";
    public double distanceTour = 0.0;
    public Timestamp pickupTime;
    public Timestamp createdTime;
    public Timestamp completedTime;
    public Timestamp droppedOffTime;
    public Timestamp engagedTime;
    public Timestamp expectedPickupTime;
    public String timeZonePickup = "";
    public double pickupLon = 0.0;
    public double pickupLat = 0.0;
    public String timeZoneDestination = "";
    public double destinationLon = 0.0;
    public double destinationLat = 0.0;
    public String typeBooking = "";  // asap || reservation || intercity
    public String corporateId = "";
    public String corporateName = "";
    public String clientCaseMatter = "";
    public String chargeCode = "";
    public String receiptComment = "";
    public String companyId = "";
    public String companyName = "";
    public String locationFrom = "";
    public String token = "";
    public String currencySymbolCharged = "";
    public String currencyISOCharged = "";
    public double totalCharged = 0.0;
    public boolean rideSharing = false;
    public boolean intercity = false;
    public String tripId = "";
    public String intercityInfo = "";
    public double originalFare = 0.0;
    public boolean delivery = false;
    public List<FleetService> fleetServices;
    public boolean isMinimumTotal;
    public double customerDebt = 0.0;

    //// SETTING DATA
    public boolean editBasicFare = false;
    public boolean editOtherFees = false;
    public boolean editTax = false;
    public boolean editTip = false;
    public boolean techFeeActive = false;
    public String techFeeType = "";
    public double techFeeValue = 0.0;
    public boolean tipActive = false;
    public boolean taxActive = false;
    public double taxValue = 0.0;
    public String couponType = "";
    public double couponValue = 0.0;
    public boolean meetDriverActive = false;
    public boolean heavyTrafficActive = false;
    public boolean airportActive = false;
    public String commissionType = "";
    public double commissionValue = 0.0;
    public boolean otherFeeActive = false;
    public boolean rushHourActive = false;
    public int bookingFeeActive = 0;

    //// ESTIMATE DATA
    public boolean actualFare = false;
    public double bookingFee = 0.0;
    public double taxFee = 0.0;
    public double tipFee = 0.0;
    public double minimum = 0.0;

    //// UPDATE TICKET AFTER PAYMENT
    public String reason;
    public int reasonCode;
    public double mDispatcherCommission;
    public double netEarning;
    public double totalFare;
    public double comm;
    public double deductions;
    public double ridePayment;
    public double grossEarning;
    public double transactionFee;
    public double subTotalCharged;
    public double exchangeRate;
    public double subTotalExchange;
    public double totalExchange;
    public String services = "";
    public String extraDestination = "";
    public JSONObject additionalFare;
    public String walletName = "";
    public boolean splitPayment;
    public double paidByWallet;
    public double paidByOtherMethod;
    public double cashReceived;
    public double transferredChangeByWallet;
    public double returnedChangeByCash;
    public String otherMethod;
    public double qupPreferredAmount;
    public double qupBuyPrice;
    public double fleetMarkup;
    public String hydraPaymentMethod;
    public boolean isFarmOut;
    public double sellPriceMarkup;
}