package com.qupworld.paymentgateway.models.mongo.collections.booking;

/**
 * Created by myasus on 9/6/21.
 */
public class WeekDays {
    public boolean sun;
    public boolean mon;
    public boolean tue;
    public boolean wed;
    public boolean thu;
    public boolean fri;
    public boolean sat;
}
