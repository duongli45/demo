
package com.qupworld.paymentgateway.models.mongo.collections.profile;

import org.bson.types.ObjectId;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Profile {

    public ObjectId _id;
    public String userId;
    public String customerNumber;
    public String localToken;
    public String password;
    public String profileNo;

}
