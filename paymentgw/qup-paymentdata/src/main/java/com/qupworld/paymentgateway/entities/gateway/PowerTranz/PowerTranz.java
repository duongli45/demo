package com.qupworld.paymentgateway.entities.gateway.PowerTranz;

import java.util.List;

/**
 * Created by thuanho on 10/05/2023.
 */
public class PowerTranz {

    public String TransactionType;
    public String Approved;
    public String TransactionIdentifier;
    public String AuthorizationCode;
    public double TotalAmount;
    public String CurrencyCode;
    public String RRN;
    public String CardBrand;
    public String IsoResponseCode;
    public String ResponseMessage;
    public String PanToken;
    public String OrderIdentifier;
    public String SpiToken;
    public String CardSuffix;
    public PowerTranzRiskManagement RiskManagement;
    public List<PowerTranzError> Errors;
}




