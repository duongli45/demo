package com.qupworld.paymentgateway.entities;

import java.sql.Timestamp;

/**
 * Created by thuanho on 06/05/2022.
 */
public class MerchantWalletBalance {

    public String fleetId;
    public String merchantId;
    public String walletType;
    public String transactionType;
    public String transactionId;
    public double cashIn;
    public double cashOut;
    public double newBalance;
    public String currencyISO;
    public Timestamp createdDate;
}