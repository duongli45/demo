package com.qupworld.paymentgateway.models.mongo.collections.additionalServices;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;
import org.bson.types.ObjectId;

import java.util.List;

/**
 * Created by hoangnguyen on 12/12/17.
 */
public class AdditionalServices {
    public ObjectId _id;
    public String fleetId;
    public String serviceName;
    public String serviceType; //Optional / Compulsory
    public List<AmountByCurrency> serviceFeeByCurrencies;
    public List<String> vehicleType; //vehicle type id
    public boolean isActive;
}
