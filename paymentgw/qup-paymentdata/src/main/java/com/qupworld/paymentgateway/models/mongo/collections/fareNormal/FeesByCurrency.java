package com.qupworld.paymentgateway.models.mongo.collections.fareNormal;

/**
 * Created by hoangnguyen on 5/17/17.
 */
public class FeesByCurrency {
    public String currencyISO;
    public Double startingNow;
    public Double startingReservation;
    public Double feeFirstDistance;
    public Double feeSecondDistance;
    public Double feeAfterSecondDistance;
    public Double feePerMinute;
    public Double minNow;
    public Double minReservation;
}
