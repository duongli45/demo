package com.qupworld.paymentgateway.entities.gateway.PayWay;

/**
 * Created by thuanho on 14/06/2023.
 */
public class PayWayReturnParam {
    public String tran_id;
    public String ctid;
    public String payment_status;
    public String return_param;
    public PayWayCardStatus card_status;
}
