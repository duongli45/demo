package com.qupworld.paymentgateway.models.mongo.collections.affiliatePackages;

import com.qupworld.paymentgateway.models.mongo.collections.affiliateRoute.OtherFees;
import org.bson.types.ObjectId;

/**
 * Created by hoangnguyen on 7/26/17.
 */
public class AffiliatePackages {
    public ObjectId _id;
    public String name;
    public String fareHourlyId;
    public int duration;
    public String type;
    public String unitDistance;
    public Double distanceCovered;
    public Boolean isActive;
    public Price buyPrice;
    public Price sellPrice;
    public OtherFees otherFees;
}
