package com.qupworld.paymentgateway.entities;

import java.sql.Timestamp;

/**
 * Created by thuanho on 07/11/2022.
 */
public class HydraPayout {

    public String payoutId = "";
    public Timestamp dateTime;
    public String payoutType = ""; // values = auto, manual
    public String accountHolderName = "";
    public String bankName = "";
    public String accountNumber = "";
    public double totalPayout = 0.0;
    public String notes = "";
    public String fleetId = "";
    public String bookId = "";
    public String transactionId = "";
    public String operatorName = "";
    public String fleetName = "";
    public String transactionType = "";
    public String paymentMethod = "";
    public String settlement = "";
    public String networkType = "";
}
