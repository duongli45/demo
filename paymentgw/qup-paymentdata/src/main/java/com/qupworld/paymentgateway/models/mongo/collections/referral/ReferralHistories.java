package com.qupworld.paymentgateway.models.mongo.collections.referral;

import org.bson.types.ObjectId;

import java.util.Date;

/**
 * Created by qup on 18/10/2018.
 */
public class ReferralHistories {

    public ObjectId _id;
    public String fleetId;
    public String refereeId;
    public String refereeName;
    public String refereePhone;
    public String userId;
    public String phone;
    public String companyId;
    public String companyName;
    public String fullName;
    public String referralCode;
    public String referralType;
    public Date createdDate;
    public Date firstBookingDate;
    public Date firstBookingDateGMT;
    public String firstBooking;
    public String firstBookingCurrency;
    public double firstEarning;
}
