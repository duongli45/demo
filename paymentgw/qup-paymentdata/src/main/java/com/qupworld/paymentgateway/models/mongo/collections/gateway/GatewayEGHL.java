package com.qupworld.paymentgateway.models.mongo.collections.gateway;

import javax.annotation.Generated;

/**
 * Created by qup on 06/05/2020.
 */
@Generated("org.jsonschema2pojo")
public class GatewayEGHL {

    public String fleetId;
    public String environment;
    public String serviceId;
    public String password;
    public Boolean isActive;
}
