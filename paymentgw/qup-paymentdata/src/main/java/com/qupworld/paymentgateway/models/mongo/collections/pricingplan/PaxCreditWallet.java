package com.qupworld.paymentgateway.models.mongo.collections.pricingplan;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import javax.annotation.Generated;
import java.util.List;

/**
 * Created by myasus on 7/17/19.
 */
@Generated("org.jsonschema2pojo")
public class PaxCreditWallet {
    public boolean enable;
    public boolean viaCredit;
    public boolean viaWallet;
    public List<AmountByCurrency> minimumByCurrencies;
    public List<AmountByCurrency> maximumByCurrencies;
}
