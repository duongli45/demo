
package com.qupworld.paymentgateway.models.mongo.collections.serverInformation;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class SubType {

    public String gateway;
    public Boolean isActive;
    public Boolean defaultGateway;

}
