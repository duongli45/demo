package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.List;
import java.util.UUID;

/**
 * Created by myasus on 12/6/21.
 */
public class ETAFareMeterEnt implements PaymentIdentifiable{
    public String bookId;
    public double distance;
    public double duration;
    public List<Double> destination;
    public String requestId;

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    public int validateData() throws JsonProcessingException {
        if(this.isEmptyString(this.bookId)){
            return 406;
        }
        return 200;
    }

    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }

    @Override
    public String getRequestId() {
        return requestId;
    }
}
