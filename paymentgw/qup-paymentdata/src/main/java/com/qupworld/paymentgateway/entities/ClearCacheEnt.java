
package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.UUID;

public class ClearCacheEnt implements PaymentIdentifiable{

    public String fleetId;
    public String bookId;
    public String customerId;

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    /**
     * Valid a Credit Card Datagson.to
     */
    public int validateData() throws JsonProcessingException {
        if ((this.isEmptyString(this.bookId) && this.isEmptyString(this.customerId)) || this.isEmptyString(this.fleetId)) {
            return 406;
        }
        return 200;
    }
    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }

    @Override
    public String getRequestId() {
        return requestId;
    }
}
