
package com.qupworld.paymentgateway.models.mongo.collections.tripNotification;

import org.bson.types.ObjectId;

import javax.annotation.Generated;
import java.util.Date;

@Generated("org.jsonschema2pojo")
public class TripNotification {

    public ObjectId _id;
    public String fleetId;
    public String fleetName;
    public String customerId;
    public String customerName;
    public String customerPhone;
    public String customerEmail;
    public Integer totalTrip;
    public Double totalFare;
    public Date date;
    public Boolean isActive;
}
