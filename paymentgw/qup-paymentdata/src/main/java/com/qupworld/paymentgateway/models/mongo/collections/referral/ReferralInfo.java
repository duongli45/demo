package com.qupworld.paymentgateway.models.mongo.collections.referral;

import java.util.Date;

/**
 * Created by qup on 18/10/2018.
 */
public class ReferralInfo {

    public String userId;
    public String fleetId;
    public String type;
    public String code;
    public Boolean edited;
    public Date createdDate;

}
