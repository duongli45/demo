package com.qupworld.paymentgateway.entities;

import java.sql.Timestamp;

/**
 * Created by myasus on 7/18/19.
 */
public class SettlementHistory {
    public long id;
    public String fleetId;
    public String driverId;
    public String driverName;
    public int totalTransaction;
    public double paidAmount;
    public String companyId;
    public Timestamp dateRangeFrom;
    public Timestamp dateRangeTo;
    public String currencyISO;
    public double totalDispatch;
    public Timestamp createdDate;
}
