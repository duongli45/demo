
package com.qupworld.paymentgateway.models.mongo.collections.fleet;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Kiosk {

    public String backgroundImage;
    public Boolean enable;
    public String mainColor;
    public String secondColor;
    public String txtColor;

}
