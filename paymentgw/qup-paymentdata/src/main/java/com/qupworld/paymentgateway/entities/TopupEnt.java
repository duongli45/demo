package com.qupworld.paymentgateway.entities;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;
import java.util.UUID;

/**
 * Created by qup on 10/7/16.
 */
public class TopupEnt {

    public String fleetId;
    public String type;
    public double amount;
    public String token;
    public String cardNumber;
    public String expiredDate;
    public String cardHolder;
    public String postalCode;
    public String cvv;
    public String userId;
    public String phone;
    public String email;
    public String currencyISO;
    public String gateway;
    public String parameter;
    public String channel; // receive payment channel from RazerPay
    public String operatorId;
    public String reason;

    // add info for AVS verification
    public String street;
    public String city;
    public String state;
    public String country;

    // add key for SCA
    public String bookId;

    // for log
    public String requestId = "";

    // for multi gateway
    public List<Double> geoLocation;
    public String zoneId;
    public Boolean sca;
    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().serializeNulls().create();
        String strCredit = gson.toJson(this);
        if (this.cardNumber != null && this.cardNumber.length() > 4)
            strCredit = strCredit.replace(this.cardNumber,"XXXXXX"+this.cardNumber.substring(this.cardNumber.length()-4));
        return strCredit;
    }

}
