
package com.qupworld.paymentgateway.models.mongo.collections.affiliateFareRegular;


import org.bson.types.ObjectId;

public class RateRegular {
    public ObjectId _id;
    public String type;
    public Boolean isActive;
    public PerMinute perMinute;
    public AfterSecondDistance afterSecondDistance;
    public NoShowPolicy noShowPolicy;
    public CancellationPolicy cancellationPolicy;
    public Minimum minimum;
    public SecondDistance secondDistance;
    public FirstDistance firstDistance;
    public Starting starting;
    public RateInfo rateInfo;

}
