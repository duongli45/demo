package com.qupworld.paymentgateway.models.mongo.collections;

import javax.annotation.Generated;

/**
 * Created by qup on 06/02/2018.
 */

@Generated("org.jsonschema2pojo")
public class LogThirdParties {

    public String action;
    public String fleetId;
    public String cardMasked;
    public String last4;
    public String bookId;
    public String requestId;
    public String requestResponse;
    public String gateway;
    public String request;
    public String response;
    public String time;
}
