package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.List;
import java.util.UUID;

/**
 * Created by qup on 10/7/16.
 */
public class TopupPaxWalletEnt {

    public String fleetId;
    public String type; // values: credit, wallet name
    public String from; // values: cc, app - default = app
    public String reason;
    public String data3ds;
    public double amount;
    public String token;
    public String userId;
    public String currencyISO;
    public String referralCode; // used to top up wallet of referee
    public String gateway;
    public String parameter;
    public String channel; // receive payment channel from RazerPay

    public String osType;
    public String appVersion;

    public boolean ignoreValidate = false;

    public String bookId; //used for caching data - support Stripe SCA
    public String zoneId;
    public List<Double> geo;
    // for log
    public String requestId = "";

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    public int validateData() throws JsonProcessingException {
        if (this.isEmptyString(this.fleetId) || this.isEmptyString(this.userId)
                || this.isEmptyString(this.currencyISO)) {
            return 406;
        }
        return 200;
    }

    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }

}
