
package com.qupworld.paymentgateway.entities;

import java.util.UUID;

public class StripeConnectEnt {

    public String fleetId;
    public String userId;
    public String companyId;
    public String returnUrl;
    public String type;
    public String token;

    // for log
    public String requestId = "";

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }
}
