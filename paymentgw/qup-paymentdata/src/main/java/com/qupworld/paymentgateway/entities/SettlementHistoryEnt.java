package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;

import javax.annotation.Generated;
import java.util.Date;
import java.util.UUID;

/**
 * Created by hoangnguyen on 3/20/19.
 */
@Generated("org.jsonschema2pojo")
public class SettlementHistoryEnt implements PaymentIdentifiable {
    public String fleetId;
    public String driverId;
    public String driverName;
    public int totalTransaction;
    public double paidAmount;
    public String companyId;
    public String dateRangeFrom;
    public String dateRangeTo;
    public String currencyISO;
    public double totalDispatch;
    public String createdDate;
    public String requestId;

    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    public int validateData() throws JsonProcessingException {
        if (this.isEmptyString(this.fleetId) || this.isEmptyString(this.driverId)
                || this.isEmptyString(this.currencyISO) || this.isEmptyString(this.companyId)) {
            return 406;
        }
        return 200;
    }
    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }

    @Override
    public String getRequestId() {
        return requestId;
    }
}
