
package com.qupworld.paymentgateway.entities.FawryResponseEnt;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BalanceAmt {

    @SerializedName("amt")
    @Expose
    private Double amt;
    @SerializedName("curCode")
    @Expose
    private String curCode;

    public Double getAmt() {
        return amt;
    }

    public void setAmt(Double amt) {
        this.amt = amt;
    }

    public String getCurCode() {
        return curCode;
    }

    public void setCurCode(String curCode) {
        this.curCode = curCode;
    }

}
