package com.qupworld.paymentgateway.models.mongo.collections.fleet;

import com.qupworld.paymentgateway.models.mongo.collections.AmountByCurrency;

import java.util.List;

/**
 * Created by myasus on 5/28/19.
 */
public class WaiveOffCommission {
    public boolean enable;
    public List<AmountByCurrency> amountByCurrencies;
    public boolean applySurcharge;
    public String applyZone;
    public List<WaiveOffByZone> waiveOffByZones;
    public boolean applyTimeRange;
    public List<TimeRange> timeRanges;
}
