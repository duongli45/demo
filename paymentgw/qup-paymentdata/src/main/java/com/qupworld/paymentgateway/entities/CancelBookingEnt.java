
package com.qupworld.paymentgateway.entities;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.UUID;

public class CancelBookingEnt implements PaymentIdentifiable{

    public String fleetId;
    public String bookId;
    public Boolean charge;
    public Double penalityAmount;
    public String from;
    public Boolean refund;
    public String driverId;
    public String finishedBookingStatus = "canceled";

    // for log
    public String requestId = "";


    public void setRequestId(String requestId) {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        this.requestId = requestId;
    }

    /**
     * Valid a request data
     */
    public int validateDataFleet() throws JsonProcessingException {
        if (this.isEmptyString(this.bookId) || this.isEmptyString(this.fleetId)) {
            return 406;
        }
        return 200;
    }

    public int validateData() throws JsonProcessingException {
        if (this.isEmptyString(this.bookId)) {
            return 406;
        }
        return 200;
    }
    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }

    @Override
    public String getRequestId() {
        return requestId;
    }
}
