package com.qupworld.paymentgateway.models.mongo.collections.booking;

import java.util.List;

/**
 * Created by myasus on 12/13/21.
 */
public class StreetSharing {
    public double distance;
    public double duration;
    public double fare;
    public List<Waypoint> waypoints;
    public List<SharedPassenger> passengers;
}
