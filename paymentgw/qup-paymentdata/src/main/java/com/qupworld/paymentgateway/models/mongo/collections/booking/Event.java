package com.qupworld.paymentgateway.models.mongo.collections.booking;

import org.bson.types.ObjectId;

/**
 * Created by hoangnguyen on 8/10/18.
 */
public class Event {
    public ObjectId _id;
    public String name;
}
