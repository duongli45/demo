package com.qupworld.paymentgateway.models.mongo.collections.corpItinerary;

import com.qupworld.paymentgateway.models.mongo.collections.Credit;

/**
 * Created by qup on 29/10/2019.
 */
public class CorpItinerary {

    public String itineraryId;
    public Credit corpCreditCard;
    public DepartmentInfo departmentInfo;
    public PsgInfo psgInfo;

}
