package com.qupworld.util;

import com.pg.util.ObjResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.benmanes.caffeine.cache.Cache;
import com.qupworld.config.ServerConfig;
import com.qupworld.paymentgateway.entities.PaymentSettingMQ;
import com.qupworld.paymentgateway.entities.Process;
import com.qupworld.paymentgateway.models.MongoDao;
import com.qupworld.paymentgateway.models.MongoDaoImpl;
import com.qupworld.paymentgateway.models.SQLDaoImpl;


/**
 * Created by qup on 18/04/2019.
 */
public class PaymentSettingUtil {

    final static Logger logger = LogManager.getLogger(SQLDaoImpl.class);

    public PaymentSettingMQ getSettingMQ(String fleetId) {
        PaymentSettingMQ setting = new PaymentSettingMQ();
        setting.payBusiness = false;
        setting.payService = false;
        if(!ServerConfig.queue_mode){
            return setting;
        }
        try {
            if (isEmptyString(fleetId)){
                setting.payBusiness = false;
                setting.payService = true;
            }else {
                CacheUtil cacheUtil = new  CacheUtil();
                Cache<String, PaymentSettingMQ> cache= cacheUtil.getCachePaymentSettingMQ();
                PaymentSettingMQ settingCache = cache.getIfPresent(fleetId);
                if(settingCache == null){
                    MongoDao mongoDao = new MongoDaoImpl();
                    Process process = mongoDao.getSettingPayment(fleetId);
                    if(process != null && process.paymentServices != null){
                        setting.payBusiness = process.paymentServices.payBussiness;
                        setting.payService = process.paymentServices.payService;
                        logger.debug("fleetId: " + fleetId + " - payBusiness: " + setting.payBusiness + " - payService: " + setting.payService);
                        cache.put(fleetId, setting);
                    }else {
                        return setting;
                    }
                }else {
                    logger.debug("fleetId: " + fleetId + " - payBusiness: " + settingCache.payBusiness + " - payService: " + settingCache.payService);
                    return settingCache;
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return setting;
    }
    public ObjResponse updateCacheSettingMQ (String fleetId) {
        ObjResponse response = new ObjResponse();
        response.returnCode = 200;
        try {
            PaymentSettingMQ setting = new PaymentSettingMQ();
                CacheUtil cacheUtil = new  CacheUtil();
                Cache<String, PaymentSettingMQ> cache= cacheUtil.getCachePaymentSettingMQ();
                PaymentSettingMQ settingCache = cache.getIfPresent(fleetId);
                if(settingCache != null){
                    MongoDao mongoDao = new MongoDaoImpl();
                    Process process = mongoDao.getSettingPayment(fleetId);
                    if(process != null && process.paymentServices != null){
                        setting.payService = process.paymentServices.payService;
                        setting.payBusiness = process.paymentServices.payBussiness;
                        cache.put(fleetId, setting);
                    }

                }

        } catch (Exception ex) {
            response.returnCode = 201;
            ex.printStackTrace();
        }

        return response;
    }
    public boolean isEmptyString(String s){
        return (s == null || s.trim().isEmpty());
    }

}
