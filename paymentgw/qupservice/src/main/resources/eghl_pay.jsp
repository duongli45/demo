<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>JSP page</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            color: white;
            text-align: center;
        }
    </style>
</head>
<body>
<div class="std">
    <div class="row">
        <div class="col-md-offset-3 col-md-6">
            <form id='frmHtmlCheckout' name='frmHtmlCheckout' action="action_page" method="POST">
                <input type="hidden" name="TransactionType" value="TransactionType_value">
                <input type="hidden" name="PymtMethod" value="ANY">
                <input type="hidden" name="ServiceID" value="ServiceID_Value">
                <input type="hidden" name="PaymentID" value="PaymentID_Value">
                <input type="hidden" name="OrderNumber" value="OrderNumber_Value">
                <input type="hidden" name="PaymentDesc" value="PaymentDesc_Value">
                <input type="hidden" name="MerchantReturnURL" value="MerchantReturnURL_Value">
                <input type="hidden" name="MerchantCallbackURL" value="MerchantCallbackURL_Value">
                <input type="hidden" name="Amount" value="Amount_Value">
                <input type="hidden" name="CurrencyCode" value="CurrencyCode_Value">
                <input type="hidden" name="CustIP" value="CustIP_Value">
                <input type="hidden" name="CustName" value="CustName_Value">
                <input type="hidden" name="CustEmail" value="CustEmail_Value">
                <input type="hidden" name="CustPhone" value="CustPhone_Value">
                <input type="hidden" name="HashValue" value="HashValue_Value">
                <input type="hidden" name="LanguageCode" value="en">
                <input type="hidden" name="TokenType" value="OCP">
                <input type="hidden" name="Token" value="Token_Value">
                <input type="hidden" name="PageTimeout" value="PageTimeout_Value">
            </form>
            Transaction is processing...
        </div>
    </div>
</div>

<SCRIPT LANGUAGE='JavaScript'>
    Checkout();
    function Checkout() {
        document.frmHtmlCheckout.submit();
    }
</SCRIPT>
</body>
</html>