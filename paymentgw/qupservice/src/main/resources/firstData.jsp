<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title>JSP page</title>
      <!-- Latest compiled and minified CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <!-- jQuery library -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <!-- Latest compiled JavaScript -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <style>
         .footer {
         position: fixed;
         left: 0;
         bottom: 0;
         width: 100%;
         color: white;
         text-align: center;
         }
      </style>
   </head>
   <body>
        <div class="std">
            <div class="row">
                <div class="col-md-offset-3 col-md-6">
                    <form id='frmHtmlCheckout' name='frmHtmlCheckout' action="action_page/" method="POST">
                        <input type="hidden" name="txntype" value="preauth">
                        <input type="hidden" name="timezone" value="Europe/Berlin">
                        <input type="hidden" name="txndatetime" value="txndatetime_value">
                        <input type="hidden" name="hash_algorithm" value="SHA256">
                        <input type="hidden" name="hash" value="hash_value">
                        <input type="hidden" name="storename" value="store_value">
                        <input type="hidden" name="chargetotal" value="charge_value">
                        <input type="hidden" name="currency" value="currency_value">
                        <input type="hidden" name="checkoutoption" value="combinedpage">
                    </form>
                    Transaction is processing...
                </div>
            </div>
        </div>
        <SCRIPT LANGUAGE='JavaScript'>
            Checkout();
            function Checkout() {
                document.frmHtmlCheckout.submit();
            }
        </SCRIPT>
   </body>
</html>