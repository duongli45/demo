<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title>JSP page</title>
      <!-- Latest compiled and minified CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <!-- jQuery library -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <!-- Latest compiled JavaScript -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <style>
         .footer {
         position: fixed;
         left: 0;
         bottom: 0;
         width: 100%;
         color: white;
         text-align: center;
         }
      </style>
   </head>
   <body>
      <div class="std">
         <div class="row">
            <div class="col-md-offset-3 col-md-6">
               <form id='frmHtmlCheckout' name='frmHtmlCheckout' action="action_page/" method="POST">
                  <input type = "hidden" name = "vcode" value = "vcode_value" />
                  <input type = "hidden" name = "merchant_id" value = "merchant_id_value" />
                  <input type = "hidden" name = "bill_desc" value = "bill_desc_value" />
                  <input type = "hidden" name = "amount" value = "amount_value" />
                  <input type = "hidden" name = "bill_name" value = "bill_name_value" />
                  <input type = "hidden" name = "bill_email" value = "bill_email_value" />
                  <input type = "hidden" name = "bill_mobile" value = "bill_mobile_value" />
                  <input type = "hidden" name = "country" value = "MY" />
                  <input type = "hidden" name = "cur" value = "MYR" />
                  <input type = "hidden" name = "returnurl" value = "returnurl_value" />
                  <input type = "hidden" name = "callbackurl" value = "callbackurl_value" />
                  <input type = "hidden" name = "orderid" value = "orderid_value" />
               </form>
                Transaction is processing...
            </div>
         </div>
      </div>
      <div class="footer">
         <img src="img/simple-logo-800x50.png" class="img-rounded" alt="footer">
      </div>
      <SCRIPT LANGUAGE='JavaScript'>
          Checkout();
          function Checkout() {
              document.frmHtmlCheckout.submit();
          }
      </SCRIPT>
   </body>
</html>