package com.pg.paymentgateway;

/**
 * Created by qup on 7/5/16.
 * Presentation Layer
 */

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.pg.config.appConfig.provider.JWT.JwTokenHelper;
import com.pg.config.appConfig.provider.JWT.User;
import com.pg.config.appConfig.provider.JWT.UserService;
import com.pg.service.PayService;
import com.pg.service.PayServiceImpl;
import com.pg.service.PaymentRPC;
import com.qupworld.paymentgateway.entities.*;
import com.qupworld.paymentgateway.entities.FawryFormEnt.*;
import com.qupworld.paymentgateway.entities.TopupAPI.*;
import com.qupworld.paymentgateway.entities.TopupEnt;
import com.qupworld.paymentgateway.entities.Vipps.VippsNotifyEnt;
import com.qupworld.service.*;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.qupworld.util.PaymentSettingUtil;
import com.stripe.model.Event;
import io.swagger.annotations.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

//@Path("{a:qupservice/api/paymentgateway|api/paymentgateway|qupservice|status}")
@Path("/")
@Api(tags = {"Payment gateway"}, value = "api/paymentgateway", basePath = "api/paymentgateway")
public class PaymentGatewayProxyApi {
    private static final String DIS_ROLE = "dispatcher";
    private static final String CC_ROLE = "cc";
    private static final String API_ROLE = "api";
    private final static Logger logger = LogManager.getLogger(PaymentGatewayProxyApi.class);
    private Gson gson = new Gson();
    private PayService payService = new PayServiceImpl();
    private PayBusiness payBusiness = new PayBusinessImpl();
    private PaymentRPC paymentRPC = PaymentRPC.getInstance();
    private PaymentSettingUtil settingMQ = new PaymentSettingUtil();
    private ProxyUtil proxyUtil = new ProxyUtil();

    @Path("api/paymentgateway/status")
    @PermitAll
    @GET
    @ApiOperation(value="", hidden = true)
    public Response detect() throws Exception {

        InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream("my.properties");
        String version = "";
        if (input != null) {
            Properties prop = new Properties();
            prop.load(input);
            version = prop.getProperty("project-version") != null ? prop.getProperty("project-version"):"";
        }
        Map<String,String> map = new HashMap<>();
        map.put("version", version);
        map.put("business", payBusiness.getVersion());
        map.put("service", payService.getVersion());
        ObjResponse detectResponse = new ObjResponse();
        detectResponse.response = map;
        detectResponse.returnCode = 200;
        return Response.status(200).entity(gson.toJson(detectResponse)).build();
    }

    @Path("api/paymentgateway/getConfigAndConnectionStatus")
    @RolesAllowed(DIS_ROLE)
    @GET
    @ApiOperation(value="", hidden = true)
    public Response getConfigAndConnectionStatus() throws Exception {
        String requestId = UUID.randomUUID().toString();
        logger.debug(requestId + " - getConfigAndConnectionStatus ...");
        String responseStr = payBusiness.getConfigAndConnectionStatus(requestId);
        logger.debug(requestId + " - getConfigAndConnectionStatusResponse : " + responseStr);
        return Response.status(200).entity(responseStr).build();
    }


    @Path("api/paymentgateway/acceptBooking")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response acceptBooking(@HeaderParam("x-request-id") String requestId, HydraBookingEnt hydraBookingEnt) throws IOException {
        hydraBookingEnt.setRequestId(requestId);
        logger.debug(hydraBookingEnt.requestId + " - acceptBooking : " + gson.toJson(hydraBookingEnt));
        ObjResponse cancelResponse = new ObjResponse();

        int returnCode = hydraBookingEnt.validateData();
        if (returnCode != 200) {
            cancelResponse.returnCode = returnCode;
            logger.debug(hydraBookingEnt.requestId + " - acceptBookingResponse : " + cancelResponse.toString());
            return Response.status(200).header("x-request-id", hydraBookingEnt.requestId).entity(cancelResponse.toString()).build();
        }
        String responseStr = "";

        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(hydraBookingEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.acceptBooking(hydraBookingEnt);
        } else {
            responseStr = payBusiness.acceptBooking(hydraBookingEnt);
        }
        logger.debug(hydraBookingEnt.requestId + " - acceptBookingResponse : " + responseStr);
        return Response.status(200).header("x-request-id", hydraBookingEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/hashPmtNotifyRq")
    //@RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response hashPmtNotifyRq(@HeaderParam("x-api-key") String apikey ,@HeaderParam("x-request-id") String requestId, FawryEnt fawryPaForm) throws IOException {
        if (requestId == null || requestId.isEmpty()){
            requestId = UUID.randomUUID().toString();
        }
        logger.debug(requestId + " - hashPmtNotifyRq : " + gson.toJson(fawryPaForm));

        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(null);//Jupiter
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.hashPmtNotifyRq(fawryPaForm , requestId);
        } else {
            responseStr = payBusiness.hashPmtNotifyRq(fawryPaForm , requestId);
        }
        logger.debug(requestId + " - hashPmtNotifyRq : " + responseStr);
        return Response.status(200).header("x-request-id", requestId).entity(responseStr).build();


    }

    @Path("api/paymentgateway/topUpFawry")
    //@RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response notificationFawry(@HeaderParam("x-api-key") String apikey ,@HeaderParam("x-request-id") String requestId, FawryEnt fawryPaForm) throws IOException {
        if (requestId == null || requestId.isEmpty()){
            requestId = UUID.randomUUID().toString();
        }
        logger.debug(requestId + " - notificationFawry : " + gson.toJson(fawryPaForm));

        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(null);//Jupiter
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.notificationFawry(fawryPaForm , requestId);
        } else {
            responseStr = payBusiness.notificationFawry(fawryPaForm , requestId);
        }
        logger.debug(requestId + " - validateFawry : " + responseStr);
        return Response.status(200).entity(responseStr).build();

    }
    @Path("api/paymentgateway/createCreditToken")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response addCreditToken(@HeaderParam("x-request-id") String requestId,String json) throws IOException {
        JsonObject jobj = new Gson().fromJson(json, JsonObject.class);
        String tokenStr = "";
        try {
            JsonObject token = jobj.get("token").getAsJsonObject();
            tokenStr = token.toString();
        }catch (Exception ex){
        }
        if (tokenStr.length() > 0){
            jobj.remove("token");
        }
        CreditEnt creditForm = gson.fromJson(jobj.toString(), CreditEnt.class);
        creditForm.setRequestId(requestId);
        if (tokenStr.length()>0){
            creditForm.token = tokenStr;
        }


            logger.debug(creditForm.requestId + " - createCreditToken : " + creditForm.toString());
            ObjResponse addCardResponse = new ObjResponse();
            int returnCode = creditForm.validateData();
            if (returnCode != 200) {
                addCardResponse.returnCode = returnCode;
                logger.debug(creditForm.requestId + " - createCreditTokenResponse : " + addCardResponse.toString());
                return Response.status(200).header("x-request-id", creditForm.requestId).entity(addCardResponse.toString()).build();
            }

            String responseStr = "";
            PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(creditForm.fleetId);
            if (settingMQData.payBusiness) {
                try {
                    responseStr = paymentRPC.addCreditToken(creditForm);
                } catch (Exception ex) {
                    ex.printStackTrace();

                }

            } else {
                responseStr = payBusiness.addCreditToken(creditForm);
            }

            String log = responseStr;
            if (creditForm.cardNumber != null && !creditForm.cardNumber.isEmpty() && creditForm.cardNumber.length() > 4) {
                log = responseStr.replace(creditForm.cardNumber, "XXXXXX" + creditForm.cardNumber.substring(creditForm.cardNumber.length() - 4));
            }
            logger.debug(creditForm.requestId + " - createCreditTokenResponse : " + log);
            return Response.status(200).header("x-request-id", creditForm.requestId).entity(responseStr).build();




    }

    @Path("api/paymentgateway/createCreditTokenForCorporate")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response addCreditTokenForCorporate(@HeaderParam("x-request-id") String requestId, CreditEnt creditForm) throws IOException {
        creditForm.setRequestId(requestId);
        logger.debug(creditForm.requestId + " - createCreditTokenForCorporate : " + creditForm.toString());
        ObjResponse addCardResponse = new ObjResponse();
        int returnCode = creditForm.validateData();
        if (returnCode != 200) {
            addCardResponse.returnCode = returnCode;
            logger.debug(creditForm.requestId + " - createCreditTokenForCorporateResponse : " + addCardResponse.toString());
            return Response.status(200).header("x-request-id", creditForm.requestId).entity(addCardResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(creditForm.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.addCreditTokenForCorporate(creditForm);
        } else {
            responseStr = payBusiness.addCreditTokenForCorporate(creditForm);
        }
        String log = responseStr;
        if (creditForm.cardNumber != null && !creditForm.cardNumber.isEmpty() && creditForm.cardNumber.length() > 4) {
            log = responseStr.replace(creditForm.cardNumber, "XXXXXX" + creditForm.cardNumber.substring(creditForm.cardNumber.length() - 4));
        }
        logger.debug(creditForm.requestId + " - createCreditTokenResponse : " + log);
        return Response.status(200).header("x-request-id", creditForm.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/createCreditTokenForCustomer")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response addCreditTokenForCustomer(@HeaderParam("x-request-id") String requestId, CreditEnt creditForm) throws IOException {
        creditForm.setRequestId(requestId);
        logger.debug(creditForm.requestId + " - createCreditTokenForCustomer : " + creditForm.toString());
        ObjResponse addCardResponse = new ObjResponse();
        int returnCode = creditForm.validateData();
        if (returnCode != 200) {
            addCardResponse.returnCode = returnCode;
            logger.debug(creditForm.requestId + " - createCreditTokenForCustomerResponse : " + addCardResponse.toString());
            return Response.status(200).header("x-request-id", creditForm.requestId).entity(addCardResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(creditForm.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.addCreditTokenForCustomer(creditForm);
        } else {
            responseStr = payBusiness.addCreditTokenForCustomer(creditForm);
        }
        String log = responseStr;
        if (creditForm.cardNumber != null && !creditForm.cardNumber.isEmpty() && creditForm.cardNumber.length() > 4) {
            log = responseStr.replace(creditForm.cardNumber, "XXXXXX" + creditForm.cardNumber.substring(creditForm.cardNumber.length() - 4));
        }
        logger.debug(creditForm.requestId + " - createCreditTokenResponse : " + log);
        return Response.status(200).header("x-request-id", creditForm.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/createCreditTokenForFleetOwner")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response addCreditTokenForFleetOwner(@HeaderParam("x-request-id") String requestId, CreditEnt creditForm) throws IOException {
        creditForm.setRequestId(requestId);
        logger.debug(creditForm.requestId + " - createCreditTokenForFleetOwner : " + creditForm.toString());
        ObjResponse addCardResponse = new ObjResponse();
        int returnCode = creditForm.validateData();
        if (returnCode != 200) {
            addCardResponse.returnCode = returnCode;
            logger.debug(creditForm.requestId + " - createCreditTokenForFleetOwnerResponse : " + addCardResponse.toString());
            return Response.status(200).header("x-request-id", creditForm.requestId).entity(addCardResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(creditForm.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.addCreditTokenForFleetOwner(creditForm);
        } else {
            responseStr = payBusiness.addCreditTokenForFleetOwner(creditForm);
        }

        String log = responseStr;
        if (creditForm.cardNumber != null && !creditForm.cardNumber.isEmpty() && creditForm.cardNumber.length() > 4) {
            log = responseStr.replace(creditForm.cardNumber, "XXXXXX" + creditForm.cardNumber.substring(creditForm.cardNumber.length() - 4));
        }
        logger.debug(creditForm.requestId + " - createCreditTokenResponse : " + log);
        return Response.status(200).header("x-request-id", creditForm.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/addPromoCodeUse")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response addPromoCodeUse(@HeaderParam("x-request-id") String requestId, PromoEnt promoEnt) throws IOException {
        promoEnt.setRequestId(requestId);
        logger.debug(promoEnt.requestId + " - addPromoCodeUse : " + gson.toJson(promoEnt));
        ObjResponse objResponse = new ObjResponse();
        int returnCode = promoEnt.validateData();
        if (returnCode != 200) {
            objResponse.returnCode = returnCode;
            logger.debug(promoEnt.requestId + " - addPromoCodeUseResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", promoEnt.requestId).entity(objResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(promoEnt.fleetId);
        if (settingMQData.payService) {
            responseStr = paymentRPC.addPromoCodeUse(promoEnt);
        } else {
            responseStr = payService.addPromoCodeUse(promoEnt);
        }
        logger.debug(promoEnt.requestId + " - addPromoCodeUseResponse : " + responseStr);
        return Response.status(200).header("x-request-id", promoEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/addPromoToList")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response addPromoToList(@HeaderParam("x-request-id") String requestId, PromoEnt promoEnt) throws IOException {
        promoEnt.setRequestId(requestId);
        logger.debug(promoEnt.requestId + " - addPromoToList : " + gson.toJson(promoEnt));
        ObjResponse objResponse = new ObjResponse();
        int returnCode = promoEnt.validateData();
        if (returnCode != 200) {
            objResponse.returnCode = returnCode;
            logger.debug(promoEnt.requestId + " - addPromoToListResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", promoEnt.requestId).entity(objResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(promoEnt.fleetId);
        if (settingMQData.payService) {
            responseStr = paymentRPC.addPromoToList(promoEnt);
        } else {
            responseStr = payService.addPromoToList(promoEnt);
        }
        logger.debug(promoEnt.requestId + " - addPromoToListResponse : " + responseStr);
        return Response.status(200).header("x-request-id", promoEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/addSignature")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response addSignature(@HeaderParam("x-request-id") String requestId, SignatureForm signatureForm) throws IOException {
        logger.debug(signatureForm.requestId + " - addSignature : " + gson.toJson(signatureForm));
        signatureForm.setRequestId(requestId);
        ObjResponse addSignatureResponse = new ObjResponse();
        int returnCode = signatureForm.validateData();
        if (returnCode != 200) {
            addSignatureResponse.returnCode = returnCode;
            logger.debug(signatureForm.requestId + " - addSignatureResponse : " + addSignatureResponse.toString());
            return Response.status(200).header("x-request-id", signatureForm.requestId).entity(addSignatureResponse.toString()).build();
        }

        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(signatureForm.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.addSignature(signatureForm);
        } else {
            responseStr = payBusiness.addSignature(signatureForm);
        }
        logger.debug(signatureForm.requestId + " - addSignatureResponse : " + responseStr);
        return Response.status(200).header("x-request-id", signatureForm.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/addTicket")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response addTicket(@HeaderParam("x-request-id") String requestId, AddTicketEnt addTicketEnt) throws IOException {
        addTicketEnt.setRequestId(requestId);
        logger.debug(addTicketEnt.requestId + " - addTicket : " + gson.toJson(addTicketEnt));
        ObjResponse addTicketResponse = new ObjResponse();
        int returnCode = addTicketEnt.validateData();
        if (returnCode != 200) {
            addTicketResponse.returnCode = returnCode;
            logger.debug(addTicketEnt.requestId + " - addTicketResponse : " + addTicketResponse.toString());
            return Response.status(200).header("x-request-id", addTicketEnt.requestId).entity(addTicketResponse.toString()).build();
        }

        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(addTicketEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.addTicket(addTicketEnt);
        } else {
            responseStr = payBusiness.addTicket(addTicketEnt);
        }
        logger.debug(addTicketEnt.requestId + " - addTicketResponse : " + responseStr);
        return Response.status(200).header("x-request-id", addTicketEnt.requestId).entity(responseStr)
                .header("Content-Type", "application/json;charset=UTF-8").build();
    }

    @Path("api/paymentgateway/applePayAsyncPayEase")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response applePayAsyncPayEase(@HeaderParam("x-request-id") String requestId, ApplePayAsyncEnt applePayAsyncEnt) throws IOException {
        applePayAsyncEnt.setRequestId(requestId);
        logger.debug(applePayAsyncEnt.requestId + " - applePayAsync : " + gson.toJson(applePayAsyncEnt));
        String fleetId = proxyUtil.applePayAsyncPayEase(applePayAsyncEnt, requestId);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.applePayAsyncPayEase(applePayAsyncEnt);
        } else {
            responseStr = payBusiness.applePayAsyncPayEase(applePayAsyncEnt);
        }
        logger.debug(applePayAsyncEnt.requestId + " - responseApplePayAsync : " + responseStr);
        return Response.status(200).header("x-request-id", applePayAsyncEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/approveWithdrawal")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response approveWithdrawal(@HeaderParam("x-request-id") String requestId, WalletTransferEnt walletTransferEnt) throws IOException {
        walletTransferEnt.setRequestId(requestId);
        logger.debug(walletTransferEnt.requestId + " - approveWithdrawal : " + gson.toJson(walletTransferEnt));
        ObjResponse approveResponse = new ObjResponse();

        int returnCode = walletTransferEnt.validateDataHandleRequest();
        if (returnCode != 200) {
            approveResponse.returnCode = returnCode;
            logger.debug(walletTransferEnt.requestId + " - approveWithdrawalResponse : " + approveResponse.toString());
            return Response.status(200).header("x-request-id", walletTransferEnt.requestId).entity(approveResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(walletTransferEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.approveWithdrawal(walletTransferEnt, KeysUtil.WITHDRAWAL_APPROVED);
        } else {
            responseStr = payBusiness.approveWithdrawal(walletTransferEnt, KeysUtil.WITHDRAWAL_APPROVED);
        }

        logger.debug(walletTransferEnt.requestId + " - approveWithdrawalResponse : " + responseStr);
        return Response.status(200).header("x-request-id", walletTransferEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/cancelBooking")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response cancelBooking(@HeaderParam("x-request-id") String requestId, CancelBookingEnt cancelEnt) throws IOException {
        cancelEnt.setRequestId(requestId);
        logger.debug(cancelEnt.requestId + " - cancelBooking : " + gson.toJson(cancelEnt));
        ObjResponse cancelResponse = new ObjResponse();

        int returnCode = cancelEnt.validateData();
        if (returnCode != 200) {
            cancelResponse.returnCode = returnCode;
            logger.debug(cancelEnt.requestId + " - cancelBookingResponse : " + cancelResponse.toString());
            return Response.status(200).header("x-request-id", cancelEnt.requestId).entity(cancelResponse.toString()).build();
        }

        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(cancelEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.cancelBooking(cancelEnt);
        } else {
            responseStr = payBusiness.cancelBooking(cancelEnt);
        }
        logger.debug(cancelEnt.requestId + " - cancelBookingResponse : " + responseStr);
        return Response.status(200).header("x-request-id", cancelEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/cancelPaymentInterWithBookId/{bookId}")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @GET
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response cancelPaymentInterWithBookId(@HeaderParam("x-request-id") String requestId, @PathParam("bookId") String bookId) throws IOException {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        logger.debug(requestId + " - cancelPaymentInterWithBookId : " + bookId);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(null);//Jupiter
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.cancelPaymentInterWithBookId(bookId, requestId);
        } else {
            responseStr = payBusiness.cancelPaymentInterWithBookId(bookId, requestId);
        }
        logger.debug(requestId + " - cancelPaymentInterWithBookId : " + responseStr);
        return Response.status(200).entity(responseStr).build();
    }

    @Path("api/paymentgateway/cancelPaymentInterWithBookId/{fleetId}/{bookId}")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @GET
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response cancelPaymentInterWithBookId(@HeaderParam("x-request-id") String requestId, @PathParam("fleetId") String fleetId, @PathParam("bookId") String bookId) throws IOException {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        logger.debug(requestId + " - cancelPaymentInterWithBookId : " + bookId);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.cancelPaymentInterWithBookId(bookId, requestId);
        } else {
            responseStr = payBusiness.cancelPaymentInterWithBookId(bookId, requestId);
        }
        logger.debug(requestId + " - cancelPaymentInterWithBookId : " + responseStr);
        return Response.status(200).entity(responseStr).build();
    }

    @Path("api/paymentgateway/chargeIntercityBooking")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response chargeIntercityBooking(@HeaderParam("x-request-id") String requestId, PaymentEnt paymentEnt) throws IOException {
        paymentEnt.setRequestId(requestId);
        logger.debug(paymentEnt.requestId + " - chargeIntercityBooking: " + gson.toJson(paymentEnt));
        ObjResponse payByCreditResponse = new ObjResponse();
        int returnCode = paymentEnt.validateData();
        if (returnCode != 200) {
            payByCreditResponse.returnCode = returnCode;
            logger.debug(paymentEnt.requestId + " - chargeIntercityBookingResponse : " + payByCreditResponse.toString());
            return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(payByCreditResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(paymentEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.chargeIntercityBooking(paymentEnt);
        } else {
            responseStr = payBusiness.chargeIntercityBooking(paymentEnt);
        }
        logger.debug(paymentEnt.requestId + " - chargeIntercityBookingResponse : " + responseStr);
        return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }

    @Path("api/paymentgateway/chargerFleetOwner")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response chargerFleetOwner(@HeaderParam("x-request-id") String requestId, ChargeFleetOwner chargeFleetOwner) throws IOException {
        chargeFleetOwner.setRequestId(requestId);
        logger.debug(chargeFleetOwner.requestId + " - chargerFleetOwner : " + gson.toJson(chargeFleetOwner));
        ObjResponse objResponse = new ObjResponse();
        int returnCode = chargeFleetOwner.validateData();
        if (returnCode != 200) {
            objResponse.returnCode = returnCode;
            logger.debug(chargeFleetOwner.requestId + " - chargerFleetOwnerResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", chargeFleetOwner.requestId).entity(objResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(chargeFleetOwner.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.chargeFleetOwner(chargeFleetOwner);
        } else {
            responseStr = payBusiness.chargeFleetOwner(chargeFleetOwner);
        }
        logger.debug(chargeFleetOwner.requestId + " - chargerFleetOwnerResponse : " + responseStr);
        return Response.status(200).header("x-request-id", chargeFleetOwner.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/checkBBLCorporate")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response checkBBLCorporate(@HeaderParam("x-request-id") String requestId, BBLCorpEnt bblCorpEnt) throws IOException {
        bblCorpEnt.setRequestId(requestId);
        logger.debug("checkBBLCorporate : " + gson.toJson(bblCorpEnt));
        String responseStr = "";

        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(bblCorpEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.checkBBLCorporate(bblCorpEnt.fleetId, bblCorpEnt.corporateId);
        } else {
            responseStr = payBusiness.checkBBLCorporate(bblCorpEnt.fleetId, bblCorpEnt.corporateId);
        }
        logger.debug("checkBBLCorporateResponse : " + bblCorpEnt.requestId + " - " + responseStr);
        return Response.status(200).header("x-request-id", bblCorpEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/checkFareAvailable")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response checkFareAvailable(@HeaderParam("x-request-id") String requestId, ETAFareEnt etaFareEnt) throws IOException {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        logger.debug(requestId + " - checkFareAvailable : " + etaFareEnt.toString());
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(etaFareEnt.fleetId);
        if (settingMQData.payService) {
            responseStr = paymentRPC.checkFareAvailable(etaFareEnt);
        } else {
            responseStr = payService.checkFareAvailable(etaFareEnt);
        }
        logger.debug(requestId + " - checkFareAvailable response : " + responseStr);
        return Response.status(200).header("x-request-id", etaFareEnt.requestId).entity(responseStr).build();
    }

    /**
     * checkFlatRoutes
     *
     * @param checkFlatRoutesEnts
     * @return
     * @throws IOException
     */
    @Path("api/paymentgateway/checkFlatRoutes")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response checkFlatRoutes(@HeaderParam("x-request-id") String requestId, List<CheckFlatRoutesEnt> checkFlatRoutesEnts) throws IOException {
        logger.debug(requestId + " - checkFlatRoutes : " + gson.toJson(checkFlatRoutesEnts));
        String responseStr = "";
        CheckFlatRoutesQEnt checkFlatRoutesQEnt = new CheckFlatRoutesQEnt();
        checkFlatRoutesQEnt.list = checkFlatRoutesEnts;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(null); // PayService
        if (settingMQData.payService) {

            responseStr = paymentRPC.checkFlatRoutes(requestId, checkFlatRoutesQEnt);
        } else {
            responseStr = payService.checkFlatRoutes(requestId, checkFlatRoutesQEnt);
        }

        logger.debug(requestId + " - checkFlatRoutesResponse : " + responseStr);
        return Response.status(200).header("x-request-id", requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/checkPromoCode")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response checkPromoCode(@HeaderParam("x-request-id") String requestId, PromoEnt promoEnt) throws IOException {
        promoEnt.setRequestId(requestId);
        logger.debug(promoEnt.requestId + " - checkPromoCode : " + gson.toJson(promoEnt));
        ObjResponse objResponse = new ObjResponse();
        int returnCode = promoEnt.validateData();
        if (returnCode != 200) {
            objResponse.returnCode = returnCode;
            logger.debug(promoEnt.requestId + " - checkPromoCodeResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", promoEnt.requestId).entity(objResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(promoEnt.fleetId);
        if (settingMQData.payService) {
            responseStr = paymentRPC.checkPromoCode(promoEnt);
        } else {
            responseStr = payService.checkPromoCode(promoEnt);
            ;
        }
        logger.debug(promoEnt.requestId + " - checkPromoCodeResponse : " + responseStr);
        return Response.status(200).header("x-request-id", promoEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/checkStripeACHToken")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response checkStripeACHToken(@HeaderParam("x-request-id") String requestId, ACHEnt achEnt) throws IOException {
        achEnt.setRequestId(requestId);
        logger.debug(achEnt.requestId + " - checkACHToken : " + gson.toJson(achEnt));
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(achEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.checkStripeACHToken(achEnt);
        } else {
            responseStr = payBusiness.checkStripeACHToken(achEnt);
        }
        logger.debug(achEnt.requestId + " - checkACHTokenResponse : " + responseStr);
        return Response.status(200).header("x-request-id", achEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/checkUnionpayCard")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response checkUnionpayCard(@HeaderParam("x-request-id") String requestId, CreditEnt creditForm) throws IOException {
        creditForm.setRequestId(requestId);
        logger.debug(creditForm.requestId + " - checkUnionpayCard : " + creditForm.toString());
        ObjResponse checkUnionpayCardResponse = new ObjResponse();
        int returnCode = creditForm.validateCardnumber();
        if (returnCode != 200) {
            checkUnionpayCardResponse.returnCode = returnCode;
            logger.debug(creditForm.requestId + " - checkUnionpayCardResponse : " + checkUnionpayCardResponse.toString());
            return Response.status(200).header("x-request-id", creditForm.requestId).entity(checkUnionpayCardResponse.toString()).build();
        }

        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(creditForm.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.checkUnionpayCard(creditForm.fleetId, creditForm.cardNumber);
        } else {
            responseStr = payBusiness.checkUnionpayCard(creditForm.fleetId, creditForm.cardNumber);
        }
        logger.debug(creditForm.requestId + " - checkUnionpayCardResponse : " + responseStr);
        return Response.status(200).header("x-request-id", creditForm.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/clearCustomerOutstanding")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response clearCustomerOutstanding(@HeaderParam("x-request-id") String requestId, OutstandingEnt outstandingEnt) throws IOException {
        outstandingEnt.setRequestId(requestId);
        logger.debug(outstandingEnt.requestId + " - clearCustomerOutstanding : " + gson.toJson(outstandingEnt));
        ObjResponse objResponse = new ObjResponse();
        int returnCode = outstandingEnt.validateData();
        if (returnCode != 200) {
            objResponse.returnCode = returnCode;
            logger.debug(outstandingEnt.requestId + " - clearCustomerOutstandingResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", outstandingEnt.requestId).header("x-request-id", outstandingEnt.requestId).entity(objResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(outstandingEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.clearCustomerOutstanding(outstandingEnt);
        } else {
            responseStr = payBusiness.clearCustomerOutstanding(outstandingEnt);
        }

        logger.debug(outstandingEnt.requestId + " - clearCustomerOutstandingResponse : " + responseStr);
        return Response.status(200).header("x-request-id", outstandingEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/completeByCardWithoutCharging")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response completeBookingWithoutCharging(@HeaderParam("x-request-id") String requestId, PaymentEnt paymentEnt) throws IOException {
        paymentEnt.setRequestId(requestId);
        logger.debug(paymentEnt.requestId + " - completeBookingWithoutCharging : " + gson.toJson(paymentEnt));
        ObjResponse payByCreditResponse = new ObjResponse();
        int returnCode = paymentEnt.validateData();
        if (returnCode != 200) {
            payByCreditResponse.returnCode = returnCode;
            logger.debug(paymentEnt.requestId + " - completeBookingWithoutChargingResponse : " + payByCreditResponse.toString());
            return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(payByCreditResponse.toString()).build();
        }
        // set default payment type is Personal card
        if (paymentEnt.paymentType.equals("7"))
            paymentEnt.paymentType = KeysUtil.PAYMENT_CORPORATE;
        else
            paymentEnt.paymentType = KeysUtil.PAYMENT_CARD;


        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(paymentEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.completeBookingWithoutCharging(paymentEnt);
        } else {
            responseStr = payBusiness.completeBookingWithoutCharging(paymentEnt);
        }
        logger.debug(paymentEnt.requestId + " - completeBookingWithoutChargingResponse : " + responseStr);

        return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }

    @Path("api/paymentgateway/completedWithoutPayment")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response completedWithoutPayment(@HeaderParam("x-request-id") String requestId, IncidentEnt incidentEnt) throws IOException {
        incidentEnt.setRequestId(requestId);
        logger.debug(incidentEnt.requestId + " - completedWithoutPayment : " + gson.toJson(incidentEnt));
        ObjResponse incidentResponse = new ObjResponse();

        int returnCode = incidentEnt.validateData();
        if (returnCode != 200) {
            incidentResponse.returnCode = returnCode;
            logger.debug(incidentEnt.requestId + " - completedWithoutPaymentResponse : " + incidentResponse.toString());
            return Response.status(200).header("x-request-id", incidentEnt.requestId).entity(incidentResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(incidentEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.completedWithoutPayment(incidentEnt);
        } else {
            responseStr = payBusiness.completedWithoutPayment(incidentEnt);
        }
        logger.debug(incidentEnt.requestId + " - completedWithoutPaymentResponse : " + responseStr);
        return Response.status(200).header("x-request-id", incidentEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/completeIntercityTrip")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response completeIntercityTrip(@HeaderParam("x-request-id") String requestId, IntercityTripCompleteEnt completeEnt) throws IOException {
        completeEnt.setRequestId(requestId);
        logger.debug(completeEnt.requestId + " - completeIntercityTrip: " + gson.toJson(completeEnt));
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(completeEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.completeIntercityTrip(completeEnt);
        } else {
            responseStr = payBusiness.completeIntercityTrip(completeEnt);
        }
        logger.debug(completeEnt.requestId + " - completeIntercityTripResponse : " + responseStr);

        return Response.status(200).header("x-request-id", completeEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }

    @Path("api/paymentgateway/confirmAuthenStripe/{bookId}")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @GET
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response confirmAuthenStripe(@HeaderParam("x-request-id") String requestId, @PathParam("bookId") String bookId) throws IOException {
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(null); // Jupiter
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.confirmStripeData(bookId);
        } else {
            responseStr = payBusiness.confirmStripeData(bookId);
        }
        logger.debug(requestId + " - confirmAuthenStripe : " + responseStr);
        return Response.status(200).entity(responseStr).build();
    }

    @Path("api/paymentgateway/confirmAuthenStripe/{fleetId}/{bookId}")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @GET
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response confirmAuthenStripe(@HeaderParam("x-request-id") String requestId, @PathParam("fleetId") String fleetId, @PathParam("bookId") String bookId) throws IOException {
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.confirmStripeData(bookId);
        } else {
            responseStr = payBusiness.confirmStripeData(bookId);
        }
        logger.debug(requestId + " - confirmAuthenStripe : " + responseStr);
        return Response.status(200).entity(responseStr).build();
    }

    @Path("api/paymentgateway/confirmPreAuth/{paymentInterId}")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @GET
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response confirmPreAuth(@HeaderParam("x-request-id") String requestId, @PathParam("paymentInterId") String paymentInterId) throws IOException {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        String responseStr = "";

        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(null); // Jupiter
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.confirmPreAuth(paymentInterId, requestId);
        } else {
            responseStr = payBusiness.confirmPreAuth(paymentInterId, requestId);
        }
        logger.debug(requestId + " - confirmPreAuth : " + responseStr);
        return Response.status(200).entity(responseStr).build();
    }

    @Path("api/paymentgateway/confirmPreAuth/{fleetId}/{paymentInterId}")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @GET
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response confirmPreAuth(@HeaderParam("x-request-id") String requestId, @PathParam("fleetId") String fleetId, @PathParam("paymentInterId") String paymentInterId) throws IOException {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        String responseStr = "";

        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.confirmPreAuth(paymentInterId, requestId);
        } else {
            responseStr = payBusiness.confirmPreAuth(paymentInterId, requestId);
        }
        logger.debug(requestId + " - confirmPreAuth : " + responseStr);
        return Response.status(200).entity(responseStr).build();
    }

    @Path("api/paymentgateway/createACHToken")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response createACHToken(@HeaderParam("x-request-id") String requestId, ACHEnt achEnt) throws IOException {
        achEnt.setRequestId(requestId);
        logger.debug(achEnt.requestId + " - createACHToken : " + gson.toJson(achEnt));
        ObjResponse achResponse = new ObjResponse();

        int returnCode = achEnt.validateData();
        if (returnCode != 200) {
            achResponse.returnCode = returnCode;
            logger.debug(achEnt.requestId + " - createACHTokenResponse : " + achResponse.toString());
            return Response.status(200).header("x-request-id", achEnt.requestId).entity(achResponse.toString()).build();
        }

        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(achEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.createACHToken(achEnt, "add");
        } else {
            responseStr = payBusiness.createACHToken(achEnt, "add");
        }
        logger.debug(achEnt.requestId + " - createACHTokenResponse : " + responseStr);
        return Response.status(200).header("x-request-id", achEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/createCreditTokenForDriver")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response createCreditTokenForDriver(@HeaderParam("x-request-id") String requestId, String json) throws IOException {
        JsonObject jobj = new Gson().fromJson(json, JsonObject.class);
        String tokenStr = "";
        try {
            JsonObject token = jobj.get("token").getAsJsonObject();
            tokenStr = token.toString();
        }catch (Exception ex){
        }
        if (tokenStr.length() > 0){
            jobj.remove("token");
        }
        CreditEnt creditForm = gson.fromJson(jobj.toString(), CreditEnt.class);
        creditForm.setRequestId(requestId);
        if (tokenStr.length()>0){
            creditForm.token = tokenStr;
        }
        logger.debug(creditForm.requestId + " - createCreditTokenForDriver : " + creditForm.toString());
        ObjResponse addCardResponse = new ObjResponse();
        int returnCode = creditForm.validateData();
        if (returnCode != 200) {
            addCardResponse.returnCode = returnCode;
            logger.debug(creditForm.requestId + " - createCreditTokenForDriverResponse : " + addCardResponse.toString());
            return Response.status(200).header("x-request-id", creditForm.requestId).entity(addCardResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(creditForm.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.createCreditTokenForDriver(creditForm);
        } else {
            responseStr = payBusiness.createCreditTokenForDriver(creditForm);
        }

        String log = responseStr;
        if (creditForm.cardNumber != null && !creditForm.cardNumber.isEmpty() && creditForm.cardNumber.length() > 4) {
            log = responseStr.replace(creditForm.cardNumber, "XXXXXX" + creditForm.cardNumber.substring(creditForm.cardNumber.length() - 4));
        }
        logger.debug(creditForm.requestId + " - createCreditTokenResponse : " + log);
        return Response.status(200).header("x-request-id", creditForm.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/createWebhookStripe")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response createWebhookStripe(@HeaderParam("x-request-id") String requestId, WebhookCreateEnt whEnt) throws IOException {
        whEnt.setRequestId(requestId);
        logger.debug(whEnt.requestId + " - createWebhookStripe : " + gson.toJson(whEnt));
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(whEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.createWebhookStripe(whEnt);
        } else {
            responseStr = payBusiness.createWebhookStripe(whEnt);
        }
        logger.debug(whEnt.requestId + " - createWebhookStripeResponse : " + whEnt.requestId + " - " + responseStr);
        return Response.status(200).header("x-request-id", whEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/deleteCorporateToken")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response deleteCorporateToken(@HeaderParam("x-request-id") String requestId, CreditDeleteEnt deleteCreditForm) throws IOException {
        deleteCreditForm.setRequestId(requestId);
        logger.debug(deleteCreditForm.requestId + " - deleteCorporateToken : " + gson.toJson(deleteCreditForm));
        ObjResponse deleteCardResponse = new ObjResponse();
        int returnCode = deleteCreditForm.validateData();
        if (returnCode != 200) {
            deleteCardResponse.returnCode = returnCode;
            logger.debug(deleteCreditForm.requestId + " - deleteCorporateTokenResponse : " + deleteCardResponse.toString());
            return Response.status(200).header("x-request-id", deleteCreditForm.requestId).entity(deleteCardResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(deleteCreditForm.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.deleteCorporateToken(deleteCreditForm);
        } else {
            responseStr = payBusiness.deleteCorporateToken(deleteCreditForm);
        }
        logger.debug(deleteCreditForm.requestId + " - deleteCorporateTokenResponse : " + responseStr);
        return Response.status(200).header("x-request-id", deleteCreditForm.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/deleteCreditToken")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response deleteCreditToken(@HeaderParam("x-request-id") String requestId, CreditDeleteEnt deleteCreditForm) throws IOException {
        deleteCreditForm.setRequestId(requestId);
        logger.debug(deleteCreditForm.requestId + " - deleteCreditToken : " + gson.toJson(deleteCreditForm));
        ObjResponse deleteCardResponse = new ObjResponse();
        int returnCode = deleteCreditForm.validateData();
        if (returnCode != 200) {
            deleteCardResponse.returnCode = returnCode;
            logger.debug(deleteCreditForm.requestId + " - deleteCreditTokenResponse : " + deleteCardResponse.toString());
            return Response.status(200).header("x-request-id", deleteCreditForm.requestId).entity(deleteCardResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(deleteCreditForm.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.deleteCreditToken(deleteCreditForm);
        } else {
            responseStr = payBusiness.deleteCreditToken(deleteCreditForm);
        }
        logger.debug(deleteCreditForm.requestId + " - deleteCreditTokenResponse : " + responseStr);
        return Response.status(200).header("x-request-id", deleteCreditForm.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/deleteCreditTokenForFleetOwner")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response deleteCreditTokenForFleetOwner(@HeaderParam("x-request-id") String requestId, CreditDeleteEnt deleteCreditForm) throws IOException {
        deleteCreditForm.setRequestId(requestId);
        logger.debug(deleteCreditForm.requestId + " - deleteCreditTokenForFleetOwner : " + gson.toJson(deleteCreditForm));
        ObjResponse deleteCardResponse = new ObjResponse();
        int returnCode = deleteCreditForm.validateData();
        if (returnCode != 200) {
            deleteCardResponse.returnCode = returnCode;
            logger.debug(deleteCreditForm.requestId + " - deleteCreditTokenForFleetOwnerResponse : " + deleteCardResponse.toString());
            return Response.status(200).header("x-request-id", deleteCreditForm.requestId).entity(deleteCardResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(deleteCreditForm.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.deleteCreditTokenForFleetOwner(deleteCreditForm);
        } else {
            responseStr = payBusiness.deleteCreditTokenForFleetOwner(deleteCreditForm);
        }
        logger.debug(deleteCreditForm.requestId + " - deleteCreditTokenForFleetOwnerResponse : " + responseStr);
        return Response.status(200).header("x-request-id", deleteCreditForm.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/deletePromoCodeUse")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response deletePromoCodeUse(@HeaderParam("x-request-id") String requestId, PromoEnt promoEnt) throws IOException {
        promoEnt.setRequestId(requestId);
        logger.debug(promoEnt.requestId + " - deletePromoCodeUse : " + promoEnt.toString());
        ObjResponse objResponse = new ObjResponse();
        int returnCode = promoEnt.validateData();
        if (returnCode != 200 || promoEnt.userId == null || promoEnt.userId.isEmpty()) {
            objResponse.returnCode = returnCode;
            logger.debug(promoEnt.requestId + " - deletePromoCodeUseResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", promoEnt.requestId).entity(objResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(promoEnt.fleetId);
        if (settingMQData.payService) {
            responseStr = paymentRPC.deletePromoCodeUse(promoEnt);
        } else {
            responseStr = payService.deletePromoCodeUse(promoEnt);
        }
        logger.debug(promoEnt.requestId + " - deletePromoCodeUseResponse : " + responseStr);
        return Response.status(200).header("x-request-id", promoEnt.requestId).entity(responseStr).build();
    }

    /**
     * estimateDeliveryFare
     *
     * @param etaFareEnt
     * @return
     * @throws IOException
     */
    @Path("api/paymentgateway/estimateDeliveryFare")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response estimateDeliveryFare(@HeaderParam("x-request-id") String requestId, @HeaderParam("Accept-Language") String language, ETADeliveryEnt etaFareEnt) throws IOException {
        etaFareEnt.setRequestId(requestId);
        etaFareEnt.language = language;
        logger.debug(etaFareEnt.requestId + " - language : " + language);
        logger.debug(etaFareEnt.requestId + " - estimateDeliveryFare : " + gson.toJson(etaFareEnt));
        ObjResponse etaResponse = new ObjResponse();
        int returnCode = etaFareEnt.validateData();
        if (returnCode != 200) {
            etaResponse.returnCode = returnCode;
            logger.debug(etaFareEnt.requestId + " - estimateDeliveryFareResponse : " + etaResponse.toString());
            return Response.status(200).header("x-request-id", etaFareEnt.requestId).entity(etaResponse.toString()).build();
        }
        String estimateFareResponse = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(etaFareEnt.fleetId);
        if (settingMQData.payService) {
            estimateFareResponse = paymentRPC.estimateDeliveryFare(etaFareEnt);
        } else {
            estimateFareResponse = payService.estimateDeliveryFare(etaFareEnt);
        }

        logger.debug(etaFareEnt.requestId + " - estimateDeliveryFareResponse from msg : " + estimateFareResponse);
        return Response.status(200).header("x-request-id", etaFareEnt.requestId).entity(estimateFareResponse).build();
    }

    /**
     * estimateFare
     *
     * @param etaFareEnt
     * @return
     * @throws IOException
     */
    @Path("api/paymentgateway/estimateFare")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response estimateFare(@HeaderParam("x-request-id") String requestId, ETAFareEnt etaFareEnt) throws IOException {
        etaFareEnt.setRequestId(requestId);
        logger.debug(etaFareEnt.requestId + " - estimateFare : " + gson.toJson(etaFareEnt));
        ObjResponse etaResponse = new ObjResponse();
        int returnCode = etaFareEnt.validateData();
        if (returnCode != 200) {
            etaResponse.returnCode = returnCode;
            logger.debug(etaFareEnt.requestId + " - estimateFareResponse : " + etaResponse.toString());
            return Response.status(200).header("x-request-id", etaFareEnt.requestId).entity(etaResponse.toString()).build();
        }
        String estimateFareResponse = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(etaFareEnt.fleetId);
        if (settingMQData.payService) {
            estimateFareResponse = paymentRPC.estimateFare(etaFareEnt);
        } else {
            estimateFareResponse = payService.estimateFare(etaFareEnt);
        }

        logger.debug(etaFareEnt.requestId + " - estimateFareResponse from msg : " + estimateFareResponse);
        //logger.debug(etaFareEnt.requestId + " - estimateFareResponse : " + etaResponse);
        return Response.status(200).header("x-request-id", etaFareEnt.requestId).entity(estimateFareResponse).build();
    }

    /**
     * estimateFareForAthena
     *
     * @param etaFareEnts
     * @return
     * @throws IOException
     */
    @Path("api/paymentgateway/estimatePrice")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response estimateFareForAthena(@HeaderParam("x-request-id") String requestId, List<ETAFareEnt> etaFareEnts) throws IOException {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        logger.debug(requestId + " - estimatePrice : " + gson.toJson(etaFareEnts));
        ObjResponse res = new ObjResponse();
        List<ObjResponse> response = new ArrayList<>();
        for (ETAFareEnt etaFareEnt : etaFareEnts) {
            etaFareEnt.setRequestId(requestId);
            ObjResponse etaResponse = new ObjResponse();
            int returnCode = etaFareEnt.validateData();
            if (returnCode != 200) {
                etaResponse.returnCode = returnCode;
                etaResponse.response = "";
            } else {
                etaResponse.returnCode = 200;
                String estimateFareResponse = "";
                PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(etaFareEnt.fleetId);
                if (settingMQData.payService) {
                    estimateFareResponse = paymentRPC.estimateFare(etaFareEnt);
                } else {
                    estimateFareResponse = payService.estimateFare(etaFareEnt);
                }
                etaResponse = gson.fromJson(estimateFareResponse, ObjResponse.class);
            }
            response.add(etaResponse);
        }
        res.returnCode = 200;
        res.response = response;
        String responseStr = res.toString();
        logger.debug(requestId + " - estimatePriceResponse : " + responseStr);
        return Response.status(200).header("x-request-id", requestId).entity(responseStr).build();
    }

    /**
     * estimateFareWithAllSupliers
     *
     * @param etaFareEnt
     * @return
     * @throws IOException
     */
    @Path("api/paymentgateway/estimateFareWithAllSupliers")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response estimateFareWithAllSupliers(@HeaderParam("x-request-id") String requestId, ETAFareEnt etaFareEnt) throws IOException {
        etaFareEnt.setRequestId(requestId);
        logger.debug(etaFareEnt.requestId + " - estimateFareWithAllSupliers : " + gson.toJson(etaFareEnt));
        ObjResponse etaResponse = new ObjResponse();
        int returnCode = etaFareEnt.validateData();
        if (returnCode != 200) {
            etaResponse.returnCode = returnCode;
            logger.debug(etaFareEnt.requestId + " - estimateFareWithAllSupliersResponse : " + etaResponse.toString());
            return Response.status(200).header("x-request-id", etaFareEnt.requestId).entity(etaResponse.toString()).build();
        }
        String response = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(etaFareEnt.fleetId);
        if (settingMQData.payService) {
            response = paymentRPC.estimateFareWithAllSupliers(etaFareEnt);
        } else {
            response = payService.estimateFareWithAllSupliers(etaFareEnt);
        }


        logger.debug(etaFareEnt.requestId + " - estimateFareWithAllSupliersResponse : " + response);
        return Response.status(200).header("x-request-id", etaFareEnt.requestId).entity(response).build();
    }

    /**
     * estimateFareWithMultiCarType
     *
     * @param etaFareEnt
     * @return
     * @throws IOException
     */
    @Path("api/paymentgateway/estimateFareWithMultiCarType")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response estimateFareWithMultiCarType(@HeaderParam("x-request-id") String requestId, ETAFareEnt etaFareEnt) throws IOException {
        etaFareEnt.setRequestId(requestId);
        logger.debug(etaFareEnt.requestId + " - estimateFareWithMultiCarType : " + gson.toJson(etaFareEnt));
        if (etaFareEnt.rateDetails == null || etaFareEnt.rateDetails.isEmpty() || etaFareEnt.isEmptyString(etaFareEnt.fleetId)
                || etaFareEnt.isEmptyString(etaFareEnt.bookFrom)) {
            ObjResponse etaResponse = new ObjResponse();
            etaResponse.returnCode = 406;
            logger.debug(etaFareEnt.requestId + " - estimateFareWithMultiCarTypeResponse : " + etaResponse.toString());
            return Response.status(200).header("x-request-id", etaFareEnt.requestId).entity(etaResponse.toString()).build();
        }
        String response = "";
        try {
            PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(etaFareEnt.fleetId);
            if (settingMQData.payService) {
                response = paymentRPC.estimateFareWithMultiCarType(etaFareEnt);
            } else {
                response = payService.estimateFareWithMultiCarType(etaFareEnt);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        logger.debug(etaFareEnt.requestId + " - estimateFareWithMultiCarTypeResponse : " + response);
        return Response.status(200).header("x-request-id", etaFareEnt.requestId).entity(response).build();
    }

    /**
     * estimateFareCarHailing
     *
     * @param etaFareEnt
     * @return
     * @throws IOException
     */
    @Path("api/paymentgateway/estimateFareCarHailing")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response estimateFareCarHailing(@HeaderParam("x-request-id") String requestId, ETAFareEnt etaFareEnt) throws IOException {
        etaFareEnt.setRequestId(requestId);
        etaFareEnt.bookFrom = KeysUtil.car_hailing;
        logger.debug(etaFareEnt.requestId + " - estimateFareCarHailing : " + gson.toJson(etaFareEnt));
        ObjResponse etaResponse = new ObjResponse();
        int returnCode = etaFareEnt.validateData();
        if (returnCode != 200) {
            etaResponse.returnCode = returnCode;
            logger.debug(etaFareEnt.requestId + " - estimateFareCarHailingResponse : " + etaResponse.toString());
            return Response.status(200).header("x-request-id", etaFareEnt.requestId).entity(etaResponse.toString()).build();
        }
        String response = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(etaFareEnt.fleetId);
        if (settingMQData.payService) {
            response = paymentRPC.estimateFareCarHailing(etaFareEnt);
        } else {
            response = payService.estimateFareCarHailing(etaFareEnt);
        }


        logger.debug(etaFareEnt.requestId + " - estimateFareCarHailingResponse : " + response);
        return Response.status(200).header("x-request-id", etaFareEnt.requestId).entity(response).build();
    }

    /**
     * estimateFareMeter
     *
     * @param etaFareMeterEnt
     * @return
     * @throws IOException
     */
    @Path("api/paymentgateway/estimateFareMeter")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response estimateFareMeter(@HeaderParam("x-request-id") String requestId, ETAFareMeterEnt etaFareMeterEnt) throws IOException {
        etaFareMeterEnt.setRequestId(requestId);
        logger.debug(etaFareMeterEnt.requestId + " - estimateFareMeter : " + gson.toJson(etaFareMeterEnt));
        ObjResponse etaResponse = new ObjResponse();
        int returnCode = etaFareMeterEnt.validateData();
        if (returnCode != 200) {
            etaResponse.returnCode = returnCode;
            logger.debug(etaFareMeterEnt.requestId + " - estimateFareMeterResponse : " + etaResponse.toString());
            return Response.status(200).header("x-request-id", etaFareMeterEnt.requestId).entity(etaResponse.toString()).build();
        }
        String response = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(null);
        if (settingMQData.payService) {
            response = paymentRPC.estimateFareMeter(etaFareMeterEnt);
        } else {
            response = payService.estimateFareMeter(etaFareMeterEnt);
        }


        logger.debug(etaFareMeterEnt.requestId + " - estimateFareMeterResponse : " + response);
        return Response.status(200).header("x-request-id", etaFareMeterEnt.requestId).entity(response).build();
    }

    /**
     * estimateFareSharing
     *
     * @param sharingEnt
     * @return
     * @throws IOException
     */
    @Path("api/paymentgateway/estimateFareSharing")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response estimateFareSharing(@HeaderParam("x-request-id") String requestId, ETASharingEnt sharingEnt) throws IOException {
        sharingEnt.setRequestId(requestId);
        logger.debug(sharingEnt.requestId + " - estimateFareSharing : " + gson.toJson(sharingEnt));
        ObjResponse etaResponse = new ObjResponse();
        int returnCode = sharingEnt.validateData();
        if (returnCode != 200) {
            etaResponse.returnCode = returnCode;
            logger.debug(sharingEnt.requestId + " - estimateFareSharingResponse : " + etaResponse.toString());
            return Response.status(200).header("x-request-id", sharingEnt.requestId).entity(etaResponse.toString()).build();
        }
        String response = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(null);
        if (settingMQData.payService) {
            response = paymentRPC.estimateFareSharing(sharingEnt);
        } else {
            response = payService.estimateFareSharing(sharingEnt);
        }
        logger.debug(sharingEnt.requestId + " - estimateFareSharingResponse : " + response);
        return Response.status(200).header("x-request-id", sharingEnt.requestId).entity(response).build();
    }

    @Path("api/paymentgateway/generateBillingReport/{month}/{year}")
    @RolesAllowed(DIS_ROLE)
    @GET
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response generateBillingReport(@HeaderParam("x-request-id") String requestId, @PathParam("month") int month, @PathParam("year") int year) throws IOException {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        logger.debug(requestId + " - generateBillingReport - month: " + month + " - year: " + year);
        String responseStr = paymentRPC.generateBillingReport(requestId, month, year); // use queue as default
        logger.debug(requestId + " - generateBillingReportResponse : " + responseStr);
        return Response.status(200).header("x-request-id", requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/getCancelAmount")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getCancelAmount(@HeaderParam("x-request-id") String requestId, CancelBookingEnt cancelEnt) throws IOException {
        cancelEnt.setRequestId(requestId);
        logger.debug(cancelEnt.requestId + " - getCancelAmount : " + gson.toJson(cancelEnt));
        ObjResponse cancelResponse = new ObjResponse();

        int returnCode = cancelEnt.validateData();
        if (returnCode != 200) {
            cancelResponse.returnCode = returnCode;
            logger.debug(cancelEnt.requestId + " - getCancelAmountResponse : " + cancelResponse.toString());
            return Response.status(200).header("x-request-id", cancelEnt.requestId).entity(cancelResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(cancelEnt.fleetId);
        if (settingMQData.payService) {
            responseStr = paymentRPC.getCancelAmount(cancelEnt);
        } else {
            responseStr = payService.getCancelAmount(cancelEnt);
        }

        logger.debug(cancelEnt.requestId + " - getCancelAmountResponse : " + responseStr);
        return Response.status(200).header("x-request-id", cancelEnt.requestId).entity(responseStr).build();
    }


    @Path("api/paymentgateway/getClientSecret")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getClientSecret(@HeaderParam("x-request-id") String requestId, ClientSecretEnt clientSecretEnt) throws IOException {
        clientSecretEnt.setRequestId(requestId);
        logger.debug(clientSecretEnt.requestId + " - getClientSecret : " + gson.toJson(clientSecretEnt));
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(clientSecretEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.getClientSecret(clientSecretEnt);
        } else {
            responseStr = payBusiness.getClientSecret(clientSecretEnt);
        }

        logger.debug(clientSecretEnt.requestId + " - getClientSecretResponse : " + responseStr);
        return Response.status(200).header("x-request-id", clientSecretEnt.requestId).entity(responseStr).build();
    }


    @Path("api/paymentgateway/getCreditBalances")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getCreditBalances(@HeaderParam("x-request-id") String requestId, CreditBalancesEnt balancesEnt) throws IOException {
        balancesEnt.setRequestId(requestId);
        logger.debug(balancesEnt.requestId + " - getCreditBalances : " + gson.toJson(balancesEnt));
        ObjResponse objResponse = new ObjResponse();
        int returnCode = balancesEnt.validateData();
        if (returnCode != 200) {
            objResponse.returnCode = returnCode;
            logger.debug(balancesEnt.requestId + " - getCreditBalancesResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", balancesEnt.requestId).entity(objResponse.toString()).build();
        }

        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(balancesEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.getCreditBalances(balancesEnt);
        } else {
            responseStr = payBusiness.getCreditBalances(balancesEnt);
        }
        logger.debug(balancesEnt.requestId + " - getCreditBalancesResponse : " + responseStr);
        return Response.status(200).header("x-request-id", balancesEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/getEncrypted/{data}")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @GET
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getEncrypted(@PathParam("data") String data) throws IOException {
        ObjResponse getEncryptedResponse = new ObjResponse();
        getEncryptedResponse.returnCode = 200;
        String responseStr = paymentRPC.getEncrypted(data); // use queue as default
        logger.debug("getEncrypted : " + responseStr);
        return Response.status(200).entity(responseStr).build();
    }

    @Path("api/paymentgateway/getFileId")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getFileId(@HeaderParam("x-request-id") String requestId, ACHEnt achEnt) throws IOException {
        achEnt.setRequestId(requestId);
        logger.debug(achEnt.requestId + " - getStripeFileId: " + gson.toJson(achEnt));
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(achEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.getFileId(achEnt.requestId, achEnt.fleetId, achEnt.gateway, achEnt.verificationDocument);
        } else {
            responseStr = payBusiness.getFileId(achEnt.requestId, achEnt.fleetId, achEnt.gateway, achEnt.verificationDocument);
        }
        logger.debug(achEnt.requestId + " - getStripeFileIdResponse : " + responseStr);

        return Response.status(200).header("x-request-id", achEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }

    @Path("api/paymentgateway/getFirstPayDetail/{bookId}")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @GET
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getFirstPayDetail(@HeaderParam("x-request-id") String requestId, @PathParam("bookId") String bookId) throws IOException {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        logger.debug(requestId + " - getFirstPayDetail - bookId:" + bookId);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(null); // Jupiter
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.getFirstPayDetail(requestId, bookId);
        } else {
            responseStr = payBusiness.getFirstPayDetail(requestId, bookId);
        }
        logger.debug(requestId + " - getFirstPayDetailResponse : " + responseStr);
        return Response.status(200).header("x-request-id", requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/getFirstPayDetail/{fleetId}/{bookId}")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @GET
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getFirstPayDetail(@HeaderParam("x-request-id") String requestId, @PathParam("fleetId") String fleetId, @PathParam("bookId") String bookId) throws IOException {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        logger.debug(requestId + " - getFirstPayDetail - bookId:" + bookId);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.getFirstPayDetail(requestId, bookId);
        } else {
            responseStr = payBusiness.getFirstPayDetail(requestId, bookId);
        }
        logger.debug(requestId + " - getFirstPayDetailResponse : " + responseStr);
        return Response.status(200).header("x-request-id", requestId).entity(responseStr).build();
    }

    /**
     * getGeneralSettingAffiliate
     *
     * @param etaFareEnt
     * @return
     * @throws IOException
     */
    @Path("api/paymentgateway/getGeneralSettingAffiliate")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getGeneralSettingAffiliate(@HeaderParam("x-request-id") String requestId, ETAFareEnt etaFareEnt) throws IOException {
        etaFareEnt.setRequestId(requestId);
        logger.debug(etaFareEnt.requestId + " - getGeneralSettingAffiliate : " + etaFareEnt.toString());
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(etaFareEnt.fleetId);
        if (settingMQData.payService) {
            responseStr = paymentRPC.getGeneralSettingAffiliate(etaFareEnt);
        } else {
            responseStr = payService.getGeneralSettingAffiliate(etaFareEnt);
        }
        logger.debug(etaFareEnt.requestId + " - getGeneralSettingAffiliateResponse : " + responseStr);
        return Response.status(200).header("x-request-id", etaFareEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/getOutstandingAmount")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getOutstandingAmount(@HeaderParam("x-request-id") String requestId, OutstandingEnt outstandingEnt) throws IOException {
        outstandingEnt.setRequestId(requestId);
        logger.debug(outstandingEnt.requestId + " - getOutstandingAmount : " + gson.toJson(outstandingEnt));
        ObjResponse objResponse = new ObjResponse();
        int returnCode = outstandingEnt.validateGetAmount();
        if (returnCode != 200) {
            objResponse.returnCode = returnCode;
            logger.debug(outstandingEnt.requestId + " - getOutstandingAmountResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", outstandingEnt.requestId).header("x-request-id", outstandingEnt.requestId).entity(objResponse.toString()).build();
        }

        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(outstandingEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.getOutstandingAmount(outstandingEnt);
        } else {
            responseStr = payBusiness.getOutstandingAmount(outstandingEnt);
        }

        logger.debug(outstandingEnt.requestId + " - getOutstandingAmountResponse : " + responseStr);
        return Response.status(200).header("x-request-id", outstandingEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/getOutstandingTicket")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getOutstandingTicket(@HeaderParam("x-request-id") String requestId, OutstandingEnt outstandingEnt) throws IOException {
        outstandingEnt.setRequestId(requestId);
        logger.debug(outstandingEnt.requestId + " - getOutstandingTicket : " + gson.toJson(outstandingEnt));
        ObjResponse objResponse = new ObjResponse();
        int returnCode = outstandingEnt.validateGetAmount();
        if (returnCode != 200) {
            objResponse.returnCode = returnCode;
            logger.debug(outstandingEnt.requestId + " - getOutstandingTicketResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", outstandingEnt.requestId).header("x-request-id", outstandingEnt.requestId).entity(objResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(outstandingEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.getOutstandingTicket(outstandingEnt);
        } else {
            responseStr = payBusiness.getOutstandingTicket(outstandingEnt);
        }

        logger.debug(outstandingEnt.requestId + " - getOutstandingTicketResponse : " + responseStr);
        return Response.status(200).header("x-request-id", outstandingEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/getPaxWallet")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getPaxWallet(@HeaderParam("x-request-id") String requestId, UserWalletEnt userWalletEnt) throws IOException {
        userWalletEnt.setRequestId(requestId);
        logger.debug(userWalletEnt.requestId + " - getPaxWallet : " + gson.toJson(userWalletEnt));
        ObjResponse getWalletResponse = new ObjResponse();

        int returnCode = userWalletEnt.validateDPaxWallet();
        if (returnCode != 200) {
            getWalletResponse.returnCode = returnCode;
            logger.debug(userWalletEnt.requestId + " - getPaxWalletResponse : " + getWalletResponse.toString());
            return Response.status(200).header("x-request-id", userWalletEnt.requestId).entity(getWalletResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(userWalletEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.getPaxWallet(userWalletEnt);
        } else {
            responseStr = payBusiness.getPaxWallet(userWalletEnt);
        }

        logger.debug(userWalletEnt.requestId + " - getPaxWalletResponse : " + responseStr);
        return Response.status(200).header("x-request-id", userWalletEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/getPaymentData")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getPaymentData(@HeaderParam("x-request-id") String requestId, UpdateEnt updateEnt) throws IOException {
        updateEnt.setRequestId(requestId);
        logger.debug(updateEnt.requestId + " - getPaymentData : " + updateEnt.bookId);
        ObjResponse objResponse = new ObjResponse();
        if (updateEnt.bookId == null || updateEnt.bookId.isEmpty()) {
            objResponse.returnCode = 406;
            logger.debug(updateEnt.requestId + " - getPaymentDataResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", updateEnt.requestId).entity(objResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(updateEnt.fleetId);
        if (settingMQData.payService) {
            responseStr = paymentRPC.getPaymentData(updateEnt);
        } else {
            responseStr = payService.getPaymentData(updateEnt);
        }

        logger.debug(updateEnt.requestId + " - getPaymentDataResponse : " + responseStr);
        return Response.status(200).header("x-request-id", updateEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/getRequiredFields")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getRequiredFields(@HeaderParam("x-request-id") String requestId, ACHEnt achEnt) throws IOException {
        achEnt.setRequestId(requestId);
        logger.debug(achEnt.requestId + " - getRequiredFields : " + gson.toJson(achEnt));
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(achEnt.fleetId);
        logger.debug(achEnt.requestId + " - settingMQData : " + gson.toJson(settingMQData));
        logger.debug(achEnt.requestId + " - payBusiness : " + settingMQData.payBusiness);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.getRequiredFields(achEnt);
        } else {
            try {
                responseStr = payBusiness.getRequiredFields(achEnt);
            } catch (Exception ex) {
                logger.debug(requestId + "  - exception: " + CommonUtils.getError(ex));
            }
        }
        logger.debug(achEnt.requestId + " - getRequiredFieldsResponse : " + responseStr);
        return Response.status(200).header("x-request-id", achEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/getRushHour/{bookId}")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @GET
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getRushHour(@HeaderParam("x-request-id") String requestId, @PathParam("bookId") String bookId) throws IOException {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        logger.debug(requestId + " - getRushHour : " + bookId);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(null); // PayService
        if (settingMQData.payService) {
            responseStr = paymentRPC.getRushHour(requestId, bookId);
        } else {
            responseStr = payService.getRushHour(requestId, bookId);
            ;
        }

        logger.debug(requestId + " - getRushHourResponse : " + responseStr);
        return Response.status(200).header("x-request-id", requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/getTicket")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getTicket(@HeaderParam("x-request-id") String requestId, AddTicketEnt addTicketEnt) throws IOException {
        addTicketEnt.setRequestId(requestId);
        logger.debug(addTicketEnt.requestId + " - getTicket : " + gson.toJson(addTicketEnt));
        ObjResponse objResponse = new ObjResponse();
        int returnCode = addTicketEnt.validateData();
        if (returnCode != 200) {
            objResponse.returnCode = returnCode;
            logger.debug(addTicketEnt.requestId + " - getTicketResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", addTicketEnt.requestId).entity(objResponse.toString()).build();
        }
        String responseStr = "";

        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(addTicketEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.getTicket(addTicketEnt);
        } else {
            responseStr = payBusiness.getTicket(addTicketEnt);
        }
        logger.debug(addTicketEnt.requestId + " - getTicketResponse : " + responseStr);
        return Response.status(200).header("x-request-id", addTicketEnt.requestId).entity(responseStr)
                .header("Content-Type", "application/json;charset=UTF-8").build();
    }

    @Path("api/paymentgateway/getWalletInfo")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getWalletInfo(@HeaderParam("x-request-id") String requestId, UserWalletEnt userWalletEnt) throws IOException {
        userWalletEnt.setRequestId(requestId);
        logger.debug(userWalletEnt.requestId + " - getWalletInfo : " + gson.toJson(userWalletEnt));
        ObjResponse getWalletResponse = new ObjResponse();

        int returnCode = userWalletEnt.validateDriverWallet();
        if (returnCode != 200) {
            getWalletResponse.returnCode = returnCode;
            logger.debug(userWalletEnt.requestId + " - getWalletInfoResponse : " + getWalletResponse.toString());
            return Response.status(200).header("x-request-id", userWalletEnt.requestId).entity(getWalletResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(userWalletEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.getWalletInfo(userWalletEnt);
        } else {
            responseStr = payBusiness.getWalletInfo(userWalletEnt);
        }
        logger.debug(userWalletEnt.requestId + " - getWalletInfoResponse : " + responseStr);
        return Response.status(200).header("x-request-id", userWalletEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/getWebhookStripe")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getWebhookStripe(@HeaderParam("x-request-id") String requestId, WebhookCreateEnt whEnt) throws IOException {
        whEnt.setRequestId(requestId);
        logger.debug(requestId + " - getWebhookStripe : " + gson.toJson(whEnt));
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(whEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.getWebhookStripe(whEnt);
        } else {
            responseStr = payBusiness.getWebhookStripe(whEnt);
        }
        logger.debug(whEnt.requestId + " - getWebhookStripeResponse : " + " - " + responseStr);
        return Response.status(200).header("x-request-id", whEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/getWithdrawal")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getWithdrawal(@HeaderParam("x-request-id") String requestId, WalletWithdrawalEnt walletWithdrawalEnt) throws IOException {
        walletWithdrawalEnt.setRequestId(requestId);
        logger.debug(walletWithdrawalEnt.requestId + " - getWithdrawal : " + gson.toJson(walletWithdrawalEnt));
        ObjResponse getWithdrawalResponse = new ObjResponse();

        int returnCode = walletWithdrawalEnt.validateData();
        if (returnCode != 200) {
            getWithdrawalResponse.returnCode = returnCode;
            logger.debug(walletWithdrawalEnt.requestId + " - getWithdrawalResponse : " + getWithdrawalResponse.toString());
            return Response.status(200).header("x-request-id", walletWithdrawalEnt.requestId).entity(getWithdrawalResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(walletWithdrawalEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.getWithdrawal(walletWithdrawalEnt);
        } else {
            responseStr = payBusiness.getWithdrawal(walletWithdrawalEnt);
        }
        logger.debug(walletWithdrawalEnt.requestId + " - getWithdrawalResponse : " + responseStr);
        return Response.status(200).header("x-request-id", walletWithdrawalEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/markAsChargeBillingReport")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response markAsChargeBillingReport(@HeaderParam("x-request-id") String requestId, FleetSubscriptionEnt fleetSubscriptionEnt) throws IOException {
        fleetSubscriptionEnt.setRequestId(requestId);
        logger.debug("markAsChargeBillingReport : " + gson.toJson(fleetSubscriptionEnt));
        String responseStr = "";

        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(fleetSubscriptionEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.markAsChargeBillingReport(fleetSubscriptionEnt);
        } else {
            responseStr = payBusiness.markAsChargeBillingReport(fleetSubscriptionEnt);
        }
        logger.debug("updateBillingReportResponse : " + fleetSubscriptionEnt.requestId + " - " + responseStr);
        return Response.status(200).header("x-request-id", fleetSubscriptionEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/migrateTicket")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response migrateTicket(@HeaderParam("x-request-id") String requestId, MigrateEnt migrateEnt) throws IOException {
        migrateEnt.setRequestId(requestId);
        logger.debug("migrateTicket : " + migrateEnt.requestId + " - " + gson.toJson(migrateEnt));
        String responseStr = "";

        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(migrateEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.migrateTicket(migrateEnt);
        } else {
            responseStr = payBusiness.migrateTicket(migrateEnt);
        }
        logger.debug("migrateTicketResponse : " + migrateEnt.requestId + " - " + responseStr);
        return Response.status(200).header("x-request-id", migrateEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/noShow3rd")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response noShow3rd(@HeaderParam("x-request-id") String requestId, CancelBookingEnt cancelEnt) throws IOException {
        cancelEnt.setRequestId(requestId);
        logger.debug(cancelEnt.requestId + " - noShow3rd : " + gson.toJson(cancelEnt));
        ObjResponse cancelResponse = new ObjResponse();

        int returnCode = cancelEnt.validateData();
        if (returnCode != 200) {
            cancelResponse.returnCode = returnCode;
            logger.debug(cancelEnt.requestId + " - noShow3rdResponse : " + cancelResponse.toString());
            return Response.status(200).header("x-request-id", cancelEnt.requestId).entity(cancelResponse.toString()).build();
        }

        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(cancelEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.noShow3rd(cancelEnt);
        } else {
            responseStr = payBusiness.noShow3rd(cancelEnt);
        }
        logger.debug(cancelEnt.requestId + " - noShow3rdResponse : " + responseStr);
        return Response.status(200).header("x-request-id", cancelEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/payByApplePay")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response payByApplePay(@HeaderParam("x-request-id") String requestId, PaymentEnt paymentEnt) throws IOException {
        paymentEnt.setRequestId(requestId);
        logger.debug(paymentEnt.requestId + " - payByApplePay : " + gson.toJson(paymentEnt));
        ObjResponse payByApplePayResponse = new ObjResponse();
        int returnCode = paymentEnt.validateData();
        if (returnCode != 200) {
            payByApplePayResponse.returnCode = returnCode;
            logger.debug(paymentEnt.requestId + " - payByApplePayResponse : " + payByApplePayResponse.toString());
            return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(payByApplePayResponse.toString()).build();
        }

        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(paymentEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.payByApplePay(paymentEnt);
        } else {
            responseStr = payBusiness.payByApplePay(paymentEnt);
        }
        logger.debug(paymentEnt.requestId + " - payByApplePayResponse : " + responseStr);

        return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }

    @Path("api/paymentgateway/payByCash")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response payByCash(@HeaderParam("x-request-id") String requestId, PaymentEnt paymentEnt) throws IOException {
        paymentEnt.setRequestId(requestId);
        logger.debug(paymentEnt.requestId + " - payByCash : " + gson.toJson(paymentEnt));
        ObjResponse objResponse = new ObjResponse();
        // convert paymentType
        String paymentType = "";
        switch (paymentEnt.paymentType) {
            case "0":
                paymentType = KeysUtil.PAYMENT_CASH;
                break;
            case "16":
                paymentType = KeysUtil.PAYMENT_CASH_SENDER;
                break;
            case "17":
                paymentType = KeysUtil.PAYMENT_CASH_RECIPIENT;
                break;
        }
        int returnCode = paymentEnt.validateData();
        if (returnCode != 200 || paymentType.isEmpty()) {
            objResponse.returnCode = 406;
            logger.debug(paymentEnt.requestId + " - payByCashResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(objResponse.toString()).build();
        }
        paymentEnt.paymentType = paymentType;
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(paymentEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.payByCash(paymentEnt);
        } else {
            responseStr = payBusiness.payByCash(paymentEnt);
        }

        logger.debug(paymentEnt.requestId + " - payByCashResponse : " + responseStr);

        return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }

    @Path("api/paymentgateway/payByCreditExternal")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response payByCreditExternal(@HeaderParam("x-request-id") String requestId, PaymentEnt paymentEnt) throws IOException {
        paymentEnt.setRequestId(requestId);
        logger.debug(paymentEnt.requestId + " - payByCreditExternal : " + gson.toJson(paymentEnt));
        ObjResponse payByCreditResponse = new ObjResponse();
        int returnCode = paymentEnt.validateData();
        if (returnCode != 200) {
            payByCreditResponse.returnCode = returnCode;
            logger.debug(paymentEnt.requestId + " - payByCreditExternalResponse : " + payByCreditResponse.toString());
            return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(payByCreditResponse.toString()).build();
        }
        paymentEnt.paymentType = KeysUtil.PAYMENT_CARD_EXTERNAL;
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(paymentEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.payByCreditExternal(paymentEnt);
        } else {
            responseStr = payBusiness.payByCreditExternal(paymentEnt);
        }
        logger.debug(paymentEnt.requestId + " - payByCreditExternalResponse : " + responseStr);
        return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/payByCreditToken")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response payByCreditToken(@HeaderParam("x-request-id") String requestId, PaymentEnt paymentEnt) throws IOException {
        paymentEnt.setRequestId(requestId);
        logger.debug(paymentEnt.requestId + " - payByCreditToken : " + gson.toJson(paymentEnt));
        ObjResponse payByCreditResponse = new ObjResponse();
        int returnCode = paymentEnt.validateData();
        if (returnCode != 200) {
            payByCreditResponse.returnCode = returnCode;
            logger.debug(paymentEnt.requestId + " - payByCreditTokenResponse : " + payByCreditResponse.toString());
            return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(payByCreditResponse.toString()).build();
        }

        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(paymentEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.payByCreditToken(paymentEnt);
        } else {
            responseStr = payBusiness.payByCreditToken(paymentEnt);
        }
        logger.debug(paymentEnt.requestId + " - payByCreditTokenResponse : " + responseStr);
        return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/payByDirectBilling")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response payByDirectBilling(@HeaderParam("x-request-id") String requestId, PaymentEnt paymentEnt) throws IOException {
        paymentEnt.setRequestId(requestId);
        logger.debug(paymentEnt.requestId + " - payByDirectBilling : " + gson.toJson(paymentEnt));
        ObjResponse payByCreditResponse = new ObjResponse();
        int returnCode = paymentEnt.validateData();
        if (returnCode != 200) {
            payByCreditResponse.returnCode = returnCode;
            logger.debug(paymentEnt.requestId + " - payByDirectBillingResponse : " + payByCreditResponse.toString());
            return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(payByCreditResponse.toString()).build();
        }
        paymentEnt.paymentType = KeysUtil.PAYMENT_DIRECTBILLING;

        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(paymentEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.payByDirectBilling(paymentEnt);
        } else {
            responseStr = payBusiness.payByDirectBilling(paymentEnt);
        }
        logger.debug(paymentEnt.requestId + " - payByDirectBillingResponse : " + responseStr);
        return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/payByFleetCard")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response payByFleetCard(@HeaderParam("x-request-id") String requestId, PaymentEnt paymentEnt) throws IOException {
        paymentEnt.setRequestId(requestId);
        logger.debug(paymentEnt.requestId + " - payByFleetCard : " + gson.toJson(paymentEnt));
        ObjResponse payByCreditResponse = new ObjResponse();
        int returnCode = paymentEnt.validateData();
        if (returnCode != 200) {
            payByCreditResponse.returnCode = returnCode;
            logger.debug(paymentEnt.requestId + " - payByFleetCardResponse : " + payByCreditResponse.toString());
            return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(payByCreditResponse.toString()).build();
        }
        paymentEnt.paymentType = KeysUtil.PAYMENT_FLEETCARD;
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(paymentEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.payByFleetCard(paymentEnt);
        } else {
            responseStr = payBusiness.payByFleetCard(paymentEnt);
        }
        logger.debug(paymentEnt.requestId + " - payByFleetCardResponse : " + responseStr);
        return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/payByInputCard")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response payByInputCard(@HeaderParam("x-request-id") String requestId, PaymentEnt paymentEnt) throws IOException {
        paymentEnt.setRequestId(requestId);
        logger.debug(paymentEnt.requestId + " - payByInputCard : " + paymentEnt.toString());
        ObjResponse payByCreditResponse = new ObjResponse();
        int returnCode = paymentEnt.validateCredit();
        if (returnCode != 200) {
            payByCreditResponse.returnCode = returnCode;
            logger.debug(paymentEnt.requestId + " - payByInputCardResponse : " + payByCreditResponse.toString());
            return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(payByCreditResponse.toString()).build();
        }

        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(paymentEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.payByInputCard(paymentEnt);
        } else {
            responseStr = payBusiness.payByInputCard(paymentEnt);
        }
        logger.debug(paymentEnt.requestId + " - payByInputCardResponse : " + responseStr);
        return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/payByPaxWallet")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response payByPaxWallet(@HeaderParam("x-request-id") String requestId, PaymentEnt paymentEnt) throws IOException {
        paymentEnt.setRequestId(requestId);
        logger.debug(paymentEnt.requestId + " - payByPaxWallet : " + gson.toJson(paymentEnt));
        ObjResponse payByCreditResponse = new ObjResponse();
        int returnCode = paymentEnt.validateData();
        if (returnCode != 200) {
            payByCreditResponse.returnCode = returnCode;
            logger.debug(paymentEnt.requestId + " - payByPaxWalletResponse : " + payByCreditResponse.toString());
            return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(payByCreditResponse.toString()).build();
        }
        paymentEnt.paymentType = KeysUtil.PAYMENT_PAXWALLET;
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(paymentEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.payByPaxWallet(paymentEnt);
        } else {
            responseStr = payBusiness.payByPaxWallet(paymentEnt);
        }
        logger.debug(paymentEnt.requestId + " - payByPaxWalletResponse : " + responseStr);

        return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }

    @Path("api/paymentgateway/payByPrePaid")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response payByPrePaid(@HeaderParam("x-request-id") String requestId, PaymentEnt paymentEnt) throws IOException {
        paymentEnt.setRequestId(requestId);
        logger.debug(paymentEnt.requestId + " - payByPrePaid : " + gson.toJson(paymentEnt));
        ObjResponse payByPrePaidResponse = new ObjResponse();
        int returnCode = paymentEnt.validateData();
        if (returnCode != 200) {
            payByPrePaidResponse.returnCode = returnCode;
            logger.debug(paymentEnt.requestId + " - payByPrePaidResponse : " + payByPrePaidResponse.toString());
            return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(payByPrePaidResponse.toString()).build();
        }
        paymentEnt.paymentType = KeysUtil.PAYMENT_PREPAID;
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(paymentEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.payByPrePaid(paymentEnt);
        } else {
            responseStr = payBusiness.payByPrePaid(paymentEnt);
        }
        logger.debug(paymentEnt.requestId + " - payByPrePaidResponse : " + responseStr);
        return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/payByWallet")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response payByWallet(@HeaderParam("x-request-id") String requestId, PaymentEnt paymentEnt) throws IOException {
        paymentEnt.setRequestId(requestId);
        logger.debug(paymentEnt.requestId + " - payByWallet : " + gson.toJson(paymentEnt));
        ObjResponse payByWalletResponse = new ObjResponse();
        int returnCode = paymentEnt.validateData();
        if (returnCode != 200) {
            payByWalletResponse.returnCode = returnCode;
            logger.debug(paymentEnt.requestId + " - payByWalletResponse : " + payByWalletResponse.toString());
            return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(payByWalletResponse.toString()).build();
        }

        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(paymentEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.payByWallet(paymentEnt);
        } else {
            responseStr = payBusiness.payByWallet(paymentEnt);
        }

        logger.debug(paymentEnt.requestId + " - payByWalletResponse : " + responseStr);

        return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }

    @Path("api/paymentgateway/payfortDirectTransaction")
    @PermitAll
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response payfortDirectTransaction(@HeaderParam("x-request-id") String requestId, PayfortResponseEnt payfortResponseEnt) throws IOException {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        logger.debug(requestId + " - payfortDirectTransaction : " + gson.toJson(payfortResponseEnt));
        String fleetId = proxyUtil.completePayfort3DSTransaction(payfortResponseEnt, requestId);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.payfortDirectTransaction(payfortResponseEnt, requestId);
        } else {
            responseStr = payBusiness.payfortDirectTransaction(payfortResponseEnt, requestId);
        }
        logger.debug(requestId + " - payfortDirectTransactionResponse : " + responseStr);
        return Response.status(200).entity(responseStr).build();
    }

    @Path("api/paymentgateway/payFrom3rd")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response payFrom3rd(@HeaderParam("x-request-id") String requestId, String request) throws IOException {
        PaymentEnt paymentEnt = gson.fromJson(request, PaymentEnt.class);
        paymentEnt.setRequestId(requestId);
        logger.debug(paymentEnt.requestId + " - payFrom3rd : " + gson.toJson(paymentEnt));
        ObjResponse payResponse = new ObjResponse();
        int returnCode = paymentEnt.validateData();
        if (returnCode != 200) {
            payResponse.returnCode = returnCode;
            logger.debug(paymentEnt.requestId + " - payFrom3rdResponse : " + payResponse.toString());
            return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(payResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(paymentEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.payFrom3rd(paymentEnt);
        } else {
            responseStr = payBusiness.payFrom3rd(paymentEnt);
        }
        logger.debug(paymentEnt.requestId + " - payFrom3rdResponse : " + responseStr);
        return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/payFromCommandCenter")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response payFromCommandCenter(@HeaderParam("x-request-id") String requestId, String request) throws IOException {
        PaymentEnt paymentEnt = gson.fromJson(request, PaymentEnt.class);
        paymentEnt.setRequestId(requestId);
        logger.debug(paymentEnt.requestId + " - payFromCommandCenter : " + gson.toJson(paymentEnt));
        ObjResponse payResponse = new ObjResponse();
        int returnCode = paymentEnt.validateData();
        if (returnCode != 200) {
            payResponse.returnCode = returnCode;
            logger.debug(paymentEnt.requestId + " - payFromCommandCenterResponse : " + payResponse.toString());
            return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(payResponse.toString()).build();
        }

        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(paymentEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.payFromCommandCenter(paymentEnt);
        } else {
            responseStr = payBusiness.payFromCommandCenter(paymentEnt);
        }
        logger.debug(paymentEnt.requestId + " - payFromCommandCenterResponse : " + responseStr);
        return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/paymentAudit")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response paymentAudit(@HeaderParam("x-request-id") String requestId, PaymentAudit paymentAudit) throws IOException {
        paymentAudit.setRequestId(requestId);
        logger.debug(paymentAudit.requestId + " - paymentAudit : " + "from " + paymentAudit.from + " - to " + paymentAudit.to);
        String responseStr = paymentRPC.paymentAudit(paymentAudit); // use queue as default
        logger.debug(paymentAudit.requestId + " - paymentAuditResponse : " + responseStr);
        return Response.status(200).header("x-request-id", paymentAudit.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/paymentDetail/{bookId}")
    @RolesAllowed(DIS_ROLE)
    @GET
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response paymentDetail(@HeaderParam("x-request-id") String requestId, @PathParam("bookId") String bookId) throws IOException {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        logger.debug(requestId + " - paymentDetail - bookId:" + bookId);

        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(null); // Jupiter
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.paymentDetail(bookId, requestId);
        } else {
            responseStr = payBusiness.paymentDetail(bookId, requestId);
        }
        logger.debug(requestId + " - paymentDetailResponse : " + responseStr);
        return Response.status(200).header("x-request-id", requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/paymentDetail/{fleetId}/{bookId}")
    @RolesAllowed(DIS_ROLE)
    @GET
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response paymentDetail(@HeaderParam("x-request-id") String requestId, @PathParam("fleetId") String fleetId, @PathParam("bookId") String bookId) throws IOException {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        logger.debug(requestId + " - paymentDetail - bookId:" + bookId);

        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.paymentDetail(bookId, requestId);
        } else {
            responseStr = payBusiness.paymentDetail(bookId, requestId);
        }
        logger.debug(requestId + " - paymentDetailResponse : " + responseStr);
        return Response.status(200).header("x-request-id", requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/paymentGCashNotificationURL")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response paymentGCashNotificationURL(@HeaderParam("x-request-id") String requestId, TnGNotification tnGNotification) throws IOException {
        tnGNotification.setRequestId(requestId);
        logger.debug("paymentGCashNotificationURL : " + gson.toJson(tnGNotification));
        String fleetId = proxyUtil.paymentGCashNotificationURL(tnGNotification, requestId);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.paymentGCashNotificationURL(tnGNotification);
        } else {
            responseStr = payBusiness.paymentGCashNotificationURL(tnGNotification);
        }
        logger.debug("paymentGCashNotificationURLResponse : " + tnGNotification.requestId + " - " + responseStr);
        return Response.status(200).header("x-request-id", tnGNotification.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/paymentMolPayNotificationURL")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response paymentMolPayNotificationURL(@HeaderParam("x-request-id") String requestId, PaymentNotificationEnt paymentNotificationEnt) throws IOException {
        paymentNotificationEnt.setRequestId(requestId);
        logger.debug("paymentMolPayNotificationURL : " + gson.toJson(paymentNotificationEnt));
        String fleetId = proxyUtil.paymentMolPayNotificationURL(paymentNotificationEnt, requestId);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.paymentMolPayNotificationURL(paymentNotificationEnt);
        } else {
            responseStr = payBusiness.paymentMolPayNotificationURL(paymentNotificationEnt);
        }
        logger.debug("paymentMolPayNotificationURLResponse : " + paymentNotificationEnt.requestId + " - " + responseStr);
        return Response.status(200).header("x-request-id", paymentNotificationEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/paymentNotificationURL")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response paymentNotification(@HeaderParam("x-request-id") String requestId, PaymentNotificationEnt paymentNotificationEnt) throws IOException {
        paymentNotificationEnt.setRequestId(requestId);
        logger.debug("paymentNotificationURL : " + gson.toJson(paymentNotificationEnt));
        String fleetId = proxyUtil.paymentNotification(paymentNotificationEnt, requestId);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.paymentNotification(paymentNotificationEnt);
        } else {
            responseStr = payBusiness.paymentNotification(paymentNotificationEnt);
        }
        logger.debug("paymentNotificationURLResponse : " + paymentNotificationEnt.requestId + " - " + responseStr);
        return Response.status(200).header("x-request-id", paymentNotificationEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/paymentPingPongNotificationURL")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response paymentPingPongNotificationURL(@HeaderParam("x-request-id") String requestId, PaymentNotificationEnt paymentNotificationEnt) throws IOException {
        paymentNotificationEnt.setRequestId(requestId);
        logger.debug("paymentPingPongNotificationURL : " + gson.toJson(paymentNotificationEnt));
        String fleetId = proxyUtil.paymentPingPongNotificationURL(paymentNotificationEnt, requestId);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.paymentPingPongNotificationURL(paymentNotificationEnt);
        } else {
            responseStr = payBusiness.paymentPingPongNotificationURL(paymentNotificationEnt);
        }
        logger.debug("paymentPingPongNotificationURLResponse : " + paymentNotificationEnt.requestId + " - " + responseStr);
        return Response.status(200).header("x-request-id", paymentNotificationEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/paymentSenangpayNotificationURL")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response paymentSenangpayNotificationURL(@HeaderParam("x-request-id") String requestId, PaymentNotificationEnt paymentNotificationEnt) throws IOException {
        paymentNotificationEnt.setRequestId(requestId);
        logger.debug("paymentSenangpayNotificationURL : " + gson.toJson(paymentNotificationEnt));
        String fleetId = proxyUtil.paymentSenangpayNotificationURL(paymentNotificationEnt, requestId);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.paymentSenangpayNotificationURL(paymentNotificationEnt);
        } else {
            responseStr = payBusiness.paymentSenangpayNotificationURL(paymentNotificationEnt);
        }
        logger.debug("paymentSenangpayNotificationURLResponse : " + paymentNotificationEnt.requestId + " - " + responseStr);
        return Response.status(200).header("x-request-id", paymentNotificationEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/paymentTnGNotificationURL")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response paymentTnGNotificationURL(@HeaderParam("x-request-id") String requestId, TnGNotification tnGNotification) throws IOException {
        tnGNotification.setRequestId(requestId);
        logger.debug("paymentTnGNotificationURL : " + gson.toJson(tnGNotification));
        String fleetId = proxyUtil.paymentTnGNotificationURL(tnGNotification, requestId);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.paymentTnGNotificationURL(tnGNotification);
        } else {
            responseStr = payBusiness.paymentTnGNotificationURL(tnGNotification);
        }
        logger.debug("paymentTnGNotificationURLResponse : " + tnGNotification.requestId + " - " + responseStr);
        return Response.status(200).header("x-request-id", tnGNotification.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/payOutstanding")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response payOutstanding(@HeaderParam("x-request-id") String requestId, OutstandingEnt outstandingEnt) throws IOException {
        outstandingEnt.setRequestId(requestId);
        logger.debug(outstandingEnt.requestId + " - payOutstanding : " + gson.toJson(outstandingEnt));
        ObjResponse objResponse = new ObjResponse();
        int returnCode = outstandingEnt.validatePay();
        if (returnCode != 200) {
            objResponse.returnCode = returnCode;
            logger.debug(outstandingEnt.requestId + " - payOutstandingResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", outstandingEnt.requestId).header("x-request-id", outstandingEnt.requestId).entity(objResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(outstandingEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.payOutstanding(outstandingEnt);
        } else {
            responseStr = payBusiness.payOutstanding(outstandingEnt);
        }
        logger.debug(outstandingEnt.requestId + " - payOutstandingResponse : " + responseStr);
        return Response.status(200).header("x-request-id", outstandingEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/payToDriver")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response payToDriver(@HeaderParam("x-request-id") String requestId, PayToDriverEnt payToDriverEnt) throws IOException {
        payToDriverEnt.setRequestId(requestId);
        logger.debug(payToDriverEnt.requestId + " - payToDriver : " + gson.toJson(payToDriverEnt));
        ObjResponse objResponse = new ObjResponse();
        int returnCode = payToDriverEnt.validateData();
        if (returnCode != 200) {
            objResponse.returnCode = returnCode;
            logger.debug(payToDriverEnt.requestId + " - payToDriverResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", payToDriverEnt.requestId).entity(objResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(payToDriverEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.payToDriver(payToDriverEnt);
        } else {
            responseStr = payBusiness.payToDriver(payToDriverEnt);
        }
        logger.debug(payToDriverEnt.requestId + " - payToDriverResponse : " + responseStr);
        return Response.status(200).header("x-request-id", payToDriverEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/preAuthPayment")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response preAuthPayment(@HeaderParam("x-request-id") String requestId, PreAuthEnt preAuthEnt) throws IOException {
        preAuthEnt.setRequestId(requestId);
        logger.debug(preAuthEnt.requestId + " - preAuthPayment : " + gson.toJson(preAuthEnt));

        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(preAuthEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.preAuthPayment(preAuthEnt);
        } else {
            responseStr = payBusiness.preAuthPayment(preAuthEnt);
        }
        Response response = Response.status(200).header("x-request-id", preAuthEnt.requestId).entity(responseStr).build();
        logger.debug(preAuthEnt.requestId + " - preAuthPaymentResponse : " + responseStr);
        return response;
    }

    @Path("api/paymentgateway/preChargeBooking")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response preChargeBooking(@HeaderParam("x-request-id") String requestId, HydraBookingEnt hydraBookingEnt) throws IOException {
        hydraBookingEnt.setRequestId(requestId);
        logger.debug(hydraBookingEnt.requestId + " - preChargeBooking : " + gson.toJson(hydraBookingEnt));
        ObjResponse payByCreditResponse = new ObjResponse();
        String bookId = hydraBookingEnt.bookId != null ? hydraBookingEnt.bookId : "";
        if (bookId.isEmpty()) {
            payByCreditResponse.returnCode = 406;
            logger.debug(hydraBookingEnt.requestId + " - preChargeBookingResponse : " + payByCreditResponse.toString());
            return Response.status(200).header("x-request-id", hydraBookingEnt.requestId).entity(payByCreditResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(hydraBookingEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.preChargeBooking(hydraBookingEnt);
        } else {
            responseStr = payBusiness.preChargeBooking(hydraBookingEnt);
        }
        logger.debug(hydraBookingEnt.requestId + " - preChargeBookingResponse : " + responseStr);

        return Response.status(200).header("x-request-id", hydraBookingEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }

    @Path("api/paymentgateway/rejectWithdrawal")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response rejectWithdrawal(@HeaderParam("x-request-id") String requestId, WalletTransferEnt walletTransferEnt) throws IOException {
        walletTransferEnt.setRequestId(requestId);
        logger.debug(walletTransferEnt.requestId + " - rejectWithdrawal : " + gson.toJson(walletTransferEnt));
        ObjResponse rejectResponse = new ObjResponse();

        int returnCode = walletTransferEnt.validateDataHandleRequest();
        if (returnCode != 200) {
            rejectResponse.returnCode = returnCode;
            logger.debug(walletTransferEnt.requestId + " - rejectWithdrawalResponse : " + rejectResponse.toString());
            return Response.status(200).header("x-request-id", walletTransferEnt.requestId).entity(rejectResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(walletTransferEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.rejectWithdrawal(walletTransferEnt, KeysUtil.WITHDRAWAL_REJECTED);
        } else {
            responseStr = payBusiness.rejectWithdrawal(walletTransferEnt, KeysUtil.WITHDRAWAL_REJECTED);
        }
        logger.debug(walletTransferEnt.requestId + " - rejectWithdrawalResponse : " + responseStr);
        return Response.status(200).header("x-request-id", walletTransferEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/removeBankInfo")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response removeBankInfo(@HeaderParam("x-request-id") String requestId, ACHEnt achEnt) throws IOException {
        achEnt.setRequestId(requestId);
        logger.debug(achEnt.requestId + " - removeBankInfo : " + achEnt.driverId);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(achEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.removeBankInfo(achEnt);
        } else {
            responseStr = payBusiness.removeBankInfo(achEnt);
        }
        logger.debug(achEnt.requestId + " - removeBankInfoResponse : " + responseStr);
        return Response.status(200).header("x-request-id", achEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/reportSettlementHistory")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response reportSettlementHistory(@HeaderParam("x-request-id") String requestId, ReportSettlementEnt reportSettlementEnt) throws IOException {
        reportSettlementEnt.setRequestId(requestId);
        logger.debug(reportSettlementEnt.requestId + " - reportSettlementHistory : " + gson.toJson(reportSettlementEnt));
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(reportSettlementEnt.fleetId);
        if (settingMQData.payService) {
            responseStr = paymentRPC.reportSettlementHistory(reportSettlementEnt);
        } else {
            responseStr = payService.reportSettlementHistory(reportSettlementEnt);
        }
        logger.debug(reportSettlementEnt.requestId + " - reportSettlementHistoryResponse : " + responseStr);
        return Response.status(200).header("x-request-id", reportSettlementEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/retryPayment")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response retryPayment(@HeaderParam("x-request-id") String requestId, PaymentEnt paymentEnt) throws IOException {
        paymentEnt.setRequestId(requestId);
        logger.debug(paymentEnt.requestId + " - retryPayment : " + gson.toJson(paymentEnt));
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(paymentEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.retryPayment(paymentEnt);
        } else {
            responseStr = payBusiness.retryPayment(paymentEnt);
        }

        logger.debug(paymentEnt.requestId + " - retryPaymentResponse : " + responseStr);
        return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/saveSettlementHistory")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response saveSettlementHistory(@HeaderParam("x-request-id") String requestId, SettlementHistoryEnt settlementHistoryEnt) throws IOException {
        settlementHistoryEnt.setRequestId(requestId);
        logger.debug(settlementHistoryEnt.requestId + " - saveSettlementHistory : " + gson.toJson(settlementHistoryEnt));
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(settlementHistoryEnt.fleetId);
        if (settingMQData.payService) {
            responseStr = paymentRPC.saveSettlementHistory(settlementHistoryEnt);
        } else {
            responseStr = payService.saveSettlementHistory(settlementHistoryEnt);
        }
        logger.debug(settlementHistoryEnt.requestId + " - saveSettlementHistoryResponse : " + responseStr);
        return Response.status(200).header("x-request-id", settlementHistoryEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/setDefaultCardForFleetOwner")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response setDefaultCardForFleetOwner(@HeaderParam("x-request-id") String requestId, CreditDeleteEnt deleteCreditForm) throws IOException {
        deleteCreditForm.setRequestId(requestId);
        logger.debug(deleteCreditForm.requestId + " - setDefaultCardForFleetOwner : " + gson.toJson(deleteCreditForm));
        ObjResponse deleteCardResponse = new ObjResponse();
        int returnCode = deleteCreditForm.validateData();
        if (returnCode != 200) {
            deleteCardResponse.returnCode = returnCode;
            logger.debug(deleteCreditForm.requestId + " - setDefaultCardForFleetOwnerResponse : " + deleteCardResponse.toString());
            return Response.status(200).header("x-request-id", deleteCreditForm.requestId).entity(deleteCardResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(deleteCreditForm.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.setDefaultCardForFleetOwner(deleteCreditForm);
        } else {
            responseStr = payBusiness.setDefaultCardForFleetOwner(deleteCreditForm);
        }
        logger.debug(deleteCreditForm.requestId + " - setDefaultCardForFleetOwnerResponse : " + responseStr);
        return Response.status(200).header("x-request-id", deleteCreditForm.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/settlementMolPayNotificationURL")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response settlementMolPayNotificationURL(@HeaderParam("x-request-id") String requestId, SettlementNotificationEnt settlementNotificationEnt) throws IOException {
        settlementNotificationEnt.setRequestId(requestId);
        logger.debug("settlementMolPayNotificationURL : " + gson.toJson(settlementNotificationEnt));
        String fleetId = proxyUtil.settlementMolPayNotificationURL(settlementNotificationEnt, requestId);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.settlementMolPayNotificationURL(settlementNotificationEnt);
        } else {
            responseStr = payBusiness.settlementMolPayNotificationURL(settlementNotificationEnt);
        }
        logger.debug("settlementMolPayNotificationURLResponse : " + settlementNotificationEnt.requestId + " - " + responseStr);
        return Response.status(200).header("x-request-id", settlementNotificationEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/storePaymentData")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response storePaymentData(@HeaderParam("x-request-id") String requestId, PaymentEnt paymentEnt) throws IOException {
        paymentEnt.setRequestId(requestId);
        logger.debug(paymentEnt.requestId + " - storePaymentData : " + gson.toJson(paymentEnt));
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(paymentEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.storePaymentData(paymentEnt);
        } else {
            responseStr = payBusiness.storePaymentData(paymentEnt);
        }
        logger.debug(paymentEnt.requestId + " - storePaymentDataResponse : " + responseStr);
        return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }

    @Path("api/paymentgateway/switchToBBLCorporate")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response switchToBBLCorporate(@HeaderParam("x-request-id") String requestId, BBLCorpEnt bblCorpEnt) throws IOException {
        bblCorpEnt.setRequestId(requestId);
        logger.debug("switchToBBLCorporate : " + gson.toJson(bblCorpEnt));
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(bblCorpEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.switchToBBLCorporate(bblCorpEnt.fleetId, bblCorpEnt.userId, requestId);
        } else {
            responseStr = payBusiness.switchToBBLCorporate(bblCorpEnt.fleetId, bblCorpEnt.userId, requestId);
        }
        logger.debug("switchToBBLCorporateResponse : " + bblCorpEnt.requestId + " - " + responseStr);
        return Response.status(200).header("x-request-id", bblCorpEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/synWebhookStripe")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response synWebhookStripe(@HeaderParam("x-request-id") String requestId, WebhookCreateEnt whEnt) throws IOException {
        whEnt.setRequestId(requestId);
        logger.debug(whEnt.requestId + " - synWebhookStripe : " + gson.toJson(whEnt));
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(whEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.synWebhookStripe(whEnt);
        } else {
            responseStr = payBusiness.synWebhookStripe(whEnt);
        }
        logger.debug(whEnt.requestId + " - synWebhookStripeResponse : " + " - " + responseStr);
        return Response.status(200).header("x-request-id", whEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/ticketDetail/{bookId}")
    @RolesAllowed(DIS_ROLE)
    @GET
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getTicketDetail(@HeaderParam("x-request-id") String requestId, @PathParam("bookId") String bookId, @QueryParam("appType") String appType) throws IOException {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        logger.debug(requestId + " - ticketDetail - bookId:" + bookId + " - appType: " + appType);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(null); // Jupiter
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.ticketDetail(requestId, bookId, appType);
        } else {
            responseStr = payBusiness.ticketDetail(requestId, bookId, appType);
        }
        logger.debug(requestId + " - ticketDetailResponse : " + responseStr);
        return Response.status(200).header("x-request-id", requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/ticketDetail/{fleetId}/{bookId}")
    @RolesAllowed(DIS_ROLE)
    @GET
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response ticketDetail(@HeaderParam("x-request-id") String requestId, @PathParam("fleetId") String fleetId, @PathParam("bookId") String bookId, @QueryParam("appType") String appType) throws IOException {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        logger.debug(requestId + " - ticketDetail - bookId:" + bookId + " - appType: " + appType);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.ticketDetail(requestId, bookId, appType);
        } else {
            responseStr = payBusiness.ticketDetail(requestId, bookId, appType);
        }
        logger.debug(requestId + " - ticketDetailResponse : " + responseStr);
        return Response.status(200).header("x-request-id", requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/tipAfterPayment")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response tipAfterPayment(@HeaderParam("x-request-id") String requestId, TipDriverEnt tipDriverEnt) throws IOException {
        tipDriverEnt.setRequestId(requestId);
        logger.debug(tipDriverEnt.requestId + " - tipAfterPayment : " + gson.toJson(tipDriverEnt));
        ObjResponse objResponse = new ObjResponse();
        int returnCode = tipDriverEnt.validateData();
        if (returnCode != 200) {
            objResponse.returnCode = returnCode;
            logger.debug(tipDriverEnt.requestId + " - tipAfterPaymentResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", tipDriverEnt.requestId).header("x-request-id", tipDriverEnt.requestId).entity(objResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(tipDriverEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.tipAfterPayment(tipDriverEnt);
        } else {
            responseStr = payBusiness.tipAfterPayment(tipDriverEnt);
        }

        logger.debug(tipDriverEnt.requestId + " - tipAfterPaymentResponse : " + responseStr);
        return Response.status(200).header("x-request-id", tipDriverEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/topupByWallet")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response topupByWallet(@HeaderParam("x-request-id") String requestId, TopupByWalletEnt topupEnt) throws IOException {
        topupEnt.setRequestId(requestId);
        String currencyISO = topupEnt.currencyISO != null ? topupEnt.currencyISO : "";
        if (currencyISO.isEmpty()) {
            topupEnt.currencyISO = "MYR";
        }
        logger.debug(topupEnt.requestId + " - topupByWallet : " + gson.toJson(topupEnt));
        ObjResponse objResponse = new ObjResponse();
        int returnCode = topupEnt.validateData();
        if (returnCode != 200) {
            objResponse.returnCode = returnCode;
            logger.debug(topupEnt.requestId + " - topupByWalletResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", topupEnt.requestId).header("x-request-id", topupEnt.requestId).entity(objResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(topupEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.topupByWallet(topupEnt);
        } else {
            responseStr = payBusiness.topupByWallet(topupEnt);
        }
        logger.debug(topupEnt.requestId + " - topupByWalletResponse : " + responseStr);
        return Response.status(200).header("x-request-id", topupEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/topupDriverBalance")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response topupDriverBalance(@HeaderParam("x-request-id") String requestId, TopupDriverBalanceEnt topupEnt) throws IOException {
        topupEnt.setRequestId(requestId);
        String currencyISO = topupEnt.currencyISO != null ? topupEnt.currencyISO : "";
        if (currencyISO.isEmpty()) {
            topupEnt.currencyISO = "MYR";
        }
        logger.debug(topupEnt.requestId + " - topupDriverBalance : " + gson.toJson(topupEnt));
        ObjResponse objResponse = new ObjResponse();
        int returnCode = topupEnt.validateData();
        if (returnCode != 200) {
            objResponse.returnCode = returnCode;
            logger.debug(topupEnt.requestId + " - topupDriverBalanceResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", topupEnt.requestId).header("x-request-id", topupEnt.requestId).entity(objResponse.toString()).build();
        }
        topupEnt.from = "app";
        topupEnt.type = "topUp";
        topupEnt.reason = "";
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(topupEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.topupDriverBalance(topupEnt);
        } else {
            responseStr = payBusiness.topupDriverBalance(topupEnt);
        }
        logger.debug(topupEnt.requestId + " - topupDriverBalanceResponse : " + responseStr);
        return Response.status(200).header("x-request-id", topupEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/topupDriverBalanceFromCC")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response topupDriverBalanceFromCC(@HeaderParam("x-request-id") String requestId, TopupDriverBalanceEnt topupEnt) throws IOException {
        topupEnt.setRequestId(requestId);
        String currencyISO = topupEnt.currencyISO != null ? topupEnt.currencyISO : "";
        if (currencyISO.isEmpty()) {
            topupEnt.currencyISO = "MYR";
        }
        logger.debug(topupEnt.requestId + " - topupDriverBalanceFromCC : " + gson.toJson(topupEnt));
        ObjResponse objResponse = new ObjResponse();
        int returnCode = topupEnt.validateData();
        if (returnCode != 200) {
            objResponse.returnCode = returnCode;
            logger.debug(topupEnt.requestId + " - topupDriverBalanceFromCCResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", topupEnt.requestId).entity(objResponse.toString()).build();
        }
        topupEnt.from = "cc";
        topupEnt.type = "editBalance";
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(topupEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.topupDriverBalance(topupEnt);
        } else {
            responseStr = payBusiness.topupDriverBalance(topupEnt);
        }
        logger.debug(topupEnt.requestId + " - topupDriverBalanceFromCCResponse : " + responseStr);
        return Response.status(200).header("x-request-id", topupEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/topUpPaxWallet")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response topUpPaxWallet(@HeaderParam("x-request-id") String requestId, TopupPaxWalletEnt topupPaxWalletEnt) throws IOException {
        topupPaxWalletEnt.setRequestId(requestId);
        String currencyISO = topupPaxWalletEnt.currencyISO != null ? topupPaxWalletEnt.currencyISO : "";
        if (currencyISO.isEmpty()) {
            topupPaxWalletEnt.currencyISO = "MYR";
        }
        logger.debug(topupPaxWalletEnt.requestId + " - topUpPaxWallet : " + gson.toJson(topupPaxWalletEnt));
        ObjResponse objResponse = new ObjResponse();
        int returnCode = topupPaxWalletEnt.validateData();
        if (returnCode != 200) {
            objResponse.returnCode = returnCode;
            logger.debug(topupPaxWalletEnt.requestId + " - topUpPaxWalletResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", topupPaxWalletEnt.requestId).header("x-request-id", topupPaxWalletEnt.requestId).entity(objResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(topupPaxWalletEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.topUpPaxWallet(topupPaxWalletEnt);
        } else {
            responseStr = payBusiness.topUpPaxWallet(topupPaxWalletEnt);
        }
        logger.debug(topupPaxWalletEnt.requestId + " - topUpPaxWalletResponse : " + responseStr);
        return Response.status(200).header("x-request-id", topupPaxWalletEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/topUpPaxWalletFromCC")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response topUpPaxWalletFromCC(@HeaderParam("x-request-id") String requestId, TopupPaxWalletEnt topupPaxWalletEnt) throws IOException {
        topupPaxWalletEnt.setRequestId(requestId);
        logger.debug(topupPaxWalletEnt.requestId + " - topUpPaxWalletFromCC : " + gson.toJson(topupPaxWalletEnt));
        ObjResponse objResponse = new ObjResponse();
        int returnCode = topupPaxWalletEnt.validateData();
        if (returnCode != 200) {
            objResponse.returnCode = returnCode;
            logger.debug(topupPaxWalletEnt.requestId + " - topUpPaxWalletFromCCResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", topupPaxWalletEnt.requestId).header("x-request-id", topupPaxWalletEnt.requestId).entity(objResponse.toString()).build();
        }
        topupPaxWalletEnt.from = "cc";
        topupPaxWalletEnt.type = "editBalance";
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(topupPaxWalletEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.topUpPaxWallet(topupPaxWalletEnt);
        } else {
            responseStr = payBusiness.topUpPaxWallet(topupPaxWalletEnt);
        }
        logger.debug(topupPaxWalletEnt.requestId + " - topUpPaxWalletFromCCResponse : " + responseStr);
        return Response.status(200).header("x-request-id", topupPaxWalletEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/topupPrepaid")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response topupPrepaid(@HeaderParam("x-request-id") String requestId, TopupEnt topupEnt) throws IOException {
        topupEnt.setRequestId(requestId);
        logger.debug(topupEnt.requestId + " - topupPrepaid : " + gson.toJson(topupEnt));
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(topupEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.topupPrepaid(topupEnt);
        } else {
            responseStr = payBusiness.topupPrepaid(topupEnt);
        }
        logger.debug(topupEnt.requestId + " - topupPrepaidResponse : " + responseStr);
        return Response.status(200).header("x-request-id", topupEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/topUpRefereeWallet")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response topUpRefereeWallet(@HeaderParam("x-request-id") String requestId, TopupPaxWalletEnt topupEnt) throws IOException {
        topupEnt.setRequestId(requestId);
        logger.debug(topupEnt.requestId + " - topUpRefereeWallet : " + gson.toJson(topupEnt));
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(topupEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.topUpRefereeWallet(topupEnt);
        } else {
            responseStr = payBusiness.topUpRefereeWallet(topupEnt);
        }
        logger.debug(topupEnt.requestId + " - topUpRefereeWalletResponse : " + responseStr);
        return Response.status(200).header("x-request-id", topupEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/unlockBooking")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response unlockBooking(@HeaderParam("x-request-id") String requestId, CancelBookingEnt cancelBookingEnt) throws IOException {
        cancelBookingEnt.setRequestId(requestId);
        logger.debug("unlockBooking for bookId " + cancelBookingEnt.bookId);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(cancelBookingEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.unlockBooking(cancelBookingEnt);
        } else {
            responseStr = payBusiness.unlockBooking(cancelBookingEnt);
        }
        logger.debug("unlockBookingResponse : " + cancelBookingEnt.requestId + " - " + responseStr);
        return Response.status(200).header("x-request-id", cancelBookingEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/updateAffiliateRate/{priceType}/{value}")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @GET
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response updateAffiliateRate(@HeaderParam("x-request-id") String requestId, @PathParam("value") int value,
                                        @PathParam("priceType") String priceType) throws IOException {
        logger.debug("updateAffiliateRate - value : " + value);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(null); // PayService
        if (settingMQData.payService) {
            responseStr = paymentRPC.updateAffiliateRate(requestId, value, priceType);
        } else {
            responseStr = payService.updateAffiliateRate(requestId, value, priceType);
        }
        logger.debug("updateAffiliateRateResponse : " + responseStr);
        return Response.status(200).header("x-request-id", requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/updateBankProfile")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response updateBankProfile(@HeaderParam("x-request-id") String requestId, ACHEnt achEnt) throws IOException {
        achEnt.setRequestId(requestId);
        logger.debug(achEnt.requestId + " - updateBankProfile : " + gson.toJson(achEnt));
        ObjResponse achResponse = new ObjResponse();

        int returnCode = achEnt.validateData();
        if (returnCode != 200) {
            achResponse.returnCode = returnCode;
            logger.debug(achEnt.requestId + " - updateBankProfileResponse : " + achResponse.toString());
            return Response.status(200).header("x-request-id", achEnt.requestId).entity(achResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(achEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.updateBankProfile(achEnt, "update");
        } else {
            responseStr = payBusiness.updateBankProfile(achEnt, "update");
        }
        logger.debug(achEnt.requestId + " - updateBankProfileResponse : " + responseStr);
        return Response.status(200).header("x-request-id", achEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/updateBillingReport")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response updateBillingReport(@HeaderParam("x-request-id") String requestId, FleetSubscriptionEnt fleetSubscriptionEnt) throws IOException {
        fleetSubscriptionEnt.setRequestId(requestId);
        logger.debug("updateBillingReport : " + gson.toJson(fleetSubscriptionEnt));
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(fleetSubscriptionEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.updateBillingReport(fleetSubscriptionEnt);
        } else {
            responseStr = payBusiness.updateBillingReport(fleetSubscriptionEnt);
        }
        logger.debug("updateBillingReportResponse : " + fleetSubscriptionEnt.requestId + " - " + responseStr);
        return Response.status(200).header("x-request-id", fleetSubscriptionEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/updateCompletedBooking")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response updateCompletedBooking(@HeaderParam("x-request-id") String requestId, UpdateEnt updateEnt) throws IOException {
        updateEnt.setRequestId(requestId);
        logger.debug(updateEnt.requestId + " - updateCompletedBooking : " + updateEnt.bookId);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(updateEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.updateCompletedBooking(updateEnt);
        } else {
            responseStr = payBusiness.updateCompletedBooking(updateEnt);
        }
        logger.debug(updateEnt.requestId + " - updateCompletedBookingResponse : " + responseStr);
        return Response.status(200).header("x-request-id", updateEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/updateDriverBalance")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response updateDriverBalance(@HeaderParam("x-request-id") String requestId, UpdateBalanceEnt updateBalanceEnt) throws IOException {
        updateBalanceEnt.setRequestId(requestId);
        logger.debug(updateBalanceEnt.requestId + " - updateDriverBalance : " + gson.toJson(updateBalanceEnt));
        ObjResponse objResponse = new ObjResponse();
        int returnCode = updateBalanceEnt.validateData();
        if (returnCode != 200) {
            objResponse.returnCode = returnCode;
            logger.debug(updateBalanceEnt.requestId + " - updateDriverBalanceResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", updateBalanceEnt.requestId).header("x-request-id", updateBalanceEnt.requestId).entity(objResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(updateBalanceEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.updateDriverBalance(updateBalanceEnt);
        } else {
            responseStr = payBusiness.updateDriverBalance(updateBalanceEnt);
        }
        logger.debug(updateBalanceEnt.requestId + " - updateDriverBalanceResponse : " + responseStr);
        return Response.status(200).header("x-request-id", updateBalanceEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/updatePaidToDriver")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response updatePaidToDriver(@HeaderParam("x-request-id") String requestId, ReportSettlementEnt reportSettlementEnt) throws IOException {
        reportSettlementEnt.setRequestId(requestId);
        logger.debug(reportSettlementEnt.requestId + " - updatePaidToDriver : " + gson.toJson(reportSettlementEnt));
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(reportSettlementEnt.fleetId);
        if (settingMQData.payService) {
            responseStr = paymentRPC.updatePaidToDriver(reportSettlementEnt);
        } else {
            responseStr = payService.updatePaidToDriver(reportSettlementEnt);
        }
        logger.debug(reportSettlementEnt.requestId + " - rupdatePaidToDriverResponse : " + responseStr);
        return Response.status(200).header("x-request-id", reportSettlementEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/updatePaymentStatus")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response updatePaymentStatus(@HeaderParam("x-request-id") String requestId, PaymentEnt paymentEnt) throws IOException {
        paymentEnt.setRequestId(requestId);
        logger.debug(paymentEnt.requestId + " - updatePaymentStatus : " + " paymentType: " + paymentEnt.paymentType);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(paymentEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.updatePaymentStatus(paymentEnt);
        } else {
            responseStr = payBusiness.updatePaymentStatus(paymentEnt);
        }
        logger.debug(paymentEnt.requestId + " - updatePaymentStatusResponse : " + responseStr);
        return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/voidPreAuth")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response voidPreAuthBeforePayment(@HeaderParam("x-request-id") String requestId, VoidEnt voidEnt) throws IOException {
        voidEnt.setRequestId(requestId);
        logger.debug(voidEnt.requestId + " - voidPreAuth : " + gson.toJson(voidEnt));
        ObjResponse voidResponse = new ObjResponse();
        int returnCode = voidEnt.validateData();
        if (returnCode != 200) {
            voidResponse.returnCode = returnCode;
            logger.debug(voidEnt.requestId + " - voidPreAuthRespponse : " + voidResponse.toString());
            return Response.status(200).header("x-request-id", voidEnt.requestId).entity(voidResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(voidEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.voidPreAuthBeforePayment(voidEnt);
        } else {
            responseStr = payBusiness.voidPreAuthBeforePayment(voidEnt);
        }
        logger.debug(voidEnt.requestId + " - voidPreAuthRespponse : " + responseStr);
        return Response.status(200).header("x-request-id", voidEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/checkWebhookStripe")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response checkWebhookStripe(@HeaderParam("x-request-id") String requestId,String json) throws IOException {
        logger.debug(requestId + " - checkWebhookStripe : " + gson.toJson(json));
        JsonObject jobj = new Gson().fromJson(json, JsonObject.class);
        String bookId = jobj.get("bookId").getAsString();
        String responseStr = payBusiness.checkWebhookStripe(bookId, requestId);
        return Response.status(200).header("x-request-id", requestId).entity(responseStr).build();

    }
    @Path("api/paymentgateway/webhookStripe")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response webhookStripe(@HeaderParam("x-request-id") String requestId, Event event) throws IOException {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        logger.debug(requestId + " - webhookStripe : " + gson.toJson(event));
        String fleetId = proxyUtil.webhookStripe(event, requestId);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.webhookStripe(gson.toJson(event), requestId);
        } else {
            responseStr = payBusiness.webhookStripe(gson.toJson(event), requestId);
        }
        logger.debug(requestId + " - webhookStripeResponse : " + event.getId() + " - " + responseStr);
        return Response.status(200).header("x-request-id", event.getId()).entity(responseStr).build();
    }

    @Path("api/paymentgateway/withdrawalRequest")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response withdrawalRequest(@HeaderParam("x-request-id") String requestId, WalletTransferEnt walletTransferEnt) throws IOException {
        walletTransferEnt.setRequestId(requestId);
        logger.debug(walletTransferEnt.requestId + " - withdrawalRequest : " + gson.toJson(walletTransferEnt));
        ObjResponse withdrawalResponse = new ObjResponse();
        int returnCode = walletTransferEnt.validateData();
        if (returnCode != 200) {
            withdrawalResponse.returnCode = returnCode;
            logger.debug(walletTransferEnt.requestId + " - withdrawalRequestResponse : " + withdrawalResponse.toString());
            return Response.status(200).header("x-request-id", walletTransferEnt.requestId).entity(withdrawalResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(walletTransferEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.withdrawalRequest(walletTransferEnt);
        } else {
            responseStr = payBusiness.withdrawalRequest(walletTransferEnt);
        }
        logger.debug(walletTransferEnt.requestId + " - withdrawalRequestResponse : " + responseStr);
        return Response.status(200).header("x-request-id", walletTransferEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/updateSettingMicroservice/{fleetId}")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @GET
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response updateSettingMicroservice(@HeaderParam("x-request-id") String requestId, @PathParam("fleetId") String fleetId) throws IOException {

        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        String responseStr = "";
        responseStr = settingMQ.updateCacheSettingMQ(fleetId).toString();
        logger.debug(requestId + " - updateSettingMicroservice : " + responseStr);
        return Response.status(200).entity(responseStr).build();
    }

    @Path("api/paymentgateway/getReferralEarning")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getReferralEarning(@HeaderParam("x-request-id") String requestId, ReferralTransactionEnt referralTransactionEnt) throws IOException {
        referralTransactionEnt.setRequestId(requestId);
        logger.debug(referralTransactionEnt.requestId + " - getReferralEarning: " + gson.toJson(referralTransactionEnt));
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(referralTransactionEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.getReferralEarning(referralTransactionEnt);
        } else {
            responseStr = payBusiness.getReferralEarning(referralTransactionEnt);
        }
        logger.debug(referralTransactionEnt.requestId + " - getReferralEarningResponse: " + responseStr);
        return Response.status(200).header("x-request-id", referralTransactionEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/driverTopupPax")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response driverTopupPax(@HeaderParam("x-request-id") String requestId, DriverTopupPaxEnt topupEnt) throws IOException {
        topupEnt.setRequestId(requestId);
        logger.debug(topupEnt.requestId + " - driverTopupPax : " + gson.toJson(topupEnt));
        ObjResponse objResponse = new ObjResponse();
        int returnCode = topupEnt.validateData();
        if (returnCode != 200) {
            objResponse.returnCode = returnCode;
            logger.debug(topupEnt.requestId + " - driverTopupPaxResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", topupEnt.requestId).header("x-request-id", topupEnt.requestId).entity(objResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(topupEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.driverTopupPax(topupEnt);
        } else {
            responseStr = payBusiness.driverTopupPax(topupEnt);
        }
        logger.debug(topupEnt.requestId + " - driverTopupPaxResponse : " + responseStr);
        return Response.status(200).header("x-request-id", topupEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/applyReferralAndAddPromo")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response applyReferralAndAddPromo(@HeaderParam("x-request-id") String requestId, PromoEnt promoEnt) throws IOException {
        promoEnt.setRequestId(requestId);
        logger.debug(promoEnt.requestId + " - applyReferralAndAddPromo: " + gson.toJson(promoEnt));
        ObjResponse objResponse = new ObjResponse();
        int returnCode = promoEnt.validateData();
        if (returnCode != 200) {
            objResponse.returnCode = returnCode;
            logger.debug(promoEnt.requestId + " - applyReferralAndAddPromoResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", promoEnt.requestId).entity(objResponse.toString()).build();
        }
        String responseStr = payService.applyReferralAndAddPromo(promoEnt);
        logger.debug(promoEnt.requestId + " - applyReferralAndAddPromoResponse : " + responseStr);
        return Response.status(200).header("x-request-id", promoEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/resetCashWallet")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response resetCashWallet(@HeaderParam("x-request-id") String requestId, UpdateBalanceEnt updateBalanceEnt) throws IOException {
        updateBalanceEnt.setRequestId(requestId);
        logger.debug(updateBalanceEnt.requestId + " - resetCashWallet: " + gson.toJson(updateBalanceEnt));
        String responseStr = payBusiness.resetCashWallet(updateBalanceEnt);
        logger.debug(updateBalanceEnt.requestId + " - resetCashWalletResponse: " + responseStr);
        return Response.status(200).header("x-request-id", updateBalanceEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/syncReferralTransaction")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response syncReferralTransaction(@HeaderParam("x-request-id") String requestId, SyncEnt syncEnt) throws IOException {
        syncEnt.setRequestId(requestId);
        logger.debug(syncEnt.requestId + " - syncReferralTransaction: " + gson.toJson(syncEnt));
        String responseStr = payBusiness.syncReferralTransaction(syncEnt);
        logger.debug(syncEnt.requestId + " - syncReferralTransactionResponse: " + responseStr);
        return Response.status(200).header("x-request-id", syncEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/resetCreditBalance")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response resetCreditBalance(@HeaderParam("x-request-id") String requestId, SyncEnt syncEnt) throws IOException {
        syncEnt.setRequestId(requestId);
        logger.debug(syncEnt.requestId + " - resetCreditBalance: " + gson.toJson(syncEnt));
        String responseStr = payBusiness.resetCreditBalance(syncEnt);
        logger.debug(syncEnt.requestId + " - resetCreditBalanceResponse: " + responseStr);
        return Response.status(200).header("x-request-id", syncEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/updateDriverCashBalance")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response updateDriverCashBalance(@HeaderParam("x-request-id") String requestId, WalletTransferEnt walletTransferEnt) throws IOException {
        walletTransferEnt.setRequestId(requestId);
        logger.debug(walletTransferEnt.requestId + " - updateDriverCashBalance : " + gson.toJson(walletTransferEnt));
        ObjResponse withdrawalResponse = new ObjResponse();
        int returnCode = walletTransferEnt.validateUpdate();
        if (returnCode != 200) {
            withdrawalResponse.returnCode = returnCode;
            logger.debug(walletTransferEnt.requestId + " - updateDriverCashBalanceResponse : " + withdrawalResponse.toString());
            return Response.status(200).header("x-request-id", walletTransferEnt.requestId).entity(withdrawalResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(walletTransferEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.updateDriverCashBalance(walletTransferEnt);
        } else {
            responseStr = payBusiness.updateDriverCashBalance(walletTransferEnt);
        }
        logger.debug(walletTransferEnt.requestId + " - updateDriverCashBalanceResponse : " + responseStr);
        return Response.status(200).header("x-request-id", walletTransferEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/checkPendingPayment")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response checkPendingPayment(@HeaderParam("x-request-id") String requestId, PaymentChecking paymentChecking) throws IOException {
        paymentChecking.setRequestId(requestId);
        logger.debug(paymentChecking.requestId + " - checkPendingPayment: " + gson.toJson(paymentChecking));
        ObjResponse withdrawalResponse = new ObjResponse();
        int returnCode = paymentChecking.validateData();
        if (returnCode != 200) {
            withdrawalResponse.returnCode = returnCode;
            logger.debug(paymentChecking.requestId + " - checkPendingPaymentResponse : " + withdrawalResponse.toString());
            return Response.status(200).header("x-request-id", paymentChecking.requestId).entity(withdrawalResponse.toString()).build();
        }
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(paymentChecking.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.checkPendingPayment(paymentChecking);
        } else {
            responseStr = payBusiness.checkPendingPayment(paymentChecking);
        }
        logger.debug(paymentChecking.requestId + " - checkPendingPaymentResponse: " + responseStr);
        return Response.status(200).header("x-request-id", paymentChecking.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/getPayoutDriverReport")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getPayoutDriverReport(@HeaderParam("x-request-id") String requestId, PayoutReportEnt payoutReportEnt) throws IOException {
        payoutReportEnt.setRequestId(requestId);
        logger.debug(payoutReportEnt.requestId + " - getPayoutDriverReport: " + gson.toJson(payoutReportEnt));
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(payoutReportEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.getPayoutDriverReport(payoutReportEnt);
        } else {
            responseStr = payBusiness.getPayoutDriverReport(payoutReportEnt);
        }
        logger.debug(payoutReportEnt.requestId + " - getPayoutDriverReportResponse: " + responseStr);
        return Response.status(200).header("x-request-id", payoutReportEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/payoutToDriver")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response payoutToDriver(@HeaderParam("x-request-id") String requestId, PayoutEnt payoutEnt) throws IOException {
        payoutEnt.setRequestId(requestId);
        logger.debug(payoutEnt.requestId + " - payoutToDriver: " + gson.toJson(payoutEnt));
        int returnCode = payoutEnt.validateData();
        if (returnCode != 200) {
            ObjResponse objResponse = new ObjResponse();
            objResponse.returnCode = returnCode;
            logger.debug(payoutEnt.requestId + " - payoutToDriverResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", payoutEnt.requestId).header("x-request-id", payoutEnt.requestId).entity(objResponse.toString()).build();
        }
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(payoutEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.payoutToDriver(payoutEnt);
        } else {
            responseStr = payBusiness.payoutToDriver(payoutEnt);
        }
        logger.debug(payoutEnt.requestId + " - payoutToDriverResponse: " + responseStr);
        return Response.status(200).header("x-request-id", payoutEnt.requestId).entity(responseStr).build();
    }


    @Path("api/paymentgateway/prePaidByWallet")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response prePaidByWallet(@HeaderParam("x-request-id") String requestId, PrePaidByWalletEnt prePaidEnt) throws IOException {
        prePaidEnt.setRequestId(requestId);
        logger.debug(prePaidEnt.requestId + " - prePaidByWallet: " + gson.toJson(prePaidEnt));
        ObjResponse withdrawalResponse = new ObjResponse();
        int returnCode = prePaidEnt.validateData();
        if (returnCode != 200) {
            withdrawalResponse.returnCode = returnCode;
            logger.debug(prePaidEnt.requestId + " - prePaidByWalletResponse : " + withdrawalResponse.toString());
            return Response.status(200).header("x-request-id", prePaidEnt.requestId).entity(withdrawalResponse.toString()).build();
        }
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(prePaidEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.prePaidByWallet(prePaidEnt);
        } else {
            responseStr = payBusiness.prePaidByWallet(prePaidEnt);
        }
        logger.debug(prePaidEnt.requestId + " - prePaidByWalletResponse: " + responseStr);
        return Response.status(200).header("x-request-id", prePaidEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/cancelWalletBooking")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response cancelWalletBooking(@HeaderParam("x-request-id") String requestId, CancelBookingEnt cancelBookingEnt) throws IOException {
        cancelBookingEnt.setRequestId(requestId);
        logger.debug(cancelBookingEnt.requestId + " - cancelWalletBooking: " + gson.toJson(cancelBookingEnt));
        ObjResponse withdrawalResponse = new ObjResponse();
        int returnCode = cancelBookingEnt.validateData();
        if (returnCode != 200) {
            withdrawalResponse.returnCode = returnCode;
            logger.debug(cancelBookingEnt.requestId + " - cancelWalletBookingResponse : " + withdrawalResponse.toString());
            return Response.status(200).header("x-request-id", cancelBookingEnt.requestId).entity(withdrawalResponse.toString()).build();
        }
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(cancelBookingEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.cancelWalletBooking(cancelBookingEnt);
        } else {
            responseStr = payBusiness.cancelWalletBooking(cancelBookingEnt);
        }
        logger.debug(cancelBookingEnt.requestId + " - cancelWalletBookingResponse: " + responseStr);
        return Response.status(200).header("x-request-id", cancelBookingEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/removeCacheData")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response removeCacheData(@HeaderParam("x-request-id") String requestId, ClearCacheEnt clearCacheEnt) throws IOException {
        clearCacheEnt.setRequestId(requestId);
        logger.debug(clearCacheEnt.requestId + " - removeCacheData: " + gson.toJson(clearCacheEnt));
        ObjResponse withdrawalResponse = new ObjResponse();
        int returnCode = clearCacheEnt.validateData();
        if (returnCode != 200) {
            withdrawalResponse.returnCode = returnCode;
            logger.debug(clearCacheEnt.requestId + " - removeCacheDataResponse : " + withdrawalResponse.toString());
            return Response.status(200).header("x-request-id", clearCacheEnt.requestId).entity(withdrawalResponse.toString()).build();
        }
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(clearCacheEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.removeCacheData(clearCacheEnt);
        } else {
            responseStr = payBusiness.removeCacheData(clearCacheEnt);
        }
        logger.debug(clearCacheEnt.requestId + " - removeCacheDataResponse: " + responseStr);
        return Response.status(200).header("x-request-id", clearCacheEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/resetExpiredBonus")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response resetExpiredBonus(@HeaderParam("x-request-id") String requestId, SyncEnt syncEnt) throws IOException {
        syncEnt.setRequestId(requestId);
        logger.debug(syncEnt.requestId + " - resetExpiredBonus: " + gson.toJson(syncEnt));
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(syncEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.resetExpiredBonus(syncEnt);
        } else {
            responseStr = payBusiness.resetExpiredBonus(syncEnt);
        }
        logger.debug(syncEnt.requestId + " - resetExpiredBonusResponse: " + responseStr);
        return Response.status(200).header("x-request-id", syncEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/updateInvalidPayout")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response updateInvalidPayout(@HeaderParam("x-request-id") String requestId, PayoutReportEnt payoutReportEnt) throws IOException {
        payoutReportEnt.setRequestId(requestId);
        logger.debug(payoutReportEnt.requestId + " - updateInvalidPayout: " + gson.toJson(payoutReportEnt));
        ObjResponse withdrawalResponse = new ObjResponse();
        int returnCode = payoutReportEnt.validateUpdatePayout();
        if (returnCode != 200) {
            withdrawalResponse.returnCode = returnCode;
            logger.debug(payoutReportEnt.requestId + " - updateInvalidPayoutResponse : " + withdrawalResponse.toString());
            return Response.status(200).header("x-request-id", payoutReportEnt.requestId).entity(withdrawalResponse.toString()).build();
        }
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(payoutReportEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.updateInvalidPayout(payoutReportEnt);
        } else {
            responseStr = payBusiness.updateInvalidPayout(payoutReportEnt);
        }
        logger.debug(payoutReportEnt.requestId + " - updateInvalidPayoutResponse: " + responseStr);
        return Response.status(200).header("x-request-id", payoutReportEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/redeemToPaxWallet")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response redeemToPaxWallet(@HeaderParam("x-request-id") String requestId, RedeemVoucherEnt redeemVoucherEnt) throws IOException {
        redeemVoucherEnt.setRequestId(requestId);
        logger.debug(redeemVoucherEnt.requestId + " - redeemToPaxWallet: " + gson.toJson(redeemVoucherEnt));
        ObjResponse withdrawalResponse = new ObjResponse();
        int returnCode = redeemVoucherEnt.validateData();
        if (returnCode != 200) {
            withdrawalResponse.returnCode = returnCode;
            logger.debug(redeemVoucherEnt.requestId + " - redeemToPaxWalletResponse : " + withdrawalResponse.toString());
            return Response.status(200).header("x-request-id", redeemVoucherEnt.requestId).entity(withdrawalResponse.toString()).build();
        }
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(redeemVoucherEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.redeemToPaxWallet(redeemVoucherEnt);
        } else {
            responseStr = payBusiness.redeemToPaxWallet(redeemVoucherEnt);
        }
        logger.debug(redeemVoucherEnt.requestId + " - redeemToPaxWalletResponse: " + responseStr);
        return Response.status(200).header("x-request-id", redeemVoucherEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/topupDriverCreditWallet")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response topupDriverCredit(@HeaderParam("x-request-id") String requestId, SyncEnt syncEnt) throws IOException {
        syncEnt.setRequestId(requestId);
        logger.debug(syncEnt.requestId + " - topupDriverCreditWallet: " + gson.toJson(syncEnt));
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(syncEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.topupDriverCredit(syncEnt);
        } else {
            responseStr = payBusiness.topupDriverCredit(syncEnt);
        }
        logger.debug(syncEnt.requestId + " - topupDriverCreditWalletResponse: " + responseStr);
        return Response.status(200).header("x-request-id", syncEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/paymentVippsNotificationURL")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response paymentVippsNotificationURL(@HeaderParam("x-request-id") String requestId, VippsNotifyEnt vippsNotifyEnt) throws IOException {
        vippsNotifyEnt.setRequestId(requestId);
        logger.debug(vippsNotifyEnt.requestId + " - paymentVippsNotificationURL : " + gson.toJson(vippsNotifyEnt));
        String responseStr = payBusiness.paymentVippsNotificationURL(vippsNotifyEnt);
        logger.debug(vippsNotifyEnt.requestId + " - paymentVippsNotificationURLResponse : " + vippsNotifyEnt.requestId + " - " + responseStr);
        return Response.status(200).header("x-request-id", vippsNotifyEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/paymentDNBNotificationURL")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response paymentDNBNotificationURL(@HeaderParam("x-request-id") String requestId, PaymentNotificationEnt paymentNotificationEnt) throws IOException {
        paymentNotificationEnt.setRequestId(requestId);
        logger.debug("paymentDNBNotificationURL : " + gson.toJson(paymentNotificationEnt));
        String responseStr = payBusiness.paymentDNBNotificationURL(paymentNotificationEnt);
        logger.debug("paymentDNBNotificationURLResponse : " + paymentNotificationEnt.requestId + " - " + responseStr);
        return Response.status(200).header("x-request-id", paymentNotificationEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/getPayoutMerchantReport")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getPayoutMerchantReport(@HeaderParam("x-request-id") String requestId, PayoutReportEnt payoutReportEnt) throws IOException {
        payoutReportEnt.setRequestId(requestId);
        logger.debug(payoutReportEnt.requestId + " - getPayoutMerchantReport : " + gson.toJson(payoutReportEnt));
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(payoutReportEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.getPayoutMerchantReport(payoutReportEnt);
        } else {
            responseStr = payBusiness.getPayoutMerchantReport(payoutReportEnt);
        }
        logger.debug(payoutReportEnt.requestId + " - getPayoutMerchantReportResponse: " + responseStr);
        return Response.status(200).header("x-request-id", payoutReportEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/payoutToMerchant")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response payoutToMerchant(@HeaderParam("x-request-id") String requestId, PayoutEnt payoutEnt) throws IOException {
        payoutEnt.setRequestId(requestId);
        logger.debug(payoutEnt.requestId + " - payoutToMerchant : " + gson.toJson(payoutEnt));
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(payoutEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.payoutToMerchant(payoutEnt);
        } else {
            responseStr = payBusiness.payoutToMerchant(payoutEnt);
        }
        logger.debug(payoutEnt.requestId + " - payoutToMerchantResponse: " + responseStr);
        return Response.status(200).header("x-request-id", payoutEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/merchantPendingAmount")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response merchantPendingAmount(@HeaderParam("x-request-id") String requestId, PayoutReportEnt payoutReportEnt) throws IOException {
        payoutReportEnt.setRequestId(requestId);
        logger.debug(payoutReportEnt.requestId + " - merchantPendingAmount : " + gson.toJson(payoutReportEnt));
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(payoutReportEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.merchantPendingAmount(payoutReportEnt);
        } else {
            responseStr = payBusiness.merchantPendingAmount(payoutReportEnt);
        }
        logger.debug(payoutReportEnt.requestId + " - merchantPendingAmountResponse: " + responseStr);
        return Response.status(200).header("x-request-id", payoutReportEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/getSecureFieldSession/{fleetId}/")
    @PermitAll
    @GET
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getSecureFieldSession(@HeaderParam("x-request-id") String requestId, @PathParam("fleetId") String fleetId) throws IOException {
        if (requestId == null || requestId.isEmpty())
            requestId = UUID.randomUUID().toString();
        logger.debug(requestId + " - getSecureFieldSession - fleetId:" + fleetId);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(fleetId);
        CreditEnt creditEnt = new CreditEnt();
        creditEnt.fleetId = fleetId;
        creditEnt.userId = "";
        creditEnt.requestId = requestId;
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.getSecureFieldSession(creditEnt);
        } else {
            responseStr = payBusiness.getSecureFieldSession(creditEnt);
        }
        logger.debug(requestId + " - getSecureFieldSessionResponse : " + responseStr);
        return Response.status(200).header("x-request-id", requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/getSecureFieldSession")
    @PermitAll
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getSecureFieldSession(@HeaderParam("x-request-id") String requestId, CreditEnt creditEnt) throws IOException {
        creditEnt.setRequestId(requestId);
        logger.debug(creditEnt.requestId + " - getSecureFieldSession: " + creditEnt.toString());
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(creditEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.getSecureFieldSession(creditEnt);
        } else {
            responseStr = payBusiness.getSecureFieldSession(creditEnt);
        }
        logger.debug(creditEnt.requestId + " - getSecureFieldSessionResponse : " + responseStr);
        return Response.status(200).header("x-request-id", requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/getBankRequiredFields")
    @PermitAll
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getBankRequiredFields(@HeaderParam("x-request-id") String requestId, ACHEnt achEnt) throws IOException {
        achEnt.setRequestId(requestId);
        logger.debug(achEnt.requestId + " - getBankRequiredFields: " + achEnt.fleetId);
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(achEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.getBankRequiredFields(achEnt);
        } else {
            responseStr = payBusiness.getBankRequiredFields(achEnt);
        }
        logger.debug(achEnt.requestId + " - getBankRequiredFieldsResponse : " + responseStr);
        return Response.status(200).header("x-request-id", requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/createBankToken")
    @PermitAll
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response createBankToken(@HeaderParam("x-request-id") String requestId, ACHEnt achEnt) throws IOException {
        achEnt.setRequestId(requestId);
        logger.debug(achEnt.requestId + " - createBankToken: " + gson.toJson(achEnt));
        ObjResponse objResponse = new ObjResponse();
        int returnCode = achEnt.validateBankToken();
        if (returnCode != 200) {
            objResponse.returnCode = returnCode;
            logger.debug(achEnt.requestId + " - createBankTokenResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", achEnt.requestId).entity(objResponse.toString()).build();
        }
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(achEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.createBankToken(achEnt);
        } else {
            responseStr = payBusiness.createBankToken(achEnt);
        }
        logger.debug(achEnt.requestId + " - createBankTokenResponse : " + responseStr);
        return Response.status(200).header("x-request-id", requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/estimateGrossEarning")
    @PermitAll
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response estimateGrossEarning(@HeaderParam("x-request-id") String requestId, List<ETAGrossEnt> etaGrossEnts) throws IOException {
        logger.debug(requestId + " - estimateGrossEarning: " + gson.toJson(etaGrossEnts));
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(null);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.estimateGrossEarning(etaGrossEnts, requestId);
        } else {
            responseStr = payBusiness.calGrossEarning(etaGrossEnts, requestId);
        }
        logger.debug(requestId + " - estimateGrossEarningResponse : " + responseStr);
        return Response.status(200).header("x-request-id", requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/topUpTransportDriver")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response topUpTransportDriver(@HeaderParam("x-request-id") String requestId, SyncEnt syncEnt) throws IOException {
        syncEnt.setRequestId(requestId);
        logger.debug(syncEnt.requestId + " - topUpTransportDriver: " + gson.toJson(syncEnt));
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(syncEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.topUpTransportDriver(syncEnt);
        } else {
            responseStr = payBusiness.topUpTransportDriver(syncEnt);
        }
        logger.debug(syncEnt.requestId + " - topUpTransportDriverResponse: " + responseStr);
        return Response.status(200).header("x-request-id", syncEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/completeBooking")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response topUpTransportDriver(@HeaderParam("x-request-id") String requestId, PaymentEnt paymentEnt) throws IOException {
        paymentEnt.setRequestId(requestId);
        logger.debug(paymentEnt.requestId + " - completeBooking: " + gson.toJson(paymentEnt));
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(paymentEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.completeBooking(paymentEnt);
        } else {
            responseStr = payBusiness.completeBooking(paymentEnt);
        }
        logger.debug(paymentEnt.requestId + " - completeBookingResponse: " + responseStr);
        return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/topUpBonusToDriverWallet")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response topUpBonusToDriverWallet(@HeaderParam("x-request-id") String requestId, SyncEnt syncEnt) throws IOException {
        syncEnt.setRequestId(requestId);
        logger.debug(syncEnt.requestId + " - topUpBonusToDriverWallet: " + gson.toJson(syncEnt));
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(syncEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.topUpBonusToDriverWallet(syncEnt);
        } else {
            responseStr = payBusiness.topUpBonusToDriverWallet(syncEnt);
        }
        logger.debug(syncEnt.requestId + " - topUpBonusToDriverWalletResponse: " + responseStr);
        return Response.status(200).header("x-request-id", syncEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/removeExpiredBonus")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response removeExpiredBonus(@HeaderParam("x-request-id") String requestId, SyncEnt syncEnt) throws IOException {
        syncEnt.setRequestId(requestId);
        logger.debug(syncEnt.requestId + " - removeExpiredBonus: " + gson.toJson(syncEnt));
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(syncEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.removeExpiredBonus(syncEnt);
        } else {
            responseStr = payBusiness.removeExpiredBonus(syncEnt);
        }
        logger.debug(syncEnt.requestId + " - removeExpiredBonusResponse: " + responseStr);
        return Response.status(200).header("x-request-id", syncEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/syncBalance")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response syncBalance(@HeaderParam("x-request-id") String requestId, SyncEnt syncEnt) throws IOException {
        syncEnt.setRequestId(requestId);
        logger.debug(syncEnt.requestId + " - syncBalance: " + gson.toJson(syncEnt));
        String responseStr = payBusiness.syncBalance(syncEnt);
        logger.debug(syncEnt.requestId + " - syncBalanceResponse: " + responseStr);
        return Response.status(200).header("x-request-id", syncEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/completeStreetSharingBooking")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response completeStreetSharingBooking(@HeaderParam("x-request-id") String requestId, PaymentEnt paymentEnt) throws IOException {
        paymentEnt.setRequestId(requestId);
        logger.debug(paymentEnt.requestId + " - completeStreetSharingBooking: " + gson.toJson(paymentEnt));
        ObjResponse objResponse = new ObjResponse();
        paymentEnt.paymentType = KeysUtil.PAYMENT_CASH; // default method
        int returnCode = paymentEnt.validateData();
        if (returnCode != 200) {
            objResponse.returnCode = returnCode;
            logger.debug(paymentEnt.requestId + " - completeStreetSharingBookingResponse : " + objResponse.toString());
            return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(objResponse.toString()).build();
        }
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(paymentEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.completeStreetSharingBooking(paymentEnt);
        } else {
            responseStr = payBusiness.completeStreetSharingBooking(paymentEnt);
        }
        logger.debug(paymentEnt.requestId + " - completeStreetSharingBookingResponse : " + responseStr);
        return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }

    @Path("api/paymentgateway/getCheckoutPage")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getCheckoutPage(@HeaderParam("x-request-id") String requestId, CreditEnt creditEnt) throws IOException {
        creditEnt.setRequestId(requestId);
        logger.debug(creditEnt.requestId + " - getCheckoutPage: " + gson.toJson(creditEnt));
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(creditEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.getCheckoutPage(creditEnt);
        } else {
            responseStr = payBusiness.getCheckoutPage(creditEnt);
        }
        logger.debug(creditEnt.requestId + " - getCheckoutPageResponse: " + responseStr);
        return Response.status(200).header("x-request-id", creditEnt.requestId).entity(responseStr).build();
    }

    @Path("pg/v1/wallet/verifyUser")
    @RolesAllowed({DIS_ROLE, API_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(produces="application/json", value = "Verify if phone number has registered on the system", httpMethod="POST", response = VerifyResponseEnt.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = VerifyResponseEnt.class, message = "Successful operation"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Authentication is required"),
            @ApiResponse(code = 402, message = "Authentication user is not correct"),
            @ApiResponse(code = 411, message = "UserType is required or not correct. Available values: customer, captain"),
            @ApiResponse(code = 412, message = "Phone number is required"),
            @ApiResponse(code = 413, message = "Phone number is not correct or not registered on the system"),
            @ApiResponse(code = 414, message = "Registered user has been deactivated on the system"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 525, message = "Something went wrong. Please try again later.")
        }
    )
    //https://github.com/swagger-api/swagger-core/wiki/Annotations-1.5.X#apiparam
    public Response verifyUser(
            @ApiParam(value = "requestId to trace the log", hidden = true) @HeaderParam("x-request-id") String xRequestId,
            @ApiParam(value = "Format: Bearer [token]", example = "Bearer [token]") @HeaderParam("Authorization") String auth,
            @ApiParam(value = "- phoneNumber: Phone number which need to verify. Format: [+] + country code + 9-11 number\n - userType: Type of user. Available values: customer, captain") VerifyEnt verifyEnt) throws IOException {
        String requestId = verifyEnt.setRequestId(xRequestId);
        logger.debug(requestId + " - wallet/verifyUser: " + gson.toJson(verifyEnt));
        logger.debug(requestId + " - auth: " + auth);
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ("");
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.verifyUser(requestId, verifyEnt, auth);
        } else {
            responseStr = payBusiness.verifyUser(requestId, verifyEnt, auth);
        }
        logger.debug(requestId + " - verifyUserResponse: " + responseStr);
        return Response.status(CommonUtils.getReturnCode(responseStr)).header("x-request-id", requestId).entity(responseStr).type(MediaType.APPLICATION_JSON_TYPE).build();
    }

    @Path("pg/v1/wallet/topup")
    @RolesAllowed({DIS_ROLE, API_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(produces="application/json", value = "Top-up to user's wallet", httpMethod="POST", response = TopupResponseEnt.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = TopupResponseEnt.class, message = "Successful operation"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Authentication is required"),
            @ApiResponse(code = 402, message = "Authentication user is not correct"),
            @ApiResponse(code = 415, message = "Token is not correct or has been expired"),
            @ApiResponse(code = 416, message = "Duplicate request for the same transaction"),
            @ApiResponse(code = 417, message = "Top up amount is not correct"),
            @ApiResponse(code = 418, message = "Currency is not correct or not supported"),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 525, message = "Something went wrong. Please try again later.")
        }
    )
    public Response topupByAPI(
            @ApiParam(value = "requestId to trace the log", hidden = true) @HeaderParam("x-request-id") String xRequestId,
            @ApiParam(value = "Format: Bearer [token]", example = "Bearer [token]") @HeaderParam("Authorization") String auth,
            @ApiParam(value = "- secureToken: Secure token received from verify request\n - amount: Top-up amount\n - currencyISO: Top-up currency ISO") com.qupworld.paymentgateway.entities.TopupAPI.TopupEnt topupEnt) throws IOException {
        String requestId = topupEnt.setRequestId(xRequestId);
        logger.debug(requestId + " - wallet/topup: " + gson.toJson(topupEnt));
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ("");
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.topupByAPI(requestId, topupEnt, auth);
        } else {
            responseStr = payBusiness.topupByAPI(requestId, topupEnt, auth);
        }
        logger.debug(requestId + " - topupResponse: " + responseStr);
        return Response.status(CommonUtils.getReturnCode(responseStr)).header("x-request-id", requestId).entity(responseStr).type(MediaType.APPLICATION_JSON_TYPE).build();
    }

    @Path("/pg/oauth/token")
    @PermitAll
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(produces="application/json", value = "Get access token to authentication for other APIs", httpMethod="POST", response = AuthResponseEnt.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = AuthResponseEnt.class, message = "Successful operation"),
            @ApiResponse(code = 402, message = "Wrong username or password")
        }
    )
    public Response getAccessToken(
            @ApiParam(value = "requestId to trace the log", hidden = true) @HeaderParam("x-request-id") String xRequestId,
            @ApiParam(value = "- username: Provided by QUp\n - password: Provided by QUp") AuthEnt authEnt) throws IOException {
        String requestId = (xRequestId == null || xRequestId.isEmpty()) ? UUID.randomUUID().toString() : xRequestId;
        logger.debug(requestId + " - getAccessToken: " + gson.toJson(authEnt));
        // Authenticate the user using the credentials provided
        UserService userService = new UserService();
        User user = userService.getUser(requestId, authEnt.username);
        int returnCode = 200;
        if (user == null || !user.getPassword().equals(authEnt.password)) {
            logger.debug(requestId + " - Wrong username or password");
            return Response.status(402).header("x-request-id", requestId).type(MediaType.APPLICATION_JSON).build();
        }
        // Issue a token for the user
        String token = JwTokenHelper.createJWT(user);

        AuthResponseEnt authResponseEnt = new AuthResponseEnt();
        authResponseEnt.returnCode = returnCode;
        if (returnCode == 200) {
            authResponseEnt.token = token;
        }
        // Return the token on the response
        //return Response.ok(token).build();
        return Response.status(returnCode).header("x-request-id", requestId).entity(authResponseEnt.toString()).type(MediaType.APPLICATION_JSON).build();
    }

    @Path("api/paymentgateway/registerAuthentication")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response registerAuthentication(@HeaderParam("x-request-id") String requestId, RegisterAuthEnt registerAuthEnt) throws IOException {
        registerAuthEnt.setRequestId(requestId);
        logger.debug(registerAuthEnt.requestId + " - registerAuthentication: " + gson.toJson(registerAuthEnt));
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(registerAuthEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.registerAuthentication(registerAuthEnt);
        } else {
            responseStr = payBusiness.registerAuthentication(registerAuthEnt);
        }
        logger.debug(registerAuthEnt.requestId + " - registerAuthenticationResponse: " + responseStr);
        return Response.status(200).header("x-request-id", registerAuthEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/updatePaymentMethod")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response updatePaymentMethod(@HeaderParam("x-request-id") String requestId, UpdateEnt updateEnt) throws IOException {
        updateEnt.setRequestId(requestId);
        logger.debug(updateEnt.requestId + " - updatePaymentMethod: " + gson.toJson(updateEnt));
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(updateEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.updatePaymentMethod(updateEnt);
        } else {
            responseStr = payBusiness.updatePaymentMethod(updateEnt);
        }
        logger.debug(updateEnt.requestId + " - updatePaymentMethodResponse: " + responseStr);
        return Response.status(200).header("x-request-id", updateEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/syncPaymentData")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response syncPaymentData(@HeaderParam("x-request-id") String requestId, SyncPaymentEnt syncPaymentEnt) throws IOException {
        syncPaymentEnt.setRequestId(requestId);
        logger.debug(syncPaymentEnt.requestId + " - syncPaymentData: " + gson.toJson(syncPaymentEnt));
        String responseStr = payBusiness.syncPaymentData(syncPaymentEnt);
        logger.debug(syncPaymentEnt.requestId + " - syncPaymentDataResponse: " + responseStr);
        return Response.status(200).header("x-request-id", syncPaymentEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/resetNegativeBalanceForDriver")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response resetNegativeBalance(@HeaderParam("x-request-id") String requestId, SyncEnt syncEnt) throws IOException {
        syncEnt.setRequestId(requestId);
        logger.debug(syncEnt.requestId + " - resetNegativeBalanceForDriver: " + gson.toJson(syncEnt));
        String responseStr = payBusiness.resetNegativeBalanceForDriver(syncEnt);
        logger.debug(syncEnt.requestId + " - resetNegativeBalanceForDriverResponse: " + responseStr);
        return Response.status(200).header("x-request-id", syncEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/migrateCreditCard")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response migrateCreditCard(@HeaderParam("x-request-id") String requestId, MigrateCreditEnt migrateCreditEnt) throws IOException {
        migrateCreditEnt.setRequestId(requestId);
        logger.debug(migrateCreditEnt.requestId + " - migrateCreditCard: " + gson.toJson(migrateCreditEnt));
        String responseStr = payBusiness.migrateCreditCard(migrateCreditEnt);
        logger.debug(migrateCreditEnt.requestId + " - migrateCreditCardResponse: " + responseStr);
        return Response.status(200).header("x-request-id", migrateCreditEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/updateMerchantWalletFromCC")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response updateMerchantWalletBalanceFromCC(@HeaderParam("x-request-id") String requestId, MerchantWalletTransactionEnt merchantWalletTransactionEnt) throws IOException {
        merchantWalletTransactionEnt.setRequestId(requestId);
        logger.debug(merchantWalletTransactionEnt.requestId + " - updateMerchantWalletBalanceFromCC: " + gson.toJson(merchantWalletTransactionEnt));
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(merchantWalletTransactionEnt.fleetId);
        merchantWalletTransactionEnt.from = "CC";
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.updateMerchantWalletBalance(merchantWalletTransactionEnt);
        } else {
            responseStr = payBusiness.updateMerchantWalletBalance(merchantWalletTransactionEnt);
        }
        logger.debug(merchantWalletTransactionEnt.requestId + " - updateMerchantWalletBalanceFromCCResponse: " + responseStr);
        return Response.status(200).header("x-request-id", merchantWalletTransactionEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/removeOldCards")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response removeOldCards(@HeaderParam("x-request-id") String requestId, MigrateCreditEnt migrateCreditEnt) throws IOException {
        migrateCreditEnt.setRequestId(requestId);
        logger.debug(migrateCreditEnt.requestId + " - removeOldCards: " + gson.toJson(migrateCreditEnt));
        String responseStr = payBusiness.removeOldCards(migrateCreditEnt);
        logger.debug(migrateCreditEnt.requestId + " - removeOldCardsResponse: " + responseStr);
        return Response.status(200).header("x-request-id", migrateCreditEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/driverTopupDriver")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response driverTopupDriver(@HeaderParam("x-request-id") String requestId, DriverTopupDriverEnt driverTopupDriverEnt) throws IOException {
        driverTopupDriverEnt.setRequestId(requestId);
        logger.debug(driverTopupDriverEnt.requestId + " - driverTopupDriver: " + gson.toJson(driverTopupDriverEnt));
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(driverTopupDriverEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.driverTopupDriver(driverTopupDriverEnt);
        } else {
            responseStr = payBusiness.driverTopupDriver(driverTopupDriverEnt);
        }
        logger.debug(driverTopupDriverEnt.requestId + " - driverTopupDriverResponse: " + responseStr);
        return Response.status(200).header("x-request-id", driverTopupDriverEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/chargeDriverCancelBooking")
    @RolesAllowed({DIS_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response chargeDriverCancelBooking(@HeaderParam("x-request-id") String requestId, CancelBookingEnt cancelBookingEnt) throws IOException {
        cancelBookingEnt.setRequestId(requestId);
        logger.debug(cancelBookingEnt.requestId + " - chargeDriverCancelBooking: " + gson.toJson(cancelBookingEnt));
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(cancelBookingEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.chargeDriverCancelBooking(cancelBookingEnt);
        } else {
            responseStr = payBusiness.chargeDriverCancelBooking(cancelBookingEnt);
        }
        logger.debug(cancelBookingEnt.requestId + " - chargeDriverCancelBookingResponse: " + responseStr);
        return Response.status(200).header("x-request-id", cancelBookingEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/completePartialPayment")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response completePartialPayment(@HeaderParam("x-request-id") String requestId, PaymentEnt paymentEnt) throws IOException {
        paymentEnt.setRequestId(requestId);
        logger.debug(paymentEnt.requestId + " - completePartialPayment: " + gson.toJson(paymentEnt));
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(paymentEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.completePartialPayment(paymentEnt);
        } else {
            responseStr = payBusiness.completePartialPayment(paymentEnt);
        }
        logger.debug(paymentEnt.requestId + " - completePartialPaymentResponse: " + responseStr);
        return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(responseStr).build();
    }

    @Path("api/paymentgateway/StripeConnectOnboarding")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response stripeConnectOnboarding(@HeaderParam("x-request-id") String requestId, StripeConnectEnt stripeConnectEnt) throws IOException {
        stripeConnectEnt.setRequestId(requestId);
        logger.debug(stripeConnectEnt.requestId + " - stripeConnectOnboarding: " + gson.toJson(stripeConnectEnt));
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(stripeConnectEnt.fleetId); // Jupiter
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.stripeConnectOnboarding(stripeConnectEnt);
        } else {
            responseStr = payBusiness.stripeConnectOnboarding(stripeConnectEnt);
        }
        logger.debug(stripeConnectEnt.requestId + " - stripeConnectOnboardingResponse : " + responseStr);
        return Response.status(200).entity(responseStr).build();
    }

    @Path("api/paymentgateway/StripeConnectLogin")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response stripeConnectLogin(@HeaderParam("x-request-id") String requestId, StripeConnectEnt stripeConnectEnt) throws IOException {
        stripeConnectEnt.setRequestId(requestId);
        logger.debug(stripeConnectEnt.requestId + " - stripeConnectLogin: " + gson.toJson(stripeConnectEnt));
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(stripeConnectEnt.fleetId); // Jupiter
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.stripeConnectLogin(stripeConnectEnt);
        } else {
            responseStr = payBusiness.stripeConnectLogin(stripeConnectEnt);
        }
        logger.debug(stripeConnectEnt.requestId + " - stripeConnectLoginResponse : " + responseStr);
        return Response.status(200).entity(responseStr).build();
    }

    @Path("api/paymentgateway/disconnectStripe")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response disconnectStripe(@HeaderParam("x-request-id") String requestId, StripeConnectEnt stripeConnectEnt) throws IOException {
        stripeConnectEnt.setRequestId(requestId);
        logger.debug(stripeConnectEnt.requestId + " - disconnectStripe: " + gson.toJson(stripeConnectEnt));
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(stripeConnectEnt.fleetId); // Jupiter
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.disconnectStripe(stripeConnectEnt);
        } else {
            responseStr = payBusiness.disconnectStripe(stripeConnectEnt);
        }
        logger.debug(stripeConnectEnt.requestId + " - disconnectStripeResponse : " + responseStr);
        return Response.status(200).entity(responseStr).build();
    }

    @Path("api/paymentgateway/content/{gateway}/{action}")
    @PermitAll
    @GET
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response returnWebContent(@PathParam("gateway") String gateway, @PathParam("action") String action) throws IOException {
        String requestId = UUID.randomUUID().toString();
        logger.debug(requestId + " - gateway : " + gateway);
        logger.debug(requestId + " - action : " + action);
        String responseStr = payBusiness.returnWebContent(requestId, gateway, action);
        logger.debug(requestId + " - returnWebPage : " + responseStr);
        return Response.status(200).entity(responseStr).build();
    }

    @Path("api/paymentgateway/preAuthForHydraReservation")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response preAuthForHydraReservation(@HeaderParam("x-request-id") String requestId, PreAuthEnt preAuthEnt) throws IOException {
        preAuthEnt.setRequestId(requestId);
        logger.debug(preAuthEnt.requestId + " - preAuthForHydraReservation: " + gson.toJson(preAuthEnt));
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(preAuthEnt.fleetId); // Jupiter
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.preAuthForHydraReservation(preAuthEnt);
        } else {
            responseStr = payBusiness.preAuthForHydraReservation(preAuthEnt);
        }
        logger.debug(preAuthEnt.requestId + " - preAuthForHydraReservationResponse : " + responseStr);
        return Response.status(200).entity(responseStr).build();
    }

    @Path("api/paymentgateway/getHydraPendingPayoutReport")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getHydraPendingPayoutReport(@HeaderParam("x-request-id") String requestId, HydraPayoutEnt hydraPayoutEnt) throws IOException {
        hydraPayoutEnt.setRequestId(requestId);
        logger.debug(hydraPayoutEnt.requestId + " - getHydraPendingPayoutReport: " + gson.toJson(hydraPayoutEnt));
        String responseStr = payBusiness.getHydraPendingPayoutReport(hydraPayoutEnt);
        logger.debug(hydraPayoutEnt.requestId + " - getHydraPendingPayoutReportResponse : " + responseStr);
        return Response.status(200).entity(responseStr).build();
    }

    @Path("api/paymentgateway/hydraManualPayout")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response hydraManualPayout(@HeaderParam("x-request-id") String requestId, HydraPayoutEnt hydraPayoutEnt) throws IOException {
        hydraPayoutEnt.setRequestId(requestId);
        logger.debug(hydraPayoutEnt.requestId + " - hydraManualPayout: " + gson.toJson(hydraPayoutEnt));
        String responseStr = payBusiness.hydraManualPayout(hydraPayoutEnt);
        logger.debug(hydraPayoutEnt.requestId + " - hydraManualPayoutResponse : " + responseStr);
        return Response.status(200).entity(responseStr).build();
    }

    @Path("api/paymentgateway/getPendingDetailByFleet")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getPendingDetailByFleet(@HeaderParam("x-request-id") String requestId, HydraPayoutEnt hydraPayoutEnt) throws IOException {
        hydraPayoutEnt.setRequestId(requestId);
        logger.debug(hydraPayoutEnt.requestId + " - getPendingDetailByFleet: " + gson.toJson(hydraPayoutEnt));
        String responseStr = payBusiness.getPendingDetailByFleet(hydraPayoutEnt);
        logger.debug(hydraPayoutEnt.requestId + " - getPendingDetailByFleetResponse : " + responseStr);
        return Response.status(200).entity(responseStr).build();
    }

    @Path("api/paymentgateway/clearPendingHydra")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response clearPendingHydra(@HeaderParam("x-request-id") String requestId, HydraPendingEnt hydraPendingEnt) throws IOException {
        hydraPendingEnt.setRequestId(requestId);
        logger.debug(hydraPendingEnt.requestId + " - clearPendingHydra: " + gson.toJson(hydraPendingEnt));
        String responseStr = payBusiness.clearPendingHydra(hydraPendingEnt);
        logger.debug(hydraPendingEnt.requestId + " - clearPendingHydraResponse : " + responseStr);
        return Response.status(200).entity(responseStr).build();
    }

    @Path("api/paymentgateway/getOutstandingHydra")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getOutstandingHydra(@HeaderParam("x-request-id") String requestId, OutstandingEnt outstandingEnt) throws IOException {
        outstandingEnt.setRequestId(requestId);
        logger.debug(outstandingEnt.requestId + " - getOutstandingHydra: " + gson.toJson(outstandingEnt));
        String responseStr = payBusiness.getOutstandingHydra(outstandingEnt);
        logger.debug(outstandingEnt.requestId + " - getOutstandingHydraResponse : " + responseStr);
        return Response.status(200).entity(responseStr).build();
    }

    @Path("api/paymentgateway/clearPendingHydraForPWA")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response clearPendingHydraForPWA(@HeaderParam("x-request-id") String requestId, HydraPendingEnt hydraPendingEnt) throws IOException {
        hydraPendingEnt.setRequestId(requestId);
        logger.debug(hydraPendingEnt.requestId + " - clearPendingHydraForPWA: " + gson.toJson(hydraPendingEnt));
        String responseStr = payBusiness.clearPendingHydraForPWA(hydraPendingEnt);
        logger.debug(hydraPendingEnt.requestId + " - clearPendingHydraForPWAResponse : " + responseStr);
        return Response.status(200).entity(responseStr).build();
    }

    @Path("api/paymentgateway/supplierRejectBooking")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response supplierRejectBooking(@HeaderParam("x-request-id") String requestId, CancelBookingEnt cancelEnt) throws IOException {
        cancelEnt.setRequestId(requestId);
        logger.debug(cancelEnt.requestId + " - supplierRejectBooking: " + gson.toJson(cancelEnt));
        String responseStr;
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(cancelEnt.fleetId); // Jupiter
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.supplierRejectBooking(cancelEnt);
        } else {
            responseStr = payBusiness.supplierRejectBooking(cancelEnt);
        }
        logger.debug(cancelEnt.requestId + " - supplierRejectBookingResponse : " + responseStr);
        return Response.status(200).entity(responseStr).build();
    }

    /**
     * estimateDeliveryFareWithMultiCarType
     *
     * @param etaFareEnt
     * @return
     * @throws IOException
     */
    @Path("api/paymentgateway/estimateDeliveryFareWithMultiCarType")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response estimateDeliveryFareWithMultiCarType(@HeaderParam("x-request-id") String requestId, ETADeliveryEnt etaFareEnt) throws IOException {
        etaFareEnt.setRequestId(requestId);
        logger.debug(etaFareEnt.requestId + " - estimateDeliveryFareWithMultiCarType : " + gson.toJson(etaFareEnt));
        if (etaFareEnt.rateDetails == null || etaFareEnt.rateDetails.isEmpty() || etaFareEnt.isEmptyString(etaFareEnt.fleetId)
                || etaFareEnt.isEmptyString(etaFareEnt.bookFrom)) {
            ObjResponse etaResponse = new ObjResponse();
            etaResponse.returnCode = 406;
            logger.debug(etaFareEnt.requestId + " - estimateDeliveryFareWithMultiCarTypeResponse : " + etaResponse.toString());
            return Response.status(200).header("x-request-id", etaFareEnt.requestId).entity(etaResponse.toString()).build();
        }
        String response = "";
        try {
            PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(etaFareEnt.fleetId);
            if (settingMQData.payService) {
                response = paymentRPC.estimateDeliveryFareWithMultiCarType(etaFareEnt);
            } else {
                response = payService.estimateDeliveryFareWithMultiCarType(etaFareEnt);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        logger.debug(etaFareEnt.requestId + " - estimateDeliveryFareWithMultiCarTypeResponse : " + response);
        return Response.status(200).header("x-request-id", etaFareEnt.requestId).entity(response).build();
    }

    @Path("api/paymentgateway/webhookApple")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response webhookApple(@HeaderParam("x-request-id") String requestId, AppleNotifyEnt appleNotifyEnt) throws IOException {
        appleNotifyEnt.setRequestId(requestId);
        logger.debug(appleNotifyEnt.requestId + " - webhookApple: " + gson.toJson(appleNotifyEnt));
        String responseStr = payBusiness.webhookApple(appleNotifyEnt);
        logger.debug(appleNotifyEnt.requestId + " - webhookAppleResponse : " + responseStr);
        return Response.status(200).entity(responseStr).build();
    }

    /**
     * checkConnectStatus
     *
     * @param stripeConnectEnt
     * @return
     */
    @Path("api/paymentgateway/checkConnectStatus")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response estimateDeliveryFareWithMultiCarType(@HeaderParam("x-request-id") String requestId, StripeConnectEnt stripeConnectEnt) {
        stripeConnectEnt.setRequestId(requestId);
        logger.debug(stripeConnectEnt.requestId + " - stripeConnectEnt : " + gson.toJson(stripeConnectEnt));
        String response = "";
        try {
            PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(stripeConnectEnt.fleetId);
            if (settingMQData.payService) {
                response = paymentRPC.checkConnectStatus(stripeConnectEnt);
            } else {
                response = payBusiness.checkConnectStatus(stripeConnectEnt);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        logger.debug(stripeConnectEnt.requestId + " - stripeConnectEntResponse : " + response);
        return Response.status(200).header("x-request-id", stripeConnectEnt.requestId).entity(response).build();
    }

    @Path("api/paymentgateway/getStripeWalletData")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getStripeApplePayData(@HeaderParam("x-request-id") String requestId, PaymentEnt paymentEnt) throws IOException {
        paymentEnt.setRequestId(requestId);
        logger.debug(paymentEnt.requestId + " - getStripeWalletData : " + gson.toJson(paymentEnt));
        String responseStr = "";
        PaymentSettingMQ settingMQData = settingMQ.getSettingMQ(paymentEnt.fleetId);
        if (settingMQData.payBusiness) {
            responseStr = paymentRPC.getStripeWalletData(paymentEnt);
        } else {
            responseStr = payBusiness.getStripeWalletData(paymentEnt);
        }
        logger.debug(paymentEnt.requestId + " - getStripeWalletDataResponse : " + responseStr);
        return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }

    @Path("api/paymentgateway/estimateFareForThirdParty")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response estimateFareForThirdParty(@HeaderParam("x-request-id") String requestId, ETAThirdPartyEnt etaFareEnt) throws IOException {
        etaFareEnt.setRequestId(requestId);
        logger.debug(etaFareEnt.requestId + " - estimateFareForThirdParty : " + gson.toJson(etaFareEnt));
        String responseStr = payService.estimateFareForThirdParty(etaFareEnt);
        logger.debug(etaFareEnt.requestId + " - estimateFareForThirdPartyResponse : " + responseStr);
        return Response.status(200).header("x-request-id", etaFareEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }

    @Path("api/paymentgateway/getPaymentLink")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getPaymentLink(@HeaderParam("x-request-id") String requestId, PaymentEnt paymentEnt) throws IOException {
        paymentEnt.setRequestId(requestId);
        logger.debug(paymentEnt.requestId + " - getPaymentLink : " + gson.toJson(paymentEnt));
        String responseStr = payBusiness.getPaymentLink(paymentEnt);
        logger.debug(paymentEnt.requestId + " - getPaymentLinkResponse : " + responseStr);
        return Response.status(200).header("x-request-id", paymentEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }

    @Path("api/paymentgateway/chargeCustomerForInvoice")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response chargeCustomerForInvoice(@HeaderParam("x-request-id") String requestId, InvoicePayEnt invoicePayEnt) throws IOException {
        invoicePayEnt.setRequestId(requestId);
        logger.debug(invoicePayEnt.requestId + " - chargeCustomerForInvoice : " + gson.toJson(invoicePayEnt));
        String responseStr = payBusiness.chargeCustomerForInvoice(invoicePayEnt);
        logger.debug(invoicePayEnt.requestId + " - chargeCustomerForInvoiceResponse : " + responseStr);
        return Response.status(200).header("x-request-id", invoicePayEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }

    @Path("api/paymentgateway/getPaymentIntentForInvoice")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getPaymentIntentForInvoice(@HeaderParam("x-request-id") String requestId, InvoicePayEnt invoicePayEnt) throws IOException {
        invoicePayEnt.setRequestId(requestId);
        logger.debug(invoicePayEnt.requestId + " - getPaymentIntentForInvoice : " + gson.toJson(invoicePayEnt));
        String responseStr = payBusiness.getPaymentIntentForInvoice(invoicePayEnt);
        logger.debug(invoicePayEnt.requestId + " - getPaymentIntentForInvoiceResponse : " + responseStr);
        return Response.status(200).header("x-request-id", invoicePayEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }

    @Path("api/paymentgateway/getPaymentMethodForInvoice")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getPaymentMethodForInvoice(@HeaderParam("x-request-id") String requestId, InvoicePayEnt invoicePayEnt) throws IOException {
        invoicePayEnt.setRequestId(requestId);
        logger.debug(invoicePayEnt.requestId + " - getPaymentMethodForInvoice : " + gson.toJson(invoicePayEnt));
        String responseStr = payBusiness.getPaymentMethodForInvoice(invoicePayEnt);
        logger.debug(invoicePayEnt.requestId + " - getPaymentMethodForInvoiceResponse : " + responseStr);
        return Response.status(200).header("x-request-id", invoicePayEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }

    @Path("api/paymentgateway/prePaidForBooking")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response prePaidForBooking(@HeaderParam("x-request-id") String requestId, PrePaidEnt prePaidEnt) throws IOException {
        prePaidEnt.setRequestId(requestId);
        logger.debug(prePaidEnt.requestId + " - prePaidForBooking : " + gson.toJson(prePaidEnt));
        String responseStr = payBusiness.prePaidOrPostPaid(prePaidEnt);
        logger.debug(prePaidEnt.requestId + " - prePaidForBookingResponse : " + responseStr);
        return Response.status(200).header("x-request-id", prePaidEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }

    @Path("api/paymentgateway/getPaymentActivities")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getPaymentActivities(@HeaderParam("x-request-id") String requestId, PrePaidEnt prePaidEnt) throws IOException {
        prePaidEnt.setRequestId(requestId);
        logger.debug(prePaidEnt.requestId + " - getPaymentActivities : " + gson.toJson(prePaidEnt));
        String responseStr = payBusiness.getPaymentActivities(prePaidEnt);
        logger.debug(prePaidEnt.requestId + " - getPaymentActivitiesResponse : " + responseStr);
        return Response.status(200).header("x-request-id", prePaidEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }

    @Path("api/paymentgateway/getPaymentLinkForPrepaid")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getPaymentLinkForPrepaid(@HeaderParam("x-request-id") String requestId, PrePaidEnt prePaidEnt) throws IOException {
        prePaidEnt.setRequestId(requestId);
        prePaidEnt.type = "prepaid";
        logger.debug(prePaidEnt.requestId + " - getPaymentLinkForPrepaid : " + gson.toJson(prePaidEnt));
        String responseStr = payBusiness.getPaymentLinkForPrepaidOrPostpaid(prePaidEnt);
        logger.debug(prePaidEnt.requestId + " - getPaymentLinkForPrepaidResponse : " + responseStr);
        return Response.status(200).header("x-request-id", prePaidEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }

    @Path("api/paymentgateway/deactivatePrepaidPaymentLink")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response deactivatePrepaidPaymentLink(@HeaderParam("x-request-id") String requestId, PrePaidEnt prePaidEnt) throws IOException {
        prePaidEnt.setRequestId(requestId);
        logger.debug(prePaidEnt.requestId + " - deactivatePrepaidPaymentLink : " + gson.toJson(prePaidEnt));
        String responseStr = payBusiness.deactivatePrepaidPaymentLink(prePaidEnt);
        logger.debug(prePaidEnt.requestId + " - deactivatePrepaidPaymentLinkResponse : " + responseStr);
        return Response.status(200).header("x-request-id", prePaidEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }

    @Path("api/paymentgateway/deactivatePaymentLinkForBooking")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response deactivatePaymentLinkForBooking(@HeaderParam("x-request-id") String requestId, PrePaidEnt prePaidEnt) throws IOException {
        prePaidEnt.setRequestId(requestId);
        logger.debug(prePaidEnt.requestId + " - deactivatePaymentLinkForBooking : " + gson.toJson(prePaidEnt));
        String responseStr = payBusiness.deactivatePaymentLinkForBooking(prePaidEnt);
        logger.debug(prePaidEnt.requestId + " - deactivatePaymentLinkForBookingResponse : " + responseStr);
        return Response.status(200).header("x-request-id", prePaidEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }

    @Path("api/paymentgateway/getPaymentLinkForPostpaid")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response getPaymentLinkForPostpaid(@HeaderParam("x-request-id") String requestId, PrePaidEnt prePaidEnt) throws IOException {
        prePaidEnt.setRequestId(requestId);
        prePaidEnt.type = "postpaid";
        logger.debug(prePaidEnt.requestId + " - getPaymentLinkForPostpaid : " + gson.toJson(prePaidEnt));
        String responseStr = payBusiness.getPaymentLinkForPrepaidOrPostpaid(prePaidEnt);
        logger.debug(prePaidEnt.requestId + " - getPaymentLinkForPostpaidResponse : " + responseStr);
        return Response.status(200).header("x-request-id", prePaidEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }

    @Path("api/paymentgateway/postPaidForBooking")
    @RolesAllowed(DIS_ROLE)
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response postPaidForBooking(@HeaderParam("x-request-id") String requestId, PrePaidEnt prePaidEnt) throws IOException {
        prePaidEnt.setRequestId(requestId);
        prePaidEnt.type = "postpaid";
        logger.debug(prePaidEnt.requestId + " - postPaidForBooking : " + gson.toJson(prePaidEnt));
        String responseStr = payBusiness.prePaidOrPostPaid(prePaidEnt);
        logger.debug(prePaidEnt.requestId + " - prePaidForBookingResponse : " + responseStr);
        return Response.status(200).header("x-request-id", prePaidEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }


    @Path("api/paymentgateway/genericCreditForm")
    @RolesAllowed({DIS_ROLE, CC_ROLE})
    @POST
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="", hidden = true)
    public Response genericCreditForm(@HeaderParam("x-request-id") String requestId, AddCardEnt addCardEnt) throws IOException {
        addCardEnt.setRequestId(requestId);
        logger.debug(addCardEnt.requestId + " - genericCreditForm : " + gson.toJson(addCardEnt));
        String responseStr = payBusiness.genericCreditForm(addCardEnt);
        logger.debug(addCardEnt.requestId + " - genericCreditFormResponse : " + responseStr);
        return Response.status(200).header("x-request-id", addCardEnt.requestId).entity(responseStr).header("x-request-id", requestId).build();
    }
}
