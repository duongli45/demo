package com.pg.paymentgateway;

import com.google.gson.Gson;
import com.qupworld.paymentgateway.controllers.GatewayUtil.StripeIntentUtil;
import com.qupworld.paymentgateway.controllers.GatewayUtil.StripeUtil;
import com.qupworld.paymentgateway.entities.*;
import com.qupworld.paymentgateway.models.*;
import com.qupworld.paymentgateway.models.mongo.collections.fleet.Fleet;
import com.pg.util.KeysUtil;
import com.pg.util.ObjResponse;
import com.stripe.model.Event;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URI;
import java.util.List;

/**
 * Created by thuanho on 24/06/2020.
 */
@SuppressWarnings("unchecked")
public class ProxyUtil {

    private final static Logger logger = LogManager.getLogger(ProxyUtil.class);

    private Gson gson;
    private MongoDao mongoDao;
    private SQLDao sqlDao;
    private RedisDao redisDao;

    ProxyUtil(){
        gson = new Gson();
        sqlDao = new SQLDaoImpl();
        mongoDao = new MongoDaoImpl();
        redisDao = new RedisImpl();
    }

    String applePayAsyncPayEase(ApplePayAsyncEnt applePayAsyncEnt, String requestId){
        String fleetId = "";
        try {
            String v_oid = applePayAsyncEnt.v_oid != null ? applePayAsyncEnt.v_oid : "";
            if (!v_oid.isEmpty()) {
                String[] resultoid = v_oid.split("[|][_][|]");
                if (resultoid.length > 0) {
                    String[] arr_oid = resultoid[0].split("-");
                    if (arr_oid.length > 2) {
                        String merchantId = arr_oid[1];
                        Fleet fleet = mongoDao.getFleetFromPayEaseMerchant(merchantId);
                        fleetId = fleet != null ? fleet.fleetId : "";
                    }
                }
            }
            logger.debug(requestId + " - fleetId: " + fleetId);
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + getError(ex));
        }
        return fleetId;
    }

    String completePayfort3DSTransaction(PayfortResponseEnt payfortResponseEnt, String requestId){
        String fleetId = "";
        try {
            String merchant_reference = "";
            String data = gson.toJson(payfortResponseEnt);
            if (data.contains(KeysUtil.CALLBACK_URL + "?")) {
                List<NameValuePair> params = URLEncodedUtils.parse(new URI(data), "UTF-8");
                for (NameValuePair param : params) {
                    if (param.getName().equals("merchant_reference")) merchant_reference = param.getValue();
                }
            } else {
                JSONObject json = (JSONObject)new org.json.simple.parser.JSONParser().parse(data);
                merchant_reference = json.get("merchant_reference") != null ? json.get("merchant_reference").toString() : "";
            }
            logger.debug(requestId + " - merchant_reference: " + merchant_reference);
            if (payfortResponseEnt.command.toUpperCase().equals("AUTHORIZATION")) {
                CreditEnt creditEnt =  gson.fromJson(redisDao.get3DSData(requestId, merchant_reference, "3dscredit"), CreditEnt.class);
                fleetId = creditEnt != null ? creditEnt.fleetId : "";
            } else if (payfortResponseEnt.command.toUpperCase().equals("PURCHASE")) {
                PaymentEnt paymentEnt =  gson.fromJson(redisDao.get3DSData(requestId, merchant_reference, "3dsinput"), PaymentEnt.class);
                fleetId = paymentEnt != null ? paymentEnt.fleetId : "";
            }
            logger.debug(requestId + " - fleetId: " + fleetId);
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + getError(ex));
        }
        return fleetId;
    }

    String paymentGCashNotificationURL(TnGNotification tnGNotification, String requestId){
        String fleetId = "";
        try {
            String merchantTransId = tnGNotification.request.body.merchantTransId;
            fleetId = redisDao.getTmp(requestId, merchantTransId+"-fleetId");
            logger.debug(requestId + " - fleetId: " + fleetId);
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + getError(ex));
        }
        return fleetId;
    }

    String paymentMolPayNotificationURL(PaymentNotificationEnt paymentNotificationEnt, String requestId) {
        String fleetId = "";
        try {
            String orderId = paymentNotificationEnt.orderid != null ? paymentNotificationEnt.orderid : "";
            String tmpData = redisDao.getTmp(paymentNotificationEnt.requestId, orderId + "-topup");
            TopupEnt topupEnt = gson.fromJson(tmpData, TopupEnt.class);
            fleetId = topupEnt != null ? topupEnt.fleetId : "";
            logger.debug(requestId + " - fleetId: " + fleetId);
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + getError(ex));
        }
        return fleetId;
    }

    public String paymentNotification(PaymentNotificationEnt paymentNotificationEnt, String requestId) {
        String fleetId = "";
        try {
            // detect gateway
            String gateway = paymentNotificationEnt.gateway != null ? paymentNotificationEnt.gateway : "";
            if (gateway.isEmpty()) {
                String transactionIdentifier = paymentNotificationEnt.TransactionIdentifier != null ? paymentNotificationEnt.TransactionIdentifier : "";
                if (!transactionIdentifier.isEmpty())
                    gateway = KeysUtil.FAC;
            }
            if (gateway.isEmpty()) {
                String fort_id = paymentNotificationEnt.fort_id != null ? paymentNotificationEnt.fort_id : "";
                if (!fort_id.isEmpty())
                    gateway = KeysUtil.PAYFORT;
            }
            if (gateway.isEmpty()) {
                String tranID = paymentNotificationEnt.tranID != null ? paymentNotificationEnt.tranID : "";
                if (!tranID.isEmpty())
                    gateway = KeysUtil.MOLPAY;
            }

            if (gateway.isEmpty()) {
                String gCashMerchantTransId = paymentNotificationEnt.gCashMerchantTransId != null ? paymentNotificationEnt.gCashMerchantTransId : "";
                if (!gCashMerchantTransId.isEmpty())
                    gateway = KeysUtil.GCASH;
            }

            if (gateway.isEmpty()) {
                String PaRes = paymentNotificationEnt.PaRes != null ? paymentNotificationEnt.PaRes : "";
                if (!PaRes.isEmpty())
                    gateway = KeysUtil.FIRSTDATA;
            }

            if (gateway.isEmpty()) {
                String ServiceID = paymentNotificationEnt.ServiceID != null ? paymentNotificationEnt.ServiceID : "";
                if (!ServiceID.isEmpty())
                    gateway = KeysUtil.EGHL;
            }

            switch (gateway) {
                case KeysUtil.FAC : {
                    String orderIdentifier = paymentNotificationEnt.OrderIdentifier != null ? paymentNotificationEnt.OrderIdentifier : "";
                    fleetId = redisDao.getTmp(requestId, "queue-"+orderIdentifier);
                }
                break;
                case KeysUtil.FIRSTDATA : {
                    String MD = paymentNotificationEnt.MD != null ? paymentNotificationEnt.MD : "";
                    if (MD.length() > 18) {
                        String key = MD.substring(MD.length() - 18);
                        fleetId = redisDao.getTmp(requestId, "queue-"+key);
                    }
                }
                break;
                case KeysUtil.EGHL : {
                    String storeId = paymentNotificationEnt.OrderNumber != null ? paymentNotificationEnt.OrderNumber : "";
                    fleetId = redisDao.getTmp(requestId, "queue-"+storeId);
                }
                break;
                case KeysUtil.TSYS : {
                    String storeId = paymentNotificationEnt.OrderNumber != null ? paymentNotificationEnt.OrderNumber : "";
                    fleetId = redisDao.getTmp(requestId, "queue-"+storeId);
                }
                break;

            }
            logger.debug(requestId + " - fleetId: " + fleetId);
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + getError(ex));
        }
        return fleetId;
    }

    String paymentPingPongNotificationURL(PaymentNotificationEnt paymentNotificationEnt, String requestId) {
        String fleetId = "";
        try {
            String merchantTransactionId = paymentNotificationEnt.merchantTransactionId != null ? paymentNotificationEnt.merchantTransactionId : "";
            fleetId = redisDao.getTmp(paymentNotificationEnt.requestId, merchantTransactionId);
            logger.debug(requestId + " - fleetId: " + fleetId);
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + getError(ex));
        }
        return fleetId;
    }

    String paymentSenangpayNotificationURL(PaymentNotificationEnt paymentNotificationEnt, String requestId) {
        String fleetId = "";
        try {
            String order_id = paymentNotificationEnt.order_id != null ? paymentNotificationEnt.order_id : "";
            String cacheData = redisDao.getTmp(paymentNotificationEnt.requestId, KeysUtil.SENANGPAY + ":" + order_id);
            CreditEnt creditEnt = gson.fromJson(cacheData, CreditEnt.class);
            fleetId = creditEnt != null ? creditEnt.fleetId : "";
            logger.debug(requestId + " - fleetId: " + fleetId);
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + getError(ex));
        }
        return fleetId;
    }

    String paymentTnGNotificationURL(TnGNotification tnGNotification, String requestId) {
        String fleetId = "";
        try {
            String TnGMerchantTransId = tnGNotification.request.body.merchantTransId;
            String storedJson = redisDao.getTmp(requestId, TnGMerchantTransId);
            JSONObject objTnG = (JSONObject)new org.json.simple.parser.JSONParser().parse(storedJson);
            PaymentEnt paymentEnt = gson.fromJson(objTnG.get("paymentEnt").toString(), PaymentEnt.class);
            fleetId = paymentEnt != null ? paymentEnt.fleetId : "";
            logger.debug(requestId + " - fleetId: " + fleetId);
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + getError(ex));
        }
        return fleetId;
    }

    String settlementMolPayNotificationURL(SettlementNotificationEnt settlementNotificationEnt, String requestId) {
        String fleetId = "";
        try {
            String referenceId = settlementNotificationEnt.reference_id != null ? settlementNotificationEnt.reference_id : "";
            WalletTransfer walletTransfer = sqlDao.getWalletTransfer(referenceId);
            fleetId = walletTransfer != null ? walletTransfer.fleetId : "";
            logger.debug(requestId + " - fleetId: " + fleetId);
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + getError(ex));
        }
        return fleetId;
    }

    String webhookStripe(Event event, String requestId) {
        String fleetId = "";
        try {
            StripeUtil stripeUtil = new StripeUtil(requestId);
            ObjResponse wbData = stripeUtil.webhookStripe(requestId, event);
            logger.debug(requestId + " - wbData: " + wbData.toString());
            if (wbData.response != null) {
                StripeIntentUtil sUtil = new  StripeIntentUtil();
                fleetId = sUtil.getAuthenRequired("queue-"+wbData.response.toString());
            }
        } catch (Exception ex) {
            logger.debug(requestId + " - exception: " + getError(ex));
        }
        return fleetId;
    }


    private String getError(Exception ex) {
        StringWriter errors = new StringWriter();
        ex.printStackTrace(new PrintWriter(errors));
        return gson.toJson(errors.toString());
    }
}
