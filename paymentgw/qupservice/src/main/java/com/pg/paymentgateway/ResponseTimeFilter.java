package com.pg.paymentgateway;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Enumeration;

public class ResponseTimeFilter implements Filter {

    Logger logger = LogManager.getLogger(ResponseTimeFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // Initialization code if needed
    }

    @SuppressWarnings("unchecked")
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        try {
            long startTime = System.currentTimeMillis();
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            // Continue the request-response chain
            chain.doFilter(request, response);
            long endTime = System.currentTimeMillis();
            long responseTime = endTime - startTime;
            final Enumeration<String> headerNames = httpRequest.getHeaderNames();
            String requestId = "";
            while (headerNames.hasMoreElements()) {
                String key = headerNames.nextElement();
                if (key.equalsIgnoreCase("x-request-id")) {
                    String value = httpRequest.getHeader(key);
                    requestId = value;
                    break;
                }
            }
            // Log the response time
            logger.debug("API: " + httpRequest.getRequestURI() + " - " + requestId + " took: " + responseTime);
        } catch (Exception e) {

        }
    }

    @Override
    public void destroy() {
        // Cleanup code if needed
    }
}
