package com.pg.service;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by qup on 18/05/2018.
 */
public class ShutdownHookListener implements ServletContextListener {

  @Override
  public void contextInitialized(ServletContextEvent servletContext) {
    // do nothing
  }

  @Override
  public void contextDestroyed(ServletContextEvent servletContext) {
    /*        try{
            PayServiceRPC serviceRPC = PayServiceRPC.getInstance();
            serviceRPC.close();
            System.out.println("Destroy Pay Service DONE !!!!");
        }catch (Exception e){
            System.out.println("============== PayServiceRPC.commander close ERROR: " + e.getMessage());
        }

        try{
            PayBusinessRPC businessRPC = PayBusinessRPC.getInstance();
            businessRPC.close();
            System.out.println("Destroy Pay Business DONE !!!!");
        }catch (Exception e){
            System.out.println("============== PayBusinessRPC.commander close ERROR: " + e.getMessage());
        }*/

    try {
      PaymentRPC paymentRPC = PaymentRPC.getInstance();
      paymentRPC.close();
      System.out.println("Destroy paymentRPC DONE !!!!");
    } catch (Exception e) {
      System.out.println(
        "============== PaymentRPC.commander close ERROR: " + e.getMessage()
      );
    }
    /*        try{
            MailService mailService = MailService.getInstance();
            mailService.close();
            System.out.println("Destroy Mail Service DONE !!!!");
        }catch (Exception e){
            System.out.println("============== MailService.commander close ERROR");
            e.printStackTrace();
        }*/
    /*
        try{
            DAOManager daoManager = DAOManager.getInstance();
            daoManager.close();
            System.out.println("Destroy Mongo client DONE !!!!");
        }catch (Exception e){
            System.out.println("============== Mongo client close ERROR");
            e.printStackTrace();
        }*/

    /*        try{
            // Now deregister JDBC drivers in this context's ClassLoader:
            // Get the webapp's ClassLoader
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            // Loop through all drivers
            Enumeration<Driver> drivers = DriverManager.getDrivers();
            while (drivers.hasMoreElements()) {
                Driver driver = drivers.nextElement();
                if (driver.getClass().getClassLoader() == cl) {
                    // This driver was registered by the webapp's ClassLoader, so deregister it:
                    try {
                        System.out.println("Deregistering JDBC driver " + driver);
                        DriverManager.deregisterDriver(driver);
                    } catch (SQLException ex) {
                        System.out.println("Error deregistering JDBC driver " + driver);
                    }
                } else {
                    // driver was not registered by the webapp's ClassLoader and may be in use elsewhere
                    System.out.println("Not deregistering JDBC driver " + driver +" as it does not belong to this webapp's ClassLoader");
            }
            }
            SQLDaoImpl sqlDao = SQLDaoImpl.getInstance();
            sqlDao.close();
            System.out.println("Destroy SQL DONE !!!!");
        }catch (Exception e){
            System.out.println("============== SQL close ERROR");
            e.printStackTrace();
        }

        try{
            RedisImpl redis = RedisImpl.getInstance();
            redis.close();
            System.out.println("Destroy Redis DONE !!!!");
        }catch (Exception e){
            System.out.println("============== Redis close ERROR");
            e.printStackTrace();
        }

        try {
            Unirest.shutdown();
            System.out.println("Destroy Unirest DONE !!!!");
        } catch (IOException e) {
            System.out.println("============== Unirest close ERROR");
            e.printStackTrace();
        }*/
  }
}
