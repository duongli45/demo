package com.pg.service;

import com.devebot.opflow.OpflowCommander;
import com.devebot.opflow.OpflowPromExporter;
import com.qupworld.paymentgateway.entities.*;
import com.qupworld.paymentgateway.entities.FawryFormEnt.FawryEnt;
import com.qupworld.paymentgateway.entities.TopupAPI.RegisterAuthEnt;
import com.qupworld.paymentgateway.entities.TopupAPI.VerifyEnt;
import com.pg.util.CommonUtils;
import com.pg.util.ObjResponse;
import com.pg.util.SlackAPI;
import com.qupworld.service.PayBusiness;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by myasus on 7/3/20.
 */
public class PaymentRPC {

  private static final Logger logger = LogManager.getLogger(PaymentRPC.class);
  public static PaymentRPC instance = null;
  private SlackAPI slack = new SlackAPI();
  private OpflowCommander commander;
  public PayBusiness payBusiness;
  public PayService payService;
  private static Object flag = "FLAG";

  public static PaymentRPC getInstance() {
    OpflowPromExporter.hook();
    try {
      synchronized (flag) {
        if (instance == null) {
          instance = new PaymentRPC();
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return instance;
  }

  private PaymentRPC() {
    try {
      // if (commander == null) {
      //   String opflowPath =
      //     "file://" +
      //     System.getProperty("catalina.base") +
      //     "/conf/qup-master.properties";
      //   logger.debug("qup-master:   " + opflowPath);
      //   commander = OpflowBuilder.createCommander(opflowPath);
      //   payBusiness =
      //     commander.registerType(
      //       "business",
      //       PayBusiness.class,
      //       new PayBusinessImpl()
      //     );
      //   payService =
      //     commander.registerType(
      //       "service",
      //       PayService.class,
      //       new PayServiceImpl()
      //     );
      //   commander.serve();
      // }
    } catch (Exception ex) {
      logger.debug("Can't not connect to Queue " + CommonUtils.getError(ex));
      slack.sendSlack(
        "Can't not connect to Queue " + CommonUtils.getError(ex),
        "URGREN",
        ""
      );
    }
  }

  public String acceptBooking(HydraBookingEnt hydraBookingEnt) {
    return payBusiness.acceptBooking(hydraBookingEnt);
  }

  /***************************************************************/

  public String hashPmtNotifyRq(FawryEnt fawryPayEnt, String requestId) {
    try {
      return payBusiness.hashPmtNotifyRq(fawryPayEnt, requestId);
    } catch (Exception ex) {
      ex.printStackTrace();
      logger.debug(requestId + " hashPmtNotifyRq error: " + ex.toString());
      slack.sendSlack(
        "hashPmtNotifyRq error " + ex.getMessage(),
        "ERROR",
        requestId
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String notificationFawry(FawryEnt fawryPayEnt, String requestId) {
    try {
      return payBusiness.notificationFawry(fawryPayEnt, requestId);
    } catch (Exception ex) {
      ex.printStackTrace();
      logger.debug(requestId + " notificationFawry error: " + ex.toString());
      slack.sendSlack(
        "notificationFawry error " + ex.getMessage(),
        "ERROR",
        requestId
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String getClientSecret(ClientSecretEnt clientSecretEnt) {
    try {
      return payBusiness.getClientSecret(clientSecretEnt);
    } catch (Exception ex) {
      ex.printStackTrace();
      logger.debug(clientSecretEnt.requestId + " getClientSecret error: " + ex.toString());
      slack.sendSlack(
        "getClientSecret error " + ex.getMessage(),
        "ERROR",
              clientSecretEnt.requestId
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String cancelPaymentInterWithBookId(String bookId, String requestId) {
    try {
      return payBusiness.cancelPaymentInterWithBookId(bookId, requestId);
    } catch (Exception ex) {
      ex.printStackTrace();
      logger.debug(
        requestId + " cancelPaymentInterWithBookId error: " + ex.toString()
      );
      slack.sendSlack(
        "cancelPaymentInterWithBookId error " + ex.getMessage(),
        "ERROR",
        requestId
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String confirmPreAuth(String paymentInterId, String requestId) {
    try {
      return payBusiness.confirmPreAuth(paymentInterId, requestId);
    } catch (Exception ex) {
      ex.printStackTrace();
      logger.debug(requestId + " confirmPreAuth error: " + ex.toString());
      slack.sendSlack(
        "confirmPreAuth error " + ex.getMessage(),
        "ERROR",
        requestId
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String confirmStripeData(String bookId) {
    try {
      return payBusiness.confirmStripeData(bookId);
    } catch (Exception ex) {
      ex.printStackTrace();
      logger.debug(bookId + " confirmStripeData error: " + ex.toString());
      slack.sendSlack(
        "confirmStripeData error " + ex.getMessage(),
        "ERROR",
        bookId
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String getWebhookStripe(WebhookCreateEnt webhookCreateEnt) {
    return payBusiness.getWebhookStripe(webhookCreateEnt);
  }

  public String synWebhookStripe(WebhookCreateEnt webhookCreateEnt) {
    return payBusiness.synWebhookStripe(webhookCreateEnt);
  }

  public String createWebhookStripe(WebhookCreateEnt webhookCreateEnt) {
    return payBusiness.createWebhookStripe(webhookCreateEnt);
  }

  public String webhookStripe(String eventstr, String requestId) {
    return payBusiness.webhookStripe(eventstr, requestId);
  }

  public String addCreditToken(CreditEnt creditForm) {
    //c
    try {
      return payBusiness.addCreditToken(creditForm);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String createCreditTokenForDriver(CreditEnt creditForm) {
    return payBusiness.createCreditTokenForDriver(creditForm);
  }

  public String addCreditTokenForCustomer(CreditEnt creditForm) {
    return payBusiness.addCreditTokenForCustomer(creditForm);
  }

  public String addCreditTokenForCorporate(CreditEnt creditForm) {
    return payBusiness.addCreditTokenForCorporate(creditForm);
  }

  public String addCreditTokenForFleetOwner(CreditEnt creditForm) {
    return payBusiness.addCreditTokenForFleetOwner(creditForm);
  }

  public String deleteCorporateToken(CreditDeleteEnt creditDeleteEnt) {
    return payBusiness.deleteCorporateToken(creditDeleteEnt);
  }

  public String deleteCreditToken(CreditDeleteEnt creditDeleteEnt) {
    return payBusiness.deleteCreditToken(creditDeleteEnt);
  }

  public String deleteCreditTokenForFleetOwner(
    CreditDeleteEnt creditDeleteEnt
  ) {
    return payBusiness.deleteCreditTokenForFleetOwner(creditDeleteEnt);
  }

  public String addTicket(AddTicketEnt addTicketEnt) {
    return payBusiness.addTicket(addTicketEnt);
  }

  public String payByCash(PaymentEnt paymentEnt) {
    return payBusiness.payByCash(paymentEnt);
  }

  public String payByPaxWallet(PaymentEnt paymentEnt) {
    return payBusiness.payByPaxWallet(paymentEnt);
  }

  public String payByApplePay(PaymentEnt paymentEnt) {
    return payBusiness.payByApplePay(paymentEnt);
  }

  public String storePaymentData(PaymentEnt paymentEnt) {
    return payBusiness.storePaymentData(paymentEnt);
  }

  public String payByDirectBilling(PaymentEnt paymentEnt) {
    return payBusiness.payByDirectBilling(paymentEnt);
  }

  public String payByPrePaid(PaymentEnt paymentEnt) {
    return payBusiness.payByPrePaid(paymentEnt);
  }

  public String payByFleetCard(PaymentEnt paymentEnt) {
    return payBusiness.payByFleetCard(paymentEnt);
  }

  public String payByCreditExternal(PaymentEnt paymentEnt) {
    return payBusiness.payByCreditExternal(paymentEnt);
  }

  public String payByCreditToken(PaymentEnt paymentEnt) {
    return payBusiness.payByCreditToken(paymentEnt);
  }

  public String payByInputCard(PaymentEnt paymentEnt) {
    return payBusiness.payByInputCard(paymentEnt);
  }

  public String preAuthPayment(PreAuthEnt preAuthEnt) {
    try {
      return payBusiness.preAuthPayment(preAuthEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String voidPreAuthBeforePayment(VoidEnt voidEnt) {
    return payBusiness.voidPreAuthBeforePayment(voidEnt);
  }

  public String cancelBooking(CancelBookingEnt cancelBookingEnt) {
    return payBusiness.cancelBooking(cancelBookingEnt);
  }

  public String noShow3rd(CancelBookingEnt cancelBookingEnt) {
    return payBusiness.noShow3rd(cancelBookingEnt);
  }

  public String completedWithoutPayment(IncidentEnt incidentEnt) {
    return payBusiness.completedWithoutPayment(incidentEnt);
  }

  public String createACHToken(ACHEnt achEnt, String type) {
    return payBusiness.createACHToken(achEnt, "add");
  }

  public String payFromCommandCenter(PaymentEnt paymentEnt) {
    return payBusiness.payFromCommandCenter(paymentEnt);
  }

  public String payFrom3rd(PaymentEnt paymentEnt) {
    return payBusiness.payFrom3rd(paymentEnt);
  }

  public String paymentDetail(String bookId, String requestId) {
    return payBusiness.paymentDetail(bookId, requestId);
  }

  public String ticketDetail(String requestId, String bookId, String appType) {
    return payBusiness.ticketDetail(requestId, bookId, appType);
  }

  public String payToDriver(PayToDriverEnt payToDriverEnt) {
    return payBusiness.payToDriver(payToDriverEnt);
  }

  public String chargeFleetOwner(ChargeFleetOwner chargeFleetOwner) {
    return payBusiness.chargeFleetOwner(chargeFleetOwner);
  }

  public String topupPrepaid(TopupEnt topupEnt) {
    try {
      return payBusiness.topupPrepaid(topupEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String topUpRefereeWallet(TopupPaxWalletEnt topupEnt) {
    try {
      return payBusiness.topUpRefereeWallet(topupEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String addSignature(SignatureForm signatureForm) {
    try {
      return payBusiness.addSignature(signatureForm);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String getEncrypted(String data) {
    try {
      return payBusiness.getEncrypted(data);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String getCreditBalances(CreditBalancesEnt creditBalancesEnt) {
    try {
      return payBusiness.getCreditBalances(creditBalancesEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String topupDriverBalance(
    TopupDriverBalanceEnt topupDriverBalanceEnt
  ) {
    try {
      return payBusiness.topupDriverBalance(topupDriverBalanceEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String topupByWallet(TopupByWalletEnt topupByWalletEnt) {
    try {
      return payBusiness.topupByWallet(topupByWalletEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String topupDriverBalanceFromCC(
    TopupDriverBalanceEnt topupDriverBalanceEnt
  ) {
    try {
      return payBusiness.topupDriverBalance(topupDriverBalanceEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String updateCompletedBooking(UpdateEnt updateEnt) {
    try {
      return payBusiness.updateCompletedBooking(updateEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String checkStripeACHToken(ACHEnt achEnt) {
    try {
      return payBusiness.checkStripeACHToken(achEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String checkUnionpayCard(String fleetId, String cardNumber) {
    try {
      return payBusiness.checkUnionpayCard(fleetId, cardNumber);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String getFirstPayDetail(String requestId, String bookId) {
    try {
      return payBusiness.getFirstPayDetail(requestId, bookId);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String applePayAsyncPayEase(ApplePayAsyncEnt applePayAsyncEnt) {
    try {
      return payBusiness.applePayAsyncPayEase(applePayAsyncEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String updatePaymentStatus(PaymentEnt paymentEnt) {
    try {
      return payBusiness.updatePaymentStatus(paymentEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String completeBookingWithoutCharging(PaymentEnt paymentEnt) {
    try {
      return payBusiness.completeBookingWithoutCharging(paymentEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String paymentAudit(PaymentAudit paymentAudit) {
    try {
      return payBusiness.paymentAudit(paymentAudit);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String checkBBLCorporate(String fleetId, String corporateId) {
    try {
      return payBusiness.checkBBLCorporate(fleetId, corporateId);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String switchToBBLCorporate(
    String fleetId,
    String userId,
    String requestId
  ) {
    try {
      return payBusiness.switchToBBLCorporate(fleetId, userId, requestId);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String getTicket(AddTicketEnt addTicketEnt) {
    try {
      return payBusiness.getTicket(addTicketEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String setDefaultCardForFleetOwner(CreditDeleteEnt creditDeleteEnt) {
    try {
      return payBusiness.setDefaultCardForFleetOwner(creditDeleteEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String generateBillingReport(String requestId, int month, int year) {
    try {
      return payBusiness.generateBillingReport(requestId, month, year);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String updateBillingReport(FleetSubscriptionEnt subscriptionEnt) {
    try {
      return payBusiness.updateBillingReport(subscriptionEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String markAsChargeBillingReport(
    FleetSubscriptionEnt fleetSubscriptionEnt
  ) {
    try {
      return payBusiness.markAsChargeBillingReport(fleetSubscriptionEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String paymentNotification(
    PaymentNotificationEnt paymentNotificationEnt
  ) {
    try {
      return payBusiness.paymentNotification(paymentNotificationEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String paymentMolPayNotificationURL(
    PaymentNotificationEnt paymentNotificationEnt
  ) {
    try {
      return payBusiness.paymentMolPayNotificationURL(paymentNotificationEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String settlementMolPayNotificationURL(
    SettlementNotificationEnt settlementNotificationEnt
  ) {
    try {
      return payBusiness.settlementMolPayNotificationURL(
        settlementNotificationEnt
      );
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String paymentTnGNotificationURL(TnGNotification tnGNotification) {
    try {
      return payBusiness.paymentTnGNotificationURL(tnGNotification);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String paymentGCashNotificationURL(TnGNotification tnGNotification) {
    try {
      return payBusiness.paymentGCashNotificationURL(tnGNotification);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String paymentPingPongNotificationURL(
    PaymentNotificationEnt paymentNotificationEnt
  ) {
    try {
      return payBusiness.paymentPingPongNotificationURL(paymentNotificationEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String preChargeBooking(HydraBookingEnt hydraBookingEnt) {
    try {
      return payBusiness.preChargeBooking(hydraBookingEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String unlockBooking(CancelBookingEnt cancelBookingEnt) {
    try {
      return payBusiness.unlockBooking(cancelBookingEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String updateBankProfile(ACHEnt achEnt, String type) {
    try {
      return payBusiness.updateBankProfile(achEnt, "update");
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String getWithdrawal(WalletWithdrawalEnt walletWithdrawalEnt) {
    try {
      return payBusiness.getWithdrawal(walletWithdrawalEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String withdrawalRequest(WalletTransferEnt walletTransferEnt) {
    try {
      return payBusiness.withdrawalRequest(walletTransferEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String approveWithdrawal(
    WalletTransferEnt walletTransferEnt,
    String action
  ) {
    try {
      return payBusiness.approveWithdrawal(walletTransferEnt, action);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String rejectWithdrawal(
    WalletTransferEnt walletTransferEnt,
    String action
  ) {
    try {
      return payBusiness.rejectWithdrawal(walletTransferEnt, action);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String getWalletInfo(UserWalletEnt userWalletEnt) {
    try {
      return payBusiness.getWalletInfo(userWalletEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String getPaxWallet(UserWalletEnt userWalletEnt) {
    try {
      return payBusiness.getPaxWallet(userWalletEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String topUpPaxWallet(TopupPaxWalletEnt topupPaxWalletEnt) {
    try {
      return payBusiness.topUpPaxWallet(topupPaxWalletEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String removeBankInfo(ACHEnt achEnt) {
    try {
      return payBusiness.removeBankInfo(achEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String tipAfterPayment(TipDriverEnt tipDriverEnt) {
    try {
      return payBusiness.tipAfterPayment(tipDriverEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String clearCustomerOutstanding(OutstandingEnt outstandingEnt) {
    try {
      return payBusiness.clearCustomerOutstanding(outstandingEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String updateDriverBalance(UpdateBalanceEnt updateBalanceEnt) {
    try {
      return payBusiness.updateDriverBalance(updateBalanceEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String getOutstandingAmount(OutstandingEnt outstandingEnt) {
    try {
      return payBusiness.getOutstandingAmount(outstandingEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String getOutstandingTicket(OutstandingEnt outstandingEnt) {
    try {
      return payBusiness.getOutstandingTicket(outstandingEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String payOutstanding(OutstandingEnt outstandingEnt) {
    try {
      return payBusiness.payOutstanding(outstandingEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String retryPayment(PaymentEnt paymentEnt) {
    try {
      return payBusiness.retryPayment(paymentEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String payByWallet(PaymentEnt paymentEnt) {
    try {
      return payBusiness.payByWallet(paymentEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String payfortDirectTransaction(
    PayfortResponseEnt payfortResponseEnt,
    String requestId
  ) {
    try {
      return payBusiness.payfortDirectTransaction(
        payfortResponseEnt,
        requestId
      );
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String paymentSenangpayNotificationURL(
    PaymentNotificationEnt paymentNotificationEnt
  ) {
    try {
      return payBusiness.paymentSenangpayNotificationURL(
        paymentNotificationEnt
      );
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String chargeIntercityBooking(PaymentEnt paymentEnt) {
    try {
      return payBusiness.chargeIntercityBooking(paymentEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String completeIntercityTrip(IntercityTripCompleteEnt completeEnt) {
    try {
      return payBusiness.completeIntercityTrip(completeEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String getFileId(
    String requestId,
    String fleetId,
    String gateway,
    String fileUrl
  ) {
    try {
      return payBusiness.getFileId(requestId, fleetId, gateway, fileUrl);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String getRequiredFields(ACHEnt achEnt) {
    try {
      return payBusiness.getRequiredFields(achEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String getReferralEarning(
    ReferralTransactionEnt referralTransactionEnt
  ) {
    try {
      return payBusiness.getReferralEarning(referralTransactionEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  private String serverError(String strError) {
    ObjResponse error = new ObjResponse();
    error.returnCode = 306;
    error.message = strError;
    return error.toString();
  }

  public void close() {
    commander.close();
  }

  public String migrateTicket(MigrateEnt migrateEnt) {
    try {
      return payBusiness.migrateTicket(migrateEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  /* Pay payService */

  public String estimateFare(ETAFareEnt etaFareEnt) {
    try {
      return payService.estimateFare(etaFareEnt);
    } catch (Exception ex) {
      logger.debug(
        etaFareEnt.requestId + " estimateFare error: " + ex.toString()
      );
      slack.sendSlack(
        "estimateFare error " + ex.getMessage(),
        "ERROR",
        etaFareEnt.requestId
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String estimateDeliveryFare(ETADeliveryEnt etaFareEnt) {
    try {
      return payService.estimateDeliveryFare(etaFareEnt);
    } catch (Exception ex) {
      logger.debug(
        etaFareEnt.requestId + " estimateDeliveryFare error: " + ex.toString()
      );
      slack.sendSlack(
        "estimateDeliveryFare error " + ex.getMessage(),
        "ERROR",
        etaFareEnt.requestId
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String estimateFareWithMultiCarType(ETAFareEnt etaFareEnt) {
    try {
      return payService.estimateFareWithMultiCarType(etaFareEnt);
    } catch (Exception ex) {
      logger.debug(
        etaFareEnt.requestId +
        " estimateFareWithMultiCarType error: " +
        ex.toString()
      );
      slack.sendSlack(
        "estimateFareWithMultiCarType error " + ex.getMessage(),
        "ERROR",
        etaFareEnt.requestId
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String estimateFareWithAllSupliers(ETAFareEnt etaFareEnt) {
    try {
      return payService.estimateFareWithAllSupliers(etaFareEnt);
    } catch (Exception ex) {
      logger.debug(
        etaFareEnt.requestId +
        " estimateFareWithAllSupliers error: " +
        ex.toString()
      );
      slack.sendSlack(
        "estimateFareWithAllSupliers error " + ex.getMessage(),
        "ERROR",
        etaFareEnt.requestId
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String estimateFareCarHailing(ETAFareEnt etaFareEnt) {
    try {
      return payService.estimateFareCarHailing(etaFareEnt);
    } catch (Exception ex) {
      logger.debug(
        etaFareEnt.requestId + " estimateFareCarHailing error: " + ex.toString()
      );
      slack.sendSlack(
        "estimateFareCarHailing error " + ex.getMessage(),
        "ERROR",
        etaFareEnt.requestId
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String estimateFareMeter(ETAFareMeterEnt etaFareMeterEnt) {
    try {
      return payService.estimateFareMeter(etaFareMeterEnt);
    } catch (Exception ex) {
      logger.debug(
        etaFareMeterEnt.requestId + " estimateFareMeter error: " + ex.toString()
      );
      slack.sendSlack(
        "estimateFareMeter error " + ex.getMessage(),
        "ERROR",
        etaFareMeterEnt.requestId
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String estimateFareSharing(ETASharingEnt sharingEnt) {
    try {
      return payService.estimateFareSharing(sharingEnt);
    } catch (Exception ex) {
      logger.debug(
        sharingEnt.requestId + " estimateFareSharing error: " + ex.toString()
      );
      slack.sendSlack(
        "estimateFareSharing error " + ex.getMessage(),
        "ERROR",
        sharingEnt.requestId
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String addPromoCodeUse(PromoEnt promoEnt) {
    try {
      return payService.addPromoCodeUse(promoEnt);
    } catch (Exception ex) {
      logger.debug(
        promoEnt.requestId + " addPromoCodeUse error: " + ex.toString()
      );
      slack.sendSlack(
        "addPromoCodeUse error " + ex.getMessage(),
        "ERROR",
        promoEnt.requestId
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String checkPromoCode(PromoEnt promoEnt) {
    try {
      return payService.checkPromoCode(promoEnt);
    } catch (Exception ex) {
      logger.debug(
        promoEnt.requestId + " checkPromoCode error: " + ex.toString()
      );
      slack.sendSlack(
        "checkPromoCode error " + ex.getMessage(),
        "ERROR",
        promoEnt.requestId
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String addPromoToList(PromoEnt promoEnt) {
    try {
      return payService.addPromoToList(promoEnt);
    } catch (Exception ex) {
      logger.debug(
        promoEnt.requestId + " addPromoToList error: " + ex.toString()
      );
      slack.sendSlack(
        "addPromoToList error " + ex.getMessage(),
        "ERROR",
        promoEnt.requestId
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String deletePromoCodeUse(PromoEnt promoEnt) {
    try {
      return payService.deletePromoCodeUse(promoEnt);
    } catch (Exception ex) {
      logger.debug(
        promoEnt.requestId + " deletePromoCodeUse error: " + ex.toString()
      );
      slack.sendSlack(
        "deletePromoCodeUse error " + ex.getMessage(),
        "ERROR",
        promoEnt.requestId
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String getGeneralSettingAffiliate(ETAFareEnt etaFareEnt) {
    try {
      return payService.getGeneralSettingAffiliate(etaFareEnt);
    } catch (Exception ex) {
      logger.debug(
        etaFareEnt.requestId +
        " getGeneralSettingAffiliate error: " +
        ex.toString()
      );
      slack.sendSlack(
        "getGeneralSettingAffiliate error " + ex.getMessage(),
        "ERROR",
        etaFareEnt.requestId
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String checkFlatRoutes(
    String s,
    CheckFlatRoutesQEnt checkFlatRoutesQEnt
  ) {
    try {
      return payService.checkFlatRoutes(s, checkFlatRoutesQEnt);
    } catch (Exception ex) {
      logger.debug(s + " checkFlatRoutes error: " + ex.toString());
      slack.sendSlack("checkFlatRoutes error " + ex.getMessage(), "ERROR", s);
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String getCancelAmount(CancelBookingEnt cancelBookingEnt) {
    try {
      return payService.getCancelAmount(cancelBookingEnt);
    } catch (Exception ex) {
      logger.debug(
        cancelBookingEnt.requestId + " getCancelAmount error: " + ex.toString()
      );
      slack.sendSlack(
        "getCancelAmount error " + ex.getMessage(),
        "ERROR",
        cancelBookingEnt.requestId
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String getPaymentData(UpdateEnt updateEnt) {
    try {
      return payService.getPaymentData(updateEnt);
    } catch (Exception ex) {
      logger.debug(
        updateEnt.requestId + " getPaymentData error: " + ex.toString()
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String getRushHour(String s, String s1) {
    try {
      return payService.getRushHour(s, s1);
    } catch (Exception ex) {
      logger.debug(s + " getRushHour error: " + ex.toString());
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String checkFareAvailable(ETAFareEnt etaFareEnt) {
    try {
      return payService.checkFareAvailable(etaFareEnt);
    } catch (Exception ex) {
      logger.debug(
        etaFareEnt.requestId + " checkFareAvailable error: " + ex.toString()
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String saveSettlementHistory(
    SettlementHistoryEnt settlementHistoryEnt
  ) {
    try {
      return payService.saveSettlementHistory(settlementHistoryEnt);
    } catch (Exception ex) {
      logger.debug(
        settlementHistoryEnt.requestId +
        " saveSettlementHistory error: " +
        ex.toString()
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String updatePaidToDriver(ReportSettlementEnt reportSettlementEnt) {
    try {
      return payService.updatePaidToDriver(reportSettlementEnt);
    } catch (Exception ex) {
      logger.debug(
        reportSettlementEnt.requestId +
        " updatePaidToDriver error: " +
        ex.toString()
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String reportSettlementHistory(
    ReportSettlementEnt reportSettlementEnt
  ) {
    try {
      return payService.reportSettlementHistory(reportSettlementEnt);
    } catch (Exception ex) {
      logger.debug(
        reportSettlementEnt.requestId +
        " reportSettlementHistory error: " +
        ex.toString()
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String updateAffiliateRate(
    String requestId,
    int value,
    String priceType
  ) {
    try {
      return payService.updateAffiliateRate(requestId, value, priceType);
    } catch (Exception ex) {
      logger.debug(requestId + " updateAffiliateRate error: " + ex.toString());
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String driverTopupPax(DriverTopupPaxEnt driverTopupPaxEnt) {
    try {
      return payBusiness.driverTopupPax(driverTopupPaxEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String updateDriverCashBalance(WalletTransferEnt walletTransferEnt) {
    try {
      return payBusiness.updateDriverCashBalance(walletTransferEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String checkPendingPayment(PaymentChecking paymentChecking) {
    try {
      return payBusiness.checkPendingPayment(paymentChecking);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String getPayoutDriverReport(PayoutReportEnt payoutReportEnt) {
    try {
      return payBusiness.getPayoutDriverReport(payoutReportEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String payoutToDriver(PayoutEnt payoutEnt) {
    try {
      return payBusiness.payoutToDriver(payoutEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String prePaidByWallet(PrePaidByWalletEnt prePaidEnt) {
    try {
      return payBusiness.prePaidByWallet(prePaidEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String cancelWalletBooking(CancelBookingEnt cancelBookingEnt) {
    try {
      return payBusiness.cancelWalletBooking(cancelBookingEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String removeCacheData(ClearCacheEnt clearCacheEnt) {
    try {
      return payBusiness.removeCacheData(clearCacheEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String resetExpiredBonus(SyncEnt syncEnt) {
    try {
      return payBusiness.resetExpiredBonus(syncEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String updateInvalidPayout(PayoutReportEnt payoutReportEnt) {
    try {
      return payBusiness.updateInvalidPayout(payoutReportEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String redeemToPaxWallet(RedeemVoucherEnt redeemVoucherEnt) {
    try {
      return payBusiness.redeemToPaxWallet(redeemVoucherEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String topupDriverCredit(SyncEnt syncEnt) {
    try {
      return payBusiness.topupDriverCredit(syncEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String getPayoutMerchantReport(PayoutReportEnt payoutReportEnt) {
    try {
      return payBusiness.getPayoutMerchantReport(payoutReportEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String payoutToMerchant(PayoutEnt payoutEnt) {
    try {
      return payBusiness.payoutToMerchant(payoutEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String getSecureFieldSession(CreditEnt creditEnt) {
    try {
      return payBusiness.getSecureFieldSession(creditEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String merchantPendingAmount(PayoutReportEnt payoutReportEnt) {
    try {
      return payBusiness.merchantPendingAmount(payoutReportEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String getBankRequiredFields(ACHEnt achEnt) {
    try {
      return payBusiness.getBankRequiredFields(achEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String createBankToken(ACHEnt achEnt) {
    try {
      return payBusiness.createBankToken(achEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String estimateGrossEarning(
    List<ETAGrossEnt> etaGrossEnts,
    String requestId
  ) {
    try {
      return payBusiness.calGrossEarning(etaGrossEnts, requestId);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String topUpTransportDriver(SyncEnt syncEnt) {
    try {
      return payBusiness.topUpTransportDriver(syncEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String completeBooking(PaymentEnt paymentEnt) {
    try {
      return payBusiness.completeBooking(paymentEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String topUpBonusToDriverWallet(SyncEnt syncEnt) {
    try {
      return payBusiness.topUpBonusToDriverWallet(syncEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String removeExpiredBonus(SyncEnt syncEnt) {
    try {
      return payBusiness.removeExpiredBonus(syncEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String completeStreetSharingBooking(PaymentEnt paymentEnt) {
    try {
      return payBusiness.completeStreetSharingBooking(paymentEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String getCheckoutPage(CreditEnt creditEnt) {
    try {
      return payBusiness.getCheckoutPage(creditEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String verifyUser(String requestId, VerifyEnt verifyEnt, String auth) {
    try {
      return payBusiness.verifyUser(requestId, verifyEnt, auth);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String topupByAPI(
    String requestId,
    com.qupworld.paymentgateway.entities.TopupAPI.TopupEnt topupEnt,
    String auth
  ) {
    try {
      return payBusiness.topupByAPI(requestId, topupEnt, auth);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String registerAuthentication(RegisterAuthEnt registerAuthEnt) {
    try {
      return payBusiness.registerAuthentication(registerAuthEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String updatePaymentMethod(UpdateEnt updateEnt) {
    try {
      return payBusiness.updatePaymentMethod(updateEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String updateMerchantWalletBalance(
    MerchantWalletTransactionEnt merchantWalletTransactionEnt
  ) {
    try {
      return payBusiness.updateMerchantWalletBalance(
        merchantWalletTransactionEnt
      );
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String driverTopupDriver(DriverTopupDriverEnt driverTopupDriverEnt) {
    try {
      return payBusiness.driverTopupDriver(driverTopupDriverEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String chargeDriverCancelBooking(CancelBookingEnt cancelBookingEnt) {
    try {
      return payBusiness.chargeDriverCancelBooking(cancelBookingEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

    public String completePartialPayment(PaymentEnt paymentEnt) {
        try{
            return payBusiness.completePartialPayment(paymentEnt);
        } catch (Exception e) {
            e.printStackTrace();
            return serverError(e.getMessage());
        }
    }public String stripeConnectOnboarding(StripeConnectEnt stripeConnectEnt) {
        try{
            return payBusiness.stripeConnectOnboarding(stripeConnectEnt);
        } catch (Exception e) {
            e.printStackTrace();
            return serverError(e.getMessage());
        }
    }

  public String stripeConnectLogin(StripeConnectEnt stripeConnectEnt) {
    try {
      return payBusiness.stripeConnectLogin(stripeConnectEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

    public String disconnectStripe(StripeConnectEnt stripeConnectEnt) {
        try{
            return payBusiness.disconnectStripe(stripeConnectEnt);
        } catch (Exception e) {
            e.printStackTrace();
            return serverError(e.getMessage());
        }
    }



  public String preAuthForHydraReservation(PreAuthEnt preAuthEnt) {
    try {
      return payBusiness.preAuthForHydraReservation(preAuthEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String supplierRejectBooking(CancelBookingEnt cancelEnt) {
    try {
      return payBusiness.supplierRejectBooking(cancelEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String estimateDeliveryFareWithMultiCarType(
    ETADeliveryEnt etaFareEnt
  ) {
    try {
      return payService.estimateDeliveryFareWithMultiCarType(etaFareEnt);
    } catch (Exception ex) {
      logger.debug(
        etaFareEnt.requestId +
        " estimateDeliveryFareWithMultiCarType error: " +
        ex.toString()
      );
      slack.sendSlack(
        "estimateDeliveryFareWithMultiCarType error " + ex.getMessage(),
        "ERROR",
        etaFareEnt.requestId
      );
    }
    ObjResponse objResponse = new ObjResponse();
    objResponse.returnCode = 406;
    return objResponse.toString();
  }

  public String checkConnectStatus(StripeConnectEnt stripeConnectEnt) {
    try {
      return payBusiness.checkConnectStatus(stripeConnectEnt);
    } catch (Exception e) {
      e.printStackTrace();
      return serverError(e.getMessage());
    }
  }

  public String getStripeWalletData(PaymentEnt paymentEnt) {
    return payBusiness.getStripeWalletData(paymentEnt);
  }
}
