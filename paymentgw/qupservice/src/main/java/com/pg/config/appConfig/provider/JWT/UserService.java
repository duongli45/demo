package com.pg.config.appConfig.provider.JWT;

import com.qupworld.paymentgateway.models.RedisDao;
import com.qupworld.paymentgateway.models.RedisImpl;
import com.pg.util.CommonUtils;
import com.pg.util.KeysUtil;
import com.pg.util.SecurityUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

/**
 * Created by thuanho on 20/01/2022.
 */
public class UserService {

    private final static Logger logger = LogManager.getLogger(UserService.class);

    public User getUser(String requestId, String username) {
        RedisDao redisDao = new RedisImpl();
        String data = redisDao.getAuthUser(requestId, username);
        logger.debug(requestId + " - data: " + data);
        if (!data.isEmpty()) {
            try {
                String decrypt = SecurityUtil.decryptProfile(SecurityUtil.getKey(), data);
                String[] arrData = decrypt.split(KeysUtil.TEMPKEY);
                String password = arrData[0];
                String role = arrData[1];
                User user = new User();
                user.setUsername(username);
                user.setPassword(password);
                user.setRoles(Arrays.asList(role));
                return user;
            } catch (Exception ex) {
                logger.debug(requestId + " - exception: " + CommonUtils.getError(ex));
            }
        }
        return null;
    }
}
