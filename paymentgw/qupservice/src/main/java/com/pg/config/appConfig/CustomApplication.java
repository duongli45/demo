package com.pg.config.appConfig;

import com.pg.config.appConfig.provider.AuthenticationFilter;
import com.qupworld.config.ServerConfig;
import com.pg.config.appConfig.provider.GsonMessageBodyHandler;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;

public class CustomApplication extends ResourceConfig
{
	public CustomApplication() 
	{
		System.out.println("CustomApplication");
		packages("com.qupworld.config");
		register(LoggingFeature.class);
		register(GsonMessageBodyHandler.class);
		register(AuthenticationFilter.class);
		ServerConfig.getInstance(null);
        //CronJob.initiateCronJob();

	}

}
