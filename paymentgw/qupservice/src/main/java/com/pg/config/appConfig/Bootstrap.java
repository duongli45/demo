package com.pg.config.appConfig;

import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.config.SwaggerContextService;
import io.swagger.models.Swagger;
import io.swagger.models.auth.BasicAuthDefinition;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by myasus on 6/2/20.
 */
public class Bootstrap extends HttpServlet {
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        String ipAddress = "localhost";
        try {
            InetAddress ip = InetAddress.getLocalHost();
            ipAddress = ip.getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("1.0.2");
        beanConfig.setSchemes(new String[]{"http", "https"});
        beanConfig.setHost(ipAddress + ":8080");
        beanConfig.setBasePath("/qupservice");
        beanConfig.setTitle("Payment API");
        beanConfig.setResourcePackage("io.swagger.resources");
        beanConfig.setScan(true);

        Swagger swagger = new Swagger();
        swagger.securityDefinition("basicAuth", new BasicAuthDefinition());
        new SwaggerContextService().withServletConfig(config).updateSwagger(swagger);
    }
}
