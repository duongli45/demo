package com.pg.config.appConfig.provider;

import com.pg.config.appConfig.provider.JWT.BasicSecurityConext;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.*;
import javax.ws.rs.ext.Provider;

import com.pg.config.appConfig.provider.JWT.JwTokenHelper;
import com.pg.config.appConfig.provider.JWT.User;
import org.glassfish.jersey.internal.util.Base64;
import com.qupworld.config.ServerConfig;

/**
 * This filter verify the access permissions for a user
 * based on username and passowrd provided in request
 * */
@Provider
public class AuthenticationFilter implements javax.ws.rs.container.ContainerRequestFilter
{

	@Context
	private ResourceInfo resourceInfo;

	private static final String AUTHORIZATION_PROPERTY = "Authorization";
	private static final String AUTHENTICATION_BASIC_SCHEME = "Basic";
	private static final String AUTHENTICATION_TOKEN_SCHEME = "Bearer";
	private static final String REALM = "qupworld";

	@Override
	public void filter(ContainerRequestContext requestContext)
	{
		Method method = resourceInfo.getResourceMethod();
		UriInfo info = requestContext.getUriInfo();
		if (info.getPath().contains("swagger")) {
			return;
		}
		//Access allowed for all
		if( ! method.isAnnotationPresent(PermitAll.class))
		{
			//Access denied for all
			if(method.isAnnotationPresent(DenyAll.class))
			{
				requestContext.abortWith(Response.status(Response.Status.FORBIDDEN)
						.entity("Access blocked for all users !!").build());
				return;
			}

			//Get request headers
			final MultivaluedMap<String, String> headers = requestContext.getHeaders();

			//Fetch authorization header
			final List<String> authorization = headers.get(AUTHORIZATION_PROPERTY);

			//If no authorization information present; block access
			if(authorization == null || authorization.isEmpty())
			{
				requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
						.entity("Authentication is required").build());
				return;
			}
			// Validate the Authorization header
			if (isTokenBasedAuthentication(authorization.get(0))) {
				String token = authorization.get(0).substring(AUTHENTICATION_TOKEN_SCHEME.length()).trim();
				try {
					// Validate the token
					if (JwTokenHelper.isTokenExpired(token)) {
						abortWithUnauthorized(requestContext);
						return;
					}
					// Getting the User information from token
					User user = JwTokenHelper.getUserFromToken(token);

					// Overriding the security context of the current request
					SecurityContext oldContext = requestContext.getSecurityContext();
					requestContext.setSecurityContext(new BasicSecurityConext(user, oldContext.isSecure()));
				} catch (Exception e) {
					abortWithUnauthorized(requestContext);
				}
			} else {
				// BASIC AUTHEN for other APIs
				//Get encoded username and password
				final String encodedUserPassword = authorization.get(0).replaceFirst(AUTHENTICATION_BASIC_SCHEME + " ", "");

				//Decode username and password
				String usernameAndPassword = new String(Base64.decode(encodedUserPassword.getBytes()));;

				//Split username and password tokens
				final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
				final String username = tokenizer.nextToken();
				final String password = tokenizer.nextToken();

				//Verify user access
				if(method.isAnnotationPresent(RolesAllowed.class))
				{
					RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
					Set<String> rolesSet = new HashSet<String>(Arrays.asList(rolesAnnotation.value()));

					//Is user valid?
					if( ! isUserAllowed(username, password, rolesSet))
					{
						requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
								.entity("Authentication is required").build());
						return;
					}
				}
			}


		}
	}
	private boolean isUserAllowed(final String username, final String password, final Set<String> rolesSet)
	{
		boolean isAllowed = false;

		//Step 1. Fetch password from database and match with password in argument
		//If both match then get the defined role for user from database and continue; else return isAllowed [false]
		//Access the database and do this part yourself
		//String userRole = userMgr.getUserRole(username);

		if(username.equals(ServerConfig.dispatcher_username) && password.equals(ServerConfig.dispatcher_password))
		{
			String userRole = "dispatcher";

			//Step 2. Verify user role
			if(rolesSet.contains(userRole))
			{
				isAllowed = true;
			}
		}
		if(username.equals(ServerConfig.cc_username) && password.equals(ServerConfig.cc_password))
		{
			String userRole = "cc";

			//Step 2. Verify user role
			if(rolesSet.contains(userRole))
			{
				isAllowed = true;
			}

		}

		return isAllowed;
	}

	private boolean isTokenBasedAuthentication(String authorizationHeader) {

		// Check if the Authorization header is valid
		// It must not be null and must be prefixed with "Bearer" plus a whitespace
		// The authentication scheme comparison must be case-insensitive
		return authorizationHeader != null
				&& authorizationHeader.toLowerCase().startsWith(AUTHENTICATION_TOKEN_SCHEME.toLowerCase() + " ");
	}

	private void abortWithUnauthorized(ContainerRequestContext requestContext) {

		// Abort the filter chain with a 401 status code response
		// The WWW-Authenticate header is sent along with the response
		Response response = Response.status(Response.Status.UNAUTHORIZED) // 401 Unauthorized
				.header(HttpHeaders.WWW_AUTHENTICATE, AUTHENTICATION_TOKEN_SCHEME + " realm=\"" + REALM + "\"")
				.entity("You cannot access this resource") // the response entity
				.build();
		requestContext.abortWith(response);
	}
}

